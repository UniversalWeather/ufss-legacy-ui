﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.CommonService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PreflightService;
using System.Globalization;
using Telerik.Web.UI;
using System.Collections.ObjectModel;
using FlightPak.Common;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Data.SqlTypes;
using FlightPak.Web.Entities.SchedulingCalendar;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class ScheduleCalendarBase : BaseSecuredPage
    {
        protected List<FlightPak.Web.CommonService.FlightCatagory> FlightCategories = new List<CommonService.FlightCatagory>();
        protected List<FlightPak.Web.CommonService.AircraftDuty> AircraftDutyTypes = new List<CommonService.AircraftDuty>();
        protected List<FlightPak.Web.CommonService.CrewDutyType> CrewDutyTypes = new List<CommonService.CrewDutyType>();
        protected FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria FilterOptions = new Preflight.ScheduleCalendar.FilterCriteria();
        protected FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.DisplayOptions DisplayOptions = new Preflight.ScheduleCalendar.DisplayOptions();

        protected DateTime StartDate = new DateTime();
        protected DateTime EndDate = new DateTime();
        protected string DefaultView = string.Empty;
        protected System.Globalization.DateTimeFormatInfo DateTimeInfo = new System.Globalization.DateTimeFormatInfo();
        protected DateTimeFormatInfo DTF = new DateTimeFormatInfo();
        protected DateTime MinDate = new DateTime();
        protected DateTime MaxDate = new DateTime();

        protected string ApplicationDateFormat = null;
        protected Int64? HomeBaseId = null;
        protected Int64? ClientId = null;
        protected string HomeBaseCode = null;
        protected string BaseDescription = null;
        protected string ClientCode = null;
        protected string DispatcherName = null;
        protected string DispatcherDescription = null;
        protected Int64? HomeAirportId = null;
        protected bool IsTripPrivacyApplied;
        protected ExceptionManager exManager;

        protected DateTime DateInput = new DateTime();
        //get the values set for Private trip in Company profile for the Homebase...
        protected string PrivateTripSettingsByCompanyProfile;
        protected PrivateTrip PrivateTrip = new PrivateTrip();
        protected double ETERound = 0.0;
        protected CommonService.SCWrapper _scWrapper;

        protected CommonService.SCWrapper scWrapper
        {
            get
            {
                return _scWrapper;
            }
        }
        protected void Page_PreLoad(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //To check the page level access.
                        CheckAutorization(Permission.Preflight.ViewCalendar);

                        using (var commonService = new CommonServiceClient())
                        {
                            _scWrapper = commonService.GetSchedulingCalendarModal();
                        }

                        //To display in tripwriter
                        Session["TabSelect"] = "PreFlight";
                        // get user filter settings and send it as filter object

                        if (Session["SCUserSettings"] == null)
                        {
                            AdvancedFilterSettings settings = GetUserSettings();
                            Session["SCUserSettings"] = settings;
                            FilterOptions = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria)settings.Filters;
                            DisplayOptions = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.DisplayOptions)settings.Display;
                        }
                        else
                        {
                            AdvancedFilterSettings settings = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.AdvancedFilterSettings)Session["SCUserSettings"];
                            FilterOptions = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria)settings.Filters;
                            DisplayOptions = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.DisplayOptions)settings.Display;
                        }

                        //// set category code details...
                        //  using (var masterCatalogueServiceClient = new MasterCatalogServiceClient())
                        //    {
                        //        var dutyTypes = masterCatalogueServiceClient.GetFlightCategoryList();
                        //        if (dutyTypes != null)
                        //        {
                        //            FlightCategories = dutyTypes.EntityList;
                        //        }
                        //    }
                        if (null != scWrapper.flightCategory) FlightCategories = scWrapper.flightCategory.EntityList;
                        if (null != scWrapper.aircraftDuty) AircraftDutyTypes = scWrapper.aircraftDuty.EntityList;
                        //using (var masterCatalogueServiceClient = new MasterCatalogServiceClient())
                        //{
                        //    var dutyTypes = masterCatalogueServiceClient.GetAircraftDuty();
                        //    if (dutyTypes != null)
                        //    {
                        //        AircraftDutyTypes = dutyTypes.EntityList;
                        //    }
                        //}

                        if (null != scWrapper.crewDutyType) CrewDutyTypes = scWrapper.crewDutyType.EntityList;
                        //using (var masterCatalogueServiceClient = new MasterCatalogServiceClient())
                        //{
                        //    var dutyTypes = masterCatalogueServiceClient.GetCrewDutyTypeList();
                        //    if (dutyTypes != null)
                        //    {
                        //        CrewDutyTypes = dutyTypes.EntityList;
                        //    }
                        //}


                        HomeBaseId = UserPrincipal.Identity._homeBaseId;
                        ClientId = UserPrincipal.Identity._clientId;
                        HomeAirportId = UserPrincipal.Identity._airportId;
                        ClientCode = UserPrincipal.Identity._clientCd;
                        HomeBaseCode = UserPrincipal.Identity._airportICAOCd;
                        IsTripPrivacyApplied = UserPrincipal.Identity._isTripPrivacy;
                        BaseDescription = UserPrincipal.Identity._fpSettings._BaseDescription;
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat == null)
                        {
                            ApplicationDateFormat = "MM/dd/yyyy";
                        }
                        else
                        {
                            ApplicationDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }

                        DTF.ShortDatePattern = ApplicationDateFormat;
                        DateTimeInfo.ShortDatePattern = ApplicationDateFormat;
                        MinDate = ((DateTime)SqlDateTime.MinValue).AddYears(1);
                        MaxDate = ((DateTime)SqlDateTime.MaxValue).AddYears(-1);

                        Session["SCHomeBaseID"] = HomeBaseId;
                        Session["SCHomeBaseCD"] = HomeBaseCode;
                        Session["SCClientID"] = ClientId;
                        Session["SCClientCD"] = ClientCode;
                        Session["SCIsHomeBaseDeActivated"] = UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter;

                        // GET PRIVATETRIP COLUMN VALUE FROM COMPANY PROFILE

                        if (Session["PrivateTrip_CompanyProfileSettings"] == null)
                        {
                            using (var masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var privacySettings = masterServiceClient.GetTripPrivacySettings();
                                if (privacySettings.EntityInfo != null)
                                {
                                    PrivateTripSettingsByCompanyProfile = privacySettings.EntityInfo.PrivateTrip;
                                }
                            }

                            if (string.IsNullOrEmpty(PrivateTripSettingsByCompanyProfile))
                            {
                                //PrivateTripSettingsByCompanyProfile = "Arrival/Dept ICAO/City/Airport Name,T" + Environment.NewLine + "Arrival/Dept Time,T" + Environment.NewLine + "Crew,T" + Environment.NewLine + "Flight Number,T" + Environment.NewLine + "Leg Purpose,T" + Environment.NewLine + "Trip Purpose,T" + Environment.NewLine + "Seats Available,T" + Environment.NewLine + "Pax Count,T" + Environment.NewLine + "Requestor,T" + Environment.NewLine + "Department,T" + Environment.NewLine + "Authorization,T" + Environment.NewLine + "Flight Category,T" + Environment.NewLine + "Cumulative ETE,T" + Environment.NewLine + "ETE,T";
                                PrivateTripSettingsByCompanyProfile = "Arrival/Dept ICAO/City/Airport Name,T" + Environment.NewLine + "Arrival/Dept Time,T" + Environment.NewLine + "Crew,T" + Environment.NewLine + "Flight Number,T" + Environment.NewLine + "Leg Purpose,T" + Environment.NewLine + "Trip Purpose,T" + Environment.NewLine + "Seats Available,T" + Environment.NewLine + "Pax Count,T" + Environment.NewLine + "Requestor,T" + Environment.NewLine + "Department,T" + Environment.NewLine + "Authorization,T" + Environment.NewLine + "Flight Category,T" + Environment.NewLine + "Total Trip ETE,T";
                            }
                            Session["PrivateTrip_CompanyProfileSettings"] = PrivateTripSettingsByCompanyProfile;
                        }
                        else
                        {
                            PrivateTripSettingsByCompanyProfile = Session["PrivateTrip_CompanyProfileSettings"].ToString();
                        }

                        // set PrivateTripSettingsByCompanyProfile to PrivateTrip entity to reuse in all views...
                        SetTripPrivacyDetails();

                        // Redirect to respective calendar view based on Default view selected by User...
                        //using (var commonServiceClient = new CommonServiceClient())
                        //{
                        //    DefaultView = commonServiceClient.GetUserDefaultCalendarView();

                        //}
                        DefaultView = scWrapper.userDefaultCalendarView;                        

                        ETERound = GetElapseTMRoundingFromSettings();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendarBase);
                }
            }

        }

        private void SetTripPrivacyDetails()
        {
            string[] privateTripProperties = PrivateTripSettingsByCompanyProfile.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var privateTripDictionary = new Dictionary<string, string>();
            for (int i = 0; i < privateTripProperties.Length; i++)
            {
                string[] propertyArray = (privateTripProperties[i].ToString()).Split(",".ToCharArray());
                privateTripDictionary.Add(propertyArray[0], propertyArray[1]);

                switch (i)
                {

                    case 0:
                        PrivateTrip.IsShowArrivalDepartICAO = privateTripDictionary.ContainsKey("Arrival/Dept ICAO/City/Airport Name") && privateTripDictionary["Arrival/Dept ICAO/City/Airport Name"].Trim().Equals("T") ? true : false;
                        break;

                    case 1:
                        PrivateTrip.IsShowArrivalDepartTime = privateTripDictionary.ContainsKey("Arrival/Dept Time") && privateTripDictionary["Arrival/Dept Time"].Trim().Equals("T") ? true : false;
                        break;

                    case 2:
                        PrivateTrip.IsShowCrew = privateTripDictionary.ContainsKey("Crew") && privateTripDictionary["Crew"].Trim().Equals("T") ? true : false;
                        break;

                    case 3:
                        PrivateTrip.IsShowFlightNumber = privateTripDictionary.ContainsKey("Flight Number") && privateTripDictionary["Flight Number"].Trim().Equals("T") ? true : false;
                        break;

                    case 4:
                        PrivateTrip.IsShowLegPurpose = privateTripDictionary.ContainsKey("Leg Purpose") && privateTripDictionary["Leg Purpose"].Trim().Equals("T") ? true : false;
                        break;

                    case 5:
                        PrivateTrip.IsShowTripPurpose = privateTripDictionary.ContainsKey("Trip Purpose") && privateTripDictionary["Trip Purpose"].Trim().Equals("T") ? true : false;
                        break;

                    case 6:
                        PrivateTrip.IsShowSeatsAvailable = privateTripDictionary.ContainsKey("Seats Available") && privateTripDictionary["Seats Available"].Trim().Equals("T") ? true : false;
                        break;

                    case 7:
                        PrivateTrip.IsShowPaxCount = privateTripDictionary.ContainsKey("Pax Count") && privateTripDictionary["Pax Count"].Trim().Equals("T") ? true : false;
                        break;

                    case 8:
                        PrivateTrip.IsShowRequestor = privateTripDictionary.ContainsKey("Requestor") && privateTripDictionary["Requestor"].Trim().Equals("T") ? true : false;
                        break;

                    case 9:
                        PrivateTrip.IsShowDepartment = privateTripDictionary.ContainsKey("Department") && privateTripDictionary["Department"].Trim().Equals("T") ? true : false;
                        break;

                    case 10:
                        PrivateTrip.IsShowAuthorization = privateTripDictionary.ContainsKey("Authorization") && privateTripDictionary["Authorization"].Trim().Equals("T") ? true : false;
                        break;

                    case 11:
                        PrivateTrip.IsShowFlightCategory = privateTripDictionary.ContainsKey("Flight Category") && privateTripDictionary["Flight Category"].Trim().Equals("T") ? true : false;
                        break;

                    case 12:
                        PrivateTrip.IsShowCumulativeETE = privateTripDictionary.ContainsKey("Total Trip ETE") && privateTripDictionary["Total Trip ETE"].Trim().Equals("T") ? true : false;
                        break;

                    //case 13:
                    //    string ete = privateTripDictionary["ETE"];
                    //    PrivateTrip.IsShowETE = ete.Equals("T".Trim()) ? true : false;
                    //    break;                   
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Changes done for the Bug ID - 5828
                        Session["IsFleetChecked"] = true;
                        if (DefaultView == null)
                        {
                            DefaultView = ModuleNameConstants.Preflight.WeeklyFleet;
                            Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                            Response.Redirect("SchedulingCalendar.aspx");
                        }
                        else
                        {
                            switch (DefaultView)
                            {
                                case ModuleNameConstants.Preflight.WeeklyCrew:
                                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;
                                    Response.Redirect("WeeklyCrew.aspx");
                                    break;

                                case ModuleNameConstants.Preflight.WeeklyMain:
                                    Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;
                                    Response.Redirect("WeeklyMain.aspx");
                                    break;

                                case ModuleNameConstants.Preflight.WeeklyDetail:
                                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                                    Response.Redirect("WeeklyDetail.aspx");
                                    break;

                                case ModuleNameConstants.Preflight.Month:
                                    Response.Redirect("Monthly.aspx?seltab=Monthly");
                                    break;

                                case ModuleNameConstants.Preflight.BusinessWeek:
                                    Response.Redirect("BusinessWeek.aspx?seltab=BusinessWeek");
                                    break;

                                case ModuleNameConstants.Preflight.Corporate:
                                    Response.Redirect("CorporateView.aspx?seltab=Corporate");
                                    break;

                                case ModuleNameConstants.Preflight.Day:
                                    Response.Redirect("DayView.aspx?seltab=Day");
                                    break;

                                case ModuleNameConstants.Preflight.Planner:
                                    Response.Redirect("Planner.aspx?seltab=Planner");
                                    break;

                                default:
                                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                                    Response.Redirect("SchedulingCalendar.aspx");
                                    break;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendarBase);
                }
            }


        }

        // To get logged in user's filter and display options string from database
        public static AdvancedFilterSettings GetUserSettings()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                DisplayOptionsPopup advanceFilters = new DisplayOptionsPopup();
                var userSettingsString = advanceFilters.GetUserFilterAndDisplayOptions();
                var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);

                return options;

            }

        }

        //To get start date of the week.. (startDay = Monday)
        public DateTime GetWeekStartDate(DateTime selectedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedDate))
            {
                return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1);
            }

        }

        // To get end day of the week...
        public DateTime GetWeekEndDate(DateTime weekStartDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                return weekStartDate.AddDays(7).AddSeconds(-1);

            }

        }

        //To convert web entity to service entity
        public static PreflightService.FilterCriteria ConvertToServiceFilterCriteria(FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(filterCriteria))
            {

                PreflightService.FilterCriteria serviceFilterCriteria = new PreflightService.FilterCriteria();
                serviceFilterCriteria.AllCrew = filterCriteria.AllCrew;
                serviceFilterCriteria.Canceled = filterCriteria.Canceled;
                serviceFilterCriteria.Client = filterCriteria.Client;
                serviceFilterCriteria.ClientID = filterCriteria.ClientID;
                serviceFilterCriteria.CrewDuty = filterCriteria.CrewDuty;
                serviceFilterCriteria.CrewDutyID = filterCriteria.CrewDutyID;
                serviceFilterCriteria.Department = filterCriteria.Department;
                serviceFilterCriteria.DepartmentID = filterCriteria.DepartmentID;
                serviceFilterCriteria.FixedWingCrewOnly = filterCriteria.FixedWingCrewOnly;
                serviceFilterCriteria.FlightCategory = filterCriteria.FlightCategory;
                serviceFilterCriteria.FlightCategoryID = filterCriteria.FlightCategoryID;
                serviceFilterCriteria.Hold = filterCriteria.Hold;
                serviceFilterCriteria.HomeBase = filterCriteria.HomeBase;
                serviceFilterCriteria.HomeBaseID = filterCriteria.HomeBaseID;
                serviceFilterCriteria.HomeBaseOnly = filterCriteria.HomeBaseOnly;
                serviceFilterCriteria.Requestor = filterCriteria.Requestor;
                serviceFilterCriteria.RequestorID = filterCriteria.RequestorID;
                serviceFilterCriteria.RotaryWingCrewCrewOnly = filterCriteria.RotaryWingCrewCrewOnly;
                serviceFilterCriteria.SchedServ = filterCriteria.SchedServ;
                serviceFilterCriteria.TimeBase = (FlightPak.Web.PreflightService.TimeBase)filterCriteria.TimeBase;
                serviceFilterCriteria.Trip = filterCriteria.Trip;
                serviceFilterCriteria.UnFulfilled = filterCriteria.UnFulfilled;
                serviceFilterCriteria.VendorsFilter = (FlightPak.Web.PreflightService.VendorOption)filterCriteria.VendorsFilter;
                serviceFilterCriteria.WorkSheet = filterCriteria.WorkSheet;
                return serviceFilterCriteria;

            }
        }

        // to get checked tree nodes from tree view...
        public Collection<TreeviewParentChildNodePair> GetCheckedTreeNodes(RadTreeView treeView)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(treeView))
            {
                Collection<TreeviewParentChildNodePair> selectedNodes = new Collection<TreeviewParentChildNodePair>();
                IList<RadTreeNode> nodeCollection = treeView.CheckedNodes;
                string parentNode = string.Empty;
                string childNode = string.Empty;
                foreach (RadTreeNode node in nodeCollection)
                {
                    if (node.Nodes.Count == 0) // to avoid adding node group headers...
                    {
                        parentNode = node.ParentNode.Value.Contains('.') ? node.ParentNode.Value.Split('.')[0] : node.ParentNode.Value;
                        childNode = node.Value.Contains('.') ? node.Value.Split('.')[1] : node.Value;
                        if (node.ParentNode!= null)
                            selectedNodes.Add(new TreeviewParentChildNodePair(parentNode, childNode));
                    }
                }
                return selectedNodes;
            }
        }

        // to get checked tree nodes from tree view...
        public Collection<TreeviewParentChildNodePair> GetCheckedTreeNodes(RadTreeView treeView, bool isGroup)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(treeView))
            {
                Collection<TreeviewParentChildNodePair> selectedNodes = new Collection<TreeviewParentChildNodePair>();
                IList<RadTreeNode> nodeCollection = treeView.CheckedNodes;
                string parentNode = string.Empty;
                string childNode = string.Empty;
                foreach (RadTreeNode node in nodeCollection)
                {
                    if (node.Nodes.Count == 0) // to avoid adding node group headers...
                    {
                        parentNode = node.ParentNode.Value.Contains('.') ? node.ParentNode.Value.Split('.')[0] : node.ParentNode.Value;
                        childNode = node.Value.Contains('.') ? node.Value.Split('.')[1] : node.Value;
                        if (!isGroup)
                        {
                            if (node.Value.Contains('.'))
                            {
                                //string[] crewgroupIDs = node.Value.Split('.');
                                if (!string.IsNullOrEmpty(childNode))
                                    selectedNodes.Add(new TreeviewParentChildNodePair(parentNode, childNode));
                            }
                            else
                                selectedNodes.Add(new TreeviewParentChildNodePair(parentNode, childNode));
                        }
                        else
                            selectedNodes.Add(new TreeviewParentChildNodePair(parentNode, childNode));
                    }
                }
                return selectedNodes;
            }
        }

        //To Check whether the Entire Fleet is checked
        public bool IsEntireCrewChecked(RadTreeView treeView)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(treeView))
            {
                bool IsChecked = false;
                bool IsEntireFleetChecked = false;
                bool IsSubCrewChecked = false; 
                Collection<string> selectedNodes = new Collection<string>();
                IList<RadTreeNode> nodeCollection = treeView.CheckedNodes;
                foreach (RadTreeNode node in nodeCollection)
                {
                    if (node.ParentNode == null && node.Level == 0 && node.CheckState == TreeNodeCheckState.Checked)
                        IsEntireFleetChecked = true;

                    if (node.Nodes.Count > 0 && node.CheckState == TreeNodeCheckState.Checked && node.Level == 1)
                    {
                        IsSubCrewChecked = true;
                        break; 
                    }
                }
                //If both of the items are checked given preference to entire fleet
                if (IsEntireFleetChecked == true && IsSubCrewChecked == true)
                    IsChecked = true; 
                else if (IsEntireFleetChecked == true && IsSubCrewChecked == false)
                    IsChecked = true;
                else if (IsEntireFleetChecked == false && IsSubCrewChecked == true)
                    IsChecked = false;
                else
                    IsChecked = false;

                return IsChecked;
            }
        }

        public FlightPak.Web.Entities.SchedulingCalendar.Appointment SetCommonCrewProperties(List<CrewCalendarDataResult> calendarData, string timeBase, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;
                appointment.TripId = tripLeg.TripID;
                appointment.TailNum = tripLeg.TailNum;
                appointment.Notes = tripLeg.Notes;
                appointment.TripStatus = tripLeg.TripStatus;
                appointment.LegNum = tripLeg.LegNUM.Value;

                appointment.LastUpdatedUser = tripLeg.LastUpdUID;

                appointment.DepartureDisplayTime = tripLeg.DepartureDisplayTime.Value;
                appointment.ArrivalDisplayTime = tripLeg.ArrivalDisplayTime.Value;
                appointment.StartDate = appointment.DepartureDisplayTime;
                appointment.EndDate = appointment.ArrivalDisplayTime;
                if (appointment.EndDate < appointment.StartDate) // this condition will occur only in Local time base
                {
                    appointment.EndDate = appointment.StartDate + (tripLeg.ArrivalGreenwichDTTM.Value - tripLeg.DepartureGreenwichDTTM.Value);
                    if (appointment.EndDate.Date > appointment.ArrivalDisplayTime.Date) // if calculated endDate > original end date...
                    {
                        appointment.EndDate = appointment.DepartureDisplayTime.Date.AddHours(23).AddMinutes(59);
                    }
                    if (appointment.EndDate.Date < appointment.StartDate.Date)
                    {
                        appointment.EndDate = appointment.StartDate.AddMinutes(30);
                    }
                    else if ((appointment.EndDate.Date == appointment.StartDate.Date) && (appointment.EndDate.Hour < appointment.StartDate.Hour))
                    {
                        appointment.EndDate = appointment.StartDate.AddHours(1);
                        if ((appointment.EndDate.Hour == appointment.StartDate.Hour) && (appointment.EndDate.Minute < appointment.StartDate.Minute))
                        {
                            appointment.EndDate = appointment.StartDate.AddMinutes(30);
                        }
                    }
                }

                appointment.ArrivalAirportName = tripLeg.ArrivalAirportName;
                appointment.ArrivalCity = tripLeg.ArrivalCity;
                appointment.ArrivalCountry = tripLeg.ArrivalCountry;
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArrivalICAOID) ? tripLeg.ArrivalICAOID : string.Empty;
                appointment.DepartureICAOID = !string.IsNullOrEmpty(tripLeg.DepartureICAOID) ? tripLeg.DepartureICAOID : string.Empty;
                appointment.AuthorizationCD = tripLeg.AuthorizationCD;
                appointment.AuthorizationId = tripLeg.AuthorizationID.HasValue ? tripLeg.AuthorizationID.Value : 0;
                

                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.AircraftDutyCD = tripLeg.AircraftDutyCD;

                appointment.HomebaseCD = tripLeg.HomebaseCD;
                appointment.HomebaseID = tripLeg.HomebaseID.HasValue ? tripLeg.HomebaseID.Value : 0;

                appointment.AircraftDutyDescription = tripLeg.AircraftDutyDescription;
                appointment.AircraftDutyBackColor = tripLeg.AircaftDutyBackColor;
                appointment.AircraftDutyForeColor = tripLeg.AircraftDutyForeColor;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;

                appointment.ClientCD = tripLeg.ClientCD;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.CrewCD = tripLeg.CrewCD;
                appointment.CrewId = tripLeg.CrewID.HasValue ? tripLeg.CrewID.Value : 0;
                appointment.CrewLastName = tripLeg.CrewLastName;
                appointment.CrewMiddleName = tripLeg.CrewMiddleName;
                appointment.CrewFirstName = tripLeg.CrewFirstName;
                appointment.CrewNotes = tripLeg.CrewNotes;
                appointment.CrewCodes = tripLeg.CrewCodes;
                if (!string.IsNullOrEmpty(tripLeg.CrewFullNames))
                {
                    string names = tripLeg.CrewFullNames.Replace(";", "</br>");
                    appointment.CrewFullNames = names;
                }

                appointment.CrewDutyTypeCD = tripLeg.CrewDutyTypeCD;
                appointment.CrewDutyType = tripLeg.CrewDutyID.HasValue ? tripLeg.CrewDutyID.Value : 0;
                appointment.CrewDutyTypeDescription = tripLeg.CrewDutyTypeDescription;
                appointment.CrewDutyBackColor = tripLeg.CrewDutyTypeBackColor;
                appointment.CrewDutyForeColor = tripLeg.CrewDutyTypeForeColor;
                appointment.DepartmentCD = !string.IsNullOrEmpty(tripLeg.DepartmentCD) ? tripLeg.DepartmentCD : string.Empty;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.DepartureAirportName = tripLeg.DepartureAirportName;
                appointment.DepartureCity = tripLeg.DepartureCity;
                appointment.DepartureCountry = tripLeg.DepartureCountry;

                appointment.ElapseTM = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.ETE = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.TotalETE = tripLeg.TotalETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.TotalETE.Value)) : string.Empty;

                appointment.CumulativeETE = tripLeg.CumulativeETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.CumulativeETE.Value)) : "0";
                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.FlightCategoryCode = !string.IsNullOrEmpty(tripLeg.FlightCatagoryCD) ? tripLeg.FlightCatagoryCD : string.Empty;
                appointment.FlightCategoryDescription = tripLeg.FlightCatagoryDescription;
                appointment.FlightCategoryBackColor = tripLeg.FlightCategoryBackColor;
                appointment.FlightCategoryForeColor = tripLeg.FlightCategoryForeColor;
                appointment.FlightNum = !string.IsNullOrEmpty(tripLeg.FlightNUM) ? tripLeg.FlightNUM : string.Empty;
                appointment.FlightPurpose = tripLeg.FlightPurpose;
                appointment.TripPurpose  = tripLeg.Description;

                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();

                appointment.IsRONAppointment = tripLeg.LegNUM == 0 ? true : false;
                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;
                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg) //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false; // SHOW details only based on company profile setting - Trip privacy settings
                }
                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }
                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();

                appointment.Notes = !string.IsNullOrEmpty(tripLeg.Notes) ? tripLeg.Notes : string.Empty;
                appointment.PassengerCount = tripLeg.PassengerTotal.HasValue ? tripLeg.PassengerTotal.Value : 0;
                appointment.PassengerRequestorCD = !string.IsNullOrEmpty(tripLeg.PassengerRequestorCD) ? tripLeg.PassengerRequestorCD : string.Empty;
                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                appointment.PassengerCodes = tripLeg.PassengerCodes;
                if (!string.IsNullOrEmpty(tripLeg.PassengerNames))
                {
                    string names = tripLeg.PassengerNames.Replace(";", "</br>");
                    appointment.PassengerNames = names;
                }
                appointment.RecordType = tripLeg.RecordType;
                appointment.ReservationAvailable = tripLeg.ReservationAvailable.HasValue ? tripLeg.ReservationAvailable.Value : 0;
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();

                return appointment;

            }

        }

        public FlightPak.Web.Entities.SchedulingCalendar.Appointment SetFleetCommonProperties(List<FlightPak.Web.PreflightService.FleetCalendarDataResult> calendarData, string timeBase, FlightPak.Web.PreflightService.FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;
                appointment.TripId = tripLeg.TripID;
                appointment.TailNum = tripLeg.TailNum;
                appointment.Notes = tripLeg.Notes;
                appointment.TripStatus = tripLeg.TripStatus;

                appointment.DepartureDisplayTime = tripLeg.DepartureDisplayTime.Value;
                appointment.ArrivalDisplayTime = tripLeg.ArrivalDisplayTime.Value;
                appointment.StartDate = appointment.DepartureDisplayTime;
                appointment.EndDate = appointment.ArrivalDisplayTime;
                if (appointment.EndDate < appointment.StartDate) // this condition will occur only in Local time base
                {
                    appointment.EndDate = appointment.StartDate + (tripLeg.ArrivalGreenwichDTTM.Value - tripLeg.DepartureGreenwichDTTM.Value);
                    if (appointment.EndDate.Date > appointment.ArrivalDisplayTime.Date) // if calculated endDate > original end date...
                    {
                        appointment.EndDate = appointment.DepartureDisplayTime.Date.AddHours(23).AddMinutes(59);
                    }
                    if (appointment.EndDate.Date < appointment.StartDate.Date)
                    {
                        appointment.EndDate = appointment.StartDate.AddMinutes(30);
                    }
                    else if ((appointment.EndDate.Date == appointment.StartDate.Date) && (appointment.EndDate.Hour < appointment.StartDate.Hour))
                    {
                        appointment.EndDate = appointment.StartDate.AddHours(1);
                        if ((appointment.EndDate.Hour == appointment.StartDate.Hour) && (appointment.EndDate.Minute < appointment.StartDate.Minute))
                        {
                            appointment.EndDate = appointment.StartDate.AddMinutes(30);
                        }
                    }
                }


                appointment.ArrivalAirportName = tripLeg.ArrivalAirportName;
                appointment.ArrivalCity = tripLeg.ArrivalCity;
                appointment.ArrivalCountry = tripLeg.ArrivalCountry;
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArrivalICAOID) ? tripLeg.ArrivalICAOID : string.Empty;
                appointment.DepartureICAOID = !string.IsNullOrEmpty(tripLeg.DepartureICAOID) ? tripLeg.DepartureICAOID : string.Empty;
                appointment.DepartureAirportName = tripLeg.DepartureAirportName;
                appointment.DepartureCity = tripLeg.DepartureCity;
                appointment.DepartureCountry = tripLeg.DepartureCountry;

                appointment.AuthorizationCD = tripLeg.AuthorizationCD;
                appointment.AuthorizationId = tripLeg.AuthorizationID.HasValue ? tripLeg.AuthorizationID.Value : 0;                

                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.AircraftDutyCD = tripLeg.AircraftDutyCD;
                
                appointment.HomebaseCD = tripLeg.HomebaseCD;
                appointment.HomebaseID = tripLeg.HomebaseID.HasValue ? tripLeg.HomebaseID.Value : 0;

                appointment.AircraftDutyDescription = tripLeg.AircraftDutyDescription;
                appointment.AircraftDutyBackColor = tripLeg.AircaftDutyBackColor;
                appointment.AircraftDutyForeColor = tripLeg.AircraftDutyForeColor;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;

                appointment.ClientCD = tripLeg.ClientCD;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;

                appointment.CrewCodes = tripLeg.CrewCodes;
                if (!string.IsNullOrEmpty(tripLeg.CrewFullNames))
                {
                    string names = tripLeg.CrewFullNames.Replace(";", "</br>");
                    appointment.CrewFullNames = names;
                }
                appointment.CrewDutyType = tripLeg.CrewDutyID.HasValue ? tripLeg.CrewDutyID.Value : 0;
                appointment.CrewDutyTypeCD = tripLeg.CrewDutyTypeCD;
                appointment.CrewDutyTypeDescription = tripLeg.CrewDutyTypeDescription;
                appointment.CrewDutyBackColor = tripLeg.CrewDutyTypeBackColor;
                appointment.CrewDutyForeColor = tripLeg.CrewDutyTypeForeColor;
                appointment.DepartmentCD = !string.IsNullOrEmpty(tripLeg.DepartmentCD) ? tripLeg.DepartmentCD : string.Empty;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;

                appointment.ElapseTM = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.CumulativeETE = tripLeg.CumulativeETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.CumulativeETE.Value)) : "0";
                appointment.TotalETE = tripLeg.TotalETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.TotalETE.Value)) : "0";
                appointment.ETE = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : "0";

                appointment.FleetNotes = tripLeg.FleetNotes;

                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.FlightCategoryCode = !string.IsNullOrEmpty(tripLeg.FlightCatagoryCD) ? tripLeg.FlightCatagoryCD : string.Empty;
                appointment.FlightCategoryDescription = tripLeg.FlightCatagoryDescription;
                appointment.FlightCategoryBackColor = tripLeg.FlightCategoryBackColor;
                appointment.FlightCategoryForeColor = tripLeg.FlightCategoryForeColor;
                appointment.FlightNum = !string.IsNullOrEmpty(tripLeg.FlightNUM) ? tripLeg.FlightNUM : string.Empty;
                appointment.FlightPurpose = tripLeg.FlightPurpose;
                appointment.TripPurpose  = tripLeg.Description;
                appointment.FuelLoad = tripLeg.FuelLoad;

                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();

                // appointment.IsRONAppointment = tripLeg.IsAircraftStandby.HasValue ? tripLeg.IsAircraftStandby.Value : false;
                appointment.IsRONAppointment = tripLeg.LegNUM == 0 ? true : false;
                appointment.IsStandByCrew = tripLeg.IsCrewStandBy.HasValue ? tripLeg.IsCrewStandBy.Value : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;
                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg) //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false; // SHOW details only based on company profile setting - Trip privacy settings
                }
                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }


                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.LastUpdatedTime = tripLeg.LastUpdTS.HasValue ? tripLeg.LastUpdTS.Value.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture) : string.Empty;
                appointment.LastUpdatedUser = tripLeg.LastUpdUID;
                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();

                appointment.Notes = !string.IsNullOrEmpty(tripLeg.Notes) ? tripLeg.Notes : string.Empty;
                appointment.OutboundInstruction = tripLeg.OutbountInstruction;
                appointment.PassengerCount = tripLeg.PassengerTotal.HasValue ? tripLeg.PassengerTotal.Value : 0;
                appointment.PassengerRequestorCD = !string.IsNullOrEmpty(tripLeg.PassengerRequestorCD) ? tripLeg.PassengerRequestorCD : string.Empty;
                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                appointment.PassengerCodes = tripLeg.PassengerCodes;
                if (!string.IsNullOrEmpty(tripLeg.PassengerNames))
                {
                    string names = tripLeg.PassengerNames.Replace(";", "</br>");
                    appointment.PassengerNames = names;
                }

                appointment.ReservationAvailable = tripLeg.ReservationAvailable.HasValue ? tripLeg.ReservationAvailable.Value : 0;
                appointment.RecordType = tripLeg.RecordType;
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();
                appointment.CrewFuelLoad = !string.IsNullOrEmpty(tripLeg.CrewFuelLoad) ? tripLeg.CrewFuelLoad : string.Empty;
                return appointment;
            }
        }

        public FlightPak.Web.Entities.SchedulingCalendar.Appointment SetFleetPlannerCommonProperties(List<FlightPak.Web.PreflightService.FleetPlannerCalendarDataResult> calendarData, string timeBase, FlightPak.Web.PreflightService.FleetPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;
                appointment.TripId = Convert.ToInt64(tripLeg.TripID);
                appointment.TailNum = tripLeg.TailNum;
                //appointment.Notes = tripLeg.Notes;
                appointment.Notes = !string.IsNullOrEmpty(tripLeg.Notes) ? tripLeg.Notes : string.Empty;

                appointment.TripStatus = tripLeg.TripStatus;

                appointment.StartDate = (DateTime)tripLeg.PlannerColumnPeriodStart;
                appointment.EndDate = (DateTime)tripLeg.PlannerColumnPeriodEnd;
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArriveICAO) ? tripLeg.ArriveICAO : string.Empty;
                //appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArrivalICAOID) ? tripLeg.ArrivalICAOID : string.Empty;

                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.CrewDutyTypeCD = tripLeg.DutyTYPE;
                //appointment.CrewDutyTypeCD = tripLeg.CrewDutyTypeCD;

                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;                

                appointment.CrewDutyType = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                //appointment.CrewDutyType = tripLeg.CrewDutyID.HasValue ? tripLeg.CrewDutyID.Value : 0;
                
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.DepartmentCD = !string.IsNullOrEmpty(tripLeg.DepartmentCD) ? tripLeg.DepartmentCD : string.Empty;

                appointment.CrewCodes = tripLeg.CrewCodes;
                if (!string.IsNullOrEmpty(tripLeg.CrewFullNames))
                {
                    string names = tripLeg.CrewFullNames.Replace(";", "</br>");
                    appointment.CrewFullNames = names;
                }

                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();                

                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg)  //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false;// SHOW details only based on company profile setting - Trip privacy settings
                }

                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }
               
                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();                

                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                //appointment.Description = tripLeg.AppointmentDisplay;
                if (tripLeg.LegID == 9999)
                {
                    appointment.Description = "R";
                }
                else
                {
                    appointment.Description = tripLeg.AppointmentDisplay;
                }
                appointment.RecordType = tripLeg.RecordType;
                appointment.TailNum = tripLeg.TailNum;
                //appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat);
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();
                

                //Other Values
                appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.DepartureDisplayTime = tripLeg.DepartureDisplayTime.Value;
                appointment.ArrivalDisplayTime = tripLeg.ArrivalDisplayTime.Value;
                /*appointment.StartDate = appointment.DepartureDisplayTime;
                appointment.EndDate = appointment.ArrivalDisplayTime;
                if (appointment.EndDate < appointment.StartDate) // this condition will occur only in Local time base
                {
                    appointment.EndDate = appointment.StartDate + (tripLeg.ArrivalGreenwichDTTM.Value - tripLeg.DepartureGreenwichDTTM.Value);
                    if (appointment.EndDate.Date > appointment.ArrivalDisplayTime.Date) // if calculated endDate > original end date...
                    {
                        appointment.EndDate = appointment.DepartureDisplayTime.Date.AddHours(23).AddMinutes(59);
                    }
                }*/


                appointment.ArrivalAirportName = tripLeg.ArrivalAirportName;
                appointment.ArrivalCity = tripLeg.ArrivalCity;
                appointment.ArrivalCountry = tripLeg.ArrivalCountry;

                appointment.DepartureICAOID = !string.IsNullOrEmpty(tripLeg.DepartureICAOID) ? tripLeg.DepartureICAOID : string.Empty;
                appointment.DepartureAirportName = tripLeg.DepartureAirportName;
                appointment.DepartureCity = tripLeg.DepartureCity;
                appointment.DepartureCountry = tripLeg.DepartureCountry;

                appointment.AuthorizationCD = tripLeg.AuthorizationCD;
                appointment.AuthorizationId = tripLeg.AuthorizationID.HasValue ? tripLeg.AuthorizationID.Value : 0;


                appointment.AircraftDutyCD = tripLeg.AircraftDutyCD;

                appointment.HomebaseCD = tripLeg.HomebaseCD;
                appointment.HomebaseID = tripLeg.HomebaseID.HasValue ? tripLeg.HomebaseID.Value : 0;

                appointment.AircraftDutyDescription = tripLeg.AircraftDutyDescription;
                appointment.AircraftDutyBackColor = tripLeg.AircaftDutyBackColor;
                appointment.AircraftDutyForeColor = tripLeg.AircraftDutyForeColor;

                appointment.ClientCD = tripLeg.ClientCD;                

                /*appointment.CrewCodes = tripLeg.CrewCodes;
                if (!string.IsNullOrEmpty(tripLeg.CrewFullNames))
                {
                    string names = tripLeg.CrewFullNames.Replace(";", "</br>");
                    appointment.CrewFullNames = names;
                }


                appointment.CrewDutyTypeDescription = tripLeg.CrewDutyTypeDescription;
                appointment.CrewDutyBackColor = tripLeg.CrewDutyTypeBackColor;
                appointment.CrewDutyForeColor = tripLeg.CrewDutyTypeForeColor;
                appointment.DepartmentCD = !string.IsNullOrEmpty(tripLeg.DepartmentCD) ? tripLeg.DepartmentCD : string.Empty;*/


                appointment.ElapseTM = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.CumulativeETE = tripLeg.CumulativeETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.CumulativeETE.Value)) : "0";
                appointment.TotalETE = tripLeg.TotalETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.TotalETE.Value)) : "0";
                appointment.ETE = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : "0";

                appointment.FleetNotes = tripLeg.FleetNotes;


                appointment.FlightCategoryCode = !string.IsNullOrEmpty(tripLeg.FlightCatagoryCD) ? tripLeg.FlightCatagoryCD : string.Empty;
                appointment.FlightCategoryDescription = tripLeg.FlightCatagoryDescription;
                appointment.FlightCategoryBackColor = tripLeg.FlightCategoryBackColor;
                appointment.FlightCategoryForeColor = tripLeg.FlightCategoryForeColor;
                appointment.FlightNum = !string.IsNullOrEmpty(tripLeg.FlightNUM) ? tripLeg.FlightNUM : string.Empty;
                appointment.FlightPurpose = tripLeg.FlightPurpose;
                appointment.TripPurpose = tripLeg.Description;
                appointment.FuelLoad = tripLeg.FuelLoad;


                // appointment.IsRONAppointment = tripLeg.IsAircraftStandby.HasValue ? tripLeg.IsAircraftStandby.Value : false;
                /*appointment.IsRONAppointment = tripLeg.LegNUM == 0 ? true : false;
                appointment.IsStandByCrew = tripLeg.IsCrewStandBy.HasValue ? tripLeg.IsCrewStandBy.Value : false;*/


                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.LastUpdatedTime = tripLeg.LastUpdTS.HasValue ? tripLeg.LastUpdTS.Value.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture) : string.Empty;
                appointment.LastUpdatedUser = tripLeg.LastUpdUID;


                appointment.OutboundInstruction = tripLeg.OutbountInstruction;
                appointment.PassengerCount = tripLeg.PassengerTotal.HasValue ? tripLeg.PassengerTotal.Value : 0;
                appointment.PassengerRequestorCD = !string.IsNullOrEmpty(tripLeg.PassengerRequestorCD) ? tripLeg.PassengerRequestorCD : string.Empty;

               /* appointment.PassengerCodes = tripLeg.PassengerCodes;
                if (!string.IsNullOrEmpty(tripLeg.PassengerNames))
                {
                    string names = tripLeg.PassengerNames.Replace(";", "</br>");
                    appointment.PassengerNames = names;
                }
                */
                appointment.ReservationAvailable = tripLeg.ReservationAvailable.HasValue ? tripLeg.ReservationAvailable.Value : 0;


                return appointment;
            }

            /*
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {                               
                //if (appointment.EndDate < appointment.StartDate) // this condition will occur only in Local time base
                //{
                //    appointment.EndDate = appointment.StartDate + (tripLeg.ArrivalGreenwichDTTM.Value - tripLeg.DepartureGreenwichDTTM.Value);
                //    if (appointment.EndDate.Date > appointment.ArrivalDisplayTime.Date) // if calculated endDate > original end date...
                //    {
                //        appointment.EndDate = appointment.DepartureDisplayTime.Date.AddHours(23).AddMinutes(59);
                //    }
                //}

                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();

                appointment.DepartureDisplayTime = tripLeg.DepartureDTTMLocal.Value;
                appointment.ArrivalDisplayTime = tripLeg.ArrivalDTTMLocal.Value;

                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                appointment.Description = tripLeg.AppointmentDisplay;
                appointment.RecordType = tripLeg.RecordType;
                appointment.TailNum = tripLeg.TailNum;
                //appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat);
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();

                appointment.TripId = Convert.ToInt64(tripLeg.TripID);
               
                appointment.TripStatus = tripLeg.TripStatus;
                appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;                                
                appointment.Notes = !string.IsNullOrEmpty(tripLeg.Notes) ? tripLeg.Notes : string.Empty;
                appointment.StartDate = (DateTime)tripLeg.PlannerColumnPeriodStart;

                appointment.EndDate = (DateTime)tripLeg.PlannerColumnPeriodEnd;
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArriveICAO) ? tripLeg.ArriveICAO : string.Empty;
                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.CrewDutyTypeCD = tripLeg.DutyTYPE;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;

                appointment.ArrivalAirportName = tripLeg.ArrivalAirportName;
                appointment.ArrivalCity = tripLeg.ArrivalCity;
                appointment.ArrivalCountry = tripLeg.ArrivalCountry;
                appointment.DepartureICAOID = !string.IsNullOrEmpty(tripLeg.DepartureICAOID) ? tripLeg.DepartureICAOID : string.Empty;
                appointment.DepartureAirportName = tripLeg.DepartureAirportName;
                appointment.DepartureCity = tripLeg.DepartureCity;
                appointment.DepartureCountry = tripLeg.DepartureCountry;

                appointment.CrewDutyType = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();
                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.ShowAllDetails = true;                
                
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg)  //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false;// SHOW details only based on company profile setting - Trip privacy settings
                }

                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }
                
                ///-------------

                appointment.AuthorizationCD = tripLeg.AuthorizationCD;
                appointment.AuthorizationId = tripLeg.AuthorizationID.HasValue ? tripLeg.AuthorizationID.Value : 0;

                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.AircraftDutyCD = tripLeg.AircraftDutyCD;

                appointment.HomebaseCD = tripLeg.HomebaseCD;
                appointment.HomebaseID = tripLeg.HomebaseID.HasValue ? tripLeg.HomebaseID.Value : 0;

                appointment.AircraftDutyDescription = tripLeg.AircraftDutyDescription;
                appointment.AircraftDutyBackColor = tripLeg.AircaftDutyBackColor;
                appointment.AircraftDutyForeColor = tripLeg.AircraftDutyForeColor;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;

                appointment.ClientCD = tripLeg.ClientCD;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;

                //appointment.CrewCodes = tripLeg.CrewCodes;
                //if (!string.IsNullOrEmpty(tripLeg.CrewFullNames))
                //{
                //    string names = tripLeg.CrewFullNames.Replace(";", "</br>");
                //    appointment.CrewFullNames = names;
                //}
                //appointment.CrewDutyType = tripLeg.CrewDutyID.HasValue ? tripLeg.CrewDutyID.Value : 0;
                //appointment.CrewDutyTypeCD = tripLeg.CrewDutyTypeCD;
                //appointment.CrewDutyTypeDescription = tripLeg.CrewDutyTypeDescription;
                //appointment.CrewDutyBackColor = tripLeg.CrewDutyTypeBackColor;
                //appointment.CrewDutyForeColor = tripLeg.CrewDutyTypeForeColor;
                appointment.DepartmentCD = !string.IsNullOrEmpty(tripLeg.DepartmentCD) ? tripLeg.DepartmentCD : string.Empty;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;

                appointment.ElapseTM = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.CumulativeETE = tripLeg.CumulativeETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.CumulativeETE.Value)) : "0";
                //appointment.ETE = tripLeg.ETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ETE.Value)) : "0";

                appointment.FleetNotes = tripLeg.FleetNotes;

                
                appointment.FlightCategoryCode = !string.IsNullOrEmpty(tripLeg.FlightCatagoryCD) ? tripLeg.FlightCatagoryCD : string.Empty;
                appointment.FlightCategoryDescription = tripLeg.FlightCatagoryDescription;
                appointment.FlightCategoryBackColor = tripLeg.FlightCategoryBackColor;
                appointment.FlightCategoryForeColor = tripLeg.FlightCategoryForeColor;
                appointment.FlightNum = !string.IsNullOrEmpty(tripLeg.FlightNUM) ? tripLeg.FlightNUM : string.Empty;
                appointment.FlightPurpose = tripLeg.FlightPurpose;
                appointment.TripPurpose = tripLeg.Description;
                appointment.FuelLoad = tripLeg.FuelLoad;
               
                appointment.IsRONAppointment = tripLeg.IsAircraftStandby.HasValue ? tripLeg.IsAircraftStandby.Value : false;
                appointment.IsRONAppointment = tripLeg.LegNUM == 0 ? true : false;
                //appointment.IsStandByCrew = tripLeg.IsCrewStandBy.HasValue ? tripLeg.IsCrewStandBy.Value : false;
              
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg) //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false; // SHOW details only based on company profile setting - Trip privacy settings
                }
                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }
               
                appointment.LastUpdatedTime = tripLeg.LastUpdTS.HasValue ? tripLeg.LastUpdTS.Value.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture) : string.Empty;
                appointment.LastUpdatedUser = tripLeg.LastUpdUID;
                               
                appointment.OutboundInstruction = tripLeg.OutbountInstruction;
                appointment.PassengerCount = tripLeg.PassengerTotal.HasValue ? tripLeg.PassengerTotal.Value : 0;
                appointment.PassengerRequestorCD = !string.IsNullOrEmpty(tripLeg.PassengerRequestorCD) ? tripLeg.PassengerRequestorCD : string.Empty;
                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                //appointment.PassengerCodes = tripLeg.PassengerCodes;
                //if (!string.IsNullOrEmpty(tripLeg.PassengerNames))
                //{
                //    string names = tripLeg.PassengerNames.Replace(";", "</br>");
                //    appointment.PassengerNames = names;
                //}

                appointment.ReservationAvailable = tripLeg.ReservationAvailable.HasValue ? tripLeg.ReservationAvailable.Value : 0;
               
                return appointment;
            }
        */
        }

        public FlightPak.Web.Entities.SchedulingCalendar.Appointment SetCrewPlannerCommonCrewProperties(List<CrewPlannerCalendarDataResult> calendarData, string timeBase, CrewPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                appointment.TripId = Convert.ToInt64(tripLeg.TripID);
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripStatus = tripLeg.TripStatus;
                appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;               

                appointment.StartDate = (DateTime)tripLeg.PlannerColumnPeriodStart;
                appointment.EndDate = (DateTime)tripLeg.PlannerColumnPeriodEnd;

                appointment.DepartureDisplayTime = tripLeg.DepartureDisplayTime.Value;
                appointment.ArrivalDisplayTime = tripLeg.ArrivalDisplayTime.Value;

                 //if (appointment.EndDate < appointment.StartDate) // this condition will occur only in Local time base
                //{
                //    appointment.EndDate = appointment.StartDate + (tripLeg.ArrivalGreenwichDTTM.Value - tripLeg.DepartureGreenwichDTTM.Value);
                //    if (appointment.EndDate.Date > appointment.ArrivalDisplayTime.Date) // if calculated endDate > original end date...
                //    {
                //        appointment.EndDate = appointment.DepartureDisplayTime.Date.AddHours(23).AddMinutes(59);
                //    }
                //}

              
                
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArriveICAO) ? tripLeg.ArriveICAO : string.Empty;
                //appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArrivalICAOID) ? tripLeg.ArrivalICAOID : string.Empty;

                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.CrewCD = tripLeg.CrewCD;
                appointment.CrewId = tripLeg.CrewID.HasValue ? tripLeg.CrewID.Value : 0;
                appointment.CrewDutyTypeCD = tripLeg.DutyTYPE;
                appointment.CrewDutyType = tripLeg.CrewDutyID.HasValue ? tripLeg.CrewDutyID.Value : 0;
                //appointment.CrewLastName = tripLeg.CrewLastName;
                //appointment.CrewMiddleName = tripLeg.CrewMiddleName;
                //appointment.CrewFirstName = tripLeg.CrewFirstName;
                //appointment.CrewNotes = tripLeg.CrewNotes;
                //appointment.CrewCodes = tripLeg.CrewCodes;
                //if (!string.IsNullOrEmpty(tripLeg.CrewFullNames))
                //{
                //    string names = tripLeg.CrewFullNames.Replace(";", "</br>");
                //    appointment.CrewFullNames = names;
                //}

                //appointment.CrewDutyTypeCD = tripLeg.CrewDutyTypeCD;                
                appointment.CrewDutyTypeDescription = tripLeg.CrewDutyTypeDescription;
                appointment.CrewDutyBackColor = tripLeg.CrewDutyTypeBackColor;
                appointment.CrewDutyForeColor = tripLeg.CrewDutyTypeForeColor;

                appointment.Notes = !string.IsNullOrEmpty(tripLeg.Notes) ? tripLeg.Notes : string.Empty;
                //appointment.Notes = tripLeg.Notes;                                
                appointment.LastUpdatedUser = tripLeg.LastUpdUID;
                
                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.AircraftDutyCD = tripLeg.AircraftDutyCD;
                appointment.AircraftDutyDescription = tripLeg.AircraftDutyDescription;
                appointment.AircraftDutyBackColor = tripLeg.AircaftDutyBackColor;
                appointment.AircraftDutyForeColor = tripLeg.AircraftDutyForeColor;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;

                appointment.HomebaseCD = tripLeg.HomebaseCD;
                appointment.HomebaseID = tripLeg.HomebaseID.HasValue ? tripLeg.HomebaseID.Value : 0;

                
                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();
                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.IsRONAppointment = tripLeg.LegNUM == 0 ? true : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;

                  
                appointment.ElapseTM = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.ETE = tripLeg.ElapseTM.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.ElapseTM.Value)) : string.Empty;
                appointment.CumulativeETE = tripLeg.CumulativeETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.CumulativeETE.Value)) : "0";
                appointment.TotalETE = tripLeg.TotalETE.HasValue ? RoundElpTime(Convert.ToDouble(tripLeg.TotalETE.Value)) : "0";

                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.FlightCategoryCode = !string.IsNullOrEmpty(tripLeg.FlightCatagoryCD) ? tripLeg.FlightCatagoryCD : string.Empty;
                appointment.FlightCategoryDescription = tripLeg.FlightCatagoryDescription;
                appointment.FlightCategoryBackColor = tripLeg.FlightCategoryBackColor;
                appointment.FlightCategoryForeColor = tripLeg.FlightCategoryForeColor;
                appointment.FlightNum = !string.IsNullOrEmpty(tripLeg.FlightNUM) ? tripLeg.FlightNUM : string.Empty;
                appointment.FlightPurpose = tripLeg.FlightPurpose;
                appointment.TripPurpose = tripLeg.Description;
                         
                appointment.DepartmentCD = !string.IsNullOrEmpty(tripLeg.DepartmentCD) ? tripLeg.DepartmentCD : string.Empty;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.DepartureAirportName = tripLeg.DepartureAirportName;
                appointment.DepartureCity = tripLeg.DepartureCity;
                appointment.DepartureCountry = tripLeg.DepartureCountry;
                               
                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg) //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false;// SHOW details only based on company profile setting - Trip privacy settings
                }

                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }
               
                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();
                
                //appointment.Description = tripLeg.AppointmentDisplay;
                if (tripLeg.LegID == 9999)
                {
                    appointment.Description = "R";
                }
                else
                {
                    appointment.Description = tripLeg.AppointmentDisplay;
                }
                appointment.RecordType = tripLeg.RecordType;

                //appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat);
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                appointment.ReservationAvailable = tripLeg.ReservationAvailable.HasValue ? tripLeg.ReservationAvailable.Value : 0;
                              

                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();

                appointment.ArrivalAirportName = tripLeg.ArrivalAirportName;
                appointment.ArrivalCity = tripLeg.ArrivalCity;
                appointment.ArrivalCountry = tripLeg.ArrivalCountry;
                
                appointment.DepartureICAOID = !string.IsNullOrEmpty(tripLeg.DepartureICAOID) ? tripLeg.DepartureICAOID : string.Empty;
                appointment.AuthorizationCD = tripLeg.AuthorizationCD;
                appointment.AuthorizationId = tripLeg.AuthorizationID.HasValue ? tripLeg.AuthorizationID.Value : 0;
                
                appointment.PassengerCount = tripLeg.PassengerTotal.HasValue ? tripLeg.PassengerTotal.Value : 0;
                appointment.PassengerRequestorCD = !string.IsNullOrEmpty(tripLeg.PassengerRequestorCD) ? tripLeg.PassengerRequestorCD : string.Empty;
                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                
                ////appointment.PassengerCodes = tripLeg.PassengerCodes;
                ////if (!string.IsNullOrEmpty(tripLeg.PassengerNames))
                ////{
                ////    string names = tripLeg.PassengerNames.Replace(";", "</br>");
                ////    appointment.PassengerNames = names;
                ////}

                return appointment;               
            }
        }

        private double GetElapseTMRoundingFromSettings()
        {
            double lnEteRound = 0.0;
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) // Minutes
            {

                decimal ElapseTMRounding = 0;
                //lnEteRound=(double)CompanyLists[0].ElapseTMRounding~lnRound;

                if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                    ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;

            }
            return lnEteRound;
        }

        private string RoundElpTime(double lnElp_Time)
        {
            string convertedValue = string.Empty;
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) // Minutes
            {
                double lnElp_Min = 0.0;
                double lnEteRound = ETERound;

                if (lnEteRound > 0)
                {

                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    //lnElp_Min = lnElp_Min % lnEteRound;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);


                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);

                    convertedValue = ConvertTenthsToMins(lnElp_Time.ToString());
                }
                convertedValue = ConvertTenthsToMins(lnElp_Time.ToString()); // None -option
            }
            else
            {
                string time = lnElp_Time.ToString();
                if (time.IndexOf(".") < 0)
                    time = time + ".0";
                convertedValue = Math.Round(Convert.ToDecimal(time), 1).ToString();
            }

            return convertedValue;
        }

        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)

                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }

        public static RadTreeNode CommonServiceBindFullAndGroupedFleetTreeNodes(List<FlightPak.Web.CommonService.FleetTreeResult> fleetList)
        {
            var fullFleetList = fleetList.Where(x => x.FleetGroupID == null).ToList();
            var groupedLists = fleetList.Where(x => x.FleetGroupID != null).ToList();

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Fleet</b></html>";
            parentNode.Value = "Fleet";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = true;


            //loop through each qualification description and add to the parent node
            foreach (var fleet in fullFleetList)
            {
                childNode = new RadTreeNode();
                childNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                childNode.Value = fleet.FleetID.ToString();
                //childNode.CssClass = "rootNode";
                childNode.Checked = true;
                parentNode.Nodes.Add(childNode);

            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.FleetGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedFleets = groupedLists.Where(x => x.FleetGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var fleet in groupedFleets)
                {
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                    groupChildNode.Value = fleet.FleetID.ToString();
                    //groupChildNode.CssClass = "rootNode";
                    groupChildNode.Checked = true;

                    parentGroupNode.Text = "<html><b>" + fleet.FleetGroupDescription + "</b></html>";
                    parentGroupNode.Value = fleet.FleetGroupCD;
                    parentGroupNode.Nodes.Add(groupChildNode);

                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }
        public static RadTreeNode CommonServiceBindFullAndGroupedFleetTreeNodesNew(List<FlightPak.Web.CommonService.FleetTreeResult> fleetList, Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            var fullFleetList = fleetList.Where(x => x.FleetGroupID == null).ToList();
            var groupedLists = fleetList.Where(x => x.FleetGroupID != null).ToList();
            bool nodeCheck = false;
            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Fleet</b></html>";
            parentNode.Value = "Fleet";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = true;


            //loop through each qualification description and add to the parent node
            foreach (var fleet in fullFleetList)
            {
                nodeCheck = false;
                childNode = new RadTreeNode();
                childNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                childNode.Value = fleet.FleetID.ToString();
                //childNode.CssClass = "rootNode";

                foreach (TreeviewParentChildNodePair s in selectedFleetIDs)
                {
                    if (Convert.ToString(parentNode.Value) == s.ParentNodeId && Convert.ToString(childNode.Value) == s.ChildNodeId)
                    {
                        nodeCheck = true;
                        break;
                    }
                }

                childNode.Checked = nodeCheck;
                parentNode.Nodes.Add(childNode);

            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.FleetGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedFleets = groupedLists.Where(x => x.FleetGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var fleet in groupedFleets)
                {
                    bool nodegrpCheck = false;
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                    groupChildNode.Value = fleet.FleetID.ToString();
                    //groupChildNode.CssClass = "rootNode";

                    parentGroupNode.Text = "<html><b>" + fleet.FleetGroupDescription + "</b></html>";
                    parentGroupNode.Value = fleet.FleetGroupCD;
                    parentGroupNode.Nodes.Add(groupChildNode);
                   
                    string parenoNodeId = string.Empty;
                    string parenoNodeSelected = string.Empty;
                    string chileNode = string.Empty;
                    string childNodeSelected = string.Empty;
                    foreach (TreeviewParentChildNodePair c in selectedFleetIDs)
                    {
                        parenoNodeId = parentGroupNode.Value.Contains('.') ? parentGroupNode.Value.Split('.')[0] : parentGroupNode.Value;
                        parenoNodeSelected = c.ParentNodeId.Contains('.') ? c.ParentNodeId.Split('.')[0] : c.ParentNodeId;
                        chileNode = groupChildNode.Value.Contains('.') ? groupChildNode.Value.Split('.')[1] : groupChildNode.Value;
                        childNodeSelected = c.ChildNodeId.Contains('.') ? c.ChildNodeId.Split('.')[1] : c.ChildNodeId;
                        if (parenoNodeId == parenoNodeSelected && chileNode == childNodeSelected)
                        {
                            nodegrpCheck = true;
                            break;
                        }
                    }
                    groupChildNode.Checked = nodegrpCheck;
                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }

        //Changes done for the Bug ID - 5828
        public static RadTreeNode BindFullAndGroupedFleetTreeNodes(List<FlightPak.Web.PreflightService.FleetTreeResult> fleetList, bool nodeCheck)
        {
            var fullFleetList = fleetList.Where(x => x.FleetGroupID == null).ToList();
            var groupedLists = fleetList.Where(x => x.FleetGroupID != null).ToList();

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Fleet</b></html>";
            parentNode.Value = "Fleet";
            parentNode.ToolTip = "Click to see subcategories";
            //parentNode.Checked = true;
            parentNode.Checked = nodeCheck;


            //loop through each qualification description and add to the parent node
            foreach (var fleet in fullFleetList)
            {
                childNode = new RadTreeNode();
                childNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                childNode.Value = fleet.FleetID.ToString();
                //childNode.CssClass = "rootNode";
                childNode.Checked = nodeCheck;
                parentNode.Nodes.Add(childNode);

            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.FleetGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedFleets = groupedLists.Where(x => x.FleetGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var fleet in groupedFleets)
                {
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                    groupChildNode.Value = fleet.FleetID.ToString();
                    //groupChildNode.CssClass = "rootNode";
                    groupChildNode.Checked = nodeCheck;

                    parentGroupNode.Text = "<html><b>" + fleet.FleetGroupDescription + "</b></html>";
                    parentGroupNode.Value = fleet.FleetGroupCD;
                    parentGroupNode.Nodes.Add(groupChildNode);

                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }

        public static RadTreeNode BindFullAndGroupedFleetTreeNodesNew(List<FlightPak.Web.PreflightService.FleetTreeResult> fleetList, Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            var fullFleetList = fleetList.Where(x => x.FleetGroupID == null).ToList();
            var groupedLists = fleetList.Where(x => x.FleetGroupID != null).ToList();

            bool nodeCheck = false; 

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Fleet</b></html>";
            parentNode.Value = "Fleet";
            parentNode.ToolTip = "Click to see subcategories";
            //parentNode.Checked = true;
            parentNode.Checked = nodeCheck;


            //loop through each qualification description and add to the parent node
            foreach (var fleet in fullFleetList)
            {
                nodeCheck = false;
                childNode = new RadTreeNode();
                childNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                childNode.Value = fleet.FleetID.ToString();
                //childNode.CssClass = "rootNode";               
                parentNode.Nodes.Add(childNode);
                foreach (TreeviewParentChildNodePair s in selectedFleetIDs)
                {
                    if (Convert.ToString(childNode.ParentNode.Value) == s.ParentNodeId && Convert.ToString(childNode.Value) == s.ChildNodeId)
                    {
                        nodeCheck = true;
                        break;
                    }
                }
                childNode.Checked = nodeCheck;
            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.FleetGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedFleets = groupedLists.Where(x => x.FleetGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var fleet in groupedFleets)
                {
                    bool nodegrpCheck = false;
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = fleet.TailNum + " - " + fleet.TypeDescription;
                    //groupChildNode.Value = fleet.FleetGroupID.ToString() + "." + fleet.FleetID.ToString();
                    groupChildNode.Value = fleet.FleetID.ToString();
                    //groupChildNode.CssClass = "rootNode";

                    parentGroupNode.Text = "<html><b>" + fleet.FleetGroupDescription + "</b></html>";
                    //parentGroupNode.Value = fleet.FleetID+"."+fleet.FleetGroupID;
                    parentGroupNode.Value = fleet.FleetGroupCD;
                    parentGroupNode.Nodes.Add(groupChildNode);
                    string parenoNodeId = string.Empty;
                    string parenoNodeSelected = string.Empty;
                    string chileNode = string.Empty;
                    string childNodeSelected = string.Empty;
                    foreach (TreeviewParentChildNodePair s in selectedFleetIDs)
                    {
                        parenoNodeId = parentGroupNode.Value.Contains('.') ? parentGroupNode.Value.Split('.')[0] : parentGroupNode.Value;
                        parenoNodeSelected = s.ParentNodeId.Contains('.') ? s.ParentNodeId.Split('.')[0] : s.ParentNodeId;
                        chileNode = groupChildNode.Value.Contains('.') ? groupChildNode.Value.Split('.')[1] : groupChildNode.Value;
                        childNodeSelected = s.ChildNodeId.Contains('.') ? s.ChildNodeId.Split('.')[1] : s.ChildNodeId;
                        if (parenoNodeId == parenoNodeSelected && chileNode == childNodeSelected)
                        {
                            nodegrpCheck = true;
                            break;
                        }
                    }
                    groupChildNode.Checked = nodegrpCheck;

                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }
        
        public static RadTreeNode CommonBindFullAndGroupedCrewTreeNodes(List<FlightPak.Web.CommonService.CrewTreeResult> crewList, bool nodeCheck)
        {
            var fullCrewList = crewList.Where(x => x.CrewGroupID == null).ToList();
            var groupedLists = crewList.Where(x => x.CrewGroupID != null).ToList();

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Crew</b></html>";
            parentNode.Value = "Crew";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = nodeCheck;

            //loop through each qualification description and add to the parent node
            foreach (var crew in fullCrewList)
            {
                childNode = new RadTreeNode();
                childNode.Text = crew.CrewCD + " - " + crew.CrewName;
                childNode.Value = crew.CrewID.ToString();
                //childNode.CssClass = "rootNode";
                childNode.Checked = nodeCheck;

                parentNode.Nodes.Add(childNode);
            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.CrewGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedCrews = groupedLists.Where(x => x.CrewGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var crew in groupedCrews)
                {
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = crew.CrewCD + " - " + crew.CrewName;
                    groupChildNode.Value = crew.CrewID.ToString();
                    //groupChildNode.CssClass = "rootNode";
                    groupChildNode.Checked = nodeCheck;

                    parentGroupNode.Text = "<html><b>" + crew.CrewGroupDescription + "</b></html>";
                    parentGroupNode.Value = crew.CrewGroupCD;
                    parentGroupNode.Nodes.Add(groupChildNode);

                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }
        public static RadTreeNode CommonBindFullAndGroupedCrewTreeNodesNew(List<FlightPak.Web.CommonService.CrewTreeResult> crewList, Collection<TreeviewParentChildNodePair> selectedCrewIDs)
        {
            var fullCrewList = crewList.Where(x => x.CrewGroupID == null).ToList();
            var groupedLists = crewList.Where(x => x.CrewGroupID != null).ToList();


            bool nodeCheck = false; 

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Crew</b></html>";
            parentNode.Value = "Crew";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = nodeCheck;

            //loop through each qualification description and add to the parent node
            foreach (var crew in fullCrewList)
            {
                nodeCheck = false;
                childNode = new RadTreeNode();
                childNode.Text = crew.CrewCD + " - " + crew.CrewName;
                childNode.Value = crew.CrewID.ToString();
                //childNode.CssClass = "rootNode";

                foreach (TreeviewParentChildNodePair s in selectedCrewIDs)
                {
                    if (Convert.ToString(childNode.Value) == s.ChildNodeId)
                    {
                        nodeCheck = true;
                        break;
                    }
                }   

                childNode.Checked = nodeCheck;

                parentNode.Nodes.Add(childNode);
            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.CrewGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedCrews = groupedLists.Where(x => x.CrewGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var crew in groupedCrews)
                {
                    bool nodegrpCheck = false; 
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = crew.CrewCD + " - " + crew.CrewName;
                    //groupChildNode.Value = crew.CrewID.ToString();
                    //groupChildNode.Value = crew.CrewID.ToString();
                    groupChildNode.Value = crew.CrewGroupID.ToString() + "." + crew.CrewID.ToString();
                    //groupChildNode.CssClass = "rootNode";

                    string parenoNodeId = string.Empty;
                    string parenoNodeSelected = string.Empty;
                    string chileNode = string.Empty;
                    string childNodeSelected = string.Empty;
                    foreach (TreeviewParentChildNodePair c in selectedCrewIDs)
                    {
                        parenoNodeId = parentGroupNode.Value.Contains('.') ? parentGroupNode.Value.Split('.')[0] : parentGroupNode.Value;
                        parenoNodeSelected = c.ParentNodeId.Contains('.') ? c.ParentNodeId.Split('.')[0] : c.ParentNodeId;
                        chileNode = groupChildNode.Value.Contains('.') ? groupChildNode.Value.Split('.')[1] : groupChildNode.Value;
                        childNodeSelected = c.ChildNodeId.Contains('.') ? c.ChildNodeId.Split('.')[1] : c.ChildNodeId;
                        if (parenoNodeId == parenoNodeSelected && chileNode == childNodeSelected)
                        {
                            nodegrpCheck = true;
                            break;
                        }
                    }

                    groupChildNode.Checked = nodegrpCheck;

                    parentGroupNode.Text = "<html><b>" + crew.CrewGroupDescription + "</b></html>";
                    //parentGroupNode.Value = crew.CrewGroupCD;
                    parentGroupNode.Value = crew.CrewGroupID+"."+crew.CrewID;
                    parentGroupNode.Nodes.Add(groupChildNode);

                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }
        public static RadTreeNode BindFullAndGroupedCrewTreeNodes(List<FlightPak.Web.PreflightService.CrewTreeResult> crewList, bool nodeCheck)
        {
            var fullCrewList = crewList.Where(x => x.CrewGroupID == null).ToList();
            var groupedLists = crewList.Where(x => x.CrewGroupID != null).ToList();

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Crew</b></html>";
            parentNode.Value = "Crew";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = nodeCheck;

            //loop through each qualification description and add to the parent node
            foreach (var crew in fullCrewList)
            {
                childNode = new RadTreeNode();
                childNode.Text = crew.CrewCD + " - " + crew.CrewName;
                childNode.Value = crew.CrewID.ToString();
                //childNode.CssClass = "rootNode";
                childNode.Checked = nodeCheck;

                parentNode.Nodes.Add(childNode);
            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.CrewGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedCrews = groupedLists.Where(x => x.CrewGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var crew in groupedCrews)
                {
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = crew.CrewCD + " - " + crew.CrewName;
                    groupChildNode.Value = crew.CrewID.ToString();
                    //groupChildNode.CssClass = "rootNode";
                    groupChildNode.Checked = nodeCheck;

                    parentGroupNode.Text = "<html><b>" + crew.CrewGroupDescription + "</b></html>";
                    parentGroupNode.Value = crew.CrewGroupCD;
                    parentGroupNode.Nodes.Add(groupChildNode);

                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }

        public static RadTreeNode BindFullAndGroupedCrewTreeNodesNew(List<FlightPak.Web.PreflightService.CrewTreeResult> crewList, Collection<TreeviewParentChildNodePair> selectedCrewIDs)
        {
            var fullCrewList = crewList.Where(x => x.CrewGroupID == null).ToList();
            var groupedLists = crewList.Where(x => x.CrewGroupID != null).ToList();

            bool nodeCheck = false;

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Crew</b></html>";
            parentNode.Value = "Crew";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = nodeCheck;

            //loop through each qualification description and add to the parent node
            foreach (var crew in fullCrewList)
            {
                nodeCheck = false; 
                childNode = new RadTreeNode();
                childNode.Text = crew.CrewCD + " - " + crew.CrewName;
                childNode.Value = crew.CrewID.ToString();
                //childNode.CssClass = "rootNode";

                foreach (TreeviewParentChildNodePair c in selectedCrewIDs)
                {
                    if (Convert.ToString(parentNode.Value) == c.ParentNodeId && Convert.ToString(childNode.Value) == c.ChildNodeId)
                    {
                        nodeCheck = true;
                        break;
                    }
                }  

                childNode.Checked = nodeCheck;
                parentNode.Nodes.Add(childNode);
            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.CrewGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedCrews = groupedLists.Where(x => x.CrewGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var crew in groupedCrews)
                {
                    bool nodeGrpCheck = false;
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = crew.CrewCD + " - " + crew.CrewName;
                    groupChildNode.Value = crew.CrewID.ToString();
                    //groupChildNode.Value = Convert.ToString(crew.CrewGroupID+"."+crew.CrewID);                    
                    //groupChildNode.CssClass = "rootNode";

                    parentGroupNode.Text = "<html><b>" + crew.CrewGroupDescription + "</b></html>";
                    parentGroupNode.Value = crew.CrewGroupCD;
                    //parentGroupNode.Value = Convert.ToString(crew.CrewGroupID + "." + crew.CrewID);                    
                    parentGroupNode.Nodes.Add(groupChildNode);
                    
                    string parenoNodeId = string.Empty;
                    string parenoNodeSelected = string.Empty;
                    string chileNode = string.Empty;
                    string childNodeSelected = string.Empty;
                    foreach (TreeviewParentChildNodePair c in selectedCrewIDs)
                    {
                        parenoNodeId = parentGroupNode.Value.Contains('.') ? parentGroupNode.Value.Split('.')[0] : parentGroupNode.Value;
                        parenoNodeSelected = c.ParentNodeId.Contains('.') ? c.ParentNodeId.Split('.')[0] : c.ParentNodeId;
                        chileNode = groupChildNode.Value.Contains('.') ? groupChildNode.Value.Split('.')[1] : groupChildNode.Value;
                        childNodeSelected = c.ChildNodeId.Contains('.') ? c.ChildNodeId.Split('.')[1] : c.ChildNodeId;
                        if (parenoNodeId == parenoNodeSelected && chileNode == childNodeSelected)
                        {
                            nodeGrpCheck = true;
                            break;
                        }
                    }
                    groupChildNode.Checked = nodeGrpCheck;
                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }

        public static RadTreeNode BindFullAndGroupedCrewTreeNodesNewGroupBased(List<FlightPak.Web.PreflightService.CrewTreeResult> crewList, Collection<TreeviewParentChildNodePair> selectedCrewIDs)
        {
            var fullCrewList = crewList.Where(x => x.CrewGroupID == null).ToList();
            var groupedLists = crewList.Where(x => x.CrewGroupID != null).ToList();

            bool nodeCheck = false;

            //create parent node
            RadTreeNode parentNode = new RadTreeNode();

            //create child node
            RadTreeNode childNode = new RadTreeNode();
            //loop through each category and get the qualification descriptions in each category

            parentNode = new RadTreeNode();
            parentNode.Text = "<html><b>Entire Crew</b></html>";
            parentNode.Value = "Crew";
            parentNode.ToolTip = "Click to see subcategories";
            parentNode.Checked = nodeCheck;

            //loop through each qualification description and add to the parent node
            foreach (var crew in fullCrewList)
            {
                nodeCheck = false;
                childNode = new RadTreeNode();
                childNode.Text = crew.CrewCD + " - " + crew.CrewName;
                childNode.Value = crew.CrewID.ToString();
                //childNode.CssClass = "rootNode";

                foreach (TreeviewParentChildNodePair c in selectedCrewIDs)
                {
                    if (Convert.ToString(parentNode.Value) == c.ParentNodeId && Convert.ToString(childNode.Value) == c.ChildNodeId)
                    {
                        nodeCheck = true;
                        break;
                    }
                }

                childNode.Checked = nodeCheck;
                parentNode.Nodes.Add(childNode);
            }

            //loop through groupedLists
            var distinctGroupIds = groupedLists.Select(x => x.CrewGroupID).Distinct().ToList();

            foreach (var groupId in distinctGroupIds)
            {
                var groupedCrews = groupedLists.Where(x => x.CrewGroupID == groupId).ToList();
                //create parent node
                RadTreeNode parentGroupNode = new RadTreeNode();

                parentGroupNode.Checked = false;
                foreach (var crew in groupedCrews)
                {
                    bool nodeGrpCheck = false;
                    RadTreeNode groupChildNode = new RadTreeNode();
                    groupChildNode.Text = crew.CrewCD + " - " + crew.CrewName;
                    //groupChildNode.Value = crew.CrewID.ToString();
                    groupChildNode.Value = Convert.ToString(crew.CrewGroupID + "." + crew.CrewID);
                    groupChildNode.ToolTip = Convert.ToString(crew.CrewID);
                    //groupChildNode.CssClass = "rootNode";

                    parentGroupNode.Text = "<html><b>" + crew.CrewGroupDescription + "</b></html>";
                    //parentGroupNode.Value = crew.CrewGroupCD;
                    parentGroupNode.Value = Convert.ToString(crew.CrewGroupID + "." + crew.CrewID);
                    parentGroupNode.Nodes.Add(groupChildNode);

                    string parenoNodeId = string.Empty;
                    string parenoNodeSelected = string.Empty;
                    string chileNode = string.Empty;
                    string childNodeSelected = string.Empty;
                    foreach (TreeviewParentChildNodePair c in selectedCrewIDs)
                    {
                        parenoNodeId = parentGroupNode.Value.Contains('.') ? parentGroupNode.Value.Split('.')[0] : parentGroupNode.Value;
                        parenoNodeSelected = c.ParentNodeId.Contains('.') ? c.ParentNodeId.Split('.')[0] : c.ParentNodeId;
                        chileNode = groupChildNode.Value.Contains('.') ? groupChildNode.Value.Split('.')[1] : groupChildNode.Value;
                        childNodeSelected = c.ChildNodeId.Contains('.') ? c.ChildNodeId.Split('.')[1] : c.ChildNodeId;
                        if (parenoNodeId == parenoNodeSelected && chileNode == childNodeSelected)
                        {
                            nodeGrpCheck = true;
                            break;
                        }
                    }
                    groupChildNode.Checked = nodeGrpCheck;
                }
                parentNode.Nodes.Add(parentGroupNode);
            }
            return parentNode;
        }
    }
}