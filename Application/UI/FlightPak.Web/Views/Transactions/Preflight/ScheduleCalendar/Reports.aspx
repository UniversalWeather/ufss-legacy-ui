﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Site.Master"
    AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.Reports"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Site.Master" %>
<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" runat="server" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

//        function openReport(radWin) {
//            debugger; 
//            var url = '';
//            
//            if (radWin == "Weekly") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=PREScheduleCalendarWeeklyDetail.xml';
//            }
//            else if (radWin == "WeeklyCrew") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=WeeklyCrew.xml';
//            }
//            else if (radWin == "WeeklyFleet") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=WeeklyFleet.xml';
//            }
//            else if (radWin == "Monthly") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=Monthly.xml';
//            }
//            else if (radWin == "Day") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=PREScheduleDayCalendar.xml';
//            }
//            else if (radWin == "Planner") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=Planner.xml';
//            }
//            else if (radWin == "Business") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=Business.xml';
//            }
//            else if (radWin == "Corporate") {
//                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=Calender&xmlFilename=PRECorporateCalendar.xml';
//            }
//            var oWnd = radopen(url, "RadCalendarPopup");
//        }
    </script>
    <telerik:RadScriptManager ID="RadScriptManager2" runat="server" />
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock2">
                    <!-- Start: Ipad Fix -->
                    <script type="text/javascript">
                        var lastArgs = null;
                        var lastContext = null;
                        var longTouchID = 0;
                        var menuShown = false;
                        function longTouch() {
                            longTouchID = 0;
                            menuShown = true;
                        }

                        function handleTouchStart(e) {

                            lastContext = e;
                            lastContext.target = e.originalTarget;
                            lastContext.pageX = e.changedTouches[0].pageX;
                            lastContext.pageY = e.changedTouches[0].pageY;
                            lastContext.clientX = e.changedTouches[0].clientX;
                            lastContext.clientY = e.changedTouches[0].clientY;
                            ID = setTimeout(longTouch(), 1000);

                        }

                        function handleClick(e) {
                            if (menuShown) {
                                menuShown = false;
                                document.body.removeEventListener('click', handleClick, true);
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }

                        function handleTouchEnd(e) {
                            if (longTouchID != 0)
                                clearTimeout(longTouchID);
                            if (menuShown) {
                                document.body.addEventListener('click', handleClick, true);
                                e.preventDefault();
                            }
                        }

                        function pageLoad() {
                            if ($telerik.isMobileSafari) {

                            }
                        }
                    </script>
                    <!-- End: Ipad Fix -->
                    <script type="text/javascript">

                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }

                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                                                function openReport(radWin) {
                                                    var url = '';
                                                    if (radWin == "WeeklyCrew") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptWeeklyCrew';
                                                    }
                                                    else if (radWin == "WeeklyFleet") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptWeeklyFleet';
                                                    }
                                                    else if (radWin == "Planner") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptPlanner';
                                                    }
                                                    else if (radWin == "Business") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptBusinessWeek';
                                                    }
                                                    else if (radWin == "Corporate") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptCorporate';
                                                    }
                                                    else if (radWin == "Day") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptDay';
                                                    }
                                                    else if (radWin == "Weekly") {
                                                        url = '../../../../Views/Reports/CalendarReports.aspx?Report=RptWeekly';
                                                    }
                                                    var oWnd = radopen(url, "RadCalendarPopup");
                                                }

                        function openReportMonthly(radWin) {
                            var url = '';
                            if (radWin == "Monthly") {
                                url = '../../../../Views/Reports/CalendarMonthly.aspx';
                            }
                            var oWnd = radopen(url, "RadMonthly");
                        }


                        function openWinAdv() {
                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }
                        function openWinFleetCalendarEntries() {
                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }
                        function openWinLegNotes() {

                            var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                        }

                        function RadBusinessWeekScheduler_OnClientAppointmentContextMenuItemClicking(sender, args) {
                            showPleaseWait();
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }

                        function openWinOutBoundInstructions() {
                            var oWnd = radopen("../PreflightOutboundInstruction.aspx", "RadWindow5");
                        }

                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow;
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                            return oWindow;
                        }

                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }

                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                       
                    </script>
                    <script type="text/javascript">
                        //Rad Date Picker
                        var currentTextBox = null;
                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox
                        function showPopup(sender, e) {
                            //this is a reference to the texbox which raised the event
                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate
                            //the selection of a date
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                            //this variable is used to store a reference to the date picker, which is currently
                            //active
                            currentDatePicker = datePicker;

                            //this method first parses the date, that the user entered or selected, and then
                            //sets it as a selected date to the picker
                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                                //value of the picker
                                //currentTextBox.value = args.get_newValue();

                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");

                                }
                                else
                                { return false; }
                            }
                        }

                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {
                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                sender.value = formattedDate;
                            }
                        }
                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }

                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }
                            // allow dot, slash and hypen characters... if any other character, throw alert
                        }

                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                       

                        

                       
                    </script>
                    <!-- Start: Tooltip integration -->
                    <script type="text/javascript">
        //<![CDATA[


                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow(sender, args) {

                        }

                        function OnClientRequestStart(sender, args) {


                        }

                        function OnClientAppointmentContextMenu(sender, args) {
                            hideActiveToolTip();
                        }
        //]]>
                    </script>
                    <!-- End: Tooltip integration -->
                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCalendarPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Reports.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadMonthly" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Height="450px" Width="450px" Behaviors="Close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Reports/CalendarMonthly.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content">
        <input type="hidden" id="hdnPostBack" value="" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="556px" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel170">
                    <div class="sc_date_display">
                    </div>
                </td>
                <td align="left" class="defaultview">
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        onclick="javascript:showPleaseWait();" OnCheckedChanged="ChkDefaultView_OnCheckedChanged" />
                </td>
                <td align="right" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
                <td class="sc_nav_topicon" align="right">
                    <span class="tab-nav-icons"><a class="refresh-icon" onclick="Refresh();" href="#"
                        title="Refresh"></a><a class="help-icon" href="/Views/Help/ViewHelp.aspx?Screen=SchedulingCalendar" title="Help"></a></span>
                </td>
            </tr>
        </table>
        <div id="DivExternalForm" runat="server" style="width: auto;">
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Skin="Sunset" />
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                        <table  width="100%" class="border-box">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="200px" class="reports_new">                                       
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkWeekly" runat="server" Font-Bold="true" Text="Weekly" OnClientClick="javascript:openReport('Weekly');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkWeeklyCrew" runat="server" Font-Bold="true" Text="Weekly Crew"
                                                    OnClientClick="javascript:openReport('WeeklyCrew');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkWeeklyFleet" runat="server" Font-Bold="true" Text="Weekly Fleet"
                                                    OnClientClick="javascript:openReport('WeeklyFleet');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkMonthly" runat="server" Font-Bold="true" Text="Monthly" OnClientClick="javascript:openReport('Monthly');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkDay" runat="server" Font-Bold="true" Text="Day" OnClientClick="javascript:openReport('Day');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkPlanner" runat="server" Font-Bold="true" Text="Planner" OnClientClick="javascript:openReport('Planner');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkBusiness" runat="server" Font-Bold="true" Text="Business Week"
                                                    OnClientClick="javascript:openReport('BusinessWeek');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkCorporate" runat="server" Font-Bold="true" Text="Corporate"
                                                    OnClientClick="javascript:openReport('Corporate');return false;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
