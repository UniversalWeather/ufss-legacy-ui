﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using System.Drawing;
using System.Collections.ObjectModel;
using FlightPak.Web.Views.Transactions.PreFlight.ScheduleCalendar;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Entities.SchedulingCalendar;
using FlightPak.Web.FlightPakMasterService;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Constants;



namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class CorporateViewFullView : ScheduleCalendarBase
    {
        private int CorporateFleetTreeCount { get; set; }
        private int CorporateCrewTreeCount { get; set; }
        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;
                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;

                        Session["SCAdvancedTab"] = CurrentDisplayOption.Corporate;

                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainContent");
                        UserControl ucSCMenu = (UserControl)contentPlaceHolder.FindControl("TabSchedulingCalendar");
                        ucSCMenu.Visible = false;

                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.Corporate)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }
                            //To reduce service call - Vishwa
                            FilterCriteria.userSettingAvailable = base.scWrapper.userSettingAvailable;
                            FilterCriteria.retriveSystemDefaultSettings = base.scWrapper.retrieveSystemDefaultSettings;
                            //End
                            var selectedDay = DateTime.Today;
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }
                            StartDate = selectedDay;
                            EndDate = new SchedulingCalendar().GetWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            RadDatePicker1.SelectedDate = DateTime.Today;
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            loadTree(false);

                            Session.Remove("SCCorporateNextCount");
                            Session.Remove("SCCorporatePreviousCount");
                            Session.Remove("SCCorporateLastCount");
                            Session.Remove("SCCorporateFirstCount");
                        }
                        else
                        {                            
                            CheckValidDate();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }
        }
         /// <summary>
        /// To Validate Date
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            StartDate = (DateTime)Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            EndDate = new SchedulingCalendar().GetWeekEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

            if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)//if no tree node is selected...
            {
                return true;
            }
            else
            {
                // avoid duplicate calls for loadTree();
                string id = Page.Request.Params["__EVENTTARGET"];

                if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
                {
                    loadTree(false);
                }
            }
            return IsDateCheck;
        }
        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;
                        //Changes done for the Bug ID - 5828
                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;
                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNew(crewList, selectedcrewIDs);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;

                        CrewTreeView.DataBind();
                    }
                }
            }
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        //RadDatePicker1.SelectedDate = DateTime.Today;
                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                        StartDate = DateTime.Today;
                        EndDate = GetWeekEndDate(StartDate);
                        Session["SCSelectedDay"] = StartDate;
                        lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetWeekEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            loadTree(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //fleet trips list
                if (!Page.IsPostBack)
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Corporate);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                    Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                    }

                    if (!string.IsNullOrEmpty(FleetCrew[1]))
                    {
                        string[] Crewids = FleetCrew[1].Split(',');
                        foreach (string cIDs in Crewids)
                        {
                            string[] ParentChild = cIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                        }
                    }
                    BindFleetTree(Fleetlst);
                    BindCrewTree(Crewlst);
                }

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();

                    if (IsSave)
                    {
                        var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                        var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);

                        objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Corporate, selectedFleetIDs, selectedCrewIDs, null, false);
                    }
                    else
                    {
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Corporate);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CrewIDs = FleetCrew[1];

                            if ((string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0) && (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0))
                            {
                                if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                                {
                                    FleetTreeView.CheckAllNodes();
                                }
                            }

                            Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                            Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                            if (!string.IsNullOrEmpty(FleetCrew[0]))
                            {
                                string[] Fleetids = FleetCrew[0].Split(',');
                                foreach (string fIDs in Fleetids)
                                {
                                    string[] ParentChild = fIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                }
                            }
                            else
                                Fleetlst = GetCheckedTreeNodes(FleetTreeView);

                            if (!string.IsNullOrEmpty(FleetCrew[1]))
                            {
                                string[] Crewids = FleetCrew[1].Split(',');
                                foreach (string cIDs in Crewids)
                                {
                                    string[] ParentChild = cIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                }
                            }                           
                        }
                    }

                    CorporateFleetTreeCount = FleetTreeView.CheckedNodes.Count;
                    CorporateCrewTreeCount = CrewTreeView.CheckedNodes.Count;

                    BindCorporateGrid(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Corporate);
                    dgCorporateView.Visible = true;
                }


                RadPanelBar2.FindItemByText("Fleet").Visible = true;
                RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                RadPanelBar2.FindItemByText("Crew").Visible = true;
                RadPanelBar2.FindItemByText("Crew").Expanded = true;
                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;

                RightPane.Collapsed = true;

            }
        }

        private Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime weekStartDate, DateTime weekEndDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {

                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();
                // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                DateTime? FirstdayTripDate = null;
                Int64? FirstdayTripID = null;

                foreach (var tripNo in distinctTripNos)
                {
                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList().OrderBy(x => x.DepartureDisplayTime).ToList();

                    foreach (var leg in groupedLegs)
                    {
                        if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                        {
                            int i = 0;
                            DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                            DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                            TimeSpan span = EndDate.Subtract(StartDate);
                            int totaldays = 0;

                            if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                            {
                                totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                            }
                            else
                            {
                                totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                            }

                            while (i < totaldays)
                            {
                                leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                if (leg.DepartureDisplayTime >= weekStartDate && leg.DepartureDisplayTime <= weekEndDate)
                                {
                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                    appointmentCollection.Add(appointment);
                                }
                                i++;
                            }
                        }
                        else
                        {
                            if (leg.DepartureDisplayTime >= weekStartDate && leg.DepartureDisplayTime <= weekEndDate)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                if (appointment.RecordType == "T")
                                {
                                    if (appointment.LegNum != 0)
                                    {
                                        FirstdayTripDate = appointment.DepartureDisplayTime.Date;
                                        FirstdayTripID = appointment.TripId;
                                        appointmentCollection.Add(appointment);
                                    }
                                    else if (appointment.IsRONAppointment && FirstdayTripDate != null && FirstdayTripID != null)
                                    {
                                        if (appointment.DepartureDisplayTime.Date == FirstdayTripDate && appointment.TripId == FirstdayTripID)
                                        {
                                            FirstdayTripDate = null;
                                            FirstdayTripID = null;
                                        }
                                        else
                                            appointmentCollection.Add(appointment);
                                    }
                                    else
                                        appointmentCollection.Add(appointment);
                                }
                                else
                                    appointmentCollection.Add(appointment);
                            }                         
                        }                                        
                    }
                }
                return appointmentCollection;
            }
        }

        private Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {

                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();
                // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                DateTime? FirstdayTripDate = null;
                Int64? FirstdayTripID = null;

                foreach (var tripNo in distinctTripNos)
                {
                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList().OrderBy(x => x.DepartureDisplayTime).ToList();

                    foreach (var leg in groupedLegs)
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                        SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                        if (appointment.RecordType == "T")
                        {
                            if (appointment.LegNum != 0)
                            {
                                FirstdayTripDate = appointment.DepartureDisplayTime.Date;
                                FirstdayTripID = appointment.TripId;
                                appointmentCollection.Add(appointment);
                            }
                            else if (appointment.IsRONAppointment && FirstdayTripDate != null && FirstdayTripID != null)
                            {
                                if (appointment.DepartureDisplayTime.Date == FirstdayTripDate && appointment.TripId == FirstdayTripID)
                                {
                                    FirstdayTripDate = null;
                                    FirstdayTripID = null;
                                }
                                else
                                    appointmentCollection.Add(appointment);
                            }
                            else
                                appointmentCollection.Add(appointment);
                        }
                        else
                            appointmentCollection.Add(appointment);
                    }
                }
                return appointmentCollection;
            }
        }

        private void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);
                appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                SetDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns
                }
            }
        }

        private void SetCrewAppointment(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);
                appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                SetCrewDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns
                }
            }
        }

        private static void SetCrewDepartureAndArrivalInfo(DisplayOptions displayOptions, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {
                switch (displayOptions.Corporate.DepartArriveInfo)
                {
                    case DepartArriveInfo.ICAO:
                        appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                        appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                        break;

                    case DepartArriveInfo.AirportName:
                        appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                        appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                        break;

                    case DepartArriveInfo.City:
                        appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                        appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                        break;
                }
            }
        }

        private void SetDepartureAndArrivalInfo(DisplayOptions displayOptions, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {
                if (appointment.ShowAllDetails)
                {
                    switch (displayOptions.Corporate.DepartArriveInfo)
                    {

                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;


                    }
                }
                else
                {
                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (displayOptions.Corporate.DepartArriveInfo)
                        {

                            case DepartArriveInfo.ICAO:
                                appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;


                        }
                    }
                    else
                    {
                        appointment.DepartureICAOID = string.Empty;
                        appointment.ArrivalICAOID = string.Empty;

                    }

                    if (!PrivateTrip.IsShowArrivalDepartTime)
                    {
                        appointment.StartDateTime = string.Empty;
                        appointment.EndDateTime = string.Empty;
                    }
                }

            }

        }

        // ON click of Apply button...
        protected void btnApply_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                          RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;

                            loadTree(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }

        }

        private void BindCorporateGrid(PreflightServiceClient preflightServiceClient, DateTime weekStartDate, DateTime weekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, weekStartDate, weekEndDate, filterCriteria, displayOptions, viewMode))
            {

                dgCorporateView.Columns.FindByUniqueName("TripStatus").Visible = displayOptions.Corporate.ShowTripStatus;

                if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                {
                    FleetTreeView.CheckAllNodes();
                }

                var schedulingCalendar = new SchedulingCalendar();
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);
                var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView).Distinct().ToList();
                var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView).Distinct().ToList();

                CorporateFleetTreeCount = inputFromFleetTree.Count; // this property is used in appointment data bound to identify the view 
                CorporateCrewTreeCount = inputFromCrewTree.Count; // this property is used in appointment data bound to identify the view 

                Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                if (inputFromFleetTree.Count > 0) // if fleet tree is selected
                {
                    Collection<string> InputFromCrewTreeAll = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                    {
                        InputFromCrewTreeAll.Add(s.ChildNodeId);
                    }
                    var calendarFleetData = preflightServiceClient.GetFleetCalendarData(weekStartDate.AddDays(-31), weekEndDate.AddDays(31), serviceFilterCriteria, InputFromCrewTreeAll.ToList(), true, false, false);
                    //calendarFleetData.EntityList = calendarFleetData.EntityList.Where(x => x.DepartureDisplayTime >= weekStartDate).ToList();
                    calendarFleetData.EntityList = calendarFleetData.EntityList.ToList();
                    // by default REcordType, T, M and C with tailNum will be displayed...
                    calendarFleetData.EntityList = calendarFleetData.EntityList.Where(x => x.TailNum != null).ToList();

                    fleetappointments = ConvertToFleetAppointmentEntity(calendarFleetData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode,weekStartDate, weekEndDate);
                    //dgCorporateView.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList();
                }

                Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                if (inputFromCrewTree.Count > 0) // if crew tree is selected
                {
                    Collection<string> InputFromCrewTreeAll = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                    {
                        InputFromCrewTreeAll.Add(s.ChildNodeId);
                    }
                    var calendarCrewData = preflightServiceClient.GetCrewCalendarData(weekStartDate.AddDays(-31), weekEndDate.AddDays(31), serviceFilterCriteria, InputFromCrewTreeAll.ToList(), false);
                    calendarCrewData.EntityList = calendarCrewData.EntityList.Where(x => x.DepartureDisplayTime >= weekStartDate).ToList();
                    crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);
                    //dgCorporateView.DataSource = crewappointments.OrderBy(x => x.StartDate).ToList();
                }

                if (fleetappointments.OrderBy(x => x.StartDate).ToList().Count > 0 && crewappointments.OrderBy(x => x.StartDate).ToList().Count > 0)
                {
                    dgCorporateView.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList().Concat(crewappointments.OrderBy(x => x.StartDate).ToList()).ToList();
                }
                else if (fleetappointments.OrderBy(x => x.StartDate).ToList().Count > 0)
                {
                    dgCorporateView.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList();
                }
                else if (crewappointments.OrderBy(x => x.StartDate).ToList().Count > 0)
                {
                    dgCorporateView.DataSource = crewappointments.OrderBy(x => x.StartDate).ToList();
                }
                else
                    dgCorporateView.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList(); 

                dgCorporateView.DataBind();

                var departureArrivalInfo = DisplayOptions.Corporate.DepartArriveInfo;
            }
        }

        protected void dgCorporateView_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            Entities.SchedulingCalendar.Appointment appt = (Entities.SchedulingCalendar.Appointment)e.Item.DataItem;

                            //When appointment is RON, then do not show passenger codes
                            if (appt.RecordType == "T")
                            {
                                if (appt.IsRONAppointment)
                                {
                                    item["PassengerCodes"].Text = " ";
                                }
                            }

                            if (DisplayOptions.Corporate.BlackWhiteColor)
                            {
                                e.Item.BackColor = Color.White;
                                e.Item.ForeColor = Color.Black;
                            }
                            // if black and white color not selected - default is off – can only be selected if Aircraft color is not selected; 
                            else
                            {
                                if (!DisplayOptions.Corporate.AircraftColors)
                                {
                                    if (appt.RecordType == "T")
                                    {
                                        if (appt.IsRONAppointment)
                                        {
                                      
                                            // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                            if (CorporateFleetTreeCount > 0) // fleet view
                                            {
                                                if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(appt.AircraftDutyCD))
                                                {
                                                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == appt.AircraftDutyCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                            else if (CorporateCrewTreeCount > 0) // crew view => CorporateCrewTreeCount > 0
                                            {
                                                if (CrewDutyTypes.Count > 0 && !string.IsNullOrEmpty(appt.CrewDutyTypeCD))
                                                {
                                                    var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == appt.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            SetFlightCategoryColor(e, e.Item);
                                        }
                                    }
                                    else if (appt.RecordType == "M")
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                        SetAircraftDutyColor(e, e.Item);
                                    }
                                    else if (appt.RecordType == "C")
                                    {
                                        // set crew duty type color...
                                        SetCrewDutyColor(e, e.Item);
                                    }
                                }
                                else if (DisplayOptions.Corporate.AircraftColors && appt.TailNum == null && appt.RecordType == "C")
                                {
                                    SetCrewDutyColor(e, e.Item);
                                }
                                else // AircraftColors checked
                                {
                                    if (appt.AircraftForeColor != null && appt.AircraftBackColor != null)
                                    {
                                        e.Item.BackColor = Color.FromName(appt.AircraftBackColor);
                                        e.Item.ForeColor = Color.FromName(appt.AircraftForeColor);
                                    }
                                }
                            }


                            if (!appt.ShowAllDetails) // show as per company profile settings - trip privacy settings
                            {
                                var options = DisplayOptions.Corporate;


                                if (PrivateTrip.IsShowCrew)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["CrewCodes"].Text = " ";
                                }

                                if (PrivateTrip.IsShowCrew)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["PassengerCodes"].Text = " ";
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }

        }

        private void SetAircraftDutyColor(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                var appointmentAircraftDuty = appointmentItem.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                    }
                }

            }

        }

        private void SetFlightCategoryColor(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                var appointmentCategory = appointmentItem.FlightCategoryID;
                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }

                }

            }

        }

        private void SetCrewDutyColor(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                var appointmentCategory = appointmentItem.CrewDutyType;
                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }

                }

            }

        }

        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCCorporatePreviousCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            //RadDatePicker1.SelectedDate = StartDate;
                            tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            loadTree(false);
                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            //RadDatePicker1.SelectedDate = StartDate;
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCCorporatePreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }

        }

        protected void nextButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCCorporateNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            //RadDatePicker1.SelectedDate = StartDate;
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            // lbStartDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            // lbEndDate.Text = EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            //RadDatePicker1.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCCorporateNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCCorporateLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCCorporateLastCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }


        }

        protected void btnFirst_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCCorporateFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            //DateTime today = DateTime.Today;
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCCorporateFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }

        }



        protected void dgCorporateView_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridSortExpression sortExpr = new GridSortExpression();
                        switch (e.OldSortOrder)
                        {
                            case GridSortOrder.None:
                                sortExpr.FieldName = e.SortExpression;
                                sortExpr.SortOrder = GridSortOrder.Descending;

                                e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                                break;
                            case GridSortOrder.Ascending:
                                sortExpr.FieldName = e.SortExpression;
                                sortExpr.SortOrder = dgCorporateView.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                                e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                                break;
                            case GridSortOrder.Descending:
                                sortExpr.FieldName = e.SortExpression;
                                sortExpr.SortOrder = GridSortOrder.Ascending;

                                e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                                break;
                        }

                        e.Canceled = true;
                        dgCorporateView.Rebind();

                        if (!e.Item.OwnerTableView.SortExpressions.ContainsExpression(e.SortExpression))
                        {
                            GridSortExpression sortExprDefault = new GridSortExpression();
                            sortExprDefault.FieldName = e.SortExpression;
                            sortExprDefault.SortOrder = GridSortOrder.Ascending;
                            e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExprDefault);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }
        }

        protected void RadCorporateGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var clickedIndex = Request.Form["radGridClickedRowIndex"];
                        Session.Remove("CurrentPreFlightTrip");
                        if (e.Item.Value == string.Empty)
                        {
                            return;
                        }

                        if (string.IsNullOrEmpty(clickedIndex))// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            if ((e.Item.Text == "Preflight") || (e.Item.Text == "Add New Trip"))
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of login user 
                                PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                Trip.EstDepartureDT = StartDate;
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = BaseDescription;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;

                                if (e.Item.Text == "Add New Trip")
                                {
                                    Response.Redirect(e.Item.Value);
                                } 
                                else if (e.Item.Text == "Preflight")
                                {
                                    if (Trip.TripNUM == null || Trip.TripNUM == 0)
                                    {
                                        Session["CurrentPreFlightTrip"] = null;
                                        Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                    }
                                    else
                                        Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                                }
                                else
                                    Response.Redirect(e.Item.Value);
                            }
                            else if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                            {// Fleet calendar entry

                                RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Add Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" );
                            }
                            else if (e.Item.Text == "UWA Services")
                            {
                                return;
                            }
                            else
                            {
                                Response.Redirect(e.Item.Value);
                            }
                        }


                        //get trip by tripId
                        var selectedTripId = dgCorporateView.MasterTableView.Items[clickedIndex].GetDataKeyValue("TripId");
                        var selectedTailNum = dgCorporateView.MasterTableView.Items[clickedIndex].GetDataKeyValue("TailNum");
                        var selectedDate = dgCorporateView.MasterTableView.Items[clickedIndex].GetDataKeyValue("StartDate");
                        var selectedLegNum = dgCorporateView.MasterTableView.Items[clickedIndex].GetDataKeyValue("LegNum");
                        if (e.Item.Text == "Add New Trip")// On click on calendar entries
                        {
                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                            Trip.EstDepartureDT = Convert.ToDateTime(selectedDate);
                            if (selectedTailNum != null)
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of Fleet in preflight 
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                PreflightService.Aircraft AircraftSample = new PreflightService.Aircraft();
                                Int64 HomebaseAirportId = 0;
                                if (fleetsample.FleetID != null)
                                {
                                    Int64 fleetHomebaseId = 0;
                                    Int64 fleetId = 0;
                                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var FleetByTailNumber = FPKMasterService.GetFleetByTailNumber(selectedTailNum.ToString().Trim()).EntityInfo;
                                        if (FleetByTailNumber != null)
                                        {
                                            fleetId = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)FleetByTailNumber).FleetID;
                                            fleetsample.TailNum = selectedTailNum.ToString();
                                            fleetsample.FleetID = fleetId;

                                        }
                                        var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                        if (FleetList.Count > 0)
                                        {
                                            GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                            if (FleetCatalogEntity.HomebaseID != null)
                                            {
                                                fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                AircraftSample.AircraftDescription = FleetCatalogEntity.AircraftDescription;
                                                AircraftSample.AircraftCD = FleetCatalogEntity.AirCraft_AircraftCD;
                                                AircraftSample.AircraftID = Convert.ToInt64(FleetCatalogEntity.AircraftID);
                                                Trip.HomebaseID = fleetHomebaseId;
                                                Trip.Fleet = fleetsample;
                                                Trip.Aircraft = AircraftSample;
                                            }
                                        }
                                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                        if (CompanyList.Count > 0)
                                        {
                                            GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                            if (CompanyCatalogEntity.HomebaseAirportID != null)
                                            {
                                                HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                HomebaseSample.BaseDescription = BaseDescription;
                                                Trip.Company = HomebaseSample;
                                            }
                                        }
                                        var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                        if (AirportList.Count > 0)
                                        {
                                            GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                            if (AirportCatalogEntity.IcaoID != null)
                                            {
                                                Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                            }
                                            if (AirportCatalogEntity.AirportID != null)
                                            {
                                                Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                            }
                                        }
                                    }                                    
                                }
                            }

                            Session["CurrentPreFlightTrip"] = Trip;
                            Session.Remove("PreflightException");
                            Trip.State = TripEntityState.Added;
                            Trip.Mode = TripActionMode.Edit;
                            Response.Redirect(e.Item.Value);
                            /*if (Trip.TripNUM != null && Trip.TripNUM != 0)
                            {
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                                Response.Redirect(e.Item.Value);
                            }
                            else
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }*/
                        }
                        //get trip by tripId
                        Int64 tripId = Convert.ToInt64(selectedTripId);
                        if (tripId != 0)
                        {

                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                {
                                    var trip = tripInfo.EntityList[0];

                                    //Set the trip.Mode and Trip.State to NoChage for read only                    
                                    trip.Mode = TripActionMode.NoChange;                 
                                    trip.Mode = TripActionMode.Edit;
                                    Session["CurrentPreFlightTrip"] = trip;
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");


                                    if (e.Item.Text == "Outbound Instruction") // outbound instruction menu selected...
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State= TripEntityState.Modified;
                                        RadWindow5.NavigateUrl = e.Item.Value + "?IsCalender=1";
                                        RadWindow5.Visible = true;
                                        RadWindow5.VisibleOnPageLoad = true;
                                        RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                        return;
                                    }
                                    else if (e.Item.Text == "Leg Notes") // Leg Notes menu selected...
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        RadWindow6.NavigateUrl = e.Item.Value + "?IsCalender=1&Leg=" + selectedLegNum;
                                        RadWindow6.Visible = true;
                                        RadWindow6.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                                    {// Fleet calendar entry
                                        string tailNumber = selectedTailNum != null ? selectedTailNum.ToString() : null;
                                        RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + tailNumber + "&startDate=" + selectedDate + "&endDate=" + selectedDate; // no need to pass tailNum since it has no resource
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                                    { // Crew calendar entry
                                        RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + selectedDate + "&endDate=" + selectedDate;// no need to pass crewCode since it has no resource
                                        RadWindow4.Visible = true;
                                        RadWindow4.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else if (e.Item.Text == "Preflight")
                                    {                                        
                                        if (trip.TripNUM != null && trip.TripNUM != 0)
                                        {
                                            Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                                        }
                                        else
                                        {
                                            Session["CurrentPreFlightTrip"] = null;
                                            Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                        }
                                    }
                                    else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                    {
                                        RedirectToPage(string.Format("{0}?xmlFilename=PRETSOverview.xml&tripnum={1}&tripid={2}", WebConstants.URL.TripSheetReportViewer, Microsoft.Security.Application.Encoder.HtmlEncode(trip.TripNUM.ToString()), Microsoft.Security.Application.Encoder.HtmlEncode(trip.TripID.ToString())));
                                    }
                                    else
                                    {
                                        Response.Redirect(e.Item.Value);
                                    }
                                }
                            }
                        }
                        else //tripid=0, that is dummy appointment
                        {
                            if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                            {// Fleet calendar entry
                                string tailNumber = selectedTailNum != null ? selectedTailNum.ToString() : null;
                                RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + tailNumber + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Preflight")
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }
                            else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                            }
                            else
                            {
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }
        }

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.Corporate);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Corporate);
                }
            }

        }
    }
}