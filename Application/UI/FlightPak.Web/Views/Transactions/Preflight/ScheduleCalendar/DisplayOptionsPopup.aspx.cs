﻿//****************************************************************************************************************************************************************
// Author: Kathiresan Moorthy
// Date : 21-June-2012
// Organization : Gavs
// Version History :
// 1. Created - 21-June-2012
//
//****************************************************************************************************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PreflightService;
using FlightPak.Common;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using System.Collections.ObjectModel;


namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    /// <summary>
    /// Advanced filters
    /// </summary>
    public partial class DisplayOptionsPopup : ScheduleCalendarBase
    {
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">args</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            RestoreDefaultSettings();
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }

        }

        /// <summary>
        /// When tab changes
        /// </summary>
        /// <param name="sender">Tab</param>
        /// <param name="e">args</param>
        protected void TabSchedulingCalendar_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RestoreDefaultSettings();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }

        }



        protected void ApplyWithOutSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var client = new PreflightServiceClient())
                        {
                            var selectedOption = (CurrentDisplayOption)Session["SCAdvancedTab"];
                            string selectedOptionValue = Convert.ToString((int)selectedOption);
                            pnlNavigation.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(selectedOptionValue)).Selected = true;

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>((AdvancedFilterSettings)Session["SCUserSettings"]);
                            var userSettingsString = GetAdvancedFilterSettings(CurrentAdvancedFilterTab.DisplayOptions, serializedXml, selectedOption);
                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            Session["SCUserSettings"] = options;

                            SetSelectedView(selectedOption);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }
        }

        private void SetSelectedView(CurrentDisplayOption selectedOption)
        {
            String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            strUrl = strUrl + "Views/Transactions/Preflight/ScheduleCalendar/";


            //    // to load the view automatically
            switch (selectedOption)
            {
                case CurrentDisplayOption.Planner:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.Planner;
                    strUrl = strUrl + "Planner.aspx?seltab=Planner";
                    break;

                case CurrentDisplayOption.WeeklyCrew:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;
                    strUrl = strUrl + "WeeklyCrew.aspx?seltab=Weekly";
                    break;

                case CurrentDisplayOption.Weekly:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;
                    strUrl = strUrl + "WeeklyMain.aspx?seltab=Weekly";
                    break;

                case CurrentDisplayOption.WeeklyDetail:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                    strUrl = strUrl + "WeeklyDetail.aspx?seltab=Weekly";
                    break;

                case CurrentDisplayOption.Monthly:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.Monthly;
                    strUrl = strUrl + "Monthly.aspx?seltab=Monthly";
                    break;

                case CurrentDisplayOption.BusinessWeek:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.BusinessWeek;
                    strUrl = strUrl + "BusinessWeek.aspx?seltab=BusinessWeek";
                    break;

                case CurrentDisplayOption.Corporate:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.Corporate;
                    strUrl = strUrl + "CorporateView.aspx?seltab=Corporate";
                    break;

                case CurrentDisplayOption.Day:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.Day;
                    strUrl = strUrl + "DayView.aspx?seltab=Day";
                    break;

                case CurrentDisplayOption.WeeklyFleet:
                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                    strUrl = strUrl + "SchedulingCalendar.aspx?seltab=Weekly";
                    break;
            }

            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "RadWindowClose('" + strUrl + "');", true);
        }

        /// <summary>
        /// Saves default settings
        /// </summary>
        /// <param name="sender">save button</param>
        /// <param name="e">args</param>
        protected void SaveDefault_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdDisplay.Value = "UserDefault";
                        RadWindowManager1.RadConfirm("Set User Calendar Defaults?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }
        }

        protected void btnCopyYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var client = new PreflightServiceClient())
                        {
                            // Session["SCUserSettings"] = GetUserSettings();
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];
                            var selectedOption = (CurrentDisplayOption)Session["SCAdvancedTab"];
                            string selectedOptionValue = Convert.ToString((int)selectedOption);
                            pnlNavigation.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(selectedOptionValue)).Selected = true;

                            if (hdDisplay.Value == "UserDefault")
                            {
                                //// get settings from database and update display options to user settings... and then save

                                var userSettings = client.RetrieveFilterSettings(); // For new user userSettings will be string.Empty  
                                if (userSettings == string.Empty) // if new user, take system default settings and overwrite with users selected settings..
                                {
                                    userSettings = client.RetrieveSystemDefaultSettings();
                                }

                                userSettings = GetAdvancedFilterSettings(CurrentAdvancedFilterTab.DisplayOptions, userSettings, selectedOption);
                                var settingsObj = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettings);

                                filterAndDisplaySettings.Display = settingsObj.Display;

                                client.SaveUserDefaultSettings(XMLSerializationHelper.Serialize(filterAndDisplaySettings));

                                Session["SCUserSettings"] = filterAndDisplaySettings;
                                SetSelectedView(selectedOption);
                            }
                            else
                            {
                                string settings = client.RetrieveSystemDefaultSettings();

                                // var settings = GetAdvancedFilterSettings(CurrentAdvancedFilterTab.DisplayOptions, settings, selectedOption);
                                var settingsObj = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(settings);
                                filterAndDisplaySettings.Display = settingsObj.Display;

                                PopulateAdvancedFilter(CurrentAdvancedFilterTab.DisplayOptions, settings, selectedOption);
                                client.SaveUserDefaultSettings(settings);
                                // Session["SCUserSettings"] = GetUserSettings();
                                Session["SCUserSettings"] = filterAndDisplaySettings;
                                SetSelectedView(selectedOption);
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
        protected void btnCopyNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }
        /// <summary>
        /// Restores default settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RestoreSystemDefault_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdDisplay.Value = "SystemDefault";
                        RadWindowManager1.RadConfirm("Do you wish to set the Display Options to the Initial System Default Values?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }
        }


        /// <summary>
        /// Resores the default settings for the current user if there is settings specific to user
        /// else loads the system default settings.
        /// </summary>
        private void RestoreDefaultSettings()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (var client = new PreflightServiceClient())
                {
                    if (Session["SCAdvancedTab"] != null && Session["SCAdvancedTab"] != string.Empty)
                    {
                        var selectedOption = (CurrentDisplayOption)Session["SCAdvancedTab"];
                        string selectedOptionValue = Convert.ToString((int)selectedOption);
                        pnlNavigation.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(selectedOptionValue)).Selected = true;

                        string settings = string.Empty;
                        var existingUser = client.IsUserSettingsAvailable();
                        if (existingUser)
                        {
                            settings = client.RetrieveFilterSettings();
                        }
                        else
                        {
                            settings = client.RetrieveSystemDefaultSettings();
                        }

                        PopulateAdvancedFilter(CurrentAdvancedFilterTab.DisplayOptions, settings, selectedOption);
                    }
                }
            }
        }

        public string GetUserFilterAndDisplayOptions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string settings = string.Empty;
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var existingUser = preflightServiceClient.IsUserSettingsAvailable();
                    if (existingUser)
                    {
                        settings = preflightServiceClient.RetrieveFilterSettings();
                    }
                    else
                    {
                        settings = preflightServiceClient.RetrieveSystemDefaultSettings();
                    }
                }
                return settings;
            }
        }

        public string GetUserScheduleCalenderPrefernces(string CalenderType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string scsettings = string.Empty;
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var existingUser = preflightServiceClient.IsScheduleCalenderPreferencesAvailable(CalenderType);
                    if (existingUser)
                    {
                        scsettings = preflightServiceClient.RetrieveScheduleCalenderPreferences(CalenderType);
                    }
                    else
                    {
                        scsettings = "|"+"|";
                    }
                }
                return scsettings;
            }
        }


        public string GetUserScheduleCalenderPreferncesMonthly(string CalenderType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string scsettings = string.Empty;
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var existingUser = preflightServiceClient.IsScheduleCalenderPreferencesAvailable(CalenderType);
                    if (existingUser)
                    {
                        scsettings = preflightServiceClient.RetrieveScheduleCalenderPreferencesMonthly(CalenderType);
                    }
                    else
                    {
                        scsettings = "|";
                    }
                }
                return scsettings;
            }
        }

        public void SetUserScheduleCalenderPrefernces(string CalenderType, Collection<TreeviewParentChildNodePair> selectedFleetIDs, Collection<TreeviewParentChildNodePair> selectedCrewIDs, Collection<TreeviewParentChildNodePair> selectedCrewGroupIDs, bool isCrewGroup)        
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    string Fids = string.Empty;
                    string Cids = string.Empty;
                    string Cgrpids = string.Empty;

                    if (selectedFleetIDs != null)
                    {
                        //To get the distinct values
                        List<string> distinctFleet = selectedFleetIDs.Select(node => node.ParentNodeId + "$" + node.ChildNodeId).Distinct().ToList();
                        Fids = string.Join(",", distinctFleet.ToArray());
                        
                        /*foreach (string fIDs in selectedFleetIDs)
                        {
                            if (Fids == string.Empty)
                                Fids = fIDs;
                            else
                                Fids = Fids + "," + fIDs;
                        }*/
                    }

                    if (selectedCrewIDs != null)
                    {
                        //To get the distinct values
                        List<string> distinctCrew = selectedCrewIDs.Select(node => node.ParentNodeId + "$" + node.ChildNodeId).Distinct().ToList();
                        Cids = string.Join(",", distinctCrew.ToArray());
                        /*foreach (string cIDs in selectedCrewIDs)
                        {
                            if (Cids == string.Empty)
                                Cids = cIDs;
                            else
                                Cids = Cids + "," + cIDs;
                        }*/
                    }

                    if (selectedCrewGroupIDs != null)
                    {
                        //To get the distinct values
                        List<string> distinctCrewGroup = selectedCrewGroupIDs.Select(node => node.ParentNodeId + "$" + node.ChildNodeId).Distinct().ToList();
                        Cgrpids = string.Join(",", distinctCrewGroup.ToArray());
                        /*foreach (string cIDs in selectedCrewIDs)
                        {
                            if (Cids == string.Empty)
                                Cids = cIDs;
                            else
                                Cids = Cids + "," + cIDs;
                        }*/
                    }

                    preflightServiceClient.SaveScheduleCalanderPreferences(CalenderType, Fids, Cids, Cgrpids, isCrewGroup);
                }
            }
        }

        public void SetUserScheduleCalenderPreferncesMonthly(string CalenderType, bool IsSaturdaySundayWeek, bool IsNoPastWeek)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    preflightServiceClient.SaveScheduleCalanderPreferencesMonthly(CalenderType, IsSaturdaySundayWeek, IsNoPastWeek);
                }
            }
        }

        /// <summary>
        /// On Navigation item clicked, resore the default settings for the view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pnlNavigation_ItemClick(object sender, Telerik.Web.UI.RadPanelBarEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (int.Parse(e.Item.Value) == -1)
                            return;
                        // TO DO : Logic to be defined to pick the data from backend or Session.
                        Session["SCAdvancedTab"] = (CurrentDisplayOption)(int.Parse(e.Item.Value));
                        RestoreDefaultSettings();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }

        }

        /// <summary>
        ///  Restores the data from Session
        /// </summary>
        private void ResoreFromSession()
        {
            // TO DO : Restore the data from session.
        }

        /// <summary>
        /// Enabling the display options based on the current selected view and loads the control as well.
        /// </summary>
        /// <param name="selectedOption"></param>
        /// <param name="dispOption"></param>
        private void HideShowDisplayOptionItems(CurrentDisplayOption selectedOption, DisplayOptions dispOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedOption, dispOption))
            {

                DisableAll();
                switch (selectedOption)
                {
                    case CurrentDisplayOption.Planner:
                        EnablePlannerOption(dispOption.Planner);
                        break;
                    case CurrentDisplayOption.WeeklyFleet:
                        EnableWeeklyFleetOption(dispOption.WeeklyFleet);
                        break;
                    case CurrentDisplayOption.WeeklyCrew:
                        EnableWeeklyCrewOption(dispOption.WeeklyCrew);
                        break;
                    case CurrentDisplayOption.Monthly:
                        EnableMonthlyViewOption(dispOption.Monthly);
                        break;
                    case CurrentDisplayOption.Weekly:
                        EnableWeeklyViewOption(dispOption.Weekly);
                        break;
                    case CurrentDisplayOption.BusinessWeek:
                        EnableBusinessWeekOption(dispOption.BusinessWeek);
                        break;
                    case CurrentDisplayOption.Day:
                        EnableDayOption(dispOption.Day);
                        break;
                    case CurrentDisplayOption.Corporate:
                        EnableCorporate(dispOption.Corporate);
                        break;
                    case CurrentDisplayOption.WeeklyDetail:
                        EnableWeeklyDetail(dispOption.WeeklyDetail);
                        break;
                }

            }

        }

        /// <summary>
        /// Diables all display options controls
        /// </summary>
        private void DisableAll()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                PanelPlannerDisplay.Visible = false;
                PanelWeeklyFleetCrewDisplayOption.Visible = false;
                PanelDayWeeklyDetailSortOption.Visible = false;
                PanelDisplayLegFleet.Visible = false;
                PanelDisplayTailCrew.Visible = false;

                // reset the radio buttons
                radioBoth.Checked = false;
                radioFleetOnly.Checked = false;
                radioCrewOnly.Checked = false;

                radICAODep.Checked = false;
                radCityDep.Checked = false;
                radAirportNameDep.Checked = false;

                radioTailNumber.Checked = false;
                radioDepatureDateTime.Checked = false;


                //reset the visibility and values of all the checkboxes
                chkShowTrip.Visible = chkShowTrip.Checked = false;
                chkActivityOnly.Visible = chkActivityOnly.Checked = false;
                chkFirstLastLeg.Visible = chkFirstLastLeg.Checked = false;
                chkCrew.Visible = chkCrew.Checked = false;
                chkCrewCalActivity.Visible = chkCrewCalActivity.Checked = false;
                chkPaxFullName.Visible = chkPaxFullName.Checked = false;
                chkShowTripNumber.Visible = chkShowTripNumber.Checked = false;
                chkCrewFullName.Visible = chkCrewFullName.Checked = false;
                chkPax.Visible = chkPax.Checked = false;
                chkAircraftColors.Visible = chkAircraftColors.Checked = false;
                chkNonReversedColors.Visible = chkNonReversedColors.Checked = false;
                chkShowTripStatus.Visible = chkShowTripStatus.Checked = false;
                chkDisplayRedEyeFlights.Visible = chkDisplayRedEyeFlights.Checked = false;
                chkBlackWhiteColor.Visible = chkBlackWhiteColor.Checked = false;
                chkSaveColoumnWidths.Visible = chkSaveColoumnWidths.Checked = false;
                chkFlightNumber.Visible = chkFlightNumber.Checked = false;
                chkLegPurpose.Visible = chkLegPurpose.Checked = false;
                chkTripPurpose.Visible = chkTripPurpose.Checked = false;
                chkSeatsAvailable.Visible = chkSeatsAvailable.Checked = false;
                chkPaxCount.Visible = chkPaxCount.Checked = false;
                chkNoLineSeparator.Visible = chkNoLineSeparator.Checked = false;
                chkRequestor.Visible = chkRequestor.Checked = false;
                chkDepartment.Visible = chkDepartment.Checked = false;
                chkAuthorization.Visible = chkAuthorization.Checked = false;
                chkFlightCategory.Visible = chkFlightCategory.Checked = false;
                chkCummulativeETE.Visible = chkCummulativeETE.Checked = false;
                chkTotalETE.Visible = chkTotalETE.Checked = false;
                chkETE.Visible = chkETE.Checked = false;
                chkDisplayRONPax.Visible = chkETE.Checked = false ;
            }
        }

        /// <summary>
        /// Enables Planner view display options
        /// </summary>
        private void EnablePlannerOption(Planner plannerOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(plannerOption))
            {

                // Enable visibility
                EnableBaseOption(plannerOption);
                PanelPlannerDisplay.Visible = true;
                chkAircraftColors.Visible = true;
                chkNonReversedColors.Visible = true;
                chkActivityOnly.Visible = chkActivityOnly.Checked = false;
                chkBlackWhiteColor.Visible = true;
                PanelActivity.Visible = true;
                chkShowTripNumber.Visible = true;  
                chkShowHomeBase.Visible = false;
                LabelWeeklyFleetCrewDisplayOption.Text = "";
				chkFirstLastLeg.Visible = false;
				PanelDisplayLegFleet.Visible = true;
                PanelDisplayTailCrew.Visible = true;				
                hoverOption.Visible = true;
                toggleHoverOption.Checked = plannerOption.DisplayDetailsOnHover;

                //Set Value
                if (plannerOption != null)
                {
                    if (plannerOption.PlannerDisplayOption == PlannerDisplayOption.Both)
                        radioBoth.Checked = true;
                    else if (plannerOption.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                        radioCrewOnly.Checked = true;
                    else if (plannerOption.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                        radioFleetOnly.Checked = true;

                    if (radICAODep.Checked)
                        plannerOption.DepartArriveInfo = DepartArriveInfo.ICAO;
                    else if (radCityDep.Checked)
                        plannerOption.DepartArriveInfo = DepartArriveInfo.City;
                    else
                        plannerOption.DepartArriveInfo = DepartArriveInfo.AirportName;

                    //if (plannerOption.DisplayLegFleet == DisplayLegFleet.All)
                    //    radAllLegs.Checked = true;
                    if (plannerOption.DisplayLegFleet == DisplayLegFleet.TripLastLeg)
                        radTripLastLeg.Checked = true;
                    else if (plannerOption.DisplayLegFleet == DisplayLegFleet.DayLastLeg)
                        radDayLastLeg.Checked = true;
                    else
                        radAllLegs.Checked = true;

                    //if (plannerOption.DisplayTailCrew == DisplayTailCrew.TripLastTail)
                    //    radTripLastTail.Checked = true;
                    if (plannerOption.DisplayTailCrew == DisplayTailCrew.DayLastTail)
                        radDayLastTail.Checked = true;
                    else
                        radTripLastTail.Checked = true;
                    
                    chkAircraftColors.Checked = plannerOption.AircraftColors;
                    chkNonReversedColors.Checked = plannerOption.NonReversedColors;
                    chkActivityOnly.Checked = false;//plannerOption.ActiveOnly;
                    chkBlackWhiteColor.Checked = plannerOption.BlackWhiteColor;
                    chkShowTripNumber.Checked = plannerOption.ShowTrip;   
                    chkShowTripStatus.Checked = plannerOption.ShowTripStatus;
                    chkFlightNumber.Checked = plannerOption.FlightNumber;
                    chkLegPurpose.Checked = plannerOption.LegPurpose;
                    chkTripPurpose.Checked = plannerOption.TripPurpose;
                    chkSeatsAvailable.Checked = plannerOption.SeatsAvailable;
                    chkPaxCount.Checked = plannerOption.PaxCount;
                    chkRequestor.Checked = plannerOption.Requestor;
                    chkDepartment.Checked = plannerOption.Department;
                    chkAuthorization.Checked = plannerOption.Authorization;
                    chkFlightCategory.Checked = plannerOption.FlightCategory;
                    chkCummulativeETE.Checked = plannerOption.CummulativeETE;
                    chkTotalETE.Checked = plannerOption.TotalETE;
                    chkETE.Checked = plannerOption.ETE;
                    chkShowTripNumber.Checked = plannerOption.ShowTrip;
                    chkCrew.Checked = plannerOption.Crew;   
                }
            }
        }

        /// <summary>
        /// Enables the weekly fleet view display options
        /// </summary>
        private void EnableWeeklyFleetOption(WeeklyFleetCrewOption fleetOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetOption))
            {

                EnableWeeklyFleetCrewOption(fleetOption);
                LabelWeeklyFleetCrewDisplayOption.Text = "Weekly Fleet - Display Options";
                chkShowHomeBase.Visible = true;
                hoverOption.Visible = false;

            }

        }

        /// <summary>
        /// Enables the weekly crew view display options
        /// </summary>
        private void EnableWeeklyCrewOption(WeeklyFleetCrewOption weeklyOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weeklyOption))
            {

                EnableWeeklyFleetCrewOption(weeklyOption);
                LabelWeeklyFleetCrewDisplayOption.Text = "Weekly Crew - Display Options";
                chkShowHomeBase.Visible = false;
                hoverOption.Visible = false;
            }

        }

        /// <summary>
        /// Enables the common options for weekly Fleet/Crew 
        /// </summary>
        private void EnableWeeklyFleetCrewOption(WeeklyFleetCrewOption fleetCrewOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetCrewOption))
            {

                EnableBaseOption(fleetCrewOption);
                // Set Visibility

                chkAircraftColors.Visible = true;
                chkActivityOnly.Visible = true;
                chkBlackWhiteColor.Visible = true;
                chkShowTrip.Visible = true;
                chkDisplayRedEyeFlights.Visible = true;

                chkShowHomeBase.Visible = false;
                //chkSaveColoumnWidths.Visible = true;
                // chkNoLineSeparator.Visible = true;

                // Set Value
                if (fleetCrewOption != null)
                {
                    chkAircraftColors.Checked = fleetCrewOption.AircraftColors;
                    chkActivityOnly.Checked = fleetCrewOption.ActivityOnly;
                    chkBlackWhiteColor.Checked = fleetCrewOption.BlackWhiteColor;
                    chkShowTrip.Checked = fleetCrewOption.ShowTrip;
                    chkDisplayRedEyeFlights.Checked = fleetCrewOption.DisplayRedEyeFlightsInContinuation;
                    chkSaveColoumnWidths.Checked = fleetCrewOption.SaveColumnWidths;
                    chkNoLineSeparator.Checked = fleetCrewOption.NoLineSeparator;
                }

            }

        }

        /// <summary>
        /// Enable the basic display options
        /// </summary>
        /// <param name="baseOption"></param>
        private void EnableBaseOption(BaseOptions baseOption)
        {
            // Set Visibility


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(baseOption))
            {

                PanelWeeklyFleetCrewDisplayOption.Visible = true;
                chkFirstLastLeg.Visible = true;
                chkCrew.Visible = true;
                chkShowTripStatus.Visible = true;
                chkFlightNumber.Visible = true;
                chkLegPurpose.Visible = true;
                chkTripPurpose.Visible = true;
                chkSeatsAvailable.Visible = true;
                chkPaxCount.Visible = true;
                chkRequestor.Visible = true;
                chkDepartment.Visible = true;
                chkAuthorization.Visible = true;
                chkFlightCategory.Visible = true;
                chkCummulativeETE.Visible = true;
                chkTotalETE.Visible = true;
                chkETE.Visible = true;
                PanelActivity.Visible = true;
                chkShowHomeBase.Visible = true;
                // Set Values
                if (baseOption != null)
                {
                    //PanelWeeklyFleetCrewDisplayOption.Visible = true;

                    if (baseOption.DepartArriveInfo == DepartArriveInfo.ICAO)
                        radICAODep.Checked = true;
                    else if (baseOption.DepartArriveInfo == DepartArriveInfo.City)
                        radCityDep.Checked = true;
                    else if (baseOption.DepartArriveInfo == DepartArriveInfo.AirportName)
                        radAirportNameDep.Checked = true;

                    chkFirstLastLeg.Checked = baseOption.FirstLastLeg;
                    chkCrew.Checked = baseOption.Crew;
                    chkShowTripStatus.Checked = baseOption.ShowTripStatus;
                    chkFlightNumber.Checked = baseOption.FlightNumber;
                    chkLegPurpose.Checked = baseOption.LegPurpose;
                    chkTripPurpose.Checked = baseOption.TripPurpose;
                    chkSeatsAvailable.Checked = baseOption.SeatsAvailable;
                    chkPaxCount.Checked = baseOption.PaxCount;
                    chkRequestor.Checked = baseOption.Requestor;
                    chkDepartment.Checked = baseOption.Department;
                    chkAuthorization.Checked = baseOption.Authorization;
                    chkFlightCategory.Checked = baseOption.FlightCategory;
                    chkCummulativeETE.Checked = baseOption.CummulativeETE;
                    chkTotalETE.Checked = baseOption.TotalETE;
                    chkETE.Checked = baseOption.ETE;
                    chkShowHomeBase.Checked = baseOption.ShowHomeBase;
                }

            }

        }

        

        /// <summary>
        /// Enables the weekly view display options
        /// </summary>
        private void EnableWeeklyViewOption(MonthlyWeeklyOption weeklyOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weeklyOption))
            {

                EnableMonthlyViewOption(weeklyOption);
                LabelWeeklyFleetCrewDisplayOption.Text = "Weekly - Display Options";
                chkShowHomeBase.Visible = false;
                hoverOption.Visible = false;
            }

        }

        /// <summary>
        /// Enables the business week view display options
        /// </summary>
        private void EnableBusinessWeekOption(BusinessWeekOption businessWeekOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(businessWeekOption))
            {

                EnableMonthlyViewOption(businessWeekOption);
                LabelWeeklyFleetCrewDisplayOption.Text = "Business Week - Display Options";
                // chkSaveColoumnWidths.Visible = true;
                if (businessWeekOption != null)
                    chkSaveColoumnWidths.Checked = businessWeekOption.SaveColumnWidhts;
                chkShowHomeBase.Visible = false;
                hoverOption.Visible = false;
            }

        }

        /// <summary>
        /// Enables the day view display options
        /// </summary>
        private void EnableDayOption(DayOption dayOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dayOption))
            {

                PanelWeeklyFleetCrewDisplayOption.Visible = true;
                LabelWeeklyFleetCrewDisplayOption.Text = "Day - Display Options";
                PanelDayWeeklyDetailSortOption.Visible = true;
                lblDayWeeklyDetailSortOption.Text = "Day - Sort Options";
                chkAircraftColors.Visible = true;
                chkActivityOnly.Visible = true;
                chkFirstLastLeg.Visible = true;
                chkCrewCalActivity.Visible = true;
                chkShowTripStatus.Visible = true;
                chkBlackWhiteColor.Visible = true;
                // chkSaveColoumnWidths.Visible = true;
                chkCummulativeETE.Visible = true;
                chkTotalETE.Visible = true;
                chkETE.Visible = true;
                PanelActivity.Visible = true;
                hoverOption.Visible = false;
                if (dayOption != null)
                {
                    if (dayOption.DepartArriveInfo == DepartArriveInfo.ICAO)
                        radICAODep.Checked = true;
                    else if (dayOption.DepartArriveInfo == DepartArriveInfo.City)
                        radCityDep.Checked = true;
                    else if (dayOption.DepartArriveInfo == DepartArriveInfo.AirportName)
                        radAirportNameDep.Checked = true;

                    if (dayOption.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.TailNumber)
                        radioTailNumber.Checked = true;
                    else if (dayOption.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.DepartureDateTime)
                        radioDepatureDateTime.Checked = true;

                    chkAircraftColors.Checked = dayOption.AircraftColors;
                    chkActivityOnly.Checked = dayOption.ActivityOnly;
                    chkFirstLastLeg.Checked = dayOption.FirstLastLeg;
                    chkCrewCalActivity.Checked = dayOption.CrewCalActivity;
                    chkShowTripStatus.Checked = dayOption.ShowTripStatus;
                    chkBlackWhiteColor.Checked = dayOption.BlackWhiteColor;
                    chkSaveColoumnWidths.Checked = dayOption.SaveColumnWidhts;
                    chkCummulativeETE.Checked = dayOption.CummulativeETE;
                    chkTotalETE.Checked = dayOption.TotalETE;
                    chkETE.Checked = dayOption.ETE;
                    chkShowHomeBase.Visible = false;
                }

            }

        }

        /// <summary>
        /// Enables the corporate view display options
        /// </summary>
        private void EnableCorporate(CorporateOption corporateOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(corporateOption))
            {

                PanelWeeklyFleetCrewDisplayOption.Visible = true;
                LabelWeeklyFleetCrewDisplayOption.Text = "Corporate - Display Options";
                chkAircraftColors.Visible = true;
                chkShowTripStatus.Visible = true;
                chkBlackWhiteColor.Visible = true;
                chkDisplayRONPax.Visible = true;   
                // chkSaveColoumnWidths.Visible = true;
                // chkNoLineSeparator.Visible = true;
                hoverOption.Visible = false;
                if (corporateOption != null)
                {
                    if (corporateOption.DepartArriveInfo == DepartArriveInfo.ICAO)
                        radICAODep.Checked = true;
                    else if (corporateOption.DepartArriveInfo == DepartArriveInfo.City)
                        radCityDep.Checked = true;
                    else if (corporateOption.DepartArriveInfo == DepartArriveInfo.AirportName)
                        radAirportNameDep.Checked = true;

                    chkAircraftColors.Checked = corporateOption.AircraftColors;
                    chkShowTripStatus.Checked = corporateOption.ShowTripStatus;
                    chkBlackWhiteColor.Checked = corporateOption.BlackWhiteColor;
                    chkSaveColoumnWidths.Checked = corporateOption.SaveColumnWidhts;
                    chkNoLineSeparator.Checked = corporateOption.NoLineSeparator;
                    chkDisplayRONPax.Checked = corporateOption.DisplayRONPax;
                }

                chkShowHomeBase.Visible = false;

            }

        }

        /// <summary>
        /// Enables the weekly detail view display options
        /// </summary>
        private void EnableWeeklyDetail(WeeklyDetailOption weeklyDetailOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weeklyDetailOption))
            {

                EnableBaseOption(weeklyDetailOption);
                LabelWeeklyFleetCrewDisplayOption.Text = "Weekly Detail - Display Options";
                PanelDayWeeklyDetailSortOption.Visible = true;
                lblDayWeeklyDetailSortOption.Text = "Weekly Detail - Sort Options";
                chkActivityOnly.Visible = true;
                //  chkSaveColoumnWidths.Visible = true;
                // chkNoLineSeparator.Visible = true;
                chkPaxFullName.Visible = true;
                chkShowTripNumber.Visible = true;
                chkCrewFullName.Visible = true;
                chkPax.Visible = true;
                PanelActivity.Visible = true;
                chkAircraftColors.Visible = true;
                chkBlackWhiteColor.Visible = true;
                hoverOption.Visible = false;
                if (weeklyDetailOption != null)
                {
                    if (weeklyDetailOption.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.TailNumber)
                        radioTailNumber.Checked = true;
                    else if (weeklyDetailOption.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.DepartureDateTime)
                        radioDepatureDateTime.Checked = true;

                    chkActivityOnly.Checked = weeklyDetailOption.ActivityOnly;
                    chkSaveColoumnWidths.Checked = weeklyDetailOption.SaveColumnWidhts;
                    chkNoLineSeparator.Checked = weeklyDetailOption.NoLineSeparator;
                    chkPaxFullName.Checked = weeklyDetailOption.PaxFullName;
                    chkShowTripNumber.Checked = weeklyDetailOption.ShowTripNumber;
                    chkCrewFullName.Checked = weeklyDetailOption.CrewFullName;
                    chkPax.Checked = weeklyDetailOption.Pax;
                    chkAircraftColors.Checked = weeklyDetailOption.AircraftColors;
                    chkBlackWhiteColor.Checked = weeklyDetailOption.BlackWhiteColor;
                }
                chkShowHomeBase.Visible = false;

            }

        }

        /// <summary>
        /// Encapsulates the data available from the controls into a single entity
        /// to be used in WCF service.
        /// </summary>
        /// <returns>Advanced filter settings</returns>
        public string GetAdvancedFilterSettings(CurrentAdvancedFilterTab selectedTab, string userSettings, CurrentDisplayOption currentDisplayOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedTab, userSettings, currentDisplayOption))
            {

                var settings = new AdvancedFilterSettings();

                if (!string.IsNullOrWhiteSpace(userSettings)) // existing user
                {
                    settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettings);
                }


                if (settings.Display == null)
                    settings.Display = new DisplayOptions();

                if (currentDisplayOption == CurrentDisplayOption.Planner)
                {
                    if (settings.Display.Planner == null)
                        settings.Display.Planner = new Planner();

                    var planneroption = settings.Display.Planner;
                    SetPlannerOptions(ref planneroption);
                }
                else if (currentDisplayOption == CurrentDisplayOption.WeeklyFleet
                    || currentDisplayOption == CurrentDisplayOption.WeeklyCrew)
                {
                    // TO DO : Check this
                    WeeklyFleetCrewOption fleetCrewOption = null;
                    var baseOption = new BaseOptions();
                    SetBaseOptions(ref baseOption);
                    ViewOptions viewOption = new ViewOptions(baseOption);
                    SetViewOptions(ref viewOption);
                    fleetCrewOption = new WeeklyFleetCrewOption(viewOption);

                    fleetCrewOption.NoLineSeparator = chkNoLineSeparator.Checked;
                    fleetCrewOption.SaveColumnWidths = chkSaveColoumnWidths.Checked;
                    fleetCrewOption.ActivityOnly = chkActivityOnly.Checked;

                    if (currentDisplayOption == CurrentDisplayOption.WeeklyFleet)
                    {
                        if (settings.Display.WeeklyFleet == null)
                            settings.Display.WeeklyFleet = new WeeklyFleetCrewOption();
                        settings.Display.WeeklyFleet = fleetCrewOption;
                    }
                    else if (currentDisplayOption == CurrentDisplayOption.WeeklyCrew)
                    {
                        if (settings.Display.WeeklyCrew == null)
                            settings.Display.WeeklyCrew = new WeeklyFleetCrewOption();
                        settings.Display.WeeklyCrew = fleetCrewOption;
                    }

                }
                else if (currentDisplayOption == CurrentDisplayOption.Monthly
                    || currentDisplayOption == CurrentDisplayOption.Weekly
                    || currentDisplayOption == CurrentDisplayOption.BusinessWeek)
                {
                    MonthlyWeeklyOption monthlyWeekly = null;
                    BusinessWeekOption businessOption = null;
                    var baseOption = new BaseOptions();
                    SetBaseOptions(ref baseOption);
                    ViewOptions viewOption = new ViewOptions(baseOption);
                    SetViewOptions(ref viewOption);
                    monthlyWeekly = new MonthlyWeeklyOption(viewOption);

                    monthlyWeekly.CrewCalActivity = chkCrewCalActivity.Checked;

                    if (currentDisplayOption == CurrentDisplayOption.Monthly)
                    {
                        if (settings.Display.Monthly == null)
                            settings.Display.Monthly = new MonthlyWeeklyOption();
                        settings.Display.Monthly = monthlyWeekly;
                    }
                    else if (currentDisplayOption == CurrentDisplayOption.Weekly)
                    {
                        if (settings.Display.Weekly == null)
                            settings.Display.Weekly = new MonthlyWeeklyOption();
                        settings.Display.Weekly = monthlyWeekly;
                    }
                    else // Businessweek option
                    {
                        if (settings.Display.BusinessWeek == null)
                            settings.Display.BusinessWeek = new BusinessWeekOption();
                        businessOption = new BusinessWeekOption(monthlyWeekly);
                        businessOption.SaveColumnWidhts = chkSaveColoumnWidths.Checked;
                        settings.Display.BusinessWeek = businessOption;
                    }
                }
                else if (currentDisplayOption == CurrentDisplayOption.Day)
                {
                    if (settings.Display.Day == null)
                        settings.Display.Day = new DayOption();

                    var dayOption = settings.Display.Day;
                    SetDayOptions(ref dayOption);
                }
                else if (currentDisplayOption == CurrentDisplayOption.Corporate)
                {
                    if (settings.Display.Corporate == null)
                        settings.Display.Corporate = new CorporateOption();

                    var corpOption = settings.Display.Corporate;
                    SetCorporateOptions(ref corpOption);
                }
                else // Weekly Detail
                {
                    if (settings.Display.WeeklyDetail == null)
                        settings.Display.WeeklyDetail = new WeeklyDetailOption();

                    var weeklyDetailOption = settings.Display.WeeklyDetail;
                    SetWeeklyDetailOptions(ref weeklyDetailOption);
                    settings.Display.WeeklyDetail = weeklyDetailOption;
                }

                return XMLSerializationHelper.Serialize(settings);

            }

        }

        /// <summary>
        /// Sets the corporate option
        /// </summary>
        /// <param name="corpOption"></param>
        private void SetCorporateOptions(ref CorporateOption corpOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(corpOption))
            {

                if (radICAODep.Checked)
                    corpOption.DepartArriveInfo = DepartArriveInfo.ICAO;
                else if (radCityDep.Checked)
                    corpOption.DepartArriveInfo = DepartArriveInfo.City;
                else
                    corpOption.DepartArriveInfo = DepartArriveInfo.AirportName;

                corpOption.AircraftColors = chkAircraftColors.Checked;
                corpOption.BlackWhiteColor = chkBlackWhiteColor.Checked;
                corpOption.NoLineSeparator = chkNoLineSeparator.Checked;
                corpOption.SaveColumnWidhts = chkSaveColoumnWidths.Checked;
                corpOption.ShowTripStatus = chkShowTripStatus.Checked;
                corpOption.DisplayRONPax = chkDisplayRONPax.Checked;

            }

        }

        /// <summary>
        /// Sets the weekly details option
        /// </summary>
        /// <param name="weeklyDetailOption"></param>
        private void SetWeeklyDetailOptions(ref WeeklyDetailOption weeklyDetailOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weeklyDetailOption))
            {

                var baseOption = new BaseOptions();
                SetBaseOptions(ref baseOption);
                weeklyDetailOption = new WeeklyDetailOption(baseOption);

                weeklyDetailOption.ActivityOnly = chkActivityOnly.Checked;
                weeklyDetailOption.SaveColumnWidhts = chkSaveColoumnWidths.Checked;
                weeklyDetailOption.NoLineSeparator = chkNoLineSeparator.Checked;

                if (radioTailNumber.Checked)
                    weeklyDetailOption.DayWeeklyDetailSortOption = DayWeeklyDetailSortOption.TailNumber;
                else if (radioDepatureDateTime.Checked)
                    weeklyDetailOption.DayWeeklyDetailSortOption = DayWeeklyDetailSortOption.DepartureDateTime;

                weeklyDetailOption.PaxFullName = chkPaxFullName.Checked;
                weeklyDetailOption.ShowTripNumber = chkShowTripNumber.Checked;
                weeklyDetailOption.CrewFullName = chkCrewFullName.Checked;
                weeklyDetailOption.Pax = chkPax.Checked;
                weeklyDetailOption.AircraftColors = chkAircraftColors.Checked;
                weeklyDetailOption.BlackWhiteColor = chkBlackWhiteColor.Checked;
            }

        }



        /// <summary>
        /// Sets the day option
        /// </summary>
        /// <param name="dayOption"></param>
        private void SetDayOptions(ref DayOption dayOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dayOption))
            {

                if (radICAODep.Checked)
                    dayOption.DepartArriveInfo = DepartArriveInfo.ICAO;
                else if (radCityDep.Checked)
                    dayOption.DepartArriveInfo = DepartArriveInfo.City;
                else
                    dayOption.DepartArriveInfo = DepartArriveInfo.AirportName;

                if (radioTailNumber.Checked)
                    dayOption.DayWeeklyDetailSortOption = DayWeeklyDetailSortOption.TailNumber;
                else if (radioDepatureDateTime.Checked)
                    dayOption.DayWeeklyDetailSortOption = DayWeeklyDetailSortOption.DepartureDateTime;

                dayOption.AircraftColors = chkAircraftColors.Checked;
                dayOption.ActivityOnly = chkActivityOnly.Checked;
                dayOption.FirstLastLeg = chkFirstLastLeg.Checked;
                dayOption.CrewCalActivity = chkCrewCalActivity.Checked;
                dayOption.ShowTripStatus = chkShowTripStatus.Checked;
                dayOption.BlackWhiteColor = chkBlackWhiteColor.Checked;
                dayOption.SaveColumnWidhts = chkSaveColoumnWidths.Checked;
                dayOption.CummulativeETE = chkCummulativeETE.Checked;
                dayOption.TotalETE = chkTotalETE.Checked;
                dayOption.ETE = chkETE.Checked;

            }

        }

        /// <summary>
        /// Enables the monthly view display options
        /// </summary>
        private void EnableMonthlyViewOption(MonthlyWeeklyOption monthlyOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(monthlyOption))
            {

                EnableBaseOption(monthlyOption);
                // Set Visibility
                chkAircraftColors.Visible = true;
                chkBlackWhiteColor.Visible = true;
                chkShowTrip.Visible = true;
                chkDisplayRedEyeFlights.Visible = true;
                chkShowHomeBase.Visible = false;
                LabelWeeklyFleetCrewDisplayOption.Text = "Month - Display Options";
                chkCrewCalActivity.Visible = true;

                // Set values
                if (monthlyOption != null)
                {
                    chkAircraftColors.Checked = monthlyOption.AircraftColors;
                    chkBlackWhiteColor.Checked = monthlyOption.BlackWhiteColor;
                    chkShowTrip.Checked = monthlyOption.ShowTrip;
                    chkDisplayRedEyeFlights.Checked = monthlyOption.DisplayRedEyeFlightsInContinuation;
                    chkCrewCalActivity.Checked = monthlyOption.CrewCalActivity;
                    hoverOption.Visible = true;
                    toggleHoverOption.Checked = monthlyOption.DisplayDetailsOnHover;
                }

            }

        }
        /// <summary>
        /// Sets the planner view options
        /// </summary>
        /// <param name="plannerOption"></param>
        private void SetPlannerOptions(ref Planner plannerOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(plannerOption))
            {
                var baseOption = new BaseOptions();
                SetBaseOptions(ref baseOption);
                //plannerOption = new Planner(baseOption);               

                if (radioBoth.Checked)
                    plannerOption.PlannerDisplayOption = PlannerDisplayOption.Both;
                else if (radioFleetOnly.Checked)
                    plannerOption.PlannerDisplayOption = PlannerDisplayOption.FleetOnly;
                else if (radioCrewOnly.Checked)
                    plannerOption.PlannerDisplayOption = PlannerDisplayOption.CrewOnly;
                else
                    plannerOption.PlannerDisplayOption = PlannerDisplayOption.None;

                if (radICAODep.Checked)
                    plannerOption.DepartArriveInfo = DepartArriveInfo.ICAO;
                else if (radCityDep.Checked)
                    plannerOption.DepartArriveInfo = DepartArriveInfo.City;
                else
                    plannerOption.DepartArriveInfo = DepartArriveInfo.AirportName;

                if (radAllLegs.Checked)
                    plannerOption.DisplayLegFleet = DisplayLegFleet.All;
                else if (radTripLastLeg.Checked)
                    plannerOption.DisplayLegFleet = DisplayLegFleet.TripLastLeg;
                else if (radDayLastLeg.Checked)
                    plannerOption.DisplayLegFleet = DisplayLegFleet.DayLastLeg;
                else
                    plannerOption.DisplayLegFleet = DisplayLegFleet.None;

                if (radTripLastTail.Checked)
                    plannerOption.DisplayTailCrew = DisplayTailCrew.TripLastTail;
                else if (radDayLastTail.Checked)
                    plannerOption.DisplayTailCrew = DisplayTailCrew.DayLastTail;
                else
                    plannerOption.DisplayTailCrew = DisplayTailCrew.None;


                plannerOption.AircraftColors = chkAircraftColors.Checked;
                plannerOption.NonReversedColors = chkNonReversedColors.Checked;
                plannerOption.ActiveOnly = false;//chkActivityOnly.Checked;
                plannerOption.BlackWhiteColor = chkBlackWhiteColor.Checked;
                plannerOption.DisplayDetailsOnHover = toggleHoverOption.Checked;

                plannerOption.LegPurpose = chkLegPurpose.Checked;
                plannerOption.TripPurpose = chkTripPurpose.Checked;
                plannerOption.Requestor = chkRequestor.Checked;
                plannerOption.SeatsAvailable = chkSeatsAvailable.Checked;
                plannerOption.PaxCount = chkPaxCount.Checked;

                plannerOption.ShowTrip  = chkShowTripNumber.Checked;                                
                plannerOption.ShowTripStatus = chkShowTripStatus.Checked;                                
                plannerOption.CummulativeETE = chkCummulativeETE.Checked;
                plannerOption.TotalETE = chkTotalETE.Checked;

                plannerOption.FlightNumber = chkFlightNumber.Checked;
                plannerOption.Department = chkDepartment.Checked;

                plannerOption.Authorization = chkAuthorization.Checked;
                plannerOption.FlightCategory = chkFlightCategory.Checked;

                plannerOption.ETE = chkETE.Checked;
                plannerOption.ShowTrip = chkShowTripNumber.Checked;
                plannerOption.Crew  = chkCrew.Checked;  

                
            }
        }

        /// <summary>
        /// Sets the view options
        /// </summary>
        /// <param name="viewOption"></param>
        private void SetViewOptions(ref ViewOptions viewOption)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(viewOption))
            {
                viewOption.AircraftColors = chkAircraftColors.Checked;
                viewOption.BlackWhiteColor = chkBlackWhiteColor.Checked;
                viewOption.ShowTrip = chkShowTrip.Checked;
                viewOption.DisplayRedEyeFlightsInContinuation = chkDisplayRedEyeFlights.Checked;
                viewOption.DisplayDetailsOnHover = toggleHoverOption.Checked;
            }
        }

        /// <summary>
        /// Sets the basic options
        /// </summary>
        /// <param name="baseOption"></param>
        private void SetBaseOptions(ref BaseOptions baseOption)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(baseOption))
            {

                if (radICAODep.Checked)
                    baseOption.DepartArriveInfo = DepartArriveInfo.ICAO;
                else if (radCityDep.Checked)
                    baseOption.DepartArriveInfo = DepartArriveInfo.City;
                else
                    baseOption.DepartArriveInfo = DepartArriveInfo.AirportName;

                baseOption.FirstLastLeg = chkFirstLastLeg.Checked;
                baseOption.Crew = chkCrew.Checked;
                baseOption.ShowTripStatus = chkShowTripStatus.Checked;
                baseOption.Requestor = chkRequestor.Checked;
                baseOption.FlightNumber = chkFlightNumber.Checked;
                baseOption.Department = chkDepartment.Checked;
                baseOption.LegPurpose = chkLegPurpose.Checked;
                baseOption.TripPurpose = chkTripPurpose.Checked;
                baseOption.Authorization = chkAuthorization.Checked;
                baseOption.SeatsAvailable = chkSeatsAvailable.Checked;
                baseOption.FlightCategory = chkFlightCategory.Checked;
                baseOption.PaxCount = chkPaxCount.Checked;
                baseOption.CummulativeETE = chkCummulativeETE.Checked;
                baseOption.TotalETE = chkTotalETE.Checked;
                baseOption.ETE = chkETE.Checked;
                baseOption.ShowHomeBase = chkShowHomeBase.Checked;

            }

        }

        /// <summary>
        /// Populates the controls with the value from the deserialized entity.
        /// </summary>
        /// <param name="settings">Advanced filter settings</param>
        public void PopulateAdvancedFilter(CurrentAdvancedFilterTab selectedTab, string xmlSettings, CurrentDisplayOption currentDispOption)
        {
            //Populates Filter criteria


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedTab, xmlSettings, currentDispOption))
            {

                if (Session["SCUserSettings"] == null)
                {
                    var settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(xmlSettings);
                    HideShowDisplayOptionItems(currentDispOption, settings.Display);
                }
                else
                {
                    var settings = (AdvancedFilterSettings)Session["SCUserSettings"];
                    HideShowDisplayOptionItems(currentDispOption, settings.Display);
                }

            }

        }

        public void PopulateScheduleCalenderPreferences(CurrentAdvancedFilterTab selectedTab, string fleetIDs, CurrentDisplayOption currentDispOption)
        {
            //Populates Filter criteria


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedTab, fleetIDs, currentDispOption))
            {

                if (Session["SCUserPreferences"] == null)
                {
                    var settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(fleetIDs);
                    HideShowDisplayOptionItems(currentDispOption, settings.Display);
                }
                else
                {
                    var settings = (AdvancedFilterSettings)Session["SCUserSettings"];
                    HideShowDisplayOptionItems(currentDispOption, settings.Display);
                }

            }

        }

        protected void chkAircraftColors_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (this.chkAircraftColors.Checked)
                        {
                            this.chkBlackWhiteColor.Enabled = false;
                            this.chkBlackWhiteColor.Checked = false;

                        }
                        else
                        {
                            this.chkBlackWhiteColor.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }


        }

        protected void chkBlackWhiteColor_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (this.chkBlackWhiteColor.Checked)
                        {
                            this.chkAircraftColors.Enabled = false;
                            this.chkAircraftColors.Checked = false;
                        }
                        else
                        {
                            this.chkAircraftColors.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }


        }

        protected void chkShowTrip_OnCheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (this.chkShowTrip.Checked)
                        {
                            this.chkCummulativeETE.Enabled = true;
                            this.chkCummulativeETE.Checked = false;
                            this.chkTotalETE.Enabled = true;
                            this.chkTotalETE.Checked = false;
                            this.chkETE.Enabled = true;
                            this.chkETE.Checked = false;
                        }
                        else
                        {
                            this.chkCummulativeETE.Enabled = false;
                            this.chkCummulativeETE.Checked = false;
                            this.chkTotalETE.Enabled = false;
                            this.chkTotalETE.Checked = false;
                            this.chkETE.Enabled = false;
                            this.chkETE.Checked = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }


        }

        protected void radCummulativeETE_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var selectedTab = (CurrentDisplayOption)Session["SCAdvancedTab"];
                        if (CurrentDisplayOption.Day.ToString() == selectedTab.ToString())
                        {
                            if (this.chkCummulativeETE.Checked)
                            {
                                this.radioTailNumber.Enabled = false;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = false;

                            }
                            else
                            {
                                this.radioTailNumber.Enabled = true;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }
        }
        protected void radTotalETE_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var selectedTab = (CurrentDisplayOption)Session["SCAdvancedTab"];
                        if (CurrentDisplayOption.Day.ToString() == selectedTab.ToString())
                        {
                            if (this.chkTotalETE.Checked)
                            {
                                this.radioTailNumber.Enabled = false;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = false;

                            }
                            else
                            {
                                this.radioTailNumber.Enabled = true;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }
        }

        protected void radETE_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var selectedTab = (CurrentDisplayOption)Session["SCAdvancedTab"];
                        if (CurrentDisplayOption.Day.ToString() == selectedTab.ToString())
                        {
                            if (this.chkETE.Checked)
                            {
                                this.radioTailNumber.Enabled = false;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = false;

                            }
                            else
                            {
                                this.radioTailNumber.Enabled = true;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }
        }

        protected void radDepatureDateTime_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var selectedTab = (CurrentDisplayOption)Session["SCAdvancedTab"];
                        if (CurrentDisplayOption.Day.ToString() == selectedTab.ToString())
                        {
                            if (this.radioDepatureDateTime.Checked)
                            {
                                this.chkCummulativeETE.Enabled = false;
                                this.chkCummulativeETE.Checked = false;
                                this.chkTotalETE.Enabled = false;
                                this.chkTotalETE.Checked = false;
                                this.chkETE.Enabled = false;
                                this.chkETE.Checked = false;
                                this.radioTailNumber.Enabled = true;
                                this.radioTailNumber.Checked = false;
                                this.radioDepatureDateTime.Checked = true;
                                this.radioDepatureDateTime.Enabled = true;
                            }
                            else
                            {
                                this.chkCummulativeETE.Enabled = false;
                                this.chkCummulativeETE.Checked = false;
                                this.chkTotalETE.Enabled = false;
                                this.chkTotalETE.Checked = false;
                                this.chkETE.Enabled = false;
                                this.chkETE.Checked = false;
                                this.radioTailNumber.Enabled = true;
                                this.radioTailNumber.Checked = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioDepatureDateTime.Enabled = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }


        }
        protected void radTailNumber_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var selectedTab = (CurrentDisplayOption)Session["SCAdvancedTab"];
                        if (CurrentDisplayOption.Day.ToString() == selectedTab.ToString())
                        {
                            if (this.radioTailNumber.Checked)
                            {
                                this.chkCummulativeETE.Enabled = true;
                                this.chkCummulativeETE.Checked = false;
                                this.chkTotalETE.Enabled = true;
                                this.chkTotalETE.Checked = false;
                                this.chkETE.Enabled = true;
                                this.chkETE.Checked = false;
                                this.radioDepatureDateTime.Enabled = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioTailNumber.Enabled = true;
                                this.radioTailNumber.Checked = true;
                            }
                            else
                            {
                                this.chkCummulativeETE.Enabled = true;
                                this.chkCummulativeETE.Checked = false;
                                this.chkTotalETE.Enabled = true;
                                this.chkTotalETE.Checked = false;
                                this.chkETE.Enabled = true;
                                this.chkETE.Checked = false;
                                this.radioDepatureDateTime.Enabled = true;
                                this.radioDepatureDateTime.Checked = false;
                                this.radioTailNumber.Enabled = false;
                                this.radioTailNumber.Checked = false;
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.AdvancedDisplayOptions);
                }
            }

        }

       
    }

    /// <summary>
    /// Advanced filter settings
    /// </summary>
    [Serializable]
    public class AdvancedFilterSettings
    {
        public FilterCriteria Filters { get; set; }
        public DisplayOptions Display { get; set; }
    }

    /// <summary>
    /// Schedule Calender Preferences
    /// </summary>
    [Serializable]
    public class ScheduleCalenderPreferences
    {
        public ScheduleCalenderCriteria SCCriteria { get; set; }
    }

    /// <summary>
    /// Filter criteria
    /// </summary>
    [Serializable]
    public class FilterCriteria
    {
        public TimeBase TimeBase { get; set; }
        public string Client { get; set; }
        public string ClientID { get; set; }
        public string Requestor { get; set; }
        public string RequestorID { get; set; }
        public string Department { get; set; }
        public string DepartmentID { get; set; }
        public string FlightCategory { get; set; }
        public string FlightCategoryID { get; set; }
        public string CrewDuty { get; set; }
        public string CrewDutyID { get; set; }
        public string HomeBase { get; set; }
        public string HomeBaseID { get; set; }
        public bool Trip { get; set; }
        public bool WorkSheet { get; set; }
        public bool SchedServ { get; set; }
        public bool Canceled { get; set; }
        public bool UnFulfilled { get; set; }
        public bool Hold { get; set; }
        public bool AllCrew { get; set; }
        public bool HomeBaseOnly { get; set; }
        public bool FixedWingCrewOnly { get; set; }
        public bool RotaryWingCrewCrewOnly { get; set; }
        public VendorOption VendorsFilter { get; set; }
        public string FleetIDs { get; set; }
        public string CrewIDs { get; set; }
        
    }

    /// <summary>
    /// Filter criteria
    /// </summary>
    [Serializable]
    public class ScheduleCalenderCriteria
    {
        public string FleetIDs { get; set; }
        public string CrewIDs { get; set; }
    }
    /// <summary>
    /// Display options.
    /// </summary>
    [Serializable]
    public class DisplayOptions
    {
        public Planner Planner { get; set; }
        public WeeklyFleetCrewOption WeeklyFleet { get; set; }
        public WeeklyFleetCrewOption WeeklyCrew { get; set; }
        public MonthlyWeeklyOption Monthly { get; set; }
        public MonthlyWeeklyOption Weekly { get; set; }
        public BusinessWeekOption BusinessWeek { get; set; }
        public DayOption Day { get; set; }
        public CorporateOption Corporate { get; set; }
        public WeeklyDetailOption WeeklyDetail { get; set; }
        public WeeklyDisplayMain WeeklyMain { get; set; }
    }

    /// <summary>
    /// Base class for display options
    /// </summary>
    [Serializable]
    public class BaseOptions
    {
        public DepartArriveInfo DepartArriveInfo { get; set; }
        public bool FirstLastLeg { get; set; }
        public bool Crew { get; set; }
        public bool ShowTripStatus { get; set; }
        public bool FlightNumber { get; set; }
        public bool LegPurpose { get; set; }
        public bool TripPurpose { get; set; }
        public bool SeatsAvailable { get; set; }
        public bool PaxCount { get; set; }
        public bool Requestor { get; set; }
        public bool Department { get; set; }
        public bool Authorization { get; set; }
        public bool FlightCategory { get; set; }
        public bool CummulativeETE { get; set; }
        public bool TotalETE { get; set; }
        public bool ETE { get; set; }
        public bool ShowHomeBase { get; set; }
    }

    /// <summary>
    /// View option
    /// </summary>
    [Serializable]
    public class ViewOptions : BaseOptions
    {
        public ViewOptions() { }
        public ViewOptions(BaseOptions baseOption)
        {
            this.Authorization = baseOption.Authorization;
            this.Crew = baseOption.Crew;
            this.CummulativeETE = baseOption.CummulativeETE;
            this.TotalETE = baseOption.TotalETE;
            this.ETE = baseOption.ETE;
            this.DepartArriveInfo = baseOption.DepartArriveInfo;
            this.Department = baseOption.Department;
            this.FirstLastLeg = baseOption.FirstLastLeg;
            this.FlightCategory = baseOption.FlightCategory;
            this.FlightNumber = baseOption.FlightNumber;
            this.LegPurpose = baseOption.LegPurpose;
            this.TripPurpose = baseOption.TripPurpose;
            this.PaxCount = baseOption.PaxCount;
            this.Requestor = baseOption.Requestor;
            this.SeatsAvailable = baseOption.SeatsAvailable;
            this.ShowTripStatus = baseOption.ShowTripStatus;
            this.ShowHomeBase = baseOption.ShowHomeBase;
        }
        public bool AircraftColors { get; set; }
        public bool BlackWhiteColor { get; set; }
        public bool ShowTrip { get; set; }
        public bool DisplayRedEyeFlightsInContinuation { get; set; }
        public bool DisplayDetailsOnHover { get; set; }
    }

    

    /// <summary>
    /// Business week view display option
    /// </summary>
    [Serializable]
    public class BusinessWeekOption : MonthlyWeeklyOption
    {
        public BusinessWeekOption() { }
        public BusinessWeekOption(MonthlyWeeklyOption monthlyWeekly)
            : base(monthlyWeekly)
        {
            this.CrewCalActivity = monthlyWeekly.CrewCalActivity;
        }
        public bool SaveColumnWidhts { get; set; }
    }

    /// <summary>
    /// Weekly fleet options
    /// </summary>
    [Serializable]
    public class WeeklyFleetCrewOption : ViewOptions
    {
        public WeeklyFleetCrewOption() { }
        public WeeklyFleetCrewOption(ViewOptions viewOption)
            : base(viewOption)
        {
            this.AircraftColors = viewOption.AircraftColors;
            this.BlackWhiteColor = viewOption.BlackWhiteColor;
            this.ShowTrip = viewOption.ShowTrip;
            this.DisplayRedEyeFlightsInContinuation = viewOption.DisplayRedEyeFlightsInContinuation;
            this.ShowHomeBase = viewOption.ShowHomeBase;
        }

        public bool ActivityOnly { get; set; }
        public bool SaveColumnWidths { get; set; }
        public bool NoLineSeparator { get; set; }
    }

    /// <summary>
    /// Weekly detail option
    /// </summary>
    [Serializable]
    public class WeeklyDetailOption : BaseOptions
    {
        public WeeklyDetailOption() { }
        public WeeklyDetailOption(BaseOptions baseOptions)
        {

            this.Authorization = baseOptions.Authorization;
            this.Crew = baseOptions.Crew;
            this.CummulativeETE = baseOptions.CummulativeETE;
            this.TotalETE = baseOptions.TotalETE;
            this.ETE = baseOptions.ETE;
            this.DepartArriveInfo = baseOptions.DepartArriveInfo;
            this.Department = baseOptions.Department;
            this.FirstLastLeg = baseOptions.FirstLastLeg;
            this.FlightCategory = baseOptions.FlightCategory;
            this.FlightNumber = baseOptions.FlightNumber;
            this.LegPurpose = baseOptions.LegPurpose;
            this.TripPurpose = baseOptions.TripPurpose;
            this.PaxCount = baseOptions.PaxCount;
            this.Requestor = baseOptions.Requestor;
            this.SeatsAvailable = baseOptions.SeatsAvailable;
            this.ShowTripStatus = baseOptions.ShowTripStatus;
        }

        public bool AircraftColors { get; set; }
        public bool BlackWhiteColor { get; set; }

        [Serializable]
        public class WeeklyDisplayMain : ViewOptions
        {
            public WeeklyDisplayMain()
            { }
            public WeeklyDisplayMain(ViewOptions viewOptions)
                : base(viewOptions)
            {
                this.Requestor = viewOptions.Requestor;
                this.Department = viewOptions.Department;
                this.Authorization = viewOptions.Authorization;
                this.FlightCategory = viewOptions.FlightNumber;
                this.CummulativeETE = viewOptions.CummulativeETE;
                this.TotalETE = viewOptions.TotalETE;
                this.ETE = viewOptions.ETE;
                this.PaxCount = viewOptions.PaxCount;
                this.SeatsAvailable = viewOptions.SeatsAvailable;
                this.LegPurpose = viewOptions.LegPurpose;
                this.TripPurpose = viewOptions.TripPurpose;
                this.FlightNumber = viewOptions.FlightNumber;
                this.ShowTripStatus = viewOptions.ShowTripStatus;
                this.FirstLastLeg = viewOptions.FirstLastLeg;
                this.Crew = viewOptions.Crew;
                this.DisplayRedEyeFlightsInContinuation = viewOptions.DisplayRedEyeFlightsInContinuation;
                this.AircraftColors = viewOptions.AircraftColors;
                this.FirstLastLeg = viewOptions.FirstLastLeg;
                this.BlackWhiteColor = viewOptions.BlackWhiteColor;

            }
            public bool CrewCalActivity;


        }

        public bool ActivityOnly { get; set; }
        public bool SaveColumnWidhts { get; set; }
        public bool NoLineSeparator { get; set; }
        public DayWeeklyDetailSortOption DayWeeklyDetailSortOption { get; set; }
        public bool PaxFullName { get; set; }
        public bool ShowTripNumber { get; set; }
        public bool CrewFullName { get; set; }
        public bool Pax { get; set; }
    }

    /// <summary>
    /// Planner options
    /// </summary>
    [Serializable]
    public class Planner : ViewOptions
    {
        public Planner() { }
        public Planner(ViewOptions view)
            : base(view)
        {
            this.AircraftColors = view.AircraftColors;
            this.BlackWhiteColor = view.BlackWhiteColor;
            this.DisplayDetailsOnHover = view.DisplayDetailsOnHover;
            this.ShowTrip = view.ShowTrip;
        }
        public bool ActiveOnly { get; set; }
        public bool NonReversedColors { get; set; }
        public bool CrewCalActivity { get; set; }
        public PlannerDisplayOption PlannerDisplayOption { get; set; }
        public DisplayLegFleet DisplayLegFleet { get; set; }
        public DisplayTailCrew DisplayTailCrew { get; set; }

    }

    /// <summary>
    /// Monthly/Weekly view display option
    /// </summary>
    [Serializable]
    public class MonthlyWeeklyOption : ViewOptions
    {
        public MonthlyWeeklyOption() { }
        public MonthlyWeeklyOption(ViewOptions view)
            : base(view)
        {
            this.AircraftColors = view.AircraftColors;
            this.BlackWhiteColor = view.BlackWhiteColor;
            this.ShowTrip = view.ShowTrip;
            this.DisplayRedEyeFlightsInContinuation = view.DisplayRedEyeFlightsInContinuation;
            this.DisplayDetailsOnHover = view.DisplayDetailsOnHover;
        }
        public bool CrewCalActivity { get; set; }
    }
    /// <summary>
    /// Day view display options
    /// </summary>
    [Serializable]
    public class DayOption
    {
        public bool AircraftColors { get; set; }
        public bool ActivityOnly { get; set; }
        public bool BlackWhiteColor { get; set; }
        public DepartArriveInfo DepartArriveInfo { get; set; }
        public bool FirstLastLeg { get; set; }
        public bool ShowTripStatus { get; set; }
        public bool SaveColumnWidhts { get; set; }
        public bool CummulativeETE { get; set; }
        public bool ETE { get; set; }
        public bool TotalETE { get; set; }
        public bool CrewCalActivity { get; set; }
        public DayWeeklyDetailSortOption DayWeeklyDetailSortOption { get; set; }
    }

    /// <summary>
    /// Corporate view display options
    /// </summary>
    [Serializable]
    public class CorporateOption
    {
        public bool AircraftColors { get; set; }
        public bool BlackWhiteColor { get; set; }
        public DepartArriveInfo DepartArriveInfo { get; set; }
        public bool ShowTripStatus { get; set; }
        public bool SaveColumnWidhts { get; set; }
        public bool NoLineSeparator { get; set; }
        public bool DisplayRONPax { get; set; }
    }

    [Serializable]
    public class WeeklyDisplayMain : ViewOptions
    {
        public WeeklyDisplayMain()
        { }
        public WeeklyDisplayMain(ViewOptions viewOptions)
            : base(viewOptions)
        {
            this.Requestor = viewOptions.Requestor;
            this.Department = viewOptions.Department;
            this.Authorization = viewOptions.Authorization;
            this.FlightCategory = viewOptions.FlightNumber;
            this.CummulativeETE = viewOptions.CummulativeETE;
            this.TotalETE = viewOptions.TotalETE;
            this.ETE = viewOptions.ETE;
            this.PaxCount = viewOptions.PaxCount;
            this.SeatsAvailable = viewOptions.SeatsAvailable;
            this.LegPurpose = viewOptions.LegPurpose;
            this.TripPurpose = viewOptions.TripPurpose;
            this.FlightNumber = viewOptions.FlightNumber;
            this.ShowTripStatus = viewOptions.ShowTripStatus;
            this.FirstLastLeg = viewOptions.FirstLastLeg;
            this.Crew = viewOptions.Crew;
            this.DisplayRedEyeFlightsInContinuation = viewOptions.DisplayRedEyeFlightsInContinuation;
            this.AircraftColors = viewOptions.AircraftColors;
            this.FirstLastLeg = viewOptions.FirstLastLeg;
            this.BlackWhiteColor = viewOptions.BlackWhiteColor;

        }
        public bool CrewCalActivity;


    }





    /// <summary>
    /// Time base option
    /// </summary>
    [Serializable]
    public enum TimeBase
    {
        None,
        Local,
        UTC,
        Home
    }

    /// <summary>
    /// Depart/Arrive Information option
    /// </summary>
    [Serializable]
    public enum DepartArriveInfo
    {
        None,
        ICAO,
        City,
        AirportName
    }

    /// <summary>
    /// Vendor selection option
    /// </summary>
    [Serializable]
    public enum VendorOption
    {
        None,
        IncludeActiveVendors,
        OnlyShowActiveVendors,
        ExcludeActiveVendors
    }

    /// <summary>
    /// Planner display option
    /// </summary>
    [Serializable]
    public enum PlannerDisplayOption
    {
        None,
        Both,
        FleetOnly,
        CrewOnly
    }

    /// <summary>
    /// Display Leg for Fleet
    /// </summary>
    [Serializable]
    public enum DisplayLegFleet
    {
        None,
        All,
        TripLastLeg,
        DayLastLeg
    }

    /// <summary>
    /// Display Tail for Crew
    /// </summary>
    [Serializable]
    public enum DisplayTailCrew
    {
        None,
        TripLastTail,
        DayLastTail
    }

    /// <summary>
    /// Day/Weekly detail sort option
    /// </summary>
    [Serializable]
    public enum DayWeeklyDetailSortOption
    {
        None,
        TailNumber,
        DepartureDateTime
    }

    /// <summary>
    /// Current display option selected.
    /// </summary>
    public enum CurrentDisplayOption
    {
        Planner = 0,
        WeeklyFleet = 1,
        WeeklyCrew,
        Monthly,
        Weekly,
        BusinessWeek,
        Day,
        Corporate,
        WeeklyDetail
    }

    /// <summary>
    /// Current tab selected on Advanced filter
    /// </summary>
    public enum CurrentAdvancedFilterTab
    {
        FilterCriteria = 0,
        DisplayOptions = 1
    }

    public enum CurrentScheduleCalenderPreferenceTab
    {
        ScheduleCalenderCriteria = 0,
    }

    [Serializable]
    public class TreeviewParentChildNodePair
    {
        public TreeviewParentChildNodePair()
        {
        }

        public TreeviewParentChildNodePair(string parentNodeId, string childNodeId)
        {
            this.ParentNodeId = parentNodeId;
            this.ChildNodeId = childNodeId;
        }

        public string ParentNodeId { get; set; }
        public string ChildNodeId { get; set; }
    }

}