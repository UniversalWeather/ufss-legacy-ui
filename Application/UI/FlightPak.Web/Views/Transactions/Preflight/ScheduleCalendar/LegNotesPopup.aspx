﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LegNotesPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.LegNotesPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Leg Notes</title>
</head>
<body style="background-color:#fff;">
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            ClientIDMode="AutoID">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="/Scripts/Common.js"></script>
            <script type="text/javascript">
                function ProcessUpdate() {
                    <%--var grid = $find(window['gridId']);
                    var oManager;

                    if (window['windowManagerId'] != null) {
                        oManager = $find(window['windowManagerId']);
                    }

                    if (grid.get_masterTableView().get_selectedItems().length == 0) {
                        if (oManager == null) {
                            radalert('Please select a record from the above table', 330, 100, "Edit", "");
                        }
                        else {
                            oManager.radalert('Please select a record from the above table', 330, 100, "Edit", "");
                        }

                        return false;
                    }--%>
                }
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
            </Windows>
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
            <AlertTemplate>
                <div class="rwDialogPopup radalert">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                        </a>
                    </div>
                </div>
            </AlertTemplate>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <table width="100%" cellpadding="0" cellspacing="0" class="box1">
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgLegSummary" OnNeedDataSource="dgLegSummary_BindData" OnSelectedIndexChanged="dgLegSummary_SelectedIndexChanged"
                            runat="server" AllowSorting="false" AutoGenerateColumns="false" PageSize="5"
                            Height="200px" AllowPaging="true" AllowFilteringByColumn="false" PagerStyle-AlwaysVisible="true" OnItemCommand="dgLegSummary_ItemCommand" OnItemCreated="dgLegSummary_ItemCreated">
                            <MasterTableView DataKeyNames="LegID,LegNUM" AllowFilteringByColumn="false" CommandItemDisplay="Bottom">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="LegNUM" HeaderText="Legs" ShowFilterIcon="false"
                                        HeaderStyle-Width="40px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DepartAir" HeaderText="Dept ICAO" ShowFilterIcon="false"
                                        HeaderStyle-Width="80px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="Arr ICAO" ShowFilterIcon="false"
                                        HeaderStyle-Width="80px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DeparttDate" HeaderText="Depart Date" ShowFilterIcon="false"
                                        HeaderStyle-Width="150px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrival Time" ShowFilterIcon="false"
                                        HeaderStyle-Width="150px">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px; float: left; clear: both;">
                                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClick="lnkInitEdit_Click" ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                    </div>
                                </CommandItemTemplate>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="DivExternalForm1" runat="server" class="ExternalForm">
                            <table id="LegNotes" width="100%">
                                <tr>
                                    <td colspan="4">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="tdLabel50">
                                                    <asp:Label ID="lblLeg" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td class="tdLabel220">
                                                        <div class="mnd_text" style="float: left;">Depart ICAO :&nbsp;</div>   
                                                        <asp:Label ID="lblDepartICAO" runat="server" Visible="true"></asp:Label>
                                                </td>
                                                <td class="tdLabel220"> 
                                                        <div class="mnd_text" style="float: left;">Arrival ICAO :&nbsp;</div>
                                                        <asp:Label ID="lblArrivalICAO" runat="server" Visible="true"></asp:Label>
                                                </td>
                                                <td class="tdLabel100">
                                                </td>
                                                <td align="right">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="tblspace_15"></td>
                                </tr>
                                <tr>
                                    <td class="tdLabel100">Crew Notes
                                    </td>
                                    <td class="tdLabel50">
                                     </td>
                                    <td class="tdLabel100" colspan="2" align="left">PAX Notes
                                    </td>
                                    <td class="tdLabel50">
                                     </td>
                                </tr>
                                <tr>
                                    <td class="tblspace_15"></td>
                                </tr>
                                <tr>
                                    <td class="tdLabel300">
                                        <asp:TextBox ID="tbCrewNotes" runat="server" OnTextChanged="tbCrewNotes_TextChanged" TextMode="MultiLine" CssClass="textarea205x60"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel50" valign="top">
                                        <asp:Button Text="" runat="server" ID="btnCrewNotesAllLegs"  CssClass="copy-icon" OnClick="CopyCrewNotesToAllLegs_click" ToolTip="Copy Crew Notes to all legs"/>
                                    </td>
                                    <td class="tdLabel300">
                                        <asp:TextBox ID="tbPaxNotes" runat="server" OnTextChanged ="tbPaxNotes_TextChanged" TextMode="MultiLine" CssClass="textarea205x60"></asp:TextBox>
                                    </td>
                                     <td class="tdLabel50" valign="top">
                                        <asp:Button runat="server" CssClass="copy-icon" ID="btnPaxNotesAllLegs" OnClick="CopyPaxNotesToAllLegs_click" ToolTip="Copy PAX Notes to all legs"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="3">
                                        <table cellpadding="0" cellspacing="0" style="min-height: 30px;">
                                            <tr>
                                                <td class="tdLabel350" align="left">
                                                    <asp:Button ID="btnSaveTrip" OnClick="btnSavelTrip_click" runat="server" Visible="false" Text="Save Trip" CssClass="button" />
                                                    <asp:Button ID="btnCancelTrip" OnClick="btnCancelTrip_click" runat="server" Visible="false" Text="Cancel Trip" CssClass="button" />
                                                </td>
                                                <td class="tdLabel350" align="right" style="width: 50%; text-align: right!important;">
                                                    <asp:Button ID="btnCancel" OnClick="Cancel_click" runat="server" Text="Cancel" CssClass="button" />
                                                    <asp:Button ID="btnSave" OnClick="Save_click" runat="server" Text="Save" CssClass="button" />
                                                    <asp:HiddenField ID="hdnLeg" runat="server" />
                                                    <asp:HiddenField ID="hdnLegID" runat="server" />
                                                    <asp:HiddenField ID="hdnLegNUM" runat="server" />
                                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tblspace_15"></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
