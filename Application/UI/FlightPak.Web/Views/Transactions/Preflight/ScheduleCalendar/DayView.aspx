﻿<%@ Page Language="C#" ClientIDMode="AutoID" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Site.master"
    CodeBehind="DayView.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.DayView" %>

<%--<%@ Register Src="~/UserControls/UCOutBoundInstructions.ascx" TagName="OutBoundInstructionsPopup"
    TagPrefix="UCPreflight" %>--%>
<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../../../../Scripts/common-calendar.js"></script>
</asp:Content>
<asp:Content ID="SiteBodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <telerik:RadCodeBlock runat="server" ID="RadCodeBlock1">
        <script type="text/javascript">
            function pageLoad() {
                if ($telerik.isMobileSafari) {
                    var scrollAreaMon = $get('<%= dgDay.ClientID%>');
                    scrollAreaMon.addEventListener('touchstart', handleTouchStart, false);
                    
                    $(".rgDataDiv").bind('scroll', function(e) {
                        $(".RadMenu_Context").css("display", "none");
                        $(".RadMenu_Context").css("opacity", "0"); // Opacity improves the user exp. Hide the context menu flickering
                    });
                }
            }
        </script>
        <table>
            <tr>
                <script type="text/javascript">

                    function showPleaseWait() {

                        document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                    }

                    function openWinLegend() {
                        var oWnd = radopen("Legend.aspx", "RadWindow1");
                    }

                    function openWinDayViewFullView() {

                        window.open("DayViewFullView.aspx", "Default", " maximize=true, fullscreen=yes, channelmode=yes, toolbar=no, menubar=no, directories=no, resizable=yes, status=no, scrollbars=auto, close=yes");
//                        params = 'width=' + screen.width;
//                        params += ', height=' + screen.height;
//                        params += ', top=0, left=0'
//                        params += ', fullscreen=yes';                        

//                        newwin = window.open("DayViewFullView.aspx?width="+screen.width+"&height="+screen.height, 'Default', params);
//                        if (window.focus) { newwin.focus() }
//                        return false;
                    } 

                    function openWinAdv() {
                        var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                    }
                    function openWinFleetCalendarEntries() {
                        var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                    }

                    function openWinCrewCalendarEntries() {
                        var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                    }
                    function openWinOutBoundInstructions() {
                        var oWnd = radopen("../PreflightOutboundInstruction.aspx", "RadWindow5");
                    }
                    function openWinLegNotes() {
                        var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                    }
                    function GetDimensions(sender, args) {
                        var bounds = sender.getWindowBounds();
                        return;
                    }
                    function GetRadWindow() {
                        var oWindow = null;
                        if (window.radWindow) oWindow = window.radWindow;
                        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                        return oWindow;
                    }


                    function OnClientClose(sender, args) {
                        showPleaseWait();
                        __doPostBack("hdnPostBack", "");
                    }

                    function Refresh(sender, args) {
                        showPleaseWait();
                        __doPostBack("hdnPostBack", "");
                    }
                       
                </script>
                <script type="text/javascript">
                    //Rad Date Picker
                    var currentTextBox = null;
                    var currentDatePicker = null;

                    //This method is called to handle the onclick and onfocus client side events for the texbox
                    function showPopup(sender, e) {
                        //this is a reference to the texbox which raised the event
                        //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                        currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                        //this gets a reference to the datepicker, which will be shown, to facilitate
                        //the selection of a date
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                        //this variable is used to store a reference to the date picker, which is currently
                        //active
                        currentDatePicker = datePicker;

                        //this method first parses the date, that the user entered or selected, and then
                        //sets it as a selected date to the picker
                        datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                        //the code lines below show the calendar, which is used to select a date. The showPopup
                        //function takes three arguments - the x and y coordinates where to show the calendar, as
                        //well as its height, derived from the offsetHeight property of the textbox
                        var position = datePicker.getElementPosition(currentTextBox);
                        datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                    }

                    //this handler is used to set the text of the TextBox to the value of selected from the popup
                    function dateSelected(sender, args) {
                        if (currentTextBox != null) {
                            //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                            //value of the picker
                            //currentTextBox.value = args.get_newValue();

                            if (currentTextBox.value != args.get_newValue()) {
                                showPleaseWait();
                                currentTextBox.value = args.get_newValue();
                                __doPostBack(currentTextBox.id, "");

                            }
                            else
                            { return false; }
                        }
                    }

                    //this function is used to parse the date entered or selected by the user
                    function parseDate(sender, e) {
                        if (currentDatePicker != null) {
                            var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                            var dateInput = currentDatePicker.get_dateInput();
                            var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                            sender.value = formattedDate;
                        }
                    }
                    // this function is used to validate the date in tbDate textbox...
                    function ontbDateKeyPress(sender, e) {
                        var code = (e.keyCode ? e.keyCode : e.which);
                        if (code == 13) { //Enter keycode
                            showPleaseWait(); // to show progress bar
                        }

                        // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                        var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                        var dateSeparator = null;
                        var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                        var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                        var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                        if (dotSeparator != -1) { dateSeparator = '.'; }
                        else if (slashSeparator != -1) { dateSeparator = '/'; }
                        else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                        else {
                            alert("Invalid Date");
                        }
                        // allow dot, slash and hypen characters... if any other character, throw alert
                        return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                    }
                    //To Hide Calender on tab out
                    function tbDate_OnKeyDown(sender, event) {
                        if (event.keyCode == 9) {
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                            datePicker.hidePopup();
                            return true;
                        }
                    }
                </script>
                <!-- Start: Tooltip integration  -->
                <script type="text/javascript">
                    function RadDayGridContextMenu_OnClientItemClicking(sender, eventArgs) {
                        showPleaseWait();
                    }
                    function IpaddgDayView_RowContextMenu(sender, eventArgs) {
                        if ($telerik.isMobileSafari) {
                            dgDayView_RowContextMenu(sender, eventArgs);
                        }
                    }
                    function dgDayView_RowContextMenu(sender, eventArgs) {
                        var hdnLegNum = $get('<%=radSelectedLegNum.ClientID %>');
                        hdnLegNum.value = eventArgs._dataKeyValues.LegNum;
                        var menu = $find("<%=RadDayGridContextMenu.ClientID %>");
                        menu._findItemByText("Preflight").set_enabled(true);
                        menu._findItemByText("Crew").set_enabled(true);
                        menu._findItemByText("PAX").set_enabled(true);
                        menu._findItemByText("Logistics").set_enabled(true);

                        var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                        if (menuItemfleetadd != null) {
                            menuItemfleetadd.set_text("Fleet Calendar Entry");
                        }
                        var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                        if (menuItemcrewadd != null) {
                            menuItemcrewadd.set_text("Crew Calendar Entry");
                        }

                        if (eventArgs == null)
                        // When clicked from no record display
                        {


                            //alert(menu.findItemByText("Postflight Flight Logs").toString());
                            menu.findItemByText("Preflight").show();
                            menu.findItemByText("Add New Trip").show();
                            var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                            menuItemfleet.show();
                            menuItemfleet.set_text("Add Fleet Calendar Entry");

                            var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                            menuItemcrew.show();
                            menuItemcrew.set_text("Add Crew Calendar Entry");
                            menu.findItemByText("Postflight Flight Logs").hide();
                            menu.findItemByValue("Separator").hide();
                            menu.findItemByText("Flight Log Legs").hide();
                            menu.findItemByText("Crew").hide();
                            menu.findItemByText("PAX").hide();
                            menu.findItemByText("Outbound Instruction").hide();
                            menu.findItemByText("Logistics").hide();
                            menu.findItemByText("Leg Notes").hide();
                            menu.findItemByText("UWA Services").set_enabled(false);
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(event);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            return;
                        }



                        var evt = eventArgs.get_domEvent();

                        if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                            return;
                        }

                        // To show / hide Postflightlog menu items based on trip logged / entry...
                        if (eventArgs._dataKeyValues.RecordType == 'T') {

                            //if trip is logged, show postflight log menu items
                            if (eventArgs._dataKeyValues.IsLog == "True") {
                                var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                postFlightLogMenuItem.set_enabled(true);
                                postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + eventArgs._dataKeyValues.TripId);
                                var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                menu._findItemByText("Flight Log Legs").set_enabled(true);
                                flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + eventArgs._dataKeyValues.TripId + "&legNum=" + eventArgs._dataKeyValues.LegNum);

                            } else {  // else, hide
                                menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                menu.findItemByText("Flight Log Legs").set_enabled(false);

                            }

                            menu.findItemByText("Postflight Flight Logs").show();
                            menu.findItemByValue("Separator").show();
                            menu.findItemByText("Flight Log Legs").show();
                            menu.findItemByText("Crew").show();
                            menu.findItemByText("PAX").show();
                            menu.findItemByText("Outbound Instruction").show();
                            menu.findItemByText("Logistics").show();
                            menu.findItemByText("Leg Notes").show();

                            if (eventArgs._dataKeyValues.ShowPrivateTripMenu == "False") {
                                if (eventArgs._dataKeyValues.IsPrivateTrip == "True") { // if user has restricted access to Private trip

                                    menu._findItemByText("Preflight").set_enabled(false);
                                    menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu._findItemByText("Flight Log Legs").set_enabled(false);
                                    menu._findItemByText("Crew").set_enabled(false);
                                    menu._findItemByText("PAX").set_enabled(false);
                                    menu._findItemByText("Logistics").set_enabled(false);
                                }
                            }
                        }
                        // ends here...
                        // To show / hide Postflightlog menu items based on trip logged / entry...
                        else if (eventArgs._dataKeyValues.RecordType == 'M' || eventArgs._dataKeyValues.RecordType == 'C') {

                            //if trip is logged, show postflight log menu items
                            menu.findItemByText("Postflight Flight Logs").hide();
                            menu.findItemByValue("Separator").hide();
                            menu.findItemByText("Flight Log Legs").hide();
                            menu.findItemByText("Crew").hide();
                            menu.findItemByText("PAX").hide();
                            menu.findItemByText("Outbound Instruction").hide();
                            menu.findItemByText("Logistics").hide();
                            menu.findItemByText("Leg Notes").hide();
                            menu.findItemByText("UWA Services").show();
                            menu.findItemByText("Preflight").show();
                            menu.findItemByText("Add New Trip").show();
                            var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                            menuItemfleet.show();
                            menuItemfleet.set_text("Add Fleet Calendar Entry");

                            var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                            menuItemcrew.show();
                            menuItemcrew.set_text("Add Crew Calendar Entry");
                        }

                        // ends here...

                        var index = eventArgs.get_itemIndexHierarchical();

                        document.getElementById("radGridClickedRowIndex").value = index;



                        if ($telerik.isMobileSafari) {
                            $telerik.cancelRawEvent(evt);
                            $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                            menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                        }
                        else {
                            sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);
                            menu.show(evt);

                            evt.cancelBubble = true;
                            evt.returnValue = false;

                            if (evt.stopPropagation) {
                                evt.stopPropagation();
                                evt.preventDefault();
                            }
                        }
                    }

                    //Fleet tree view...

                    function FleetTreeView_NodeCheck(sender, args) {
                        var treeView = $find("<%= FleetTreeView.ClientID %>");
                        var isChecked = args.get_node().get_checked();
                        var valueChecked = args.get_node().get_text();
                        var crewTree = $find("<%= CrewTreeView.ClientID %>");
                        CalendarFleetCrewTreeViewCheck(isChecked, treeView, crewTree);
                        CheckDuplicates(isChecked, valueChecked, treeView);
                    }

                    // Crew tree view...
                    function CrewTreeView_NodeCheck(sender, args) {
                        var treeView = $find("<%= CrewTreeView.ClientID %>");
                        var isChecked = args.get_node().get_checked();
                        var valueChecked = args.get_node().get_text();
                        var fleetTree = $find("<%= FleetTreeView.ClientID %>");
                        CalendarFleetCrewTreeViewCheck(isChecked, treeView, fleetTree);
                        CheckDuplicates(isChecked, valueChecked, treeView);
                    }
             
                </script>
            </tr>
        </table>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager2" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/FleetCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/LegNotesPopup.aspx?IsCalender=1">
            </telerik:RadWindow>
            <%--<telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/UserControls/UCOutBoundInstructions.ascx">
                <ContentTemplate>
                    <UCPreflight:OutBoundInstructionsPopup ID="OutboundInstructions" runat="server" />
                </ContentTemplate>
            </telerik:RadWindow>--%>
        </Windows>
    </telerik:RadWindowManager>
    <%--<div class="globalMenu_mapping">
        <a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
        <a href="#" class="globalMenu_mapping">Preflight</a> <span class="breadcrumbCaret">
        </span><a href="#" class="globalMenu_mapping">Scheduling Calendar</a>
    </div>--%>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="radSelectedLegNum" name="radSelectedLegNum" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="556px" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel140"  style='width:160px'; align = "left">
                    <div class="sc_date_display">
                        <span>
                            <asp:Label ID="lbDay" runat="server" CssClass="date"></asp:Label></span>
                    </div>
                </td>
                <td class="defaultview" align ="left">
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        OnCheckedChanged="ChkDefaultView_OnCheckedChanged" onclick="javascript:showPleaseWait();" />                         
                </td>
                <td class="tdLabel70" align ="left">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>                 
                <td class="sc_nav_topicon" align="left">
                    <span class="tab-nav-icons"><a class="refresh-icon" onclick="Refresh();" href="#"
                        title="Refresh"></a><a class="help-icon" href="/Views/Help/ViewHelp.aspx?Screen=SchedulingCalendar" title="Help"></a></span> 
                 </td>    
                    <td class="tdLabel70" align="right" style='width:20px;'>
                   <asp:Button ID="btnFullView" runat="server" CssClass="ui_nav" ToolTip="Full View" Text="FV" OnClientClick="javascript:openWinDayViewFullView();return false;" />                       
                </td>                           
            </tr>
            <tr>
                <td colspan="6">
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0" Width="980px"
                        Skin="Windows7" Height="500px">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="None" Width="100%">
                            <div class="sc_navigation">
                                <div class="sc_navigation_right">
                                    <span class="act-btn"><span></span><span>
                                        <asp:Button ID="prevButton" runat="server" OnClick="prevButton_Click" CssClass="icon-prev_sc"
                                            ToolTip="Go Back 1 Day" OnClientClick="showPleaseWait();" /></span><span>
                                                <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                    AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" OnClientClick="showPleaseWait();"
                                                    onfocus="showPopup(this, event);" onKeyPress="return ontbDateKeyPress(this, event);"
                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onBlur="parseDate(this, event);"
                                                    MaxLength="10"></asp:TextBox></span> <span>
                                                        <asp:Button ID="nextButton" runat="server" OnClick="nextButton_Click" CssClass="icon-next_sc"
                                                            ToolTip="Go Forward 1 Day" OnClientClick="showPleaseWait();" /></span>
                                        <span></span></span><span>
                                            <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                                OnClientClick="showPleaseWait();" />
                                        </span>
                                </div>
                            </div>
                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="None" runat="server">
                                <Items>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div>
                                                <telerik:RadGrid ID="dgDay" OnItemDataBound="dgDay_Databound" runat="server" Skin="Office2010Silver"
                                                    CssClass="sc_dayview_grid" AutoGenerateColumns="true" AllowPaging="false" PagerStyle-AlwaysVisible="false"
                                                    AllowSorting="true" ShowFooter="false" AllowFilteringByColumn="false" BackColor="#dfeaf7" ClientSettings-Resizing-AllowColumnResize="true"
                                                    Width="100%" Height="496px" ClientSettings-Scrolling-UseStaticHeaders="true" OnSortCommand="dgDay_OnSortCommand">
                                                    <PagerStyle Visible="false" />
                                                    <MasterTableView PagerStyle-AlwaysVisible="false" ClientDataKeyNames="TripId,RecordType,IsLog,LastUpdatedTime,StartDate,TripNum,EndDate,PassengerCount,ReservationAvailable,ElapseTM,CrewDutyType,FlightCategoryCode,PassengerRequestorCD,AuthorizationCD,FuelLoad,OutboundInstruction,FlightNum,DepartureICAOID,ArrivalICAOID,LegNum,IsPrivateTrip,ShowPrivateTripMenu"
                                                        DataKeyNames="TripId,TailNum,LastUpdatedTime,StartDate,TripNum,EndDate,PassengerCount,ReservationAvailable,ElapseTM,CrewDutyType,FlightCategoryCode,PassengerRequestorCD,AuthorizationCD,FuelLoad,OutboundInstruction,FlightNum,DepartureICAOID,ArrivalICAOID,LegNum"
                                                        CommandItemDisplay="Bottom" ShowHeader="true" AllowFilteringByColumn="false"
                                                        PageSize="500" ShowFooter="false" Width="100%">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="TailNum" UniqueName="TailNum" HeaderText="Tail No."
                                                                HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" AllowFiltering="false"
                                                                ShowFilterIcon="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="StartDateTime" UniqueName="StartDateTime" HeaderText="Dep Date/Time"
                                                                HeaderStyle-Width="115px" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" AllowFiltering="false"
                                                                ShowFilterIcon="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="DepartureICAOID" UniqueName="Depart" HeaderText="Dep ICAO"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top" AllowFiltering="false"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" ShowFilterIcon="false"  HeaderStyle-Width="50px" >
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ArrivalICAOID" HeaderText="Arr ICAO" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top"  HeaderStyle-Width="50px"
                                                                UniqueName="Arrive" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="EndDateTime" HeaderText="Arr Date/Time" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="115px"
                                                                HeaderStyle-VerticalAlign="Top" UniqueName="EndDateTime" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ElapseTM" HeaderText="ETE" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="50px"
                                                                HeaderStyle-VerticalAlign="Top" UniqueName="ETE" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CumulativeETE" HeaderText="Cum ETE" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="50px"
                                                                HeaderStyle-VerticalAlign="Top" UniqueName="CummETE" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TotalETE" HeaderText="Total Trip ETE" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="50px"
                                                                HeaderStyle-VerticalAlign="Top" UniqueName="TotalETE" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            
                                                            <telerik:GridBoundColumn DataField="PassengerCount" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="30px"
                                                                HeaderStyle-VerticalAlign="Top" HeaderText="PAX" UniqueName="Pax" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AircraftDutyCD" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="40px"
                                                                HeaderStyle-VerticalAlign="Top" HeaderText="Duty" UniqueName="Duty" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CrewCodes" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="130px"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top"
                                                                HeaderText="Crew" UniqueName="Crew" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FlightCategoryCode" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="40px"
                                                                HeaderStyle-VerticalAlign="Top" UniqueName="FltCat" HeaderText="Flt Cat" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="70px"
                                                                HeaderStyle-VerticalAlign="Top" UniqueName="Reqstor" HeaderText="Requestor" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AuthorizationCD" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="85px"
                                                                HeaderStyle-VerticalAlign="Top" HeaderText="Authorization" UniqueName="Authorization"
                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TripNUM" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="50px" HeaderStyle-VerticalAlign="Top"
                                                                HeaderText="Trip No." UniqueName="TripNo" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Status" AllowFiltering="false"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="50px"
                                                                ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top"
                                                                UniqueName="TripStatus">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FuelLoad" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="40px"
                                                                HeaderStyle-VerticalAlign="Top" HeaderText="Fuel Load" UniqueName="FuelLoad"
                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="OutboundInstruction" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top"
                                                                UniqueName="OutboundInstruction" HeaderText="Outbound Instruction" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FlightNum" HeaderText="Flight No." UniqueName="FlightNo"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="50px"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top" ShowFilterIcon="false"
                                                                AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                        <NoRecordsTemplate>
                                                            <div oncontextmenu="dgDayView_RowContextMenu(event, null);" onclick="dgDayView_RowContextMenu(event, null);"
                                                                style="height: 1200px; width: 1200px">
                                                                No records to Display.
                                                            </div>
                                                        </NoRecordsTemplate>
                                                        <CommandItemTemplate>
                                                        </CommandItemTemplate>
                                                        <EditItemTemplate>
                                                        </EditItemTemplate>
                                                        <GroupFooterTemplate>
                                                        </GroupFooterTemplate>
                                                        <PagerTemplate>
                                                        </PagerTemplate>
                                                        <FilterItemStyle Height="0" />
                                                    </MasterTableView>
                                                    <HeaderStyle Width="100px" />
                                                    <ClientSettings>
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="false">
                                                        </Scrolling>
                                                        <%-- <Resizing AllowColumnResize="True" AllowRowResize="false" ResizeGridOnColumnResize="false"
                                         ClipCellContentOnResize="true" EnableRealTimeResize="false" AllowResizeToFit="false"  />    --%>
                                                        <ClientEvents OnRowContextMenu="dgDayView_RowContextMenu" OnRowHidden="dgDayView_RowContextMenu"
                                                            OnRowClick="dgDayView_RowContextMenu"></ClientEvents>
                                                    </ClientSettings>
                                                    <GroupingSettings CaseSensitive="false" />
                                                </telerik:RadGrid>
                                                <telerik:RadContextMenu ID="RadDayGridContextMenu" runat="server" EnableRoundedCorners="true"
                                                    OnItemClick="RadDayGridContextMenu_ItemClick" OnClientItemClicking="RadDayGridContextMenu_OnClientItemClicking"
                                                    EnableShadows="true">
                                                    <Targets>
                                                        <telerik:ContextMenuControlTarget ControlID="dgDay" />
                                                        <%--<telerik:ContextMenuTagNameTarget TagName="NoRecordsTemplate" />--%>
                                                    </Targets>
                                                    <Items>
                                                        <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                        <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                        <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                        <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                        <telerik:RadMenuItem Text="Logistics">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                            </Items>
                                                        </telerik:RadMenuItem>
                                                        <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                        <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                        <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                        <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                        <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                    </Items>
                                                </telerik:RadContextMenu>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward" />
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="None" Width="250px" Collapsed="true">
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="500px" ExpandMode="SingleExpandedItem" AllowCollapseAllItems="true">
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="FleetTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="FleetTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="CrewTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Expanded="True" Text="Filter Criteria" runat="server">
                                        <ContentTemplate>
                                       <div class="sc_filter_criteria">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </div> 
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                OnClientClick="javascript:openWinAdv();return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
