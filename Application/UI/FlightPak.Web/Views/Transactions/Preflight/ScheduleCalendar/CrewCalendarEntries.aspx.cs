﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using System.Collections.ObjectModel;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class CrewCalendarEntries : ScheduleCalendarBase
    {
        public PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
        private bool IsEmptyCheck = true;
        private bool isfill = false;
        DateTime SDate;
        DateTime EDate;
        string ViewName;
        bool selectFirstItem = false; 
        private bool CrewPageNavigated = false;
        bool isInsert = false;
        bool isEdit = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewCalendarEntries, dgCrewCalendarEntries, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewCalendarEntries.ClientID));
                        AdvancedFilterSettings settings = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.AdvancedFilterSettings)Session["SCUserSettings"];
                        FilterOptions = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria)settings.Filters;
                        rdpostflight.VisibleOnPageLoad = false;

                        pnlExternalForm.Visible = true;
                        pnlExternalForm1.Visible = false;
                        CheckAutorization(Permission.Preflight.ViewCrewCalendarEntry);                        
                        dgCrewCalendarEntries.DataBound += new EventHandler(dgCrewCalendarEntries_DataBound); 

                        if (!IsPostBack)
                        {
                            Session["chkCrewConflicts"] = "false";
                            SDate = Convert.ToDateTime(Session["SCSelectedDay"]);
                            ViewName = Convert.ToString(Session["SCAdvancedTab"]);
                            EDate = GetEndDate(SDate, ViewName);



                            if ((!string.IsNullOrEmpty(Request["crewCode"])) && (Convert.ToString(Request["crewCode"]) != "null"))
                                hdndefaultCrew.Value = Request["crewCode"].ToString();

                            if (!string.IsNullOrEmpty(Request["startDate"]))
                            {
                                DateTime DefaultDate;
                                try
                                {
                                    if ((ViewName == "Weekly") || (ViewName == "Planner") || (ViewName == "WeeklyDetail") || (ViewName == "Monthly") || (ViewName == "Day") || (ViewName == "BusinessWeek") || (ViewName == "Corporate"))
                                        DefaultDate = Convert.ToDateTime(Request["startDate"].ToString());
                                    else
                                        DefaultDate = Convert.ToDateTime(Request["startDate"].ToString(DTF), DTF);
                                }
                                catch
                                {
                                    DefaultDate = Convert.ToDateTime(Request["startDate"].ToString());
                                }
                                hdndefaultSDate.Value = Convert.ToString(DefaultDate.ToShortDateString(), DTF);
                                //tdGoto.Text = Convert.ToString(DefaultDate.ToShortDateString(), DTF);                               
                                tdGoto.Text = DefaultDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(Convert.ToString(ClientCode)))
                                {
                                    tbClientCD.Text = ClientCode;
                                    tbClientCD.Enabled = false;
                                    btnClientCD.Visible = false;
                                }
                                DefaultSelection();
                            }
                            //GridBoundColumn SDates = dgCrewCalendarEntries.Columns.FindByUniqueName("SDate") as GridBoundColumn;
                            //DTF.ShortTimePattern = "H:mm";
                            //SDates.DataFormatString = "{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}";
                            //GridBoundColumn EDates = dgCrewCalendarEntries.Columns.FindByUniqueName("EDate") as GridBoundColumn;
                            //EDates.DataFormatString = "{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}";

                        }
                        if (isfill == true)
                            DefaultSelection();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }


        void dgCrewCalendarEntries_DataBound(object sender, EventArgs e)
        {
            if (selectFirstItem)
            {
                dgCrewCalendarEntries.MasterTableView.Items[0].Selected = true;

                GridDataItem item = (GridDataItem)dgCrewCalendarEntries.SelectedItems[0];
                Session["SelectedCrewItem"] = Convert.ToString(item.GetDataKeyValue("TripID"));
                Session["chkCrewConflicts"] = "false";
                chkmultiplecrew(Convert.ToInt64(item.GetDataKeyValue("PreviousNUM")));                
                GridEnable(true, true, true);
            }
        } 

        protected void DefaultSelection()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                dgCrewCalendarEntries.Rebind();
                int index = 0;
                string crewCode = string.Empty;
                if (dgCrewCalendarEntries.MasterTableView.Items.Count > 0)
                {
                    if(!string.IsNullOrEmpty(Request["crewCode"]))
                    {
                        crewCode = Request["crewCode"];
                    }
                    DateTime sdate = Convert.ToDateTime(Request.QueryString["StartDate"]);
                    DateTime edate = Convert.ToDateTime(Request.QueryString["EndDate"]);
                    foreach (GridDataItem Item in dgCrewCalendarEntries.Items)
                    {
                        if (Convert.ToString(Item.GetDataKeyValue("CrewCD")).Trim() == crewCode.Trim() && (sdate <= Convert.ToDateTime(Item.GetDataKeyValue("StartDate")) && edate <= Convert.ToDateTime(Item.GetDataKeyValue("EndDate"))))
                        {
                            index = Item.ItemIndex;
                        }
                    }
                   
                    Session["SelectedCrewItem"] = Convert.ToString(dgCrewCalendarEntries.Items[index].GetDataKeyValue("TripID")).Trim();
                    hdnTripIds.Value = Convert.ToString(dgCrewCalendarEntries.Items[index].GetDataKeyValue("TripID")).Trim();
                    ReadOnlyForm();
                    if (Trip != null)
                    {
                        if (Trip.State != TripEntityState.Added && Trip.State != TripEntityState.Modified)
                        {
                            btnCopyCCR.Enabled = true;
                            btnCopyCCR.CssClass = "button";
                        }
                        else
                        {
                            btnCopyCCR.Enabled = false;
                            btnCopyCCR.CssClass = "button-disable";
                        }
                    }
                    else
                    {
                        btnCopyCCR.Enabled = false;
                        btnCopyCCR.CssClass = "button-disable";
                    }
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                    hdnTripIds.Value = "0";
                    btnCopyCCR.Enabled = false;
                    btnCopyCCR.CssClass = "button-disable";
                }
                dgCrewCalendarEntries.SelectedIndexes.Add(index);
                GridEnable(true, true, true);
                DisplayDeleteForm(false);
            }
        }

        protected DateTime GetEndDate(DateTime startdate, string Viewname)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startdate, Viewname))
            {
                //if (ViewName == "Planner")
                //    return startdate.AddDays(30).AddSeconds(-1);
                //else if (ViewName == "Day")
                //    return startdate.AddHours(24).AddMilliseconds(-1);
                //else if ((ViewName == "Weekly") || (ViewName == "WeeklyDetail") || (ViewName == "WeeklyCrew") || (ViewName == "WeeklyFleet") || (ViewName == "Corporate"))
                //    return startdate.AddDays(7).AddSeconds(-1);
                //else if (ViewName == "BusinessWeek")
                //    return startdate.AddDays(15).AddSeconds(-1);
                //else
                //    return startdate.AddDays(42).AddSeconds(-1);
                return startdate = startdate.AddYears(7).AddMonths(2);
            }
        }

        protected void dgCrewCalendarEntries_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            
                            LinkButton deleteButton = ((e.Item as GridCommandItem).FindControl("lnkInitDelete") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton deleteGroupButton = ((e.Item as GridCommandItem).FindControl("lnkMultiDelete") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteGroupButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton editGroupButton = ((e.Item as GridCommandItem).FindControl("lnkMultiEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editGroupButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void dgCrewCalendarEntries_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                        GetDatatoFillGrid();
                        if (selectFirstItem)
                        {
                            dgCrewCalendarEntries.MasterTableView.Items[0].Selected = true;                                                      
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        private void GetDatatoFillGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient objservice = new PreflightServiceClient())
                {
                    SDate = Convert.ToDateTime(Session["SCSelectedDay"]);
                    ViewName = Convert.ToString(Session["SCAdvancedTab"]);
                    EDate = GetEndDate(SDate, ViewName);
                    if (!String.IsNullOrEmpty(tdGoto.Text))
                        SDate = Convert.ToDateTime(tdGoto.Text.ToString(), DTF);
                    if (SDate > EDate)
                        EDate = SDate.AddDays(42);

                    Session["CrewStartDate"] = SDate;
                    Session["CrewEndDate"] = EDate;

                    var objvalue = objservice.GetCrewCalendarEntries(FilterOptions.TimeBase.ToString(), SDate, EDate);
                    if (objvalue.ReturnFlag == true)
                    {
                        if (Convert.ToString(Session["chkCrewConflicts"]) == "true")
                            showconflict(SDate, EDate);

                        if ((String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//No Filter
                            dgCrewCalendarEntries.DataSource = objvalue.EntityList;
                        else
                        {
                            string[] homeBaseIds = null;

                            if (!string.IsNullOrEmpty(FilterOptions.HomeBaseID))
                                homeBaseIds = FilterOptions.HomeBaseID.Split(',');

                            if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//only clientcode                       
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text));
                            else if ((String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//only EndDate
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF));
                            else if ((String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//only Homebase
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => homeBaseIds.Contains(x.HomebaseID.Value.ToString()));
                            else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//clientcode & homebase
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text) && (homeBaseIds.Contains(x.HomebaseID.Value.ToString())));
                            else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//ClientCode & Enddate
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text) && x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF));
                            else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//ClientCode & Enddate & Homebase
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text) && (homeBaseIds.Contains(x.HomebaseID.Value.ToString())) && x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF));
                            else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//Enddate & Homebase                       
                                dgCrewCalendarEntries.DataSource = objvalue.EntityList.Where(x => homeBaseIds.Contains(x.HomebaseID.Value.ToString()) && x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF));
                        }
                    }                   
                }
            }
        }

        public void showconflict(DateTime startdate, DateTime enddate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startdate, enddate))
            {


                //PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(FilterOptions);
                PreflightService.FilterCriteria serviceFilterCriteria = new PreflightService.FilterCriteria();
                serviceFilterCriteria.TimeBase = (FlightPak.Web.PreflightService.TimeBase)FilterOptions.TimeBase;
                Collection<string> selectedNodes = new Collection<string>();
                string[] chkcrewConflicts;
                string[] chktripids;
                //if (Convert.ToString(hdnOptionGroup.Value) == "1")
                if (Convert.ToString(hdnMultiCrew.Value).Split(',').Length > 1)
                {
                    chkcrewConflicts = Convert.ToString(hdnMultiCrew.Value).Split(',');
                    chktripids = Convert.ToString(hdnMultiTrip.Value).TrimStart(',').TrimEnd(',').Split(',');
                }
                else
                {
                    chkcrewConflicts = Convert.ToString(hdnCrew.Value).Split(',');
                    chktripids = Convert.ToString(hdnTripID.Value).TrimStart(',').TrimEnd(',').Split(',');
                }
                for (int j = 0; j <= chkcrewConflicts.Length - 1; j++)
                {
                    selectedNodes.Add(Convert.ToString(chkcrewConflicts[j]));
                }
                var inputFromCrewTree = selectedNodes;

                using (PreflightServiceClient objservice = new PreflightServiceClient())
                {
                    var objvalue = objservice.GetCrewCalendarData(Convert.ToDateTime(Session["chkCrewsdate"]), Convert.ToDateTime(Session["chkCrewedate"]), serviceFilterCriteria, inputFromCrewTree.ToList(), false);
                    var tripdata = objvalue.EntityList.Where(x => x.RecordType == "T").ToList();
                    List<CrewCalendarDataResult> calendarResult = new List<CrewCalendarDataResult>();
                    calendarResult = objvalue.EntityList.Where(x => x.RecordType == "C" && chkcrewConflicts.Contains(x.CrewID.Value.ToString()) && !chktripids.Contains(x.TripID.ToString())).ToList();
                    calendarResult.AddRange(tripdata);

                    var conflictitems = calendarResult.ToList();
                    string strWarning = "Warning - Crew Conflict Exists";
                    int i = 0;
                    //if (conflictitems.Count > chkcrewConflicts.Length)
                    //{
                    foreach (var items in conflictitems)
                    {
                        i++;
                        if (i == 1)
                            strWarning = strWarning + "<br/";
                        else
                            strWarning = strWarning + "========================<br/>";
                        if (items.RecordType == "C")
                            strWarning = strWarning + "<br/> Crew Code:" + Convert.ToString(items.CrewCD) + "; Crew Calendar Entry:" + i + ";<br/> Date/Time:" + Convert.ToDateTime(Session["chkCrewsdate"]) + " to " + Convert.ToDateTime(Session["chkCrewedate"]) + "<br/> Description: " + items.CrewDutyTypeDescription + "<br/>Conflicts with Crew Calendar Entry #" + items.TripNUM + " Scheduled on " + items.HomeDepartureDTTM.ToString() + "<br/>";
                        else if (items.RecordType == "T")
                            strWarning = strWarning + "<br/> Crew Code:" + Convert.ToString(items.CrewCD) + "; Crew Calendar Entry:" + i + ";<br/> Date/Time:" + Convert.ToDateTime(Session["chkCrewsdate"]) + " to " + Convert.ToDateTime(Session["chkCrewedate"]) + "<br/> Description: " + items.CrewDutyTypeDescription + "<br/>Conflicts with Trip #" + items.TripNUM + " Scheduled on " + items.HomeDepartureDTTM.ToString() + "<br/>";
                        //strWarning = strWarning + "<br/> Tail Number:" + Convert.ToString(Session["chkCrewtailnumber"]) + "; Crew Calendar Entry:" + i + ";<br/> Date/Time:" + Convert.ToDateTime(Session["chkCrewsdate"]) + " to " + Convert.ToDateTime(Session["chkCrewedate"]) + "<br/> Description: " + items.CrewDutyTypeDescription + "<br/>Conflicts with Trip #" + items.TripNUM + " Scheduled on " + items.HomeDepartureDTTM.ToString() + "<br/>";


                        //strWarning = strWarning + "<br/> Tail Number:" + Convert.ToString(Session["chkCrewtailnumber"]) + "; Crew Calendar Entry:" + i + ";<br/> Date/Time:" + Convert.ToDateTime(Session["chkCrewsdate"]) + " to " + Convert.ToDateTime(Session["chkCrewedate"]) + "<br/> Description: " + items.DutyTypesDescription + "<br/>Conflicts with Crew Calendar Entry #" + items.LegID + " Scheduled on " + items.StartDate + "<br/>";
                    }
                    if (i > 0)
                        RadAjaxManager1.ResponseScripts.Add("radalert('" + strWarning + "',400,250);");
                    Session["chkCrewConflicts"] = "false";
                    //}
                }
            }
        }

        protected void dgCrewCalendarEntries_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                DisplayEditForm();
                                GridEnable(false, true, false);
                                btnCopyCCR.Enabled = false;
                                btnCopyCCR.CssClass = "button-disable";
                                btnCancel.Visible = true;
                                isEdit = true;
                                GridPagerItem pagerItem = (GridPagerItem)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                                pagerItem.Display = false;
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgCrewCalendarEntries.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                btnCopyCCR.Enabled = false;
                                btnCopyCCR.CssClass = "button-disable";
                                btnCancel.Visible = true;
                                isInsert = true;
                                break;                           
                            case RadGrid.DeleteSelectedCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                dgCrewCalendarEntries_DeleteCommand(sender, e);
                                DisplayDeleteForm(true);
                                GridEnable(true, true, true);
                                break;
                            case "Delete":
                                dgCrewCalendarEntries_DeleteCommand(sender, e);
                                break;
                            case "UpdateEdited":
                                dgCrewCalendarEntries_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void dgCrewCalendarEntries_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (PreflightServiceClient UpdateCrewEntry = new PreflightServiceClient())
                        {
                            string legid = "";
                            string tripid = "";
                            string crewid = "";
                            string crewlist = "";
                            if ((Convert.ToString(hdnOptionGroup.Value) == "1") && (Convert.ToString(hdnOptionSingle.Value) == "0"))
                            {
                                legid = Convert.ToString(hdnMultiLeg.Value);
                                tripid = Convert.ToString(hdnMultiTrip.Value);
                                crewid = Convert.ToString(hdnMultiCrew.Value);
                                crewlist = Convert.ToString(hdnMultiCrewList.Value);
                            }
                            else
                            {
                                legid = Convert.ToString(hdnLegID.Value);
                                tripid = Convert.ToString(hdnTripID.Value);
                                crewid = Convert.ToString(hdnCrew.Value);
                                crewlist = Convert.ToString(hdncrewlistid.Value);
                            }

                            Int64 Previousnum;
                            if ((Convert.ToString(hdnOptionGroup.Value) == "1") && (Convert.ToString(hdnHaveGroup.Value) == "1"))
                                Previousnum = Convert.ToInt64(hdnPreviousNUM.Value);
                            else
                                Previousnum = 0;

                            string[] Crewids = crewid.Split(',');
                            string[] Tripids = tripid.Split(',');
                            string[] Legids = legid.Split(',');
                            string[] Crewlists = crewlist.Split(',');
                            bool flag = false;

                            long TripID = 0;
                            for (int i = 0; i < Crewids.Length; i++)
                            {
                                using (PreflightServiceClient Service = new PreflightServiceClient())
                                {
                                    PreflightMain Trip = new PreflightMain();
                                    Trip = Getitems(Convert.ToInt64(Crewids[i]), Previousnum, Convert.ToInt64(Tripids[i]), Convert.ToInt64(Legids[i]), Convert.ToInt64(Crewlists[i]));

                                    if (Trip.TripID > 0)
                                        TripID = Trip.TripID;
                                    var ReturnValue = Service.Update(Trip);
                                    //var ReturnValue = Service.Update(Getitems(Convert.ToInt64(Crewids[i]), Previousnum, Convert.ToInt64(Tripids[i]), Convert.ToInt64(Legids[i]), Convert.ToInt64(Crewlists[i])));
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        flag = true;
                                    }
                                }
                            }
                            if (flag == true)
                            {
                                //Unlock the records
                                UnLockRecord();
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                GridEnable(true, true, true);
                                if (TripID != 0)
                                {
                                    SelectItem();
                                    ReadOnlyForm();
                                    if (Trip != null)
                                    {
                                        if (Trip.State != TripEntityState.Added && Trip.State != TripEntityState.Modified)
                                        {
                                            btnCopyCCR.Enabled = true;
                                            btnCopyCCR.CssClass = "button";
                                        }
                                        else
                                        {
                                            btnCopyCCR.Enabled = false;
                                            btnCopyCCR.CssClass = "button-disable";
                                        }
                                    }
                                    else
                                    {
                                        btnCopyCCR.Enabled = false;
                                        btnCopyCCR.CssClass = "button-disable";
                                    }
                                }
                                else
                                    DefaultSelection();
                            }
                        }
                        GridPagerItem pagerItem = (GridPagerItem)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, delGrpCtl, editCtl, editGrpCtl;
                insertCtl = (LinkButton)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                delCtl = (LinkButton)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitDelete");
                delGrpCtl = (LinkButton)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkMultiDelete");
                editGrpCtl = (LinkButton)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkMultiEdit");
                editCtl = (LinkButton)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (IsAuthorized(Permission.Preflight.AddCrewCalendarEntry))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                    insertCtl.Visible = false;

                if (IsAuthorized(Permission.Preflight.DeleteCrewCalendarEntry))
                {
                    if (delete) 
                    {
                        delCtl.Visible = true;
                        delGrpCtl.Visible = true;
                        if (Convert.ToString(hdnHaveGroup.Value) == "0")
                        {                         
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";                         
                            delGrpCtl.Visible = false;                            
                            delGrpCtl.Enabled = false;
                        }
                        else if (Convert.ToString(hdnHaveGroup.Value) == "1")
                        {                            
                            delGrpCtl.Enabled = true;
                            delCtl.Enabled = false;
                            delCtl.Visible = false;                            
                        }
                        else
                        {
                            delCtl.Visible = true;
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                            delGrpCtl.Enabled = false;
                            delGrpCtl.Visible = false;
                        }
                    }                    
                    else
                    {                        
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                        delGrpCtl.Enabled = false;
                        delGrpCtl.Visible = false;
                    }
                }
                else                
                {                    
                    delCtl.Visible = false;
                    delGrpCtl.Visible = false;
                }

                if (IsAuthorized(Permission.Preflight.EditCrewCalendarEntry))
                {
                    if (edit)
                    {
                        editCtl.Visible = true;
                        editGrpCtl.Visible = true;

                        if (Convert.ToString(hdnHaveGroup.Value) == "0")
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            editGrpCtl.Visible = false;
                            editGrpCtl.Enabled = false;
                        }
                        else if (Convert.ToString(hdnHaveGroup.Value) == "1")
                        {
                            editGrpCtl.Enabled = true;
                            editCtl.Enabled = false;
                            editCtl.Visible = false;
                        }
                        else
                        {
                            editCtl.Visible = true;
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            editGrpCtl.Enabled = false;
                            editGrpCtl.Visible = false;
                        }
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                        editGrpCtl.Enabled = false;
                        editGrpCtl.Visible = false;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                    editGrpCtl.Visible = false;
                }
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //GridDataItem Item = (GridDataItem)Session["SelectedCrewItem"];
                if (dgCrewCalendarEntries.SelectedItems.Count > 0)
                {
                    GridDataItem Item = dgCrewCalendarEntries.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("CrewCD") != null)
                        tbCrew.Text = Convert.ToString(Item.GetDataKeyValue("CrewCD"));
                    else
                        tbCrew.Text = string.Empty;

                    if (Item.GetDataKeyValue("TailNum") != null)
                        tbTailNumber.Text = Convert.ToString((Item).GetDataKeyValue("TailNum"));
                    else
                        tbTailNumber.Text = string.Empty;

                    if (Item.GetDataKeyValue("StartDate") != null)
                    {
                        string startDateTime = ((DateTime) Item.GetDataKeyValue("StartDate")).ToString(
                            ApplicationDateFormat, CultureInfo.InvariantCulture);
                        tbStartDate.Text = startDateTime;
                        RadMaskedTextBox1.Text = ((DateTime) Item.GetDataKeyValue("StartDate")).ToString("H:mm",
                            CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        tbStartDate.Text = string.Empty;
                        RadMaskedTextBox1.Text = string.Empty;
                    }

                    if (Item.GetDataKeyValue("EndDate") != null)
                    {
                        tbEndDate.Text = ((DateTime) Item.GetDataKeyValue("EndDate")).ToString(ApplicationDateFormat,
                            CultureInfo.InvariantCulture);
                        rmtbStartTime.Text = ((DateTime) Item.GetDataKeyValue("EndDate")).ToString("H:mm",
                            CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        tbEndDate.Text = string.Empty;
                        rmtbStartTime.Text = string.Empty;
                    }

                    if (Item.GetDataKeyValue("IcaoID") != null)
                        tbICAO.Text = Convert.ToString((Item).GetDataKeyValue("IcaoID"));
                    else
                        tbICAO.Text = string.Empty;

                    if (Item.GetDataKeyValue("HomeBaseCD") != null)
                        tbHomeBase.Text = Convert.ToString((Item).GetDataKeyValue("HomeBaseCD"));
                    else
                        tbHomeBase.Text = string.Empty;

                    if (Item.GetDataKeyValue("DutyTypeCD") != null)
                        tbDuty.Text = Convert.ToString((Item).GetDataKeyValue("DutyTypeCD"));
                    else
                        tbDuty.Text = string.Empty;

                    if (Item.GetDataKeyValue("Notes") != null)
                        tbComment.Text = Convert.ToString((Item).GetDataKeyValue("Notes"));
                    else
                        tbComment.Text = string.Empty;

                    if (Item.GetDataKeyValue("ClientCD") != null)
                        tbClientCode.Text = Convert.ToString((Item).GetDataKeyValue("ClientCD"));
                    else
                        tbClientCode.Text = string.Empty;

                    if (Item.GetDataKeyValue("NoteSec") != null)
                        tbNotes.Text = Convert.ToString((Item).GetDataKeyValue("NoteSec"));
                    else
                        tbNotes.Text = string.Empty;

                    Item.Selected = true;
                }
                EnableForm(false);
            }
        }

        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbCrew.Enabled = Enable;
                btnCrew.Enabled = Enable;
                btnMultiCrew.Enabled = Enable;
                tbTailNumber.Enabled = Enable;
                btnTailNumber.Enabled = Enable;
                tbICAO.Enabled = Enable;
                btnICAO.Enabled = Enable;
                tbStartDate.Enabled = Enable;
                RadMaskedTextBox1.Enabled = Enable;
                tbHomeBase.Enabled = Enable;
                btnHomeBase.Enabled = Enable;
                tbEndDate.Enabled = Enable;
                rmtbStartTime.Enabled = Enable;
                tbClientCode.Enabled = false;
                btnClientCode.Enabled = false;
                if (Enable == true)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ClientId)))
                    {
                        tbClientCode.Enabled = false;
                        btnClientCode.Visible = false;
                    }
                    else
                    {
                        tbClientCode.Enabled = true;
                        btnClientCode.Visible = true;
                        btnClientCode.Enabled = true;
                    }
                }
                if (dgCrewCalendarEntries.Items.Count > 0)
                {
                    lnkHistory.CssClass = "history-icon";
                    lnkHistory.Enabled = true;
                }
                else
                {
                    lnkHistory.CssClass = "history-icon-disable";
                    lnkHistory.Enabled = false;
                }
                tbDuty.Enabled = Enable;
                btnDuty.Enabled = Enable;
                tbComment.Enabled = Enable;
                tbNotes.Enabled = Enable;
                //btnCancel.Visible = Enable;
                //btnSaveChanges.Visible = Enable;
                //btnGroupSaveChanges.Visible = Enable;
                btnSaveChanges.Visible = btnCancel.Visible = Enable;                
                btnGroupSaveChanges.Visible = false;
                //btnMultiDelete.Visible = Enable;
                //divMultiDelete.Visible = Enable; 
            }
        }

        protected void dgCrewCalendarEntries_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (PreflightServiceClient AddCrewEntry = new PreflightServiceClient())
                        {
                            string CrewIDs = "";
                            Int64 PreviousNum = Convert.ToInt64("0");
                            if (Convert.ToString(hdnMultiCrew.Value).Split(',').Length > 1)
                            {
                                CrewIDs = Convert.ToString(hdnMultiCrew.Value);
                                using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
                                {
                                    var sequenceNumber = commonService.GetSequenceNumber(Common.Constants.SequenceNumber.PreflightCurrentNo.ToString());
                                    if (sequenceNumber.HasValue)
                                        PreviousNum = Convert.ToInt64(sequenceNumber.Value);
                                }
                            }
                            else
                            {
                                CrewIDs = Convert.ToString(hdnCrew.Value);
                            }

                            string[] crewids = CrewIDs.Split(',');


                            bool flag = false;
                            for (int i = 0; i < crewids.Length; i++)
                            {
                                using (PreflightServiceClient Service = new PreflightServiceClient())
                                {
                                    var ReturnValue = Service.Add(Getitems(Convert.ToInt64(crewids[i]), PreviousNum, Convert.ToInt64("0"), Convert.ToInt64("0"), Convert.ToInt64("0")));
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        hdnMultiTrip.Value = hdnMultiTrip.Value + "," + Convert.ToString(ReturnValue.EntityInfo.TripID);
                                        hdnTripID.Value = hdnTripID.Value + "," + Convert.ToString(ReturnValue.EntityInfo.TripID);
                                        flag = true;
                                    }
                                }
                            }
                            if (flag == true)
                            {
                                pnlExternalForm.Visible = false;
                                dgCrewCalendarEntries.Rebind();
                                GridEnable(true, true, true);
                                DisplayInsertForm();
                                DefaultSelection();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void dgCrewCalendarEntries_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgCrewCalendarEntries.SelectedItems[0];
                        Session["SelectedCrewItem"] = Convert.ToString(item.GetDataKeyValue("TripID"));
                        Session["chkCrewConflicts"] = "false";
                        chkmultiplecrew(Convert.ToInt64(item.GetDataKeyValue("PreviousNUM")));
                        dgCrewCalendarEntries.Rebind();

                        hdnFleetID.Value = Convert.ToString((item).GetDataKeyValue("FleetID"));
                        hdnClientCode.Value = Convert.ToString(item.GetDataKeyValue("ClientID"));
                        hdnLegID.Value = Convert.ToString(item.GetDataKeyValue("LegID"));
                        hdnTripID.Value = Convert.ToString(item.GetDataKeyValue("TripID"));
                        hdnPreviousNUM.Value = Convert.ToString(item.GetDataKeyValue("PreviousNUM"));
                        hdnCrew.Value = Convert.ToString(item.GetDataKeyValue("CrewID"));
                        hdncrewlistid.Value = Convert.ToString(item.GetDataKeyValue("PreflightCrewListID"));
                        hdntripnum.Value = Convert.ToString(item.GetDataKeyValue("TripNUM"));
                        hdnTripIds.Value = Convert.ToString(item.GetDataKeyValue("TripID"));
                        item.Selected = true;
                        ReadOnlyForm();                        
                        GridPagerItem pagerItem = (GridPagerItem)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = false;
                        DisplayDeleteForm(false);
                        GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        public void chkmultiplecrew(Int64 previousnum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(previousnum))
            {
                if (previousnum > 0)
                {
                    using (PreflightServiceClient objservice = new PreflightServiceClient())
                    {
                        var objvalue = objservice.GetCrewCalendarEntries(FilterOptions.TimeBase.ToString(), Convert.ToDateTime(Session["CrewStartDate"]), Convert.ToDateTime(Session["CrewEndDate"]));

                        var conflictitems = objvalue.EntityList.Where(x => x.PreviousNUM == previousnum);
                        string strCrew = "", strtrip = "", strleg = "", strcrewlist = "";
                        int i = 0;
                        foreach (var items in conflictitems)
                        {
                            i++;
                            if (i == 1)
                            {
                                strCrew = Convert.ToString(items.CrewID);
                                strtrip = Convert.ToString(items.TripID);
                                strleg = Convert.ToString(items.LegID);
                                strcrewlist = Convert.ToString(items.PreflightCrewListID);
                            }
                            else
                            {
                                strCrew = strCrew + "," + Convert.ToString(items.CrewID);
                                strtrip = strtrip + "," + Convert.ToString(items.TripID);
                                strleg = strleg + "," + Convert.ToString(items.LegID);
                                strcrewlist = strcrewlist + "," + Convert.ToString(items.PreflightCrewListID);
                            }
                        }

                        if (strCrew.Split(',').Length > 1)
                        {
                            hdnHaveGroup.Value = "1";
                            hdnMultiCrew.Value = Convert.ToString(strCrew);
                            hdnMultiLeg.Value = Convert.ToString(strleg);
                            hdnMultiTrip.Value = Convert.ToString(strtrip);
                            hdnMultiCrewList.Value = Convert.ToString(strcrewlist);
                            lblMultiCrew.Attributes.Add("style", "display:inherit;");
                            lblMultiCrew.Attributes.Add("style", "Color:Red;");
                        }
                        else
                        {
                            hdnHaveGroup.Value = "0";
                            lblMultiCrew.Attributes.Add("style", "display:none;");
                        }
                    }
                }
                else
                {
                    hdnHaveGroup.Value = "0";
                    lblMultiCrew.Attributes.Add("style", "display:none;");
                }
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCrew.Text = string.Empty;
                tbTailNumber.Text = string.Empty;
                tbICAO.Text = string.Empty;
                tbStartDate.Text = string.Empty;
                RadMaskedTextBox1.Text = "00:00";
                tbHomeBase.Text = string.Empty;
                tbEndDate.Text = string.Empty;
                rmtbStartTime.Text = "00:00";
                tbClientCode.Text = string.Empty;
                tbDuty.Text = string.Empty;
                tbComment.Text = string.Empty;
                tbNotes.Text = string.Empty;
                lbcvClientCode.Text = string.Empty;
                lbcvCrew.Text = string.Empty;
                lbcvDuty.Text = string.Empty;
                lbcvHomeBase.Text = string.Empty;
                lbcvICAO.Text = string.Empty;
                lbcvTailNumber.Text = string.Empty;
                hdnClientCode.Value = string.Empty;
                hdnCrew.Value = string.Empty;
                hdnFleetID.Value = string.Empty;
                hdnHaveGroup.Value = string.Empty;
                hdnHomeBase.Value = string.Empty;
                hdnICAO.Value = string.Empty;
                hdnLegID.Value = string.Empty;
                hdnMultiCrew.Value = string.Empty;
                hdnMultiLeg.Value = string.Empty;
                hdnMultiTrip.Value = string.Empty;
                hdnOptionGroup.Value = string.Empty;
                hdnOptionSingle.Value = string.Empty;
                hdnPreviousNUM.Value = string.Empty;
                hdnTripID.Value = string.Empty;
                hdncrewlistid.Value = string.Empty;
                hdnMultiCrewList.Value = string.Empty;
                hdntripnum.Value = string.Empty;
                lbcvstartdate.Text = string.Empty;
                lbcvenddate.Text = string.Empty;
            }
        }

        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                btnSaveChanges.Text = "Save";
                pnlExternalForm.Visible = true;
                hdnSaveFlag.Value = "Save";
                ClearForm();
                dgCrewCalendarEntries.Rebind();
                EnableForm(true);
                btnSaveChanges.Visible = btnCancel.Visible = true; 
                btnGroupSaveChanges.Visible = false;
                //btnMultiDelete.Visible = false;
                //lnkMultiDelete.Visible = false; 
                hdnHaveGroup.Value = "0";
                lblMultiCrew.Attributes.Add("style", "display:none;");
                hdnMultiCrew.Value = "";
                if ((!string.IsNullOrEmpty(Request["tailNum"])) && (Convert.ToString(Request["tailNum"]) != "null"))
                {
                    tbTailNumber.Text = Request["tailNum"].ToString();
                }
                else
                {
                    tbCrew.Text = hdndefaultCrew.Value;
                }
                //tbStartDate.Text = hdndefaultSDate.Value;
                tbStartDate.Text = Convert.ToString(Convert.ToDateTime(hdndefaultSDate.Value).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                tbEndDate.Text = Convert.ToString(Convert.ToDateTime(hdndefaultSDate.Value).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = ObjService.GetCrewList().EntityList.Where(x => x.CrewCD.Trim().ToUpper() == (hdndefaultCrew.Value.Trim().ToUpper())).ToList();
                    if (objRetVal.Count() <= 0)
                    {
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(hdnMultiCrew.Value))
                        {
                            hdnCrew.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).CrewID.ToString();
                            hdnMultiCrew.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).CrewID.ToString();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Convert.ToString(HomeBaseId)))
                {
                    tbHomeBase.Text = HomeBaseCode;
                    hdnHomeBase.Value = Convert.ToString(HomeBaseId);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(HomeAirportId)))
                {
                    tbICAO.Text = HomeBaseCode;
                    hdnICAO.Value = Convert.ToString(HomeAirportId);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ClientId)))
                {
                    tbClientCode.Text = ClientCode;
                    hdnClientCode.Value = Convert.ToString(ClientId);
                    tbClientCode.Enabled = false;
                    btnClientCode.Visible = false;
                }
            }
        }

        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedCrewItem"] != null)
                {
                    foreach (GridDataItem Item in dgCrewCalendarEntries.MasterTableView.Items)
                    {
                        if (Item.GetDataKeyValue("TripID").ToString().Trim() == Session["SelectedCrewItem"].ToString().Trim())
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedCrewItem"];
                            tbCrew.Text = Convert.ToString(Item.GetDataKeyValue("CrewCD"));
                            hdnCrew.Value = Convert.ToString((Item).GetDataKeyValue("CrewID"));//CrewID

                            tbTailNumber.Text = Convert.ToString((Item).GetDataKeyValue("TailNum"));

                            tbStartDate.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            RadMaskedTextBox1.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString("H:mm", CultureInfo.InvariantCulture);

                            tbICAO.Text = Convert.ToString((Item).GetDataKeyValue("IcaoID"));
                            hdnICAO.Value = Convert.ToString((Item).GetDataKeyValue("AirportID"));// ICAOID as AirportID

                            tbHomeBase.Text = Convert.ToString((Item).GetDataKeyValue("HomeBaseCD"));
                            hdnHomeBase.Value = Convert.ToString((Item).GetDataKeyValue("HomebaseID"));//HomebaseID

                            tbEndDate.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            rmtbStartTime.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString("H:mm", CultureInfo.InvariantCulture);

                            tbDuty.Text = Convert.ToString((Item).GetDataKeyValue("DutyTypeCD"));
                            tbComment.Text = Convert.ToString((Item).GetDataKeyValue("Notes"));
                            tbNotes.Text = Convert.ToString((Item).GetDataKeyValue("NoteSec"));
                            tbClientCode.Text = Convert.ToString((Item).GetDataKeyValue("ClientCD"));
                            hdnClientCode.Value = Convert.ToString((Item).GetDataKeyValue("ClientID"));//ClientID

                            if (!string.IsNullOrEmpty(Convert.ToString(UserPrincipal.Identity._clientId)))
                            {
                                tbClientCode.Enabled = false;
                                btnClientCode.Visible = false;
                            }

                            hdnFleetID.Value = Convert.ToString((Item).GetDataKeyValue("FleetID"));//FleetID
                            hdnTripID.Value = Convert.ToString((Item).GetDataKeyValue("TripID"));//TripID               
                            hdnLegID.Value = Convert.ToString((Item).GetDataKeyValue("LegID"));//LegID               
                            hdnPreviousNUM.Value = Convert.ToString(Item.GetDataKeyValue("PreviousNUM"));//PreviousNUM
                            hdnCrew.Value = Convert.ToString(Item.GetDataKeyValue("CrewID"));//CrewID   
                            hdncrewlistid.Value = Convert.ToString(Item.GetDataKeyValue("PreflightCrewListID"));//PreflightCrewListID
                            hdntripnum.Value = Convert.ToString(Item.GetDataKeyValue("TripNUM"));//TripNUM
                            chkmultiplecrew(Convert.ToInt64(Item.GetDataKeyValue("PreviousNUM")));

                            //Logic: If Multiple Crews, then Lock all the Crews, Else Single Crew
                            if (hdnHaveGroup.Value == "1")
                            {
                                string[] LockTrip = Convert.ToString(hdnMultiTrip.Value).Split(',');
                                string[] LockLeg = Convert.ToString(hdnMultiLeg.Value).Split(',');
                                for (int k = 0; k < LockTrip.Length; k++)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var TripreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(LockTrip[k]));
                                        var LegreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(LockLeg[k]));
                                        if (!TripreturnValue.ReturnFlag)
                                        {
                                            ShowAlert(TripreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                            return;
                                        }
                                        if (!LegreturnValue.ReturnFlag)
                                        {
                                            ShowAlert(LegreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var TripreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(hdnTripID.Value));
                                    var LegreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(hdnLegID.Value));
                                    if (!TripreturnValue.ReturnFlag)
                                    {
                                        ShowAlert(TripreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                        return;
                                    }
                                    if (!LegreturnValue.ReturnFlag)
                                    {
                                        ShowAlert(LegreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                        return;
                                    }
                                }
                            }
                            hdnSaveFlag.Value = "Update";

                            btnSaveChanges.Visible = btnCancel.Visible = true; 
                            dgCrewCalendarEntries.Rebind();
                            Item.Selected = true;
                            EnableForm(true);
                            tbCrew.Enabled = false;
                            btnCrew.Enabled = false;
                            btnMultiCrew.Enabled = false;
                            if (Convert.ToString(hdnHaveGroup.Value) == "1")
                            {
                                //btnMultiDelete.Visible = true;
                                //lnkMultiDelete.Visible = true; 
                                //btnGroupSaveChanges.Visible = true;
                                btnGroupSaveChanges.Visible = false;
                                btnSaveChanges.Visible = btnCancel.Visible = true;  
                                //btnGroupSaveChanges.Text = "EDIT GROUP";
                                //btnGroupSaveChanges.ToolTip = "Select GROUP To Perform Action On Entire Group";
                                //btnSaveChanges.Text = "EDIT SINGLE";
                                //btnSaveChanges.ToolTip = "Select SINGLE To Perform Action On Selected Record";
                                //btnCancel.Visible = false; 
                            }
                            else
                            {
                                btnGroupSaveChanges.Visible = false;
                                //lnkMultiDelete.Visible = false;
                                //btnMultiDelete.Visible = false;
                                btnSaveChanges.Visible = btnCancel.Visible = true;   
                                btnGroupSaveChanges.Text = "Group";                                                                                                                            
                            }
                            break;
                        }
                    }
                }
            }
        }

        protected void dgCrewCalendarEntries_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CrewPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void dgCrewCalendarEntries_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["chkCrewConflicts"] = "false";
                        CrewPageNavigated = true;
                        dgCrewCalendarEntries.ClientSettings.Scrolling.ScrollTop = "0";                        
                        selectFirstItem = true;                                                 
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewItem"] != null)
                    {
                        string ID = Session["SelectedCrewItem"].ToString();
                        foreach (GridDataItem item in dgCrewCalendarEntries.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("TripID").ToString().Trim() == ID)
                            {
                                //dgFleetCalendarEntries.Rebind();
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void DisplayDeleteForm(bool isDelete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedCrewItem"] != null)
                {
                    foreach (GridDataItem Item in dgCrewCalendarEntries.MasterTableView.Items)
                    {
                        if (Item.GetDataKeyValue("TripID").ToString().Trim() == Session["SelectedCrewItem"].ToString().Trim())
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedCrewItem"];
                            tbCrew.Text = Convert.ToString(Item.GetDataKeyValue("CrewCD"));
                            hdnCrew.Value = Convert.ToString((Item).GetDataKeyValue("CrewID"));//CrewID

                            tbTailNumber.Text = Convert.ToString((Item).GetDataKeyValue("TailNum"));

                            tbStartDate.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            RadMaskedTextBox1.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString("H:mm", CultureInfo.InvariantCulture);

                            tbICAO.Text = Convert.ToString((Item).GetDataKeyValue("IcaoID"));
                            hdnICAO.Value = Convert.ToString((Item).GetDataKeyValue("AirportID"));// ICAOID as AirportID

                            tbHomeBase.Text = Convert.ToString((Item).GetDataKeyValue("HomeBaseCD"));
                            hdnHomeBase.Value = Convert.ToString((Item).GetDataKeyValue("HomebaseID"));//HomebaseID

                            tbEndDate.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            rmtbStartTime.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString("H:mm", CultureInfo.InvariantCulture);

                            tbDuty.Text = Convert.ToString((Item).GetDataKeyValue("DutyTypeCD"));
                            tbComment.Text = Convert.ToString((Item).GetDataKeyValue("Notes"));
                            tbNotes.Text = Convert.ToString((Item).GetDataKeyValue("NoteSec"));
                            tbClientCode.Text = Convert.ToString((Item).GetDataKeyValue("ClientCD"));
                            hdnClientCode.Value = Convert.ToString((Item).GetDataKeyValue("ClientID"));//ClientID
                            hdnFleetID.Value = Convert.ToString((Item).GetDataKeyValue("FleetID"));//FleetID
                            hdnTripID.Value = Convert.ToString((Item).GetDataKeyValue("TripID"));//TripID
                            hdnLegID.Value = Convert.ToString((Item).GetDataKeyValue("LegID"));//LegID

                            hdnPreviousNUM.Value = Convert.ToString(Item.GetDataKeyValue("PreviousNUM"));//PreviousNUM
                            hdnCrew.Value = Convert.ToString(Item.GetDataKeyValue("CrewID"));//CrewID  
                            hdncrewlistid.Value = Convert.ToString(Item.GetDataKeyValue("PreflightCrewListID"));//PreflightCrewListID
                            hdntripnum.Value = Convert.ToString(Item.GetDataKeyValue("TripNUM"));//TripNUM
                            chkmultiplecrew(Convert.ToInt64(Item.GetDataKeyValue("PreviousNUM")));
                            hdnSaveFlag.Value = "Delete";
                            //btnCancel.Visible = true;
                            btnSaveChanges.Visible = btnCancel.Visible = true; 
                            dgCrewCalendarEntries.Rebind();
                            Item.Selected = true;
                            EnableForm(false);
                            EnableDeleteForm(true);
                            GridEnable(true, true, true);   
                            if (Convert.ToString(hdnHaveGroup.Value) == "1")
                            {
                                //btnMultiDelete.Visible = true;
                                //lnkMultiDelete.Visible = true; 
                                //btnGroupSaveChanges.Text = "DELETE GROUP";
                                //btnGroupSaveChanges.ToolTip = "Select GROUP To Perform Action On Entire Group";
                                //btnSaveChanges.Text = "DELETE SINGLE";
                                //btnSaveChanges.ToolTip = "Select SINGLE To Perform Action On Selected Record";
                                btnGroupSaveChanges.Visible = false;
                                btnSaveChanges.Visible = btnCancel.Visible = false;  
                                //if (isDelete) 
                                //    RadWindowManager1.RadConfirm("Are you sure you want to delete the entire group1?", "ConfirmDeleteAllfn", 300, 150, null, "Crew Calendar Entries");
                            }
                            else
                            {
                                //btnMultiDelete.Visible = false;
                                //lnkMultiDelete.Visible = false;
                                btnGroupSaveChanges.Visible = false;
                                btnSaveChanges.Visible = btnCancel.Visible = false; 
                                //btnGroupSaveChanges.Text = "Group";
                                //btnSaveChanges.Text = "Delete";
                            }
                            break;
                        }
                    }
                }
            }
        }

        protected void EnableDeleteForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //btnCancel.Visible = Enable;
                //btnSaveChanges.Visible = Enable;
                //btnGroupSaveChanges.Visible = Enable;
                btnGroupSaveChanges.Visible = !Enable;
                //btnMultiDelete.Visible = Enable;
                //lnkMultiDelete.Visible = Enable;
                btnSaveChanges.Visible = btnCancel.Visible = !Enable;  
            }
        }

        protected void radCalenderEntry_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        pnlExternalForm.Visible = true;
                        pnlExternalForm1.Visible = false;                        
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void radHistory_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        pnlExternalForm.Visible = false;
                        pnlExternalForm1.Visible = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            //hdnOptionGroup.Value = "0";
                            //hdnOptionSingle.Value = "1";
                            tbTailNumber.Focus();
                            tbDuty.Focus();
                            tbTailNumber.Focus();
                            if ((string.IsNullOrEmpty(lbcvTailNumber.Text)) && (string.IsNullOrEmpty(lbcvICAO.Text)) && (string.IsNullOrEmpty(lbcvHomeBase.Text)) && (string.IsNullOrEmpty(lbcvClientCode.Text)) && (string.IsNullOrEmpty(lbcvDuty.Text)) && CheckDateRange())
                            {
                                if (hdnSaveFlag.Value == "Update")
                                {
                                    (dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else if (hdnSaveFlag.Value == "Delete")
                                {
                                    RadWindowManager1.RadConfirm("Are you sure you want to delete this record?", "ConfirmDeletefn", 330, 150, null, "Crew Calendar Entries");
                                }
                                else
                                    (dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            btnCopyCCR.Enabled = true;
                            btnCopyCCR.CssClass = "button";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        (dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.DeleteCommandName, string.Empty);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //TO DO
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void GroupEditSaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnOptionGroup.Value = "1";
                        hdnOptionSingle.Value = "0";
                        DisplayEditForm();
                        GridEnable(false, true, false);
                        btnCopyCCR.Enabled = false;
                        btnCopyCCR.CssClass = "button-disable";
                        btnCancel.Visible = true;
                        GridPagerItem pagerItem =
                            (GridPagerItem) dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }
        protected void SaveEditChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnOptionGroup.Value = "0";
                        hdnOptionSingle.Value = "1";
                        DisplayEditForm();
                        GridEnable(false, true, false);
                        btnCopyCCR.Enabled = false;
                        btnCopyCCR.CssClass = "button-disable";
                        btnCancel.Visible = true;
                        GridPagerItem pagerItem =
                            (GridPagerItem) dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void GroupSaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnOptionGroup.Value = "1";
                        hdnOptionSingle.Value = "0";
                        if (IsEmptyCheck)
                        {
                            if ((string.IsNullOrEmpty(lbcvTailNumber.Text)) && (string.IsNullOrEmpty(lbcvICAO.Text)) && (string.IsNullOrEmpty(lbcvHomeBase.Text)) && (string.IsNullOrEmpty(lbcvClientCode.Text)) && (string.IsNullOrEmpty(lbcvDuty.Text)) && CheckDateRange())
                            {
                                if (hdnSaveFlag.Value == "Update")
                                {
                                    (dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                    RadWindowManager1.RadConfirm("Are you sure you want to delete the entire group?", "ConfirmDeleteAllfn", 350, 150, null, "Crew Calendar Entries");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void btnDeleteAllYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        (dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.DeleteCommandName, string.Empty);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void btnDeleteAllNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //TO DO
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected bool CheckDateRange()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool check = true;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    DateTimeFormatInfo DTF = new DateTimeFormatInfo();
                    DTF.ShortDatePattern = ApplicationDateFormat;
                    DateTime StartDate = new DateTime(), EndDate = new DateTime();
                    if (string.IsNullOrEmpty(tbStartDate.Text))
                    {
                        //RadAjaxManager1.ResponseScripts.Add("radalert('Start Date cannot be Empty.',300,150);");
                        check = false;
                        return check;
                    }
                    if (string.IsNullOrEmpty(tbEndDate.Text))
                    {
                        //RadAjaxManager1.ResponseScripts.Add("radalert('End Date cannot be Empty.',300,150);");
                        check = false;
                        return check;
                    }
                    if (!string.IsNullOrEmpty(hdndefaultSDate.Value) && (Convert.ToString(hdndefaultSDate.Value) == tbStartDate.Text))
                    {
                        StartDate = Convert.ToDateTime(tbStartDate.Text.ToString());
                        StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                        StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));

                        EndDate = Convert.ToDateTime(tbEndDate.Text.ToString());
                        EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                        EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbStartDate.Text))
                        {
                            StartDate = Convert.ToDateTime(tbStartDate.Text.ToString(), DTF);
                            StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                            StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));

                            EndDate = Convert.ToDateTime(tbEndDate.Text.ToString(), DTF);
                            EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                            EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                        }
                    }

                    if (StartDate > EndDate)
                    {
                        if (StartDate > EndDate)
                            RadAjaxManager1.ResponseScripts.Add("radalert('End Date cannot be less than Start Date.',300,150, 'System Messages');");
                        //else if (StartDate == EndDate)
                        //    RadAjaxManager1.ResponseScripts.Add("radalert('Startdate and EndDate are Equal',100,150);");
                        check = false;
                    }
                }
                return check;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["chkCrewConflicts"] = "false";
                        DefaultSelection();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UnLockRecord();
                        Session.Remove("SelectedCrewItem");
                        Session["chkCrewConflicts"] = "false";
                        DefaultSelection();
                        btnCancel.Visible = false;
                        GridPagerItem pagerItem = (GridPagerItem)dgCrewCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
                            e.Updated = dgCrewCalendarEntries;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void dgCrewCalendarEntries_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Commented by Vishwa
                        //We are not using this pager any more.
                        //if (e.Item is GridPagerItem)
                        //{
                        //    GridPagerItem pager = (GridPagerItem)e.Item;
                        //    RadComboBox PageSizeComboBox = (RadComboBox)pager.FindControl("PageSizeComboBox");
                        //    PageSizeComboBox.Visible = false;

                        //    Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                        //    lbl.Visible = false;
                        //}
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["SDate"];
                            TableCell cell1 = (TableCell)Item["EDate"];
                            DTF.ShortTimePattern = "H:mm";
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = String.Format("{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}", Convert.ToDateTime(cell.Text));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = String.Format("{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}", Convert.ToDateTime(cell1.Text));
                            }                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void dgCrewCalendarEntries_ColumnCreating(object sender, GridColumnCreatingEventArgs e)
        {
        }

        protected void dgCrewCalendarEntries_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (PreflightServiceClient DeleteCrewEntry = new PreflightServiceClient())
                        {
                            Session["chkCrewConflicts"] = "false";
                            string legid = "";
                            string tripid = "";
                            string crewid = "";
                            string crewlist = "";
                            if ((Convert.ToString(hdnOptionGroup.Value) == "1") && (Convert.ToString(hdnOptionSingle.Value) == "0"))
                            {
                                legid = Convert.ToString(hdnMultiLeg.Value);
                                tripid = Convert.ToString(hdnMultiTrip.Value);
                                crewid = Convert.ToString(hdnMultiCrew.Value);
                                crewlist = Convert.ToString(hdnMultiCrewList.Value);
                            }
                            else
                            {
                                legid = Convert.ToString(hdnLegID.Value);
                                tripid = Convert.ToString(hdnTripID.Value);
                                crewid = Convert.ToString(hdnCrew.Value);
                                crewlist = Convert.ToString(hdncrewlistid.Value);
                            }
                            //Lock the record Before Delete
                            if ((hdnHaveGroup.Value == "1") && (Convert.ToString(hdnOptionGroup.Value) == "1"))
                            {
                                string[] LockTrip = Convert.ToString(hdnMultiTrip.Value).Split(',');
                                string[] LockLeg = Convert.ToString(hdnMultiLeg.Value).Split(',');
                                for (int k = 0; k < LockTrip.Length; k++)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var TripreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(LockTrip[k]));
                                        var LegreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(LockLeg[k]));
                                        if (!TripreturnValue.ReturnFlag)
                                        {
                                            ShowAlert(TripreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                            return;
                                        }
                                        if (!LegreturnValue.ReturnFlag)
                                        {
                                            ShowAlert(LegreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                            return;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var TripreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(hdnTripID.Value));
                                    var LegreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(hdnLegID.Value));
                                    if (!TripreturnValue.ReturnFlag)
                                    {
                                        ShowAlert(TripreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                        return;
                                    }
                                    if (!LegreturnValue.ReturnFlag)
                                    {
                                        ShowAlert(LegreturnValue.LockMessage, ModuleNameConstants.Preflight.CrewCalendarEntry);
                                        return;
                                    }
                                }
                            }

                            //DeleteCrewEntry.DeleteCrewCalendarEntries(legid, tripid, crewid);

                            bool flag = false;
                            string[] deletetripid = tripid.Split(',');
                            for (int i = 0; i < deletetripid.Length; i++)
                            {
                                using (PreflightServiceClient Service = new PreflightServiceClient())
                                {
                                    var ReturnValue = Service.Delete(Convert.ToInt64(deletetripid[i]));
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        flag = true;
                                    }
                                }
                            }
                            if (flag == true)
                            {
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
                finally
                {
                    //Unlock the records
                    UnLockRecord();
                    ClearForm();
                    DefaultSelection();
                }
            }
        }

        protected void tbCrew_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbCrew.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetCrewList().EntityList.Where(x => x.CrewCD.Trim().ToUpper() == (tbCrew.Text.Trim().ToUpper())).ToList();
                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvCrew.Text = System.Web.HttpUtility.HtmlEncode("Invalid Crew Code");
                                    hdnCrew.Value = string.Empty;
                                    hdnMultiCrew.Value = string.Empty;
                                    tbCrew.Focus();
                                }
                                else
                                {
                                    lbcvCrew.Text = string.Empty;
                                    if (string.IsNullOrEmpty(hdnMultiCrew.Value))
                                    {
                                        hdnCrew.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).CrewID.ToString();
                                        hdnMultiCrew.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).CrewID.ToString();
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbTailNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbTailNumber.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetFleetByTailNumber(tbTailNumber.Text.Trim().ToUpper()).EntityInfo;
                                if (objRetVal == null)
                                {
                                    lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Tail Number");
                                    hdnFleetID.Value = string.Empty;
                                    tbTailNumber.Focus();
                                }
                                else
                                {
                                    lbcvTailNumber.Text = string.Empty;
                                    //Session["hdnfleetid"] = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)objRetVal).FleetID.ToString(); // Commented becoz of not used any where in the pages.
                                    hdnFleetID.Value = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)objRetVal).FleetID.ToString();
                                    tbTailNumber.Text = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)objRetVal).TailNum.ToUpper().ToString();
                                }
                            }
                        }
                        else
                        {
                            lbcvTailNumber.Text = string.Empty;
                            hdnFleetID.Value = string.Empty;
                            tbTailNumber.Focus();
                            tbDuty.Focus();
                            tbTailNumber.Focus();
                        }
                        chkMultipleCrew();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbICAO.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbICAO.Text.Trim()).EntityList;
                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvICAO.Text = System.Web.HttpUtility.HtmlEncode("Invalid ICAO");
                                    tbICAO.Focus();
                                }
                                else
                                {
                                    lbcvICAO.Text = string.Empty;
                                    hdnICAO.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportID.ToString();
                                }
                            }
                        }
                        chkMultipleCrew();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbHomeBase.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomeBase.Text.ToUpper().Trim())).ToList();

                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Invalid HomeBase");
                                    tbHomeBase.Focus();
                                }
                                else
                                {
                                    lbcvHomeBase.Text = string.Empty;
                                    hdnHomeBase.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)objRetVal[0]).HomebaseID.ToString();
                                }
                            }
                        }
                        chkMultipleCrew();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbClientCode.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetClientByClientCD(tbClientCode.Text.Trim()).EntityList;
                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid ClientCode");
                                    tbClientCode.Focus();
                                }
                                else
                                {
                                    lbcvClientCode.Text = string.Empty;
                                    hdnClientCode.Value = ((FlightPak.Web.FlightPakMasterService.ClientByClientCDResult)objRetVal[0]).ClientID.ToString();
                                }
                            }
                        }
                        else
                        {
                            lbcvClientCode.Text = string.Empty;
                            hdnClientCode.Value = string.Empty;
                        }
                        chkMultipleCrew();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbDuty_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDuty.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetCrewDutyTypeList().EntityList.Where(x => x.DutyTypeCD.Trim().ToUpper() == (tbDuty.Text.Trim().ToUpper())).ToList();
                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvDuty.Text = System.Web.HttpUtility.HtmlEncode("Invalid Duty");
                                    tbDuty.Focus();
                                }
                                else
                                {
                                    lbcvDuty.Text = string.Empty;
                                    tbComment.Text = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyTypesDescription.ToString();
                                    if (hdnCrewDutyTimeChangeFlag.Value == "false")
                                    {
                                        RadMaskedTextBox1.Text = Convert.ToString(((FlightPak.Web.FlightPakMasterService.CrewDutyType) objRetVal[0]).DutyStartTM);
                                        rmtbStartTime.Text = Convert.ToString(((FlightPak.Web.FlightPakMasterService.CrewDutyType) objRetVal[0]).DutyEndTM);
                                    }
                                }
                            }
                        }
                        chkMultipleCrew();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void UnLockRecord()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //if ((hdnHaveGroup.Value == "1") && (Convert.ToString(hdnOptionGroup.Value) == "1"))
                if ((hdnSaveFlag.Value == "Update") || (hdnSaveFlag.Value == "Delete"))
                {
                    if (hdnHaveGroup.Value == "1")
                    {
                        string[] LockTrip = Convert.ToString(hdnMultiTrip.Value).Split(',');
                        string[] LockLeg = Convert.ToString(hdnMultiLeg.Value).Split(',');
                        for (int k = 0; k < LockTrip.Length; k++)
                        {
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var TripreturnValue = CommonService.UnLock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(LockTrip[k]));
                                    var LegreturnValue = CommonService.UnLock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(LockLeg[k]));
                                }
                            }
                        }
                    }
                    else
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var TripreturnValue = CommonService.UnLock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(hdnTripID.Value));
                            var LegreturnValue = CommonService.UnLock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(hdnLegID.Value));
                        }
                    }
                }
            }
        }

        protected void Log_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCrewItem"] != null)
                        {
                            foreach (GridDataItem Item in dgCrewCalendarEntries.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("TripID").ToString().Trim() == Session["SelectedCrewItem"].ToString().Trim())
                                {
                                    string DisplayMessage = "";
                                    if (Item.GetDataKeyValue("SimulatorLog") != null && !string.IsNullOrEmpty(Item.GetDataKeyValue("SimulatorLog").ToString()) && (Convert.ToString(Item.GetDataKeyValue("SimulatorLog")) != "0"))
                                    {
                                        DisplayMessage = "The Selected Crew Entry Is Already Logged.<br/> All Existing Log Information Will Be Overwritten?";

                                        Session["SimulatorID"] = Item.GetDataKeyValue("SimulatorLog");
                                    }
                                    else
                                        DisplayMessage = "Are you sure you want to create a Crew Log?";


                                    RadWindowManager1.RadConfirm(DisplayMessage, "confirmLog", 340, 100, null, "Confirmation");
                                    break;
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }

        }
        protected void btnCopyYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Web.PostflightService.PostflightSimulatorLog PFSL = new FlightPak.Web.PostflightService.PostflightSimulatorLog();
                        //GridDataItem Item = (GridDataItem)Session["SelectedCrewItem"];
                        if (Session["SelectedCrewItem"] != null)
                        {
                            foreach (GridDataItem Item in dgCrewCalendarEntries.MasterTableView.Items)
                            {
                                if (Item.GetDataKeyValue("TripID").ToString().Trim() == Session["SelectedCrewItem"].ToString().Trim())
                                {
                                    PFSL = GetLog(Item);

                                    using (PostflightService.PostflightServiceClient postflightservice = new PostflightService.PostflightServiceClient())
                                    {
                                        var ReturnValue = postflightservice.UpdateOtherCrew(PFSL);
                                        if (ReturnValue.ReturnFlag == true && ReturnValue.EntityInfo != null && ReturnValue.EntityInfo.SimulatorID != null)
                                        {
                                            Session["SimulatorID"] = ReturnValue.EntityInfo.SimulatorID.ToString();
                                            RadWindowManager1.RadConfirm("Edit Other Crew Duty Log?", "confirmEdit", 330, 100, null, "Confirmation");
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }
        protected void btnCopyNo_Click(object sender, EventArgs e)
        {
        }
        protected void btnEditYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string url = "../../PostFlight/OtherCrewDutyLog.aspx?IsLogPopup=Popup&SimulatorID=" + Convert.ToInt64(Session["SimulatorID"]);
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", "window.open('" + url + "','','scrollbars=yes,width=770,height=600')", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }
        protected void btnEditNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCrewCalendarEntries.Rebind();
                        dgCrewCalendarEntries.SelectedIndexes.Add(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }
        private FlightPak.Web.PostflightService.PostflightSimulatorLog GetLog(GridDataItem Item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Item))
            {
                FlightPak.Web.PostflightService.PostflightSimulatorLog PFSL = new FlightPak.Web.PostflightService.PostflightSimulatorLog();
                PFSL.AirportID = Convert.ToInt64((Item).GetDataKeyValue("AirportID"));
                PFSL.ArrivalDTTMLocal = Convert.ToDateTime((Item).GetDataKeyValue("EndDate"));
                string ClientID = null;
                ClientID = Convert.ToString((Item).GetDataKeyValue("ClientID"));
                if (!string.IsNullOrEmpty(ClientID))
                    PFSL.ClientID = Convert.ToInt64(ClientID);
                else
                    PFSL.ClientID = null;
                PFSL.CrewID = Convert.ToInt64((Item).GetDataKeyValue("CrewID"));
                PFSL.DepartureDTTMLocal = Convert.ToDateTime((Item).GetDataKeyValue("StartDate"));
                PFSL.SimulatorDescription = Convert.ToString((Item).GetDataKeyValue("DutyTypesDescription"));

                using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = ObjService.GetCrewDutyTypeList().EntityList.Where(x => x.DutyTypeCD.Trim().ToUpper() == (Convert.ToString((Item).GetDataKeyValue("DutyType")).Trim().ToUpper())).ToList();
                    PFSL.DutyTypeID = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyTypeID;
                }

                PFSL.HomeBaseID = Convert.ToInt64((Item).GetDataKeyValue("HomebaseID"));
                PFSL.LastUserID = UserPrincipal.Identity._name;
                PFSL.LastUptTS = DateTime.Now;
                PFSL.TripID = Convert.ToInt64((Item).GetDataKeyValue("TripID"));
                if (Item.GetDataKeyValue("SimulatorLog") != null && !string.IsNullOrEmpty(Item.GetDataKeyValue("SimulatorLog").ToString()) && (Convert.ToString(Item.GetDataKeyValue("SimulatorLog")) != "0"))
                {
                    PFSL.State = FlightPak.Web.PostflightService.TripEntityState.Modified;
                    PFSL.SimulatorID = Convert.ToInt64(Item.GetDataKeyValue("SimulatorLog").ToString());
                }
                else
                    PFSL.State = FlightPak.Web.PostflightService.TripEntityState.Added;

                // Fix for #UW-1207 
                // Issue Desc : When the entry is logged from Crew Calendar Entry to Other Crew Duty log, below fields are not getting updated in Other Crew Duty log.
                // Session Date, Aircraft Duty Type and Duty
                PFSL.SessionDT = Convert.ToDateTime((Item).GetDataKeyValue("StartDate"));
                if (Item.GetDataKeyValue("FleetID") != null && !string.IsNullOrEmpty(Item.GetDataKeyValue("FleetID").ToString()))
                {
                    Int64 _fleetId = Convert.ToInt64((Item).GetDataKeyValue("FleetID"));

                    // Get Aircraft Details by Fleet ID
                    using (FlightPakMasterService.MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                        var objRetVal = masterService.GetFleetProfileList();
                        if (objRetVal.ReturnFlag)
                        {
                            Fleetlist = objRetVal.EntityList.Where(x => x.FleetID == _fleetId && x.IsDeleted == false).ToList();

                            if (Fleetlist != null && Fleetlist.Count > 0)
                            {
                                PFSL.AircraftID = Fleetlist[0].AircraftID;
                            }
                        }
                    }
                }

                if ((PFSL.ArrivalDTTMLocal != null && PFSL.ArrivalDTTMLocal.HasValue) && (PFSL.DepartureDTTMLocal != null && PFSL.DepartureDTTMLocal.HasValue))
                {
                    decimal dutyHours = 0;
                    dutyHours = System.Math.Round(Convert.ToDecimal(PFSL.ArrivalDTTMLocal.Value.Subtract(PFSL.DepartureDTTMLocal.Value).TotalHours), 1);
                    if (dutyHours < 0)
                        dutyHours = 0;

                    PFSL.DutyHours = dutyHours;
                }

                return PFSL;
            }

        }

        private PreflightService.PreflightMain Getitems(Int64 CrewID, Int64 PreviousNumber, Int64 TripID, Int64 LegID, Int64 CrewlistID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID, PreviousNumber, TripID, LegID, CrewlistID))
            {
                try
                {
                    Session["chkCrewConflicts"] = "true";
                    Int64 homebaseid = Convert.ToInt64(hdnHomeBase.Value);
                    DateTime StartDate, EndDate;
                    if (!string.IsNullOrEmpty(hdndefaultSDate.Value) && (Convert.ToString(hdndefaultSDate.Value) == tbStartDate.Text) && hdnSaveFlag.Value != "Update")
                    {
                        StartDate = Convert.ToDateTime(tbStartDate.Text.ToString());
                        StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                        StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));
                        Session["chkCrewsdate"] = StartDate;

                        EndDate = Convert.ToDateTime(tbEndDate.Text.ToString());
                        EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                        EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                        Session["chkCrewedate"] = EndDate;
                    }
                    else
                    {
                        StartDate = Convert.ToDateTime(tbStartDate.Text.ToString(), DTF);
                        StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                        StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));
                        Session["chkCrewsdate"] = StartDate;

                        EndDate = Convert.ToDateTime(tbEndDate.Text.ToString(), DTF);
                        EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                        EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                        Session["chkCrewedate"] = EndDate;
                    }
                    Int64 icaoid = Convert.ToInt64(hdnICAO.Value);
                    string duty = Convert.ToString(tbDuty.Text);
                    string notes = Convert.ToString(tbComment.Text);
                    string notesec = Convert.ToString(tbNotes.Text);

                    Int64 Clientid = Convert.ToInt64("0");
                    if (!string.IsNullOrEmpty(hdnClientCode.Value))
                        Clientid = Convert.ToInt64(hdnClientCode.Value);

                    Int64 fleetid = Convert.ToInt64("0");
                    //if (!string.IsNullOrEmpty(Convert.ToString(tbTailNumber.Text)))
                    //{
                    //    using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                    //    {
                    //        var objRetVal = ObjService.GetFleetProfileList().EntityList.Where(x => x.TailNum.Trim().ToUpper() == (tbTailNumber.Text.Trim().ToUpper())).ToList();
                    //        hdnFleetID.Value = ((FlightPak.Web.FlightPakMasterService.Fleet)objRetVal[0]).FleetID.ToString();
                    //    }
                    //}
                    if (!string.IsNullOrEmpty(hdnFleetID.Value))
                        fleetid = Convert.ToInt64(hdnFleetID.Value);

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Trip = new PreflightMain();
                        Trip.CrewID = CrewID;
                        if (fleetid == 0)
                            Trip.FleetID = null;
                        else
                            Trip.FleetID = fleetid;
                        //12/26/2012 changing notes  to trip description
                        Trip.TripDescription = notes;
                        Trip.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                        Trip.RecordType = "C";
                        Trip.PreviousNUM = PreviousNumber;
                        Trip.IsDeleted = false;
                        Trip.CrewCalendarNotes = notesec;
                        if (Clientid == 0)
                            Trip.ClientID = null;
                        else
                            Trip.ClientID = Clientid;
                        Trip.LastUpdUID = UserPrincipal.Identity._name;
                        Trip.LastUpdTS = System.DateTime.UtcNow;

                        PreflightLeg Leg = new PreflightLeg();
                        Leg.LegNUM = Convert.ToInt64("1");
                        string timebase = FilterOptions.TimeBase.ToString();

                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            if (timebase.ToUpper() == "LOCAL")
                            {
                                Leg.DepartureDTTMLocal = StartDate;
                                Leg.ArrivalDTTMLocal = EndDate;
                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, true, true);
                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, true, true);
                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), StartDate, false, false);
                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), EndDate, false, false);
                            }
                            else if (timebase.ToUpper() == "UTC")
                            {
                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, false, false);
                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, false, false);
                                Leg.DepartureGreenwichDTTM = StartDate;
                                Leg.ArrivalGreenwichDTTM = EndDate;
                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), StartDate, false, false);
                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), EndDate, false, false);
                            }
                            else
                            {
                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, false, false);
                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, false, false);
                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, true, true);
                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, true, true);
                                Leg.HomeDepartureDTTM = StartDate;
                                Leg.HomeArrivalDTTM = EndDate;
                            }

                        }
                        Leg.DepartICAOID = icaoid;
                        Leg.ArriveICAOID = icaoid;
                        Leg.DutyTYPE = duty;
                        Leg.IsDeleted = false;

                        if (Clientid == 0)
                            Leg.ClientID = null;
                        else
                            Leg.ClientID = Clientid;
                        Leg.LastUpdUID = UserPrincipal.Identity._name;
                        Leg.LastUpdTS = System.DateTime.UtcNow;

                        PreflightCrewList Crew = new PreflightCrewList();
                        Crew.CrewID = CrewID;

                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = ObjService.GetCrewList().EntityList.Where(x => x.CrewID == CrewID).ToList();
                            Crew.CrewFirstName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).FirstName.ToString();
                            Crew.CrewLastName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).LastName.ToString();
                            Crew.CrewMiddleName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal[0]).MiddleInitial.ToString();
                        }

                        Crew.IsDeleted = false;
                        Crew.LastUpdUID = UserPrincipal.Identity._name;
                        Crew.LastUpdTS = System.DateTime.UtcNow;

                        if (hdnSaveFlag.Value == "Update")
                        {
                            Trip.TripID = TripID;
                            Trip.TripNUM = Convert.ToInt64(hdntripnum.Value);//We need to get this From the Get SP
                            Trip.State = TripEntityState.Modified;

                            Leg.LegID = LegID;
                            Leg.TripID = TripID;
                            Leg.TripNUM = Convert.ToInt64(hdntripnum.Value);
                            Leg.State = TripEntityState.Modified;

                            Crew.PreflightCrewListID = CrewlistID;
                            Crew.LegID = LegID;
                            Crew.State = TripEntityState.Modified;
                        }
                        else
                        {
                            Trip.State = TripEntityState.Added;
                            Leg.State = TripEntityState.Added;
                            Crew.State = TripEntityState.Added;
                        }
                        Leg.PreflightCrewLists = new List<PreflightCrewList>();
                        Leg.PreflightCrewLists.Add(Crew);
                        Trip.PreflightLegs = new List<PreflightLeg>();
                        Trip.PreflightLegs.Add(Leg);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
            return Trip;
        }

        protected void tdGoto_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(tdGoto.Text))
                            {
                                DateTime inputDate = Convert.ToDateTime(tdGoto.Text.ToString(DateTimeInfo), DateTimeInfo);
                                if (inputDate < MinDate || inputDate > MaxDate)
                                {
                                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid Goto date range: " + tdGoto.Text + "', 200, 100); return false;});", true);
                                    tdGoto.Text = DateTime.Today.Date.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    return;
                                }

                            }
                        }
                        catch (FormatException)
                        { // Manually handled
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid Goto date format: " + tdGoto.Text + "', 200, 100); return false;});", true);
                            tdGoto.Text = DateTime.Today.Date.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return;
                        }
                        DefaultSelection();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbStartDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbStartDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbStartDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate < MinDate || inputDate > MaxDate)
                    {
                        RadAjaxManager1.ResponseScripts.Add("radalert('Invalid Start date range: " + tbStartDate.Text + "',200,100);");
                    }
                    else
                        lbcvstartdate.Text = string.Empty;
                }
                else
                {
                    lbcvstartdate.Text = System.Web.HttpUtility.HtmlEncode("Start Date is Required.");
                }
                chkMultipleCrew();
            }
            catch (FormatException)
            { //Manually handled
                RadAjaxManager1.ResponseScripts.Add("radalert('Invalid Start date format: " + tbStartDate.Text + "',200,100);");
                tbStartDate.Text = "";
            }
        }
        protected void tbEndDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbEndDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbEndDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate < MinDate || inputDate > MaxDate)
                    {
                        RadAjaxManager1.ResponseScripts.Add("radalert('Invalid End date range: " + tbEndDate.Text + "',200,100);");
                    }
                    else
                        lbcvenddate.Text = string.Empty;
                }
                else
                {
                    lbcvenddate.Text = System.Web.HttpUtility.HtmlEncode("End Date is Required.");
                }
                chkMultipleCrew();
            }
            catch (FormatException)
            { //Manually handled
                RadAjaxManager1.ResponseScripts.Add("radalert('Invalid End date format: " + tbEndDate.Text + "',200,100);");
                tbEndDate.Text = "";
            }
        }

        protected void chkMultipleCrew()
        {
            string multipleCrew = hdnMultiCrew.Value;

            if (!string.IsNullOrEmpty(multipleCrew))
            {
                if (multipleCrew.Split(',').Length > 1)
                {
                    lblMultiCrew.Attributes.Add("style", "display:block;");
                    lblMultiCrew.Attributes.Add("style", "Color:Red;");
                }
                else
                {
                    lblMultiCrew.Attributes.Add("style", "display:none;");
                }
            }
            else
            {
                lblMultiCrew.Attributes.Add("style", "display:none;");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!isInsert && !isEdit)
            {
                GridEnable(true,true,true);
            }
        }
    }
}
