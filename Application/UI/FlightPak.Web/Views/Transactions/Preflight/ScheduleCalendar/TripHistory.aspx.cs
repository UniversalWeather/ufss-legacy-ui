﻿using FlightPak.Web.CalculationService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class TripHistory : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
        protected void Page_Load(object sender, EventArgs e)
        {
            #region This must be visible only for Preflight
            TableLogisticHistory.Visible = false;
            this.divHistory.Visible = false;
            #endregion

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Int64 TripID = 0;
                    Int64 CustomerId = 0;

                    if (Request.QueryString["TripID"] != null)
                    {
                        TripID = Int64.TryParse(Request.QueryString["TripID"].ToString(), out TripID) ? TripID : 0;
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            TableLogisticHistory.Visible = true;
                            CustomerId = UserPrincipal.Identity._customerID;
                            StringBuilder tripSheetBuilder = new StringBuilder();
                            this.divHistory.Visible = true;
                            var ObjRetval = PrefSvc.GetHistory(CustomerId, TripID);
                            if (ObjRetval.ReturnFlag)
                            {
                                var historyList = ObjRetval.EntityList;
                                StringBuilder historybuilder = new StringBuilder();

                                foreach (PreflightTripHistory tripHistory in historyList)
                                {
                                    historybuilder.Append("Last Updated user");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    historybuilder.Append(tripHistory.LastUpdUID.Trim());
                                    historybuilder.Append("<br>");

                                    historybuilder.Append("Last Updated Time");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    DateTime LastModifiedUTCDate = DateTime.Now;
                                    if (UserPrincipal.Identity._airportId != null)
                                    {
                                        using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                                        {
                                            LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)tripHistory.LastUpdTS, false, false);
                                        }
                                    }
                                    else
                                        LastModifiedUTCDate = (DateTime)tripHistory.LastUpdTS;

                                    historybuilder.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", LastModifiedUTCDate));
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("Revision No.");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    historybuilder.Append(tripHistory.RevisionNumber);
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    if (tripHistory.HistoryDescription != null)
                                        historybuilder.Append(tripHistory.HistoryDescription.Replace(Environment.NewLine, "<br>"));
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    divHistory.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));

                                    break;
                                }
                            }

                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

    }
}