﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Legend.aspx.cs" ClientIDMode="AutoID"
    Inherits="FlightPak.Web.Views.Transactions.PreFlight.ScheduleCalendar.Legend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Duty Color Legend</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function CloseWindow() {
                window.close();
                return false;
            }

            function ShowReports(radWin, ReportFormat) {
                var ReportName = "RptPREColorLegend";
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;

                return true;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dgLegend">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgLegend" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table width="100%" align="center" class="box1">
        <tr>
            <td colspan="3" height="20px">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                <telerik:RadGrid ID="dgLegend" runat="server" OnItemDataBound="dgLegend_ItemDataBound"
                    OnNeedDataSource="dgLegend_BindData" AllowSorting="true" AutoGenerateColumns="true"
                    PageSize="1000" Height="280px" AllowPaging="true" Width="500px" AllowFilteringByColumn="false">
                    <PagerStyle Visible="false" />
                    <MasterTableView ClientDataKeyNames="Type, Code, Description" DataKeyNames="Type, Code, Description"
                        CommandItemDisplay="Bottom" Width="100%" PageSize="1000" ShowHeader="true" AllowFilteringByColumn="false">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Type" HeaderText="Type of Duty" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" AutoPostBackOnFilter="false"
                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true" EnableVirtualScrollPaging="false">
                        </Scrolling>
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false" />
                </telerik:RadGrid>
            </td>
            <td>
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" height="15px">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Button runat="server" ID="btnHtmlReport" Text="HTML Report" CssClass="button"
                    OnClientClick="javascript:ShowReports('','MHTML');" OnClick="btnHtmlReport_Click"
                    title="Export to HTML" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="button" 
                    OnClientClick="javascript:CloseWindow();return false;" />
            </td>
        </tr>
        <tr>
            <td colspan="3" height="15px">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnReportName" runat="server" />
    <asp:HiddenField ID="hdnReportFormat" runat="server" />
    <asp:HiddenField ID="hdnReportParameters" runat="server" />
    </form>
</body>
</html>
