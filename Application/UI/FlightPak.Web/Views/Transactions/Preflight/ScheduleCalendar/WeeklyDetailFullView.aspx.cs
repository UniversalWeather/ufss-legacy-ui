﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using FlightPak.Common;
using FlightPak.Web.CommonService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{

    public partial class WeeklyDetailFullView : ScheduleCalendarBase
    {
        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty; 

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;

                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;

                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                        radDetail.Checked = true;
                        radFleet.Checked = false;
                        radCrew.Checked = false;

                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainContent");
                        UserControl ucSCMenu = (UserControl)contentPlaceHolder.FindControl("TabSchedulingCalendar");
                        ucSCMenu.Visible = false;
                        Master.FindControl("logo").Visible = false;
                        Master.FindControl("Combo").Visible = false;
                        Master.FindControl("menu").Visible = false;
                        Master.FindControl("globalNav").Visible = false;
                        Master.FindControl("footer").Visible = false;
                       /* if (!string.IsNullOrEmpty(Request.QueryString["width"]))
                        {

                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["height"]))
                        {
                            int scrHeight = Convert.ToInt32(Request.QueryString["height"]);
                            RadSplitter1.Height = scrHeight - scrHeight / 11;
                            LeftPane.Height = scrHeight - scrHeight / 11;
                            RadPanelBar1.Height = scrHeight - (scrHeight / 11) - 75;
                            dgWeeklyDetail.Height = scrHeight - (scrHeight / 11) - 100;                            
                        }*/
                        if (DefaultView == ModuleNameConstants.Preflight.WeeklyDetail)
                        {
                            ChkDefaultView.Checked = true;
                        }

                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.WeeklyDetail)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }

                            var selectedDay = DateTime.Today;
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }

                            StartDate = selectedDay;
                            EndDate = GetWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);


                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            Session.Remove("SCWeeklyDetailNextCount");
                            Session.Remove("SCWeeklyDetailPreviousCount");
                            Session.Remove("SCWeeklyDetailLastCount");
                            Session.Remove("SCWeeklyDetailFirstCount");

                            loadTree(false);
                        }
                        else
                        {
                            CheckValidDate();

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }
        /// <summary>
        /// To Validate Date 
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {

            StartDate = (DateTime)Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            EndDate = GetWeekEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

            // avoid duplicate calls for loadTree();
            string id = Page.Request.Params["__EVENTTARGET"];
            if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
            {
                loadTree(false);
            }
            return IsDateCheck;
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                        StartDate = DateTime.Today;
                        EndDate = GetWeekEndDate(StartDate);

                        lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        Session["SCSelectedDay"] = StartDate;

                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetWeekEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                //fleet trips list
                if (!Page.IsPostBack)
                {
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyDetail);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();                    

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                    }                    
                    BindFleetTree(Fleetlst);                                        
                }

                if (radDetail.Checked)
                {

                    using (var preflightServiceClient = new PreflightServiceClient())
                    {
                        radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyDetail;
                        Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyDetail);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CrewIDs = FleetCrew[1];

                            if (!string.IsNullOrEmpty(FleetCrew[1]))
                            {
                                string[] Crewids = FleetCrew[1].Split(',');
                                foreach (string cIDs in Crewids)
                                {
                                    string[] ParentChild = cIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                }
                            }
                        }

                    // check if Sunday/ Saturday is selected as First day / last day for the view...
                        if (IsSave)
                        {
                            var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                            //var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);

                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyDetail, selectedFleetIDs, Crewlst, null, false);
                            //BindWeeklyDetailGrid(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions);
                        }
                        else
                        {                            
                            if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                            {
                                string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                                var FleetIDs = FleetCrew[0];
                                var CrewIDs = FleetCrew[1];

                                if (!string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0)
                                {
                                    if (FleetTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0)
                                    {
                                        FleetTreeView.CheckAllNodes();
                                    }
                                }

                                Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();                                

                                if (!string.IsNullOrEmpty(FleetCrew[0]))
                                {
                                    string[] Fleetids = FleetCrew[0].Split(',');
                                    foreach (string fIDs in Fleetids)
                                    {
                                        string[] ParentChild = fIDs.Split('$');
                                        if (ParentChild.Length > 1)
                                            Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                        else
                                            Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                    }
                                }
                                else
                                    Fleetlst = GetCheckedTreeNodes(FleetTreeView);                                
                            }

                            if (FleetTreeView.CheckedNodes.Count == 0 && FleetTreeView.Visible)
                            {
                                FleetTreeView.CheckAllNodes();
                            }
                        }
                        var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                        BindWeeklyDetailGrid(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions);
                    }

                    RadPanelBar2.FindItemByText("Fleet").Visible = true;
                    RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                    RadPanelBar2.FindItemByText("Crew").Visible = false;
                    RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                }
                else if (radFleet.Checked)
                {
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyFleet;
                    Response.Redirect("SchedulingCalendar.aspx");
                }
                else if (radCrew.Checked)
                {
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyCrew;
                    Response.Redirect("WeeklyCrew.aspx");
                }
                else
                {
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyMain;
                    Response.Redirect("WeeklyMain.aspx");
                }


            }
            RightPane.Collapsed = true;
        }

        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;
                        //Changes done for the Bug ID - 5828
                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;

                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        // ON click of Apply button...
        protected void btnApply_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;

                            loadTree(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }
        }

        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, DateTime weekStartDate, DateTime weekEndDate)
        {
            Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions))
            {

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();
                // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList();                                        
                    groupedLegs = groupedLegs.OrderBy(x => x.DepartureDisplayTime).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();//    
                    
                    decimal TotalETE = 0;
                    foreach (var leg in groupedLegs)
                    {
                        if (displayOptions.WeeklyDetail.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, leg, appointment);
                                appointmentCollection.Add(appointment);

                            }
                            else if (leg.LegNUM == 0) // RON appointment
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                               // SetFleetAppointment(calendarData, timeBase, displayOptions, leg, appointment);
                               //  appointmentCollection.Add(appointment);
                            }
                        }
                        else // No first leg last leg selected
                        {
                            if ((leg.RecordType == "M"  || leg.RecordType == "C") && leg.TailNum.ToUpper() != "DUMMY"  && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;                                
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);                               
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i+1).Date.AddTicks(-1): EndDate;

                                    if (leg.DepartureDisplayTime >= weekStartDate && leg.DepartureDisplayTime <= weekEndDate)
                                    {
                                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();

                                        SetFleetAppointment(calendarData, timeBase, displayOptions, leg, appointment);
                                        appointmentCollection.Add(appointment);                                        
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                if (leg.DepartureDisplayTime >= weekStartDate && leg.DepartureDisplayTime <= weekEndDate)
                                {
                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    //SetFleetAppointment(calendarData, timeBase, displayOptions, leg, appointment);

                                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, leg, appointment))
                                    {
                                        appointment = SetFleetCommonProperties(calendarData, timeBase, leg, appointment);
                                        //appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                                        //appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                                        appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                                        appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                                        SetDepartureAndArrivalInfo(displayOptions, leg, appointment);
                                        string totalhrs = string.Empty;
                                        if (appointment.IsRONAppointment)
                                        {
                                            appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns                    
                                        }
                                        else
                                        {
                                            DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                            DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                            TimeSpan span = EndDate.Subtract(StartDate);

                                            totalhrs = Convert.ToString(span.TotalHours);
                                            if (StartDate.TimeOfDay.ToString() == "00:00:00.0000001" && EndDate.TimeOfDay.ToString() != "23:59:59.9999999")
                                            {
                                                totalhrs = Convert.ToString(Math.Round(span.TotalHours, 2));
                                            }
                                            else
                                            {
                                                totalhrs = Convert.ToString(span.TotalHours);
                                            }

                                            if (!string.IsNullOrEmpty(totalhrs) && totalhrs.Contains("."))
                                            {
                                                string[] res = totalhrs.Split('.');
                                                if (!string.IsNullOrEmpty(res[1]) && res[1].Length > 1)
                                                    res[1] = res[1].Substring(1, 1);
                                                totalhrs = res[0] + "." + res[1];
                                            }
                                            else
                                                totalhrs = (totalhrs == "0") ? "" : totalhrs + ".0";
                                        }
                                        //appointment.CumulativeETE = span.TotalHours.ToString("n2");// string.Format("{0.0}", span.ToString());
                                        TotalETE += (!string.IsNullOrEmpty(totalhrs))?Convert.ToDecimal(totalhrs):0;
                                        //appointment.CumulativeETE = Convert.ToString(TotalETE); // (leg.TailNum.ToUpper() != "DUMMY") ? totalhrs : "";
                                        //appointment.ElapseTM = (leg.TailNum.ToUpper() != "DUMMY") ? totalhrs : "";
                                        appointment.Description = leg.Description;
                                    }

                                    appointmentCollection.Add(appointment);
                                }
                            }
                        }
                    }
                }
            }
            return appointmentCollection;
        }

        public void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, tripLeg, appointment))
            {
                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);
                //appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                //appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                SetDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                string totalhrs = string.Empty;
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns                    
                }
                else
                {                   
                    DateTime StartDate = Convert.ToDateTime(tripLeg.DepartureDisplayTime);
                    DateTime EndDate = Convert.ToDateTime(tripLeg.ArrivalDisplayTime);
                    TimeSpan span = EndDate.Subtract(StartDate);

                    totalhrs = Convert.ToString(span.TotalHours);
                    if (StartDate.TimeOfDay.ToString() == "00:00:00.0000001" && EndDate.TimeOfDay.ToString() != "23:59:59.9999999")
                    {
                        totalhrs = Convert.ToString(Math.Round(span.TotalHours, 2));
                    }
                    else
                    {
                        totalhrs = Convert.ToString(span.TotalHours);
                    }

                    if (!string.IsNullOrEmpty(totalhrs) && totalhrs.Contains("."))
                    {
                        string[] res = totalhrs.Split('.');
                        if (!string.IsNullOrEmpty(res[1]) && res[1].Length > 1)
                            res[1] = res[1].Substring(1, 1);
                        totalhrs = res[0] + "." + res[1];
                    }
                    else
                        totalhrs = (totalhrs == "0") ? "" : totalhrs + ".0";
                }
                //appointment.CumulativeETE = span.TotalHours.ToString("n2");// string.Format("{0.0}", span.ToString());
                //appointment.CumulativeETE = (tripLeg.TailNum.ToUpper() != "DUMMY")?totalhrs:"";
                //appointment.ElapseTM = (tripLeg.TailNum.ToUpper() != "DUMMY")?totalhrs:"";
                appointment.Description = tripLeg.Description;
            }
        }

        private void SetDepartureAndArrivalInfo(DisplayOptions displayOptions, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {
                if (appointment.ShowAllDetails)
                {
                    switch (displayOptions.WeeklyDetail.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;

                    }
                }
                else // show based on trip privacy settings + display option settings
                {
                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (displayOptions.WeeklyDetail.DepartArriveInfo)
                        {
                            case DepartArriveInfo.ICAO:
                                appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;

                        }
                    }
                    else
                    {
                        appointment.DepartureICAOID = string.Empty;
                        appointment.ArrivalICAOID = string.Empty;
                    }

                    if (!PrivateTrip.IsShowArrivalDepartTime)
                    {
                        appointment.StartDateTime = string.Empty;
                        appointment.EndDateTime = string.Empty;
                    }
                }
            }


        }

        protected void radCrew_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;
                        Response.Redirect("WeeklyCrew.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }


        }

        protected void radFleet_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                        Response.Redirect("SchedulingCalendar.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        protected void radDetail_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        protected void radMain_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;
                        Response.Redirect("WeeklyMain.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        private void BindWeeklyDetailGrid(PreflightServiceClient preflightServiceClient, DateTime weekStartDate, DateTime weekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, weekStartDate, weekEndDate, filterCriteria, displayOptions))
            {

                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);
                var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                if (inputFromFleetTree.Count > 0)
                {
                    Collection<string> InputFromFleetTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                    {
                        InputFromFleetTree.Add(s.ChildNodeId);
                    }
                    var calendarData = preflightServiceClient.GetFleetCalendarData(weekStartDate, weekEndDate, serviceFilterCriteria, InputFromFleetTree.ToList(), true, false, false);
                    //calendarData.EntityList = calendarData.EntityList.Where(x => x.DepartureDisplayTime >= weekStartDate).ToList();
                    calendarData.EntityList = calendarData.EntityList.ToList();
                    //Trip Departure Date is in between the selected week start & End Date, Then only the Record Needs to Display
                    //if (filterCriteria.TimeBase == TimeBase.Local)
                    //    calendarData.EntityList = calendarData.EntityList.Where(x => x.DepartureDTTMLocal >= weekStartDate).ToList();
                    //else if (filterCriteria.TimeBase == TimeBase.UTC)
                    //    calendarData.EntityList = calendarData.EntityList.Where(x => x.DepartureGreenwichDTTM >= weekStartDate).ToList();
                    //else
                    //    calendarData.EntityList = calendarData.EntityList.Where(x => x.HomeDepartureDTTM >= weekStartDate).ToList();

                    // by default REcordType, T, M and C with tailNum will be displayed...
                    calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                    var appointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, weekStartDate, weekEndDate);
                    appointments.OrderBy(x => x.TripDate).ToList();
                    // build dummy appointments to load in grid for selected tree view...

                    List<string> tailNumsFromTree = new List<string>();
                    IList<RadTreeNode> nodeCollection = FleetTreeView.CheckedNodes;
                    if (nodeCollection.Count > 0)
                    {
                        nodeCollection.RemoveAt(0);
                        foreach (RadTreeNode node in nodeCollection)
                        {
                            string text = node.Text;
                            int delimiterIndex = text.IndexOf(" - ");
                            if (delimiterIndex != -1)
                            {
                                tailNumsFromTree.Add(text.Substring(0, delimiterIndex));
                            }
                        }
                    }

                    if (!displayOptions.WeeklyDetail.ActivityOnly)
                    {
                        var tailNumsFromAppointments = appointments.Select(x => x.TailNum).Distinct().ToList();
                        // get tailNums that dont have any trip / entry...
                        var tailNumsWithoutActivity = tailNumsFromTree.Except(tailNumsFromAppointments).ToList();

                        if (tailNumsWithoutActivity.Count > 0)
                        {
                            if (tailNumsWithoutActivity.Contains(ModuleNameConstants.Preflight.FleetMode))
                            {
                                tailNumsFromTree.RemoveAt(0); //"Fleet"
                            }
                            foreach (var tail in tailNumsWithoutActivity)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment dummyAppointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                dummyAppointment.TailNum = tail;
                                dummyAppointment.LastUpdatedTime = string.Empty;
                                dummyAppointment.FlightNum = string.Empty;
                                dummyAppointment.FlightPurpose = string.Empty;
                                // for handling the context menu for dummy data.
                                dummyAppointment.RecordType = null;
                                // dummyAppointment.ShowAllDetails = true;
                                appointments.Add(dummyAppointment);
                            }
                        }
                    }

                    var sortOption = (DayWeeklyDetailSortOption)displayOptions.WeeklyDetail.DayWeeklyDetailSortOption;


                    if (sortOption.ToString() == DayWeeklyDetailSortOption.TailNumber.ToString())
                    { // sort by tailNumber primarily and then with departure datetime.
                        appointments = new Collection<Entities.SchedulingCalendar.Appointment>(appointments.OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList());

                    }
                    else // departureDate
                    {
                        appointments = new Collection<Entities.SchedulingCalendar.Appointment>(appointments.OrderBy(x => x.StartDate).ToList());
                    }
                    dgWeeklyDetail.DataSource = appointments;
                }
                else
                {
                    dgWeeklyDetail.DataSource = new Collection<Entities.SchedulingCalendar.Appointment>();
                }

                // Start: Hide the columns based on the display options

                dgWeeklyDetail.Columns.FindByUniqueName("TripStatus").Visible = displayOptions.WeeklyDetail.ShowTripStatus;

                dgWeeklyDetail.Columns.FindByUniqueName("TripStatus").Visible = displayOptions.WeeklyDetail.ShowTripStatus;

                dgWeeklyDetail.Columns.FindByUniqueName("FlightNumber").Visible = displayOptions.WeeklyDetail.FlightNumber;
                dgWeeklyDetail.Columns.FindByUniqueName("LegPurpose").Visible = displayOptions.WeeklyDetail.LegPurpose;
                dgWeeklyDetail.Columns.FindByUniqueName("SeatsAvailable").Visible = displayOptions.WeeklyDetail.SeatsAvailable;
                dgWeeklyDetail.Columns.FindByUniqueName("PAXCount").Visible = displayOptions.WeeklyDetail.PaxCount;
                dgWeeklyDetail.Columns.FindByUniqueName("PassengerFullNames").Visible = (displayOptions.WeeklyDetail.PaxFullName & displayOptions.WeeklyDetail.Pax);
                dgWeeklyDetail.Columns.FindByUniqueName("PassengerCodes").Visible = ((!displayOptions.WeeklyDetail.PaxFullName) & displayOptions.WeeklyDetail.Pax);
                dgWeeklyDetail.Columns.FindByUniqueName("TripNUM").Visible = displayOptions.WeeklyDetail.ShowTripNumber;


                // right
                dgWeeklyDetail.Columns.FindByUniqueName("Requestor").Visible = displayOptions.WeeklyDetail.Requestor;
                dgWeeklyDetail.Columns.FindByUniqueName("Department").Visible = displayOptions.WeeklyDetail.Department;
                dgWeeklyDetail.Columns.FindByUniqueName("Authorization").Visible = displayOptions.WeeklyDetail.Authorization;
                dgWeeklyDetail.Columns.FindByUniqueName("FlightCategory").Visible = displayOptions.WeeklyDetail.FlightCategory;
                dgWeeklyDetail.Columns.FindByUniqueName("ETE").Visible = displayOptions.WeeklyDetail.ETE;
                dgWeeklyDetail.Columns.FindByUniqueName("CumulativeETE").Visible = displayOptions.WeeklyDetail.CummulativeETE;
                dgWeeklyDetail.Columns.FindByUniqueName("TotalETE").Visible = displayOptions.WeeklyDetail.TotalETE;

                dgWeeklyDetail.Columns.FindByUniqueName("CrewFullNames").Visible = (displayOptions.WeeklyDetail.CrewFullName & displayOptions.WeeklyDetail.Crew);
                dgWeeklyDetail.Columns.FindByUniqueName("CrewCodes").Visible = ((!displayOptions.WeeklyDetail.CrewFullName) & displayOptions.WeeklyDetail.Crew);

                // End: Hide the columns based on the display options
                dgWeeklyDetail.DataBind();
            }
        }

        protected void dgweeklyDetail_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            Entities.SchedulingCalendar.Appointment appt = (Entities.SchedulingCalendar.Appointment)e.Item.DataItem;


                            if (DisplayOptions.WeeklyDetail.BlackWhiteColor)
                            {
                                e.Item.BackColor = Color.White;
                                e.Item.ForeColor = Color.Black;
                            }
                            // if black and white color not selected - default is off – can only be selected if Aircraft color is not selected; 
                            else
                            {
                                if (!DisplayOptions.WeeklyDetail.AircraftColors)
                                {
                                    if (appt.RecordType == "T")
                                    {
                                        if (appt.IsRONAppointment)
                                        {

                                            // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                            if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(appt.AircraftDutyCD))
                                            {
                                                var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == appt.AircraftDutyCD.Trim()).FirstOrDefault();
                                                if (dutyCode != null)
                                                {
                                                    e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                    e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                }

                                            }
                                            else// crew view => CorporateCrewTreeCount > 0
                                            {
                                                if (CrewDutyTypes.Count > 0)
                                                {
                                                    var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == appt.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            SetFlightCategoryColorForDetailGrid(e, e.Item);
                                        }
                                    }
                                    else if (appt.RecordType == "M")
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                        SetAircraftDutyColorForDetailGrid(e, e.Item);
                                    }
                                    else if (appt.RecordType == "C")
                                    {
                                        // set crew duty type color...
                                        SetCrewDutyColorForDetailGrid(e, e.Item);
                                    }
                                }
                                else if (DisplayOptions.WeeklyDetail.AircraftColors && appt.TailNum == null && appt.RecordType == "C")
                                {
                                    SetCrewDutyColorForDetailGrid(e, e.Item);
                                }
                                else // AircraftColors checked
                                {
                                    if (appt.AircraftForeColor != null && appt.AircraftBackColor != null)
                                    {
                                        e.Item.BackColor = Color.FromName(appt.AircraftBackColor);
                                        e.Item.ForeColor = Color.FromName(appt.AircraftForeColor);
                                    }
                                }
                            }

                            if (!appt.ShowAllDetails)
                            {
                                var options = DisplayOptions.WeeklyDetail;
                                item["LastUpdatedTime"].Text = " ";

                                if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["FlightNumber"].Text = " ";

                                }

                                if (PrivateTrip.IsShowRequestor && options.Requestor)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["Requestor"].Text = " ";

                                }

                                if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["ETE"].Text = " ";
                                    item["CumulativeETE"].Text = " ";

                                }


                                if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["LegPurpose"].Text = " ";

                                }

                                if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["FlightCategory"].Text = " ";

                                }

                                if (PrivateTrip.IsShowDepartment && options.Department)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["Department"].Text = " ";

                                }

                                if (PrivateTrip.IsShowAuthorization && options.Authorization)
                                { //values set in aspx
                                }
                                else
                                {
                                    item["Authorization"].Text = " ";

                                }

                                if (PrivateTrip.IsShowCrew && options.Crew)
                                { //values set in aspx
                                }
                                else
                                {
                                    item["CrewCodes"].Text = " ";
                                }

                                if (PrivateTrip.IsShowCrew && options.CrewFullName)
                                { //values set in aspx
                                }
                                else
                                {
                                    item["CrewFullNames"].Text = " ";
                                }


                                if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                                { //values set in aspx
                                }
                                else
                                {
                                    item["PAXCount"].Text = " ";
                                }

                                if (PrivateTrip.IsShowPaxCount && options.Pax)
                                { //values set in aspx
                                }
                                else
                                {
                                    item["PassengerCodes"].Text = " ";
                                }

                                if (PrivateTrip.IsShowPaxCount && options.PaxFullName)
                                { //values set in aspx
                                }
                                else
                                {
                                    item["PassengerFullNames"].Text = " ";
                                }


                                if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                                {
                                }
                                else
                                {
                                    item["SeatsAvailable"].Text = " ";
                                }


                                item["FuelLoad"].Text = " ";
                                item["OutboundInstructions"].Text = " ";

                            }
                        }
                        //else if (appt.RecordType == "M")
                        //{
                        //    // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                        //    SetAircraftDutyColorForDetailGrid(e, e.Item);
                        //}
                        //else if (appt.RecordType == "C")
                        //{
                        //    // set crew duty type color...
                        //    SetCrewDutyColorForDetailGrid(e, e.Item);
                        //}





                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        protected void RadWeeklyDetailGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var clickedIndex = Request.Form["radGridClickedRowIndex"];
                        Session["CurrentPreFlightTrip"] = null;
                        if (e.Item.Value == string.Empty)
                        {
                            return;
                        }

                        if (string.IsNullOrEmpty(clickedIndex))// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            if (e.Item.Text == "Preflight" || e.Item.Text == "Add New Trip")
                            {

                                /// 12/11/2012
                                ///Changes done for displaying the homebase of login user 
                                PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                Trip.EstDepartureDT = StartDate;
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = BaseDescription;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;

                                if (e.Item.Text == "Add New Trip")
                                {
                                    Response.Redirect(e.Item.Value);
                                }
                                else if (e.Item.Text == "Preflight" && Trip.TripNUM != null && Trip.TripNUM != 0)
                                {
                                    Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                                }
                                else
                                {
                                    Session["CurrentPreFlightTrip"] = null;
                                    Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                    Response.Redirect(e.Item.Value);
                                }
                            }
                            else if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                            {// Fleet calendar entry

                                RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "UWA Services")
                            {
                                return;
                            }
                            else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                            }
                            else
                            {
                                Response.Redirect(e.Item.Value);
                            }
                        }

                        //get trip by tripId
                        var selectedTripId = dgWeeklyDetail.MasterTableView.Items[clickedIndex].GetDataKeyValue("TripId");
                        var selectedTailNum = dgWeeklyDetail.MasterTableView.Items[clickedIndex].GetDataKeyValue("TailNum");
                        var selectedDate = dgWeeklyDetail.MasterTableView.Items[clickedIndex].GetDataKeyValue("StartDate");
                        var selectedLegNum = dgWeeklyDetail.MasterTableView.Items[clickedIndex].GetDataKeyValue("LegNum");
                        if (e.Item.Text == "Add New Trip")// On click on calendar entries
                        {
                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                            Trip.EstDepartureDT = Convert.ToDateTime(selectedDate);
                            if (selectedTailNum != null)
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of Fleet in preflight 
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                PreflightService.Aircraft AircraftSample = new PreflightService.Aircraft();
                                Int64 HomebaseAirportId = 0;
                                if (fleetsample.FleetID != null)
                                {
                                    Int64 fleetHomebaseId = 0;
                                    Int64 fleetId = 0;
                                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var FleetByTailNumber = FPKMasterService.GetFleetByTailNumber(selectedTailNum.ToString().Trim()).EntityInfo;
                                        if (FleetByTailNumber != null)
                                        {
                                            fleetId = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)FleetByTailNumber).FleetID;
                                            fleetsample.TailNum = selectedTailNum.ToString();
                                            fleetsample.FleetID = fleetId;
                                        }
                                        var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                        if (FleetList.Count > 0)
                                        {
                                            GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                            if (FleetCatalogEntity.HomebaseID != null)
                                            {
                                                fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                AircraftSample.AircraftDescription = FleetCatalogEntity.AircraftDescription;
                                                AircraftSample.AircraftCD = FleetCatalogEntity.AirCraft_AircraftCD;
                                                AircraftSample.AircraftID = Convert.ToInt64(FleetCatalogEntity.AircraftID);
                                                Trip.HomebaseID = fleetHomebaseId;
                                                Trip.Fleet = fleetsample;
                                                Trip.Aircraft = AircraftSample;
                                            }
                                        }
                                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                        if (CompanyList.Count > 0)
                                        {
                                            GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                            if (CompanyCatalogEntity.HomebaseAirportID != null)
                                            {
                                                HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                HomebaseSample.BaseDescription = BaseDescription;
                                                Trip.Company = HomebaseSample;
                                            }
                                        }
                                        var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                        if (AirportList.Count > 0)
                                        {
                                            GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                            if (AirportCatalogEntity.IcaoID != null)
                                            {
                                                Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                            }
                                            if (AirportCatalogEntity.AirportID != null)
                                            {
                                                Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                            }
                                        }

                                    }
                                }
                            }

                            Session["CurrentPreFlightTrip"] = Trip;
                            Session.Remove("PreflightException");
                            Trip.State = TripEntityState.Added;
                            Trip.Mode = TripActionMode.Edit;
                            Response.Redirect(e.Item.Value);

                            /*if (Trip.TripNUM != null && Trip.TripNUM != 0)
                            {
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                                Response.Redirect(e.Item.Value);
                            }
                            else
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }*/
                        }
                        //get trip by tripId
                        Int64 tripId = Convert.ToInt64(selectedTripId);
                        if (tripId != 0)
                        {

                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                {
                                    var trip = tripInfo.EntityList[0];

                                    //Set the trip.Mode and Trip.State to NoChage for read only                    
                                    trip.Mode = TripActionMode.NoChange;
                                    trip.Mode = TripActionMode.Edit;
                                    Session["CurrentPreFlightTrip"] = trip;
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");

                                    if (e.Item.Text == "Outbound Instruction") // outbound instruction menu selected...
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        RadWindow5.NavigateUrl = e.Item.Value + "?IsCalender=1";
                                        RadWindow5.Visible = true;
                                        RadWindow5.VisibleOnPageLoad = true;
                                        RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                        return;
                                    }
                                    else if (e.Item.Text == "Leg Notes") // Leg Notes menu selected...
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        RadWindow6.NavigateUrl = e.Item.Value + "?IsCalender=1&Leg=" + selectedLegNum;
                                        RadWindow6.Visible = true;
                                        RadWindow6.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                                    {// Fleet calendar entry
                                        string tailNumber = selectedTailNum != null ? selectedTailNum.ToString() : null;
                                        RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + tailNumber + "&startDate=" + selectedDate + "&endDate=" + selectedDate; // no need to pass tailNum since it has no resource
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                                    { // Crew calendar entry
                                        RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + selectedDate + "&endDate=" + selectedDate;// no need to pass crewCode since it has no resource
                                        RadWindow4.Visible = true;
                                        RadWindow4.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else if (e.Item.Text == "Preflight")
                                    {                                        
                                        if (trip.TripNUM != null && trip.TripNUM != 0)
                                        {                                            
                                            Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                                        }
                                        else
                                        {
                                            Session["CurrentPreFlightTrip"] = null;
                                            Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                        }
                                    }
                                    else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                    {
                                        RedirectToPage(string.Format("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml&tripnum={0}&tripid={1}", Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripNUM)), Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripID))));
                                    }
                                    else
                                    {
                                        Response.Redirect(e.Item.Value);
                                    }
                                }
                            }
                        }
                        else //tripid=0, that is dummy appointment
                        {
                            if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                            {// Fleet calendar entry
                                string tailNumber = selectedTailNum != null ? selectedTailNum.ToString() : null;
                                RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + tailNumber + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Preflight")
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }
                            else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                            }
                            else
                            {
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        private void SetAircraftDutyColorForDetailGrid(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                        var appointmentAircraftDuty = appointmentItem.AircraftDutyID;
                        if (AircraftDutyTypes.Count > 0)
                        {
                            var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                            if (dutyCode != null)
                            {
                                e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        private void SetFlightCategoryColorForDetailGrid(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                        var appointmentCategory = appointmentItem.FlightCategoryID;
                        // Aircraft Color – if on display will use color set in flight category 
                        if (FlightCategories.Count > 0)
                        {
                            var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                            if (categoryCode != null)
                            {
                                e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                                e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        private void SetCrewDutyColorForDetailGrid(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                        var appointmentCategory = appointmentItem.CrewDutyType;
                        // Aircraft Color – if on display will use color set in flight category 
                        if (CrewDutyTypes.Count > 0)
                        {
                            var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                            if (categoryCode != null)
                            {
                                e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                                e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        # region "Navigation bar"
        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyDetailPreviousCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            loadTree(false);


                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCWeeklyDetailPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        protected void nextButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyDetailNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCWeeklyDetailNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyDetailLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCWeeklyDetailLastCount"] = ++count;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        protected void btnFirst_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyDetailFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            //DateTime today = DateTime.Today;
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCWeeklyDetailFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

        #endregion

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.WeeklyDetail);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyDetail);
                }
            }

        }

    }
}

