﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Data.SqlClient;
using System.Text;
using FlightPak.Web.PreflightService;
using System.Drawing;
using FlightPak.Web.UserControls;
using FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar;
using FlightPak.Web.Framework.Helpers;
using System.Collections.ObjectModel;
using FlightPak.Web.Entities.SchedulingCalendar;
using System.Web.UI.HtmlControls;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;


namespace FlightPak.Web.Views.Transactions.PreFlight.ScheduleCalendar
{
    public partial class Legend : BaseSecuredPage
    {
        protected ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLegend_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<DutyType> dutyTypesList = new List<DutyType>();
                        using (var masterCatalogServiceClient = new MasterCatalogServiceClient())
                        {

                            // aircraft Duty Types
                            var aircraftDuties = masterCatalogServiceClient.GetAircraftDuty();
                            if (aircraftDuties != null)
                            {
                                var aircraftDutyTypes = aircraftDuties.EntityList;

                                foreach (var type in aircraftDutyTypes)
                                {
                                    DutyType dutyType = new DutyType();
                                    dutyType.Type = ModuleNameConstants.Database.AircraftDuty;
                                    dutyType.Code = type.AircraftDutyCD;
                                    dutyType.Description = type.AircraftDutyDescription;
                                    dutyType.BackColor = type.BackgroundCustomColor;
                                    dutyType.Color = type.ForeGrndCustomColor;
                                    dutyTypesList.Add(dutyType);
                                }
                            }

                            // Crew duty types

                            var crewDuties = masterCatalogServiceClient.GetCrewDutyTypeList();
                            if (crewDuties != null)
                            {
                                var crewDutyTypes = crewDuties.EntityList;

                                foreach (var type in crewDutyTypes)
                                {
                                    DutyType dutyType = new DutyType();
                                    dutyType.Type = ModuleNameConstants.Database.CrewDutyType;
                                    dutyType.Code = type.DutyTypeCD;
                                    dutyType.Description = type.DutyTypesDescription;
                                    dutyType.BackColor = type.BackgroundCustomColor;
                                    dutyType.Color = type.ForeGrndCustomColor;
                                    dutyTypesList.Add(dutyType);
                                }
                            }

                            // Flight categories
                            var flightCategories = masterCatalogServiceClient.GetFlightCategoryList();
                            if (flightCategories != null)
                            {
                                var flightCategory = flightCategories.EntityList;

                                foreach (var type in flightCategory)
                                {
                                    DutyType dutyType = new DutyType();
                                    dutyType.Type = ModuleNameConstants.Database.FlightCategory;
                                    dutyType.Code = type.FlightCatagoryCD;
                                    dutyType.Description = type.FlightCatagoryDescription;
                                    dutyType.BackColor = type.BackgroundCustomColor;
                                    dutyType.Color = type.ForeGrndCustomColor;
                                    dutyTypesList.Add(dutyType);
                                }
                            }
                        }
                        dgLegend.DataSource = dutyTypesList;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Legend);
                }
            }
        }


        /// <summary>
        /// To show reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }



        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLegend_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)item["Type"];
                            TableCell cell1 = (TableCell)item["Description"];
                            if ((Session["SCAdvancedTab"] != null) && (Convert.ToString(Session["SCAdvancedTab"]) == "Planner"))
                            {
                                if (Convert.ToBoolean(Session["Planner_NonReverseColor"]) == true)
                                {
                                    cell1.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackColor"), CultureInfo.CurrentCulture));
                                    cell1.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Color"), CultureInfo.CurrentCulture));
                                }
                                else
                                {
                                    cell1.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackColor"), CultureInfo.CurrentCulture));
                                    cell1.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Color"), CultureInfo.CurrentCulture));
                                }
                            }
                            else
                            {
                                cell1.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackColor"), CultureInfo.CurrentCulture));
                                cell1.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Color"), CultureInfo.CurrentCulture));
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Legend);
                }
            }
        }

        public class DutyType
        {
            public string Type { get; set; }
            public string Code { get; set; }
            public string Description { get; set; }
            public string Color { get; set; }
            public string BackColor { get; set; }
        }

        protected void btnHtmlReport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool NonReversedColor = true;
                        if ((Session["SCAdvancedTab"] != null) && (Convert.ToString(Session["SCAdvancedTab"]) == "Planner"))
                        {
                            if ((Session["Planner_NonReverseColor"] != null) && Convert.ToBoolean(Session["Planner_NonReverseColor"]) == false)
                            {
                                NonReversedColor = false;
                            }
                        }
                        Session["PARAMETER"] = string.Format("UserCD={0};UserCustomerID={1};NonReversedColor={2}", UserPrincipal.Identity._name, UserPrincipal.Identity._customerID, NonReversedColor);                                                    
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["REPORT"] = hdnReportName.Value;
                        Response.Redirect(@"..\..\..\Reports\ReportRenderView.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Legend);
                }
            }
        }
    }
}