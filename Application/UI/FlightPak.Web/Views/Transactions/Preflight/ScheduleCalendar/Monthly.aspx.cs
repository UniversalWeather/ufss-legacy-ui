﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.UserControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Text;
using System.Globalization;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class Monthly : ScheduleCalendarBase
    {
        private int FleetTreeCount { get; set; }
        private int CrewTreeCount { get; set; }
        private string SelectedFleetCrewIDs = string.Empty;
        private string SelectedSatSunWeekNoPastWeek = string.Empty;
        bool IsDateCheck = true;
        DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;
                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        RadScheduler1.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        RadScheduler2.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        RadScheduler3.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        RadScheduler4.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        RadScheduler5.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        RadScheduler6.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        Session["SCAdvancedTab"] = CurrentDisplayOption.Monthly;
                        //Session["CurrentPreFlightTrip"] = null; 

                        // page load tasks...
                        if (!Page.IsPostBack)
                        {
                            //To reduce service call - Vishwa
                            FilterCriteria.userSettingAvailable = base.scWrapper.userSettingAvailable;
                            FilterCriteria.retriveSystemDefaultSettings = base.scWrapper.retrieveSystemDefaultSettings;
                            //End                        
                            var sessionSettings = (AdvancedFilterSettings)Session["SCUserSettings"];
                            if (sessionSettings != null)
                            {
                                var displaySession = sessionSettings.Display;
                                hdnEnableHoverDisplay.Value = displaySession.Monthly.DisplayDetailsOnHover.ToString();
                            }
                            else
                            {
                                var userSettings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(FilterCriteria.retriveSystemDefaultSettings);
                                var displaySettings = userSettings.Display;
                                hdnEnableHoverDisplay.Value = displaySettings.Monthly.DisplayDetailsOnHover.ToString();
                            }
                            
                            //hdnEnableHoverDisplay.Value = "True";
                            // check / uncheck default view check box...
                            ChkDefaultView.Checked = (DefaultView == ModuleNameConstants.Preflight.Month) ? true : false;

                            var newStartDay = new DateTime();
                            if (Session["SCSelectedDay"] == null)
                            {
                                newStartDay = GetMonthStartDate(DateTime.Today);
                                if (newStartDay.Month == DateTime.Today.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }

                                tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(DateTime.Today.Month) + " " + DateTime.Today.Year);
                                Session["SCSelectedDay"] = DateTime.Today;

                            }
                            else
                            { // from session......
                                var selectedDay = (DateTime)Session["SCSelectedDay"];
                                newStartDay = GetMonthStartDate(selectedDay);
                                if (newStartDay.Month == selectedDay.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }

                                tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                //RadDatePicker1.SelectedDate = selectedDay;

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);
                                Session["SCSelectedDay"] = selectedDay;
                            }

                            EndDate = GetMonthEndDate(StartDate);

                            Session["SCMonthStartDay"] = StartDate;                            
                            RadScheduler1.SelectedDate = StartDate;
                            RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler2.SelectedDate = StartDate.AddDays(7);
                            RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler3.SelectedDate = StartDate.AddDays(14);
                            RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler4.SelectedDate = StartDate.AddDays(21);
                            RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler5.SelectedDate = StartDate.AddDays(28);
                            RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler6.SelectedDate = StartDate.AddDays(35);
                            RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                            Session.Remove("SCMonthlyNextCount");
                            Session.Remove("SCMonthlyPreviousCount");
                            Session.Remove("SCMonthlyLastCount");
                            Session.Remove("SCMonthlyFirstCount");
                                                      

                            ////Retains the Sunday - Saturday Week check box when moved to other view
                            //if (Session["chksunsat"] != null && Convert.ToString(Session["chksunsat"]) == "true")
                            //{
                            //    chksunsat.Checked = true;
                            //    if (Session["chkNoPastWeeks"] != null && Convert.ToString(Session["chkNoPastWeeks"]) == "true")
                            //    {
                            //        chkNoPastWeeks.Checked = true;
                            //    }
                            //    SundayToSaturdayCheck();
                            //}
                            //else
                            //{
                            //    chksunsat.Checked = false;
                            //}

                            ////Retains the NoPastWeeks check box when moved to other view
                            //if (Session["chkNoPastWeeks"] != null && Convert.ToString(Session["chkNoPastWeeks"]) == "true")
                            //{
                            //    chkNoPastWeeks.Checked = true;
                            //    if (Session["chksunsat"] != null && Convert.ToString(Session["chksunsat"]) == "true")
                            //    {
                            //        chksunsat.Checked = true;
                            //    }
                            //    NoPastWeeksCheck();
                            //}
                            //else
                            //{
                            //    chkNoPastWeeks.Checked = false;
                            //}

                            // check / uncheck saturday sunday week check box...

                            SelectedSatSunWeekNoPastWeek = objFleetCrewIDs.GetUserScheduleCalenderPreferncesMonthly(ModuleNameConstants.Preflight.Month);
                            string[] IsSatSunWeekNoPastWeek = SelectedSatSunWeekNoPastWeek.Split('|');

                            if (!string.IsNullOrEmpty(IsSatSunWeekNoPastWeek[0]))
                            {
                                if (Convert.ToBoolean(IsSatSunWeekNoPastWeek[0]))
                                {
                                    chksunsat.Checked = true;
                                    SundayToSaturdayCheck();
                                }
                                else
                                    chksunsat.Checked = false;
                            }
                            else
                            {
                                chksunsat.Checked = false;
                            }


                            if (!string.IsNullOrEmpty(IsSatSunWeekNoPastWeek[1]))
                            {
                                if (Convert.ToBoolean(IsSatSunWeekNoPastWeek[1]))
                                {
                                    chkNoPastWeeks.Checked = true;
                                    NoPastWeeksCheck();
                                }
                                else
                                    chkNoPastWeeks.Checked = false;
                            }
                            else
                            {
                                chkNoPastWeeks.Checked = false;
                            }                            

                            loadTree(false);                           
                        }
                        else // On post back...
                        {
                            CheckValidDate();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        /// <summary>
        /// To Validate Date
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            var selectedDay = (DateTime)Session["SCSelectedDay"];
            var newStartDay = GetMonthStartDate(selectedDay);

            if (newStartDay.Month == selectedDay.Month)
            {
                if (newStartDay.Day <= 7)
                {
                    StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                }
                else if (newStartDay.Day <= 14)
                {
                    StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                }
            }
            else
            {
                StartDate = newStartDay;
            }

            string id = Page.Request.Params["__EVENTTARGET"];
            if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
            {
                return true;
            }
            else
            {
                // avoid duplicate calls for loadTree();

                if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
                {
                    loadTree(false);
                }
            }

            try
            {
                DateInput = new DateTime();
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);

                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        if (id.Contains("RadScheduler")) //post back event is triggered on close of entry popups / refresh icon.
                        {
                            return true;
                        }

                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;

                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            {//Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }

            EndDate = GetMonthEndDate(StartDate);
            Session["SCMonthStartDay"] = StartDate;
            RadScheduler1.SelectedDate = StartDate;
            RadScheduler2.SelectedDate = StartDate.AddDays(7);
            RadScheduler3.SelectedDate = StartDate.AddDays(14);
            RadScheduler4.SelectedDate = StartDate.AddDays(21);
            RadScheduler5.SelectedDate = StartDate.AddDays(28);
            RadScheduler6.SelectedDate = StartDate.AddDays(35);

            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
            //RadDatePicker1.SelectedDate = selectedDay;
            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);
            return IsDateCheck;

        }

        //-- Start: Tooltip Integration --// 


        protected void RadScheduler6_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManager1.TargetControls.Add(domElementID, id, true);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler5_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManager2.TargetControls.Add(domElementID, id, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }

        protected void RadScheduler4_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManager3.TargetControls.Add(domElementID, id, true);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler3_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManager4.TargetControls.Add(domElementID, id, true);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler2_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManager5.TargetControls.Add(domElementID, id, true);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler1_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManager6.TargetControls.Add(domElementID, id, true);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        private bool IsAppointmentRegisteredForTooltip(Telerik.Web.UI.Appointment apt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(apt))
            {
                foreach (ToolTipTargetControl targetControl in RadToolTipManager1.TargetControls)
                {
                    if (apt.DomElements.Contains(targetControl.TargetControlID))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        protected void RadScheduler6_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManager1.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler5_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManager2.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler4_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManager3.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler3_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManager4.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler2_DataBound(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManager5.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler1_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManager6.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadToolTipManager1_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int aptId;

                        Telerik.Web.UI.Appointment apt;
                        if (!int.TryParse(e.Value, out aptId)) //The appoitnment is occurrence and FindByID expects a string
                            apt = RadScheduler6.Appointments.FindByID(e.Value);
                        else //The appointment is not occurrence and FindByID expects an int
                            apt = RadScheduler6.Appointments.FindByID(aptId);

                        UCCalendarAppointmentTooltip toolTip = (UCCalendarAppointmentTooltip)LoadControl("UCCalendarAppointmentTooltip.ascx");

                        toolTip.TargetAppointment = apt;

                        e.UpdatePanel.ContentTemplateContainer.Controls.Add(toolTip);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler1_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        BindAppointmentData(e, item);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler2_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        BindAppointmentData(e, item);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler3_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        BindAppointmentData(e, item);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler4_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        BindAppointmentData(e, item);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler5_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        BindAppointmentData(e, item);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void RadScheduler6_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        BindAppointmentData(e, item);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        private void BindAppointmentData(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                //Rendition based on red-eye option - if red eye option is not selected, shorten the end date to the start date of the appointment...
                if (DisplayOptions.Monthly != null && !DisplayOptions.Monthly.DisplayRedEyeFlightsInContinuation)
                {
                    if (item.ArrivalDisplayTime.Date != item.DepartureDisplayTime.Date)
                    {
                        e.Appointment.End = item.StartDate;
                    }
                }


                // if black and white color not selected - default is off – can only be selected if Aircraft color is not selected; 
                e.Appointment.Resources.Add(new Resource("Leg", "LegNum", item.LegNum.ToString()));
                e.Appointment.Resources.Add(new Resource("StartDate", "StartDate", e.Appointment.Start.ToString()));
                if (DisplayOptions.Monthly != null)
                {
                    if (DisplayOptions.Monthly.DisplayDetailsOnHover)
                    {
                        hdnEnableHoverDisplay.Value = DisplayOptions.Monthly.DisplayDetailsOnHover.ToString();                    
                    }
                    if (DisplayOptions.Monthly.BlackWhiteColor)
                    {
                        e.Appointment.BackColor = Color.White;
                        e.Appointment.ForeColor = Color.Black;
                    }
                    else
                    {
                        if (!DisplayOptions.Monthly.AircraftColors)
                        {
                            if (item.RecordType == "T")
                            {
                                if (item.IsRONAppointment)
                                {
                                    if (FleetTreeCount > 0) // fleet view
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category

                                        if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.AircraftDutyCD))
                                        {
                                            var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == item.AircraftDutyCD.Trim()).FirstOrDefault();
                                            if (dutyCode != null)
                                            {
                                                e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                            }
                                        }
                                    }
                                    else if (CrewTreeCount > 0) // crew view => CrewTreeCount > 0                                    
                                    {                                    
                                        if (CrewDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.CrewDutyTypeCD))
                                        {
                                            var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == item.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                            if (dutyCode != null)
                                            {
                                                e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    SetFlightCategoryColor(e, item);
                                }

                            }
                            else if (item.RecordType == "M")
                            {
                                // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                SetAircraftDutyColor(e, item);
                            }
                            else if (item.RecordType == "C")
                            {
                                // set crew duty type color...
                                SetCrewDutyColor(e, item);
                            }
                        }
                        else if (DisplayOptions.Monthly.AircraftColors && item.TailNum == null && item.RecordType == "C")
                        {
                            SetCrewDutyColor(e, item);
                        }
                        else
                        {
                            if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                            {
                                e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                            }
                        }
                    }
                }
            }
        }

        private void SetAircraftDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentAircraftDuty = item.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void SetFlightCategoryColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            var appointmentCategory = item.FlightCategoryID;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void SetCrewDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentCategory = item.CrewDutyType;
                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }


        // ON click of Apply button...
        protected void btnApply_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                RadPanelBar2.FindItemByText("Crew").Expanded = false;
                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                if (FilterCriteria.ValidateValue())
                {
                    // get settings from database and update filterSettings option to user settings... and then save
                    var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                    var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                    var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                    var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                    FilterOptions = options.Filters;
                    Session["SCUserSettings"] = options;
                    loadTree(true);
                }
            }
        }


        protected void chksunsat_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                        if (chksunsat.Checked)
                        {
                            SundayToSaturdayCheck();
                        }
                        else // unchecked Chk Sun - sat
                        {
                            //Session["chksunsat"] = "false";
                            if (chkNoPastWeeks.Checked) // if no past weeks checked
                            {
                                // StartDate = GetWeekStartDate(DateTime.Today); // Monday - start day
                                // Session["SCSelectedDay"] = DateTime.Today;

                                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                StartDate = GetWeekStartDate(selectedDay); // Monday - start day
                                Session["SCSelectedDay"] = selectedDay;
                                Session["chkNoPastWeeks"] = "true";
                            }
                            else
                            {
                                var selectedDay = (DateTime)Session["SCSelectedDay"];
                                StartDate = GetMonthStartDate(selectedDay);
                                Session["SCSelectedDay"] = selectedDay;
                                Session["chkNoPastWeeks"] = "false";
                            }

                            RadScheduler1.SelectedDate = StartDate;
                            RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler2.SelectedDate = StartDate.AddDays(7);
                            RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler3.SelectedDate = StartDate.AddDays(14);
                            RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler4.SelectedDate = StartDate.AddDays(21);
                            RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler5.SelectedDate = StartDate.AddDays(28);
                            RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler6.SelectedDate = StartDate.AddDays(35);
                            RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                            Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                        }                        
                        EndDate = GetMonthEndDate(StartDate);

                        objFleetCrewIDs.SetUserScheduleCalenderPreferncesMonthly(ModuleNameConstants.Preflight.Month, chksunsat.Checked, chkNoPastWeeks.Checked);

                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }

        private void SundayToSaturdayCheck()
        {
            //Session["chksunsat"] = "true";
            if (chkNoPastWeeks.Checked) // if no past weeks checked
            {
                //StartDate = GetWeekStartDate(DateTime.Today).AddDays(-1); // Sunday - start day
                // Session["SCSelectedDay"] = DateTime.Today;
                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                StartDate = GetWeekStartDate(selectedDay).AddDays(-1); // Sunday - start day;
                Session["SCSelectedDay"] = selectedDay;
                //Session["chkNoPastWeeks"] = "true";
            }
            else
            {
                var selectedDay = (DateTime)Session["SCSelectedDay"];
                StartDate = GetMonthStartDate(selectedDay);
                Session["SCSelectedDay"] = selectedDay;
                //Session["chkNoPastWeeks"] = "false";
            }

            RadScheduler1.SelectedDate = StartDate;
            RadScheduler1.FirstDayOfWeek = DayOfWeek.Sunday;
            RadScheduler1.LastDayOfWeek = DayOfWeek.Saturday;

            RadScheduler2.SelectedDate = StartDate.AddDays(7);
            RadScheduler2.FirstDayOfWeek = DayOfWeek.Sunday;
            RadScheduler2.LastDayOfWeek = DayOfWeek.Saturday;

            RadScheduler3.SelectedDate = StartDate.AddDays(14);
            RadScheduler3.FirstDayOfWeek = DayOfWeek.Sunday;
            RadScheduler3.LastDayOfWeek = DayOfWeek.Saturday;

            RadScheduler4.SelectedDate = StartDate.AddDays(21);
            RadScheduler4.FirstDayOfWeek = DayOfWeek.Sunday;
            RadScheduler4.LastDayOfWeek = DayOfWeek.Saturday;

            RadScheduler5.SelectedDate = StartDate.AddDays(28);
            RadScheduler5.FirstDayOfWeek = DayOfWeek.Sunday;
            RadScheduler5.LastDayOfWeek = DayOfWeek.Saturday;

            RadScheduler6.SelectedDate = StartDate.AddDays(35);
            RadScheduler6.FirstDayOfWeek = DayOfWeek.Sunday;
            RadScheduler6.LastDayOfWeek = DayOfWeek.Saturday;

            Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
        }

        protected void chkNoPastWeeks_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {                                                
                        if (chkNoPastWeeks.Checked)
                        {
                            NoPastWeeksCheck();
                        }
                        else // unchecked - no past weeks
                        {
                            //Session["chkNoPastWeeks"] = "false";
                            if (!chksunsat.Checked) // StartDay -> Monday
                            {
                                //Session["chksunsat"] = "false";
                                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                StartDate = GetMonthStartDate(selectedDay);
                                Session["SCSelectedDay"] = selectedDay;

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                //RadDatePicker1.SelectedDate = DateTime.Today;

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                            }
                            else //StartDay -> Sunday
                            {
                                //Session["chksunsat"] = "true";
                                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                //StartDate = GetWeekStartDate(selectedDay);
                                //Session["SCSelectedDay"] = selectedDay;

                                StartDate = GetMonthStartDate(selectedDay);
                                Session["SCSelectedDay"] = selectedDay;

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                //RadDatePicker1.SelectedDate = DateTime.Today;

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                            }
                        }

                        EndDate = GetMonthEndDate(StartDate);

                        objFleetCrewIDs.SetUserScheduleCalenderPreferncesMonthly(ModuleNameConstants.Preflight.Month, chksunsat.Checked, chkNoPastWeeks.Checked);   
    
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        private void NoPastWeeksCheck()
        {
            //Session["chkNoPastWeeks"] = "true";
            if (chksunsat.Checked)
            {
                //Session["chksunsat"] = "true";

                //var selectedDay = DateTime.Today;
                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                StartDate = GetWeekStartDate(selectedDay).AddDays(-1); // Sunday - start day;

                RadScheduler1.SelectedDate = StartDate;
                RadScheduler1.FirstDayOfWeek = DayOfWeek.Sunday;
                RadScheduler1.LastDayOfWeek = DayOfWeek.Saturday;

                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                RadScheduler2.FirstDayOfWeek = DayOfWeek.Sunday;
                RadScheduler2.LastDayOfWeek = DayOfWeek.Saturday;

                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                RadScheduler3.FirstDayOfWeek = DayOfWeek.Sunday;
                RadScheduler3.LastDayOfWeek = DayOfWeek.Saturday;

                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                RadScheduler4.FirstDayOfWeek = DayOfWeek.Sunday;
                RadScheduler4.LastDayOfWeek = DayOfWeek.Saturday;

                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                RadScheduler5.FirstDayOfWeek = DayOfWeek.Sunday;
                RadScheduler5.LastDayOfWeek = DayOfWeek.Saturday;

                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                RadScheduler6.FirstDayOfWeek = DayOfWeek.Sunday;
                RadScheduler6.LastDayOfWeek = DayOfWeek.Saturday;

                Session["SCSelectedDay"] = selectedDay;
                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

            }
            else
            {
                //Session["chksunsat"] = "false";
                //var selectedDay = DateTime.Today;
                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                StartDate = GetWeekStartDate(selectedDay); // Monday - start day;

                RadScheduler1.SelectedDate = StartDate;
                RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                Session["SCSelectedDay"] = selectedDay;
                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                EndDate = GetMonthEndDate(StartDate);

                //fleet trips list
                if (!Page.IsPostBack)
                {
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Month);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                    Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length>1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                        //BindFleetTree(Fleetlst, true);
                        //BindCrewTree(Crewlst, false);
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                    else if (!string.IsNullOrEmpty(FleetCrew[2]))
                    {
                        string[] Crewids = FleetCrew[2].Split(',');
                        foreach (string cIDs in Crewids)
                        {
                            string[] ParentChild = cIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                        }
                        //BindFleetTree(Fleetlst, false);
                        //BindCrewTree(Crewlst, true);
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                    else
                    {
                        //BindFleetTree(Fleetlst, true);
                        //BindCrewTree(Crewlst, false);
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }                    
                }

                using (var preflightServiceClient = new PreflightServiceClient())
                {                    
                    // check if Sunday/ Saturday is selected as First day / last day for the view...
                    if (IsSave)
                    {
                        if (GetCheckedTreeNodes(FleetTreeView).Count == 0 && GetCheckedTreeNodes(CrewTreeView).Count == 0)
                        {
                            FleetTreeView.CheckAllNodes();
                        }                        

                        var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                        //var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);
                        var selectedGroupCrewIDs = GetCheckedTreeNodes(CrewTreeView, true);
                        var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView, false);
                        BindFleetTree(selectedFleetIDs);
                        BindCrewTree(selectedCrewIDs);
                        bool IsEntireCrewSelected = IsEntireCrewChecked(CrewTreeView);

                        if (selectedFleetIDs.Count > 0)
                        {
                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Month, selectedFleetIDs, null, null, false);
                            //To get the distinct values
                            List<string> distinctFleet = selectedFleetIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                            string strTestFleet = string.Join(",", distinctFleet.ToArray());
                            Session["FleetIDs"] = strTestFleet;
                        }
                        else
                        {
                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Month, null, selectedCrewIDs, selectedGroupCrewIDs, !IsEntireCrewSelected);
                            //To get the distinct values
                            List<string> distinctCrew = selectedCrewIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                            string strTestCrew = string.Join(",", distinctCrew.ToArray());
                            Session["CrewIDs"] = strTestCrew;
                        }
                        FleetTreeCount = FleetTreeView.CheckedNodes.Count;
                        CrewTreeCount = CrewTreeView.CheckedNodes.Count;
                        Collection<string> flitList = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in selectedFleetIDs)
                        {
                            flitList.Add(s.ChildNodeId);
                        }

                        Collection<string> SelectedCrewIDs = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in selectedCrewIDs)
                        {
                            SelectedCrewIDs.Add(s.ChildNodeId);
                        }
                        BindFleetCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, flitList, SelectedCrewIDs);
                        //BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, selectedFleetIDs);
                        //BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, selectedCrewIDs);
                    }
                    else
                    {
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Month);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CrewIDs = FleetCrew[1];

                            if ((string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0) && (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0))
                            {
                                if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                                {
                                    FleetTreeView.CheckAllNodes();                                    
                                }
                            }

                            Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                            Collection<string> Crewlst = new Collection<string>();

                            if (!string.IsNullOrEmpty(FleetCrew[0]))
                            {
                                string[] Fleetids = FleetCrew[0].Split(',');
                                foreach (string fIDs in Fleetids)
                                {
                                    string[] ParentChild = fIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                }
                                //To get the distinct values
                                List<string> distinctFleet = Fleetlst.Select(node => node.ChildNodeId).Distinct().ToList();
                                string strTestFleet = string.Join(",", distinctFleet.ToArray());
                                Session["FleetIDs"] = strTestFleet;
                            }
                            else if (!string.IsNullOrEmpty(FleetCrew[1]))
                            {
                                string[] Crewids = FleetCrew[1].Split(',');
                                foreach (string cIDs in Crewids)
                                {
                                    Crewlst.Add(cIDs);
                                }
                                //To get the distinct values
                                List<string> distinctCrew = Crewlst.Distinct().ToList();
                                string strTestCrew = string.Join(",", distinctCrew.ToArray());
                                Session["CrewIDs"] = strTestCrew;
                            }
                            else
                                Fleetlst = GetCheckedTreeNodes(FleetTreeView);                                                       

                            FleetTreeCount = FleetTreeView.CheckedNodes.Count;
                            CrewTreeCount = CrewTreeView.CheckedNodes.Count;
                            Collection<string> flitList = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in Fleetlst)
                            {
                                flitList.Add(s.ChildNodeId);
                            }
                            BindFleetCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, flitList, Crewlst);

                            //if ((!string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() > 0) || (Fleetlst.Count > 0))
                            //{
                            //    BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, Fleetlst);
                            //}

                            //if (!string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() > 0)
                            //{
                            //    BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, Crewlst);
                            //}
                        }
                    }

                    /* inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                     FleetTreeCount = inputFromFleetTree.Count; // this property is used in appointment data bound to identify the view 
                     inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                     CrewTreeCount = inputFromCrewTree.Count; // this property is used in appointment data bound to identify the view 

                     if (inputFromFleetTree.Count > 0)
                     {
                         BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, inputFromFleetTree);
                     }
                     else if (inputFromCrewTree.Count > 0)
                     {
                         BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, inputFromCrewTree);
                     }*/
                }

                RadPanelBar2.FindItemByText("Fleet").Visible = true;
                RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                RadPanelBar2.FindItemByText("Crew").Visible = true;
                RadPanelBar2.FindItemByText("Crew").Expanded = true;
                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;

                //-- Start: Tooltip Integration  --// 
                //-- RadScheduler1 --//
                RadScheduler1.AppointmentCreated += RadScheduler1_AppointmentCreated;
                RadScheduler1.DataBound += RadScheduler1_DataBound;
                RadToolTipManager6.OnClientRequestStart = "OnClientRequestStart5";
                //-- RadScheduler1 --//

                //-- RadScheduler2 --//
                RadScheduler2.AppointmentCreated += RadScheduler2_AppointmentCreated;
                RadScheduler2.DataBound += RadScheduler2_DataBound;
                RadToolTipManager5.OnClientRequestStart = "OnClientRequestStart4";
                //-- RadScheduler2 --//

                //-- RadScheduler3 --//
                RadScheduler3.AppointmentCreated += RadScheduler3_AppointmentCreated;
                RadScheduler3.DataBound += RadScheduler3_DataBound;
                RadToolTipManager4.OnClientRequestStart = "OnClientRequestStart3";
                //-- RadScheduler3 --//

                //-- RadScheduler4 --//
                RadScheduler4.AppointmentCreated += RadScheduler4_AppointmentCreated;
                RadScheduler4.DataBound += RadScheduler4_DataBound;
                RadToolTipManager3.OnClientRequestStart = "OnClientRequestStart2";
                //-- RadScheduler4 --//

                //-- RadScheduler5 --//
                RadScheduler5.AppointmentCreated += RadScheduler5_AppointmentCreated;
                RadScheduler5.DataBound += RadScheduler5_DataBound;
                RadToolTipManager2.OnClientRequestStart = "OnClientRequestStart1";
                //-- RadScheduler5 --//

                //-- RadScheduler6 --//
                RadScheduler6.AppointmentCreated += RadScheduler6_AppointmentCreated;
                RadScheduler6.DataBound += RadScheduler6_DataBound;
                RadToolTipManager1.OnClientRequestStart = "OnClientRequestStart";
                //-- RadScheduler6 --//

                //-- End: Tooltip Integration  --//  

                //this.RadPanelBar2.Items[0].Expanded = false;
                //this.RadPanelBar2.Items[1].Expanded = false;
                //this.RadPanelBar2.Items[2].Expanded = true;

                RightPane.Collapsed = true;
            }
        }


        private void BindFleetCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime monthStartDate, DateTime monthEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromFleetTree, Collection<string> inputFromCrewTree)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, monthStartDate, monthEndDate, filterCriteria, displayOptions, viewMode, inputFromFleetTree))
            {

                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                /*if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                {
                    FleetTreeView.CheckAllNodes();
                }

                var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView).Distinct().ToList();
                var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView, false).Distinct().ToList();
                */
                if (inputFromFleetTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetFleetCalendarData(monthStartDate, monthEndDate, serviceFilterCriteria, inputFromFleetTree.Distinct().ToList(), false, false, true);

                    if (displayOptions.Monthly.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                    {
                        CrewTreeView.CheckAllNodes();
                        var inputFromCrewTreeAll = GetCheckedTreeNodes(CrewTreeView, false);
                        CrewTreeView.UncheckAllNodes();

                        Collection<string> InputFromCrewTreeAll = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromCrewTreeAll)
                        {
                            InputFromCrewTreeAll.Add(s.ChildNodeId);
                        }

                        var crewcalendarData = preflightServiceClient.GetCrewCalendarData(monthStartDate, monthEndDate, serviceFilterCriteria, InputFromCrewTreeAll.ToList(), true);

                        // remove records of type='C' and tailNum == null...
                        fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                        crewappointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                        fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }
                }
                else if (inputFromCrewTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetCrewCalendarData(monthStartDate, monthEndDate, serviceFilterCriteria, inputFromCrewTree.Distinct().ToList(), true);
                    crewappointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    /*if (displayOptions.Monthly.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                    {
                        // remove records of type='C' and tailNum == null...
                        //var CalData = calendarData.EntityList.GroupBy(a => a.TripNUM);
                        //calendarData.EntityList = calendarData.EntityList.GroupBy(a => a.TripNUM).Distinct().ToList(); // st(); //   scheduler1Appointments.GroupBy(a => a.StartDate.Date) .Where(x => x.TailNum != null).ToList();
                        crewappointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                        crewappointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }*/
                }

                var scheduler1FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler1.VisibleRangeStart && x.StartDate <= RadScheduler1.VisibleRangeEnd).ToList(); //).GroupBy(x => x.TripNUM);
                var scheduler1CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler1.VisibleRangeStart && x.StartDate <= RadScheduler1.VisibleRangeEnd).ToList();

                //RadScheduler1.DataSource = scheduler1FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler1CrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();

                var calDataS1WithTrip = (scheduler1FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler1CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType == "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS1WithoutTrip = (scheduler1FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler1CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType != "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                RadScheduler1.DataSource = calDataS1WithTrip.OrderBy(x => x.StartDate).ToList().Concat(calDataS1WithoutTrip.OrderBy(x => x.StartDate).ToList());

                //RadScheduler1.DataSource = (scheduler1FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler1CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();

                RadScheduler1.DataStartField = "StartDate";
                RadScheduler1.DataEndField = "EndDate";
                RadScheduler1.DataSubjectField = "Description";
                // RadScheduler1.DataKeyField = "CrewCD";
                RadScheduler1.DataBind();


                var scheduler2FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler2.VisibleRangeStart && x.StartDate <= RadScheduler2.VisibleRangeEnd).ToList();
                var scheduler2CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler2.VisibleRangeStart && x.StartDate <= RadScheduler2.VisibleRangeEnd).ToList();

                //RadScheduler2.DataSource = scheduler2FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler2CrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();

                var calDataS2WithTrip = (scheduler2FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler2CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType == "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS2WithoutTrip = (scheduler2FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler2CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType != "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                RadScheduler2.DataSource = calDataS2WithTrip.OrderBy(x => x.StartDate).ToList().Concat(calDataS2WithoutTrip.OrderBy(x => x.StartDate).ToList());

                //RadScheduler2.DataSource = (scheduler2FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler2CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();

                RadScheduler2.DataStartField = "StartDate";
                RadScheduler2.DataEndField = "EndDate";
                RadScheduler2.DataSubjectField = "Description";
                //   RadScheduler2.DataKeyField = "TailNum";
                RadScheduler2.DataBind();


                var scheduler3FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler3.VisibleRangeStart && x.StartDate <= RadScheduler3.VisibleRangeEnd).ToList();
                var scheduler3CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler3.VisibleRangeStart && x.StartDate <= RadScheduler3.VisibleRangeEnd).ToList();

                //RadScheduler3.DataSource = scheduler3FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler3CrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                //RadScheduler3.DataSource = (scheduler3FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler3CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();

                var calDataS3WithTrip = (scheduler3FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler3CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType == "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS3WithoutTrip = (scheduler3FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler3CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType != "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                RadScheduler3.DataSource = calDataS3WithTrip.OrderBy(x => x.StartDate).ToList().Concat(calDataS3WithoutTrip.OrderBy(x => x.StartDate).ToList());

                RadScheduler3.DataStartField = "StartDate";
                RadScheduler3.DataEndField = "EndDate";
                RadScheduler3.DataSubjectField = "Description";
                //     RadScheduler3.DataKeyField = "TailNum";
                RadScheduler3.DataBind();


                //var scheduler4FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler4.VisibleRangeStart && x.StartDate <= RadScheduler4.VisibleRangeEnd).ToList();
                //var scheduler4CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler4.VisibleRangeStart && x.StartDate <= RadScheduler4.VisibleRangeEnd).ToList();

                var scheduler4FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler4.VisibleRangeStart && x.StartDate <= RadScheduler4.VisibleRangeEnd).ToList();
                var scheduler4CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler4.VisibleRangeStart && x.StartDate <= RadScheduler4.VisibleRangeEnd).ToList();

                //RadScheduler4.DataSource = scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                
                //FinalRadSch4List = FinalRadSch4List.Distinct().ToList(); //.Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Select(x => Convert.ToInt32(x.TripNUM)).Distinct().ToList();// (scheduler4FleetAppointments.Select(x => x.TripId).Distinct()).ToList(); //..OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Select(x => x.TripId).Distinct().ToList();

                //RadScheduler4.DataSource = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(a => a.TripNUM,a => a.LegNum).Select(g => g.First()).ToList();
                //RadScheduler4.DataSource = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Select(p => p.TripNUM).Distinct();               

               // RadScheduler4.DataSource = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum }); // Entities.Employees.GroupBy(e => new { e.Name, e.Company });

                

                //RadScheduler4.DataSource = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Distinct().Select(g => g.TripId,g.First()).ToList();
               


                //RadScheduler4.DataSource = (scheduler4FleetAppointments.GroupBy(x => x.LegNum).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).Select(x => Convert.ToInt64(x.LegId)).Distinct().ToList();


                //RadScheduler4.DataSource = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS4WithTrip = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType == "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS4WithoutTrip = (scheduler4FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler4CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType != "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                RadScheduler4.DataSource = calDataS4WithTrip.OrderBy(x => x.StartDate).ToList().Concat(calDataS4WithoutTrip.OrderBy(x => x.StartDate).ToList());

                RadScheduler4.DataStartField = "StartDate";
                RadScheduler4.DataEndField = "EndDate";
                RadScheduler4.DataSubjectField = "Description";
                //  RadScheduler4.DataKeyField = "TailNum";
                RadScheduler4.DataBind();


                var scheduler5FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler5.VisibleRangeStart && x.StartDate <= RadScheduler5.VisibleRangeEnd).ToList();
                var scheduler5CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler5.VisibleRangeStart && x.StartDate <= RadScheduler5.VisibleRangeEnd).ToList();

                //RadScheduler5.DataSource = scheduler5FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler5CrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                //RadScheduler5.DataSource = (scheduler5FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler5CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();

                var calDataS5WithTrip = (scheduler5FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler5CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType == "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS5WithoutTrip = (scheduler5FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler5CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType != "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                RadScheduler5.DataSource = calDataS5WithTrip.OrderBy(x => x.StartDate).ToList().Concat(calDataS5WithoutTrip.OrderBy(x => x.StartDate).ToList());

                RadScheduler5.DataStartField = "StartDate";
                RadScheduler5.DataEndField = "EndDate";
                RadScheduler5.DataSubjectField = "Description";
                //  RadScheduler5.DataKeyField = "TailNum";
                RadScheduler5.DataBind();


                var scheduler6FleetAppointments = fleetappointments.Where(x => x.StartDate >= RadScheduler6.VisibleRangeStart && x.StartDate <= RadScheduler6.VisibleRangeEnd).ToList();
                var scheduler6CrewAppointments = crewappointments.Where(x => x.StartDate >= RadScheduler6.VisibleRangeStart && x.StartDate <= RadScheduler6.VisibleRangeEnd).ToList();

                //RadScheduler6.DataSource = scheduler6FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler6CrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                //RadScheduler6.DataSource = (scheduler6FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler6CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();

                var calDataS6WithTrip = (scheduler6FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler6CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType == "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                var calDataS6WithoutTrip = (scheduler6FleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(scheduler6CrewAppointments.OrderBy(x => x.StartDate).ToList())).ToList().Where(x => x.RecordType != "T").ToList().GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                RadScheduler6.DataSource = calDataS6WithTrip.OrderBy(x => x.StartDate).ToList().Concat(calDataS6WithoutTrip.OrderBy(x => x.StartDate).ToList());

                RadScheduler6.DataStartField = "StartDate";
                RadScheduler6.DataEndField = "EndDate";
                RadScheduler6.DataSubjectField = "Description";
                //  RadScheduler6.DataKeyField = "TailNum";
                RadScheduler6.DataBind();
            }
        }

        /*private void BindFleetTripInfo(PreflightServiceClient preflightServiceClient, DateTime monthStartDate, DateTime monthEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromFleetTree)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, monthStartDate, monthEndDate, filterCriteria, displayOptions, viewMode, inputFromFleetTree))
            {

                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                if (inputFromFleetTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetFleetCalendarData(monthStartDate, monthEndDate, serviceFilterCriteria, inputFromFleetTree.Distinct().ToList(), false, false, true);

                    Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                    if (displayOptions.Monthly.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                    {
                        // remove records of type='C' and tailNum == null...
                        appointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                        appointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);

                    }

                    var scheduler1Appointments = appointments.Where(x => x.StartDate >= RadScheduler1.VisibleRangeStart && x.StartDate <= RadScheduler1.VisibleRangeEnd).ToList();

                    RadScheduler1.DataSource = scheduler1Appointments.OrderBy(x => x.StartDate).ToList();

                    RadScheduler1.DataStartField = "StartDate";
                    RadScheduler1.DataEndField = "EndDate";
                    RadScheduler1.DataSubjectField = "Description";
                    //    RadScheduler1.DataKeyField = "TailNum";
                    RadScheduler1.DataBind();

                    var scheduler2Appointments = appointments.Where(x => x.StartDate >= RadScheduler2.VisibleRangeStart && x.StartDate <= RadScheduler2.VisibleRangeEnd).ToList();

                    RadScheduler2.DataSource = scheduler2Appointments.OrderBy(x => x.StartDate).ToList();

                    RadScheduler2.DataStartField = "StartDate";
                    RadScheduler2.DataEndField = "EndDate";
                    RadScheduler2.DataSubjectField = "Description";
                    //   RadScheduler2.DataKeyField = "TailNum";
                    RadScheduler2.DataBind();


                    var scheduler3Appointments = appointments.Where(x => x.StartDate >= RadScheduler3.VisibleRangeStart && x.StartDate <= RadScheduler3.VisibleRangeEnd).ToList();

                    RadScheduler3.DataSource = scheduler3Appointments.OrderBy(x => x.StartDate).ToList();

                    RadScheduler3.DataStartField = "StartDate";
                    RadScheduler3.DataEndField = "EndDate";
                    RadScheduler3.DataSubjectField = "Description";
                    //     RadScheduler3.DataKeyField = "TailNum";
                    RadScheduler3.DataBind();

                    var scheduler4Appointments = appointments.Where(x => x.StartDate >= RadScheduler4.VisibleRangeStart && x.StartDate <= RadScheduler4.VisibleRangeEnd).ToList();

                    RadScheduler4.DataSource = scheduler4Appointments.OrderBy(x => x.StartDate).ToList();

                    RadScheduler4.DataStartField = "StartDate";
                    RadScheduler4.DataEndField = "EndDate";
                    RadScheduler4.DataSubjectField = "Description";
                    //  RadScheduler4.DataKeyField = "TailNum";
                    RadScheduler4.DataBind();

                    var scheduler5Appointments = appointments.Where(x => x.StartDate >= RadScheduler5.VisibleRangeStart && x.StartDate <= RadScheduler5.VisibleRangeEnd).ToList();

                    RadScheduler5.DataSource = scheduler5Appointments.OrderBy(x => x.StartDate).ToList();

                    RadScheduler5.DataStartField = "StartDate";
                    RadScheduler5.DataEndField = "EndDate";
                    RadScheduler5.DataSubjectField = "Description";
                    //  RadScheduler5.DataKeyField = "TailNum";
                    RadScheduler5.DataBind();

                    var scheduler6Appointments = appointments.Where(x => x.StartDate >= RadScheduler6.VisibleRangeStart && x.StartDate <= RadScheduler6.VisibleRangeEnd).ToList();

                    RadScheduler6.DataSource = scheduler6Appointments.OrderBy(x => x.StartDate).ToList();

                    RadScheduler6.DataStartField = "StartDate";
                    RadScheduler6.DataEndField = "EndDate";
                    RadScheduler6.DataSubjectField = "Description";
                    //  RadScheduler6.DataKeyField = "TailNum";
                    RadScheduler6.DataBind();

                }
            }

        }

        private void BindCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime monthStartDate, DateTime monthEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromCrewTree)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, monthStartDate, monthEndDate, filterCriteria, displayOptions, viewMode, inputFromCrewTree))
            {

                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                if (inputFromCrewTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetCrewCalendarData(monthStartDate, monthEndDate, serviceFilterCriteria, inputFromCrewTree.Distinct().ToList(), true);

                    Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                    if (displayOptions.Monthly.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                    {
                        // remove records of type='C' and tailNum == null...
                        appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                        appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    }
                    //var appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, monthStartDate, monthEndDate);
                    var scheduler1Appointments = appointments.Where(x => x.StartDate >= RadScheduler1.VisibleRangeStart && x.StartDate <= RadScheduler1.VisibleRangeEnd).ToList();
                    RadScheduler1.DataSource = scheduler1Appointments.OrderBy(x => x.StartDate).ToList();

                    //var maxAppointmentsPerStack = (scheduler1Appointments.Count() == 0) ? 0 : scheduler1Appointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    //if (maxAppointmentsPerStack > 1)
                    //{
                    //    RadScheduler1.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat + "<b> (..)</b>";
                    //}
                    RadScheduler1.DataStartField = "StartDate";
                    RadScheduler1.DataEndField = "EndDate";
                    RadScheduler1.DataSubjectField = "Description";
                    // RadScheduler1.DataKeyField = "CrewCD";
                    RadScheduler1.DataBind();

                    var scheduler2Appointments = appointments.Where(x => x.StartDate >= RadScheduler2.VisibleRangeStart && x.StartDate <= RadScheduler2.VisibleRangeEnd).ToList();
                    RadScheduler2.DataSource = scheduler2Appointments.OrderBy(x => x.StartDate).ToList(); ;
                    //maxAppointmentsPerStack = (scheduler2Appointments.Count() == 0) ? 0 : scheduler2Appointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    //if (maxAppointmentsPerStack > 1)
                    //{
                    //    RadScheduler2.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat + "<b> (..)</b>";
                    //}
                    RadScheduler2.DataStartField = "StartDate";
                    RadScheduler2.DataEndField = "EndDate";
                    RadScheduler2.DataSubjectField = "Description";
                    //RadScheduler2.DataKeyField = "CrewCD";
                    RadScheduler2.DataBind();


                    var scheduler3Appointments = appointments.Where(x => x.StartDate >= RadScheduler3.VisibleRangeStart && x.StartDate <= RadScheduler3.VisibleRangeEnd).ToList();
                    RadScheduler3.DataSource = scheduler3Appointments.OrderBy(x => x.StartDate).ToList(); ;
                    //maxAppointmentsPerStack = (scheduler3Appointments.Count() == 0) ? 0 : scheduler3Appointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    //if (maxAppointmentsPerStack > 1)
                    //{
                    //    RadScheduler3.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat + "<b> (..)</b>";
                    //}
                    RadScheduler3.DataStartField = "StartDate";
                    RadScheduler3.DataEndField = "EndDate";
                    RadScheduler3.DataSubjectField = "Description";
                    //  RadScheduler3.DataKeyField = "CrewCD";
                    RadScheduler3.DataBind();

                    var scheduler4Appointments = appointments.Where(x => x.StartDate >= RadScheduler4.VisibleRangeStart && x.StartDate <= RadScheduler4.VisibleRangeEnd).ToList();

                    RadScheduler4.DataSource = scheduler4Appointments.OrderBy(x => x.StartDate).ToList(); ;
                    //maxAppointmentsPerStack = (scheduler4Appointments.Count() == 0) ? 0 : scheduler4Appointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    //if (maxAppointmentsPerStack > 1)
                    //{
                    //    RadScheduler4.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat + "<b> (..)</b>";
                    //}
                    RadScheduler4.DataStartField = "StartDate";
                    RadScheduler4.DataEndField = "EndDate";
                    RadScheduler4.DataSubjectField = "Description";
                    //  RadScheduler4.DataKeyField = "CrewCD";
                    RadScheduler4.DataBind();

                    var scheduler5Appointments = appointments.Where(x => x.StartDate >= RadScheduler5.VisibleRangeStart && x.StartDate <= RadScheduler5.VisibleRangeEnd).ToList();

                    RadScheduler5.DataSource = scheduler5Appointments.OrderBy(x => x.StartDate).ToList(); ;
                    //maxAppointmentsPerStack = (scheduler5Appointments.Count() == 0) ? 0 : scheduler5Appointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    //if (maxAppointmentsPerStack > 1)
                    //{
                    //    RadScheduler5.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat + "<b> (..)</b>";
                    //}
                    RadScheduler5.DataStartField = "StartDate";
                    RadScheduler5.DataEndField = "EndDate";
                    RadScheduler5.DataSubjectField = "Description";
                    //  RadScheduler5.DataKeyField = "CrewCD";
                    RadScheduler5.DataBind();

                    var scheduler6Appointments = appointments.Where(x => x.StartDate >= RadScheduler6.VisibleRangeStart && x.StartDate <= RadScheduler6.VisibleRangeEnd).ToList();

                    RadScheduler6.DataSource = scheduler6Appointments.OrderBy(x => x.StartDate).ToList(); ;
                    //maxAppointmentsPerStack = (scheduler6Appointments.Count() == 0) ? 0 : scheduler6Appointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    //if (maxAppointmentsPerStack > 1)
                    //{
                    //    RadScheduler6.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat + "<b> (..)</b>";
                    //}
                    RadScheduler6.DataStartField = "StartDate";
                    RadScheduler6.DataEndField = "EndDate";
                    RadScheduler6.DataSubjectField = "Description";
                    //  RadScheduler6.DataKeyField = "CrewCD";
                    RadScheduler6.DataBind();
                }
            }
        }*/

        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;

                        //Changes done for the Bug ID - 5828
                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);
                        FleetTreeView.Nodes.Clear();
                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;

                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNewGroupBased(crewList, selectedcrewIDs);
                        CrewTreeView.Nodes.Clear();
                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;
                        CrewTreeView.DataBind();
                    }
                }
            }
        }

        /*private void BindFleetTree(Collection<string> selectedFleetIDs, bool IsChecked)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;

                        //Changes done for the Bug ID - 5828
                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodes(fleetList, IsChecked);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;

                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<string> selectedcrewIDs, bool IsChecked)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodes(crewList, IsChecked);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;

                        CrewTreeView.DataBind();

                    }
                }
            }
        }*/

        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime monthStartDate, DateTime monthEndDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                var distinctTripNos = calendarData.Select(x => x.TripNUM).Distinct().ToList();

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripNUM == tripNo).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                    foreach (var leg in groupedLegs)
                    {
                        if (displayOptions.Monthly.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);
                            }
                            else if (leg.LegNUM == 0) // RON appointment
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);
                            }
                        }
                        else // No first leg last leg selected
                        {
                            if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.TailNum.ToUpper() != "DUMMY" && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                    if (leg.DepartureDisplayTime >= monthStartDate && leg.DepartureDisplayTime <= monthEndDate)
                                    {
                                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();

                                        SetFleetAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);
                                        appointmentCollection.Add(appointment);
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                if (leg.DepartureDisplayTime >= monthStartDate && leg.DepartureDisplayTime <= monthEndDate)
                                {
                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetFleetAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);
                                    appointmentCollection.Add(appointment);
                                }
                            }
                        }
                    }
                }
                return appointmentCollection;
            }
        }

        private void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);

                // todo: build description for appointment based on view selected and display options for rendering
               var options = displayOptions.Monthly;

                if (tripLeg.RecordType == "T")
                {
                    if (tripLeg.LegNUM == 0) // dummy appointment
                    {
                        if (options.FirstLastLeg == false)
                        {

                            appointment.Description = appointment.TailNum + " ( " + appointment.AircraftDutyCD + " ) ";


                            switch (options.DepartArriveInfo)
                            {
                                case DepartArriveInfo.ICAO:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                    break;

                                case DepartArriveInfo.AirportName:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                    break;

                                case DepartArriveInfo.City:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                    break;
                            }
                            appointment.Description = appointment.Description + appointment.ArrivalICAOID;

                            //  below 2 lines are added to fix IE issue - hide / show tooltip...
                            appointment.ToolTipSubject = appointment.Description;
                            appointment.ToolTipDescription = BuildWeeklyFleetAndCrewToolTipDescription(appointment,
                                                                                                       viewMode,
                                                                                                       displayOptions);
                                //" ";
                            //appointment.Description = appointment.Description + BuildWeeklyFleetAndCrewOnlyToolTipDescriptionAppt(appointment, viewMode, displayOptions);
                        }
                    }
                    else // db trips
                    {
                        appointment.Description = BuildWeeklyFleetAndCrewAppointmentDescription(appointment, null,
                                                                                                calendarData,
                                                                                                displayOptions);
                        appointment.ToolTipSubject = appointment.Description;
                        appointment.ToolTipDescription = BuildWeeklyFleetAndCrewToolTipDescription(appointment, viewMode,
                                                                                                   displayOptions);
                        //appointment.Description = appointment.Description + BuildWeeklyFleetAndCrewToolTipDescriptionAppt(appointment, viewMode, displayOptions);
                    }
                }
                else // entries...
                {
                    switch (displayOptions.Monthly.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;
                    }

                    StringBuilder builder = new StringBuilder();
                    builder.Append("<b>");
                    builder.Append(appointment.TailNum);
                    builder.Append("</b>");
                    builder.Append("</br>");
                    if (appointment.RecordType == "M")
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(") ");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(") ");
                        }
                    }

                    if (appointment.RecordType == "M")
                    {
                        if (appointment.StartDate == appointment.HomelDepartureTime)
                        {
                            builder.Append(appointment.StartDate.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }
                        //builder.Append("</br>");
                        //builder.Append(appointment.CrewDutyTypeDescription);
                    }
                    else
                    {
                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");                        
                    }

                    if (appointment.RecordType == "M")
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append("</br>");
                            builder.Append(appointment.AircraftDutyDescription);                            
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeDescription))
                        {
                            builder.Append("</br>");
                            builder.Append(appointment.CrewDutyTypeDescription);
                        }
                    }

                    //builder.Append(appointment.StartDate.ToString("HH:mm"));
                    //builder.Append(" ");

                    //builder.Append(appointment.DepartureICAOID);
                    //builder.Append(" ");

                    //builder.Append(appointment.EndDate.ToString("HH:mm"));
                    //builder.Append("</br>");

                    builder.Append(appointment.Notes);
                    appointment.Description = builder.ToString();
                    //  below 2 lines are added to fix IE issue - hide / show tooltip...
                    appointment.ToolTipSubject = appointment.Description;
                    appointment.ToolTipDescription = BuildWeeklyFleetAndCrewOnlyToolTipDescription(appointment, viewMode, displayOptions); //appointment.Description;
                    //appointment.Description = appointment.Description + BuildWeeklyFleetAndCrewOnlyToolTipDescriptionAppt(appointment, viewMode, displayOptions); 
                }
                appointment.ViewMode = viewMode;
            }
        }

        //Convert calendar data result to Appointment entity
        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime monthStartDate, DateTime monthEndDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {

                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                // var groupedLegs = calendarData.GroupBy(p => new { p.TripNUM }).Select(g => g.First()).ToList();

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();
                   
                    foreach (var leg in groupedLegs)
                    {                          
                        if (displayOptions.Monthly.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);

                                appointmentCollection.Add(appointment);

                            }
                            else if (leg.LegNUM == 0) // RON appointment
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);

                                appointmentCollection.Add(appointment);
                            }
                        }
                        else // No first leg last leg selected
                        {
                            if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                    if (leg.DepartureDisplayTime >= monthStartDate && leg.DepartureDisplayTime <= monthEndDate)
                                    {
                                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();

                                        SetCrewAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);
                                        appointmentCollection.Add(appointment);
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                if (leg.DepartureDisplayTime >= monthStartDate && leg.DepartureDisplayTime <= monthEndDate)
                                {
                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetCrewAppointment(groupedLegs, timeBase, displayOptions, viewMode, leg, appointment);
                                    appointmentCollection.Add(appointment);
                                }                                
                            }
                        }
                    }
                }
                return appointmentCollection;
            }
        }

        private void SetCrewAppointment(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, displayOptions, viewMode, tripLeg, appointment))
            {

                SetCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);

                // todo: build description for appointment based on view selected and display options for rendering

                var options = displayOptions.Monthly;
                if (tripLeg.RecordType == "T")
                {
                    if (tripLeg.LegNUM == 0) // dummy appointment
                    {
                        if (options.FirstLastLeg == false)
                        {

                            var builder = new StringBuilder();
                            builder.Append("<b>" + appointment.CrewCD);
                            builder.Append(" ");
                            builder.Append(appointment.TailNum);
                            builder.Append("</b></br> ( ");
                            builder.Append(appointment.CrewDutyTypeCD + " ) ");


                            switch (options.DepartArriveInfo)
                            {
                                case DepartArriveInfo.ICAO:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                    break;

                                case DepartArriveInfo.AirportName:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                    break;

                                case DepartArriveInfo.City:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                    break;

                            }
                            builder.Append(" ");
                            builder.Append(appointment.ArrivalICAOID);


                            var flightNumber = string.Empty;
                            if (options.FlightNumber)
                            {
                                flightNumber = " Flt No.: " + appointment.FlightNum;
                                builder.Append(" ");
                                builder.Append(flightNumber);
                            }

                            appointment.Description = builder.ToString();
                            //  below 2 lines are added to fix IE issue - hide / show tooltip...
                            appointment.ToolTipSubject = appointment.Description;
                            appointment.ToolTipDescription = BuildWeeklyFleetAndCrewToolTipDescription(appointment,
                                                                                                       viewMode,
                                                                                                       displayOptions);
                                //" ";
                            //appointment.Description += BuildWeeklyFleetAndCrewToolTipDescriptionAppt(appointment, viewMode, displayOptions); //" ";
                        }
                    }
                    else // db trips
                    {
                        //appointment.Description = //BuildWeeklyFleetAndCrewAppointmentDescription(appointment, calendarData, null, displayOptions);
                        appointment.Description = BuildWeeklyFleetAndCrewAppointmentDescription(appointment,
                                                                                                calendarData, null,
                                                                                                displayOptions);
                        appointment.ToolTipSubject = appointment.Description;
                        appointment.ToolTipDescription = BuildWeeklyFleetAndCrewToolTipDescription(appointment, viewMode,
                                                                                                   displayOptions);
                        // appointment.Description += BuildWeeklyFleetAndCrewToolTipDescriptionAppt(appointment, viewMode, displayOptions);
                    }
                }
                else // entries...
                {
                    switch (displayOptions.Monthly.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;

                    }

                    StringBuilder builder = new StringBuilder();
                    builder.Append("<b>");
                    builder.Append(appointment.CrewCD);
                    builder.Append(" ");
                    builder.Append(appointment.TailNum);
                    builder.Append("</b>");
                    builder.Append("</br>");
                    if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                    {
                        builder.Append("(");
                        builder.Append(appointment.CrewDutyTypeCD);
                        builder.Append(") ");
                    }

                    builder.Append(appointment.StartDate.ToString("HH:mm"));
                    builder.Append(" ");

                    builder.Append(appointment.DepartureICAOID);
                    builder.Append(" ");

                    builder.Append(appointment.EndDate.ToString("HH:mm"));
                    builder.Append("</br>");
                    
                    builder.Append(appointment.CrewDutyTypeDescription);
                    builder.Append("</br>");

                    builder.Append(appointment.Notes);
                    appointment.Description = builder.ToString();
                    //  below 2 lines are added to fix IE issue - hide / show tooltip...
                    appointment.ToolTipSubject = appointment.Description;
                    appointment.ToolTipDescription = BuildWeeklyFleetAndCrewOnlyToolTipDescription(appointment, viewMode, displayOptions); //appointment.Description;
                    //appointment.Description += BuildWeeklyFleetAndCrewOnlyToolTipDescriptionAppt(appointment, viewMode, displayOptions); 
                }
                appointment.ViewMode = viewMode;
            }
        }


        //To get start date of the Month.. (startDay = Monday)
        private DateTime GetMonthStartDate(DateTime selectedDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedDate))
            {

                if (!chksunsat.Checked)// StartDay -> Monday
                {
                    var weekNumber = GetWeekOfMonth(selectedDate); //todo: correct this method

                    if (weekNumber == 1)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1);
                    }
                    else if (weekNumber == 2)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1).AddDays(-7);
                    }
                    else if (weekNumber == 3)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1).AddDays(-14);
                    }
                    else if (weekNumber == 4)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1).AddDays(-21);
                    }
                    return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1).AddDays(-28);

                }
                else //Sunday - Start day
                {
                    // return selectedDate.AddDays(-30);
                    var weekNumber = GetWeekOfMonth(selectedDate);

                    if (weekNumber == 1)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek);
                    }
                    else if (weekNumber == 2)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(-7);
                    }
                    else if (weekNumber == 3)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(-14);
                    }
                    else if (weekNumber == 4)
                    {
                        return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(-21);
                    }

                    return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(-28);

                }

            }

        }

        public static int GetWeekOfMonth(DateTime date)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(date))
            {

                DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);

                while (date.Date.AddDays(1).DayOfWeek != CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                    date = date.AddDays(1);

                return (int)Math.Truncate((double)date.Subtract(beginningOfMonth).TotalDays / 7f) + 1;

            }

        }

        // To get end day of the Month...
        private DateTime GetMonthEndDate(DateTime monthStartDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(monthStartDate))
            {
                return monthStartDate.AddDays(41).AddHours(23).AddMinutes(59);
            }
        }

        // description for appointment
        public string BuildWeeklyFleetAndCrewAppointmentDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, List<CrewCalendarDataResult> calendarData, List<FleetCalendarDataResult> fleetCalendarData, DisplayOptions displayOptions)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, calendarData, fleetCalendarData, displayOptions))
            {

                var description = new StringBuilder();
                var options = displayOptions.Monthly;

                description.Append("<b>");
                description.Append(appointment.TailNum);
                description.Append("</b>");
                description.Append("<br />\n");

                if (options.ShowTrip)
                {
                    description.Append("Trip: ");
                    description.Append(appointment.TripNUM);
                    description.Append("   ");
                    description.Append("Leg No.: ");
                    description.Append(appointment.LegNum);
                }

                if (options.ShowTripStatus)
                {
                    description.Append("( ");
                    description.Append(appointment.TripStatus);
                    description.Append(" ) ");
                }
                description.Append("<br />\n");

                if (calendarData != null && calendarData.Count > 0) // calendar crew data
                {
                    var departureArrivalInfo = options.DepartArriveInfo;
                    GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);
                    description.Append(" ");
                }
                else // calendar fleet data
                {
                    var departureArrivalInfo = options.DepartArriveInfo;
                    GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);
                    description.Append(" ");
                }
                //To display Arrival Date in Scheduler
                if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date && appointment.RecordType == "T")
                {
                    description.Append("<br />\n");
                    description.Append("* Arrival Date: ");
                    //description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                    description.Append(String.Format("{0:" + ApplicationDateFormat + "}", Convert.ToDateTime(appointment.ArrivalDisplayTime.ToShortDateString().ToString())));
                    description.Append(" *");
                    description.Append("<br />\n");
                    description.Append(" ");
                }
                return description.ToString();
            }
        }

        //description for tooltip 

        public string BuildWeeklyFleetAndCrewToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, viewMode, displayOptions))
            {

                var description = new StringBuilder();
                var options = displayOptions.Monthly;

                //if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date)
                //{
                //    description.Append("<br />\n");
                //    description.Append("** Arrival Date: ");
                //    description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                //    description.Append(" **");
                //    description.Append("<br />\n");
                //}

                description.Append("<table> <tr><td>");
                if (appointment.ShowAllDetails)
                {
                    if (options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append(" ");
                    }
                    description.Append(" </td><td>");

                    if (options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");

                    if (options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    //description.Append("</td><td>");
                    if (options.CummulativeETE)
                    {
                        description.Append("<strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");
                    if (options.TotalETE)
                    {
                        description.Append("<strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td>");
                    if (options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td>");
                    if (options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");
                    if (options.TripPurpose)
                    {
                        description.Append("<strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");
                    if (options.LegPurpose)
                    {
                        description.Append("<strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                   
                    description.Append(" </td></tr>");                                     

                    description.Append(" <tr> <td>");
                    if (options.SeatsAvailable)
                    {
                        description.Append("<strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");

                    if (options.Crew)
                    {
                        // todo: get from Anoop
                        description.Append("<strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCodes);
                            //description.Append(appointment.CrewCD);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                    }
                }
                else // show details based on Trip Privacy Settins from company profile
                {
                    if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append(" ");
                    }
                    description.Append(" </td><td>");

                    if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");

                    if (PrivateTrip.IsShowETE && options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    //description.Append("</td><td>");

                    if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                    {
                        description.Append("<strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");
                    if (PrivateTrip.IsShowTotalETE && options.TotalETE)
                    {
                        description.Append("<strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td>");
                    if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (PrivateTrip.IsShowRequestor && options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");


                    description.Append(" <tr> <td>");
                    if (PrivateTrip.IsShowDepartment && options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (PrivateTrip.IsShowAuthorization && options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");                                    

                    description.Append(" <tr><td colspan='2'>");
                    if (PrivateTrip.IsShowTripPurpose && options.TripPurpose)
                    {
                        description.Append("<strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    description.Append(" </td></tr>");

                    description.Append(" <tr><td colspan='2'>");
                    if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                    {
                        description.Append("<strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }                   
                    description.Append(" </td></tr>");                                      

                    description.Append(" <tr><td>");
                    if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                    {
                        description.Append("<strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");

                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {
                        // todo: get from Anoop
                        description.Append("<strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            //description.Append(appointment.CrewCD);
                            description.Append(appointment.CrewCodes);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                    }
                }
                description.Append(" </td></tr></table>");

                return description.ToString();
            }
        }

        public string BuildWeeklyFleetAndCrewToolTipDescriptionAppt(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.Monthly;
                //if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date)
                //{
                //    description.Append("<br />\n");
                //    description.Append("** Arrival Date: ");
                //    description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                //    description.Append(" **");
                //    description.Append("<br />\n");
                //}

                description.Append("\n");
                if (appointment.ShowAllDetails)
                {
                    description.Append("<br />");
                    if (options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append("     ");
                    }
                    //description.Append("<br />");

                    if (options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    description.Append("<br />");

                    //description.Append(" <tr><td colspan='2'>");
                    if (options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    //description.Append("</td><td>");
                    if (options.CummulativeETE)
                    {
                        description.Append("<br /><strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    if (options.TotalETE)
                    {
                        description.Append("<br /><strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append("     ");
                    }
                    //description.Append("<br />");
                    if (options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("     ");
                    }

                    if (options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.TripPurpose)
                    {
                        description.Append("<br /><strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.LegPurpose)
                    {
                        description.Append("<br /><strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.SeatsAvailable)
                    {
                        description.Append("<br /><strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    //description.Append("<br />");

                    if (options.Crew)
                    {
                        description.Append("<br /><strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCodes);
                            //description.Append(appointment.CrewCD);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                    }
                }
                else // show details based on Trip Privacy Settins from company profile
                {
                    description.Append("<br />");
                    if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append("  ");
                    }
                    //description.Append("<br />");

                    if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    description.Append("\n");
                    if (PrivateTrip.IsShowETE && options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    //description.Append("</td><td>");
                    if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                    {
                        description.Append("<br /><strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    if (PrivateTrip.IsShowTotalETE && options.TotalETE)
                    {
                        description.Append("<br /><strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append("     ");
                    }
                    //description.Append("<\n>");
                    if (PrivateTrip.IsShowRequestor && options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (PrivateTrip.IsShowDepartment && options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("     ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowAuthorization && options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    // description.Append("<br />");
                    if (PrivateTrip.IsShowTripPurpose && options.TripPurpose)
                    {
                        description.Append("<br /><strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                    {
                        description.Append("<br /><strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                    {
                        description.Append("<br /><strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    //description.Append("<br />");

                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {

                        description.Append("<br /><strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            //description.Append(appointment.CrewCD);
                            description.Append(appointment.CrewCodes);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                    }
                }
                //description.Append(" </td></tr></table>");
            }
            return description.ToString();
        }

        public string BuildWeeklyFleetAndCrewOnlyToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, viewMode, displayOptions))
            {

                var description = new StringBuilder();
                var options = displayOptions.Monthly;
                description.Append("<table> <tr> <td>");
                //description.Append(appointment.CrewDutyTypeDescription);
                //description.Append("</td></tr><tr><td>");
                description.Append("<strong>Home Base: <strong>");
                description.Append(appointment.HomebaseCD);
                description.Append(" </td> </tr> </table>");
                return description.ToString();
            }
        }

        public string BuildWeeklyFleetAndCrewOnlyToolTipDescriptionAppt(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {
                var options = displayOptions.Monthly;
                description.Append("<br />");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append("<br />");
            }

            return description.ToString();
        }

        //Departure and Arrival information for tooltip
        private void GetDepartureArrivalInfo(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DepartArriveInfo departureArrivalInfo, StringBuilder description)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, departureArrivalInfo, description))
            {


                if (appointment.ShowAllDetails)
                {
                    description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                    description.Append(" ");
                    switch (departureArrivalInfo)
                    {
                        case DepartArriveInfo.ICAO:

                            description.Append(appointment.DepartureICAOID);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                            break;

                        case DepartArriveInfo.AirportName:

                            description.Append(appointment.DepartureAirportName);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                            break;

                        case DepartArriveInfo.City:

                            description.Append(appointment.DepartureCity);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                            break;

                    }
                    description.Append(" ");
                    description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                }
                else // show as trip privacy settings from company profile
                {
                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        description.Append(" ");
                    }

                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (departureArrivalInfo)
                        {
                            case DepartArriveInfo.ICAO:

                                description.Append(appointment.DepartureICAOID);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                                break;

                            case DepartArriveInfo.AirportName:

                                description.Append(appointment.DepartureAirportName);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                                break;

                            case DepartArriveInfo.City:

                                description.Append(appointment.DepartureCity);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                                break;

                        }
                    }

                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (PrivateTrip.IsShowArrivalDepartICAO)
                        {
                            description.Append(" ");
                        }
                        else
                        {
                            description.Append(" | ");

                        }

                        description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                    }
                }


            }
        }

        // button today click...
        protected void btnToday_Click(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (chksunsat.Checked) // Sunday - start day
                        {
                            StartDate = GetMonthStartDate(DateTime.Today);
                            Session["SCSelectedDay"] = DateTime.Today;

                            RadScheduler1.SelectedDate = StartDate;
                            RadScheduler1.FirstDayOfWeek = DayOfWeek.Sunday;
                            RadScheduler1.LastDayOfWeek = DayOfWeek.Saturday;

                            RadScheduler2.SelectedDate = StartDate.AddDays(7);
                            RadScheduler2.FirstDayOfWeek = DayOfWeek.Sunday;
                            RadScheduler2.LastDayOfWeek = DayOfWeek.Saturday;

                            RadScheduler3.SelectedDate = StartDate.AddDays(14);
                            RadScheduler3.FirstDayOfWeek = DayOfWeek.Sunday;
                            RadScheduler3.LastDayOfWeek = DayOfWeek.Saturday;

                            RadScheduler4.SelectedDate = StartDate.AddDays(21);
                            RadScheduler4.FirstDayOfWeek = DayOfWeek.Sunday;
                            RadScheduler4.LastDayOfWeek = DayOfWeek.Saturday;

                            RadScheduler5.SelectedDate = StartDate.AddDays(28);
                            RadScheduler5.FirstDayOfWeek = DayOfWeek.Sunday;
                            RadScheduler5.LastDayOfWeek = DayOfWeek.Saturday;

                            RadScheduler6.SelectedDate = StartDate.AddDays(35);
                            RadScheduler6.FirstDayOfWeek = DayOfWeek.Sunday;
                            RadScheduler6.LastDayOfWeek = DayOfWeek.Saturday;

                            Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            //RadDatePicker1.SelectedDate = DateTime.Today;

                            lbMonth.Text = DateTimeInfo.GetMonthName(DateTime.Today.Month) + " " + DateTime.Today.Year;

                        }
                        else // unchecked Chk Sun - sat - // Monday - start day
                        {

                            StartDate = GetMonthStartDate(DateTime.Today);
                            Session["SCSelectedDay"] = DateTime.Today;

                            RadScheduler1.SelectedDate = StartDate;
                            RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler2.SelectedDate = StartDate.AddDays(7);
                            RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler3.SelectedDate = StartDate.AddDays(14);
                            RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler4.SelectedDate = StartDate.AddDays(21);
                            RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler5.SelectedDate = StartDate.AddDays(28);
                            RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                            RadScheduler6.SelectedDate = StartDate.AddDays(35);
                            RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                            RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                            Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                            tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            //RadDatePicker1.SelectedDate = DateTime.Today;

                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(DateTime.Today.Month) + " " + DateTime.Today.Year);
                        }

                        EndDate = GetMonthEndDate(StartDate);

                        #region "No Past week"

                        if (chkNoPastWeeks.Checked)
                        {
                            NoPastWeeksCheck();
                        }
                        else
                        {
                            Session["chkNoPastWeeks"] = "false";
                            if (!chksunsat.Checked) // StartDay -> Monday
                            {
                                Session["chksunsat"] = "false";
                                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                StartDate = GetMonthStartDate(selectedDay);
                                Session["SCSelectedDay"] = selectedDay;

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                //RadDatePicker1.SelectedDate = DateTime.Today;

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                            }
                            else //StartDay -> Sunday
                            {
                                Session["chksunsat"] = "true";
                                DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                //StartDate = GetWeekStartDate(selectedDay);
                                //Session["SCSelectedDay"] = selectedDay;

                                StartDate = GetMonthStartDate(selectedDay);
                                Session["SCSelectedDay"] = selectedDay;

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                //RadDatePicker1.SelectedDate = DateTime.Today;

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                            }
                        }

                        #endregion

                        loadTree(false);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }



        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            // var selectedDateFromPicker = RadDatePicker1.SelectedDate.Value;
                            if (chksunsat.Checked) // Sunday - start day
                            {
                                StartDate = GetMonthStartDate(DateInput);
                                //Session["SCSelectedDay"] = selectedDateFromPicker;

                                Session["SCSelectedDay"] = DateInput;

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler1.FirstDayOfWeek = DayOfWeek.Sunday;
                                RadScheduler1.LastDayOfWeek = DayOfWeek.Saturday;

                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler2.FirstDayOfWeek = DayOfWeek.Sunday;
                                RadScheduler2.LastDayOfWeek = DayOfWeek.Saturday;

                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler3.FirstDayOfWeek = DayOfWeek.Sunday;
                                RadScheduler3.LastDayOfWeek = DayOfWeek.Saturday;

                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler4.FirstDayOfWeek = DayOfWeek.Sunday;
                                RadScheduler4.LastDayOfWeek = DayOfWeek.Saturday;

                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler5.FirstDayOfWeek = DayOfWeek.Sunday;
                                RadScheduler5.LastDayOfWeek = DayOfWeek.Saturday;

                                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                RadScheduler6.FirstDayOfWeek = DayOfWeek.Sunday;
                                RadScheduler6.LastDayOfWeek = DayOfWeek.Saturday;

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                                tbDate.Text = DateInput.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(DateInput.Month) + " " + DateInput.Year);
                            }
                            else // unchecked Chk Sun - sat - // Monday - start day
                            {

                                StartDate = GetMonthStartDate(DateInput);
                                //Session["SCSelectedDay"] = selectedDateFromPicker;

                                Session["SCSelectedDay"] = DateInput;

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                tbDate.Text = DateInput.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                                lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(DateInput.Month) + " " + DateInput.Year);
                            }

                            EndDate = GetMonthEndDate(StartDate);
                            loadTree(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }



        }

        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        int count = 0;

                        if (Convert.ToInt32(Session["SCMonthlyPreviousCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }

                            StartDate = GetMonthStartDate(DateTime.Today); // no change in start date...
                            EndDate = GetMonthEndDate(StartDate);

                            var nextDate = selectedDate.AddDays(-7);
                            Session["SCSelectedDay"] = nextDate;
                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(nextDate.Month) + " " + nextDate.Year);
                            //RadDatePicker1.SelectedDate = nextDate;
                            tbDate.Text = nextDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            Session["SCMonthStartDay"] = StartDate;





                            if (selectedDate.Month != nextDate.Month)
                            {

                                var newStartDay = GetMonthStartDate(nextDate);
                                if (newStartDay.Month == nextDate.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }

                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion

                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionDate = (DateTime)Session["SCSelectedDay"];
                            var selectedNextDay = sessionDate.AddDays(-7);

                            Session["SCSelectedDay"] = selectedNextDay;
                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedNextDay.Month) + " " + selectedNextDay.Year);
                            //RadDatePicker1.SelectedDate = selectedNextDay;
                            tbDate.Text = selectedNextDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            StartDate = (DateTime)Session["SCMonthStartDay"];
                            EndDate = GetMonthEndDate(StartDate);

                            if (sessionDate.Month != selectedNextDay.Month)
                            {
                                var newStartDay = GetMonthStartDate(selectedNextDay);
                                if (newStartDay.Month == selectedNextDay.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }


                                EndDate = GetMonthEndDate(StartDate);

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }
                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion
                            loadTree(false);
                        }

                        Session["SCMonthlyPreviousCount"] = ++count;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }



        }

        protected void nextButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCMonthlyNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }

                            StartDate = GetMonthStartDate(DateTime.Today); // no change in start date...
                            EndDate = GetMonthEndDate(StartDate);

                            var nextDate = selectedDate.AddDays(7);
                            Session["SCSelectedDay"] = nextDate;
                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(nextDate.Month) + " " + nextDate.Year);
                            //RadDatePicker1.SelectedDate = nextDate;
                            tbDate.Text = nextDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            Session["SCMonthStartDay"] = StartDate;





                            if (selectedDate.Month != nextDate.Month)
                            {

                                var newStartDay = GetMonthStartDate(nextDate);
                                if (newStartDay.Month == nextDate.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }

                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionDate = (DateTime)Session["SCSelectedDay"];
                            var selectedNextDay = sessionDate.AddDays(7);

                            Session["SCSelectedDay"] = selectedNextDay;
                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedNextDay.Month) + " " + selectedNextDay.Year);
                            //RadDatePicker1.SelectedDate = selectedNextDay;
                            tbDate.Text = selectedNextDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            StartDate = (DateTime)Session["SCMonthStartDay"];
                            EndDate = GetMonthEndDate(StartDate);

                            if (sessionDate.Month != selectedNextDay.Month)
                            {
                                var newStartDay = GetMonthStartDate(selectedNextDay);
                                if (newStartDay.Month == selectedNextDay.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }


                                EndDate = GetMonthEndDate(StartDate);

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }

                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion

                            loadTree(false);
                        }

                        Session["SCMonthlyNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCMonthlyLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                                StartDate = GetMonthStartDate(DateTime.Today); // no change in start date...
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                                StartDate = GetMonthStartDate(selectedDate); // no change in start date...
                            }


                            EndDate = GetMonthEndDate(StartDate);

                            //if selected date is 27/7, next date should be 27/8 no matter current month has 30 / 31/ 28 / 29 days...
                            int days = DateTime.DaysInMonth(selectedDate.Year, selectedDate.Month);

                            var nextDate = selectedDate.AddDays(days);
                            Session["SCSelectedDay"] = nextDate;
                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(nextDate.Month) + " " + nextDate.Year);
                            //RadDatePicker1.SelectedDate = nextDate;
                            tbDate.Text = nextDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            Session["SCMonthStartDay"] = StartDate;





                            if (selectedDate.Month != nextDate.Month)
                            {
                                var newStartDay = GetMonthStartDate(nextDate);
                                if (newStartDay.Month == nextDate.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }

                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }
                                EndDate = GetMonthEndDate(StartDate);

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }
                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion

                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionDate = (DateTime)Session["SCSelectedDay"];

                            //if selected date is 27/7, next date should be 27/8 no matter current month has 30 / 31/ 28 / 29 days...
                            int days = DateTime.DaysInMonth(sessionDate.Year, sessionDate.Month);
                            var selectedNextDay = sessionDate.AddDays(days);
                            Session["SCSelectedDay"] = selectedNextDay;

                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedNextDay.Month) + " " + selectedNextDay.Year);
                            //RadDatePicker1.SelectedDate = selectedNextDay;
                            tbDate.Text = selectedNextDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            StartDate = (DateTime)Session["SCMonthStartDay"];
                            EndDate = GetMonthEndDate(StartDate);

                            if (sessionDate.Month != selectedNextDay.Month)
                            {
                                var newStartDay = GetMonthStartDate(selectedNextDay);
                                if (newStartDay.Month == selectedNextDay.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }


                                EndDate = GetMonthEndDate(StartDate);

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }

                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion

                            loadTree(false);
                        }

                        Session["SCMonthlyLastCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }

        protected void btnFirst_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCMonthlyFirstCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                                StartDate = GetMonthStartDate(DateTime.Today);
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                                StartDate = GetMonthStartDate(selectedDate); // no change in start date...
                            }


                            EndDate = GetMonthEndDate(StartDate);

                            //if selected date is 27/7, next date should be 27/8 no matter current month has 30 / 31/ 28 / 29 days...
                            int days = DateTime.DaysInMonth(selectedDate.Year, selectedDate.Month);

                            var nextDate = selectedDate.AddDays(-days);

                            if (!nextDate.Day.Equals(selectedDate.Day))
                            {
                                // var diff = selectedDate.Day - nextDate.Day;

                                var previousMonth = selectedDate.AddMonths(-1);
                                nextDate = previousMonth;

                                //nextDate = nextDate.AddDays(diff);
                            }

                            Session["SCSelectedDay"] = nextDate;
                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(nextDate.Month) + " " + nextDate.Year);
                            //RadDatePicker1.SelectedDate = nextDate;
                            tbDate.Text = nextDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            Session["SCMonthStartDay"] = StartDate;



                            if (selectedDate.Month != nextDate.Month)
                            {
                                var newStartDay = GetMonthStartDate(nextDate);
                                if (newStartDay.Month == nextDate.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }
                                EndDate = GetMonthEndDate(StartDate);

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }

                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionDate = (DateTime)Session["SCSelectedDay"];

                            //if selected date is 27/7, next date should be 27/8 no matter current month has 30 / 31/ 28 / 29 days...
                            int days = DateTime.DaysInMonth(sessionDate.Year, sessionDate.Month);

                            var selectedNextDay = sessionDate.AddDays(-days);
                            if (!selectedNextDay.Day.Equals(sessionDate.Day))
                            {

                                //var diff = sessionDate.Day - selectedNextDay.Day;
                                //selectedNextDay = selectedNextDay.AddDays(diff);

                                var previousMonth = sessionDate.AddMonths(-1);
                                selectedNextDay = previousMonth;
                            }

                            Session["SCSelectedDay"] = selectedNextDay;

                            lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedNextDay.Month) + " " + selectedNextDay.Year);
                            //RadDatePicker1.SelectedDate = selectedNextDay;
                            tbDate.Text = selectedNextDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            StartDate = (DateTime)Session["SCMonthStartDay"];
                            EndDate = GetMonthEndDate(StartDate);

                            if (sessionDate.Month != selectedNextDay.Month)
                            {
                                var newStartDay = GetMonthStartDate(selectedNextDay);
                                if (newStartDay.Month == selectedNextDay.Month)
                                {
                                    if (newStartDay.Day <= 7)
                                    {
                                        StartDate = newStartDay.AddDays(-7); // 3 months dates should be displayed in calendar....
                                    }
                                    else if (newStartDay.Day <= 14)
                                    {
                                        StartDate = newStartDay.AddDays(-14); // 3 months dates should be displayed in calendar....
                                    }
                                }
                                else
                                {
                                    StartDate = newStartDay;
                                }


                                EndDate = GetMonthEndDate(StartDate);

                                RadScheduler1.SelectedDate = StartDate;
                                RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                RadScheduler6.SelectedDate = StartDate.AddDays(35);

                                Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;

                            }
                            #region "No Past week"

                            if (chkNoPastWeeks.Checked)
                            {
                                NoPastWeeksCheck();
                            }
                            else
                            {
                                Session["chkNoPastWeeks"] = "false";
                                if (!chksunsat.Checked) // StartDay -> Monday
                                {
                                    Session["chksunsat"] = "false";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;


                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                                else //StartDay -> Sunday
                                {
                                    Session["chksunsat"] = "true";
                                    DateTime selectedDay = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                                    //StartDate = GetWeekStartDate(selectedDay);
                                    //Session["SCSelectedDay"] = selectedDay;

                                    StartDate = GetMonthStartDate(selectedDay);
                                    Session["SCSelectedDay"] = selectedDay;

                                    RadScheduler1.SelectedDate = StartDate;
                                    RadScheduler1.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler1.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler2.SelectedDate = StartDate.AddDays(7);
                                    RadScheduler2.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler2.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler3.SelectedDate = StartDate.AddDays(14);
                                    RadScheduler3.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler3.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler4.SelectedDate = StartDate.AddDays(21);
                                    RadScheduler4.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler4.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler5.SelectedDate = StartDate.AddDays(28);
                                    RadScheduler5.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler5.LastDayOfWeek = DayOfWeek.Sunday;

                                    RadScheduler6.SelectedDate = StartDate.AddDays(35);
                                    RadScheduler6.FirstDayOfWeek = DayOfWeek.Monday;
                                    RadScheduler6.LastDayOfWeek = DayOfWeek.Sunday;

                                    tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    //RadDatePicker1.SelectedDate = DateTime.Today;

                                    lbMonth.Text = System.Web.HttpUtility.HtmlEncode(DateTimeInfo.GetMonthName(selectedDay.Month) + " " + selectedDay.Year);

                                    Session["SCMonthStartDay"] = RadScheduler1.SelectedDate;
                                }
                            }

                            #endregion
                            loadTree(false);
                        }

                        Session["SCMonthlyFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }

        protected void RadScheduler1_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Redirect(e);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }
        protected void RadScheduler2_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Redirect(e);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }
        protected void RadScheduler3_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Redirect(e);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }
        protected void RadScheduler4_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Redirect(e);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }
        protected void RadScheduler5_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Redirect(e);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }
        protected void RadScheduler6_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Redirect(e);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }


        }

        protected void RadScheduler1_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RedirectToMenuFromTimeSlot(e);
                        return;
                        // Response.Redirect(e.MenuItem.Value);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }

        private void RedirectToMenuFromTimeSlot(Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {
            Session.Remove("CurrentPreFlightTrip");
            if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
            {// Fleet calendar entry

                RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start; // no need to pass tailNum since it has no resource
                RadWindow3.Visible = true;
                RadWindow3.VisibleOnPageLoad = true;
                return;
            }
            else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
            { // Crew calendar entry
                RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + null + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start;// no need to pass crewCode since it has no resource
                RadWindow4.Visible = true;
                RadWindow4.VisibleOnPageLoad = true;
                return;
            }
            else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
            {
                var tripnum = string.Empty;
                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
            }
            if (e.MenuItem.Value == "../PreFlightMain.aspx") // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
            {
                PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(e.TimeSlot.Start); //todo: get exact column date
                if (HomeBaseId != null && HomeBaseId != 0)
                {
                    /// 12/11/2012
                    ///Changes done for displaying the homebase of  login user
                    PreflightService.Company HomebaseSample = new PreflightService.Company();
                    HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                    HomebaseSample.BaseDescription = BaseDescription;
                    Trip.HomeBaseAirportICAOID = HomeBaseCode;
                    Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                    Trip.HomebaseID = HomeBaseId;
                    Trip.Company = HomebaseSample;
                }
                if (ClientId != null && ClientId != 0)
                {
                    Trip.ClientID = ClientId;
                    PreflightService.Client Client = new PreflightService.Client();
                    Client.ClientCD = ClientCode;
                    Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                    Trip.Client = Client;
                }

                if (e.MenuItem.Text == "Add New Trip")
                {
                    Session["CurrentPreFlightTrip"] = Trip;
                    Session.Remove("PreflightException");
                    Trip.State = TripEntityState.Added;
                    Trip.Mode = TripActionMode.Edit;
                }
                else if (Trip.TripNUM != null && Trip.TripNUM != 0)
                {
                    Session["CurrentPreFlightTrip"] = Trip;
                    Session.Remove("PreflightException");
                    Trip.State = TripEntityState.Added;
                    Trip.Mode = TripActionMode.Edit;
                }
                else
                {
                    Session["CurrentPreFlightTrip"] = null;
                    Session[WebSessionKeys.CurrentPreflightTrip] = null;
                    Session.Remove("PreflightException");
                }
                   
                Response.Redirect(e.MenuItem.Value);
            }
            else
            {
                Response.Redirect(e.MenuItem.Value);
            }
        }

        protected void RadScheduler2_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                { //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RedirectToMenuFromTimeSlot(e);
                        return;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }
        protected void RadScheduler3_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                { //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RedirectToMenuFromTimeSlot(e);
                        return;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }
        protected void RadScheduler4_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RedirectToMenuFromTimeSlot(e);
                        return;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }
        protected void RadScheduler5_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                { //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RedirectToMenuFromTimeSlot(e);
                        return;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }
        protected void RadScheduler6_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RedirectToMenuFromTimeSlot(e);
                        return;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }

        private void Redirect(Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove("CurrentPreFlightTrip");
                        if (e.MenuItem.Value == string.Empty)
                        {
                            return;
                        }

                        var appointment = (Telerik.Web.UI.Appointment)e.Appointment;
                        var appointmentNumber = appointment.ID;

                        //get trip by tripNumber
                        Int64 tripId = Convert.ToInt64(appointmentNumber);
                        if (tripId != 0)
                        {
                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                var trip = tripInfo.EntityList[0];
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                {
                                    //Set the trip.Mode and Trip.State to NoChage for read only
                                    using (var commonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = commonService.Lock(WebSessionKeys.PreflightMainLock, trip.TripID);
                                        trip.Mode =returnValue.ReturnFlag? TripActionMode.Edit:TripActionMode.NoChange;
                                    }
                                    Session["CurrentPreFlightTrip"] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");

                                    if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                                    {// Fleet calendar entry
                                        if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0)
                                        {
                                            string tailNumber = trip.Fleet != null ? trip.Fleet.TailNum : null;
                                            RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + tailNumber + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start; // no need to pass tailNum since it has no resource
                                        }
                                        else
                                            RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start; // no need to pass tailNum since it has no resource
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }

                                }
                                if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                                {
                                    //trip.Mode = TripActionMode.NoChange;
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;   
                                    RadWindow5.NavigateUrl = e.MenuItem.Value + "?IsCalender=1";
                                    RadWindow5.Visible = true;
                                    RadWindow5.VisibleOnPageLoad = true;
                                    RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                    return;
                                }
                                else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                {
                                    RedirectToPage(string.Format("{0}?xmlFilename=PRETSOverview.xml&tripnum={1}&tripid={2}", WebConstants.URL.TripSheetReportViewer, Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToString(trip.TripNUM)), Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToString(trip.TripID))));
                                }
                                else if (e.MenuItem.Text == "Leg Notes") //  Leg Notes menu selected...
                                {
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;
                                    RadWindow6.NavigateUrl = e.MenuItem.Value + "?IsCalender=1&Leg=" + radSelectedLegNum.Value;
                                    RadWindow6.Visible = true;
                                    RadWindow6.VisibleOnPageLoad = true;
                                    return;
                                }

                                else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                                { // Crew calendar entry
                                    RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + null + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start;// no need to pass crewCode since it has no resource
                                    RadWindow4.Visible = true;
                                    RadWindow4.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Add New Trip")// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                                {
                                    string strStartDate = radSelectedStartDate.Value;
                                    PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                    PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                    Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(strStartDate)); // todo: get column date
                                    if (trip.Fleet != null)
                                    {
                                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                                        Int64 HomebaseAirportId = 0;
                                        fleetsample.TailNum = trip.Fleet.TailNum;
                                        fleetsample.FleetID = trip.Fleet.FleetID;
                                        Trip.FleetID = fleetsample.FleetID;
                                        if (fleetsample.FleetID != null)
                                        {
                                            Int64 fleetHomebaseId = 0;
                                            Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                if (FleetList.Count > 0)
                                                {
                                                    GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                    if (FleetCatalogEntity.HomebaseID != null)
                                                    {
                                                        fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                        fleetsample.TypeDescription = FleetCatalogEntity.AircraftDescription;
                                                    }
                                                }
                                                var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                if (CompanyList.Count > 0)
                                                {
                                                    GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                    if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                    {
                                                        HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                        HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                    }
                                                }
                                                var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                if (AirportList.Count > 0)
                                                {
                                                    GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                    if (AirportCatalogEntity.IcaoID != null)
                                                    {
                                                        Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                    }
                                                    if (AirportCatalogEntity.AirportID != null)
                                                    {
                                                        Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                    }
                                                }

                                            }
                                            Trip.HomebaseID = fleetHomebaseId;
                                            Trip.Company = HomebaseSample;
                                        }
                                    }
                                    Trip.Fleet = fleetsample;

                                    PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();

                                   if (HomeBaseId != null && HomeBaseId != 0 && Trip.HomebaseID <= 0)
                                    {
                                        /// 12/11/2012
                                        ///Changes done for displaying the homebase of login user
                                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                                        HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                        HomebaseSample.BaseDescription = BaseDescription;
                                        Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                        Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                        Trip.HomebaseID = HomeBaseId;
                                        Trip.Company = HomebaseSample;
                                    }
                                    if (ClientId != null && ClientId != 0)
                                    {
                                        Trip.ClientID = ClientId;
                                        PreflightService.Client Client = new PreflightService.Client();
                                        Client.ClientCD = ClientCode;
                                        Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                        Trip.Client = Client;
                                    }
                                    Trip.Fleet = null;
                                    Session["CurrentPreFlightTrip"] = Trip;
                                    Session.Remove("PreflightException");
                                    Trip.State = TripEntityState.Added;
                                    Trip.Mode = TripActionMode.Edit;
                                    Response.Redirect(e.MenuItem.Value);

                                    /*if (Trip.TripNUM != null && Trip.TripNUM != 0)
                                    {
                                        Session["CurrentPreFlightTrip"] = Trip;
                                        Session.Remove("PreflightException");
                                        Trip.State = TripEntityState.Added;
                                        Trip.Mode = TripActionMode.Edit;
                                        Response.Redirect(e.MenuItem.Value);
                                    }
                                    else
                                    {
                                        Session["CurrentPreFlightTrip"] = null;
                                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                    }*/
                                }
                                else
                                {
                                    if (trip.TripNUM != null && trip.TripNUM != 0)
                                    {
                                        /*trip.State = TripEntityState.Added;
                                        trip.Mode = TripActionMode.NoChange;
                                        trip.Mode = TripActionMode.Edit; 
                                        Session["CurrentPreFlightTrip"] = trip;
                                        Session.Remove("PreflightException"); */
                                        //trip.Mode = TripActionMode.Edit;
                                        //trip.State = TripEntityState.Modified;
                                        string url = e.MenuItem.Value;
                                        string menuItemText = e.MenuItem.Text;
                                        radSelectedLegNum.Value = (((!string.IsNullOrWhiteSpace(radSelectedLegNum.Value)) && radSelectedLegNum.Value != "0") ? radSelectedLegNum.Value : "1");
                                        if (menuItemText == "FBO" || menuItemText == "Catering")
                                            url += "&legNo=" + radSelectedLegNum.Value + "#" + menuItemText;
                                        else if (menuItemText == "Crew Hotel" || menuItemText == "PAX Hotel")
                                            url += "&subTab=1&legNo=" + radSelectedLegNum.Value;
                                        else if (menuItemText == "Crew Transport" || menuItemText == "PAX Transport")
                                            url += "&subTab=1&legHotel=1&legNo=" + radSelectedLegNum.Value;
                                         if (Session[WebSessionKeys.CurrentPreflightTrip] == null)
                                        {
                                            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                                            pfViewModel = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.InjectPreflightMainToPreflightTripViewModel(trip, pfViewModel);
                                            Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                                        }
                                        Response.Redirect(url, false);                     
                                    }
                                    else
                                    {
                                        Session["CurrentPreFlightTrip"] = null;
                                        Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                    }
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }
        }

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.Month);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Month);
                }
            }

        }




        protected void RadScheduler1_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
        {

            //Set the CssClass property to visually distinguish your special days. 
            if (e.TimeSlot.Appointments.Count > 1)
            {
                e.TimeSlot.CssClass = "Disabled";

            }

        }

        protected void RadScheduler2_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
        {

            //Set the CssClass property to visually distinguish your special days. 
            if (e.TimeSlot.Appointments.Count > 1)
            {
                e.TimeSlot.CssClass = "Disabled";

            }

        }
        protected void RadScheduler3_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
        {

            //Set the CssClass property to visually distinguish your special days. 
            if (e.TimeSlot.Appointments.Count > 1)
            {
                e.TimeSlot.CssClass = "Disabled";

            }

        }
        protected void RadScheduler4_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
        {

            //Set the CssClass property to visually distinguish your special days. 
            if (e.TimeSlot.Appointments.Count > 1)
            {
                e.TimeSlot.CssClass = "Disabled";

            }

        }
        protected void RadScheduler5_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
        {

            //Set the CssClass property to visually distinguish your special days. 
            if (e.TimeSlot.Appointments.Count > 1)
            {
                e.TimeSlot.CssClass = "Disabled";

            }

        }
        protected void RadScheduler6_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
        {

            //Set the CssClass property to visually distinguish your special days. 
            if (e.TimeSlot.Appointments.Count > 1)
            {
                e.TimeSlot.CssClass = "Disabled";

            }

        }
    }
}
