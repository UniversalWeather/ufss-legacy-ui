﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusinessWeekFullView.aspx.cs" ClientIDMode="AutoID"
    EnableViewState="true" MasterPageFile="~/Framework/Masters/SiteFV.master" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.BusinessWeekFullView" %>

<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" runat="server" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Scripts/stylefixes.css" rel="stylesheet" type="text/css" />
    <style>
        .rsContentScrollArea{max-height: 97% !important;}
    </style>
</asp:Content>
<asp:Content ID="SiteBodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadScriptManager ID="RadScriptManager2" runat="server" />
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock2">
                    <!-- Start: Ipad Fix -->
                    <script type="text/javascript">
                        var lastArgs = null;
                        var lastContext = null;
                        var longTouchID = 0;
                        var menuShown = false;
                        function longTouch() {
                            longTouchID = 0;
                            menuShown = true;
                            
                            var scheduler = $find("<%=RadBusinessWeekScheduler.ClientID %>");
                            var eventArgs = null;
                            var target = null;
                            
                            if (lastContext.target.nodeName == 'TD' && lastContext.target.outerHTML.indexOf('rsWrap rsLastSpacingWrapper') != -1) {

                                target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);

                                eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);

                                scheduler._raiseTimeSlotContextMenu(eventArgs);


                            } 
                            //Need to Check
                            else if (lastContext.target.nodeName == 'TD' || lastContext.target.nodeName == 'DIV') {

                                target = scheduler.getAppointmentFromDomElement(lastContext.target);
                                eventArgs = new Telerik.Web.UI.SchedulerAppointmentContextMenuEventArgs(target, lastContext);
                                scheduler._raiseAppointmentContextMenu(eventArgs);

                            }
                        }

                        function handleTouchStart(e) {

                            lastContext = e;
                            lastContext.target = e.originalTarget;
                            lastContext.pageX = e.changedTouches[0].pageX;
                            lastContext.pageY = e.changedTouches[0].pageY;
                            lastContext.clientX = e.changedTouches[0].clientX;
                            lastContext.clientY = e.changedTouches[0].clientY;
                            ID = setTimeout(longTouch(), 1000);

                        }

                        function handleClick(e) {
                            if (menuShown) {
                                menuShown = false;
                                document.body.removeEventListener('click', handleClick, true);
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }

                        function handleTouchEnd(e) {
                            if (longTouchID != 0)
                                clearTimeout(longTouchID);
                            if (menuShown) {
                                document.body.addEventListener('click', handleClick, true);
                                e.preventDefault();
                            }
                        }

                        function pageLoad1() {
                            if ($telerik.isMobileSafari) {
                                var scrollArea = $get('<%= RadBusinessWeekScheduler.ClientID%>');
                                scrollArea.addEventListener('touchstart', handleTouchStart, false);
                                scrollArea.addEventListener('touchend', handleTouchEnd, false);
                            }
                        }
                    </script>
                    <!-- End: Ipad Fix -->
                    <script type="text/javascript">

                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }

                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                        function openWinAdv() {
                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }
                        function openWinFleetCalendarEntries() {
                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }
                        function openWinLegNotes() {

                            var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                        }
                        //To enable and disable rad context menu item based on appointment...
                        function RadBusinessWeekScheduler_OnClientAppointmentContextMenu(sender, eventArgs) {

                            var target = eventArgs.get_appointment();
                            var data = target._serverData;
                            var recordType = data.attributes.RecordType;
                            var isLoggedToPostFlight = data.attributes.IsLog;

                            var menu = $find('<%=RadSchedulerContextMenu1.ClientID %>');

                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLoggedToPostFlight == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + data.attributes.TripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + data.attributes.TripId + "&legNum=" + data.attributes.LegNum);
                                } else {  // else, hide
                                    menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu._findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu._findItemByText("Postflight Flight Logs").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu.findItemByValue("Separator").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu._findItemByText("Crew").show();
                                menu._findItemByText("PAX").show();
                                menu._findItemByText("Outbound Instruction").show();
                                menu._findItemByText("Logistics").show();
                                menu._findItemByText("Leg Notes").show();

                                if (data.attributes.ShowPrivateTripMenu == "False") {
                                    if (data.attributes.IsPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }

                            } else {
                                // hide postflight log menu items
                                menu._findItemByText("Postflight Flight Logs").hide();
                                menu._findItemByText("Flight Log Legs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu._findItemByText("Crew").hide();
                                menu._findItemByText("PAX").hide();
                                menu._findItemByText("Outbound Instruction").hide();
                                menu._findItemByText("Logistics").hide();
                                menu._findItemByText("Leg Notes").hide();
                                menu._findItemByText("UWA Services").show();
                                menu._findItemByText("Preflight").show();
                                menu._findItemByText("Add New Trip").show();
                                menu._findItemByText("Fleet Calendar Entry").show();
                                menu._findItemByText("Crew Calendar Entry").show();

                            }
                        }


                        //On click of menu item on RightClick Context menu of Scheduler (not appointment)..., to get time slot details and redirect

                        function RadBusinessWeekScheduler_OnClientTimeSlotContextMenuItemClicking(sender, eventArgs) {

                            showPleaseWait();

                        }

                        function RadBusinessWeekScheduler_OnClientAppointmentContextMenuItemClicking(sender, args) {
                            var target = args.get_appointment();
                            var resource = target._serializedResources;
                            var hdnLegNum = $get('<%=radSelectedLegNum.ClientID %>');
                            var hdnStartDate = $get('<%=radSelectedStartDate.ClientID %>');
                            hdnLegNum.value = "1";
                            if (resource != null && resource.length > 0) {
                                hdnLegNum.value = resource[0].text;
                                if (resource.length > 1) {
                                    hdnStartDate.value = resource[1].text;
                                }
                            }
                            showPleaseWait();
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }

                        function openWinOutBoundInstructions() {
                            var oWnd = radopen("../PreflightOutboundInstruction.aspx?IsCalender=1", "RadWindow5");
                        }

                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow;
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                            return oWindow;
                        }

                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }

                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                       
                    </script>
                    <script type="text/javascript">
                        //Rad Date Picker
                        var currentTextBox = null;
                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox
                        function showPopup(sender, e) {
                            //this is a reference to the texbox which raised the event
                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate
                            //the selection of a date
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                            //this variable is used to store a reference to the date picker, which is currently
                            //active
                            currentDatePicker = datePicker;

                            //this method first parses the date, that the user entered or selected, and then
                            //sets it as a selected date to the picker
                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                                //value of the picker
                                //currentTextBox.value = args.get_newValue();

                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");

                                }
                                else
                                { return false; }
                            }
                        }

                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {
                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                sender.value = formattedDate;
                            }
                        }
                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }

                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }
                            // allow dot, slash and hypen characters... if any other character, throw alert
                            return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                        }

                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                        //Fleet tree view...

                        function FleetTreeView_NodeCheck(sender, args) {

                            var treeView = $find("<%= FleetTreeView.ClientID %>");

                            var isChecked = args.get_node().get_checked();

                            var crewTree = $find("<%= CrewTreeView.ClientID %>");
                            if (isChecked) {

                                for (var i = 0; i < crewTree.get_nodes().get_count(); i++) {
                                    var node = crewTree.get_nodes().getNode(i);
                                    node.set_checked(false);
                                }
                                // crewTree.unselectAllNodes();
                            }
                        }

                        // Crew tree view...
                        function CrewTreeView_NodeCheck(sender, args) {

                            var treeView = $find("<%= CrewTreeView.ClientID %>");

                            var isChecked = args.get_node().get_checked();

                            var fleetTree = $find("<%= FleetTreeView.ClientID %>");
                            if (isChecked) {

                                for (var i = 0; i < fleetTree.get_nodes().get_count(); i++) {
                                    var node = fleetTree.get_nodes().getNode(i);
                                    node.set_checked(false);
                                }
                                // fleetTree.unselectAllNodes();
                            }
                        }
                    </script>
     
                    <script type="text/javascript">
        function SelectTab()   
    {   
        var myTab = <%= TabSchedulingCalendar.ClientID %>.FindTabByValue("Business Week");   
        myTab.Select();   
    }   
                    </script>
                    <script type="text/javascript">
        function SelectfiltercriteriaTab()   
    {   
        var myTab = <%= FilterCriteria.ClientID %>.FindTabByValue("Week");   
        myTab.Selectfiltercriteria();   
    }   
                    </script>

                     <script  type="text/javascript">
                         function pageLoad() {
                             $telerik.$(".rsHorizontalHeaderTable th").each(function (index, item) {
                                 var childNode = item.childNodes[0];
                                 if (childNode != null) {
                                     var today = new Date();
                                     today.setHours(0); today.setMinutes(0); today.setSeconds(0);
                                     if (childNode.href != null) {
                                         var hrefDate = childNode.href.split('#')[1];
                                         var date = new Date(hrefDate);
                                         date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                         if (date.toDateString() == today.toDateString()) {
                                             item.style.backgroundImage = "none";
                                             item.style.backgroundColor = "yellow";
                                         }
                                     } else {
                                         var innerText = childNode.innerHTML.trim();
                                         var date = new Date(innerText);
                                         date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                         if (date.toDateString() == today.toDateString()) {
                                             item.style.backgroundImage = "none";
                                             item.style.backgroundColor = "yellow";
                                         }
                                     }
                                 }
                             });
                             pageLoad1();
                             setrsRowHeight();
                         }                    
                    </script>
                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager2" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/FleetCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/LegNotesPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <%--<div class="globalMenu_mapping">
        <a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
        <a href="#" class="globalMenu_mapping">Preflight</a> <span class="breadcrumbCaret">
        </span><a href="#" class="globalMenu_mapping">Scheduling Calendar</a>
    </div>--%>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content fullview fullviewbw" style="width:100%; height:100%">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="radSelectedLegNum" name="radSelectedLegNum" runat="server" />
        <input type="hidden" id="radSelectedStartDate" name="radSelectedStartDate" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0" style="width:100%; height:100%;vertical-align:top;" >
            <tr>
                <td width="80%" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel170">
                    <div class="sc_date_display">
                        <span>
                            <asp:Label ID="lbStartDate" runat="server" CssClass="date"></asp:Label></span><span
                                class="padleft_5"><asp:Label ID="lbdate" runat="server" CssClass="date" Text=" - "></asp:Label></span>
                        <span class="padleft_5">
                            <asp:Label ID="lbEndDate" runat="server" CssClass="date"></asp:Label></span>
                    </div>
                </td>
                <td align="left" class="defaultview"  style='display:none';>
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        onclick="javascript:showPleaseWait();" OnCheckedChanged="ChkDefaultView_OnCheckedChanged" />
                </td>
                <td align="center" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
               <td align="right" class="tdLabel20">
                    <span><a class="refresh-icon" onclick="Refresh();" href="#" title="Refresh"></a></span>                         
                </td>
                <td class="tdLabel50" align="left">                   
                          <asp:Button ID="btnClose" runat="server" CssClass="ui_nav" Text="Close" ToolTip="Close" OnClientClick="javascript:window.close();" />
                </td>
            </tr>
            <tr  style="width:100%; height:97%;vertical-align:top;">
                <td colspan="6"  width="95%"  style="height:98%;vertical-align:top;">
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0" Width="100%"
                        Skin="Windows7" height="97%">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="None" Width="100%" Height="98%">
                            <div class="sc_navigation">
                                <div class="sc_navigation_right">
                                    <span class="act-btn"><span>
                                        <asp:Button ID="btnFirst" runat="server" ToolTip="Go Back 15 Days" CssClass="icon-first_sc"
                                            OnClick="btnFirst_click" OnClientClick="showPleaseWait();" /></span> <span>
                                                <asp:Button ID="prevButton" runat="server" OnClick="prevButton_Click" CssClass="icon-prev_sc"
                                                    ToolTip="Go Back 1 Day" OnClientClick="showPleaseWait();" /></span>
                                        <span>
                                            <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" OnClientClick="showPleaseWait();"
                                                onfocus="showPopup(this, event);" onKeyPress="return ontbDateKeyPress(this, event);"
                                                onkeydown="return tbDate_OnKeyDown(this, event);" onBlur="parseDate(this, event);"
                                                MaxLength="10"></asp:TextBox></span> <span>
                                                    <asp:Button ID="nextButton" runat="server" OnClick="nextButton_Click" CssClass="icon-next_sc"
                                                        OnClientClick="showPleaseWait();" ToolTip="Go Forward 1 Day" /></span>
                                        <span>
                                            <asp:Button ID="btnPrevious" runat="server" CssClass="icon-last_sc" OnClick="btnLast_click"
                                                OnClientClick="showPleaseWait();" ToolTip="Go Forward 15 Days" /></span></span>
                                    <span>
                                        <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                            OnClientClick="showPleaseWait();" /></span>
                                </div>
                            </div>
                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="None" runat="server" Height="98%">
                                <Items>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                                <asp:Panel ID="pnlBusinessWeekScheduler" runat="server"  style="height:97%;">
                                                    <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadBusinessWeekScheduler" style='overflow:auto;'
                                                        AllowEdit="true" AllowInsert="true" DataEndField="End" DataKeyField="TripId"
                                                        DataStartField="Start" DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                        EnableAdvancedForm="true" OverflowBehavior="Scroll" SelectedView="TimelineView"
                                                        Skin="Windows7" ShowHeader="false" AppointmentStyleMode="Default"
                                                        OnAppointmentDataBound="RadBusinessWeekScheduler_AppointmentDataBound" 
                                                        ColumnWidth="125px" RowHeight="170px"
                                                        Localization-HeaderToday="Today" CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu"
                                                        OnClientAppointmentContextMenu="RadBusinessWeekScheduler_OnClientAppointmentContextMenu"
                                                        OnAppointmentContextMenuItemClicked="RadBusinessWeekScheduler_OnAppointmentContextMenuItemClicked"
                                                        OnTimeSlotContextMenuItemClicked="RadBusinessWeekScheduler_OnTimeSlotContextMenuItemClicked"
                                                        OnClientTimeSlotContextMenuItemClicking="RadBusinessWeekScheduler_OnClientTimeSlotContextMenuItemClicking"
                                                        OnClientAppointmentContextMenuItemClicking="RadBusinessWeekScheduler_OnClientAppointmentContextMenuItemClicking">
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AdvancedForm Modal="true" />
                                                        <Reminders Enabled="true" />
                                                        <ResourceHeaderTemplate>
                                                            <div style="text-align: left">
                                                                <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                    Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                                <br />
                                                                <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                    ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                    Font-Size="Smaller" />
                                                            </div>
                                                        </ResourceHeaderTemplate>
                                                        <%--   <ResourceStyles>
                                                        <telerik:ResourceStyleMapping Type="CrewFleetCodes" />
                                                    </ResourceStyles>--%>
                                                        <TimelineView UserSelectable="false" NumberOfSlots="15" GroupingDirection="Vertical"
                                                            ShowInsertArea="false" />
                                                        <%--     <WeekView UserSelectable="true" GroupBy="CrewFleetCodes" GroupingDirection="Vertical" />--%>
                                                        <TimeSlotContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadBusinessWeekSchedulerContextMenu" runat="server">
                                                               <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                        Enabled="false" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <%--<telerik:RadMenuItem Text="Tripsheet Report Writer" />--%>
                                                                    <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                    <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </TimeSlotContextMenus>
                                                        <AppointmentContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu1" runat="server">
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </AppointmentContextMenus>
                                                        <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AppointmentTemplate>
                                                            <div style='height:740px;'>
                                                                <%# Eval("Subject")%></div>
                                                            <div style="font-style: italic;" style='height:740px;'>
                                                                <%# Eval("Description") %></div>
                                                        </AppointmentTemplate>
                                                    </telerik:RadScheduler>
                                                </asp:Panel>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward" Visible="false" style='display:none;'  />
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="None" Width="250px" Collapsed="true" style='display:none;'>
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="618px" ExpandMode="FullExpandedItem">
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="FleetTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="FleetTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="CrewTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Expanded="True" Text="Filter Criteria" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                            <td align="right">
                                                                <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                    OnClientClick="javascript:openWinAdv();return false;" />
                                                            </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </td>
            </tr>
        </table>
    </div>
     <script type="text/javascript">
         SetBrowserDetails();
         $(window).resize(function () {
             setrsRowHeight();
         });

         function setrsRowHeight(){
             var scheduler = $find('<%=RadBusinessWeekScheduler.ClientID %>');
             var rowCount = 1;
             var schHeight=$(window).height();
             setFullViewSchedulerRowColumnSize(scheduler, rowCount, schHeight);
             $('.rsContentScrollArea').height(schHeight-20);
             $('.rsContentScrollArea').css('height', parseInt(schHeight-20).toString() + 'px !important');

             if ($.browser != typeof(undefined)) {
                 if ($.browser.name != typeof(undefined)) {
                     if ($.browser.name == 'Safari') {
                         $('.rsContentScrollArea').css('height', '100% !important');
                     }
                 }
             }

         }
     </script>
</asp:Content>
