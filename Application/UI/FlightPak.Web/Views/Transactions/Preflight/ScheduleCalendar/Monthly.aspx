﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Monthly.aspx.cs" ClientIDMode="AutoID"
    EnableViewState="true" MasterPageFile="~/Framework/Masters/Site.master" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.Monthly" %>

<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Scripts/stylefixes.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../../Scripts/common-calendar.js"></script>
</asp:Content>
<asp:Content ID="SiteBodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock2">
                    <!-- Start: Ipad Fix -->
                    <script type="text/javascript">
                        
                        function pageLoad1() {
                            if ($telerik.isMobileSafari) {
                                var scrollArea1 = $get('<%= RadScheduler1.ClientID%>');
                                scrollArea1.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollArea2 = $get('<%= RadScheduler2.ClientID%>');
                                scrollArea2.addEventListener('touchstart', handleTouchStart, false);

                                var scrollArea3 = $get('<%= RadScheduler3.ClientID%>');
                                scrollArea3.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollArea4 = $get('<%= RadScheduler4.ClientID%>');
                                scrollArea4.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollArea5 = $get('<%= RadScheduler5.ClientID%>');
                                scrollArea5.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollArea6 = $get('<%= RadScheduler6.ClientID%>');
                                scrollArea6.addEventListener('touchstart', handleTouchStart, false);
                                
                                HandleIpadTouchCalendarContextMenu();
                            }
                        }
                    </script>
                    <!-- End: Ipad Fix -->
                    <script type="text/javascript">


                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }

                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                        function openWinMonthlyFullView() {

                            //window.open("MonthlyFullView.aspx", "Default", "maximize=true, fullscreen=yes, channelmode=yes, toolbar=no, menubar=no, statusbar=no, titlebar=no, directories=no, resizable=yes, status=no, scrollbars=auto, close=yes", true);
                            //window.open("MonthlyFullView.aspx", "Default", "maximize=true, fullscreen=yes, channelmode=yes, toolbar=no, menubar=no, statusbar=no, titlebar=no, directories=no, resizable=yes, status=no, scrollbars=auto, close=yes");
                            window.open("MonthlyFullView.aspx", "Default", "fullscreen=yes, directories=no, resizable=yes, scrollbars=auto, close=no");
                            //window.open("MonthlyFullView.aspx", "Default", " maximize=true, fullscreen=1, channelmode=yes, visibletoolbar=false, visiblemenubar=no, visibledirectories=no, resizable=yes, visiblestatusbar=no, scrollbars=auto, close=yes");                             
                            //var obj = new ActiveXObject("Wscript.shell");
                            //obj.SendKeys("{f11}");
                        }

                        function openWinAdv() {
                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }
                        function openWinFleetCalendarEntries() {
                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }

                        function openWinLegNotes() {
                            var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                        }
                        //To enable and disable rad context menu item based on appointment...

                        function RadScheduler1_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadSchedulerContextMenu1.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);

                        }
                        function RadScheduler2_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadSchedulerContextMenu4.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);

                        }
                        function RadScheduler3_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadSchedulerContextMenu5.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);

                        }
                        function RadScheduler4_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadSchedulerContextMenu7.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);

                        }
                        function RadScheduler5_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadSchedulerContextMenu9.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);

                        }
                        function RadScheduler6_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadSchedulerContextMenu11.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);

                        }
                        function ShowAndHideContextMenuItem(sender, eventArgs, menu) {
                            hideActiveToolTip();
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var target = eventArgs.get_appointment();
                            var data = target._serverData;
                            var recordType = data.attributes.RecordType;
                            var isLoggedToPostFlight = data.attributes.IsLog;

                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLoggedToPostFlight == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + data.attributes.TripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + data.attributes.TripId + "&legNum=" + data.attributes.LegNum);
                                } else {  // else, hide
                                    menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu._findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu._findItemByText("Postflight Flight Logs").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu.findItemByValue("Separator").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu._findItemByText("Crew").show();
                                menu._findItemByText("PAX").show();
                                menu._findItemByText("Outbound Instruction").show();
                                menu._findItemByText("Logistics").show();
                                menu._findItemByText("Leg Notes").show();

                                if (data.attributes.ShowPrivateTripMenu == "False") {
                                    if (data.attributes.IsPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }

                            } else {
                                // hide postflight log menu items
                                menu._findItemByText("Postflight Flight Logs").hide();
                                menu._findItemByText("Flight Log Legs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu._findItemByText("Crew").hide();
                                menu._findItemByText("PAX").hide();
                                menu._findItemByText("Outbound Instruction").hide();
                                menu._findItemByText("Logistics").hide();
                                menu._findItemByText("Leg Notes").hide();
                                menu._findItemByText("UWA Services").show();
                                menu._findItemByText("Preflight").show();
                                menu._findItemByText("Add New Trip").show();
                                menu._findItemByText("Fleet Calendar Entry").show();
                                menu._findItemByText("Crew Calendar Entry").show();

                            }
                        }

                        //On click of menu item on RightClick Context menu   of Scheduler (not appointment)..., to get time slot details and redirect

                        function RadScheduler1_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();

                        }
                        function RadScheduler2_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();

                        }
                        function RadScheduler3_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();

                        }
                        function RadScheduler4_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();

                        }
                        function RadScheduler5_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();

                        }
                        function RadScheduler6_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();

                        }

                        function RadScheduler1_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                            showPleaseWait();
                        }
                        function RadScheduler2_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                            showPleaseWait();
                        }
                        function RadScheduler3_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                            showPleaseWait();
                        }
                        function RadScheduler4_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                            showPleaseWait();
                        }
                        function RadScheduler5_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                            showPleaseWait();
                        }
                        function RadScheduler6_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                            showPleaseWait();
                        }

                        function getAppoinmentLegNum(eventArgs)
                        {
                            var target = eventArgs.get_appointment();
                            var resource = target._serializedResources;
                            var hdnLegNum = $get('<%=radSelectedLegNum.ClientID %>');
                            var hdnStartDate = $get('<%=radSelectedStartDate.ClientID %>');
                            hdnLegNum.value = "1";
                            if (resource != null && resource.length > 0) {
                                hdnLegNum.value = resource[0].text;
                                if (resource.length > 1) {
                                    hdnStartDate.value = resource[1].text;
                                }
                            }
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }
                        function openWinOutBoundInstructions() {
                            var oWnd = radopen("../PreflightOutboundInstruction.aspx", "RadWindow5");
                        }

                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow;
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                            return oWindow;
                        }
                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");


                        }

                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");

                        }
                       
                    </script>
                    <script type="text/javascript">
                        //Rad Date Picker
                        var currentTextBox = null;
                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox
                        function showPopup(sender, e) {
                            //this is a reference to the texbox which raised the event
                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate
                            //the selection of a date
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                            //this variable is used to store a reference to the date picker, which is currently
                            //active
                            currentDatePicker = datePicker;

                            //this method first parses the date, that the user entered or selected, and then
                            //sets it as a selected date to the picker
                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                                //value of the picker
                                //currentTextBox.value = args.get_newValue();

                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");

                                }
                                else
                                { return false; }
                            }
                        }

                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {
                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                sender.value = formattedDate;
                            }
                        }
                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }

                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }
                            // allow dot, slash and hypen characters... if any other character, throw alert
                            return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                        }
                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                    </script>
                    <!-- Start: Tooltip integration -->
                    <script type="text/javascript">
        //<![CDATA[
                        function hideActiveToolTip() {
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            //                            if (tooltip) {
                            //                                tooltip.hide();
                            //                                //  alert("1");
                            //                            }
                        }
                        // Rad Schedular-6
                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadScheduler6') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow(sender, args) {
                            var chk = $('#<%=hdnEnableHoverDisplay.ClientID%>').val();
                            if (chk == "True"){                    
                                args.set_cancel(false);
                            }
                            else{
                                args.set_cancel(true);
                            }
                            var menu = $find('<%=RadSchedulerContextMenu11.ClientID %>');
                            menu.hide();


                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler6.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }

                        function OnClientRequestStart(sender, args) {

                            args.set_cancel(true);
                           
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler6.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()

                                tooltip.set_text($get("contentContainer").innerHTML);

                            }
                        }

                        // Rad Schedular-5
                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler1(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadScheduler5') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow1(sender, args) {
                            var chk = $('#<%=hdnEnableHoverDisplay.ClientID%>').val();
                            if (chk == "True"){                    
                                args.set_cancel(false);
                            }
                            else{
                                args.set_cancel(true);
                            }
                            var menu = $find('<%=RadSchedulerContextMenu9.ClientID %>');
                            menu.hide();
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler5.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }

                        function OnClientRequestStart1(sender, args) {

                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler5.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()


                                tooltip.set_text($get("contentContainer").innerHTML);
                            }
                        }

                        // Rad Schedular-4 tooltip.hide();
                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler1(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadScheduler4') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow2(sender, args) {
                            var chk = $('#<%=hdnEnableHoverDisplay.ClientID%>').val();
                            if (chk == "True"){                    
                                args.set_cancel(false);
                            }
                            else{
                                args.set_cancel(true);
                            }
                            var menu = $find('<%=RadSchedulerContextMenu7.ClientID %>');
                            menu.hide();
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler4.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }

                        function OnClientRequestStart2(sender, args) {

                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler4.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()

                                tooltip.set_text($get("contentContainer").innerHTML);
                            }
                        }


                        // Rad Schedular-3
                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler1(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadScheduler3') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow3(sender, args) {
                            var chk = $('#<%=hdnEnableHoverDisplay.ClientID%>').val();
                            if (chk == "True"){                    
                                args.set_cancel(false);
                            }
                            else{
                                args.set_cancel(true);
                            }
                            var menu = $find('<%=RadSchedulerContextMenu5.ClientID %>');
                            menu.hide();
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler3.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }


                        function OnClientRequestStart3(sender, args) {

                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler3.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()

                                tooltip.set_text($get("contentContainer").innerHTML);
                            }
                        }


                        // Rad Schedular-2
                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler1(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadScheduler2') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow4(sender, args) {
                            var chk = $('#<%=hdnEnableHoverDisplay.ClientID%>').val();
                            if (chk == "True"){                    
                                args.set_cancel(false);
                            }
                            else{
                                args.set_cancel(true);
                            }
                            var menu = $find('<%=RadSchedulerContextMenu4.ClientID %>');
                            menu.hide();
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler2.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }

                        function OnClientRequestStart4(sender, args) {

                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler2.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()

                                tooltip.set_text($get("contentContainer").innerHTML);
                            }
                        }

                        // Rad Schedular-1
                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler1(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadScheduler1') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShow5(sender, args) {
                            var chk = $('#<%=hdnEnableHoverDisplay.ClientID%>').val();
                            if (chk == "True"){                    
                                args.set_cancel(false);
                            }
                            else{
                                args.set_cancel(true);
                            }
                            var menu = $find('<%=RadSchedulerContextMenu1.ClientID %>');
                            menu.hide();
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler1.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }


                        function OnClientRequestStart5(sender, args) {

                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadScheduler1.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()

                                tooltip.set_text($get("contentContainer").innerHTML);
                            }
                        }



                        function OnClientAppointmentContextMenu(sender, args) {
                            hideActiveToolTip();
                        }
        //]]>
                    </script>
                    <!-- End: Tooltip integration -->
                    <script type="text/javascript">
        function SelectfiltercriteriaTab()   
    {   
        var myTab = <%= FilterCriteria.ClientID %>.FindTabByValue("Week");   
        myTab.Selectfiltercriteria();   
    }   

     //Fleet tree view...

                    function FleetTreeView_NodeCheck(sender, args) {
                        var treeView = $find("<%= FleetTreeView.ClientID %>");
                        var isChecked = args.get_node().get_checked();
                        var valueChecked = args.get_node().get_text();
                        var crewTree = $find("<%= CrewTreeView.ClientID %>");
                        CalendarFleetCrewTreeViewCheck(isChecked, treeView, crewTree);
                        CheckDuplicates(isChecked,valueChecked,treeView);
                    }

                    // Crew tree view...
                    function CrewTreeView_NodeCheck(sender, args) {
                        var treeView = $find("<%= CrewTreeView.ClientID %>");
                        var isChecked = args.get_node().get_checked();
                        var valueChecked = args.get_node().get_text();
                        var fleetTree = $find("<%= FleetTreeView.ClientID %>");
                        CalendarFleetCrewTreeViewCheck(isChecked, treeView, fleetTree);
                        CheckDuplicates(isChecked,valueChecked,treeView);
                    }
             
                    </script>
                    <script type="text/javascript">
                        function pageLoad() {
                            $telerik.$(".rsHorizontalHeaderTable th").each(function (index, item) {
                                var childNode = item.childNodes[0];
                                if (childNode != null) {
                                    var today = new Date();
                                    today.setHours(0); today.setMinutes(0); today.setSeconds(0);
                                    if (childNode.href != null) {
                                        var hrefDate = childNode.href.split('#')[1];
                                        var date = new Date(hrefDate);
                                        date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                        if (date.toDateString() == today.toDateString()) {
                                            item.style.backgroundImage = "none";
                                            item.style.backgroundColor = "yellow";
                                        }
                                    } else {
                                        var innerText = childNode.innerHTML.trim();
                                        var date = new Date(innerText);
                                        date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                        if (date.toDateString() == today.toDateString()) {
                                            item.style.backgroundImage = "none";
                                            item.style.backgroundColor = "yellow";
                                        }
                                    }
                                }
                            });
                            pageLoad1();
                        }                    
                        
                        var agt = navigator.userAgent.toLowerCase();
                        if (agt.indexOf("chrome") != -1) 
                        {
                            Telerik.Web.UI.RadScheduler.prototype.saveClientState = function () {
                                return '{"scrollTop":' + Math.round(this._scrollTop) + ',"scrollLeft":' + Math.round(this._scrollLeft) + ',"isDirty":' + this._isDirty + '}';
                            }
                        }

                    </script>
                    <style type="text/css">
                        .rwStatusbarRow
                        {
                            display: none !important;
                        }
                        .rwAddressbarRow
                        {
                            display: none !important;
                        }
                        
                        
                        /* iPad scrollbar fix */@media all and (device-width: 768px) and (device-height: 1024px) and (orientation:portrait)
                        {
                            /* your css rules for ipad portrait */    ::-webkit-scrollbar
                            {
                                -webkit-appearance: none;
                                width: 7px;
                            }
                            ::-webkit-scrollbar-thumb
                            {
                                border-radius: 4px;
                                background-color: rgba(0,0,0,.5);
                                -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
                            }
                        }
                        @media all and (device-width: 768px) and (device-height: 1024px) and (orientation:landscape)
                        {
                            /* your css rules for ipad landscape */    ::-webkit-scrollbar
                            {
                                -webkit-appearance: none;
                                width: 7px;
                            }
                            ::-webkit-scrollbar-thumb
                            {
                                border-radius: 4px;
                                background-color: rgba(0,0,0,.5);
                                -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
                            }
                        }
                    </style>
                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="RadScriptManager2" runat="server" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager2" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/FleetCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/LegNotesPopup.aspx?IsCalender=1">
            </telerik:RadWindow>
            <%--<telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/UserControls/UCOutBoundInstructions.ascx">
                <ContentTemplate>
                    <UCPreflight:OutBoundInstructionsPopup ID="OutboundInstructions" runat="server" />
                </ContentTemplate>
            </telerik:RadWindow>--%>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index: 8000"
        AutoSize="true" Modal="true" Behaviors="close" Height="1000px">
        <Windows>
            <telerik:RadWindow ID="RadWindow7" runat="server" OnClientResizeEnd="GetDimensions"
                CssClass="rwStatusbarRow" AutoSize="true" KeepInScreenBounds="false" Modal="true"
                maximize="true" fullscreen="yes" channelmode="yes" VisibleTitlebar="false" VisibleToolbar="false"
                VisibleMenubar="false" VisibleDirectories="false" resizable="yes" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/MonthlyFullView.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <%--  AutoSize="true" KeepInScreenBounds="false" Modal="true" maximize="true" fullscreen="yes" channelmode="yes" toolbar="no" menubar="no" directories="no" resizable="yes"
    
    <div class="globalMenu_mapping">
        <a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
        <a href="#" class="globalMenu_mapping">Preflight</a> <span class="breadcrumbCaret">
        </span><a href="#" class="globalMenu_mapping">Scheduling Calendar</a>
    </div>--%>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="hdnEnableHoverDisplay" value="" runat="server"/>
        <input type="hidden" id="radSelectedLegNum" name="radSelectedLegNum" runat="server" />
        <input type="hidden" id="radSelectedStartDate" name="radSelectedStartDate" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="556px" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel140">
                    <div class="sc_date_display">
                        <span>
                            <asp:Label ID="lbMonth" runat="server" CssClass="date"></asp:Label></span>
                    </div>
                </td>
                <td align="left" class="defaultview">
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        OnCheckedChanged="ChkDefaultView_OnCheckedChanged" onclick="javascript:showPleaseWait();" />
                </td>
                <td align="left" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
                <td class="sc_nav_topicon" align="left">
                    <span class="tab-nav-icons"><a class="refresh-icon" onclick="Refresh();" href="#"
                        title="Refresh"></a><a class="help-icon" href="/Views/Help/ViewHelp.aspx?Screen=SchedulingCalendar" title="Help"></a></span>
                </td>
                <td class="tdLabel70" align="left">
                    <asp:Button ID="btnFullView" runat="server" CssClass="ui_nav" Text="FV" ToolTip="Full View"
                        OnClientClick="javascript:openWinMonthlyFullView();return false;" />
                </td>
            </tr>
            <tr>
                <td colspan="6" class="monthly_box">
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0" Width="980px"
                        Skin="Windows7" Height="500px">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="None">
                            <div class="sc_navigation">
                                <div class="sc_navigation">
                                    <div class="sc_navigation_left">
                                        <span class="sc_weekly_checkbox">
                                            <asp:CheckBox ID="chksunsat" runat="server" Text="Sunday - Saturday Week" OnCheckedChanged="chksunsat_OnCheckedChanged"
                                                AutoPostBack="true" onclick="javascript:showPleaseWait();" />
                                            <asp:CheckBox ID="chkNoPastWeeks" runat="server" Text="No Past Weeks" OnCheckedChanged="chkNoPastWeeks_OnCheckedChanged"
                                                AutoPostBack="true" onclick="javascript:showPleaseWait();" />
                                        </span>
                                    </div>
                                    <div class="sc_navigation_right">
                                        <span class="act-btn"><span>
                                            <asp:Button ID="btnFirst" runat="server" ToolTip="Go back 1 month" CssClass="icon-first_sc"
                                                OnClick="btnFirst_click" OnClientClick="showPleaseWait();" /></span> <span>
                                                    <asp:Button ID="btnPrevious" runat="server" CssClass="icon-prev_sc" ToolTip="Go back 1 week"
                                                        OnClick="prevButton_Click" OnClientClick="showPleaseWait();" /></span>
                                            <span><span>
                                                <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                    AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" OnClientClick="showPleaseWait();"
                                                    onfocus="showPopup(this, event);" onKeyPress="return ontbDateKeyPress(this, event);"
                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onBlur="parseDate(this, event);"
                                                    MaxLength="10"></asp:TextBox></span>
                                                <asp:Button ID="nextButton" runat="server" CssClass="icon-next_sc" ToolTip="Go forward 1 week"
                                                    OnClick="nextButton_Click" OnClientClick="showPleaseWait();" /></span> <span>
                                                        <asp:Button ID="btnLast" runat="server" CssClass="icon-last_sc" ToolTip="Go forward 1 month"
                                                            OnClick="btnLast_click" OnClientClick="showPleaseWait();" /></span></span>
                                        <span>
                                            <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                                OnClientClick="showPleaseWait();" /></span>
                                    </div>
                                </div>
                            </div>
                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="None" runat="server"
                                CssClass="sc_monthly_calendar_box">
                                <Items>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlWeeklyScheduler" runat="server">
                                                <%--  First--%>
                                                <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadScheduler1"
                                                    AllowEdit="true" AllowInsert="true" DataEndField="End" DataKeyField="TripId"
                                                    OnAppointmentDataBound="RadScheduler1_AppointmentDataBound" DataStartField="Start"
                                                    DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                    EnableAdvancedForm="true" SelectedView="TimelineView" Skin="Windows7" ShowHeader="false"
                                                    Height="77px" RowHeight="50px" AppointmentStyleMode="Default" Localization-HeaderToday="Today"
                                                    OnClientAppointmentContextMenu="RadScheduler1_OnClientAppointmentContextMenu"
                                                    OnClientAppointmentContextMenuItemClicking="RadScheduler1_OnClientAppointmentContextMenuItemClicking"
                                                    OnAppointmentContextMenuItemClicked="RadScheduler1_OnAppointmentContextMenuItemClicked"
                                                    CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu"
                                                    OnClientTimeSlotContextMenuItemClicked="RadScheduler1_OnClientTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotContextMenuItemClicked="RadScheduler1_OnTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotCreated="RadScheduler1_TimeSlotCreated">
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AdvancedForm Modal="true" />
                                                    <Reminders Enabled="true" />
                                                    <ResourceHeaderTemplate>
                                                        <div style="text-align: left">
                                                            <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                            <br />
                                                            <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                Font-Size="Smaller" />
                                                        </div>
                                                    </ResourceHeaderTemplate>
                                                    <TimelineView UserSelectable="false" NumberOfSlots="7" GroupingDirection="Vertical"
                                                        ShowInsertArea="false" />
                                                    <TimeSlotContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadWeeklySchedulerContextMenu" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                    Enabled="false" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </TimeSlotContextMenus>
                                                    <AppointmentContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu1" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                <telerik:RadMenuItem Text="Logistics">
                                                                    <Items>
                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                    </Items>
                                                                </telerik:RadMenuItem>
                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </AppointmentContextMenus>
                                                    <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AppointmentTemplate>
                                                        <div>
                                                            <%# Eval("Subject")%></div>
                                                        <div style="font-style: italic;">
                                                            <%# Eval("Description") %></div>
                                                    </AppointmentTemplate>
                                                </telerik:RadScheduler>
                                                <telerik:RadToolTipManager runat="server" ID="RadToolTipManager6" Width="300" Height="150"
                                                    Animation="None" HideEvent="LeaveToolTip" Text="Loading..." OnClientRequestStart="OnClientRequestStart5"
                                                    OnClientBeforeShow="OnClientBeforeShow5" OnAjaxUpdate="RadToolTipManager1_AjaxUpdate"
                                                    Position="TopCenter" />
                                                <div style="display: none;">
                                                    <div id="Div1">
                                                        <div class="appointment-tooltip">
                                                            <strong><span id="Span1"></span></strong><span id="Span2"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--  second--%>
                                                <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadScheduler2"
                                                    AllowEdit="true" AllowInsert="true" DataEndField="End" DataKeyField="TripId"
                                                    OnAppointmentDataBound="RadScheduler2_AppointmentDataBound" DataStartField="Start"
                                                    DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                    EnableAdvancedForm="true" SelectedView="TimelineView" Skin="Windows7" ShowHeader="false"
                                                    Height="77px" RowHeight="50px" AppointmentStyleMode="Default" Localization-HeaderToday="Today"
                                                    OnClientAppointmentContextMenu="RadScheduler2_OnClientAppointmentContextMenu"
                                                    OnClientAppointmentContextMenuItemClicking="RadScheduler2_OnClientAppointmentContextMenuItemClicking"
                                                    OnAppointmentContextMenuItemClicked="RadScheduler2_OnAppointmentContextMenuItemClicked"
                                                    CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu"
                                                    OnClientTimeSlotContextMenuItemClicked="RadScheduler2_OnClientTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotContextMenuItemClicked="RadScheduler2_OnTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotCreated="RadScheduler2_TimeSlotCreated">
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AdvancedForm Modal="true" />
                                                    <Reminders Enabled="true" />
                                                    <ResourceHeaderTemplate>
                                                        <div style="text-align: left">
                                                            <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                            <br />
                                                            <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                Font-Size="Smaller" />
                                                        </div>
                                                    </ResourceHeaderTemplate>
                                                    <TimelineView UserSelectable="false" NumberOfSlots="7" ShowInsertArea="false" ColumnHeaderDateFormat="MM/dd" />
                                                    <TimeSlotContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu2" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                    Enabled="false" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </TimeSlotContextMenus>
                                                    <AppointmentContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu4" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                <telerik:RadMenuItem Text="Logistics">
                                                                    <Items>
                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                    </Items>
                                                                </telerik:RadMenuItem>
                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </AppointmentContextMenus>
                                                    <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AppointmentTemplate>
                                                        <div>
                                                            <%# Eval("Subject")%></div>
                                                        <div style="font-style: italic;">
                                                            <%# Eval("Description") %></div>
                                                    </AppointmentTemplate>
                                                </telerik:RadScheduler>
                                                <telerik:RadToolTipManager runat="server" ID="RadToolTipManager5" Width="300" Height="150"
                                                    Animation="None" HideEvent="LeaveToolTip" Text="Loading..." OnClientRequestStart="OnClientRequestStart4"
                                                    OnClientBeforeShow="OnClientBeforeShow4" OnAjaxUpdate="RadToolTipManager1_AjaxUpdate"
                                                    Position="TopCenter" />
                                                <%-- third --%>
                                                <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadScheduler3"
                                                    OnAppointmentDataBound="RadScheduler3_AppointmentDataBound" AllowEdit="true"
                                                    AllowInsert="true" DataEndField="End" DataKeyField="TripId" DataStartField="Start"
                                                    DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                    EnableAdvancedForm="true" SelectedView="TimelineView" Skin="Windows7" ShowHeader="false"
                                                    OnAppointmentContextMenuItemClicked="RadScheduler3_OnAppointmentContextMenuItemClicked"
                                                    OnClientAppointmentContextMenu="RadScheduler3_OnClientAppointmentContextMenu"
                                                    OnClientAppointmentContextMenuItemClicking="RadScheduler3_OnClientAppointmentContextMenuItemClicking"
                                                    Height="77px" RowHeight="50px" AppointmentStyleMode="Default" CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu"
                                                    OverflowBehavior="Scroll" OnClientTimeSlotContextMenuItemClicked="RadScheduler3_OnClientTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotContextMenuItemClicked="RadScheduler3_OnTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotCreated="RadScheduler3_TimeSlotCreated">
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AdvancedForm Modal="true" />
                                                    <Reminders Enabled="true" />
                                                    <ResourceHeaderTemplate>
                                                        <scrollviewer verticalscrollbarvisibility="Visible">
                                                        <telerikScheduler:radScheduler x:name="scheduler" height="3000" />
                                                        </scrollviewer>
                                                        <div style="text-align: left">
                                                            <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                            <br />
                                                            <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                Font-Size="Smaller" />
                                                        </div>
                                                    </ResourceHeaderTemplate>
                                                    <TimelineView UserSelectable="false" NumberOfSlots="7" ColumnHeaderDateFormat="MM/dd"
                                                        ShowInsertArea="false" />
                                                    <TimeSlotContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu3" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                    Enabled="false" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </TimeSlotContextMenus>
                                                    <AppointmentContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu5" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                <telerik:RadMenuItem Text="Logistics">
                                                                    <Items>
                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                    </Items>
                                                                </telerik:RadMenuItem>
                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </AppointmentContextMenus>
                                                    <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AppointmentTemplate>
                                                        <div>
                                                            <%# Eval("Subject")%></div>
                                                        <div style="font-style: italic;">
                                                            <%# Eval("Description") %></div>
                                                    </AppointmentTemplate>
                                                </telerik:RadScheduler>
                                                <telerik:RadToolTipManager runat="server" ID="RadToolTipManager4" Width="300" Height="150"
                                                    Position="TopCenter" Animation="None" HideEvent="LeaveToolTip" Text="Loading..."
                                                    OnClientRequestStart="OnClientRequestStart3" OnClientBeforeShow="OnClientBeforeShow3"
                                                    OnAjaxUpdate="RadToolTipManager1_AjaxUpdate" />
                                                <%-- Fourth --%>
                                                <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadScheduler4"
                                                    AllowEdit="true" AllowInsert="true" DataEndField="End" DataKeyField="TripId"
                                                    DataStartField="Start" DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                    EnableAdvancedForm="true" SelectedView="TimelineView" Skin="Windows7" ShowHeader="false"
                                                    OnAppointmentDataBound="RadScheduler4_AppointmentDataBound" OnClientAppointmentContextMenu="RadScheduler4_OnClientAppointmentContextMenu"
                                                    OnClientAppointmentContextMenuItemClicking="RadScheduler4_OnClientAppointmentContextMenuItemClicking"
                                                    OnAppointmentContextMenuItemClicked="RadScheduler4_OnAppointmentContextMenuItemClicked"
                                                    Height="77px" RowHeight="50px" AppointmentStyleMode="Default" Localization-HeaderToday="Today"
                                                    CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu"
                                                    OnClientTimeSlotContextMenuItemClicked="RadScheduler4_OnClientTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotContextMenuItemClicked="RadScheduler4_OnTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotCreated="RadScheduler4_TimeSlotCreated">
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AdvancedForm Modal="true" />
                                                    <Reminders Enabled="true" />
                                                    <ResourceHeaderTemplate>
                                                        <div style="text-align: left">
                                                            <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                            <br />
                                                            <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                Font-Size="Smaller" />
                                                        </div>
                                                    </ResourceHeaderTemplate>
                                                    <TimelineView UserSelectable="false" NumberOfSlots="7" ShowInsertArea="false" ColumnHeaderDateFormat="MM/dd" />
                                                    <TimeSlotContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu6" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                    Enabled="false" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </TimeSlotContextMenus>
                                                    <AppointmentContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu7" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                <telerik:RadMenuItem Text="Logistics">
                                                                    <Items>
                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                    </Items>
                                                                </telerik:RadMenuItem>
                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </AppointmentContextMenus>
                                                    <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AppointmentTemplate>
                                                        <div>
                                                            <%# Eval("Subject")%></div>
                                                        <div style="font-style: italic;">
                                                            <%# Eval("Description") %></div>
                                                    </AppointmentTemplate>
                                                </telerik:RadScheduler>
                                                <telerik:RadToolTipManager runat="server" ID="RadToolTipManager3" Width="300" Height="150"
                                                    Position="TopCenter" Animation="None" HideEvent="LeaveToolTip" Text="Loading..."
                                                    OnClientRequestStart="OnClientRequestStart2" OnClientBeforeShow="OnClientBeforeShow2"
                                                    OnAjaxUpdate="RadToolTipManager1_AjaxUpdate" />
                                                <%-- Fifth --%>
                                                <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadScheduler5"
                                                    OnAppointmentDataBound="RadScheduler5_AppointmentDataBound" AllowEdit="true"
                                                    AllowInsert="true" DataEndField="End" DataKeyField="TripId" DataStartField="Start"
                                                    DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                    EnableAdvancedForm="true" SelectedView="TimelineView" Skin="Windows7" ShowHeader="false"
                                                    OnClientAppointmentContextMenu="RadScheduler5_OnClientAppointmentContextMenu"
                                                    OnClientAppointmentContextMenuItemClicking="RadScheduler5_OnClientAppointmentContextMenuItemClicking"
                                                    OnAppointmentContextMenuItemClicked="RadScheduler5_OnAppointmentContextMenuItemClicked"
                                                    Height="77px" RowHeight="50px" AppointmentStyleMode="Default" Localization-HeaderToday="Today"
                                                    OnClientTimeSlotContextMenuItemClicked="RadScheduler5_OnClientTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotContextMenuItemClicked="RadScheduler5_OnTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotCreated="RadScheduler5_TimeSlotCreated" CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu">
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AdvancedForm Modal="true" />
                                                    <Reminders Enabled="true" />
                                                    <ResourceHeaderTemplate>
                                                        <div style="text-align: left">
                                                            <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                            <br />
                                                            <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                Font-Size="Smaller" />
                                                        </div>
                                                    </ResourceHeaderTemplate>
                                                    <TimelineView UserSelectable="false" NumberOfSlots="7" ShowInsertArea="false" ColumnHeaderDateFormat="MM/dd" />
                                                    <TimeSlotContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu8" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                    Enabled="false" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </TimeSlotContextMenus>
                                                    <AppointmentContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu9" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                <telerik:RadMenuItem Text="Logistics">
                                                                    <Items>
                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                    </Items>
                                                                </telerik:RadMenuItem>
                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </AppointmentContextMenus>
                                                    <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AppointmentTemplate>
                                                        <div>
                                                            <%# Eval("Subject")%></div>
                                                        <div style="font-style: italic;">
                                                            <%# Eval("Description") %></div>
                                                    </AppointmentTemplate>
                                                </telerik:RadScheduler>
                                                <telerik:RadToolTipManager runat="server" ID="RadToolTipManager2" Width="300" Height="150"
                                                    Animation="None" HideEvent="LeaveToolTip" Text="Loading..." OnClientRequestStart="OnClientRequestStart1"
                                                    OnClientBeforeShow="OnClientBeforeShow1" OnAjaxUpdate="RadToolTipManager1_AjaxUpdate"
                                                    Position="TopCenter" />
                                                <%-- Sixth --%>
                                                <telerik:RadScheduler ReadOnly="true" CssClass="rsHeader" runat="server" ID="RadScheduler6"
                                                    OnAppointmentDataBound="RadScheduler6_AppointmentDataBound" AllowEdit="true"
                                                    AllowInsert="true" DataEndField="End" DataKeyField="TripId" DataStartField="Start"
                                                    DataSubjectField="Description" DayEndTime="24:00:00" DayStartTime="00:00:00"
                                                    EnableAdvancedForm="true" SelectedView="TimelineView" Skin="Windows7" ShowHeader="false"
                                                    OnClientAppointmentContextMenu="RadScheduler6_OnClientAppointmentContextMenu"
                                                    OnClientAppointmentContextMenuItemClicking="RadScheduler6_OnClientAppointmentContextMenuItemClicking"
                                                    OnAppointmentContextMenuItemClicked="RadScheduler6_OnAppointmentContextMenuItemClicked"
                                                    Height="77px" RowHeight="50px" AppointmentStyleMode="Default" Localization-HeaderToday="Today"
                                                    CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu"
                                                    OnClientTimeSlotContextMenuItemClicked="RadScheduler6_OnClientTimeSlotContextMenuItemClicked"
                                                    OnTimeSlotCreated="RadScheduler6_TimeSlotCreated" OnTimeSlotContextMenuItemClicked="RadScheduler6_OnTimeSlotContextMenuItemClicked">
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AdvancedForm Modal="true" />
                                                    <Reminders Enabled="true" />
                                                    <ResourceHeaderTemplate>
                                                        <div style="text-align: left">
                                                            <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                            <br />
                                                            <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                Font-Size="Smaller" />
                                                        </div>
                                                    </ResourceHeaderTemplate>
                                                    <TimelineView UserSelectable="false" NumberOfSlots="7" ShowInsertArea="false" ColumnHeaderDateFormat="MM/dd" />
                                                    <TimeSlotContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu10" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                    Enabled="false" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </TimeSlotContextMenus>
                                                    <AppointmentContextMenus>
                                                        <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu11" runat="server">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                <telerik:RadMenuItem Text="Logistics">
                                                                    <Items>
                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                    </Items>
                                                                </telerik:RadMenuItem>
                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                            </Items>
                                                        </telerik:RadSchedulerContextMenu>
                                                    </AppointmentContextMenus>
                                                    <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                    <AppointmentContextMenuSettings EnableDefault="false" />
                                                    <AppointmentTemplate>
                                                        <div>
                                                            <%# Eval("Subject")%></div>
                                                        <div style="font-style: italic;">
                                                            <%# Eval("Description") %></div>
                                                    </AppointmentTemplate>
                                                </telerik:RadScheduler>
                                                <telerik:RadToolTipManager runat="server" ID="RadToolTipManager1" Width="300" Height="150"
                                                    Animation="None" HideEvent="LeaveToolTip" Text="Loading..." OnClientRequestStart="OnClientRequestStart"
                                                    OnClientBeforeShow="OnClientBeforeShow" OnAjaxUpdate="RadToolTipManager1_AjaxUpdate"
                                                    Position="TopCenter" />
                                                <div style="display: none;">
                                                    <div id="contentContainer">
                                                        <div class="appointment-tooltip">
                                                            <strong><span id="ToolTipSubject"></span></strong><span id="ToolTipDescription">
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward" />
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="None" Width="250px" Collapsed="true">
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="500px" ExpandMode="SingleExpandedItem" AllowCollapseAllItems="true">                            
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="FleetTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="FleetTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="CrewTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Expanded="True" Text="Filter Criteria" runat="server">
                                        <ContentTemplate>
                                       <div class="sc_filter_criteria">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                           </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                OnClientClick="javascript:openWinAdv();return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </td>
            </tr>
        </table>
    </div>
     <script type="text/javascript">
         $(document).ready(function(){
             if(OSName.isMac())
             {  $(".rsInnerFix").css("width","100%").parent().css("width",'100%');
             }
                 $(".rsApt").each(function(){
                     var marginLeft=parseInt($(this).css("margin-left"));
                     if(marginLeft>=3)
                         $(this).css("-webkit-margin-start",marginLeft-(marginLeft-2)+"px");
                 });
         });
    </script>
</asp:Content>
