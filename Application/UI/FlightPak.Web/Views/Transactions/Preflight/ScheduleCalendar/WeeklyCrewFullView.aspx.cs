﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.CommonService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using FlightPak.Web.UserControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class WeeklyCrewFullView : ScheduleCalendarBase
    {

        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;

                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        RadWeeklyScheduler.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;

                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainContent");
                        UserControl ucSCMenu = (UserControl)contentPlaceHolder.FindControl("TabSchedulingCalendar");
                        ucSCMenu.Visible = false;
                        Master.FindControl("logo").Visible = false;
                        Master.FindControl("Combo").Visible = false;
                        Master.FindControl("menu").Visible = false;
                        Master.FindControl("globalNav").Visible = false;
                        Master.FindControl("footer").Visible = false;

                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.WeeklyCrew)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }

                            var selectedDay = DateTime.Today;
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }

                            StartDate = selectedDay;
                            EndDate = GetWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);


                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            Session.Remove("SCWeeklyCrewNextCount");
                            Session.Remove("SCWeeklyCrewPreviousCount");
                            Session.Remove("SCWeeklyCrewLastCount");
                            Session.Remove("SCWeeklyCrewFirstCount");

                            loadTree(false);
                        }
                        else
                        {
                            CheckValidDate();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }
        /// <summary>
        /// To Validate Date
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            StartDate = (DateTime)Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "',275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            EndDate = GetWeekEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

            // avoid duplicate calls for loadTree();
            string id = Page.Request.Params["__EVENTTARGET"];

            if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
            {
                loadTree(false);
            }
            return IsDateCheck;
        }
        

        protected void BindQuickCrewContextMenu()
        {

            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            //{
            //    RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Fleet Calendar Entry").Enabled = false;

            //    //To check add access and prevent user...
            //    if (!IsAuthorized(Permission.Preflight.AddCrewCalendarEntry))
            //    {
            //        RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Crew Calendar Entry").Enabled = false;
            //        return;
            //    }

            //    RadMenuItem CrewMenuItem = RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Crew Calendar Entry");
            //    using (var objService = new MasterCatalogServiceClient())
            //    {
            //        var objRetValCrew = objService.GetCrewDutyTypeList().EntityList.Where(x => x.CalendarEntry == true).ToList();

            //        if (objRetValCrew.Count() <= 0)
            //        {
            //            CrewMenuItem.Enabled = false;
            //        }
            //        else
            //        {
            //            CrewMenuItem.Items.Clear();
            //            for (int i = 0; i <= objRetValCrew.Count() - 1; i++)
            //            {
            //                RadMenuItem fleetduty = new RadMenuItem();
            //                fleetduty.Text = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetValCrew[i]).DutyTypesDescription.ToString();
            //                fleetduty.Value = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetValCrew[i]).DutyTypeCD.ToString();
            //                CrewMenuItem.Items.Add(fleetduty);
            //            }
            //            CrewMenuItem.Enabled = true;
            //        }
            //    }
            //    PleaseWait.Style["display"] = "none";
            //    // PleaseWait.Attributes.Add("style", "display:none;");
            //    //PleaseWait.Visible = false;
            //}
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                        StartDate = DateTime.Today;
                        EndDate = GetWeekEndDate(StartDate);

                        lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        Session["SCSelectedDay"] = StartDate;

                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetWeekEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyCrew);
                }
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                //fleet trips list
                if (!Page.IsPostBack)
                {
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyCrew);                   

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                     /*if (!string.IsNullOrEmpty(FleetCrew[1]))
                     {
                         string[] Crewids = FleetCrew[1].Split(',');
                         foreach (string cIDs in Crewids)
                         {
                             Crewlst.Add(cIDs);
                         }
                     } */

                    if (!string.IsNullOrEmpty(FleetCrew[2]))
                    {
                        string[] CrewGroupids = FleetCrew[2].Split(',');
                        foreach (string cgrpIDs in CrewGroupids)
                        {
                            string[] ParentChild = cgrpIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Crewlst.Add(new TreeviewParentChildNodePair("Crew", cgrpIDs));
                        }
                    }

                    BindCrewTree(Crewlst);
                }
                // resource type
                if (radFleet.Checked)
                {
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyFleet;
                    Response.Redirect("SchedulingCalendar.aspx");
                }
                else if (radCrew.Checked)
                {
                    BindQuickCrewContextMenu();//Quick Crew Entry Sub Context Menu Creation
                    
                    using (var preflightServiceClient = new PreflightServiceClient())
                    {                                          
                        // radSelectedView.Value = "View_WeeklyCrew";
                        Collection<string> Fleetlst = new Collection<string>();
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyCrew);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CreewIDs = FleetCrew[1];                             

                            if (!string.IsNullOrEmpty(FleetCrew[0]))
                            {
                                string[] Fleetids = FleetCrew[0].Split(',');
                                foreach (string fIDs in Fleetids)
                                {
                                    Fleetlst.Add(fIDs);
                                }
                            } 
                        }
                        if (IsSave)
                        {
                            //var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                            if (GetCheckedTreeNodes(CrewTreeView).Count == 0)
                            {
                                CrewTreeView.CheckAllNodes();
                            }
                            var selectedGroupCrewIDs = GetCheckedTreeNodes(CrewTreeView, true);
                            var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView, false);

                            bool IsEntireCrewSelected = IsEntireCrewChecked(CrewTreeView);


                            //To get the distinct values
                            List<string> distinctCrew = selectedCrewIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                            string strTestCrew = string.Join(",", distinctCrew.ToArray());
                            Session["CrewIDs"] = strTestCrew;

                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyCrew, null, selectedCrewIDs, selectedGroupCrewIDs, !IsEntireCrewSelected);
                            Collection<string> SelectedCrewIDs = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in selectedCrewIDs)
                            {
                                SelectedCrewIDs.Add(s.ChildNodeId);
                            }
                            BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.WeeklyCrew, SelectedCrewIDs);
                        }
                        else
                        {                            
                            if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                            {
                                string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                                var FleetIDs = FleetCrew[0];
                                var CrewIDs = FleetCrew[1];

                                if (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0)
                                {
                                    if (CrewTreeView.Visible && CrewTreeView.CheckedNodes.Count == 0)
                                    {
                                        CrewTreeView.CheckAllNodes();
                                    }
                                }

                                Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();                                                                                            

                                if (!string.IsNullOrEmpty(FleetCrew[1]))
                                {
                                    string[] Crewids = FleetCrew[1].Split(',');
                                    foreach (string cIDs in Crewids)
                                    {
                                        string[] ParentChild = cIDs.Split('$');
                                        if (ParentChild.Length > 1)
                                            Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                        else
                                            Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                    }
                                }
                                else
                                    Crewlst = GetCheckedTreeNodes(CrewTreeView, false);

                                //To get the distinct values
                                List<string> distinctCrew = Crewlst.Select(node => node.ChildNodeId).Distinct().ToList();
                                string strTestCrew = string.Join(",", distinctCrew.ToArray());
                                Session["CrewIDs"] = strTestCrew;
                                Collection<string> crewlst = new Collection<string>();
                                foreach (TreeviewParentChildNodePair s in Crewlst)
                                {
                                    crewlst.Add(s.ChildNodeId);
                                }
                                BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.WeeklyCrew, crewlst);                                
                            }
                        }

                        /*
                        var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);

                        BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.WeeklyCrew, inputFromCrewTree);

                        RadWeeklyScheduler.SelectedDate = StartDate;*/
                    }

                    RadPanelBar2.FindItemByText("Fleet").Visible = false;
                    RadPanelBar2.FindItemByText("Crew").Visible = true;
                    RadPanelBar2.FindItemByText("Crew").Expanded = true;
                    RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                    // RadWeeklyScheduler.Visible = true;
                    // dgWeeklyDetail.Visible = false;

                }
                else if (radDetail.Checked)
                {
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyDetail;
                    Response.Redirect("WeeklyDetail.aspx");
                }
                else
                {
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyMain;
                    Response.Redirect("WeeklyMain.aspx");
                }
                RightPane.Collapsed = true;

            }
        }


        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {                
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNewGroupBased(crewList, selectedcrewIDs);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;

                        CrewTreeView.DataBind();
                    }
                }
            }
        }

        // ON click of Apply button...
        protected void btnApply_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;

                            loadTree(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        private void BindCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime weekStartDate, DateTime weekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromCrewTree)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, weekStartDate, weekEndDate, filterCriteria, displayOptions, viewMode, inputFromCrewTree))
            {
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                if (RadWeeklyScheduler.ResourceTypes.Count == 1)
                {
                    RadWeeklyScheduler.ResourceTypes.Remove(RadWeeklyScheduler.ResourceTypes.First());
                }
                ResourceType crewResourceType = GetResourceType();

                if (inputFromCrewTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetCrewCalendarData(weekStartDate, weekEndDate, serviceFilterCriteria, inputFromCrewTree.Distinct().ToList(), true);

                    var appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);

                    RadWeeklyScheduler.DataSource = appointments.OrderBy(x => x.StartDate).ToList(); ;

                    crewResourceType.DataSource = GetCrewResourceType(appointments);
                    RadWeeklyScheduler.ResourceTypes.Add(crewResourceType);
                }
                else
                {
                    RadWeeklyScheduler.DataSource = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                    crewResourceType.DataSource = GetCrewResourceType(new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>());
                    RadWeeklyScheduler.ResourceTypes.Add(crewResourceType);
                }
                RadWeeklyScheduler.DataStartField = "StartDate";
                RadWeeklyScheduler.DataEndField = "EndDate";
                RadWeeklyScheduler.DataSubjectField = "Description";
                RadWeeklyScheduler.SelectedDate = weekStartDate;
                RadWeeklyScheduler.DataBind();
            }
        }


        //Convert calendar data result to Appointment entity
        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {
            Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList().OrderBy(x => x.DepartureDisplayTime).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                    foreach (var leg in groupedLegs)
                    {
                        if (displayOptions.WeeklyCrew.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                removeUnwantedAppintement(appointmentCollection, appointment);
                            }
                            else if (leg.LegNUM == 0) // RON appointment
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                //removeUnwantedAppintement(appointmentCollection, appointment);
                                if (appointment.RecordType == "T")
                                {
                                    if (appointment.LegNum != 0)
                                    {
                                        appointmentCollection.Add(appointment);
                                    }
                                    else if (appointment.IsRONAppointment && appointment.DepartureDisplayTime != null && appointment.ArrivalDisplayTime != null)
                                    {
                                        var countRON = appointmentCollection.Where(p => p.CrewId == appointment.CrewId &&
                                            p.TripId == appointment.TripId &&
                                            (p.DepartureDisplayTime.Date == appointment.DepartureDisplayTime.Date
                                            || p.ArrivalDisplayTime.Date == appointment.ArrivalDisplayTime.Date)
                                            ).ToList().Count();

                                        if (countRON == 0)
                                        {
                                            var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                            var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                            if (foundItem1 == null && foundItem2 == null)
                                            {
                                                appointmentCollection.Add(appointment);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                        var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                        if (foundItem1 == null && foundItem2 == null)
                                        {
                                            appointmentCollection.Add(appointment);
                                        }
                                    }
                                }
                                else
                                    appointmentCollection.Add(appointment);

                            }
                        }
                        else // No first leg last leg selected
                        {
                            //FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();

                            if (leg.RecordType == "C" && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i) : EndDate;

                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                    removeUnwantedAppintement(appointmentCollection, appointment);
                                    i++;
                                }
                            }
                            else
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                //removeUnwantedAppintement(appointmentCollection,appointment);
                                if (appointment.RecordType == "T")
                                {
                                    if (appointment.LegNum != 0)
                                    {
                                        appointmentCollection.Add(appointment);
                                    }
                                    else if (appointment.IsRONAppointment && appointment.DepartureDisplayTime != null && appointment.ArrivalDisplayTime != null)
                                    {
                                        var countRON = appointmentCollection.Where(p => p.CrewId == appointment.CrewId &&
                                            p.TripId == appointment.TripId &&
                                            (p.DepartureDisplayTime.Date == appointment.DepartureDisplayTime.Date)
                                            ).ToList().Count();

                                        if (countRON == 0)
                                        {
                                            var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                            var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                            if (foundItem1 == null && foundItem2 == null)
                                            {
                                                appointmentCollection.Add(appointment);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                        var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date).FirstOrDefault();
                                        if (foundItem1 == null && foundItem2 == null)
                                        {
                                            appointmentCollection.Add(appointment);
                                        }
                                    }
                                }
                                else
                                    appointmentCollection.Add(appointment);
                            }
                        }
                    }
                }
            }
            return appointmentCollection;
        }

        void removeUnwantedAppintement(Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {


            if (appointment.LegNum != 0 ||
                (appointmentCollection.Where(
                    t =>
                        t.TripId == appointment.TripId && t.CrewId == appointment.CrewId &&
                        t.LegNum == appointment.LegNum).FirstOrDefault() == null))
            {
                appointmentCollection.Add(appointment);
            }
            else
            {
                int index = 0;
                if (
                    (index =
                        appointmentCollection.ToList()
                            .FindIndex(
                                t =>
                                    t.TripId == appointment.TripId && t.CrewId == appointment.CrewId &&
                                    t.LegNum == appointment.LegNum)) > -1)
                {
                    appointmentCollection[index] = appointment;
                }
            }
        }
        private void SetCrewAppointment(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {
                SetCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);
                appointment.EndDate = appointment.StartDate.AddSeconds(1);

                // todo: build description for appointment based on view selected and display options for rendering

                var options = displayOptions.WeeklyCrew;
                if (tripLeg.RecordType == "T")
                {
                    if (tripLeg.LegNUM == 0) //  dummy appointment
                    {
                        if (options.FirstLastLeg == false)
                        {

                            appointment.Description = "(" + appointment.CrewDutyTypeCD + ") ";


                            switch (options.DepartArriveInfo)
                            {
                                case DepartArriveInfo.ICAO:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                    break;

                                case DepartArriveInfo.AirportName:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                    break;

                                case DepartArriveInfo.City:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                    break;
                            }

                            var flightNumber = string.Empty;
                            if (options.FlightNumber)
                            {
                                flightNumber = " Flt No.: " + appointment.FlightNum;
                            }

                            //appointment.Description = String.Format("{0}{1}{2}",appointment.Description,appointment.ArrivalICAOID,flightNumber);
                            //appointment.Description = String.Format("{0}{1}", appointment.Description,
                            //    options.ShowTrip ? string.Format("  {0} ", appointment.TripNUM) : "");

                            appointment.Description = String.Format("{0}{1}{2}{3}", appointment.Description,
                                                                    appointment.ArrivalICAOID, flightNumber,
                                                                    BuildWeeklyCrewOnlyToolTipDescriptionAppt(
                                                                        appointment, displayOptions));

                        }
                    }
                    else // db trips
                    {
                        appointment.Description = String.Format("{0}{1}", appointment.Description,
                                                                BuildWeeklyCrewToolTipDescription(appointment,
                                                                                                  displayOptions));
                    }
                }
                else // entries...
                {
                    switch (displayOptions.WeeklyCrew.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;
                    }

                    StringBuilder builder = new StringBuilder();
                    if (appointment.RecordType == "M")
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(") ");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(") ");
                        }
                    }

                    if (appointment.RecordType == "C")
                    {
                        if (appointment.StartDate == appointment.HomelDepartureTime)
                        {
                            builder.Append(appointment.StartDate.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }
                        builder.Append("</br>");
                        builder.Append(appointment.CrewDutyTypeDescription);
                    }
                    else
                    {
                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");
                    }

                    builder.Append(appointment.Notes);
                    appointment.Description = builder.ToString();
                    appointment.Description = String.Format("{0}{1}", appointment.Description, BuildWeeklyCrewOnlyToolTipDescriptionAppt(appointment, displayOptions));
                }
                appointment.ViewMode = viewMode;
            }
        }


        protected void RadWeeklyScheduler_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;

                        //set contextmenu id for each appointment..
                        // e.Appointment.ContextMenuID = item.TripId.ToString();

                        //Rendition based on red-eye option - if red eye option is not selected, shorten the end date to the start date of the appointment...
                        e.Appointment.Resources.Add(new Resource("Leg", "LegNum", item.LegNum.ToString()));
                        //if (DisplayOptions.WeeklyCrew != null && DisplayOptions.WeeklyCrew.DisplayRedEyeFlightsInContinuation == false)
                        if (DisplayOptions.WeeklyCrew != null && DisplayOptions.WeeklyCrew.DisplayRedEyeFlightsInContinuation == false && item.RecordType != "C")
                        {
                            if (item.ArrivalDisplayTime.Date != item.DepartureDisplayTime.Date)
                            {
                                item.EndDate = item.StartDate;
                            }
                        }


                        // if black and white color not selected - default is off – can only be selected if Aircraft color is not selected; 


                        if (DisplayOptions.WeeklyCrew != null)
                        {
                            if (DisplayOptions.WeeklyCrew.BlackWhiteColor)
                            {
                                e.Appointment.BackColor = Color.White;
                                e.Appointment.ForeColor = Color.Black;
                            }
                            else
                            {
                                if (!DisplayOptions.WeeklyCrew.AircraftColors)
                                {
                                    if (item.RecordType == "T")
                                    {
                                        if (item.IsRONAppointment)
                                        {
                                            // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category

                                            if (CrewDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.CrewDutyTypeCD))
                                            {
                                                var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == item.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                                if (dutyCode != null)
                                                {
                                                    e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                    e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            SetFlightCategoryColor(e, item);
                                        }
                                    }
                                    else if (item.RecordType == "M")
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                        SetAircraftDutyColor(e, item);
                                    }
                                    else if (item.RecordType == "C")
                                    {
                                        // set crew duty type color...
                                        SetCrewDutyColor(e, item);
                                    }
                                }
                                else if (DisplayOptions.WeeklyCrew.AircraftColors && item.TailNum == null && item.RecordType == "C")
                                {
                                    SetCrewDutyColor(e, item);
                                }
                                else
                                {
                                    if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                                    {
                                        e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                        e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                                    }
                                }
                            }
                        }
                        e.Appointment.ToolTip = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        protected void RadWeeklyScheduler_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove("CurrentPreFlightTrip");
                        if (e.MenuItem.Value == string.Empty)
                        {
                            return;
                        }
                        if (e.MenuItem.Text == "Add Fleet Calendar Entry")
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Add Crew Calendar Entry")
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                        {
                            var appointmentWriter = (Telerik.Web.UI.Appointment)e.Appointment;
                            var appointmentNumberWriter = appointmentWriter.ID;
                            Int64 tripIdWriter = Convert.ToInt64(appointmentNumberWriter);
                            if (tripIdWriter != 0)
                            {
                                using (var preflightServiceClient = new PreflightServiceClient())
                                {
                                    var tripInfo = preflightServiceClient.GetTrip(tripIdWriter);
                                    if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                    {
                                        var trip = tripInfo.EntityList[0];
                                        RedirectToPage(string.Format("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml&tripnum={0}&tripid={1}", Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripNUM)), Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripID))));
                                    }
                                }
                            }
                            else
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                            }
                            
                        }
                        else if (e.MenuItem.Text == "Add New Trip")// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                            Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(radSelectedDate.Value)); 
                            if (HomeBaseId != null && HomeBaseId != 0)
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of login user
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = BaseDescription;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;
                            }
                            if (ClientId != null && ClientId != 0)
                            {
                                Trip.ClientID = ClientId;
                                PreflightService.Client Client = new PreflightService.Client();
                                Client.ClientCD = ClientCode;
                                Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                Trip.Client = Client;
                            }
                            
                            Session["CurrentPreFlightTrip"] = Trip;
                            Session.Remove("PreflightException");
                            Trip.State = TripEntityState.Added;
                            Trip.Mode = TripActionMode.Edit;
                            Response.Redirect(e.MenuItem.Value);

                            /*if (Trip.TripNUM == null || Trip.TripNUM == 0)
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");                             
                            }
                            else                            
                                Response.Redirect(e.MenuItem.Value);*/
                        }

                        //get trip by tripNumber
                        var appointment = (Telerik.Web.UI.Appointment)e.Appointment;
                        var appointmentNumber = appointment.ID;

                        Int64 tripId = Convert.ToInt64(appointmentNumber);
                        if (tripId != 0)
                        {

                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                {
                                    var trip = tripInfo.EntityList[0];

                                    //Set the trip.Mode and Trip.State to NoChage for read only                    
                                    trip.Mode = TripActionMode.NoChange;
                                    trip.Mode = TripActionMode.Edit;
                                    Session["CurrentPreFlightTrip"] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");


                                    if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        RadWindow5.NavigateUrl = e.MenuItem.Value + "?IsCalender=1"; 
                                        RadWindow5.Visible = true;
                                        RadWindow5.VisibleOnPageLoad = true;
                                        RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                        return;
                                    }
                                    else if (e.MenuItem.Text == "Leg Notes") //  Leg Notes menu selected...
                                    {
                                        RadWindow6.NavigateUrl = e.MenuItem.Value;
                                        RadWindow6.Visible = true;
                                        RadWindow6.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else
                                    {
                                        if (trip.TripNUM == null || trip.TripNUM == 0)
                                        {
                                            Session["CurrentPreFlightTrip"] = null;
                                            Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                        }
                                        else
                                            Response.Redirect(e.MenuItem.Value);                                        
                                    }
                                }

                            }
                        }
                        else
                        {
                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        private void SetAircraftDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentAircraftDuty = item.AircraftDutyID;
                        if (AircraftDutyTypes.Count > 0)
                        {
                            var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                            if (dutyCode != null)
                            {
                                e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        private void SetFlightCategoryColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentCategory = item.FlightCategoryID;
                        // Aircraft Color – if on display will use color set in flight category 
                        if (FlightCategories.Count > 0)
                        {
                            var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                            if (categoryCode != null)
                            {
                                e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                                e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        private void SetCrewDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentCategory = item.CrewDutyType;
                        // Aircraft Color – if on display will use color set in flight category 
                        if (CrewDutyTypes.Count > 0)
                        {
                            var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                            if (categoryCode != null)
                            {
                                e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                                e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void radMain_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;
                        Response.Redirect("WeeklyMain.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void radCrew_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }


        }

        protected void radFleet_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                        Response.Redirect("SchedulingCalendar.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void radDetail_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                        Response.Redirect("WeeklyDetail.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        public List<CrewResourceTypeResult> GetCrewResourceType(Collection<Entities.SchedulingCalendar.Appointment> appointments)
        {
            var crewResourceTypes = new List<CrewResourceTypeResult>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointments))
            {

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var crewIdsFromTree = GetCheckedTreeNodes(CrewTreeView, false);   //if homebaseOnly is true, filter based on HomeBase ID
                    Collection<string> CrewIdsFromTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in crewIdsFromTree)
                    {
                        CrewIdsFromTree.Add(s.ChildNodeId);
                    }
                    var crewResources = preflightServiceClient.GetCrewResourceType(CrewIdsFromTree.ToList(), FilterOptions.HomeBaseOnly, FilterOptions.HomeBaseID, FilterOptions.FixedWingCrewOnly, FilterOptions.RotaryWingCrewCrewOnly, ModuleNameConstants.Preflight.WeeklyCrew);
                    if (crewResources.EntityList != null)
                    {
                        if (DisplayOptions.WeeklyCrew.ActivityOnly)
                        {
                            var distinctCrewIds = appointments.Select(x => x.CrewCD).Distinct().ToList();
                            crewResources.EntityList = crewResources.EntityList.Where(x => distinctCrewIds.Contains(x.CrewCD.Trim())).ToList();
                            //crewResourceTypes = crewResources.EntityList;
                            crewResourceTypes = crewResources.EntityList.GroupBy(i => i.CrewID).Select(g => g.First()).OrderBy(i => i.CrewCD).ToList();
                        }
                        else
                        {
                            //crewResourceTypes = crewResources.EntityList;
                            crewResourceTypes = crewResources.EntityList.GroupBy(i => i.CrewID).Select(g => g.First()).OrderBy(i => i.CrewCD).ToList();
                        }
                    }
                }
            }
            return crewResourceTypes;
        }

        // description for appointment
        public string BuildWeeklyCrewAppointmentDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, List<CrewCalendarDataResult> calendarData, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, calendarData, displayOptions))
            {
                var description = new StringBuilder();

                var options = displayOptions.WeeklyCrew;


                if (options.ShowTrip)
                {
                    description.Append("<strong>Trip: </strong>");
                    description.Append(appointment.TripNUM);
                    description.Append(" ");

                }
                if (appointment.ShowAllDetails)
                {
                    description.Append(options.FlightNumber ? string.Format("<strong> Flt No:</strong>{0}", appointment.FlightNum) : "");
                }

                if (options.ShowTripStatus)
                {
                    description.Append("( ");
                    description.Append(appointment.TripStatus);
                    description.Append(" ) ");
                }
                description.Append("<br />\n");

                if (options.ShowTrip)
                {
                    description.Append("<strong>Leg No.: </strong>");
                    description.Append(appointment.LegNum);

                }
                if (appointment.ShowAllDetails)
                {
                    description.Append(options.FlightCategory
                                           ? string.Format("<strong>Cat: </strong>{0}<br />\n ", appointment.FlightCategoryCode)
                                           : "<br />\n");
                }
                else
                {
                    description.Append("<br />\n");
                }


                var departureArrivalInfo = options.DepartArriveInfo;
                GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);

                description.Append("\n");
                description.Append(appointment.TailNum);

                description.Append(" ");
                //Added for Displaying date in radsceduler
                if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date && appointment.RecordType == "T")
                {
                    description.Append("<br />\n");
                    description.Append("* Arrival Date: ");
                    //description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                    description.Append(String.Format("{0:" + ApplicationDateFormat + "}", Convert.ToDateTime(appointment.ArrivalDisplayTime.ToShortDateString().ToString())));
                    description.Append(" *");
                    description.Append("<br />\n");
                    description.Append(" ");
                }
                return description.ToString();
            }

        }

        //description for tooltip

        public string BuildWeeklyCrewToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.WeeklyCrew;

                description.Append(options.ShowTrip ? string.Format("<strong>Trip:</strong>{0}", appointment.TripNUM) : "");
                if (appointment.ShowAllDetails)
                {
                    description.Append(options.FlightNumber ? string.Format("<strong> Flt No:</strong>{0}", appointment.FlightNum) : "");
                }
                description.Append(options.ShowTripStatus ? string.Format("({0})<br />\n", appointment.TripStatus) : "<br />\n");
                description.Append(options.ShowTrip ? string.Format("<strong>Leg No.: </strong>{0} ", appointment.LegNum) : "");
                if (appointment.ShowAllDetails)
                {
                    description.Append(options.FlightCategory
                                           ? string.Format("<strong>Cat: </strong>{0}<br />\n ", appointment.FlightCategoryCode)
                                           : "<br />\n");
                }
                else
                {
                    description.Append("<br />\n");
                }


                if (options.DepartArriveInfo != null)
                {
                    GetDepartureArrivalInfo(appointment, options.DepartArriveInfo, description);
                }

                description.Append(" ");
                //Added for Displaying date in radsceduler
                if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date && appointment.RecordType == "T")
                {
                    description.Append("<br />\n");
                    description.Append("* Arrival Date: ");
                    //description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                    description.Append(String.Format("{0:" + ApplicationDateFormat + "}", Convert.ToDateTime(appointment.ArrivalDisplayTime.ToShortDateString().ToString())));
                    description.Append(" *");
                    description.Append("<br />\n");
                    description.Append(" ");
                }

                description.Append("<table class='calendarinfo'><tr><td>");
                if (appointment.ShowAllDetails)
                {
                    description.Append(options.ETE ? string.Format("<tr><td ><strong>ETE: </strong>{0}  ", appointment.ETE) : "<tr><td> ");
                    description.Append(options.CummulativeETE ? string.Format("<strong>Cum ETE: </strong>{0}  </td></tr>", appointment.CumulativeETE) : "</td></tr> ");

                    description.Append(options.TotalETE ? string.Format("<tr><td><strong>Trip ETE: </strong>{0}  ", appointment.TotalETE) : "<tr><td> ");
                    description.Append(options.Requestor ? string.Format("<strong> Req: </strong>{0} </td></tr>", appointment.PassengerRequestorCD) : "</td></tr> ");

                    description.Append(options.PaxCount ? string.Format("<tr><td><strong>PAX: </strong>{0} ", appointment.PassengerCount) : "<tr><td> ");
                    description.Append(options.SeatsAvailable ? string.Format("<strong> Seats Avail: </strong>{0} </td></tr>", appointment.ReservationAvailable) : "</td></tr> ");

                    description.Append(options.Department ? string.Format("<tr><td><strong>Dept: </strong>{0} ", appointment.DepartmentCD) : "<tr><td> ");
                    description.Append(options.Authorization ? string.Format("<strong>Auth: </strong>{0}  </td></tr>", appointment.AuthorizationCD) : "</td></tr> ");

                    description.Append(options.TripPurpose ? string.Format("<tr><td colspan='2'><strong>Trip Purp.: </strong>{0} </td></tr>", appointment.TripPurpose) : "<tr><td colspan='2'></td></tr>");
                    description.Append(options.LegPurpose ? string.Format("<tr><td colspan='2'><strong>Leg Purp.: </strong>{0} </td></tr>", appointment.FlightPurpose) : "<tr><td colspan='2'></td></tr>");

                    if (options.Crew)
                    {
                        // todo: get from Anoop
                        if (!string.IsNullOrEmpty(appointment.CrewCodes))
                        {
                            string currentCrews = appointment.CrewCodes;
                            //currentCrews = currentCrews.Replace(appointment.CrewCD, "");
                            if (!string.IsNullOrEmpty(currentCrews))
                            {
                                string[] crews = currentCrews.Split(',');
                                string newCrews = string.Empty;
                                foreach (string crew in crews)
                                {
                                    if (!string.IsNullOrEmpty(crew) && crew != "," && crew.Trim() != appointment.CrewCD.Trim())
                                    {
                                        if (string.IsNullOrEmpty(newCrews))
                                            newCrews = crew;
                                        else
                                            newCrews = newCrews + "," + crew;
                                    }
                                }
                                description.Append("</td></tr><tr><td colspan='2'><strong>Crew: </strong>");
                                //description.Append(newCrews);
                                if (Convert.ToString(newCrews).Length > 67)
                                {
                                    description.Append(Convert.ToString(newCrews).Substring(0, 45) + "...  <a class='seeMoreLink' onClick='SeeMoreCrew(" + appointment.TripId + ");' href='javascript:void(0)'>See more</a>");
                                }
                                else
                                {
                                    description.Append(Convert.ToString(newCrews));
                                }
                            }
                        }
                    }
                }
                else // show details based on Trip Privacy Settins from company profile
                {

                    description.Append((PrivateTrip.IsShowFlightNumber && options.FlightNumber) ? string.Format("<strong>Flt No.: </strong>{0}  </td><td>", appointment.FlightNum) : " </td><td>");
                    description.Append((PrivateTrip.IsShowFlightCategory && options.FlightCategory) ? string.Format("<strong>Cat: </strong>{0} </td></tr>", appointment.FlightCategoryCode) : " </td></tr>");


                    description.Append((PrivateTrip.IsShowETE && options.ETE) ? string.Format("<tr><td><strong>ETE: </strong>{0}  ", appointment.ETE) : "<tr><td> ");
                    description.Append((PrivateTrip.IsShowCumulativeETE && options.CummulativeETE) ? string.Format("<strong> Cum ETE: </strong>{0}  </td></tr>", appointment.CumulativeETE) : "</td></tr>");

                    description.Append((PrivateTrip.IsShowTotalETE && options.TotalETE) ? string.Format("<tr><td ><strong>Trip ETE: </strong>{0}  ", appointment.TotalETE) : "<tr><td >");
                    description.Append((PrivateTrip.IsShowRequestor && options.Requestor) ? string.Format("<strong> Req: </strong>{0} </td></tr>", appointment.PassengerRequestorCD) : " </td></tr>");

                    description.Append((PrivateTrip.IsShowPaxCount && options.PaxCount) ? string.Format("<tr><td><strong>PAX: </strong>{0}  ", appointment.PassengerCount) : "<tr><td> ");
                    description.Append((PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable) ? string.Format(" <strong> Seats Avail: </strong>{0} </td></tr>", appointment.ReservationAvailable) : "</td></tr>");

                    description.Append((PrivateTrip.IsShowDepartment && options.Department) ? string.Format("<tr><td><strong>Dept: </strong>{0} </td><td>", appointment.DepartmentCD) : " </td><td>");
                    description.Append((PrivateTrip.IsShowAuthorization && options.Authorization) ? string.Format("<strong>Auth: </strong>{0}  </td></tr>", appointment.AuthorizationCD) : " </td></tr>");

                    description.Append((PrivateTrip.IsShowTripPurpose && options.TripPurpose) ? string.Format("<tr><td colspan='2'><strong>Trip Purp.: </strong>{0} </td></tr>", appointment.TripPurpose) : "<tr><td colspan='2'></td></tr>");
                    description.Append((PrivateTrip.IsShowLegPurpose && options.LegPurpose) ? string.Format("<tr><td colspan='2'><strong>Leg Purp.: </strong>{0} </td></tr>", appointment.FlightPurpose) : "<tr><td colspan='2'></td></tr>");




                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {

                        if (!string.IsNullOrEmpty(appointment.CrewCodes))
                        {
                            string currentCrews = appointment.CrewCodes;
                            //currentCrews = currentCrews.Replace(appointment.CrewCD, "");
                            if (!string.IsNullOrEmpty(currentCrews))
                            {
                                string[] crews = currentCrews.Split(',');
                                string newCrews = string.Empty;
                                foreach (string crew in crews)
                                {
                                    if (!string.IsNullOrEmpty(crew) && crew != "," && crew.Trim() != appointment.CrewCD.Trim())
                                    {
                                        if (string.IsNullOrEmpty(newCrews))
                                            newCrews = crew;
                                        else
                                            newCrews = newCrews + "," + crew;
                                    }
                                }
                                description.Append("</td></tr><tr><td colspan='2'><strong>Crew: </strong>");
                                description.Append(newCrews);
                            }
                        }
                    }
                }
                description.Append(" </td></tr></table>");
            }

            return description.ToString();
        }



        public string BuildWeeklyCrewOnlyToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.WeeklyCrew;

                description.Append("<br />");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append(" <br>");
            }

            return description.ToString();
        }

        public string BuildWeeklyCrewToolTipDescriptionAppt(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.WeeklyCrew;
                //if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date)
                //{
                //    description.Append("<br />\n");
                //    description.Append("** Arrival Date: ");
                //    description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                //    description.Append(" **");
                //    description.Append("<br />\n");
                //}

                description.Append("<br />");
                if (appointment.ShowAllDetails)
                {
                    if (options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append("     ");
                    }
                    //description.Append("<br />");

                    if (options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);
                    }
                    //description.Append("<br />");

                    description.Append(" <tr><td colspan='2'>");
                    if (options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (options.CummulativeETE)
                    {
                        description.Append("<br /><strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (options.TotalETE)
                    {
                        description.Append("<br /><strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append("     ");
                    }
                    //description.Append("<br />");
                    if (options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("    ");
                    }

                    if (options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.TripPurpose)
                    {
                        description.Append("<br /><strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.LegPurpose)
                    {
                        description.Append("<br /><strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.SeatsAvailable)
                    {
                        description.Append("<br /><strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    //description.Append("<br />");

                    if (options.Crew)
                    {
                        description.Append("<br /><strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCodes))
                        {
                            //description.Append("<strong>Crew: </strong>");
                            //description.Append(appointment.CrewCodes);

                            string currentCrews = appointment.CrewCodes;
                            currentCrews = currentCrews.Replace(appointment.CrewCD, "");
                            if (!string.IsNullOrEmpty(currentCrews))
                            {
                                string[] crews = currentCrews.Split(',');
                                string newCrews = string.Empty;
                                foreach (string crew in crews)
                                {
                                    if (!string.IsNullOrEmpty(crew) && crew != ",")
                                    {
                                        if (string.IsNullOrEmpty(newCrews))
                                            newCrews = crew;
                                        else
                                            newCrews = newCrews + "," + crew;
                                    }
                                }
                                //description.Append("<strong>Crew: </strong>");
                                description.Append(newCrews);
                            }
                        }

                        /*if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCD);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }*/
                    }
                }
                else // show details based on Trip Privacy Settins from company profile
                {

                    if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append("     ");
                    }
                    //description.Append("<br />");

                    if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                    {
                        description.Append("<br /><strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    description.Append("\n");
                    if (PrivateTrip.IsShowETE && options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                    {
                        description.Append("<br /><strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (PrivateTrip.IsShowTotalETE && options.TotalETE)
                    {
                        description.Append("<br /><strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append("     ");
                    }
                    //description.Append("<\n>");
                    if (PrivateTrip.IsShowRequestor && options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (PrivateTrip.IsShowDepartment && options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("     ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowAuthorization && options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    // description.Append("<br />");
                    if (PrivateTrip.IsShowTripPurpose && options.TripPurpose)
                    {
                        description.Append("<br /><strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                    {
                        description.Append("<br /><strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                    {
                        description.Append("<br /><strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    //description.Append("<br />");

                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {

                        description.Append("<br /><strong>Crew: </strong>");
                        // todo: get from Anoop
                        /*description.Append("<strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCD);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }*/
                        if (!string.IsNullOrEmpty(appointment.CrewCodes))
                        {
                            //description.Append("<strong>Crew: </strong>");
                            //description.Append(appointment.CrewCodes);

                            string currentCrews = appointment.CrewCodes;
                            currentCrews = currentCrews.Replace(appointment.CrewCD, "");
                            if (!string.IsNullOrEmpty(currentCrews))
                            {
                                string[] crews = currentCrews.Split(',');
                                string newCrews = string.Empty;
                                foreach (string crew in crews)
                                {
                                    if (!string.IsNullOrEmpty(crew) && crew != ",")
                                    {
                                        if (string.IsNullOrEmpty(newCrews))
                                            newCrews = crew;
                                        else
                                            newCrews = newCrews + "," + crew;
                                    }
                                }
                                //description.Append("<strong>Crew: </strong>");
                                description.Append(newCrews);
                            }
                        }
                    }
                }
                //description.Append(" </td></tr></table>");
            }
            return description.ToString();
        }

        public string BuildWeeklyCrewOnlyToolTipDescriptionAppt(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {
                var options = displayOptions.WeeklyCrew;
                description.Append("<br />");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append("<br />");
            }

            return description.ToString();
        }
        //Departure and Arrival information for tooltip
        private void GetDepartureArrivalInfo(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DepartArriveInfo departureArrivalInfo, StringBuilder description)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, departureArrivalInfo, description))
            {
                if (appointment.ShowAllDetails)
                {
                    description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                    description.Append(" ");
                    switch (departureArrivalInfo)
                    {
                        case DepartArriveInfo.ICAO:

                            description.Append(appointment.DepartureICAOID);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                            break;

                        case DepartArriveInfo.AirportName:

                            description.Append(appointment.DepartureAirportName);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                            break;

                        case DepartArriveInfo.City:

                            description.Append(appointment.DepartureCity);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                            break;

                    }
                    description.Append(" ");
                    description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                }
                else // show as trip privacy settings from company profile
                {
                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        description.Append(" ");
                    }

                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (departureArrivalInfo)
                        {
                            case DepartArriveInfo.ICAO:

                                description.Append(appointment.DepartureICAOID);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                                break;

                            case DepartArriveInfo.AirportName:

                                description.Append(appointment.DepartureAirportName);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                                break;

                            case DepartArriveInfo.City:

                                description.Append(appointment.DepartureCity);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                                break;

                        }
                    }

                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (PrivateTrip.IsShowArrivalDepartICAO)
                        {
                            description.Append(" ");
                        }
                        else
                        {
                            description.Append(" | ");

                        }

                        description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                    }
                }


            }


        }



        # region "Navigation bar"
        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyCrewPreviousCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            loadTree(false);
                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCWeeklyCrewPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        protected void nextButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyCrewNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCWeeklyCrewNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyCrewLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCWeeklyCrewLastCount"] = ++count;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void btnFirst_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyCrewFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            //DateTime today = DateTime.Today;
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCWeeklyCrewFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        #endregion


        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.WeeklyCrew);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }


        protected void RadWeeklyScheduler_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove("CurrentPreFlightTrip");
                        if (e.MenuItem.Text == "Add Fleet Calendar Entry")
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Add Crew Calendar Entry")
                        {
                            return;
                        }

                        if (e.MenuItem.Value == "../PreFlightMain.aspx") // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                            Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(e.TimeSlot.Start);

                            if (HomeBaseId != null && HomeBaseId != 0)
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of login user

                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = BaseDescription;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;
                            }
                            if (ClientId != null && ClientId != 0)
                            {
                                Trip.ClientID = ClientId;
                                PreflightService.Client Client = new PreflightService.Client();
                                Client.ClientCD = ClientCode;
                                Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                Trip.Client = Client;
                            }

                            if (e.MenuItem.Text == "Add New Trip")
                            {
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                            }
                            else if (Trip.TripNUM != null && Trip.TripNUM != 0)
                            {
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                            }
                            else
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                Session.Remove("PreflightException");
                            }
                            Response.Redirect(e.MenuItem.Value);
                        }


                        RadMenuItem item = e.MenuItem.Parent as RadMenuItem;
                        if (item != null && (item.Text == "Quick Crew Calendar Entry"))
                        {
                            if (!string.IsNullOrEmpty(e.MenuItem.Value))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objService.GetCrewDutyTypeList().EntityList.Where(x => x.DutyTypeCD.Trim().ToUpper() == (e.MenuItem.Value.ToUpper())).ToList();

                                    if (objRetVal.Count() > 0)
                                    {
                                        string CrewDutyDescription = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyTypesDescription.ToString();
                                        string CrewDutyCode = e.MenuItem.Value;
                                        DateTime Sdate = e.TimeSlot.Start;
                                        string StartTime = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyStartTM.ToString();
                                        Sdate = Sdate.AddHours(Convert.ToInt16(StartTime.Substring(0, 2)));
                                        Sdate = Sdate.AddMinutes(Convert.ToInt16(StartTime.Substring(3, 2)));
                                        DateTime EDate = e.TimeSlot.Start;
                                        string EndTime = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyEndTM.ToString();
                                        EDate = EDate.AddHours(Convert.ToInt16(EndTime.Substring(0, 2)));
                                        EDate = EDate.AddMinutes(Convert.ToInt16(EndTime.Substring(3, 2)));

                                        string crewcode = e.TimeSlot.Resource.Key.ToString();
                                        string CrewID = string.Empty;
                                        using (var client = new MasterCatalogServiceClient())
                                        {
                                            var objRetValCrewID = client.GetCrewList().EntityList.Where(x => x.CrewCD.Trim().ToUpper() == (crewcode.Trim().ToUpper())).ToList();

                                            if (objRetValCrewID.Count() > 0)
                                                CrewID = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetValCrewID[0]).CrewID.ToString();
                                        }
                                        //----
                                        PreflightMain Trip = new PreflightMain();
                                        Trip.CrewID = Convert.ToInt64(CrewID);
                                        Trip.FleetID = null;
                                        Trip.Notes = CrewDutyDescription;
                                        Trip.HomebaseID = Convert.ToInt64(HomeBaseId);
                                        Trip.RecordType = "C";
                                        Trip.PreviousNUM = Convert.ToInt64("0");
                                        Trip.IsDeleted = false;
                                        Trip.ClientID = ClientId;
                                        Trip.LastUpdUID = UserPrincipal.Identity._name;
                                        Trip.LastUpdTS = System.DateTime.UtcNow;

                                        PreflightLeg Leg = new PreflightLeg();
                                        Leg.LegNUM = Convert.ToInt64("1");
                                        string timebase = FilterOptions.TimeBase.ToString();

                                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                        {
                                            if (timebase.ToUpper() == "LOCAL")
                                            {
                                                Leg.DepartureDTTMLocal = Sdate;
                                                Leg.ArrivalDTTMLocal = EDate;
                                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                            }
                                            else if (timebase.ToUpper() == "UTC")
                                            {
                                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                                Leg.DepartureGreenwichDTTM = Sdate;
                                                Leg.ArrivalGreenwichDTTM = EDate;
                                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                            }
                                            else
                                            {
                                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                                Leg.HomeDepartureDTTM = Sdate;
                                                Leg.HomeArrivalDTTM = EDate;
                                            }
                                        }
                                        Leg.DepartICAOID = HomeAirportId;
                                        Leg.ArriveICAOID = HomeAirportId;
                                        Leg.DutyTYPE = CrewDutyCode;
                                        Leg.IsDeleted = false;
                                        Leg.ClientID = ClientId;
                                        Leg.LastUpdUID = UserPrincipal.Identity._name;
                                        Leg.LastUpdTS = System.DateTime.UtcNow;

                                        PreflightCrewList Crew = new PreflightCrewList();
                                        Crew.CrewID = Convert.ToInt64(CrewID);
                                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService1 = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal1 = ObjService1.GetCrewList().EntityList.Where(x => x.CrewID == Convert.ToInt64(CrewID)).ToList();
                                            Crew.CrewFirstName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal1[0]).FirstName.ToString();
                                            Crew.CrewLastName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal1[0]).LastName.ToString();
                                            Crew.CrewMiddleName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal1[0]).MiddleInitial.ToString();
                                        }

                                        Crew.IsDeleted = false;
                                        Crew.LastUpdUID = UserPrincipal.Identity._name;
                                        Crew.LastUpdTS = System.DateTime.UtcNow;
                                        Trip.State = TripEntityState.Added;
                                        Leg.State = TripEntityState.Added;
                                        Crew.State = TripEntityState.Added;
                                        Leg.PreflightCrewLists = new List<PreflightCrewList>();
                                        Leg.PreflightCrewLists.Add(Crew);
                                        Trip.PreflightLegs = new List<PreflightLeg>();
                                        Trip.PreflightLegs.Add(Leg);
                                        //---------------------
                                        using (PreflightServiceClient Service = new PreflightServiceClient())
                                        {
                                            var ReturnValue = Service.Add(Trip);
                                            if (ReturnValue.ReturnFlag == true)
                                            {
                                                loadTree(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (e.MenuItem.Value == "QuickCrewEntry") // quick crew entry main menu item selected...
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                        {
                            var tripnum = string.Empty;
                            Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml"+ "&tripnum=" + tripnum );
                        }
                        else
                        {
                            Response.Redirect(e.MenuItem.Value);
                        }
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "PleaseWait", "stopPleaseWait();", true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        //Get resource type based on  Crew type...
        public ResourceType GetResourceType()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                ResourceType resourceType = new ResourceType("CrewFleetCodes");

                resourceType.KeyField = ModuleNameConstants.Preflight.CewCode;
                resourceType.ForeignKeyField = ModuleNameConstants.Preflight.CewCode;
                resourceType.TextField = ModuleNameConstants.Preflight.Resource_Description;


                resourceType.AllowMultipleValues = true;

                return resourceType;

            }

        }

    }
}
