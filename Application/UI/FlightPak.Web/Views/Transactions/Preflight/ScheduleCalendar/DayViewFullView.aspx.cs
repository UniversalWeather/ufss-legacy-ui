﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using System.Drawing;
using FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar;
using System.Collections.ObjectModel;
using FlightPak.Web.Views.Transactions.PreFlight.ScheduleCalendar;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Entities.SchedulingCalendar;
using FlightPak.Web.FlightPakMasterService;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class DayViewFullView : ScheduleCalendarBase
    {
        private int DayFleetTreeCount { get; set; }
        private int DayCrewTreeCount { get; set; }
        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty;  

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        Session["SCAdvancedTab"] = CurrentDisplayOption.Day;

                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainContent");
                        UserControl ucSCMenu = (UserControl)contentPlaceHolder.FindControl("TabSchedulingCalendar");
                        ucSCMenu.Visible = false;
                       /* if (!string.IsNullOrEmpty(Request.QueryString["width"]))
                        {

                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["height"]))
                        {
                            int scrHeight = Convert.ToInt32(Request.QueryString["height"]);
                            RadSplitter1.Height = scrHeight - scrHeight / 11;
                            LeftPane.Height = scrHeight - scrHeight / 11;
                            RadPanelBar1.Height = scrHeight - scrHeight / 11;
                            dgDay.Height = scrHeight - scrHeight / 11;
                        }*/
                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.Day)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }
                            //To reduce service call - Vishwa
                            FilterCriteria.userSettingAvailable = base.scWrapper.userSettingAvailable;
                            FilterCriteria.retriveSystemDefaultSettings = base.scWrapper.retrieveSystemDefaultSettings;
                            //End
                            var selectedDay = DateTime.Today;
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }

                            StartDate = selectedDay;
                            EndDate = GetEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;
                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session.Remove("SCNextCount");
                            Session.Remove("SCPreviousCount");
                            Session.Remove("SCLastCount");
                            Session.Remove("SCFirstCount");

                            loadTree(false);
                        }
                        else
                        {
                           
                            CheckValidDate();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Day);
                }
            }


        }

          /// <summary>
        /// To Validate Date
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            StartDate = (DateTime)Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "',275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            EndDate = GetEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
            lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

            if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
            {
                return true;
            }
            else
            {
                // avoid duplicate calls for loadTree();
                string id = Page.Request.Params["__EVENTTARGET"];

                if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
                {
                    loadTree(false);
                }
            }
            return IsDateCheck;
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //fleet trips list
                if (!Page.IsPostBack)
                {
                    /*//Changes done for the Bug ID - 5828
                    bool IsFleetTV = true;
                    if (Session["IsFleetChecked"] != null)
                    {
                        IsFleetTV = Convert.ToBoolean(Session["IsFleetChecked"]);
                    }
                    BindFleetTree(IsFleetTV);
                    BindCrewTree(!IsFleetTV);*/

                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Day);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                    Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                        //BindFleetTree(Fleetlst, true);
                        //BindCrewTree(Crewlst, false);
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }

                    if (!string.IsNullOrEmpty(FleetCrew[1]))
                    {
                        string[] Crewids = FleetCrew[1].Split(',');
                        foreach (string cIDs in Crewids)
                        {
                            string[] ParentChild = cIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                        }
                        //BindFleetTree(Fleetlst, false);
                        //BindCrewTree(Crewlst, true);
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                    else
                    {
                        //BindFleetTree(Fleetlst, true);
                        //BindCrewTree(Crewlst, false);
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                }

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();

                    if (IsSave)
                    {
                        var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                        var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);

                        if (selectedFleetIDs.Count > 0)
                        {
                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Day, selectedFleetIDs, null, null, false);
                            //To get the distinct values
                            List<string> distinctFleet = selectedFleetIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                            string strTestFleet = string.Join(",", distinctFleet.ToArray());
                            Session["FleetIDs"] = strTestFleet;
                        }
                        else
                        {
                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Day, null, selectedCrewIDs, null, false);
                            //To get the distinct values
                            List<string> distinctCrew = selectedCrewIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                            string strTestCrew = string.Join(",", distinctCrew.ToArray());
                            Session["CrewIDs"] = strTestCrew;
                        }

                        //BindDayViewInfo(preflightServiceClient, StartDate.AddDays(-15), EndDate.AddDays(15), FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Day);
                        //dgDay.Visible = true;
                    }
                    else
                    {
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Day);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CrewIDs = FleetCrew[1];

                            if ((string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0) && (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0))
                            {
                                if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                                {
                                    FleetTreeView.CheckAllNodes();
                                }
                            }

                            Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                            Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                            if (!string.IsNullOrEmpty(FleetCrew[0]))
                            {
                                string[] Fleetids = FleetCrew[0].Split(',');
                                foreach (string fIDs in Fleetids)
                                {
                                    string[] ParentChild = fIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                }
                            }
                            else if (Fleetlst.Count == 0)
                            {
                                string[] Crewids = FleetCrew[1].Split(',');
                                foreach (string cIDs in Crewids)
                                {
                                    string[] ParentChild = cIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                }
                            }
                            else
                                Fleetlst = GetCheckedTreeNodes(FleetTreeView);
                        }
                    }
                    BindDayViewInfo(preflightServiceClient, StartDate.AddDays(-15), EndDate.AddDays(15), FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Day);
                    dgDay.Visible = true;
                }
                RadPanelBar2.FindItemByText("Fleet").Visible = true;
                RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                RadPanelBar2.FindItemByText("Crew").Visible = true;
                RadPanelBar2.FindItemByText("Crew").Expanded = true;
                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;

                RightPane.Collapsed = true;
            }
        }

        /*private void BindFleetTree(Collection<string> selectedFleetIDs, bool IsChecked)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;

                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodes(fleetList, IsChecked);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;
                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<string> selectedcrewIDs, bool IsChecked)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodes(crewList, IsChecked);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;
                        CrewTreeView.DataBind();
                    }
                }
            }
        }*/

        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;

                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;
                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true
                        
                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNew(crewList, selectedcrewIDs);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;
                        CrewTreeView.DataBind();
                    }
                }
            }
        }


        protected DateTime GetEndDate(DateTime startDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate))
            {
                return startDate.AddHours(24).AddMilliseconds(-1);
            }
        }

        private void BindDayViewInfo(PreflightServiceClient preflightServiceClient, DateTime dayStartDate, DateTime dayEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, dayStartDate, dayEndDate, filterCriteria, displayOptions, viewMode))
            {
                if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                {
                    FleetTreeView.CheckAllNodes();
                }

                var schedulingCalendar = new SchedulingCalendar();
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);
                var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView).Distinct().ToList();
                var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView, false).Distinct().ToList();

                DayFleetTreeCount = inputFromFleetTree.Count; // this property is used in appointment data bound to identify the view 
                DayCrewTreeCount = inputFromCrewTree.Count; // this property is used in appointment data bound to identify the view 

                Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                if (inputFromFleetTree.Count > 0) // if fleet tree is selected
                {
                    Collection<string> InputFromFleetTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                    {
                        InputFromFleetTree.Add(s.ChildNodeId);
                    }
                    var calendarFleetData = preflightServiceClient.GetFleetCalendarData(dayStartDate, dayEndDate, serviceFilterCriteria, InputFromFleetTree.ToList(), true, false, true);

                    //Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                    if (displayOptions.Day.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                    {
                        // remove records of type='C' and tailNum == null...

                        CrewTreeView.CheckAllNodes();
                        var inputFromCrewTreeAll = GetCheckedTreeNodes(CrewTreeView, false);

                        Collection<string> InputFromCrewTreeAll = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromCrewTreeAll)
                        {
                            InputFromCrewTreeAll.Add(s.ChildNodeId);
                        }

                        var calendarcrewData = preflightServiceClient.GetCrewCalendarData(dayStartDate, dayEndDate, serviceFilterCriteria, InputFromCrewTreeAll.ToList(), true);

                        if (calendarFleetData.EntityList != null || calendarcrewData.EntityList != null)
                        {
                            fleetappointments = ConvertToFleetAppointmentEntity(calendarFleetData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
                            if (calendarcrewData.EntityList != null)
                            {
                                crewappointments = ConvertToCrewAppointmentEntity(calendarcrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);

                                IList<RadTreeNode> nodecrewCollection = CrewTreeView.CheckedNodes;
                                List<string> crewCodesFromTree = GetTailNumsFromTree(nodecrewCollection);
                                CrewTreeView.UncheckAllNodes();
                                SetDummyAppointments(displayOptions, crewappointments, crewCodesFromTree);
                            }
                        }
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarFleetData.EntityList = calendarFleetData.EntityList.Where(x => x.TailNum != null).ToList();
                        fleetappointments = ConvertToFleetAppointmentEntity(calendarFleetData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
                    }

                    // build dummy appointments to load in grid for selected tree view...
                    IList<RadTreeNode> nodeCollection = FleetTreeView.CheckedNodes;
                    List<string> tailNumsFromTree = GetTailNumsFromTree(nodeCollection);
                    SetDummyAppointments(displayOptions, fleetappointments, tailNumsFromTree);
                    var sortOption = (DayWeeklyDetailSortOption)displayOptions.Day.DayWeeklyDetailSortOption;

                    //Sort data based on display option 
                    if (sortOption.ToString() == DayWeeklyDetailSortOption.TailNumber.ToString())
                    {
                        fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>(fleetappointments.OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList());
                        if (crewappointments.Count > 0)
                        {
                            var appointmentsWithoutTailNum = crewappointments.Where(x => x.TailNum == null).ToList();
                            var appointmentsWithTailNum = crewappointments.Where(x => x.TailNum != null).OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList();
                            crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                            crewappointments.AddRange(appointmentsWithoutTailNum);
                            crewappointments.AddRange(appointmentsWithTailNum);
                        }
                        //dgDay.DataSource = new Collection<Entities.SchedulingCalendar.Appointment>(fleetappointments.OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList());                        
                    }
                    else // departureDate
                    {
                        fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>(fleetappointments.OrderBy(x => x.StartDate).ToList());
                        //dgDay.DataSource = new Collection<Entities.SchedulingCalendar.Appointment>(fleetappointments.OrderBy(x => x.StartDate).ToList());
                    }

                    if (displayOptions.Day.FirstLastLeg)
                    {
                        Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> firstLastLegFleetappColl = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                        Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> firstLastLegCrewappColl = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                        var distinctTripNos = fleetappointments.Select(x => x.TripId).Distinct().ToList();
                        // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                        foreach (var tripNo in distinctTripNos)
                        {
                            // build appointment data based on first/last leg display option
                            var groupedLegs = fleetappointments.Where(p => p.TripId == tripNo).ToList();
                            //DateTime tstartdate = Convert.ToDateTime(tbDate.Text).Date;
                            //DateTime tenddate = Convert.ToDateTime(tbDate.Text).Date.AddDays(1).AddSeconds(-1);
                            var firstLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).LastOrDefault();
                            var lastLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).FirstOrDefault();
                            foreach (var leg in groupedLegs)
                            {
                                if (leg.LegNum == 0 || leg.LegNum == firstLeg.LegNum || leg.LegNum == lastLeg.LegNum)
                                {
                                    firstLastLegFleetappColl.Add(leg);
                                }
                            }
                        }


                        var distinctcrewTripNos = crewappointments.Select(x => x.TripId).Distinct().ToList();
                        // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                        foreach (var crewtripNo in distinctcrewTripNos)
                        {
                            // build appointment data based on first/last leg display option
                            var groupedcrewLegs = crewappointments.Where(p => p.TripId == crewtripNo).ToList();
                            //DateTime tstartdate = Convert.ToDateTime(tbDate.Text).Date;
                            //DateTime tenddate = Convert.ToDateTime(tbDate.Text).Date.AddDays(1).AddSeconds(-1);
                            var firstcrewLeg = groupedcrewLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).LastOrDefault();
                            var lastcrewLeg = groupedcrewLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).FirstOrDefault();
                            foreach (var crewleg in groupedcrewLegs)
                            {
                                if (crewleg.LegNum == 0 || crewleg.LegNum == firstcrewLeg.LegNum || crewleg.LegNum == lastcrewLeg.LegNum)
                                {
                                    firstLastLegCrewappColl.Add(crewleg);
                                }
                            }
                        }
                        dgDay.DataSource = firstLastLegFleetappColl.Concat(firstLastLegCrewappColl).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                    }
                    else
                        dgDay.DataSource = fleetappointments.Concat(crewappointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                }
                else if (inputFromCrewTree.Count > 0) // if crew tree is selected
                {
                    //Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                    Collection<string> InputFromCrewTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                    {
                        InputFromCrewTree.Add(s.ChildNodeId);
                    }
                    var calendarCrewData = preflightServiceClient.GetCrewCalendarData(dayStartDate, dayEndDate, serviceFilterCriteria, InputFromCrewTree.ToList(), true);
                    //var crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);
                    crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
                    //Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                    /*if (displayOptions.Day.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                    {
                        // remove records of type='C' and tailNum == null...
                        crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarCrewData.EntityList = calendarCrewData.EntityList.Where(x => x.TailNum != null).ToList();
                        crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
                    }*/

                    IList<RadTreeNode> nodeCollection = CrewTreeView.CheckedNodes;
                    List<string> crewCodesFromTree = GetTailNumsFromTree(nodeCollection);

                    SetDummyAppointments(displayOptions, crewappointments, crewCodesFromTree);
                    var sortOption = (DayWeeklyDetailSortOption)displayOptions.Day.DayWeeklyDetailSortOption;

                    //Sort data based on display option -departureDate
                    if (sortOption.ToString() == DayWeeklyDetailSortOption.TailNumber.ToString())
                    {
                        var appointmentsWithoutTailNum = crewappointments.Where(x => x.TailNum == null).ToList();
                        var appointmentsWithTailNum = crewappointments.Where(x => x.TailNum != null).OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList();
                        crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                        crewappointments.AddRange(appointmentsWithoutTailNum);
                        crewappointments.AddRange(appointmentsWithTailNum);
                        //dgDay.DataSource = crewappointmentsnew.ToList();
                        //dgDay.DataSource = crewappointments.Where(x => x.TailNum == null).ToList();
                        //dgDay.DataSource = crewappointments.Where(x => x.TailNum != null).OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList();
                    }
                    else // departureDate
                    {
                        crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>(crewappointments.OrderBy(x => x.StartDate).ToList());
                        //dgDay.DataSource = new Collection<Entities.SchedulingCalendar.Appointment>(crewappointments.OrderBy(x => x.StartDate).ToList());
                    }

                    if (displayOptions.Day.FirstLastLeg)
                    {
                        Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> firstLastLegCrewappColl = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                        var distinctTripNos = crewappointments.Select(x => x.TripId).Distinct().ToList();
                        // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                        foreach (var tripNo in distinctTripNos)
                        {
                            // build appointment data based on first/last leg display option
                            var groupedLegs = crewappointments.Where(p => p.TripId == tripNo).ToList();
                            //DateTime tstartdate = Convert.ToDateTime(tbDate.Text).Date;
                            //DateTime tenddate = Convert.ToDateTime(tbDate.Text).Date.AddDays(1).AddSeconds(-1);
                            var firstLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).LastOrDefault();
                            var lastLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).FirstOrDefault();
                            foreach (var leg in groupedLegs)
                            {
                                if (leg.LegNum == 0 || leg.LegNum == firstLeg.LegNum || leg.LegNum == lastLeg.LegNum)
                                {
                                    firstLastLegCrewappColl.Add(leg);
                                }
                            }
                        }
                        dgDay.DataSource = firstLastLegCrewappColl.ToList();
                    }
                    else
                        dgDay.DataSource = crewappointments.ToList();

                    //dgDay.DataSource = crewappointments.ToList();                   
                    //dgDay.DataSource =  (fleetappointments.ToList().Count > 0)?fleetappointments.ToList().Count > 0;
                }
                var departureArrivalInfo = DisplayOptions.Day.DepartArriveInfo;

                /*if (fleetappointments.ToList().Count > 0 && crewappointments.ToList().Count > 0)
                {
                    dgDay.DataSource = fleetappointments.ToList().Concat(crewappointments.ToList()).ToList();
                }
                else if (fleetappointments.ToList().Count > 0)
                {
                    dgDay.DataSource = fleetappointments.ToList();
                }
                else if (crewappointments.ToList().Count > 0)
                {
                    dgDay.DataSource = crewappointments.ToList();
                }
                else
                    dgDay.DataSource = fleetappointments.ToList();*/

                dgDay.Columns.FindByUniqueName("Depart").HeaderText = "Dep " + departureArrivalInfo.ToString().Replace("Name", "");
                dgDay.Columns.FindByUniqueName("Arrive").HeaderText = "Arr " + departureArrivalInfo.ToString().Replace("Name", "");

                dgDay.Columns.FindByUniqueName("ETE").Visible = displayOptions.Day.ETE;
                dgDay.Columns.FindByUniqueName("TotalETE").Visible = displayOptions.Day.TotalETE;
                dgDay.Columns.FindByUniqueName("CummETE").Visible = displayOptions.Day.CummulativeETE;
                dgDay.Columns.FindByUniqueName("TripStatus").Visible = displayOptions.Day.ShowTripStatus;
                dgDay.DataBind();
            }
        }

        //private void BindDayViewInfo(PreflightServiceClient preflightServiceClient, DateTime dayStartDate, DateTime dayEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, dayStartDate, dayEndDate, filterCriteria, displayOptions, viewMode))
        //    {
        //        if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
        //        {
        //            FleetTreeView.CheckAllNodes();
        //        }

        //        var schedulingCalendar = new SchedulingCalendar();
        //        PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);
        //        var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView).Distinct().ToList();
        //        var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView).Distinct().ToList();

        //        DayFleetTreeCount = inputFromFleetTree.Count; // this property is used in appointment data bound to identify the view 
        //        DayCrewTreeCount = inputFromCrewTree.Count; // this property is used in appointment data bound to identify the view 

        //        Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

        //        if (inputFromFleetTree.Count > 0) // if fleet tree is selected
        //        {
        //            var calendarFleetData = preflightServiceClient.GetFleetCalendarData(dayStartDate, dayEndDate, serviceFilterCriteria, inputFromFleetTree, true, false, true);
        //            //Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
        //            if (displayOptions.Day.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
        //            {
        //                // remove records of type='C' and tailNum == null...
        //                fleetappointments = ConvertToFleetAppointmentEntity(calendarFleetData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
        //            }
        //            else
        //            {
        //                // by default REcordType, T, M and C with tailNum will be displayed...
        //                calendarFleetData.EntityList = calendarFleetData.EntityList.Where(x => x.TailNum != null).ToList();
        //                fleetappointments = ConvertToFleetAppointmentEntity(calendarFleetData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
        //            }

        //            // build dummy appointments to load in grid for selected tree view...
        //            IList<RadTreeNode> nodeCollection = FleetTreeView.CheckedNodes;
        //            List<string> tailNumsFromTree = GetTailNumsFromTree(nodeCollection);
        //            SetDummyAppointments(displayOptions, fleetappointments, tailNumsFromTree);
        //            var sortOption = (DayWeeklyDetailSortOption)displayOptions.Day.DayWeeklyDetailSortOption;

        //            //Sort data based on display option 
        //            if (sortOption.ToString() == DayWeeklyDetailSortOption.TailNumber.ToString())
        //            {
        //                fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>(fleetappointments.OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList());
        //            }
        //            else // departureDate
        //            {
        //                fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>(fleetappointments.OrderBy(x => x.StartDate).ToList());
        //            }
        //            if (displayOptions.Day.FirstLastLeg)
        //            {
        //                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> firstLastLegFleetappColl = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

        //                var distinctTripNos = fleetappointments.Select(x => x.TripId).Distinct().ToList();
        //                // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

        //                foreach (var tripNo in distinctTripNos)
        //                {
        //                    // build appointment data based on first/last leg display option
        //                    var groupedLegs = fleetappointments.Where(p => p.TripId == tripNo).ToList();
        //                    //DateTime tstartdate = Convert.ToDateTime(tbDate.Text).Date;
        //                    //DateTime tenddate = Convert.ToDateTime(tbDate.Text).Date.AddDays(1).AddSeconds(-1);
        //                    var firstLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).LastOrDefault();
        //                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).FirstOrDefault();
        //                    foreach (var leg in groupedLegs)
        //                    {
        //                        if (leg.LegNum == 0 || leg.LegNum == firstLeg.LegNum || leg.LegNum == lastLeg.LegNum)
        //                        {
        //                            firstLastLegFleetappColl.Add(leg);
        //                        }
        //                    }
        //                }
        //                dgDay.DataSource = firstLastLegFleetappColl.ToList();
        //            }
        //            else
        //                dgDay.DataSource = fleetappointments.ToList();
        //        }
        //        else  if (inputFromCrewTree.Count > 0) // if crew tree is selected                
        //        {
        //            Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
        //            var calendarCrewData = preflightServiceClient.GetCrewCalendarData(dayStartDate, dayEndDate, serviceFilterCriteria, inputFromCrewTree, true);
        //            //var crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);

        //            //Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
        //            if (displayOptions.Day.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
        //            {
        //                // remove records of type='C' and tailNum == null...
        //                crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
        //            }
        //            else
        //            {
        //                // by default REcordType, T, M and C with tailNum will be displayed...
        //                calendarCrewData.EntityList = calendarCrewData.EntityList.Where(x => x.TailNum != null).ToList();
        //                crewappointments = ConvertToCrewAppointmentEntity(calendarCrewData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, dayStartDate, dayEndDate);
        //            }

        //            IList<RadTreeNode> nodeCollection = CrewTreeView.CheckedNodes;
        //            List<string> crewCodesFromTree = GetTailNumsFromTree(nodeCollection);

        //            SetDummyAppointments(displayOptions, crewappointments, crewCodesFromTree);
        //            var sortOption = (DayWeeklyDetailSortOption)displayOptions.Day.DayWeeklyDetailSortOption;

        //            //Sort data based on display option -departureDate
        //            if (sortOption.ToString() == DayWeeklyDetailSortOption.TailNumber.ToString())
        //            {
        //                var appointmentsWithoutTailNum = crewappointments.Where(x => x.TailNum == null).ToList();
        //                var appointmentsWithTailNum = crewappointments.Where(x => x.TailNum != null).OrderBy(x => x.TailNum.Trim()).ThenBy(x => x.StartDate).ToList();
        //                crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();                        
        //                crewappointments.AddRange(appointmentsWithoutTailNum);
        //                crewappointments.AddRange(appointmentsWithTailNum);
        //            }
        //            else // departureDate
        //            {
        //                crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>(crewappointments.OrderBy(x => x.StartDate).ToList());
        //            }
        //            if (displayOptions.Day.FirstLastLeg)
        //            {
        //                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> firstLastLegCrewappColl = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

        //                var distinctTripNos = crewappointments.Select(x => x.TripId).Distinct().ToList();
        //                // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

        //                foreach (var tripNo in distinctTripNos)
        //                {
        //                    // build appointment data based on first/last leg display option
        //                    var groupedLegs = crewappointments.Where(p => p.TripId == tripNo).ToList();
        //                    //DateTime tstartdate = Convert.ToDateTime(tbDate.Text).Date;
        //                    //DateTime tenddate = Convert.ToDateTime(tbDate.Text).Date.AddDays(1).AddSeconds(-1);
        //                    var firstLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).LastOrDefault();
        //                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).FirstOrDefault();
        //                    foreach (var leg in groupedLegs)
        //                    {
        //                        if (leg.LegNum == 0 || leg.LegNum == firstLeg.LegNum || leg.LegNum == lastLeg.LegNum)
        //                        {
        //                            firstLastLegCrewappColl.Add(leg);
        //                        }
        //                    }
        //                }
        //                dgDay.DataSource = firstLastLegCrewappColl.ToList();
        //            }
        //            else
        //                dgDay.DataSource = crewappointments.ToList();

        //            //dgDay.DataSource = crewappointments.ToList();                   
        //            //dgDay.DataSource =  (fleetappointments.ToList().Count > 0)?fleetappointments.ToList().Count > 0;
        //        }

        //        var departureArrivalInfo = DisplayOptions.Day.DepartArriveInfo;

        //        /*if (fleetappointments.ToList().Count > 0 && crewappointments.ToList().Count > 0)
        //        {
        //            dgDay.DataSource = fleetappointments.ToList().Concat(crewappointments.ToList()).ToList();
        //        }
        //        else if (fleetappointments.ToList().Count > 0)
        //        {
        //            dgDay.DataSource = fleetappointments.ToList();
        //        }
        //        else if (crewappointments.ToList().Count > 0)
        //        {
        //            dgDay.DataSource = crewappointments.ToList();
        //        }
        //        else
        //            dgDay.DataSource = fleetappointments.ToList();*/

        //        //dgDay.Columns.FindByUniqueName("CummETE").Visible = displayOptions.Day.CummulativeETE;
        //        dgDay.Columns.FindByUniqueName("TripStatus").Visible = displayOptions.Day.ShowTripStatus;
        //        dgDay.DataBind();
        //    }
        //}

        private static void SetDummyAppointments(DisplayOptions displayOptions, Collection<Entities.SchedulingCalendar.Appointment> appointments, List<string> tailNumsFromTree)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, appointments, tailNumsFromTree))
            {

                if (!displayOptions.Day.ActivityOnly)
                {
                    var tailNumsFromAppointments = appointments.Select(x => x.TailNum).Distinct().ToList();
                    // get tailNums that dont have any trip / entry...
                    var tailNumsWithoutActivity = tailNumsFromTree.Except(tailNumsFromAppointments).ToList();

                    if (tailNumsWithoutActivity.Count > 1)
                    {
                        // tailNumsFromTree.RemoveAt(0); //"Fleet"
                        foreach (var tail in tailNumsWithoutActivity)
                        {
                            FlightPak.Web.Entities.SchedulingCalendar.Appointment dummyAppointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                            dummyAppointment.TailNum = tail;
                            dummyAppointment.LastUpdatedTime = string.Empty;
                            dummyAppointment.FlightNum = string.Empty;
                            dummyAppointment.FlightPurpose = string.Empty;
                            // dummyAppointment.ShowAllDetails = true;
                            appointments.Add(dummyAppointment);
                        }
                    }
                }
            }
        }

        private static List<string> GetTailNumsFromTree(IList<RadTreeNode> nodeCollection)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(nodeCollection))
            {

                List<string> tailNumsFromTree = new List<string>();
                if (nodeCollection.Count > 0)
                {
                    nodeCollection.RemoveAt(0);
                    foreach (RadTreeNode node in nodeCollection)
                    {
                        string text = node.Text;
                        int delimiterIndex = text.IndexOf(" - ");
                        if (delimiterIndex != -1)
                        {
                            tailNumsFromTree.Add(text.Substring(0, delimiterIndex));
                        }
                    }
                }
                return tailNumsFromTree;
            }
        }

        private Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime dayStartDate, DateTime dayEndDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();
                // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option
                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                    foreach (var leg in groupedLegs)
                    {
                        /*if (displayOptions.Day.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                    if (leg.DepartureDisplayTime >= dayStartDate && leg.DepartureDisplayTime <= dayEndDate)
                                    {
                                        if (Convert.ToDateTime(leg.DepartureDisplayTime).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture) == tbDate.Text)
                                        {
                                            FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                            SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment); 
                                            appointmentCollection.Add(appointment);
                                        }
                                    }
                                    i++;
                                }                                
                            }
                        }
                        else // No first leg last leg selected
                        {*/
                            if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                    if (leg.DepartureDisplayTime >= dayStartDate && leg.DepartureDisplayTime <= dayEndDate)
                                    {
                                        if (Convert.ToDateTime(leg.DepartureDisplayTime).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture) == tbDate.Text)
                                        {                                            
                                            FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                            SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                            appointmentCollection.Add(appointment);
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(leg.DepartureDisplayTime).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture) == tbDate.Text)
                                {
                                    if (leg.DepartureDisplayTime >= dayStartDate && leg.DepartureDisplayTime <= dayEndDate)
                                    {

                                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                        SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                        appointmentCollection.Add(appointment);
                                    }
                                }
                            }                           
                        //}
                    }
                }
                return appointmentCollection;
            }
        }

        private Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime dayStartDate, DateTime dayEndDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
                {
                    Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                    var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();
                    // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                    foreach (var tripNo in distinctTripNos)
                    {
                        // build appointment data based on first/last leg display option

                        var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList();
                        var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                        foreach (var leg in groupedLegs)
                        {
                            /*if (displayOptions.Day.FirstLastLeg)
                            {
                                if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                                {
                                    int i = 0;
                                    DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                    DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                    TimeSpan span = EndDate.Subtract(StartDate);
                                    int totaldays = 0;

                                    if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                    {
                                        totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                    }
                                    else
                                    {
                                        totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                    }

                                    while (i < totaldays)
                                    {
                                        leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                        leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                        if (leg.DepartureDisplayTime >= dayStartDate && leg.DepartureDisplayTime <= dayEndDate)
                                        {
                                            if (Convert.ToDateTime(leg.DepartureDisplayTime).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture) == tbDate.Text)
                                            {
                                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                                appointmentCollection.Add(appointment);
                                            }
                                        }
                                        i++;
                                    }                                             
                                }
                                else if (leg.LegNUM == 0)
                                {
                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                    appointmentCollection.Add(appointment);
                                }
                            }
                            else // No first leg last leg selected
                            {*/
                                if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                                {
                                    int i = 0;
                                    DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                    DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                    TimeSpan span = EndDate.Subtract(StartDate);
                                    int totaldays = 0;

                                    if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                    {
                                        totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                    }
                                    else
                                    {
                                        totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                    }

                                    while (i < totaldays)
                                    {
                                        leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                        leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                        if (leg.DepartureDisplayTime >= dayStartDate && leg.DepartureDisplayTime <= dayEndDate)
                                        {
                                            if (Convert.ToDateTime(leg.DepartureDisplayTime).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture) == tbDate.Text)
                                            {
                                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                                appointmentCollection.Add(appointment);
                                            }
                                        }
                                        i++;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(leg.DepartureDisplayTime).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture) == tbDate.Text)
                                    {
                                        if (leg.DepartureDisplayTime >= dayStartDate && leg.DepartureDisplayTime <= dayEndDate)
                                        {

                                            FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                            SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                            appointmentCollection.Add(appointment);
                                        }
                                    }
                                }
                            //}
                        }
                    }
                    return appointmentCollection;
                }
            }
        }

        private void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;
                appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                SetDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                if (appointment.RecordType == "C")// for crew entry -> assign crewduty type code to aircraftDutyCD since it is used in aspx for binding
                {
                    appointment.AircraftDutyCD = appointment.CrewDutyTypeCD;
                }
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns
                }

                DateTime StartDate = Convert.ToDateTime(tripLeg.DepartureDisplayTime);
                DateTime EndDate = Convert.ToDateTime(tripLeg.ArrivalDisplayTime);
                TimeSpan span = EndDate.Subtract(StartDate);

                string totalhrs = Convert.ToString(span.TotalHours);
                if (StartDate.TimeOfDay.ToString() == "00:00:00.0000001" && EndDate.TimeOfDay.ToString() != "23:59:59.9999999")
                {
                    totalhrs = Convert.ToString(Math.Round(span.TotalHours, 2));
                }
                else
                {
                    totalhrs = Convert.ToString(span.TotalHours);
                }

                if (!string.IsNullOrEmpty(totalhrs) && totalhrs.Contains("."))
                {
                    string[] res = totalhrs.Split('.');
                    if (!string.IsNullOrEmpty(res[1]) && res[1].Length > 1)
                        res[1] = res[1].Substring(1, 1);
                    totalhrs = res[0] + "." + res[1];
                }
                else
                    totalhrs = (totalhrs == "0") ? "" : totalhrs + ".0";

                //appointment.CumulativeETE = span.TotalHours.ToString("n2");// string.Format("{0.0}", span.ToString());
                //appointment.CumulativeETE = (tripLeg.TailNum.ToUpper() != "DUMMY") ? totalhrs : "";
                //appointment.ElapseTM = (tripLeg.TailNum.ToUpper() != "DUMMY") ? totalhrs : "";
            }

        }

        private void SetCrewAppointment(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);
                appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                SetCrewDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                if (appointment.RecordType == "C")// for crew entry -> assign crewduty type code
                {
                    appointment.AircraftDutyCD = appointment.CrewDutyTypeCD;
                }
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns
                }

                DateTime StartDate = Convert.ToDateTime(tripLeg.DepartureDisplayTime);
                DateTime EndDate = Convert.ToDateTime(tripLeg.ArrivalDisplayTime);
                TimeSpan span = EndDate.Subtract(StartDate);

                string totalhrs = Convert.ToString(span.TotalHours);
                if (StartDate.TimeOfDay.ToString() == "00:00:00.0000001" && EndDate.TimeOfDay.ToString() != "23:59:59.9999999")
                {
                    totalhrs = Convert.ToString(Math.Round(span.TotalHours, 2));
                }
                else
                {
                    totalhrs = Convert.ToString(span.TotalHours);
                }

                if (!string.IsNullOrEmpty(totalhrs) && totalhrs.Contains("."))
                {
                    string[] res = totalhrs.Split('.');
                    if (!string.IsNullOrEmpty(res[1]) && res[1].Length > 1)
                        res[1] = res[1].Substring(1, 1);
                    totalhrs = res[0] + "." + res[1];
                }
                else
                    totalhrs = (totalhrs == "0") ? "" : totalhrs + ".0";

                //appointment.CumulativeETE = span.TotalHours.ToString("n2");// string.Format("{0.0}", span.ToString());
                if (!string.IsNullOrEmpty(tripLeg.TailNum))
                {
                    //appointment.CumulativeETE = (tripLeg.TailNum.ToUpper() != "DUMMY") ? totalhrs : "";
                    //appointment.ElapseTM = (tripLeg.TailNum.ToUpper() != "DUMMY") ? totalhrs : "";
                }
                else
                {
                    appointment.CumulativeETE = "";
                    appointment.ElapseTM = "";
                }
            }
        }

        private void SetDepartureAndArrivalInfo(DisplayOptions displayOptions, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {
                if (appointment.ShowAllDetails)
                {
                    switch (displayOptions.Day.DepartArriveInfo)
                    {

                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;


                    }
                }
                else // Show as per company profile settings + display option settings
                {
                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (displayOptions.Day.DepartArriveInfo)
                        {

                            case DepartArriveInfo.ICAO:
                                appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;
                        }
                    }
                    else
                    {
                        appointment.DepartureICAOID = string.Empty;
                        appointment.ArrivalICAOID = string.Empty;
                    }

                    if (!PrivateTrip.IsShowArrivalDepartTime)
                    {
                        appointment.StartDateTime = string.Empty;
                        appointment.EndDateTime = string.Empty;
                    }
                }
            }
        }

        private static void SetCrewDepartureAndArrivalInfo(DisplayOptions displayOptions, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {

                switch (displayOptions.Day.DepartArriveInfo)
                {

                    case DepartArriveInfo.ICAO:
                        appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                        appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                        break;

                    case DepartArriveInfo.AirportName:
                        appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                        appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                        break;

                    case DepartArriveInfo.City:
                        appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                        appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                        break;
                }
            }
        }

        protected void dgDay_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            Entities.SchedulingCalendar.Appointment appt = (Entities.SchedulingCalendar.Appointment)e.Item.DataItem;
                            if (DisplayOptions.Day.BlackWhiteColor)
                            {
                                e.Item.BackColor = Color.White;
                                e.Item.ForeColor = Color.Black;
                            }
                            else
                            {
                                if (!DisplayOptions.Day.AircraftColors)
                                {
                                    if (appt.RecordType == "T")
                                    {
                                        if (appt.IsRONAppointment)
                                        {
                                            if (DayFleetTreeCount > 0) // fleet view
                                            {
                                                // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category

                                                if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(appt.AircraftDutyCD))
                                                {
                                                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == appt.AircraftDutyCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                            else// crew view => CrewTreeCount > 0
                                            {
                                                if (CrewDutyTypes.Count > 0 && !string.IsNullOrEmpty(appt.CrewDutyTypeCD))
                                                {
                                                    var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == appt.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            SetFlightCategoryColor(e, e.Item);
                                        }
                                    }
                                    else if (appt.RecordType == "M")
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                        SetAircraftDutyColor(e, e.Item);
                                    }
                                    else if (appt.RecordType == "C")
                                    {
                                        // set crew duty type color...
                                        SetCrewDutyColorForDayGrid(e, e.Item);
                                    }
                                }
                                else if (DisplayOptions.Day.AircraftColors && appt.TailNum == null && appt.RecordType == "C")
                                {
                                    SetCrewDutyColorForDayGrid(e, e.Item);
                                }
                                else // AircraftColors checked
                                {
                                    if (appt.AircraftForeColor != null && appt.AircraftBackColor != null)
                                    {
                                        e.Item.BackColor = Color.FromName(appt.AircraftBackColor);
                                        e.Item.ForeColor = Color.FromName(appt.AircraftForeColor);
                                    }
                                }
                            }

                            if (!appt.ShowAllDetails) // show as per company profile settings - trip privacy settings
                            {

                                if (PrivateTrip.IsShowCumulativeETE && DisplayOptions.Day.CummulativeETE)
                                {
                                    //values set in aspx
                                }
                                else
                                {
                                    item["ETE"].Text = " ";
                                    item["CummETE"].Text = " ";
                                }

                                if (PrivateTrip.IsShowPaxCount)
                                {
                                    //value set in aspx
                                }
                                else
                                {
                                    item["Pax"].Text = " ";
                                }

                                if (PrivateTrip.IsShowCrew)
                                {
                                    //value set in aspx
                                }
                                else
                                {
                                    item["Crew"].Text = " ";
                                }

                                if (PrivateTrip.IsShowFlightCategory)
                                {
                                    //value set in aspx
                                }
                                else
                                {
                                    item["FltCat"].Text = " ";
                                }

                                if (PrivateTrip.IsShowRequestor)
                                {
                                    //value set in aspx
                                }
                                else
                                {
                                    item["Reqstor"].Text = " ";

                                }

                                if (PrivateTrip.IsShowAuthorization)
                                {
                                    //value set in aspx
                                }
                                else
                                {
                                    item["Authorization"].Text = " ";

                                }


                                item["FuelLoad"].Text = " ";
                                item["OutboundInstruction"].Text = " ";

                                if (PrivateTrip.IsShowFlightNumber)
                                {
                                    //value set in aspx
                                }
                                else
                                {
                                    item["FlightNo"].Text = " ";
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Day);
                }
            }
        }

        protected void SetAircraftDutyColor(GridItemEventArgs e, GridItem item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)item.DataItem;
                var appointmentAircraftDuty = appointmentItem.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                        e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        protected void SetFlightCategoryColor(GridItemEventArgs e, GridItem item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)item.DataItem;
                var appointmentCategory = appointmentItem.FlightCategoryID;

                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void SetCrewDutyColorForDayGrid(GridItemEventArgs e, GridItem gridItem)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, gridItem))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)gridItem.DataItem;
                var appointmentCategory = appointmentItem.CrewDutyType;
                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        // ON click of Apply button...
        protected void btnApply_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;
                            //Changes done for the Bug ID - 5828
                            Session["IsFleetChecked"] = (FleetTreeView.CheckedNodes.Count > 0) ? true : false;
                            loadTree(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Day);
                }
            }
        }

        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCPreviousCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            EndDate = GetEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;

                            lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            EndDate = GetEndDate(StartDate);

                            lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;

                        }                        
                        Session["IsFleetChecked"] = (FleetTreeView.CheckedNodes.Count > 0) ? true : false;
                        loadTree(false);
                        Session["SCPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Day);
                }
            }

        }

        protected void nextButton_Click(object sender, EventArgs e)
        {
            int count = 0;

            if (Convert.ToInt32(Session["SCNextCount"]) == 0)
            {
                // DateTime today = DateTime.Today;
                DateTime selectedDate;
                if (Session["SCSelectedDay"] == null)
                {
                    selectedDate = DateTime.Today;
                }
                else
                {
                    selectedDate = (DateTime)Session["SCSelectedDay"];
                }
                StartDate = selectedDate.AddDays(1);
                EndDate = GetEndDate(StartDate);

                lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                Session["SCSelectedDay"] = StartDate;


            }
            else
            {
                DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                StartDate = sessionnextDay.AddDays(1);
                EndDate = GetEndDate(StartDate);

                lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                tbDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                Session["SCSelectedDay"] = StartDate;
            }
            Session["IsFleetChecked"] = (FleetTreeView.CheckedNodes.Count > 0) ? true : false;
            loadTree(false);
            Session["SCNextCount"] = ++count;
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {

            //RadDatePicker1.SelectedDate = DateTime.Today;
            tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
            StartDate = DateTime.Today;
            EndDate = GetEndDate(StartDate);
            Session["SCSelectedDay"] = StartDate;
            Session["IsFleetChecked"] = (FleetTreeView.CheckedNodes.Count > 0) ? true : false;
            lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            loadTree(false);
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            Session["IsFleetChecked"] = (FleetTreeView.CheckedNodes.Count > 0) ? true : false;
                            lbDay.Text = System.Web.HttpUtility.HtmlEncode(StartDate.DayOfWeek.ToString() + " " + StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            loadTree(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Day);
                }
            }
        }


        protected void RadDayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            var clickedIndex = Request.Form["radGridClickedRowIndex"];
            Session.Remove("CurrentPreFlightTrip");
            if (e.Item.Value == string.Empty)
            {
                return;
            }

            if (string.IsNullOrEmpty(clickedIndex))// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
            {
                if ((e.Item.Text == "Preflight") || (e.Item.Text == "Add New Trip"))
                {
                    /// 12/11/2012
                    ///Changes done for displaying the homebase of login user 
                    PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                    PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                    Trip.EstDepartureDT = StartDate;
                    PreflightService.Company HomebaseSample = new PreflightService.Company();
                    HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                    HomebaseSample.BaseDescription = BaseDescription;
                    Trip.HomeBaseAirportICAOID = HomeBaseCode;
                    Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                    Trip.HomebaseID = HomeBaseId;
                    Trip.Company = HomebaseSample;
                    Session["CurrentPreFlightTrip"] = Trip;
                    Session.Remove("PreflightException");
                    Trip.State = TripEntityState.Added;
                    Trip.Mode = TripActionMode.Edit;

                    if (e.Item.Text == "Add New Trip")
                    {
                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                    }
                    else if (e.Item.Text == "Preflight")
                    {
                        if (Trip.TripNUM != null && Trip.TripNUM != 0)
                        {
                            Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                        }
                        else
                        {
                            Session["CurrentPreFlightTrip"] = null;
                            Session[WebSessionKeys.CurrentPreflightTrip] = null;
                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                        }
                    }
                    else
                    Response.Redirect(e.Item.Value);
                }
                else if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                {// Fleet calendar entry

                    RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                    RadWindow3.Visible = true;
                    RadWindow3.VisibleOnPageLoad = true;
                    return;
                }
                else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                { // Crew calendar entry
                    RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                    RadWindow4.Visible = true;
                    RadWindow4.VisibleOnPageLoad = true;
                    return;
                }
                else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                {
                    var tripnum = string.Empty;
                    Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                }
                else if (e.Item.Text == "UWA Services")
                {
                    return;
                }
                else
                {
                    Response.Redirect(e.Item.Value);
                }
            }
            //get trip by tripId
            var selectedTripId = dgDay.MasterTableView.Items[clickedIndex].GetDataKeyValue("TripId");
            var selectedTailNum = dgDay.MasterTableView.Items[clickedIndex].GetDataKeyValue("TailNum");
            var selectedDate = dgDay.MasterTableView.Items[clickedIndex].GetDataKeyValue("StartDate");
            var selectedLegNum = dgDay.MasterTableView.Items[clickedIndex].GetDataKeyValue("LegNum");
             // On click on calendar entries
            if (e.Item.Text == "Add New Trip")
            {
                PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                Trip.EstDepartureDT = Convert.ToDateTime(selectedDate);
                if (selectedTailNum != null)
                {
                    /// 12/11/2012
                    ///Changes done for displaying the homebase of Fleet in preflight 
                    PreflightService.Company HomebaseSample = new PreflightService.Company();
                    PreflightService.Aircraft AircraftSample = new PreflightService.Aircraft();
                    Int64 HomebaseAirportId = 0;
                    if (fleetsample.FleetID != null)
                    {
                        Int64 fleetHomebaseId = 0;
                        Int64 fleetId = 0;
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var FleetByTailNumber = FPKMasterService.GetFleetByTailNumber(selectedTailNum.ToString().Trim()).EntityInfo;
                            if (FleetByTailNumber != null)
                            {
                                fleetId = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)FleetByTailNumber).FleetID;
                                fleetsample.TailNum = selectedTailNum.ToString();
                                fleetsample.FleetID = fleetId;


                            }
                            var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                            if (FleetList.Count > 0)
                            {
                                GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                if (FleetCatalogEntity.HomebaseID != null)
                                {
                                    fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                    AircraftSample.AircraftDescription = FleetCatalogEntity.AircraftDescription;
                                    AircraftSample.AircraftCD = FleetCatalogEntity.AirCraft_AircraftCD;
                                    AircraftSample.AircraftID = Convert.ToInt64(FleetCatalogEntity.AircraftID);
                                    Trip.HomebaseID = fleetHomebaseId;
                                    Trip.Fleet = fleetsample;
                                    Trip.Aircraft = AircraftSample;
                                }
                            }
                            var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                            if (CompanyList.Count > 0)
                            {
                                GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                if (CompanyCatalogEntity.HomebaseAirportID != null)
                                {
                                    HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                    HomebaseSample.BaseDescription = BaseDescription;
                                    Trip.Company = HomebaseSample;
                                }
                            }
                            var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                            if (AirportList.Count > 0)
                            {
                                GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                if (AirportCatalogEntity.IcaoID != null)
                                {
                                    Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                }
                                if (AirportCatalogEntity.AirportID != null)
                                {
                                    Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                }
                            }
                        }
                    }
                }

                Session["CurrentPreFlightTrip"] = Trip;
                Session.Remove("PreflightException");
                Trip.State = TripEntityState.Added;
                Trip.Mode = TripActionMode.Edit;
                Response.Redirect(e.Item.Value);

                /*if (Trip.TripNUM != null && Trip.TripNUM != 0)
                {
                    Session["CurrentPreFlightTrip"] = Trip;
                    Session.Remove("PreflightException");
                    Trip.State = TripEntityState.Added;
                    Trip.Mode = TripActionMode.Edit;
                    Response.Redirect(e.Item.Value);
                }
                else
                {
                    Session["CurrentPreFlightTrip"] = null;
                    Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                }*/
            }    
            
            //get trip by tripId
            //var selectedTripId = dgDay.MasterTableView.Items[clickedIndex].GetDataKeyValue("TripId");
            //var selectedTailNum = dgDay.MasterTableView.Items[clickedIndex].GetDataKeyValue("TailNum");

            Int64 tripId = Convert.ToInt64(selectedTripId);
            if (tripId != 0)
            {

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var tripInfo = preflightServiceClient.GetTrip(tripId);
                    if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                    {
                        var trip = tripInfo.EntityList[0];

                        //Set the trip.Mode and Trip.State to NoChage for read only                    

                        trip.Mode = TripActionMode.Edit;
                        Session["CurrentPreFlightTrip"] = trip;
                        Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                        PreflightTripManager.populateCurrentPreflightTrip(trip);
                        Session.Remove("PreflightException");

                        if (e.Item.Text == "Outbound Instruction") // outbound instruction menu selected...
                        {
                            trip.Mode = TripActionMode.Edit;
                            trip.State= TripEntityState.Modified;
                            RadWindow5.NavigateUrl = e.Item.Value + "?IsCalender=1";
                            RadWindow5.Visible = true;
                            RadWindow5.VisibleOnPageLoad = true;
                            RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                            return;
                        }
                        else if (e.Item.Text == "Leg Notes") // Leg Notes menu selected...
                        {
                            trip.Mode = TripActionMode.Edit;
                            trip.State = TripEntityState.Modified;
                            RadWindow6.NavigateUrl = e.Item.Value + "?IsCalender=1&Leg=" + selectedLegNum;
                            RadWindow6.Visible = true;
                            RadWindow6.VisibleOnPageLoad = true;
                            return;
                        }
                        else if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                        {// Fleet calendar entry
                            string tailNumber = selectedTailNum != null ? selectedTailNum.ToString() : null;
                            RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + tailNumber + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                            RadWindow3.Visible = true;
                            RadWindow3.VisibleOnPageLoad = true;
                            return;
                        }
                        else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                        { // Crew calendar entry
                            RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                            RadWindow4.Visible = true;
                            RadWindow4.VisibleOnPageLoad = true;
                            return;
                        }
                        else if (e.Item.Text == "Preflight")
                        {                            
                            if (trip.TripNUM != null && trip.TripNUM != 0)
                            {
                                Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                            }
                            else
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                            }
                        }
                        else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                        {
                            RedirectToPage(string.Format("{0}?xmlFilename=PRETSOverview.xml&tripnum={1}&tripid={2}", WebConstants.URL.TripSheetReportViewer, Microsoft.Security.Application.Encoder.HtmlEncode(trip.TripNUM.ToString()), Microsoft.Security.Application.Encoder.HtmlEncode(trip.TripID.ToString())));
                        }
                        else
                        {
                            Response.Redirect(e.Item.Value);
                        }
                    }
                }
            }
            else //tripid=0, that is dummy appointment
            {
                if (e.Item.Text == "Add Fleet Calendar Entry" || e.Item.Text == "Fleet Calendar Entry")
                {// Fleet calendar entry
                    string tailNumber = selectedTailNum != null ? selectedTailNum.ToString() : null;
                    RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + tailNumber + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                    RadWindow3.Visible = true;
                    RadWindow3.VisibleOnPageLoad = true;
                    return;
                }
                else if (e.Item.Text == "Add Crew Calendar Entry" || e.Item.Text == "Crew Calendar Entry")
                { // Crew calendar entry
                    RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + StartDate + "&endDate=" + StartDate;// no need to pass crewCode since it has no resource
                    RadWindow4.Visible = true;
                    RadWindow4.VisibleOnPageLoad = true;
                    return;
                }
                else if (e.Item.Text == "Preflight")
                {
                    Session["CurrentPreFlightTrip"] = null;
                    Session[WebSessionKeys.CurrentPreflightTrip] = null;
                    Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                }
                else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                {
                    var tripnum = string.Empty;
                    Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                }
                else
                {
                    Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                }
            }
        }

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (var commonServiceClient = new CommonServiceClient())
            {
                if (ChkDefaultView.Checked)
                {
                    commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.Day);
                }
                else
                {
                    commonServiceClient.UpdateCalendarDefaultView(null);
                }
            }
            loadTree(false);
        }
    }
}