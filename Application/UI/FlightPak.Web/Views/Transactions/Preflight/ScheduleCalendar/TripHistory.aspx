﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TripHistory.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.TripHistory" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
        <table width="100%" class="box1">
            <tr>
                <td>
                                        
                    <table id="TableLogisticHistory" width="450px" runat="server">
                        <tr>
                            <td align="left" class="mnd_text" valign="top">
                                Last Changes
                            </td>
                        </tr>
                    </table>
                    <table width="100%" class="border-box">
                        <tr>
                            <td class="note-box" valign="top">
                                <div class="history_div" runat="server" id="divHistory">
                                </div>
                                <asp:HiddenField runat="server" ID="hdnTripID" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
