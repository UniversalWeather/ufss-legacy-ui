﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.PreflightService;
using FlightPak.Web.CalculationService;
using Telerik.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using FlightPak.Common;
using FlightPak.Common.Constants;
using System.Globalization;
using Omu.ValueInjecter;


namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class LegNotesPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public PreflightMain Trip = new PreflightMain();

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgLegSummary, dgLegSummary, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgLegSummary.ClientID));

                        if (!IsPostBack)
                        {
                            hdnLeg.Value = "1";
                            hdnLegNUM.Value = "1";
                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                Session["TmpLegsNotes"] = CopyLegsFromSession();
                                hdnLeg.Value = "1";
                                hdnLegNUM.Value = "1";

                                if (Request.QueryString["Leg"] != null && String.IsNullOrEmpty(Request.QueryString["Leg"]) == false)
                                {
                                    hdnLeg.Value = Request.QueryString["Leg"].ToString() == "0" ? "1" : Request.QueryString["Leg"].ToString();
                                    hdnLegNUM.Value = Request.QueryString["Leg"].ToString() == "0" ? "1" : Request.QueryString["Leg"].ToString();
                                }

                                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                                {
                                    LoadLegDetails();
                                    dgLegSummary.SelectedIndexes.Add(Convert.ToInt32(hdnLegNUM.Value) - 1);
                                }
                            }
                            EnableForm(false);
                            dgLegSummary.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void dgLegSummary_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                            List<PreflightLeg> Dt = new List<PreflightLeg>();
                            if (Trip.PreflightLegs != null)
                            {
                                Dt = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                if (Dt.Count > 0)
                                {
                                    dgLegSummary.DataSource = (from u in Dt
                                                               where u.Airport != null && u.Airport1 != null
                                                               select
                                                                new
                                                                {
                                                                    LegID = u.LegID,
                                                                    LegNum = u.LegNUM,
                                                                    DepartAir = u.Airport1.IcaoID == null ? string.Empty : u.Airport1.IcaoID,
                                                                    DeparttDate = u.DepartureDTTMLocal == null ? DateTime.Now : u.DepartureDTTMLocal,
                                                                    ArrivalAir = u.Airport.IcaoID == null ? string.Empty : u.Airport.IcaoID,
                                                                    ArrivalDate = u.ArrivalDTTMLocal == null ? DateTime.Now : u.ArrivalDTTMLocal
                                                                });
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void dgLegSummary_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;
                                if (dgLegSummary.SelectedItems.Count > 0)
                                {
                                    GridDataItem Item = (GridDataItem)dgLegSummary.SelectedItems[0];
                                    hdnLegID.Value = Item.GetDataKeyValue("LegID").ToString().Trim();
                                    hdnLegNUM.Value = Item.GetDataKeyValue("LegNUM").ToString().Trim();
                                    LoadLegDetails();
                                    Item.Selected = true;
                                }
                                EnableForm(true);
                                //GridEnable(false, true, false);                                
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void dgLegSummary_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegSummary.Enabled == true && dgLegSummary.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgLegSummary.SelectedItems[0];
                            hdnLegID.Value = Item.GetDataKeyValue("LegID").ToString().Trim();
                            hdnLegNUM.Value = Item.GetDataKeyValue("LegNUM").ToString().Trim();
                            LoadLegDetails();
                        }
                        //EnableForm(false);
                        //GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton editCtl;
                editCtl = (LinkButton)dgLegSummary.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");

                // RadAjaxManager.GetCurrent(Page).FocusControl(editCtl);

                if (edit)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["IsCalender"]) &&
                        Convert.ToString(Request.QueryString["IsCalender"]) == "1")
                        editCtl.Visible = true;
                    editCtl.Enabled = true;
                    editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                }
                else
                {
                    editCtl.Enabled = false;
                    editCtl.OnClientClick = string.Empty;
                }
            }
        }

        protected void ClearFormFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCrewNotes.Text = string.Empty;
                tbPaxNotes.Text = string.Empty;
            }
        }

        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbCrewNotes.Enabled = enable;
                tbPaxNotes.Enabled = enable;
                btnCancel.Visible = enable;
                btnSave.Visible = enable;
                btnCrewNotesAllLegs.Visible = enable;
                btnPaxNotesAllLegs.Visible = enable;
            }
        }

        protected void Cancel_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Session["TmpLegsNotes"] = CopyLegsFromSession();
                            ClearFormFields();
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                            {
                                LoadLegDetails();
                            }
                            EnableForm(false);
                            //if (!String.IsNullOrEmpty(Request.QueryString["IsCalender"]) && Convert.ToString(Request.QueryString["IsCalender"]) == "1")
                            //{
                            //    dgLegSummary.Enabled = true;
                            //    LinkButton editCtl;
                            //    editCtl = (LinkButton)dgLegSummary.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                            //    editCtl.Visible = true;

                            //}
                            dgLegSummary.Enabled = true;
                            LinkButton editCtl;
                            editCtl = (LinkButton)dgLegSummary.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                            editCtl.Visible = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Save_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            if (Request.QueryString["IsCalender"] != null)
                            {
                                if (Convert.ToString(Request.QueryString["IsCalender"]) == "1")
                                    btnSaveTrip.Visible = true;
                            }
                            
                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (Session["TmpLegsNotes"] != null)
                                {
                                    var preflightLegs = (List<PreflightLeg>)Session["TmpLegsNotes"];
                                    if (preflightLegs != null && preflightLegs.Count > 0)
                                    {
                                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                                        {
                                            foreach (PreflightLeg Leg in Trip.PreflightLegs)
                                            {
                                                foreach (PreflightLeg pfLeg in preflightLegs)
                                                {
                                                    if (Leg.LegNUM == pfLeg.LegNUM)
                                                    {
                                                        Leg.State = pfLeg.State;
                                                        Leg.CrewNotes = pfLeg.CrewNotes;
                                                        Leg.Notes = pfLeg.Notes;

                                                    }
                                                }
                                            }
                                            Session["CurrentPreFlightTrip"] = Trip;
                                            LoadLegDetails();
                                        }
                                    }
                                }
                            }
                            EnableForm(false);
                            GridEnable(true, true, true);
                            if (Request.QueryString["IsCalender"] != null &&
                                Convert.ToString(Request.QueryString["IsCalender"]) == "1")
                                btnSavelTrip_click(null, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void LoadLegDetails()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                var preflightLegs = (List<PreflightLeg>)Session["TmpLegsNotes"];
                if (preflightLegs != null && preflightLegs.Count > 0)
                {
                    foreach (PreflightLeg Leg in preflightLegs)
                    {
                        if (Leg.LegNUM.ToString() == hdnLegNUM.Value)
                        {
                            lblLeg.Text = System.Web.HttpUtility.HtmlEncode("Leg" + Leg.LegNUM.ToString() + ":");
                            if (Leg.Airport1 != null)
                            {
                                lblDepartICAO.Text = System.Web.HttpUtility.HtmlEncode(Leg.Airport1.IcaoID.ToString());
                            }
                            if (Leg.Airport != null)
                            {
                                lblArrivalICAO.Text = System.Web.HttpUtility.HtmlEncode(Leg.Airport.IcaoID.ToString());
                            }

                            tbCrewNotes.Text = Leg.CrewNotes;
                            tbPaxNotes.Text = Leg.Notes;
                            hdnLegNUM.Value = Leg.LegNUM.ToString();
                        }
                    }
                }
            }
        }

        protected void btnCancelTrip_click(object sender, EventArgs e)
        {

        }

        protected void btnSavelTrip_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())//PreflightTrip))
            {
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {

                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                    //var returnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Trip.TripID);

                    // Lock the record

                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Trip.TripID);
                    }

                    if (Trip.TripID > 0)
                        Trip.State = TripEntityState.Modified;


                    //check whether you want authorization
                    CheckAutorization(Permission.Preflight.AddTripManager);

                    if ((bool)UserPrincipal.Identity._fpSettings._IsAutoRevisionNum)
                    {

                        if (Trip.State == TripEntityState.Added)
                        {
                            Trip.RevisionNUM = 1;

                        }
                        else
                            Trip.RevisionNUM = (Trip.RevisionNUM == null ? 0 : Trip.RevisionNUM) + 1;
                    }


                    if (UserPrincipal != null)
                        Trip.LastUpdUID = UserPrincipal.Identity._name;
                    Trip.LastUpdTS = DateTime.UtcNow;

                    //For other than Copy trip we make IsTripCopied to false
                    if (Trip.State == TripEntityState.Added)
                    {
                        Trip.IsTripCopied = false;
                    }
                    if (Trip.Account != null) Trip.Account = null;
                    if (Trip.Aircraft != null) Trip.Aircraft = null;
                    if (Trip.Client != null) Trip.Client = null;
                    if (Trip.Crew != null) Trip.Crew = null;
                    if (Trip.Company != null) Trip.Company = null;
                    if (Trip.Department != null) Trip.Department = null;
                    if (Trip.DepartmentAuthorization != null) Trip.DepartmentAuthorization = null;
                    if (Trip.EmergencyContact != null) Trip.EmergencyContact = null;
                    if (Trip.Fleet != null) Trip.Fleet = null;
                    if (Trip.Passenger != null) Trip.Passenger = null;
                    if (Trip.UserMaster != null) Trip.UserMaster = null;
                    if (Trip.UserMaster1 != null) Trip.UserMaster1 = null;
                    if (Trip.RecordType == null) Trip.RecordType = "T";
                    if (Trip.LastUpdTS == null) Trip.LastUpdTS = DateTime.UtcNow;
                    if (Trip.CQCustomer != null) Trip.CQCustomer = null;

                    #region Legs

                    if (Trip.PreflightLegs != null)
                    {


                        foreach (PreflightLeg Leg in Trip.PreflightLegs)
                        {

                            if (Leg.Account != null) Leg.Account = null;
                            if (Leg.Airport != null) Leg.Airport = null;
                            if (Leg.Airport1 != null) Leg.Airport1 = null;
                            if (Leg.Client != null) Leg.Client = null;
                            if (Leg.FlightCatagory != null) Leg.FlightCatagory = null;
                            if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                            if (Leg.Department != null) Leg.Department = null;
                            if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                            if (Leg.DepartmentAuthorization != null) Leg.DepartmentAuthorization = null;
                            if (Leg.FBO != null) Leg.FBO = null;
                            if (Leg.Passenger != null) Leg.Passenger = null;
                            if (Leg.UserMaster != null) Leg.UserMaster = null;
                            if (Leg.CQCustomer != null) Leg.CQCustomer = null;

                            if ((bool)Leg.IsDeleted)
                            {
                                if (Leg.PreflightCrewLists != null) Leg.PreflightCrewLists.Clear();
                                if (Leg.PreflightCateringDetails != null) Leg.PreflightCateringDetails.Clear();
                                if (Leg.PreflightCateringLists != null) Leg.PreflightCateringLists.Clear();
                                if (Leg.PreflightCheckLists != null) Leg.PreflightCheckLists.Clear();
                                if (Leg.PreflightFBOLists != null) Leg.PreflightFBOLists.Clear();
                                if (Leg.PreflightPassengerLists != null) Leg.PreflightPassengerLists.Clear();
                                if (Leg.PreflightTransportLists != null) Leg.PreflightTransportLists.Clear();
                                if (Leg.PreflightHotelLists != null) Leg.PreflightHotelLists.Clear();
                                if (Leg.PreflightTripOutbounds != null) Leg.PreflightTripOutbounds.Clear();
                                if (Leg.PreflightTripSIFLs != null) Leg.PreflightTripSIFLs.Clear();
                                if (Leg.UWALs != null) Leg.UWALs.Clear();
                                if (Leg.UWALTsPers != null) Leg.UWALTsPers.Clear();
                                if (Leg.UWAPermits != null) Leg.UWAPermits.Clear();
                                if (Leg.UWASpServs != null) Leg.UWASpServs.Clear();
                                if (Leg.UWATSSLegs != null) Leg.UWATSSLegs.Clear();

                            }
                            else
                            {
                                if (Leg.PreflightCrewLists != null)
                                {
                                    foreach (PreflightCrewList Crew in Leg.PreflightCrewLists)
                                    {
                                        //Crew.LegID = Leg.LegID;
                                        //Crew.PreflightLeg = Leg;
                                        if (Crew.PreflightLeg != null) Crew.PreflightLeg = null;
                                        if (Crew.Crew != null) Crew.Crew = null;
                                        if (Crew.UserMaster != null) Crew.UserMaster = null;
                                        #region Hotelcode for Preflight Crew needs to be removed
                                        if (Crew.PreflightCrewHotelLists != null)
                                        {
                                            foreach (PreflightCrewHotelList Hotel in Crew.PreflightCrewHotelLists)
                                            {
                                                if (Hotel.Airport != null) Hotel.Airport = null;
                                                if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                                if (Hotel.Hotel != null) Hotel.Hotel = null;
                                            }
                                        }
                                        #endregion
                                    }
                                }

                                #region New Code
                                if (Leg.PreflightHotelLists != null)
                                {

                                    foreach (PreflightHotelList Hotel in Leg.PreflightHotelLists)
                                    {
                                        if (Hotel.Airport != null) Hotel.Airport = null;
                                        if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                        if (Hotel.Hotel != null) Hotel.Hotel = null;
                                    }

                                }
                                #endregion

                                if (Leg.PreflightPassengerLists != null)
                                {
                                    foreach (PreflightPassengerList PAX in Leg.PreflightPassengerLists)
                                    {

                                        if (PAX.PreflightLeg != null) PAX.PreflightLeg = null;
                                        if (PAX.Passenger != null) PAX.Passenger = null;
                                        if (PAX.UserMaster != null) PAX.UserMaster = null;

                                        #region Hotelcode for Preflight PAX needs to be removed when PAX re design is implemented
                                        if (PAX.PreflightPassengerHotelLists != null)
                                        {
                                            foreach (PreflightPassengerHotelList Hotel in PAX.PreflightPassengerHotelLists)
                                            {
                                                if (Hotel.Airport != null) Hotel.Airport = null;
                                                if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                                if (Hotel.Hotel != null) Hotel.Hotel = null;
                                            }
                                        }
                                        #endregion
                                    }
                                }

                                if (Leg.PreflightTransportLists != null)
                                {
                                    foreach (PreflightTransportList Transport in Leg.PreflightTransportLists)
                                    {

                                        if (Transport.Airport != null) Transport.Airport = null;
                                        if (Transport.UserMaster != null) Transport.UserMaster = null;
                                        if (Transport.Transport != null) Transport.Transport = null;

                                    }
                                }

                                if (Leg.PreflightFBOLists != null)
                                {
                                    foreach (PreflightFBOList FBO in Leg.PreflightFBOLists)
                                    {
                                        if (FBO.Airport != null) FBO.Airport = null;
                                        if (FBO.UserMaster != null) FBO.UserMaster = null;
                                        if (FBO.PreflightLeg != null) FBO.PreflightLeg = null;
                                        if (FBO.FBO != null) FBO.FBO = null;
                                    }
                                }

                                if (Leg.PreflightCateringDetails != null)
                                {
                                    foreach (PreflightCateringDetail Catering in Leg.PreflightCateringDetails)
                                    {
                                        if (Catering.Airport != null) Catering.Airport = null;
                                        if (Catering.UserMaster != null) Catering.UserMaster = null;
                                        if (Catering.PreflightLeg != null) Catering.PreflightLeg = null;
                                        if (Catering.Catering != null) Catering.Catering = null;
                                    }
                                }


                                if (Leg.PreflightTripOutbounds != null)
                                {
                                    foreach (PreflightTripOutbound outboundInstruction in Leg.PreflightTripOutbounds)
                                    {
                                        if (outboundInstruction.UserMaster != null) outboundInstruction.UserMaster = null;
                                        if (outboundInstruction.PreflightLeg != null) outboundInstruction.PreflightLeg = null;

                                    }
                                }

                                if (Leg.PreflightCheckLists != null)
                                {
                                    foreach (PreflightCheckList checklist in Leg.PreflightCheckLists)
                                    {
                                        if (checklist.UserMaster != null) checklist.UserMaster = null;
                                        if (checklist.PreflightLeg != null) checklist.PreflightLeg = null;
                                        if (checklist.TripManagerCheckList != null) checklist.TripManagerCheckList = null;
                                    }
                                }

                            }
                        }
                    }
                    #endregion


                    var ReturnValue = Service.Add(Trip);
                    if (ReturnValue.ReturnFlag == true)
                    {
                        //If successfull do what ever you want here                         
                        RadWindowManager1.RadAlert("Trip has been saved successfully", 330, 100, "Trip Alert", null);
                        Trip.Mode = TripActionMode.NoChange;
                        Trip.State = TripEntityState.NoChange;
                        //dgLegSummary.Enabled = false;
                        //Session["CurrentPreFlightTrip"] = null;
                        //Session.Remove("PreflightException");
                        //LinkButton editCtl;
                        //editCtl = (LinkButton)dgLegSummary.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                        //editCtl.Visible = false;
                        if (!String.IsNullOrEmpty(Request.QueryString["IsCalender"]) && Convert.ToString(Request.QueryString["IsCalender"]) == "1")
                        {
                            dgLegSummary.Enabled = true;
                            LinkButton editCtl;
                            editCtl = (LinkButton)dgLegSummary.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                            editCtl.Visible = true;
                            EnableForm(false);

                        }
                        else
                        {
                            Session["CurrentPreFlightTrip"] = null;
                            Session.Remove("PreflightException");
                        }
                        btnSaveTrip.Visible = false;
                        //RadScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "CloseWindow()", true);
                    }
                    else
                    {
                        //if fail do what ever you want here
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"jAlert('" + ReturnValue.ErrorMessage + " ','" + ModuleNameConstants.Preflight.TripManager + "');");
                    }
                }
            }
        }

        protected void dgLegSummary_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (Trip != null && Trip.Mode != TripActionMode.Edit)
                                {
                                    LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                                    editButton.Visible = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void lnkInitEdit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton editCtl;
                        editCtl = (LinkButton)dgLegSummary.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                        editCtl.Visible = false;
                        btnSave.Visible = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void tbCrewNotes_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["TmpLegsNotes"] != null)
                        {
                            var preflightLegs = (List<PreflightLeg>)Session["TmpLegsNotes"];
                            if (preflightLegs != null && preflightLegs.Count > 0)
                            {

                                foreach (PreflightLeg Leg in preflightLegs)
                                {
                                    if (Leg.LegNUM.ToString() == hdnLegNUM.Value)
                                    {
                                        if (Leg.LegID != 0)
                                            Leg.State = TripEntityState.Modified;
                                        else
                                            Leg.State = TripEntityState.Added;

                                        Leg.CrewNotes = tbCrewNotes.Text;
                                    }
                                }
                                Session["TmpLegsNotes"] = preflightLegs;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void tbPaxNotes_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["TmpLegsNotes"] != null)
                        {
                            var preflightLegs = (List<PreflightLeg>)Session["TmpLegsNotes"];
                            if (preflightLegs != null && preflightLegs.Count > 0)
                            {

                                foreach (PreflightLeg Leg in preflightLegs)
                                {
                                    if (Leg.LegNUM.ToString() == hdnLegNUM.Value)
                                    {
                                        if (Leg.LegID != 0)
                                            Leg.State = TripEntityState.Modified;
                                        else
                                            Leg.State = TripEntityState.Added;

                                        Leg.Notes = tbPaxNotes.Text;
                                    }
                                }
                                Session["TmpLegsNotes"] = preflightLegs;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        private List<PreflightLeg> CopyLegsFromSession()
        {
            List<PreflightLeg> pfLegList = new List<PreflightLeg>();
            try
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                        {
                            foreach (PreflightLeg Leg in Trip.PreflightLegs)
                            {
                                PreflightLeg pfLeg = new PreflightLeg();
                                pfLeg.InjectFrom(Leg);
                                pfLegList.Add(pfLeg);
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            catch (Exception ex)
            {
                //The exception will be handled, logged and replaced by our custom exception. 
                ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.Preflight.PreflightMain);
            }
            return pfLegList;
        }

        protected void CopyCrewNotesToAllLegs_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["TmpLegsNotes"] != null)
                        {
                            var preflightLegs = (List<PreflightLeg>)Session["TmpLegsNotes"];
                            if (preflightLegs != null && preflightLegs.Count > 0)
                            {

                                foreach (PreflightLeg Leg in preflightLegs)
                                {
                                    if (Leg.LegID != 0)
                                        Leg.State = TripEntityState.Modified;
                                    else
                                        Leg.State = TripEntityState.Added;

                                    Leg.CrewNotes = tbCrewNotes.Text;
                                }
                                Session["TmpLegsNotes"] = preflightLegs;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void CopyPaxNotesToAllLegs_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["TmpLegsNotes"] != null)
                        {
                            var preflightLegs = (List<PreflightLeg>)Session["TmpLegsNotes"];
                            if (preflightLegs != null && preflightLegs.Count > 0)
                            {
                                foreach (PreflightLeg Leg in preflightLegs)
                                {
                                    if (Leg.LegID != 0)
                                        Leg.State = TripEntityState.Modified;
                                    else
                                        Leg.State = TripEntityState.Added;

                                    Leg.Notes = tbPaxNotes.Text;     
                                }
                                Session["TmpLegsNotes"] = preflightLegs;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
    }
}