﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Drawing;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Text;
using FlightPak.Web.UserControls;
using System.Data.SqlTypes;
using FlightPak.Web.Entities.SchedulingCalendar;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.ViewModels;
namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class Planner1 : ScheduleCalendarBase
    {

        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty;
        protected CommonService.SCPlannerWrapper _scPlannerWrapper;

        protected CommonService.SCPlannerWrapper scPlannerWrapper
        {
            get
            {
                return _scPlannerWrapper;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;
                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        
                        

                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.Planner)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }

                            var selectedDay = DateTime.Today;
                            //To reduce service call - Vishwa
                            FilterCriteria.userSettingAvailable = base.scWrapper.userSettingAvailable;
                            FilterCriteria.retriveSystemDefaultSettings = base.scWrapper.retrieveSystemDefaultSettings;
                            var sessionSettings = (AdvancedFilterSettings)Session["SCUserSettings"];
                            if (sessionSettings != null)
                            {
                                var displaySession = sessionSettings.Display;
                                hdnEnableHoverDisplay.Value = displaySession.Planner.DisplayDetailsOnHover.ToString();
                            }
                            else
                            {
                                var userSettings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(FilterCriteria.retriveSystemDefaultSettings);
                                var displaySettings = userSettings.Display;
                                hdnEnableHoverDisplay.Value = displaySettings.Planner.DisplayDetailsOnHover.ToString();
                            }
                            //End
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }

                            StartDate = selectedDay;
                            EndDate = GetEndDate(StartDate);
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);                            

                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCAdvancedTab"] = CurrentDisplayOption.Planner;
                            
                            if (Session["SCPlannerSlot"] != null)
                            {
                                ddlDisplayDays.SelectedValue = Convert.ToString(Session["SCPlannerSlot"]);
                                RadPlannerCrewScheduler.TimelineView.SlotDuration = TimeSpan.Parse(ddlDisplayDays.SelectedValue);
                                RadPlannerFleetScheduler.TimelineView.SlotDuration = TimeSpan.Parse(ddlDisplayDays.SelectedValue);
                                if (ddlDisplayDays.SelectedValue == "1.00:00:00")
                                {
                                    RadPlannerFleetScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd";
                                    RadPlannerCrewScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd";
                                }
                                else
                                {
                                    RadPlannerFleetScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd HH:mm";
                                    RadPlannerCrewScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd HH:mm";
                                }                                
                            }
                            loadTree(false);

                            Session.Remove("SCPlannerNextCount");
                            Session.Remove("SCPlannePreviousCount");
                            Session.Remove("SCPlanneLastCount");
                            Session.Remove("SCPlanneFirstCount");

                        }
                        else
                        {
                            CheckValidDate();

                        }
                        //-- Start: Tooltip Integration  --// 
                        
                        RadPlannerFleetScheduler.AppointmentCreated += RadPlannerFleetScheduler_AppointmentCreated;
                        RadPlannerFleetScheduler.DataBound += RadPlannerFleetScheduler_DataBound;
                        RadToolTipManagerFleetPlanner.OnClientRequestStart = "OnClientRequestStartFleetPlanner";


                        RadPlannerCrewScheduler.AppointmentCreated += RadPlannerCrewScheduler_AppointmentCreated;
                        RadPlannerCrewScheduler.DataBound += RadPlannerCrewScheduler_DataBound;
                        RadToolTipManagerCrewPlanner.OnClientRequestStart = "OnClientRequestStartCrewPlanner";
                        Session["Planner_NonReverseColor"] = DisplayOptions.Planner.NonReversedColors;
                        //Session["CurrentPreFlightTrip"] = null;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }
        /// <summary>
        /// To Validate Date 
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {

            StartDate = (DateTime)Session["SCSelectedDay"];
            hdnIsTodaysDate.Value = StartDate.ToString();
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            EndDate = GetEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            hdnIsTodaysDate.Value = StartDate.ToString();
            if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)//if no tree node is selected...
            {
                return true;
            }
            else
            {
                // avoid duplicate calls for loadTree();
                string id = Page.Request.Params["__EVENTTARGET"];
                if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
                {
                    if (Session[WebSessionKeys.CurrentPreflightTrip] != null) Session[WebSessionKeys.CurrentPreflightTrip] = null;
                    if (Session[WebSessionKeys.CurrentPreflightTripMain] != null) Session[WebSessionKeys.CurrentPreflightTripMain] = null;
                    loadTree(false);
                }
            }
            return IsDateCheck;
        }
        protected void BindQuickFleet()
        {
            //To check add access and prevent user...
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!IsAuthorized(Permission.Preflight.AddFleetCalendarEntry))
                {
                    RadPlannerFleetScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Fleet Calendar Entry").Enabled = false;
                }
                else
                {
                    RadMenuItem FleetMenuItem = RadPlannerFleetScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Fleet Calendar Entry");

                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = base.scWrapper.aircraftDuty.EntityList.Where(x => x.IsCalendarEntry == true).ToList();

                        if (objRetVal.Count() <= 0)
                        {
                            FleetMenuItem.Enabled = false;
                        }
                        else
                        {
                            FleetMenuItem.Items.Clear();
                            for (int i = 0; i <= objRetVal.Count() - 1; i++)
                            {
                                RadMenuItem fleetduty = new RadMenuItem();
                                fleetduty.Text = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[i]).AircraftDutyDescription.ToString();
                                fleetduty.Value = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[i]).AircraftDutyCD.ToString();
                                FleetMenuItem.Items.Add(fleetduty);
                            }
                            FleetMenuItem.Enabled = true;
                        }
                    }
                }
            }
        }

        protected void BindQuickCrew()
        {
            //To check add access and prevent user...
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!IsAuthorized(Permission.Preflight.AddCrewCalendarEntry))
                {
                    RadPlannerCrewScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Crew Calendar Entry").Enabled = false;
                }
                else
                {
                    RadMenuItem CrewMenuItem = RadPlannerCrewScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Crew Calendar Entry");

                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetValCrew = base.scWrapper.crewDutyType.EntityList.Where(x => x.CalendarEntry == true).ToList();

                        if (objRetValCrew.Count() <= 0)
                        {
                            CrewMenuItem.Enabled = false;
                        }
                        else
                        {
                            CrewMenuItem.Items.Clear();
                            for (int i = 0; i <= objRetValCrew.Count() - 1; i++)
                            {
                                RadMenuItem fleetduty = new RadMenuItem();
                                fleetduty.Text = ((FlightPak.Web.CommonService.CrewDutyType)objRetValCrew[i]).DutyTypesDescription.ToString();
                                fleetduty.Value = ((FlightPak.Web.CommonService.CrewDutyType)objRetValCrew[i]).DutyTypeCD.ToString();
                                CrewMenuItem.Items.Add(fleetduty);
                            }
                            CrewMenuItem.Enabled = true;
                        }
                    }
                }
            }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {

                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;
                            loadTree(true);

                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                                var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                                if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                                {
                                    IList<RadTreeNode> nodeCollection = FleetTreeView.CheckedNodes;
                                    if (nodeCollection.Count == 0)
                                    {
                                        FleetTreeView.Nodes.Clear();
                                        BindFleetTree(inputFromFleetTree);
                                    }


                                    Collection<string> InputFromFleetTree = new Collection<string>();
                                    foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                                    {
                                        InputFromFleetTree.Add(s.ChildNodeId);
                                    }

                                    BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromFleetTree);
                                    RadPlannerFleetScheduler.SelectedDate = StartDate;
                                }
                                else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                                {
                                    IList<RadTreeNode> nodeCollection = CrewTreeView.CheckedNodes;
                                    if (nodeCollection.Count == 0)
                                    {
                                        CrewTreeView.Nodes.Clear();
                                        BindCrewTree(inputFromCrewTree);
                                    }
                                    //var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                                    Collection<string> InputFromCrewTree = new Collection<string>();
                                    foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                                    {
                                        InputFromCrewTree.Add(s.ChildNodeId);
                                    }
                                    BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                                    RadPlannerCrewScheduler.SelectedDate = StartDate;
                                }
                                else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                                {
                                    IList<RadTreeNode> nodeCollection = FleetTreeView.CheckedNodes;
                                    if (nodeCollection.Count == 0)
                                    {
                                        FleetTreeView.Nodes.Clear();
                                        BindFleetTree(inputFromFleetTree);
                                    }
                                    //var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                                    Collection<string> InputFromFleetTree = new Collection<string>();
                                    foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                                    {
                                        InputFromFleetTree.Add(s.ChildNodeId);
                                    }
                                    BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromFleetTree);
                                    RadPlannerFleetScheduler.SelectedDate = StartDate;

                                    IList<RadTreeNode> CrewnodeCollection = CrewTreeView.CheckedNodes;
                                    if (CrewnodeCollection.Count == 0)
                                    {
                                        CrewTreeView.Nodes.Clear();
                                        BindCrewTree(inputFromCrewTree);
                                    }

                                    Collection<string> InputFromCrewTree = new Collection<string>();
                                    foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                                    {
                                        InputFromCrewTree.Add(s.ChildNodeId);
                                    }

                                    BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                                    RadPlannerCrewScheduler.SelectedDate = StartDate;
                                }
                            }

                            if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                            {
                                RadPanelBar2.FindItemByText("Fleet").Visible = true;
                                RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                                RadPanelBar2.FindItemByText("Crew").Visible = true;
                                RadPanelBar2.FindItemByText("Crew").Expanded = true;
                                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                                RadPlannerFleetScheduler.Visible = true;
                                RadPlannerCrewScheduler.Visible = true;
                                RadPlannerCrewScheduler.Height = 235;
                            }
                            else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                            {
                                RadPanelBar2.FindItemByText("Fleet").Visible = true;
                                RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                                RadPanelBar2.FindItemByText("Crew").Visible = false;
                                RadPanelBar2.FindItemByText("Crew").Expanded = false;
                                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                                RadPlannerFleetScheduler.Visible = true;
                                RadPlannerCrewScheduler.Visible = false;
                            }
                            else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                            {
                                RadPanelBar2.FindItemByText("Fleet").Visible = false;
                                RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                                RadPanelBar2.FindItemByText("Crew").Visible = true;
                                RadPanelBar2.FindItemByText("Crew").Expanded = true;
                                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                                RadPlannerFleetScheduler.Visible = false;
                                RadPlannerCrewScheduler.Visible = true;
                                RadPlannerCrewScheduler.Height = 470;                            
                            }

                            this.RadPanelBar2.Items[0].Expanded = false;
                            this.RadPanelBar2.Items[1].Expanded = false;
                            this.RadPanelBar2.Items[2].Expanded = true;

                            RightPane.Collapsed = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {                
                if (!Page.IsPostBack)
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Planner);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                    Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                    }

                    /*if (!string.IsNullOrEmpty(FleetCrew[1]))
                    {
                        string[] Crewids = FleetCrew[1].Split(',');
                        foreach (string cIDs in Crewids)
                        {
                            Crewlst.Add(cIDs);
                        }
                    }*/

                    if (!string.IsNullOrEmpty(FleetCrew[2]))
                    {
                        string[] CrewGroupids = FleetCrew[2].Split(',');
                        foreach (string cgrpIDs in CrewGroupids)
                        {
                            string[] ParentChild = cgrpIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Crewlst.Add(new TreeviewParentChildNodePair("Crew", cgrpIDs));
                        }
                    }

                    //To get the distinct values
                    List<string> distinctFleet = Fleetlst.Select(node => node.ChildNodeId).Distinct().ToList();
                    string strTestFleet = string.Join(",", distinctFleet.ToArray());
                    Session["FleetIDs"] = strTestFleet;

                    //To get the distinct values
                    List<string> distinctCrew = Crewlst.Select(node => node.ChildNodeId).Distinct().ToList();
                    string strTestCrew = string.Join(",", distinctCrew.ToArray());
                    Session["CrewIDs"] = strTestCrew;

                    switch (DisplayOptions.Planner.PlannerDisplayOption)
                    {
                        case PlannerDisplayOption.FleetOnly:
                            BindFleetTree(Fleetlst);
                            break;

                        case PlannerDisplayOption.CrewOnly:
                            BindCrewTree(Crewlst);
                            break;

                        case PlannerDisplayOption.Both:
                            BindFleetTree(Fleetlst);
                            BindCrewTree(Crewlst);
                            break;
                    }
                }
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                    if (IsSave)
                    {
                        if (GetCheckedTreeNodes(CrewTreeView).Count == 0)
                        {
                            CrewTreeView.CheckAllNodes();
                        }

                        var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                        //var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);

                        var selectedGroupCrewIDs = GetCheckedTreeNodes(CrewTreeView, true);
                        var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView, false);

                        bool IsEntireCrewSelected = IsEntireCrewChecked(CrewTreeView);

                        //To get the distinct values
                        List<string> distinctFleet = selectedFleetIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                        string strTestFleet = string.Join(",", distinctFleet.ToArray());
                        Session["FleetIDs"] = strTestFleet;

                        //To get the distinct values
                        List<string> distinctCrew = selectedCrewIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                        string strTestCrew = string.Join(",", distinctCrew.ToArray());
                        Session["CrewIDs"] = strTestCrew;

                        objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Planner, selectedFleetIDs, selectedCrewIDs, selectedGroupCrewIDs, !IsEntireCrewSelected);
                       
                        if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                        {
                            BindQuickFleet();
                            var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                            Collection<string> InputFromFleetTree = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                            {
                                InputFromFleetTree.Add(s.ChildNodeId);
                            }
                            BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromFleetTree);
                            RadPlannerFleetScheduler.SelectedDate = StartDate;
                        }
                        else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                        {
                            BindQuickCrew();
                            Collection<string> InputFromCrewTree = new Collection<string>();
                            var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                            foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                            {
                                InputFromCrewTree.Add(s.ChildNodeId);
                            }
                            BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                            RadPlannerCrewScheduler.SelectedDate = StartDate;
                        }
                        else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                        {
                            BindQuickFleet();
                            BindQuickCrew();
                            var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                            Collection<string> InputFromFleetTree = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                            {
                                InputFromFleetTree.Add(s.ChildNodeId);
                            }
                            BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromFleetTree);
                            RadPlannerFleetScheduler.SelectedDate = StartDate;
                            var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                            Collection<string> InputFromCrewTree = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                            {
                                InputFromCrewTree.Add(s.ChildNodeId);
                            }
                            BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                            RadPlannerCrewScheduler.SelectedDate = StartDate;
                        }
                    }

                    if (FleetTreeView.CheckedNodes.Count == 0)
                    {
                        FleetTreeView.CheckAllNodes();
                    }

                    if (CrewTreeView.CheckedNodes.Count == 0)
                    {
                        CrewTreeView.CheckAllNodes();
                    }

                    if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                    {
                        BindQuickFleet();
                        var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                        BindFleetTree(inputFromFleetTree);
                        Collection<string> InputFromFleetTree = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                        {
                            InputFromFleetTree.Add(s.ChildNodeId);
                        }
                        BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromFleetTree);

                        //To get the distinct values
                        List<string> distinctFleet = inputFromFleetTree.Select(node => node.ChildNodeId).Distinct().ToList();
                        string strTestFleet = string.Join(",", distinctFleet.ToArray());
                        Session["FleetIDs"] = strTestFleet;
                    }
                    else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                    {
                        BindQuickCrew();
                        var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                        BindCrewTree(inputFromCrewTree);
                        Collection<string> InputFromCrewTree = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                        {
                            InputFromCrewTree.Add(s.ChildNodeId);
                        }
                        BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                        RadPlannerCrewScheduler.SelectedDate = StartDate;

                        //To get the distinct values
                        List<string> distinctCrew = inputFromCrewTree.Select(node => node.ChildNodeId).Distinct().ToList();
                        string strTestCrew = string.Join(",", distinctCrew.ToArray());
                        Session["CrewIDs"] = strTestCrew;
                    }
                    else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                    {
                        BindQuickFleet();
                        BindQuickCrew();
                        var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                        BindFleetTree(inputFromFleetTree);
                        Collection<string> InputFromFleetTree = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                        {
                            InputFromFleetTree.Add(s.ChildNodeId);
                        }
                        BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromFleetTree);
                        RadPlannerFleetScheduler.SelectedDate = StartDate;                        

                        //To get the distinct values
                        List<string> distinctFleet = inputFromFleetTree.Select(node => node.ChildNodeId).Distinct().ToList();
                        string strTestFleet = string.Join(",", distinctFleet.ToArray());
                        Session["FleetIDs"] = strTestFleet;

                        var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                        BindCrewTree(inputFromCrewTree);
                        Collection<string> InputFromCrewTree = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                        {
                            InputFromCrewTree.Add(s.ChildNodeId);
                        }
                        BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                        RadPlannerCrewScheduler.SelectedDate = StartDate;

                        //To get the distinct values
                        List<string> distinctCrew = inputFromCrewTree.Select(node => node.ChildNodeId).Distinct().ToList();
                        string strTestCrew = string.Join(",", distinctCrew.ToArray());
                        Session["CrewIDs"] = strTestCrew;
                    }
                }

                if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                {
                    RadPanelBar2.FindItemByText("Fleet").Visible = true;
                    RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                    RadPanelBar2.FindItemByText("Crew").Visible = true;
                    RadPanelBar2.FindItemByText("Crew").Expanded = true;
                    RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                    RadPlannerFleetScheduler.Visible = true;
                    RadPlannerCrewScheduler.Visible = true;
                    RadPlannerFleetScheduler.Height = 400;
                }
                else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                {
                    RadPanelBar2.FindItemByText("Fleet").Visible = true;
                    RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                    RadPanelBar2.FindItemByText("Crew").Visible = false;
                    RadPanelBar2.FindItemByText("Crew").Expanded = false;
                    RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                    RadPlannerFleetScheduler.Visible = true;
                    RadPlannerCrewScheduler.Visible = false;                    
                }
                else if (DisplayOptions.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                {
                    RadPanelBar2.FindItemByText("Fleet").Visible = false;
                    RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                    RadPanelBar2.FindItemByText("Crew").Visible = true;
                    RadPanelBar2.FindItemByText("Crew").Expanded = true;
                    RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                    RadPlannerFleetScheduler.Visible = false;
                    RadPlannerCrewScheduler.Visible = true;
                    RadPlannerCrewScheduler.Height = 470;
                }
                this.RadPanelBar2.Items[0].Expanded = false;
                this.RadPanelBar2.Items[1].Expanded = false;
                this.RadPanelBar2.Items[2].Expanded = true;
            }
        }

        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    if (_scPlannerWrapper == null)
                    {
                        using (var client = new CommonServiceClient())
                        {
                            _scPlannerWrapper = client.GetPlannerModal(FilterOptions.HomeBaseOnly);
                        }
                    }
                    var list = _scPlannerWrapper.fleetTreeResult; // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;

                        RadTreeNode parentNode = CommonServiceBindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);
                        FleetTreeView.Nodes.Clear();
                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;

                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    if (_scPlannerWrapper == null)
                    {
                        using (var client = new CommonServiceClient())
                        {
                            _scPlannerWrapper = client.GetPlannerModal(FilterOptions.HomeBaseOnly);
                        }
                    }
                    //var list = _scPlannerWrapper.crewTreeResult;  //if homebaseonly option is selected in filter, filter by homebaseId
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNewGroupBased(crewList, selectedcrewIDs);
                        CrewTreeView.Nodes.Clear();
                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;

                        CrewTreeView.DataBind();
                    }
                }
            }
        }

        private void BindFleetTripInfo(PreflightServiceClient preflightServiceClient, DateTime weekStartDate, DateTime weekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromFleetTree)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, weekStartDate, weekEndDate, filterCriteria, displayOptions, viewMode, inputFromFleetTree))
            {
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                var calendarData = preflightServiceClient.GetFleetPlannerCalendarData(weekStartDate, (short)TimeSpan.Parse(ddlDisplayDays.SelectedValue).TotalHours, serviceFilterCriteria, inputFromFleetTree.Distinct().ToList(), filterCriteria.Trip, filterCriteria.Hold, filterCriteria.WorkSheet, filterCriteria.Canceled, filterCriteria.UnFulfilled);
                Int32 fleetrow = 0;
                string fleetro = null;
               
                var fleetCalendarData = calendarData.EntityList.Where(x => x.TailNum != null || x.RecordType != "C");


                if (displayOptions.Planner.DisplayLegFleet == DisplayLegFleet.All)
                {

                    fleetCalendarData = fleetCalendarData.Where(x => x.TripNUM == 0).Union(fleetCalendarData.Where(x => x.TripNUM != 0).OrderBy(x => x.PlannerColumnPeriodStart).ThenBy(x => x.ArrivalDisplayTime));

                }
                else if (displayOptions.Planner.DisplayLegFleet == DisplayLegFleet.TripLastLeg)
                {
                    fleetCalendarData = fleetCalendarData.GroupBy(x => new { x.PlannerColumnPeriodStart, x.TailNum, x.TripNUM }).Select(x => x.OrderByDescending(y => y.ArrivalDisplayTime).ThenBy(y => y.TailNum)).Select(x => x.First());

                }
                else if (displayOptions.Planner.DisplayLegFleet == DisplayLegFleet.DayLastLeg)
                {

                    fleetCalendarData = fleetCalendarData
                                                            .Where(x => x.RecordType == "M")
                                                            .GroupBy(x => new { x.PlannerColumnPeriodStart, x.TailNum })
                                                            .Select(x => x.OrderByDescending(y => y.ArrivalDisplayTime).ThenBy(y => y.TailNum))
                                                            .Select(x => x.First())
                                                            .Union
                                                (fleetCalendarData
                                                            .Where(x => x.RecordType == "T")
                                                            .GroupBy(x => new { x.PlannerColumnPeriodStart, x.TailNum })
                                                            .Select(x => x.OrderByDescending(y => y.ArrivalDisplayTime)
                                                                .ThenBy(y => y.TailNum)
                                                                .ThenBy(y => y.DepartureDisplayTime))
                                                            .Select(x => x.First()));

                }
                
                calendarData.EntityList = fleetCalendarData.ToList();

                
                var appointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);
                
                if (fleetro == null)
                {
                    RadPlannerFleetScheduler.DataSource = appointments;
                }
                else
                {
                    if (appointments.Count() > fleetrow)
                        //RadPlannerFleetScheduler.DataSource = appointments.Take(fleetrow).ToList();
                        RadPlannerFleetScheduler.DataSource = appointments;
                    else
                        RadPlannerFleetScheduler.DataSource = appointments;
                }

                RadPlannerFleetScheduler.DataStartField = "StartDate";
                RadPlannerFleetScheduler.DataEndField = "EndDate";
                RadPlannerFleetScheduler.DataSubjectField = "Description";
                RadPlannerFleetScheduler.TimelineView.SlotDuration = TimeSpan.Parse(ddlDisplayDays.SelectedValue);
                RadPlannerFleetScheduler.SelectedDate = weekStartDate;
                ResourceType fleetResourceType = GetResourceType("Fleet");
                fleetResourceType.DataSource = GetFleetResourceType(appointments, viewMode);
                RadPlannerFleetScheduler.ResourceTypes.Clear();
                RadPlannerFleetScheduler.ResourceTypes.Add(fleetResourceType);
                RadPlannerFleetScheduler.GroupBy = "TailNum";
                RadPlannerFleetScheduler.AppointmentComparer = new RadSchedulerCustomAppointmentComparer();
                RadPlannerFleetScheduler.GroupingDirection = GroupingDirection.Vertical;
                RadPlannerFleetScheduler.DataBind();

                this.RadPanelBar2.Items[0].Expanded = false;
                this.RadPanelBar2.Items[1].Expanded = false;
                this.RadPanelBar2.Items[2].Expanded = true;

            }
        }

        private void BindCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime weekStartDate, DateTime weekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromCrewTree)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, weekStartDate, weekEndDate, filterCriteria, displayOptions, viewMode, inputFromCrewTree))
            {
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                //var calendarData = preflightServiceClient.GetCrewCalendarData(weekStartDate, weekEndDate, serviceFilterCriteria, inputFromCrewTree.ToList());
                var calendarData = preflightServiceClient.GetCrewPlannerCalendarData(weekStartDate, (short)TimeSpan.Parse(ddlDisplayDays.SelectedValue).TotalHours, serviceFilterCriteria, inputFromCrewTree.Distinct().ToList(), filterCriteria.Trip, filterCriteria.Hold, filterCriteria.WorkSheet, filterCriteria.Canceled, filterCriteria.UnFulfilled);

                calendarData.EntityList = calendarData.EntityList
                                    .Where(x => x.TripNUM == 0)
                                    .Union
                                    (calendarData.EntityList
                                    .Where(x => x.TripNUM != 0)
                                    .OrderBy(x => x.PlannerColumnPeriodStart)
                                    .ThenBy(x => x.ArrivalDisplayTime))
                                    .ToList();                

                var appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode).ToList();


                
                if (displayOptions.Planner.DisplayTailCrew == DisplayTailCrew.TripLastTail)
                {
                    appointments = appointments
                                    .GroupBy(x => new { x.StartDate, x.CrewCD, x.TripNUM })
                                    .Select(x => x.OrderByDescending(y => y.ArrivalDisplayTime)
                                    .ThenBy(y => y.CrewCD)).Select(x => x.First())
                                    .ToList();

                    appointments = appointments
                                    .Where(x => x.TripNUM == 0)
                                    .Union
                                    (appointments
                                    .Where(x => x.TripNUM != 0)
                                    .OrderBy(x => x.StartDate)
                                    .ThenBy(x => x.ArrivalDisplayTime))
                                    .ToList();

                }
                else if (displayOptions.Planner.DisplayTailCrew == DisplayTailCrew.DayLastTail)
                {
                    appointments = appointments
                                    .Where(x => x.TripNUM == 0)
                                    .GroupBy(x => new { x.StartDate, x.CrewCD })
                                    .Select(x => x.OrderByDescending(y => y.ArrivalDisplayTime).ThenBy(y => y.CrewCD))
                                    .Select(x => x.First())
                                    .Union
                                    (appointments
                                    .Where(x => x.TripNUM != 0)
                                    .GroupBy(x => new { x.StartDate, x.CrewCD })
                                    .Select(x => x.OrderByDescending(y => y.ArrivalDisplayTime).ThenBy(y => y.CrewCD))
                                    .Select(x => x.First()))
                                    .ToList();

                    appointments = appointments
                                    .Where(x => x.TripNUM == 0)
                                    .Union
                                    (appointments
                                    .Where(x => x.TripNUM != 0)
                                    .OrderBy(x => x.StartDate)
                                    .ThenBy(x => x.ArrivalDisplayTime))
                                    .ToList();

                }

                var appointment = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>(appointments.ToList());

                RadPlannerCrewScheduler.DataSource = appointment;
                RadPlannerCrewScheduler.DataStartField = "StartDate";
                RadPlannerCrewScheduler.DataEndField = "EndDate";
                RadPlannerCrewScheduler.DataSubjectField = "Description";
                RadPlannerCrewScheduler.TimelineView.SlotDuration = TimeSpan.Parse(ddlDisplayDays.SelectedValue);
                RadPlannerCrewScheduler.SelectedDate = weekStartDate;
                ResourceType crewResourceType = GetResourceType("Crew");
                crewResourceType.DataSource = GetCrewResourceType(appointment);
                RadPlannerCrewScheduler.ResourceTypes.Clear();
                RadPlannerCrewScheduler.ResourceTypes.Add(crewResourceType);
                RadPlannerCrewScheduler.GroupBy = "CrewCD";
                RadPlannerCrewScheduler.AppointmentComparer = new RadSchedulerCustomAppointmentComparer();
                RadPlannerCrewScheduler.GroupingDirection = GroupingDirection.Vertical;
                RadPlannerCrewScheduler.DataBind();

                this.RadPanelBar2.Items[0].Expanded = false;
                this.RadPanelBar2.Items[1].Expanded = false;
                this.RadPanelBar2.Items[2].Expanded = true;

            }
        }

        private static ResourceType GetResourceType(string mode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(mode))
            {
                ResourceType resourceType = new ResourceType("CrewFleetCodes");
                if (mode.Equals(ModuleNameConstants.Preflight.FleetMode))
                {

                    resourceType.KeyField = ModuleNameConstants.Preflight.TailNum;
                    resourceType.Name = ModuleNameConstants.Preflight.TailNum;
                    resourceType.ForeignKeyField = ModuleNameConstants.Preflight.TailNum;
                    resourceType.TextField = ModuleNameConstants.Preflight.Resource_Description;
                }
                else
                {
                    resourceType.KeyField = ModuleNameConstants.Preflight.CewCode;
                    resourceType.Name = ModuleNameConstants.Preflight.CewCode;
                    resourceType.ForeignKeyField = ModuleNameConstants.Preflight.CewCode;
                    resourceType.TextField = ModuleNameConstants.Preflight.Resource_Description;
                }
                resourceType.AllowMultipleValues = true;
                return resourceType;
            }
        }

       

        public List<FleetResourceTypeResult> GetFleetResourceType(Collection<Entities.SchedulingCalendar.Appointment> appointments, string viewMode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointments, viewMode))
            {
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var fleetIdsFromTree = GetCheckedTreeNodes(FleetTreeView);
                    Int32 fleetrows = 0;
                    string fleetro = null;
                    Collection<string> FleetIdsFromTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in fleetIdsFromTree)
                    {
                        FleetIdsFromTree.Add(s.ChildNodeId);
                    }
                    var fleetResources = preflightServiceClient.GetFleetResourceType((FlightPak.Web.PreflightService.VendorOption)FilterOptions.VendorsFilter, FleetIdsFromTree.ToList(), FilterOptions.HomeBaseOnly, FilterOptions.HomeBaseID);
                    if (fleetResources.EntityList != null)
                    {
                        if (fleetro == null)
                        {
                            return fleetResources.EntityList;

                        }
                        else
                        {
                            if (fleetResources.EntityList.Count() > fleetrows)
                                return fleetResources.EntityList.Take(fleetrows).ToList();
                            else
                                return fleetResources.EntityList;
                        }
                    }
                    return new List<FleetResourceTypeResult>();
                }
            }
        }

        public List<CrewResourceTypeResult> GetCrewResourceType(Collection<Entities.SchedulingCalendar.Appointment> appointments)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointments))
            {
                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var crewIdsFromTree = GetCheckedTreeNodes(CrewTreeView, false);
                    Collection<string> CrewIdsFromTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in crewIdsFromTree)
                    {
                        CrewIdsFromTree.Add(s.ChildNodeId);
                    }
                    var crewResources = preflightServiceClient.GetCrewResourceType(CrewIdsFromTree.ToList(), FilterOptions.HomeBaseOnly, FilterOptions.HomeBaseID, FilterOptions.FixedWingCrewOnly, FilterOptions.RotaryWingCrewCrewOnly, ModuleNameConstants.Preflight.Planner);
                    if (crewResources.EntityList != null)
                    {
                        if (chkCrewavailability.Checked == false)
                        {
                            //return crewResources.EntityList;
                            return crewResources.EntityList.GroupBy(i => i.CrewID).Select(g => g.First()).OrderBy(i => i.CrewCD).ToList();
                            //return ReorderResource(crewResources.EntityList);
                        }
                        else
                        {
                            List<CrewResourceTypeResult> Finallist = new List<CrewResourceTypeResult>();
                            var distinctAppointmentCrewIds = appointments.Select(x => x.CrewId).Distinct().ToList();
                            Finallist.AddRange(crewResources.EntityList.Where(x => distinctAppointmentCrewIds.Contains(x.CrewID)).ToList());
                            Finallist.AddRange(crewResources.EntityList.Where(x => !distinctAppointmentCrewIds.Contains(x.CrewID)).ToList());
                            //return Finallist;
                            return Finallist.GroupBy(i => i.CrewID).Select(g => g.First()).OrderBy(i => i.CrewCD).ToList();
                        }
                    }
                    return new List<CrewResourceTypeResult>();
                }
            }
        }
        //public List<CrewResourceTypeResult> ReorderResource(List<CrewResourceTypeResult> CrewResources)
        //{
        //    if ((chkCrewavailability.Checked == true) && !string.IsNullOrEmpty(Convert.ToString(Session["SelectedTripID"])))
        //    {
        //        Int64 Tid = Convert.ToInt64(Session["SelectedTripID"]);
        //        string[] Ccodes = Convert.ToString(Session["CrewCodes"]).Split(',');
        //        if (Ccodes.Length > 0)
        //        {
        //            var crewResources = CrewResources.OrderBy(x => Ccodes.Contains(x.CrewCD)).ToList();
        //            return crewResources;
        //        }
        //        else
        //            return CrewResources;
        //    }
        //    else
        //    {
        //        return CrewResources;
        //    }
        //}

        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetPlannerCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {
            Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {

                foreach (var leg in calendarData)
                {
                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                    SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                    appointmentCollection.Add(appointment);
                }
                return appointmentCollection;
            }
        }
      

        private void SetFleetAppointment(List<FleetPlannerCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, displayOptions, viewMode, tripLeg, appointment))
            {

                appointment = SetFleetPlannerCommonProperties(calendarData, timeBase, tripLeg, appointment);


                // todo: build description for appointment based on view selected and display options for rendering
                if (tripLeg.RecordType == "T")
                {
                    if (tripLeg.LegNUM == 0) // dummy appointment
                    {
                        appointment.Description = appointment.TailNum + " ( " + appointment.AircraftDutyCD + " ) ";

                        var options = displayOptions.Planner;

                        switch (options.DepartArriveInfo)
                        {
                            case DepartArriveInfo.ICAO:
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;
                        }

                        var flightNumber = string.Empty;
                        if (options.FlightNumber)
                        {
                            flightNumber = " Flt No.: " + appointment.FlightNum;

                        }

                        //appointment.Description = appointment.Description + appointment.ArrivalICAOID;

                        //  below 2 lines are added to fix IE issue - hide / show tooltip...
                        //appointment.ToolTipSubject = appointment.Description;
                        appointment.ToolTipSubject = appointment.Description + appointment.ArrivalICAOID;
                        appointment.ToolTipDescription = BuildWeeklyFleetOnlyToolTipDescription(appointment, displayOptions); //" ";
                    }
                    else // db trips
                    {
                        appointment.ToolTipSubject = BuildWeeklyFleetAppointmentDescription(appointment, calendarData, displayOptions);
                        appointment.ToolTipDescription = BuildWeeklyFleetToolTipDescription(appointment, displayOptions);
                    }                   
                }
                else // entries...
                {
                    switch (displayOptions.Planner.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;
                    }

                    StringBuilder builder = new StringBuilder();
                    if (appointment.RecordType == "M" || (appointment.RecordType == "C" && !string.IsNullOrEmpty(appointment.TailNum) && appointment.TailNum != "DUMMY"))
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(") ");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                            {
                                builder.Append("(");
                                builder.Append(appointment.CrewDutyTypeCD);
                                builder.Append(") ");
                            }
                        }
                        
                        if (appointment.DepartureDisplayTime.Date == appointment.StartDate)
                        {
                            builder.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.ArrivalDisplayTime.Date == appointment.StartDate)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }
                        builder.Append("</br>");

                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append(appointment.AircraftDutyDescription);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(appointment.CrewDutyTypeDescription))
                            {
                                builder.Append(appointment.CrewDutyTypeDescription);
                            }
                        }
                       
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(") ");
                        }

                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append("</br>");
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append(appointment.AircraftDutyDescription);
                        }
                    }

                    if (appointment.IsStandByCrew && !string.IsNullOrEmpty(appointment.CrewCodes))
                    {
                        builder.Append("<br />\n");
                        builder.Append(appointment.CrewCodes);
                        builder.Append(" - ");
                    }
                    builder.Append(appointment.Notes);
                    //appointment.Description = builder.ToString();
                    //  below 2 lines are added to fix IE issue - hide / show tooltip...
                    appointment.ToolTipSubject = builder.ToString();
                    appointment.ToolTipDescription = BuildWeeklyFleetOnlyToolTipDescription(appointment, displayOptions); //appointment.Description;
                }
                appointment.ViewMode = viewMode;
            }
        }
        
        public FlightPak.Web.Entities.SchedulingCalendar.Appointment SetFleetCommonProperties(List<FlightPak.Web.PreflightService.FleetPlannerCalendarDataResult> calendarData, string timeBase, FlightPak.Web.PreflightService.FleetPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                //appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;
                appointment.TripId = Convert.ToInt64(tripLeg.TripID);
                appointment.TailNum = tripLeg.TailNum;
                //appointment.Notes = tripLeg.Notes;
                appointment.TripStatus = tripLeg.TripStatus;

                //appointment.DepartureDisplayTime = tripLeg.DepartureDisplayTime.Value;
                //appointment.ArrivalDisplayTime = tripLeg.ArrivalDisplayTime.Value;
                appointment.StartDate = appointment.DepartureDisplayTime;
                appointment.EndDate = appointment.ArrivalDisplayTime;
                if (appointment.EndDate < appointment.StartDate) // this condition will occur only in Local time base
                {
                    appointment.EndDate = appointment.StartDate + (tripLeg.ArrivalGreenwichDTTM.Value - tripLeg.DepartureGreenwichDTTM.Value);
                    if (appointment.EndDate.Date > appointment.ArrivalDisplayTime.Date) // if calculated endDate > original end date...
                    {
                        appointment.EndDate = appointment.DepartureDisplayTime.Date.AddHours(23).AddMinutes(59);
                    }
                }

                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;               
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();
                appointment.IsRONAppointment = tripLeg.LegNUM == 0 ? true : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;
                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg) //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false; // SHOW details only based on company profile setting - Trip privacy settings
                }
                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }
                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();
                appointment.RecordType = tripLeg.RecordType;
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();

                return appointment;

            }

        }

        public string BuildWeeklyFleetOnlyToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {
                var options = displayOptions.Planner;
                description.Append("<table><tr><td>");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append(" </td></tr></table>");
            }
            return description.ToString();
        }

       

        // description for appointment
        public string BuildWeeklyFleetAppointmentDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, List<FleetPlannerCalendarDataResult> fleetCalendarData, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, fleetCalendarData, displayOptions))
            {
                var description = new StringBuilder();

                var options = displayOptions.Planner;
                var departureArrivalInfo = options.DepartArriveInfo;

                if (appointment.LegNum == 9999)
                {
                    description.Append("<b>");
                    description.Append("(R)");
                    description.Append("  ");
                    //description.Append(appointment.ArrivalICAOID);

                    switch (options.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            description.Append(appointment.ArrivalICAOID);
                            break;

                        case DepartArriveInfo.AirportName:
                            description.Append(appointment.ArrivalAirportName);
                            break;

                        case DepartArriveInfo.City:
                            description.Append(appointment.ArrivalCity);
                            break;
                    }

                    description.Append("</b><br/>");
                    description.Append("  ");
                    description.Append("<table><tr><td>");
                    description.Append("<strong>Home Base: </strong>");
                    description.Append(appointment.HomebaseCD);
                    description.Append(" </td></tr></table>");
                }
                else
                {
                    description.Append("<table>");
                    if (options.ShowTrip)
                    {
                        description.Append("<tr><td>");
                        description.Append("Trip: ");
                        description.Append(appointment.TripNUM);
                        description.Append("</td></tr>");

                    }
                    if (appointment.ShowAllDetails)
                    {
                        if (options.FlightNumber)
                        {
                            description.Append("<tr><td>");
                            description.Append("Flt No.: ");
                            description.Append(appointment.FlightNum);
                            description.Append("</td></tr>");
                        }                        
                    }                    
                    else
                        {
                            if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                            {
                                description.Append("<tr><td>");
                                description.Append("Flt No.: ");
                                description.Append(appointment.FlightNum);
                                description.Append("</td></tr>");
                            }
                        }

                    if (options.ShowTripStatus)
                    {
                        description.Append("<tr><td>");
                        description.Append("( ");
                        description.Append(appointment.TripStatus);
                        description.Append(" ) ");
                        description.Append("</td></tr>");
                    }

                    //description.Append("<br />\n");
                    if (options.ShowTrip)
                    {
                        description.Append("<tr><td>");
                        description.Append("Leg No.: ");
                        description.Append(appointment.LegNum);
                        description.Append("</td></tr>");
                    }

                    if (appointment.ShowAllDetails)
                    {
                        if (options.FlightCategory)
                        {
                            description.Append("<tr><td>");
                            description.Append("<strong>Cat: </strong>");
                            description.Append(appointment.FlightCategoryCode);
                            description.Append("</td></tr>");
                        }
                    }
                    else
                    {
                        if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                        {
                            description.Append("<tr><td>");
                            description.Append("<strong>Cat: </strong>");
                            description.Append(appointment.FlightCategoryCode);
                            description.Append("</td></tr>");
                        }
                    }
                    description.Append("</table>");
                    GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);

                    description.Append(" ");
                   
                }
                return description.ToString();
            }

        }

        //description for tooltip

        public string BuildWeeklyFleetToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.Planner;               

                description.Append("<table cellpadding='0' cellspacing='0'><tr><td>");

                if (appointment.ShowAllDetails)
                {                
                           
                    if (options.ETE)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>ETE: </strong>");
                        string ETE = (appointment.LegNum == 9999) ? RoundElpTime(0) : appointment.ETE;
                        description.Append(ETE);
                        description.Append("</td></tr>");
                    }
                    
                    if (options.CummulativeETE)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append("&nbsp;&nbsp;&nbsp;");
                        description.Append("</td></tr>");
                    }
                    if (options.TotalETE)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);                        
                        description.Append("</td></tr>");
                    }
                    
                    if (options.Requestor)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);                        
                        description.Append("</td></tr>");
                    }                   
                    
                    if (options.PaxCount)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>PAX: </strong>");
                        int PaxCount = (appointment.LegNum == 9999) ? 0 : appointment.PassengerCount;
                        description.Append(PaxCount);
                        description.Append("</td></tr>");
                    }
                    if (options.SeatsAvailable)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append("</td></tr>");
                    }
                    
                    if (options.Department)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("</td></tr>");
                    }
                    
                    if (options.Authorization)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append("</td></tr>");
                    }
                                        
                    if (options.TripPurpose)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append("</td></tr>");
                    }
                  
                    if (options.LegPurpose)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append("</td></tr>");
                    }        
                   
                   
                    if (options.Crew)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCodes);
                           
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                        description.Append("</td></tr>");
                    }

                }
                else // show details based on Trip Privacy Settins from company profile
                {

                    
                    if (PrivateTrip.IsShowETE && options.ETE)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>ETE: </strong>");
                        string ETE = (appointment.LegNum == 9999) ? RoundElpTime(0) : appointment.ETE;
                        description.Append(ETE);
                        description.Append("</td></tr>");
                    }                    
                    if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append("</td></tr>");
                    }
                                        
                    if (PrivateTrip.IsShowTotalETE && options.TotalETE)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append("</td></tr>");
                    }

                    if (PrivateTrip.IsShowRequestor && options.Requestor)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append("</td></tr>");
                    }
                    
                    if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>PAX: </strong>");
                        int PaxCount = (appointment.LegNum == 9999) ? 0 : appointment.PassengerCount;
                        description.Append(PaxCount);
                        description.Append("</td></tr>");
                    }

                    if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append("</td></tr>");
                    }
                    
                    if (PrivateTrip.IsShowDepartment && options.Department)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("</td></tr>");
                    }
                    
                    if (PrivateTrip.IsShowAuthorization && options.Authorization)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append("</td></tr>");
                    }
                                       
                    if (PrivateTrip.IsShowTripPurpose && options.TripPurpose)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append("</td></tr>");
                    }
                                        
                    if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append("</td></tr>");
                    }                  
                    
                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {
                        description.Append("<tr><td>");
                        description.Append("<strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                          
                            description.Append(appointment.CrewCodes);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                        description.Append("</td></tr>");
                    }
                }

                description.Append("</table>");
            }
            return description.ToString();
        }    

        public string BuildWeeklyCrewAppointmentDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, List<CrewPlannerCalendarDataResult> crewPlannerCalendarData, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, crewPlannerCalendarData, displayOptions))
            {
                var description = new StringBuilder();

                var options = displayOptions.Planner;
                var departureArrivalInfo = options.DepartArriveInfo;

                /*Start: Added for displaying multiple Legs in Tooltip*/
                crewPlannerCalendarData = crewPlannerCalendarData.Where(x => x.TripNUM == appointment.TripNUM && x.PlannerColumnPeriodStart == appointment.StartDate && x.CrewCD==appointment.CrewCD).ToList();

                foreach (var crewData in crewPlannerCalendarData)
                {
                    if (crewData.LegNUM == 9999)
                    {
                        description.Append("<b>");
                        description.Append("(R)");
                        description.Append("&nbsp;&nbsp;");
                        description.Append(crewData.TailNum);
                        description.Append("&nbsp;&nbsp;");
                        
                        switch (options.DepartArriveInfo)
                        {
                            case DepartArriveInfo.ICAO:
                                description.Append(crewData.ArrivalICAOID);
                                break;

                            case DepartArriveInfo.AirportName:
                                description.Append(crewData.ArrivalAirportName);
                                break;

                            case DepartArriveInfo.City:
                                description.Append(crewData.ArrivalCity);
                                break;
                        }
                        description.Append("</b>");
                        description.Append("  ");
                        
                    }
                    else
                    {
                        description.Append("<b>");
                        description.Append(crewData.TailNum);
                        description.Append("</b>");
                        description.Append("<br />\n");

                        if (options.ShowTrip)
                        {
                            description.Append("Trip: ");
                            description.Append(crewData.TripNUM);
                            description.Append("   ");
                            description.Append("Leg No.: ");
                            description.Append(crewData.LegNUM);
                        }

                        if (options.ShowTripStatus)
                        {
                            description.Append("( ");
                            description.Append(crewData.TripStatus);
                            description.Append(" ) ");
                        }
                        description.Append("<br />\n");

                       
                        GetDepartureArrivalInfoCrew(appointment, departureArrivalInfo, description, crewData);

                        description.Append(" ");
                        //Added for Displaying date in radsceduler
                        if (!options.DisplayRedEyeFlightsInContinuation && crewData.ArrivalDisplayTime.Value.Date != crewData.DepartureDisplayTime.Value.Date && crewData.RecordType == "T")
                        {
                            description.Append("<br />\n");
                            description.Append("* Arrival Date: ");
                            description.Append(String.Format("{0:" + ApplicationDateFormat + "}", Convert.ToDateTime(crewData.ArrivalDisplayTime.Value.ToShortDateString().ToString())));
                            description.Append(" *");
                        }
                        description.Append("<br />\n");

                        if (options.ETE)
                        {
                            description.Append("<strong>ETE: </strong>");
                            description.Append(RoundElpTime(Convert.ToDouble(crewData.ElapseTM)));
                            description.Append("&nbsp;&nbsp;");
                        }

                        if (options.CummulativeETE)
                        {
                            description.Append("<strong>Cum ETE: </strong>");
                            description.Append(RoundElpTime(Convert.ToDouble(crewData.CumulativeETE)));
                            description.Append("&nbsp;&nbsp;");
                        }

                        if (options.TotalETE)
                        {
                            description.Append("<strong>Total ETE: </strong>");
                            description.Append(appointment.TotalETE);
                            description.Append(" ");
                        }

                        description.Append("<br /><br />\n");
                        description.Append(" ");
                    }
                }
                /*End-Added for multiple Legs*/

                return description.ToString();
            }

        }

        private string RoundElpTime(double lnElp_Time)
        {
            string convertedValue = string.Empty;
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) // Minutes
            {
                double lnElp_Min = 0.0;
                double lnEteRound = ETERound;

                if (lnEteRound > 0)
                {

                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    //lnElp_Min = lnElp_Min % lnEteRound;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);


                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);

                    convertedValue = ConvertTenthsToMins(lnElp_Time.ToString());
                }
                convertedValue = ConvertTenthsToMins(lnElp_Time.ToString()); // None -option
            }
            else
            {
                convertedValue = Math.Round(lnElp_Time, 1).ToString();
            }

            return convertedValue;
        }

        //Departure and Arrival information for tooltip
        private void GetDepartureArrivalInfo(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DepartArriveInfo departureArrivalInfo, StringBuilder description)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, departureArrivalInfo, description))
            {
                if (appointment.ShowAllDetails)
                {
                    description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                    description.Append(" ");
                    switch (departureArrivalInfo)
                    {
                        case DepartArriveInfo.ICAO:

                            description.Append(appointment.DepartureICAOID);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                            break;

                        case DepartArriveInfo.AirportName:

                            description.Append(appointment.DepartureAirportName);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                            break;

                        case DepartArriveInfo.City:

                            description.Append(appointment.DepartureCity);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                            break;

                    }
                    description.Append(" ");
                    description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                }
                else // show as trip privacy settings from company profile
                {
                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        description.Append(" ");
                    }

                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (departureArrivalInfo)
                        {
                            case DepartArriveInfo.ICAO:

                                description.Append(appointment.DepartureICAOID);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                                break;

                            case DepartArriveInfo.AirportName:

                                description.Append(appointment.DepartureAirportName);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                                break;

                            case DepartArriveInfo.City:

                                description.Append(appointment.DepartureCity);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                                break;

                        }
                    }

                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (PrivateTrip.IsShowArrivalDepartICAO)
                        {
                            description.Append(" ");
                        }
                        else
                        {
                            description.Append(" | ");

                        }

                        description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                    }
                }
            }
        }       

        //description for tooltip

        //Departure and Arrival information for tooltip for Crew
        private void GetDepartureArrivalInfoCrew(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DepartArriveInfo departureArrivalInfo, StringBuilder description, CrewPlannerCalendarDataResult CalendarData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, departureArrivalInfo, description))
            {
                if (appointment.ShowAllDetails)
                {
                    //description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                    if (CalendarData.DepartureDisplayTime.HasValue)
                    description.Append(CalendarData.DepartureDisplayTime.Value.ToString("HH:mm"));
                    description.Append(" ");
                    switch (departureArrivalInfo)
                    {
                        case DepartArriveInfo.ICAO:

                            description.Append(CalendarData.DepartureICAOID);
                            description.Append(" | ");
                            description.Append(CalendarData.ArrivalICAOID != null ? CalendarData.ArrivalICAOID : string.Empty);

                            break;

                        case DepartArriveInfo.AirportName:

                            description.Append(CalendarData.DepartureAirportName);
                            description.Append(" | ");
                            description.Append(CalendarData.ArrivalAirportName != null ? CalendarData.ArrivalAirportName : string.Empty);

                            break;

                        case DepartArriveInfo.City:

                            description.Append(CalendarData.DepartureCity);
                            description.Append(" | ");
                            description.Append(CalendarData.ArrivalCity != null ? CalendarData.ArrivalCity : string.Empty);

                            break;

                    }
                    description.Append(" ");
                    if (CalendarData.ArrivalDisplayTime.HasValue)
                        description.Append(CalendarData.ArrivalDisplayTime.Value.ToString("HH:mm"));
                    
                }
                else // show as trip privacy settings from company profile
                {
                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (CalendarData.DepartureDisplayTime.HasValue)
                            description.Append(CalendarData.DepartureDisplayTime.Value.ToString("HH:mm"));                      
                        description.Append(" ");
                    }

                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (departureArrivalInfo)
                        {
                            case DepartArriveInfo.ICAO:

                                description.Append(CalendarData.DepartureICAOID);
                                description.Append(" | ");
                                description.Append(CalendarData.ArrivalICAOID != null ? CalendarData.ArrivalICAOID : string.Empty);

                                break;

                            case DepartArriveInfo.AirportName:

                                description.Append(CalendarData.DepartureAirportName);
                                description.Append(" | ");
                                description.Append(CalendarData.ArrivalAirportName != null ? CalendarData.ArrivalAirportName : string.Empty);

                                break;

                            case DepartArriveInfo.City:

                                description.Append(CalendarData.DepartureCity);
                                description.Append(" | ");
                                description.Append(CalendarData.ArrivalCity != null ? CalendarData.ArrivalCity : string.Empty);

                                break;

                        }
                    }

                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (PrivateTrip.IsShowArrivalDepartICAO)
                        {
                            description.Append(" ");
                        }
                        else
                        {
                            description.Append(" | ");

                        }
                        if(CalendarData.ArrivalDisplayTime.HasValue)
                            description.Append(CalendarData.ArrivalDisplayTime.Value.ToString("HH:mm"));
                    }
                }
            }
        }

        //description for tooltip Crew

        public string BuildWeeklyCrewToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {
                var options = displayOptions.Planner;
                description.Append("<table>");
                if (appointment.ShowAllDetails)
                {
                    
                    if (options.Crew)
                    {
                        // todo: get from Anoop
                        if (!string.IsNullOrEmpty(appointment.CrewCodes))
                        {
                           

                            string currentCrews = appointment.CrewCodes;
                            currentCrews = currentCrews.Replace(appointment.CrewCD, "");
                            if (!string.IsNullOrEmpty(currentCrews))
                            {
                                string[] crews = currentCrews.Split(',');
                                string newCrews = string.Empty;
                                foreach (string crew in crews)
                                {
                                    if (!string.IsNullOrEmpty(crew) && crew != ",")
                                    {
                                        if (string.IsNullOrEmpty(newCrews))
                                            newCrews = crew;
                                        else
                                            newCrews = newCrews + "," + crew;
                                    }
                                }
                                description.Append("<strong>Crew: </strong>");
                                description.Append(newCrews);
                            }
                        }

                                            }
                }
                else // show details based on Trip Privacy Settins from company profile
                {
                    //if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                    //{
                    //    description.Append("<strong>Flt No.: </strong>");
                    //    description.Append(appointment.FlightNum);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td><td>");

                    //if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                    //{
                    //    description.Append("<strong>Category: </strong>");
                    //    description.Append(appointment.FlightCategoryCode);

                    //}
                    //description.Append(" </td></tr>");

                    //description.Append(" <tr><td colspan='2'>");
                    //if (PrivateTrip.IsShowETE && options.ETE)
                    //{
                    //    description.Append("<strong>ETE: </strong>");
                    //    description.Append(appointment.ETE);
                    //    description.Append(" ");
                    //}
                    //description.Append("</td><td>");
                    //if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                    //{
                    //    description.Append("<strong>Cum ETE: </strong>");
                    //    description.Append(appointment.CumulativeETE);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td></tr>");

                    //description.Append(" <tr><td colspan='2'>");
                    //if (PrivateTrip.IsShowTotalETE && options.TotalETE)
                    //{
                    //    description.Append("<strong>Total ETE: </strong>");
                    //    description.Append(appointment.TotalETE);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td></tr>");

                    //description.Append(" <tr><td>");
                    //if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                    //{
                    //    description.Append("<strong>PAX: </strong>");
                    //    description.Append(appointment.PassengerCount);
                    //    description.Append(" ");
                    //}
                    //description.Append("</td><td>");
                    //if (PrivateTrip.IsShowRequestor && options.Requestor)
                    //{
                    //    description.Append("<strong>Req: </strong>");
                    //    description.Append(appointment.PassengerRequestorCD);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td></tr>");

                    //description.Append(" <tr><td>");
                    //if (PrivateTrip.IsShowDepartment && options.Department)
                    //{
                    //    description.Append("<strong>Dept: </strong>");
                    //    description.Append(appointment.DepartmentCD);
                    //    description.Append(" ");
                    //}
                    //description.Append("</td><td>");
                    //if (PrivateTrip.IsShowAuthorization && options.Authorization)
                    //{
                    //    description.Append("<strong>Auth: </strong>");
                    //    description.Append(appointment.AuthorizationCD);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td></tr>");


                    //description.Append(" <tr><td colspan='2'>");
                    //if (PrivateTrip.IsShowTripPurpose && options.TripPurpose)
                    //{
                    //    description.Append("<strong>Trip Purp.: </strong>");
                    //    description.Append(appointment.TripPurpose);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td></tr>");


                    //description.Append(" <tr><td colspan='2'>");
                    //if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                    //{
                    //    description.Append("<strong>Leg Purp.: </strong>");
                    //    description.Append(appointment.FlightPurpose);
                    //    description.Append(" ");
                    //}
                    //description.Append(" </td></tr>");

                    //description.Append(" <tr><td>");
                    //if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                    //{
                    //    description.Append("<strong>Seats Avail: </strong>");
                    //    description.Append(appointment.ReservationAvailable);
                    //    description.Append(" ");
                    //}
                    //description.Append("</td><td>");

                    //if (PrivateTrip.IsShowCrew && options.Crew)
                    {
                        // todo: get from Anoop
                        /*description.Append("<strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCD);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }*/
                        if (!string.IsNullOrEmpty(appointment.CrewCodes))
                        {
                            //description.Append("<strong>Crew: </strong>");
                            //description.Append(appointment.CrewCodes);

                            string currentCrews = appointment.CrewCodes;
                            currentCrews = currentCrews.Replace(appointment.CrewCD, "");
                            if (!string.IsNullOrEmpty(currentCrews))
                            {
                                string[] crews = currentCrews.Split(',');
                                string newCrews = string.Empty;
                                foreach (string crew in crews)
                                {
                                    if (!string.IsNullOrEmpty(crew) && crew != ",")
                                    {
                                        if (string.IsNullOrEmpty(newCrews))
                                            newCrews = crew;
                                        else
                                            newCrews = newCrews + "," + crew;
                                    }
                                }
                                description.Append("<strong>Crew: </strong>");
                                description.Append(newCrews);
                            }
                        }
                    }
                }
                description.Append(" </td></tr></table>");
            }

            return description.ToString();
        }

        public string BuildWeeklyCrewOnlyToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.Planner;

                description.Append("<table><tr><td>");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append(" </td></tr></table>");
            }

            return description.ToString();
        }     

        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewPlannerCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                foreach (var leg in calendarData)
                {
                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                    SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                    appointmentCollection.Add(appointment);
                }
                return appointmentCollection;
            }
        }

        private void SetCrewAppointment(List<CrewPlannerCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {
                
                appointment = SetCrewPlannerCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);

                // todo: build description for appointment based on view selected and display options for rendering
                if (tripLeg.RecordType == "T")
                {

                    if (tripLeg.LegNUM == 0) // dummy appointment
                    {
                        appointment.Description = appointment.TailNum + " ( " + appointment.AircraftDutyCD + " ) ";

                        var options = displayOptions.Planner;

                        switch (options.DepartArriveInfo)
                        {
                            case DepartArriveInfo.ICAO:
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;
                        }

                        var flightNumber = string.Empty;
                        if (options.FlightNumber)
                        {
                            flightNumber = " Flt No.: " + appointment.FlightNum;
                        }

                        appointment.ToolTipSubject = appointment.Description + appointment.ArrivalICAOID;
                        appointment.ToolTipDescription = "";
                    }
                    else // db trips
                    {
                        appointment.ToolTipSubject = BuildWeeklyCrewAppointmentDescription(appointment, calendarData, displayOptions);
                        appointment.ToolTipDescription = BuildWeeklyCrewToolTipDescription(appointment, displayOptions);
                    }
                }
                else // entries...
                {
                    switch (displayOptions.Planner.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;
                    }

                    StringBuilder builder = new StringBuilder();
                    if (appointment.RecordType == "M")
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(") ");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(") ");
                        }
                    }

                    if (appointment.RecordType == "C")
                    {
                        if (appointment.StartDate == appointment.DepartureDisplayTime.Date)
                        {
                            builder.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.StartDate == appointment.ArrivalDisplayTime.Date)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }
                        builder.Append("</br>");
                        builder.Append(appointment.CrewDutyTypeDescription);
                        
                    }
                    else
                    {
                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");
                    }

                    builder.Append(appointment.Notes);
                    
                    //  below 2 lines are added to fix IE issue - hide / show tooltip...
                    appointment.ToolTipSubject = builder.ToString();
                    appointment.ToolTipDescription = BuildWeeklyCrewOnlyToolTipDescription(appointment, displayOptions); //appointment.Description;
                }
                appointment.ViewMode = viewMode;
            }
        }

     
        protected void prevButton_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCPlannerPreviousCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                                selectedDate = Convert.ToDateTime(selectedDate.ToShortDateString());
                            }
                            StartDate = GetStartDate(selectedDate, "-", 1);
                            EndDate = GetEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            Session["SCSelectedDay"] = StartDate;
                            RadDatePicker1.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            loadTree(false);
                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            sessionpreviousDay = Convert.ToDateTime(sessionpreviousDay.ToShortDateString());
                            StartDate = GetStartDate(sessionpreviousDay, "-", 1);
                            EndDate = GetEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCPlannerPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void nextButton_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;
                        if (Convert.ToInt32(Session["SCPlannerNextCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                                selectedDate = Convert.ToDateTime(selectedDate.ToShortDateString());
                            }
                            StartDate = GetStartDate(selectedDate, "+", 1);
                            EndDate = GetEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            sessionnextDay = Convert.ToDateTime(sessionnextDay.ToShortDateString());
                            StartDate = GetStartDate(sessionnextDay, "+", 1);
                            EndDate = GetEndDate(StartDate);                            
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCPlannerNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        int count = 0;

                        if (Convert.ToInt32(Session["SCPlannerLastCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                                selectedDate = Convert.ToDateTime(selectedDate.ToShortDateString());
                            }
                            StartDate = GetStartDate(selectedDate, "+", 30);
                            EndDate = GetEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);                            

                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            sessionnextDay = Convert.ToDateTime(sessionnextDay.ToShortDateString());
                            StartDate = GetStartDate(sessionnextDay, "+", 30);
                            EndDate = GetEndDate(StartDate);                            
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCPlannerLastCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCPlannerFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                                selectedDate = Convert.ToDateTime(selectedDate.ToShortDateString());
                            }
                            StartDate = GetStartDate(selectedDate, "-", 30);
                            EndDate = GetEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            selectedDate = Convert.ToDateTime(selectedDate.ToShortDateString());
                            StartDate = GetStartDate(selectedDate, "-", 30);
                            EndDate = GetEndDate(StartDate);
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadDatePicker1.SelectedDate = StartDate;

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCPlannerFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        public DateTime GetStartDate(DateTime weekStartDate, string addsubtract, int noofslots)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weekStartDate, addsubtract, noofslots))
            {
                weekStartDate = Convert.ToDateTime(weekStartDate.ToShortDateString());

                if (addsubtract == "+")
                {
                    if (noofslots == 30)
                    {
                        if (ddlDisplayDays.SelectedItem.Text == "12 Hours Per Column")
                            return weekStartDate.AddDays(15);
                        else if (ddlDisplayDays.SelectedItem.Text == "8 Hours Per Column")
                            return weekStartDate.AddDays(10);
                        else if (ddlDisplayDays.SelectedItem.Text == "6 Hours Per Column")
                            return weekStartDate.AddDays(7).AddHours(12);
                        else if (ddlDisplayDays.SelectedItem.Text == "1 Hour Per Column")
                            return weekStartDate.AddDays(1);
                        else
                            return weekStartDate.AddDays(31);
                    }
                    else
                    {
                        return weekStartDate.AddDays(1);
                    }
                }
                else
                {
                    if (noofslots == 30)
                    {
                        if (ddlDisplayDays.SelectedItem.Text == "12 Hours Per Column")
                            return weekStartDate.AddDays(-15);
                        else if (ddlDisplayDays.SelectedItem.Text == "8 Hours Per Column")
                            return weekStartDate.AddDays(-10);
                        else if (ddlDisplayDays.SelectedItem.Text == "6 Hours Per Column")
                            return weekStartDate.AddDays(-7).AddHours(12);
                        else if (ddlDisplayDays.SelectedItem.Text == "1 Hour Per Column")
                            return weekStartDate.AddDays(-1);
                        else
                            return weekStartDate.AddDays(-31);
                    }
                    else
                    {
                        return weekStartDate.AddDays(-1);
                    }
                }
            }
        }

        public DateTime GetEndDate(DateTime weekStartDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weekStartDate))
            {
                weekStartDate = Convert.ToDateTime(weekStartDate.ToShortDateString());

                if (ddlDisplayDays.SelectedItem.Text == "12 Hours Per Column")
                    return weekStartDate.AddDays(15).AddSeconds(-1);
                else if (ddlDisplayDays.SelectedItem.Text == "8 Hours Per Column")
                    return weekStartDate.AddDays(10).AddSeconds(-1);
                else if (ddlDisplayDays.SelectedItem.Text == "6 Hours Per Column")
                    return weekStartDate.AddDays(7).AddHours(12).AddSeconds(-1);
                else if (ddlDisplayDays.SelectedItem.Text == "1 Hour Per Column")
                    return weekStartDate.AddDays(1).AddHours(6).AddSeconds(-1);
                else
                    return weekStartDate.AddDays(30).AddSeconds(-1);
            }
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadDatePicker1.SelectedDate = DateTime.Today;
                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                        StartDate = DateTime.Today;
                        EndDate = GetEndDate(StartDate);
                        hdnIsTodaysDate.Value = StartDate.ToString();
                        Session["SCSelectedDay"] = StartDate;                        

                        loadTree(false);                        

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerFleetScheduler_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltip(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManagerFleetPlanner.TargetControls.Add(domElementID, id, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerFleetScheduler_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        e.Appointment.Resources.Add(new Resource("Leg", "LegNum", item.LegNum.ToString()));
                        e.Appointment.Resources.Add(new Resource("StartDate", "StartDate", e.Appointment.Start.ToString()));
                        if (DisplayOptions.Planner.BlackWhiteColor)
                        {
                            e.Appointment.BackColor = Color.White;
                            e.Appointment.ForeColor = Color.Black;
                        }
                        else if ((DisplayOptions.Planner.AircraftColors) && (DisplayOptions.Planner.NonReversedColors))
                        {
                            
                            //To give preference to Aircraft Color, if both aircraft and non reversed color is selected
                            if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                            {
                                e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                            }                            
                        }
                        else if ((DisplayOptions.Planner.AircraftColors) && (!DisplayOptions.Planner.NonReversedColors))
                        {
                            
                                if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                                {
                                    e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                    e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                                }
                           
                        }                        
                        else
                        {
                            if ((!DisplayOptions.Planner.AircraftColors) && (!DisplayOptions.Planner.NonReversedColors) && (!DisplayOptions.Planner.BlackWhiteColor))// Reversed colors...
                            {
                                if ((item.RecordType == "T") && (item.LegId != 9999))
                                    SetFlightCategoryReverseColor(e, item);
                                else if ((item.RecordType == "M") || (item.RecordType == "T" && item.LegId == 9999))
                                    SetAircraftDutyReverseColor(e, item);
                                else if (item.RecordType == "C")
                                    SetCrewDutyReverseColor(e, item);                                
                            }
                            else // Non Reversed colors...
                            {
                                if ((item.RecordType == "T") && (item.LegId != 9999))
                                    SetFlightCategoryColor(e, item);
                                else if ((item.RecordType == "M") || (item.RecordType == "T" && item.LegId == 9999))
                                    SetAircraftDutyColor(e, item);
                                else if (item.RecordType == "C")
                                    SetCrewDutyColor(e, item);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerCrewScheduler_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        e.Appointment.Resources.Add(new Resource("Leg", "LegNum", item.LegNum.ToString()));
                        e.Appointment.Resources.Add(new Resource("StartDate", "StartDate", e.Appointment.Start.ToString()));
                        if (DisplayOptions.Planner.BlackWhiteColor)
                        {
                            e.Appointment.BackColor = Color.White;
                            e.Appointment.ForeColor = Color.Black;
                        }
                        else if ((DisplayOptions.Planner.AircraftColors) && (DisplayOptions.Planner.NonReversedColors))
                        {
                            
                            //To give preference to Aircraft Color, if both aircraft and non reversed color is selected
                            if (DisplayOptions.Planner.AircraftColors && item.TailNum == null && item.RecordType == "C")
                            {
                                SetCrewDutyColor(e, item);
                            }
                            else
                            {
                                if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                                {
                                    e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                    e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                                }
                            }
                        }
                        else if ((DisplayOptions.Planner.AircraftColors) && (!DisplayOptions.Planner.NonReversedColors))
                        {
                           
                            if (DisplayOptions.Planner.AircraftColors && item.TailNum == null && item.RecordType == "C")
                            {
                                SetCrewDutyColor(e, item);
                            }
                            else
                            {
                                if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                                {
                                    e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                    e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                                }
                            }

                        }
                        else
                        {
                            if ((!DisplayOptions.Planner.AircraftColors) && (!DisplayOptions.Planner.NonReversedColors) && (!DisplayOptions.Planner.BlackWhiteColor))// Reversed colors...
                            {
                                if ((item.RecordType == "T") && (item.LegId != 9999))
                                    SetFlightCategoryReverseColor(e, item);
                                else if ((item.RecordType == "M"))
                                    SetAircraftDutyReverseColor(e, item);
                                else if ((item.RecordType == "C") || (item.RecordType == "T" && item.LegId == 9999))
                                    SetCrewDutyReverseColor(e, item);   
                            }
                            else // Non Reversed colors...
                            {                                
                                if ((item.RecordType == "T") && (item.LegId != 9999))
                                    SetFlightCategoryColor(e, item);
                                else if ((item.RecordType == "M"))
                                    SetAircraftDutyColor(e, item);
                                else if ((item.RecordType == "C") || (item.RecordType == "T" && item.LegId == 9999))
                                    SetCrewDutyColor(e, item);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerFleetScheduler_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManagerFleetPlanner.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerCrewScheduler_DataBound(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadToolTipManagerCrewPlanner.TargetControls.Clear();
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideToolTip", "hideActiveToolTip();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        private void SetAircraftDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentAircraftDuty = item.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                    }
                }
            }
        }
        private void SetCrewDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentCategory = item.CrewDutyType;
                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }
        private void SetFlightCategoryColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentCategory = item.FlightCategoryID;
                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void SetAircraftDutyReverseColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentAircraftDuty = item.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        e.Appointment.ForeColor = Color.FromName(dutyCode.BackgroundCustomColor);
                    }
                }
            }
        }
        private void SetCrewDutyReverseColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentCategory = item.CrewDutyType;
                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.BackgroundCustomColor);
                    }
                }
            }
        }
        private void SetFlightCategoryReverseColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentCategory = item.FlightCategoryID;
                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.BackgroundCustomColor);
                    }

                }
            }
        }

        protected void RadPlannerCrewScheduler_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible && !IsAppointmentRegisteredForTooltipCrew(e.Appointment))
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                RadToolTipManagerCrewPlanner.TargetControls.Add(domElementID, id, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        private bool IsAppointmentRegisteredForTooltip(Telerik.Web.UI.Appointment apt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(apt))
            {
                foreach (ToolTipTargetControl targetControl in RadToolTipManagerFleetPlanner.TargetControls)
                {
                    if (apt.DomElements.Contains(targetControl.TargetControlID))
                    {
                        return true;
                    }
                }

                return false;
            }
        }
        private bool IsAppointmentRegisteredForTooltipCrew(Telerik.Web.UI.Appointment apt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(apt))
            {
                foreach (ToolTipTargetControl targetControl in RadToolTipManagerCrewPlanner.TargetControls)
                {
                    if (apt.DomElements.Contains(targetControl.TargetControlID))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        protected void RadToolTipManagerFleetPlanner_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int aptId;
                        Telerik.Web.UI.Appointment apt;
                        if (!int.TryParse(e.Value, out aptId)) //The appoitnment is occurrence and FindByID expects a string
                            apt = RadPlannerFleetScheduler.Appointments.FindByID(e.Value);
                        else //The appointment is not occurrence and FindByID expects an int
                            apt = RadPlannerFleetScheduler.Appointments.FindByID(aptId);

                        UCCalendarAppointmentTooltip toolTip = (UCCalendarAppointmentTooltip)LoadControl("UCCalendarAppointmentTooltip.ascx");

                        toolTip.TargetAppointment = apt;

                        e.UpdatePanel.ContentTemplateContainer.Controls.Add(toolTip);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }
        protected void RadToolTipManagerCrewPlanner_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int aptId;
                        Telerik.Web.UI.Appointment apt;
                        if (!int.TryParse(e.Value, out aptId)) //The appoitnment is occurrence and FindByID expects a string
                            apt = RadPlannerCrewScheduler.Appointments.FindByID(e.Value);
                        else //The appointment is not occurrence and FindByID expects an int
                            apt = RadPlannerCrewScheduler.Appointments.FindByID(aptId);

                        UCCalendarAppointmentTooltip toolTip = (UCCalendarAppointmentTooltip)LoadControl("UCCalendarAppointmentTooltip.ascx");

                        toolTip.TargetAppointment = apt;

                        e.UpdatePanel.ContentTemplateContainer.Controls.Add(toolTip);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void ddlDisplayDays_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCPlannerSlot"] = ddlDisplayDays.SelectedValue;
                        RadPlannerCrewScheduler.TimelineView.SlotDuration = TimeSpan.Parse(ddlDisplayDays.SelectedValue);
                        RadPlannerFleetScheduler.TimelineView.SlotDuration = TimeSpan.Parse(ddlDisplayDays.SelectedValue);
                        if (ddlDisplayDays.SelectedValue == "1.00:00:00")
                        {                            
                            RadPlannerFleetScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd";
                            RadPlannerCrewScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd";
                        }
                        else
                        {
                            RadPlannerFleetScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd HH:mm";
                            RadPlannerCrewScheduler.TimelineView.ColumnHeaderDateFormat = "d-ddd HH:mm";
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;
                            hdnIsTodaysDate.Value = StartDate.ToString();
                            loadTree(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerFleetScheduler_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                        if (e.MenuItem.Value == string.Empty)
                        {
                            return;
                        }

                        var appointment = (Telerik.Web.UI.Appointment)e.Appointment;
                        var appointmentNumber = appointment.ID;
                        //get trip by tripNumber
                        Int64 tripId = Convert.ToInt64(appointmentNumber);
                        if (tripId != 0)
                        {
                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                if (tripInfo.EntityList == null || tripInfo.EntityList.Count <= 0)
                                    Response.Redirect(WebConstants.URL.PreflightMain, false);
                                var trip = tripInfo.EntityList[0];
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                {
                                    using (var commonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = commonService.Lock(WebSessionKeys.PreflightMainLock, trip.TripID);
                                        trip.Mode = returnValue.ReturnFlag ? TripActionMode.Edit : TripActionMode.NoChange;
                                    }
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");

                                    if (e.MenuItem.Text == "Add New Trip")// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                                    {
                                        PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                        PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                        Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(radSelectedStartDate.Value));
                                        /// 12/11/2012
                                        ///Changes done for displaying the homebase of Fleet in preflight 
                                        if (trip.Fleet != null)
                                        {

                                            PreflightService.Company HomebaseSample = new PreflightService.Company();
                                            Int64 HomebaseAirportId = 0;
                                            fleetsample.TailNum = trip.Fleet.TailNum;
                                            fleetsample.FleetID = trip.Fleet.FleetID;
                                            Trip.FleetID = fleetsample.FleetID;
                                            if (fleetsample.FleetID != null)
                                            {
                                                Int64 fleetHomebaseId = 0;
                                                Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                    if (FleetList.Count > 0)
                                                    {
                                                        GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                        if (FleetCatalogEntity.HomebaseID != null)
                                                        {
                                                            fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                        }
                                                    }
                                                    var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                    if (CompanyList.Count > 0)
                                                    {
                                                        GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                        if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                        {
                                                            HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                            HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                        }
                                                    }
                                                    var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                    if (AirportList.Count > 0)
                                                    {
                                                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                        if (AirportCatalogEntity.IcaoID != null)
                                                        {
                                                            Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                        }
                                                        if (AirportCatalogEntity.AirportID != null)
                                                        {
                                                            Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                        }
                                                    }

                                                }
                                                Trip.HomebaseID = fleetHomebaseId;
                                                Trip.Company = HomebaseSample;
                                            }
                                        }
                                        Trip.Fleet = fleetsample;

                                        PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();

                                        //Sridhar Fixed for aircraft type code
                                        using (MasterCatalogServiceClient masterCatalogueServiceClient = new MasterCatalogServiceClient())
                                        {
                                            var masterFleet = masterCatalogueServiceClient.GetFleetByTailNumber(trip.Fleet.TailNum).EntityInfo;

                                            if (masterFleet != null)
                                            {
                                                aircraftSample.AircraftID = masterFleet.AircraftID;
                                                aircraftSample.AircraftCD = masterFleet.AircraftCD;
                                                aircraftSample.AircraftDescription = masterFleet.AircraftDescription;
                                                Trip.AircraftID = aircraftSample.AircraftID;
                                                Trip.Aircraft = aircraftSample;
                                            }
                                        }

                                        if (HomeBaseId != null && HomeBaseId != 0)
                                        {
                                            Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                            Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                            Trip.HomebaseID = HomeBaseId;
                                        }
                                        if (trip.ClientID != null && trip.Client != null)
                                        {
                                            ClientId = trip.ClientID;
                                            ClientCode = trip.Client.ClientCD;
                                        }
                                        if (ClientId != null && ClientId != 0)
                                        {
                                            Trip.ClientID = ClientId;
                                            PreflightService.Client Client = new PreflightService.Client();
                                            Client.ClientCD = ClientCode;
                                            Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                            Trip.Client = Client;
                                        }

                                        Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                        PreflightTripManager.populateCurrentPreflightTrip(trip);
                                        Session.Remove("PreflightException");
                                        Trip.State = TripEntityState.Added;
                                        Trip.Mode = TripActionMode.Edit;
                                        Response.Redirect(e.MenuItem.Value);
                                    }
                                }
                                if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                                {
                                    //trip.Mode = TripActionMode.NoChange;
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;
                                    RadWindow5.NavigateUrl = e.MenuItem.Value + "?IsCalender=1";
                                    RadWindow5.Visible = true;
                                    RadWindow5.VisibleOnPageLoad = true;
                                    RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                }
                                else if (e.MenuItem.Text == "Leg Notes") //  Leg Notes menu selected...
                                {
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;
                                    RadWindow6.NavigateUrl = e.MenuItem.Value + "?IsCalender=1&Leg=" + radSelectedLegNum.Value;
                                    RadWindow6.Visible = true;
                                    RadWindow6.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                {
                                    RedirectToPage(string.Format("{0}?xmlFilename=PRETSOverview.xml&tripnum={1}&tripid={2}", WebConstants.URL.TripSheetReportViewer, Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripNUM)), Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripID))));
                                }
                                else if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                                {// Fleet calendar entry
                                    string strStartDate = radSelectedStartDate.Value;
                                    if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                    {
                                        string tailNumber = trip.Fleet != null ? trip.Fleet.TailNum : null;
                                        RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + tailNumber + "&startDate=" + strStartDate + "&endDate=" + strStartDate; // no need to pass tailNum since it has no resource                                        
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else
                                    {
                                        RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + strStartDate + "&endDate=" + strStartDate; // no need to pass tailNum since it has no resource                                        
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                }
                                else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                                { // Crew calendar entry
                                    RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + null + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start;// no need to pass crewCode since it has no resource
                                    RadWindow4.Visible = true;
                                    RadWindow4.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Preflight")
                                {
                                    if (trip.RecordType == "M")
                                    {
                                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                                        Response.Redirect(WebConstants.URL.PreflightMain, false);
                                    }
                                    else
                                    {
                                        string url = e.MenuItem.Value;
                                        Response.Redirect(url, false);
                                    }
                                }
                                else
                                {
                                    if (trip.TripNUM != null && trip.TripNUM != 0)
                                    {
                                        
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        string url = e.MenuItem.Value;
                                        string menuItemText = e.MenuItem.Text;
                                        radSelectedLegNum.Value = (((!string.IsNullOrWhiteSpace(radSelectedLegNum.Value)) && radSelectedLegNum.Value != "0" )? radSelectedLegNum.Value : "1");
                                        if (menuItemText == "FBO" || menuItemText == "Catering")
                                            url += "&legNo=" + radSelectedLegNum.Value + "#" + menuItemText;
                                        else if (menuItemText == "Crew Hotel" || menuItemText == "PAX Hotel")
                                            url += "&subTab=1&legNo=" + radSelectedLegNum.Value;
                                        else if (menuItemText == "Crew Transport" || menuItemText == "PAX Transport")
                                            url += "&subTab=1&legHotel=1&legNo=" + radSelectedLegNum.Value;
                                        if (Session[WebSessionKeys.CurrentPreflightTrip] == null)
                                        {
                                            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                                            pfViewModel = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.InjectPreflightMainToPreflightTripViewModel(trip, pfViewModel);
                                            Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                                        }
                                        Response.Redirect(url, false);
                                    }
                                    else
                                    {
                                        Session[WebSessionKeys.CurrentPreflightTripMain] = null;
                                        Response.Redirect(WebConstants.URL.PreflightMain, false);
                                    }
                                }
                            }
                        }
                        else
                            Response.Redirect(WebConstants.URL.PreflightMain, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerCrewScheduler_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                        if (e.MenuItem.Value == string.Empty)
                        {
                            return;
                        }
                        if (e.MenuItem.Text == "Add New Trip")// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                            Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(radSelectedStartDate.Value)); // todo: get column date
                            if (HomeBaseId != null && HomeBaseId != 0)
                            {
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = HomeBaseCode;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;
                            }
                            if (ClientId != null && ClientId != 0)
                            {
                                Trip.ClientID = ClientId;
                                PreflightService.Client Client = new PreflightService.Client();
                                Client.ClientCD = ClientCode;
                                Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                Trip.Client = Client;
                            }

                            Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                            Session.Remove("PreflightException");
                            Trip.State = TripEntityState.Added;
                            Trip.Mode = TripActionMode.Edit;
                            Response.Redirect(e.MenuItem.Value);
                            
                        }
                        var appointment = (Telerik.Web.UI.Appointment)e.Appointment;
                        var appointmentNumber = appointment.ID;
                        //get trip by tripNumber
                        Int64 tripId = Convert.ToInt64(appointmentNumber);
                        if (tripId != 0)
                        {
                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                if (tripInfo.EntityList == null || tripInfo.EntityList.Count <= 0)
                                    Response.Redirect(WebConstants.URL.PreflightMain, false);
                                var trip = tripInfo.EntityList[0];
                                if (tripInfo.EntityList != null && tripInfo.EntityList[0] != null)
                                {
                                    using (var commonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = commonService.Lock(WebSessionKeys.PreflightMainLock, trip.TripID);
                                        trip.Mode = returnValue.ReturnFlag ? TripActionMode.Edit : TripActionMode.NoChange;
                                    }
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");
                                }

                                if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                                {
                                    //trip.Mode = TripActionMode.NoChange;
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;
                                    RadWindow5.NavigateUrl = e.MenuItem.Value + "?IsCalender=1";
                                    RadWindow5.Visible = true;
                                    RadWindow5.VisibleOnPageLoad = true;
                                    RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                }
                                else if (e.MenuItem.Text == "Leg Notes") //  Leg Notes menu selected...
                                {
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;
                                    RadWindow6.NavigateUrl = e.MenuItem.Value + "?IsCalender=1&Leg=" + radSelectedLegNum.Value;
                                    RadWindow6.Visible = true;
                                    RadWindow6.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                {
                                    RedirectToPage(string.Format("{0}?xmlFilename=PRETSOverview.xml&tripnum={1}&tripid={2}", WebConstants.URL.TripSheetReportViewer, Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripNUM)), Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripID))));
                                }
                                else if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                                {// Fleet calendar entry
                                    string strStartDate = radSelectedStartDate.Value;
                                    if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                    {
                                        string tailNumber = trip.Fleet != null ? trip.Fleet.TailNum : null;
                                        RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + tailNumber + "&startDate=" + strStartDate + "&endDate=" + strStartDate; // no need to pass tailNum since it has no resource
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    else
                                    {
                                        RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + strStartDate + "&endDate=" + strStartDate; // no need to pass tailNum since it has no resource
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                }
                                else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                                { // Crew calendar entry
                                    RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + appointment.Resources[0].Key + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start;// no need to pass crewCode since it has no resource
                                    RadWindow4.Visible = true;
                                    RadWindow4.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Preflight")
                                {
                                    if (trip.RecordType == "M")
                                    {
                                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                                        Response.Redirect(WebConstants.URL.PreflightMain, false);
                                    }
                                    else
                                    {
                                        string url = e.MenuItem.Value;
                                        Response.Redirect(url, false);
                                    }
                                }
                                else
                                {
                                    if (trip.TripNUM != null && trip.TripNUM != 0)
                                    {
                                        Redirect(e.MenuItem);
                                    }
                                    else
                                    {
                                        Session[WebSessionKeys.CurrentPreflightTripMain] = null;
                                        Response.Redirect(WebConstants.URL.PreflightMain, false);
                                    }
                                }
                                
                            }
                        }
                        else
                            Response.Redirect(WebConstants.URL.PreflightMain, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        private void Redirect(RadMenuItem radMenuItem)
        {
            string url = radMenuItem.Value;
            string menuItemText = radMenuItem.Text;
            if (menuItemText == "FBO" || menuItemText == "Catering")
                url += "&legNo=" + radSelectedLegNum.Value + "#" + menuItemText;
            else if (menuItemText == "Crew Hotel" || menuItemText == "PAX Hotel")
                url += "&subTab=1&legNo=" + radSelectedLegNum.Value;
            else if (menuItemText == "Crew Transport" || menuItemText == "PAX Transport")
                url += "&subTab=1&legHotel=1&legNo=" + radSelectedLegNum.Value;
            Response.Redirect(url, false);
        }

        private FlightPak.Web.Entities.SchedulingCalendar.Appointment SetFleetProperties(List<FleetPlannerCalendarDataResult> calendarData, string timeBase, FleetPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                appointment.TripId = Convert.ToInt64(tripLeg.TripID);
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripStatus = tripLeg.TripStatus;
                appointment.TripNUM = tripLeg.TripNUM.HasValue ? tripLeg.TripNUM.Value : 0;
                appointment.TripNumberString = appointment.TripNUM != 0 ? appointment.TripNUM.ToString() : string.Empty;
                appointment.StartDate = (DateTime)tripLeg.PlannerColumnPeriodStart;
                appointment.EndDate = (DateTime)tripLeg.PlannerColumnPeriodEnd;
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArriveICAO) ? tripLeg.ArriveICAO : string.Empty;
                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.CrewDutyTypeCD = tripLeg.DutyTYPE;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;
                appointment.CrewDutyType = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();
                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg)  //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false;// SHOW details only based on company profile setting - Trip privacy settings
                }

                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }

                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();
                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                appointment.Description = tripLeg.AppointmentDisplay;
                appointment.RecordType = tripLeg.RecordType;
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat);
                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();
                return appointment;
            }
        }

        private FlightPak.Web.Entities.SchedulingCalendar.Appointment SetCrewProperties(List<CrewPlannerCalendarDataResult> calendarData, string timeBase, CrewPlannerCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, tripLeg, appointment))
            {
                appointment.TripId = Convert.ToInt64(tripLeg.TripID);
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripStatus = tripLeg.TripStatus;
                appointment.StartDate = (DateTime)tripLeg.PlannerColumnPeriodStart;
                appointment.EndDate = (DateTime)tripLeg.PlannerColumnPeriodEnd;
                appointment.ArrivalICAOID = !string.IsNullOrEmpty(tripLeg.ArriveICAO) ? tripLeg.ArriveICAO : string.Empty;
                appointment.AircraftDutyID = tripLeg.AircraftDutyID.HasValue ? tripLeg.AircraftDutyID.Value : 0;
                appointment.ClientId = tripLeg.ClientID.HasValue ? tripLeg.ClientID.Value : 0;
                appointment.CrewCD = tripLeg.CrewCD;
                appointment.CrewId = tripLeg.CrewID.HasValue ? tripLeg.CrewID.Value : 0;
                appointment.CrewDutyTypeCD = tripLeg.DutyTYPE;
                appointment.AircraftBackColor = tripLeg.AircraftBackColor;
                appointment.AircraftForeColor = tripLeg.AircraftForeColor;
                appointment.CrewDutyType = tripLeg.CrewDutyID.HasValue ? tripLeg.CrewDutyID.Value : 0;
                appointment.DepartmentId = tripLeg.DepartmentID.HasValue ? tripLeg.DepartmentID.Value : 0;
                appointment.FlightCategoryID = tripLeg.FlightCategoryID.HasValue ? tripLeg.FlightCategoryID.Value : 0;
                appointment.HomeArrivalTime = tripLeg.HomeArrivalDTTM.HasValue ? tripLeg.HomeArrivalDTTM.Value : new DateTime();
                appointment.HomelDepartureTime = tripLeg.HomeDepartureDTTM.HasValue ? tripLeg.HomeDepartureDTTM.Value : new DateTime();
                appointment.IsLog = tripLeg.IsLog.HasValue ? tripLeg.IsLog.Value : false;
                appointment.IsPrivateTrip = tripLeg.IsPrivateTrip.HasValue ? tripLeg.IsPrivateTrip.Value : false;
                appointment.IsPrivateLeg = tripLeg.IsPrivateLeg.HasValue ? tripLeg.IsPrivateLeg.Value : false;

                appointment.ShowAllDetails = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateLeg) //if user has no access to Private trip and current leg is Private one, dont show ALL details..
                {
                    appointment.ShowAllDetails = false;// SHOW details only based on company profile setting - Trip privacy settings
                }

                appointment.ShowPrivateTripMenu = true;
                if (IsTripPrivacyApplied && appointment.IsPrivateTrip) //if user has no access to Private trip 
                {
                    appointment.ShowPrivateTripMenu = false; // hide trip related menu (on private trip) except OutboundInstruction and legNotes
                }

                appointment.LegId = tripLeg.LegID;
                appointment.LegNum = tripLeg.LegNUM.Value;
                appointment.LocalArrivalTime = tripLeg.ArrivalDTTMLocal.HasValue ? tripLeg.ArrivalDTTMLocal.Value : new DateTime();
                appointment.LocalDepartureTime = tripLeg.DepartureDTTMLocal.HasValue ? tripLeg.DepartureDTTMLocal.Value : new DateTime();
                appointment.PassengerRequestorId = tripLeg.PassengerRequestorID.HasValue ? tripLeg.PassengerRequestorID.Value : 0;
                appointment.Description = tripLeg.AppointmentDisplay;
                appointment.RecordType = tripLeg.RecordType;
                appointment.TailNum = tripLeg.TailNum;
                appointment.TripDate = appointment.StartDate.ToString(ApplicationDateFormat);
                appointment.UTCArrivalTime = tripLeg.ArrivalGreenwichDTTM.HasValue ? tripLeg.ArrivalGreenwichDTTM.Value : new DateTime();
                appointment.UTCDepartureTime = tripLeg.DepartureGreenwichDTTM.HasValue ? tripLeg.DepartureGreenwichDTTM.Value : new DateTime();
                return appointment;
            }
        }


        protected void RadPlannerCrewScheduler_OnTimeSlotContextMenuItemClicked(object sender, TimeSlotContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                        RadMenuItem item = e.MenuItem.Parent as RadMenuItem;
                        if (item != null && (item.Text == "Quick Crew Calendar Entry"))
                        {
                            if (!string.IsNullOrEmpty(e.MenuItem.Value))
                            {
                                FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient();
                                var objRetVal = ObjService.GetCrewDutyTypeList().EntityList.Where(x => x.DutyTypeCD.Trim().ToUpper() == (e.MenuItem.Value.ToUpper())).ToList();
                                if (objRetVal.Count() > 0)
                                {
                                    string CrewDutyDescription = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyTypesDescription.ToString();
                                    string CrewDutyCode = e.MenuItem.Value;
                                    DateTime Sdate = e.TimeSlot.Start;
                                    if (!String.IsNullOrWhiteSpace(objRetVal[0].DutyStartTM))
                                    {
                                        string StartTime = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyStartTM.ToString();
                                        Sdate = Sdate.AddHours(Convert.ToInt16(StartTime.Substring(0, 2)));
                                        Sdate = Sdate.AddMinutes(Convert.ToInt16(StartTime.Substring(3, 2)));
                                    }
                                    DateTime EDate = e.TimeSlot.Start;
                                    if (!String.IsNullOrWhiteSpace(objRetVal[0].DutyEndTM))
                                    {
                                        string EndTime = ((FlightPak.Web.FlightPakMasterService.CrewDutyType)objRetVal[0]).DutyEndTM.ToString();
                                        EDate = EDate.AddHours(Convert.ToInt16(EndTime.Substring(0, 2)));
                                        EDate = EDate.AddMinutes(Convert.ToInt16(EndTime.Substring(3, 2)));
                                    }

                                    string crewcode = e.TimeSlot.Resource.Key.ToString();
                                    string CrewID = "";
                                    var objRetValCrewID = ObjService.GetCrewList().EntityList.Where(x => x.CrewCD.Trim().ToUpper() == (crewcode.Trim().ToUpper())).ToList();
                                    if (objRetValCrewID.Count() > 0)
                                        CrewID = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetValCrewID[0]).CrewID.ToString();

                                    //----------------------
                                    PreflightMain Trip = new PreflightMain();
                                    Trip.CrewID = Convert.ToInt64(CrewID);
                                    Trip.FleetID = null;
                                    Trip.Notes = CrewDutyDescription;
                                    Trip.HomebaseID = Convert.ToInt64(HomeBaseId);
                                    Trip.RecordType = "C";
                                    Trip.PreviousNUM = Convert.ToInt64("0");
                                    Trip.IsDeleted = false;
                                    Trip.ClientID = ClientId;
                                    Trip.LastUpdUID = UserPrincipal.Identity._name;
                                    Trip.LastUpdTS = System.DateTime.UtcNow;

                                    PreflightLeg Leg = new PreflightLeg();
                                    Leg.LegNUM = Convert.ToInt64("1");
                                    string timebase = FilterOptions.TimeBase.ToString();

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        if (timebase.ToUpper() == "LOCAL")
                                        {
                                            Leg.DepartureDTTMLocal = Sdate;
                                            Leg.ArrivalDTTMLocal = EDate;
                                            Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                            Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                            Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                            Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                        }
                                        else if (timebase.ToUpper() == "UTC")
                                        {
                                            Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                            Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                            Leg.DepartureGreenwichDTTM = Sdate;
                                            Leg.ArrivalGreenwichDTTM = EDate;
                                            Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                            Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                        }
                                        else
                                        {
                                            Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                            Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                            Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                            Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                            Leg.HomeDepartureDTTM = Sdate;
                                            Leg.HomeArrivalDTTM = EDate;
                                        }
                                    }

                                    Leg.DepartICAOID = HomeAirportId;
                                    Leg.ArriveICAOID = HomeAirportId;
                                    Leg.DutyTYPE = CrewDutyCode;
                                    Leg.IsDeleted = false;
                                    Leg.ClientID = ClientId;
                                    Leg.LastUpdUID = UserPrincipal.Identity._name;
                                    Leg.LastUpdTS = System.DateTime.UtcNow;

                                    PreflightCrewList Crew = new PreflightCrewList();
                                    Crew.CrewID = Convert.ToInt64(CrewID);
                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjService1 = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal1 = ObjService1.GetCrewList().EntityList.Where(x => x.CrewID == Convert.ToInt64(CrewID)).ToList();
                                        Crew.CrewFirstName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal1[0]).FirstName.ToString();
                                        Crew.CrewLastName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal1[0]).LastName.ToString();
                                        Crew.CrewMiddleName = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)objRetVal1[0]).MiddleInitial.ToString();
                                    }

                                    Crew.IsDeleted = false;
                                    Crew.LastUpdUID = UserPrincipal.Identity._name;
                                    Crew.LastUpdTS = System.DateTime.UtcNow;
                                    Trip.State = TripEntityState.Added;
                                    Leg.State = TripEntityState.Added;
                                    Crew.State = TripEntityState.Added;
                                    Leg.PreflightCrewLists = new List<PreflightCrewList>();
                                    Leg.PreflightCrewLists.Add(Crew);
                                    Trip.PreflightLegs = new List<PreflightLeg>();
                                    Trip.PreflightLegs.Add(Leg);
                                    //---------------------
                                    using (PreflightServiceClient Service = new PreflightServiceClient())
                                    {
                                        var ReturnValue = Service.Add(Trip);
                                        if (ReturnValue.ReturnFlag == true)
                                        {
                                            loadTree(false);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                            {
                                RadWindow5.NavigateUrl = e.MenuItem.Value;
                                RadWindow5.Visible = true;
                                RadWindow5.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                            {// Fleet calendar entry

                                RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + e.TimeSlot.Resource.Key.ToString() + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.MenuItem.Value == "QuickCrewEntry") // quick crew entry main menu item selected...
                            {
                                return;
                            }
                            if (e.MenuItem.Value == "../PreFlightMain.aspx") // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                            {
                                PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(e.TimeSlot.Start); 

                                if (HomeBaseId != null && HomeBaseId != 0)
                                {
                                    /// 12/11/2012
                                    ///Changes done for displaying the homebase of Fleet in preflight 
                                    PreflightService.Company HomebaseSample = new PreflightService.Company();
                                    HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                    HomebaseSample.BaseDescription = HomeBaseCode;
                                    Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                    Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                    Trip.HomebaseID = HomeBaseId;
                                    Trip.Company = HomebaseSample;
                                }
                                if (ClientId != null && ClientId != 0)
                                {
                                    Trip.ClientID = ClientId;
                                    PreflightService.Client Client = new PreflightService.Client();
                                    Client.ClientCD = ClientCode;
                                    Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                    Trip.Client = Client;
                                }

                                if (e.MenuItem.Text == "Add New Trip")
                                {
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                    Session.Remove("PreflightException");
                                    Trip.State = TripEntityState.Added;
                                    Trip.Mode = TripActionMode.Edit;
                                }
                                else if (Trip.TripNUM != null && Trip.TripNUM != 0)
                                {
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                    Session.Remove("PreflightException");
                                    Trip.State = TripEntityState.Added;
                                    Trip.Mode = TripActionMode.Edit;
                                }
                                else
                                {
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = null;
                                    Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                }

                                Response.Redirect(e.MenuItem.Value);
                            }
                            if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                            }
                            else
                            {
                                Response.Redirect(e.MenuItem.Value);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerFleetScheduler_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                        RadMenuItem item = e.MenuItem.Parent as RadMenuItem;

                        if (item != null && (item.Text == "Quick Fleet Calendar Entry"))
                        {
                            if (!string.IsNullOrEmpty(e.MenuItem.Value))
                            {
                                FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient();
                                var objRetVal = base.scWrapper.aircraftDuty.EntityList.Where(x => x.AircraftDutyCD.Trim().ToUpper() == (e.MenuItem.Value.Trim().ToUpper())).ToList();
                                if (objRetVal.Count() > 0)
                                {
                                    string AircraftDutyDescription = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[0]).AircraftDutyDescription.ToString();
                                    string AircraftDutyCode = e.MenuItem.Value;
                                    DateTime Sdate = e.TimeSlot.Start;
                                    string StartTime = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[0]).DefaultStartTM.ToString();
                                    Sdate = Sdate.AddHours(Convert.ToInt16(StartTime.Substring(0, 2)));
                                    Sdate = Sdate.AddMinutes(Convert.ToInt16(StartTime.Substring(3, 2)));
                                    DateTime EDate = e.TimeSlot.Start;
                                    string EndTime = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[0]).DefualtEndTM.ToString();
                                    EDate = EDate.AddHours(Convert.ToInt16(EndTime.Substring(0, 2)));
                                    EDate = EDate.AddMinutes(Convert.ToInt16(EndTime.Substring(3, 2)));

                                    string tailnum = e.TimeSlot.Resource.Key.ToString();
                                    var objRetfleetVal = ObjService.GetFleetProfileList().EntityList.Where(x => x.TailNum.Trim().ToUpper() == (tailnum.Trim().ToUpper())).ToList();
                                    Int64 fleetid = Convert.ToInt64(objRetfleetVal[0].FleetID);

                                    PreflightMain Trip = new PreflightMain();
                                    Trip.FleetID = fleetid;
                                    Trip.Notes = AircraftDutyDescription;
                                    Trip.HomebaseID = Convert.ToInt64(HomeBaseId);
                                    Trip.RecordType = "M";
                                    Trip.IsDeleted = false;
                                    Trip.ClientID = ClientId;
                                    Trip.LastUpdUID = UserPrincipal.Identity._name;
                                    Trip.LastUpdTS = System.DateTime.UtcNow;
                                    PreflightLeg Leg = new PreflightLeg();
                                    Leg.LegNUM = Convert.ToInt64("1");
                                    string timebase = FilterOptions.TimeBase.ToString();

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        if (timebase.ToUpper() == "LOCAL")
                                        {
                                            Leg.DepartureDTTMLocal = Sdate;
                                            Leg.ArrivalDTTMLocal = EDate;
                                            Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                            Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                            Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                            Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                        }
                                        else if (timebase.ToUpper() == "UTC")
                                        {
                                            Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                            Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                            Leg.DepartureGreenwichDTTM = Sdate;
                                            Leg.ArrivalGreenwichDTTM = EDate;
                                            Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                            Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                        }
                                        else
                                        {
                                            Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                            Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                            Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                            Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                            Leg.HomeDepartureDTTM = Sdate;
                                            Leg.HomeArrivalDTTM = EDate;
                                        }
                                    }

                                    Leg.DepartICAOID = HomeAirportId;
                                    Leg.ArriveICAOID = HomeAirportId;
                                    Leg.DutyTYPE = AircraftDutyCode;
                                    Leg.IsDeleted = false;
                                    Leg.ClientID = ClientId;
                                    Leg.LastUpdUID = UserPrincipal.Identity._name;
                                    Leg.LastUpdTS = System.DateTime.UtcNow;
                                    Trip.State = TripEntityState.Added;
                                    Leg.State = TripEntityState.Added;
                                    Trip.PreflightLegs = new List<PreflightLeg>();
                                    Trip.PreflightLegs.Add(Leg);

                                    using (PreflightServiceClient Service = new PreflightServiceClient())
                                    {
                                        var ReturnValue = Service.Add(Trip);
                                        if (ReturnValue.ReturnFlag == true)
                                        {

                                            loadTree(false);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                            {
                                RadWindow5.NavigateUrl = e.MenuItem.Value;
                                RadWindow5.Visible = true;
                                RadWindow5.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                var tripnum = string.Empty;
                                Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                            }
                            else if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                            {// Fleet calendar entry

                                RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + e.TimeSlot.Resource.Key.ToString() + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + null + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.MenuItem.Value == "QuickFleetEntry") // quick fleet entry main menu item selected...
                            {
                                return;
                            }
                            if (e.MenuItem.Value == "../PreFlightMain.aspx") // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                            {
                                PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(e.TimeSlot.Start); 

                                string tailnum = e.TimeSlot.Resource.Key.ToString();
                                using (MasterCatalogServiceClient masterCatalogueServiceClient = new MasterCatalogServiceClient())
                                {
                                    var masterFleet = masterCatalogueServiceClient.GetFleetByTailNumber(tailnum).EntityInfo;

                                    if (masterFleet != null)
                                    {
                                        fleetsample.TailNum = tailnum;
                                        fleetsample.FleetID = masterFleet.FleetID;
                                        Trip.Fleet = fleetsample;
                                        Trip.FleetID = fleetsample.FleetID;

                                        if (Trip.FleetID != null)
                                        {
                                            PreflightService.Company HomebaseSample = new PreflightService.Company();
                                            Int64 HomebaseAirportId = 0;
                                            if (fleetsample.FleetID != null)
                                            {
                                                Int64 fleetHomebaseId = 0;
                                                Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                    if (FleetList.Count > 0)
                                                    {
                                                        GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                        if (FleetCatalogEntity.HomebaseID != null)
                                                        {
                                                            fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                            fleetsample.TypeDescription = FleetCatalogEntity.AircraftDescription;
                                                        }
                                                    }
                                                    var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                    if (CompanyList.Count > 0)
                                                    {
                                                        GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                        if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                        {
                                                            HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                            HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                        }
                                                    }
                                                    var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                    if (AirportList.Count > 0)
                                                    {
                                                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                        if (AirportCatalogEntity.IcaoID != null)
                                                        {
                                                            Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                        }
                                                        if (AirportCatalogEntity.AirportID != null)
                                                        {
                                                            Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                        }
                                                    }

                                                }
                                                Trip.HomebaseID = fleetHomebaseId;
                                                Trip.Company = HomebaseSample;
                                            }
                                        }
                                        Trip.Fleet = fleetsample;

                                        PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();
                                        aircraftSample.AircraftID = masterFleet.AircraftID;
                                        aircraftSample.AircraftCD = masterFleet.AircraftCD;
                                        aircraftSample.AircraftDescription = masterFleet.AircraftDescription;
                                        Trip.AircraftID = aircraftSample.AircraftID;
                                        Trip.Aircraft = aircraftSample;
                                    }
                                }

                                if (HomeBaseId != null && HomeBaseId != 0 && Trip.HomebaseID <= 0)
                                {
                                    /// 12/11/2012
                                    ///Changes done for displaying the homebase of login user
                                    PreflightService.Company HomebaseSample = new PreflightService.Company();
                                    HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                    HomebaseSample.BaseDescription = HomeBaseCode;
                                    Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                    Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                    Trip.HomebaseID = HomeBaseId;
                                    Trip.Company = HomebaseSample;
                                }
                                if (ClientId != null && ClientId != 0)
                                {
                                    Trip.ClientID = ClientId;
                                    PreflightService.Client Client = new PreflightService.Client();
                                    Client.ClientCD = ClientCode;
                                    Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                    Trip.Client = Client;
                                }

                                if (e.MenuItem.Text == "Add New Trip")
                                {
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                    Session.Remove("PreflightException");
                                    Trip.State = TripEntityState.Added;
                                    Trip.Mode = TripActionMode.Edit;
                                    Response.Redirect(e.MenuItem.Value);
                                }
                                else if (Trip.TripNUM != null && Trip.TripNUM != 0)
                                {
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                    Session.Remove("PreflightException");
                                    Trip.State = TripEntityState.Added;
                                    Trip.Mode = TripActionMode.Edit;
                                    Response.Redirect(e.MenuItem.Value);
                                }
                                else
                                {
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = null;
                                    Session[WebSessionKeys.CurrentPreflightTrip] = null;

                                    Response.Redirect(WebConstants.URL.PreflightMain, false);
                                }
                            }
                            else
                            {
                                Response.Redirect(e.MenuItem.Value);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.Planner);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.Planner);
                }
            }
        }

        protected void RadPlannerFleetScheduler_OnAppointmentClick(object sender, SchedulerEventArgs e)
        {
            FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
            if ((item != null) && (item.RecordType == "T") && (chkCrewavailability.Checked == true))
            {
                var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                Session["SelectedTripID"] = item.TripId;
                Session["CrewCodes"] = item.CrewCodes;
                IList<RadTreeNode> nodeCollection = CrewTreeView.CheckedNodes;
                if (nodeCollection.Count == 0)
                {
                    CrewTreeView.Nodes.Clear();
                    BindCrewTree(inputFromCrewTree);
                }

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    Collection<string> InputFromCrewTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                    {
                        InputFromCrewTree.Add(s.ChildNodeId);
                    }
                    BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Planner, InputFromCrewTree);
                }
                RadPlannerCrewScheduler.SelectedDate = StartDate;
            }

        }

        protected void tbFleetRows_TextChanged(object sender, EventArgs e)
        {
            if (tbFleetRows.Text == "0")
            {

                RadWindowManager1.RadConfirm("Do you want to set the Fleet Rows to 0 ?", "confirmCallBackFn", 350, 30, null, "Planner");
            }
            else
            {
                loadTree(false);
            }
        }

        protected void btnthroughYes_Click(object sender, EventArgs e)
        {
            if (tbFleetRows.Text == "0")
            {
                RadPlannerFleetScheduler.Height = 0;
                fleet.Attributes.Add("CssClass", "planner_scheduler");
                crew.Attributes.Add("CssClass", "planner_scheduler");
            }
            loadTree(false);
        }

        protected void chkCrewavailability_CheckedChanged(object sender, EventArgs e)
        {
            loadTree(false);
        }
    }

}
