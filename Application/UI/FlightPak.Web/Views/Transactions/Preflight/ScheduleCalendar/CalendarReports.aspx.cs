﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Text;
using FlightPak.Web.PreflightService;
using System.Drawing;
using FlightPak.Web.UserControls;
using System.Collections.ObjectModel;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class CalendarReports : System.Web.UI.Page
    {
        private int BusinessFleetTreeCount { get; set; }
        private int BusinessCrewTreeCount { get; set; }
        bool IsDateCheck = true;
        protected void Page_Load(object sender, EventArgs e)
        {            

        }

        protected void RadBusinessWeekScheduler_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
        }

        private void SetAircraftDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

        }

        private void SetFlightCategoryColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

        }

        private void SetCrewDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

        }



        private void BindFleetTripInfo(PreflightServiceClient preflightServiceClient, DateTime businessWeekStartDate, DateTime businessWeekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromFleetTree)
        {
        }

        private void BindCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime businessWeekStartDate, DateTime businessWeekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromCrewTree)
        {

        }

        private void BindFleetTree()
        {

        }



        private void BindCrewTree()
        {

        }

        protected DateTime GetBusinessWeekStartDate(DateTime selectedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedDate))
            {
                return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1);
            }
        }

        // To get end day of the Month...
        public static DateTime GetBusinessWeekEndDate(DateTime businessWeekStartDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(businessWeekStartDate))
            {

                return businessWeekStartDate.AddDays(15).AddSeconds(-1);
            }
        }


        protected void RadBusinessWeekScheduler_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
        }



        protected void RadBusinessWeekScheduler_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {
        }
    }
}