﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Text;
using FlightPak.Web.PreflightService;
using System.Drawing;
using FlightPak.Web.UserControls;
using FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar;
using System.Collections.ObjectModel;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Web.Entities.SchedulingCalendar;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.Views.Transactions.Preflight;


namespace FlightPak.Web.Views.Transactions.PreFlight.ScheduleCalendar
{

    public partial class SchedulingCalendar : ScheduleCalendarBase
    {

        bool IsDateCheck = true;
        string prevPage = "";
        private string SelectedFleetCrewIDs = string.Empty;
        
        protected override void OnPreInit(EventArgs e)
        {
            if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Contains("?FV"))
            {
                this.MasterPageFile = "~/Framework/Masters/SiteFV.master";
            }
            else
            { this.MasterPageFile = "~/Framework/Masters/Site.master"; }

        }

        
         
        protected void Page_Load(object sender, EventArgs e)
        {
            string parameter = Request["__EVENTARGUMENT"];
            string target = Request["__EVENTTARGET"];
            if (target=="SeeMore")
            {
                Telerik.Web.UI.Appointment app = new Telerik.Web.UI.Appointment();
                Telerik.Web.UI.RadMenuItem menu = new Telerik.Web.UI.RadMenuItem();
                app.ID = parameter;
                menu.Value = "../PreFlightCrewInfo.aspx?seltab=Crew";
                Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs evnt = new AppointmentContextMenuItemClickedEventArgs(app, menu);
                RadWeeklyScheduler_OnAppointmentContextMenuItemClicked(null, evnt);
            }
            bool isDisplayTripInfo = chkDisplayTripInformation.Checked;
            if (isDisplayTripInfo)
                RadWeeklyScheduler.Height = 349;
            else
                RadWeeklyScheduler.Height = 496;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;

                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        RadWeeklyScheduler.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;

                        if (!Page.IsPostBack)
                        {
                            //Retains the standbycrew check box when moved to other view
                            if (Session["chkstandbycrew"] != null && Convert.ToString(Session["chkstandbycrew"]) == "true")
                                chkStandByCrew.Checked = true;
                            else
                                chkStandByCrew.Checked = false;
                            // check / uncheck default view check box...
                            //To reduce service call - Vishwa
                            FilterCriteria.userSettingAvailable = base.scWrapper.userSettingAvailable;
                            FilterCriteria.retriveSystemDefaultSettings = base.scWrapper.retrieveSystemDefaultSettings;
                            //End
                            if (DefaultView == ModuleNameConstants.Preflight.WeeklyFleet)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }
                            var selectedDay = DateTime.Today;
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }

                            StartDate = selectedDay;
                            EndDate = GetWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            Session.Remove("SCNextCount");
                            Session.Remove("SCPreviousCount");
                            Session.Remove("SCLastCount");
                            Session.Remove("SCFirstCount");

                            loadTree(false);

                            

                        }
                        else
                        {
                            CheckValidDate();
                            EndDate = GetWeekEndDate(StartDate);
                            loadTree(false);
                        }

                        //-- Start: Tooltip Integration  --// 
                        //-- End: Tooltip Integration  --//

                    }, FlightPak.Common.Constants.Policy.UILayer);


                    if (Request.QueryString["FV"] != null)
                    {
                        Page.Header.Controls.Add(new LiteralControl(@"<style type='text/css'>
                                 #RAD_SPLITTER_PANE_CONTENT_ctl00_MainContent_LeftPane {
                                 width:100%!important;
                                 }
                                #ctl00_MainContent_RadSplitter1 {   
                                width:100%!important;
                                }
                            </style>"
                                        ));


                        ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "myFunction();", true);
                    }

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyFleet);
                }
            }

         }
        /// <summary>
        /// To Validate Date 
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            StartDate = String.IsNullOrWhiteSpace(Convert.ToString(Session["SCSelectedDay"]))
                ? DateTime.Today
                : (DateTime) Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            IsDateCheck = false;
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                IsDateCheck = false;
                return true;
            }
            EndDate = GetWeekEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

            // avoid duplicate calls for loadTree();
            string id = Page.Request.Params["__EVENTTARGET"];

            if (id == null || (id != string.Empty && (id.Contains("hdnPostBack") || id.Contains("chkDisplayTripInformation")))) //post back event is triggered on close of entry popups / refresh icon.
            {
                loadTree(false);
            }
            return IsDateCheck;
        }

        protected void DisableQuickEntryContextMenu()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Fleet Calendar Entry").Enabled = false;
                RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Crew Calendar Entry").Enabled = false;
            }
        }

        protected void BindQuickFleetContextMenu()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Crew Calendar Entry").Enabled = false;

                //To check add access and prevent user...
                if (!IsAuthorized(Permission.Preflight.AddFleetCalendarEntry))
                {
                    RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Fleet Calendar Entry").Enabled = false;
                    return;
                }
                RadMenuItem FleetMenuItem = RadWeeklyScheduler.TimeSlotContextMenus[0].FindItemByText("Quick Fleet Calendar Entry");


                var objRetVal = scWrapper.aircraftDuty.EntityList.Where(x => x.IsCalendarEntry == true).ToList();
                if (objRetVal.Count() <= 0)
                {
                    FleetMenuItem.Enabled = false;
                }
                else
                {
                    FleetMenuItem.Items.Clear();
                    for (int i = 0; i <= objRetVal.Count() - 1; i++)
                    {
                        RadMenuItem fleetduty = new RadMenuItem();
                        fleetduty.Text = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[i]).AircraftDutyDescription.ToString();
                        fleetduty.Value = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[i]).AircraftDutyCD.ToString();
                        FleetMenuItem.Items.Add(fleetduty);
                    }
                    FleetMenuItem.Enabled = true;
                }
            }
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                        StartDate = DateTime.Today;
                        EndDate = GetWeekEndDate(StartDate);

                        lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                        Session["SCSelectedDay"] = StartDate;

                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        protected void chkStandByCrew_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                          {
                              using (var commonServiceClient = new CommonServiceClient())
                              {
                                  //Retains the standbycrew check box when moved to other view
                                  if (chkStandByCrew.Checked)
                                      Session["chkstandbycrew"] = "true";
                                  else
                                      Session["chkstandbycrew"] = "false";

                                  loadTree(false);
                              }
                          }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetWeekEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;

                            loadTree(false);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                //fleet trips list
                if (!Page.IsPostBack)
                {
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();                    

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                    }                    
                    BindFleetTree(Fleetlst);                    
                }
                // resource type
                if (radFleet.Checked)
                {
                    BindQuickFleetContextMenu();//Quick Fleet Entry Sub Context Menu Creation
                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                    chkStandByCrew.Visible = true;
                    radSelectedView.Value = ModuleNameConstants.Preflight.WeeklyFleet;

                    using (var preflightServiceClient = new PreflightServiceClient())
                    {
                        Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CrewIDs = FleetCrew[1];

                            if (!string.IsNullOrEmpty(FleetCrew[1]))
                            {
                                string[] Crewids = FleetCrew[1].Split(',');
                                foreach (string cIDs in Crewids)
                                {
                                    string[] ParentChild = cIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                }
                            } 
                        }
                        if (IsSave)
                        {
                            if (GetCheckedTreeNodes(FleetTreeView).Count == 0)
                            {
                                FleetTreeView.CheckAllNodes();
                            }  
                            var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);                           
                            //To get the distinct values
                            BindFleetTree(selectedFleetIDs);
                            List<string> distinct = selectedFleetIDs.Select(node => node.ChildNodeId).Distinct().ToList();
                            string strTest = string.Join(",", distinct.ToArray());
                            Session["FleetIDs"] = strTest;
                            objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet, selectedFleetIDs, Crewlst, null, false);
                            EndDate = GetWeekEndDate(StartDate);
                            Collection<string> SelectedFleetIDs = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in selectedFleetIDs)
                            {
                                SelectedFleetIDs.Add(s.ChildNodeId);
                            }
                            BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.WeeklyFleet, SelectedFleetIDs);
                        }
                        else
                        {
                             if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                             {
                                string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                                var FleetIDs = FleetCrew[0];
                                var CrewIDs = FleetCrew[1];

                                if (string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0)
                                {
                                    if (FleetTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0)
                                    {
                                        FleetTreeView.CheckAllNodes();
                                    }
                                }

                                Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();

                                if (!string.IsNullOrEmpty(FleetCrew[0]))
                                {
                                    string[] Fleetids = FleetCrew[0].Split(',');
                                    foreach (string fIDs in Fleetids)
                                    {
                                        string[] ParentChild = fIDs.Split('$');
                                        if (ParentChild.Length > 1)
                                            Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                        else
                                            Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                    }
                                }
                                else
                                    Fleetlst = GetCheckedTreeNodes(FleetTreeView);


                                if ((!string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() > 0) || (Fleetlst.Count > 0))
                                {
                                    
                                    RadWeeklyScheduler.SelectedDate = StartDate;
                                    Collection<string> fleetlst = new Collection<string>();
                                    foreach (TreeviewParentChildNodePair s in Fleetlst)
                                    {
                                        fleetlst.Add(s.ChildNodeId);
                                    }
                                    BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.WeeklyFleet, fleetlst);
                                    List<string> distinct = Fleetlst.Select(node => node.ChildNodeId).Distinct().ToList();
                                    string strTest = string.Join(",", distinct.ToArray());
                                    Session["FleetIDs"] = strTest;

                                }
                            }
                            /*if (FleetTreeView.CheckedNodes.Count == 0 && FleetTreeView.Visible)
                        {
                            FleetTreeView.CheckAllNodes();
                        }

                        var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);

                        BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.WeeklyFleet, inputFromFleetTree);

                        RadWeeklyScheduler.SelectedDate = StartDate;*/
                        }

                        RadPanelBar2.FindItemByText("Fleet").Visible = true;
                        RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                        RadPanelBar2.FindItemByText("Crew").Visible = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;                       
                    }
                }
                else if (radCrew.Checked)
                {
                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;
                    Response.Redirect("WeeklyCrew.aspx");

                }
                else if (radDetail.Checked)
                {
                    //radSelectedView.Value = "View_WeeklyDetail";
                    Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                    Response.Redirect("WeeklyDetail.aspx");
                }
                else
                {
                    // radSelectedView.Value = "View_Weekly";
                    Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;
                    Response.Redirect("WeeklyMain.aspx");
                }

                this.RadPanelBar2.Items[0].Expanded = false;
                //this.RadPanelBar2.Items[1].Expanded = false;
                this.RadPanelBar2.Items[2].Expanded = true;

                RightPane.Collapsed = true;
            }
        }

        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;
                        //Changes done for the Bug ID - 5828
                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);
                        FleetTreeView.Nodes.Clear();
                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;

                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        // ON click of Apply button...
        protected void btnApply_Click(object sender, EventArgs e)
        {
           using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
           {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;

                            loadTree(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        private void BindFleetTripInfo(PreflightServiceClient preflightServiceClient, DateTime weekStartDate, DateTime weekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromFleetTree)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, weekStartDate, weekEndDate, filterCriteria, displayOptions, viewMode, inputFromFleetTree))
            {
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                if (RadWeeklyScheduler.ResourceTypes.Count == 1)
                {
                    RadWeeklyScheduler.ResourceTypes.Remove(RadWeeklyScheduler.ResourceTypes.First());
                }
                ResourceType fleetResourceType = GetResourceType();              
                if (inputFromFleetTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetFleetCalendarData(weekStartDate, weekEndDate, serviceFilterCriteria, inputFromFleetTree.Distinct().ToList(), false, true, true);

                    Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();                  
                    
                    // by default REcordType, T, M and C with tailNum will be displayed...
                    calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                    if (chkStandByCrew.Checked)
                    {
                        // remove records of type='C' , where IsStandBy flag is not true for crew duty type...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.RecordType == "M" || x.RecordType == "T" || (x.RecordType == "C" && x.IsCrewStandBy == true)).ToList(); // so REcord Type M and T with TailNums are in the filtered list..
                    }

                    // by default All trips + All Entries (fleet and crew) will be retrieved when tailNum is not null...

                    appointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);
                    
                    fleetResourceType.DataSource = GetFleetResourceType(new Collection<Entities.SchedulingCalendar.Appointment>(appointments));

                    RadWeeklyScheduler.ResourceTypes.Add(fleetResourceType);
                    appointments = new Collection<Entities.SchedulingCalendar.Appointment>(appointments.OrderBy(x => x.StartDate).ToList());
                    if (displayOptions.WeeklyFleet.FirstLastLeg)
                    {
                        Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> firstLastLegFleetappColl = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                        var distinctTripNos = appointments.Select(x => x.TripId).Distinct().ToList();
                        // TODO: if distinctTripNos count is greater than 3, Take(n) as per DisplayRows selection...

                        foreach (var tripNo in distinctTripNos)
                        {
                            // build appointment data based on first/last leg display option
                            var groupedLegs = appointments.Where(p => p.TripId == tripNo).ToList();
                            DateTime tstartdate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo).Date;
                            DateTime tenddate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo).Date.AddDays(1).AddSeconds(-1);
                            var firstLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).LastOrDefault();
                            var lastLeg = groupedLegs.OrderByDescending(x => x.LegNum).Where(x => x.LegNum != 0).FirstOrDefault();
                            //var lastLeg1 = groupedLegs.Where(x => x.DepartureDisplayTime.Date <= tenddate).OrderByDescending(x => x.LegNum).First();
                            //var lastLeg1 = groupedLegs.Where(x => x.DepartureDisplayTime.Date <= tenddate && x.LegNum != 0).ToList();
                            //var lastLeg = lastLeg1.OrderByDescending(x => x.LegNum).First();                            
                            foreach (var leg in groupedLegs)
                            {
                                if (leg.LegNum == 0 || leg.LegNum == firstLeg.LegNum || leg.LegNum == lastLeg.LegNum)
                                {
                                    firstLastLegFleetappColl.Add(leg);
                                }
                            }
                        }
                        RadWeeklyScheduler.DataSource = firstLastLegFleetappColl.ToList();
                    }
                    else
                        RadWeeklyScheduler.DataSource = appointments.ToList();

                }
                else
                {
                    RadWeeklyScheduler.DataSource = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                    fleetResourceType.DataSource = GetFleetResourceType(new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>());
                    RadWeeklyScheduler.ResourceTypes.Add(fleetResourceType);
                }
                RadWeeklyScheduler.SelectedDate = weekStartDate;
                RadWeeklyScheduler.DataStartField = "StartDate";
                RadWeeklyScheduler.DataEndField = "EndDate";
                RadWeeklyScheduler.DataSubjectField = "Description";

                RadWeeklyScheduler.DataBind();
            }
        }

        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {
            Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();

                DateTime? FirstdayTripDate = null;
                Int64? FirstdayTripID = null;

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList().OrderBy(x => x.DepartureDisplayTime).ToList();
                    //var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                    foreach (var leg in groupedLegs)
                    {
                       /* if (displayOptions.WeeklyFleet.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);

                            }
                            else if (leg.LegNUM == 0) // RON appointment
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);
                            }
                        }
                        else // No first leg last leg selected
                        {
                            FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                            SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                            appointmentCollection.Add(appointment);*/
                        
                            if (leg.RecordType == "M" || (leg.RecordType == "C" && !string.IsNullOrEmpty(leg.TailNum) && leg.TailNum != "DUMMY")  && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i) : EndDate;

                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                    appointmentCollection.Add(appointment);
                                    i++;
                                }
                            }
                            else
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                                               
                                //if (appointment.RecordType == "T" && (appointment.LegNum == 1 || appointment.IsRONAppointment))
                                if (appointment.RecordType == "T")
                                {
                                    if (appointment.LegNum != 0)
                                    {
                                        FirstdayTripDate = appointment.DepartureDisplayTime.Date;
                                        FirstdayTripID = appointment.TripId;
                                        appointmentCollection.Add(appointment);
                                    }
                                    else if (appointment.IsRONAppointment && FirstdayTripDate != null && FirstdayTripID != null) 
                                    {
                                        if (appointment.DepartureDisplayTime.Date == FirstdayTripDate && appointment.TripId == FirstdayTripID)
                                        {
                                            FirstdayTripDate = null;
                                            FirstdayTripID = null;
                                        }
                                        else
                                        {
                                            //var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && (x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date || x.ArrivalDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                            //var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && (x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date || x.ArrivalDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                            var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && (x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                            var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && (x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                            if (foundItem1 == null && foundItem2 == null)
                                            {
                                                appointmentCollection.Add(appointment);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && (x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date || x.ArrivalDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                        //var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && (x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date || x.ArrivalDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                        var foundItem1 = groupedLegs.Where(x => x.LegNUM > 0 && (x.DepartureDisplayTime.Value.Date == appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                        var foundItem2 = groupedLegs.Where(x => x.LegNUM == 1 && (x.DepartureDisplayTime.Value.Date > appointment.DepartureDisplayTime.Date)).FirstOrDefault();
                                        if (foundItem1 == null && foundItem2 == null)
                                        {
                                            appointmentCollection.Add(appointment);
                                        }
                                    }
                                }
                                 else                                
                                    appointmentCollection.Add(appointment);                                
                            }                                                            
                        }                        
                    }

                //}

            }

            return appointmentCollection;
        }

        public void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);
                appointment.EndDate = appointment.StartDate.AddSeconds(1);
               
                //  build description for appointment based on view selected and display options for rendering
                var options = displayOptions.WeeklyFleet;
                if (tripLeg.RecordType == "T")
                {
                    if (tripLeg.LegNUM == 0) //  dummy appointment
                    {
                        if (options.FirstLastLeg == false)
                        {
                            appointment.Description = "(" + appointment.AircraftDutyCD + ") ";


                            switch (options.DepartArriveInfo)
                            {
                                case DepartArriveInfo.ICAO:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                    break;

                                case DepartArriveInfo.AirportName:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                    break;

                                case DepartArriveInfo.City:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                    break;

                            }

                            var flightNumber = string.Empty;
                            if (options.FlightNumber)
                            {
                                flightNumber = " Flt No.: " + appointment.FlightNum;

                            }

                            appointment.Description = String.Format("{0}{1}{2}{3}", appointment.Description,
                                                                    appointment.ArrivalICAOID, flightNumber,
                                                                    BuildWeeklyFleetOnlyToolTipDescription(appointment,
                                                                                                           displayOptions));
                        }
                    }
                    else // db trips
                    {
                        appointment.Description = BuildWeeklyFleetAppointmentDescription(appointment, calendarData,
                                                                                         displayOptions);
                    }

                }
                else // entries...
                {

                    switch (displayOptions.WeeklyFleet.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;

                    }

                    StringBuilder builder = new StringBuilder();
                    if (appointment.RecordType == "M" || (appointment.RecordType == "C" && !string.IsNullOrEmpty(appointment.TailNum) && appointment.TailNum != "DUMMY"))
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(") ");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                            {
                                builder.Append("(");
                                builder.Append(appointment.CrewDutyTypeCD);
                                builder.Append(") ");
                            }
                        }

                        if (appointment.StartDate == appointment.HomelDepartureTime)
                        {
                            builder.Append(appointment.StartDate.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }
                        builder.Append("</br>");

                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append(appointment.AircraftDutyDescription);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(appointment.CrewDutyTypeDescription))
                            {
                                builder.Append(appointment.CrewDutyTypeDescription);
                            }
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(") ");
                        }

                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append("</br>");
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append(appointment.AircraftDutyDescription);
                        }
                    }                 

                    if (appointment.IsStandByCrew && chkStandByCrew.Checked && !string.IsNullOrEmpty(appointment.CrewCodes))
                    {
                        builder.Append("<br />\n");
                        builder.Append(appointment.CrewCodes);
                        builder.Append(" - ");
                    }
                    builder.Append(appointment.Notes);
                    appointment.Description =String.Format("{0}{1}",builder.ToString(),BuildWeeklyFleetOnlyToolTipDescription(appointment, displayOptions));
                }

                appointment.ViewMode = viewMode;
            }
        }

        protected void RadWeeklyScheduler_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;

                        //set contextmenu id for each appointment..
                        // e.Appointment.ContextMenuID = item.TripId.ToString();
                        e.Appointment.Resources.Add(new Resource("Leg", "LegNum", item.LegNum.ToString()));
                        e.Appointment.Resources.Add(new Resource("TripNum", "TripNum", Convert.ToString(item.TripNUM)));
                        e.Appointment.Resources.Add(new Resource("TailNum", "TailNum", Convert.ToString(item.TailNum)));

                        //Rendition based on red-eye option - if red eye option is not selected, shorten the end date to the start date of the appointment...
                        if (DisplayOptions.WeeklyFleet != null && DisplayOptions.WeeklyFleet.DisplayRedEyeFlightsInContinuation == false && item.RecordType != "M" && item.RecordType != "C")
                        {
                            if (item.ArrivalDisplayTime.Date != item.DepartureDisplayTime.Date)
                            {
                                item.EndDate = item.StartDate;

                            }
                        }
                        
                        // if black and white color not selected - default is off – can only be selected if Aircraft color is not selected; 

                        // if (ModuleNameConstants.Preflight.WeeklyFleet.Equals(item.ViewMode) && DisplayOptions.WeeklyFleet != null)
                        // {
                        if (DisplayOptions.WeeklyFleet.BlackWhiteColor)
                        {
                            e.Appointment.BackColor = Color.White;
                            e.Appointment.ForeColor = Color.Black;
                        }
                        else
                        {
                            if (!DisplayOptions.WeeklyFleet.AircraftColors)
                            {
                                if (item.RecordType == "T")
                                {
                                    if (item.IsRONAppointment)
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category

                                        if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.AircraftDutyCD))
                                        {
                                            var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == item.AircraftDutyCD.Trim()).FirstOrDefault();
                                            if (dutyCode != null)
                                            {
                                                e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        SetFlightCategoryColor(e, item);
                                    }
                                }
                                else if (item.RecordType == "M")
                                {
                                    // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                    SetAircraftDutyColor(e, item);
                                }
                                else if (item.RecordType == "C")
                                {
                                    // set crew duty type color...
                                    SetCrewDutyColor(e, item);
                                    if (e.Appointment.Start == e.Appointment.End)
                                    {

                                    }
                                }
                            }
                            else if (DisplayOptions.WeeklyFleet.AircraftColors && item.TailNum == null && item.RecordType == "C")
                            {
                                SetCrewDutyColor(e, item);
                            }
                            else
                            {
                                if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                                {

                                    e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                    e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                                }
                            }
                        }                                                                    
                        // }
                        e.Appointment.ToolTip = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void RadWeeklyScheduler_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                        if (e.MenuItem.Value == string.Empty)
                        {
                            return;
                        }

                        if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                        {
                            return;
                        }

                        //get trip by tripNumber
                        var appointment = (Telerik.Web.UI.Appointment)e.Appointment;
                        var appointmentNumber = appointment.ID;

                        Int64 tripId = Convert.ToInt64(appointmentNumber);
                        if (tripId != 0)
                        {

                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0)
                                {
                                    var trip = tripInfo.EntityList[0];

                                    //Set the trip.Mode and Trip.State to NoChage for read only                    
                                    trip.Mode = TripActionMode.NoChange;
                                    //trip.Mode = TripActionMode.Edit;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);
                                    Session.Remove("PreflightException");
                                   
                                    if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                                    {
                                        //trip.Mode = TripActionMode.NoChange;                                       
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        RadWindow5.NavigateUrl = e.MenuItem.Value + "?IsCalender=1";
                                        RadWindow5.Visible = true;
                                        RadWindow5.VisibleOnPageLoad = true;
                                        return;
                                    }
                                    
                                    if (e.MenuItem.Text == "Leg Notes") //  Leg Notes menu selected...
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        RadWindow6.NavigateUrl = e.MenuItem.Value + "?IsCalender=1";
                                        RadWindow6.Visible = true;
                                        RadWindow6.VisibleOnPageLoad = true;
                                        return;
                                    }

                                    if (e.MenuItem.Text == "Add New Trip")// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                                    {
                                        PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                        PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                        Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(radSelectedDate.Value));
                                        
                                        /// 12/11/2012
                                        ///Changes done for displaying the homebase of Fleet in preflight 
                                        if (trip.Fleet != null)
                                        {
                                            PreflightService.Company HomebaseSample = new PreflightService.Company();
                                            Int64 HomebaseAirportId = 0;
                                            fleetsample.TailNum = trip.Fleet.TailNum;
                                            fleetsample.FleetID = trip.Fleet.FleetID;
                                            Trip.FleetID = fleetsample.FleetID;
                                            if (fleetsample.FleetID != null)
                                            {
                                                Int64 fleetHomebaseId = 0;
                                                Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                    if (FleetList.Count > 0)
                                                    {
                                                        GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                        if (FleetCatalogEntity.HomebaseID != null)
                                                        {
                                                            fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                            fleetsample.TypeDescription = FleetCatalogEntity.AircraftDescription;
                                                        }
                                                    }
                                                    var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                    if (CompanyList.Count > 0)
                                                    {
                                                        GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                        if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                        {
                                                            HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                            HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                        }
                                                    }
                                                    var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                    if (AirportList.Count > 0)
                                                    {
                                                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                        if (AirportCatalogEntity.IcaoID != null)
                                                        {
                                                            Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                        }
                                                        if (AirportCatalogEntity.AirportID != null)
                                                        {
                                                            Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                        }
                                                    }

                                                }
                                                Trip.HomebaseID = fleetHomebaseId;
                                                Trip.Company = HomebaseSample;
                                            }
                                        }
                                        Trip.Fleet = fleetsample;

                                        PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();

                                        //Sridhar Fixed for aircraft type code
                                        using (MasterCatalogServiceClient masterCatalogueServiceClient = new MasterCatalogServiceClient())
                                        {
                                            var masterFleet = masterCatalogueServiceClient.GetFleetByTailNumber(trip.Fleet.TailNum).EntityInfo;

                                            if (masterFleet != null)
                                            {
                                                aircraftSample.AircraftID = masterFleet.AircraftID;
                                                aircraftSample.AircraftCD = masterFleet.AircraftCD;
                                                aircraftSample.AircraftDescription = masterFleet.AircraftDescription;
                                                Trip.AircraftID = aircraftSample.AircraftID;
                                                Trip.Aircraft = aircraftSample;
                                            }
                                        }


                                        if (HomeBaseId != null && HomeBaseId != 0 && Trip.HomebaseID <= 0)
                                        {
                                            /// 12/11/2012
                                            ///Changes done for displaying the homebase of login user
                                            PreflightService.Company HomebaseSample = new PreflightService.Company();
                                            HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                            HomebaseSample.BaseDescription = BaseDescription;
                                            Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                            Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                            Trip.HomebaseID = HomeBaseId;
                                            Trip.Company = HomebaseSample;
                                        }

                                        if (trip.ClientID != null && trip.Client != null)
                                        {
                                            ClientId = trip.ClientID ;
                                            ClientCode = trip.Client.ClientCD;
                                        }

                                        if (ClientId != null && ClientId != 0)
                                        {
                                            Trip.ClientID = ClientId;
                                            PreflightService.Client Client = new PreflightService.Client();
                                            Client.ClientCD = ClientCode;
                                            Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                            Trip.Client = Client;
                                        }

                                        Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                        Session.Remove("PreflightException");
                                        Trip.State = TripEntityState.Added;
                                        Trip.Mode = TripActionMode.Edit;
                                        Response.Redirect(e.MenuItem.Value);

                                        /*if (Trip.TripNUM != null && Trip.TripNUM != 0)
                                        {
                                            Session["CurrentPreFlightTrip"] = Trip;
                                            Session.Remove("PreflightException");
                                            Trip.State = TripEntityState.Added;
                                            Trip.Mode = TripActionMode.Edit;
                                            Response.Redirect(e.MenuItem.Value);
                                        }                                        
                                        else
                                        {
                                            //Session["CurrentPreFlightTrip"] = null;
                                            Session["CurrentPreFlightTrip"] = Trip;
                                            Session.Remove("PreflightException");
                                            Trip.State = TripEntityState.Added;
                                            Trip.Mode = TripActionMode.Edit;
                                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                        }     */                                                                           
                                    }                                    
                                    else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                    {
                                        if (trip.TripNUM != 0)
                                        {
                                            RedirectToPage(string.Format("{0}?xmlFilename=PRETSOverview.xml&tripnum={1}&tripid={2}", WebConstants.URL.TripSheetReportViewer, Microsoft.Security.Application.Encoder.HtmlEncode(trip.TripNUM.ToString()), Microsoft.Security.Application.Encoder.HtmlEncode(trip.TripID.ToString())));
                                            //Session["Trip"] = trip.TripNUM.ToString();
                                        }
                                        else
                                        {
                                            var tripnum = string.Empty;
                                            Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                                           // Session["Trip"] = "";
                                        }
                                    }
                                    else if (e.MenuItem.Text == "Preflight")
                                    {
                                        if (trip.RecordType == "M")
                                        {
                                            Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                                            Session[WebSessionKeys.CurrentPreflightTrip] = null;

                                            Response.Redirect(WebConstants.URL.PreflightMain, false);
                                        }
                                        else
                                        {
                                            string url = e.MenuItem.Value + "&legNum=" + (string.IsNullOrWhiteSpace(radSelectedLegNum.Value) ? "0" : radSelectedLegNum.Value);
                                            Response.Redirect(url, false);
                                        }
                                    }
                                    else
                                    {
                                        trip.Mode = TripActionMode.Edit;
                                        trip.State = TripEntityState.Modified;
                                        string url = e.MenuItem.Value;
                                        string menuItemText = e.MenuItem.Text;
                                        radSelectedLegNum.Value = (((!string.IsNullOrWhiteSpace(radSelectedLegNum.Value)) && radSelectedLegNum.Value != "0") ? radSelectedLegNum.Value : "1");
                                        if (menuItemText == "FBO" || menuItemText == "Catering")
                                            url += "&legNo=" + radSelectedLegNum.Value + "#" + menuItemText;
                                        else if (menuItemText == "Crew Hotel" || menuItemText == "PAX Hotel")
                                            url += "&subTab=1&legNo=" + radSelectedLegNum.Value;
                                        else if (menuItemText == "Crew Transport" || menuItemText == "PAX Transport")
                                            url += "&subTab=1&legHotel=1&legNo=" + radSelectedLegNum.Value;
                                        if (Session[WebSessionKeys.CurrentPreflightTrip] == null)
                                        {
                                            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                                            pfViewModel = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.InjectPreflightMainToPreflightTripViewModel(trip, pfViewModel);
                                            Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                                        }
                                        Response.Redirect(url, false);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect(WebConstants.URL.PreflightMain, false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        private void SetAircraftDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentAircraftDuty = item.AircraftDutyID;
                        if (AircraftDutyTypes.Count > 0)
                        {
                            var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                            if (dutyCode != null)
                            {
                                e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        private void SetFlightCategoryColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentCategory = item.FlightCategoryID;
                        // Aircraft Color – if on display will use color set in flight category 
                        if (FlightCategories.Count > 0)
                        {
                            var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                            if (categoryCode != null)
                            {
                                e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                                e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        private void SetCrewDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var appointmentCategory = item.CrewDutyType;
                        // Aircraft Color – if on display will use color set in flight category 
                        if (CrewDutyTypes.Count > 0)
                        {
                            var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                            if (categoryCode != null)
                            {
                                e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                                e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);

                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void radCrew_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;
                        Response.Redirect("WeeklyCrew.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }


        }

        protected void radDetail_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                        Response.Redirect("WeeklyDetail.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void radFleet_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;

                        chkStandByCrew.Visible = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void radMain_Checked(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;
                        Response.Redirect("WeeklyMain.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        public List<FleetResourceTypeResult> GetFleetResourceType(Collection<Entities.SchedulingCalendar.Appointment> appointments)
        {

            var fleetResourceTypes = new List<FleetResourceTypeResult>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointments))
            {

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    var fleetIdsFromTree = GetCheckedTreeNodes(FleetTreeView);
                    //if homebaseOnly is true, filter based on HomeBase ID
                    Collection<string> FleetIdsFromTree = new Collection<string>();
                    foreach (TreeviewParentChildNodePair s in fleetIdsFromTree)
                    {
                        FleetIdsFromTree.Add(s.ChildNodeId);
                    }
                    var fleetResources = preflightServiceClient.GetFleetResourceType((FlightPak.Web.PreflightService.VendorOption)FilterOptions.VendorsFilter, FleetIdsFromTree.ToList(), FilterOptions.HomeBaseOnly, FilterOptions.HomeBaseID);
                    if (fleetResources.EntityList != null)
                    {
                        if (DisplayOptions.WeeklyFleet.ActivityOnly)
                        {
                            var distinctFleetIds = appointments.Select(x => x.TailNum).Distinct().ToList();
                            fleetResources.EntityList = fleetResources.EntityList.Where(x => distinctFleetIds.Contains(x.TailNum)).ToList();
                            fleetResourceTypes = fleetResources.EntityList;
                        }
                        else
                        {
                            fleetResourceTypes = fleetResources.EntityList;
                        }

                    }
                }
            }
            return fleetResourceTypes;
        }

        //-- Start: Tooltip Integration --// 

        protected void RadWeeklyScheduler_AppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Appointment.Visible)
                        {
                            string id = e.Appointment.ID.ToString();

                            foreach (string domElementID in e.Appointment.DomElements)
                            {
                                //RadToolTipManager1.TargetControls.Add(domElementID, id, true);
                            }
                        }
                       
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        protected void RadToolTipManager1_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int aptId;

                        Telerik.Web.UI.Appointment apt;
                        if (!int.TryParse(e.Value, out aptId)) //The appoitnment is occurrence and FindByID expects a string
                            apt = RadWeeklyScheduler.Appointments.FindByID(e.Value);
                        else //The appointment is not occurrence and FindByID expects an int
                            apt = RadWeeklyScheduler.Appointments.FindByID(aptId);

                        UCCalendarAppointmentTooltip toolTip = (UCCalendarAppointmentTooltip)LoadControl("UCCalendarAppointmentTooltip.ascx");

                        toolTip.TargetAppointment = apt;

                        e.UpdatePanel.ContentTemplateContainer.Controls.Add(toolTip);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }
        //-- End: Tooltip Integration  --// 

        // description for appointment

        public string BuildWeeklyFleetAppointmentDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, List<FleetCalendarDataResult> fleetCalendarData, DisplayOptions displayOptions)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, fleetCalendarData, displayOptions))
            {
                var description = new StringBuilder();

                var options = displayOptions.WeeklyFleet;
                var departureArrivalInfo = options.DepartArriveInfo;
                bool ShowAllDetails = appointment.ShowAllDetails;
                description.Append(options.ShowTrip ? string.Format("<strong>Trip:</strong>{0}", appointment.TripNUM) : "");
                if (ShowAllDetails)
                {
                    description.Append(options.FlightNumber ? string.Format("<strong> Flt No:</strong>{0}", appointment.FlightNum) : "");
                }
                description.Append(options.ShowTripStatus ? string.Format("({0})<br />\n", appointment.TripStatus) : "<br />\n");
                description.Append(options.ShowTrip ? string.Format("<strong> Leg No.: </strong>{0} ", appointment.LegNum) : "");

                if (ShowAllDetails)
                {
                    description.Append(options.FlightCategory
                                           ? string.Format("<strong>Cat: </strong>{0}<br />\n ", appointment.FlightCategoryCode)
                                           : "<br />\n");
                }
                else
                {
                    description.Append("<br />\n");
                }


                if (options.DepartArriveInfo != null)
                {
                    GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);
                }
                description.Append(" ");
                //Added for Displaying date in radsceduler
                if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date && appointment.RecordType == "T")
                {
                    description.Append("<br />\n");
                    description.Append("* Arrival Date: ");
                    //description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                    description.Append(String.Format("{0:" + ApplicationDateFormat + "}", Convert.ToDateTime(appointment.ArrivalDisplayTime.ToShortDateString().ToString())));
                    description.Append(" *");
                    description.Append("<br />\n");
                    description.Append(" ");
                }


                description.Append("<table class='calendarinfo'><tr><td>");
                if (appointment.ShowAllDetails)
                {
                    //description.Append(options.FlightNumber ? string.Format("<strong>Flt No.: </strong>{0}  </td><td>", appointment.FlightNum) : " </td><td>");
                    //description.Append(options.FlightCategory ? string.Format("<strong>Cat: </strong>{0} </td></tr>", appointment.FlightCategoryCode) : " </td></tr>");
                    description.Append(options.ETE ? string.Format("<tr><td ><strong>ETE: </strong>{0}  ", appointment.ETE) : "<tr><td> ");
                    description.Append(options.CummulativeETE ? string.Format("<strong>Cum ETE: </strong>{0}  </td></tr>", appointment.CumulativeETE) : "</td></tr> ");

                    description.Append(options.TotalETE ? string.Format("<tr><td><strong>Trip ETE: </strong>{0} </td></tr> ", appointment.TotalETE) : "");
                    description.Append(options.Requestor ? string.Format("<tr><td><strong> Req: </strong>{0} </td></tr>", appointment.PassengerRequestorCD) : "");

                    description.Append(options.PaxCount ? string.Format("<tr><td><strong>PAX: </strong>{0} ", appointment.PassengerCount) : "<tr><td> ");
                    description.Append(options.SeatsAvailable ? string.Format("<strong> Seats Avail: </strong>{0} </td></tr>", appointment.ReservationAvailable) : "</td></tr> ");

                    description.Append(options.Department ? string.Format("<tr><td><strong>Dept: </strong>{0} ", appointment.DepartmentCD) : "<tr><td> ");
                    description.Append(options.Authorization ? string.Format("<strong>Auth: </strong>{0}  </td></tr>", appointment.AuthorizationCD) : "</td></tr> ");

                    description.Append(options.TripPurpose ? string.Format("<tr><td colspan='2'><strong>Trip Purp.: </strong>{0} </td></tr>", appointment.TripPurpose) : "<tr><td colspan='2'></td></tr>");
                    description.Append(options.LegPurpose ? string.Format("<tr><td colspan='2'><strong>Leg Purp.: </strong>{0} </td></tr>", appointment.FlightPurpose) : "<tr><td colspan='2'></td></tr>");

                    if (options.Crew)
                    {
                        description.Append(FlightPak.Web.Framework.Helpers.MiscUtils.AppendSeeMoreLink(appointment.CrewCD, appointment.CrewCodes, Convert.ToString(appointment.TripId)));
                    }
                    else
                    {
                        description.Append("<tr><td colspan='2'>");
                    }
                }
                else // show details based on Trip Privacy Settins from company profile
                {

                    description.Append((PrivateTrip.IsShowFlightNumber && options.FlightNumber) ? string.Format("<strong>Flt No.: </strong>{0}  </td><td>", appointment.FlightNum) : " </td><td>");
                    description.Append((PrivateTrip.IsShowFlightCategory && options.FlightCategory) ? string.Format("<strong>Cat: </strong>{0} </td></tr>", appointment.FlightCategoryCode) : " </td></tr>");


                    description.Append((PrivateTrip.IsShowETE && options.ETE) ? string.Format("<tr><td><strong>ETE: </strong>{0}  ", appointment.ETE) : "<tr><td> ");
                    description.Append((PrivateTrip.IsShowCumulativeETE && options.CummulativeETE) ? string.Format("<strong> Cum ETE: </strong>{0}  </td></tr>", appointment.CumulativeETE) : "</td></tr>");

                    description.Append((PrivateTrip.IsShowTotalETE && options.TotalETE) ? string.Format("<tr><td ><strong>Trip ETE: </strong>{0}  </td></tr>", appointment.TotalETE) : "");
                    description.Append((PrivateTrip.IsShowRequestor && options.Requestor) ? string.Format("<tr><td><strong> Req: </strong>{0} </td></tr>", appointment.PassengerRequestorCD) : "");

                    description.Append((PrivateTrip.IsShowPaxCount && options.PaxCount) ? string.Format("<tr><td><strong>PAX: </strong>{0}  ", appointment.PassengerCount) : "<tr><td> ");
                    description.Append((PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable) ? string.Format(" <strong> Seats Avail: </strong>{0} </td></tr>", appointment.ReservationAvailable) : "</td></tr>");

                    description.Append((PrivateTrip.IsShowDepartment && options.Department) ? string.Format("<tr><td><strong>Dept: </strong>{0} ", appointment.DepartmentCD) : " <tr><td>");
                    description.Append((PrivateTrip.IsShowAuthorization && options.Authorization) ? string.Format("<strong>Auth: </strong>{0}  </td></tr>", appointment.AuthorizationCD) : " </td></tr>");

                    description.Append((PrivateTrip.IsShowTripPurpose && options.TripPurpose) ? string.Format("<tr><td colspan='2'><strong>Trip Purp.: </strong>{0} </td></tr>", appointment.TripPurpose) : "<tr><td colspan='2'></td></tr>");
                    description.Append((PrivateTrip.IsShowLegPurpose && options.LegPurpose) ? string.Format("<tr><td colspan='2'><strong>Leg Purp.: </strong>{0} </td></tr>", appointment.FlightPurpose) : "<tr><td colspan='2'></td></tr>");

                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {
                        description.Append(FlightPak.Web.Framework.Helpers.MiscUtils.AppendSeeMoreLink(appointment.CrewCD, appointment.CrewCodes, Convert.ToString(appointment.TripId)));
                    }
                    else
                    {
                        description.Append("<tr><td colspan='2'>");
                    }
                }
                description.Append(" </td></tr></table>");
                return description.ToString();
            }

        }        

        public string BuildWeeklyFleetOnlyToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {
                var options = displayOptions.WeeklyFleet;
                description.Append("<table class='calendarinfo'><tr><td>");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append(" </td></tr></table>");
            }

            return description.ToString();
        }
        
        private void GetDepartureArrivalInfo(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DepartArriveInfo departureArrivalInfo, StringBuilder description)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, departureArrivalInfo, description))
            {

                if (appointment.ShowAllDetails)
                {
                    description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                    description.Append(" ");
                    switch (departureArrivalInfo)
                    {
                        case DepartArriveInfo.ICAO:

                            description.Append(appointment.DepartureICAOID);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                            break;

                        case DepartArriveInfo.AirportName:

                            description.Append(appointment.DepartureAirportName);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                            break;

                        case DepartArriveInfo.City:

                            description.Append(appointment.DepartureCity);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                            break;

                    }
                    description.Append(" ");
                    description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                }
                else // show as trip privacy settings from company profile
                {
                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        description.Append(" ");
                    }

                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (departureArrivalInfo)
                        {
                            case DepartArriveInfo.ICAO:

                                description.Append(appointment.DepartureICAOID);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                                break;

                            case DepartArriveInfo.AirportName:

                                description.Append(appointment.DepartureAirportName);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                                break;

                            case DepartArriveInfo.City:

                                description.Append(appointment.DepartureCity);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                                break;

                        }
                    }

                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (PrivateTrip.IsShowArrivalDepartICAO)
                        {
                            description.Append(" ");
                        }
                        else
                        {
                            description.Append(" | ");
                        }

                        description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                    }
                }
            }
        }

        # region "Navigation bar"
        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCPreviousCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            loadTree(false);


                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void nextButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(1);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCLastCount"] = ++count;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        protected void btnFirst_click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            //DateTime today = DateTime.Today;
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            StartDate = selectedDate.AddDays(-7);
                            EndDate = GetWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }

        }

        #endregion

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.WeeklyFleet);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        protected void RadWeeklyScheduler_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove(WebSessionKeys.CurrentPreflightTripMain);
                        if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                        {
                            return;
                        }

                        if (e.MenuItem.Value == "../PreFlightMain.aspx") // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            string tailnum = e.TimeSlot.Resource.Key.ToString();

                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();

                            using (MasterCatalogServiceClient masterCatalogueServiceClient = new MasterCatalogServiceClient())
                            {
                                var masterFleet = masterCatalogueServiceClient.GetFleetByTailNumber(tailnum).EntityInfo;

                                if (masterFleet != null)
                                {
                                    fleetsample.TailNum = tailnum;
                                    fleetsample.FleetID = masterFleet.FleetID;
                                    /// 12/11/2012
                                    ///Changes done for displaying the homebase of Fleet in preflight                                   
                                    Trip.Fleet = fleetsample;
                                    Trip.FleetID = fleetsample.FleetID;
                                    if (Trip.FleetID != null)
                                    {
                                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                                        Int64 HomebaseAirportId = 0;
                                        if (fleetsample.FleetID != null)
                                        {
                                            Int64 fleetHomebaseId = 0;
                                            Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                if (FleetList.Count > 0)
                                                {
                                                    GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                    if (FleetCatalogEntity.HomebaseID != null)
                                                    {
                                                        fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                        fleetsample.TypeDescription = FleetCatalogEntity.AircraftDescription;
                                                    }
                                                }
                                                var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                if (CompanyList.Count > 0)
                                                {
                                                    GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                    if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                    {
                                                        HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                        HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                    }
                                                }
                                                var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                if (AirportList.Count > 0)
                                                {
                                                    GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                    if (AirportCatalogEntity.IcaoID != null)
                                                    {
                                                        Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                    }
                                                    if (AirportCatalogEntity.AirportID != null)
                                                    {
                                                        Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                    }
                                                }

                                            }
                                            Trip.HomebaseID = fleetHomebaseId;
                                            Trip.Company = HomebaseSample;
                                        }
                                    }
                                    Trip.Fleet = fleetsample;


                                    PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();
                                    aircraftSample.AircraftID = masterFleet.AircraftID;
                                    aircraftSample.AircraftCD = masterFleet.AircraftCD;
                                    aircraftSample.AircraftDescription = masterFleet.AircraftDescription;
                                    Trip.AircraftID = aircraftSample.AircraftID;

                                    Trip.Aircraft = aircraftSample;

                                }
                            }
                            Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(e.TimeSlot.Start);

                            if (HomeBaseId != null && HomeBaseId != 0 && Trip.HomebaseID <= 0)
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of login user
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = HomeBaseCode;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;


                            }
                            if (ClientId != null && ClientId != 0)
                            {
                                Trip.ClientID = ClientId;
                                PreflightService.Client Client = new PreflightService.Client();
                                Client.ClientCD = ClientCode;
                                Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                Trip.Client = Client;
                            }

                            if (e.MenuItem.Text == "Add New Trip")
                            {
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                                Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                Session.Remove("PreflightException");
                            }
                            else if (Trip.TripNUM != null && Trip.TripNUM != 0)
                            {
                                Session[WebSessionKeys.CurrentPreflightTripMain] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                            }
                            else
                            {
                                Session[WebSessionKeys.CurrentPreflightTripMain] = null;
                                Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                Session.Remove("PreflightException");
                            }                          
                            Response.Redirect(e.MenuItem.Value);
                        }

                        RadMenuItem item = e.MenuItem.Parent as RadMenuItem;
                        if (item != null && (item.Text == "Quick Fleet Calendar Entry"))
                        {
                            if (!string.IsNullOrEmpty(e.MenuItem.Value))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = base.scWrapper.aircraftDuty.EntityList.Where(x => x.AircraftDutyCD.Trim().ToUpper() == (e.MenuItem.Value.Trim().ToUpper())).ToList();

                                    if (objRetVal.Count() > 0)
                                    {
                                        string AircraftDutyDescription = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[0]).AircraftDutyDescription.ToString();
                                        string AircraftDutyCode = e.MenuItem.Value;

                                        DateTime Sdate = e.TimeSlot.Start;
                                        string StartTime = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[0]).DefaultStartTM.ToString();
                                        Sdate = Sdate.AddHours(Convert.ToInt16(StartTime.Substring(0, 2)));
                                        Sdate = Sdate.AddMinutes(Convert.ToInt16(StartTime.Substring(3, 2)));
                                        DateTime EDate = e.TimeSlot.Start;

                                        string EndTime = ((FlightPak.Web.CommonService.AircraftDuty)objRetVal[0]).DefualtEndTM.ToString();
                                        EDate = EDate.AddHours(Convert.ToInt16(EndTime.Substring(0, 2)));
                                        EDate = EDate.AddMinutes(Convert.ToInt16(EndTime.Substring(3, 2)));

                                        string tailnum = e.TimeSlot.Resource.Key.ToString();

                                        Int64 fleetid = 0;
                                        using (var client = new MasterCatalogServiceClient())
                                        {
                                            var objRetfleetVal = client.GetFleetProfileList().EntityList.Where(x => x.TailNum.Trim().ToUpper() == (tailnum.Trim().ToUpper())).ToList();
                                            fleetid = Convert.ToInt64(objRetfleetVal[0].FleetID);
                                        }

                                        PreflightMain Trip = new PreflightMain();
                                        Trip.FleetID = fleetid;
                                        Trip.Notes = AircraftDutyDescription;
                                        Trip.HomebaseID = Convert.ToInt64(HomeBaseId);
                                        Trip.RecordType = "M";
                                        Trip.IsDeleted = false;
                                        Trip.ClientID = ClientId;
                                        Trip.LastUpdUID = UserPrincipal.Identity._name;
                                        Trip.LastUpdTS = System.DateTime.UtcNow;
                                        PreflightLeg Leg = new PreflightLeg();
                                        Leg.LegNUM = Convert.ToInt64("1");
                                        string timebase = FilterOptions.TimeBase.ToString();
                                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                        {
                                            if (timebase.ToUpper() == "LOCAL")
                                            {
                                                Leg.DepartureDTTMLocal = Sdate;
                                                Leg.ArrivalDTTMLocal = EDate;
                                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                            }
                                            else if (timebase.ToUpper() == "UTC")
                                            {
                                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                                Leg.DepartureGreenwichDTTM = Sdate;
                                                Leg.ArrivalGreenwichDTTM = EDate;
                                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), Sdate, false, false);
                                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeBaseId), EDate, false, false);
                                            }
                                            else
                                            {
                                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, false, false);
                                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, false, false);
                                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), Sdate, true, true);
                                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(HomeAirportId), EDate, true, true);
                                                Leg.HomeDepartureDTTM = Sdate;
                                                Leg.HomeArrivalDTTM = EDate;
                                            }
                                        }
                                        Leg.DepartICAOID = HomeAirportId;
                                        Leg.ArriveICAOID = HomeAirportId;
                                        Leg.DutyTYPE = AircraftDutyCode;
                                        Leg.IsDeleted = false;
                                        Leg.ClientID = ClientId;
                                        Leg.LastUpdUID = UserPrincipal.Identity._name;
                                        Leg.LastUpdTS = System.DateTime.UtcNow;
                                        Trip.State = TripEntityState.Added;
                                        Leg.State = TripEntityState.Added;
                                        Trip.PreflightLegs = new List<PreflightLeg>();
                                        Trip.PreflightLegs.Add(Leg);

                                        using (PreflightServiceClient Service = new PreflightServiceClient())
                                        {
                                            var ReturnValue = Service.Add(Trip);
                                            if (ReturnValue.ReturnFlag == true)
                                            {
                                                loadTree(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        else if (e.MenuItem.Value == "QuickFleetEntry")// quick fleet entry main menu item selected...
                        {
                            return;
                        }
                        else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                        {
                            var tripnum = string.Empty;
                            Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                        }
                        else
                        {
                            Response.Redirect(e.MenuItem.Value);
                        }
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "PleaseWait", "stopPleaseWait();", true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.ScheduleCalendar);
                }
            }
        }

        //Get resource type based on Fleet / Crew type...
        public ResourceType GetResourceType()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                ResourceType resourceType = new ResourceType("CrewFleetCodes");

                resourceType.KeyField = ModuleNameConstants.Preflight.TailNum;

                resourceType.ForeignKeyField = ModuleNameConstants.Preflight.TailNum;

                resourceType.TextField = ModuleNameConstants.Preflight.Resource_Description;                

                resourceType.AllowMultipleValues = false;

                return resourceType;

            }



        }
        //To display homebase on resource header
        protected void RadWeeklyScheduler_ResourceHeaderCreated(object sender, Telerik.Web.UI.ResourceHeaderCreatedEventArgs e)
        {
            if (DisplayOptions.WeeklyFleet != null && DisplayOptions.WeeklyFleet.ShowHomeBase != null && DisplayOptions.WeeklyFleet.ShowHomeBase == true)
            {
                ((TextBox)e.Container.FindControl("ResourceSubHeader1")).Visible = true;              
                 string HomeBase = e.Container.Resource.Key.ToString();
                 if (!string.IsNullOrEmpty(HomeBase))
                 {
                     using (MasterCatalogServiceClient svc = new MasterCatalogServiceClient())
                     {
                         var fleet = svc.GetFleetByTailNumber(HomeBase).EntityInfo;
                         if (fleet != null)
                         {
                             Int64 Id = fleet.FleetID;
                             if (Id != null)
                             {
                                 var FleetList = svc.GetFleetIDInfo(Id).EntityList.ToList();
                                 if (FleetList.Count > 0)
                                 {
                                     GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                     if (FleetCatalogEntity.HomeBaseCD != null)
                                     {
                                         ((TextBox)e.Container.FindControl("ResourceSubHeader1")).Text = FleetCatalogEntity.HomeBaseCD;
                                         ((TextBox)e.Container.FindControl("ResourceSubHeader1")).ToolTip = FleetCatalogEntity.HomeBaseCD;
                                     }
                                 }
                             }

                         }
                     }
                 }

            }
            else
            {
                ((TextBox)e.Container.FindControl("ResourceSubHeader1")).Visible = false;

            }
        }
    }
}
