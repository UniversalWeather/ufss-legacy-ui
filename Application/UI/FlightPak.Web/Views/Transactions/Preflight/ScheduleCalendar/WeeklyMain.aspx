﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="AutoID" MasterPageFile="~/Framework/Masters/Site.master"
    CodeBehind="WeeklyMain.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.WeeklyMain" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/common-calendar.js"></script>
</asp:Content>
<asp:Content ID="SiteBodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dgWeeklyMainMonday">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgWeeklyMainMonday" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMenu1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadMenu1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock1">
                    
                    <script type="text/javascript">
                        function pageLoad() {
                            if ($telerik.isMobileSafari) {
                                var scrollAreaMon = $get('<%= dgWeeklyMainMonday.ClientID%>');
                                scrollAreaMon.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollAreaTue = $get('<%= dgWeeklyMainTuesday.ClientID%>');
                                scrollAreaTue.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollAreaWed = $get('<%= dgWeeklyMainWednesday.ClientID%>');
                                scrollAreaWed.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollAreaThru = $get('<%= dgWeeklyMainThursday.ClientID%>');
                                scrollAreaThru.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollAreaFri = $get('<%= dgWeeklyMainFriday.ClientID%>');
                                scrollAreaFri.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollAreaSat = $get('<%= dgWeeklyMainSaturday.ClientID%>');
                                scrollAreaSat.addEventListener('touchstart', handleTouchStart, false);
                                
                                var scrollAreaSun = $get('<%= dgWeeklyMainSunday.ClientID%>');
                                scrollAreaSun.addEventListener('touchstart', handleTouchStart, false);
                                
                                $(".rgDataDiv").bind('scroll', function (e) {
                                    $(".RadMenu_Context").css("display", "none");
                                    $(".RadMenu_Context").css("opacity", "0"); // Opacity improves the user exp. Hide the context menu flickering
                                });
                            }
                        }
                    </script>

                    <!--Context Menu Script -->
                    <script type="text/javascript">
                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }

                        function RadMondayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }

                        function RadTuesdayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }

                        function RadWednesdayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }

                        function RadThursdayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                            document.getElementById('<%=radSelectedLegNum.ClientID%>').value = eventArgs._dataKeyValues.LegNum;
                        }

                        function RadFridayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }
                        function RadSaturdayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }

                        function RadSundayGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }



                        function dgMondayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {


                            var menu = $find("<%= dgWeeklyMainMondayRadMenu1.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            // if (window.event) { evt = window.event; } else { evt = e; }
                            // if (evt.button != 2) { return false; }

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            if (tripId == null)
                            // When clicked from no record display
                            {

                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);

                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }
                                return;
                            }




                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...
                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;

                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        function dgTuesdayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {
                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            var menu = $find("<%= dgWeeklyMainTuesdayRadMenu2.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            if (tripId == null)
                            // When clicked from no record display
                            {

                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }

                                return;
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...

                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        function dgWednesdayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {
                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            var menu = $find("<%= dgWeeklyMainWednesdayRadMenu3.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            if (tripId == null)
                            // When clicked from no record display
                            {

                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }
                                return;
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...

                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        function dgThursdayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {
                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            var menu = $find("<%= dgWeeklyMainThursdayRadMenu4.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            if (tripId == null)
                            // When clicked from no record display
                            {
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }
                                return;
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...

                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        function dgFridayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {
                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            var menu = $find("<%= dgWeeklyMainFridayRadMenu5.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            if (tripId == null)
                            // When clicked from no record display
                            {

                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }

                                return;
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);

                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...

                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        function dgSaturdayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {
                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            var menu = $find("<%= dgWeeklyMainSaturdayRadMenu6.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (tripId == null)
                            // When clicked from no record display
                            {

                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }

                                return;
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...

                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        function dgSundayContextMenu(e, tripId, LegNum, startDate, tailNum, recordType, isLog, isPrivateTrip, showPrivateTripMenu) {

                            var evt = e;
                            var tagName = (e.target || e.srcElement).tagName.toUpperCase();
                            if (tagName == "INPUT" || tagName == "A") {
                                return;
                            }

                            var menu = $find("<%= dgWeeklyMainSundayRadMenu7.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (tripId == null)
                            // When clicked from no record display
                            {

                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                    menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                                }
                                else {
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }

                                return;
                            }

                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + tripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + tripId + "&legNum=" + LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (showPrivateTripMenu == "False") {
                                    if (isPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (recordType == 'M' || recordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...

                            document.getElementById("radGridSelectedTripId").value = tripId;
                            document.getElementById("radGridSelectedStartDate").value = startDate;
                            document.getElementById("radGridSelectedTailNum").value = tailNum;
                            document.getElementById("radGridClickedLegNum").value = LegNum;
                            if ($telerik.isMobileSafari) {
                                $telerik.cancelRawEvent(evt);
                                $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
                                menu.showAt(lastCalendarContext.clientX, lastCalendarContext.clientY);
                            }
                            else {
                                menu.show(evt);
                                evt.cancelBubble = true;
                                evt.returnValue = false;

                                if (evt.stopPropagation) {
                                    evt.stopPropagation();
                                    evt.preventDefault();
                                }
                            }
                        }

                        //Fleet tree view...

                        function FleetTreeView_NodeCheck(sender, args) {

                            var treeView = $find("<%= FleetTreeView.ClientID %>");

                            var isChecked = args.get_node().get_checked();
                            var valueChecked = args.get_node().get_text();
                            var crewTree = $find("<%= CrewTreeView.ClientID %>");
                            if (isChecked) {

                                for (var i = 0; i < crewTree.get_nodes().get_count(); i++) {
                                    var node = crewTree.get_nodes().getNode(i);
                                    node.set_checked(false);
                                }
                                // crewTree.unselectAllNodes();
                            }
                            CheckDuplicates(isChecked, valueChecked, treeView);
                        }

                        // Crew tree view...
                        function CrewTreeView_NodeCheck(sender, args) {

                            var treeView = $find("<%= CrewTreeView.ClientID %>");

                            var isChecked = args.get_node().get_checked();
                            var valueChecked = args.get_node().get_text();
                            var fleetTree = $find("<%= FleetTreeView.ClientID %>");
                            if (isChecked) {

                                for (var i = 0; i < fleetTree.get_nodes().get_count(); i++) {
                                    var node = fleetTree.get_nodes().getNode(i);
                                    node.set_checked(false);
                                }
                                // fleetTree.unselectAllNodes();
                            }
                            CheckDuplicates(isChecked, valueChecked, treeView);
                        }
                        
                    </script>
                    <!--Context Menu Script -->
                    <script type="text/javascript">
                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                        function openWinWeeklyMainFullView() {
                            window.open("WeeklyMainFullView.aspx", "Default", " maximize=true, fullscreen=1, channelmode=yes, toolbar=no, menubar=no, directories=no, resizable=yes, status=no, scrollbars=auto, close=yes");
                        }

                        function openWinAdv() {
                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }
                        function openWinFleetCalendarEntries() {
                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }

                        function openWinOutBoundInstructions() {
                            var oWnd = radopen("../PreflightOutboundInstruction.aspx?IsCalender=1", "RadWindow5");
                        }

                        function openWinLegNotes() {
                            var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                        }
                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow;
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                            return oWindow;
                        }
                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                       
                    </script>
                    <script type="text/javascript">

                        //Rad Date Picker

                        var currentTextBox = null;

                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox

                        function showPopup(sender, e) {

                            //this is a reference to the texbox which raised the event

                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate

                            //the selection of a date

                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                            //this variable is used to store a reference to the date picker, which is currently

                            //active

                            currentDatePicker = datePicker;

                            //this method first parses the date, that the user entered or selected, and then

                            //sets it as a selected date to the picker

                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));
                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                                //value of the picker
                                //currentTextBox.value = args.get_newValue();

                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");
                                }
                                else
                                { return false; }
                            }
                        }
                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {
                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                sender.value = formattedDate;
                            }
                        }

                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }

                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();
                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }
                            // allow dot, slash and hypen characters... if any other character, throw alert
                            return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                        }
                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                    </script>
                    <!-- Start: Tooltip integration -->
                    <script type="text/javascript">
//<![CDATA[
                        function hideActiveToolTip() {
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                tooltip.hide();
                            }
                        }
//]]>
                    </script>
                    <!-- End: Tooltip integration -->

                   

                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager2" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/FleetCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/LegNotesPopup.aspx?IsCalender=1">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <%-- <div class="globalMenu_mapping">
        <a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
        <a href="#" class="globalMenu_mapping">Preflight</a> <span class="breadcrumbCaret">
        </span><a href="#" class="globalMenu_mapping">Scheduling Calendar</a>
    </div>--%>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content splitter_box">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="radSelectedLegNum" name="radSelectedLegNum" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="556px" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel140" style='width:160px'; align = "left">
                    <div>
                        <span></span>
                    </div>
                </td>
                <td align="left" class="defaultview">
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        OnCheckedChanged="ChkDefaultView_OnCheckedChanged" onclick="javascript:showPleaseWait();" />
                </td>
                <td align="left" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
                <td class="sc_nav_topicon" align="left">
                    <span class="tab-nav-icons"><a class="refresh-icon" onclick="Refresh();" href="#"
                        title="Refresh"></a><a class="help-icon" href="/Views/Help/ViewHelp.aspx?Screen=SchedulingCalendar" title="Help"></a></span>
                </td>
                 <td class="tdLabel70" align="right" style='width:20px;'>
                    <asp:Button ID="btnFullView" runat="server" CssClass="ui_nav" Text="FV" ToolTip="Full View" OnClientClick="javascript:openWinWeeklyMainFullView();return false;" />
                 </td> 
            </tr>
            <tr>
                <td colspan="6">
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0" Width="980px"
                        Skin="Windows7" Height="480px">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="Both" Width="100%">
                            <div class="sc_navigation">
                                <div class="sc_navigation_left">
                                    <span class="sc_weekly_selection">
                                        <asp:RadioButton ID="radFleet" AutoPostBack="true" OnCheckedChanged="radFleet_Checked"
                                            onclick="javascript:showPleaseWait();" GroupName="Mode" runat="server" Text="Fleet">
                                        </asp:RadioButton>
                                        <asp:RadioButton ID="radCrew" AutoPostBack="true" OnCheckedChanged="radCrew_Checked"
                                            onclick="javascript:showPleaseWait();" runat="server" GroupName="Mode" Text="Crew">
                                        </asp:RadioButton>
                                        <asp:RadioButton ID="radDetail" Visible="true" AutoPostBack="true" OnCheckedChanged="radDetail_Checked"
                                            onclick="javascript:showPleaseWait();" runat="server" GroupName="Mode" Text="Detail">
                                        </asp:RadioButton>
                                        <asp:RadioButton ID="radMain" AutoPostBack="true" runat="server" Checked="true" OnCheckedChanged="radMain_Checked"
                                            onclick="javascript:showPleaseWait();" GroupName="Mode" Text="Main"></asp:RadioButton>
                                    </span>
                                </div>
                                <div class="sc_navigation_right">
                                    <span class="act-btn"><span>
                                        <asp:Button ID="btnFirst" runat="server" ToolTip="Go Back 7 Days" CssClass="icon-first_sc"
                                            OnClientClick="showPleaseWait();" OnClick="btnFirst_click" /></span><span>
                                                <asp:Button ID="prevButton" runat="server" OnClick="prevButton_Click" CssClass="icon-prev_sc"
                                                    OnClientClick="showPleaseWait();" ToolTip="Go Back 1 Day" /></span><span>
                                                        <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                            AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" OnClientClick="showPleaseWait();"
                                                            onkeydown="return tbDate_OnKeyDown(this, event);" onfocus="showPopup(this, event);"
                                                            onKeyPress="return ontbDateKeyPress(this, event);" onBlur="parseDate(this, event);"
                                                            MaxLength="10"></asp:TextBox></span> <span>
                                                                <asp:Button ID="nextButton" runat="server" OnClick="nextButton_Click" CssClass="icon-next_sc"
                                                                    OnClientClick="showPleaseWait();" ToolTip="Go Forward 1 Day" /></span>
                                        <span>
                                            <asp:Button ID="btnPrevious" runat="server" CssClass="icon-last_sc" OnClick="btnLast_click"
                                                OnClientClick="showPleaseWait();" ToolTip="Go Forward 7 Days" /></span></span>
                                    <span>
                                        <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                            OnClientClick="showPleaseWait();" />
                                    </span>
                                </div>
                            </div>
                            <input type="hidden" id="radGridSelectedTripId" name="radGridSelectedTripId" />
                            <input type="hidden" id="radGridSelectedStartDate" name="radGridSelectedStartDate" />
                            <input type="hidden" id="radGridSelectedTailNum" name="radGridSelectedTailNum" />
                            <input type="hidden" id="radGridClickedLegNum" name="radGridClickedLegNum" />
                            <table width="100%" class="sc_weeklymain_calendar">
                                <tr>
                                    <telerik:RadPanelBar ID="RadPanelBar4" Width="100%" ExpandAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <div>
                                                        <td width="50%">
                                                            <asp:Label ID="weeklyMainMondayDate" runat="server"></asp:Label>
                                                            <telerik:RadGrid ID="dgWeeklyMainMonday" OnItemDataBound="dgWeeklyMainMonday_Databound"
                                                                BackColor=" #dfeaf7" runat="server" Skin="Office2010Silver" ClientSettings-Scrolling-AllowScroll="true"
                                                                PagerStyle-AlwaysVisible="false" AllowFilteringByColumn="false" Height="135px">
                                                                <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn UniqueName="weeklyMonday">

                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="FlightCategoryCode"  Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <ItemTemplate>
                                                                        <div oncontextmenu="dgMondayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');"
                                                                            onclick="dgMondayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');">
                                                                            <table class="event_info">
                                                                                <tr id='Row1' runat="server" colspan="3">
                                                                                    <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                        margin-left: 0px !important;">
                                                                                        <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                        <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                        <%--<asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%# String.IsNullOrEmpty (Eval("CrewCodes").ToString()) ? "":" - "+ Eval("CrewCodes")%>'></asp:Label>--%>
                                                                                        <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                        <%--  <asp:HiddenField ID="hdnRecordTypeMonday" runat="server" Value='<%# Eval("RecordType")%>' />--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id='Row2' runat="server" colspan="3">
                                                                                    <td id="cellTripNum" runat="server">
                                                                                        <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                        <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                        <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row3" runat="server">
                                                                                    <td id='LegInfo' runat="server" colspan="3">
                                                                                        <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                        <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                    <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                        <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                        <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                    <td id='Td1' runat="server">
                                                                                        <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                    <td id='Cell5' runat="server">
                                                                                        <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# " (" + Eval("FlightCategoryCode") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row5" runat="server" visible="false">
                                                                                    <td>
                                                                                        <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <NoRecordsTemplate>
                                                                        <div oncontextmenu="dgMondayContextMenu(event, null, null, null, null, null, null, null, null);" 
                                                                            onclick="dgMondayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                            style="height: 110px; width: 448px">
                                                                            No records to Display.
                                                                        </div>
                                                                    </NoRecordsTemplate>
                                                                    <CommandItemTemplate>
                                                                    </CommandItemTemplate>
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <GroupFooterTemplate>
                                                                    </GroupFooterTemplate>
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <FilterItemStyle />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                    </Scrolling>
                                                                    <ClientEvents OnRowContextMenu="dgMondayContextMenu" OnRowHidden="dgMondayContextMenu"
                                                                        OnRowClick="dgMondayContextMenu"></ClientEvents>
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                            <telerik:RadContextMenu ID="dgWeeklyMainMondayRadMenu1" runat="server" EnableRoundedCorners="true"
                                                                EnableShadows="true" OnItemClick="RadMondayGridContextMenu_ItemClick" OnClientItemClicked="RadMondayGridContextMenu_OnClientItemClicked">
                                                                <Targets>
                                                                    <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainMonday" />
                                                                </Targets>
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadContextMenu>
                                                        </td>
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                    <telerik:RadPanelBar ID="RadPanelBar6" Width="100%" ExpandAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <div>
                                                        <td width="50%">
                                                            <input type="hidden" id="dgWeeklyMainThursdayClickedRowIndex" name="dgWeeklyMainThursdayClickedRowIndex" />
                                                            <telerik:RadGrid ID="dgWeeklyMainThursday" runat="server" Skin="Office2010Silver"
                                                                BackColor=" #dfeaf7" PagerStyle-AlwaysVisible="false" AllowFilteringByColumn="false"
                                                                OnItemDataBound="dgWeeklyMainThursday_Databound" Height="135px">
                                                                <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn UniqueName="weeklyThursday">
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="FlightCategoryCode"  Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <ItemTemplate>
                                                                        <div oncontextmenu="dgThursdayContextMenu(event,'<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');"
                                                                            onclick="dgThursdayContextMenu(event,'<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');">
                                                                            <table class="event_info">
                                                                                <tr id='Row1' runat="server" colspan="3">
                                                                                    <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                        margin-left: 0px !important;">
                                                                                        <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                        <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                        <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                        <%--<asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%# String.IsNullOrEmpty (Eval("CrewCodes").ToString()) ? "":" - "+ Eval("CrewCodes")%>'></asp:Label>--%>
                                                                                        <%-- <asp:HiddenField ID="hdnRecordTypeThursday" runat="server" Value='<%# Eval("RecordType")%>' />--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id='Row2' runat="server" colspan="3">
                                                                                    <td id="cellTripNum" runat="server">
                                                                                        <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                        <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                        <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row3" runat="server">
                                                                                    <td id='LegInfo' runat="server" colspan="3">
                                                                                        <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                        <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                    <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                        <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                        <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                    <td id='Td1' runat="server">
                                                                                        <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                    <td id='Cell5' runat="server">
                                                                                        <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# Eval("FlightCategoryCode")%> '></asp:Label>
                                                                                        <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row5" runat="server" visible="false">
                                                                                    <td>
                                                                                        <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <NoRecordsTemplate>
                                                                        <div oncontextmenu="dgThursdayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                            onclick="dgThursdayContextMenu(event, null, null, null, null, null, null, null, null);" 
                                                                            style="height: 105px; width: 447px">
                                                                            No records to Display.
                                                                        </div>
                                                                    </NoRecordsTemplate>
                                                                    <CommandItemTemplate>
                                                                    </CommandItemTemplate>
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <GroupFooterTemplate>
                                                                    </GroupFooterTemplate>
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <FilterItemStyle />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                    </Scrolling>
                                                                    <ClientEvents OnRowContextMenu="dgThursdayContextMenu" OnRowHidden="dgThursdayContextMenu"
                                                                        OnRowClick="dgThursdayContextMenu"></ClientEvents>
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                            <telerik:RadContextMenu ID="dgWeeklyMainThursdayRadMenu4" runat="server" EnableRoundedCorners="true"
                                                                EnableShadows="true" OnItemClick="RadThursdayGridContextMenu_ItemClick" OnClientItemClicked="RadThursdayGridContextMenu_OnClientItemClicked">
                                                                <Targets>
                                                                    <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainThursday" />
                                                                </Targets>
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadContextMenu>
                                                        </td>
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </tr>
                                <tr>
                                    <telerik:RadPanelBar ID="RadPanelBar5" Width="100%" ExpandAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <div>
                                                        <td width="50%">
                                                            <input type="hidden" id="dgWeeklyMainTuesdayClickedRowIndex" name="dgWeeklyMainTuesdayClickedRowIndex" />
                                                            <telerik:RadGrid ID="dgWeeklyMainTuesday" runat="server" Skin="Office2010Silver"
                                                                BackColor=" #dfeaf7" PagerStyle-AlwaysVisible="false" AllowFilteringByColumn="false"
                                                                OnItemDataBound="dgWeeklyMainTuesday_Databound" Height="135px">
                                                                <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn UniqueName="weeklyTuesday">
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="FlightCategoryCode"  Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <ItemTemplate>
                                                                        <div oncontextmenu="dgTuesdayContextMenu(event,  '<%# Eval("TripId")%>', '<%# Eval("LegNum")%>','<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');"
                                                                            onclick="dgTuesdayContextMenu(event,  '<%# Eval("TripId")%>', '<%# Eval("LegNum")%>','<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');">
                                                                            <table width="100%" class="event_info">
                                                                                <tr id='Row1' runat="server" colspan="3">
                                                                                    <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                        margin-left: 0px !important;">
                                                                                        <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                        <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                        <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                        <%--   <asp:HiddenField ID="hdnRecordTypeTuesday" runat="server" Value='<%# Eval("RecordType")%>' />--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id='Row2' runat="server" colspan="3">
                                                                                    <td id="cellTripNum" runat="server">
                                                                                        <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                        <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                        <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row3" runat="server">
                                                                                    <td id='LegInfo' runat="server" colspan="3">
                                                                                        <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                        <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                    <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                        <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                        <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                    <td id='Td1' runat="server">
                                                                                        <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                    <td id='Cell5' runat="server">
                                                                                        <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# " (" + Eval("FlightCategoryCode") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%> '></asp:Label>
                                                                                        <br />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row5" runat="server" visible="false">
                                                                                    <td>
                                                                                        <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <NoRecordsTemplate>
                                                                        <div oncontextmenu="dgTuesdayContextMenu(event, null, null, null, null, null, null, null, null);" 
                                                                            onclick="dgTuesdayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                            style="height: 105px; width: 448px">
                                                                            No records to Display.
                                                                        </div>
                                                                    </NoRecordsTemplate>
                                                                    <CommandItemTemplate>
                                                                    </CommandItemTemplate>
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <GroupFooterTemplate>
                                                                    </GroupFooterTemplate>
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <FilterItemStyle />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                    </Scrolling>
                                                                    <ClientEvents OnRowContextMenu="dgTuesdayContextMenu" OnRowHidden="dgTuesdayContextMenu"
                                                                        OnRowClick="dgTuesdayContextMenu"></ClientEvents>
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                            <telerik:RadContextMenu ID="dgWeeklyMainTuesdayRadMenu2" runat="server" EnableRoundedCorners="true"
                                                                EnableShadows="true" OnItemClick="RadTuesdayGridContextMenu_ItemClick" OnClientItemClicked="RadTuesdayGridContextMenu_OnClientItemClicked">
                                                                <Targets>
                                                                    <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainTuesday" />
                                                                </Targets>
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadContextMenu>
                                                        </td>
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                    <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <div>
                                                        <td width="50%">
                                                            <input type="hidden" id="dgWeeklyMainFridayClickedRowIndex" name="dgWeeklyMainFridayClickedRowIndex" />
                                                            <telerik:RadGrid ID="dgWeeklyMainFriday" runat="server" Skin="Office2010Silver" PagerStyle-AlwaysVisible="false"
                                                                AllowFilteringByColumn="false" OnItemDataBound="dgWeeklyMainFriday_Databound"
                                                                BackColor=" #dfeaf7" Height="135px">
                                                                <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn UniqueName="weeklyFriday">
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="FlightCategoryCode"  Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <ItemTemplate>
                                                                        <div oncontextmenu="dgFridayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');"
                                                                            onclick="dgFridayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');">
                                                                            <table width="100%" class="event_info">
                                                                                <tr id='Row1' runat="server" colspan="3">
                                                                                    <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                        margin-left: 0px !important;">
                                                                                        <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                        <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                        <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id='Row2' runat="server" colspan="3">
                                                                                    <td id="cellTripNum" runat="server">
                                                                                        <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                        <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                        <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row3" runat="server">
                                                                                    <td id='LegInfo' runat="server" colspan="3">
                                                                                        <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                        <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                    <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                        <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                        <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                    <td id='Td1' runat="server">
                                                                                        <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                    <td id='Cell5' runat="server">
                                                                                        <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# " (" + Eval("FlightCategoryCode") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%> '></asp:Label>
                                                                                        <br />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row5" runat="server" visible="false">
                                                                                    <td>
                                                                                        <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <NoRecordsTemplate>
                                                                        <div oncontextmenu="dgFridayContextMenu(event, null, null, null, null, null, null, null, null);" 
                                                                            onclick="dgFridayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                            style="height: 105px; width: 447px">
                                                                            No records to Display.
                                                                        </div>
                                                                    </NoRecordsTemplate>
                                                                    <CommandItemTemplate>
                                                                    </CommandItemTemplate>
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <GroupFooterTemplate>
                                                                    </GroupFooterTemplate>
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <FilterItemStyle />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                    </Scrolling>
                                                                    <ClientEvents OnRowContextMenu="dgFridayContextMenu" OnRowHidden="dgFridayContextMenu"
                                                                        OnRowClick="dgFridayContextMenu"></ClientEvents>
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                            <telerik:RadContextMenu ID="dgWeeklyMainFridayRadMenu5" runat="server" EnableRoundedCorners="true"
                                                                EnableShadows="true" OnItemClick="RadFridayGridContextMenu_ItemClick" OnClientItemClicked="RadFridayGridContextMenu_OnClientItemClicked">
                                                                <Targets>
                                                                    <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainFriday" />
                                                                </Targets>
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadContextMenu>
                                                        </td>
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </tr>
                                <tr>
                                    <telerik:RadPanelBar ID="RadPanelBar7" Width="100%" ExpandAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <div>
                                                        <td width="50%">
                                                            <input type="hidden" id="dgWeeklyMainWednesdayClickedRowIndex" name="dgWeeklyMainWednesdayClickedRowIndex" />
                                                            <telerik:RadGrid ID="dgWeeklyMainWednesday" runat="server" Skin="Office2010Silver"
                                                                BackColor=" #dfeaf7" PagerStyle-AlwaysVisible="false" AllowFilteringByColumn="false"
                                                                OnItemDataBound="dgWeeklyMainWednesday_Databound" Height="154px">
                                                                <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn UniqueName="weeklyWednesday">                                                                        
       
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="FlightCategoryCode"  Display="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <ItemTemplate>
                                                                        <div oncontextmenu="dgWednesdayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');"
                                                                            onclick="dgWednesdayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');">
                                                                            <table width="100%" class="event_info">
                                                                                <tr id='Row1' runat="server" colspan="3">
                                                                                    <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                        margin-left: 0px !important;">
                                                                                        <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                        <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                        <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id='Row2' runat="server" colspan="3">
                                                                                    <td id="cellTripNum" runat="server">
                                                                                        <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                        <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                        <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                        <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row3" runat="server">
                                                                                    <td id='LegInfo' runat="server" colspan="3">
                                                                                        <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                        <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                    <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                    <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                        <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                        <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                    <td id='Td1' runat="server">
                                                                                        <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                    <td id='Cell5' runat="server">
                                                                                        <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# " (" + Eval("FlightCategoryCode") + ") " %> '></asp:Label>
                                                                                        <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                        <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                        <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%> '></asp:Label>
                                                                                        <br />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Row5" runat="server" visible="false">
                                                                                    <td>
                                                                                        <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <NoRecordsTemplate>
                                                                        <div oncontextmenu="dgWednesdayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                            onclick="dgWednesdayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                            style="height: 105px; width: 447px">
                                                                            No records to Display.
                                                                        </div>
                                                                    </NoRecordsTemplate>
                                                                    <CommandItemTemplate>
                                                                    </CommandItemTemplate>
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <GroupFooterTemplate>
                                                                    </GroupFooterTemplate>
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <FilterItemStyle />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                    </Scrolling>
                                                                    <ClientEvents OnRowContextMenu="dgWednesdayContextMenu" OnRowHidden="dgWednesdayContextMenu"
                                                                        OnRowClick="dgWednesdayContextMenu"></ClientEvents>
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                            <telerik:RadContextMenu ID="dgWeeklyMainWednesdayRadMenu3" runat="server" EnableRoundedCorners="true"
                                                                EnableShadows="true" OnItemClick="RadWednesdayGridContextMenu_ItemClick" OnClientItemClicked="RadWednesdayGridContextMenu_OnClientItemClicked">
                                                                <Targets>
                                                                    <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainWednesday" />
                                                                </Targets>
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadContextMenu>
                                                        </td>
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                    <telerik:RadPanelBar ID="RadPanelBar3" Width="100%" ExpandAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <div>
                                                        <td width="50%" valign="top">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td width="50%">
                                                                        <input type="hidden" id="dgWeeklyMainSaturdayClickedRowIndex" name="dgWeeklyMainSaturdayClickedRowIndex" />
                                                                        <telerik:RadGrid ID="dgWeeklyMainSaturday" runat="server" Skin="Office2010Silver"
                                                                            BackColor=" #dfeaf7" PagerStyle-AlwaysVisible="false" AllowFilteringByColumn="false"
                                                                            OnItemDataBound="dgWeeklyMainSaturday_Databound" Height="77px">
                                                                            <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                                <Columns>
                                                                                    <telerik:GridTemplateColumn UniqueName="weeklySaturday">
                                                                                    </telerik:GridTemplateColumn>
                                                                                    <telerik:GridBoundColumn DataField="FlightCategoryCode"  Display="false">
                                                                                    </telerik:GridBoundColumn>
                                                                                </Columns>
                                                                                <ItemTemplate>
                                                                                    <div oncontextmenu="dgSaturdayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');"
                                                                                        onclick="dgSaturdayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>', '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>', '<%# Eval("IsPrivateTrip")%>', '<%# Eval("ShowPrivateTripMenu")%>');">
                                                                                        <table width="100%" class="event_info">
                                                                                            <tr id='Row1' runat="server" colspan="3">
                                                                                                <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                                    margin-left: 0px !important;">
                                                                                                    <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                                    <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id='Row2' runat="server" colspan="3">
                                                                                                <td id="cellTripNum" runat="server">
                                                                                                    <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                                    <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                                    <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                                    <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                                    <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label><br />
                                                                                                    <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="Row3" runat="server">
                                                                                                <td id='LegInfo' runat="server" colspan="3">
                                                                                                    <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                                    <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                                    <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                                <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                                <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                                <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                                    <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                                    <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                                <td id='Td1' runat="server">
                                                                                                    <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                                <td id='Cell5' runat="server">
                                                                                                    <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# " (" + Eval("FlightCategoryCode") + ") " %> '></asp:Label>
                                                                                                    <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                                    <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%> '></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="Row5" runat="server" visible="false">
                                                                                                <td>
                                                                                                    <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                                    <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                                <NoRecordsTemplate>
                                                                                    <div oncontextmenu="dgSaturdayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                                        onclick="dgSaturdayContextMenu(event, null, null, null, null, null, null, null, null);" style="height: 46px;
                                                                                        width: 448px">
                                                                                        No records to Display.
                                                                                    </div>
                                                                                </NoRecordsTemplate>
                                                                                <CommandItemTemplate>
                                                                                </CommandItemTemplate>
                                                                                <EditItemTemplate>
                                                                                </EditItemTemplate>
                                                                                <GroupFooterTemplate>
                                                                                </GroupFooterTemplate>
                                                                                <PagerTemplate>
                                                                                </PagerTemplate>
                                                                                <FilterItemStyle />
                                                                            </MasterTableView>
                                                                            <ClientSettings>
                                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                                </Scrolling>
                                                                                <ClientEvents OnRowContextMenu="dgSaturdayContextMenu" OnRowHidden="dgSaturdayContextMenu"
                                                                                    OnRowClick="dgSaturdayContextMenu"></ClientEvents>
                                                                            </ClientSettings>
                                                                        </telerik:RadGrid>
                                                                        <telerik:RadContextMenu ID="dgWeeklyMainSaturdayRadMenu6" runat="server" EnableRoundedCorners="true"
                                                                            EnableShadows="true" OnItemClick="RadSaturdayGridContextMenu_ItemClick" OnClientItemClicked="RadSaturdayGridContextMenu_OnClientItemClicked">
                                                                            <Targets>
                                                                                <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainSaturday" />
                                                                            </Targets>
                                                                            <Items>
                                                                                <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                                <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                                <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                                <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                                <telerik:RadMenuItem Text="Logistics">
                                                                                    <Items>
                                                                                        <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                                        <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                                        <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                                        <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                                        <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                                        <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                                    </Items>
                                                                                </telerik:RadMenuItem>
                                                                                <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                                <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                                <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                                <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                                <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                                <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                                <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                                <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                                <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                                <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                                <telerik:RadMenuItem IsSeparator="True" />
                                                                            </Items>
                                                                        </telerik:RadContextMenu>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <telerik:RadPanelBar ID="RadPanelBar8" Width="100%" ExpandAnimation-Type="None" runat="server">
                                                                        <Items>
                                                                            <telerik:RadPanelItem>
                                                                                <ContentTemplate>
                                                                                    <div>
                                                                                        <td width="50%">
                                                                                            <telerik:RadGrid ID="dgWeeklyMainSunday" runat="server" Skin="Office2010Silver" OnItemDataBound="dgWeeklyMainSunday_Databound"
                                                                                                PagerStyle-AlwaysVisible="false" AllowFilteringByColumn="false" Height="77px"
                                                                                                BackColor=" #dfeaf7">
                                                                                                <MasterTableView AllowFilteringByColumn="false" DataKeyNames="TripId,LegNum" PageSize="500">
                                                                                                    <Columns>
                                                                                                        <telerik:GridTemplateColumn UniqueName="weeklySunday">
                                                                                                        </telerik:GridTemplateColumn>
                                                                                                    </Columns>
                                                                                                    <ItemTemplate>
                                                                                                        <div oncontextmenu="dgSundayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>',  '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>');"
                                                                                                            onclick="dgSundayContextMenu(event,  '<%# Eval("TripId")%>','<%# Eval("LegNum")%>',  '<%# Eval("StartDate")%>', '<%# Eval("TailNum")%>', '<%# Eval("RecordType")%>', '<%# Eval("IsLog")%>');">
                                                                                                            <table style="border-width: 0px !important; font-size: x-small !important;" width="100%"
                                                                                                                class="event_info">
                                                                                                                <tr id='Row1' runat="server" colspan="3">
                                                                                                                    <td style="margin-right: 0px !important; margin-bottom: 0px !important; margin-top: 0px !important;
                                                                                                                        margin-left: 0px !important;">
                                                                                                                        <asp:Label ID="lbCrewCD" runat="server" Text=''></asp:Label>
                                                                                                                        <asp:Label ID="lbTailNum" runat="server" Text=' <%# Eval("TailNum")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbCrewCodesTop" runat="server" Text=' <%#(Eval("CrewCodes"))%>'></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id='Row2' runat="server" colspan="3">
                                                                                                                    <td id="cellTripNum" runat="server">
                                                                                                                        <asp:Label ID="lbTripNum" runat="server" Text='<%# "Trip: " + Eval("TripNUM")%>'></asp:Label>
                                                                                                                        <asp:Label ID="lbLegNum" runat="server" Text='<%# "Leg No.: " + Eval("LegNum")%>'></asp:Label>
                                                                                                                        <asp:Label ID="lbTripStatus" runat="server" Text='<%# " (" + Eval("TripStatus") + ") "%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbLegETE" runat="server" Text='<%#"ETE: " + String.Format (Eval("ElapseTM").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                                                        <asp:Label ID="lbETE" runat="server" Text='<%#"Cum ETE: " + String.Format (Eval("CumulativeETE").ToString(), "{0:#.#}")%>'></asp:Label><br />
                                                                                                                        <asp:Label ID="lbTotalETE" runat="server" Text='<%#"Total ETE: " + String.Format (Eval("TotalETE").ToString(), "{0:#.#}")%>'></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="Row3" runat="server">
                                                                                                                    <td id='LegInfo' runat="server" colspan="3">
                                                                                                                        <asp:Label ID="lbCrewDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("CrewDutyTypeCD") + ") " %> '></asp:Label>
                                                                                                                        <asp:Label ID="lbAircraftDutyType" runat="server" Visible="false" Text='<%# "(" + Eval("AircraftDutyCD") + ") " %> '></asp:Label>
                                                                                                                        <%--<asp:Label ID="lbStartTime" runat="server" Text='<%# Eval("StartDate", "{0:HH:mm}")%> '></asp:Label>
                                                                                <asp:Label ID="lbDepartureICAOID" runat="server" Text='<%# Eval("DepartureICAOID")%> '></asp:Label>
                                                                                <asp:Label ID="lbArrivalICAOID" runat="server" Text='<%# " | " + Eval("ArrivalICAOID")%> '></asp:Label>
                                                                                <asp:Label ID="lbEndTime" runat="server" Text='<%# Eval("EndDate", "{0:HH:mm}")%> '></asp:Label>--%>
                                                                                                                        <asp:Label ID="lbArrivalDepartureInfo" Visible="true" runat="server" Text='<%#"[Continued]"%>'></asp:Label>
                                                                                                                        <asp:Label ID="lbFlightNo" runat="server" Text='<%#"Flt No.: " + Eval("FlightNum")%> '></asp:Label><br />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="Row6" runat="server" style="overflow: hidden">
                                                                                                                    <td id='Td1' runat="server">
                                                                                                                        <asp:Label ID="lbArrivalDateInfo" Visible="false" runat="server" Text='<%#"** Arrival Date: " + Eval("TripDate") + " **" %>'></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="Row4" runat="server" style="overflow: hidden">
                                                                                                                    <td id='Cell5' runat="server">
                                                                                                                        <asp:Label ID="lbPAXCount" runat="server" Text=' <%#"Pax: " + Eval("PassengerCount", "{0}")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbFlightCategoryCode" runat="server" Text=' <%# " (" + Eval("FlightCategoryCode") + ") " %> '></asp:Label>
                                                                                                                        <asp:Label ID="lbPassengerRequestorCD" runat="server" Text=' <%#"Req: " + Eval("PassengerRequestorCD")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbDepartmentName" runat="server" Text=' <%#"Dept: "+ Eval("DepartmentCD")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbAuthorizationCD" runat="server" Text=' <%#"Auth: " + Eval("AuthorizationCD")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbTripPurpose" runat="server" Text=' <%#"Trip Purp.: " + Eval("TripPurpose")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbFlightPurpose" runat="server" Text=' <%#"Leg Purp.: " + Eval("FlightPurpose")%> '></asp:Label>
                                                                                                                        <asp:Label ID="lbReservationAvailable" runat="server" Text='<%#"Seats Avail: " + Eval("ReservationAvailable")%> '></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="Row5" runat="server" visible="false">
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lbCrewCodesBottom" runat="server" Visible="false" Text='<%# Eval("CrewCodes") + "-" %> '></asp:Label>
                                                                                                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Eval("Description")%> '></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </ItemTemplate>
                                                                                                    <NoRecordsTemplate>
                                                                                                        <div oncontextmenu="dgSundayContextMenu(event, null, null, null, null, null, null, null, null);" 
                                                                                                            onclick="dgSundayContextMenu(event, null, null, null, null, null, null, null, null);"
                                                                                                            style="height: 51px; width: 448px">
                                                                                                            No records to Display.
                                                                                                        </div>
                                                                                                    </NoRecordsTemplate>
                                                                                                    <CommandItemTemplate>
                                                                                                    </CommandItemTemplate>
                                                                                                    <EditItemTemplate>
                                                                                                    </EditItemTemplate>
                                                                                                    <GroupFooterTemplate>
                                                                                                    </GroupFooterTemplate>
                                                                                                    <PagerTemplate>
                                                                                                    </PagerTemplate>
                                                                                                    <FilterItemStyle />
                                                                                                </MasterTableView>
                                                                                                <ClientSettings>
                                                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="True">
                                                                                                    </Scrolling>
                                                                                                    <Selecting AllowRowSelect="true" />
                                                                                                    <ClientEvents OnRowContextMenu="dgSundayContextMenu" OnRowHidden="dgSundayContextMenu"
                                                                                                        OnRowClick="dgSundayContextMenu"></ClientEvents>
                                                                                                </ClientSettings>
                                                                                            </telerik:RadGrid>
                                                                                            <telerik:RadContextMenu ID="dgWeeklyMainSundayRadMenu7" runat="server" EnableRoundedCorners="true"
                                                                                                EnableShadows="true" OnItemClick="RadSundayGridContextMenu_ItemClick" OnClientItemClicked="RadSundayGridContextMenu_OnClientItemClicked">
                                                                                                <Targets>
                                                                                                    <telerik:ContextMenuControlTarget ControlID="dgWeeklyMainSunday" />
                                                                                                </Targets>
                                                                                                <Items>
                                                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx?IsCalendar=1" />
                                                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                                                        <Items>
                                                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew&IsCalendar=1" />
                                                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax&IsCalendar=1" />
                                                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics&IsCalendar=1" />
                                                                                                        </Items>
                                                                                                    </telerik:RadMenuItem>
                                                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                                                </Items>
                                                                                            </telerik:RadContextMenu>
                                                                                        </td>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </telerik:RadPanelItem>
                                                                        </Items>
                                                                    </telerik:RadPanelBar>
                                                                </tr>
                                                                <!-- sat/sun -->
                                                            </table>
                                                        </td>
                                                    </div>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </tr>
                            </table>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward" />
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="None"  Width="250px" Collapsed="true">
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="480px" ExpandMode="SingleExpandedItem" AllowCollapseAllItems="true">
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" TriStateCheckBoxes="true" ID="FleetTreeView" OnClientNodeChecked="FleetTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true" OnClientNodeChecked="CrewTreeView_NodeCheck">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Filter Criteria" Expanded="True" runat="server">
                                        <ContentTemplate>
                                        <div class="sc_filter_criteriamain">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </div> 
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                OnClientClick="javascript:openWinAdv();return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
