﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/SiteFV.Master"
    AutoEventWireup="true" ClientIDMode="AutoID" CodeBehind="WeeklyDetailFullView.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.WeeklyDetailFullView" %>

<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Scripts/stylefixes.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="SiteBodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock1">
                    <script type="text/javascript">
                        function showLegHistory(vTripID) {
                            var oWnd = radopen("TripHistory.aspx?FromPage=Preflight&TripID=" + vTripID, "RadWindow1");
                            return;
                        }


                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }

                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                        function openWinAdv() {
                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }
                        function openWinFleetCalendarEntries() {

                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }
                        function openWinOutBoundInstructions() {
                            var oWnd = radopen("../PreflightOutboundInstruction.aspx?IsCalender=1", "RadWindow5");
                        }
                        function openWinLegNotes() {
                            var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                        }
                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow;
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                            return oWindow;
                        }
                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }

                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                       

                    </script>
                    <script type="text/javascript">
                        //Rad Date Picker
                        var currentTextBox = null;
                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox
                        function showPopup(sender, e) {
                            //this is a reference to the texbox which raised the event
                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate
                            //the selection of a date
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                            //this variable is used to store a reference to the date picker, which is currently
                            //active
                            currentDatePicker = datePicker;

                            //this method first parses the date, that the user entered or selected, and then
                            //sets it as a selected date to the picker
                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                                //value of the picker
                                //currentTextBox.value = args.get_newValue();

                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");

                                }
                                else
                                { return false; }
                            }
                        }

                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {
                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                sender.value = formattedDate;
                            }
                        }

                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }

                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();
                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }
                            // allow dot, slash and hypen characters... if any other character, throw alert
                            return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                        }
                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }

                    </script>
                    <!-- Grid-->
                    <script type="text/javascript">

                        function RadWeeklyDetailGridContextMenu_OnClientItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }
                        function IpaddgWeeklyDetail_RowContextMenu(sender, eventArgs) {
                            if ($telerik.isMobileSafari) {
                                dgWeeklyDetail_RowContextMenu(sender, eventArgs);
                            }
                        }

                        var showMenu = false;
                        // dgWeeklyDetail Grid
                        function dgWeeklyDetail_RowContextMenu(sender, eventArgs) {
                            showMenu = true;
                            var menu = $find("<%=RadWeeklyDetailGridContextMenu.ClientID %>");
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var menuItemfleetadd = menu.findItemByText("Add Fleet Calendar Entry");
                            if (menuItemfleetadd != null) {
                                menuItemfleetadd.set_text("Fleet Calendar Entry");
                            }
                            var menuItemcrewadd = menu.findItemByText("Add Crew Calendar Entry");
                            if (menuItemcrewadd != null) {
                                menuItemcrewadd.set_text("Crew Calendar Entry");
                            }

                            if (eventArgs == null)
                            // When clicked from no record display
                            {
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(event);
                                    menu.showAt(screen.height / 2, screen.width / 2);
                                }
                                return;
                            }
                            var evt = eventArgs.get_domEvent();

                            if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                                return;
                            }

                            if (eventArgs._dataKeyValues.RecordType == null)
                            // When clicked from dummy appointment
                            {
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").set_enabled(false);
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    menu.showAt(screen.height / 2, screen.width / 2);
                                }
                                return;
                            }
                            // To show / hide Postflightlog menu items based on trip logged / entry...
                            else if (eventArgs._dataKeyValues.RecordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (eventArgs._dataKeyValues.IsLog == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + eventArgs._dataKeyValues.TripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + eventArgs._dataKeyValues.TripId + "&legNum=" + eventArgs._dataKeyValues.LegNum);
                                } else {  // else, hide
                                    menu.findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu.findItemByText("Flight Log Legs").set_enabled(false);

                                    // hide postflight log menu items
                                }
                                menu.findItemByText("Postflight Flight Logs").show();
                                menu.findItemByValue("Separator").show();
                                menu.findItemByText("Flight Log Legs").show();
                                menu.findItemByText("Crew").show();
                                menu.findItemByText("PAX").show();
                                menu.findItemByText("Outbound Instruction").show();
                                menu.findItemByText("Logistics").show();
                                menu.findItemByText("Leg Notes").show();

                                if (eventArgs._dataKeyValues.ShowPrivateTripMenu == "False") {
                                    if (eventArgs._dataKeyValues.IsPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }
                            }
                            else if (eventArgs._dataKeyValues.RecordType == 'M' || eventArgs._dataKeyValues.RecordType == 'C') {

                                //if trip is logged, show postflight log menu items
                                menu.findItemByText("Postflight Flight Logs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu.findItemByText("Flight Log Legs").hide();
                                menu.findItemByText("Crew").hide();
                                menu.findItemByText("PAX").hide();
                                menu.findItemByText("Outbound Instruction").hide();
                                menu.findItemByText("Logistics").hide();
                                menu.findItemByText("Leg Notes").hide();
                                menu.findItemByText("UWA Services").show();
                                menu.findItemByText("Preflight").show();
                                menu.findItemByText("Add New Trip").show();
                                var menuItemfleet = menu.findItemByText("Fleet Calendar Entry");
                                menuItemfleet.show();
                                menuItemfleet.set_text("Add Fleet Calendar Entry");

                                var menuItemcrew = menu.findItemByText("Crew Calendar Entry");
                                menuItemcrew.show();
                                menuItemcrew.set_text("Add Crew Calendar Entry");
                            }

                            // ends here...
                            var index = eventArgs.get_itemIndexHierarchical();
                            document.getElementById("radGridClickedRowIndex").value = index;

                            var domEvent = eventArgs.get_domEvent();
                            var source = domEvent.target || domEvent.srcElement;
                            var masterTable = sender.get_masterTableView();

                            masterTable.clearSelectedItems();
                            masterTable.selectItem(masterTable.get_dataItems()[index].get_element());
                            var cell = masterTable.get_dataItems()[index].get_element().cells[0];

                            if (source == cell) {
                                showMenu = false;
                                showLegHistory(eventArgs._dataKeyValues.TripId);
                            }
                            else
                            {
                                if ($telerik.isMobileSafari) {
                                    $telerik.cancelRawEvent(evt);
                                    menu.showAt(screen.height / 2, screen.width / 2);
                                }
                                else
                                {
                                    sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);
                                    menu.show(evt);
                                    evt.cancelBubble = true;
                                    evt.returnValue = false;

                                    if (evt.stopPropagation) {
                                        evt.stopPropagation();
                                        evt.preventDefault();
                                    }
                                }
                            }
                        }

                        function RadWeeklyDetailGridContextMenu_Showing(sender, eventArgs) {
                            if (showMenu == false) {
                                eventArgs.set_cancel(true);
                            }
                        }
                    </script>
                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div runat="server" id="PleaseWait" class="pageloader">
    </div>
    <div class="art-scheduling-content fullviewgrid" style="width: 100%; height: 100%">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="radSelectedView" name="radSelectedView" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0" style="width:100%; height:100%">
            <tr>
                <td width="80%" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel170">
                    <div class="sc_date_display">
                        <span>
                            <asp:Label ID="lbStartDate" runat="server" CssClass="date"></asp:Label></span><span
                                class="padleft_5"><asp:Label ID="lbdate" runat="server" CssClass="date" Text=" - "></asp:Label></span>
                        <span class="padleft_5">
                            <asp:Label ID="lbEndDate" runat="server" CssClass="date"></asp:Label></span>
                    </div>
                </td>
                <td align="left" class="defaultview" style='display: none;'>
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        OnCheckedChanged="ChkDefaultView_OnCheckedChanged" />
                </td>
                <td align="center" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
                <td align="right" class="tdLabel20">
                    <span><a class="refresh-icon" onclick="Refresh();" href="#" title="Refresh"></a></span>                         
                </td>
                <td class="tdLabel50" align="left">                   
                          <asp:Button ID="btnClose" runat="server" CssClass="ui_nav" Text="Close" ToolTip="Close" OnClientClick="javascript:window.close();" />
                </td>
            </tr>
            <tr style="width:100%; height:97%;vertical-align:top;">
                <td colspan="6" width="100%" style="width:100%; height:97%;vertical-align:top;">
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0" Width="100%"
                        Skin="Windows7" Height="97%">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="None" Width="100%" Height="97%">
                            <div class="sc_navigation">
                                <div class="sc_navigation_left">
                                    <span class="sc_weekly_selection">
                                        <asp:RadioButton ID="radFleet" AutoPostBack="true" OnCheckedChanged="radFleet_Checked"
                                            onclick="javascript:showPleaseWait();" GroupName="Mode" runat="server" Text="Fleet"
                                            Enabled="false"></asp:RadioButton>
                                        <asp:RadioButton ID="radCrew" AutoPostBack="true" OnCheckedChanged="radCrew_Checked"
                                            onclick="javascript:showPleaseWait();" runat="server" GroupName="Mode" Text="Crew"
                                            Enabled="false"></asp:RadioButton>
                                        <asp:RadioButton ID="radDetail" Visible="true" AutoPostBack="true" Checked="true"
                                            onclick="javascript:showPleaseWait();" OnCheckedChanged="radDetail_Checked" runat="server"
                                            GroupName="Mode" Text="Detail" Enabled="false"></asp:RadioButton>
                                        <asp:RadioButton ID="radMain" AutoPostBack="true" runat="server" OnCheckedChanged="radMain_Checked"
                                            onclick="javascript:showPleaseWait();" GroupName="Mode" Text="Main" Enabled="false">
                                        </asp:RadioButton>
                                    </span><span class="sc_weekly_checkbox"></span>
                                </div>
                                <div class="sc_navigation_right">
                                    <span class="act-btn"><span>
                                        <asp:Button ID="btnFirst" runat="server" ToolTip="Go Back 7 Days" CssClass="icon-first_sc"
                                            OnClientClick="showPleaseWait();" OnClick="btnFirst_click" /></span> <span>
                                                <asp:Button ID="prevButton" runat="server" OnClick="prevButton_Click" CssClass="icon-prev_sc"
                                                    OnClientClick="showPleaseWait();" ToolTip="Go Back 1 Day" /></span>
                                        <span>
                                            <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                OnClientClick="showPleaseWait();" onfocus="showPopup(this, event);" onKeyPress="return ontbDateKeyPress(this, event);"
                                                onBlur="parseDate(this, event);" MaxLength="10"></asp:TextBox></span> <span>
                                                    <asp:Button ID="nextButton" runat="server" OnClick="nextButton_Click" CssClass="icon-next_sc"
                                                        OnClientClick="showPleaseWait();" ToolTip="Go Forward 1 Day" /></span>
                                        <span>
                                            <asp:Button ID="btnPrevious" runat="server" CssClass="icon-last_sc" OnClick="btnLast_click"
                                                OnClientClick="showPleaseWait();" ToolTip="Go Forward 7 Days" /></span></span>
                                    <span>
                                        <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                            OnClientClick="showPleaseWait();" /></span>
                                </div>
                            </div>
                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" Height="97%" ExpandAnimation-Type="None" runat="server">
                                <Items>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div style="height:97%;" >
                                                <!-- [Start] Weekly Detail -->
                                                <telerik:RadGrid ID="dgWeeklyDetail" OnItemDataBound="dgweeklyDetail_ItemDataBound"
                                                    BackColor=" #dfeaf7" runat="server" AllowSorting="true" Skin="Office2010Silver"
                                                    AutoGenerateColumns="true" CssClass="fullviewgridmain fullviewgridmaindetail" AllowPaging="false" PagerStyle-AlwaysVisible="false"
                                                    ShowFooter="false" AllowFilteringByColumn="false" Width="100%" Height="590px">
                                                    <ClientSettings>
                                                        <Scrolling AllowScroll="True" ScrollHeight="550px" UseStaticHeaders="True" SaveScrollPosition="true"
                                                            FrozenColumnsCount="2" />
                                                    </ClientSettings>
                                                    <MasterTableView ClientDataKeyNames="TripId,RecordType,IsLog,FlightCategoryID, StartDate, TailNum, LocalDepartureTime,DepartureICAOID,LocalArrivalTime,ArrivalICAOID,CumulativeETE,FlightCategoryCode,CrewCD,PassengerRequestorCD,LegNum,IsPrivateTrip,ShowPrivateTripMenu"
                                                        DataKeyNames="TripId,FlightCategoryID,StartDate,TailNum,LocalDepartureTime,DepartureICAOID,LocalArrivalTime,ArrivalICAOID,CumulativeETE,FlightCategoryCode,CrewCD,PassengerRequestorCD,LegNum"
                                                        CommandItemDisplay="Bottom" ShowHeader="true" Width="100%" AllowFilteringByColumn="false"
                                                        AllowSorting="false" PageSize="500">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="LastUpdatedTime" HeaderText="Last Updated (UTC)"
                                                                AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-VerticalAlign="Top" HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-VerticalAlign="Middle" UniqueName="LastUpdatedTime">
                                                                <ItemStyle Wrap="false" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TripDate" HeaderText="Trip Date" AllowFiltering="false"
                                                                HeaderStyle-Width="75px" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                UniqueName="TripDate">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AllowFiltering="false"
                                                                HeaderStyle-Width="70px" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                UniqueName="TailNum">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="StartDateTime" HeaderText="Dep Date/Time" AllowFiltering="false"
                                                                ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top"
                                                                HeaderStyle-Width="110px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                UniqueName="DepartureTime" DataFormatString="{0:HH:mm}">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="DepartureICAOID" HeaderText="Dep ICAO" UniqueName="DepartureICAO"
                                                                HeaderStyle-Width="70px" AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ArrivalICAOID" HeaderText="Arr ICAO" AllowFiltering="false"
                                                                HeaderStyle-Width="65px" UniqueName="ArrivalICAO" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="EndDateTime" HeaderText="Arr Date/Time" UniqueName="ArrivalTime"
                                                                HeaderStyle-Width="110px" DataFormatString="{0:HH:mm}" AllowFiltering="false"
                                                                ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TripNumberString" HeaderText="Trip No." AllowFiltering="false"
                                                                HeaderStyle-Width="50px" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                UniqueName="TripNUM">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Status" AllowFiltering="false"
                                                                HeaderStyle-Width="50px" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                UniqueName="TripStatus">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FlightNum" HeaderText="Flight No." AllowFiltering="false"
                                                                HeaderStyle-Width="60px" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                UniqueName="FlightNumber">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="Requestor"
                                                                HeaderStyle-Width="70px" AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top"
                                                                UniqueName="Requestor">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ElapseTM" HeaderText="ETE" AllowFiltering="false"
                                                                HeaderStyle-Width="50px" UniqueName="ETE" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle"  HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CumulativeETE" HeaderText="Cum ETE" AllowFiltering="false"
                                                                HeaderStyle-Width="60px" UniqueName="CumulativeETE" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TotalETE" HeaderText="Total ETE" AllowFiltering="false"
                                                                HeaderStyle-Width="60px" UniqueName="TotalETE" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FlightPurpose" HeaderText="Leg Purpose" AllowFiltering="false"
                                                                UniqueName="LegPurpose" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FlightCategoryCode" HeaderText="Flight Category"
                                                                HeaderStyle-Width="70px" UniqueName="FlightCategory" AllowFiltering="false" ShowFilterIcon="false"
                                                                HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department" UniqueName="Department"
                                                                AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AuthorizationCD" HeaderText="Authorization" UniqueName="Authorization"
                                                                AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="85px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FuelLoad" HeaderText="Fuel" AllowFiltering="false"
                                                                UniqueName="FuelLoad" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="OutboundInstruction" HeaderText="Outbound Instructions"
                                                                AllowFiltering="false" UniqueName="OutboundInstructions" ShowFilterIcon="false"
                                                                HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CrewCodes" HeaderText="Crew" AllowFiltering="false"
                                                                UniqueName="CrewCodes" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CrewFullNames" HeaderText="Crew" AllowFiltering="false"
                                                                UniqueName="CrewFullNames" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PassengerCodes" HeaderText="PAX" AllowFiltering="false"
                                                                HeaderStyle-Width="70px" UniqueName="PassengerCodes" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PassengerNames" HeaderText="PAX" UniqueName="PassengerFullNames"
                                                                AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ReservationAvailable" Visible="false" HeaderText="Seats Aval"
                                                                HeaderStyle-Width="50px" UniqueName="SeatsAvailable" AllowFiltering="false" ShowFilterIcon="false"
                                                                HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PassengerCount" HeaderText="PAX Count" UniqueName="PAXCount"
                                                                AllowFiltering="false" ShowFilterIcon="false" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                                                                HeaderStyle-VerticalAlign="Top">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                        <NoRecordsTemplate>
                                                            <div oncontextmenu="dgWeeklyDetail_RowContextMenu(event, null);" onclick="IpaddgWeeklyDetail_RowContextMenu(event, null);"
                                                                style="height: 460px">
                                                            No records to Display. </div>
                                                        </NoRecordsTemplate>
                                                        <CommandItemTemplate>
                                                        </CommandItemTemplate>
                                                        <EditItemTemplate>
                                                        </EditItemTemplate>
                                                        <GroupFooterTemplate>
                                                        </GroupFooterTemplate>
                                                        <PagerTemplate>
                                                        </PagerTemplate>
                                                        <GroupFooterTemplate>
                                                        </GroupFooterTemplate>
                                                        <FilterItemStyle Height="0" />
                                                    </MasterTableView>
                                                    <ClientSettings ClientEvents-OnRowSelected="IpaddgWeeklyDetail_RowContextMenu">
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="false">
                                                        </Scrolling>
                                                        <ClientEvents OnRowContextMenu="dgWeeklyDetail_RowContextMenu"></ClientEvents>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                                <telerik:RadContextMenu ID="RadWeeklyDetailGridContextMenu" runat="server" EnableRoundedCorners="true"
                                                    EnableShadows="true" OnClientShowing="RadWeeklyDetailGridContextMenu_Showing" 
                                                    OnItemClick="RadWeeklyDetailGridContextMenu_ItemClick" 
                                                    OnClientItemClicked="RadWeeklyDetailGridContextMenu_OnClientItemClicked">
                                                    <Targets>
                                                        <telerik:ContextMenuControlTarget ControlID="dgWeeklyDetail" />
                                                    </Targets>
                                                     <Items>
                                                        <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                        <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                        <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax" />
                                                        <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                        <telerik:RadMenuItem Text="Logistics">
                                                            <Items>
                                                                <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                            </Items>
                                                        </telerik:RadMenuItem>
                                                        <telerik:RadMenuItem Text="Leg Notes" NavigateUrl="javascript:void(openWinLegNotes());"
                                                            Value="LegNotesPopup.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                        <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                        <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                        <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                        <telerik:RadMenuItem IsSeparator="True" />
                                                        <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                        <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                    </Items>
                                                </telerik:RadContextMenu>
                                                <!-- [End] Weekly Detail -->
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward"
                            Visible="false" Style='display: none;' />
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="None" Width="250px" Collapsed="true"
                            Style='display: none;'>
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="618px" ExpandMode="FullExpandedItem">
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeviewtree">
                                                <telerik:RadTreeView runat="server" ID="FleetTreeView" TriStateCheckBoxes="true">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeviewtree">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Expanded="True" Text="Filter Criteria" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                OnClientClick="javascript:openWinAdv();return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
