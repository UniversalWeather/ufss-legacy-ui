﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/SiteFV.master"
    EnableViewState="true" AutoEventWireup="true" CodeBehind="SchedulingCalendarFullView.aspx.cs"
    ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Transactions.PreFlight.ScheduleCalendar.SchedulingCalendarFullView" %>

<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Scripts/stylefixes.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="SiteBodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock1">
                    <!-- Start: Ipad Fix -->
                    <script type="text/javascript">
                        var lastArgs = null;
                        var lastContext = null;
                        var longTouchID = 0;
                        var menuShown = false;
                        function longTouch() {
                            longTouchID = 0;
                            menuShown = true;

                            var scheduler = $find("<%=RadWeeklyScheduler.ClientID %>");
                            var eventArgs = null;
                            var target = null;

                            if (lastContext.target.nodeName == 'DIV' && lastContext.target.outerHTML.indexOf('rsWrap rsLastSpacingWrapper') != -1) {

                                target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);

                                eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);

                                scheduler._raiseTimeSlotContextMenu(eventArgs);


                            } else if (lastContext.target.nodeName == 'DIV' && lastContext.target.outerHTML.indexOf('rsAptContent') != -1) {

                                target = scheduler.getAppointmentFromDomElement(lastContext.target);
                                eventArgs = new Telerik.Web.UI.SchedulerAppointmentContextMenuEventArgs(target, lastContext);
                                scheduler._raiseAppointmentContextMenu(eventArgs);

                            }
                        }

                        function handleTouchStart(e) {

                            lastContext = e;
                            lastContext.target = e.originalTarget;
                            lastContext.pageX = e.changedTouches[0].pageX;
                            lastContext.pageY = e.changedTouches[0].pageY;
                            lastContext.clientX = e.changedTouches[0].clientX;
                            lastContext.clientY = e.changedTouches[0].clientY;
                            ID = setTimeout(longTouch(), 1000);

                        }

                        function handleClick(e) {
                            if (menuShown) {
                                menuShown = false;
                                document.body.removeEventListener('click', handleClick, true);
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }

                        function handleTouchEnd(e) {
                            if (longTouchID != 0)
                                clearTimeout(longTouchID);
                            if (menuShown) {
                                document.body.addEventListener('click', handleClick, true);
                                e.preventDefault();
                            }
                        }

                        function pageLoad1() {
                            if ($telerik.isMobileSafari) {
                                var scrollArea = $telerik.$('.rsContent', $get('RadWeeklyScheduler'))[0];
                                scrollArea.addEventListener('touchstart', handleTouchStart, false);
                                scrollArea.addEventListener('touchend', handleTouchEnd, false);
                            }
                        }
                    </script>
                    <!-- End: Ipad Fix -->
                    <script type="text/javascript">
                        // To disable the Schedular to extend and display up to next Tail No.
                        Sys.Application.add_load(function () {
                            var $ = $telerik.$;

                            var headers = $('.rsVerticalHeaderTable th', scheduler);
                            var scheduler = $get('<%= RadWeeklyScheduler.ClientID %>');
                            $('.rsAllDayTable tbody tr.rsAllDayRow', scheduler).each(function (index) {
                                headers.eq(index).css('height', ($(this).innerHeight()-1) + 'px');
                            });

                            setrsRowHeight();
                        });

                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }

                        //To enable and disable rad context menu item based on appointment...
                        function RadWeeklyScheduler_OnClientAppointmentContextMenu(sender, eventArgs) {

                            //hideActiveToolTip();
                            var target = eventArgs.get_appointment();
                            var data = target._serverData;
                            var recordType = data.attributes.RecordType;
                            var isLoggedToPostFlight = data.attributes.IsLog;

                            var menu = $find('<%=RadSchedulerContextMenu1.ClientID %>');
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            if (recordType == 'T') {

                                //if trip is logged, show postflight log menu items
                                if (isLoggedToPostFlight == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + data.attributes.TripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + data.attributes.TripId + "&legNum=" + data.attributes.LegNum);
                                } else {  // else, hide
                                    menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu._findItemByText("Flight Log Legs").set_enabled(false);
                                }

                                menu._findItemByText("Postflight Flight Logs").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu.findItemByValue("Separator").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu._findItemByText("Crew").show();
                                menu._findItemByText("PAX").show();
                                menu._findItemByText("Outbound Instruction").show();
                                menu._findItemByText("Logistics").show();
                                menu._findItemByText("Leg Notes").show();


                                if (data.attributes.ShowPrivateTripMenu == "False") {
                                    if (data.attributes.IsPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }

                            } else {
                                // hide postflight log menu items
                                menu._findItemByText("Postflight Flight Logs").hide();
                                menu._findItemByText("Flight Log Legs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu._findItemByText("Crew").hide();
                                menu._findItemByText("PAX").hide();
                                menu._findItemByText("Outbound Instruction").hide();
                                menu._findItemByText("Logistics").hide();
                                menu._findItemByText("Leg Notes").hide();
                                menu._findItemByText("UWA Services").show();
                                menu._findItemByText("Preflight").show();
                                menu._findItemByText("Add New Trip").show();
                                menu._findItemByText("Fleet Calendar Entry").show();
                                menu._findItemByText("Crew Calendar Entry").show();

                            }
                        }

                        //On click of menu item on RightClick Context menu of Scheduler (not appointment)..., to get time slot details and redirect
                        function RadWeeklyScheduler_OnClientTimeSlotContextMenuItemClicking(sender, eventArgs) {

                            TimeSlotContextMenu_Redirection(sender, eventArgs);

                        }

                        function TimeSlotContextMenu_Redirection(sender, eventArgs) {
                            showPleaseWait();
                            var timeSlot = eventArgs.get_slot();
                            var resource = timeSlot.get_resource();
                            var key = null;

                            if (resource.get_key() != null) {
                                key = resource.get_key();
                            }


                            var startDate = timeSlot.get_startTime().toDateString();

                            var endDate = timeSlot.get_endTime().toDateString();

                            var menu = eventArgs.get_item();
                            var menuValue = menu.get_value();

                            if (menuValue == "FleetCalendarEntries.aspx") {
                                var oWnd = radopen("FleetCalendarEntries.aspx?tailNum=" + key + "&startDate=" + startDate + "&endDate=" + endDate + "", "RadWindow3");


                            } else if (menuValue == "CrewCalendarEntries.aspx") {

                                var oWnd = radopen("CrewCalendarEntries.aspx?crewCode=" + null + "&startDate=" + startDate + "&endDate=" + endDate + "", "RadWindow4");
                            }
                            else {
                                //do nothing;
                            }


                        }

                        //On click of menu item on RightClick Context menu of Scheduler (On appointment)..., to get time slot details and redirect

                        function RadWeeklyScheduler_OnClientAppointmentContextMenuItemClicking(sender, args) {
                            // hideActiveToolTip();
                            showPleaseWait();
                            var target = args.get_appointment();
                            var resource = target._serializedResources;
                            var key = null;
                            if (resource != null) {
                                key = resource[0].key;
                                var hdnLegNum = $get('<%= radSelectedLegNum.ClientID %>');
                                hdnLegNum.value = "1";
                                if(resource.length > 1)
                                    hdnLegNum.value = resource[1].text;
                            }
                            var selectedDate = target._start.toDateString(); ;

                            var menu = args.get_item();
                            var menuValue = menu.get_value();
                            document.getElementById('<%=radSelectedKey.ClientID%>').value = key;
                            document.getElementById('<%=radSelectedDate.ClientID%>').value = selectedDate;
                            if (menuValue == "FleetCalendarEntries.aspx") {
                                // set hidden values based on view
                                var oWnd = radopen("FleetCalendarEntries.aspx?tailNum=" + key + "&startDate=" + selectedDate + "&endDate=" + selectedDate + "", "RadWindow3");

                            } else if (menuValue == "CrewCalendarEntries.aspx") {
                                
                                var oWnd = radopen("CrewCalendarEntries.aspx?crewCode=" + null + "&startDate=" + selectedDate + "&endDate=" + selectedDate + "", "RadWindow4");

                            } else {
                                //do nothing;
                            }

                        }


                        function openWinFleetCalendarEntries() {

                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }

                        function openWinAdv() {

                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }


                        function openWinOutBoundInstructions() {

                            var oWnd = radopen("../PreflightOutboundInstruction.aspx?IsCalender=1", "RadWindow5");
                        }

                        function openWinLegNotes() {
                            var hdnLegNum = $get('<%= radSelectedLegNum.ClientID %>');
                            var oWnd = radopen("LegNotesPopup.aspx?IsCalender=1&Leg="+hdnLegNum.value, "RadWindow6");
                        }

                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow;
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                            return oWindow;
                        }

                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }

                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                       

                    </script>
                    <script type="text/javascript">

                        //Rad Date Picker
                        var currentTextBox = null;
                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox
                        function showPopup(sender, e) {
                            //this is a reference to the texbox which raised the event
                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate
                            //the selection of a date
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");


                            //this variable is used to store a reference to the date picker, which is currently
                            //active
                            currentDatePicker = datePicker;
                            //this method first parses the date, that the user entered or selected, and then
                            //sets it as a selected date to the picker
                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                                //value of the picker
                                //currentTextBox.value = args.get_newValue();

                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");

                                }
                                else
                                { return false; }
                            }
                        }
                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {

                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                //                                if (formattedDate == "") {
                                //                                    alert("Invalid Date");
                                //                                    document.getElementById("<%=tbDate.ClientID %>").focus();
                                //                                   
                                //                                    return false;
                                //                                }
                                //                                else {
                                sender.value = formattedDate;

                                //  }

                            }
                        }

                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }


                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }

                            //  alert(dateSeparator);
                            // allow dot, slash and hypen characters... if any other character, throw alert
                            return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                            // if (fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator) == false) {
                            //   alert("Invalid Date Format");

                            // }

                        }
                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }
                    </script>

                    <script type="text/javascript">
        function SelectTab()   
    {   
        var myTab = <%= TabSchedulingCalendar.ClientID %>.FindTabByValue("Week");   
        myTab.Select();   
    }   
                    </script>
                    <script type="text/javascript">
        function SelectfiltercriteriaTab()   
    {   
        var myTab = <%= FilterCriteria.ClientID %>.FindTabByValue("Week");   
        myTab.Selectfiltercriteria();
    }   
   
    
     
                    </script>
                    <script type="text/javascript">
                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }
                        function stopPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'none';
                        }

                    </script>

                     <script  type="text/javascript">

                         function hideActiveToolTip() {
                             var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                         }

                         function pageLoad() {
                             $telerik.$(".rsHorizontalHeaderTable th").each(function (index, item) {
                                 var childNode = item.childNodes[0];
                                 if (childNode != null) {
                                     var today = new Date();
                                     today.setHours(0); today.setMinutes(0); today.setSeconds(0);
                                     if (childNode.href != null) {
                                         var hrefDate = childNode.href.split('#')[1];
                                         var date = new Date(hrefDate);
                                         date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                         if (date.toDateString() == today.toDateString()) {
                                             item.style.backgroundImage = "none";
                                             item.style.backgroundColor = "yellow";
                                         }
                                     } else {
                                         var innerText = childNode.innerHTML.trim();
                                         var date = new Date(innerText);
                                         date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                         if (date.toDateString() == today.toDateString()) {
                                             item.style.backgroundImage = "none";
                                             item.style.backgroundColor = "yellow";
                                         }
                                     }
                                 }
                             });
                             pageLoad1();
                         }                    
                    </script>
                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <%--OnSelectedDateChanged="RadDatePicker1_OnSelectedDateChanged"--%>
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" Height="570px" Width="900px" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" Height="642px" Width="950px" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadWeeklyScheduler">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadWeeklyScheduler" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadScheduler2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadScheduler2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <%--<div class="globalMenu_mapping">
        <a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
        <a href="#" class="globalMenu_mapping">Preflight</a> <span class="breadcrumbCaret">
        </span><a href="#" class="globalMenu_mapping">Scheduling Calendar</a>
    </div>--%>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content fullview" style="width:100%; height:100%">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="radSelectedKey" name="radSelectedKey" runat="server" />
        <input type="hidden" id="radSelectedDate" name="radSelectedDate" runat="server" />
        <input type="hidden" id="radSelectedView" name="radSelectedView" runat="server" />
        <input type="hidden" id="radSelectedLegNum" name="radSelectedLegNum" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0" style="width:100%; height:98%;vertical-align:top;" >
            <tr>
                <td width="70%" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel170" align="right">
                    <div class="sc_date_display">
                        <span>
                            <asp:Label ID="lbStartDate" runat="server" CssClass="date"></asp:Label></span><span
                                class="padleft_5"><asp:Label ID="lbdate" runat="server" CssClass="date" Text=" - "></asp:Label></span>
                        <span class="padleft_5">
                            <asp:Label ID="lbEndDate" runat="server" CssClass="date"></asp:Label></span>
                    </div>
                </td>
                <td align="right" class="defaultview" style='display:none';>
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        OnCheckedChanged="ChkDefaultView_OnCheckedChanged" onclick="javascript:showPleaseWait();" />
                </td>
                <td align="center" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
               <td align="right" class="tdLabel20">
                    <span><a class="refresh-icon" onclick="Refresh();" href="#" title="Refresh"></a></span>                         
                </td>
                <td class="tdLabel50" align="left">                   
                          <asp:Button ID="btnClose" runat="server" CssClass="ui_nav" Text="Close" ToolTip="Close" OnClientClick="javascript:window.close();" />
                </td>
            </tr>
            <tr style="width:100%; height:97%;vertical-align:top;">
                <td colspan="6" width="100%" style="height:98%;vertical-align:top;">
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0"
                        Skin="Windows7" width = "100%" Height="98%" align="left">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="None" Height="96%" Width="100%" align="left">
                            <div class="sc_navigation">
                                <div class="sc_navigation_left">
                                    <span class="sc_weekly_selection">
                                        <asp:RadioButton ID="radFleet" Checked="true" AutoPostBack="true" OnCheckedChanged="radFleet_Checked"
                                            onclick="javascript:showPleaseWait();" GroupName="Mode" runat="server" Text="Fleet" Enabled ="false">
                                        </asp:RadioButton>
                                        <asp:RadioButton ID="radCrew" AutoPostBack="true" OnCheckedChanged="radCrew_Checked"
                                            onclick="javascript:showPleaseWait();" runat="server" GroupName="Mode" Text="Crew" Enabled ="false">
                                        </asp:RadioButton>
                                        <asp:RadioButton ID="radDetail" Visible="true" AutoPostBack="true" OnCheckedChanged="radDetail_Checked"
                                            onclick="javascript:showPleaseWait();" runat="server" GroupName="Mode" Text="Detail" Enabled ="false">
                                        </asp:RadioButton>
                                        <asp:RadioButton ID="radMain" AutoPostBack="true" runat="server" OnCheckedChanged="radMain_Checked"
                                            onclick="javascript:showPleaseWait();" GroupName="Mode" Text="Main" Enabled ="false"></asp:RadioButton>
                                    </span><span class="sc_weekly_checkbox" style='display:none';>
                                        <asp:CheckBox ID="chkStandByCrew" runat="server" Text="Standby Crew" AutoPostBack="true"
                                            onclick="javascript:showPleaseWait();" OnCheckedChanged="chkStandByCrew_OnCheckedChanged" />
                                    </span>
                                </div>
                                <div class="sc_navigation_right">
                                    <span class="act-btn"><span>
                                        <asp:Button ID="btnFirst" runat="server" ToolTip="Go Back 7 Days" CssClass="icon-first_sc"
                                            OnClick="btnFirst_click" OnClientClick="showPleaseWait();" /></span> <span>
                                                <asp:Button ID="prevButton" runat="server" OnClick="prevButton_Click" CssClass="icon-prev_sc"
                                                    ToolTip="Go Back 1 Day" OnClientClick="showPleaseWait();" /></span>
                                        <span>
                                            <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                OnClientClick="showPleaseWait();" onfocus="showPopup(this, event);" onKeyPress="return ontbDateKeyPress(this, event);"
                                                onBlur="parseDate(this, event);" MaxLength="10"></asp:TextBox></span> <span>
                                                    <asp:Button ID="nextButton" runat="server" OnClick="nextButton_Click" CssClass="icon-next_sc"
                                                        OnClientClick="showPleaseWait();" ToolTip="Go Forward 1 Day" /></span>
                                        <span>
                                            <asp:Button ID="btnPrevious" runat="server" CssClass="icon-last_sc" OnClick="btnLast_click"
                                                OnClientClick="showPleaseWait();" ToolTip="Go Forward 7 Days" /></span></span>
                                    <span>
                                        <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                            OnClientClick="showPleaseWait();" /></span>
                                </div>
                            </div>
                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%"  Height="96%"  ExpandAnimation-Type="None" runat="server" align="left">
                                <Items>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div>
                                                <asp:Panel ID="pnlWeeklyScheduler" runat="server" style="height:96%;">
                                                    <telerik:RadScheduler EnableViewState="true" ReadOnly="true" CssClass="rsHeader"
                                                        runat="server" ID="RadWeeklyScheduler" AllowEdit="true" AllowInsert="true" DataEndField="End"
                                                        DataKeyField="TripId" DataStartField="Start" DataSubjectField="Description" DayEndTime="24:00:00"
                                                        DayStartTime="00:00:00" EnableAdvancedForm="true" OverflowBehavior="Scroll" SelectedView="TimelineView"
                                                        Skin="Windows7" ShowHeader="false" RowHeight="170px" Height="585px" OnAppointmentDataBound="RadWeeklyScheduler_AppointmentDataBound"
                                                        AppointmentStyleMode="Default" Localization-HeaderToday="Today" 

                                                        OnClientAppointmentContextMenu="RadWeeklyScheduler_OnClientAppointmentContextMenu"
                                                        OnClientAppointmentContextMenuItemClicking="RadWeeklyScheduler_OnClientAppointmentContextMenuItemClicking"
                                                        OnAppointmentContextMenuItemClicked="RadWeeklyScheduler_OnAppointmentContextMenuItemClicked"
                                                        OnClientTimeSlotContextMenuItemClicking="RadWeeklyScheduler_OnClientTimeSlotContextMenuItemClicking"
                                                        OnTimeSlotContextMenuItemClicked="RadWeeklyScheduler_OnTimeSlotContextMenuItemClicked"

                                                        OnResourceHeaderCreated="RadWeeklyScheduler_ResourceHeaderCreated" 
                                                        CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu">
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AdvancedForm Modal="true" />
                                                        <Reminders Enabled="true" />
                                                        <ResourceHeaderTemplate>
                                                            <div style="text-align: left">
                                                                <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_rowheader" ReadOnly="true"
                                                                    Width="70px" Text='<%# Eval("Key") %>' Font-Bold="true" />
                                                                <br />
                                                                <asp:TextBox ID="ResourceSubHeader" runat="server" CssClass="input_sc_rowsubheader"
                                                                    ReadOnly="true" Width="70px" Text='<%# Eval("Text") %>' ToolTip='<%# Eval("Text") %>'
                                                                    Font-Size="Smaller" /><br />
                                                                <asp:TextBox ID="ResourceSubHeader1" runat="server" CssClass="input_sc_rowsubheader"
                                                                    ReadOnly="true" Width="70px"></asp:TextBox>
                                                            </div>
                                                        </ResourceHeaderTemplate>
                                                        <ResourceStyles>
                                                            <telerik:ResourceStyleMapping Type="CrewFleetCodes" />
                                                        </ResourceStyles>
                                                        <TimelineView UserSelectable="false" NumberOfSlots="7" GroupBy="CrewFleetCodes" GroupingDirection="Vertical"
                                                            ShowInsertArea="false" />
                                                        <WeekView UserSelectable="true" GroupBy="CrewFleetCodes" GroupingDirection="Vertical" />
                                                        <TimeSlotContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadWeeklyContextMenu" runat="server">
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                        Enabled="false" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Value="QuickFleetEntry" />
                                                                    <telerik:RadMenuItem Text="Quick Crew Calendar Entry" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </TimeSlotContextMenus>
                                                        <AppointmentContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenu1" runat="server">
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx"
                                                                        NavigateUrl="javascript:void(openWinOutBoundInstructions());" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" NavigateUrl="javascript:void(openWinLegNotes());" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </AppointmentContextMenus>
                                                        <TimeSlotContextMenuSettings EnableDefault="false" />
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AppointmentTemplate>
                                                            <div>
                                                                <%# Eval("Subject")%></div>
                                                            <div style="font-style: italic;">
                                                                <%# Eval("Description") %></div>
                                                        </AppointmentTemplate>
                                                    </telerik:RadScheduler>
                                                   
                                                </asp:Panel>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward"  Visible="false" style='display:none;'/>
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="None" Width="250px" Collapsed="true"  style='display:none;'>
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="618px" ExpandMode="FullExpandedItem">
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeviewtree">
                                                <telerik:RadTreeView runat="server" ID="FleetTreeView" TriStateCheckBoxes="true">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeviewtree">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Expanded="True" Text="Filter Criteria" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                OnClientClick="javascript:openWinAdv();return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </td>
            </tr>
        </table>
    </div>

        <script type="text/javascript">
            $(window).resize(function() { 
                setrsRowHeight();
            }); 
            
            function setrsRowHeight(){
                var scheduler = $find('<%=RadWeeklyScheduler.ClientID %>');
                var rowCount = $('.rsVerticalHeaderTable tr').length;
                var schHeight=$(window).height();
                setFullViewSchedulerRowHeight(scheduler, rowCount, schHeight);
            }

    </script>

</asp:Content>
