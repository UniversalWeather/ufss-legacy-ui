﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI;
using FlightPak.Web.PreflightService;
using System.Collections.ObjectModel;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class FleetCalendarEntries : ScheduleCalendarBase
    {
        public PreflightMain Trip = new PreflightMain();
        DateTime SDate;
        DateTime EDate;
        string ViewName;

        private bool FleetPageNavigated = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
            //RadDatePicker1.DateInput.DisplayDateFormat = ApplicationDateFormat;
            //DTF.ShortDatePattern = ApplicationDateFormat;    

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFleetCalendarEntries, dgFleetCalendarEntries, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFleetCalendarEntries.ClientID));

                        AdvancedFilterSettings settings = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.AdvancedFilterSettings)Session["SCUserSettings"];
                        FilterOptions = (FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria)settings.Filters;

                        pnlExternalForm.Visible = true;
                        pnlExternalForm1.Visible = false;
                        CheckAutorization(Permission.Preflight.ViewFleetCalendarEntry);
                        if (!IsPostBack)
                        {
                            Session["chkFleetConflicts"] = "false";
                            SDate = Convert.ToDateTime(Session["SCSelectedDay"]);
                            ViewName = Convert.ToString(Session["SCAdvancedTab"]);
                            EDate = GetEndDate(SDate, ViewName);

                            if ((!string.IsNullOrEmpty(Request["tailNum"])) && (Convert.ToString(Request["tailNum"]) != "null"))
                                hdndefaulttailnum.Value = Request["tailNUM"].ToString();
                            if (!string.IsNullOrEmpty(Request["startDate"]))
                            {
                                DateTime DefaultDate;
                                //if((ViewName == "Planner")||(ViewName == "Day"))
                                //    DefaultDate = Convert.ToDateTime(Request["startDate"].ToString());
                                //else
                                //    DefaultDate = Convert.ToDateTime(Request["startDate"].ToString(DTF), DTF);
                                try
                                {
                                    if ((ViewName == "Weekly") || (ViewName == "Planner") || (ViewName == "WeeklyDetail") || (ViewName == "Monthly") || (ViewName == "Day") || (ViewName == "BusinessWeek") || (ViewName == "Corporate"))
                                        DefaultDate = Convert.ToDateTime(Request["startDate"].ToString());
                                    else
                                        DefaultDate = Convert.ToDateTime(Request["startDate"].ToString(DTF), DTF);
                                }
                                catch
                                {
                                    DefaultDate = Convert.ToDateTime(Request["startDate"].ToString());
                                }

                                //DateTime DefaultDate = DateTime.Today;
                                hdndefaultSDate.Value = Convert.ToString(DefaultDate.ToShortDateString(), DTF);
                                //string dt = DefaultDate.ToShortDateString();
                                if (!string.IsNullOrEmpty(Convert.ToString(ClientCode)))
                                {
                                    tbClientCD.Text = ClientCode;
                                    tbClientCD.Enabled = false;
                                    btnClientCD.Visible = false;
                                }
                                tdGoto.Text = DefaultDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            }
                            DefaultSelection(true);
                            //GridBoundColumn SDates = dgFleetCalendarEntries.Columns.FindByUniqueName("SDate") as GridBoundColumn;
                            //DTF.ShortTimePattern = "H:mm";
                            //SDates.DataFormatString = "{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}";                           
                            //GridBoundColumn EDates = dgFleetCalendarEntries.Columns.FindByUniqueName("EDate") as GridBoundColumn;
                            //EDates.DataFormatString = "{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}";

                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void DefaultSelection(bool bind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (bind)
                {
                    dgFleetCalendarEntries.Rebind();
                }
                if (dgFleetCalendarEntries.MasterTableView.Items.Count > 0)
                {
                    string tailnum = Request.QueryString["tailNum"];
                    DateTime sdate = Convert.ToDateTime(Request.QueryString["StartDate"]).Date;
                    DateTime edate = Convert.ToDateTime(Request.QueryString["EndDate"]).Date;
                    int index = 0;
                    foreach (GridDataItem Item in dgFleetCalendarEntries.Items)
                    {
                        var ItemStartDate =Convert.ToDateTime(Item.GetDataKeyValue("StartDate")).Date;
                        var ItemEndDate = Convert.ToDateTime(Item.GetDataKeyValue("EndDate")).Date;

                        if (Item.GetDataKeyValue("TailNumber").ToString().Trim() == tailnum.Trim() && sdate == ItemStartDate && edate <= ItemEndDate)
                        {
                            index = Item.ItemIndex;
                        }
                    }
                   
                       dgFleetCalendarEntries.SelectedIndexes.Add(index);
                    Session["SelectedItem"] = dgFleetCalendarEntries.Items[index].GetDataKeyValue("TripID").ToString().Trim();
                    hdnTripIds.Value = dgFleetCalendarEntries.Items[index].GetDataKeyValue("TripID").ToString().Trim();
                    ReadOnlyForm();
                    if (Trip != null)
                    {
                        if (Trip.State != TripEntityState.Added && Trip.State != TripEntityState.Modified)
                        {
                            btnCopyFCE.Enabled = true;
                            btnCopyFCE.CssClass = "button";
                        }
                        else
                        {
                            btnCopyFCE.Enabled = false;
                            btnCopyFCE.CssClass = "button-disable";
                        }
                    }
                    else
                    {
                        btnCopyFCE.Enabled = false;
                        btnCopyFCE.CssClass = "button-disable";
                    }
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                    hdnTripIds.Value = "0";
                    btnCopyFCE.Enabled = false;
                    btnCopyFCE.CssClass = "button-disable";
                }
                GridEnable(true, true, true);
            }
        }

        protected DateTime GetEndDate(DateTime startdate, string Viewname)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startdate, Viewname))
            {
                //if (ViewName == "Planner")
                //    startdate = startdate.AddDays(30).AddSeconds(-1);
                //else if (ViewName == "Day")
                //    startdate = startdate.AddHours(24).AddMilliseconds(-1);
                //else if ((ViewName == "Weekly") || (ViewName == "WeeklyDetail") || (ViewName == "WeeklyCrew") || (ViewName == "WeeklyFleet") || (ViewName == "Corporate"))
                //    startdate = startdate.AddDays(7).AddSeconds(-1);
                //else if (ViewName == "BusinessWeek")
                //    startdate = startdate.AddDays(15).AddSeconds(-1);
                //else
                //    startdate = startdate.AddDays(42).AddSeconds(-1);
                startdate = startdate.AddYears(7).AddMonths(2);
            }
            return startdate;
        }

        protected void dgFleetCalendarEntries_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = ((e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton insertButton = ((e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);

                            LinkButton deleteButton = ((e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void dgFleetCalendarEntries_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SDate = Convert.ToDateTime(Session["SCSelectedDay"]);
                        ViewName = Convert.ToString(Session["SCAdvancedTab"]);
                        EDate = GetEndDate(SDate, ViewName);
                        //tdGoto.Text = Convert.ToString(SDate);
                        if (!String.IsNullOrEmpty(tdGoto.Text))
                            SDate = Convert.ToDateTime(tdGoto.Text.ToString(), DTF);
                        if (SDate > EDate)
                            EDate = SDate.AddDays(42);

                        using (PreflightServiceClient objservice = new PreflightServiceClient())
                        {
                            var objvalue = objservice.GetFleetCalendarEntries(FilterOptions.TimeBase.ToString(), SDate, EDate);

                            if (objvalue.ReturnFlag == true)
                            {
                                if (Convert.ToString(Session["chkFleetConflicts"]) == "true")
                                    showconflict(SDate, EDate);

                                if ((String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//No Filter
                                    dgFleetCalendarEntries.DataSource = objvalue.EntityList;
                                else
                                {
                                    string[] homeBaseIds = null;

                                    if (!string.IsNullOrEmpty(FilterOptions.HomeBaseID))
                                        homeBaseIds = FilterOptions.HomeBaseID.Split(',');                                    

                                    if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//only clientcode                       
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Trim().Contains(tbClientCD.Text));

                                    else if ((String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//only StartDate
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => x.StartDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF) || x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF));

                                    else if ((String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//only Homebase
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => homeBaseIds.Contains(x.HomebaseID.ToString()));

                                    else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//clientcode & homebase
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text) && homeBaseIds.Contains(x.HomebaseID.ToString()));

                                    else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == false))//ClientCode & Startdate
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text) && (x.StartDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF) || x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF)));

                                    else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//ClientCode & Startdate & Homebase
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => !string.IsNullOrEmpty(x.ClientCD) && x.ClientCD.Contains(tbClientCD.Text) && homeBaseIds.Contains(x.HomebaseID.ToString()) && (x.StartDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF) || x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF)));

                                    else if ((!String.IsNullOrEmpty(tbClientCD.Text)) && (!String.IsNullOrEmpty(tdGoto.Text)) && (chkHomebaseOnly.Checked == true))//Startdate & Homebase
                                        dgFleetCalendarEntries.DataSource = objvalue.EntityList.Where(x => homeBaseIds.Contains(x.HomebaseID.ToString()) && (x.StartDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF) || x.EndDate >= Convert.ToDateTime(tdGoto.Text.ToString(), DTF)));

                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }

        }

        public void showconflict(DateTime startdate, DateTime enddate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startdate, enddate))
            {

                PreflightService.FilterCriteria serviceFilterCriteria = new PreflightService.FilterCriteria();
                serviceFilterCriteria.TimeBase = (FlightPak.Web.PreflightService.TimeBase)FilterOptions.TimeBase;
                Collection<string> selectedNodes = new Collection<string>();
                selectedNodes.Add(Convert.ToString(hdnFleetID.Value));
                var inputFromFleetTree = selectedNodes;
                if (inputFromFleetTree.Count > 0)
                {
                    using (PreflightServiceClient objservice = new PreflightServiceClient())
                    {
                        var objvalue = objservice.GetFleetCalendarData(Convert.ToDateTime(Session["chksdate"]), Convert.ToDateTime(Session["chkedate"]), serviceFilterCriteria, inputFromFleetTree.ToList(), false, false, false);
                        var tripdata = objvalue.EntityList.Where(x => x.RecordType == "T").ToList();

                        List<FleetCalendarDataResult> calendarResult = new List<FleetCalendarDataResult>();
                        calendarResult = objvalue.EntityList.Where(x => x.RecordType == "M" && x.TailNum == Convert.ToString(Session["chktailnumber"]) && x.TripID != Convert.ToInt64(Session["ctripid"])).ToList();
                        calendarResult.AddRange(tripdata);

                        var conflictitems = calendarResult.ToList();
                        string strWarning = "Warning - Aircraft Conflict Exists";
                        int i = 0;
                        foreach (var items in conflictitems)
                        {
                            i++;
                            if (i == 1)
                                strWarning = strWarning + "<br/";
                            else
                                strWarning = strWarning + "========================<br/>";
                            if (items.RecordType == "M")
                                strWarning = strWarning + "<br/> Tail Number:" + Convert.ToString(Session["chktailnumber"]) + "; Fleet Calendar Entry:" + i + ";<br/> Date/Time:" + Convert.ToDateTime(Session["chksdate"]) + " to " + Convert.ToDateTime(Session["chkedate"]) + "<br/> Description: " + items.AircraftDutyDescription + "<br/>Conflicts with Fleet Calendar Entry #" + items.LegID + " Scheduled on " + items.HomeDepartureDTTM.ToString() + "<br/>";
                            else if (items.RecordType == "T")
                                strWarning = strWarning + "<br/> Tail Number:" + Convert.ToString(Session["chktailnumber"]) + "; Fleet Calendar Entry:" + i + ";<br/> Date/Time:" + Convert.ToDateTime(Session["chksdate"]) + " to " + Convert.ToDateTime(Session["chkedate"]) + "<br/> Description: " + items.AircraftDutyDescription + "<br/>Conflicts with Trip #" + items.TripNUM + " Scheduled on " + items.HomeDepartureDTTM.ToString() + "<br/>";
                        }
                        if (i > 0)
                            RadAjaxManager1.ResponseScripts.Add("radalert('" + strWarning + "',400,250);");
                        Session["chkFleetConflicts"] = "false";
                    }
                }
            }
        }

        protected void dgFleetCalendarEntries_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                DisplayEditForm();
                                GridEnable(false, true, false);
                                SelectItem();
                                btnCopyFCE.Enabled = false;
                                btnCopyFCE.CssClass = "button-disable";
                                GridPagerItem pagerItem = (GridPagerItem)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                                pagerItem.Display = false;
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFleetCalendarEntries.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                btnCopyFCE.Enabled = false;
                                btnCopyFCE.CssClass = "button-disable";
                                break;
                            case "UpdateEdited":
                                dgFleetCalendarEntries_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }
        protected void UnLockRecord()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    var TripreturnValue = CommonService.UnLock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(Session["LockTripID"]));
                    var LegreturnValue = CommonService.UnLock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(Session["LockLegID"]));
                    Session.Remove("LockTripID");
                    Session.Remove("LockLegID");
                }
            }
        }

        protected void dgFleetCalendarEntries_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (PreflightServiceClient Service = new PreflightServiceClient())
                        {
                            PreflightMain Trip = new PreflightMain();
                            Trip = Getitems();
                            long TripID = 0;
                            TripID = Trip.TripID;
                            var ReturnValue = Service.Update(Trip);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                Session["ctripid"] = ReturnValue.EntityInfo.TripID;
                                UnLockRecord();
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                GridEnable(true, true, true);
                                if (TripID != 0)
                                {
                                    SelectItem();
                                    ReadOnlyForm();
                                    if (Trip != null)
                                    {
                                        if (Trip.State != TripEntityState.Added && Trip.State != TripEntityState.Modified)
                                        {
                                            btnCopyFCE.Enabled = true;
                                            btnCopyFCE.CssClass = "button";
                                        }
                                        else
                                        {
                                            btnCopyFCE.Enabled = false;
                                            btnCopyFCE.CssClass = "button-disable";
                                        }
                                    }
                                    else
                                    {
                                        btnCopyFCE.Enabled = false;
                                        btnCopyFCE.CssClass = "button-disable";
                                    }
                                }
                                else
                                    DefaultSelection(true);

                            }
                        }
                        GridPagerItem pagerItem = (GridPagerItem)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
                finally
                {
                    //UnLockRecord();
                }
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                LinkButton insertCtl, delCtl, editCtl;
                insertCtl = (LinkButton)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                delCtl = (LinkButton)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                editCtl = (LinkButton)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                if (IsAuthorized(Permission.Preflight.AddFleetCalendarEntry))
                {
                    insertCtl.Visible = true;
                    if (add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                    insertCtl.Visible = false;

                if (IsAuthorized(Permission.Preflight.DeleteFleetCalendarEntry))
                {
                    delCtl.Visible = true;
                    if (delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                    delCtl.Visible = false;

                if (IsAuthorized(Permission.Preflight.EditFleetCalendarEntry))
                {
                    editCtl.Visible = true;
                    if (edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                    editCtl.Visible = false;
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                GridDataItem Item = dgFleetCalendarEntries.SelectedItems[0] as GridDataItem;
                if (Item.GetDataKeyValue("TailNumber") != null)
                    tbTailNumber.Text = Convert.ToString((Item).GetDataKeyValue("TailNumber"));
                else
                    tbTailNumber.Text = string.Empty;

                if (Item.GetDataKeyValue("StartDate") != null)
                {
                    string startDateTime = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    tbStartDate.Text = startDateTime;
                    RadMaskedTextBox1.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString("H:mm", CultureInfo.InvariantCulture);
                }
                else
                {
                    tbStartDate.Text = string.Empty;
                    RadMaskedTextBox1.Text = string.Empty;
                }

                if (Item.GetDataKeyValue("EndDate") != null)
                {
                    tbEndDate.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    rmtbStartTime.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString("H:mm", CultureInfo.InvariantCulture);
                }
                else
                {
                    tbEndDate.Text = string.Empty;
                    rmtbStartTime.Text = string.Empty;
                }

                if (Item.GetDataKeyValue("ICAOID") != null)
                    tbICAO.Text = Convert.ToString((Item).GetDataKeyValue("ICAOID"));
                else
                    tbICAO.Text = string.Empty;

                if (Item.GetDataKeyValue("HomeBaseCD") != null)
                    tbHomeBase.Text = Convert.ToString((Item).GetDataKeyValue("HomeBaseCD"));
                else
                    tbHomeBase.Text = string.Empty;

                if (Item.GetDataKeyValue("AircraftDutyCD") != null)
                    tbDuty.Text = Convert.ToString((Item).GetDataKeyValue("AircraftDutyCD"));
                else
                    tbDuty.Text = string.Empty;

                if (Item.GetDataKeyValue("Notes") != null)
                    tbComment.Text = Convert.ToString((Item).GetDataKeyValue("Notes"));
                else
                    tbComment.Text = string.Empty;

                if (Item.GetDataKeyValue("ClientCD") != null)
                    tbClientCode.Text = Convert.ToString((Item).GetDataKeyValue("ClientCD"));
                else
                    tbClientCode.Text = string.Empty;

                if (Item.GetDataKeyValue("NoteSec") != null)
                    tbNotes.Text = Convert.ToString((Item).GetDataKeyValue("NoteSec"));
                else
                    tbNotes.Text = string.Empty;

                Item.Selected = true;
                EnableForm(false);
            }
        }

        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbTailNumber.Enabled = Enable;
                btnTailNumber.Enabled = Enable;
                tbICAO.Enabled = Enable;
                btnICAO.Enabled = Enable;
                tbStartDate.Enabled = Enable;
                RadMaskedTextBox1.Enabled = Enable;
                tbHomeBase.Enabled = Enable;
                btnHomeBase.Enabled = Enable;
                tbEndDate.Enabled = Enable;
                rmtbStartTime.Enabled = Enable;
                tbClientCode.Enabled = false;
                btnClientCode.Enabled = false;
                if (Enable == true)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ClientId)))
                    {
                        tbClientCode.Enabled = false;
                        btnClientCode.Visible = false;
                    }
                    else
                    {
                        tbClientCode.Enabled = true;
                        btnClientCode.Visible = true;
                        btnClientCode.Enabled = true;
                    }
                }
                tbDuty.Enabled = Enable;
                btnDuty.Enabled = Enable;
                tbComment.Enabled = Enable;
                tbNotes.Enabled = Enable;
                btnCancel.Visible = Enable;
                btnSaveChanges.Visible = Enable;
                if (dgFleetCalendarEntries.Items.Count > 0)
                {
                    lnkHistory.CssClass = "history-icon";
                    lnkHistory.Enabled = true;
                }
                else
                {
                    lnkHistory.CssClass = "history-icon-disable";
                    lnkHistory.Enabled = false;
                }
            }
        }

        protected void dgFleetCalendarEntries_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (PreflightServiceClient Service = new PreflightServiceClient())
                        {
                            var ReturnValue = Service.Add(Getitems());
                            if (ReturnValue.ReturnFlag == true)
                            {
                                Session["ctripid"] = ReturnValue.EntityInfo.TripID;
                                pnlExternalForm.Visible = false;
                                dgFleetCalendarEntries.Rebind();
                                GridEnable(true, true, true);
                                DisplayInsertForm();
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void dgFleetCalendarEntries_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgFleetCalendarEntries.SelectedItems[0];
                        Session["SelectedItem"] = Convert.ToString(item.GetDataKeyValue("TripID"));
                        Session["chkFleetConflicts"] = "false";
                        //dgFleetCalendarEntries.Rebind();
                        hdnFleetID.Value = Convert.ToString((item).GetDataKeyValue("FleetID"));
                        hdnClientCode.Value = Convert.ToString(item.GetDataKeyValue("ClientID"));
                        hdnLegID.Value = Convert.ToString(item.GetDataKeyValue("LegID"));
                        hdnTripID.Value = Convert.ToString(item.GetDataKeyValue("TripID"));
                        Session["LockTripID"] = hdnTripID.Value;
                        Session["LockLegID"] = hdnLegID.Value;
                        hdnTripIds.Value = Convert.ToString(item.GetDataKeyValue("TripID"));
                        hdntripnum.Value = Convert.ToString(item.GetDataKeyValue("TripNUM"));
                        item.Selected = true;
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                        GridPagerItem pagerItem = (GridPagerItem)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        //protected void dgFleetCalendarEntries_PageIndexChanged(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        Session["chkFleetConflicts"] = "false";
        //    }
        //}

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbTailNumber.Text = string.Empty;
                tbICAO.Text = string.Empty;
                tbStartDate.Text = string.Empty;
                RadMaskedTextBox1.Text = string.Empty;
                RadMaskedTextBox1.Text = "08:00";
                tbHomeBase.Text = string.Empty;
                tbEndDate.Text = string.Empty;
                rmtbStartTime.Text = string.Empty;
                rmtbStartTime.Text = "17:00";
                tbClientCode.Text = string.Empty;
                tbDuty.Text = string.Empty;
                tbComment.Text = string.Empty;
                tbNotes.Text = string.Empty;
                lbcvTailNumber.Text = string.Empty;
                lbcvICAO.Text = string.Empty;
                lbcvHomeBase.Text = string.Empty;
                lbcvClientCode.Text = string.Empty;
                lbcvDuty.Text = string.Empty;
                hdnClientCode.Value = string.Empty;
                hdnFleetID.Value = string.Empty;
                hdnHomeBase.Value = string.Empty;
                hdnICAO.Value = string.Empty;
                hdnLegID.Value = string.Empty;
                hdnTripID.Value = string.Empty;
                hdntripnum.Value = string.Empty;
                lbcvstartdate.Text = string.Empty;
                lbcvenddate.Text = string.Empty;
            }
        }

        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                btnSaveChanges.Text = "Save";
                pnlExternalForm.Visible = true;
                hdnSaveFlag.Value = "Save";
                ClearForm();
                dgFleetCalendarEntries.Rebind();
                EnableForm(true);
                tbTailNumber.Text = hdndefaulttailnum.Value;               
                using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = ObjService.GetFleetProfileList().EntityList.Where(x => x.TailNum.Trim().ToUpper() == (hdndefaulttailnum.Value.Trim().ToUpper())).ToList();
                    if (objRetVal.Count() > 0)
                    {
                        hdnFleetID.Value = ((FlightPak.Web.FlightPakMasterService.Fleet)objRetVal[0]).FleetID.ToString();
                        var objRetFleetHomebase = ObjService.GetFleetSearch(true, "B", 0).EntityList.Where(x => x.TailNum.Trim().ToUpper() == (hdndefaulttailnum.Value.Trim().ToUpper())).ToList();
                        if (objRetFleetHomebase.Count > 0)
                        {
                            FlightPak.Web.FlightPakMasterService.GetFleetSearch objFleetSearch = ((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]);
                            hdnHomeBase.Value = Convert.ToString(objFleetSearch.HomebaseID);
                            HomeBaseId = objFleetSearch.HomebaseID ?? 0;
                            HomeAirportId = objFleetSearch.AirportID ?? 0;
                            HomeBaseCode = Convert.ToString(objFleetSearch.IcaoID);
                        }
                    }
                }
                tbStartDate.Text = Convert.ToString(Convert.ToDateTime(hdndefaultSDate.Value).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                tbEndDate.Text = Convert.ToString(Convert.ToDateTime(hdndefaultSDate.Value).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));

                if (!string.IsNullOrEmpty(Convert.ToString(HomeBaseId)))
                {
                    tbHomeBase.Text = HomeBaseCode;
                    hdnHomeBase.Value = Convert.ToString(HomeBaseId);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(HomeAirportId)))
                {
                    tbICAO.Text = HomeBaseCode;
                    hdnICAO.Value = Convert.ToString(HomeAirportId);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ClientId)))
                {
                    tbClientCode.Text = ClientCode;
                    hdnClientCode.Value = Convert.ToString(ClientId);
                    tbClientCode.Enabled = false;
                    btnClientCode.Visible = false;
                }

                btnCancel.Visible = true;
                btnSaveChanges.Visible = true;
            }
        }
        protected void dgFleetCalendarEntries_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (FleetPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }
        protected void dgFleetCalendarEntries_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["chkFleetConflicts"] = "false";
                        FleetPageNavigated = true;
                        dgFleetCalendarEntries.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedItem"] != null)
                {
                    foreach (GridDataItem Item in dgFleetCalendarEntries.MasterTableView.Items)
                    {
                        if (Item.GetDataKeyValue("TripID").ToString().Trim() == Session["SelectedItem"].ToString().Trim())
                        {
                            // GridDataItem Item = dgFleetCalendarEntries.SelectedItems[0] as GridDataItem;
                            tbTailNumber.Text = Convert.ToString((Item).GetDataKeyValue("TailNumber"));
                            tbStartDate.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            RadMaskedTextBox1.Text = ((DateTime)Item.GetDataKeyValue("StartDate")).ToString("H:mm", CultureInfo.InvariantCulture);

                            tbICAO.Text = Convert.ToString((Item).GetDataKeyValue("ICAOID"));
                            hdnICAO.Value = Convert.ToString((Item).GetDataKeyValue("AirportID"));// ICAOID as AirportID

                            tbHomeBase.Text = Convert.ToString((Item).GetDataKeyValue("HomeBaseCD"));
                            hdnHomeBase.Value = Convert.ToString((Item).GetDataKeyValue("HomebaseID"));//HomebaseID

                            tbEndDate.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            rmtbStartTime.Text = ((DateTime)Item.GetDataKeyValue("EndDate")).ToString("H:mm", CultureInfo.InvariantCulture);

                            tbDuty.Text = Convert.ToString((Item).GetDataKeyValue("AircraftDutyCD"));
                            tbComment.Text = Convert.ToString((Item).GetDataKeyValue("Notes"));
                            tbClientCode.Text = Convert.ToString((Item).GetDataKeyValue("ClientCD"));

                            if (!string.IsNullOrEmpty(Convert.ToString(UserPrincipal.Identity._clientId)))
                            {
                                tbClientCode.Enabled = false;
                                btnClientCode.Visible = false;
                            }
                            hdnClientCode.Value = Convert.ToString((Item).GetDataKeyValue("ClientID"));//ClientID
                            hdnTripID.Value = Convert.ToString((Item).GetDataKeyValue("TripID"));//TripID
                            Session["LockTripID"] = hdnTripID.Value;//To Lock the Selected PreflightMain record
                            hdnLegID.Value = Convert.ToString((Item).GetDataKeyValue("LegID"));//LegID
                            Session["LockLegID"] = hdnLegID.Value;//To Lock the Selected PreflightLeg record
                            hdnFleetID.Value = Convert.ToString((Item).GetDataKeyValue("FleetID"));//FleetID               
                            hdntripnum.Value = Convert.ToString((Item).GetDataKeyValue("TripNUM"));//TripNUM
                            tbNotes.Text = Convert.ToString((Item).GetDataKeyValue("NoteSec"));

                            //Item.Selected = true;
                            hdnSaveFlag.Value = "Update";
                            btnSaveChanges.Text = "Save";
                            btnCancel.Visible = true;
                            btnSaveChanges.Visible = true;
                            //dgFleetCalendarEntries.Rebind();               
                            EnableForm(true);
                            break;
                        }
                    }
                }

                // Lock the record

                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    var TripreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(Session["LockTripID"]));
                    var LegreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(Session["LockLegID"]));
                    if (!TripreturnValue.ReturnFlag)
                    {
                        ShowAlert(TripreturnValue.LockMessage, ModuleNameConstants.Preflight.FleetCalendarEntry);
                        return;
                    }
                    if (!LegreturnValue.ReturnFlag)
                    {
                        ShowAlert(LegreturnValue.LockMessage, ModuleNameConstants.Preflight.FleetCalendarEntry);
                        return;
                    }
                }

            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedItem"] != null)
                    {
                        string ID = Session["SelectedItem"].ToString();
                        foreach (GridDataItem item in dgFleetCalendarEntries.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("TripID").ToString().Trim() == ID)
                            {
                                //dgFleetCalendarEntries.Rebind();
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void radCalenderEntry_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        pnlExternalForm.Visible = true;
                        pnlExternalForm1.Visible = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void radHistory_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        pnlExternalForm.Visible = false;
                        pnlExternalForm1.Visible = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((string.IsNullOrEmpty(lbcvTailNumber.Text)) && (string.IsNullOrEmpty(lbcvICAO.Text)) && (string.IsNullOrEmpty(lbcvHomeBase.Text)) && (string.IsNullOrEmpty(lbcvClientCode.Text)) && (string.IsNullOrEmpty(lbcvDuty.Text)) && CheckDateRange())
                        {
                            if (hdnSaveFlag.Value == "Update")
                                (dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            else
                                (dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);

                            btnCopyFCE.Enabled = true;
                            btnCopyFCE.CssClass = "button";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected bool CheckDateRange()
        {
            bool check = true;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTimeFormatInfo DTF = new DateTimeFormatInfo();
                DTF.ShortDatePattern = ApplicationDateFormat;
                DateTime StartDate, EndDate;
                if (string.IsNullOrEmpty(tbStartDate.Text))
                {
                    check = false;
                    return check;
                }
                if (string.IsNullOrEmpty(tbEndDate.Text))
                {
                    check = false;
                    return check;
                }
                if (!string.IsNullOrEmpty(hdndefaultSDate.Value) && (Convert.ToString(hdndefaultSDate.Value) == tbStartDate.Text))
                {
                    StartDate = Convert.ToDateTime(tbStartDate.Text.ToString(), DTF);
                    StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                    StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));

                    EndDate = Convert.ToDateTime(tbEndDate.Text.ToString(), DTF);
                    EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                    EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                }
                else
                {
                    StartDate = Convert.ToDateTime(tbStartDate.Text.ToString(), DTF);
                    StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                    StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));

                    EndDate = Convert.ToDateTime(tbEndDate.Text.ToString(), DTF);
                    EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                    EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                }

                if (StartDate > EndDate)
                {
                    if (StartDate > EndDate)
                        RadAjaxManager1.ResponseScripts.Add("radalert('End Date cannot be less than Start Date.',300,150,'System Messages');");
                    //else if (StartDate == EndDate)
                    //    RadAjaxManager1.ResponseScripts.Add("radalert('Startdate and EndDate are Equal',300,150);");
                    check = false;
                }
            }
            return check;
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["chkFleetConflicts"] = "false";
                        DefaultSelection(true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UnLockRecord();
                        Session.Remove("SelectedItem");
                        Session["chkFleetConflicts"] = "false";
                        DefaultSelection(true);
                        GridPagerItem pagerItem = (GridPagerItem)dgFleetCalendarEntries.MasterTableView.GetItems(GridItemType.Pager)[1];
                        pagerItem.Display = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
                            e.Updated = dgFleetCalendarEntries;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void dgFleetCalendarEntries_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Commented by Vishwa
                        //We are not using this pager any more.
                        //if (e.Item is GridPagerItem)
                        //{
                        //    GridPagerItem pager = (GridPagerItem)e.Item;
                        //    RadComboBox PageSizeComboBox = (RadComboBox)pager.FindControl("PageSizeComboBox");
                        //    PageSizeComboBox.Visible = false;

                        //    Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                        //    lbl.Visible = false;
                        //}
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["SDate"];
                            TableCell cell1 = (TableCell)Item["EDate"];
                            DTF.ShortTimePattern = "H:mm";
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = String.Format("{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}", Convert.ToDateTime(cell.Text));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = String.Format("{0:" + DTF.ShortDatePattern + " " + DTF.ShortTimePattern + "}", Convert.ToDateTime(cell1.Text));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void dgFleetCalendarEntries_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        Session["chkFleetConflicts"] = "false";
                        if (string.IsNullOrEmpty(Convert.ToString(hdnTripID.Value)))
                        {
                            dgFleetCalendarEntries.Rebind();
                            if (dgFleetCalendarEntries.MasterTableView.Items.Count > 0)
                            {
                                dgFleetCalendarEntries.SelectedIndexes.Add(0);
                                GridDataItem Item = (GridDataItem)dgFleetCalendarEntries.Items[0];
                                hdnTripID.Value = Convert.ToString((Item).GetDataKeyValue("TripID"));//TripID
                                hdnLegID.Value = Convert.ToString((Item).GetDataKeyValue("LegID"));
                                Session["LockTripID"] = hdnTripID.Value;
                                Session["LockLegID"] = hdnLegID.Value;
                            }
                        }
                        Int64 tripid = Convert.ToInt64(hdnTripID.Value);

                        // Lock the record
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var TripreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Convert.ToInt64(Session["LockTripID"]));
                            var LegreturnValue = CommonService.Lock(EntitySet.Preflight.PreflightLeg, Convert.ToInt64(Session["LockLegID"]));
                            if (!TripreturnValue.ReturnFlag)
                            {
                                e.Item.Selected = true;
                                DefaultSelection(false);
                                ShowAlert(TripreturnValue.LockMessage, ModuleNameConstants.Preflight.FleetCalendarEntry);
                                return;
                            }
                            if (!LegreturnValue.ReturnFlag)
                            {
                                e.Item.Selected = true;
                                DefaultSelection(false);
                                ShowAlert(LegreturnValue.LockMessage, ModuleNameConstants.Preflight.FleetCalendarEntry);
                                return;
                            }
                        }

                        using (PreflightServiceClient Service = new PreflightServiceClient())
                        {
                            var ReturnValue = Service.Delete(tripid);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                ClearForm();
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
                finally
                {
                    //Unlock the record
                    UnLockRecord();
                }
            }
        }

        protected void tbTailNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbTailNumber.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetFleetByTailNumber(tbTailNumber.Text.Trim()).EntityInfo;
                                if (objRetVal == null)
                                {
                                    lbcvTailNumber.Text = "Invalid TailNumber";
                                    tbTailNumber.Focus();
                                }
                                else
                                {
                                    lbcvTailNumber.Text = "";
                                    hdnFleetID.Value = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)objRetVal).FleetID.ToString();

                                    var objRetFleetHomebase = ObjService.GetFleetSearch(true, "B", 0).EntityList.Where(x => x.TailNum.Trim().ToUpper() == (tbTailNumber.Text.Trim().ToUpper())).ToList();
                                    if (objRetFleetHomebase.Count > 0)
                                    {
                                        hdnHomeBase.Value = Convert.ToString(((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]).HomebaseID);
                                        hdnICAO.Value = Convert.ToString(((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]).AirportID);
                                        tbICAO.Text = tbHomeBase.Text = Convert.ToString(((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]).IcaoID);

                                        HomeBaseId = Convert.ToInt64(Convert.ToString(((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]).HomebaseID));
                                        HomeAirportId = Convert.ToInt64(Convert.ToString(((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]).AirportID));
                                        HomeBaseCode = Convert.ToString(((FlightPak.Web.FlightPakMasterService.GetFleetSearch)objRetFleetHomebase[0]).IcaoID);
                                    }                                                                       
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void tbICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbICAO.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbICAO.Text.Trim()).EntityList;

                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvICAO.Text = "Invalid ICAO";
                                    tbICAO.Focus();
                                }
                                else
                                {
                                    lbcvICAO.Text = "";
                                    hdnICAO.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportID.ToString();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbHomeBase.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomeBase.Text.ToUpper().Trim())).ToList();

                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvHomeBase.Text = "Invalid HomeBase";
                                    tbHomeBase.Focus();
                                }
                                else
                                {
                                    lbcvHomeBase.Text = "";
                                    hdnHomeBase.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster)objRetVal[0]).HomebaseID.ToString();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbClientCode.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objScheduler.GetClientByClientCD(tbClientCode.Text.Trim()).EntityList;
                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvClientCode.Text = "Invalid ClientCode";
                                    tbClientCode.Focus();
                                }
                                else
                                {
                                    lbcvClientCode.Text = "";
                                    hdnClientCode.Value = ((FlightPak.Web.FlightPakMasterService.ClientByClientCDResult)objRetVal[0]).ClientID.ToString();
                                }
                            }
                        }
                        else
                        {
                            lbcvClientCode.Text = "";
                            hdnClientCode.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        protected void tbDuty_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDuty.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = ObjService.GetAircraftDuty().EntityList.Where(x => x.AircraftDutyCD.Trim().ToUpper() == (tbDuty.Text.Trim().ToUpper())).ToList();
                                if (objRetVal.Count() <= 0)
                                {
                                    lbcvDuty.Text = "Invalid Duty";
                                    tbDuty.Focus();
                                }
                                else
                                {
                                    lbcvDuty.Text = "";
                                    tbComment.Text = ((FlightPak.Web.FlightPakMasterService.AircraftDuty)objRetVal[0]).AircraftDutyDescription.ToString();
                                    RadMaskedTextBox1.Text = Convert.ToString(((FlightPak.Web.FlightPakMasterService.AircraftDuty)objRetVal[0]).DefaultStartTM);
                                    rmtbStartTime.Text = Convert.ToString(((FlightPak.Web.FlightPakMasterService.AircraftDuty)objRetVal[0]).DefualtEndTM);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }

        private PreflightMain Getitems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    Session["chkFleetConflicts"] = "true";
                    Session["chktailnumber"] = tbTailNumber.Text;
                    string TailNum = tbTailNumber.Text;
                    Int64 homebaseid = Convert.ToInt64(hdnHomeBase.Value);
                    DateTime StartDate, EndDate;
                    if (!string.IsNullOrEmpty(hdndefaultSDate.Value) && (Convert.ToString(hdndefaultSDate.Value) == tbStartDate.Text) && hdnSaveFlag.Value != "Update")
                    {
                        StartDate = Convert.ToDateTime(tbStartDate.Text.ToString());
                        StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                        StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));
                        Session["chksdate"] = StartDate;

                        EndDate = Convert.ToDateTime(tbEndDate.Text.ToString());
                        EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                        EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                        Session["chkedate"] = EndDate;
                    }
                    else
                    {
                        StartDate = Convert.ToDateTime(tbStartDate.Text.ToString(), DTF);
                        StartDate = StartDate.AddHours(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(0, 2)));
                        StartDate = StartDate.AddMinutes(Convert.ToInt32(RadMaskedTextBox1.Text.Substring(2, 2)));
                        Session["chksdate"] = StartDate;

                        EndDate = Convert.ToDateTime(tbEndDate.Text.ToString(), DTF);
                        EndDate = EndDate.AddHours(Convert.ToInt32(rmtbStartTime.Text.Substring(0, 2)));
                        EndDate = EndDate.AddMinutes(Convert.ToInt32(rmtbStartTime.Text.Substring(2, 2)));
                        Session["chkedate"] = EndDate;
                    }
                    Int64 icaoid = Convert.ToInt64(hdnICAO.Value);
                    string duty = Convert.ToString(tbDuty.Text);
                    string notes = Convert.ToString(tbComment.Text);

                    string notesec = Convert.ToString(tbNotes.Text);

                    Int64 Clientid = Convert.ToInt64("0");
                    if (!string.IsNullOrEmpty(hdnClientCode.Value))
                        Clientid = Convert.ToInt64(hdnClientCode.Value);

                    Int64 fleetid = Convert.ToInt64("0");
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = ObjService.GetFleetProfileList().EntityList.Where(x => x.TailNum.Trim().ToUpper() == (tbTailNumber.Text.Trim().ToUpper())).ToList();
                        hdnFleetID.Value = ((FlightPak.Web.FlightPakMasterService.Fleet)objRetVal[0]).FleetID.ToString();
                    }
                    if (!string.IsNullOrEmpty(hdnFleetID.Value))
                        fleetid = Convert.ToInt64(hdnFleetID.Value);
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Trip = new PreflightMain();
                        Trip.FleetID = fleetid;
                        //12/26/2012 changing notes  to trip description
                        Trip.TripDescription = notes;
                        Trip.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                        Trip.RecordType = "M";
                        Trip.IsDeleted = false;
                        if (Clientid == 0)
                            Trip.ClientID = null;
                        else
                            Trip.ClientID = Clientid;
                        Trip.LastUpdUID = UserPrincipal.Identity._name;
                        Trip.LastUpdTS = System.DateTime.UtcNow;
                        Trip.FleetCalendarNotes = notesec;

                        PreflightLeg Leg = new PreflightLeg();
                        Leg.LegNUM = Convert.ToInt64("1");

                        string timebase = FilterOptions.TimeBase.ToString();
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            if (timebase.ToUpper() == "LOCAL")
                            {
                                Leg.DepartureDTTMLocal = StartDate;
                                Leg.ArrivalDTTMLocal = EndDate;
                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, true, true);
                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, true, true);
                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), StartDate, false, false);
                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), EndDate, false, false);
                            }
                            else if (timebase.ToUpper() == "UTC")
                            {
                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, false, false);
                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, false, false);
                                Leg.DepartureGreenwichDTTM = StartDate;
                                Leg.ArrivalGreenwichDTTM = EndDate;
                                Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), StartDate, false, false);
                                Leg.HomeArrivalDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnHomeBase.Value), EndDate, false, false);
                            }
                            else
                            {
                                Leg.DepartureDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, false, false);
                                Leg.ArrivalDTTMLocal = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, false, false);
                                Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), StartDate, true, true);
                                Leg.ArrivalGreenwichDTTM = objDstsvc.GetGMT(Convert.ToInt64(hdnICAO.Value), EndDate, true, true);
                                Leg.HomeDepartureDTTM = StartDate;
                                Leg.HomeArrivalDTTM = EndDate;
                            }
                        }
                        Leg.DepartICAOID = icaoid;
                        Leg.ArriveICAOID = icaoid;
                        Leg.DutyTYPE = duty;
                        Leg.IsDeleted = false;

                        if (Clientid == 0)
                            Leg.ClientID = null;
                        else
                            Leg.ClientID = Clientid;
                        Leg.LastUpdUID = UserPrincipal.Identity._name;
                        Leg.LastUpdTS = System.DateTime.UtcNow;
                        if (hdnSaveFlag.Value == "Update")
                        {
                            Int64 tripid = Convert.ToInt64(hdnTripID.Value);
                            Int64 legid = Convert.ToInt64(hdnLegID.Value);
                            Trip.TripID = tripid;
                            Trip.TripNUM = Convert.ToInt64(hdntripnum.Value);
                            Trip.State = TripEntityState.Modified;

                            Leg.LegID = legid;
                            Leg.TripID = tripid;
                            Leg.State = TripEntityState.Modified;
                        }
                        else
                        {
                            Trip.State = TripEntityState.Added;
                            Leg.State = TripEntityState.Added;
                        }
                        Trip.PreflightLegs = new List<PreflightLeg>();
                        Trip.PreflightLegs.Add(Leg);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
            return Trip;
        }

        protected void tdGoto_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(tdGoto.Text))
                            {
                                DateTime inputDate = Convert.ToDateTime(tdGoto.Text.ToString(DateTimeInfo), DateTimeInfo);
                                if (inputDate < MinDate || inputDate > MaxDate)
                                {
                                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid Goto date range: " + tdGoto.Text + "', 200, 100); return false;});", true);
                                    tdGoto.Text = DateTime.Today.Date.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    return;
                                }

                            }
                        }
                        catch (FormatException)
                        { // Manually handled
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid Goto date format: " + tdGoto.Text + "', 200, 100); return false;});", true);
                            tdGoto.Text = DateTime.Today.Date.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return;
                        }
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.CrewCalendarEntry);
                }
            }
        }

        protected void tbStartDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbStartDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbStartDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate < MinDate || inputDate > MaxDate)
                    {
                        RadAjaxManager1.ResponseScripts.Add("radalert('Invalid Start date range: " + tbStartDate.Text + "',200,100);");
                    }
                    else
                        lbcvstartdate.Text = "";
                }
                else
                {
                    lbcvstartdate.Text = "Start Date is Required.";
                }
            }
            catch (FormatException)
            { //Manually handled
                RadAjaxManager1.ResponseScripts.Add("radalert('Invalid Start date format: " + tbStartDate.Text + "',200,100);");
                tbStartDate.Text = "";
            }
        }
        protected void tbEndDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbEndDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbEndDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate < MinDate || inputDate > MaxDate)
                    {
                        RadAjaxManager1.ResponseScripts.Add("radalert('Invalid End date range: " + tbEndDate.Text + "',200,100);");
                    }
                    else
                        lbcvenddate.Text = "";
                }
                else
                {
                    lbcvenddate.Text = "End Date is Required.";
                }
            }
            catch (FormatException)
            { //Manually handled
                RadAjaxManager1.ResponseScripts.Add("radalert('Invalid End date format: " + tbEndDate.Text + "',200,100);");
                tbEndDate.Text = "";
            }
        }

        protected void tbClientCD_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["chkFleetConflicts"] = "false";
                        DefaultSelection(true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.FleetCalendarEntry);
                }
            }
        }
    }
}