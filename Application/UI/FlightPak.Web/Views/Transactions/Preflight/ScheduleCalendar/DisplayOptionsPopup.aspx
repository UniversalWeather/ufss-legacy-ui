﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="DisplayOptionsPopup.aspx.cs"
    EnableViewState="true" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.DisplayOptionsPopup"
    ClientIDMode="AutoID" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Display Options</title>
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scr1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        function confirmCopyCallBackFn(arg) {
            if (arg == true) {
                document.getElementById('<%=btnCopyYes.ClientID%>').click();
            }
            else {
                document.getElementById('<%=btnCopyNo.ClientID%>').click;
            }
        }




        function RadWindowClose(argument) {

            GetRadWindow().BrowserWindow.location = argument;
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }
        function showPleaseWait() {

            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
        }
        
    </script>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" />
    <table width="300px" cellpadding="0" cellspacing="0">
        <%-- <div class="RadAjax_Sunset raDiv" runat="server" id="PleaseWait" style="display: none;
            text-align: center; color: White; vertical-align: middle; z-index: 9999; position: relative; top: 150px;">
            <img src="../../../../../App_Themes/Default/images/loading.gif" alt="Please wait..." />
        </div>
        --%>
        <div class="pageloader" runat="server" id="PleaseWait">
        </div>
        <tr>
            <td align="left">
                <table cellpadding="0" cellspacing="0" class="box1">
                    <tr>
                        <td>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table class="preflight_800" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left">
                                                            <div class="nav-space">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <telerik:RadPanelBar runat="server" ExpandMode="SingleExpandedItem" ID="pnlNavigation"
                                                                OnItemClick="pnlNavigation_ItemClick" CssClass="RadNavMenu" ExpandAnimation-Type="None"
                                                                CollapseAnimation-Type="none">
                                                                <Items>
                                                                    <telerik:RadPanelItem Text="Views" Font-Bold="true" CssClass="RadNavMenu" Expanded="true"
                                                                        Value="-1">
                                                                        <Items>
                                                                            <telerik:RadPanelItem Text="Planner" Value="0" Selected="true">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Weekly Fleet" Value="1">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Weekly Crew" Value="2">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Monthly" Value="3">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Weekly" Value="4">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Business Week" Value="5">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Day" Value="6">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Corporate" Value="7">
                                                                            </telerik:RadPanelItem>
                                                                            <telerik:RadPanelItem Text="Weekly Detail" Value="8">
                                                                            </telerik:RadPanelItem>
                                                                        </Items>
                                                                    </telerik:RadPanelItem>
                                                                </Items>
                                                                <ExpandAnimation Type="None"></ExpandAnimation>
                                                            </telerik:RadPanelBar>
                                                        </td>
                                                        <td class="tdLabel10">
                                                        </td>
                                                        <td valign="top" align="left" class="tdLabel300">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Panel ID="PanelPlannerDisplay" runat="server">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <b>Planner Display Options:</b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div class="nav-6">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <fieldset>
                                                                                <table width="278px" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <asp:RadioButton ID="radioBoth" runat="server" Text="Both" GroupName="PlanerDisp" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:RadioButton ID="radioFleetOnly" runat="server" Text="Fleet Only" GroupName="PlanerDisp" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:RadioButton ID="radioCrewOnly" runat="server" Text="Crew Only" GroupName="PlanerDisp" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelWeeklyFleetCrewDisplayOption" runat="server">
                                                                <strong>
                                                                    <asp:Label ID="LabelWeeklyFleetCrewDisplayOption" Text="Weekly Fleet - Display Options"
                                                                        runat="server" />
                                                                </strong>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div class="nav-6">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <fieldset title="Depart/ArriveInfo">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radICAODep" runat="server" Text="ICAO Dep/Arr Info" GroupName="Mode" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radCityDep" runat="server" Text="City Dep/Arr Info" GroupName="Mode" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radAirportNameDep" runat="server" Text="Airport Name Dep/Arr Info"
                                                                                    GroupName="Mode" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </asp:Panel>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Panel ID="PanelDayWeeklyDetailSortOption" runat="server">
                                                                <strong>
                                                                    <asp:Label ID="lblDayWeeklyDetailSortOption" Text="Day - Sort Options" runat="server" />
                                                                </strong>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div class="nav-space">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <fieldset>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radioTailNumber" runat="server" Text="Tail No." GroupName="DaySort"
                                                                                    OnCheckedChanged="radTailNumber_Checked" AutoPostBack="true" onclick="javascript:showPleaseWait();" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="radioDepatureDateTime" runat="server" Text="Departure Date/Time"
                                                                                    onclick="javascript:showPleaseWait();" OnCheckedChanged="radDepatureDateTime_Checked"
                                                                                    AutoPostBack="true" GroupName="DaySort" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </asp:Panel>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Panel ID="PanelActivity" runat="server">
                                                                <fieldset>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkShowTrip" runat="server" Text="Show Trip No." AutoPostBack="true"
                                                                                                OnCheckedChanged="chkShowTrip_OnCheckedChanged" onclick="javascript:showPleaseWait();" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkActivityOnly" runat="server" Text="Activity Only" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkFirstLastLeg" runat="server" Text="First/Last Legs" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkCrew" runat="server" Text="Crew" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkShowHomeBase" runat="server" Text="Show Home Base" Visible="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkPaxFullName" runat="server" Text="PAX Full Name" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkShowTripNumber" runat="server" Text="Show Trip No." />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkCrewFullName" runat="server" Text="Crew Full Name" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkPax" runat="server" Text="PAX" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkCrewCalActivity" runat="server" Text="Crew Calendar Activity" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkDisplayRONPax" runat="server" Text="Display RON Pax" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </asp:Panel>
  								                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>															
                                                            <asp:Panel ID="PanelDisplayLegFleet" runat="server">
                                                                <fieldset>
                                                                    <legend>Fleet</legend>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radAllLegs" runat="server" Text="Show all legs" GroupName="DisplayLegFleet" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radTripLastLeg" runat="server" Text="Show last leg per trip" GroupName="DisplayLegFleet" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:RadioButton ID="radDayLastLeg" runat="server" Text="Show last leg per day" GroupName="DisplayLegFleet" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </asp:Panel>	
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>																
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Panel ID="hoverOption" runat="server">
                                                                <fieldset>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="toggleHoverOption" runat="server" Text="Show details on hover" AutoPostBack="true"
                                                                                                 onclick="javascript:showPleaseWait();" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </asp:Panel>                                                        
                                                        </td> 
                                                        <td valign="top" align="left" class="tdLabel300">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr valign="bottom">
                                                                    <td valign="bottom">
                                                                        <fieldset>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkAircraftColors" runat="server" Text="Aircraft Colors" OnCheckedChanged="chkAircraftColors_OnCheckedChanged"
                                                                                            onclick="javascript:showPleaseWait();" AutoPostBack="true" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkNonReversedColors" runat="server" Text="Non-Reversed Colors" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkShowTripStatus" runat="server" Text="Show Trip Status" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkDisplayRedEyeFlights" runat="server" Text="Display Red-eye Flights in Continuation" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div class="nav-space">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="bottom">
                                                                    <td valign="bottom">
                                                                        <fieldset>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td width="50%" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkBlackWhiteColor" runat="server" Text="Black/White Color" OnCheckedChanged="chkBlackWhiteColor_OnCheckedChanged"
                                                                                                        onclick="javascript:showPleaseWait();" AutoPostBack="true" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkPaxCount" runat="server" Text="PAX Count" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkSaveColoumnWidths" runat="server" Text="Save Column Widths"
                                                                                                        Visible="false" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkFlightNumber" runat="server" Text="Flight No." />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkLegPurpose" runat="server" Text="Leg Purpose" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkTripPurpose" runat="server" Text="Trip Purpose" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkSeatsAvailable" runat="server" Text="Seats Available" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="50%" valign ="top">
                                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkETE" runat="server" Text="ETE" OnCheckedChanged="radETE_Checked"
                                                                                                        onclick="javascript:showPleaseWait();" AutoPostBack="true" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkCummulativeETE" runat="server" Text="Cumulative ETE" OnCheckedChanged="radCummulativeETE_Checked"
                                                                                                        onclick="javascript:showPleaseWait();" AutoPostBack="true" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkTotalETE" runat="server" Text="Total Trip ETE" OnCheckedChanged="radTotalETE_Checked"
                                                                                                        onclick="javascript:showPleaseWait();" AutoPostBack="true" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkNoLineSeparator" runat="server" Text="No Line Separator" Visible="false" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkRequestor" runat="server" Text="Requestor" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkDepartment" runat="server" Text="Department" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkAuthorization" runat="server" Text="Authorization" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkFlightCategory" runat="server" Text="Flight Category" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <div class="nav-space">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <asp:Panel ID="PanelDisplayTailCrew" runat="server">
                                                                            <fieldset>
                                                                                <legend>Crew</legend>
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:RadioButton ID="radTripLastTail" runat="server" Text="Show tail numbers for all trips" GroupName="DisplayTailCrew" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:RadioButton ID="radDayLastTail" runat="server" Text="Show tail number for last trip of day" GroupName="DisplayTailCrew" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Button ID="btnApply" Text="Apply for Current Session" runat="server" ValidationGroup="save"
                                                                CssClass="button" OnClick="ApplyWithOutSave_Click" OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Button ID="btnSaveDisplay" Text="Save Defaults" runat="server" ValidationGroup="save"
                                                                CssClass="button" OnClick="SaveDefault_Click" OnClientClick="showPleaseWait();" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnSysDisplay" Text="Reset To System Defaults" runat="server" CssClass="button"
                                                                OnClick="RestoreSystemDefault_Click" OnClientClick="showPleaseWait();" />
                                                            <asp:HiddenField ID="hdDisplay" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </td>
                    </tr>
                </table>
                <table id="tblHidden" style="display: none;">
                    <tr>
                        <td>
                            <asp:Button ID="btnCopyYes" runat="server" Text="Button" OnClick="btnCopyYes_Click"
                                OnClientClick="showPleaseWait();" />
                            <asp:Button ID="btnCopyNo" runat="server" Text="Button" OnClick="btnCopyNo_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
