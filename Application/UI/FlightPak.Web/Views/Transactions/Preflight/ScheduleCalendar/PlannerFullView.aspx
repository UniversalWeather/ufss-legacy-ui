﻿<%@ Page Language="C#" MasterPageFile="~/Framework/Masters/SiteFV.Master" AutoEventWireup="true"
    EnableViewState="true" CodeBehind="PlannerFullView.aspx.cs" ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.PlannerFullView" %>

<%@ Register Src="~/UserControls/UCScheduleCalendarHeader.ascx" TagName="Calendar"
    TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/UCFilterCriteria.ascx" TagName="FilterCriteria"
    TagPrefix="UCPreflight" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Scripts/stylefixes.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #ctl00_MainContent_RadPanelBar3_i0_RadPlannerCrewScheduler{height: auto!important}

    </style>
</asp:Content>
<asp:Content ID="SiteBodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadStyleSheetManager runat="server" ID="RadStyleSheetManager1">
        <CdnSettings TelerikCdn="Disabled" />
    </telerik:RadStyleSheetManager>
    <table>
        <tr>
            <td>
                <telerik:RadCodeBlock runat="server" ID="RadCodeBlock2">
                    <%--IPad fix - Start--%>
                    <script type="text/javascript">

                        $(document).ready(function() {
                            var fleetscroll = $("#<%=RadPlannerFleetScheduler.ClientID %>"+" .rsContentScrollArea");
                            var crewscroll = $("#<%=RadPlannerCrewScheduler.ClientID %>"+" .rsContentScrollArea");

                            fleetscroll.scroll(function() {
                                crewscroll.scrollLeft($(this).scrollLeft());
                            });

                            crewscroll.scroll(function() {
                                fleetscroll.scrollLeft($(this).scrollLeft());
                            });
                        });

                        function longTouch(sch) {
                            longTouchID = 0;
                            menuShown = true;
                            var scheduler = null;
                            if (sch == 'RadPlannerFleetScheduler')
                                scheduler = $find("<%=RadPlannerFleetScheduler.ClientID %>");
                            else if (sch == 'RadPlannerCrewScheduler')
                                scheduler = $find("<%=RadPlannerCrewScheduler.ClientID %>");
                            var eventArgs = null;
                            var target = null;

                            if (lastContext.target.nodeName == 'DIV' && lastContext.target.outerHTML.indexOf('rsWrap rsLastSpacingWrapper') != -1) {

                                target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);

                                eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);

                                scheduler._raiseTimeSlotContextMenu(eventArgs);


                            } else if (lastContext.target.nodeName == 'DIV') {

                                target = scheduler.getAppointmentFromDomElement(lastContext.target);
                                eventArgs = new Telerik.Web.UI.SchedulerAppointmentContextMenuEventArgs(target, lastContext);
                                scheduler._raiseAppointmentContextMenu(eventArgs);

                            }
                        }
                        function handleTouchStart1(e) {
                            lastContext = e;
                            lastContext.target = e.originalTarget;
                            lastContext.pageX = e.changedTouches[0].pageX;
                            lastContext.pageY = e.changedTouches[0].pageY;
                            lastContext.clientX = e.changedTouches[0].clientX;
                            lastContext.clientY = e.changedTouches[0].clientY;
                            ID = setTimeout(longTouch('RadPlannerFleetScheduler'), 1000);

                        }
                        function handleTouchStart2(e) {
                            lastContext = e;
                            lastContext.target = e.originalTarget;
                            lastContext.pageX = e.changedTouches[0].pageX;
                            lastContext.pageY = e.changedTouches[0].pageY;
                            lastContext.clientX = e.changedTouches[0].clientX;
                            lastContext.clientY = e.changedTouches[0].clientY;
                            ID = setTimeout(longTouch('RadPlannerCrewScheduler'), 1000);

                        }
                        function handleClick1(e) {
                            if (menuShown) {
                                menuShown = false;
                                document.body.removeEventListener('click', handleClick1, true);
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }
                        function handleClick2(e) {
                            if (menuShown) {
                                menuShown = false;
                                document.body.removeEventListener('click', handleClick2, true);
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }
                        function handleTouchEnd1(e) {
                            if (longTouchID != 0)
                                clearTimeout(longTouchID);
                            if (menuShown) {
                                document.body.addEventListener('click', handleClick1, true);
                                e.preventDefault();
                            }
                        }
                        function handleTouchEnd2(e) {
                            if (longTouchID != 0)
                                clearTimeout(longTouchID);
                            if (menuShown) {
                                document.body.addEventListener('click', handleClick2, true);
                                e.preventDefault();
                            }
                        }
                        function pageLoad1() {
                            if ($telerik.isMobileSafari) {
                                var scrollArea1 = $get('<%= RadPlannerFleetScheduler.ClientID%>');
                                scrollArea1.addEventListener('touchstart', handleTouchStart1, false);
                                scrollArea1.addEventListener('touchend', handleTouchEnd1, false);
                                var scrollArea2 = $get('<%= RadPlannerCrewScheduler.ClientID%>');
                                scrollArea2.addEventListener('touchstart', handleTouchStart2, false);
                                scrollArea2.addEventListener('touchend', handleTouchEnd2, false);
                            }
                        }
                    </script>
                    <%--IPad fix - End--%>
                    <script type="text/javascript">

                        function fnOpenFleetPopup(page, radwindow) {
                            var oWnd = radopen("../../../Views/Settings/Fleet/FleetProfileCatalog.aspx?IsPopup=1", "radFleetProfileCRUDPopup");
                        }


                        //                        $(window).load(function () {
                        //                            showPleaseWait();
                        //                            setTimeout(function () {
                        //                                showPleaseWait();
                        //                                document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'none';
                        //                            }, 3000);

                        //                        });

                        function showPleaseWait() {

                            document.getElementById("<%=PleaseWait.ClientID %>").style.display = 'block';
                        }

                        function openWinLegend() {
                            var oWnd = radopen("Legend.aspx", "RadWindow1");
                        }

                        function openWinAdv() {
                            var oWnd = radopen("DisplayOptionsPopup.aspx", "RadWindow2");
                        }

                        function openWinFleet(TailNum) {
                            window.radopen("../../../Settings/Fleet/FleetProfileCatalog.aspx?IsPopup=&ScTailNum=" + TailNum, "radFleetProfilePopup");
                            return false;

                        }

                        //On click of menu item on RightClick Context menu of Scheduler (not appointment)..., to get time slot details and redirect
                        //                        function RadPlannerFleetScheduler_OnClientTimeSlotContextMenuItemClicking(sender, eventArgs) {
                        //                            debugger;
                        //                            var timeSlot = eventArgs.get_slot();                           
                        //                            var resource = timeSlot.get_resource();                           
                        //                            var key = null;                            
                        //                            if (resource.get_key() != null) {
                        //                                key = resource.get_key();
                        //                            }
                        //                           
                        //                            var startDate = timeSlot.get_startTime().toDateString();
                        //                            var endDate = timeSlot.get_endTime().toDateString();

                        //                            var menu = eventArgs.get_item();
                        //                            var menuValue = menu.get_value();
                        //                           
                        //                            if (menuValue == "FleetCalendarEntries.aspx") {
                        //                                alert(menuValue);
                        //                                var oWnd = radopen("FleetCalendarEntries.aspx?tailNum=" + key + "&startDate=" + startDate + "&endDate=" + endDate + "", "RadWindow3");
                        //                            } else if (menuValue == "CrewCalendarEntries.aspx") {

                        //                                var oWnd = radopen("CrewCalendarEntries.aspx?crewCode=" + null + "&startDate=" + startDate + "&endDate=" + endDate + "", "RadWindow4");
                        //                            }
                        ////                            else {
                        ////                                alert(menu.get_navigateUrl());
                        ////                                window.location.href = menu.get_navigateUrl();
                        ////                            }
                        //                        }
                        function RadPlannerCrewScheduler_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();
                            var timeSlot = eventArgs.get_slot();
                            var resource = timeSlot.get_resource();
                            var key = null;

                            if (resource.get_key() != null) {
                                key = resource.get_key();
                            }

                            var startDate = timeSlot.get_startTime().toDateString();
                            var endDate = timeSlot.get_endTime().toDateString();

                            var menu = eventArgs.get_item();
                            var menuValue = menu.get_value();

                            if (menuValue == "FleetCalendarEntry") {
                                var oWnd = radopen("FleetCalendarEntries.aspx?tailNum=" + null + "&startDate=" + startDate + "&endDate=" + endDate + "", "RadWindow3");
                            } else if (menuValue == "CrewCalendarEntry") {

                                var oWnd = radopen("CrewCalendarEntries.aspx?crewCode=" + key + "&startDate=" + startDate + "&endDate=" + endDate + "", "RadWindow4");
                            }
                            else {

                                window.location.href = menu.get_navigateUrl();
                            }
                        }

                        function RadPlannerFleetScheduler_OnClientTimeSlotContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }

                        function getAppoinmentLegNum(eventArgs)
                        {
                            var target = eventArgs.get_appointment();
                            var resource = target._serializedResources;
                            var hdnLegNum = $get('<%=radSelectedLegNum.ClientID %>');
                            var hdnStartDate = $get('<%=radSelectedStartDate.ClientID %>');
                            hdnLegNum.value = "1";
                            if (resource != null && resource.length > 1) {
                                hdnLegNum.value = resource[1].text;
                                if (resource.length > 2) {
                                    hdnStartDate.value = resource[2].text;
                                }
                            }
                        }
                        //To enable and disable rad context menu item based on appointment...
                        function RadPlannerCrewScheduler_OnClientAppointmentContextMenu(sender, eventArgs) {

                            var menu = $find('<%=RadPlannerCrewSchedulerContextMenu1.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);
                        }

                        function RadPlannerFleetScheduler_OnClientAppointmentContextMenu(sender, eventArgs) {
                            var menu = $find('<%=RadPlannerFleetSchedulerContextMenu1.ClientID %>');
                            ShowAndHideContextMenuItem(sender, eventArgs, menu);
                        }

                        function ShowAndHideContextMenuItem(sender, eventArgs, menu) {
                            hideActiveToolTip();
                            menu._findItemByText("Preflight").set_enabled(true);
                            menu._findItemByText("Crew").set_enabled(true);
                            menu._findItemByText("PAX").set_enabled(true);
                            menu._findItemByText("Logistics").set_enabled(true);

                            var target = eventArgs.get_appointment();
                            var data = target._serverData;

                            var recordType = data.attributes.RecordType;
                            var isLoggedToPostFlight = data.attributes.IsLog;
                            if (recordType == 'T') {
                                //if trip is logged, show postflight log menu items
                                if (isLoggedToPostFlight == "True") {
                                    var postFlightLogMenuItem = menu._findItemByText("Postflight Flight Logs");
                                    postFlightLogMenuItem.set_enabled(true);
                                    postFlightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightMain.aspx?seltab=main&reqFrom=SC&tripId=" + data.attributes.TripId);
                                    var flightLogMenuItem = menu._findItemByText("Flight Log Legs");
                                    menu._findItemByText("Flight Log Legs").set_enabled(true);
                                    flightLogMenuItem.set_navigateUrl("../../PostFlight/PostFlightLegs.aspx?seltab=legs&reqFrom=SC&tripId=" + data.attributes.TripId + "&legNum=" + data.attributes.LegNum);
                                } else {  // else, hide
                                    menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                    menu._findItemByText("Flight Log Legs").set_enabled(false);
                                }
                                menu._findItemByText("Postflight Flight Logs").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu.findItemByValue("Separator").show();
                                menu._findItemByText("Flight Log Legs").show();
                                menu._findItemByText("Crew").show();
                                menu._findItemByText("PAX").show();
                                menu._findItemByText("Outbound Instruction").show();
                                menu._findItemByText("Logistics").show();
                                menu._findItemByText("Leg Notes").show();

                                if (data.attributes.ShowPrivateTripMenu == "False") {
                                    if (data.attributes.IsPrivateTrip == "True") { // if user has restricted access to Private trip

                                        menu._findItemByText("Preflight").set_enabled(false);
                                        menu._findItemByText("Postflight Flight Logs").set_enabled(false);
                                        menu._findItemByText("Flight Log Legs").set_enabled(false);
                                        menu._findItemByText("Crew").set_enabled(false);
                                        menu._findItemByText("PAX").set_enabled(false);
                                        menu._findItemByText("Logistics").set_enabled(false);
                                    }
                                }

                            } else {
                                // hide postflight log menu items
                                menu._findItemByText("Postflight Flight Logs").hide();
                                menu._findItemByText("Flight Log Legs").hide();
                                menu.findItemByValue("Separator").hide();
                                menu._findItemByText("Crew").hide();
                                menu._findItemByText("PAX").hide();
                                menu._findItemByText("Outbound Instruction").hide();
                                menu._findItemByText("Logistics").hide();
                                menu._findItemByText("Leg Notes").hide();
                                menu._findItemByText("UWA Services").show();
                                menu._findItemByText("Preflight").show();
                                menu._findItemByText("Add New Trip").show();
                                menu._findItemByText("Fleet Calendar Entry").show();
                                menu._findItemByText("Crew Calendar Entry").show();
                            }
                        }


                        function RadPlannerCrewScheduler_OnClientAppointmentContextMenuItemClicked(sender, eventArgs) {

                            showPleaseWait();
                        }

                        function RadPlannerFleetScheduler_OnClientAppointmentContextMenuItemClicked(sender, eventArgs) {
                            showPleaseWait();
                        }

                        function RadPlannerCrewScheduler_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                        }

                        function RadPlannerFleetScheduler_OnClientAppointmentContextMenuItemClicking(sender, eventArgs) {
                            getAppoinmentLegNum(eventArgs);
                        }

                        function openWinFleetCalendarEntries() {
                            var oWnd = radopen("FleetCalendarEntries.aspx", "RadWindow3");
                        }

                        function openWinCrewCalendarEntries() {
                            var oWnd = radopen("CrewCalendarEntries.aspx", "RadWindow4");
                        }

                        function openWinOutBoundInstructions() {
                            var oWnd = radopen("../PreflightOutboundInstruction.aspx?IsCalender=1", "RadWindow5");
                        }

                        function openWinLegNotes() {
                            var oWnd = radopen("LegNotesPopup.aspx", "RadWindow6");
                        }
                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) {
                                oWindow = window.radWindow;
                            }
                            else if (window.frameElement.radWindow) {
                                oWindow = window.frameElement.radWindow;
                            }
                            return oWindow;
                        }
                        // var oWnd = GetRadWindow();
                        //                        if (oArg) {
                        //                            oWnd.close(oArg);
                        //                        }
                        function OnClientClose(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                        function Close() {
                            GetRadWindow().Close();
                        }
                        function Refresh(sender, args) {
                            showPleaseWait();
                            __doPostBack("hdnPostBack", "");
                        }
                       
                    </script>
                    <script type="text/javascript">
                        //Rad Date Picker
                        var currentTextBox = null;
                        var currentDatePicker = null;

                        //This method is called to handle the onclick and onfocus client side events for the texbox
                        function showPopup(sender, e) {
                            //this is a reference to the texbox which raised the event
                            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                            //this gets a reference to the datepicker, which will be shown, to facilitate
                            //the selection of a date
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                            //this variable is used to store a reference to the date picker, which is currently
                            //active
                            currentDatePicker = datePicker;

                            //this method first parses the date, that the user entered or selected, and then
                            //sets it as a selected date to the picker
                            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                            //the code lines below show the calendar, which is used to select a date. The showPopup
                            //function takes three arguments - the x and y coordinates where to show the calendar, as
                            //well as its height, derived from the offsetHeight property of the textbox
                            var position = datePicker.getElementPosition(currentTextBox);
                            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                        }

                        //this handler is used to set the text of the TextBox to the value of selected from the popup                      
                        function dateSelected(sender, args) {
                            if (currentTextBox != null) {
                                if (currentTextBox.value != args.get_newValue()) {
                                    showPleaseWait();
                                    currentTextBox.value = args.get_newValue();
                                    __doPostBack(currentTextBox.id, "");
                                }
                                else
                                { return false; }
                            }
                        }

                        //this function is used to parse the date entered or selected by the user
                        function parseDate(sender, e) {
                            if (currentDatePicker != null) {
                                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                                var dateInput = currentDatePicker.get_dateInput();
                                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                                sender.value = formattedDate;
                            }
                        }
                        // this function is used to validate the date in tbDate textbox...
                        function ontbDateKeyPress(sender, e) {
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if (code == 13) { //Enter keycode
                                showPleaseWait(); // to show progress bar
                            }

                            // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                            var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();
                            var dateSeparator = null;
                            var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                            var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                            var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                            if (dotSeparator != -1) { dateSeparator = '.'; }
                            else if (slashSeparator != -1) { dateSeparator = '/'; }
                            else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                            else {
                                alert("Invalid Date");
                            }
                            // allow dot, slash and hypen characters... if any other character, throw alert
                            return fnAllowNumericAndChar($find("<%=tbDate.ClientID %>"), e, dateSeparator);
                        }

                        //To Hide Calender on tab out
                        function tbDate_OnKeyDown(sender, event) {
                            if (event.keyCode == 9) {
                                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                                datePicker.hidePopup();
                                return true;
                            }
                        }


                        //Fleet tree view...

                        function FleetTreeView_NodeCheck(sender, args) {

                            var treeView = $find("<%= FleetTreeView.ClientID %>");

                            var isChecked = args.get_node().get_checked();
                        }

                        // Crew tree view...
                        function CrewTreeView_NodeCheck(sender, args) {

                            var treeView = $find("<%= CrewTreeView.ClientID %>");

                            var isChecked = args.get_node().get_checked();
                        }
                        //this function is called  to get value
                        function confirmCallBackFn(arg) {
                            if (arg == false) {
                                document.getElementById('<%=tbFleetRows.ClientID%>').value = null;
                                document.getElementById('<%=btnthroughYes.ClientID%>').click();


                            }
                            else {
                                document.getElementById('<%=tbFleetRows.ClientID%>').value = "0";
                                document.getElementById('<%=btnthroughYes.ClientID%>').click();
                            }
                        }
                    </script>
                    <!-- Start: Tooltip integration -->
                    <script type="text/javascript">
                   //<![CDATA[
                        function hideActiveToolTip() {
//                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
//                            if (tooltip) {
//                                tooltip.hide();
//                            }
                        }

                        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
                        function beginRequestHandler(sender, args) {
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            if (args.get_postBackElement().id.indexOf('RadPlannerFleetScheduler') != -1) {
                                hideActiveToolTip();
                            }
                            if (args.get_postBackElement().id.indexOf('RadPlannerCrewScheduler') != -1) {
                                hideActiveToolTip();
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShowFleetPlanner(sender, args) {

                            var menu = $find('<%=RadPlannerFleetSchedulerContextMenu1.ClientID %>');
                            menu.hide();

                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadPlannerFleetScheduler.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }

                        function OnClientRequestStartFleetPlanner(sender, args) {


                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadPlannerFleetScheduler.ClientID %>").getAppointmentFromDomElement(element);

                                $get("ToolTipDescription").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubject").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()


                                tooltip.set_text($get("contentContainerFleetPlanner").innerHTML);
                            }
                        }

                        //This event is added to fix IE issue - hide / show tooltip...
                        function OnClientBeforeShowCrewPlanner(sender, args) {

                            var menu = $find('<%=RadPlannerCrewSchedulerContextMenu1.ClientID %>');
                            menu.hide();

                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadPlannerCrewScheduler.ClientID %>").getAppointmentFromDomElement(element);

                                var data = apt._serverData;
                                //                                if (data.attributes.RecordType != "T") {
                                //                                    args.set_cancel(true);

                                //                                }
                            }
                        }

                        function OnClientRequestStartCrewPlanner(sender, args) {
                            args.set_cancel(true);
                            var tooltip = Telerik.Web.UI.RadToolTip.getCurrent();
                            if (tooltip) {
                                var element = tooltip.get_targetControl();
                                var apt = $find("<%=RadPlannerCrewScheduler.ClientID %>").getAppointmentFromDomElement(element);
                                $get("ToolTipDescriptionCrew").innerHTML = apt.get_attributes().getAttribute("ToolTipDescription").toLocaleString()
                                $get("ToolTipSubjectCrew").innerHTML = apt.get_attributes().getAttribute("ToolTipSubject").toLocaleString()
                                tooltip.set_text($get("contentContainerCrewPlanner").innerHTML);
                            }
                        }
                        function OnClientAppointmentContextMenu(sender, args) {
                            hideActiveToolTip();
                        }
                     //]]>
                    </script>
                    <!-- End: Tooltip integration -->
                    <script type="text/javascript">
                          function SelectTab() {   
                                var myTab = <%= TabSchedulingCalendar.ClientID %>.FindTabByValue("Planner");   
                                myTab.Select();   
                                }   
                    </script>
                    <script type="text/javascript">
                        function SelectfiltercriteriaTab() {   
                                var myTab = <%= FilterCriteria.ClientID %>.FindTabByValue("Planner");   
                                myTab.Selectfiltercriteria();   
                            }   
                    </script>

                    <script  type="text/javascript">
                        function pageLoad() {
                            $telerik.$(".rsHorizontalHeaderTable th").each(function (index, item) {
                                var childNode = item.childNodes[0];
                                if (childNode != null) {
                                    var today = new Date();
                                    today.setHours(0); today.setMinutes(0); today.setSeconds(0);
                                    if (childNode.href != null) {
                                        var hrefDate = childNode.href.split('#')[1];
                                        var date = new Date(hrefDate);
                                        date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                        if (date.toDateString() == today.toDateString()) {
                                            item.style.backgroundImage = "none";
                                            item.style.backgroundColor = "yellow";
                                        }
                                    } else {
                                        var innerText = childNode.innerHTML.trim();
                                        var date = new Date(innerText);
                                        date.setHours(0); date.setMinutes(0); date.setSeconds(0);
                                        var parm = document.getElementById("<%=ddlDisplayDays.ClientID%>");
                                        if (index >= 30) {
                                            index = index - 30;
                                        }
                                        if (parm.options[parm.selectedIndex].text == "12 Hours Per Column") {
                                            if (index < 2) {
                                                index = 0;
                                            }
                                            else {
                                                index = Math.floor(index / 2);
                                            }
                                        }
                                        else if (parm.options[parm.selectedIndex].text == "8 Hours Per Column") {
                                            if (index < 3) {
                                                index = 0;
                                            }
                                            else {
                                                index = Math.floor(index / 3);
                                            }
                                        }
                                        else if (parm.options[parm.selectedIndex].text == "6 Hours Per Column") {
                                            if (index < 4) {
                                                index = 0;
                                            }
                                            else {
                                                index = Math.floor(index / 4);
                                            }
                                        }
                                        else if (parm.options[parm.selectedIndex].text == "1 Hour Per Column") {
                                            if (index < 24) {
                                                index = 0;
                                            }
                                            else {
                                                index = Math.floor(index / 24);
                                            }
                                        }

                                        var stdate = new Date(Date.parse(document.getElementById('<%=hdnIsTodaysDate.ClientID%>').value));
                                        stdate.setDate(stdate.getDate() + index);
                                        if (today.toDateString("M") == stdate.toDateString("M")) {
                                            item.style.backgroundImage = "none";
                                            item.style.backgroundColor = "yellow";
                                        }
                                    }
                                }
                            });
                            pageLoad1();
                        }                    
                    </script>

                    <style type="text/css"> 
                        .RadScheduler TR{
                         font-size : 11px;
                        }      
                        .rsHorizontalHeaderTable{
                            width:1750px;  
                        }     
                        .rsAllDayTable{
                            width:1750px;
                        }                                  
                    </style>
                </telerik:RadCodeBlock>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="RadScriptManager2" runat="server" />
   
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" Date="" ItemStyle-BorderColor="#cd3a00"
                    ItemStyle-BackColor="#ffa44b" ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/Legend.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/ScheduleCalendar/DisplayOptionsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/FleetCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                Width="950px" Height="600px" OnClientClose="OnClientClose" AutoSize="false" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/ScheduleCalendar/CrewCalendarEntries.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartmentAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose=" OnClientDepartmentAuthorizationPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebaseMultipleSelectionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFlightCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewDutyTypeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseMultipleSelectionClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopupMultipleSelection.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PreFlight/PreflightOutboundInstruction.aspx?IsCalender=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/FleetProfileCatalog.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfileCatalog.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="pageloader" runat="server" id="PleaseWait">
    </div>
    <div class="art-scheduling-content fullviewplanner" style="width:100%; height:100%">
        <input type="hidden" id="hdnPostBack" value="" />
        <input type="hidden" id="radSelectedLegNum" name="radSelectedLegNum" runat="server" />
        <input type="hidden" id="radSelectedStartDate" name="radSelectedStartDate" runat="server" />
        <table id="plannerfvtable" width="100%" cellpadding="0" cellspacing="0" style="width:100%; height:100%;">
            <tr>
                <td width="78%" valign="bottom">
                    <UCPreflight:Calendar ID="TabSchedulingCalendar" runat="server" />
                </td>
                <td class="tdLabel170">
                    <div class="sc_date_display">
                        <span>
                            <asp:Label ID="lbStartDate" runat="server" CssClass="date"></asp:Label></span><span
                                class="padleft_5"><asp:Label ID="lbdate" runat="server" CssClass="date" Text=" - "></asp:Label></span>
                        <span class="padleft_5">
                            <asp:Label ID="lbEndDate" runat="server" CssClass="date"></asp:Label></span>
                    </div>
                </td>
                <td align="left" class="defaultview" style='display:none';>
                    <asp:CheckBox ID="ChkDefaultView" runat="server" AutoPostBack="true" Text="Default View"
                        OnCheckedChanged="ChkDefaultView_OnCheckedChanged" onclick="javascript:showPleaseWait();" />
                </td>
                <td align="center" class="tdLabel70">
                    <asp:Button ID="btnLegend" runat="server" CssClass="ui_nav" Text="Color Legend" OnClientClick="javascript:openWinLegend();return false;" />
                </td>
               <td align="right" class="tdLabel20">
                    <span><a class="refresh-icon" onclick="Refresh();" href="#" title="Refresh"></a></span>                         
                </td>
                <td class="tdLabel50" align="left">                   
                          <asp:Button ID="btnClose" runat="server" CssClass="ui_nav" Text="Close" ToolTip="Close" OnClientClick="javascript:window.close();" />
                </td>
            </tr>
            <tr style="width:100%; height:100%;vertical-align:top;">
                <td colspan="6" width="100%" style="width:100%; height:100%;vertical-align:top;"> 
                    <telerik:RadSplitter runat="server" ID="RadSplitter1" PanesBorderSize="0"  Width="100%"
                        Skin="Windows7" Height="100%">
                        <telerik:RadPane runat="Server" ID="LeftPane" Scrolling="Y" Width="100%">
                            <div class="sc_navigation">
                                <div class="sc_navigation_left">
                                    <span class="sc_weekly_checkbox">
                                        <asp:CheckBox ID="chkCrewavailability" runat="server" Text="Crew Availability" OnCheckedChanged="chkCrewavailability_CheckedChanged"
                                            AutoPostBack="true" />
                                    </span>
                                </div>
                                <div class="sc_navigation_right">
                                    <span class="planner_section">
                                        <asp:Label ID="lblDisplayDays" runat="server" Text="Display Days:" class="tdLabel300"
                                            Width="80px"></asp:Label>
                                        <asp:DropDownList ID="ddlDisplayDays" runat="server" CssClass="text112" AutoPostBack="true"
                                            Width="150px" Height="20px" OnSelectedIndexChanged="ddlDisplayDays_SelectedIndexChanged"
                                            onchange="javascript:showPleaseWait();">
                                            <asp:ListItem Text="1 Day Per Column" Value="1.00:00:00" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="12 Hours Per Column" Value="12:00:00"></asp:ListItem>
                                            <asp:ListItem Text="8 Hours Per Column" Value="08:00:00"></asp:ListItem>
                                            <asp:ListItem Text="6 Hours Per Column" Value="06:00:00"></asp:ListItem>
                                            <asp:ListItem Text="1 Hour Per Column" Value="01:00:00"></asp:ListItem>
                                        </asp:DropDownList>
                                    </span><span class="fleet_rows">
                                        <asp:Label ID="lblFleetRows" runat="server" Text="Fleet Rows:" Width="70px"></asp:Label>
                                        <asp:TextBox ID="tbFleetRows" runat="server" MaxLength="2" OnTextChanged="tbFleetRows_TextChanged"
                                            AutoPostBack="true" CssClass="text20" ValidationGroup="fleetrowfullview" CausesValidation="True"></asp:TextBox>
                                        <asp:CompareValidator ID="validator" runat="server" ValidationGroup="fleetrowfullview" ControlToValidate="tbFleetRows" Operator="DataTypeCheck" Type="Integer"  ForeColor="#ff0000" ErrorMessage="Number only!" />

                                    </span><span class="act-btn"><span>
                                        <asp:Button ID="btnFirst" runat="server" ToolTip="Go Back 1 Month" CssClass="icon-first_sc"
                                            OnClick="btnFirst_click" OnClientClick="showPleaseWait();" /></span> <span>
                                                <asp:Button ID="prevButton" runat="server" OnClick="prevButton_Click" CssClass="icon-prev_sc"
                                                    ToolTip="Go Back 1 Day" OnClientClick="showPleaseWait();" /></span>
                                        <span>
                                            <asp:TextBox ID="tbDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                AutoPostBack="true" OnTextChanged="tbDate_OnTextChanged" OnClientClick="showPleaseWait();"
                                                onkeydown="return tbDate_OnKeyDown(this, event);" onfocus="showPopup(this, event);"
                                                onKeyPress="return ontbDateKeyPress(this, event);" onBlur="parseDate(this, event);"
                                                MaxLength="10"></asp:TextBox></span> <span>
                                                    <asp:Button ID="nextButton" runat="server" OnClick="nextButton_Click" CssClass="icon-next_sc"
                                                        OnClientClick="showPleaseWait();" ToolTip="Go Forward 1 Day" /></span>
                                        <span>
                                            <asp:Button ID="btnPrevious" runat="server" CssClass="icon-last_sc" OnClick="btnLast_click"
                                                OnClientClick="showPleaseWait();" ToolTip="Go Forward 1 Month" /></span></span>
                                    <span class="padright_10">
                                        <asp:Button ID="btnToday" runat="server" Text="Today" CssClass="ui_nav_sm" OnClick="btnToday_Click"
                                            OnClientClick="showPleaseWait();" /></span>
                                </div>
                            </div>
                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%"   ExpandAnimation-Type="None" runat="server"  CssClass="MainControlCSSFV"  >
                                <Items>
                                    <%--  Start For First View--%>
                                    <telerik:RadPanelItem Height="100%">
                                        <ContentTemplate>
                                            <div id="fleet" runat="server" style="height:100%;">
                                                <asp:Panel ID="pnlPlannerFleetScheduler" runat="server" style="height:100%">
                                                    <telerik:RadScheduler ReadOnly="true" CssClass="input_sc_rowsubheader" runat="server"
                                                        ID="RadPlannerFleetScheduler" AllowEdit="true" AllowInsert="true" DataEndField="End"
                                                        DataKeyField="TripID" DataStartField="Start" DataSubjectField="Description" DayEndTime="24:00:00"
                                                        DayStartTime="00:00:00" EnableAdvancedForm="true" OverflowBehavior="Expand" SelectedView="TimelineView"
                                                        Skin="Windows7" ShowHeader="false" AppointmentStyleMode="Default" OnAppointmentDataBound="RadPlannerFleetScheduler_AppointmentDataBound"
                                                        OnDataBound="RadPlannerFleetScheduler_DataBound" OnAppointmentCreated="RadPlannerFleetScheduler_AppointmentCreated"
                                                        ColumnWidth="45px" RowHeight="30px" OnClientAppointmentContextMenu="RadPlannerFleetScheduler_OnClientAppointmentContextMenu"
                                                        OnAppointmentContextMenuItemClicked="RadPlannerFleetScheduler_OnAppointmentContextMenuItemClicked"
                                                        OnClientAppointmentContextMenuItemClicked="RadPlannerFleetScheduler_OnClientAppointmentContextMenuItemClicked"
                                                        OnClientAppointmentContextMenuItemClicking="RadPlannerFleetScheduler_OnClientAppointmentContextMenuItemClicking"
                                                        OnClientTimeSlotContextMenuItemClicked="RadPlannerFleetScheduler_OnClientTimeSlotContextMenuItemClicked"
                                                        OnTimeSlotContextMenuItemClicked="RadPlannerFleetScheduler_OnTimeSlotContextMenuItemClicked"
                                                        Localization-HeaderToday="Today" CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu,ArrivalDisplayTime"
                                                        EnableCustomAttributeEditing="true">
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AdvancedForm Modal="true" />
                                                        <Reminders Enabled="true" />
                                                        <ResourceHeaderTemplate>
                                                            <div style="text-align: left; width: 75px!important;height:40px;" >
                                                                <asp:LinkButton ID="ResourceHeader" runat="server" CssClass="input_sc_planner_rowheader"
                                                                    OnClientClick='<%#Eval("Key", "return openWinFleet(\"{0}\")")%>' Text='<%# Eval("Key")+" - "+Eval("Text") %>'
                                                                    Font-Bold="true" ToolTip='<%# Eval("Key") %>'></asp:LinkButton>
                                                            </div>
                                                        </ResourceHeaderTemplate>
                                                        <ResourceStyles>
                                                            <telerik:ResourceStyleMapping Type="CrewFleetCodes" />
                                                        </ResourceStyles>
                                                        <TimelineView UserSelectable="false" ColumnHeaderDateFormat="d-ddd" NumberOfSlots="30"
                                                            GroupingDirection="Vertical" ShowInsertArea="false" />
                                                        <TimeSlotContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadPlannerFleetSchedulerContextMenu" runat="server">
                                                               <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                        Enabled="false" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Value="QuickFleetEntry" />
                                                                    <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Enabled="false" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </TimeSlotContextMenus>
                                                        <AppointmentContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadPlannerFleetSchedulerContextMenu1" runat="server">
                                                                <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </AppointmentContextMenus>
                                                        <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AppointmentTemplate>
                                                            <div>
                                                                <%# Eval("Subject")%></div>
                                                            <div style="font-style: italic;">
                                                                <%# Eval("Description") %></div>
                                                        </AppointmentTemplate>
                                                    </telerik:RadScheduler>
                                                   <telerik:RadToolTipManager runat="server" ID="RadToolTipManagerFleetPlanner" Width="300" Height="150"
                                                        Animation="None" HideEvent="LeaveToolTip" Text="Loading..." OnClientRequestStart="OnClientRequestStartFleetPlanner"
                                                        OnAjaxUpdate="RadToolTipManagerFleetPlanner_AjaxUpdate" OnClientBeforeShow="OnClientBeforeShowFleetPlanner" Position="TopCenter" />
                                                        <div style="display: none;">
                                                        <div id="contentContainerFleetPlanner">
                                                            <div class="appointment-tooltip" style="width:270px;">
                                                                <strong><span id="ToolTipSubject"></span></strong><span id="ToolTipDescription">
                                                                </span>
                                                            </div>
                                                        </div>
                                                         </div>
                                                </asp:Panel>
                                            </div>
                                                </ContentTemplate>
                                    </telerik:RadPanelItem>
                                 <%--End For First View
                                              Start For 2nd View--%>
                                 </Items>
                            </telerik:RadPanelBar>
                            <telerik:RadPanelBar ID="RadPanelBar3" Width="100%" ExpandAnimation-Type="None" runat="server" CssClass="RadPanelBar3 MainCrewControlCSSFV" >
                                <Items>
                                  <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div id="crew" runat="server" style="height:100%;">
                                                <asp:Panel ID="pnlPlannerCrewScheduler" runat="server" style="height:100%">
                                                    <telerik:RadScheduler ReadOnly="true" CssClass="input_sc_rowsubheader" runat="server"
                                                        ID="RadPlannerCrewScheduler" AllowEdit="true" AllowInsert="true" DataEndField="End"
                                                        DataKeyField="TripID" DataStartField="Start" DataSubjectField="Description" DayEndTime="24:00:00"
                                                        DayStartTime="00:00:00" EnableAdvancedForm="true" OverflowBehavior="Expand" SelectedView="TimelineView"
                                                        Skin="Windows7" ShowHeader="false" AppointmentStyleMode="Default" OnAppointmentDataBound="RadPlannerCrewScheduler_AppointmentDataBound"
                                                        OnDataBound="RadPlannerCrewScheduler_DataBound" OnAppointmentCreated="RadPlannerCrewScheduler_AppointmentCreated"
                                                        ColumnWidth="45px" RowHeight="30px" OnClientAppointmentContextMenu="RadPlannerCrewScheduler_OnClientAppointmentContextMenu"
                                                        OnClientAppointmentContextMenuItemClicked="RadPlannerCrewScheduler_OnClientAppointmentContextMenuItemClicked"
                                                        OnClientAppointmentContextMenuItemClicking="RadPlannerCrewScheduler_OnClientAppointmentContextMenuItemClicking"
                                                        OnClientTimeSlotContextMenuItemClicked="RadPlannerCrewScheduler_OnClientTimeSlotContextMenuItemClicked"
                                                        OnAppointmentContextMenuItemClicked="RadPlannerCrewScheduler_OnAppointmentContextMenuItemClicked"
                                                        OnTimeSlotContextMenuItemClicked="RadPlannerCrewScheduler_OnTimeSlotContextMenuItemClicked"
                                                        Localization-HeaderToday="Today" CustomAttributeNames="ToolTipDescription,ToolTipSubject,RecordType,IsLog,LegNum,TripId,IsPrivateTrip,ShowPrivateTripMenu,ArrivalDisplayTime"
                                                        EnableCustomAttributeEditing="true">
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AdvancedForm Modal="true" />
                                                        <Reminders Enabled="true" />
                                                        <ResourceHeaderTemplate>
                                                            <div style="text-align: left; width: 75px !important;">
                                                                <asp:TextBox ID="ResourceHeader" runat="server" CssClass="input_sc_planner_rowheader plannerCrewHeader"
                                                                    ReadOnly="true" Width="60px" Text='<%# Eval("Key")%>' Font-Bold="true"
                                                                    ToolTip='<%# Eval("Key") +" - "+Eval("Text") %>' />
                                                                <br />
                                                                <asp:TextBox ID="ResourceHeaderText" runat="server" CssClass="input_sc_planner_rowheader plannerCrewHeader"
                                                                    ReadOnly="true" Width="60px" Text='<%#Eval("Text") %>' Font-Bold="true"
                                                                    ToolTip='<%# Eval("Key") +" - "+Eval("Text") %>' />
                                                            </div>
                                                        </ResourceHeaderTemplate>
                                                        <ResourceStyles>
                                                            <telerik:ResourceStyleMapping Type="CrewFleetCodes" />
                                                        </ResourceStyles>
                                                        <TimelineView UserSelectable="false" ColumnHeaderDateFormat="d-ddd" NumberOfSlots="30"
                                                            GroupingDirection="Vertical" ShowInsertArea="false" />
                                                        <%--     <WeekView UserSelectable="true" GroupBy="CrewFleetCodes" GroupingDirection="Vertical" />--%>
                                                        <TimeSlotContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadPlannerCrewSchedulerContextMenu" runat="server">
                                                               <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Add Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" NavigateUrl="../PreFlightUVServices.aspx?seltab=UWA"
                                                                        Enabled="false" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Quick Fleet Calendar Entry" Enabled="false" />
                                                                    <telerik:RadMenuItem Text="Quick Crew Calendar Entry" Value="QuickCrewEntry" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </TimeSlotContextMenus>
                                                        <AppointmentContextMenus>
                                                            <telerik:RadSchedulerContextMenu ID="RadPlannerCrewSchedulerContextMenu1" runat="server">
                                                              <Items>
                                                                    <telerik:RadMenuItem Text="Preflight" Value="../PreflightLegs.aspx?seltab=Legs" />
                                                                    <telerik:RadMenuItem Text="Crew" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                    <telerik:RadMenuItem Text="PAX" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                    <telerik:RadMenuItem Text="Outbound Instruction" Value="../PreflightOutboundInstruction.aspx" />
                                                                    <telerik:RadMenuItem Text="Logistics">
                                                                        <Items>
                                                                            <telerik:RadMenuItem Text="FBO" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                            <telerik:RadMenuItem Text="Crew Hotel" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="Crew Transport" Value="../PreFlightCrewInfo.aspx?seltab=Crew" />
                                                                            <telerik:RadMenuItem Text="PAX Hotel" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="PAX Transport" Value="../PreflightPax.aspx?seltab=Pax" />
                                                                            <telerik:RadMenuItem Text="Catering" Value="../PreFlightLogistics.aspx?seltab=Logistics" />
                                                                        </Items>
                                                                    </telerik:RadMenuItem>
                                                                    <telerik:RadMenuItem Text="Leg Notes" Value="LegNotesPopup.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Add New Trip" Value="../PreFlightMain.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" Value="Separator" />
                                                                    <telerik:RadMenuItem Text="Postflight Flight Logs" Value="" />
                                                                    <telerik:RadMenuItem Text="Flight Log Legs" Value="" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Fleet Calendar Entry" Value="FleetCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem Text="Crew Calendar Entry" Value="CrewCalendarEntries.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="UWA Services" Value="../PreFlightUVServices.aspx?seltab=UWA" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                    <telerik:RadMenuItem Text="Tripsheet Report Writer" Value="Tripsheet Report Writer" />
                                                                    <telerik:RadMenuItem Text="Charter Quote Manager" Value="../../CharterQuote/CharterQuoteRequest.aspx" />
                                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                                </Items>
                                                            </telerik:RadSchedulerContextMenu>
                                                        </AppointmentContextMenus>
                                                        <TimeSlotContextMenuSettings EnableDefault="false" EnableEmbeddedScripts="true" />
                                                        <AppointmentContextMenuSettings EnableDefault="false" />
                                                        <AppointmentTemplate>
                                                            <div>
                                                                <%# Eval("Subject")%></div>
                                                            <div style="font-style: italic;">
                                                                <%# Eval("Description") %></div>
                                                        </AppointmentTemplate>
                                                    </telerik:RadScheduler>
                                                    <telerik:RadToolTipManager runat="server" ID="RadToolTipManagerCrewPlanner" Width="300" Height="150"
                                                        Animation="None" HideEvent="LeaveToolTip" Text="Loading..." OnClientRequestStart="OnClientRequestStartCrewPlanner"
                                                        OnAjaxUpdate="RadToolTipManagerCrewPlanner_AjaxUpdate" OnClientBeforeShow="OnClientBeforeShowCrewPlanner" />
                                                    <div style="display: none;">
                                                        <div id="contentContainerCrewPlanner">
                                                            <div class="appointment-tooltip" style="height:150px; width:280px; overflow-y:auto;">
                                                                <strong><span id="ToolTipSubjectCrew"></span></strong><span id="ToolTipDescriptionCrew">
                                                                </span>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <%--  End For 2nd View--%>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                        <telerik:RadSplitBar runat="server" ID="CalendarSplitBar" CollapseMode="Backward" Visible="false" style='display:none;'/>
                        <telerik:RadPane runat="server" ID="RightPane" Scrolling="Y" Width="250px" Collapsed="true" style='display:none;'>
                            <telerik:RadPanelBar runat="server" ID="RadPanelBar2" Height="914px" ExpandMode="FullExpandedItem">
                                <Items>
                                    <telerik:RadPanelItem Text="Fleet" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="FleetTreeView" TriStateCheckBoxes="true">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Text="Crew" runat="server">
                                        <ContentTemplate>
                                            <div class="sc_treeview">
                                                <telerik:RadTreeView runat="server" ID="CrewTreeView" TriStateCheckBoxes="true">
                                                </telerik:RadTreeView>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem Expanded="True" Text="Filter Criteria" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <UCPreflight:FilterCriteria ID="FilterCriteria" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <div class="sc_filter_btm">
                                                <table align="right" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="btnApply" runat="server" CssClass="ui_nav" Text="Apply" OnClick="btnApply_Click"
                                                                OnClientClick="showPleaseWait();" />
                                                            <td align="right">
                                                                <asp:Button ID="btnDisplayOptions" runat="server" CssClass="ui_nav" Text="Display Options"
                                                                    OnClientClick="javascript:openWinAdv();return false;" />
                                                                <asp:Button ID="btnthroughYes" runat="server" Text="Button" OnClick="btnthroughYes_Click"
                                                                    Style="display: none" />
                                                            </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                     <asp:HiddenField ID="hdnIsTodaysDate" runat="server" Value="" ClientIDMode="Static" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
