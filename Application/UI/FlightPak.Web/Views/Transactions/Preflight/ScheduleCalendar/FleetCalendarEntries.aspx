﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="FleetCalendarEntries.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FleetCalendarEntries"
    ClientIDMode="AutoID" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Fleet Calendar Entries</title>
    <script src="../../../../Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function deleterecord() {

                var masterTable = $find('<%= dgFleetCalendarEntries.ClientID %>').get_masterTableView();
                if (masterTable.get_selectedItems().length > 0) {
                    //                 var cell1 = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "chkUWAID");
                    //                 if (cell1.innerHTML.substring(0, 3) == "UWA") {
                    //                     alert('You Cant Delete UWA Record');
                    //                     return false;
                    //                 }
                    //                 else {

                    if (confirm('Are you sure you want to delete this record?'))
                        return true;
                    else
                        return false;
                    //                 }
                }
                else {
                    alert('Please select a record from the above table.');
                    return false;
                }
            }

        </script>
        <script type="text/javascript">

            function OpenCopyFCE() {
                var oWnd = radopen('../../Preflight/AdvancedTripCopy.aspx?tripID=' + document.getElementById('<%=hdnTripIds.ClientID%>').value + '&FromPage=' + 'FleetCalender', "rdCopyFleetCal");
            }


            function openWin(radWin) {

                var url = '';
                if (radWin == "radFleetProfilePopup") {
                    url = '/Views/Settings/Fleet/FleetProfilePopup.aspx?TailNumber=' + document.getElementById('<%=tbTailNumber.ClientID%>').value;
                }

                if (radWin == "radAirportPopup") {
                    url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbICAO.ClientID%>').value;
                }


                if (radWin == "rdHomebasePopup") {
                    url = '/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                }

                
                if (radWin == "rdClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCode.ClientID%>').value;
                }
                
                if (radWin == "rdClientCDPopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCD.ClientID%>').value;
                 }

                if (radWin == "rdDutyCodePopup") {
                    url = '/Views/Settings/Fleet/AircraftDutyTypePopup.aspx?AircraftDutyCD=' + document.getElementById('<%=tbDuty.ClientID%>').value;
                }
                if (radWin == "rdHistory") {

                    url = '/Views/Transactions/History.aspx?FromPage=' + 'FleetAndCrew' + '&TripId=' + document.getElementById('<%=hdnTripIds.ClientID%>').value;

                }
                var oWnd = radopen(url, radWin);
            }

            //            function openWin(url, value, radWin) {

            //                var oWnd = radopen(url + value, radWin);
            //            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientFleetPopupClose(oWnd, args) {
                //get the transferred arguments
                var combo = $find("<%= tbTailNumber.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=lbcvTailNumber.ClientID%>").innerHTML = "";
                        var step = "tbTailNumber_Changed";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientIcaoClose(oWnd, args) {
                //get the transferred arguments
                var combo = $find("<%= tbICAO.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbICAO.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnICAO.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=lbcvICAO.ClientID %>").innerHTML = "";
                        var step = "tbICAO_Changed";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbICAO.ClientID%>").value = "";
                        document.getElementById("<%=hdnICAO.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }


            function OnClientHomeBaseClose(oWnd, args) {
                //get the transferred arguments  
                var combo = $find("<%= tbHomeBase.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomeBase.ClientID%>").value = arg.HomebaseID;
                        document.getElementById("<%=lbcvHomeBase.ClientID %>").innerHTML = "";
                        var step = "tbHomeBase_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomeBase.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientClientCDClose(oWnd, args) {
                //get the transferred arguments 
                var combo = $find("<%= tbClientCD.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCD.ClientID%>").value = arg.ClientCD;
                        var step = "tbClientCD_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbClientCD.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            
            function OnClientClientCodeClose(oWnd, args) {
                //get the transferred arguments 
                var combo = $find("<%= tbClientCode.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=hdnClientCode.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=lbcvClientCode.ClientID %>").innerHTML = "";
                        var step = "tbClientCode_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnClientCode.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientDutyCodeClose(oWnd, args) {
                //get the transferred arguments
                var combo = $find("<%= tbDuty.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDuty.ClientID%>").value = arg.AircraftDutyCD;
                        document.getElementById("<%=tbComment.ClientID%>").value = arg.AircraftDutyDescription;
                        document.getElementById("<%=lbcvDuty.ClientID %>").innerHTML = "";

                        var radTextBox1 = $find("<%= RadMaskedTextBox1.ClientID %>");
                        radTextBox1.set_value(arg.DefaultStartTM);
                        var radTextBox2 = $find("<%= rmtbStartTime.ClientID %>");
                        radTextBox2.set_value(arg.DefualtEndTM);

                        var step = "tbDuty_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbDuty.ClientID%>").value = "";
                        document.getElementById("<%=tbComment.ClientID%>").value = "";
                        var radTextBox1 = $find("<%= RadMaskedTextBox1.ClientID %>");
                        radTextBox1.set_value("0");
                        var radTextBox2 = $find("<%= rmtbStartTime.ClientID %>");
                        radTextBox2.set_value("0");

                        combo.clearSelection();
                    }
                }
            }
            function pageLoad() {
                var oBrowserWnd = GetRadWindow().BrowserWindow;
                var HF = document.getElementById("hdnparentpage");
                HF.value = oBrowserWnd.location.href;
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

        </script>
        <script type="text/javascript">
            //Rad Date Picker
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {
                if (currentTextBox != null) {
                    //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                    //value of the picker
                    //currentTextBox.value = args.get_newValue();

                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                        __doPostBack(currentTextBox.id, "");

                    }
                    else
                    { return false; }
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                    //$find("<%=lbcvstartdate.ClientID %>").value = "";
                }
            }

            // this function is used to validate the date in tbDate textbox...
            function ontbStartDateKeyPress(sender, e) {
                // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();
                var dateSeparator = null;
                var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                if (dotSeparator != -1) { dateSeparator = '.'; }
                else if (slashSeparator != -1) { dateSeparator = '/'; }
                else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                else {
                    alert("Invalid Start Date");
                }
                // allow dot, slash and hypen characters... if any other character, throw alert
                return fnAllowNumericAndChar($find("<%=tbStartDate.ClientID %>"), e, dateSeparator);
            }
            // this function is used to validate the date in tbDate textbox...
            function ontbEndDateKeyPress(sender, e) {
                // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();
                var dateSeparator = null;
                var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                if (dotSeparator != -1) { dateSeparator = '.'; }
                else if (slashSeparator != -1) { dateSeparator = '/'; }
                else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                else {
                    alert("Invalid End Date");
                }
                // allow dot, slash and hypen characters... if any other character, throw alert
                return fnAllowNumericAndChar($find("<%=tbEndDate.ClientID %>"), e, dateSeparator);
            }
            // this function is used to validate the date in tbDate textbox...
            function ontbGotoDateKeyPress(sender, e) {
                // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();
                var dateSeparator = null;
                var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                if (dotSeparator != -1) { dateSeparator = '.'; }
                else if (slashSeparator != -1) { dateSeparator = '/'; }
                else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                else {
                    alert("Invalid Goto Date");
                }
                // allow dot, slash and hypen characters... if any other character, throw alert
                return fnAllowNumericAndChar($find("<%=tdGoto.ClientID %>"), e, dateSeparator);
            }
            //To Hide Calender on tab out
            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }
        </script>
        <style>
        .RadGrid div.rgHeaderWrapper .rgHeaderDiv{ width: 833px !important;}
    </style>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="RadScriptManager2" runat="server" />
    <asp:HiddenField ID="hdnparentpage" runat="server" />
    <asp:HiddenField ID="hdndefaulttailnum" runat="server" />
    <asp:HiddenField ID="hdndefaultSDate" runat="server" />
    <div class="divGridPanel">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager2" runat="server">
            <Windows>
                <telerik:RadWindow ID="rdCopyFleetCal" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientIcaoClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientHomeBaseClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientFleetPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                
                <telerik:RadWindow ID="rdClientCDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClientCDClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>

                <telerik:RadWindow ID="rdDutyCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientDutyCodeClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftDutyTypePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdDutyCodeCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftDutyTypePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
                </telerik:RadWindow>
                <%-- 
                <telerik:RadWindow ID="rdICAOPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientICAOClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
               
                <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClientCodeClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                --%>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server"
            CssClass="rcViewSel" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
            ItemStyle-BorderStyle="Solid">
            <ClientEvents OnDateSelected="dateSelected" />
            <DateInput ID="DateInput1" runat="server">
            </DateInput>
        </telerik:RadDatePicker>
        <table width="100%" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td valign="top">
                    <table width="850px" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <div id="DivExternalForm" runat="server" class="ExternalForm">
                                    <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                                        <tr style="display: none;">
                                            <td>
                                                <asp:TextBox ID="tbDate" runat="server" CssClass="text80" onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                      onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
                                                      ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="status-list">
                                                    <asp:RadioButton ID="radCalendarEntry" Checked="true" AutoPostBack="true" GroupName="Mode"
                                                        OnCheckedChanged="radCalenderEntry_Checked" runat="server" Text="Calendar Entries"
                                                        Visible="false"></asp:RadioButton>
                                                    <asp:RadioButton ID="radHistory" AutoPostBack="true" runat="server" GroupName="Mode"
                                                        Visible="false" OnCheckedChanged="radHistory_Checked" Text="History"></asp:RadioButton>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="status-list">
                                                    <span>
                                                        <asp:CheckBox ID="chkHomebaseOnly" runat="server" Text="Home Base Only" AutoPostBack="true" /></span>
                                                    <span class="padtop_10">Go To:</span> <span class="padtop_5">
                                                        <asp:TextBox ID="tdGoto" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                            MaxLength="10" OnTextChanged="tdGoto_TextChanged" AutoPostBack="true"></asp:TextBox></span>
                                                    <span class="padtop_10">Client Code </span><span class="padtop_5">
                                                        <asp:TextBox ID="tbClientCD" runat="server" MaxLength="5" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="True" OnTextChanged="tbClientCD_OnTextChanged"   ></asp:TextBox>
                                                        <asp:Button ID="btnClientCD" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('rdClientCDPopup');return true;" OnClick="Search_Click" />
                                                        <asp:CustomValidator ID="cvClientCD" runat="server" ControlToValidate="tbClientCD"
                                                            ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                    </span>
                                                </div>
                                            </td>
                                            <td align="right">
                                                <asp:LinkButton ID="lnkHistory" runat="server" OnClientClick="javascript:openWin('rdHistory');return false;"
                                                    ToolTip="History" />
                                            </td>
                                        </tr>
                                    </table>
                                    <telerik:RadGrid ID="dgFleetCalendarEntries" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                        OnItemCreated="dgFleetCalendarEntries_ItemCreated" OnNeedDataSource="dgFleetCalendarEntries_BindData"
                                        OnItemCommand="dgFleetCalendarEntries_ItemCommand" OnUpdateCommand="dgFleetCalendarEntries_UpdateCommand"
                                        OnInsertCommand="dgFleetCalendarEntries_InsertCommand" OnDeleteCommand="dgFleetCalendarEntries_DeleteCommand"
                                        OnSelectedIndexChanged="dgFleetCalendarEntries_SelectedIndexChanged" OnItemDataBound="dgFleetCalendarEntries_ItemDataBound"
                                        OnPageIndexChanged="dgFleetCalendarEntries_PageIndexChanged" AllowPaging="true" OnPreRender = "dgFleetCalendarEntries_PreRender"
                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="235px"
                                        Width="850px">
                                        <MasterTableView CommandItemDisplay="Bottom" PageSize="5" Width="100px" DataKeyNames="TailNumber,StartDate,EndDate,ICAOID,HomeBaseCD,AircraftDutyCD,AircraftDutyDescription,DutyType,ClientCD,AirportID,HomebaseID,ClientID,FleetID,Notes,TripID,LegID,TripNUM,NoteSec">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="StartDate" HeaderText="Start Date/Time" AutoPostBackOnFilter="true"
                                                    ShowFilterIcon="false" HeaderStyle-Width="28%" CurrentFilterFunction="Contains"
                                                    AllowFiltering="true" UniqueName="SDate">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="EndDate" HeaderText="End Date/Time" AutoPostBackOnFilter="true"
                                                    AllowFiltering="true" HeaderStyle-Width="28%" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                    UniqueName="EDate">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TailNumber" HeaderText="Tail No." AutoPostBackOnFilter="true"
                                                    AllowFiltering="true" HeaderStyle-Width="22%" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="AircraftDutyCD" HeaderText="Duty" AutoPostBackOnFilter="true"
                                                    AllowFiltering="true" HeaderStyle-Width="22%" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="AircraftDutyDescription" HeaderText="Description"
                                                    AutoPostBackOnFilter="true" AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                     Display="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="DutyType" HeaderText="Duty Type" AutoPostBackOnFilter="true"
                                                    AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                    Display="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" AutoPostBackOnFilter="true"
                                                    AllowFiltering="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                    Display="false">
                                                </telerik:GridBoundColumn>
                                                <%--<telerik:GridBoundColumn DataField="AircraftDutyDescription" HeaderText="Description"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ICAO" HeaderText="ICAO" AutoPostBackOnFilter="true"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>--%>
                                            </Columns>
                                            <CommandItemTemplate>
                                                <div style="padding: 5px 5px; float: left;">
                                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                                        OnClientClick="javascript:return ProcessUpdate();"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                                </div>
                                                <div>
                                                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                                </div>
                                            </CommandItemTemplate>
                                        </MasterTableView>
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <GroupingSettings CaseSensitive="false" />
                                    </telerik:RadGrid>
                                    <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="right">
                                                    <div class="mandatory">
                                                        <span>Bold</span> Indicates required field</div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="border-box">
                                            <tr>
                                                <td valign="top" class="tdLabel100">
                                                    <asp:Label ID="lbTailNumber" runat="server" Text="Tail No." CssClass="mnd_text" Visible="true"></asp:Label>
                                                </td>
                                                <td class="tdLabel200">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbTailNumber" runat="server" MaxLength="6" CssClass="text60" AutoPostBack="true"
                                                                    onKeyPress="return fnAllowAlphaNumericAndChar(this, event)" onBlur="return RemoveSpecialChars(this)"
                                                                    OnTextChanged="tbTailNumber_TextChanged"></asp:TextBox>
                                                                <input type="hidden" id="hdnFleetID" runat="server" />
                                                                <input type="hidden" id="hdnTripID" runat="server" />
                                                                <input type="hidden" id="hdnLegID" runat="server" />
                                                                <input type="hidden" id="hdntripnum" runat="server" />
                                                                <asp:Button ID="btnTailNumber" OnClientClick="javascript:openWin('radFleetProfilePopup');return false;"
                                                                    CssClass="browse-button" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvTailNumber" runat="server" ControlToValidate="tbTailNumber"
                                                                    Text="Tail No. is Required" ValidationGroup="save" Display="Dynamic" CssClass="alert-text"
                                                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="lbcvTailNumber" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="tdLabel100" valign="top">
                                                    <asp:Label ID="lbICAO" runat="server" Text="ICAO" CssClass="mnd_text" Visible="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbICAO" runat="server" MaxLength="4" CssClass="text50" OnKeyPress="return fnAllowAlphaNumericAndChar(this,event);"
                                                                    onBlur="return RemoveSpecialChars(this)" OnTextChanged="tbICAO_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                <input type="hidden" id="hdnICAO" runat="server" />
                                                                <asp:Button ID="btnICAO" OnClientClick="javascript:openWin('radAirportPopup');return false;"
                                                                    CssClass="browse-button" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvICAO" runat="server" ControlToValidate="tbICAO"
                                                                    Text="ICAO is Required" ValidationGroup="save" Display="Dynamic" CssClass="alert-text"
                                                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="lbcvICAO" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="tdLabel100">
                                                    <asp:Label ID="lbDuty" runat="server" Text="Duty" CssClass="mnd_text" Visible="true"></asp:Label>
                                                </td>
                                                <td class="tdLabel200">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbDuty" runat="server" MaxLength="2" CssClass="text30" OnKeyPress="return fnAllowAlphaNumericAndChar(this,event);"
                                                                    onBlur="return RemoveSpecialChars(this)" OnTextChanged="tbDuty_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                <asp:Button ID="btnDuty" OnClientClick="javascript:openWin('rdDutyCodePopup');return false;"
                                                                    CssClass="browse-button" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvDuty" runat="server" ControlToValidate="tbDuty"
                                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                                    ErrorMessage="Aircraft Duty is Required"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="lbcvDuty" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" class="tdLabel100">
                                                    Comment
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbComment" runat="server" MaxLength="25" CssClass="text200" AutoPostBack="true"></asp:TextBox>
                                                    <%--   <telerik:RadTextBox ID="tbComments" MaxLength="25" runat="server"  CssClass="text200"
                                            AutoPostBack="true" />--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="mnd_text">Start Date </span>
                                                </td>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbStartDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onfocus="showPopup(this, event);"
                                                                    onKeyPress="return ontbStartDateKeyPress(this, event);" onBlur="parseDate(this, event);"
                                                                    MaxLength="10" OnTextChanged="tbStartDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                            </td>
                                                            <td valign="top">
                                                                <telerik:RadMaskedTextBox ID="RadMaskedTextBox1" runat="server" SelectionOnFocus="SelectAll"
                                                                    Mask="<0..23>:<0..59>" CssClass="RadMaskedTextBox35">
                                                                </telerik:RadMaskedTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="tbStartDate"
                                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                                    ErrorMessage="Start Date is Required"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="lbcvstartdate" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="tdLabel100">
                                                    <asp:Label ID="lbHomeBase" runat="server" Text="Home Base" CssClass="mnd_text" Visible="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                    onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" OnTextChanged="tbHomeBase_TextChanged"></asp:TextBox>
                                                                <input type="hidden" id="hdnHomeBase" runat="server" />
                                                                <asp:Button ID="btnHomeBase" OnClientClick="javascript:openWin('rdHomebasePopup');return false;"
                                                                    CssClass="browse-button" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvHomebase" runat="server" ControlToValidate="tbHomebase"
                                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                                    ErrorMessage="Home Base is Required"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="lbcvHomeBase" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:CustomValidator ID="cvHomebase" runat="server" ControlToValidate="tbHomebase"
                                                                    ErrorMessage="Invalid Home Base" Display="Dynamic" ForeColor="Red" ValidationGroup="save"></asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="tdLabel100">
                                                    <span class="mnd_text">End Date </span>
                                                </td>
                                                <td class="tdLabel200">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbEndDate" runat="server" CssClass="text70" onclick="showPopup(this, event);"
                                                                    onfocus="showPopup(this, event);" onKeyPress="return ontbEndDateKeyPress(this, event);"
                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onBlur="parseDate(this, event);"
                                                                    MaxLength="10" OnTextChanged="tbEndDate_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                            </td>
                                                            <td valign="top">
                                                                <telerik:RadMaskedTextBox ID="rmtbStartTime" runat="server" SelectionOnFocus="SelectAll"
                                                                    Mask="<0..23>:<0..59>" CssClass="RadMaskedTextBox35">
                                                                </telerik:RadMaskedTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="tbEndDate"
                                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="save" SetFocusOnError="true"
                                                                    ErrorMessage="End Date is Required"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="lbcvenddate" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <%-- <td class="tdLabel150">
                            <asp:Label ID="lbClientCode" runat="server" Text="Client Code" CssClass="mnd_text"
                                Visible="true"></asp:Label>
                        </td>--%>
                                                <td valign="top" class="tdLabel100">
                                                    Client Code
                                                </td>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbClientCode" runat="server" MaxLength="6" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                    onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" OnTextChanged="tbClientCode_TextChanged"></asp:TextBox>
                                                                <input type="hidden" id="hdnClientCode" runat="server" />
                                                                <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('rdClientCodePopup');return false;"
                                                                    CssClass="browse-button" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CustomValidator ID="cvClientCode" runat="server" ControlToValidate="tbClientCode"
                                                                    ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                <asp:Label ID="lbcvClientCode" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="tdLabel100">
                                                    Notes
                                                </td>
                                                <td class="pr_radtextbox_300">
                                                    <%-- <asp:TextBox ID="tbComment" runat="server" MaxLength="25" CssClass="text200" AutoPostBack="true"></asp:TextBox>--%>
                                                    <telerik:RadTextBox ID="tbNotes" runat="server" TextMode="MultiLine"
                                                        AutoPostBack="true" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="pad-top-btm" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="btnCopyFCE" Text="Copy" CausesValidation="false" runat="server" OnClientClick="javascript:OpenCopyFCE();return false;"
                                                        CssClass="button" />
                                                </td>
                                                <td align="right" valign="top">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnSaveChanges" Text="Save" ValidationGroup="save" runat="server"
                                                                    OnClick="SaveChanges_Click" CssClass="button" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                                                                    OnClick="Cancel_Click" CssClass="button" />
                                                                <asp:HiddenField ID="hdnSaveFlag" runat="server" />
                                                                <asp:HiddenField ID="hdnTripIds" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div id="DivExternalForm1" runat="server" class="ExternalForm">
                                    <asp:Panel ID="pnlExternalForm1" runat="server" Visible="True">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <div class="tblspace_10">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="border-box">
                                            <tr>
                                                <td>
                                                    History
                                                </td>
                                                <td>
                                                    <div class="add-info-box" id="divHistory" runat="server">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
