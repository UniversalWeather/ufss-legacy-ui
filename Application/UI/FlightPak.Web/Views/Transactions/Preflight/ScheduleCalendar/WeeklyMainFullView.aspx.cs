﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FlightPak.Common;
using FlightPak.Web.CommonService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using FlightPak.Web.Entities.SchedulingCalendar;
using System.Text;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class WeeklyMainFullView : ScheduleCalendarBase
    {
        DateTime startdateMonday = new DateTime();
        DateTime startdateTuesday = new DateTime();
        DateTime startdateWednesday = new DateTime();
        DateTime startdateThursday = new DateTime();
        DateTime startdateFriday = new DateTime();
        DateTime startdateSaturday = new DateTime();
        DateTime startdateSunday = new DateTime();
        private int WeeklyFleetTreeCount { get; set; }
        private int WeeklyCrewTreeCount { get; set; }
        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty;
        DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;
                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        Session["SCAdvancedTab"] = CurrentDisplayOption.Weekly;

                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainContent");
                        UserControl ucSCMenu = (UserControl)contentPlaceHolder.FindControl("TabSchedulingCalendar");
                        ucSCMenu.Visible = false;                      

                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.WeeklyMain)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyMain);
                            if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                            {
                                string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                                var FleetIDs = FleetCrew[0];
                                var CrewIDs = FleetCrew[1];

                                if ((string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0) && (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0))
                                {
                                    if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                                    {
                                        FleetTreeView.CheckAllNodes();
                                    }
                                }

                                Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                                Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                                if (!string.IsNullOrEmpty(FleetCrew[0]))
                                {
                                    string[] Fleetids = FleetCrew[0].Split(',');
                                    foreach (string fIDs in Fleetids)
                                    {
                                        string[] ParentChild = fIDs.Split('$');
                                        if (ParentChild.Length > 1)
                                            Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                        else
                                            Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                    }
                                }
                                else
                                    Fleetlst = GetCheckedTreeNodes(FleetTreeView);

                                if (!string.IsNullOrEmpty(FleetCrew[1]))
                                {
                                    string[] Crewids = FleetCrew[1].Split(',');
                                    foreach (string cIDs in Crewids)
                                    {
                                        string[] ParentChild = cIDs.Split('$');
                                        if (ParentChild.Length > 1)
                                            Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                        else
                                            Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                    }
                                }

                                BindFleetTree(Fleetlst);
                                BindCrewTree(Crewlst); 
                                /*
                                if ((!string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() > 0) || (Fleetlst.Count > 0))
                                {
                                    BindFleetTree(Fleetlst); 
                                }

                                if (!string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() > 0)
                                {
                                    BindCrewTree(Crewlst); 
                                }*/
                            }
                                                       
                            if (Session["SCSelectedDay"] == null)
                            {
                                StartDate = DateTime.Today;
                                tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                                Session["SCSelectedDay"] = StartDate;

                                Session.Remove("SCWeeklyMainNextCount");
                                Session.Remove("SCWeeklyMainPreviousCount");
                                Session.Remove("SCWeeklyMainLastCount");
                                Session.Remove("SCWeeklyMainFirstCount");
                            }
                            else
                            {
                                if (Session["SCSelectedDay"] != null)
                                {
                                    StartDate = (DateTime)Session["SCSelectedDay"];
                                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                                }
                            }
                            loadTreeGrid(false);
                        }
                        else
                        {
                            CheckValidDate();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        /// <summary>
        /// To Validate Date 
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            StartDate = (DateTime)Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // Manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

            if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
            {
                return true;
            }
            else
            {
                string id = Page.Request.Params["__EVENTTARGET"];

                if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
                {
                    loadTreeGrid(false);
                }

            }
            return IsDateCheck;
        }

        private void loadTreeGrid(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                startdateMonday = GetDateMonday(StartDate);//mondayDate;
                startdateTuesday = startdateMonday.AddDays(1);
                startdateWednesday = startdateMonday.AddDays(2);
                startdateThursday = startdateMonday.AddDays(3);
                startdateFriday = startdateMonday.AddDays(4);
                startdateSaturday = startdateMonday.AddDays(5);
                startdateSunday = startdateMonday.AddDays(6); //sundayDate;

                EndDate = GetWeekEndDate(startdateMonday);

                dgWeeklyMainMonday.MasterTableView.GetColumn("weeklyMonday").HeaderText = startdateMonday.DayOfWeek.ToString() + " " + startdateMonday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                dgWeeklyMainTuesday.MasterTableView.GetColumn("weeklyTuesday").HeaderText = startdateTuesday.DayOfWeek.ToString() + " " + startdateTuesday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                dgWeeklyMainWednesday.MasterTableView.GetColumn("weeklyWednesday").HeaderText = startdateWednesday.DayOfWeek.ToString() + " " + startdateWednesday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                dgWeeklyMainThursday.MasterTableView.GetColumn("weeklyThursday").HeaderText = startdateThursday.DayOfWeek.ToString() + " " + startdateThursday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                dgWeeklyMainFriday.MasterTableView.GetColumn("weeklyFriday").HeaderText = startdateFriday.DayOfWeek.ToString() + " " + startdateFriday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                dgWeeklyMainSaturday.MasterTableView.GetColumn("weeklySaturday").HeaderText = startdateSaturday.DayOfWeek.ToString() + " " + startdateSaturday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                dgWeeklyMainSunday.MasterTableView.GetColumn("weeklySunday").HeaderText = startdateSunday.DayOfWeek.ToString() + " " + startdateSunday.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);

                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(FilterOptions);
                DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();                   
                if (IsSave)
                {
                    var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                    var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);

                    objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyMain, selectedFleetIDs, selectedCrewIDs, null, false);
                    //BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, selectedFleetIDs);
                    //BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.Month, selectedCrewIDs);
                    //var inputFromFleetTreeNew = selectedFleetIDs;
                    //var inputFromCrewTreeNew = selectedCrewIDs;
                                    
                    BindFleetTree(selectedFleetIDs);
                    BindCrewTree(selectedCrewIDs); 
                    //WeeklyFleetTreeCount = inputFromFleetTreeNew.Count; // this property is used in appointment data bound to identify the view 
                    //WeeklyCrewTreeCount = inputFromCrewTreeNew.Count; 
                }
                else
                {
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyMain);
                    if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                    {
                        string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                        var FleetIDs = FleetCrew[0];
                        var CrewIDs = FleetCrew[1];

                        if ((string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0) && (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0))
                        {
                            if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                            {
                                FleetTreeView.CheckAllNodes();
                            }
                        }

                        Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                        Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                        if (!string.IsNullOrEmpty(FleetCrew[0]))
                        {
                            string[] Fleetids = FleetCrew[0].Split(',');
                            foreach (string fIDs in Fleetids)
                            {
                                string[] ParentChild = fIDs.Split('$');
                                if (ParentChild.Length > 1)
                                    Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                else
                                    Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                            }
                        }
                        else
                            Fleetlst = GetCheckedTreeNodes(FleetTreeView);

                        if (!string.IsNullOrEmpty(FleetCrew[1]))
                        {
                            string[] Crewids = FleetCrew[1].Split(',');
                            foreach (string cIDs in Crewids)
                            {
                                string[] ParentChild = cIDs.Split('$');
                                if (ParentChild.Length > 1)
                                    Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                else
                                    Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                            }
                        }
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }                   
                }             

                if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                {
                    if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                    {
                        FleetTreeView.CheckAllNodes();
                    }
                }

                var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView);
                var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView);
                WeeklyFleetTreeCount = inputFromFleetTree.Count; // this property is used in appointment data bound to identify the view 
                WeeklyCrewTreeCount = inputFromCrewTree.Count; // this property is used in appointment data bound to identify the view 

                Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                if (FleetTreeView.CheckedNodes.Count > 0)
                {
                    using (var preflightServiceClient = new PreflightServiceClient())
                    {
                        Collection<string> InputFromFleetTree = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromFleetTree)
                        {
                            InputFromFleetTree.Add(s.ChildNodeId);
                        }
                        var calendarData = preflightServiceClient.GetFleetCalendarData(startdateMonday.AddDays(-31), EndDate.AddDays(31), serviceFilterCriteria, InputFromFleetTree.Distinct().ToList(), true, false, false);                        
                        //Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                        if (!DisplayOptions.Weekly.CrewCalActivity)
                        {
                            if (calendarData.EntityList != null)
                            {
                                fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                                BindAllGrids(startdateMonday, startdateTuesday, startdateWednesday, startdateThursday, startdateFriday, startdateSaturday, startdateSunday, fleetappointments);
                            }
                        }
                        else
                        {
                            CrewTreeView.CheckAllNodes();
                            var inputFromCrewTreeAll = GetCheckedTreeNodes(CrewTreeView, false);
                            CrewTreeView.UncheckAllNodes();
                            Collection<string> InputFromCrewTreeAll = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in inputFromCrewTreeAll)
                            {
                                InputFromCrewTreeAll.Add(s.ChildNodeId);
                            }
                            var crewcalendarData = preflightServiceClient.GetCrewCalendarData(startdateMonday.AddDays(-31), EndDate.AddDays(31), serviceFilterCriteria, InputFromCrewTreeAll.Distinct().ToList(), false);

                            if (calendarData.EntityList != null || crewcalendarData.EntityList != null)
                            {
                                fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                                //var appointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.Month);                                                
                                //Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                                if (crewcalendarData.EntityList != null)
                                {
                                    crewappointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                                }
                                //Bind all grids...
                                BindFleetCrewGrids(startdateMonday, startdateTuesday, startdateWednesday, startdateThursday, startdateFriday, startdateSaturday, startdateSunday, fleetappointments, crewappointments);
                            }
                        }

                        //if (calendarData.EntityList != null)
                        //{
                            //if (DisplayOptions.Weekly.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                           // {
                                // remove records of type='C' and tailNum == null...
                                //fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                            //}
                            //else
                            //{
                                // by default REcordType, T, M and C with tailNum will be displayed...
                               // calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                               // fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                            //}
                            //Bind all grids...
                            //BindAllGrids(startdateMonday, startdateTuesday, startdateWednesday, startdateThursday, startdateFriday, startdateSaturday, startdateSunday, fleetappointments);
                        //}
                    }
                }
                else if (CrewTreeView.CheckedNodes.Count > 0)
                {
                    using (var preflightServiceClient = new PreflightServiceClient())
                    {
                        Collection<string> InputFromCrewTree = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromCrewTree)
                        {
                            InputFromCrewTree.Add(s.ChildNodeId);
                        }

                        var crewcalendarData = preflightServiceClient.GetCrewCalendarData(startdateMonday.AddDays(-31), EndDate.AddDays(31), serviceFilterCriteria, InputFromCrewTree.Distinct().ToList(), false);
                        //var appointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.Month);
                                                
                        //Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                        if (crewcalendarData.EntityList != null)
                        {
                            crewappointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                            /*if (DisplayOptions.Weekly.CrewCalActivity) // if crew cal activity  is checked,  display crew entries with out tail num
                            {
                                // remove records of type='C' and tailNum == null...
                                crewappointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                            }
                            else
                            {
                                // by default REcordType, T, M and C with tailNum will be displayed...
                                crewcalendarData.EntityList = crewcalendarData.EntityList.Where(x => x.TailNum != null).ToList();
                                crewappointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, serviceFilterCriteria.TimeBase.ToString(), DisplayOptions, ModuleNameConstants.Preflight.WeeklyMain);
                            }*/
                            //Bind all grids...                            
                            BindAllGrids(startdateMonday, startdateTuesday, startdateWednesday, startdateThursday, startdateFriday, startdateSaturday, startdateSunday, crewappointments);
                        }
                    }
                }
                //BindFleetCrewGrids(startdateMonday, startdateTuesday, startdateWednesday, startdateThursday, startdateFriday, startdateSaturday, startdateSunday, fleetappointments, crewappointments);
            }
            //RightPane.Collapsed = true;
        }

        private void BindAllGrids(DateTime startdateMonday, DateTime startdateTuesday, DateTime startdateWednesday, DateTime startdateThursday, DateTime startdateFriday, DateTime startdateSaturday, DateTime startdateSunday, Collection<Entities.SchedulingCalendar.Appointment> appointments)
        {
            //Monday
            var mondayAppointments = appointments.Where(x => x.StartDate < startdateTuesday && x.EndDate >= startdateMonday);
            var firstLastmondayAppointments = SetFirstLegLastLeg(mondayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONMondayAppointments = mondayAppointments.Where(x => x.LegNum == 0);
            var mondayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            mondayFinalAppointments.AddRange(firstLastmondayAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
            mondayFinalAppointments.AddRange(RONMondayAppointments);
            }
            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingMondayAsStartDate = mondayFinalAppointments.Where(x => x.StartDate.Date != startdateMonday.Date).ToList();
                mondayAppointments = mondayFinalAppointments.Except(appointmentsNotHavingMondayAsStartDate);
                dgWeeklyMainMonday.DataSource = mondayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainMonday.DataSource = mondayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }
            dgWeeklyMainMonday.DataBind();

            //Tuesday
            var tuesdayAppointments = appointments.Where(x => x.StartDate < startdateWednesday && x.EndDate >= startdateTuesday);
            var firstLastTuesdayAppointments = SetFirstLegLastLeg(tuesdayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONTuesdayAppointments = tuesdayAppointments.Where(x => x.LegNum == 0).ToList();
            var tuesdayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            tuesdayFinalAppointments.AddRange(firstLastTuesdayAppointments.ToList());
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                tuesdayFinalAppointments.AddRange(RONTuesdayAppointments);
            }

            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingTuesdayAsStartDate = tuesdayFinalAppointments.Where(x => x.StartDate.Date != startdateTuesday.Date).ToList();
                tuesdayAppointments = tuesdayFinalAppointments.Except(appointmentsNotHavingTuesdayAsStartDate);
                dgWeeklyMainTuesday.DataSource = tuesdayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainTuesday.DataSource = tuesdayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }

            dgWeeklyMainTuesday.DataBind();

            //Wednesday
            var wednesdayAppointments = appointments.Where(x => x.StartDate < startdateThursday && x.EndDate >= startdateWednesday);
            var firstLastWednesdayAppointments = SetFirstLegLastLeg(wednesdayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONWednesdayAppointments = wednesdayAppointments.Where(x => x.LegNum == 0).ToList();
            var WednesdayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            WednesdayFinalAppointments.AddRange(firstLastWednesdayAppointments.ToList());

            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                WednesdayFinalAppointments.AddRange(RONWednesdayAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingWednesdayAsStartDate = WednesdayFinalAppointments.Where(x => x.StartDate.Date != startdateWednesday.Date).ToList();
                wednesdayAppointments = WednesdayFinalAppointments.Except(appointmentsNotHavingWednesdayAsStartDate);
                dgWeeklyMainWednesday.DataSource = wednesdayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainWednesday.DataSource = WednesdayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }

            dgWeeklyMainWednesday.DataBind();

            //Thursday
            var thursdayAppointments = appointments.Where(x => x.StartDate < startdateFriday && x.EndDate >= startdateThursday);
            var firstLastThursdayAppointments = SetFirstLegLastLeg(thursdayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONThursdayAppointments = thursdayAppointments.Where(x => x.LegNum == 0).ToList();
            var thursdayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            thursdayFinalAppointments.AddRange(firstLastThursdayAppointments.ToList());

            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                thursdayFinalAppointments.AddRange(RONThursdayAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingThursdayAsStartDate = thursdayFinalAppointments.Where(x => x.StartDate.Date != startdateThursday.Date).ToList();
                thursdayAppointments = thursdayFinalAppointments.Except(appointmentsNotHavingThursdayAsStartDate);
                dgWeeklyMainThursday.DataSource = thursdayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainThursday.DataSource = thursdayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }

            dgWeeklyMainThursday.DataBind();

            //Friday
            var fridayAppointments = appointments.Where(x => x.StartDate < startdateSaturday && x.EndDate >= startdateFriday);
            var firstLastFridayAppointments = SetFirstLegLastLeg(fridayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONFridayAppointments = fridayAppointments.Where(x => x.LegNum == 0).ToList();
            var fridayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            fridayFinalAppointments.AddRange(firstLastFridayAppointments.ToList());

            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                fridayFinalAppointments.AddRange(RONFridayAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingFridayAsStartDate = fridayFinalAppointments.Where(x => x.StartDate.Date != startdateFriday.Date).ToList();
                fridayAppointments = fridayFinalAppointments.Except(appointmentsNotHavingFridayAsStartDate);
                dgWeeklyMainFriday.DataSource = fridayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainFriday.DataSource = fridayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }

            dgWeeklyMainFriday.DataBind();

            //Saturday
            var saturdayAppointments = appointments.Where(x => x.StartDate < startdateSunday && x.EndDate >= startdateSaturday);
            var firstLastSaturdayAppointments = SetFirstLegLastLeg(saturdayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONSaturdayAppointments = saturdayAppointments.Where(x => x.LegNum == 0).ToList();
            var saturdayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            saturdayFinalAppointments.AddRange(firstLastSaturdayAppointments.ToList());

            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                saturdayFinalAppointments.AddRange(RONSaturdayAppointments);
            }

            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingSaturdayAsStartDate = saturdayFinalAppointments.Where(x => x.StartDate.Date != startdateSaturday.Date).ToList();
                saturdayAppointments = saturdayFinalAppointments.Except(appointmentsNotHavingSaturdayAsStartDate);
                dgWeeklyMainSaturday.DataSource = saturdayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainSaturday.DataSource = saturdayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }

            dgWeeklyMainSaturday.DataBind();

            //Sunday
            var sundayAppointments = appointments.Where(x => x.StartDate < startdateSunday.AddDays(1).AddSeconds(-1) && x.EndDate >= startdateSunday);
            var firstLastSundayAppointments = SetFirstLegLastLeg(sundayAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONSundayAppointments = sundayAppointments.Where(x => x.LegNum == 0).ToList();
            var sundayFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            sundayFinalAppointments.AddRange(firstLastSundayAppointments.ToList());
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                sundayFinalAppointments.AddRange(RONSundayAppointments);
            }
            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var appointmentsNotHavingSundayAsStartDate = sundayFinalAppointments.Where(x => x.StartDate.Date != startdateSunday.Date).ToList();
                sundayAppointments = sundayFinalAppointments.Except(appointmentsNotHavingSundayAsStartDate);
                dgWeeklyMainSunday.DataSource = sundayAppointments.OrderBy(x => x.StartDate).ToList();
            }
            else
            {
                dgWeeklyMainSunday.DataSource = sundayFinalAppointments.OrderBy(x => x.StartDate).ToList();
            }

            dgWeeklyMainSunday.DataBind();
        }

        private void BindFleetCrewGrids(DateTime startdateMonday, DateTime startdateTuesday, DateTime startdateWednesday, DateTime startdateThursday, DateTime startdateFriday, DateTime startdateSaturday, DateTime startdateSunday, Collection<Entities.SchedulingCalendar.Appointment> fleetappointments, Collection<Entities.SchedulingCalendar.Appointment> crewappointments)
        {
            //Monday

            //Fleet Info
            var mondayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateTuesday && x.EndDate >= startdateMonday);
            var firstLastmondayFleetAppointments = SetFirstLegLastLeg(mondayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONMondayFleetAppointments = mondayFleetAppointments.Where(x => x.LegNum == 0);
            var mondayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            mondayFleetFinalAppointments.AddRange(firstLastmondayFleetAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                mondayFleetFinalAppointments.AddRange(RONMondayFleetAppointments);
            }

            //Crew Info
            var mondayCrewAppointments = crewappointments.Where(x => x.StartDate < startdateTuesday && x.EndDate >= startdateMonday);
            var firstLastmondayCrewAppointments = SetFirstLegLastLeg(mondayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONMondayCrewAppointments = mondayCrewAppointments.Where(x => x.LegNum == 0);
            var mondayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            mondayCrewFinalAppointments.AddRange(firstLastmondayCrewAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                mondayCrewFinalAppointments.AddRange(RONMondayCrewAppointments);
            }

            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingMondayAsStartDate = mondayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateMonday.Date).ToList();
                mondayFleetAppointments = mondayFleetFinalAppointments.Except(fleetappointmentsNotHavingMondayAsStartDate);

                var crewappointmentsNotHavingMondayAsStartDate = mondayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateMonday.Date).ToList();
                mondayCrewAppointments = mondayCrewFinalAppointments.Except(crewappointmentsNotHavingMondayAsStartDate);

                //dgWeeklyMainMonday.DataSource = mondayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(mondayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();                
                dgWeeklyMainMonday.DataSource = mondayFleetAppointments.Concat(mondayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            else
            {
                //dgWeeklyMainMonday.DataSource = mondayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(mondayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainMonday.DataSource = mondayFleetFinalAppointments.Concat(mondayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            dgWeeklyMainMonday.DataBind();


            //Tuesday

            //Fleet Info
            var tuesdayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateWednesday && x.EndDate >= startdateTuesday);
            var firstLastTuesdayFleetAppointments = SetFirstLegLastLeg(tuesdayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONTuesdayFleetAppointments = tuesdayFleetAppointments.Where(x => x.LegNum == 0).ToList();
            var tuesdayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            tuesdayFleetFinalAppointments.AddRange(firstLastTuesdayFleetAppointments.ToList());
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                tuesdayFleetFinalAppointments.AddRange(RONTuesdayFleetAppointments);
            }


            //Crew Info
            var tuesdayCrewAppointments = crewappointments.Where(x => x.StartDate < startdateWednesday && x.EndDate >= startdateTuesday);
            var firstLastTuesdayCrewAppointments = SetFirstLegLastLeg(tuesdayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONTuesdayCrewAppointments = tuesdayCrewAppointments.Where(x => x.LegNum == 0).ToList();
            var tuesdayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            tuesdayCrewFinalAppointments.AddRange(firstLastTuesdayCrewAppointments.ToList());

            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                tuesdayCrewFinalAppointments.AddRange(RONTuesdayCrewAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingTuesdayAsStartDate = tuesdayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateTuesday.Date).ToList();
                tuesdayFleetAppointments = tuesdayFleetFinalAppointments.Except(fleetappointmentsNotHavingTuesdayAsStartDate);

                var crewappointmentsNotHavingTuesdayAsStartDate = tuesdayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateTuesday.Date).ToList();
                tuesdayCrewAppointments = tuesdayCrewFinalAppointments.Except(crewappointmentsNotHavingTuesdayAsStartDate);

                //dgWeeklyMainTuesday.DataSource = tuesdayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(tuesdayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                //dgWeeklyMainTuesday.DataSource = tuesdayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(tuesdayCrewAppointments.OrderBy(x => x.StartDate).ToList()).Select(x => new { x.TripId, x.LegId }).Distinct().ToList();
                //dgWeeklyMainTuesday.DataSource = tuesdayFleetAppointments.Concat(tuesdayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(a => a.TripId, a => a.TripId).Distinct().ToList();                
                dgWeeklyMainTuesday.DataSource = tuesdayFleetAppointments.Concat(tuesdayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            else
            {
                //dgWeeklyMainTuesday.DataSource = tuesdayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(tuesdayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainTuesday.DataSource = tuesdayFleetFinalAppointments.Concat(tuesdayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }

            dgWeeklyMainTuesday.DataBind();


            //Wednesday

            //Fleet Info
            var wednesdayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateThursday && x.EndDate >= startdateWednesday);
            var firstLastWednesdayFleetAppointments = SetFirstLegLastLeg(wednesdayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONWednesdayFleetAppointments = wednesdayFleetAppointments.Where(x => x.LegNum == 0).ToList();
            var wednesdayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            wednesdayFleetFinalAppointments.AddRange(firstLastWednesdayFleetAppointments.ToList());

            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                wednesdayFleetFinalAppointments.AddRange(RONWednesdayFleetAppointments);
            }

            //Crew Info
            var wednesdayCrewAppointments = crewappointments.Where(x => x.StartDate < startdateThursday && x.EndDate >= startdateWednesday);
            var firstLastWednesdayCrewAppointments = SetFirstLegLastLeg(wednesdayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONWednesdayCrewAppointments = wednesdayCrewAppointments.Where(x => x.LegNum == 0).ToList();
            var wednesdayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            wednesdayCrewFinalAppointments.AddRange(firstLastWednesdayCrewAppointments.ToList());
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                wednesdayCrewFinalAppointments.AddRange(RONWednesdayCrewAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingWednesdayAsStartDate = wednesdayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateWednesday.Date).ToList();
                wednesdayFleetAppointments = wednesdayFleetFinalAppointments.Except(fleetappointmentsNotHavingWednesdayAsStartDate);

                var crewappointmentsNotHavingWednesdayAsStartDate = wednesdayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateWednesday.Date).ToList();
                wednesdayCrewAppointments = wednesdayCrewFinalAppointments.Except(crewappointmentsNotHavingWednesdayAsStartDate);

                //dgWeeklyMainWednesday.DataSource = wednesdayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(wednesdayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainWednesday.DataSource = wednesdayFleetAppointments.Concat(wednesdayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            else
            {
                //dgWeeklyMainWednesday.DataSource = wednesdayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(wednesdayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainWednesday.DataSource = wednesdayFleetFinalAppointments.Concat(wednesdayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }

            dgWeeklyMainWednesday.DataBind();


            //Thursday

            //Fleet Info
            var thursdayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateFriday && x.EndDate >= startdateThursday);
            var firstLastThursdayFleetAppointments = SetFirstLegLastLeg(thursdayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONThursdayFleetAppointments = thursdayFleetAppointments.Where(x => x.LegNum == 0).ToList();
            var thursdayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            thursdayFleetFinalAppointments.AddRange(firstLastThursdayFleetAppointments.ToList());
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                thursdayFleetFinalAppointments.AddRange(RONThursdayFleetAppointments);
            }


            //Crew Info
            var thursdayCrewAppointments = crewappointments.Where(x => x.StartDate < startdateFriday && x.EndDate >= startdateThursday);
            var firstLastThursdayCrewAppointments = SetFirstLegLastLeg(thursdayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONThursdayCrewAppointments = thursdayCrewAppointments.Where(x => x.LegNum == 0).ToList();
            var thursdayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            thursdayCrewFinalAppointments.AddRange(firstLastThursdayCrewAppointments.ToList());
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                thursdayCrewFinalAppointments.AddRange(RONThursdayCrewAppointments);
            }

            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingThursdayAsStartDate = thursdayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateThursday.Date).ToList();
                thursdayFleetAppointments = thursdayFleetFinalAppointments.Except(fleetappointmentsNotHavingThursdayAsStartDate);

                var crewappointmentsNotHavingThursdayAsStartDate = thursdayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateThursday.Date).ToList();
                thursdayCrewAppointments = thursdayCrewFinalAppointments.Except(crewappointmentsNotHavingThursdayAsStartDate);

                //dgWeeklyMainThursday.DataSource = thursdayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(thursdayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainThursday.DataSource = thursdayFleetAppointments.Concat(thursdayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            else
            {
                //dgWeeklyMainThursday.DataSource = thursdayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(thursdayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainThursday.DataSource = thursdayFleetFinalAppointments.Concat(thursdayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }

            dgWeeklyMainThursday.DataBind();


            //Friday

            //Fleet Info
            var fridayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateSaturday && x.EndDate >= startdateFriday);
            var firstLastFridayFleetAppointments = SetFirstLegLastLeg(fridayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONFridayFleetAppointments = fridayFleetAppointments.Where(x => x.LegNum == 0);
            var fridayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            fridayFleetFinalAppointments.AddRange(firstLastFridayFleetAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                fridayFleetFinalAppointments.AddRange(RONFridayFleetAppointments);
            }

            //Crew Info
            var fridayCrewAppointments = crewappointments.Where(x => x.StartDate < startdateSaturday && x.EndDate >= startdateFriday);
            var firstLastFridayCrewAppointments = SetFirstLegLastLeg(fridayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONFridayCrewAppointments = fridayCrewAppointments.Where(x => x.LegNum == 0);
            var fridayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            fridayCrewFinalAppointments.AddRange(firstLastFridayCrewAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                fridayCrewFinalAppointments.AddRange(RONFridayCrewAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingFridayAsStartDate = fridayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateFriday.Date).ToList();
                fridayFleetAppointments = fridayFleetFinalAppointments.Except(fleetappointmentsNotHavingFridayAsStartDate);

                var crewappointmentsNotHavingFridayAsStartDate = fridayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateFriday.Date).ToList();
                fridayCrewAppointments = fridayFleetFinalAppointments.Except(crewappointmentsNotHavingFridayAsStartDate);

                //dgWeeklyMainFriday.DataSource = fridayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(fridayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainFriday.DataSource = fridayFleetAppointments.Concat(fridayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();

            }
            else
            {
                //dgWeeklyMainFriday.DataSource = fridayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(fridayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainFriday.DataSource = fridayFleetFinalAppointments.Concat(fridayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }

            dgWeeklyMainFriday.DataBind();

            //Saturday

            //Fleet Info
            var saturdayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateSunday && x.EndDate >= startdateSaturday);
            var firstLastSaturdayFleetAppointments = SetFirstLegLastLeg(saturdayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONSaturdayFleetAppointments = saturdayFleetAppointments.Where(x => x.LegNum == 0);
            var saturdayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            saturdayFleetFinalAppointments.AddRange(firstLastSaturdayFleetAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                saturdayFleetFinalAppointments.AddRange(RONSaturdayFleetAppointments);
            }

            //Crew Info
            var saturdayCrewAppointments = crewappointments.Where(x => x.StartDate < startdateSunday && x.EndDate >= startdateSaturday);
            var firstLastSaturdayCrewAppointments = SetFirstLegLastLeg(saturdayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONSaturdayCrewAppointments = saturdayCrewAppointments.Where(x => x.LegNum == 0);
            var saturdayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            saturdayCrewFinalAppointments.AddRange(firstLastSaturdayCrewAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                saturdayCrewFinalAppointments.AddRange(RONSaturdayCrewAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingSaturdayAsStartDate = saturdayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateSaturday.Date).ToList();
                saturdayFleetAppointments = saturdayFleetFinalAppointments.Except(fleetappointmentsNotHavingSaturdayAsStartDate);

                var crewappointmentsNotHavingSaturdayAsStartDate = saturdayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateSaturday.Date).ToList();
                saturdayCrewAppointments = saturdayCrewFinalAppointments.Except(crewappointmentsNotHavingSaturdayAsStartDate);

                //dgWeeklyMainSaturday.DataSource = saturdayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(saturdayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainSaturday.DataSource = saturdayFleetAppointments.Concat(saturdayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            else
            {
                //dgWeeklyMainSaturday.DataSource = saturdayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(saturdayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainSaturday.DataSource = saturdayFleetFinalAppointments.Concat(saturdayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }

            dgWeeklyMainSaturday.DataBind();


            //Sunday

            //Fleet Info
            var sundayFleetAppointments = fleetappointments.Where(x => x.StartDate < startdateSunday.AddDays(1).AddSeconds(-1) && x.EndDate >= startdateSunday);
            var firstLastSundayFleetAppointments = SetFirstLegLastLeg(sundayFleetAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONSundayFleetAppointments = sundayFleetAppointments.Where(x => x.LegNum == 0);
            var sundayFleetFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            sundayFleetFinalAppointments.AddRange(firstLastSundayFleetAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                sundayFleetFinalAppointments.AddRange(RONSundayFleetAppointments);
            }

            //Crew Info
            var sundayCrewAppointments = fleetappointments.Where(x => x.StartDate < startdateSunday.AddDays(1).AddSeconds(-1) && x.EndDate >= startdateSunday);
            var firstLastSundayCrewAppointments = SetFirstLegLastLeg(sundayCrewAppointments.Where(x => x.LegNum > 0));//first/last leg should be found only for db appointments
            var RONSundayCrewAppointments = sundayCrewAppointments.Where(x => x.LegNum == 0);
            var sundayCrewFinalAppointments = new List<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
            sundayCrewFinalAppointments.AddRange(firstLastSundayCrewAppointments);
            if (DisplayOptions.Weekly.FirstLastLeg == false)
            {
                sundayCrewFinalAppointments.AddRange(RONSundayCrewAppointments);
            }


            if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
            {
                var fleetappointmentsNotHavingSundayAsStartDate = sundayFleetFinalAppointments.Where(x => x.StartDate.Date != startdateSunday.Date).ToList();
                sundayFleetAppointments = sundayFleetFinalAppointments.Except(fleetappointmentsNotHavingSundayAsStartDate);

                var crewappointmentsNotHavingSundayAsStartDate = sundayCrewFinalAppointments.Where(x => x.StartDate.Date != startdateSunday.Date).ToList();
                sundayCrewAppointments = sundayCrewFinalAppointments.Except(crewappointmentsNotHavingSundayAsStartDate);

                //dgWeeklyMainSunday.DataSource = sundayFleetAppointments.OrderBy(x => x.StartDate).ToList().Concat(sundayCrewAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainSunday.DataSource = sundayFleetAppointments.Concat(sundayCrewAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }
            else
            {
                //dgWeeklyMainSunday.DataSource = sundayFleetFinalAppointments.OrderBy(x => x.StartDate).ToList().Concat(sundayCrewFinalAppointments.OrderBy(x => x.StartDate).ToList()).ToList();
                dgWeeklyMainSunday.DataSource = sundayFleetFinalAppointments.Concat(sundayCrewFinalAppointments).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
            }

            dgWeeklyMainSunday.DataBind();
        }

        private IEnumerable<Entities.SchedulingCalendar.Appointment> SetFirstLegLastLeg(IEnumerable<Entities.SchedulingCalendar.Appointment> appointments)
        {
            // build appointment data based on first/last leg display option
            if (DisplayOptions.Weekly.FirstLastLeg)
            {
                // group legs by trip Id, find 1st and last leg if a trip has more than one leg. exclude other legs from collection...
                // if trip has only one leg, just add the leg to collection...
                var distinctTripIds = appointments.Select(x => x.TripId).Distinct().ToList();
                var noFirstOrLastLegs = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                foreach (var id in distinctTripIds)
                {
                    var groupedLegs = appointments.Where(p => p.TripId == id).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNum).First();
                    if (groupedLegs.Count > 1)
                    {
                        noFirstOrLastLegs.AddRange(appointments.Where(x => x.LegNum != 1 && x.LegNum != lastLeg.LegNum).ToList());
                    }
                }
                appointments = appointments.Except(noFirstOrLastLegs);
            }
            return appointments;
        }


        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;
                        //Changes done for the Bug ID - 5828

                        FleetTreeView.Nodes.Clear();

                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;
                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        CrewTreeView.Nodes.Clear();

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNew(crewList, selectedcrewIDs);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;

                        CrewTreeView.DataBind();
                    }
                }
            }
        }

        protected void radFleet_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Response.Redirect("SchedulingCalendar.aspx?View=Fleet");
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyFleet;
                        Response.Redirect("SchedulingCalendar.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void radCrew_Checked(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyCrew;
                        Response.Redirect("WeeklyCrew.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }


        }

        protected void radDetail_Checked(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["SCAdvancedTab"] = CurrentDisplayOption.WeeklyDetail;
                        Response.Redirect("WeeklyDetail.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }


        }

        protected void RadMondayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateMonday = GetDateMonday(StartDate);//mondayDate;
                        RedirectToRespectivePage(e, startdateMonday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        protected void RadTuesdayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateTuesday = GetDateMonday(StartDate).AddDays(1);
                        RedirectToRespectivePage(e, startdateTuesday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        protected void RadWednesdayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateWednesday = GetDateMonday(StartDate).AddDays(2);
                        RedirectToRespectivePage(e, startdateWednesday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        protected void RadThursdayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateThursday = GetDateMonday(StartDate).AddDays(3);
                        RedirectToRespectivePage(e, startdateThursday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void RadFridayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateFriday = GetDateMonday(StartDate).AddDays(4);
                        RedirectToRespectivePage(e, startdateFriday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        protected void RadSaturdayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateSaturday = GetDateMonday(StartDate).AddDays(5);
                        RedirectToRespectivePage(e, startdateSaturday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        protected void RadSundayGridContextMenu_ItemClick(object sender, RadMenuEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        startdateSunday = GetDateMonday(StartDate).AddDays(6);
                        RedirectToRespectivePage(e, startdateSunday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        private void RedirectToRespectivePage(RadMenuEventArgs e, DateTime gridDay)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e))
            {
                var selectedTripId = Request.Form["radGridSelectedTripId"];
                var selectedDate = Request.Form["radGridSelectedStartDate"];
                var selectedTailNum = Request.Form["radGridSelectedTailNum"];
                var selectedLegNum = Request.Form["radGridClickedLegNum"];
                Session.Remove("CurrentPreFlightTrip");
                if (e.Item.Value == string.Empty)
                {
                    return;
                }
                if (selectedDate == "" || selectedDate == "undefined")// for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                {

                    if (e.Item.Text == "Preflight" || e.Item.Text == "Add New Trip")
                    {
                        /// 12/11/2012
                        ///Changes done for displaying the homebase of login user
                        PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                        PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                        Trip.EstDepartureDT = gridDay;
                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                        HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                        HomebaseSample.BaseDescription = BaseDescription;
                        Trip.HomeBaseAirportICAOID = HomeBaseCode;
                        Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                        Trip.HomebaseID = HomeBaseId;
                        Trip.Company = HomebaseSample;
                        Session["CurrentPreFlightTrip"] = Trip;
                        Session.Remove("PreflightException");
                        Trip.State = TripEntityState.Added;
                        Trip.Mode = TripActionMode.Edit;

                        if (e.Item.Text == "Add New Trip")
                            Response.Redirect(e.Item.Value);
                        else if (Trip.TripNUM == null || Trip.TripNUM == 0)                        
                        {
                            Session["CurrentPreFlightTrip"] = null;
                            Session[WebSessionKeys.CurrentPreflightTrip] = null;
                        }
                        
                        Response.Redirect(e.Item.Value);
                    }
                    if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                    {
                        var tripnum = string.Empty;
                        Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                    }
                }
                else
                {
                    if (e.Item.Text == "Add New Trip")
                    {
                        PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                        PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                        PreflightService.Aircraft AircraftSample = new PreflightService.Aircraft();
                        Trip.EstDepartureDT = Convert.ToDateTime(selectedDate);
                        /// 12/11/2012
                        ///Changes done for displaying the homebase of Fleet in preflight 
                        if (selectedTailNum != null)
                        {
                            PreflightService.Company HomebaseSample = new PreflightService.Company();
                            Int64 HomebaseAirportId = 0;
                            if (fleetsample.FleetID != null)
                            {
                                Int64 fleetHomebaseId = 0;
                                Int64 fleetId = 0;
                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var FleetByTailNumber = FPKMasterService.GetFleetByTailNumber(selectedTailNum.ToString().Trim()).EntityInfo;
                                    if (FleetByTailNumber != null)
                                    {
                                        fleetId = ((FlightPak.Web.FlightPakMasterService.FleetByTailNumberResult)FleetByTailNumber).FleetID;
                                        fleetsample.TailNum = selectedTailNum.ToString();
                                        fleetsample.FleetID = fleetId;

                                    }
                                    var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                    if (FleetList.Count > 0)
                                    {
                                        GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                        if (FleetCatalogEntity.HomebaseID != null)
                                        {
                                            fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                            AircraftSample.AircraftDescription = FleetCatalogEntity.AircraftDescription;
                                            AircraftSample.AircraftCD = FleetCatalogEntity.AirCraft_AircraftCD;
                                            AircraftSample.AircraftID = Convert.ToInt64(FleetCatalogEntity.AircraftID);
                                            Trip.HomebaseID = fleetHomebaseId;
                                            Trip.Fleet = fleetsample;
                                            Trip.Aircraft = AircraftSample;
                                        }
                                    }
                                    var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                    if (CompanyList.Count > 0)
                                    {
                                        GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                        if (CompanyCatalogEntity.HomebaseAirportID != null)
                                        {
                                            HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                            HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                            Trip.Company = HomebaseSample;
                                        }
                                    }
                                    var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                    if (AirportList.Count > 0)
                                    {
                                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                        if (AirportCatalogEntity.IcaoID != null)
                                        {
                                            Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                        }
                                        if (AirportCatalogEntity.AirportID != null)
                                        {
                                            Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                        }
                                    }
                                }
                            }
                        }

                        Session["CurrentPreFlightTrip"] = Trip;
                        Session.Remove("PreflightException");
                        Trip.State = TripEntityState.Added;
                        Trip.Mode = TripActionMode.Edit;

                        /*if (Trip.TripNUM != null && Trip.TripNUM != 0)
                        {
                            Session["CurrentPreFlightTrip"] = Trip;
                            Session.Remove("PreflightException");
                            Trip.State = TripEntityState.Added;
                            Trip.Mode = TripActionMode.Edit;
                        }
                        else
                            Session["CurrentPreFlightTrip"] = null;*/
                                                    
                        Response.Redirect(e.Item.Value);
                    }
                }

                //get trip by tripId
                Int64 tripId = 0;
                if (!String.IsNullOrEmpty(selectedTripId))
                    tripId = Convert.ToInt64(selectedTripId);

                if (tripId != 0)
                {
                    using (var preflightServiceClient = new PreflightServiceClient())
                    {
                        var tripInfo = preflightServiceClient.GetTrip(tripId);
                        if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                        {
                            var trip = tripInfo.EntityList[0];

                            //Set the trip.Mode and Trip.State to NoChage for read only                    
                            trip.Mode = TripActionMode.NoChange;
                            trip.Mode = TripActionMode.Edit;
                            Session["CurrentPreFlightTrip"] = trip;
                            Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                            PreflightTripManager.populateCurrentPreflightTrip(trip);
                            Session.Remove("PreflightException");

                            if (e.Item.Text == "Outbound Instruction") // outbound instruction menu selected...
                            {
                                trip.Mode = TripActionMode.Edit;
                                trip.State= TripEntityState.Modified;
                                RadWindow5.NavigateUrl = e.Item.Value + "?IsCalender=1";
                                RadWindow5.Visible = true;
                                RadWindow5.VisibleOnPageLoad = true;
                                RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                return;
                            }
                            else if (e.Item.Text == "Leg Notes") //  Leg Notes menu selected...
                            {
                                trip.Mode = TripActionMode.Edit;
                                trip.State = TripEntityState.Modified;
                                RadWindow6.NavigateUrl = e.Item.Value + "?IsCalender=1&Leg=" + selectedLegNum;
                                RadWindow6.Visible = true;
                                RadWindow6.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Add Fleet Calendar Entry")
                            {// Fleet calendar entry                                                              
                                RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + selectedTailNum + "&startDate=" + selectedDate + "&endDate=" + selectedDate; // no need to pass tailNum since it has no resource
                                RadWindow3.Visible = true;
                                RadWindow3.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Add Crew Calendar Entry")
                            { // Crew calendar entry
                                RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + selectedDate + "&endDate=" + selectedDate;// no need to pass crewCode since it has no resource
                                RadWindow4.Visible = true;
                                RadWindow4.VisibleOnPageLoad = true;
                                return;
                            }
                            else if (e.Item.Text == "Preflight")
                            {
                                if (trip.TripNUM == null || trip.TripNUM == 0)
                                {
                                    Session["CurrentPreFlightTrip"] = null;
                                    Session[WebSessionKeys.CurrentPreflightTrip] = null;
                                    Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                }
                                else
                                    Response.Redirect("../PreflightLegs.aspx?seltab=Legs");
                            }
                            else if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                            {
                                RedirectToPage(string.Format("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml&tripnum={0}&tripid={1}", Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripNUM)), Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(trip.TripID))));
                            }
                            else
                            {
                                Response.Redirect(e.Item.Value);
                            }
                        }
                    }
                }
                else //tripid=0, that is dummy appointment
                {
                    if (e.Item.Text == "Add Fleet Calendar Entry")
                    {// Fleet calendar entry
                        if (Convert.ToString(selectedTailNum) == "undefined")
                        {
                            RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + null + "&startDate=" + gridDay + "&endDate=" + gridDay; // no need to pass tailNum since it has no resource
                        }
                        else
                        {
                            RadWindow3.NavigateUrl = e.Item.Value + "?tailNum=" + selectedTailNum + "&startDate=" + StartDate + "&endDate=" + StartDate; // no need to pass tailNum since it has no resource
                        }
                        RadWindow3.Visible = true;
                        RadWindow3.VisibleOnPageLoad = true;
                        return;
                    }
                    else if (e.Item.Text == "Add Crew Calendar Entry")
                    { // Crew calendar entry
                        RadWindow4.NavigateUrl = e.Item.Value + "?crewCode=" + null + "&startDate=" + gridDay + "&endDate=" + gridDay;// no need to pass crewCode since it has no resource
                        RadWindow4.Visible = true;
                        RadWindow4.VisibleOnPageLoad = true;
                        return;
                    }
                    else if (e.Item.Text == "Preflight")
                    {                        
                        Session["CurrentPreFlightTrip"] = null;
                        Session[WebSessionKeys.CurrentPreflightTrip] = null;
                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                    }
                    if (e.Item.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                    {
                        var tripnum = string.Empty;
                        Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                    }
                    else
                    {
                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                    }
                }
            }
        }

        private void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {

                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);

                //appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(" HH:mm");
                //appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(" HH:mm");

                if (appointment.RecordType == "C" || appointment.RecordType == "M")
                {
                    if (appointment.StartDate == appointment.HomelDepartureTime)
                    {
                        appointment.StartDateTime = appointment.StartDate.ToString("HH:mm");
                    }
                    else
                    {
                        appointment.StartDateTime = "00:00";
                    }

                    if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                    {
                        appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString("HH:mm");
                    }
                    else
                    {
                        appointment.EndDateTime = "23:59";
                    }
                }
                else
                {
                    appointment.StartDateTime = appointment.StartDate.ToString("HH:mm");
                    appointment.EndDateTime = appointment.EndDate.ToString("HH:mm");
                }
                SetDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                appointment.TripDate = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture); // TripDate property is used to show arrivalDate
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns
                }
            }
        }

        private void SetCrewAppointment(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {

                appointment = SetCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);

                //appointment.StartDateTime = appointment.DepartureDisplayTime.ToString(" HH:mm");
                //appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString(" HH:mm");                

                if (appointment.RecordType == "C" || appointment.RecordType == "M")
                {
                    if (appointment.StartDate == appointment.HomelDepartureTime)
                    {
                        appointment.StartDateTime = appointment.StartDate.ToString("HH:mm");
                    }
                    else
                    {
                        appointment.StartDateTime = "00:00";
                    }                    

                    if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                    {
                        appointment.EndDateTime = appointment.ArrivalDisplayTime.ToString("HH:mm");
                    }
                    else
                    {
                        appointment.EndDateTime =  "23:59";
                    }
                }
                else
                {
                    appointment.StartDateTime = appointment.StartDate.ToString("HH:mm");
                    appointment.EndDateTime = appointment.EndDate.ToString("HH:mm");
                }
                
                SetCrewDepartureAndArrivalInfo(displayOptions, tripLeg, appointment);
                appointment.TripDate = appointment.ArrivalDisplayTime.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture); // TripDate property is used to show arrivalDate
                if (appointment.IsRONAppointment)
                {
                    appointment.DepartureICAOID = appointment.ArrivalICAOID; //show only arrival icao for both departure and arrival columns
                }
            }
        }

        private Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                foreach (var leg in calendarData)
                {
                    if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                    {
                        int i = 0;
                        DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                        DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                        TimeSpan span = EndDate.Subtract(StartDate);
                        int totaldays = 0;

                        if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                        {
                            totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                        }
                        else
                        {
                            totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                        }

                        while (i < totaldays)
                        {
                            leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i);
                            leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i) : EndDate;

                            FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                            SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                            appointmentCollection.Add(appointment);
                            i++;
                        }
                    }
                    else
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                        SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                        appointmentCollection.Add(appointment);
                    }                  
                }
                return appointmentCollection;
            }
        }

        private Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();
                var distinctCalendarData = calendarData.Where(x => x.LegNUM > 0).GroupBy(i => new { i.TripID, i.LegNUM }, (key, group) => group.First()).ToList(); //added to avoid duplicate data...
                var ronCalendarData = calendarData.Where(x => x.LegNUM == 0);
                distinctCalendarData.AddRange(ronCalendarData);
                foreach (var leg in distinctCalendarData)
                {
                    if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                    {
                        int i = 0;
                        DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                        DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                        TimeSpan span = EndDate.Subtract(StartDate);
                        int totaldays = 0;

                        if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                        {
                            totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                        }
                        else
                        {
                            totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                        }

                        while (i < totaldays)
                        {
                            leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i);
                            leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i) : EndDate;

                            FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                            SetCrewAppointment(distinctCalendarData, timeBase, displayOptions, viewMode, leg, appointment);
                            appointmentCollection.Add(appointment);
                            i++;
                        }
                    }
                    else
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                        SetCrewAppointment(distinctCalendarData, timeBase, displayOptions, viewMode, leg, appointment);
                        appointmentCollection.Add(appointment);
                    }                       
                }
                return appointmentCollection;
            }
        }

        private void SetCrewDepartureAndArrivalInfo(DisplayOptions displayOptions, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {
                if (appointment.ShowAllDetails)
                {
                    switch (displayOptions.Weekly.DepartArriveInfo)
                    {

                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;
                    }
                }
                else // Show as per company profile settings + display option settings
                {
                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (displayOptions.Weekly.DepartArriveInfo)
                        {

                            case DepartArriveInfo.ICAO:
                                appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;
                        }
                    }
                    //else
                    //{
                    //    appointment.DepartureICAOID = string.Empty;
                    //    appointment.ArrivalICAOID = string.Empty;
                    //}

                    //if (!PrivateTrip.IsShowArrivalDepartTime)
                    //{
                    //    appointment.StartDateTime = string.Empty;
                    //    appointment.EndDateTime = string.Empty;
                    //}
                }                
            }
        }        

        private void SetDepartureAndArrivalInfo(DisplayOptions displayOptions, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(displayOptions, tripLeg, appointment))
            {
                if (appointment.ShowAllDetails)
                {
                    switch (displayOptions.Weekly.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;
                    }
                }
                else // Show as per company profile settings + display option settings
                {
                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (displayOptions.Weekly.DepartArriveInfo)
                        {
                            case DepartArriveInfo.ICAO:
                                appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;
                        }
                    }
                    //else
                    //{
                    //    appointment.DepartureICAOID = string.Empty;
                    //    appointment.ArrivalICAOID = string.Empty;
                    //}

                    //if (!PrivateTrip.IsShowArrivalDepartTime)
                    //{
                    //    appointment.StartDateTime = string.Empty;
                    //    appointment.EndDateTime = string.Empty;

                    //}
                }
            }
        }

        protected DateTime GetDateMonday(DateTime selectedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedDate))
            {
                if ((int)selectedDate.DayOfWeek == 0) // Sunday
                {
                    return selectedDate.AddDays(-6);
                }
                else
                {
                    return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1);
                }
            }
        }

        protected void radMain_Checked(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("WeeklyMain.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void dgWeeklyMainMonday_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateMonday);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        private void BindDataToGrid(GridItemEventArgs e, DateTime gridDay)
        {
            FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Item.DataItem;
            if (e.Item is GridDataItem)
            {

                if (DisplayOptions.Weekly.BlackWhiteColor)
                {
                    e.Item.BackColor = Color.White;
                    e.Item.ForeColor = Color.Black;
                }
                else
                {
                    if (!DisplayOptions.Weekly.AircraftColors)
                    {
                        if (item.RecordType == "T")
                        {
                            if (item.IsRONAppointment)
                            {
                                // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                if (WeeklyFleetTreeCount > 0) // fleet view
                                {
                                    if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.AircraftDutyCD))
                                    {
                                        var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == item.AircraftDutyCD.Trim()).FirstOrDefault();
                                        if (dutyCode != null)
                                        {
                                            //sujitha
                                            e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                            ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                            //e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                        }
                                    }
                                }
                                else if (WeeklyCrewTreeCount > 0)// crew view => CrewTreeCount > 0
                                {
                                    if (CrewDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.CrewDutyTypeCD))
                                    {
                                        var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == item.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                        if (dutyCode != null)
                                        {
                                            //sujitha
                                            ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                            e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                            //e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                SetFlightCategoryColor(e, e.Item);
                            }
                        }
                        else if (item.RecordType == "M")
                        {
                            // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                            SetAircraftDutyColor(e, e.Item);
                        }
                        else if (item.RecordType == "C")
                        {
                            // set crew duty type color...
                            SetCrewDutyColor(e, e.Item);
                        }
                    }
                    else if (DisplayOptions.Weekly.AircraftColors && item.TailNum == null && item.RecordType == "C")
                    {
                        SetCrewDutyColor(e, e.Item);
                    }
                    else
                    {
                        if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                        {
                            e.Item.BackColor = Color.FromName(item.AircraftBackColor);
                            ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbTailNum")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbCrewCodesTop")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbTripNum")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbLegNum")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbTripStatus")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbETE")).ForeColor = Color.FromName(item.AircraftForeColor);
                            //((Label)e.Item.FindControl("lbCumETE")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbCrewDutyType")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbAircraftDutyType")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbArrivalDateInfo")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbFlightNo")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbPAXCount")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbFlightCategoryCode")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbPassengerRequestorCD")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbDepartmentName")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbAuthorizationCD")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbTripPurpose")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbFlightPurpose")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbReservationAvailable")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbCrewCodesBottom")).ForeColor = Color.FromName(item.AircraftForeColor);
                            ((Label)e.Item.FindControl("lbDescription")).ForeColor = Color.FromName(item.AircraftForeColor);
                            e.Item.ForeColor = Color.FromName(item.AircraftForeColor);
                        }
                    }     
                }

                string encodedText = "";
                if (!DisplayOptions.Weekly.DisplayRedEyeFlightsInContinuation)
                {
                    if (item.ArrivalDisplayTime.Date == item.DepartureDisplayTime.Date)
                    {
                        ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = false;
                        encodedText = String.Format("{0} {1} | {2} {3}", item.StartDateTime, item.DepartureICAOID, item.ArrivalICAOID, item.EndDateTime);
                        encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                        ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText;
                    }

                    else //when enddate is not equal to start date and current leg binding is for start date, show arrival date info label
                    {
                        if (gridDay.Date == item.DepartureDisplayTime.Date)
                        {
                            if (item.RecordType == "T")
                            {
                                ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = true; // show arrival date info label
                            }
                            encodedText = String.Format("{0} {1} | {2} {3}", item.StartDateTime, item.DepartureICAOID, item.ArrivalICAOID, item.EndDateTime);
                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                            ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText;
                        }
                        else
                        {
                            ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = false;
                             encodedText = String.Format("{0} {1} | {2} {3}", item.StartDateTime, item.DepartureICAOID, item.ArrivalICAOID, item.EndDateTime);
                             encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                             ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText;
                        }
                    }
                }
                else
                {
                    if (item.ArrivalDisplayTime.Date == item.DepartureDisplayTime.Date)
                    {
                        ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = false;
                        encodedText = String.Format("{0} {1} | {2} {3}", item.StartDateTime, item.DepartureICAOID, item.ArrivalICAOID, item.EndDateTime);
                        encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                        ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText;
                    }
                    else
                    {

                        if (gridDay.Date == item.DepartureDisplayTime.Date) //when this is start day of leg
                        {
                            ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = false; // show arrival date info label
                            encodedText = String.Format("{0} {1} [Continued]", item.StartDateTime, item.DepartureICAOID);
                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                            ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText;
                        }
                        else if (gridDay.Date == item.ArrivalDisplayTime.Date) // end day of leg
                        {
                            ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = false;
                            encodedText = String.Format("[Continued] {0} {1}",item.ArrivalICAOID, item.EndDateTime);
                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                            ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text =encodedText;
                            ((HtmlTableRow)e.Item.FindControl("Row4")).Visible = false;
                        }
                        else // dont show the leg info in middle days...
                        {
                            e.Item.Display = false;
                            //((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible = false;
                            //((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = item.StartDateTime + " " + item.DepartureICAOID + " | " + item.ArrivalICAOID + item.EndDateTime;
                        }
                    }
                }

                encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(item.TailNum);//
                ((Label)e.Item.FindControl("lbTailNum")).Text = String.Format("<b> {0} </b>", encodedText);
                if (item.RecordType == "T")
                {// Record Type = Trip (T)

                    if (item.IsRONAppointment)
                    {
                        ((HtmlTableRow)e.Item.FindControl("Row4")).Visible = false;
                        ((HtmlTableRow)e.Item.FindControl("Row2")).Visible = false;
                        ((HtmlTableRow)e.Item.FindControl("Row3")).Visible = false;
                        ((HtmlTableRow)e.Item.FindControl("Row5")).Visible = true;
                        ((HtmlTableRow)e.Item.FindControl("Row6")).Visible = false;
                        ((HtmlTableRow)e.Item.FindControl("Row1")).Visible = true;
                        if (item.CrewCD == null) // fleet tree mode
                        {
                            encodedText = String.Format("{0} {1} ( {2} ) {3}", item.CrewCD, item.TailNum, item.AircraftDutyCD, item.ArrivalICAOID);
                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                            ((Label)e.Item.FindControl("lbCrewCD")).Text = String.Format("<b> {0} </b>", encodedText);
                        }
                        else
                        {
                            var builder = new StringBuilder();
                            builder.Append("<b>" + item.CrewCD);
                            builder.Append(" ");
                            builder.Append(item.TailNum);
                            builder.Append("</b></br>(");
                            builder.Append(item.CrewDutyTypeCD);
                            builder.Append(") ");
                            builder.Append(item.ArrivalICAOID);
                            var flightNumber = string.Empty;
                            if (DisplayOptions.Weekly.FlightNumber)
                            {
                                flightNumber = " Flt No.: " + item.FlightNum;
                                builder.Append(" ");
                                builder.Append(flightNumber);
                            }
                            // ((Label)e.Item.FindControl("lbCrewCD")).Text = " <b>" + item.CrewCD + item.TailNum +"</b>" + "<br> (" + item.CrewDutyTypeCD + ") " + item.ArrivalICAOID + "</br>";
                             encodedText = Microsoft.Security.Application.Encoder.HtmlEncode( builder.ToString());
                             ((Label)e.Item.FindControl("lbCrewCD")).Text = encodedText;
                        }
                        ((Label)e.Item.FindControl("lbTailNum")).Visible = false;
                        ((Label)e.Item.FindControl("lbCrewCodesTop")).Visible = false;
                    }
                    else // NORMAL APPOINTMENTS
                    {
                        ((HtmlTableRow)e.Item.FindControl("Row2")).Visible = DisplayOptions.Weekly.ShowTrip;
                        ((HtmlTableRow)e.Item.FindControl("Row6")).Visible = ((Label)e.Item.FindControl("lbArrivalDateInfo")).Visible;

                        ((Label)e.Item.FindControl("lbLegNum")).Visible = true;
                        ((Label)e.Item.FindControl("lbETE")).Visible = false;
                        //((Label)e.Item.FindControl("lbCumETE")).Visible = false;

                        string hyphen = string.Empty;
                        if (item.TailNum != null)
                        {
                            hyphen = " - ";
                        }

                        if (item.ShowAllDetails)
                        {
                            ((Label)e.Item.FindControl("lbCrewCodesTop")).Visible = DisplayOptions.Weekly.Crew;

                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(string.IsNullOrEmpty(item.CrewCodes) ? string.Empty : String.Format("{0} {1}", hyphen,item.CrewCodes));//string.IsNullOrEmpty(item.CrewCodes) ? string.Empty : hyphen + "<b>" + item.CrewCodes + "</b>"
                            ((Label)e.Item.FindControl("lbCrewCodesTop")).Text = String.Format("<b> {0} </b>", encodedText);

                            ((Label)e.Item.FindControl("lbCrewCD")).Visible = false;

                            ((Label)e.Item.FindControl("lbTripStatus")).Visible = DisplayOptions.Weekly.ShowTripStatus;

                            ((Label)e.Item.FindControl("lbFlightNo")).Visible = DisplayOptions.Weekly.FlightNumber;
                            ((Label)e.Item.FindControl("lbTripPurpose")).Visible = DisplayOptions.Weekly.TripPurpose;
                            ((Label)e.Item.FindControl("lbFlightPurpose")).Visible = DisplayOptions.Weekly.LegPurpose;

                            ((Label)e.Item.FindControl("lbReservationAvailable")).Visible = DisplayOptions.Weekly.SeatsAvailable;
                            ((Label)e.Item.FindControl("lbDepartmentName")).Visible = DisplayOptions.Weekly.Department;
                            ((Label)e.Item.FindControl("lbAuthorizationCD")).Visible = DisplayOptions.Weekly.Authorization;
                            ((Label)e.Item.FindControl("lbPassengerRequestorCD")).Visible = DisplayOptions.Weekly.Requestor;
                            ((Label)e.Item.FindControl("lbPAXCount")).Visible = DisplayOptions.Weekly.PaxCount;

                            //((Label)e.Item.FindControl("lbETE")).Visible = DisplayOptions.Weekly.ETE;
                            ((Label)e.Item.FindControl("lbETE")).Visible = DisplayOptions.Weekly.CummulativeETE;
                            ((Label)e.Item.FindControl("lbFlightCategoryCode")).Visible = DisplayOptions.Weekly.FlightCategory;

                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(string.IsNullOrEmpty(item.FlightCategoryCode) ? string.Empty :String.Format("({0})", item.FlightCategoryCode));
                            ((Label)e.Item.FindControl("lbFlightCategoryCode")).Text = encodedText;
                        }
                        else // set details based on company profile settings and display options
                        {

                            if (!PrivateTrip.IsShowArrivalDepartTime)
                            {
                                ((HtmlTableRow)e.Item.FindControl("Row6")).Visible = false;
                                ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Visible = false;
                            }

                            if (!PrivateTrip.IsShowArrivalDepartICAO)
                            {
                                ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Visible = false;
                            }

                            if (PrivateTrip.IsShowCrew)
                            {
                                ((Label)e.Item.FindControl("lbCrewCodesTop")).Visible = DisplayOptions.Weekly.Crew;
                                encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(string.IsNullOrEmpty(item.CrewCodes) ? string.Empty : String.Format("{0} {1}", hyphen, item.CrewCodes));//string.IsNullOrEmpty(item.CrewCodes) ? string.Empty : hyphen + "<b>" + item.CrewCodes + "</b>"
                                ((Label)e.Item.FindControl("lbCrewCodesTop")).Text = String.Format("<b> {0} </b>", encodedText);
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbCrewCodesTop")).Visible = false;
                            }


                            ((Label)e.Item.FindControl("lbCrewCD")).Visible = false;

                            ((Label)e.Item.FindControl("lbTripStatus")).Visible = DisplayOptions.Weekly.ShowTripStatus;

                            if (PrivateTrip.IsShowFlightNumber)
                            {
                                ((Label)e.Item.FindControl("lbFlightNo")).Visible = DisplayOptions.Weekly.FlightNumber;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbFlightNo")).Visible = false;
                            }

                            if (PrivateTrip.IsShowTripPurpose)
                            {
                                ((Label)e.Item.FindControl("lbTripPurpose")).Visible = DisplayOptions.Weekly.TripPurpose;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbTripPurpose")).Visible = false;
                            }

                            if (PrivateTrip.IsShowLegPurpose)
                            {
                                ((Label)e.Item.FindControl("lbFlightPurpose")).Visible = DisplayOptions.Weekly.LegPurpose;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbFlightPurpose")).Visible = false;
                            }

                            if (PrivateTrip.IsShowSeatsAvailable)
                            {
                                ((Label)e.Item.FindControl("lbReservationAvailable")).Visible = DisplayOptions.Weekly.SeatsAvailable;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbReservationAvailable")).Visible = false;
                            }

                            if (PrivateTrip.IsShowDepartment)
                            {
                                ((Label)e.Item.FindControl("lbDepartmentName")).Visible = DisplayOptions.Weekly.Department;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbDepartmentName")).Visible = false;
                            }

                            if (PrivateTrip.IsShowAuthorization)
                            {
                                ((Label)e.Item.FindControl("lbAuthorizationCD")).Visible = DisplayOptions.Weekly.Authorization;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbAuthorizationCD")).Visible = false;
                            }

                            if (PrivateTrip.IsShowRequestor)
                            {
                                ((Label)e.Item.FindControl("lbPassengerRequestorCD")).Visible = DisplayOptions.Weekly.Requestor;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbPassengerRequestorCD")).Visible = false;
                            }

                            if (PrivateTrip.IsShowPaxCount)
                            {
                                ((Label)e.Item.FindControl("lbPAXCount")).Visible = DisplayOptions.Weekly.PaxCount;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbPAXCount")).Visible = false;
                            }

                            //if (PrivateTrip.IsShowETE)
                            //{
                            //    ((Label)e.Item.FindControl("lbETE")).Visible = DisplayOptions.Weekly.ETE;
                            //}
                            //else
                            //{
                            //    ((Label)e.Item.FindControl("lbETE")).Visible = false;
                            //}

                            if (PrivateTrip.IsShowCumulativeETE)
                            {
                                ((Label)e.Item.FindControl("lbETE")).Visible = DisplayOptions.Weekly.CummulativeETE;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbETE")).Visible = false;
                            }

                            if (PrivateTrip.IsShowFlightCategory)
                            {
                                ((Label)e.Item.FindControl("lbFlightCategoryCode")).Visible = DisplayOptions.Weekly.FlightCategory;
                                encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(string.IsNullOrEmpty(item.FlightCategoryCode) ? string.Empty : String.Format("({0})", item.FlightCategoryCode));
                                ((Label)e.Item.FindControl("lbFlightCategoryCode")).Text = encodedText;
                            }
                            else
                            {
                                ((Label)e.Item.FindControl("lbFlightCategoryCode")).Visible = false;
                            }


                        }
                    }
                }

                else if (item.RecordType == "M")
                { // Record Type = Fleet (M)

                    ((HtmlTableRow)e.Item.FindControl("Row4")).Visible = false;
                    ((HtmlTableRow)e.Item.FindControl("Row2")).Visible = false;
                    ((HtmlTableRow)e.Item.FindControl("Row5")).Visible = true;
                    ((HtmlTableRow)e.Item.FindControl("Row6")).Visible = false;

                    ((Label)e.Item.FindControl("lbAircraftDutyType")).Visible = true;
                    // ((Label)e.Item.FindControl("lbArrivalICAOID")).Visible = false;
                    encodedText = String.Format("{0} {1} {2}", item.StartDateTime, item.DepartureICAOID, item.EndDateTime);
                    encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                    ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText;
                    ((Label)e.Item.FindControl("lbDescription")).Visible = true;
                    encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(item.Notes);
                    ((Label)e.Item.FindControl("lbDescription")).Text = encodedText;
                    ((Label)e.Item.FindControl("lbFlightNo")).Visible = false;
                    ((Label)e.Item.FindControl("lbLegNum")).Visible = false;
                    ((Label)e.Item.FindControl("lbCrewCD")).Visible = false;
                }

                else if (item.RecordType == "C")
                {// Record Type = Crew (C)


                    // Record Type = Crew (C)
                    ((HtmlTableRow)e.Item.FindControl("Row1")).Visible = false;
                    ((HtmlTableRow)e.Item.FindControl("Row2")).Visible = false;
                    ((HtmlTableRow)e.Item.FindControl("Row4")).Visible = false;
                    ((HtmlTableRow)e.Item.FindControl("Row5")).Visible = true;
                    ((HtmlTableRow)e.Item.FindControl("Row6")).Visible = false;


                    ((Label)e.Item.FindControl("lbCrewDutyType")).Visible = true;

                    // ((Label)e.Item.FindControl("lbArrivalICAOID")).Visible = false;
                    encodedText = String.Format("{0} {1} {2}", item.StartDateTime, item.DepartureICAOID,item.EndDateTime);
                    encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(encodedText);
                    ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).Text = encodedText ;
                    ((Label)e.Item.FindControl("lbCrewCodesBottom")).Visible = false;
                    ((Label)e.Item.FindControl("lbDescription")).Visible = true;
                    encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(item.Notes);
                    ((Label)e.Item.FindControl("lbDescription")).Text = encodedText;
                    ((Label)e.Item.FindControl("lbFlightNo")).Visible = false;
                    ((Label)e.Item.FindControl("lbLegNum")).Visible = false;
                    ((Label)e.Item.FindControl("lbCrewCD")).Visible = false;


                    if (!string.IsNullOrEmpty(item.TailNum) || !string.IsNullOrEmpty(item.CrewCD))
                    {
                        ((HtmlTableRow)e.Item.FindControl("Row1")).Visible = true;
                        ((Label)e.Item.FindControl("lbCrewCodesTop")).Visible = false;
                        ((Label)e.Item.FindControl("lbCrewCD")).Visible = true;
                        if (!string.IsNullOrEmpty(item.TailNum))
                        {
                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(item.CrewCD);
                            ((Label)e.Item.FindControl("lbCrewCD")).Text = String.Format("<b> {0} </b>", encodedText);
                        }
                        else
                        {
                            encodedText = Microsoft.Security.Application.Encoder.HtmlEncode(item.CrewCD);
                            ((Label)e.Item.FindControl("lbCrewCD")).Text = String.Format("<b> {0} </b>", encodedText);
                        }
                    }
                }
            }
        }

        protected void dgWeeklyMainTuesday_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateTuesday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void dgWeeklyMainWednesday_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateWednesday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void dgWeeklyMainThursday_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateThursday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void dgWeeklyMainFriday_Databound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateFriday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void dgWeeklyMainSaturday_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateSaturday);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void dgWeeklyMainSunday_Databound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindDataToGrid(e, startdateSunday);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }


        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyMainPreviousCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            loadTreeGrid(false);
                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);

                        }
                        Session["SCWeeklyMainPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void nextButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyMainNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(1);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(1);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }

                        Session["SCWeeklyMainNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }

        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyMainLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(7);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(7);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }

                        Session["SCWeeklyMainLastCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCWeeklyMainFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            //DateTime today = DateTime.Today;
                            StartDate = selectedDate.AddDays(-7);
                            // EndDate = StartDate.AddHours(24).AddMilliseconds(-1);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            StartDate = selectedDate.AddDays(-7);
                            // EndDate = GetWeekEndDate(StartDate);

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }

                        Session["SCWeeklyMainFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }
        // ON click of Apply button...

        protected void btnApply_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;
                            RightPane.Collapsed = true;
                            loadTreeGrid(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //RadDatePicker1.SelectedDate = DateTime.Today;
                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                        StartDate = DateTime.Today;
                        // EndDate = GetWeekEndDate(StartDate);
                        Session["SCSelectedDay"] = StartDate;
                        loadTreeGrid(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            // EndDate = GetWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;
                            loadTreeGrid(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }
        }

        protected void SetAircraftDutyColor(GridItemEventArgs e, GridItem item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)item.DataItem;
                var appointmentAircraftDuty = appointmentItem.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Item.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                        //sujitha
                        ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTailNum")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCodesTop")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripNum")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbLegNum")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripStatus")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbETE")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        //((Label)e.Item.FindControl("lbCumETE")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewDutyType")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbAircraftDutyType")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbArrivalDateInfo")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightNo")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbPAXCount")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightCategoryCode")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbPassengerRequestorCD")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbDepartmentName")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbAuthorizationCD")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripPurpose")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightPurpose")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbReservationAvailable")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCodesBottom")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbDescription")).ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                        //e.Item.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        protected void SetFlightCategoryColor(GridItemEventArgs e, GridItem item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)item.DataItem;
                var appointmentCategory = appointmentItem.FlightCategoryID;

                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTailNum")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCodesTop")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripNum")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbLegNum")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripStatus")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbETE")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        //((Label)e.Item.FindControl("lbCumETE")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewDutyType")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbAircraftDutyType")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbArrivalDateInfo")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightNo")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbPAXCount")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightCategoryCode")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbPassengerRequestorCD")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbDepartmentName")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbAuthorizationCD")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripPurpose")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightPurpose")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbReservationAvailable")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCodesBottom")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbDescription")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);

                    }

                }

            }

        }

        protected void SetCrewDutyColor(GridItemEventArgs e, GridItem item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {

                var appointmentItem = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)item.DataItem;
                var appointmentCategory = appointmentItem.CrewDutyType;

                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Item.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCD")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTailNum")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCodesTop")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripNum")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbLegNum")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripStatus")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbETE")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        //((Label)e.Item.FindControl("lbCumETE")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewDutyType")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbAircraftDutyType")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbArrivalDepartureInfo")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbArrivalDateInfo")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightNo")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbPAXCount")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightCategoryCode")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbPassengerRequestorCD")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbDepartmentName")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbAuthorizationCD")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbTripPurpose")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbFlightPurpose")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbReservationAvailable")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbCrewCodesBottom")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        ((Label)e.Item.FindControl("lbDescription")).ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                        //e.Item.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }

                }

            }

        }

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.WeeklyMain);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTreeGrid(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.WeeklyMain);
                }
            }

        }
    }
}