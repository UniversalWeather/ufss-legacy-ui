﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightSIFL.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightSIFL" EnableViewState="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Standard Industry Fare Level</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <telerik:RadCodeBlock ID="RadCodeblock1" runat="server">
        <script type="text/javascript">
            function alertCallArrAirFn(arg) {
                document.getElementById("<%=tbArrives.ClientID%>").focus();
            }

            function alertCallDepAirFn(arg) {
                document.getElementById("<%=tbDeparts.ClientID%>").focus();
            }

            function alertNotPaxFn(arg) {
                document.getElementById("<%=tbPax.ClientID%>").focus();
            }

            function AssocPaxAlert(arg) {
                document.getElementById("<%=tbAssocPax.ClientID%>").focus();
            }

            function HtmlEncode(str) {
                return str.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&apos;/g, '\'');
            }

            function alertCallBackFn(arg) {
                document.getElementById("<%=btnNew.ClientID%>").focus();
            }

            function alertNoCusFn(arg) {
                document.getElementById("<%=btnNew.ClientID%>").focus();
            }

            function openWin(radWin) {

                var url = '';

                if (radWin == "RadPassengerAssociatePopup") {
                    url = '/Views/Settings/People/PassengerAssociatePopup.aspx';
                }

                if (radWin == "radPaxInfoPopup") {
                    url = '/Views/Transactions/Preflight/PreflightSIFLPaxInfoPopup.aspx?PassengerRequestorID=' + document.getElementById('<%=tbPax.ClientID%>').value;
                }

                if (radWin == "rdDepartPopup") {
                    url = '/Views/Transactions/Preflight/PreFlightSIFLAirport.aspx?flag=Depart&Departs=' + document.getElementById('<%=tbDeparts.ClientID%>').value;
                }

                if (radWin == "rdArrivePopup") {
                    url = '/Views/Transactions/Preflight/PreFlightSIFLAirport.aspx?flag=Arrival&Arrives=' + document.getElementById('<%=tbArrives.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            // this function is used to display the value of selected Passenger code from popup
            function PassengerAssociatePopupClose(oWnd, args) {
                var combo = $find("<%= tbAssocPax.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //var 
                if (arg !== null) {
                    if (arg) {

                        var paxCode = HtmlEncode(arg.PassengerRequestorCD);
                        var paxName = HtmlEncode(arg.PassengerName);

                        document.getElementById("<%=lbcvAssoc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=tbAssocPax.ClientID%>").value = paxCode;
                        //alert(arg.PassengerRequestorCD);
                        document.getElementById("<%=hdnAssocPax.ClientID%>").value = arg.PassengerRequestorID;
                        if (arg.PassengerName != null)
                            document.getElementById("<%=lbAssocPaxName.ClientID%>").innerHTML = paxName;
                        document.getElementById("<%=hdnAssCode.ClientID%>").value = paxCode;
                    }
                    else {
                        document.getElementById("<%=tbAssocPax.ClientID%>").value = "";
                        document.getElementById("<%=hdnAssocPax.ClientID%>").value = "";
                        document.getElementById("<%=lbAssocPaxName.ClientID%>").innerHTML = "";
                        document.getElementById("<%=lbcvAssoc.ClientID%>").innerHTML = "";
                        //combo.clearSelection();
                    }
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientPaxClose(oWnd, args) {
                var combo = $find("<%= tbPax.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        var paxCode = HtmlEncode(arg.PassengerRequestorCD);
                        var paxName = HtmlEncode(arg.PassengerName);

                        document.getElementById("<%=tbPax.ClientID%>").value = paxCode;
                        document.getElementById("<%=hdnSIFLPaxCode.ClientID%>").value = paxCode;
                        document.getElementById("<%=hdnPax.ClientID %>").value = arg.PassengerRequestorID;
                        document.getElementById("<%=lbPaxName.ClientID %>").innerHTML = paxName;
                        document.getElementById("<%=rbEmployeeType.ClientID%>").value = arg.IsEmployeeType;
                        document.getElementById("<%=hdnPaxLegID.ClientID%>").value = arg.LegID;
                        document.getElementById("<%=hdnIsPersonalTravel.ClientID%>").value = arg.IsPersonalTravel;
                        document.getElementById("<%=hdnPaxName.ClientID%>").value = paxName;
                        document.getElementById("<%=hdnAssociatedPassengerID.ClientID%>").value = arg.AssociatedPassengerID;
                        document.getElementById("<%=hdnAssociatedPassengerCD.ClientID%>").value = HtmlEncode(arg.AssociatedPassengerCD);
                        if (arg.IsEmployeeType == "C") {
                            document.getElementById("<%=tbAssocPax.ClientID%>").disabled = true;
                            document.getElementById("<%=btnAssocPax.ClientID%>").className = "browse-button-disabled";
                            document.getElementById("<%=btnAssocPax.ClientID%>").disabled = true;

                            var rbl = document.getElementById("<%=rbEmployeeType.ClientID%>");
                            var radio = rbl.getElementsByTagName("input");
                            radio[0].checked = true;
                        }
                        if (arg.IsEmployeeType == "N") {
                            document.getElementById("<%=tbAssocPax.ClientID%>").disabled = true;
                            document.getElementById("<%=btnAssocPax.ClientID%>").className = "browse-button-disabled";
                            document.getElementById("<%=btnAssocPax.ClientID%>").disabled = true;

                            var rbl = document.getElementById("<%=rbEmployeeType.ClientID%>");
                            var radio = rbl.getElementsByTagName("input");
                            radio[1].checked = true;
                        }
                        if (arg.IsEmployeeType == "G") {
                            document.getElementById("<%=tbAssocPax.ClientID%>").disabled = false;
                            document.getElementById("<%=btnAssocPax.ClientID%>").className = "browse-button";
                            document.getElementById("<%=btnAssocPax.ClientID%>").disabled = false;

                            var rbl = document.getElementById("<%=rbEmployeeType.ClientID%>");
                            var radio = rbl.getElementsByTagName("input");
                            radio[2].checked = true;
                        }
                        if (arg.PassengerRequestorCD != null)
                            var step = "tbPaxTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function OnClientDepartClose(oWnd, args) {

                var combo = $find("<%= tbDeparts.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=lbcvDeparts.ClientID%>").innerHTML = "";
                        document.getElementById("<%=tbDeparts.ClientID%>").value = arg.ICAOID;
                        document.getElementById("<%=hdnDeparts.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=lbDepart.ClientID%>").innerHTML = arg.AirportName;
                    }
                }
            }

            function OnClientArriveClose(oWnd, args) {
                var combo = $find("<%= tbArrives.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=lbcvArrives.ClientID%>").innerHTML = "";
                        document.getElementById("<%=tbArrives.ClientID%>").value = arg.ICAOID;
                        document.getElementById("<%=hdnArrives.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=lbArrives.ClientID%>").innerHTML = arg.AirportName;
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            //this function is used to replace default value when textbox is empty
            function validateEmptyMilesTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0";
                }
            }

            //this function is used to replace default value when textbox is empty
            function validateEmptyMultiplerTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.0";
                }
            }

            //this function is used to replace default value when textbox is empty
            function validateEmptyTerminalTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.00";
                }
            }

            //this function is used to replace default value when textbox is empty
            function validateEmptyRateTextbox(ctrlID, e) {
                if (ctrlID.value == "") {
                    ctrlID.value = "0.0000";
                }
            }
            function validateEmptyRadRateTextbox(sender, eventArgs) {

                if (sender.get_textBoxValue().length < 0) {
                    sender.set_value('0.0000');
                }
            }
            function fnAllowNumeric(e) {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                var reg = new RegExp("[0-9.]")
                if (key == 8) {
                    keychar = String.fromCharCode(key);
                }
                if (key == 13) {
                    key = 8;
                    keychar = String.fromCharCode(key);
                }
                return reg.test(keychar);
            }

            function alertCallBackFn(arg) {
                //alert(arg);
                if (arg == true) {
                    document.getElementById('<%=btnAlert.ClientID%>').click();
                }
            }

            function alertCallBack() {
            }

            function refreshPage(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }
            /*
            * <summary>
            * Function to allow Numeric with customized Characters (i.e., for Date, Phone etc. Eg: 10/12/2011, 123-456-7890)
            * </summary>
            * <param name="myfield">Control Id to be passed</param>
            * <param name="e">Event to be passed</param>
            * <param name="char">Character you need to allow to be passed</param>
            **/
            function fnAllowNumericAndChar(myfield, e, char) {
                var key;
                var keychar;
                var allowedString = "0123456789" + char;

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);

                // control keys
                if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    return true;
                // numeric values and allow slash("/")
                else if ((allowedString).indexOf(keychar) > -1)
                    return true;
                else
                    return false;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="tbPax" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rbEmployeeType" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbAssocPax" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbDeparts" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbArrives" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="btnUpdateSIFL" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgSifl" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnReCalcSIFL" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnNew" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnEdit" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnDelete" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnSave" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnCancel" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rbEmployeeType" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Div1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="tbPax" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rbEmployeeType" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbAssocPax" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbDeparts" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbArrives" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbStatueMiles" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbAircraftMultiplier" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbMiles1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbRate1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbMiles2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbRate2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbMiles3" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbRate3" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbTerminalCharge" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbSIFLTotal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnUpdateSIFL" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="dgSifl" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="btnReCalcSIFL" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnNew" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnEdit" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnDelete" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnSave" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnCancel" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateSIFL">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="btnUpdateSIFL" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgSifl">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgSifl" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnReCalcSIFL">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgSifl" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateSIFL">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="btnUpdateSIFL" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnNew" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <%--<telerik:AjaxSetting AjaxControlID="btnEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnEdit" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <%--<telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnDelete" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnSave" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="rbEmployeeType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rbEmployeeType" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div id="Div1" runat="server">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientPaxClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFLPaxInfoPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadPassengerAssociatePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="PassengerAssociatePopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerAssociatePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdDepartPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientDepartClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreFlightSIFLAirport.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdArrivePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientArriveClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreFlightSIFLAirport.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <asp:Panel ID="pnlMessge" Visible="false" runat="server">
            <table cellspacing="0" cellpadding="0" class="box1">
                <tr>
                    <td>
                        <asp:Label ID="lblSIFL" CssClass="alert-text" runat="server" Text="SIFL"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblentries" CssClass="alert-text" runat="server" Text="entries"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblare" CssClass="alert-text" runat="server" Text="are"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblonly" CssClass="alert-text" runat="server" Text="only"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblallowed" CssClass="alert-text" runat="server" Text="allowed"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblfor" CssClass="alert-text" runat="server" Text="for"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblFlight" CssClass="alert-text" runat="server" Text="Flight"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblLogs" CssClass="alert-text" runat="server" Text="Logs"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblcontaining" CssClass="alert-text" runat="server" Text="containing"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lbldatabase" CssClass="alert-text" runat="server" Text="database"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblpassengers" CssClass="alert-text" runat="server" Text="passengers"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblwith" CssClass="alert-text" runat="server" Text="with"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblpersonal" CssClass="alert-text" runat="server" Text="personal"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lbltravel" CssClass="alert-text" runat="server" Text="travel."></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlContents" runat="server">
            <table cellpadding="0" cellspacing="0" class="box1" width="100%">
                <tr>
                    <td>
                        <telerik:RadTabStrip ID="rtSIFL" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                            MultiPageID="RadMultiPage1" SelectedIndex="0" Align="Justify" Style="float: inherit">
                            <Tabs>
                                <telerik:RadTab Value="SIFLMaintenance" Text="SIFL Maintenance" Selected="true">
                                </telerik:RadTab>
                                <telerik:RadTab Value="LegInformation" Text="Leg Information">
                                </telerik:RadTab>
                            </Tabs>
                        </telerik:RadTabStrip>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table class="border-box">
                                            <tr>
                                                <td width="50%">
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td width="50%">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel40">
                                                                            PAX
                                                                        </td>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPax" CssClass="text70" runat="server" MaxLength="5" AutoPostBack="true"
                                                                                            OnTextChanged="tbPaxTextChanged"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnSIFLPaxCode" runat="server" />
                                                                                        <asp:HiddenField ID="hdnPax" runat="server" />
                                                                                        <asp:HiddenField ID="hdnPaxLegID" runat="server" />
                                                                                        <asp:HiddenField ID="hdnIsPersonalTravel" runat="server" />
                                                                                        <asp:HiddenField ID="hdnAssociatedPassengerID" runat="server" />
                                                                                        <asp:HiddenField ID="hdnAssociatedPassengerCD" runat="server" />
                                                                                        <asp:Button ID="btnPax" runat="server" OnClientClick="javascript:openWin('radPaxInfoPopup');return false;"
                                                                                            class="browse-button" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lbPaxName" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                        <asp:HiddenField ID="hdnPaxName" runat="server" />
                                                                                        <asp:Label ID="lbcvPaxName" runat="server" CssClass="alert-text"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="50%" align="right">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RadioButtonList ID="rbEmployeeType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="EmployeeType_SelectedIndexChanged"
                                                                                RepeatDirection="Horizontal">
                                                                                <asp:ListItem Text="Control" Value="C"></asp:ListItem>
                                                                                <asp:ListItem Selected="True" Text="Non-Control" Value="N"></asp:ListItem>
                                                                                <asp:ListItem Text="Guest" Value="G"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table class="border-box">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        Assoc. With
                                                                                    </td>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbAssocPax" CssClass="text70" runat="server" MaxLength="5" OnTextChanged="tbAssocPaxChanged"
                                                                                                        AutoPostBack="true"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnAssocPax" runat="server" EnableViewState="true" />
                                                                                                    <asp:HiddenField ID="hdnAssCode" runat="server" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Button class="browse-button" ID="btnAssocPax" runat="server" OnClientClick="javascript:openWin('RadPassengerAssociatePopup');return false;" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lbAssocPaxName" CssClass="input_no_bg" runat="server" Visible="true"
                                                                                            EnableViewState="true"></asp:Label>
                                                                                        <asp:Label ID="lbcvAssoc" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        Departs
                                                                                    </td>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbDeparts" CssClass="text70" runat="server" MaxLength="4" AutoPostBack="true"
                                                                                                        OnTextChanged="DepartsTextChanged"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnDeparts" runat="server" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Button ID="btnDeparts" class="browse-button" runat="server" OnClientClick="javascript:openWin('rdDepartPopup');return false;" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lbcvDeparts" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                                        <asp:Label ID="lbDepart" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                                                        <asp:HiddenField ID="hdnDepartName" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        Arrives
                                                                                    </td>
                                                                                    <td class="tdLabel110">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbArrives" CssClass="text70" runat="server" MaxLength="4" AutoPostBack="true"
                                                                                                        EnableViewState="true" ViewStateMode="enabled" OnTextChanged="ArrivesTextChanged"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnArrives" runat="server" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Button ID="btnArrives" runat="server" OnClientClick="javascript:openWin('rdArrivePopup');return false;"
                                                                                                        class="browse-button" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel70">
                                                                                        <asp:Button ID="btnUpdateSIFL" runat="server" CssClass="button" Text="Update" OnClick="UpdateSIFL_Click" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <span class="alert-text">Click on the Update button to populate the values below.</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lbcvArrives" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                                        <asp:Label ID="lbArrives" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                                                        <asp:HiddenField ID="hdnArriveName" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="tblspace_5">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="border-box" id="tblSifl" runat="server">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        Statute Miles
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbStatueMiles" CssClass="text70 rt_text" runat="server" MaxLength="5"
                                                                                            AutoPostBack="true" OnTextChanged="StatueMilesChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            onBlur="return validateEmptyMilesTextbox(this, event)">
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        Aircraft Multiplier
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbAircraftMultiplier" CssClass="text70 rt_text" runat="server" MaxLength="5"
                                                                                            AutoPostBack="true" OnTextChanged="AircraftMultiplierChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            onBlur="return validateEmptyMultiplerTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        <span class="mnd_text">Mile Range</span>
                                                                                    </td>
                                                                                    <td class="tdLabel110">
                                                                                        <span class="mnd_text">Miles</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span class="mnd_text">Fare Level</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        0 - 500
                                                                                    </td>
                                                                                    <td class="tdLabel110">
                                                                                        <asp:TextBox ID="tbMiles1" CssClass="text70 rt_text" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                            onBlur="return validateEmptyMilesTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbRate1" CssClass="text70 rt_text" runat="server" MaxLength="8"
                                                                                            AutoPostBack="true" OnTextChanged="Rate1Changed" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            onBlur="return validateEmptyRateTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        501 - 1500
                                                                                    </td>
                                                                                    <td class="tdLabel110">
                                                                                        <asp:TextBox ID="tbMiles2" CssClass="text70 rt_text" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                            onBlur="return validateEmptyMilesTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbRate2" CssClass="text70 rt_text" runat="server" MaxLength="8"
                                                                                            AutoPostBack="true" OnTextChanged="Rate2Changed" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            onBlur="return validateEmptyRateTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        Over 1500
                                                                                    </td>
                                                                                    <td class="tdLabel110">
                                                                                        <asp:TextBox ID="tbMiles3" CssClass="text70 rt_text" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                            onBlur="return validateEmptyMilesTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbRate3" CssClass="text70 rt_text" runat="server" MaxLength="8"
                                                                                            AutoPostBack="true" OnTextChanged="Rate3Changed" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            onBlur="return validateEmptyRateTextbox(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        Terminal Charge
                                                                                    </td>
                                                                                    <td class="pr_radtextbox_76">
                                                                                        <telerik:RadNumericTextBox ID="tbTerminalCharge" runat="server" Type="Currency" Culture="en-US"
                                                                                            AutoPostBack="true" MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="."
                                                                                            EnabledStyle-HorizontalAlign="Right" OnTextChanged="TerminalChargeChanged">
                                                                                        </telerik:RadNumericTextBox>
                                                                                        <%-- <asp:TextBox ID="tbTerminalCharge" CssClass="text70 rt_text" runat="server" MaxLength="6"
                                                                                            AutoPostBack="true" OnTextChanged="TerminalChargeChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            onBlur="return validateEmptyTerminalTextbox(this, event)"></asp:TextBox>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        SIFL Total
                                                                                    </td>
                                                                                    <td class="pr_radtextbox_76">
                                                                                        <telerik:RadNumericTextBox ID="tbSIFLTotal" runat="server" Type="Currency" Culture="en-US"
                                                                                            MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                                                                            EnabledStyle-BackColor="#EDEDED">
                                                                                        </telerik:RadNumericTextBox>
                                                                                        <%-- <asp:TextBox ID="tbSIFLTotal" CssClass="text70 rt_text" runat="server" MaxLength="6"
                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onBlur="return validateEmptyTerminalTextbox(this, event)"></asp:TextBox>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        Grand Total
                                                                                    </td>
                                                                                    <td class="pr_radtextbox_76">
                                                                                        <telerik:RadNumericTextBox ID="tbGrandTotal" runat="server" Type="Currency" Culture="en-US"
                                                                                            MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                                                                            EnabledStyle-BackColor="#EDEDED">
                                                                                        </telerik:RadNumericTextBox>
                                                                                        <%--<asp:TextBox ID="tbGrandtotal" CssClass="text70 inpt_non_edit rt_text" runat="server"></asp:TextBox>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblSiflException" runat="server" CssClass="alert-text" Text="Flight Scheduling Software has estimated the SIFL calculations for the trip. Please review these records to ensure accuracy"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <telerik:RadGrid ID="dgSifl" runat="server" AllowSorting="true" Visible="true" OnSelectedIndexChanged="Sifl_SelectedIndexChanged"
                                                        AllowFilteringByColumn="true" Width="550px" AutoGenerateColumns="false" PageSize="1000"
                                                        Height="135px" AllowPaging="false" PagerStyle-AlwaysVisible="false">
                                                        <PagerStyle Visible="false" />
                                                        <MasterTableView DataKeyNames="PreflightTripSIFLID,LegID,CustomerID,FareLevelID,PassengerRequestorID,
                                                                    PassengerName,EmployeeTYPE,AssociatedPassengerID,DepartICAOID,ArriveICAOID,Distance,AircraftMultiplier,Rate1,Rate2,Rate3"
                                                            CommandItemDisplay="None" Width="100%" PageSize="1000" ShowHeader="true" AllowFilteringByColumn="false">
                                                            <Columns>
                                                                <telerik:GridTemplateColumn HeaderText="PAX" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                    ShowFilterIcon="false" UniqueName="PaxCode" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbPaxCode" runat="server" Text='<%#Eval("Passenger1.PassengerRequestorCD")%>'></asp:Label>
                                                                        <asp:HiddenField ID="hdnPaxID" runat="server" Value='<%#Eval("PassengerRequestorID")%>' />
                                                                        <asp:HiddenField ID="hdnAssocPaxCD" runat="server" Value='<%#Eval("Passenger.PassengerRequestorCD") %>' />
                                                                        <asp:HiddenField ID="hdnAssocPaxID" runat="server" Value='<%#Eval("AssociatedPassengerID") %>' />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="CNG" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                    ShowFilterIcon="false" UniqueName="EmployeeTYPE" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbEmployeeType" runat="server" Text='<%#Eval("EmployeeTYPE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Depart" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="Depart" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDepartCD" runat="server" Text='<%#Eval("Airport1.IcaoID")%>' CssClass="text40"></asp:Label>
                                                                        <asp:HiddenField ID="hdnDepartID" runat="server" Value='<%#Eval("DepartICAOID")%>' />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Arrive" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="Arrive" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbArriveCD" runat="server" Text='<%#Eval("Airport.IcaoID")%>' CssClass="text40"></asp:Label>
                                                                        <asp:HiddenField ID="hdnArriveID" runat="server" Value='<%#Eval("ArriveICAOID")%>' />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="L/F" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                    ShowFilterIcon="false" UniqueName="LoadFactor" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbLoadFactor" runat="server" Text='<%#Eval("LoadFactor") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Statute" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="Statute" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbStatute" runat="server" Text='<%#Eval("Distance")%>' CssClass="text40"></asp:Label>
                                                                        <asp:HiddenField ID="hdnDistance1" runat="server" Value='<%#Eval("Distance1")%>' />
                                                                        <asp:HiddenField ID="hdnDistance2" runat="server" Value='<%#Eval("Distance2")%>' />
                                                                        <asp:HiddenField ID="hdnDistance3" runat="server" Value='<%#Eval("Distance3")%>' />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="T. Charge" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="TeminalCharge" AllowFiltering="false"
                                                                    ItemStyle-CssClass="fc_textbox">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label ID="lbTCharge" runat="server" Text='<%#Eval("TeminalCharge")%>' CssClass="text40"></asp:Label>--%>
                                                                        <telerik:RadNumericTextBox ID="tbTeminalCharge" runat="server" Type="Currency" Culture="en-US"
                                                                            DbValue='<%# Bind("TeminalCharge") %>' NumberFormat-DecimalSeparator="." ReadOnly="true">
                                                                        </telerik:RadNumericTextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="SIFL Total" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="SIFLTotal" AllowFiltering="false"
                                                                    ItemStyle-CssClass="fc_textbox">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label ID="lbSIFLTotal" runat="server" Text='<%#Eval("AmtTotal")%>' CssClass="text40"></asp:Label>--%>
                                                                        <telerik:RadNumericTextBox ID="tbAmtTotal" runat="server" Type="Currency" Culture="en-US"
                                                                            DbValue='<%# Bind("AmtTotal") %>' NumberFormat-DecimalSeparator="." ReadOnly="true">
                                                                        </telerik:RadNumericTextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                        </MasterTableView>
                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" EnableVirtualScrollPaging="false">
                                                            </Scrolling>
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnReCalcSIFL" runat="server" CssClass="button" Text="Recalc SIFL"
                                                                    CausesValidation="false" OnClientClick="if(!confirm('The Recalc Process Will Delete All Existing SIFL Records For This Trip Before Creating New Ones. Continue With The Recalc Process ?')) return false;"
                                                                    OnClick="RecalcSIFLLeg_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnNew" runat="server" OnClick="New_Click" Text="New" CssClass="button" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEdit" runat="server" OnClick="Edit_Click" Text="Edit" CssClass="button" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnDelete" runat="server" OnClick="Delete_Click" Text="Delete" CssClass="button"
                                                                    CausesValidation="false" OnClientClick="if(!confirm('Are you sure you want to delete this record?')) return false;" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnSave" runat="server" OnClick="Save_Click" Text="Save" CssClass="button" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnCancel" runat="server" OnClick="Cancel_Click" Text="Cancel" CssClass="button"
                                                                    CausesValidation="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table class="border-box">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <telerik:RadGrid ID="dgSIFLPersonal" runat="server" AllowSorting="true" Visible="true"
                                                        AutoGenerateColumns="false" PageSize="1000" AllowPaging="true" AllowFilteringByColumn="true"
                                                        PagerStyle-AlwaysVisible="false" Width="550px" Height="135px">
                                                        <PagerStyle Visible="false" />
                                                        <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" DataKeyNames="LegID,Depart,Arrive,Personal,LoadFactor"
                                                            Width="100%" PageSize="1000" ShowHeader="true">
                                                            <Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Depart" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="Depart" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDepart" runat="server" CssClass="text40" Text='<%#Eval("Depart") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Arrive" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="Arrive" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbArrive" runat="server" CssClass="text40" Text='<%#Eval("Arrive") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Personal" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="Personal" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbPersonal" runat="server" CssClass="text40" Text='<%#Eval("Personal") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="L/F" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                    ShowFilterIcon="false" UniqueName="LoadFactor" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbLoadFactor" runat="server" CssClass="text40" Text='<%#Eval("LoadFactor") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                        </MasterTableView>
                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" EnableVirtualScrollPaging="false">
                                                            </Scrolling>
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                        <%--  <asp:HiddenField ID="hdnLeg" runat="server" />--%>
                        <asp:HiddenField ID="hdnLegNUM" runat="server" />
                        <%--  <asp:HiddenField ID="hdnPaxID" runat="server" />
                                <asp:HiddenField ID="hdnLegID" runat="server" />--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tblHidden" style="display: none;">
                            <tr>
                                <td>
                                    <asp:Button ID="btnAlert" runat="server" Text="Button" OnClick="Alert_Click" />
                                    <asp:Button ID="btnDepartOK" runat="server" Text="Button" AccessKey="D" OnClick="hdnControl_Click" />
                                    <asp:Button ID="btnArrivalOK" runat="server" Text="Button" AccessKey="A" OnClick="hdnControl_Click" />
                                    <asp:Button ID="btnPaxOK" runat="server" Text="Button" AccessKey="P" OnClick="hdnControl_Click" />
                                    <asp:Button ID="btnAssocPaxOK" runat="server" Text="Button" AccessKey="S" OnClick="hdnControl_Click" />
                                    <input type="hidden" id="hdnControl" runat="server" />
                                    <input type="hidden" id="hdnFareLevel" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
