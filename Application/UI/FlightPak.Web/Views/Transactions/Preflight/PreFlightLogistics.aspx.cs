﻿using System;
using FlightPak.Web.Views.Transactions.Preflight;
using Newtonsoft.Json;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreFlightLogistics : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PreflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                
                PreflightUtils.ValidatePreflightSession();
                dynamic preflightLogisticsInitializer = new { Logistics= new ViewModels.PreflightLogisticsViewModel(), FBO = new ViewModels.PreflightFBOListViewModel(), Catering = new ViewModels.PreflightCateringDetailViewModel() };
                hdnPreflightLogisticsInitializationObject.Value = JsonConvert.SerializeObject(preflightLogisticsInitializer);
            }
        }
    }
}
