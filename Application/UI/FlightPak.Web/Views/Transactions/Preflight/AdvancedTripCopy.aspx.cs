﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Globalization;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web.Views.Settings.PreFlight
{
    [Serializable]
    public class TripCopyDetails
    {
        public string DayofWeek = "";
        public DateTime DateToCopy = DateTime.Now;


        public TripCopyDetails(string dayofWeek, DateTime dateToCopy)
        {
            this.DayofWeek = dayofWeek;
            this.DateToCopy = dateToCopy;
        }
    }

    [Serializable]
    public class DateRanges
    {
        public DateTime startDate;
        public DateTime endDate;
        public DateRanges(DateTime StartDate, DateTime EndDate)
        {
            startDate = StartDate;
            endDate = EndDate;
        }
    }

    public partial class AdvancedTripCopy : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public Int64 TripIDtobeCopied = 0;
        List<TripCopyDetails> GridList = new List<TripCopyDetails>();

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }

                        if (Request.QueryString["tripID"] != null && Request.QueryString["tripID"] != "0" && Request.QueryString["tripID"] != "")
                        {
                            TripIDtobeCopied = Convert.ToInt64(Request.QueryString["tripID"]);
                        }

                        if (!IsPostBack)
                        {
                            Session.Remove("CopyGridList");

                            radDaily.Checked = true;

                            tbStartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", DateTime.Now);
                            DayOfWeek weekDay = DateTime.Now.DayOfWeek;

                            radEndAfter.Checked = true;
                            EnableRecurcheckboxes(false);
                            //radWeekly.Checked = true;
                            //EnableRecurcheckboxes(true);
                            //setDayofWeekSelected(weekDay.ToString());
                            //tbRecurEvery.Text = "1";
                            tbRecurEvery.Text = "0";
                            tbRecurEvery.Enabled = false;
                            tbEndAfter.Text = "0";

                            if (Request.QueryString["Logistics"] != null && Request.QueryString["Logistics"].ToLower() == "yes")
                            {
                                chkLogistics.Checked = true;
                            }
                            if (Request.QueryString["PAX"] != null && Request.QueryString["PAX"].ToLower() == "yes")
                            {
                                chkPassManifest.Checked = true;
                            }
                            if (Request.QueryString["crew"] != null && Request.QueryString["crew"].ToLower() == "yes")
                            {
                                chkCrewManifest.Checked = true;
                            }
                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "CrewCalender")
                            {
                                copy.Visible = false;
                                dgCopyGrid.Visible = false;
                                dgCopyFleetCalender.Visible = false;
                                dgCopyCalender.Visible = true;
                            }
                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "FleetCalender")
                            {
                                copy.Visible = false;
                                dgCopyGrid.Visible = false;
                                dgCopyCalender.Visible = false;
                                dgCopyFleetCalender.Visible = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        private void setDayofWeekSelected(string weekday)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(weekday))
            {
                switch (weekday.ToLower())
                {
                    case "sunday": chkSunday.Checked = true; break;
                    case "monday": chkMonday.Checked = true; break;
                    case "tuesday": chkTuesday.Checked = true; break;
                    case "wednesday": chkWednesday.Checked = true; break;
                    case "thursday": chkThursday.Checked = true; break;
                    case "friday": chkFriday.Checked = true; break;
                    case "saturday": chkSaturday.Checked = true; break;
                }
            }
        }

        private void resetDayofWeekSelected()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //chkSunday.Checked = false;
                //chkMonday.Checked = false;
                //chkTuesday.Checked = false;
                //chkWednesday.Checked = false;
                //chkThursday.Checked = false;
                //chkFriday.Checked = false;
                //chkSaturday.Checked = false;
                if (radBiWeekly.Checked || radWeekly.Checked)
                {
                    if (!string.IsNullOrEmpty(tbStartDate.Text))
                    {
                        DateTime dateToday = DateTime.Today;

                        dateToday = DateTime.ParseExact(tbStartDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                        DayOfWeek startDay = dateToday.DayOfWeek;

                        switch (startDay.ToString().ToLower())
                        {
                            case "sunday": chkSunday.Checked = true; break;
                            case "monday": chkMonday.Checked = true; break;
                            case "tuesday": chkTuesday.Checked = true; break;
                            case "wednesday": chkWednesday.Checked = true; break;
                            case "thursday": chkThursday.Checked = true; break;
                            case "friday": chkFriday.Checked = true; break;
                            case "saturday": chkSaturday.Checked = true; break;
                        }
                    }
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            //if (e.Argument.ToString() == "date_changed")
            //    resetDayofWeekSelected();

        }

        protected void bntAddDateGrid_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        btnCopy.Focus();
                        if (tbStartDate.Text == "")
                        {
                            RadWindowManager1.RadAlert("Must Have Beginning Date In Range Of Occurrences", 330, 100, "Advance Copy Alert", "alertStartDateFn", null);
                            //RadWindowManager1.RadAlert("Must Have Beginning Date In Range Of Occurrences", 330, 100, "Advance Copy Alert", null);
                            //Response.Write(@"<script language='javascript'>alert('Must have beginning date in Range Of Occurrences');</script>");
                        }

                        else
                        {

                            if (Session["CopyGridList"] != null)
                                GridList = (List<TripCopyDetails>)Session["CopyGridList"];

                            if (GridList.Count == 0)
                                GridList = new List<TripCopyDetails>();




                            DateTime dateToday = DateTime.Today;

                            dateToday = DateTime.ParseExact(tbStartDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                            DayOfWeek startDay = dateToday.DayOfWeek;

                            DateTime startDate = dateToday.Date;

                            // startDate = dateToday.ToShortDateString();

                            int startMonth = dateToday.Month;

                            int startYear = dateToday.Year;


                            if (radEndBy.Checked == true)
                            {
                                radEndAfter.Checked = false;
                            }

                            if ((radWeekly.Checked == true) | (radBiWeekly.Checked == true) | (radMonthly.Checked == true))
                            {
                                radDaily.Checked = false;
                            }

                            //End of Occurences with Daily, Weekly, Bi weekly, monthly 
                            if ((radEndAfter.Checked == true) & (tbRecurEvery.Text == "0"))
                            {
                                #region End of Occurences with Daily, Weekly, Bi weekly, monthly
                                if (tbEndAfter.Text == "0" || tbEndAfter.Text == "")
                                {
                                    RadWindowManager1.RadAlert("The number of Occurrences should be greater than 0", 330, 100, "Advance Copy Alert", "alertCallBackFn", null);
                                    //RadWindowManager1.RadAlert("The number of Occurrences should be greater than 0", 330, 100, "Advance Copy Alert", null);
                                    //Response.Write(@"<script language='javascript'>alert('The number of Occurrences should be greater than 0');</script>");
                                }
                                else
                                {

                                    if (!GridList.Any(x => x.DateToCopy == startDate))
                                        GridList.Add(new TripCopyDetails(startDay.ToString(), startDate));

                                    if (string.IsNullOrEmpty(tbEndAfter.Text))
                                        tbEndAfter.Text = "0";
                                    int occurrInput = Convert.ToInt32(tbEndAfter.Text);
                                    #region End After Occurance Daily
                                    if (radDaily.Checked == true)
                                    {
                                        int occurrInputAct = occurrInput - 1;
                                        for (int i = 0; i < occurrInputAct; i++)
                                        {
                                            DateTime Datim = startDate.AddDays(i + 1);
                                            DayOfWeek Day = startDate.AddDays(i + 1).DayOfWeek;
                                            if (!GridList.Any(x => x.DateToCopy == Datim))
                                                GridList.Add(new TripCopyDetails(Day.ToString(), Datim));
                                        }
                                    }
                                    #endregion
                                    #region End After Occurance Weekly****************
                                    if (radWeekly.Checked == true)
                                    {
                                        int j = 7;
                                        for (int i = 1; i < occurrInput; i++)
                                        {

                                            DateTime Datim = startDate.AddDays(j);
                                            DayOfWeek Day = startDate.AddDays(j).DayOfWeek;
                                            if (!GridList.Any(x => x.DateToCopy == Datim))
                                                GridList.Add(new TripCopyDetails(Day.ToString(), Datim));

                                            j = j + 7;
                                        }
                                    }
                                    #endregion
                                    #region End After Occurance Bi Weekly****************
                                    if (radBiWeekly.Checked == true)
                                    {
                                        int j = 14;
                                        for (int i = 1; i < occurrInput; i++)
                                        {

                                            DateTime Datim = startDate.AddDays(j);
                                            DayOfWeek Day = startDate.AddDays(j).DayOfWeek;
                                            if (!GridList.Any(x => x.DateToCopy == Datim))
                                                GridList.Add(new TripCopyDetails(Day.ToString(), Datim));

                                            j = j + 14;
                                        }
                                    }
                                    #endregion
                                    #region End After Occurance Monthly****************

                                    if (radMonthly.Checked == true)
                                    {
                                        for (int i = 1; i < occurrInput; i++)
                                        {
                                            DateTime Datim = startDate.AddMonths(i).AddDays(0);
                                            DayOfWeek Day = startDate.AddMonths(i).AddDays(0).DayOfWeek;
                                            if (!GridList.Any(x => x.DateToCopy == Datim))
                                                GridList.Add(new TripCopyDetails(Day.ToString(), Datim));
                                        }
                                    }
                                    #endregion

                                }

                                #endregion End of Occurences with Daily, Weekly, Bi weekly, monthly
                            }
                            //For Recurrence Parttern End After Occurance***********************
                            else if ((radEndAfter.Checked == true) & (tbRecurEvery.Text != "0"))
                            {
                                #region End Of Occurance After Occurance  Recurence pattern
                                if (tbEndAfter.Text == "0" || tbEndAfter.Text == "")
                                {
                                    RadWindowManager1.RadAlert("The number of Occurrences should be greater than 0", 330, 100, "Advance Copy Alert", "alertCallBackFn", null);
                                    //Response.Write(@"<script language='javascript'>alert('The number of Occurrences should be greater than 0');</script>");
                                }
                                else
                                {

                                    if (string.IsNullOrEmpty(tbEndAfter.Text))
                                        tbEndAfter.Text = "0";
                                    int occurrInput = Convert.ToInt32(tbEndAfter.Text);
                                    if (string.IsNullOrEmpty(tbRecurEvery.Text))
                                        tbRecurEvery.Text = "0";
                                    int occurrInputWeek = Convert.ToInt32(tbRecurEvery.Text);

                                    CheckBox[] boxes = new CheckBox[7];

                                    boxes[0] = chkSunday;
                                    boxes[1] = chkMonday;
                                    boxes[2] = chkTuesday;
                                    boxes[3] = chkWednesday;
                                    boxes[4] = chkThursday;
                                    boxes[5] = chkFriday;
                                    boxes[6] = chkSaturday;

                                    int j = 0;
                                    int k = occurrInputWeek * 7;
                                    int lmax = occurrInput - 1;

                                    int NoOfChecked = 0;

                                    foreach (CheckBox Chk in boxes)
                                    {
                                        if (Chk.Checked)
                                            NoOfChecked++;
                                    }
                                    int MaxNoGridDate = NoOfChecked * occurrInput;


                                    List<DateRanges> DateRanges = new List<DateRanges>();
                                    DateTime StartDateRange = new DateTime();
                                    DateTime EndDateRange = new DateTime();

                                    for (int i = 0; i < occurrInput; i++)
                                    {
                                        if (i == 0)
                                            StartDateRange = startDate;
                                        else
                                            StartDateRange = StartDateRange.AddDays(k);

                                        EndDateRange = StartDateRange.AddDays(6);
                                        DateRanges.Add(new DateRanges(StartDateRange, EndDateRange));

                                    }


                                    foreach (DateRanges Range in DateRanges)
                                    {
                                        DateTime RangeStartDate = Range.startDate;
                                        while (RangeStartDate <= Range.endDate)
                                        {
                                            for (int i = 0; i <= 6; i++)
                                            {
                                                if (boxes[i].Checked && boxes[i].Text == RangeStartDate.DayOfWeek.ToString())
                                                {
                                                    if (!GridList.Any(x => x.DateToCopy == RangeStartDate))
                                                        GridList.Add(new TripCopyDetails(RangeStartDate.DayOfWeek.ToString(), RangeStartDate));
                                                }
                                            }
                                            RangeStartDate = RangeStartDate.AddDays(1);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if ((radEndBy.Checked == true) & (tbRecurEvery.Text == "0"))
                            {
                                #region For End By Date and display in Daily,weekly,Bi weekly**********
                                if (tbEndDate.Text == "")
                                {
                                    RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", "alertStartGreatFn", null);
                                    //RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", null);

                                    //Response.Write(@"<script language='javascript'>alert('The End Date must be greater than the Start Date');</script>");
                                }
                                else
                                {
                                    DateTime dt1 = DateTime.ParseExact(tbStartDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    DateTime dt2 = DateTime.ParseExact(tbEndDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                    if (dt1 > dt2)
                                    {
                                        RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", "alertStartGreatFn", null);
                                        //RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", null);
                                    }
                                    else
                                    {

                                        //End date date,day ///

                                        DateTime EndDate = DateTime.ParseExact(tbEndDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);


                                        DayOfWeek endDay = EndDate.DayOfWeek;

                                        DateTime endDate = EndDate.Date;

                                        int endMonth = EndDate.Month;

                                        int endYear = EndDate.Year;

                                        TimeSpan d = Convert.ToDateTime(endDate).Subtract(Convert.ToDateTime(startDate));

                                        int dayDiff = Convert.ToInt32(d.TotalDays);
                                        if (!GridList.Any(x => x.DateToCopy == startDate))
                                            GridList.Add(new TripCopyDetails(startDay.ToString(), startDate));


                                        ///End By Date Daily************
                                        if (radDaily.Checked == true)
                                        {
                                            for (int i = 0; i < dayDiff; i++)
                                            {
                                                DateTime Datim = startDate.AddDays(i + 1);
                                                DayOfWeek Day = startDate.AddDays(i + 1).DayOfWeek;
                                                if (!GridList.Any(x => x.DateToCopy == Datim))
                                                    GridList.Add(new TripCopyDetails(Day.ToString(), Datim));

                                            }
                                        }

                                        ///End By Date Weekly************
                                        if (radWeekly.Checked == true)
                                        {

                                            int j = 7;
                                            int cntNum = dayDiff / 7;
                                            for (int i = 0; i < cntNum; i++)
                                            {
                                                DateTime Datim = startDate.AddDays(j);
                                                DayOfWeek Day = startDate.AddDays(j).DayOfWeek;
                                                if (!GridList.Any(x => x.DateToCopy == Datim))
                                                    GridList.Add(new TripCopyDetails(Day.ToString(), Datim));

                                                j = j + 7;
                                            }
                                        }

                                        ///End By Date Bi Weekly************
                                        if (radBiWeekly.Checked == true)
                                        {
                                            int j = 14;
                                            int cntNum = dayDiff / 14;
                                            for (int i = 0; i < cntNum; i++)
                                            {
                                                DateTime Datim = startDate.AddDays(j);
                                                DayOfWeek Day = startDate.AddDays(j).DayOfWeek;
                                                if (!GridList.Any(x => x.DateToCopy == Datim))
                                                    GridList.Add(new TripCopyDetails(Day.ToString(), Datim));

                                                j = j + 14;
                                            }
                                        }

                                        ///End By Date Monthly************
                                        if (radMonthly.Checked == true)
                                        {
                                            int cntNum = dayDiff / 30;

                                            for (int i = 1; i < cntNum; i++)
                                            {
                                                DateTime Datim = startDate.AddMonths(i).AddDays(0);
                                                DayOfWeek Day = startDate.AddMonths(i).AddDays(0).DayOfWeek;
                                                if (!GridList.Any(x => x.DateToCopy == Datim))
                                                    GridList.Add(new TripCopyDetails(Day.ToString(), Datim));

                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                            /// For End By Date and display in in put trhrough Days eg Sunday,Monday*******

                            else if ((radEndBy.Checked == true) & (tbRecurEvery.Text != "0"))
                            {
                                #region For End By Date and display in in put trhrough Days eg Sunday,Monday*******
                                if (tbEndDate.Text == "")
                                {
                                    RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", "alertStartGreatFn", null);
                                    //RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", null);

                                    //Response.Write(@"<script language='javascript'>alert('The End Date must be greater than the Start Date');</script>");
                                }
                                else
                                {
                                    DateTime dt1 = DateTime.ParseExact(tbStartDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    DateTime dt2 = DateTime.ParseExact(tbEndDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                    if (dt1 > dt2)
                                    {
                                        RadWindowManager1.RadAlert("The End Date must be greater than the Start Date", 330, 100, "Advance Copy Alert", "alertStartGreatFn", null);

                                        //string alertMsg = "radalert('The End Date must be greater than the Start Date');";
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    }
                                    else
                                    {


                                        //End date date,day ///

                                        DateTime EndDate = DateTime.ParseExact(tbEndDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);


                                        DayOfWeek endDay = EndDate.DayOfWeek;

                                        DateTime endDate = EndDate.Date;


                                        int endMonth = EndDate.Month;


                                        int endYear = EndDate.Year;

                                        TimeSpan d = Convert.ToDateTime(endDate).Subtract(Convert.ToDateTime(startDate));

                                        int dayDiff = Convert.ToInt32(d.TotalDays);

                                        //dt.Rows.Add(startDay, startDate);
                                        if (string.IsNullOrEmpty(tbRecurEvery.Text))
                                            tbRecurEvery.Text = "0";
                                        int occurrEveryWeek = Convert.ToInt32(tbRecurEvery.Text);
                                        if (string.IsNullOrEmpty(tbEndAfter.Text))
                                            tbEndAfter.Text = "0";
                                        int occurrInput = Convert.ToInt32(tbEndAfter.Text);

                                        int j = 1;
                                        //int l = occurrEveryWeek * 7;
                                        int k = occurrEveryWeek * 7;
                                        int lmax = occurrInput - 1;

                                        CheckBox[] boxes = new CheckBox[7];

                                        boxes[0] = chkSunday;
                                        boxes[1] = chkMonday;
                                        boxes[2] = chkTuesday;
                                        boxes[3] = chkWednesday;
                                        boxes[4] = chkThursday;
                                        boxes[5] = chkFriday;
                                        boxes[6] = chkSaturday;


                                        int NoOfChecked = 0;

                                        foreach (CheckBox Chk in boxes)
                                        {
                                            if (Chk.Checked)
                                                NoOfChecked++;
                                        }
                                        int MaxNoGridDate = NoOfChecked * occurrInput;


                                        List<DateRanges> DateRanges = new List<DateRanges>();
                                        DateTime StartDateRange = new DateTime();
                                        DateTime EndDateRange = new DateTime();

                                        j = 0;
                                        while (StartDateRange <= EndDate)
                                        {
                                            if (j == 0)
                                                StartDateRange = startDate;
                                            else
                                                StartDateRange = StartDateRange.AddDays(k);

                                            EndDateRange = StartDateRange.AddDays(6);
                                            DateRanges.Add(new DateRanges(StartDateRange, EndDateRange));
                                            j = j + 1;
                                        }


                                        foreach (DateRanges Range in DateRanges)
                                        {
                                            DateTime RangeStartDate = Range.startDate;
                                            while (RangeStartDate <= Range.endDate)
                                            {
                                                for (int i = 0; i <= 6; i++)
                                                {
                                                    if (boxes[i].Checked && boxes[i].Text == RangeStartDate.DayOfWeek.ToString())
                                                    {
                                                        if (RangeStartDate <= EndDate)
                                                        {
                                                            if (!GridList.Any(x => x.DateToCopy == RangeStartDate))
                                                                GridList.Add(new TripCopyDetails(RangeStartDate.DayOfWeek.ToString(), RangeStartDate));
                                                        }
                                                    }
                                                }
                                                RangeStartDate = RangeStartDate.AddDays(1);
                                            }
                                        }
                                    }
                                }
                                #endregion

                            }


                            //dgDateCreate.DataSource = dt;

                            Session["CopyGridList"] = GridList;

                            dgDateCreate.DataSource = (from Tripcopydet in GridList.OrderBy(x => x.DateToCopy).ToList()
                                                       select new
                                                       {
                                                           AdvCopyDay = Tripcopydet.DayofWeek,
                                                           //string passportDate = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", crewPassengerPassportLists[0].PassportExpiryDT);
                                                           DepartDate = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", Tripcopydet.DateToCopy)
                                                           //DepartDate = Tripcopydet.DateToCopy;
                                                       });

                            dgDateCreate.Enabled = true;
                            dgDateCreate.Visible = true;
                            dgDateCreate.DataBind();
                            //ViewState["dgDateCreate"] = dt;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void tbStartDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        resetDayofWeekSelected();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void radOption_Changed(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbRecurEvery.Focus();

                        if (radWeekly.Checked)
                        {
                            tbRecurEvery.Text = "1";
                            resetDayofWeekSelected();
                            EnableRecurcheckboxes(true);
                            tbRecurEvery.Enabled = true;

                        }
                        else if (radBiWeekly.Checked)
                        {
                            tbRecurEvery.Text = "2";
                            resetDayofWeekSelected();
                            EnableRecurcheckboxes(true);
                            tbRecurEvery.Enabled = false;
                        }
                        else
                        {
                            tbRecurEvery.Text = "0";
                            tbRecurEvery.Enabled = false;
                            EnableRecurcheckboxes(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }

        }

        private void EnableRecurcheckboxes(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                chkSunday.Enabled = enable;
                chkMonday.Enabled = enable;
                chkTuesday.Enabled = enable;
                chkWednesday.Enabled = enable;
                chkThursday.Enabled = enable;
                chkFriday.Enabled = enable;
                chkSaturday.Enabled = enable;
            }
        }

        protected void bntResetDateGrid_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // tbStartDate.Text = tbStartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", DateTime.Now);
                        //tbEndDate.Text = string.Empty;
                        //tbRecurEvery.Text = "1";
                        //radWeekly.Checked = true;
                        //tbEndAfter.Text = "0";
                        //radEndAfter.Checked = true;

                        DataTable dt = new DataTable();
                        dt.Reset();
                        GridList.Clear();
                        dgDateCreate.DataSource = dt;
                        Session.Remove("CopyGridList");
                        dgDateCreate.Enabled = true;
                        dgDateCreate.Visible = true;
                        dgDateCreate.DataBind();
                        dgCopyGrid.DataSource = dt;
                        dgCopyGrid.DataBind();
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgCopyGrid);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void btnCopyYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (PreflightService.PreflightServiceClient objPreflightClient = new PreflightService.PreflightServiceClient())
                        {
                            List<PreflightService.PreflightMain> CopyTripList = new List<PreflightService.PreflightMain>();
                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "CrewCalender")
                            {
                                if (TripIDtobeCopied != null && TripIDtobeCopied != 0)
                                {
                                    GridList = (List<TripCopyDetails>)Session["CopyGridList"];
                                    if (GridList != null && GridList.Count > 0)
                                    {
                                        foreach (TripCopyDetails Tripcpy in GridList)
                                        {
                                            var ObjRetval = objPreflightClient.CopyCrewCalenderDetails(Tripcpy.DateToCopy, TripIDtobeCopied, UserPrincipal.Identity._customerID); //(long)Trip.CustomerID);
                                            if (ObjRetval.ReturnFlag)
                                            {
                                                PreflightService.PreflightMain CopyTrip = new PreflightService.PreflightMain();
                                                CopyTrip = ObjRetval.EntityInfo;
                                                CopyTripList.Add(CopyTrip);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RadWindowManager1.RadAlert("Add Depart Dates to Date Grid!!!", 330, 100, "Advance Copy Alert", "alertAddDatesFn", null);
                                    }
                                }

                                if (CopyTripList.Count > 0)
                                {
                                    dgCopyCalender.DataSource = (from CpyTrip in CopyTripList.OrderBy(x => x.EstDepartureDT)
                                                                 select
                                                               new
                                                               {
                                                                   TripID = CpyTrip.TripID,
                                                                   TripNUM = CpyTrip.TripNUM,
                                                                   EstDepartureDT = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].DepartureDTTMLocal != null ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", CpyTrip.PreflightLegs[0].DepartureDTTMLocal)) : string.Empty : string.Empty,
                                                                   EstArrivalDT = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].ArrivalDTTMLocal != null ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", CpyTrip.PreflightLegs[0].ArrivalDTTMLocal)) : string.Empty : string.Empty,
                                                                   DepICAO = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].Airport1 != null ? CpyTrip.PreflightLegs[0].Airport1.IcaoID != null ? CpyTrip.PreflightLegs[0].Airport1.IcaoID : string.Empty : string.Empty : string.Empty,
                                                                   ArrICAO = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].Airport != null ? CpyTrip.PreflightLegs[0].Airport.IcaoID != null ? CpyTrip.PreflightLegs[0].Airport.IcaoID : string.Empty : string.Empty : string.Empty,
                                                                   Crew = CpyTrip.Crew != null ? CpyTrip.Crew.CrewCD != null ? CpyTrip.Crew.CrewCD : string.Empty : string.Empty,
                                                                   Duty = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].DutyTYPE != null ? CpyTrip.PreflightLegs[0].DutyTYPE : string.Empty : string.Empty,
                                                               });
                                    dgCopyCalender.DataBind();
                                }
                            }
                            else if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "FleetCalender")
                            {
                                if (TripIDtobeCopied != null && TripIDtobeCopied != 0)
                                {
                                    GridList = (List<TripCopyDetails>)Session["CopyGridList"];
                                    if (GridList != null && GridList.Count > 0)
                                    {
                                        foreach (TripCopyDetails Tripcpy in GridList)
                                        {
                                            var ObjRetval = objPreflightClient.CopyCrewCalenderDetails(Tripcpy.DateToCopy, TripIDtobeCopied, UserPrincipal.Identity._customerID); //(long)Trip.CustomerID);
                                            if (ObjRetval.ReturnFlag)
                                            {
                                                PreflightService.PreflightMain CopyTrip = new PreflightService.PreflightMain();
                                                CopyTrip = ObjRetval.EntityInfo;
                                                CopyTripList.Add(CopyTrip);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RadWindowManager1.RadAlert("Add Depart Dates to Date Grid!!!", 330, 100, "Advance Copy Alert", "alertAddDatesFn", null);
                                    }
                                }

                                if (CopyTripList.Count > 0)
                                {
                                    dgCopyFleetCalender.DataSource = (from CpyTrip in CopyTripList.OrderBy(x => x.EstDepartureDT)
                                                                      select
                                                                    new
                                                                    {
                                                                        TripID = CpyTrip.TripID,
                                                                        TripNUM = CpyTrip.TripNUM,
                                                                        EstDepartureDT = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].DepartureDTTMLocal != null ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", CpyTrip.PreflightLegs[0].DepartureDTTMLocal)) : string.Empty : string.Empty,
                                                                        EstArrivalDT = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].ArrivalDTTMLocal != null ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", CpyTrip.PreflightLegs[0].ArrivalDTTMLocal)) : string.Empty : string.Empty,
                                                                        DepICAO = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].Airport1 != null ? CpyTrip.PreflightLegs[0].Airport1.IcaoID != null ? CpyTrip.PreflightLegs[0].Airport1.IcaoID : string.Empty : string.Empty : string.Empty,
                                                                        ArrICAO = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].Airport != null ? CpyTrip.PreflightLegs[0].Airport.IcaoID != null ? CpyTrip.PreflightLegs[0].Airport.IcaoID : string.Empty : string.Empty : string.Empty,
                                                                        TailNum = CpyTrip.Fleet != null ? CpyTrip.Fleet.TailNum != null ? CpyTrip.Fleet.TailNum : string.Empty : string.Empty,
                                                                        Duty = CpyTrip.PreflightLegs[0] != null ? CpyTrip.PreflightLegs[0].DutyTYPE != null ? CpyTrip.PreflightLegs[0].DutyTYPE : string.Empty : string.Empty,
                                                                    });
                                    dgCopyFleetCalender.DataBind();
                                }
                            }
                            else
                            {
                                PreflightTripViewModel Trip = (PreflightTripViewModel)Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip];
                                if (Trip != null && Trip.TripId != 0)
                                {
                                    GridList = (List<TripCopyDetails>)Session["CopyGridList"];
                                    if (GridList != null && GridList.Count > 0)
                                    {
                                        foreach (TripCopyDetails Tripcpy in GridList)
                                        {
                                            bool varLogistics = chkLogistics.Checked;
                                            bool varPAXManifest = chkPassManifest.Checked;
                                            bool varCrewMAnifest = chkCrewManifest.Checked;
                                            //bool varHistory = chkHistory.Checked;

                                            var ObjRetval = objPreflightClient.CopyTripDetails(Tripcpy.DateToCopy, (long)Trip.TripId, (long)Trip.PreflightMain.CustomerID, varLogistics, varCrewMAnifest, varPAXManifest, false);
                                            if (ObjRetval.ReturnFlag)
                                            {
                                                PreflightService.PreflightMain CopyTrip = new PreflightService.PreflightMain();

                                                CopyTrip = ObjRetval.EntityInfo;
                                                CopyTripList.Add(CopyTrip);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RadWindowManager1.RadAlert("Add Depart Dates to Date Grid!!!", 330, 100, "Advance Copy Alert", "alertAddDatesFn", null);
                                    }
                                }

                                if (CopyTripList.Count > 0)
                                {
                                    dgCopyGrid.DataSource = (from CpyTrip in CopyTripList.OrderBy(x => x.EstDepartureDT)
                                                             select
                                                           new
                                                           {
                                                               TripID = CpyTrip.TripID,
                                                               TripNUM = CpyTrip.TripNUM,
                                                               EstDepartureDT = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", CpyTrip.EstDepartureDT),
                                                               //EstDepartureDT = CpyTrip.EstDepartureDT,
                                                               TripDescription = CpyTrip.TripDescription,
                                                               TripStatus = CpyTrip.TripStatus,
                                                               RequestDT = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", CpyTrip.RequestDT),
                                                               Except = CpyTrip.TripException
                                                           });
                                    dgCopyGrid.DataBind();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void btnCopyNo_Click(object sender, EventArgs e)
        {
        }


        protected void tbEndDate_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbEndDate.Text))
                        {
                            DateTime dtlaterStart;
                            dtlaterStart = Convert.ToDateTime(tbStartDate.Text);
                            DateTime dtDate = Convert.ToDateTime(tbEndDate.Text);


                            //double diff = dtlaterStart.Subtract(dtDate).Days / (365.25 / 12);
                            int months = (dtDate.Year - dtlaterStart.Year) * 12 + dtDate.Month - dtlaterStart.Month;
                            if (months > 6)
                            {
                                //RadWindowManager1.RadConfirm("Period is greater than 6 months. Enter Date that is within 6 months of Start Date. Close.", "confirmEnddateCallBackFn", 330, 100, null, "Confirmation!");
                                RadWindowManager1.RadAlert("Period is greater than 6 months. Enter Date that is within 6 months of Start Date. Close.", 330, 100, "Advance Copy Alert", "alertEnddateCallFn", null);
                                tbEndDate.Text = string.Empty;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void bntCopyDateGrid_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        btnCancel.Focus();
                        if (dgDateCreate.Items.Count > 0)
                        {
                            RadWindowManager1.RadConfirm("You are about to create new trips.", "confirmCopyCallBackFn", 330, 100, null, "Advance Copy Alert");
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Add Depart Dates to Date Grid!!!", 330, 100, "Advance Copy Alert", "alertAddDatesFn", null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
    }
}