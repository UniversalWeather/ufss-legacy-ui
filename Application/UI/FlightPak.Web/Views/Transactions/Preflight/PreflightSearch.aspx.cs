﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightSearch : Page
    {
       
        protected void ReloadSession(object sender, EventArgs e)
        {
             Session["SearchItemPrimaryKeyValue"] = hdSelectedTripID.Value;

             if (Session["SearchItemPrimaryKeyValue"] != null)
             {
                 long TripID = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"].ToString());
                 Session.Remove("SearchItemPrimaryKeyValue");
             }

             ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "CloseAndRebind('navigateToInserted');", true);
        }

    }
}