﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightLegInfoPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightLegInfoPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgLegInfo.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgLegInfo.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "LegID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "DepArr");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "AirportID");
                }
                if (selectedRows.length > 0) {
                    oArg.LegID = cell1.innerHTML;
                    oArg.DepArr = cell2.innerHTML;
                    oArg.AirportID = cell3.innerHTML;
                }
                else {
                    oArg.LegID = "";
                    oArg.DepArr = "";
                    oArg.AirportID = "";

                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgLegInfo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgLegInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgLegInfo" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgLegInfo" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
            OnNeedDataSource="dgLegInfo_BindData" OnItemDataBound="dgLegInfo_ItemDataBound"
            AutoGenerateColumns="false" Height="330px" PageSize="10" AllowPaging="true" Width="500px">
            <MasterTableView DataKeyNames="LegID" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="AirportID" UniqueName="AirportID" HeaderText="AirportID"
                        ShowFilterIcon="false" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg #" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DepArr" UniqueName="DepArr" HeaderText="Dep ICAO- City"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LegID" HeaderText="ID" ShowFilterIcon="false"
                        Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
