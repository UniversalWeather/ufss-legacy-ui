﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common.Constants;
using FlightPak.Web;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Views.Transactions.Preflight;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreflightReports : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.PreflightReports))
            {
                Response.Redirect("~/Views/Home.aspx?m=PreflightReports");
            }
        }
    }
}