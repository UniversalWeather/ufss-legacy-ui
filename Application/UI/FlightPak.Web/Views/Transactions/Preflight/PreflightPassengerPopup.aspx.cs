﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using System.Drawing;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightPassengerPopup : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        private bool IsValidateCustom = true;
        private bool IsUpdate = true;
        string DateFormat;
        private ExceptionManager exManager;
        private bool PassengerRequestorPageNavigated = false;
        protected FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();
        private Int64 Paxid;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // To Assign Date Format from User Control, based on Company Profile
                        if (!string.IsNullOrEmpty(Request.QueryString["Paxid"]))
                        {
                            Paxid = Convert.ToInt64(Request.QueryString["Paxid"]);
                        }

                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPassenger, dgPassenger, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgPassenger.ClientID));

                        bool check = IsAuthorized(Permission.DatabaseReports.ViewPassengerReport);
                        // lbtnReports.Visible = lbtnSaveReports.Visible = btnShowReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewPassengerReport);
                        //if (IsAuthorized(Permission.DatabaseReports.ViewPassengerReport))
                        //{
                        //    lbtnReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewPassengerReport);
                        //    lbtnSaveReports.Visible = true;
                        //    btnShowReports.Visible = true;
                        //}
                        //else
                        //{
                        //    lbtnReports.Visible = false;
                        //    lbtnSaveReports.Visible = false;
                        //    btnShowReports.Visible = false;
                        //}

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewPassengerRequestor);
                            // Method to display first record in read only format
                            DefaultSelection(true);
                            CreateDictionayForImgUpload();
                            LoadDefaultData();
                        }
                        else
                        {
                            // Date Format and Pax Code format settings
                            GetCompanyProfileInfo();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// To Display first record as default
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgPassenger.Rebind();
                    }
                    if (dgPassenger.MasterTableView.Items.Count > 0)
                    {
                        dgPassenger.SelectedIndexes.Add(0);
                        Session["PassengerRequestorID"] = dgPassenger.Items[0].GetDataKeyValue("PassengerRequestorID").ToString();
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item which they selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["PassengerRequestorID"] != null)
                    {
                        string ID = Session["PassengerRequestorID"].ToString();
                        foreach (GridDataItem Item in dgPassenger.MasterTableView.Items)
                        {
                            if (Item["PassengerRequestorID"].Text.Trim() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl;
                    insertCtl = (LinkButton)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    if (IsAuthorized(Permission.Database.AddPassengerRequestor))
                    {
                        insertCtl.Visible = true;
                        if (add)
                            insertCtl.Enabled = true;
                        else
                            insertCtl.Enabled = false;
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeletePassengerRequestor))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditPassengerRequestor))
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// <summary>
        /// To Empty all the fields
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.ReadOnly = false;
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                    GetDefaultFlightPurpose();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.ReadOnly = true;
                    hdnSave.Value = "Update";
                    LoadControlData();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //tbCode.Enabled = enable;
                    tbFirstName.Enabled = enable;
                    tbMiddleName.Enabled = enable;
                    tbLastName.Enabled = enable;
                    tbCompany.Enabled = enable;
                    chkActive1.Enabled = enable;
                    chkRequestor.Enabled = enable;
                    tbEmpId.Enabled = enable;
                    tbPaxPhone.Enabled = enable;
                    tbAddlPhone.Enabled = enable;
                    tbPaxFax.Enabled = enable;
                    tbEmail.Enabled = enable;
                    tbNationality.Enabled = enable;
                    BtnNationality.Enabled = enable;
                    tbCityOfResi.Enabled = enable;
                    btnCityOfResi.Enabled = enable;
                    tbAddress1.Enabled = enable;
                    tbAddress2.Enabled = enable;
                    tbCity.Enabled = enable;
                    tbState.Enabled = enable;
                    tbPostal.Enabled = enable;
                    rbFemale.Enabled = enable;
                    rbMale.Enabled = enable;
                    tbPinNumber.Enabled = enable;
                    ((TextBox)ucDateOfBirth.FindControl("tbDate")).Enabled = enable;
                    if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                    {
                        if (!string.IsNullOrEmpty(tbClientCde.Text))
                        {
                            if (tbClientCde.Text.Trim() == UserPrincipal.Identity._clientCd.ToString().Trim())
                            {
                                tbClientCde.Enabled = false;
                            }
                            else
                            {
                                tbClientCde.Enabled = true;
                            }
                        }
                        tbClientCde.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                        hdnClientCde.Value = UserPrincipal.Identity._clientId.ToString().Trim();
                        btnClientCde.Enabled = false;
                    }
                    else
                    {
                        tbClientCde.Enabled = enable;
                        btnClientCde.Enabled = enable;
                    }
                    if (hdnSave.Value == "Save")
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportId != 0)
                        {
                            tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                            hdnHomeBase.Value = UserPrincipal.Identity._airportId.ToString().Trim();
                        }
                    }
                    tbHomeBase.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    tbFlightPurpose.Enabled = enable;
                    btnFlightPurpose.Enabled = enable;
                    tbDepartment.Enabled = enable;
                    btnDepartment.Enabled = enable;
                    tbAuth.Enabled = enable;
                    btnAuth.Enabled = enable;
                    tbStdBilling.Enabled = enable;
                    tbAccountNumber.Enabled = enable;
                    btnAccount.Enabled = enable;
                    radType.Enabled = enable;
                    radNonControlled.Enabled = enable;
                    radGuest.Enabled = enable;
                    tbAssociated.Enabled = enable;
                    btnAssociated.Enabled = enable;
                    if ((hdnSave.Value == "Save" || hdnSave.Value == "Update") && (enable == true))
                    {
                        if (radGuest.Checked == true)
                        {
                            tbAssociated.Enabled = true;
                            btnAssociated.Enabled = true;
                        }
                        else
                        {
                            tbAssociated.Enabled = false;
                            btnAssociated.Enabled = false;
                        }
                    }
                    chkSiflSecurity.Enabled = enable;
                    tbTsenseid.Enabled = enable;
                    tbTitle.Enabled = enable;
                    tbSalaryLevel.Enabled = enable;
                    tbNotes.Enabled = enable;
                    tbPaxItems.Enabled = enable;
                    dgPassengerAddlInfo.Enabled = enable;
                    btnAddlInfo.Enabled = enable;
                    btnDeleteInfo.Enabled = enable;
                    dgVisa.Enabled = enable;
                    btnAddVisa.Enabled = enable;
                    btnDeleteVisa.Enabled = enable;
                    dgPassport.Enabled = enable;
                    btnAddPassport.Enabled = enable;
                    btnDeletePassport.Enabled = enable;
                    btnSaveChangesTop.Visible = enable;
                    btnCancelTop.Visible = enable;
                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    ddlImg.Enabled = enable;
                    tbImgName.Enabled = enable;
                    btndeleteImage.Enabled = enable;
                    fileUL.Enabled = enable;

                    //<new field>
                    tbAddress3.Enabled = enable;
                    tbOtherEmail.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbOtherPhone.Enabled = enable;
                    tbSecondaryMobile.Enabled = enable;
                    tbBusinessFax.Enabled = enable;
                    tbPrimaryMobile.Enabled = enable;
                    //<\new field>
                    pnlSearchPanel.Enabled = !(enable);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    hdnCode.Value = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbCompany.Text = string.Empty;
                    chkActive1.Checked = true;
                    chkRequestor.Checked = false;
                    tbEmpId.Text = string.Empty;
                    tbPaxPhone.Text = string.Empty;
                    tbAddlPhone.Text = string.Empty;
                    tbPaxFax.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbNationality.Text = string.Empty;
                    tbNationalityName.Text = string.Empty;
                    hdnNationality.Value = string.Empty;
                    tbCityOfResi.Text = string.Empty;
                    tbCityOfResiName.Text = string.Empty;
                    hdnCityOfResi.Value = string.Empty;
                    tbAddress1.Text = string.Empty;
                    tbAddress2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    rbMale.Checked = true;
                    rbFemale.Checked = false;
                    tbPinNumber.Text = string.Empty;
                    ((TextBox)ucDateOfBirth.FindControl("tbDate")).Text = string.Empty;
                    tbClientCde.Text = string.Empty;
                    hdnClientCde.Value = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    hdnHomeBase.Value = string.Empty;
                    tbFlightPurpose.Text = string.Empty;
                    hdnFlightPurpose.Value = string.Empty;
                    tbDepartment.Text = string.Empty;
                    tbDepartmentDesc.Text = string.Empty;
                    hdnDepartment.Value = string.Empty;
                    tbAuth.Text = string.Empty;
                    tbAuthDesc.Text = string.Empty;
                    hdnAuth.Value = string.Empty;
                    tbStdBilling.Text = string.Empty;
                    tbAccountNumber.Text = string.Empty;
                    hdnAccountNumber.Value = string.Empty;
                    radType.Checked = false;
                    radNonControlled.Checked = true;
                    radGuest.Checked = false;
                    tbAssociated.Text = string.Empty;
                    chkSiflSecurity.Checked = false;
                    tbTsenseid.Text = string.Empty;
                    tbTitle.Text = string.Empty;
                    tbSalaryLevel.Text = string.Empty;
                    tbNotes.Text = string.Empty;
                    tbPaxItems.Text = string.Empty;

                    // <new fields>
                    tbAddress3.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbSecondaryMobile.Text = string.Empty;
                    tbBusinessFax.Text = string.Empty;
                    tbPrimaryMobile.Text = string.Empty;
                    // <\new fields>

                    //Clear Passenger Additional Information grid
                    //Updated by Manish V Performance Optimization 
                    //This could have been done in a better way
                    //but due to the stray html need to call this with
                    //fake id but the impact is minimized since I've modified the 
                    //base function to limit the fetched data
                    Session.Remove("PassengerAdditionalInfo");
                    BindAdditionalInfoGrid(0);

                    //Clear Passenger Visa grid
                    Session.Remove("PassengerVisa");
                    BindVisaGrid(0);

                    //Clear Passenger Additional Information grid
                    Session.Remove("PassengerPassport");
                    BindPassportGrid(0);

                    //Clear Image related data
                    CreateDictionayForImgUpload();
                    fileUL.Enabled = false;
                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbNotes.Text = "";
                    tbImgName.Text = "";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ClearForm();
                    if (Session["PassengerRequestorID"] != null)
                    {
                        List<FlightPakMasterService.GetAllPassengerWithFilters> objPassenger = new List<GetAllPassengerWithFilters>();
                        using (FlightPakMasterService.MasterCatalogServiceClient objPassengerService = new MasterCatalogServiceClient())
                        {
                            objPassenger = objPassengerService.GetAllPassengerWithFilters(0,0,Convert.ToInt64(Session["PassengerRequestorID"].ToString()),string.Empty,false,false,string.Empty,0).EntityList;

                            if (objPassenger.Count != 0)
                            {
                                //if (objPassenger[0].PassengerRequestorID != null)
                                //{
                                hdnCode.Value = objPassenger[0].PassengerRequestorID.ToString();
                                //}
                                if (objPassenger[0].PassengerRequestorCD != null)
                                { tbCode.Text = objPassenger[0].PassengerRequestorCD.ToString(); }
                                if (objPassenger[0].FirstName != null)
                                { tbFirstName.Text = objPassenger[0].FirstName.ToString(); }
                                if (objPassenger[0].MiddleInitial != null)
                                { tbMiddleName.Text = objPassenger[0].MiddleInitial.ToString(); }
                                if (objPassenger[0].LastName != null)
                                { tbLastName.Text = objPassenger[0].LastName.ToString(); }
                                if (objPassenger[0].CompanyName != null)
                                { tbCompany.Text = objPassenger[0].CompanyName.ToString(); }
                                if (objPassenger[0].IsActive != null)
                                { chkActive1.Checked = (bool)objPassenger[0].IsActive; }
                                else
                                { chkActive1.Checked = false; }
                                if (objPassenger[0].IsRequestor != null)
                                { chkRequestor.Checked = (bool)objPassenger[0].IsRequestor; }
                                else
                                { chkRequestor.Checked = false; }
                                if (objPassenger[0].EmployeeID != null)
                                { tbEmpId.Text = objPassenger[0].EmployeeID.ToString(); }
                                if (objPassenger[0].PhoneNum != null)
                                { tbPaxPhone.Text = objPassenger[0].PhoneNum.ToString(); }
                                if (objPassenger[0].AdditionalPhoneNum != null)
                                { tbAddlPhone.Text = objPassenger[0].AdditionalPhoneNum.ToString(); }
                                if (objPassenger[0].FaxNum != null)
                                { tbPaxFax.Text = objPassenger[0].FaxNum.ToString(); }
                                if (objPassenger[0].EmailAddress != null)
                                { tbEmail.Text = objPassenger[0].EmailAddress.ToString(); }
                                if (objPassenger[0].NationalityCD != null)
                                { tbNationality.Text = objPassenger[0].NationalityCD.ToString(); }
                                if (objPassenger[0].NationalityName != null)
                                { tbNationalityName.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].NationalityName.ToString()); }
                                if (objPassenger[0].CountryID != null)
                                { hdnNationality.Value = objPassenger[0].CountryID.ToString(); }
                                if (objPassenger[0].ResidenceCountryCD != null)
                                { tbCityOfResi.Text = objPassenger[0].ResidenceCountryCD.ToString(); }
                                if (objPassenger[0].ResidenceCountryName != null)
                                { tbCityOfResiName.Text = System.Web.HttpUtility.HtmlEncode(objPassenger[0].ResidenceCountryName.ToString()); }
                                if (objPassenger[0].CountryOfResidenceID != null)
                                { hdnCityOfResi.Value = objPassenger[0].CountryOfResidenceID.ToString(); }
                                if (objPassenger[0].Addr1 != null)
                                { tbAddress1.Text = objPassenger[0].Addr1.ToString(); }
                                if (objPassenger[0].Addr2 != null)
                                { tbAddress2.Text = objPassenger[0].Addr2.ToString(); }
                                if (objPassenger[0].City != null)
                                { tbCity.Text = objPassenger[0].City.ToString(); }
                                if (objPassenger[0].StateName != null)
                                { tbState.Text = objPassenger[0].StateName.ToString(); }
                                if (objPassenger[0].PostalZipCD != null)
                                { tbPostal.Text = objPassenger[0].PostalZipCD.ToString(); }
                                if (objPassenger[0].Gender != null)
                                {
                                    if (objPassenger[0].Gender.ToString() == "M")
                                    { rbMale.Checked = true; }
                                    else
                                    { rbFemale.Checked = true; }
                                }
                                else
                                { rbMale.Checked = true; }
                                if (objPassenger[0].PersonalIDNum != null)
                                { tbPinNumber.Text = objPassenger[0].PersonalIDNum.ToString(); }
                                if (objPassenger[0].DateOfBirth != null)
                                {
                                    if (DateFormat != null)
                                    { ((TextBox)ucDateOfBirth.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(objPassenger[0].DateOfBirth.ToString())); }
                                    else
                                    { ((TextBox)ucDateOfBirth.FindControl("tbDate")).Text = objPassenger[0].DateOfBirth.ToString(); }
                                }
                                if (objPassenger[0].ClientCD != null)
                                { tbClientCde.Text = objPassenger[0].ClientCD.ToString(); }
                                if (objPassenger[0].ClientID != null)
                                { hdnClientCde.Value = objPassenger[0].ClientID.ToString(); }
                                if (objPassenger[0].HomeBaseCD != null)
                                { tbHomeBase.Text = objPassenger[0].HomeBaseCD.ToString(); }
                                if (objPassenger[0].HomebaseID != null)
                                { hdnHomeBase.Value = objPassenger[0].HomebaseID.ToString(); }
                                if (objPassenger[0].FlightPurposeCD != null)
                                { tbFlightPurpose.Text = objPassenger[0].FlightPurposeCD.ToString(); }
                                if (objPassenger[0].FlightPurposeID != null)
                                { hdnFlightPurpose.Value = objPassenger[0].FlightPurposeID.ToString(); }
                                if (objPassenger[0].DepartmentCD != null)
                                { tbDepartment.Text = objPassenger[0].DepartmentCD.ToString(); }
                                if (objPassenger[0].DepartmentID != null)
                                { hdnDepartment.Value = objPassenger[0].DepartmentID.ToString(); }
                                if (objPassenger[0].AuthorizationCD != null)
                                { tbAuth.Text = objPassenger[0].AuthorizationCD.ToString(); }
                                if (objPassenger[0].AuthorizationID != null)
                                { hdnAuth.Value = objPassenger[0].AuthorizationID.ToString(); }
                                if (objPassenger[0].StandardBilling != null)
                                { tbStdBilling.Text = objPassenger[0].StandardBilling.ToString(); }
                                if (objPassenger[0].AccountNum != null)
                                { tbAccountNumber.Text = objPassenger[0].AccountNum.ToString(); }
                                if (objPassenger[0].AccountID != null)
                                { hdnAccountNumber.Value = objPassenger[0].AccountID.ToString(); }
                                if (objPassenger[0].IsEmployeeType != null)
                                {
                                    if (objPassenger[0].IsEmployeeType.ToString() == "C")
                                    {
                                        radType.Checked = true;
                                        radNonControlled.Checked = false;
                                        radGuest.Checked = false;
                                        tbAssociated.Text = string.Empty;
                                        tbAssociated.Enabled = false;
                                        btnAssociated.Enabled = false;
                                    }
                                    else if (objPassenger[0].IsEmployeeType.ToString() == "N")
                                    {
                                        radNonControlled.Checked = true;
                                        radType.Checked = false;
                                        radGuest.Checked = false;
                                        tbAssociated.Text = string.Empty;
                                        tbAssociated.Enabled = false;
                                        btnAssociated.Enabled = false;
                                    }
                                    else if (objPassenger[0].IsEmployeeType.ToString() == "G")
                                    {
                                        radGuest.Checked = true;
                                        radType.Checked = false;
                                        radNonControlled.Checked = false;
                                        if (objPassenger[0].AssociatedWithCD != null)
                                        {
                                            hdnAssociated.Value = objPassenger[0].AssociatedWithCD.ToString();
                                            tbAssociated.Text = objPassenger[0].AssociatedWithCD.ToString();
                                        }
                                        tbAssociated.Enabled = true;
                                        btnAssociated.Enabled = true;
                                    }
                                }
                                else
                                {
                                    radNonControlled.Checked = false;
                                    radType.Checked = false;
                                    radGuest.Checked = false;
                                    tbAssociated.Text = string.Empty;
                                    tbAssociated.Enabled = false;
                                    btnAssociated.Enabled = false;
                                }
                                if (objPassenger[0].IsSIFL != null)
                                { chkSiflSecurity.Checked = (bool)objPassenger[0].IsSIFL; }
                                else
                                { chkSiflSecurity.Checked = false; }
                                if (objPassenger[0].SSN != null)
                                { tbTsenseid.Text = objPassenger[0].SSN.ToString(); }
                                if (objPassenger[0].Title != null)
                                { tbTitle.Text = objPassenger[0].Title.ToString(); }
                                if (objPassenger[0].SalaryLevel != null)
                                { tbSalaryLevel.Text = objPassenger[0].SalaryLevel.ToString(); }
                                if (objPassenger[0].Notes != null)
                                { tbNotes.Text = objPassenger[0].Notes.ToString(); }
                                if (objPassenger[0].PassengerAlert != null)
                                { tbPaxItems.Text = objPassenger[0].PassengerAlert.ToString(); }

                                // <new fields>
                                if (objPassenger[0].Addr3 != null)
                                { tbAddress3.Text = objPassenger[0].Addr3.ToString(); }
                                if (objPassenger[0].OtherEmail != null)
                                { tbOtherEmail.Text = objPassenger[0].OtherEmail.ToString(); }
                                if (objPassenger[0].PersonalEmail != null)
                                { tbPersonalEmail.Text = objPassenger[0].PersonalEmail.ToString(); }
                                if (objPassenger[0].OtherPhone != null)
                                { tbOtherPhone.Text = objPassenger[0].OtherPhone.ToString(); }
                                if (objPassenger[0].CellPhoneNum2 != null)
                                { tbSecondaryMobile.Text = objPassenger[0].CellPhoneNum2.ToString(); }
                                if (objPassenger[0].BusinessFax != null)
                                { tbBusinessFax.Text = objPassenger[0].BusinessFax.ToString(); }
                                if (objPassenger[0].PrimaryMobile != null)
                                { tbPrimaryMobile.Text = objPassenger[0].PrimaryMobile.ToString(); }
                                // <\new fields>

                                if (CompanySett.IsEnableTSAPX != null)
                                {
                                    if (CompanySett.IsEnableTSAPX == true)
                                    {
                                        if (objPassenger[0].TSAStatus != null)
                                        {
                                            string LastUpdTS = string.Empty;
                                            if (objPassenger[0].LastUpdTS != null)
                                            {
                                                LastUpdTS = objPassenger[0].LastUpdTS.ToString();
                                            }
                                            if (objPassenger[0].TSAStatus.ToString() == "R")
                                            {
                                                lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode("Potentially Restricted from flying by TSA as per list date " + LastUpdTS);
                                                lbIsEnableTSAPX.ForeColor = Color.Red;

                                            }
                                            else if (objPassenger[0].TSAStatus.ToString() == "S")
                                            {
                                                lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode("Part of selectee list, potentially restricted for flying by TSA as Per List Dated " + LastUpdTS);
                                                lbIsEnableTSAPX.ForeColor = Color.Yellow;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lbIsEnableTSAPX.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbIsEnableTSAPX.ForeColor = Color.Black;
                                    }
                                }

                                // Bind Passenger Additional Information, Visa, Passport from DB
                                BindAdditionalInfoGrid(Convert.ToInt64(objPassenger[0].PassengerRequestorID));
                                BindVisaGrid(Convert.ToInt64(objPassenger[0].PassengerRequestorID));
                                BindPassportGrid(Convert.ToInt64(objPassenger[0].PassengerRequestorID));

                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (objPassenger[0].LastUpdUID != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + objPassenger[0].LastUpdUID.ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }
                                if (objPassenger[0].LastUpdTS != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDate(Convert.ToDateTime(objPassenger[0].LastUpdTS.ToString())));
                                }
                                #region Get Image from Database
                                CreateDictionayForImgUpload();
                                imgFile.ImageUrl = "";
                                ImgPopup.ImageUrl = null;
                                using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ReturnImage = ImgService.GetFileWarehouseList("Passenger", Convert.ToInt64(Session["PassengerRequestorID"].ToString())).EntityList;
                                    ddlImg.Items.Clear();
                                    ddlImg.Text = "";
                                    int i = 0;
                                    foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImage)
                                    {
                                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                                        ddlImg.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(FWH.UWAFileName), FWH.UWAFileName));
                                        DicImage.Add(FWH.UWAFileName, FWH.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = FWH.UWAFilePath;
                                            imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
                                    }
                                }
                                #endregion
                                //break;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private FlightPakMasterService.Passenger GetItems(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    if (IsEmptyCheck)
                    {
                        if (hdnSave.Value == "Update")
                        {
                            oPassenger.PassengerRequestorID = Convert.ToInt64(hdnCode.Value);
                            oPassenger.CustomerID = Convert.ToInt64(UserPrincipal.Identity._customerID);
                            //Convert.ToInt64(((GridDataItem)dgPassenger.SelectedItems[0]).GetDataKeyValue("CustomerID").ToString());
                        }
                        oPassenger.PassengerRequestorCD = GetPassengerCode(CompanySett);
                        oPassenger.FirstName = tbFirstName.Text;
                        oPassenger.LastName = tbLastName.Text;
                        if (!String.IsNullOrEmpty(tbMiddleName.Text))
                        {
                            oPassenger.MiddleInitial = tbMiddleName.Text;
                        }
                        oPassenger.PassengerName = tbLastName.Text + ", " + tbFirstName.Text + " " + tbMiddleName.Text;
                        if (!String.IsNullOrEmpty(tbCompany.Text))
                        {
                            oPassenger.CompanyName = tbCompany.Text;
                        }
                        oPassenger.IsActive = chkActive1.Checked;
                        oPassenger.IsRequestor = chkRequestor.Checked;
                        oPassenger.IsScheduledServiceCoord = chkSchdServCoord.Checked;
                        if (!String.IsNullOrEmpty(tbEmpId.Text))
                        {
                            oPassenger.EmployeeID = tbEmpId.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPaxPhone.Text))
                        {
                            oPassenger.PhoneNum = tbPaxPhone.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAddlPhone.Text))
                        {
                            oPassenger.AdditionalPhoneNum = tbAddlPhone.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPaxFax.Text))
                        {
                            oPassenger.FaxNum = tbPaxFax.Text;
                        }
                        if (!String.IsNullOrEmpty(tbEmail.Text))
                        {
                            oPassenger.EmailAddress = tbEmail.Text;
                        }
                        if (!String.IsNullOrEmpty(tbNationality.Text))
                        {
                            oPassenger.CountryID = Convert.ToInt64(hdnNationality.Value);
                        }
                        if (!String.IsNullOrEmpty(tbCityOfResi.Text))
                        {
                            oPassenger.CountryOfResidenceID = Convert.ToInt64(hdnCityOfResi.Value);
                        }
                        if (!String.IsNullOrEmpty(tbAddress1.Text))
                        {
                            oPassenger.Addr1 = tbAddress1.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAddress2.Text))
                        {
                            oPassenger.Addr2 = tbAddress2.Text;
                        }
                        if (!String.IsNullOrEmpty(tbCity.Text))
                        {
                            oPassenger.City = tbCity.Text;
                        }
                        if (!String.IsNullOrEmpty(tbState.Text))
                        {
                            oPassenger.StateName = tbState.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPostal.Text))
                        {
                            oPassenger.PostalZipCD = tbPostal.Text;
                        }
                        if (rbMale.Checked == true)
                        {
                            oPassenger.Gender = "M";
                        }
                        else
                        {
                            oPassenger.Gender = "F";
                        }
                        if (!String.IsNullOrEmpty(tbPinNumber.Text))
                        {
                            oPassenger.PersonalIDNum = tbPinNumber.Text;
                        }
                        if (!String.IsNullOrEmpty(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text))
                        {
                            oPassenger.DateOfBirth = FormatDate(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text, DateFormat);
                        }
                        if (!String.IsNullOrEmpty(tbClientCde.Text))
                        {
                            oPassenger.ClientID = Convert.ToInt64(hdnClientCde.Value);
                        }
                        if (!String.IsNullOrEmpty(tbHomeBase.Text))
                        {
                            oPassenger.HomebaseID = Convert.ToInt64(hdnHomeBase.Value);
                        }
                        if (!String.IsNullOrEmpty(tbFlightPurpose.Text))
                        {
                            oPassenger.FlightPurposeID = Convert.ToInt64(hdnFlightPurpose.Value);
                        }
                        if (!String.IsNullOrEmpty(tbDepartment.Text))
                        {
                            oPassenger.DepartmentID = Convert.ToInt64(hdnDepartment.Value);
                            oPassenger.PassengerDescription = tbDepartment.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAuth.Text))
                        {
                            oPassenger.AuthorizationID = Convert.ToInt64(hdnAuth.Value);
                            oPassenger.AuthorizationDescription = tbAuth.Text;
                        }
                        if (!String.IsNullOrEmpty(tbStdBilling.Text))
                        {
                            oPassenger.StandardBilling = tbStdBilling.Text;
                        }
                        if (!String.IsNullOrEmpty(tbAccountNumber.Text))
                        {
                            oPassenger.AccountID = Convert.ToInt64(hdnAccountNumber.Value);
                        }
                        if (!String.IsNullOrEmpty(tbNotes.Text))
                        {
                            oPassenger.Notes = tbNotes.Text;
                        }
                        if (!String.IsNullOrEmpty(tbPaxItems.Text))
                        {
                            oPassenger.PassengerAlert = tbPaxItems.Text;
                        }
                        if (radGuest.Checked == true)
                        {
                            oPassenger.IsEmployeeType = "G";
                            if (!String.IsNullOrEmpty(tbAssociated.Text))
                            {
                                oPassenger.AssociatedWithCD = tbAssociated.Text;
                            }
                        }
                        else if (radNonControlled.Checked == true)
                        {
                            oPassenger.IsEmployeeType = "N";
                        }
                        else if (radType.Checked == true)
                        {
                            oPassenger.IsEmployeeType = "C";
                        }
                        oPassenger.IsSIFL = chkSiflSecurity.Checked;
                        if (!String.IsNullOrEmpty(tbTsenseid.Text))
                        {
                            oPassenger.SSN = tbTsenseid.Text;
                        }
                        if (!String.IsNullOrEmpty(tbTitle.Text))
                        {
                            oPassenger.Title = tbTitle.Text;
                        }
                        if (!String.IsNullOrEmpty(tbSalaryLevel.Text))
                        {
                            oPassenger.SalaryLevel = Convert.ToDecimal(tbSalaryLevel.Text);
                        }
                        //else
                        //{
                        //    oPassenger.SalaryLevel = 0;
                        //}

                        // <new fields added>
                        if (!string.IsNullOrEmpty(tbAddress3.Text))
                        {
                            oPassenger.Addr3 = tbAddress3.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                        {
                            oPassenger.OtherEmail = tbOtherEmail.Text;
                        }
                        if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                        {
                            oPassenger.PersonalEmail = tbPersonalEmail.Text;
                        }
                        if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                        {
                            oPassenger.OtherPhone = tbOtherPhone.Text;
                        }
                        if (!string.IsNullOrEmpty(tbSecondaryMobile.Text))
                        {
                            oPassenger.CellPhoneNum2 = tbSecondaryMobile.Text;  // Secondary Mobile / Cell
                        }
                        if (!string.IsNullOrEmpty(tbBusinessFax.Text))
                        {
                            oPassenger.BusinessFax = tbBusinessFax.Text;
                        }
                        if (!string.IsNullOrEmpty(tbPrimaryMobile.Text))
                        {
                            oPassenger.PrimaryMobile = tbPrimaryMobile.Text;
                        }
                        // </new fields>

                        oPassenger.IsDeleted = false;
                    }
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private string GetPassengerCode(FlightPakMasterService.Company Company)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Company))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<string>(() =>
                {
                    string PassengerCD = "";
                    int Count = 1;
                    while (Count <= 3)
                    {
                        if (Company.PassengerOFullName == Count)
                        {
                            if (tbFirstName.Text.Length > 0)
                            {
                                if (Company.PassengerOLastFirst != null)
                                {
                                    if (Company.PassengerOLastFirst.Value > 0)
                                    {
                                        PassengerCD = PassengerCD + tbFirstName.Text.Substring(0, Company.PassengerOLastFirst.Value);
                                    }
                                    else if ((Company.PassengerOLastFirst.Value == 0) && (Company.PassengerOLastMiddle.Value == 0) && (Company.PassengerOLastLast.Value == 0))
                                    {
                                        PassengerCD = PassengerCD + tbFirstName.Text.Substring(0, 1);
                                    }
                                }
                                else
                                {
                                    PassengerCD = PassengerCD + tbFirstName.Text.Substring(0, 2);
                                }
                            }
                        }
                        if (Company.PassengerOMiddle == Count)
                        {
                            if (tbMiddleName.Text.Length > 0)
                            {
                                if (Company.PassengerOLastMiddle != null)
                                {
                                    if (Company.PassengerOLastMiddle.Value > 0)
                                    {
                                        PassengerCD = PassengerCD + tbMiddleName.Text.Substring(0, Company.PassengerOLastMiddle.Value);
                                    }
                                    else if ((Company.PassengerOLastFirst.Value == 0) && (Company.PassengerOLastMiddle.Value == 0) && (Company.PassengerOLastLast.Value == 0))
                                    {
                                        PassengerCD = PassengerCD + tbMiddleName.Text.Substring(0, 1);
                                    }
                                }
                                else
                                {
                                    PassengerCD = PassengerCD + tbMiddleName.Text.Substring(0, 1);
                                }
                            }
                        }
                        if (Company.PassengerOLast == Count)
                        {
                            if (tbLastName.Text.Length > 0)
                            {
                                if (Company.PassengerOLastLast != null)
                                {
                                    if (Company.PassengerOLastLast.Value > 0)
                                    {
                                        PassengerCD = PassengerCD + tbLastName.Text.Substring(0, Company.PassengerOLastLast.Value);
                                    }
                                    else if ((Company.PassengerOLastFirst.Value == 0) && (Company.PassengerOLastMiddle.Value == 0) && (Company.PassengerOLastLast.Value == 0))
                                    {
                                        PassengerCD = PassengerCD + tbLastName.Text.Substring(0, 1);
                                    }
                                }
                                else
                                {
                                    PassengerCD = PassengerCD + tbLastName.Text.Substring(0, 2);
                                }
                            }
                        }
                        Count++;
                    }
                    return PassengerCD;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GetCompanyProfileInfo()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                {
                    DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                    ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                }
                else
                {
                    DateFormat = "MM/dd/yyyy";
                    ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                }

                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objCompany = CompanyService.GetCompanyWithFilters(string.Empty, (long)UserPrincipal.Identity._homeBaseId, true,true).EntityList;
                    if (objCompany.Count != 0)
                    {
                        CompanySett.PassengerOFullName = objCompany[0].PassengerOFullName;
                        CompanySett.PassengerOMiddle = objCompany[0].PassengerOMiddle;
                        CompanySett.PassengerOLast = objCompany[0].PassengerOLast;
                        CompanySett.PassengerOLastFirst = objCompany[0].PassengerOLastFirst;
                        CompanySett.PassengerOLastMiddle = objCompany[0].PassengerOLastMiddle;
                        CompanySett.PassengerOLastLast = objCompany[0].PassengerOLastLast;
                        CompanySett.IsEnableTSAPX = objCompany[0].IsEnableTSAPX;
                    }
                }
            }
        }
        private void GetDefaultFlightPurpose()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objFlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    List<GetAllFlightPurposes> lstGetAllFlightPurposes = new List<GetAllFlightPurposes>();
                    lstGetAllFlightPurposes = objFlightPurposeService.GetAllFlightPurposeList().EntityList.Where(x => x.IsDefaultPurpose == true).ToList();
                    if (lstGetAllFlightPurposes.Count != 0)
                    {
                        lstGetAllFlightPurposes = lstGetAllFlightPurposes.OrderBy(x => x.FlightPurposeCD).ToList();
                        if (lstGetAllFlightPurposes.Count != 0)
                        {
                            tbFlightPurpose.Text = lstGetAllFlightPurposes[0].FlightPurposeCD;
                        }
                        else
                        {
                            tbFlightPurpose.Text = string.Empty;
                        }
                    }
                }
            }
        }
        #region "Passenger Grid"
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        protected void dgPassenger_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Data into Grid
        /// </summary>
        protected void dgPassenger_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetAllPassengerWithFilters> PassengerList = new List<GetAllPassengerWithFilters>();
                        //if (Session["PassengerList"] != null)
                        //{
                        //    PassengerList = (List<GetAllPassenger>)Session["PassengerList"];
                        //}
                        //else
                        //{
                        using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (Paxid != 0)
                            {
                                var PassengerInfo = PassengerService.GetAllPassengerWithFilters(0,0,Paxid,string.Empty,false,false,string.Empty,0);
                                if (PassengerInfo.ReturnFlag == true)
                                {
                                    PassengerList = PassengerInfo.EntityList;
                                    //PassengerInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetAllPassenger>();
                                }
                                dgPassenger.DataSource = PassengerList;
                                Session["PassengerList"] = PassengerList;
                                if ((chkSearchActiveOnly.Checked == true) || (chkSearchRequestorOnly.Checked == true) || (chkSearchHomebaseOnly.Checked == true) || (tbSearchClientCode.Text != string.Empty))
                                {
                                    FilterAndSearch(false);
                                }
                            }
                        }
                        //}                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Data into Grid
        /// </summary>
        protected void dgPassenger_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                    Session["IsEditLockPassengerRequestor"] = "True";
                                    if (!returnValue.ReturnFlag)
                                    {
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                        return;
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false);
                                    ddlImg.Enabled = true;
                                    if (imgFile.ImageUrl.Trim() != "")
                                    {
                                        btndeleteImage.Enabled = true;
                                    }
                                    tbImgName.Enabled = true;
                                    fileUL.Enabled = false;
                                    tbFirstName.Focus();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgPassenger.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                EnableForm(true);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = false;
                                fileUL.Enabled = false;
                                tbFirstName.Focus();
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Update Command for Passenger Grid
        /// </summary>
        protected void dgPassenger_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((CheckCountryExist(tbNationality, cvNationality)) && (IsUpdate))
                        {
                            cvNationality.IsValid = false;
                            tbNationality.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCountryExist(tbCityOfResi, cvCityOfResi)) && (IsUpdate))
                        {
                            cvCityOfResi.IsValid = false;
                            tbCityOfResi.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckClientCodeExist(tbClientCde, cvClientCde)) && (IsUpdate))
                        {
                            cvClientCde.IsValid = false;
                            tbClientCde.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckhomebaseExist(tbHomeBase)) && (IsUpdate))
                        {
                            cvHomeBase.IsValid = false;
                            tbHomeBase.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckFlightPurposeExist(tbFlightPurpose)) && (IsUpdate))
                        {
                            cvFlightPurpose.IsValid = false;
                            tbFlightPurpose.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckDepartmentExist(tbDepartment)) && (IsUpdate))
                        {
                            cvDepartment.IsValid = false;
                            tbDepartment.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAuthorizationExist(tbAuth)) && (IsUpdate))
                        {
                            cvAuth.IsValid = false;
                            tbAuth.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAccountNumberExist(tbAccountNumber)) && (IsUpdate))
                        {
                            cvAccountNumber.IsValid = false;
                            tbAccountNumber.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAssociatedExist(tbAssociated)) && (IsUpdate))
                        {
                            cvAssociated.IsValid = false;
                            tbAssociated.Focus();
                            IsValidateCustom = false;
                        }
                        if (!ValidateVisa())
                        {
                            IsValidateCustom = false;
                        }
                        if (!ValidatePassport())
                        {
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient())
                            {
                                // Updating Passenger - AddlInfo, Visa, Passport
                                FlightPakMasterService.Passenger oPassenger = new FlightPakMasterService.Passenger();
                                oPassenger = GetItems(oPassenger);
                                SavePassengerAddlInfo(oPassenger);
                                SaveCrewVisa(oPassenger);
                                SavePassengerPassport(oPassenger);
                                PassengerService.UpdatePassengerRequestor(oPassenger);
                                UpdateFutureTripsheet(oPassenger);
                                #region Image Upload in Edit Mode
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    foreach (var DicItem in dicImg)
                                    {
                                        //objService = new FlightPakMasterService.MasterCatalogServiceClient();
                                        FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                        Service.FileWarehouseID = 0;
                                        Service.RecordType = "Passenger";
                                        Service.UWAFileName = DicItem.Key;
                                        Service.RecordID = Convert.ToInt64(oPassenger.PassengerRequestorID);
                                        Service.UWAWebpageName = "PassengerCatalog.aspx";
                                        Service.UWAFilePath = DicItem.Value;
                                        Service.IsDeleted = false;
                                        Service.FileWarehouseID = 0;
                                        objService.AddFWHType(Service);
                                    }
                                }
                                #endregion
                                #region Image Upload in Delete
                                using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                    foreach (var DicItem in dicImgDelete)
                                    {
                                        objFWHType.UWAFileName = DicItem.Key;
                                        objFWHType.RecordID = Convert.ToInt64(oPassenger.PassengerRequestorID);
                                        objFWHType.RecordType = "Passenger";
                                        objFWHType.IsDeleted = true;
                                        objFWHType.LastUpdTS = System.DateTime.UtcNow;
                                        objFWHType.LastUpdUID = "UC";
                                        objFWHType.IsDeleted = true;
                                        objFWHTypeService.DeleteFWHType(objFWHType);
                                    }
                                }
                                #endregion
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                    Session["IsEditLockPassengerRequestor"] = "False";
                                }
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            GridEnable(true, true, true);
                            dgPassenger.Rebind();
                            SelectItem();
                            ReadOnlyForm();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Update Command for Passenger Grid
        /// </summary>
        protected void dgPassenger_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if ((CheckCountryExist(tbNationality, cvNationality)) && (IsUpdate))
                        {
                            cvNationality.IsValid = false;
                            tbNationality.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckCountryExist(tbCityOfResi, cvCityOfResi)) && (IsUpdate))
                        {
                            cvCityOfResi.IsValid = false;
                            tbCityOfResi.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckClientCodeExist(tbClientCde, cvClientCde)) && (IsUpdate))
                        {
                            cvClientCde.IsValid = false;
                            tbClientCde.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckhomebaseExist(tbHomeBase)) && (IsUpdate))
                        {
                            cvHomeBase.IsValid = false;
                            tbHomeBase.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckFlightPurposeExist(tbFlightPurpose)) && (IsUpdate))
                        {
                            cvFlightPurpose.IsValid = false;
                            tbFlightPurpose.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckDepartmentExist(tbDepartment)) && (IsUpdate))
                        {
                            cvDepartment.IsValid = false;
                            tbDepartment.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAuthorizationExist(tbAuth)) && (IsUpdate))
                        {
                            cvAuth.IsValid = false;
                            tbAuth.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAccountNumberExist(tbAccountNumber)) && (IsUpdate))
                        {
                            cvAccountNumber.IsValid = false;
                            tbAccountNumber.Focus();
                            IsValidateCustom = false;
                        }
                        if ((CheckAssociatedExist(tbAssociated)) && (IsUpdate))
                        {
                            cvAssociated.IsValid = false;
                            tbAssociated.Focus();
                            IsValidateCustom = false;
                        }
                        if (!ValidateVisa())
                        {
                            IsValidateCustom = false;
                        }
                        if (!ValidatePassport())
                        {
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient())
                            {
                                // Updating Passenger - AddlInfo, Visa, Passport
                                FlightPakMasterService.Passenger oPassenger = new FlightPakMasterService.Passenger();
                                oPassenger = GetItems(oPassenger);
                                SavePassengerAddlInfo(oPassenger);
                                SaveCrewVisa(oPassenger);
                                SavePassengerPassport(oPassenger);
                                PassengerService.AddPassengerRequestor(oPassenger);
                                #region Image Upload in Edit Mode
                                var PassengerRequestorList = PassengerService.GetPassengerRequestorList().EntityList.Where(x => x.FirstName == oPassenger.FirstName
                                                                                                                        && x.LastName == oPassenger.LastName
                                                                                                                        && x.MiddleInitial == oPassenger.MiddleInitial).ToList();
                                if (PassengerRequestorList != null)
                                {
                                    oPassenger.PassengerRequestorID = PassengerRequestorList[0].PassengerRequestorID;
                                }
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    foreach (var DicItem in dicImg)
                                    {
                                        //objService = new FlightPakMasterService.MasterCatalogServiceClient();
                                        FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                        Service.FileWarehouseID = 0;
                                        Service.RecordType = "Passenger";
                                        Service.UWAFileName = DicItem.Key;
                                        Service.RecordID = Convert.ToInt64(oPassenger.PassengerRequestorID);
                                        Service.UWAWebpageName = "PassengerCatalog.aspx";
                                        Service.UWAFilePath = DicItem.Value;
                                        Service.IsDeleted = false;
                                        Service.FileWarehouseID = 0;
                                        objService.AddFWHType(Service);
                                    }
                                }
                                #endregion
                            }
                            GridEnable(true, true, true);
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
                finally
                {
                    dgPassenger.Rebind();
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        protected void dgPassenger_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            if (Session["PassengerRequestorID"] != null)
                            {
                                MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient();
                                Passenger oPassenger = new Passenger();
                                GridDataItem item = dgPassenger.SelectedItems[0] as GridDataItem;
                                oPassenger.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString());
                                oPassenger.PassengerRequestorCD = item["PassengerRequestorCD"].Text;
                                if (item.GetDataKeyValue("IsActive") != null)
                                {
                                    if (Convert.ToBoolean(item.GetDataKeyValue("IsActive")) == true)
                                    {
                                        string alertMsg = "radalert('Active passenger cannot be deleted.', 360, 50, 'Passenger/ Requestor');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        return;
                                    }
                                }
                                oPassenger.LastUpdTS = System.DateTime.UtcNow;
                                oPassenger.IsDeleted = true;
                                var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                    return;
                                }
                                PassengerService.DeletePassengerRequestor(oPassenger);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(false);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                    }
                    finally
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString()));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        protected void dgPassenger_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((btnSaveChanges.Visible == false) && (btnSaveChangesTop.Visible == false))
                        {
                            GridDataItem item = dgPassenger.SelectedItems[0] as GridDataItem;
                            Session["PassengerRequestorID"] = item["PassengerRequestorID"].Text;
                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        protected void dgPassenger_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem dataItem = e.Item as GridDataItem;
                            GridColumn column = dgPassenger.MasterTableView.GetColumn("PassengerRequestorCD");
                            string PassengerRequestorCDValue = dataItem["PassengerRequestorCD"].Text;
                            if (PassengerRequestorCDValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter colConvert = new ColorConverter();
                                GridDataItem item = (GridDataItem)e.Item;
                                TableCell cell = (TableCell)item["PassengerRequestorCD"];
                                cell.ForeColor = (System.Drawing.Color)colConvert.ConvertFromString(color);
                                cell.Font.Bold = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPassenger_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PassengerRequestorPageNavigated = true;
                        dgPassenger.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPassenger_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (PassengerRequestorPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        #endregion
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPassenger;
                        }
                        if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPassenger;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void btnSaveChangesTop_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgPassenger.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            dgPassenger.Rebind();
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Cancel Airport Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["PassengerRequestorID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim()));
                            }
                        }
                        Session.Remove("PassengerRequestorID");
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void lbtnTravelSense_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Passenger = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var PassengerInfo = Passenger.GetAllPassengerWithFilters(0,0,0,string.Empty,false,false,string.Empty,0);
                            string Quotes = "\"";
                            string Comma = ",";
                            System.Text.StringBuilder sbPassenger = new System.Text.StringBuilder();
                            if (PassengerInfo.ReturnFlag == true)
                            {
                                foreach (GetAllPassengerWithFilters PassengerList in PassengerInfo.EntityList)
                                {
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.SSN);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.LastName);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.FirstName);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    if (!String.IsNullOrEmpty(PassengerList.Title))
                                    {
                                        sbPassenger.Append(PassengerList.Title);
                                    }
                                    else
                                    {
                                        sbPassenger.Append("PASSENGER");
                                    }
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.PassengerDescription);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(Comma);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.Append(PassengerList.SalaryLevel);
                                    sbPassenger.Append(Quotes);
                                    sbPassenger.AppendLine();
                                }
                            }
                            string FilePath = Server.MapPath("~/" + "T$Pass.txt");
                            System.IO.File.WriteAllText(FilePath, sbPassenger.ToString(), System.Text.Encoding.ASCII);
                            Response.AppendHeader("content-disposition", "attachment; filename=" + "T$Pass.txt");
                            Response.ContentType = "text/plain";
                            Response.WriteFile(FilePath);
                            Response.End();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Method to validate country code in the Passport grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void RadDatePicker1_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DateTime dt = new DateTime();
                        dt = DateTime.Now;
                        DateTime? bdate = new DateTime();
                        bdate = RadDatePicker1.SelectedDate;
                        TimeSpan age = new TimeSpan();
                        age = dt.Subtract(bdate.Value);
                        int years = (int)age.TotalDays / 365;
                        int months = ((int)age.TotalDays % 365) / 30;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// To validate the whether the date is valid or not
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public bool IsValidDate(string datePart, string monthPart, string yearPart)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(datePart, monthPart, yearPart))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    DateTime date;
                    string strDate = string.Format("{0}-{1}-{2}", datePart, monthPart, yearPart, CultureInfo.CurrentCulture);
                    if (DateTime.TryParseExact(strDate, "dd-MM-yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None, out date))
                    {
                        return true;
                    }
                    else
                    {
                        if (strDate == "00-00-0000")
                        {
                            return false;
                            hdnDate.Value = string.Empty;
                        }
                        else
                        {
                            hdnDate.Value = "Error";
                            return false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "Validations"
        protected void btnshowdupes_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool checkUWARecord = true;
                        string CustomerID = "";
                        foreach (GridDataItem Item in dgPassenger.MasterTableView.Items)
                        {
                            if (Item["PassengerRequestorID"].Text.Trim() == Session["PassengerRequestorID"].ToString().Trim())
                            {
                                CustomerID = Item["CustomerID"].Text.Trim();
                                break;
                            }
                        }
                        string custnum = CustomerID;
                        if (tbCode.Text != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objPassenger = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objCustVal = objPassenger.GetAllPassengerWithFilters(0,0,0,tbCode.Text,false,false,string.Empty,0).EntityList;
                                if (objCustVal.Count() > 0 && objCustVal != null)
                                {
                                    cvCode.IsValid = false;
                                    checkUWARecord = false;
                                }
                                if (checkUWARecord)
                                {
                                    var objUWAVal = objPassenger.GetAllPassengerWithFilters(0, 0, 0, tbCode.Text, false, false, string.Empty, 0).EntityList;
                                    if (objUWAVal.Count() > 0 && objUWAVal != null)
                                    {
                                        RadWindowManager1.RadConfirm("Passenger Code already exist. Please create code unique.", "confirmCallBackFn", 500, 30, null, ModuleNameConstants.Preflight.PreflightPAX);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// To check unique Passenger Code on Tab Out
        /// </summary>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool checkUWARecord = true;
                        string CustomerID = "";
                        foreach (GridDataItem Item in dgPassenger.MasterTableView.Items)
                        {
                            if (Item["PassengerRequestorID"].Text.Trim() == Session["PassengerRequestorID"].ToString().Trim())
                            {
                                CustomerID = Item["CustomerID"].Text.Trim();
                                break;
                            }
                        }
                        string custnum = CustomerID;
                        if (tbCode.Text != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objPassenger = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objCustVal = objPassenger.GetAllPassengerWithFilters(0,0,0,tbCode.Text.ToString(),false,false,string.Empty,0).EntityList;//.GetPassengerRequestorList().EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Equals(tbCode.Text.ToString().ToUpper().Trim()) && x.CustomerID.ToString().ToUpper().Equals(custnum.ToString().ToUpper()));
                                if (objCustVal.Count() > 0 && objCustVal != null)
                                {
                                    cvCode.IsValid = false;
                                    checkUWARecord = false;
                                }
                                if (checkUWARecord)
                                {
                                    var objUWAVal = objPassenger.GetAllPassengerWithFilters(0,0,0,tbCode.Text.ToString(),false,false,string.Empty,0).EntityList; //.GetPassengerRequestorList().EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                                    if (objUWAVal.Count() > 0 && objUWAVal != null)
                                    {
                                        RadWindowManager1.RadConfirm("Passenger Code already exist. Please create code unique.", "confirmCallBackFn", 500, 30, null, ModuleNameConstants.Preflight.PreflightPAX);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbNationality_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist(tbNationality, cvNationality);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbCityOfResi_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist(tbCityOfResi, cvCityOfResi);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbClientCde_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientCodeExist(tbClientCde, cvClientCde);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbHomeBase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckhomebaseExist(tbHomeBase);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbFlightPurpose_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFlightPurposeExist(tbFlightPurpose);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbDepartment_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckDepartmentExist(tbDepartment);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbAuth_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAuthorizationExist(tbAuth);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbAccountNumber_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAccountNumberExist(tbAccountNumber);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void tbAssociated_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAssociatedExist(tbAssociated);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        private bool CheckCountryExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx, cval))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetCountryWithFilters(txtbx.Text.Trim(),0).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                CountryList = (List<FlightPakMasterService.Country>)objRetVal.ToList();
                                if (txtbx.ID.Equals("tbNationality"))
                                {
                                    hdnNationality.Value = CountryList[0].CountryID.ToString();
                                    tbNationalityName.Text = System.Web.HttpUtility.HtmlEncode(CountryList[0].CountryName.ToString());
                                }
                                else if (txtbx.ID.Equals("tbCityOfResi"))
                                {
                                    hdnCityOfResi.Value = CountryList[0].CountryID.ToString();
                                    tbCityOfResiName.Text = System.Web.HttpUtility.HtmlEncode(CountryList[0].CountryName.ToString());
                                }
                                returnVal = false;
                            }
                            else
                            {
                                if (txtbx.ID.Equals("tbNationality"))
                                {
                                    tbNationalityName.Text = string.Empty;
                                }
                                else if (txtbx.ID.Equals("tbCityOfResi"))
                                {
                                    tbCityOfResiName.Text = string.Empty;
                                }
                                cval.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckClientCodeExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetClientWithFilters(0, txtbx.Text.Trim(),false).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                                ClientList = (List<FlightPakMasterService.Client>)objRetVal.ToList();
                                if (txtbx.ID.Equals("tbClientCde"))
                                {
                                    txtbx.Text = ClientList[0].ClientCD;
                                    hdnClientCde.Value = ClientList[0].ClientID.ToString();
                                }
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckhomebaseExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetAirportForGridWithFilters(0,txtbx.Text.Trim(),0,false).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAirportForGridWithFilters> AirportList = new List<FlightPakMasterService.GetAirportForGridWithFilters>();
                                AirportList = (List<FlightPakMasterService.GetAirportForGridWithFilters>)objRetVal.ToList();
                                txtbx.Text = AirportList[0].IcaoID.ToUpper();

                                hdnHomeBase.Value = AirportList[0].AirportID.ToString();
                                returnVal = false;
                            }
                            else
                            {
                                cvHomeBase.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckFlightPurposeExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetAllFlightPurposesWithFilters(0, 0, txtbx.Text, false, false, false).EntityList;
                                //.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                //List<FlightPakMasterService.FlightPurpose> FlightPurposeList = new List<FlightPakMasterService.FlightPurpose>();
                                //FlightPurposeList = objRetVal.ToList();
                                hdnFlightPurpose.Value = objRetVal[0].FlightPurposeID.ToString(); // FlightPurposeList[0].FlightPurposeID.ToString();
                                txtbx.Text = objRetVal[0].FlightPurposeCD;
                                returnVal = false;
                            }
                            else
                            {
                                cvFlightPurpose.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckDepartmentExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetDepartmentByWithFilters(txtbx.Text,0,0,false).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetDepartmentByWithFilters> DepartmentList = new List<FlightPakMasterService.GetDepartmentByWithFilters>();
                                DepartmentList = (List<FlightPakMasterService.GetDepartmentByWithFilters>)objRetVal.ToList();
                                hdnDepartment.Value = DepartmentList[0].DepartmentID.ToString();
                                txtbx.Text = DepartmentList[0].DepartmentCD;
                                returnVal = false;
                            }
                            else
                            {
                                cvDepartment.IsValid = false;
                                tbAuth.Text = string.Empty;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckAuthorizationExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = true;
                    
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckAccountNumberExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetAccountList().EntityList.Where(x => x.AccountNum.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Account> AccountList = new List<FlightPakMasterService.Account>();
                                AccountList = (List<FlightPakMasterService.Account>)objRetVal.ToList();
                                hdnAccountNumber.Value = AccountList[0].AccountID.ToString();
                                txtbx.Text = AccountList[0].AccountNum;
                                returnVal = false;
                            }
                            else
                            {
                                cvAccountNumber.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckAssociatedExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetPassengerRequestorList().EntityList.Where(x => x.PassengerRequestorCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Passenger> PassengerList = new List<FlightPakMasterService.Passenger>();
                                PassengerList = (List<FlightPakMasterService.Passenger>)objRetVal.ToList();
                                hdnAssociated.Value = PassengerList[0].PassengerRequestorID.ToString();
                                txtbx.Text = PassengerList[0].PassengerRequestorCD;
                                returnVal = false;
                            }
                            else
                            {
                                cvAssociated.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to check custom Validator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckCustomValidator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((CheckCountryExist(tbNationality, cvNationality)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbNationality.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckCountryExist(tbCityOfResi, cvCityOfResi)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbCityOfResi.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckClientCodeExist(tbClientCde, cvClientCde)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbClientCde.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckhomebaseExist(tbHomeBase)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbHomeBase.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckFlightPurposeExist(tbFlightPurpose)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbFlightPurpose.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckDepartmentExist(tbDepartment)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbDepartment.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckAuthorizationExist(tbAuth)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbAuth.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckAccountNumberExist(tbAccountNumber)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbAccountNumber.Focus();
                        IsValidateCustom = false;
                    }
                    if ((CheckAssociatedExist(tbAccountNumber)) && (IsUpdate))
                    {
                        //cvICAO.IsValid = false;
                        tbAccountNumber.Focus();
                        IsValidateCustom = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Passenger Additional Information"
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Additional Info
                                // Declaring Lists
                                List<FlightPakMasterService.PassengerAdditionalInfo> finalList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                                List<FlightPakMasterService.PassengerAdditionalInfo> retainList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                                List<FlightPakMasterService.PassengerAdditionalInfo> newList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                                // Retain Grid Items and bind into List and add into Final List
                                retainList = RetainPassengerAddlInfoGrid(dgPassengerAddlInfo);
                                finalList.AddRange(retainList);
                                // Bind Selected New Item and add into Final List
                                if (Session["PassengerAdditionalInfoNew"] != null)
                                {
                                    newList = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfoNew"];
                                    finalList.AddRange(newList);
                                }
                                // Bind final list into Session
                                Session["PassengerAdditionalInfo"] = finalList;
                                // Bind final list into Grid
                                dgPassengerAddlInfo.DataSource = finalList.Where(x => x.IsDeleted == false).ToList();
                                dgPassengerAddlInfo.DataBind();
                                // Empty Newly added Session item
                                Session.Remove("PassengerAdditionalInfoNew");
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Delete Selected Crew Roster Additional Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteInfo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPassengerAddlInfo.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgPassengerAddlInfo.SelectedItems[0];
                            Int64 AdditionalINFOID = Convert.ToInt64(item.GetDataKeyValue("PassengerAdditionalInfoID").ToString());
                            string AdditionalINFOCD = item.GetDataKeyValue("AdditionalINFOCD").ToString();
                            List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAddlInfo = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                            PassengerAddlInfo = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                            for (int Index = 0; Index < PassengerAddlInfo.Count; Index++)
                            {
                                if (PassengerAddlInfo[Index].AdditionalINFOCD == AdditionalINFOCD)
                                {
                                    PassengerAddlInfo[Index].IsDeleted = true;
                                }
                            }
                            Session["PassengerAdditionalInfo"] = PassengerAddlInfo;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                            PassengerAdditionalInfoList = RetainPassengerAddlInfoGrid(dgPassengerAddlInfo);
                            dgPassengerAddlInfo.DataSource = PassengerAdditionalInfoList.Where(x => x.IsDeleted == false).ToList();
                            dgPassengerAddlInfo.DataBind();
                            Session["PassengerAdditionalInfo"] = PassengerAddlInfo;
                        }
                        else
                        {
                            string alertMsg = "radalert('Please select the record from Additional Info table', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Optimized on 5/9/12 by Manish Varma
        /// Method to Bind Additional Information Grid, based on Crew Code
        /// </summary>
        /// <param name="crewCode">Pass Crew Code</param>
        private void BindAdditionalInfoGrid(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                    {
                        var PassengerAddlInfo = Service.GetPassengerAddlInfoListReqID(PassengerRequestorID);
                        if (PassengerAddlInfo.ReturnFlag == true)
                        {
                            dgPassengerAddlInfo.DataSource = PassengerAddlInfo.EntityList.ToList();
                            dgPassengerAddlInfo.DataBind();
                            Session["PassengerAdditionalInfo"] = PassengerAddlInfo.EntityList.ToList();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Retain Additional Info Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Definition List</returns>
        private List<FlightPakMasterService.PassengerAdditionalInfo> RetainPassengerAddlInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.PassengerAdditionalInfo>>(() =>
                {
                    List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAddlInfo = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    PassengerAddlInfo = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                    List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    for (int Index = 0; Index < PassengerAddlInfo.Count; Index++)
                    {
                        foreach (GridDataItem item in grid.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("AdditionalINFOCD").ToString() == PassengerAddlInfo[Index].AdditionalINFOCD)
                            {
                                PassengerAddlInfo[Index].AdditionalINFOValue = ((TextBox)item["AdditionalINFOValue"].FindControl("tbAddlInfo")).Text;
                                break;
                            }
                        }
                    }
                    return PassengerAddlInfo;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Save Crew Definiton Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private FlightPakMasterService.Passenger SavePassengerAddlInfo(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    // added information items from the Grid to List
                    oPassenger.PassengerAdditionalInfo = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.PassengerAdditionalInfo> PassengerAdditionalInfoList = new List<FlightPakMasterService.PassengerAdditionalInfo>();
                    FlightPakMasterService.PassengerAdditionalInfo PassengerAddlInfoDef = new FlightPakMasterService.PassengerAdditionalInfo();
                    PassengerAdditionalInfoList = (List<FlightPakMasterService.PassengerAdditionalInfo>)Session["PassengerAdditionalInfo"];
                    for (int Index = 0; Index < PassengerAdditionalInfoList.Count; Index++)
                    {
                        foreach (GridDataItem Item in dgPassengerAddlInfo.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AdditionalINFOCD").ToString() == PassengerAdditionalInfoList[Index].AdditionalINFOCD.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString()) != 0)
                                {
                                    PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID = Convert.ToInt64(Item.GetDataKeyValue("PassengerAdditionalInfoID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID = Indentity;
                                }
                                PassengerAdditionalInfoList[Index].PassengerRequestorID = oPassenger.PassengerRequestorID;
                                PassengerAdditionalInfoList[Index].AdditionalINFODescription = Item.GetDataKeyValue("AdditionalINFODescription").ToString();
                                PassengerAdditionalInfoList[Index].AdditionalINFOValue = ((TextBox)Item["AdditionalINFOValue"].FindControl("tbAddlInfo")).Text;
                                PassengerAdditionalInfoList[Index].IsDeleted = false;
                                PassengerAdditionalInfoList[Index].CustomerID = oPassenger.CustomerID;
                                break;
                            }
                        }
                        if (PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID == 0)
                        {
                            Indentity = Indentity - 1;
                            PassengerAdditionalInfoList[Index].PassengerAdditionalInfoID = Indentity;
                        }
                    }
                    oPassenger.PassengerAdditionalInfo = PassengerAdditionalInfoList;
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Passenger Visa"
        /// <summary>
        /// Method to Add New Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ValidateVisa())
                        {
                            // Declaring Lists
                            List<GetAllCrewPaxVisa> FinalList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> RetainList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> NewList = new List<GetAllCrewPaxVisa>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainVisaGrid(dgVisa);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List
                            GetAllCrewPaxVisa CrewPaxVisa = new GetAllCrewPaxVisa();
                            CrewPaxVisa.VisaID = 0;
                            CrewPaxVisa.CrewID = null;
                            if (Session["PassengerRequestorID"] != null)
                            {
                                CrewPaxVisa.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                            }
                            else
                            {
                                CrewPaxVisa.PassengerRequestorID = 0;
                            }
                            NewList.Add(CrewPaxVisa);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["PassengerVisa"] = FinalList;
                            dgVisa.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Method to Delete Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgVisa.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgVisa.SelectedItems[0];
                            string VisaNumber = ((TextBox)(Item["VisaNum"].FindControl("tbVisaNum"))).Text;
                            List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                            CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                            for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                            {
                                if (CrewPaxVisaList[Index].VisaNum == VisaNumber)
                                {
                                    CrewPaxVisaList[Index].IsDeleted = true;
                                }
                            }
                            Session["PassengerVisa"] = CrewPaxVisaList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<GetAllCrewPaxVisa> CrewPaxList = new List<GetAllCrewPaxVisa>();
                            CrewPaxList = RetainVisaGrid(dgVisa);
                            for (int Index = 0; Index < CrewPaxList.Count; Index++)
                            {
                                if (CrewPaxList[Index].VisaNum == VisaNumber)
                                {
                                    CrewPaxList[Index].IsDeleted = true;
                                }
                            }
                            //CrewPaxList.RemoveAll(x => x.VisaNum.ToString().Trim().ToUpper() == VisaNumber.Trim().ToUpper());
                            dgVisa.DataSource = CrewPaxList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.DataBind();
                            Session["PassengerVisa"] = CrewPaxVisaList;
                        }
                        else
                        {
                            string alertMsg = "radalert('Please select the record from Visa table', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Crew Additional Information into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Visa_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //BindVisaGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Crew Visa based on Crew Code
        /// </summary>
        /// <param name="crewCode">Pass Crew Code</param>
        private void BindVisaGrid(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (Session["PassengerVisa"] != null)
                    //{
                    //    List<GetAllCrewPaxVisa> CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    //    dgVisa.DataSource = CrewPaxVisaList;
                    //    dgVisa.DataBind();
                    //}
                    //else
                    //{
                    using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPaxVisa> CrewPassengerVisaList = new List<GetAllCrewPaxVisa>();
                        CrewPassengerVisa CrewVisa = new CrewPassengerVisa();
                        CrewVisa.CrewID = null;
                        CrewVisa.PassengerRequestorID = PassengerRequestorID;
                        var CrewPaxVisaList = Service.GetCrewVisaListInfo(CrewVisa);
                        if (CrewPaxVisaList.ReturnFlag == true)
                        {
                            CrewPassengerVisaList = CrewPaxVisaList.EntityList.Where(x => x.PassengerRequestorID == PassengerRequestorID && x.IsDeleted == false).ToList();
                            dgVisa.DataSource = CrewPassengerVisaList;
                            dgVisa.DataBind();
                            Session["PassengerVisa"] = CrewPassengerVisaList;
                        }
                    }
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Retain Crew Passenger Visa Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Pax Visa List</returns>
        private List<GetAllCrewPaxVisa> RetainVisaGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<GetAllCrewPaxVisa>>(() =>
                {
                    List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                    GetAllCrewPaxVisa CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                    //CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    //bool IsExist = false;
                    //for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                    //{
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                        //if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum) //Item.GetDataKeyValue("VisaNum")
                        //if ((Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) == CrewPaxVisaList[Index].VisaID) && (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum))
                        //if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum)
                        //{
                        CrewPaxVisaListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        CrewPaxVisaListDef.CrewID = null;
                        if (Session["PassengerRequestorID"] != null)
                        {
                            CrewPaxVisaListDef.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        }
                        else
                        {
                            CrewPaxVisaListDef.PassengerRequestorID = 0;
                        }
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                            CrewPaxVisaListDef.CountryCD = ((TextBox)Item["CountryCD"].FindControl("tbCountryCD")).Text;
                        }
                        CrewPaxVisaListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaListDef.VisaNum = string.Empty;
                        }
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.ExpiryDT = FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.IssueDate = FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }
                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaListDef.IsDeleted = false;
                        CrewPaxVisaList.Add(CrewPaxVisaListDef);
                        //IsExist = true;
                        //break;
                        //}
                    }
                    //if (IsExist == false)
                    //{
                    //}
                    //}
                    return CrewPaxVisaList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Save Crew Visa Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private FlightPakMasterService.Passenger SaveCrewVisa(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    // added information items from the Grid to List
                    oPassenger.CrewPassengerVisa = new List<FlightPakMasterService.CrewPassengerVisa>();
                    FlightPakMasterService.CrewPassengerVisa CrewPaxVisaInfoListDef = new FlightPakMasterService.CrewPassengerVisa();
                    List<FlightPakMasterService.GetAllCrewPaxVisa> CrewPaxVisaInfoList = new List<FlightPakMasterService.GetAllCrewPaxVisa>();
                    CrewPaxVisaInfoList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    List<FlightPakMasterService.CrewPassengerVisa> CrewPaxVisaGridInfo = new List<FlightPakMasterService.CrewPassengerVisa>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgVisa.MasterTableView.Items)
                    {
                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                        if (Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) != 0)
                        {
                            CrewPaxVisaInfoListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                        }
                        CrewPaxVisaInfoListDef.CrewID = null;
                        CrewPaxVisaInfoListDef.PassengerRequestorID = oPassenger.PassengerRequestorID;  //Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        CrewPaxVisaInfoListDef.CustomerID = oPassenger.CustomerID;
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                        }
                        CrewPaxVisaInfoListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaInfoListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaInfoListDef.VisaNum = string.Empty;
                        }
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.IssueDate = FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }
                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaInfoListDef.IsDeleted = false;
                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                    }
                    if (CrewPaxVisaInfoList != null)
                    {
                        for (int sIndex = 0; sIndex < CrewPaxVisaInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxVisaGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxVisaInfoList[sIndex].VisaNum == CrewPaxVisaGridInfo[gIndex].VisaNum) || (CrewPaxVisaInfoList[sIndex].VisaID == CrewPaxVisaGridInfo[gIndex].VisaID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxVisaInfoList[sIndex].VisaNum != null)
                                {
                                    if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                    {
                                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                                        CrewPaxVisaInfoListDef.CountryID = CrewPaxVisaInfoList[sIndex].CountryID;
                                        CrewPaxVisaInfoListDef.CrewID = CrewPaxVisaInfoList[sIndex].CrewID;
                                        CrewPaxVisaInfoListDef.CustomerID = CrewPaxVisaInfoList[sIndex].CustomerID;
                                        CrewPaxVisaInfoListDef.ExpiryDT = CrewPaxVisaInfoList[sIndex].ExpiryDT;
                                        CrewPaxVisaInfoListDef.IsDeleted = true;// CrewPaxVisaInfoList[sIndex].IsDeleted;
                                        CrewPaxVisaInfoListDef.IssueDate = CrewPaxVisaInfoList[sIndex].IssueDate;
                                        CrewPaxVisaInfoListDef.IssuePlace = CrewPaxVisaInfoList[sIndex].IssuePlace;
                                        CrewPaxVisaInfoListDef.LastUpdTS = CrewPaxVisaInfoList[sIndex].LastUpdTS;
                                        CrewPaxVisaInfoListDef.LastUpdUID = CrewPaxVisaInfoList[sIndex].LastUpdUID;
                                        CrewPaxVisaInfoListDef.Notes = CrewPaxVisaInfoList[sIndex].Notes;
                                        CrewPaxVisaInfoListDef.PassengerRequestorID = CrewPaxVisaInfoList[sIndex].PassengerRequestorID;
                                        if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                        {
                                            CrewPaxVisaInfoListDef.VisaID = CrewPaxVisaInfoList[sIndex].VisaID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                                        }
                                        CrewPaxVisaInfoListDef.VisaNum = CrewPaxVisaInfoList[sIndex].VisaNum;
                                        CrewPaxVisaInfoListDef.VisaTYPE = CrewPaxVisaInfoList[sIndex].VisaTYPE;
                                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oPassenger.CrewPassengerVisa = CrewPaxVisaGridInfo;
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to validate country code in the Visa grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox countryCD = new TextBox();
                        int rowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgVisa.Items.Count - 1; i += 1)
                        {
                            countryCD.Text = ((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).Text;
                            if (countryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var RetVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (countryCD.Text.Trim().ToUpper()));
                                    if (RetVal.Count() <= 0)
                                    {
                                        string alertMsg = "radalert('Invalid Country Code', 360, 50, 'Passenger / Requestor');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        ((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).Text = string.Empty;
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)RetVal.ToList();
                                        ((TextBox)dgVisa.Items[rowIndex].Cells[0].FindControl("tbCountryCD")).Text = CountryList[0].CountryCD;
                                        ((HiddenField)dgVisa.Items[rowIndex].Cells[0].FindControl("hdnCountryID")).Value = CountryList[0].CountryID.ToString();
                                    }
                                }
                            }
                            rowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Method to validate Issue Date and Expiry Date in the Visa grid
        /// </summary>
        protected bool ValidateVisa()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string VisaNum = string.Empty;
                    string CountryCode = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    foreach (GridDataItem item in dgVisa.Items)
                    {
                        VisaNum = ((TextBox)item.FindControl("tbVisaNum")).Text;
                        CountryCode = ((TextBox)item.FindControl("tbCountryCD")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDate")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbExpiryDT")).Text;
                        if (String.IsNullOrEmpty(VisaNum))
                        {
                            string alertMsg = "radalert('Visa Number should not be empty', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!string.IsNullOrEmpty(IssueDate))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = FormatDate(IssueDate, DateFormat);
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(IssueDate);
                            }
                        }
                        if (!string.IsNullOrEmpty(ExpiryDT))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = FormatDate(ExpiryDT, DateFormat);
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(ExpiryDT);
                            }
                        }
                        if (!String.IsNullOrEmpty(IssueDate))
                        {
                            if (DateStart >= DateTime.Today)
                            {
                                string alertMsg = "radalert('Visa Issue Date should not be future date.', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateStart > DateEnd)
                            {
                                string alertMsg = "radalert('Visa Issue Date should be before Expiry Date', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVisa_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("ExpiryDT") != null)
                            {
                                ((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("ExpiryDT").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("ExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDate") != null)
                            {
                                ((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("IssueDate").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDate").ToString())));
                            }
                            if (Item.GetDataKeyValue("VisaID") != null)
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("VisaID")) <= 0)
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgVisa_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgVisa.ClientSettings.Scrolling.ScrollTop = "0";
                        //dgVisa.CurrentPageIndex = e.NewPageIndex;
                        //dgVisa.Rebind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        #endregion
        #region "Passenger Passport"
        /// <summary>
        /// Method for Add New Passport Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddPassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ValidatePassport())
                        {
                            // Declaring Lists
                            List<FlightPakMasterService.GetAllCrewPassport> FinalList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            List<FlightPakMasterService.GetAllCrewPassport> RetainList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            List<FlightPakMasterService.GetAllCrewPassport> NewList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainPassengerPassportGrid(dgPassport);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List
                            FlightPakMasterService.GetAllCrewPassport PassengerPassport = new FlightPakMasterService.GetAllCrewPassport();
                            PassengerPassport.PassportID = 0;
                            PassengerPassport.CrewID = null;
                            if (Session["PassengerRequestorID"] != null)
                            {
                                PassengerPassport.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                            }
                            else
                            {
                                PassengerPassport.PassengerRequestorID = 0;
                            }
                            NewList.Add(PassengerPassport);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["PassengerPassport"] = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Method To Delete Passport Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeletePassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPassport.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgPassport.SelectedItems[0];
                            string PassportNumber = ((TextBox)(Item.FindControl("tbPassportNum"))).Text.Trim();
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> CrewPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            CrewPassportList = (List<FlightPakMasterService.GetAllCrewPassport>)Session["PassengerPassport"];
                            for (int Index = 0; Index < CrewPassportList.Count; Index++)
                            {
                                if (CrewPassportList[Index].PassportNum == PassportNumber)
                                {
                                    CrewPassportList[Index].IsDeleted = true;
                                }
                            }
                            Session["PassengerPassport"] = CrewPassportList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> PassengerPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            PassengerPassportList = RetainPassengerPassportGrid(dgPassport);
                            for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                            {
                                if (PassengerPassportList[Index].PassportNum == PassportNumber)
                                {
                                    PassengerPassportList[Index].IsDeleted = true;
                                }
                            }
                            //PassengerPassportList.RemoveAll(x => x.PassportNum.ToString().Trim().ToUpper() == PassportNumber.Trim().ToUpper());
                            dgPassport.DataSource = PassengerPassportList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.DataBind();
                            Session["PassengerPassport"] = CrewPassportList;
                        }
                        else
                        {
                            string alertMsg = "radalert('Please select the record from Passport table.', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Crew Additional Information into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Passport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //BindPassportGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Bind Passport Info into Grid based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindPassportGrid(Int64 PassengerRequestorID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (Session["PassengerPassport"] != null)
                    //{
                    //    List<FlightPakMasterService.GetAllCrewPassport> PassengerPassport = (List<FlightPakMasterService.GetAllCrewPassport>)Session["PassengerPassport"];
                    //    dgPassport.DataSource = PassengerPassport;
                    //    dgPassport.DataBind();
                    //}
                    //else
                    //{
                    using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                        CrewPassengerPassport PassengerPassport = new CrewPassengerPassport();
                        PassengerPassport.CrewID = null;
                        PassengerPassport.PassengerRequestorID = PassengerRequestorID;
                        var objPassengerPassport = Service.GetCrewPassportListInfo(PassengerPassport);
                        if (objPassengerPassport.ReturnFlag == true)
                        {
                            PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.PassengerRequestorID == PassengerRequestorID && x.IsDeleted == false).ToList();
                            dgPassport.DataSource = PassengerPassportList;
                            dgPassport.DataBind();
                            Session["PassengerPassport"] = PassengerPassportList;
                        }
                    }
                    // }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Retain Crew Passenger Passport Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        private List<FlightPakMasterService.GetAllCrewPassport> RetainPassengerPassportGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetAllCrewPassport>>(() =>
                {
                    List<GetAllCrewPassport> CrewPassportList = new List<GetAllCrewPassport>();
                    GetAllCrewPassport CrewPassportListDef = new GetAllCrewPassport();
                    //CrewPassportList = (List<GetAllCrewPassport>)Session["PassengerPassport"];
                    //for (int Index = 0; Index < CrewPassportList.Count; Index++)
                    //{
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        CrewPassportListDef = new GetAllCrewPassport();
                        //if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text == CrewPassportList[Index].PassportNum)
                        //{
                        CrewPassportListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        if (Session["PassengerRequestorID"] != null)
                        {
                            CrewPassportListDef.PassengerRequestorID = Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        }
                        else
                        {
                            CrewPassportListDef.PassengerRequestorID = 0;
                        }
                        CrewPassportListDef.CrewID = null;
                        if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                        {
                            CrewPassportListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        else
                        {
                            CrewPassportListDef.PassportNum = string.Empty;
                        }
                        CrewPassportListDef.Choice = ((CheckBox)Item["IsChoice"].FindControl("chkChoice")).Checked;
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassportListDef.PassportExpiryDT = FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPassportListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                        {
                            CrewPassportListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassportListDef.IssueDT = FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPassportListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPassportListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value);
                            CrewPassportListDef.CountryCD = ((TextBox)Item["CountryCD"].FindControl("tbPassportCountry")).Text;
                        }
                        CrewPassportListDef.IsDeleted = false;
                        CrewPassportList.Add(CrewPassportListDef);
                        //}
                    }
                    //}
                    return CrewPassportList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Save Crew Passport Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private FlightPakMasterService.Passenger SavePassengerPassport(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {

                    // added information items from the Grid to List
                    oPassenger.CrewPassengerPassport = new List<FlightPakMasterService.CrewPassengerPassport>();
                    FlightPakMasterService.CrewPassengerPassport CrewPaxPassportInfoListDef = new FlightPakMasterService.CrewPassengerPassport();
                    List<FlightPakMasterService.GetAllCrewPassport> CrewPaxPassportInfoList = new List<FlightPakMasterService.GetAllCrewPassport>();
                    CrewPaxPassportInfoList = (List<GetAllCrewPassport>)Session["PassengerPassport"];
                    List<FlightPakMasterService.CrewPassengerPassport> CrewPaxPassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
                    {
                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                        if (Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString()) != 0)
                        {
                            CrewPaxPassportInfoListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                        }
                        CrewPaxPassportInfoListDef.PassengerRequestorID = oPassenger.PassengerRequestorID;  //Convert.ToInt64(Session["PassengerRequestorID"].ToString().Trim());
                        CrewPaxPassportInfoListDef.CustomerID = oPassenger.CustomerID;
                        CrewPaxPassportInfoListDef.CrewID = null;
                        if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        else
                        {
                            CrewPaxPassportInfoListDef.PassportNum = string.Empty;
                        }
                        CrewPaxPassportInfoListDef.Choice = ((CheckBox)Item["IsChoice"].FindControl("chkChoice")).Checked;
                        if (((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }
                        if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.IssueDT = FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnPassportCountry")).Value);
                        }
                        CrewPaxPassportInfoListDef.IsDeleted = false;
                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                    }
                    if (CrewPaxPassportInfoList != null)
                    {
                        for (int sIndex = 0; sIndex < CrewPaxPassportInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxPassportGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxPassportInfoList[sIndex].PassportNum == CrewPaxPassportGridInfo[gIndex].PassportNum) || (CrewPaxPassportInfoList[sIndex].PassportID == CrewPaxPassportGridInfo[gIndex].PassportID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxPassportInfoList[sIndex].PassportNum != null)
                                {
                                    if (CrewPaxPassportInfoList[sIndex].PassportID != 0)
                                    {
                                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.Choice = CrewPaxPassportInfoList[sIndex].Choice;
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.CrewID = CrewPaxPassportInfoList[sIndex].CrewID;
                                        CrewPaxPassportInfoListDef.CustomerID = CrewPaxPassportInfoList[sIndex].CustomerID;
                                        CrewPaxPassportInfoListDef.IsDefaultPassport = CrewPaxPassportInfoList[sIndex].IsDefaultPassport;
                                        CrewPaxPassportInfoListDef.IsDeleted = true;// CrewPaxPassportInfoList[sIndex].IsDeleted;
                                        CrewPaxPassportInfoListDef.IssueCity = CrewPaxPassportInfoList[sIndex].IssueCity;
                                        CrewPaxPassportInfoListDef.IssueDT = CrewPaxPassportInfoList[sIndex].IssueDT;
                                        CrewPaxPassportInfoListDef.LastUpdTS = CrewPaxPassportInfoList[sIndex].LastUpdTS;
                                        CrewPaxPassportInfoListDef.LastUpdUID = CrewPaxPassportInfoList[sIndex].LastUpdUID;
                                        CrewPaxPassportInfoListDef.PassengerRequestorID = CrewPaxPassportInfoList[sIndex].PassengerRequestorID;
                                        CrewPaxPassportInfoListDef.PassportExpiryDT = CrewPaxPassportInfoList[sIndex].PassportExpiryDT;
                                        CrewPaxPassportInfoListDef.PassportNum = CrewPaxPassportInfoList[sIndex].PassportNum;
                                        CrewPaxPassportInfoListDef.PilotLicenseNum = CrewPaxPassportInfoList[sIndex].PilotLicenseNum;
                                        if (CrewPaxPassportInfoList[sIndex].PassportID != 0)
                                        {
                                            CrewPaxPassportInfoListDef.PassportID = CrewPaxPassportInfoList[sIndex].PassportID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                                        }
                                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oPassenger.CrewPassengerPassport = CrewPaxPassportGridInfo;
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void Nation_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox countryCD = new TextBox();
                        int rowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgPassport.Items.Count - 1; i += 1)
                        {
                            countryCD.Text = ((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Text;
                            if (countryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var RetVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (countryCD.Text.Trim().ToUpper()));
                                    if (RetVal.Count() <= 0)
                                    {
                                        string alertMsg = "radalert('Invalid Country Code', 360, 50, 'Passenger / Requestor');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        ((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Text = string.Empty;
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)RetVal.ToList();
                                        ((TextBox)dgPassport.Items[rowIndex].Cells[0].FindControl("tbPassportCountry")).Text = CountryList[0].CountryCD;
                                        ((HiddenField)dgPassport.Items[rowIndex].Cells[0].FindControl("hdnPassportCountry")).Value = CountryList[0].CountryID.ToString();
                                    }
                                }
                            }
                            rowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        /// <summary>
        /// Method to validate Issue Date and Expiry Date in the Passport grid
        /// </summary>
        protected bool ValidatePassport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string PassportNum = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    string IssueCity = string.Empty;
                    foreach (GridDataItem item in dgPassport.Items)
                    {
                        PassportNum = ((TextBox)item.FindControl("tbPassportNum")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDT")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbPassportExpiryDT")).Text;
                        IssueCity = ((TextBox)item.FindControl("tbIssueCity")).Text;
                        if (String.IsNullOrEmpty(PassportNum))
                        {
                            string alertMsg = "radalert('Passport Number should not be empty', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(ExpiryDT))
                        {
                            string alertMsg = "radalert('Passport Expiry Date should not be empty', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(IssueDate))
                        {
                            string alertMsg = "radalert('Passport Issue Date should not be empty', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (String.IsNullOrEmpty(IssueCity))
                        {
                            string alertMsg = "radalert('Passport Issue City should not be empty', 360, 50, 'Passenger/ Requestor');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!(string.IsNullOrEmpty(IssueDate)))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = FormatDate(IssueDate, DateFormat);
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(IssueDate);
                            }
                        }
                        if (!(string.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = FormatDate(ExpiryDT, DateFormat);
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(ExpiryDT);
                            }
                        }
                        if (!String.IsNullOrEmpty(IssueDate))
                        {
                            if (DateStart >= DateTime.Today)
                            {
                                string alertMsg = "radalert('Passport Issue Date should not be future date.', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateStart > DateEnd)
                            {
                                string alertMsg = "radalert('Passport Issue Date should be before Expiry Date', 360, 50, 'Passenger/ Requestor');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassport_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("PassportExpiryDT") != null)
                            {
                                ((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDT") != null)
                            {
                                ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Item.GetDataKeyValue("IssueDT").ToString()));
                                //String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("PassportID") != null)
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("PassportID")) <= 0)
                                {
                                    ((TextBox)Item["PassportNUM"].FindControl("tbPassportNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["PassportNUM"].FindControl("tbPassportNum")).Enabled = false;
                                }
                            }
                            CheckBox checkColumn = Item.FindControl("chkChoice") as CheckBox;
                            checkColumn.Attributes.Add("onclick", "uncheckOther(this);");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPassport_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //List<FlightPakMasterService.GetAllCrewPassport> lstCrewPassport = (List<FlightPakMasterService.GetAllCrewPassport>)Session["PagingPassengerPassport"];
                        //dgPassport.CurrentPageIndex = e.NewPageIndex;
                        //dgPassport.DataSource = lstCrewPassport;
                        //dgPassport.Rebind();
                        dgPassport.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        private FlightPakMasterService.Passenger UpdateFutureTripsheet(FlightPakMasterService.Passenger oPassenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oPassenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Passenger>(() =>
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                    {
                        List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                        CrewPassengerPassport PassengerPassport = new CrewPassengerPassport();
                        PassengerPassport.CrewID = null;
                        PassengerPassport.PassengerRequestorID = oPassenger.PassengerRequestorID;
                        var objPassengerPassport = objService.GetCrewPassportListInfo(PassengerPassport);
                        if (objPassengerPassport.ReturnFlag == true)
                        {
                            PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.PassengerRequestorID == oPassenger.PassengerRequestorID && x.IsDeleted == false).ToList();
                        }

                        Int64 PassportID = 0;
                        for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                        {
                            PassportID = PassengerPassportList[0].PassportID;
                            if ((PassengerPassportList[Index].IsDeleted == false) && (PassengerPassportList[Index].Choice == true))
                            {
                                PassportID = PassengerPassportList[Index].PassportID;
                                break;
                            }
                        }
                        if (hdnIsPassportChoice.Value == "Yes")
                        {

                            objService.UpdatePassportForFutureTrip(PassportID, oPassenger.PassengerRequestorID, 0);
                        }
                    }
                    return oPassenger;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #endregion
        #region "Image Upload"
        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> DicImgage = new Dictionary<string, byte[]>();
                Session["DicImg"] = DicImgage;
                Dictionary<string, string> DicImageDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = DicImageDelete;
            }
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = DicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            DicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = DicImage;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            Session["DicImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            FileValidators fileValidate = new FileValidators();
                            int i = 0;
                            foreach (var DicItem in DicImage)
                            {
                                var fileName = fileValidate.CleanFileNameOnly(DicItem.Key);
                                if (!string.IsNullOrEmpty(fileName))
                                {
                                    ddlImg.Items.Insert(i, new ListItem(fileName, fileName));
                                    i = i + 1;
                                }
                            }
                            imgFile.ImageUrl = "";
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream MyStream;
                Int32 FileLen;
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        string FileName = fileUL.FileName;
                        FileLen = fileUL.PostedFile.ContentLength;
                        Byte[] Input = new Byte[FileLen];
                        MyStream = fileUL.FileContent;
                        MyStream.Read(Input, 0, FileLen);
                        imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        if (tbImgName.Text.Trim() != "")
                        {
                            FileName = tbImgName.Text.Trim();
                        }
                        int Count = DicImage.Count(D => D.Key.Equals(FileName));
                        if (Count > 0)
                        {
                            DicImage[FileName] = Input;
                        }
                        else
                        {
                            DicImage.Add(FileName, Input);
                            ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(System.Web.HttpUtility.HtmlEncode(FileName), FileName));
                        }
                        tbImgName.Text = "";
                        if (ddlImg.Items.Count != 0)
                        {
                            ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                        }
                        btndeleteImage.Enabled = true;
                    }
                }
            }
        }
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool ImgFound = false;
                            imgFile.ImageUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ReturnImg = ImgService.GetFileWarehouseList("Passenger", Convert.ToInt64(Session["PassengerRequestorID"].ToString())).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImg)
                                {
                                    ImgFound = true;
                                    byte[] Picture = FWH.UWAFilePath;
                                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Picture);
                                }
                                tbImgName.Text = ddlImg.Text.Trim();
                                if (!ImgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (Count > 0)
                                    {
                                        imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        #endregion
        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FilterAndSearch(true);
                        return false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void FilterByClient_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientCodeExist(tbSearchClientCode, cvSearchClientCode))
                        {
                            cvSearchClientCode.IsValid = false;
                        }
                        else
                        {
                            FilterAndSearch(true);
                            DefaultSelection(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        private bool FilterAndSearch(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                List<GetAllPassenger> PassengerList = new List<GetAllPassenger>();
                if (Session["PassengerList"] != null)
                {
                    PassengerList = (List<FlightPakMasterService.GetAllPassenger>)Session["PassengerList"];
                }
                if (PassengerList.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.IsActive == true).ToList<GetAllPassenger>(); }
                    if (chkSearchRequestorOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.IsRequestor == true).ToList<GetAllPassenger>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { PassengerList = PassengerList.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllPassenger>(); }
                    if (!string.IsNullOrEmpty(tbSearchClientCode.Text))
                    {
                        PassengerList = PassengerList.Where(x => x.ClientCD != null && x.ClientCD.Trim() == tbSearchClientCode.Text.Trim()).ToList<GetAllPassenger>();
                    }
                    dgPassenger.DataSource = PassengerList;
                    if (IsDataBind)
                    {
                        dgPassenger.DataBind();
                    }
                }
                return false;
            }
        }
        #endregion
        #region "Reports"
        //protected void btnShowReports_OnClick(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                Session["REPORT"] = hdnReportName.Value;
        //                Session["FORMAT"] = hdnReportFormat.Value;
        //                Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";PassengerCD=" + hdnReportParameters.Value;
        //                if (hdnReportFormat.Value == "PDF")
        //                {
        //                    Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
        //        }
        //    }
        //}
        #endregion
        #region "Date formatting"
        private DateTime FormatDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');

                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];

                if (DateFormat.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                }
                else if (DateFormat.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                }
                else if (DateFormat.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                }

                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }

                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                return new DateTime(yyyy, mm, dd);
            }
        }
        private string DisplayFormattedDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                string[] ISplitFormat = new string[3];
                string IFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString();
                string ReturnFormat = string.Empty;
                string Seperator = string.Empty;

                if (IFormat.Contains("/"))
                {
                    ISplitFormat = IFormat.Split('/');
                }
                else if (IFormat.Contains("-"))
                {
                    ISplitFormat = IFormat.Split('-');
                }
                else if (IFormat.Contains("."))
                {
                    ISplitFormat = IFormat.Split('.');
                }

                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                    Seperator = "/";
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                    Seperator = "-";
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                    Seperator = ".";
                }

                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }

                string dd = "", mm = "", yyyy = "";
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = SplitDate[Index];
                        for (int idxD = 0; idxD < ISplitFormat.Count(); idxD++)
                        {
                            if (ISplitFormat[idxD].ToString().ToLower().Contains("d"))
                            {
                                dd = SplitDate[idxD];
                                break;
                            }
                        }
                        ReturnFormat = ReturnFormat + dd.ToString() + Seperator;
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = SplitDate[Index];
                        for (int idxM = 0; idxM < ISplitFormat.Count(); idxM++)
                        {
                            if (ISplitFormat[idxM].ToString().ToLower().Contains("m"))
                            {
                                mm = SplitDate[idxM];
                                break;
                            }
                        }
                        ReturnFormat = ReturnFormat + mm.ToString() + Seperator;
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = SplitDate[Index];
                        for (int idxY = 0; idxY < ISplitFormat.Count(); idxY++)
                        {
                            if (ISplitFormat[idxY].ToString().ToLower().Contains("y"))
                            {
                                yyyy = SplitDate[idxY];
                                break;
                            }
                        }
                        ReturnFormat = ReturnFormat + yyyy.ToString() + Seperator;
                    }
                }
                ReturnFormat = ReturnFormat.Remove(ReturnFormat.LastIndexOf(Seperator));
                return ReturnFormat;
            }
        }
        //private DateTime FormatDate(string Date, string Format)
        //{
        //    string dateformat = Format;    //can be any format UTC, US, UK...
        //    DateTime date1 = DateTime.ParseExact(Date, dateformat, null);

        //    return Convert.ToDateTime(date1.ToString(dateformat, CultureInfo.InvariantCulture));
        //}
        #endregion
        protected void TaxType_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (radGuest.Checked == true)
                        {
                            tbAssociated.Enabled = true;
                            btnAssociated.Enabled = true;
                        }
                        else if (radNonControlled.Checked == true)
                        {
                            tbAssociated.Text = string.Empty;
                            tbAssociated.Enabled = false;
                            btnAssociated.Enabled = false;
                        }
                        else if (radType.Checked == true)
                        {
                            tbAssociated.Text = string.Empty;
                            tbAssociated.Enabled = false;
                            btnAssociated.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        private void LoadDefaultData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                {
                    tbSearchClientCode.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                    tbSearchClientCode.Enabled = false;
                }
                else
                {
                    tbSearchClientCode.Text = string.Empty;
                    tbSearchClientCode.Enabled = true;
                }
            }
        }
    }
}
