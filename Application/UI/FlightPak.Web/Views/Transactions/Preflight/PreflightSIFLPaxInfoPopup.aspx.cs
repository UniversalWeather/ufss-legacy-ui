﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightSIFLPaxInfoPopup : BaseSecuredPage
    {
        public PreflightMain Trip = new PreflightMain();
        private ExceptionManager exManager;
        private string PassengerRequestorID;
        private bool blShowRequestor = false;

        public Dictionary<Int64, Int64> NonPrivatePaxIds { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid
                        if (Request.QueryString["PassengerRequestorID"] != null)
                        {
                            PassengerRequestorID = Request.QueryString["PassengerRequestorID"];
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["ShowRequestor"]))
                            blShowRequestor = Convert.ToBoolean(Request.QueryString["ShowRequestor"]);

                        if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                        {
                            Trip = (PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                            PreparePassengerCatalog();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgPassengerCatalog;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method to get selected the Passenger in the grid
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (PassengerRequestorID != null || PassengerRequestorID != string.Empty)
                    {
                        foreach (GridDataItem item in dgPassengerCatalog.MasterTableView.Items)
                        {
                            if (item["PassengerRequestorID"].Text.Trim() == PassengerRequestorID)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Binds the grid
        /// </summary>
        protected void PreparePassengerCatalog()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // List for Personal Grid Bind
                List<PreflightSIFLPassengerCatalog> paxList = new List<PreflightSIFLPassengerCatalog>();
                // Get Flight Purpose List from Master
                Session.Remove("FPurposeList");

                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var retVal = Service.GetFlightPurposeList();
                    if (retVal.ReturnFlag == true)
                    {
                        Session["FPurposeList"] = retVal.EntityList;
                    }

                    for (int legIndex = 0; legIndex < Trip.PreflightLegs.Count; legIndex++)
                    {
                        if (Trip.PreflightLegs[legIndex].PreflightPassengerLists != null)
                        {
                            if (Trip.PreflightLegs[legIndex].PreflightPassengerLists.Count > 0)
                            {
                                foreach (PreflightPassengerList Pax in Trip.PreflightLegs[legIndex].PreflightPassengerLists)
                                {
                                    if (Session["FPurposeList"] != null)
                                    {
                                        List<FlightPakMasterService.FlightPurpose> purposeList = (List<FlightPakMasterService.FlightPurpose>)Session["FPurposeList"];
                                        purposeList = purposeList.Where(x => x.FlightPurposeID == Pax.FlightPurposeID && x.IsPersonalTravel == true).ToList();

                                        PreflightSIFLPassengerCatalog PassengerCatalog = new PreflightSIFLPassengerCatalog();
                                        PassengerCatalog.LegID = Convert.ToInt64(Pax.LegID);
                                        PassengerCatalog.LegIndex = legIndex + 1;
                                        PassengerCatalog.PassengerRequestorID = Convert.ToInt64(Pax.PassengerID);

                                        using (PreflightService.PreflightServiceClient objService = new PreflightServiceClient())
                                        {
                                            var retValue = objService.GetSIFLPassengerEmployeeType(PassengerCatalog.PassengerRequestorID);
                                            List<GetSIFLPassengerEmployeeType> resultList = new List<GetSIFLPassengerEmployeeType>();
                                            if (retValue.ReturnFlag == true && retValue.EntityList.Count > 0)
                                            {
                                                foreach (GetSIFLPassengerEmployeeType result in retValue.EntityList)
                                                {
                                                    if (result.IsEmployeeType == null)
                                                        PassengerCatalog.EmpType = ModuleNameConstants.Preflight.Employee_Type_NonControlled;
                                                    else
                                                        PassengerCatalog.EmpType = result.IsEmployeeType;
                                                    PassengerCatalog.PassengerRequestorCD = result.PassengerRequestorCD;
                                                }
                                            }
                                            else
                                            {
                                                PassengerCatalog.EmpType = string.Empty;
                                                PassengerCatalog.PassengerRequestorCD = string.Empty;
                                            }

                                            var AssoReturnValue = objService.GetAssociatePassengerID(Convert.ToInt64(Pax.PassengerID));
                                            if (AssoReturnValue.ReturnFlag == true && AssoReturnValue.EntityList.Count > 0)
                                            {
                                                PassengerCatalog.AssociatedPassengerID = AssoReturnValue.EntityList[0].PassengerRequestorID;
                                                PassengerCatalog.AssociatedPassengerCD = AssoReturnValue.EntityList[0].PassengerRequestorCD;
                                            }
                                            else
                                            {
                                                PassengerCatalog.AssociatedPassengerCD = string.Empty;
                                                PassengerCatalog.AssociatedPassengerID = 0;
                                            }

                                            PassengerCatalog.PaxListID = Pax.PreflightPassengerListID;
                                        }

                                        StringBuilder name = new StringBuilder();
                                        name.Append(Pax.PassengerLastName);
                                        if (Pax.PassengerLastName.IndexOf(",") == -1 && (!string.IsNullOrEmpty(Pax.PassengerFirstName) || !string.IsNullOrEmpty(Pax.PassengerFirstName)))
                                        {
                                            name.Append(",");
                                        }

                                        if (!string.IsNullOrEmpty(Pax.PassengerFirstName))
                                        {
                                            name.Append(" ");
                                            name.Append(Pax.PassengerFirstName);
                                            if (Pax.PassengerFirstName.IndexOf(",") == -1 && !string.IsNullOrEmpty(Pax.PassengerMiddleName))
                                            {
                                                name.Append(",");
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(Pax.PassengerMiddleName))
                                        {
                                            name.Append(" ");
                                            name.Append(Pax.PassengerMiddleName);

                                        }
                                        PassengerCatalog.PassengerName = name.ToString();

                                        if (purposeList.Count > 0)
                                            PassengerCatalog.IsPersonalTravel = purposeList[0].IsPersonalTravel;
                                        else
                                            PassengerCatalog.IsPersonalTravel = false;

                                        paxList.Add(PassengerCatalog);
                                    }
                                }
                            }
                        }
                    }
                }
                // Bind SIFL Personal Grid
                dgPassengerCatalog.DataSource = paxList;
                dgPassengerCatalog.DataBind();
            }
        }

        /// <summary>
        /// Class added for SIFL Personal Grid
        /// </summary>
        [Serializable()]
        public class PreflightSIFLPassengerCatalog
        {
            public long LegID { get; set; }
            public int LegIndex { get; set; }
            public long PaxListID { get; set; }
            public long PassengerRequestorID { get; set; }
            public long AssociatedPassengerID { get; set; }
            public string AssociatedPassengerCD { get; set; }
            public string EmpType { get; set; }
            public string PassengerRequestorCD { get; set; }
            public string PassengerName { get; set; }
            public bool? IsPersonalTravel { get; set; }
        }
        protected void dgPassengerCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string resolvedurl = string.Empty;
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;

                                GridDataItem item = dgPassengerCatalog.SelectedItems[0] as GridDataItem;
                                Session["PassengerRequestorID"] = item["PassengerRequestorID"].Text;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPassengerCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            string PassengerRequestorID = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient();
                            FlightPak.Web.FlightPakMasterService.Passenger oPassenger = new FlightPak.Web.FlightPakMasterService.Passenger();
                            GridDataItem item = dgPassengerCatalog.SelectedItems[0] as GridDataItem;
                            PassengerRequestorID = item.GetDataKeyValue("PassengerRequestorID").ToString();
                            oPassenger.PassengerRequestorID = Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString());
                            oPassenger.PassengerRequestorCD = item.GetDataKeyValue("PassengerRequestorCD").ToString();
                            if (item.GetDataKeyValue("IsActive") != null)
                            {
                                if (Convert.ToBoolean(item.GetDataKeyValue("IsActive")) == true)
                                {
                                    string alertMsg = "Active passenger cannot be deleted";
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    ShowLockAlertPopup(alertMsg, ModuleNameConstants.Database.Passenger);
                                    return;
                                }
                            }
                            oPassenger.IsDeleted = true;
                            var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString()));
                            if (!returnValue.ReturnFlag)
                            {
                                e.Item.Selected = true;
                                SelectItem();
                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                return;
                            }
                            PassengerService.DeletePassengerRequestor(oPassenger);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            SelectItem();

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(PassengerRequestorID));

                    }
                }
            }
        }
    }
}