﻿using System;
using System.Linq;
using Telerik.Web.UI;
using System.Web.UI;
using FlightPak.Web.PreflightService;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightPaxPopup : BaseSecuredPage
    {
        private string PreflightLeg;
        public Int64 TripID = 0;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["TripID"] != null)
                        {
                            TripID = Convert.ToInt64(Request.QueryString["TripID"]);
                        }
                        if (Request.QueryString["MultiSelect"] != null)
                        {
                            if (Request.QueryString["MultiSelect"].ToString().Trim() == "true")
                            {
                                dgPreflightPax.AllowMultiRowSelection = true;
                            }
                            else
                            {
                                dgPreflightPax.AllowMultiRowSelection = false;
                            }
                        }
                        else
                        {
                            dgPreflightPax.AllowMultiRowSelection = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgPreflightRetrieve_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (PreflightService.PreflightServiceClient ObjService = new PreflightServiceClient())
                        {
                            var objRetVal = ObjService.GetTrip(TripID);

                            DataTable dtpax = new DataTable();
                            dtpax.Columns.Add("PassengerID");
                            dtpax.Columns.Add("FirstName");
                            dtpax.Columns.Add("MiddleInitial");
                            dtpax.Columns.Add("LastName");
                            dtpax.Columns.Add("PassengerRequestorCD");
                            dtpax.Columns.Add("LegID");
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                for (int ival = 0; ival < objRetVal.EntityList[0].PreflightLegs.Count; ival++)
                                {
                                    for (int ipas = 0; ipas < objRetVal.EntityList[0].PreflightLegs[ival].PreflightPassengerLists.Count; ipas++)
                                    {

                                        var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(objRetVal.EntityList[0].PreflightLegs[ival].PreflightPassengerLists[ipas].PassengerID));
                                        DataRow row = dtpax.NewRow();
                                       
                                        //row["PassengerID"] = objRetVal.EntityList[0].PreflightLegs[ival].PreflightPassengerLists[ipas].PreflightPassengerListID;
                                        row["PassengerID"] = objRetVal.EntityList[0].PreflightLegs[ival].PreflightPassengerLists[ipas].PassengerID;
                                        row["FirstName"] = objPaxRetVal.EntityList[0].FirstName;
                                        row["MiddleInitial"] = objPaxRetVal.EntityList[0].MiddleInitial;
                                        row["LastName"] = objPaxRetVal.EntityList[0].LastName;
                                        row["PassengerRequestorCD"] = objPaxRetVal.EntityList[0].PassengerRequestorCD;
                                        row["LegID"] = objRetVal.EntityList[0].PreflightLegs[ival].PreflightPassengerLists[ipas].LegID;
                                        dtpax.Rows.Add(row);

                                    }
                                }
                                DataTable dtDistinctValue = dtpax.Clone();

                                foreach (DataRow dr in dtpax.Rows)
                                {
                                    Int64 crew = Convert.ToInt64(dr["PassengerID"]);
                                    //Int32 leg = Convert.ToInt32(dr["Leg"]);

                                    int intFoundRows = dtDistinctValue.Select("PassengerID=" + crew).Count();
                                    if (intFoundRows == 0)
                                        dtDistinctValue.ImportRow(dr);
                                }
                                //RemoveDuplicateRows(dtpax, "PassengerID"); 
                                dgPreflightPax.DataSource = dtDistinctValue;
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }

        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPreflightPax_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            GridCommandItem commandItem = e.Item as GridCommandItem;
                            HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                            if (Request.QueryString["MultiSelect"] != null)
                            {
                                if (Request.QueryString["MultiSelect"].ToString().Trim() == "true")
                                {
                                    container.Visible = true;
                                }
                                else
                                {
                                    container.Visible = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        //public DataTable RemoveDuplicateRows(DataTable dtpax, string PassengerID)
        //{

        //    Hashtable hTable = new Hashtable();

        //    ArrayList duplicateList = new ArrayList();

        //    foreach (DataRow row in dtpax.Rows)
        //    {

        //        if (hTable.Contains(row[PassengerID]))

        //            duplicateList.Add(PassengerID);

        //        else

        //            hTable.Add(row[PassengerID], string.Empty);

        //    }

        //    foreach (DataRow row in duplicateList)

        //        dtpax.Rows.Remove(row);

        //    return dtpax;

        //}

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgPreflightPax;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (PreflightLeg != null || PreflightLeg != string.Empty)
                {
                    foreach (GridDataItem Item in dgPreflightPax.MasterTableView.Items)
                    {
                        if (Item["PassengerRequestorCD"].Text.Trim() == PreflightLeg)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
            }
        }

        protected void dgPreflightPax_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgPreflightPax_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPreflightPax, Page.Session);

        }
    }
}