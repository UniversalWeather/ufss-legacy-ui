﻿using System;
using System.Collections.Generic;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Web.CorporateRequestService;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightCR : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public FlightPak.Web.PreflightService.PreflightMain Trip = new FlightPak.Web.PreflightService.PreflightMain();

        private delegate void SaveSession();
        private delegate void TripEdit();

        List<PreflightTripException> ExceptionList = new List<PreflightTripException>();

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        SaveSession SavePreflightSession = new SaveSession(_SavePreflightToSession);
                        Master.SaveToSession = SavePreflightSession;

                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;
                        //Replicate Save Cancel Delete Buttons in header
                        Master.SaveClick += btnSave_Click;
                        Master.CancelClick += btnCancel_Click;
                        Master.DeleteClick += btnDelete_Click;

                        if (!IsPostBack)
                        {
                            BindPreflightFromSession();

                            #region Authorization
                            CheckAutorization(Permission.Preflight.ViewPreflightCorporateRequest);

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightCorporateRequest))
                            {
                                btnNext.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.DeletePreflightCorporateRequest))
                            {
                                btnDeleteTrip.Visible = false;
                            }

                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];

                                if (Trip.State == TripEntityState.Added)
                                {
                                    if (!IsAuthorized(Permission.Preflight.AddPreFlightMain))
                                    {
                                        if (!IsAuthorized(Permission.Preflight.AddPreFlightMain))
                                        {
                                            btnSave.Visible = false;
                                            btnCancel.Visible = false;
                                        }
                                    }
                                    else if (Trip.State == TripEntityState.Modified)
                                    {
                                        if (!IsAuthorized(Permission.Preflight.EditPreFlightMain))
                                        {
                                            btnSave.Visible = false;
                                            btnCancel.Visible = false;
                                        }
                                    }

                                }
                            }

                            #endregion
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];
                            if (Trip.Mode == TripActionMode.Edit)
                            {
                                btnSave.Enabled = true;
                                btnCancel.Enabled = true;
                                btnDeleteTrip.Enabled = false;
                                btnEditTrip.Enabled = false;

                                btnSave.CssClass = "button";
                                btnCancel.CssClass = "button";
                                btnDeleteTrip.CssClass = "button-disable";
                                btnEditTrip.CssClass = "button-disable";
                                disableenablefields(true);
                            }
                            else
                            {
                                btnSave.Enabled = false;
                                btnCancel.Enabled = false;
                                btnDeleteTrip.Enabled = true;
                                btnEditTrip.Enabled = true;

                                btnSave.CssClass = "button-disable";
                                btnCancel.CssClass = "button-disable";
                                btnDeleteTrip.CssClass = "button";
                                btnEditTrip.CssClass = "button";
                                disableenablefields(false);
                            }
                        }
                        else
                        {
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDeleteTrip.Enabled = false;
                            btnEditTrip.Enabled = false;
                            btnNext.Enabled = false;

                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDeleteTrip.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            btnNext.CssClass = "button-disable";
                            disableenablefields(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }

        }

        protected void _SavePreflightToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];

                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        bool SaveToSession = false;
                        if (Trip.State == TripEntityState.Modified)
                        {
                            if (IsAuthorized(Permission.Preflight.EditPreFlightMain))
                            {
                                SaveToSession = true;
                            }
                        }
                        else if (Trip.State == TripEntityState.Added)
                        {
                            if (IsAuthorized(Permission.Preflight.AddPreFlightMain))
                            {
                                SaveToSession = true;
                            }
                        }

                        if (SaveToSession)
                        {
                            if (Trip.TripID == 0)
                                Trip.State = TripEntityState.Added;
                            else
                                Trip.State = TripEntityState.Modified;

                            Trip.RevisionDescriptioin = Master.RevisionDescription;
                            Trip.DispatchNotes = tbDispatcherNotes.Text;
                        }

                        Session["CurrentPreFlightTrip"] = Trip;


                        ExceptionList = (List<PreflightTripException>)Session["PreflightException"];
                        if (Trip.TripID == 0 && Trip.State == TripEntityState.Added)
                        {
                            Master.ValidationTrip(PreflightService.Group.Legs);
                            Master.BindException();
                        }

                        Master.TrackerLegs.Rebind();
                    }
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }

        protected void BindPreflightFromSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];

                    //Need to change this code for Revision number Label
                    //tbrevNumber.Text = Trip.RevisionNUM.ToString();
                    Master.RevisionDescription = Trip.RevisionDescriptioin;

                    tbDispatcherNotes.Text = Trip.DispatchNotes;

                    if (Trip.DispatchNUM != null)
                    {
                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                        {
                            if (!string.IsNullOrWhiteSpace(Trip.DispatchNUM))
                            {
                                var objRetVal = CorpSvc.GetRequestByTripNum(Convert.ToInt64(Trip.DispatchNUM));
                                if (objRetVal.ReturnFlag)
                                {
                                    if (objRetVal.EntityList[0].CRDispatchNotes != null)
                                    {
                                        if (objRetVal.EntityList[0].CRDispatchNotes[0].CRDispatchNotes != null)
                                            tbRequestorNotes.Text = objRetVal.EntityList[0].CRDispatchNotes[0].CRDispatchNotes.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];
                        Master.Delete(Trip.TripID);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }
        }

        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditTripEvent();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];
                        Master.Cancel(Trip);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            if (IsAuthorized(Permission.Preflight.AddPreflightCorporateRequest) || IsAuthorized(Permission.Preflight.EditPreflightCorporateRequest))
                            {
                                _SavePreflightToSession();
                                validateandrebindmaster();
                                Trip = (FlightPak.Web.PreflightService.PreflightMain)Session["CurrentPreFlightTrip"];
                                Master.Save(Trip);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        _SavePreflightToSession();

                        Master.Next("CR");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCorporateRequest);
                }
            }

        }

        private void validateandrebindmaster()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Trip.TripID == 0 && Trip.State == TripEntityState.Added)
                {
                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        Master.ValidationTrip(PreflightService.Group.CorporateRequest);
                        Master.BindException();
                        Master.Exceptions.Rebind();
                    }
                }
            }
        }

        private void disableenablefields(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                tbDispatcherNotes.Enabled = status;
            }
        }
    }
}