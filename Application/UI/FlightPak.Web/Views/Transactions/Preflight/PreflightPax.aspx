﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Preflight.Master"
    AutoEventWireup="true" CodeBehind="PreflightPax.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreflightPax" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Preflight.Master" %>
<%@ Register Src="~/UserControls/UCPreflightFooterButton.ascx" TagName="Footer" TagPrefix="UCPreflight" %>
<%@ Register Src="~/UserControls/Preflight/Pax/PaxSelection.ascx" TagPrefix="UCPreflight" TagName="PaxSelection" %>
<%@ Register Src="~/UserControls/Preflight/Pax/PaxSummary.ascx" TagPrefix="UCPreflight" TagName="PaxSummary" %>



<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <%=Scripts.Render("~/bundles/preflightpax") %>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />        
<style type="text/css">
    .removepad { text-align:center !important;}
    

    .ui-jqgrid .ui-jqgrid-htable th div {
        height: auto !important;
        overflow: hidden;
        padding-right: 4px;
        padding-top:0px;
        position: relative;
        vertical-align: text-top;
        white-space: normal !important;
        font-size:11px;
    }

    .jqgridTable tr td:first-child {
        text-align: center !important;
    }
    .ui-jqgrid tr.jqgrow td {
    padding:2px 1px !important;}
</style>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radPaxRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
            <telerik:RadWindow ID="rdPaxPassport" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false" OnClientClose="OnClientVisaClose"
                 Title="Pax Passport Visa">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientClosePaxInfoPopup"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                 AutoSize="true" KeepInScreenBounds="true" OnClientClose="OnClientHotelClose"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                 OnClientClose="OnClientHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMetroPopup" runat="server" OnClientResizeEnd="GetDimensions"
                 AutoSize="true" KeepInScreenBounds="true" OnClientClose="OnClientMetroClose" 
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/MetroMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCtryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                 AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientClose="OnClientCtryClose"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx" OnClientClose="OnClientTransportClose" >
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTransportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                 OnClientClose="OnClientTransportClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdArriveTransportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientArriveTransportClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnTripSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdOutBoundInstructions" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightOutboundInstruction.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdChecklist" runat="server" VisibleOnPageLoad="false" Width="700px" Height="500px" 
                Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightChecklist.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="990px" Height="590px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPaxPage" runat="server" OnClientResizeEnd="GetDimensions"
                Width="830px" Height="550px" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false"
                Behaviors="Close" VisibleStatusbar="false" Title="Passenger/Requestor">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
         
        <asp:HiddenField ID="hdFleetCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hdnPreflightPaxInitializationObject" Value="" ClientIDMode="Static" />
       
         <ul class="tabStrip">
            <li id="ulPaxInfo" class="tabStripItem tabStripItemSelected" showBlock="pnlPax" onclick="paxTabMainModulesChange(this);">PAX Info
            </li>
            <li id="ulPaxLogistics" class="tabStripItem" showBlock="pnlPaxLogistics" onclick="paxTabMainModulesChange(this);">PAX Logistics
            </li>
         </ul>
        

        <div id="pnlPax" >
            <div runat="server" id="ChecklistOutboundTable" style="padding-top: 8px; padding-bottom: 5px">
                <asp:LinkButton CssClass="link_small" ID="lnkbtnChecklist" Text="Checklist" runat="server"
                    OnClientClick="javascript:openWin('rdChecklist');return false;"></asp:LinkButton>
                |
                <asp:LinkButton CssClass="link_small" ID="lnkbtnOutbound" Text="Outbound Instructions"
                    runat="server" OnClientClick="javascript:openWin('rdOutBoundInstructions');return false;"></asp:LinkButton>
            </div>
          
            <div class="preflightpax_information" style="float: left; width: 718px; padding: 5px 0 0 0px;">
                <UCPreflight:PaxSelection runat="server" ID="PaxSelection" />
            </div>
            <div style="text-align: center; float: left; width: 718px; padding: 5px 0  5px 0px;">
            </div>
            <telerik:RadPanelBar ID="pnlPaxSummary" Width="100%" ExpandAnimation-Type="None"
                CollapseAnimation-Type="None" runat="server" OnClientItemExpand="LoadPaxSummaryExpand">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="false" Text="PAX Summary">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <UCPreflight:PaxSummary runat="server" ID="PaxSummary" />
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
        </div>
       
         <div id="pnlPaxLogistics" style="display:none;">
            
            <div class="ULTab-subTab">
                <ul class="tabmenu">
                    <li id="ulHotel" class="tabSelected" onclick="paxLogisticsModuleChange('1');">Hotel
                    </li>
                    <li id="ulTransportation" onclick="paxLogisticsModuleChange('2');">Transportation
                    </li>
                </ul>
            </div>
            <div style="height:10px;"></div>
            <div id="RadMultiPage1">
                 
                <div id="rdPgeViewHotel" style="display:block;">
                   
                            <div id="divHotel">
                                
                                <ul id="legTabhotel" data-bind="foreach: Legs" class="tabStrip">
                                    <li data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: LegtabClick }" class="tabStripItem">
                                        Leg <span data-bind="text: LegNUM"></span>(<span data-bind="    text: DepartureAirport.IcaoID"></span>-<span data-bind="    text: ArrivalAirport.IcaoID"></span>)</li>
                                </ul>

                                <input type="hidden" id="hdnHotelMode" value="none" />
                                <input type="hidden" id="hdnSelectedHotelID" value="0" />
                                <input type="hidden" id="hdnSelectedLegNum" value="1" />
                                <input type="hidden" id="hdnSelectedTransportLegNum" value="1" />
                                
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="jqgrid">

                                                <div>
                                                    <table class="box1" cellpadding="0" cellspacing="0">

                                                        <tr>
                                                            <td>
                                                                <table id="PaxHotelListGrid" class="table table-striped table-hover table-bordered preflight_paxhotel_grid"></table>
                                                            </td>
                                                        </tr>
                                                        <tr>                                                            
                                                            <td>
                                                                <!-- ko if: self.EditMode -->
                                                                <div class="ui-state-default ui-jqgrid-pager ui-corner-bottom grid_icon" dir="ltr" style="width: 716px;border:1px solid #aaaaaa;border-width:0 1px 1px!important;padding:3px 0px 4px;">
                                                                    <div role="group" id="PaxHotelListGrid_gridPager">
                                                                    <a title="Add" class="add-icon-grid"  id="PaxHotelrecordAdd" data-bind="event: { click: PaxHotelrecordAddClick }" ></a>
                                                                    <a id="PaxHotelrecordEdit" title="Edit" class="edit-icon-grid" data-bind="event: { click: PaxHotelrecordEditClick }" ></a>
                                                                    <a id="PaxHotelrecordDelete" title="Delete" class="delete-icon-grid" data-bind="event: { click: PaxHotelrecordDeleteClick }" ></a>
                                                                    </div>
                                                                    <div id="PaxHotelListGridpagesizebox"> </div>
                                                                </div>                                                                
                                                                <!-- /ko -->
                                                            </td>                                                            
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td style="display: none"></td>
                                        <td>                                            
                                            <input type="radio" id="rdArrival" name="rdArrivalDeparture" onclick="return validateButtonList();" data-bind="    enable: EnablePaxHotelMode, checked: viewModel.isArrivalHotel, checkedValue: true" />
                                            <label for="rdArrival">Arrival</label>


                                            <input type="radio" id="rdDeparture" name="rdArrivalDeparture" onclick="return validateButtonList();" data-bind="    enable: EnablePaxHotelMode, checked: viewModel.isArrivalHotel, checkedValue: false" />
                                            <label for="rdDeparture">Departure</label>

                                            <input type="hidden" id="hdnrbArriveDepartPrev" />
                                            <input type="hidden" id="hdnArrivalDate" />
                                            <input type="hidden" id="hdnNextDeptDate" />
                                            <input type="hidden" id="hdnHotelchanged" value="false" />
                                        </td>
                                        <td align="right">
                                            
                                            <table>
                                                <tr>
                                                    <td>
                                                        <button type="button" id="btnCancelHotel" value="Cancel Hotel" onclick="btnCancelHotelClick(); return false;" data-bind="enable: EnablePaxHotelMode, css: PaxHotelSaveIcon">
                                                            Cancel Hotel</button>                                                        
                                                    </td>
                                                    <td>
                                                        <button type="button" id="btnSaveHotel" onclick="saveHotelToTripSession();return false;" value="Save Hotel"
                                                            data-bind="enable: EnablePaxHotelMode, css: PaxHotelSaveIcon">
                                                            Save Hotel</button>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                </table>
                                <div style="float: left; width: 713px; padding: 5px 5px 0 0; background: #f6f6f6;">
                                    <asp:HiddenField ID="hdnLegNumHotel" runat="server" />
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center"></td>
                                            <td align="right">
                                                <table>
                                                    <tr>
                                                        <td>Status
                                                        </td>
                                                        <td>
                                                            <select data-bind="enable: EnablePaxHotelMode, value: viewModel.Status() == null ? '0' : viewModel.Status" id="ddlHotelCompleteds">
                                                                <option value="Select" selected="selected">Select</option>
                                                                <option value="Required">Required</option>
                                                                <option value="Not Required">Not Required</option>
                                                                <option value="In Progress">In Progress</option>
                                                                <option value="Change">Change</option>
                                                                <option value="Canceled">Canceled</option>
                                                                <option value="Completed">Completed</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="float: left; width: 718px; padding: 5px 0 0 0; background: #f6f6f6;">
                                    <table width="100%">
                                        <tr>
                                            <td width="54%" valign="top">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table class="border-box" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel90" align="left" style="display: none"></td>
                                                                    <td align="left" colspan="3" style="display: none"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90" valign="top">Hotel Code
                                                                    </td>
                                                                    <td class="tdLabel90">
                                                                        <input type="text"  id="tbHotelCode" maxlength="4" class="text50" data-bind="enable: EnablePaxHotelMode, value: viewModel.HotelCode, event: { change: HotelCodeChange }" />
                                                                        <input type="button" id="btnCode" onclick="javascript: openWin('radHotelPopup'); return false;" data-bind="    enable: self.EnablePaxHotelMode, css: BrowseBtnPaxHotel" />
                                                                    </td>
                                                                    <td align="left" class="tdLabel30">Rate
                                                                    </td>
                                                                    <td class="pr_radtextbox_75" align="left">                                                                        
                                                                        <input pattern="[0-9]*" type="text" id="tbHotelRate" data-bind="enable: EnablePaxHotelMode, CostCurrencyElement: viewModel.Rate"  style="width:95px;"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td align="left" colspan="3">
                                                                        <asp:Label ID="lbHotelCode" runat="server" Visible="true"></asp:Label>
                                                                        <label  id="lbcvHotelCode" class="alert-text"></label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90" valign="top">Hotel Name
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbHotelName" maxlength="60" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.PreflightHotelName"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">Addr 1:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbAddr1" maxlength="100" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.Address1"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">Addr 2:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input id="tbAddr2" type="text" maxlength="100" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.Address2"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">Addr 3:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbAddr3" maxlength="100" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.Address3"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">City:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbCity" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.CityName"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">State/Prov:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbState" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.StateProvince"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">Country:
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="tbCtry" class="text50" data-bind="enable: EnablePaxHotelMode, value: viewModel.CountryCode, event: { change: countryChange }"></input>
                                                                        <input type="button" id="btnCtry" onclick="javascript: openWin('rdCtryPopup'); return false;"
                                                                             data-bind="enable: self.EnablePaxHotelMode, css: BrowseBtnPaxHotel"/>
                                                                    </td>
                                                                    <td valign="justify">Metro:
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="tbMetro" maxlength="3"  class="text50" data-bind="enable: EnablePaxHotelMode, value: viewModel.Metro, event: { change: metroChange }"></input>
                                                                        <input type="button" id="btnMetro" onclick="javascript: openWin('rdMetroPopup'); return false;"
                                                                             data-bind="enable: self.EnablePaxHotelMode, css: BrowseBtnPaxHotel"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <label id="lbcvCtry" class="alert-text"></label>
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <label id="lbcvMetro"  class="alert-text"></label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90" valign="top">Postal Code:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbPost" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.PostalCode"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90" valign="top">Phone
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbHotelPhone" maxlength="25" class="text228" onKeyPress="return fnAllowPhoneFormat(this,event)" data-bind="enable: EnablePaxHotelMode, value: viewModel.PhoneNum1"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90" valign="top">Fax
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <input type="text" id="tbHotelFax" maxlength="25"  class="text228" onKeyPress="return fnAllowPhoneFormat(this,event)" data-bind="enable: EnablePaxHotelMode, value: viewModel.FaxNUM"></input>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="border-box" cellspacing="0" cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel100" colspan="2">

                                                                        <input type="checkbox" id="chkBookNightPrior" data-bind="enable: EnablePaxHotelMode, checked: viewModel.IsBookNight" />
                                                                        <label for="chkBookNightPrior">Book Night Prior</label>
                                                                    </td>
                                                                    <td align="left" colspan="2">
                                                                        <input type="checkbox" id="chkEarlyCheckin" value="Early Check-in" data-bind="enable: EnablePaxHotelMode, checked: viewModel.IsEarlyCheckIn" />
                                                                        <label for="chkEarlyCheckin">Early Check-in</label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">Date In
                                                                    </td>
                                                                    <td class="tdLabel80">
                                                                        <input type="text" id="tbDateIn" class="text70" maxlength="10" data-bind="enable: EnablePaxHotelMode, datepicker: viewModel.ClientDateIn"></input>
                                                                    </td>
                                                                    <td valign="top">No. of Rooms
                                                                    </td>
                                                                    <td valign="top">
                                                                        <input type="text" id="tbNoforooms" maxlength="2" class="text46" onKeyPress="return fnAllowNumeric(this, event)" data-bind="enable: EnablePaxHotelMode, checked: viewModel.NoofRooms"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Date Out
                                                                    </td>
                                                                    <td class="tdLabel80">
                                                                        <input type="text" id="tbDateOut" class="text70"  maxlength="10" data-bind="enable: EnablePaxHotelMode, datepicker: viewModel.ClientDateOut"/>
                                                                    </td>
                                                                    <td valign="top">No. of Beds
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" data-bind="enable: EnablePaxHotelMode, value: viewModel.NoofBeds" id="tbNoofBeds" maxlength="3" class="text46" onKeyPress="return fnAllowNumeric(this, event)"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">Type of Room
                                                                    </td>
                                                                    <td align="left" colspan="3">
                                                                        <select id="ddltypeofRoom" data-bind="enable: EnablePaxHotelMode, options: self.Rooms, optionsValue: 'RoomTypeID', optionsText: 'RoomDescription', value: viewModel.RoomTypeID" class="text170">
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">Room Desc.
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <select data-bind="enable: EnablePaxHotelMode, options: self.RoomDescription, optionsValue: 'RoomDescCode', optionsText: 'RoomDescription', value: viewModel.RoomDescription" id="ddlRoomDesc" class="text170">                                                                           
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <input type="checkbox" id="chkRequestOnly" value="Request Only" data-bind="enable: EnablePaxHotelMode, checked: viewModel.IsRequestOnly" />
                                                                        <label for="chkRequestOnly">Request Only</label>
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <input type="checkbox" id="chkSmoking" value="Smoking" data-bind="enable: EnablePaxHotelMode, checked: viewModel.IsSmoking" />
                                                                        <label for="chkSmoking">Smoking</label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="horizontalLine"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdLabel90">Club Card
                                                                    </td>
                                                                    <td align="left" colspan="3">
                                                                        <input type="text" id="tbClubCard" maxlength="100" class="text228" data-bind="enable: EnablePaxHotelMode, value: viewModel.ClubCard"></input>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Max. Cap Amt
                                                                    </td>
                                                                    <td align="left" class="pr_radtextbox_100">
                                                                        <telerik:RadNumericTextBox ID="tbMaxcapamt" runat="server" Type="Currency" Culture="en-US"
                                                                            MaxLength="10" Value="0.00"
                                                                            NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right" data-bind="enable: EnablePaxHotelMode,value:viewModel.MaxCapAmount">
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                    <td align="left" colspan="2">
                                                                        <input type="checkbox" id="chkRatecap" value="Rate Cap" data-bind="enable: EnablePaxHotelMode, checked: viewModel.IsRateCap" />
                                                                        <label for="chkRatecap">Rate Cap</label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50%" valign="top">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table class="border-box" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="checkbox" id="chkNoPAX" onclick="NoPAX_OnCheckedChanged(this);" data-bind="    enable: EnablePaxHotelMode, checked: viewModel.NoPAX" />
                                                                                    <label for="chkNoPAX">No PAX</label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="jqgrid">
                                                                                        <div>
                                                                                            <table class="box1 paxlogistic_nopax">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table id="PaxLegEntityGrid" class="table table-striped table-hover table-bordered jqgridTable"></table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div class="grid_icon">
                                                                                                            <div role="group" id="pg_gridPager"></div>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="border-box paxconfirmation_comment" width="100%">
                                                                <tr>
                                                                    <td valign="bottom" colspan="4">Confirmation
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pr_radtextbox_320x40" colspan="4">
                                                                        <textarea id="tbConfirmation" rows="1" data-bind="enable: EnablePaxHotelMode, value: viewModel.ConfirmationStatus"></textarea>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="bottom" colspan="4">Comments
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pr_radtextbox_320x40" colspan="4">
                                                                        <textarea id="tbComments" rows="1" data-bind="enable: EnablePaxHotelMode, value: viewModel.Comments"></textarea>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                      </div>
                </div>                
                   <div id="radPgeViewTransport" style="display:none;">
                            
                            <ul id="legTabTransport" data-bind="foreach: Legs" class="tabStrip">
                                <li data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: LegtabClick }" class="tabStripItem">
                                    Leg <span data-bind="text: LegNUM"></span>(<span data-bind="    text: DepartureAirport.IcaoID"></span>-<span data-bind="    text: ArrivalAirport.IcaoID"></span>)</li>
                            </ul>
                            
                            <div style="float: left; width: 718px; padding: 5px 0 0 0; background: #f6f6f6;">
                                <div style="float: left; width: 352px; padding: 5px 5px 5px 0px;">                                    
                                    <fieldset>
                                        <legend>Depart Transportation</legend>
                                        <table cellpadding="1" cellspacing="2">
                                            <tr>
                                                <td>
                                                    Status
                                                </td>
                                                <td colspan="4">
                                                    <asp:CheckBox ID="chkTransCompleted" runat="server" Text="Completed" Visible="false" />
                                                    <select class="depart_transport" id="ddlDepartTransportCompleteds" data-bind="enable: EditMode, value: Transport.SelectedDepartTransportStatus, event: { change: valueChangeCall }"  >
                                                        <option Value="Select" selected="selected">Select</option>
                                                        <option value="Required">Required</option>
                                                        <option value="Not Required">Not Required</option>
                                                        <option value="In Progress">In Progress</option>
                                                        <option value="Change">Change</option>
                                                        <option value="Canceled">Canceled</option>
                                                        <option value="Completed">Completed</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Code
                                                </td>
                                                <td class="tdLabel70" colspan="2">
                                                    <input type="text" id="tbTransCode" class="text50"   data-bind="enable: EditMode, value: Transport.DepartCode, event: { change: DepartTransportCodeChange }"></input>
                                                    <input type="button" id="btnTransport" onclick="javascript: openWin('rdTransportPopup'); return false;"
                                                         data-bind="enable: self.EditMode, css: BrowseBtn" />
                                                </td>
                                                <td class="tdLabel60" align="center">
                                                    <label for="tbTransRate">Rate</label>
                                                </td>
                                                <td class="pr_radtextbox_55" align="left">    
                                                    
                                                    <input type="text" pattern="[0-9]*"  id="tbTransRate" maxlength="6" text="0.00"  style="  margin-right: 13px;width: 50px;" data-bind="enable: EditMode, transportRateElement: Transport.DepartRate"/>                                                    
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="left" colspan="4">
                                                    <label id="lbTransCode" style="display:none;" ></label>
                                                    <label id="lbcvTransCode" class="alert-text" ></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Name
                                                </td>
                                                <td colspan="4">
                                                    <input type="text" id="tbTransName" maxlength="60" class="text242" data-bind="enable: EditMode, value: Transport.DepartName"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Phone
                                                </td>
                                                <td colspan="4">
                                                    <input id="tbTransPhone" type="text" maxlength="25" class="text242" onKeyPress="return fnAllowPhoneFormat(this,event)" data-bind="enable: EditMode, value: Transport.DepartPhone"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Fax
                                                </td>
                                                <td colspan="4">
                                                    <input type="text" id="tbTransFax" maxlength="25" class="text242" data-bind="enable: EditMode, value: Transport.DepartFax" onKeyPress="return fnAllowPhoneFormat(this,event)" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Confirmation
                                                </td>
                                                <td colspan="4" class="pr_radtextbox_226">
                                                    <textarea  id="tbTransConfirmation" style="width:235px" data-bind="enable: EditMode, value: Transport.DepartConfirmation, event: { change: valueChangeCall }" rows="2" cols="35" ></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Comments
                                                </td>
                                                <td colspan="4" class="pr_radtextbox_226">
                                                    <textarea  id="tbTransComments" style="width:235px" data-bind="enable: EditMode, value: Transport.DepartComments, event: { change: valueChangeCall }" rows="2" cols="35" ></textarea>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                                <div style="float: left; width: 352px; padding: 5px 5px 5px 0px;">
                                    <fieldset>
                                        <legend>Arrive Transportation</legend>
                                        <table cellpadding="1" cellspacing="2">
                                            <tr>
                                                <td>
                                                    Status
                                                </td>
                                                <td colspan="4">
                                                    <select class="depart_transport" id="ddlArriveTransportCompleteds" data-bind="enable: EditMode, value: Transport.SelectedArrivalTransportStatus, event: { change: valueChangeCall }" >
                                                        <option value="Select" Selected="Selected">Select</option>
                                                        <option value="Required">Required</option>
                                                        <option value="Not Required">Not Required</option>
                                                        <option value="In Progress">In Progress</option>
                                                        <option value="Change">Change</option>
                                                        <option value="Canceled">Canceled</option>
                                                        <option value="Completed">Completed</option>
                                                    </select>                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Code
                                                </td>
                                                <td class="tdLabel70" colspan="2">
                                                    <input type="text" id="tbArriveTransCode" class="text50"   data-bind="enable: EditMode, value: Transport.ArrivalCode, event: { change: ArrivalTransportCodeChange }"/>
                                                    <input type="button" id="btnArriveTrans" onclick="javascript: openWin('rdArriveTransportPopup'); return false;"
                                                         data-bind="enable: self.EditMode, css: BrowseBtn"/>
                                                </td>
                                                <td class="tdLabel50" align="center">
                                                    Rate
                                                </td>
                                                <td class="pr_radtextbox_55">
                                                    
                                                    <input type="text" pattern="[0-9]*" id="tbArriveRate" style="  margin-right: 13px;width: 68px;" maxlength="6" data-bind="enable: EditMode, transportRateElement: Transport.ArrivalRate"/>
                                                    
                                                </td>                                                
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td align="left" colspan="4">
                                                        <label id="lbArriveTransCode" style="display:none;" ></label>
                                                        <label id="lbcvArriveTransCode" class="alert-text" ></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Name
                                                    </td>
                                                    <td colspan="4">
                                                        <input type="text" id="tbArriveTransName" maxlength="60" class="text242" data-bind="enable: EditMode, value: Transport.ArrivalName" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Phone
                                                    </td>
                                                    <td colspan="4">
                                                        <input type="text" id="tbArrivePhone" maxlength="25" class="text242"
                                                            onkeypress="return fnAllowPhoneFormat(this,event)" data-bind="enable: EditMode, value: Transport.ArrivalPhone" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Fax
                                                    </td>
                                                    <td colspan="4">
                                                        <input type="text" id="tbArriveFax" maxlength="25" class="text242" onkeypress="return fnAllowPhoneFormat(this,event)" data-bind="enable: EditMode, value: Transport.ArrivalFax" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">Confirmation
                                                    </td>                                                    
                                                    <td colspan="4" class="pr_radtextbox_226">
                                                        <textarea id="tbArriveConfirmation" style="width:235px" cols="35" rows="2" data-bind="enable: EditMode, value: Transport.ArrivalConfirmation, event: { change: valueChangeCall }" ></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">Comments
                                                    </td>                                                 
                                                    <td colspan="4" class="pr_radtextbox_226">
                                                        <textarea id="tbArriveComments" style="width:235px" cols="35" rows="2" data-bind="enable: EditMode, value: Transport.ArrivalComments, event: { change: valueChangeCall }" ></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                                
                            
                    </div>
            
            </div>            
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <asp:Button ID="btnInteractiveMap" Visible="false" CssClass="button" runat="server"
                            Text="InteractiveMap" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnTranHotCancel" Visible="false" CssClass="button" runat="server"
                            Text="Cancel"  />
                        <asp:Button ID="btnTranHotSave" CssClass="button" runat="server" Visible="false"
                            Text="Save" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="text-align: center; float: left; width: 718px; padding: 5px 0  5px 0px;">
        </div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right" style="padding: 11px 0 7px;">                    
                     <UCPreflight:Footer ID="PreflightHeader" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnInsert" />
                </td>
            </tr>
        </table>             
    </div>
</asp:Content>
