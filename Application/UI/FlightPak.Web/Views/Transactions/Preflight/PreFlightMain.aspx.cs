﻿using System;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Views.Transactions.Preflight;
using Newtonsoft.Json;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreFlightMain : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["TabSelect"] = "PreFlight";
            PreflightTripManager.CheckSessionAndRedirect();
            if (IsPostBack == false)
            {
                PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                pfViewModel.PreflightMain = new PreflightMainViewModel();
                pfViewModel.PreflightMain = PreflightTripManager.CreateOrCancelTrip(pfViewModel.PreflightMain);
                BusinessLite.Preflight.PreflightTripManagerLite.UpdatePreflightTripViewModelAsPerCalenderSelection(pfViewModel);

                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    pfViewModel = PreflightTripManager.TripByTripID(Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]));
                    Session.Remove("SearchItemPrimaryKeyValue");
                }

                hdnPreflightMainInitializationObject.Value = JsonConvert.SerializeObject(pfViewModel);
                UserPrincipalViewModel userPrincipal = PreflightTripManager.getUserPrincipal();
                hdnUserPrincipalInitializationObject.Value = JsonConvert.SerializeObject(userPrincipal);

            }
        }
    }
}
