﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>PAX Group</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript">
        var selectedRowData = "";
        var jqgridTableId = '#gridPaxGroup';
        var selectedRowMultiplePaxGroup = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var passengerGroupCD = [];
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            var selectedRowData = getSelectedItems('PassengerGroupCD', '');
            selectedRowMultiplePaxGroup = $.trim(decodeURI(getSelectedItems("PassengerGroupCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultiplePaxGroup);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                passengerGroupCD = selectedRowData.split(',');
            }
            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultiplePaxGroup.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please Select a Pax Group.', popupTitle);
                }
                return false;
            });

            jQuery(jqgridTableId).jqGrid({

                url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.apiType = 'fss';
                    postData.method = 'PaxGroups';
                    postData.passengerGroupCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return passengerGroupCD[0]; } else { return selectedRowData; } };
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },

                height: 200,
                width: 600,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                enableclear: true,
                emptyrecords: "No records to view.",
                colNames: ['', 'PAX Group Code', 'Description', 'Home Base'],
                colModel: [
                    { name: 'PassengerGroupID', index: 'PassengerGroupID', key: true, hidden: true },
                    { name: 'PassengerGroupCD', index: 'PassengerGroupCD', align: 'left' },
                    { name: 'PassengerGroupName', index: 'PassengerGroupName', width: 220, align: 'left' },
                    { name: 'HomebaseCD', index: 'IcaoID', align: 'left' }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultiplePaxGroup = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultiplePaxGroup, rowData.PassengerGroupCD);

                }, onSelectAll: function (id, status) {
                    selectedRowMultiplePaxGroup = JqgridSelectAll(selectedRowMultiplePaxGroup, jqgridTableId, id, status, 'PassengerGroupCD');
                    jQuery("#hdnselectedfccd").val(selectedRowMultiplePaxGroup);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultiplePaxGroup, rowid, rowObject.PassengerGroupCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });

    </script>

    <script type="text/javascript">

        function returnToParent(rowData, one) {
            selectedRowMultiplePaxGroup = jQuery.trim(selectedRowMultiplePaxGroup);
            if (selectedRowMultiplePaxGroup.lastIndexOf(",") == selectedRowMultiplePaxGroup.length - 1)
                selectedRowMultiplePaxGroup = selectedRowMultiplePaxGroup.substr(0, selectedRowMultiplePaxGroup.length - 1);

            var oArg = new Object();
            var oWnd = GetRadWindow();
            if (one == 0) {
                oArg.PassengerGroupCD = selectedRowMultiplePaxGroup;
            }
            else {
                oArg.PassengerGroupCD = rowData["PassengerGroupCD"];
            }

            oArg.PassengerGroupID = rowData["PassengerGroupID"];
            oArg.PassengerGroupName = rowData["PassengerGroupName"];
            oArg.HomebaseCD = rowData["HomebaseCD"];
            oArg.Arg1 = oArg.PassengerGroupCD;
            oArg.CallingButton = "PassengerGroupCD";

            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>
    
</head>
<body>
    <form id="form1" runat="server">
        <div class="jqgrid">
            <input type="hidden" id="hdnselectedfccd" value=""/>
            <div>
                <table class="box1">
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridPaxGroup" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                            <div role="group" id="pg_gridPager"></div>
                            <div id="pagesizebox">
                                <span>Page Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                            </div>
                        </div>
                            <div style="text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
