﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PreflightLegacy.Master"
    AutoEventWireup="true" CodeBehind="PreflightPAXNewTemplate.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightPAXNewTemplate" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PreflightLegacy.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptBlock runat="server" ID="scriptBlock">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">

            function confirmDeletePaxCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeletePaxYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeletePaxNo.ClientID%>').click();
                }
            }



            function confirmPaxUpdateAllLegsCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnApplyPAXAllLegsYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnApplyPAXAllLegsNo.ClientID%>').click();
                }
            }


            function HeaderClick(CheckBox) {
                //Get target base & child control.
                var TargetBaseControl = document.getElementById('<%= this.dgLegEntity.ClientID %>');
                var TargetChildControl = "chkSelect";

                //Get all the control of the type INPUT in the base control.
                var Inputs = TargetBaseControl.getElementsByTagName("input");

                //Checked/Unchecked all the checkBoxes in side the GridView.
                for (var n = 0; n < Inputs.length; ++n)
                    if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                        Inputs[n].checked = CheckBox.checked;
            }

            function confirmPaxRemoveCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnPaxRemoveYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnPaxRemoveNo.ClientID%>').click();
                }
            }

            function confirmPaxNextCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnPaxNextYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnPaxNextNo.ClientID%>').click();
                }
            }

            function confirmInfoTabCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnInfoTabYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnInfoTabNo.ClientID%>').click();
                }
            }

            function confirmDeleteHotelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeleteHotelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeleteHotelNo.ClientID%>').click;
                }
            }

            function alertPaxCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnAlert.ClientID%>').click();
                }
            }
            //            function onRowDropping(sender, args) {
            //                if (sender.get_id() == "<%=dgAvailablePax.ClientID %>") {
            //                    var node = args.get_destinationHtmlElement();
            //                    if (!isChildOf('<%=dgAvailablePax.ClientID %>', node)) {
            //                        args.set_cancel(true);
            //                    }
            //                }
            //                else {
            //                    var node = args.get_destinationHtmlElement();
            //                    if (!isChildOf('trashCan', node)) {
            //                        args.set_cancel(true);
            //                    }
            //                    else {
            //                        if (confirm("Are you sure you want to delete this order?"))
            //                            args.set_destinationHtmlElement($get('trashCan'));
            //                        else
            //                            args.set_cancel(true);
            //                    }
            //                }
            //            }

            function isChildOf(parentId, element) {
                while (element) {
                    if (element.id && element.id.indexOf(parentId) > -1) {
                        return true;
                    }
                    element = element.parentNode;
                }
                return false;
            }
            function maxLength(field, maxChars) {
                if (field.value.length >= maxChars) {
                    event.returnValue = false;
                    return false;
                }
            }

            function openWinTransport(type) {
                var url = '';
                if (type == "DEPART") {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdnDepartIcao.ClientID%>').value + '&TransportCD=' + document.getElementById('<%=tbTransCode.ClientID%>').value;
                    var oWnd = radopen(url, 'radTransportCodePopup');
                    oWnd.add_close(OnClientTransportClose);
                }
                else {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdnArrivalICao.ClientID%>').value + '&TransportCD=' + document.getElementById('<%=tbTransCode.ClientID%>').value;
                    var oWnd = radopen(url, 'radTransportCodePopup');
                    oWnd.add_close(OnClientArriveTransportClose);
                }

            }

            function validateButtonList() {
                if (document.getElementById('<%=tbHotelCode.ClientID %>').value != "" || document.getElementById('<%=tbHotelName.ClientID %>').value != "") {
                    var list = document.getElementById("<% =rbArriveDepart.ClientID %>"); //Cleint ID of RadioButtonList
                    var rdbtnLstValues = list.getElementsByTagName("input");
                    var Checkdvalue;
                    for (var i = 0; i < rdbtnLstValues.length; i++) {
                        if (rdbtnLstValues[i].checked) {
                            Checkdvalue = rdbtnLstValues[i];
                            break;
                        }
                    }

                    if (document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value != Checkdvalue.value) {
                        document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value = Checkdvalue.value;
                        radconfirm("Changing Arrival or Departure will clear hotel information. Do you want to continue?", confirmrbArrivalDepartCallBackFn, 330, 110, null, "Confirmation!")
                    }
                }
            }


            function confirmrbArrivalDepartCallBackFn(arg) {
                var list = document.getElementById("<% =rbArriveDepart.ClientID %>"); //Cleint ID of RadioButtonList
                var rdbtnLstValues = list.getElementsByTagName("input");
                var Checkdvalue;
                for (var i = 0; i < rdbtnLstValues.length; i++) {
                    if (rdbtnLstValues[i].checked) {
                        Checkdvalue = rdbtnLstValues[i];
                        break;
                    }
                }
                if (arg == true) {
                    document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value = Checkdvalue.value;
                    document.getElementById('<%=tbHotelCode.ClientID%>').value = "";
                    document.getElementById('<%=tbHotelRate.ClientID%>').value = "";
                    document.getElementById('<%=tbHotelRate.ClientID%>').value = "0.00";
                    document.getElementById('<%=tbHotelName.ClientID%>').value = "";
                    document.getElementById('<%=tbHotelPhone.ClientID%>').value = "";
                    document.getElementById('<%=tbHotelFax.ClientID%>').value = "";
                    document.getElementById('<%=tbNoforooms.ClientID%>').value = "";
                    document.getElementById('<%=tbNoofBeds.ClientID%>').value = "";
                    document.getElementById('<%=tbDateIn.ClientID%>').value = "";
                    document.getElementById('<%=tbDateOut.ClientID%>').value = "";
                    document.getElementById('<%=hdnHotelAirID.ClientID%>').value = "";
                    document.getElementById('<%=hdnHotelID.ClientID%>').value = "";
                    document.getElementById('<%=radlstArrangements.ClientID%>').selectedValue = "None";
                    document.getElementById('<%=tbAddr1.ClientID%>').value = "";
                    document.getElementById('<%=tbAddr2.ClientID%>').value = "";
                    document.getElementById('<%=tbAddr3.ClientID%>').value = "";
                    document.getElementById('<%=tbMetro.ClientID%>').value = "";
                    document.getElementById('<%=tbState.ClientID%>').value = "";
                    document.getElementById('<%=tbCity.ClientID%>').value = "";
                    document.getElementById('<%=tbPost.ClientID%>').value = "";
                    document.getElementById('<%=tbCtry.ClientID%>').value = "";
                    document.getElementById('<%=hdnCtryID.ClientID%>').value = "";
                    document.getElementById('<%=lbcvMetro.ClientID%>').innerHTML = "";
                    document.getElementById('<%=lbcvCtry.ClientID%>').innerHTML = "";
                    document.getElementById('<%=lbcvHotelCode.ClientID%>').innerHTML = "";

                    if (document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value == "0") {
                        document.getElementById('<%=tbDateIn.ClientID%>').value = document.getElementById('<%=hdnArrivalDate.ClientID%>').value;
                        document.getElementById('<%=tbDateOut.ClientID%>').value = document.getElementById('<%=hdnNextDeptDate.ClientID%>').value;
                    }
                    else {
                        document.getElementById('<%=tbDateIn.ClientID%>').value = "";
                        document.getElementById('<%=tbDateOut.ClientID%>').value = "";

                    }

                }
                else {
                    if (Checkdvalue.value == "0")
                        document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value = "1";
                    else
                        document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value = "0";


                    var list = document.getElementById("<% =rbArriveDepart.ClientID %>"); //Cleint ID of RadioButtonList
                    var rdbtnLstValues = list.getElementsByTagName("input");
                    for (var i = 0; i < rdbtnLstValues.length; i++) {
                        if (rdbtnLstValues[i].value == document.getElementById('<%=hdnrbArriveDepartPrev.ClientID%>').value)
                            rdbtnLstValues[i].checked = true;
                        else
                            rdbtnLstValues[i].checked = false;
                    }
                }

            }

            function OnClientHotelClose(oWnd, args) {
                var combo = $find("<%= tbHotelCode.ClientID %>");
                //get the transferred arguments

                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdnHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbHotelName.ClientID%>").value = arg.Name;
                        //document.getElementById("<%=lbHotelCode.ClientID%>").innerHTML = arg.Name;
                        if (arg.NegociatedRate != undefined && arg.NegociatedRate != "&nbsp;")
                            document.getElementById("<%=tbHotelRate.ClientID%>").value = arg.NegociatedRate;

                        if (arg.HotelCD != null)
                            document.getElementById("<%=lbcvHotelCode.ClientID%>").innerHTML = "";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        ajaxManager.ajaxRequest("tbHotelCode_TextChanged");
                    }
                    else {
                        document.getElementById("<%=hdnHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdnHotelID.ClientID%>").value = "";
                        //document.getElementById("<%=lbHotelCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=tbHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbHotelFax.ClientID%>").value = "";
                    }
                }
            }

            function OnClientMetroClose(oWnd, args) {
                var combo = $find("<%= tbMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnMetroID.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=tbMetro.ClientID%>").value = arg.MetroCD;
                        if (arg.MetroCD != null)
                            document.getElementById("<%=lbcvMetro.ClientID%>").innerHTML = "";

                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        ajaxManager.ajaxRequest("tbMetroCode_TextChanged");
                    }
                    else {
                        document.getElementById("<%=hdnMetroID.ClientID%>").value = "";
                        document.getElementById("<%=tbMetro.ClientID%>").value = "";
                    }
                }
            }

            function OnClientCtryClose(oWnd, args) {
                var combo = $find("<%= tbCtry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnCtryID.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=tbCtry.ClientID%>").value = arg.CountryCD;
                        if (arg.CountryCD != null)
                            document.getElementById("<%=lbcvCtry.ClientID%>").innerHTML = "";

                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        ajaxManager.ajaxRequest("tbCtryCode_TextChanged");
                    }
                    else {
                        document.getElementById("<%=hdnCtryID.ClientID%>").value = "";
                        document.getElementById("<%=tbCtry.ClientID%>").value = "";
                    }
                }
            }

            function OnClientTransportClose(oWnd, args) {
                var combo = $find("<%= tbTransCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnTransAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdnTransportID.ClientID%>").value = arg.TransportID;
                        document.getElementById("<%=tbTransCode.ClientID%>").value = arg.TransportCD;
                        document.getElementById("<%=tbTransName.ClientID%>").value = arg.TransportationVendor;

                        if (arg.NegotiatedRate != undefined && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbTransRate.ClientID%>").value = arg.NegotiatedRate;
                        if (arg.TransportCD != null)
                            document.getElementById("<%=lbcvTransCode.ClientID%>").innerHTML = "";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        ajaxManager.ajaxRequest("tbTransCode_TextChanged");
                    }
                    else {
                        document.getElementById("<%=hdnTransAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdnTransportID.ClientID%>").value = "";
                        document.getElementById("<%=lbTransCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=tbTransCode.ClientID%>").value = "";
                        document.getElementById("<%=tbTransName.ClientID%>").value = "";
                        document.getElementById("<%=tbTransPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbTransFax.ClientID%>").value = "";
                        document.getElementById("<%=tbTransRate.ClientID%>").value = "";
                    }
                }
            }

            function OnClientArriveTransportClose(oWnd, args) {
                var combo = $find("<%= tbArriveTransCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnArriveTransAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdnArriveTransportID.ClientID%>").value = arg.TransportID;
                        document.getElementById("<%=tbArriveTransCode.ClientID%>").value = arg.TransportCD;
                        document.getElementById("<%=tbArriveTransName.ClientID%>").value = arg.TransportationVendor;

                        if (arg.NegotiatedRate != undefined && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbArriveRate.ClientID%>").value = arg.NegotiatedRate;
                        if (arg.TransportCD != null)
                            document.getElementById("<%=lbcvArriveTransCode.ClientID%>").innerHTML = "";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        ajaxManager.ajaxRequest("tbArriveTransCode_TextChanged");
                    }
                    else {
                        document.getElementById("<%=hdnArriveTransAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdnArriveTransportID.ClientID%>").value = "";
                        document.getElementById("<%=lbArriveTransCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=tbArriveTransCode.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveTransName.ClientID%>").value = "";
                        document.getElementById("<%=tbArrivePhone.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveFax.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveRate.ClientID%>").value = "";
                    }
                }
            }

            function maxLengthPaste(field, maxChars) {
                event.returnValue = false;
                if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
                    return false;
                }
                event.returnValue = true;
            }

            function checkRadNumericCapValue(sender, eventArgs) {

                var value = sender.get_value().toString();
                if (sender.get_textBoxValue().length > 0) {
                    if (value.indexOf('.') >= 0) {
                        if (value.indexOf('.') > 0) {
                            var strarr = value.split('.');
                            if (strarr[0].length > 7) {
                                alert("Invalid format, Required format is #######.## ");
                                sender.set_value('0.00');
                                sender.focus();
                                sender.selectAllText();

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is #######.## ");
                                sender.set_value('0.00');
                                sender.focus();
                                sender.selectAllText();

                            }
                        }
                    }
                    else {


                        if ((sender.get_textBoxValue().length > 7) && (value != "0")) {
                            alert("Invalid format, Required format is #######.## ");
                            sender.set_value('0.00');
                            sender.focus();
                            sender.selectAllText();


                        }
                    }
                }



            }
            function checkMaxcap(myfield) {
                var postbackval = true;
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") > 0) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length > 7) {
                                alert("Invalid format, Required format is #######.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is #######.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;
                            }
                        }
                    }
                    else {
                        if (myfield.value.length > 7) {
                            alert("Invalid format, Required format is #######.## ");
                            myfield.value = "0.00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }
                }

                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                ajaxManager.ajaxRequest("tbMaxcapamt_Text_Changed");
                return postbackval;
            }

            function checkTransRatenumericValue(myfield) {
                var postbackval = true;
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") > 0) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length > 3) {
                                alert("Invalid format, Required format is ###.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is ###.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;
                            }
                        }
                    }
                    else {
                        if (myfield.value.length > 3) {
                            alert("Invalid format, Required format is ###.## ");
                            myfield.value = "0.00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }
                }

                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                ajaxManager.ajaxRequest("tbArriveRate_Text_Changed");
                return postbackval;
            }





            function fnAllowNumericAndOneDecimal(fieldname, myfield, e) {
                var key;
                var keychar;
                var allowedString = "0123456789" + ".";

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);

                if (keychar == ".") {

                    if (myfield.value.indexOf(keychar) > -1) {
                        return false;
                    }
                }

                if (fieldname == "override") {
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(".") > myfield.value.length - 1)
                            return false;
                    }
                }
                // control keys
                if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    return true;
                // numeric values and allow slash("/")
                else if ((allowedString).indexOf(keychar) > -1)
                    return true;
                else
                    return false;
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">


            function checknumericValue(sender, eventArgs) {

                var value = sender.get_value().toString();
                if (sender.get_textBoxValue().length > 0) {
                    if (value.indexOf('.') >= 0) {
                        if (value.indexOf('.') > 0) {
                            var strarr = value.split('.');
                            if (strarr[0].length > 3) {
                                alert("Invalid format, Required format is ###.## ");
                                sender.set_value('000.00');
                                sender.focus();

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is ###.## ");
                                sender.set_value('000.00');
                                sender.focus();

                            }
                        }
                    }
                    else {


                        if ((sender.get_textBoxValue().length > 3) && (value != "0")) {


                            alert("Invalid format, Required format is ###.## ");
                            sender.set_value('000.00');
                            sender.focus();

                        }
                    }
                }



            }

            function handleDropDownEvents(ddlPAXPurpose) {

                //alert("test");
                //alert(ddlPAXPurpose.prevSelectedIndex);
            }
            function openWin(radWin) {
                var url = '';

                if (radWin == "radHotelPopup") {
                    var list = document.getElementById("<% =rbArriveDepart.ClientID %>"); //Cleint ID of RadioButtonList
                    var rdbtnLstValues = list.getElementsByTagName("input");
                    var Checkdvalue;
                    for (var i = 0; i < rdbtnLstValues.length; i++) {
                        if (rdbtnLstValues[i].checked) {
                            Checkdvalue = rdbtnLstValues[i];
                            break;
                        }
                    }
                    if (Checkdvalue.value == "0") {
                        url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdnArrivalICao.ClientID%>').value + '&HotelCD=' + document.getElementById('<%=tbHotelCode.ClientID%>').value;
                    }
                    else {
                        url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdnDepartIcao.ClientID%>').value + '&HotelCD=' + document.getElementById('<%=tbHotelCode.ClientID%>').value;
                    }

                }
                if (radWin == "rdTransportPopup") {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdnDepartIcao.ClientID%>').value;
                }
                if (radWin == "rdArriveTransportPopup") {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdnArrivalICao.ClientID%>').value;
                }

                if (radWin == "RadRetrievePopup") {
                    url = '/Views/Transactions/Preflight/PreflightSearch.aspx';
                }

                if (radWin == "rdChecklist") {
                    url = '../../Transactions/Preflight/PreflightChecklist.aspx';
                }

                if (radWin == "rdOutBoundInstructions") {
                    url = '../../Transactions/Preflight/PreflightOutboundInstruction.aspx';
                }

                if (radWin == "rdSIFL") {
                    url = '/Views/Transactions/Preflight/PreflightSIFL.aspx';
                }

                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/Preflight/CopyTrip.aspx';
                }

                if (radWin == "rdMoveTrip") {
                    url = '/Views/Transactions/Preflight/MoveTrip.aspx';
                }

                if (radWin == "rdPreflightAPIS") {
                    url = "/Views/Transactions/Preflight/PreflightAPIS.aspx?APIS=" + "APIS";
                }

                if (radWin == "rdHistory") {
                    url = "../../Transactions/History.aspx?FromPage=" + "Preflight";
                }


                // PROD-38
                if (radWin == "rdPreflightEMAIL") {
                    url = '/Views/Transactions/Preflight/EmailTrip.aspx';
                }
                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
                if (radWin == "rdTravelSense") {

                    var left = (screen.width / 2) - (2 / 2);
                    var top = (screen.height / 2) - (2 / 2);
                    var test = window.open("/Views/Transactions/Preflight/PreflightTravelSense.aspx", "Travelsense", "width=2px,height=2px,top=" + top + ", left=" + left);
                }
            }



            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }
            // this function is used to display the PaxInfo popup 
            function ShowPaxInfoPopup(ParameterCall) {
                if (ParameterCall == 'insert') {
                    document.getElementById('<%=hdnInsert.ClientID%>').value = "true";
                }
                else
                    document.getElementById('<%=hdnInsert.ClientID%>').value = "false";
                var oWnd = radopen("/Views/Transactions/Preflight/PaxInfoPopup.aspx", "radPaxInfoPopup"); // Added for Preflight Pax Tab
                return false;
            }

            // this function is used to display the details of a particular PAX
            function ShowPaxDetailsPopup(Paxid) {
                window.radopen("/Views/Transactions/Preflight/PreflightPassengerPopup.aspx?PaxID=" + Paxid, "rdPaxPage");
                return false;
            }

            // this function is used to display the Pax passport visa type popup 
            function ShowPaxPassportPopup(Paxid, LegID, PaxName) {
                //window.radopen("/Views/Transactions/Preflight/PaxPassportVisa.aspx?PassengerRequestorID=" + Paxid, "rdPaxPassport");
                window.radopen("/Views/Transactions/Preflight/PaxPassportVisa.aspx?PassengerRequestorID=" + Paxid + "&PaxLeg=" + LegID + "&PaxName=" + PaxName, "radPaxInfoPopup");
                document.getElementById("<%=hdnPassPax.ClientID%>").value = Paxid;
                document.getElementById('<%=hdnAssignPassLeg.ClientID%>').value = LegID;
                return false;
            }



            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnTextFocus(txtPax) {
                document.getElementById("hdnPAXPurposeOnFocus").value = txtPax.value;

            }

            function CalculateLegCount(obj, LegNum, objectType) {

                var PanelBar2 = $find("<%= pnlPaxSummary.ClientID %>");
                PanelBar2.get_items().getItem(0).set_expanded(false);
                if (objectType == "dropdown") {
                    var previousVal = $(obj).attr('previous');
                    var newVal = obj.value;
                    $(obj).attr('previous', newVal);
                    //alert(previousVal);
                    //alert(document.getElementById("hdleg" + LegNum + "PaxCount").value + " - " + document.getElementById("hdleg" + LegNum + "Avail").value + " - " + document.getElementById("tbLeg" + LegNum).value);
                    if ((previousVal == "0" || previousVal == "") && newVal != "0") {
                        document.getElementById("hdleg" + LegNum + "PaxCount").value = Number(document.getElementById("hdleg" + LegNum + "PaxCount").value) + 1;

                    }
                    else if (!(previousVal == "0" || previousVal == "") && newVal == "0") {
                        document.getElementById("hdleg" + LegNum + "PaxCount").value = Number(document.getElementById("hdleg" + LegNum + "PaxCount").value) - 1;
                    }
                }
                document.getElementById("hdleg" + LegNum + "Avail").value = Number(document.getElementById("hdFleetCount").value) - (Number(document.getElementById("hdleg" + LegNum + "PaxCount").value) + Number(document.getElementById("tbLeg" + LegNum).value));

                var IdPAXCount = "lblLeg" + LegNum + "p" + LegNum;
                var IdAvailableCount = "lblLeg" + LegNum + "a" + LegNum;

                //alert(document.getElementById("hdleg" + LegNum + "PaxCount").value + " - " + document.getElementById("hdleg" + LegNum + "Avail").value + " - " + document.getElementById("tbLeg" + LegNum).value); 

                document.getElementById(IdPAXCount).innerHTML = Number(document.getElementById("hdleg" + LegNum + "PaxCount").value) + Number(document.getElementById("tbLeg" + LegNum).value);
                document.getElementById(IdAvailableCount).innerHTML = document.getElementById("hdleg" + LegNum + "Avail").value;

            }

            function OnTextChanged(txtPax, legnum) {

                var IdPAXCount = "lblLeg" + legnum + "p" + legnum;
                var IdAvailableCount = "lblLeg" + legnum + "a" + legnum;
                //var IdBlokedCount = "tbLeg" + legnum;

                var PAXCount = document.getElementById(IdPAXCount).innerHTML;
                var AvailableCount = document.getElementById(IdAvailableCount).innerHTML;

                //var BlokedCount = document.getElementById(IdBlokedCount).value;
                var hdnCount = document.getElementById("hdnLeg" + legnum).value;

                if (!isNaN(txtPax.value)) {
                    document.getElementById(IdPAXCount).innerHTML = Number(PAXCount) + Number(txtPax.value) - Number(hdnCount);
                    document.getElementById(IdAvailableCount).innerHTML = Number(AvailableCount) - Number(txtPax.value) + Number(hdnCount);
                    document.getElementById("hdnLeg" + legnum).value = txtPax.value;
                }

                if (legnum == 1) {
                    document.getElementById('<%=hdnLeg1p1.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg1a1.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 2) {
                    document.getElementById('<%=hdnLeg2p2.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg2a2.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 3) {
                    document.getElementById('<%=hdnLeg3p3.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg3a3.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 4) {
                    document.getElementById('<%=hdnLeg4p4.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg4a4.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 5) {
                    document.getElementById('<%=hdnLeg5p5.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg5a5.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 6) {
                    document.getElementById('<%=hdnLeg6p6.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg6a6.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 7) {
                    document.getElementById('<%=hdnLeg7p7.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg7a7.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 8) {
                    document.getElementById('<%=hdnLeg8p8.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg8a8.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 9) {
                    document.getElementById('<%=hdnLeg9p9.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg9a9.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 10) {
                    document.getElementById('<%=hdnLeg10p10.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg10a10.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 11) {
                    document.getElementById('<%=hdnLeg11p11.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg11a11.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 12) {
                    document.getElementById('<%=hdnLeg12p12.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg12a12.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 13) {
                    document.getElementById('<%=hdnLeg13p13.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg13a13.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 14) {
                    document.getElementById('<%=hdnLeg14p14.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg14a14.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 15) {
                    document.getElementById('<%=hdnLeg15p15.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg15a15.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 16) {
                    document.getElementById('<%=hdnLeg16p16.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg16a16.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 17) {
                    document.getElementById('<%=hdnLeg17p17.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg17a17.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 18) {
                    document.getElementById('<%=hdnLeg18p18.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg18a18.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 19) {
                    document.getElementById('<%=hdnLeg19p19.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg19a19.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 20) {
                    document.getElementById('<%=hdnLeg20p20.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg20a20.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 21) {
                    document.getElementById('<%=hdnLeg21p21.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg21a21.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 22) {
                    document.getElementById('<%=hdnLeg22p22.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg22a22.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 23) {
                    document.getElementById('<%=hdnLeg23p23.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg23a23.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 24) {
                    document.getElementById('<%=hdnLeg24p24.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg24a24.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (legnum == 25) {
                    document.getElementById('<%=hdnLeg25p25.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg25a25.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

            }



            function OnGridDropDownFocus(ddlPAX) {

                //document.getElementById("hdnPAXPurposeOnFocus").value = ddlPAX.value;
                // alert(document.getElementById("hdnPAXPurposeOnFocus").value);
                //ddlPAX.focus;

                //alert(document.getElementById("hdnPAXPurposeOnFocus").value)
            }

            function OnDropDownSelectedIndexChanged(ddlPax, LegNum, Code) {
                var newPAXPurpose = ddlPax.value;



                var hdnOldPurposeID = "hdnOldPurpose_" + LegNum + "_" + Code;

                //alert(1);
                //alert(document.getElementById("div_" + LegNum + "_" + Code).getElementsByTagName("input").item(0).value);

                //var oldPAXPurpose = document.getElementById(hdnOldPurposeID).value;
                //var oldPAXPurpose = document.getElementById("div_" + LegNum + "_" + Code).children("hdnOldPurpose_" + LegNum).value

                var oldPAXPurpose = document.getElementById("div_" + LegNum + "_" + Code).getElementsByTagName("input").item(0).value



                //alert(document.getElementById("div_" + LegNum + "_" + Code).children("hdnOldPurpose_" + LegNum).value);

                var IdPAXCount = "lblLeg" + LegNum + "p" + LegNum;
                var IdAvailableCount = "lblLeg" + LegNum + "a" + LegNum;

                //                var hdnLegMaxPax = "hdnLeg" + LegNum + "p" + LegNum;
                //                var hdnLegMaxAvail = "hdnLeg" + LegNum + "a" + LegNum;

                var PAXCount = document.getElementById(IdPAXCount).innerHTML;
                var AvailableCount = document.getElementById(IdAvailableCount).innerHTML;

                if (newPAXPurpose != 0 && oldPAXPurpose == 0) {
                    document.getElementById(IdPAXCount).innerHTML = Number(PAXCount) + 1;
                    document.getElementById(IdAvailableCount).innerHTML = Number(AvailableCount) - 1;
                }
                else if (newPAXPurpose == 0 && oldPAXPurpose != 0) //&& PAXCount !=0
                //else
                {
                    document.getElementById(IdPAXCount).innerHTML = Number(PAXCount) - 1;
                    document.getElementById(IdAvailableCount).innerHTML = Number(AvailableCount) + 1;
                }

                //document.getElementById("div_" + LegNum + "_" + Code).children("hdnOldPurpose_" + LegNum).value = newPAXPurpose;
                document.getElementById("div_" + LegNum + "_" + Code).getElementsByTagName("input").item(0).value = newPAXPurpose;

                if (LegNum == 1) {
                    document.getElementById('<%=hdnLeg1p1.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg1a1.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 2) {
                    document.getElementById('<%=hdnLeg2p2.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg2a2.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 3) {
                    document.getElementById('<%=hdnLeg3p3.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg3a3.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 4) {
                    document.getElementById('<%=hdnLeg4p4.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg4a4.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 5) {
                    document.getElementById('<%=hdnLeg5p5.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg5a5.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 6) {
                    document.getElementById('<%=hdnLeg6p6.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg6a6.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 7) {
                    document.getElementById('<%=hdnLeg7p7.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg7a7.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 8) {
                    document.getElementById('<%=hdnLeg8p8.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg8a8.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 9) {
                    document.getElementById('<%=hdnLeg9p9.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg9a9.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 10) {
                    document.getElementById('<%=hdnLeg10p10.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg10a10.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 11) {
                    document.getElementById('<%=hdnLeg11p11.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg11a11.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 12) {
                    document.getElementById('<%=hdnLeg12p12.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg12a12.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 13) {
                    document.getElementById('<%=hdnLeg13p13.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg13a13.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 14) {
                    document.getElementById('<%=hdnLeg14p14.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg14a14.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 15) {
                    document.getElementById('<%=hdnLeg15p15.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg15a15.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 16) {
                    document.getElementById('<%=hdnLeg16p16.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg16a16.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 17) {
                    document.getElementById('<%=hdnLeg17p17.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg17a17.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 18) {
                    document.getElementById('<%=hdnLeg18p18.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg18a18.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 19) {
                    document.getElementById('<%=hdnLeg19p19.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg19a19.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 20) {
                    document.getElementById('<%=hdnLeg20p20.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg20a20.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }
                if (LegNum == 21) {
                    document.getElementById('<%=hdnLeg21p21.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg21a21.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 22) {
                    document.getElementById('<%=hdnLeg22p22.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg22a22.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 23) {
                    document.getElementById('<%=hdnLeg23p23.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg23a23.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 24) {
                    document.getElementById('<%=hdnLeg24p24.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg24a24.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

                if (LegNum == 25) {
                    document.getElementById('<%=hdnLeg25p25.ClientID %>').value = document.getElementById(IdPAXCount).innerHTML;
                    document.getElementById('<%=hdnLeg25a25.ClientID %>').value = document.getElementById(IdAvailableCount).innerHTML;
                }

            }


            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, "");
            }

            function allownumbers(e) {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                var reg = new RegExp("[0-9.]")
                if (key == 8) {
                    keychar = String.fromCharCode(key);
                }
                if (key == 13) {
                    key = 8;
                    keychar = String.fromCharCode(key);
                }
                return reg.test(keychar);
            }
            function ShowAirportInfoPopup(Airportid) {
                window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
                return false;
            }

            var isPaxSummaryExpand = false;
            var isPaxLogisticsExpand = false;

            function pnlPaxSummaryIsExpand() {
                isPaxSummaryExpand = true;
            }
            function pnlPaxSummaryIsCollapse() {
                isPaxSummaryExpand = false;
            }
            function pnlPaxLogisticsIsExpand() {
                isPaxLogisticsExpand = true;
            }
            function pnlPaxLogisticsIsCollapse() {
                isPaxLogisticsExpand = false;
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }

            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">

            function refreshGrid(arg) {
                if (!arg) {
                    $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("Rebind");
                }
                else {
                    if (document.getElementById('<%=hdnInsert.ClientID%>').value == "true")
                        $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("RebindAndNavigateInsert");
                    else
                        $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("RebindAndNavigate");
                }
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMetroPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientMetroClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/MetroMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCtryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCtryClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx" OnClientClose="OnClientTransportClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTransportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTransportClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdArriveTransportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientArriveTransportClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdOutBoundInstructions" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                Title="Outbound Instructions" NavigateUrl="~/Views/Transactions/Preflight/PreflightOutboundInstruction.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdChecklist" runat="server" VisibleOnPageLoad="false" AutoSize="true"
                Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close" Title="Checklist"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightChecklist.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="990px" Height="590px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPreflightAPIS" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="APIS" NavigateUrl="~/Views/Transactions/Preflight/PreflightAPIS.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPaxPage" runat="server" OnClientResizeEnd="GetDimensions"
                Width="830px" Height="550px" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false"
                Behaviors="Close" VisibleStatusbar="false" Title="Passenger/Requestor">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTravelSense" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="TravelSense" NavigateUrl="~/Views/Transactions/Preflight/PreflightTravelSense.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:HiddenField ID="hdFleetCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg1PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg2PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg3PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg4PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg5PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg6PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg7PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg8PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg9PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg10PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg11PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg12PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg13PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg14PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg15PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg16PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg17PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg18PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg19PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg20PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg21PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg22PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg23PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg24PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg25PaxCount" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg1Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg2Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg3Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg4Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg5Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg6Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg7Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg8Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg9Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg10Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg11Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg12Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg13Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg14Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg15Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg16Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg17Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg18Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg19Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg20Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg21Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg22Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg23Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg24Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdleg25Avail" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCtryID" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnMetroID" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnAssignPassLeg" runat="server"></asp:HiddenField>
        <input type="hidden" id="hdnPAXPurposeOnFocus" value="0" />
        <asp:HiddenField ID="hdnPaxNotes" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnLeg1" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnPassPax" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnStreet" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnCity" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnPostal" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnState" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnHotelID" runat="server" />
        <asp:HiddenField ID="hdnHotelAirID" runat="server" />
        <asp:HiddenField ID="hdnTransportID" runat="server" />
        <asp:HiddenField ID="hdnTransAirID" runat="server" />
        <asp:HiddenField ID="hdnArriveTransportID" runat="server" />
        <asp:HiddenField ID="hdnArriveTransAirID" runat="server" />
        <asp:HiddenField ID="hdnArrivalICao" runat="server" />
        <asp:HiddenField ID="hdnDepartIcao" runat="server" />
        <asp:HiddenField ID="hdnLeg1p1" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnLeg1a1" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnLeg2p2" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnLeg2a2" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg3p3" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg3a3" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg4p4" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg4a4" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg5p5" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg5a5" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg6p6" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg6a6" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg7p7" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg7a7" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg8p8" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg8a8" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg9p9" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg9a9" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg10p10" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg10a10" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg11p11" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg11a11" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg12p12" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg12a12" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg13p13" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg13a13" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg14p14" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg14a14" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg15p15" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg15a15" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg16p16" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg16a16" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg17p17" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg17a17" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg18p18" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg18a18" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg19p19" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg19a19" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg20p20" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg20a20" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg21p21" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg21a21" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg22p22" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg22a22" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg23p23" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg23a23" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg24p24" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg24a24" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg25p25" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnLeg25a25" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnPaxSelectedTab" runat="server" />
        <asp:HiddenField ID="hdnPaxLogisticsSelectedTab" runat="server" />
        <telerik:RadTabStrip OnTabClick="rtsInfoLogistic_OnTabClick" Skin="Windows7" AutoPostBack="true"
            Width="718px" ID="rtsInfoLogistic" runat="server" ReorderTabsOnSelect="true"
            CausesValidation="false" SelectedIndex="0">
            <Tabs>
                <telerik:RadTab Text="PAX Info" Value="PaxInfo">
                </telerik:RadTab>
                <telerik:RadTab Text="PAX Logistics" Value="PaxLogistics">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <asp:Panel ID="pnlPax" runat="server" Visible="true">
            <div runat="server" id="ChecklistOutboundTable" style="padding-top: 8px; padding-bottom: 5px">
                <asp:LinkButton CssClass="link_small" ID="lnkbtnChecklist" Text="Checklist" runat="server"
                    OnClientClick="javascript:openWin('rdChecklist');return false;"></asp:LinkButton>
                |
                <asp:LinkButton CssClass="link_small" ID="lnkbtnOutbound" Text="Outbound Instructions"
                    runat="server" OnClientClick="javascript:openWin('rdOutBoundInstructions');return false;"></asp:LinkButton>
            </div>
            <div style="float: left; text-align: left; width: 718px; padding: 5px 0 0 0px;">
                <table width="200px" cellpadding="0" cellspacing="0" class="nav_bg_preflight">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" CssClass="SearchBox_sc_crew" AutoPostBack="true" OnTextChanged="Search_Click"
                                ID="SearchBox"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button runat="server" CssClass="searchsubmitbutton" CausesValidation="false"
                                OnClick="Search_Click" ID="Submit1" />
                        </td>
                        <td class="tdLabel520">
                            <asp:LinkButton ID="lnkPaxRoaster" runat="server" CausesValidation="false" OnClientClick="javascript:return ShowPaxInfoPopup('all');"
                                Text="PAX Table" CssClass="link_small"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="nav-3" colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            (Search by Passenger Code)
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float: left; width: 718px; padding: 5px 0 0 0px;">
                <fieldset>
                    <legend>PAX</legend>
                    <div style="text-align: right; padding: 5px 0 0 0px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdLabel140">
                                    TSA Verification No.
                                </td>
                                <td class="tdLabel180">
                                    <asp:TextBox ID="tbTsaVerfiNum" runat="server" MaxLength="40" CssClass="text150"></asp:TextBox>
                                </td>
                                <td class="tdLabel50">
                                    Status
                                </td>
                                <td class="tdLabel180">
                                    <asp:TextBox ID="tbStaus" CssClass="tdLabel160" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td class="tdLabel60">
                                    <!--All Legs-->
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlFlightPurpose" runat="server" AppendDataBoundItems="true"
                                        AutoPostBack="true" OnSelectedIndexChanged="FlightPurpose_SelectedIndexChanged">
                                        <asp:ListItem Selected="True" Text="Select" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="tdLabel87" align="right">
                                    <asp:Button ID="btnAddremoveColumns" runat="server" Visible="false" CssClass="button"
                                        Text="+/- Columns" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float: left; width: 700px; padding: 5px 0 0 0px;">
                        <telerik:RadGrid ID="dgAvailablePax" runat="server" OnNeedDataSource="dgAvailablePax_BindData"
                            EnableAJAX="True" AutoGenerateColumns="false" OnItemCommand="dgAvailablePax_ItemCommand"
                            OnItemDataBound="dgAvailablePax_ItemDataBound" Width="680px" Height="250px" AllowMultiRowSelection="true"
                            HeaderStyle-HorizontalAlign="Center" AllowPaging="false">
                            <MasterTableView DataKeyNames="OrderNUM,Code,PassengerRequestorID" CommandItemDisplay="None"
                                AllowFilteringByColumn="false" ShowFooter="true" ItemStyle-HorizontalAlign="Left"
                                AllowPaging="false" AllowSorting="false">
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="OrderNUM" SortOrder="Ascending" />
                                </SortExpressions>
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="PassengerRequestorID" SortExpression="PassengerRequestorID"
                                        HeaderText="PassengerRequestorID" DataField="PassengerRequestorID" Display="false" />
                                    <telerik:GridTemplateColumn HeaderText="Code" CurrentFilterFunction="StartsWith"
                                        ShowFilterIcon="false" UniqueName="Code" AllowFiltering="false" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPaxCode" runat="server" Text='<%# Eval("Code") %>' CssClass="tdtext100"
                                                CausesValidation="false" OnClientClick='<%#Eval("PassengerRequestorID", "return ShowPaxDetailsPopup(\"{0}\")")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblBlockedSeats" runat="server" Text="Blocked Seats"></asp:Label>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn UniqueName="Name" HeaderText="Name" DataField="Name" HeaderStyle-Width="175px" />
                                    <%-- <telerik:GridBoundColumn UniqueName="Status" SortExpression="Status" HeaderText="Status"
                                                                                            DataField="Status" HeaderStyle-Width="50px" Display="false" />--%>
                                    <telerik:GridTemplateColumn HeaderText="Leg1" UniqueName="Leg1" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg1" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg1PaxHeader" class="small_text" runat="server" ClientIDMode="Static"
                                                            Text="Total PAX booked:"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg1p1" ClientIDMode="Static" runat="server" class="small_text"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg1AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg1a1" ClientIDMode="Static" runat="server" class="small_text"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg1:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo1" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg1Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg1Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_1_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg1" runat="server" onchange="javascript:CalculateLegCount(this,1,'dropdown');"
                                                    CausesValidation="false">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_1" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg1" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,1,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg1" runat="server"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg2" UniqueName="Leg2" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg2" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg2PaxHeader" ClientIDMode="Static" runat="server" Text="Total PAX booked:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg2p2" runat="server" ClientIDMode="Static" class="small_text"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg2AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg2a2" runat="server" ClientIDMode="Static" class="small_text"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg2:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo2" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg2Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg2Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_2_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg2" onchange="javascript:CalculateLegCount(this,2,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_2" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg2" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,2,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg2" runat="server"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg3" UniqueName="Leg3" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg3" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg3PaxHeader" ClientIDMode="Static" runat="server" Text="Total PAX booked:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg3p3" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg3AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg3a3" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg3:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo3" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg3Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg3Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_3_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg3" onchange="javascript:CalculateLegCount(this,3,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_3" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg3" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,3,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg3" runat="server"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg4" UniqueName="Leg4" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg4" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg4PaxHeader" ClientIDMode="Static" runat="server" Text="Total PAX booked:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg4p4" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg4AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg4a4" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg4:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo4" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg4Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg4Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_4_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg4" onchange="javascript:CalculateLegCount(this,4,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_4" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg4" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,4,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg4" runat="server"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg5" UniqueName="Leg5" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg5" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg5PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg5p5" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg5AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg5a5" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg5:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo5" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg5Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg5Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_5_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg5" onchange="javascript:CalculateLegCount(this,5,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_5" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg5" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,5,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg5" runat="server"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg6" UniqueName="Leg6" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg6" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg6PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg6p6" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg6AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg6a6" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg6:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo6" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg6Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg6Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_6_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg6" onchange="javascript:CalculateLegCount(this,6,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_6" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg6" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,6,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg6" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg7" UniqueName="Leg7" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg7" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg7PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg7p7" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg7AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg7a7" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg7:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo7" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg7Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg7Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_7_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg7" onchange="javascript:CalculateLegCount(this,7,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_7" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg7" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,7,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg7" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg8" UniqueName="Leg8" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg8" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg8PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg8p8" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg8AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg8a8" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg8:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo8" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg8Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg8Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_8_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg8" onchange="javascript:CalculateLegCount(this,8,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_8" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg8" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,8,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg8" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg9" UniqueName="Leg9" Display="false" HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg9" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg9PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg9p9" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg9AvailableHeader" runat="server" Text="Total Seats Avail:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg9a9" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg9:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo9" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg9Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg9Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_9_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg9" onchange="javascript:CalculateLegCount(this,9,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_9" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg9" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,9,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg9" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg10" UniqueName="Leg10" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg10" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg10PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg10P10" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg10AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg10a10" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg10:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo10" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg10Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg10Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_10_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg10" onchange="javascript:CalculateLegCount(this,10,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_10" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg10" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,10,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg10" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg11" UniqueName="Leg11" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg11" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg11PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg11p11" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg11AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg11a11" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg11:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo11" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg11Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg11Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_11_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg11" onchange="javascript:CalculateLegCount(this,11,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_11" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg11" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,11,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg11" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg12" UniqueName="Leg12" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg12" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg12PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg12p12" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg12AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg12a12" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg12:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo12" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg12Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg12Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_12_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg12" onchange="javascript:CalculateLegCount(this,12,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_12" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg12" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,12,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg12" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg13" UniqueName="Leg13" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg13" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg13PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg13p13" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg13AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg13a13" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg13:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo13" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg13Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg13Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_13_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg13" onchange="javascript:CalculateLegCount(this,13,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_13" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg13" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,13,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg13" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg14" UniqueName="Leg14" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg14" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg14PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg14p14" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg14AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg14a14" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg14:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo14" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg14Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg14Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_14_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg14" onchange="javascript:CalculateLegCount(this,14,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_14" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg14" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,14,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg14" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg15" UniqueName="Leg15" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg15" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg15PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg15p15" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg15AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg15a15" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg15:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo15" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg15Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg15Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_15_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg15" onchange="javascript:CalculateLegCount(this,15,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_15" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg15" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,15,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg15" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg16" UniqueName="Leg16" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg16" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg16PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg16p16" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg16AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg16a16" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg16:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo16" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg16Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg16Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_16_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg16" onchange="javascript:CalculateLegCount(this,16,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_16" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg16" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,16,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg16" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg17" UniqueName="Leg17" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg17" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg17PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg17p17" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg17AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg17a17" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg17:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo17" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg17Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg17Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_17_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg17" onchange="javascript:CalculateLegCount(this,17,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_17" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg17" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,17,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg17" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg18" UniqueName="Leg18" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg18" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg18PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg18p18" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg18AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg18a18" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg18:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo18" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg18Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg18Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_18_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg18" onchange="javascript:CalculateLegCount(this,18,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_18" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg18" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,18,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg18" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Le19" UniqueName="Leg19" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg19" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg19PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg19p19" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg19AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg19a19" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg19:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo19" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg19Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg19Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_19_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg19" onchange="javascript:CalculateLegCount(this,19,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_19" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg19" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,19,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg19" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg20" UniqueName="Leg20" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg20" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg20PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg20p20" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg20AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg20a20" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg20:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo20" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg20Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg20Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_20_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg20" onchange="javascript:CalculateLegCount(this,20,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_20" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg20" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,20,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg20" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg21" UniqueName="Leg21" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg21" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg21PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg21p21" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg21AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg21a21" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg21:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo21" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg21Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg21Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_21_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg21" onchange="javascript:CalculateLegCount(this,21,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_21" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg21" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,21,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg21" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg22" UniqueName="Leg22" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg22" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg22PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg22p22" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg22AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg22a22" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg22:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo22" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg22Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg22Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_22_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg22" onchange="javascript:CalculateLegCount(this,22,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_22" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg22" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,22,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg22" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg23" UniqueName="Leg23" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg23" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg23PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg23p23" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg23AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg23a23" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg23:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo23" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg23Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg23Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_23_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg23" onchange="javascript:CalculateLegCount(this,23,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_23" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg23" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,23,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg23" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg24" UniqueName="Leg24" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg24" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg24PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg24p24" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg24AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg24a24" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg24:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo24" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg24Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg24Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_24_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg24" onchange="javascript:CalculateLegCount(this,24,'dropdown');"
                                                    runat="server">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_24" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg24" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,24,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg24" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Leg25" UniqueName="Leg25" Display="false"
                                        HeaderStyle-Width="150px">
                                        <HeaderTemplate>
                                            <table id="tblLeg25" cellpadding="1" cellspacing="1" width="220px">
                                                <tr>
                                                    <td class="tdLabel120" align="left">
                                                        <asp:Label ID="lblLeg25PaxHeader" runat="server" Text="Total PAX booked:" class="small_text"></asp:Label>
                                                    </td>
                                                    <td class="tdLabel100" align="left">
                                                        <asp:Label ID="lblLeg25p25" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg25AvailableHeader" runat="server" Text="Total Seats Avail:"
                                                            class="small_text"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblLeg25a25" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" class="small_text tdLabel52">
                                                                    Leg25:
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLeginfo25" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnLeg25Pax" runat="server" />
                                                                    <asp:HiddenField ID="hdnLeg25Avail" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="text-align: center" id='<%# "div_25_" + Eval("PassengerRequestorID") %>'>
                                                <asp:DropDownList ID="ddlLeg25" onchange="javascript:CalculateLegCount(this,25,'dropdown');"
                                                    runat="server" CausesValidation="false">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnOldPurpose_25" runat="server" ClientIDMode="Static" Value="0" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="tbLeg25" MaxLength="3" CssClass="tdLabel60" runat="server" ClientIDMode="Static"
                                                onchange="javascript:CalculateLegCount(this,25,'blocked');" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                            <asp:HiddenField ID="hdnLeg25" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn UniqueName="Notes" SortExpression="Notes" HeaderText="Notes"
                                        DataField="Notes" Display="false" />
                                    <telerik:GridBoundColumn UniqueName="PassengerAlert" SortExpression="PassengerAlert"
                                        HeaderText="PAX Alert" DataField="PassengerAlert" Display="false" />
                                    <telerik:GridBoundColumn UniqueName="OrderNUM" HeaderText="OrderNUM" Display="false"
                                        DataField="OrderNUM" HeaderStyle-Width="175px" />
                                    <telerik:GridBoundColumn UniqueName="BillingCode" HeaderText="Billing Info" Display="false"
                                        DataField="BillingCode" HeaderStyle-Width="175px" />
                                </Columns>
                                <CommandItemTemplate>
                                    <div class="grid_icon">
                                        <asp:LinkButton ID="lnkPaxDelete" runat="server" CommandName="DeleteSelected" CssClass="delete-icon-grid"
                                            ToolTip="Delete" OnClick="lnkPaxDelete_OnClick"></asp:LinkButton>
                                        <a href="#" onclick="ShowPaxInfoPopup('insert')" id="aInsertCrew" title="Insert PAX"
                                            runat="server" class="insert-icon-grid"></a>
                                    </div>
                                    <div id="divMultiSelectAvailableCrew" class="multiselect" runat="server" clientidmode="Static">
                                        Use CTRL key to multi select
                                    </div>
                                </CommandItemTemplate>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="false" AllowRowsDragDrop="false" AllowColumnsReorder="false"
                                ReorderColumnsOnClient="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" FrozenColumnsCount="3" />
                                <ClientEvents />
                                <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </div>
                    <div style="float: left; width: 700px; padding: 5px 0 0 0px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdLabel600">
                                </td>
                                <td class="tdtext100" align="left">
                                    <asp:HiddenField ID="hdnTSA" runat="server" />
                                    <asp:Button ID="btnCheckTsa" runat="server" Text="Check TSA" OnClick="btnCheckTsa_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </div>
            <div style="text-align: center; float: left; width: 718px; padding: 5px 0  5px 0px;">
            </div>
            <telerik:RadPanelBar ID="pnlPaxSummary" Width="100%" ExpandAnimation-Type="None"
                CollapseAnimation-Type="None" runat="server" OnItemClick="pnlPaxSummary_Click"
                OnClientItemExpand="pnlPaxSummaryIsExpand" OnClientItemCollapse="pnlPaxSummaryIsCollapse">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="false" Text="PAX Summary">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <telerik:RadGrid ID="dgPaxSummary" runat="server" GridLines="None" AutoGenerateColumns="false"
                                        AllowSorting="true" AllowPaging="false" ShowStatusBar="true" OnItemDataBound="dgPaxSummary_ItemDataBound"
                                        Width="698px" Height="250px" OnItemCommand="dgPaxSummary_ItemCommand" OnSelectedIndexChanged="dgPaxSummary_OnSelectedIndexChanged"
                                        Skin="Office2010Silver" HorizontalAlign="Left" AllowMultiRowSelection="false">
                                        <SortingSettings SortedBackColor="" />
                                        <MasterTableView DataKeyNames="OrderNUM,PaxCode,PaxName,PaxID,LegID" CommandItemDisplay="None"
                                            AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left" AllowPaging="false">
                                            <SortExpressions>
                                                <telerik:GridSortExpression FieldName="PaxCode" SortOrder="Ascending" />
                                                <telerik:GridSortExpression FieldName="LegID" SortOrder="Ascending" />
                                            </SortExpressions>
                                            <Columns>
                                                <telerik:GridTemplateColumn UniqueName="PaxID" SortExpression="PassengerRequestorID"
                                                    HeaderText="PassengerRequestorID" Display="false" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnPassengerRequestorID" runat="server" Value='<%# Bind("PaxID") %>' />
                                                        <asp:Label ID="lblPassengerRequestorID" runat="server" Text='<%# Bind("PaxID") %>'
                                                            Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="PaxCode" HeaderText="Code" HeaderStyle-Width="60px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("PaxCode") %>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="PaxName" HeaderText="Name" HeaderStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("PaxName") %>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="LegID" HeaderText="Leg No." HeaderStyle-Width="60px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLegID" runat="server" Text='<%# Bind("LegID") %>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="Street" HeaderText="Street" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStreet" runat="server" Text='<%# Bind("Street") %>' Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="Postal" HeaderText="Postal" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPostal" runat="server" Text='<%# Bind("Postal") %>' Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="City" HeaderText="City" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("City") %>' Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="State" HeaderText="State" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtState" runat="server" Text='<%# Bind("State") %>' Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="Purpose" HeaderText="Purpose" HeaderStyle-Width="100px"
                                                    Display="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbPurpose" runat="server" Text='<%# Bind("Purpose") %>' Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="Passport" HeaderText="Passport" Display="true"
                                                    HeaderStyle-Width="140px">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnVisaID" runat="server" Value='<%# Eval("VisaID") %>' />
                                                        <asp:HiddenField ID="hdnPassID" runat="server" Value='<%# Eval("PassportID") %>' />
                                                        <asp:HiddenField ID="hdnPaxName" runat="server" Value='<%# Eval("PaxName") %>' />
                                                        <asp:TextBox ID="tbPassport" runat="server" Text='<%# Bind("Passport") %>' CssClass="tdLabel80"
                                                            Enabled="false" />
                                                        <%--<asp:LinkButton ID="lnkPassport1" runat="server" CssClass="browse-button" Width="16px"
                                                                                                                        Height="16px" CausesValidation="false" OnClientClick='<%#Eval("PaxID", "return ShowPaxPassportPopup(\"{0}\")")%>'></asp:LinkButton>--%>
                                                        <asp:LinkButton ID="lnkPassport" runat="server" CssClass="browse-button" Width="16px"
                                                            Height="16px" CausesValidation="false" OnClientClick='<%#String.Format("return ShowPaxPassportPopup(\"{0}\",\"{1}\",\"{2}\")",Eval("PaxID"),Eval("LegID"),Eval("PaxName")) %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn UniqueName="BillingCode" HeaderText="Billing Info" Display="true"
                                                    HeaderStyle-Width="140px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbBillingCode" runat="server" Text='<%# Bind("BillingCode") %>'
                                                            Width="95%" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn UniqueName="Nation" HeaderText="Nationality" DataField="Nation"
                                                    AllowSorting="false" AllowFiltering="false" HeaderStyle-Width="80px" />
                                                <telerik:GridBoundColumn UniqueName="PassportExpiryDT" HeaderText="Expiration Date"
                                                    DataField="PassportExpiryDT" AllowSorting="false" AllowFiltering="false" HeaderStyle-Width="100px" />
                                                <telerik:GridTemplateColumn HeaderText="Choice" UniqueName="Choice1" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="ChkChoice" Enabled="false" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--<telerik:GridBoundColumn UniqueName="Choice" DataField="Choice" HeaderText="Choice"
                                                                                                                ShowFilterIcon="false" AllowSorting="false" AllowFiltering="false" HeaderStyle-Width="50px" />--%>
                                                <telerik:GridBoundColumn UniqueName="IssueDT" HeaderText="Issue Date" DataField="IssueDT"
                                                    AllowSorting="false" AllowFiltering="false" HeaderStyle-Width="100px" />
                                            </Columns>
                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        </MasterTableView>
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <ClientEvents />
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <GroupingSettings CaseSensitive="false" />
                                    </telerik:RadGrid>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
        </asp:Panel>
        <asp:Panel ID="pnlPaxLogistics" runat="server" Visible="false">
            <telerik:RadTabStrip ID="rtsLogistics" Skin="Simple" runat="server" ReorderTabsOnSelect="true"
                MultiPageID="RadMultiPage1" Align="Justify" Width="200px" AutoPostBack="true"
                OnTabClick="rtsLogistics_TabClick">
                <Tabs>
                    <telerik:RadTab Text="Hotel">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Transportation">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                <telerik:RadPageView ID="rdPgeViewHotel" runat="server">
                    <telerik:RadPanelItem>
                        <ContentTemplate>
                            <telerik:RadTabStrip ID="rtsLegHotel" Skin="Windows7" AutoPostBack="true" runat="server"
                                OnTabClick="rtsLegHotel_TabClick" ReorderTabsOnSelect="true">
                                <Tabs>
                                    <telerik:RadTab Text="Leg1" Selected="true">
                                    </telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadGrid ID="dgLegHotel" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                            ClientIDMode="AutoID" PagerStyle-AlwaysVisible="false" Width="100%" PageSize="10"
                                            OnItemCommand="dgLegHotel_ItemCommand" OnItemDataBound="dgLegHotel_ItemDataBound"
                                            OnNeedDataSource="dgLegHotel_BindData" CssClass="rgLegHotel" OnSelectedIndexChanged="dgLegHotel_SelectedIndexChanged">
                                            <MasterTableView CommandItemDisplay="Bottom" AllowFilteringByColumn="false" DataKeyNames="HotelIdentifier,HotelID,isArrivalHotel">
                                                <Columns>
                                                    <telerik:GridBoundColumn UniqueName="HotelCode" HeaderText="Hotel Code" DataField="HotelCode"
                                                        HeaderStyle-Width="40px" />
                                                    <telerik:GridBoundColumn UniqueName="HotelName" HeaderText="Hotel Name" DataField="PreflightHotelName"
                                                        HeaderStyle-Width="150px" />
                                                    <telerik:GridTemplateColumn UniqueName="ArrDep" HeaderText="Arr. / Dep." HeaderStyle-Width="60px"
                                                        ItemStyle-Width="40px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblArrDep" runat="server" Text='' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="PAXCDs" HeaderText="PAX Code" HeaderStyle-Width="60px"
                                                        ItemStyle-Width="70px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCDs" runat="server" Text='' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn UniqueName="Address" HeaderText="Address" DataField="Address1"
                                                        HeaderStyle-Width="90px" />
                                                    <telerik:GridBoundColumn UniqueName="City" HeaderText="City" DataField="CityName"
                                                        HeaderStyle-Width="60px" />
                                                    <telerik:GridBoundColumn UniqueName="State" HeaderText="State / Province" DataField="StateName"
                                                        HeaderStyle-Width="60px" />
                                                    <telerik:GridBoundColumn UniqueName="Country" HeaderText="Country" DataField="CountryCode"
                                                        HeaderStyle-Width="40px" />
                                                    <telerik:GridBoundColumn UniqueName="PhoneNo" HeaderText="Phone No." DataField="PhoneNum1"
                                                        HeaderStyle-Width="80px" />
                                                </Columns>
                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                                <CommandItemTemplate>
                                                    <div class="grid_icon">
                                                        <asp:LinkButton ID="lnkInitInsert" runat="server" CommandName="InitInsert" CssClass="add-icon-grid"
                                                            ToolTip="Add"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                                            OnClick="lnkLegHotelEdit_OnClick"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="delete-icon-grid" ToolTip="Delete"
                                                            OnClick="lnkLegHotelDelete_OnClick"></asp:LinkButton>
                                                    </div>
                                                </CommandItemTemplate>
                                            </MasterTableView>
                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                <Scrolling AllowScroll="true" />
                                                <Selecting AllowRowSelect="true" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td style="display: none">
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbArriveDepart" runat="server" ClientIDMode="Static" RepeatDirection="Horizontal"
                                            onclick="validateButtonList();">
                                            <asp:ListItem Selected="True" Value="0">Arrival</asp:ListItem>
                                            <asp:ListItem Value="1">Departure</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <asp:HiddenField runat="server" ID="hdnrbArriveDepartPrev" ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hdnArrivalDate" ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hdnNextDeptDate" ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="hdnHotelchanged" Value="false" />
                                    </td>
                                    <td align="right">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnCancelHotel" runat="server" ToolTip="Cancel Hotel" Text="Cancel Hotel"
                                                        CssClass="ui_nav" OnClick="btnCancelHotel_OnClick" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSaveHotel" runat="server" ToolTip="Save Hotel" Text="Save Hotel"
                                                        CssClass="ui_nav" OnClick="btnSaveHotel_OnClick" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div style="float: left; width: 710px; padding: 5px 5px 0 0; background: #f6f6f6;">
                                <asp:HiddenField ID="hdnLegNumHotel" runat="server" />
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:HiddenField runat="server" ID="hdnHotelIdentifier" />
                                            <asp:RadioButtonList ID="radlstArrangements" runat="server" RepeatDirection="Horizontal"
                                                OnSelectedIndexChanged="radlstArrangements_SelectedIndexChanged">
                                                <asp:ListItem Value="UWA">UWA Arranged</asp:ListItem>
                                                <asp:ListItem Value="Client">Client Arranged</asp:ListItem>
                                                <asp:ListItem Value="None">None</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td align="center">
                                        </td>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Status
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHotelCompleteds" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Required</asp:ListItem>
                                                            <asp:ListItem Value="2">Not Required</asp:ListItem>
                                                            <asp:ListItem Value="3">In Progress</asp:ListItem>
                                                            <asp:ListItem Value="4">Change</asp:ListItem>
                                                            <asp:ListItem Value="5">Canceled</asp:ListItem>
                                                            <asp:ListItem Value="6">Completed</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="float: left; width: 718px; padding: 5px 0 0 0; background: #f6f6f6;">
                                <table width="100%">
                                    <tr>
                                        <td width="54%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkUniversalPreferences" runat="server" Text="Use Universal Preferences" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" align="left" style="display: none">
                                                                </td>
                                                                <td align="left" colspan="3" style="display: none">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Hotel Code
                                                                </td>
                                                                <td class="tdLabel90">
                                                                    <asp:TextBox ID="tbHotelCode" runat="server" CssClass="text50" AutoPostBack="true"
                                                                        OnTextChanged="tbHotelCode_TextChanged"></asp:TextBox>
                                                                    <asp:Button ID="btnCode" OnClientClick="javascript:openWin('radHotelPopup');return false;"
                                                                        CssClass="browse-button" runat="server" />
                                                                </td>
                                                                <td align="left" class="tdLabel30">
                                                                    Rate
                                                                </td>
                                                                <td class="pr_radtextbox_75" align="left">
                                                                    <telerik:RadNumericTextBox ID="tbHotelRate" runat="server" Type="Currency" Culture="en-US"
                                                                        MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                                                        ClientEvents-OnValueChanged="checkRadNumericValue" ClientEvents-OnFocus="Focus">
                                                                    </telerik:RadNumericTextBox>
                                                                </td>
                                                                <%--<td align="left">
                                                                <asp:TextBox ID="tbHotelRate" MaxLength="6" CssClass="text70" Text="0.00" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                    onchange="javascript:return checknumericValue(this);" runat="server"></asp:TextBox>
                                                            </td>--%>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:Label ID="lbHotelCode" runat="server" Visible="true"></asp:Label>
                                                                    <asp:Label ID="lbcvHotelCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Hotel Name
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbHotelName" MaxLength="60" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Addr 1:
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbAddr1" MaxLength="100" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Addr 2:
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbAddr2" MaxLength="100" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Addr 3:
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbAddr3" MaxLength="100" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    City:
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbCity" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    State/Prov:
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbState" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Country:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbCtry" runat="server" CssClass="text50" AutoPostBack="true" OnTextChanged="tbCtryCode_TextChanged"></asp:TextBox>
                                                                    <asp:Button ID="btnCtry" OnClientClick="javascript:openWin('rdCtryPopup');return false;"
                                                                        CssClass="browse-button" runat="server" />
                                                                </td>
                                                                <td class="tdLabel60" valign="top">
                                                                    Metro:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbMetro" runat="server" CssClass="text50" AutoPostBack="true" OnTextChanged="tbMetroCode_TextChanged"></asp:TextBox>
                                                                    <asp:Button ID="btnMetro" OnClientClick="javascript:openWin('rdMetroPopup');return false;"
                                                                        CssClass="browse-button" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Label ID="lbcvCtry" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:Label ID="lbcvMetro" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Postal Code:
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbPost" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Phone
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbHotelPhone" MaxLength="25" runat="server" CssClass="text220" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Fax
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="tbHotelFax" MaxLength="25" runat="server" CssClass="text220" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" cellspacing="0" cellpadding="2" width="100%">
                                                            <tr>
                                                                <td class="tdLabel100" colspan="2">
                                                                    <asp:CheckBox ID="chkBookNightPrior" runat="server" Text="Book Night Prior" />
                                                                </td>
                                                                <td align="left" colspan="2">
                                                                    <asp:CheckBox ID="chkEarlyCheckin" runat="server" Text="Early Check-in" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel80" valign="top">
                                                                    Date In
                                                                </td>
                                                                <td class="tdLabel80">
                                                                    <asp:TextBox ID="tbDateIn" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                        onclick="showPopup(this, event);" MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                                                </td>
                                                                <td valign="top">
                                                                    No. of Rooms
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:TextBox ID="tbNoforooms" MaxLength="2" runat="server" CssClass="text50" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Date Out
                                                                </td>
                                                                <td class="tdLabel80">
                                                                    <asp:TextBox ID="tbDateOut" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                        onclick="showPopup(this, event);" MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                                                </td>
                                                                <td valign="top">
                                                                    No. of Beds
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbNoofBeds" MaxLength="3" runat="server" CssClass="text50" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    Type of Room
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:DropDownList ID="ddltypeofRoom" CssClass="text170" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    Room Desc.
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:DropDownList ID="ddlRoomDesc" CssClass="text170" runat="server">
                                                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                                                        <asp:ListItem Value="Standard">Standard</asp:ListItem>
                                                                        <asp:ListItem Value="Deluxe">Deluxe</asp:ListItem>
                                                                        <asp:ListItem Value="Executive">Executive</asp:ListItem>
                                                                        <asp:ListItem Value="Other">Other</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <%--<asp:RadioButtonList ID="radRequestOnly" runat="server" RepeatDirection="Horizontal"
                                                                        CellPadding="0" CellSpacing="0">
                                                                        <asp:ListItem Value="R" Selected="True">Request Only</asp:ListItem>
                                                                        <asp:ListItem Value="B">Book Night Prior</asp:ListItem>
                                                                    </asp:RadioButtonList>--%>
                                                                    <asp:CheckBox ID="chkRequestOnly" runat="server" Text="Request Only" />
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:CheckBox ID="chkSmoking" runat="server" Text="Smoking" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="horizontalLine">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Club Card
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:TextBox ID="tbClubCard" MaxLength="100" runat="server" CssClass="text220"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Max. Cap Amt
                                                                </td>
                                                                <td align="left" class="pr_radtextbox_100">
                                                                    <telerik:RadNumericTextBox ID="tbMaxcapamt" runat="server" Type="Currency" Culture="en-US"
                                                                        MaxLength="10" Value="0.00" ClientEvents-OnValueChanged="checkRadNumericCapValue"
                                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                                    </telerik:RadNumericTextBox>
                                                                    <%-- <asp:TextBox ID="tbMaxcapamt" MaxLength="10" Text="0.00" runat="server" CssClass="text82"
                                                                    onKeyPress="return fnAllowNumericAndChar(this,event,'.') " onchange="javascript:return checkMaxcap(this);"></asp:TextBox>--%>
                                                                </td>
                                                                <td align="left" colspan="2">
                                                                    <asp:CheckBox ID="chkRatecap" runat="server" Text="Rate Cap" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="50%" valign="top">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" width="100%">
                                                            <tr>
                                                                <td valign="top">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkNoPAX" runat="server" Text="No PAX" OnCheckedChanged="NoPAX_OnCheckedChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <telerik:RadGrid ID="dgLegEntity" runat="server" OnNeedDataSource="dgLegEntity_BindData"
                                                                                    EnableAJAX="True" AutoGenerateColumns="false" OnItemDataBound="dgLegEntity_ItemDataBound"
                                                                                    Width="100%" Height="350px" AllowMultiRowSelection="true" HeaderStyle-HorizontalAlign="Center"
                                                                                    AllowPaging="false">
                                                                                    <MasterTableView DataKeyNames="OrderNUM,PaxCode,PaxID,LegId,PaxName" CommandItemDisplay="None"
                                                                                        AllowFilteringByColumn="false" ShowFooter="false" ItemStyle-HorizontalAlign="Left"
                                                                                        AllowPaging="false">
                                                                                        <Columns>
                                                                                            <telerik:GridTemplateColumn UniqueName="IsSelect" HeaderText="Select" HeaderStyle-Width="40px"
                                                                                                ItemStyle-Width="40px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                                                                </ItemTemplate>
                                                                                                <HeaderTemplate>
                                                                                                    Select All<br />
                                                                                                    <asp:CheckBox ID="chkSelectHeader" onclick="javascript:HeaderClick(this);" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridTemplateColumn UniqueName="PaxCode" HeaderText="PAX Code" HeaderStyle-Width="50px"
                                                                                                ItemStyle-Width="50px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblPAXCD" runat="server" Text='<%# Bind("PaxCode") %>' />
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridBoundColumn UniqueName="Name" HeaderText="Name" DataField="PaxName"
                                                                                                HeaderStyle-Width="150px" />
                                                                                        </Columns>
                                                                                        <CommandItemTemplate>
                                                                                            <commanditemsettings showaddnewrecordbutton="false" showrefreshbutton="false" />
                                                                                        </CommandItemTemplate>
                                                                                    </MasterTableView>
                                                                                    <ClientSettings EnablePostBackOnRowClick="false" AllowRowsDragDrop="false" AllowColumnsReorder="false"
                                                                                        ReorderColumnsOnClient="true">
                                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" FrozenColumnsCount="3" />
                                                                                        <ClientEvents />
                                                                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                                                    </ClientSettings>
                                                                                    <GroupingSettings CaseSensitive="false" />
                                                                                </telerik:RadGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" width="100%">
                                                            <tr>
                                                                <td valign="bottom" colspan="4">
                                                                    Confirmation
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pr_radtextbox_320x40" colspan="4">
                                                                    <telerik:RadTextBox ID="tbConfirmation" runat="server" TextMode="MultiLine" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="bottom" colspan="4">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pr_radtextbox_320x40" colspan="4">
                                                                    <telerik:RadTextBox ID="tbComments" runat="server" TextMode="MultiLine" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPgeViewTransport" runat="server">
                    <telerik:RadPanelItem>
                        <ContentTemplate>
                            <telerik:RadTabStrip ID="rtsLegTransport" Skin="Windows7" AutoPostBack="true" runat="server"
                                OnTabClick="rtsLegTransport_TabClick" ReorderTabsOnSelect="true">
                                <Tabs>
                                    <telerik:RadTab Text="Leg1" Selected="true">
                                    </telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                            <div style="float: left; width: 718px; padding: 5px 0 0 0; background: #f6f6f6;">
                                <div style="float: left; width: 353px; padding: 5px 5px 5px 0px;">
                                    <asp:HiddenField ID="hdnLegNumTransport" runat="server" />
                                    <fieldset>
                                        <legend>Depart Transportation</legend>
                                        <table cellpadding="1" cellspacing="2">
                                            <tr>
                                                <td>
                                                    Status
                                                </td>
                                                <td colspan="4">
                                                    <asp:CheckBox ID="chkTransCompleted" runat="server" Text="Completed" Visible="false" />
                                                    <asp:DropDownList ID="ddlDepartTransportCompleteds" runat="server" AutoPostBack="true">
                                                        <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Required</asp:ListItem>
                                                        <asp:ListItem Value="2">Not Required</asp:ListItem>
                                                        <asp:ListItem Value="3">In Progress</asp:ListItem>
                                                        <asp:ListItem Value="4">Change</asp:ListItem>
                                                        <asp:ListItem Value="5">Canceled</asp:ListItem>
                                                        <asp:ListItem Value="6">Completed</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Code
                                                </td>
                                                <td class="tdLabel50" colspan="2">
                                                    <asp:TextBox ID="tbTransCode" runat="server" CssClass="text50" AutoPostBack="true"
                                                        OnTextChanged="tbTransCode_TextChanged"></asp:TextBox>
                                                    <asp:Button ID="btnTransport" OnClientClick="javascript:openWin('rdTransportPopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                                <td class="tdLabel60" align="center">
                                                    Rate
                                                </td>
                                                <td class="pr_radtextbox_55" align="left">
                                                    <telerik:RadNumericTextBox ID="tbTransRate" runat="server" Type="Currency" Culture="en-US"
                                                        MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                                        ClientEvents-OnValueChanged="checkRadNumericValue" ClientEvents-OnFocus="Focus">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <%-- <td>
                                                                <asp:TextBox ID="tbTransRate" MaxLength="6" CssClass="text50" Text="0.00" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                    onchange="javascript:return checknumericValue(this);" runat="server"></asp:TextBox>
                                                            </td>--%>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="left" colspan="4">
                                                    <asp:Label ID="lbTransCode" runat="server" Visible="true"></asp:Label>
                                                    <asp:Label ID="lbcvTransCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Name
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="tbTransName" MaxLength="60" runat="server" CssClass="text220"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Phone
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="tbTransPhone" MaxLength="25" runat="server" CssClass="text220" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Fax
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="tbTransFax" MaxLength="25" runat="server" CssClass="text220" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Confirmation
                                                </td>
                                                <td colspan="4" class="pr_radtextbox_226">
                                                    <telerik:RadTextBox ID="tbTransConfirmation" runat="server" TextMode="MultiLine" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Comments
                                                </td>
                                                <td colspan="4" class="pr_radtextbox_226">
                                                    <telerik:RadTextBox ID="tbTransComments" runat="server" TextMode="MultiLine" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                                <div style="float: left; width: 353px; padding: 5px 5px 5px 0px;">
                                    <fieldset>
                                        <legend>Arrive Transportation</legend>
                                        <table cellpadding="1" cellspacing="2">
                                            <tr>
                                                <td>
                                                    Status
                                                </td>
                                                <td colspan="4">
                                                    <asp:DropDownList ID="ddlArriveTransportCompleteds" runat="server" AutoPostBack="true">
                                                        <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Required</asp:ListItem>
                                                        <asp:ListItem Value="2">Not Required</asp:ListItem>
                                                        <asp:ListItem Value="3">In Progress</asp:ListItem>
                                                        <asp:ListItem Value="4">Change</asp:ListItem>
                                                        <asp:ListItem Value="5">Canceled</asp:ListItem>
                                                        <asp:ListItem Value="6">Completed</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:CheckBox ID="chkArriveCompleted" runat="server" Text="Completed" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Code
                                                </td>
                                                <td class="tdLabel60" colspan="2">
                                                    <asp:TextBox ID="tbArriveTransCode" runat="server" CssClass="text50" AutoPostBack="true"
                                                        OnTextChanged="tbArriveTransCode_TextChanged"></asp:TextBox>
                                                    <asp:Button ID="btnArriveTrans" OnClientClick="javascript:openWin('rdArriveTransportPopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                </td>
                                                <td class="tdLabel50" align="center">
                                                    Rate
                                                </td>
                                                <td class="pr_radtextbox_55">
                                                    <telerik:RadNumericTextBox ID="tbArriveRate" runat="server" Type="Currency" Culture="en-US"
                                                        MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                                        ClientEvents-OnValueChanged="checkRadNumericValue" ClientEvents-OnFocus="Focus">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <%--  <td>
                                                                <asp:TextBox ID="tbArriveRate" MaxLength="6" runat="server" CssClass="text50" Text="0.00"
                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onchange="javascript:return checknumericValue(this);"></asp:TextBox>
                                                            </td>--%>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="left" colspan="4">
                                                    <asp:Label ID="lbArriveTransCode" runat="server" Visible="true"></asp:Label>
                                                    <asp:Label ID="lbcvArriveTransCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Name
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="tbArriveTransName" MaxLength="60" runat="server" CssClass="text220"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Phone
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="tbArrivePhone" MaxLength="25" runat="server" CssClass="text220"
                                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Fax
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="tbArriveFax" MaxLength="25" runat="server" CssClass="text220" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Confirmation
                                                </td>
                                                <td colspan="4" class="pr_radtextbox_226">
                                                    <telerik:RadTextBox ID="tbArriveConfirmation" runat="server" TextMode="MultiLine" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Comments
                                                </td>
                                                <td colspan="4" class="pr_radtextbox_226">
                                                    <telerik:RadTextBox ID="tbArriveComments" runat="server" TextMode="MultiLine" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <asp:Button ID="btnInteractiveMap" Visible="false" CssClass="button" runat="server"
                            Text="InteractiveMap" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnTranHotCancel" Visible="false" CssClass="button" runat="server"
                            Text="Cancel" OnClick="btnTranHotCancel_Click" />
                        <asp:Button ID="btnTranHotSave" CssClass="button" runat="server" Visible="false"
                            Text="Save" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="text-align: center; float: left; width: 718px; padding: 5px 0  5px 0px;">
        </div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right">
                    <asp:HiddenField ID="hdnLeg" runat="server" />
                    <asp:Button ID="btnDeleteTrip" runat="server" CssClass="button" Text="Delete Trip"
                        ToolTip="Delete Selected Record" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnEditTrip" runat="server" Text="Edit Trip" ToolTip="Edit Selected Record"
                        OnClick="btnEditTrip_Click" CssClass="button" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" ToolTip="Cancel All Changes"
                        OnClick="btnCancel_Click" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ToolTip="Save Changes"
                        OnClick="btnSave_Click" />
                    <asp:Button ID="btnNext" runat="server" CssClass="button" ToolTip="Next" Text="Next >"
                        OnClick="btnNext_Click" />
                    <asp:HiddenField runat="server" ID="hdnInsert" />
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnAlert" runat="server" Text="Button" OnClick="Alert_Click" />
                </td>
            </tr>
        </table>
        <table id="Table1" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnApplyPAXAllLegsYes" runat="server" Text="Button" OnClick="btnApplyPAXAllLegsYes_Click" />
                    <asp:Button ID="btnApplyPAXAllLegsNo" runat="server" Text="Button" OnClick="btnApplyPAXAllLegsNo_Click" />
                    <asp:Button ID="btnPaxRemoveYes" runat="server" Text="Button" OnClick="btnPaxRemoveYes_Click" />
                    <asp:Button ID="btnPaxRemoveNo" runat="server" Text="Button" OnClick="btnPaxRemoveNo_Click" />
                    <asp:Button ID="btnPaxNextYes" runat="server" Text="Button" OnClick="btnPaxNextYes_Click" />
                    <asp:Button ID="btnPaxNextNo" runat="server" Text="Button" OnClick="btnPaxNextNo_Click" />
                    <asp:Button ID="btnDeletePaxYes" runat="server" Text="Button" OnClick="btnDeletePaxYes_Click" />
                    <asp:Button ID="btnDeletePaxNo" runat="server" Text="Button" OnClick="btnDeletePaxNo_Click" />
                    <asp:Button ID="btnInfoTabYes" runat="server" Text="Button" OnClick="btnInfoTabYes_Click" />
                    <asp:Button ID="btnInfoTabNo" runat="server" Text="Button" OnClick="btnInfoTabNo_Click" />
                    <asp:Button ID="btnDeleteHotelYes" runat="server" Text="Button" OnClick="btnDeleteHotelYes_Click" />
                    <asp:Button ID="btnDeleteHotelNo" runat="server" Text="Button" OnClick="btnDeleteHotelNo_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
