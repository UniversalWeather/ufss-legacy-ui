﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightCrewRosterPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreflightCrewRosterPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Crew Roster</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <style type="text/css">
       #gbox_gridCrew #gview_gridCrew .ui-jqgrid-bdiv table#gridCrew tr td:first-child { text-align:center !important;}
        #gbox_gridCrew #gview_gridCrew .ui-jqgrid-bdiv table#gridCrew tr td:nth-last-child(-n+4) { text-align:center !important;}
    </style>
    <script type="text/javascript">
        var selectedRowMultipleCrew = "";
        var selectedRowData = null;
        var jqgridTableId = '#gridCrew';
        var crewId = null;
        var crewcd = null;
        var msg = " Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
        var crewcdList = "";
        var isopenlookup = false;
        var ismultiSelect = true;
        var selectedCrewId = "";
        var selectedCrewsList = new Array();
        $(document).ready(function () {

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            selectedCrewId = $.trim(decodeURI(getQuerystring("CrewID", "")));
            selectedRowData = $.trim(decodeURI(getQuerystring("CrewCD", "")));
            selectedRowMultipleCrew = $.trim(decodeURI(getQuerystring("CrewCD", "")));
            jQuery("#hdnselectedCrewCd").val(selectedRowMultipleCrew);
            jQuery("#hdnselectedCrewId").val(selectedCrewId);
            if (selectedRowData != "") {
                isopenlookup = true;
            }
            $("#btnSubmit").click(function () {
                if (selectedCrewsList.length > 0) {
                    returnToParent(selectedCrewsList, 0);
                }
                else {
                    showMessageBox('Please select a crew.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                popupwindowForFilterGrid("/Views/Settings/People/CrewRoster.aspx?IsPopup=Add", "rdAddWindow", popupTitle, 1100, 680, jqgridTableId);
                return false;
            });


            $("#btnClientCodeFilter").click(function () {
                openWinForFilterGrid("/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("tbClientCodeFilter").value, "rdAddpopupwindow", popupTitle, 650, 495, jqgridTableId, "tbClientCodeFilter", "ClientCD", "hdnClientId", "ClientID");
                return false;
            });

            $("#tbClientCodeFilter").change(function () {
                $("#hdnClientId").val("");
                return false;
            });

            $("#btnCrewGroupFilter").click(function () {
                openWinForFilterGrid("/Views/Settings/People/CrewGroupPopup.aspx?CrewGroupCD=" + document.getElementById("tbCrewGroupFilter").value, "rdAddpopupwindow", popupTitle, 650, 500, jqgridTableId, "tbCrewGroupFilter", "CrewGroupCD", "hdnCrewGroupId", "CrewGroupID");
                return false;
            });

            $("#tbCrewGroupFilter").change(function () {
                $("#hdnCrewGroupId").val("");
                return false;
            });

            var CrewLists = [];
            $("#recordDelete").click(function () {
                var selectedrows = $('#gridCrew').jqGrid('getGridParam', 'selarrrow');
                if (selectedrows.length > 0) {
                    for (var i = 0; i < selectedrows.length; i++) {
                        var elem = new Object();
                        var rowsDataValues = $(jqgridTableId).jqGrid('getRowData', selectedrows[i]);
                        elem.CrewID = $.trim(rowsDataValues['CrewID']);
                        elem.CrewCD = $.trim(rowsDataValues['CrewCD']);
                        elem.Value = 0;
                        CrewLists.push(elem);
                    }

                    if (CrewLists.length == 1) {

                        showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                    }
                    else {
                        showConfirmPopup('Are you sure you want to delete these records?', confirmCallBackFn, 'Confirmation');
                    }

                }
                else {
                    showMessageBox('Please select a crew.', popupTitle);
                }
                return false;

            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    DeleteCrewList(arg, jqgridTableId, CrewLists);
                }
            }

            $("#recordEdit").click(function () {
                $("#divMsg1").removeClass().addClass('hideDiv');
                var selectedrows = $('#gridCrew').jqGrid('getGridParam', 'selarrrow');
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if (selr != null) {
                    if (selectedrows.length == 1) {

                        var rowData = $(jqgridTableId).getRowData(selr);
                        crewId = rowData['CrewID'];
                        if (crewId != undefined && crewId != null && crewId != '' && crewId != 0) {
                            popupwindowForFilterGrid("/Views/Settings/People/CrewRoster.aspx?IsPopup=&CrewID=" + crewId, "rdEditWindow", popupTitle, 1100, 680, jqgridTableId);
                        }
                        else {
                            showMessageBox('Please select a crew.', popupTitle);
                        }
                    }
                    else {
                        $("#divMsg1").removeClass().addClass('showDiv');
                    }
                }
                else {
                    $("#divMsg1").removeClass().addClass('showDiv');
                }
                setTimeout(function () {
                    $("#divMsg1").removeClass().addClass('hideDiv');
                }, 120000);
                return false;
            });


            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'crewlist';
                    postData.showInactive = $('#chkActiveOnly').is(':checked') ? true : false;
                    postData.sendIcaoId = $("#chkHomebaseOnly").is(':checked') ? true : false;
                    postData.IsFixedRotary = $("#chkRotaryWingOnly").is(':checked') ? true : false;
                    postData.IsFixedWing = $("#chkFixedWingOnly").is(':checked') ? true : false;
                    postData.CrewGroupID = $("#hdnCrewGroupId").val();
                    postData.ClientID = $("#hdnClientId").val();
                    postData.CrewCD = selectedRowData;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 300,
                width: 884,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                colNames: ['CrewID', 'Code', 'Crew Name', 'Phone', 'Home Base', 'Active', '', 'Checklist', 'Notes'],
                colModel: [
                 { name: 'CrewID', index: 'CrewID', key: true, hidden: true },
                 { name: 'CrewCD', index: 'CrewCD', width: 87, fixed: true },
                 { name: 'CrewName', index: 'CrewName', width: 250, formatter: fullNameFormatter, searchoptions: { sopt: ['cn'] }, fixed: true },
                 { name: 'PhoneNum', index: 'PhoneNum', width: 85, fixed: true },
                 { name: 'IcaoID', index: 'IcaoID', width: 90, fixed: true },
                 { name: 'IsStatus', index: 'IsStatus', width: 80, formatter: 'checkbox', sortable: false, search: false, align: 'center', fixed: true },
                 { name: 'HomebaseCD', index: 'HomebaseCD', width: 50, hidden: true },
                 { name: 'CheckList', index: 'CheckList', width: 80, formatter: 'checkbox', sortable: false, search: false, align: 'center', fixed: true },
                 { name: 'Notes', index: 'Notes', width: 55, sortable: false, search: false, hidden: true }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = $(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    if (status)
                        selectedCrewsList.push(rowData);
                    else {
                        for (var rowId = 0; rowId < selectedCrewsList.length; rowId++)
                        {
                            if (selectedCrewsList[rowId].CrewID == id)
                            {
                                selectedCrewsList.splice(rowId, 1);
                            }
                        }
                    }
                },
                onSelectAll: function (id, status) {
                    if (status) {
                        for (var rowId = 0; rowId < id.length; rowId++) {
                            var rowData = $(jqgridTableId).jqGrid("getRowData", id[rowId]);
                            if (status)
                                selectedCrewsList.push(rowData);
                        }
                    }
                    else {
                        for (var rowId = 0; rowId < id.length; rowId++) {
                            var rowData = $(jqgridTableId).jqGrid("getRowData", id[rowId]);
                            if (!status) 
                                selectedCrewsList.pop(rowData);
                        }
                    }                          
                },
                afterInsertRow: function (rowid, rowObject) {                
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                    if (ismultiSelect) {
                        for (var rowId = 0; rowId < selectedCrewsList.length; rowId++) {
                            jQuery(jqgridTableId).setSelection(selectedCrewsList[rowId].CrewID, false);
                        }
                    }
                    setSelectRow();
                }
            });

            function fullNameFormatter(cellvalue, options, rowObject) {
                var mname = "";
                var lname = "";
                var fname = "";
                if (rowObject.MiddleInitial == null) {
                    mname = "";
                } else {
                    mname = rowObject.MiddleInitial;
                }
                if (rowObject.LastName == null) {
                    lname = "";
                } else {
                    lname = rowObject.LastName;
                }
                if (rowObject.FirstName == null) {
                    fname = "";
                } else {
                    fname = rowObject.FirstName;
                }
                return lname + ', ' + fname + ' ' + mname;
            }

            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            $("#pagesizebox").insertBefore('.ui-paging-info');
        });

        function reloadCrewRoster(id) {
            document.getElementById("hfUpdatedId").value = id;
            var myGrid = $(jqgridTableId);
            isopenlookup = true;
            selectedRowData = id;
            myGrid.trigger('reloadGrid');
        }
        function setSelectRow() {
            if (document.getElementById("hfUpdatedId").value != "") {
                var myGrid = $(jqgridTableId);
                var rowIds = myGrid.jqGrid('getDataIDs');
                for (i = 1; i <= rowIds.length; i++) {
                    var rowData = myGrid.jqGrid('getRowData', rowIds[i]);
                    if (rowData['CrewCD'] == document.getElementById("hfUpdatedId").value) {
                        myGrid.jqGrid('setSelection', rowIds[i]);
                    }
                }
                document.getElementById("hfUpdatedId").value = "";
            }
        }

        function reloadCrew() {
            var crewgroupCd = $.trim($("#tbCrewGroupFilter").val());
            var clientCd = $.trim($("#tbClientCodeFilter").val());
            if (!IsNullOrEmptyOrUndefined(crewgroupCd)) {
                if (IsNullOrEmptyOrUndefined(clientCd))
                {
                    $("#hdnClientId").val(0);
                }
                $("#hdnSearchResult").val("1");
                if (!IsNullOrEmptyOrUndefined($("#hdnCrewGroupId").val()) && IsNullOrEmptyOrUndefined(clientCd))
                {
                    $("#gridCrew").jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
                else if(!IsNullOrEmptyOrUndefined($("#hdnCrewGroupId").val()) && !IsNullOrEmptyOrUndefined($("#hdnClientId").val()))
                {
                    $("#gridCrew").jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
            }
            else if (IsNullOrEmptyOrUndefined(clientCd)) {
                $("#hdnCrewGroupId").val(0);
                $("#gridCrew").jqGrid().trigger('reloadGrid');
            }
            else if (!IsNullOrEmptyOrUndefined(clientCd)) {
                if (IsNullOrEmptyOrUndefined(crewgroupCd))
                {
                    $("#hdnCrewGroupId").val(0);
                }
                $("#hdnSearchResult").val("1");
                if (!IsNullOrEmptyOrUndefined($("#hdnClientId").val()) && IsNullOrEmptyOrUndefined(crewgroupCd)) {
                    $("#gridCrew").jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
            }
            else if (IsNullOrEmptyOrUndefined(crewgroupCd)) {
                $("#hdnClientId").val(0);
                $("#gridCrew").jqGrid().trigger('reloadGrid');
            }
            selectedRowMultipleCrew = jQuery("#hdnselectedCrewCd").val();
            selectedCrewId = jQuery("#hdnselectedCrewId").val();
        }

        function returnToParent(rowData, oneRow) {
            var oArg = new Object();
            var val = "";
            if (oneRow == 0) {
                var Crews = new Array();
                for (rowId = 0; rowId < rowData.length; rowId++) {
                    var dtNewAssign = new Object();
                    dtNewAssign.CrewID = rowData[rowId].CrewID
                    dtNewAssign.CrewCD = rowData[rowId].CrewCD
                    dtNewAssign.CrewName = rowData[rowId].CrewName
                    dtNewAssign.IsNotified = false;
                    dtNewAssign.OrderNUM = 0;
                    dtNewAssign.City = rowData[rowId].CityName;
                    dtNewAssign.State = rowData[rowId].StateName;
                    dtNewAssign.Postal = rowData[rowId].PostalZipCD;
                    dtNewAssign.AddlNotes = rowData[rowId].Notes2;
                    dtNewAssign.PassportID = "";
                    dtNewAssign.VisaID = "";
                    dtNewAssign.Street = "";
                    dtNewAssign.Passport = "";
                    dtNewAssign.PassportExpiryDT = null;
                    dtNewAssign.Notes = rowData[rowId].Notes;
                    dtNewAssign.Nation = "";
                    dtNewAssign.IsStatus = rowData[rowId].IsStatus;
                    dtNewAssign.Street = rowData[rowId].Addr1;
                    Crews.push(dtNewAssign);
                }
                oArg.val = Crews;
            }
            else {
                var Crews = new Array();
                var rowId = rowData;
                var dtNewAssign = new Object();
                dtNewAssign.CrewID = rowId.CrewID
                dtNewAssign.CrewCD = rowId.CrewCD
                dtNewAssign.CrewName = rowId.CrewName
                dtNewAssign.IsNotified = false;
                dtNewAssign.OrderNUM = 0;
                dtNewAssign.City = rowId.CityName;
                dtNewAssign.State = rowId.StateName;
                dtNewAssign.Postal = rowId.PostalZipCD;
                dtNewAssign.AddlNotes = rowId.Notes2;
                dtNewAssign.PassportID = "";
                dtNewAssign.VisaID = "";
                dtNewAssign.Street = "";
                dtNewAssign.Passport = "";
                dtNewAssign.PassportExpiryDT = null;
                dtNewAssign.Notes = rowId.Notes;
                dtNewAssign.Nation = "";
                dtNewAssign.IsStatus = rowId.IsStatus;
                dtNewAssign.Street = rowId.Addr1;
                Crews.push(dtNewAssign);
                oArg.val = Crews;
            }
            var oWnd = GetRadWindow();
            if (oArg) {

                oWnd.close(oArg);
            }
        }


        $(document).ready(function () {
            $("#tbClientCodeFilter").blur(function () {
                var clientCd = $.trim($("#tbClientCodeFilter").val());
                if (clientCd != undefined && clientCd != null && clientCd != '') {
                    CheckClientCodeExistance(clientCd, hdnClientId, errorMsg, "#gridCrew");
                }
                else {
                    $("#hdnClientId").val(0);
                    $("#errorMsg").removeClass().addClass('hideDiv');
                }
            });
        });

        $(document).ready(function () {
            $("#tbCrewGroupFilter").bind('blur keydown', function (e) {
                if (e.type === 'keydown' && e.keyCode !== 10 && e.keyCode !== 13)
                    return;
                var crewgroupCd = $.trim($("#tbCrewGroupFilter").val());
                if (crewgroupCd != undefined && crewgroupCd != null && crewgroupCd != '') {
                    CheckCrewGroupExistance(crewgroupCd, hdnCrewGroupId, errorMsg1, "#gridCrew");
                }
                else {
                    
                    $("#hdnCrewGroupId").val(0);
                    $(jqgridTableId).jqGrid().trigger('reloadGrid');
                    $("#errorMsg1").removeClass().addClass('hideDiv');
                }
            });
        });

    </script>
    <style type="text/css">
        .showDiv {
            text-align: right;
            margin-right: 30px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }
    </style>
</head>
<body class="removeBlueBar">
    
        <div class="jqgrid">
            <input type="hidden" id="hdnSearchResult" value="" />
            <input type="hidden" id="hdnselectedCrewCd" value=""/>
            <input type="hidden" id="hdnselectedCrewId" value=""/>
            <input type="hidden" id="hfUpdatedId" value="" />
            <div>
                <table class="box1">

                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="tdLabel100">
                                            <input type="checkbox" checked="checked" id="chkActiveOnly" /><label for="chkActiveOnly">Active Only</label>
                                        </td>
                                        <td valign="top" class="tdLabel120">
                                            <input type="checkbox" name="Homebase" id="chkHomebaseOnly" /><label for="chkHomeBaseOnly">Home Base Only</label>
                                        </td>
                                        <td valign="top" class="tdLabel90">
                                            <input type="checkbox" id="chkFixedWingOnly" /><label for="chkFixedWingOnly">Fixed Wing</label>
                                        </td>
                                        <td valign="top" class="tdLabel100">
                                            <input type="checkbox" id="chkRotaryWingOnly" /><label for="chkRotaryWingOnly">Rotary Wing</label>
                                        </td>
                                        <td valign="top">
                                            <table cellspacing="0" cellpadding="0" id="Table1">
                                                <tbody>
                                                    <tr>
                                                        <td class="tdLabel80">
                                                            <span style="color: #444444; font-family: Arial; font-size: 12px;" id="lbClientCode">Client Code :</span>
                                                        </td>
                                                        <td class="tdLabel100">
                                                            <input type="text" style="font-family: Arial; font-size: 12px;" class="text50" id="tbClientCodeFilter" maxlength="5" name="tbClientCodeFilter" />
                                                            <input type="button" class="browse-button" id="btnClientCodeFilter" value="" name="btnClientCodeFilter" />
                                                            <input id="hdnClientId" type="hidden" value="" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                             <div id="errorMsg" class="hideDiv">Invalid client code.!</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table cellspacing="0" cellpadding="0" id="Exception">
                                                <tbody>
                                                    <tr>
                                                        <td class="tdLabel80">
                                                            <span style="color: #444444; font-family: Arial; font-size: 12px;" id="Label2">Crew Group :</span>
                                                        </td>
                                                        <td class="tdLabel100">
                                                            <input type="text" style="font-family: Arial; font-size: 12px;" class="text50" id="tbCrewGroupFilter" name="tbCrewGroupFilter" />
                                                            <input type="button" class="browse-button" id="btnCrewGroupFilter" value="" name="btnCrewGroupFilter" />
                                                            <input id="hdnCrewGroupId" type="hidden" value="" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                             <div id="errorMsg1" class="hideDiv">Invalid Crew Group.!</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td valign="top" class="tdLabel100">
                                            <input type="button" class="button" id="btnSearch" onclick="reloadCrew(); return false;" value="Search" name="btnSearch" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table id="gridCrew" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                    <tr style="min-height:15px">
                      <td>
                        <div id="divMsg1" style="text-align:left" class="hideDiv">Please select one (1) Crew to edit.</div>                            
                      </td>
                    </tr>
                </table>
            </div>
        </div>
    
</body>
</html>
