﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PreflightLegacy.Master"
    EnableViewState="true" AutoEventWireup="true" CodeBehind="PreFlightUVServices.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreFlightUVServices" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PreflightLegacy.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function callBackFnToReload() {
                __doPostBack('__Page');
            }

            function ConfirmClear() {
                radconfirm("Are you sure you want to CLEAR all the Trip Request default values?", confirmUpdateDefaultClear, 300, 110, null, "FlightPak System");
            }

            function confirmUpdateDefaultClear(arg) {
                if (arg == true)
                    document.getElementById('<%=btnUpdateDefaultClearYes.ClientID%>').click();
            }

            function openWin(radWin) {

                var url = '';
                if (radWin == "RadRetrievePopup") {
                    url = 'PreflightSearch.aspx';
                }
                if (radWin == "rdSIFL") {
                    url = '/Views/Transactions/Preflight/PreflightSIFL.aspx';
                }
                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/Preflight/CopyTrip.aspx';
                }
                if (radWin == "rdMoveTrip") {
                    url = '/Views/Transactions/Preflight/MoveTrip.aspx';
                }
                if (radWin == "rdPreflightAPIS") {
                    url = "/Views/Transactions/Preflight/PreflightAPIS.aspx?APIS=" + "APIS";
                }
                if (radWin == "rdHistory") {
                    url = "../../Transactions/History.aspx?FromPage=" + "Preflight";
                }
                // PROD-38
                if (radWin == "rdPreflightEMAIL") {
                    url = '/Views/Transactions/Preflight/EmailTrip.aspx';
                }
                if (url != "")
                    var oWnd = radopen(url, radWin);

                if (radWin == "rdTravelSense") {
                    var left = (screen.width / 2) - (2 / 2);
                    var top = (screen.height / 2) - (2 / 2);
                    var test = window.open("/Views/Transactions/Preflight/PreflightTravelSense.aspx", "Travelsense", "width=2px,height=2px,top=" + top + ", left=" + left);
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            // this function is used to the refresh the  grid
            function refreshGrid(arg) {
                $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest('Rebind');

            }

            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }

            function gridCreated(sender, args) {
                if (sender.get_editIndexes && sender.get_editIndexes().length > 0) {
                    document.getElementById("OutPut").innerHTML = sender.get_editIndexes().join();
                }
                else {
                    document.getElementById("OutPut").innerHTML = "";
                }
            }

            function fnValidateTrip(varTripID) {
                var lbl = document.getElementById("<%=lblTripManualSubmit.ClientID%>");
                if (lbl.nodeValue != null) {

                    return true;
                }
                else {
                    lbl.nodeValue = "*";
                    lbl.innerHTML = "<b><font color='red'>*</font></b>";
                    return false;
                }
            }

            function fnCloseRadWindow() {
                GetRadWindow().Close();
            }            

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="990px" Height="590px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPreflightAPIS" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="APIS" NavigateUrl="~/Views/Transactions/Preflight/PreflightAPIS.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTravelSense" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="TravelSense" NavigateUrl="~/Views/Transactions/Preflight/PreflightTravelSense.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radwindowPopup" runat="server" VisibleOnPageLoad="false" Height="240px"
                Width="380px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None">
                <ContentTemplate>
                    <div style="margin: 10px">
                        <table cellpadding="2px" cellspacing="2px">
                            <tr>
                                <td align="left">
                                    Description can be upto 200 characters
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:TextBox ID="tbDescription" runat="server" CssClass="textarea310x100" MaxLength="200"
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCancelDesc" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancelPopUp_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSaveDesc" runat="server" Text="Save" CssClass="button" OnClick="btnSavePopUp_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="radwindowTSSSubmitAlert" runat="server" Visible="false" Height="150px"
                Width="350px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None">
                <ContentTemplate>
                    <div style="margin: 10px">
                        <table cellpadding="5px" cellspacing="5px">
                            <tr align="center">
                                <td>
                                    <asp:Label ID="lblTSSManualSubmitAlert" runat="server" />
                                </td>
                            </tr>
                            <tr align="center">
                                <td>
                                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" OnClick="btnYes_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" OnClick="btnCancelManualSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="radwindowManualSubmit" runat="server" VisibleOnPageLoad="false"
                Height="150px" Width="250px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false"
                Behaviors="None">
                <ContentTemplate>
                    <div style="margin: 10px">
                        <table cellpadding="5px" cellspacing="5px">
                            <tr align="center">
                                <td>
                                    Enter Trip ID:
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="tbTripManualSubmit" runat="server" CssClass="tdtext90" MaxLength="30"
                                        TextMode="SingleLine"></asp:TextBox>
                                    <asp:Label ID="lblTripManualSubmit" runat="server" Font-Bold="true" ForeColor="Red" />
                                </td>
                            </tr>
                            <tr align="center">
                                <td>
                                    <asp:Button ID="btnSaveManualSubmit" runat="server" Text="Save" CssClass="button"
                                        OnClick="btnSaveManualSubmit_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelManualSubmit" runat="server" Text="Cancel" CssClass="button"
                                        OnClick="btnCancelManualSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="radUpdateDefaults" runat="server" VisibleOnPageLoad="false"
                Height="260px" Width="370px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false"
                Behaviors="None">
                <ContentTemplate>
                    <div style="margin: 10px">
                        <table cellpadding="5px" cellspacing="5px">
                            <tr>
                                <td align="left">
                                    Select Trip Request Default Options:
                                </td>
                            </tr>
                            <tr align="center">
                                <td>
                                    <fieldset>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkMainInfo" runat="server" Text="Main Information" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkFuel" runat="server" Text="Fuel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkHandle" runat="server" Text="Handling" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkCrewHotel" runat="server" Text="Crew Hotel Booking" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkPermits" runat="server" Text="Permits/Slots/AROs" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkPaxHotel" runat="server" Text="Pax Hotel Booking" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <asp:Button ID="btnUpdateDefaultsContinue" runat="server" Text="Continue" CssClass="button"
                                        OnClick="btnUpdateDefaultsContinue_Click" />
                                    <asp:Button ID="btnUpdateDefaultsClear" runat="server" Text="Clear" CssClass="button"
                                        OnClientClick="javascript:return ConfirmClear();" />
                                    <asp:Button ID="btnUpdateDefaultsCancel" runat="server" Text="Cancel" CssClass="button"
                                        OnClick="btnUpdateDefaultsCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="radwindowCheckStatus" runat="server" VisibleOnPageLoad="false"
                Height="300px" Width="450px" Modal="true" BackColor="#DADADA" VisibleStatusbar="false"
                Behaviors="Close">
                <ContentTemplate>
                    <div runat="server" id="divRdWndCheckStatus">
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="radGridPopUp" runat="server" VisibleOnPageLoad="false" Modal="true"
                BackColor="#DADADA" VisibleStatusbar="false" Behaviors="None" Width="770px">
                <ContentTemplate>
                    <div>
                        <telerik:RadGrid ID="dgCrewPassportVisa" EnableAJAX="True" runat="server" GridLines="None"
                            Visible="false">
                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" AllowAutomaticUpdates="false"
                                AllowSorting="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="false"
                                ClientDataKeyNames=" Name, BirthDT,  PassportNum, PassportExpiryDT, CountryName, IssueCity, PilotLicenseNum, VisaNum">
                                <PagerStyle Mode="NumericPages" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name" Visible="true"
                                        MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BirthDT" HeaderText="DOB" UniqueName="BirthDT"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PassportNum" HeaderText="Passport No." UniqueName="PassportNum"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PassportExpiryDT" HeaderText="Expires" UniqueName="PassportExpiryDT"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CountryName" HeaderText="Nation" UniqueName="CountryName"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IssueCity" HeaderText="Place of Issue" UniqueName="IssueCity"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PilotLicenseNum" HeaderText="License No." UniqueName="PilotLicenseNum"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="VisaNum" HeaderText="Visa No." UniqueName="VisaNum"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                        </telerik:RadGrid>
                        <telerik:RadGrid ID="dgPassengerPassportVisa" EnableAJAX="True" runat="server" GridLines="None"
                            Visible="false">
                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" AllowAutomaticUpdates="false"
                                AllowSorting="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="false"
                                ClientDataKeyNames=" PassengerName, DateOfBirth, PassportNum, PassportExpiryDT, CountryName, IssueCity, VisaNum">
                                <PagerStyle Mode="NumericPages" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="PassengerName" HeaderText="Name" UniqueName="PassengerName"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DateOfBirth" HeaderText="DOB" UniqueName="DateOfBirth"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PassportNum" HeaderText="Passport No." UniqueName="PassportNum"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PassportExpiryDT" HeaderText="Expires" UniqueName="PassportExpiryDT"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CountryName" HeaderText="Nation" UniqueName="CountryName"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IssueCity" HeaderText="Place of Issue" UniqueName="IssueCity"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="VisaNum" HeaderText="Visa No." UniqueName="VisaNum"
                                        Visible="true" MaxLength="30">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                    <div align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="nav-3">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnCrewCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancelGridPopUp_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div class="tblspace_10">
        </div>
        <div style="width: 718px; padding: 5px 0 0 0px;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="tdLabel90">
                        Requested by:
                    </td>
                    <td class="tdLabel120">
                        <asp:TextBox ID="tbRequestedby" runat="server" CssClass="text100" MaxLength="30"></asp:TextBox>
                    </td>
                    <td class="tdLabel90">
                        Last Submitted:
                    </td>
                    <td class="tdLabel110">
                        <asp:TextBox ID="tbLastSubmitted" runat="server" Enabled="false" CssClass="inpt_non_edit text80"></asp:TextBox>
                    </td>
                    <td class="tdLabel45">
                        Trip ID:
                    </td>
                    <td>
                        <asp:Label ID="lblTripID" runat="server" Font-Bold="true"></asp:Label>
                        <asp:LinkButton ID="lnkTripID" runat="server" OnClick="btnCheckStatus_Click" Font-Bold="true"
                            Visible="false"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <div class="mnd_text padleft_10" style="float: right; width: 115px; padding: 0px 0 0 0px;">
            NOTAMS
        </div>
        <div style="width: 718px; padding: 5px 0 0 0px;">
            <div style="float: left; width: 560px; padding: 5px 0 0 0px;">
                <fieldset>
                    <legend>
                        <asp:RadioButtonList ID="radlstUV" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="radlstUV_Changed">
                            <asp:ListItem Selected="True" Value="1">Pre-Trip</asp:ListItem>
                            <asp:ListItem Value="2">Flight Following</asp:ListItem>
                        </asp:RadioButtonList>
                    </legend>
                    <div style="float: left; width: 548px; padding: 0 0 0 0px;">
                        <asp:Panel ID="pnlPretrip" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%" style="height: 180px">
                                <tr>
                                    <td>
                                        <b>Location</b>
                                    </td>
                                    <td>
                                        <b>Delivery Method</b>
                                    </td>
                                    <td>
                                        <b>Delivery Address</b>
                                    </td>
                                    <td>
                                        <b>Attention</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlstLocation1" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlstDeliveryMethod1" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDeliveryAddress1" runat="server" CssClass="text150" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAttention1" runat="server" CssClass="text120" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlstLocation2" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlstDeliveryMethod2" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDeliveryAddress2" runat="server" CssClass="text150" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAttention2" runat="server" CssClass="text120" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlstLocation3" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlstDeliveryMethod3" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDeliveryAddress3" runat="server" CssClass="text150" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAttention3" runat="server" CssClass="text120" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlstLocation4" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlstDeliveryMethod4" runat="server" CssClass="text112">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDeliveryAddress4" runat="server" CssClass="text150" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbAttention4" runat="server" CssClass="text120" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                    <div style="float: left; width: 548px; padding: 0 0 0 0px;">
                        <asp:Panel ID="pnlFlightfollowing" runat="server">
                            <table width="100%" style="height: 180px" cellpadding="1px" cellspacing="2px">
                                <tr>
                                    <td colspan="2" class="tdLabel100">
                                        <b></b>
                                    </td>
                                    <td>
                                        <b>Delivery Address</b>
                                    </td>
                                    <td>
                                        <b>Attention</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" rowspan="4">
                                        <asp:RadioButtonList ID="radlstFlightFollowing" runat="server" RepeatDirection="Vertical">
                                            <asp:ListItem Value="1">Dom.Legs</asp:ListItem>
                                            <asp:ListItem Value="2">Intl.Legs</asp:ListItem>
                                            <asp:ListItem Value="3">All Legs</asp:ListItem>
                                            <asp:ListItem Value="0" Selected="True">None</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td valign="top">
                                        <asp:CheckBox ID="chkFax" runat="server" Text="Fax" />
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffdeliveryAddress1" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffattention1" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkEmail" runat="server" Text="E-mail" />
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffdeliveryAddress2" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffattention2" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkPhone" runat="server" Text="Phone" />
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffdeliveryAddress3" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffattention3" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkSITA" runat="server" Text="SITA/AIRINC" />
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffdeliveryAddress4" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="tbffattention4" runat="server" CssClass="text150"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </fieldset>
            </div>
            <div style="float: right; width: 150px; padding: 5px 0 0 0px;">
                <div style="float: right; width: 150px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Leg Settings</legend>
                        <asp:RadioButtonList ID="radlstLegSettings" runat="server" RepeatDirection="Vertical">
                            <asp:ListItem Value="1">Domestic Legs</asp:ListItem>
                            <asp:ListItem Value="2">International Legs</asp:ListItem>
                            <asp:ListItem Value="3">All Legs</asp:ListItem>
                            <asp:ListItem Value="4" Selected="True">None</asp:ListItem>
                        </asp:RadioButtonList>
                    </fieldset>
                </div>
                <div style="float: right; width: 150px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Type</legend>
                        <asp:RadioButtonList ID="radlstType" runat="server" RepeatDirection="Vertical">
                            <asp:ListItem Value="1">Plain Language</asp:ListItem>
                            <asp:ListItem Value="2" Selected="True">Standard</asp:ListItem>
                        </asp:RadioButtonList>
                        <div style="float: right; height: 4px; padding: 5px 0 0 0px;">
                        </div>
                    </fieldset>
                </div>
            </div>
            <div style="width: 718px; padding: 5px 0 0 0px;">
                <div style="float: left; width: 550px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>FAR</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="radlstFAR" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Selected="True">91</asp:ListItem>
                                        <asp:ListItem Value="2">121</asp:ListItem>
                                        <asp:ListItem Value="3">125</asp:ListItem>
                                        <asp:ListItem Value="4">135</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkSuppressCrew" runat="server" Text="SuppressCrew/PAX Detail on All Legs" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: right; width: 168px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Permits</legend>
                        <table style="height: 43px" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkPermitsOverflight" runat="server" Text="Overflight" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel75">
                                    <asp:CheckBox ID="chkPermitsLanding" runat="server" Text="Landing" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: left; width: 550px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Handling</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkHandling" runat="server" Text="Handling" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkArrivalDepartureDeclarationForm" runat="server" Text="Arrival/Departure Declaration Form" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: right; width: 168px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Prefile</legend>
                        <asp:CheckBox ID="chkPrefileAllLegs" runat="server" Text="Prefile" />
                        <div style="float: right; height: 32px; padding: 5px 0 0 0px;">
                        </div>
                    </fieldset>
                </div>
                <div style="float: left; width: 550px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Security</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkSecurityPassenger" runat="server" Text="PAX" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkSecurityAircraft" runat="server" Text="Aircraft" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: right; width: 168px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Visas</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkVisasPassenger" runat="server" Text="PAX" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkVisasCrew" runat="server" Text="Crew" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: left; width: 550px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Hotels</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkHotelsPassenger" runat="server" Text="PAX" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkHotelsCrew" runat="server" Text="Crew" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: right; width: 168px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Fuel</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="ChkUVairFuel" runat="server" Text="UVair Fuel" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="ChkUVairFuelQuote" runat="server" Text="UVair Fuel Quote" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: left; width: 550px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Others</legend>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkCustoms" runat="server" Text="Customs" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkServicePayment" runat="server" Text="Payment/Credit for Services" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
        <div style="width: 718px; padding: 5px 0 0 0px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="tdLabel120" align="center" valign="top">
                        Special Instructions
                    </td>
                    <td class="pr_radtextbox_590">
                        <telerik:RadTextBox ID="tbFBODepartFBOInfo" runat="server" TextMode="MultiLine" />
                    </td>
                </tr>
            </table>
            <div runat="server" class="nav-6">
            </div>
        </div>
        <div style="width: 718px; background-color: #F6F6F6;">
            <telerik:RadPanelBar ID="pnlLegInfo" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Leg Info" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <div class="box-bg-normal-pad" style="float: left; width: 718px;">
                                        <div class="Windows7" style="float: left; width: 706px;">
                                            <telerik:RadTabStrip Skin="Windows7" AutoPostBack="true" ID="rtsLegs" OnTabClick="rtsLegs_TabClick"
                                                runat="server" ReorderTabsOnSelect="true">
                                                <Tabs>
                                                    <telerik:RadTab Text="Leg1" Selected="true">
                                                    </telerik:RadTab>
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                        </div>
                                        <div class="border-box" style="float: left; width: 695px;">
                                            <div style="float: left; width: 290px;">
                                                <asp:RadioButtonList ID="radlstLegsInfo" runat="server" RepeatDirection="Horizontal"
                                                    AutoPostBack="true" OnSelectedIndexChanged="radlstLegsInfo_Changed">
                                                    <asp:ListItem Selected="True" Value="1">Handling</asp:ListItem>
                                                    <asp:ListItem Value="2">Permits/Slots/AROs</asp:ListItem>
                                                    <asp:ListItem Value="3">Fuel</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div style="float: right;">
                                                <table class="preflight-box" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel75">
                                                            Depart Date
                                                        </td>
                                                        <td class="tdLabel75">
                                                            <asp:Label ID="lblDepartDate" Text="08/30/2012" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="right" class="tdLabel85">
                                                            Depart ICAO
                                                        </td>
                                                        <td class="tdLabel65" align="center">
                                                            <asp:Label ID="lblDepartIcao" Text="MHTG" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Arrival Date
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblArrivalDate" Text="02/04/2011" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            Arrival ICAO
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblArrivalIcao" Text="KHOU" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="nav-space">
                                                </div>
                                            </div>
                                            <div class="border-box" style="float: left; width: 683px;">
                                                <asp:Panel ID="pnlHandler" runat="server">
                                                    <div style="float: left; width: 680px;">
                                                        <div style="float: left; width: 680px;">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel150" align="left">
                                                                        Selected Arrival Handler
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="tbSelectedArrivalHandler" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="nav-space">
                                                            </div>
                                                        </div>
                                                        <div style="float: left; width: 490px;">
                                                            <fieldset id="fldsetLegChk" runat="server">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkUniversalArrangesHandling" runat="server" Text="Universal Arranges Handling" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkPaxSecurity" runat="server" Text="PAX Security" OnCheckedChanged="chkPaxSecurity_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                            <asp:Button ID="imgbtnPaxSecurity" runat="server" CssClass="note-icon" OnClick="imgbtnPaxSecurity_Click" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkUniversalArrangesHandlingCustomers" runat="server" Text="Universal Arranges US/Canadian Customs" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkAircraftSecurity" runat="server" Text="Aircraft Security" OnCheckedChanged="chkAircraftSecurity_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                            <asp:Button ID="imgbtnAircraftSecurity" runat="server" CssClass="note-icon" OnClick="imgbtnAircraftSecurity_Click" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkHomebaseFlightFollowing" runat="server" Text="Home Base Flight Following Required" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkPaxPassportVisas" runat="server" Text="PAX Passport/Visas" OnCheckedChanged="chkPaxPassportVisas_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                            <asp:Button ID="imgbtnPaxPassportVisas" runat="server" CssClass="note-icon" OnClick="imgbtnPaxPassportVisas_Click" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkWideBodyHandling" runat="server" Text="Wide Body Handling" AutoPostBack="true"
                                                                                OnCheckedChanged="chkWideBodyHandling_CheckedChanged" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:CheckBox ID="chkCrewPassportVisas" runat="server" Text="Crew Passport/Visas"
                                                                                OnCheckedChanged="chkCrewPassportVisas_CheckedChanged" AutoPostBack="true" />
                                                                            <asp:Button ID="imgbtnCrewPassportVisas" runat="server" CssClass="note-icon" OnClick="imgbtnCrewPassportVisas_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </div>
                                                        <div style="float: right; width: 180px;">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                        <asp:CheckBox ID="chkCatering" runat="server" Font-Bold="true" Text="Catering Order"
                                                                            OnCheckedChanged="chkCatering_CheckedChanged" AutoPostBack="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="pr_radtextbox_180">
                                                                        <telerik:RadTextBox ID="tbCatering" runat="server" TextMode="MultiLine" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="nav-space">
                                                            </div>
                                                        </div>
                                                        <div style="float: left; width: 680px;">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr class="inpt_non_edit_uwa">
                                                                    <td class="tdLabel100" align="left">
                                                                        <asp:Label ID="Label1" runat="server" Text="Selected" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td align="left" class="tdLabel250">
                                                                        <asp:Label ID="Label2" runat="server" Text="Description" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="Label3" runat="server" Text="Additional Instructions" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="box_outline_uwa" style="float: left; width: 680px;">
                                                            <table width="100%" cellpadding="1" cellspacing="1">
                                                                <tr>
                                                                    <td align="left" class="tdLabel90">
                                                                        <asp:CheckBox ID="chkAPMechanic" runat="server" />
                                                                    </td>
                                                                    <td align="left" class="tdLabel240">
                                                                        <asp:Label ID="lblAPMechanic" runat="server" Text="A&P Mechanic"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAPMechanic" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="tdLabel60">
                                                                        <asp:CheckBox ID="chkAIRStartCart" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblAIRStartCart" runat="server" Text="Air-Start Cart"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAIRStartCart" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkCleaning" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblCleaning" runat="server" Text="Aircraft Cleaning"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCleaning" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trBELTLOADER" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkBELTLOADER" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblBELTLOADER" runat="server" Text="BELTLOADER"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbBELTLOADER" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCONTAINERLOADERS" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkCONTAINERLOADERS" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblCONTAINERLOADERS" runat="server" Text="CONTAINER LOADERS"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCONTAINERLOADERS" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkCrewTransportation" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblCrewTransportation" runat="server" Text="Crew Transportation"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCrewTransportation" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkDishCleaning" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblDishCleaning" runat="server" Text="Dish Cleaning"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbDishCleaning" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trDOLLIES" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkDOLLIES" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblDOLLIES" runat="server" Text="DOLLIES"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbDOLLIES" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFREIGHTHANDLING" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkFREIGHTHANDLING" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblFREIGHTHANDLING" runat="server" Text="FREIGHT HANDLING"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbFREIGHTHANDLING" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkGupAup" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblGupAup" runat="server" Text="GPU"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbGupAup" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkGROUNDSTAIRS" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblGROUNDSTAIRS" runat="server" Text="Ground Stairs"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbGROUNDSTAIRS" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkHangar" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblHangar" runat="server" Text="Hangar"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbHangar" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKLOADER" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkKLOADER" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblKLOADER" runat="server" Text="K-LOADER"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbKLOADER" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkLadder" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblLadder" runat="server" Text="Ladder"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbLadder" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkLaundry" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblLaundry" runat="server" Text="Laundry"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbLaundry" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkLavAndWater" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblLavAndWater" runat="server" Text="Lav and Water"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbLavAndWater" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trLD3DOLLIES" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkLD3DOLLIES" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblLD3DOLLIES" runat="server" Text="LD3 DOLLIES"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbLD3DOLLIES" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trLD6DOLLIES" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkLD6DOLLIES" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblLD6DOLLIES" runat="server" Text="LD6 DOLLIES"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbLD6DOLLIES" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trLOADINGEQUIPMENT" runat="server" visible="false">
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkLOADINGEQUIPMENT" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblLOADINGEQUIPMENT" runat="server" Text="LOADING EQUIPMENT"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbLOADINGEQUIPMENT" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkMiscellaneous" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblMiscellaneous" runat="server" Text="Miscellaneous"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbMiscellaneous" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkNavigator" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblNavigator" runat="server" Text="Navigator"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbNavigator" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkNavigatorArrangements" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblNavigatorArrangements" runat="server" Text="Navigator Arrangements"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbNavigatorArrangements" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkNewspapers" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblNewspapers" runat="server" Text="Newspapers"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbNewspapers" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkOvertime" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblOvertime" runat="server" Text="Overtime"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOvertime" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkOxygen" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblOxygen" runat="server" Text="Oxygen"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOxygen" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkParking" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblParking" runat="server" Text="Parking"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbParking" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkPaxTransportation" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblPaxTransportation" runat="server" Text="PAX Transportation"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPaxTransportation" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkTowbarTug" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblTowbarTug" runat="server" Text="Towbar/Tug"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbTowbarTug" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkVipLounge" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblVipLounge" runat="server" Text="VIP Lounge"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbVipLounge" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkWeaponsClearance" runat="server" />
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblWeaponsClearance" runat="server" Text="Weapons Clearance"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbWeaponsClearance" runat="server" CssClass="text200"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlPermits" runat="server">
                                                    <div style="float: left; width: 680px;">
                                                        <div style="float: left; width: 680px;">
                                                            <div style="float: left; width: 340px;">
                                                                <fieldset>
                                                                    <legend>Landing Permits</legend>
                                                                    <table cellpadding="0" cellspacing="0" style="height: 50px;">
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:RadioButtonList ID="radlstLandingPermits" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="1" Selected="True">Universal Arranged</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Client Arranged</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Client Arranged Permit No :
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbClientArragnedPermitNo" runat="server" MaxLength="15"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </div>
                                                            <div style="float: right; width: 340px;">
                                                                <fieldset>
                                                                    <legend>Airport Slots/AROs</legend>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                Depart:
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radlstDepart" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="1" Selected="True">Universal Arranged</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Client Arranged</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                Arrive:
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="radlstArrive" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="1" Selected="True">Universal Arranged</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Client Arranged</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; width: 680px;">
                                                            <fieldset>
                                                                <legend>Overflight Permits</legend>
                                                                <asp:RadioButtonList ID="radlstOverflightPermits" runat="server" RepeatDirection="Horizontal"
                                                                    OnSelectedIndexChanged="radlstOverflightPermits_Changed" AutoPostBack="true">
                                                                    <asp:ListItem Value="1" Selected="True">Universal Arranged</asp:ListItem>
                                                                    <asp:ListItem Value="2">Client Arranged</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" visible="true" style="float: left; width: 680px;" id="divPermitDetails">
                                                            <fieldset>
                                                                <legend>Permit Details</legend>
                                                                <telerik:RadGrid ID="dgPermitDetails" EnableAJAX="True" runat="server" GridLines="None"
                                                                    AllowPaging="false" AllowAutomaticUpdates="false" AllowAutomaticInserts="false"
                                                                    AllowAutomaticDeletes="false" AllowSorting="true" AutoGenerateColumns="false"
                                                                    OnNeedDataSource="dgPermitDetails_BindData" OnInsertCommand="dgPermitDetails_InsertCommand"
                                                                    OnUpdateCommand="dgPermitDetails_UpdateCommand" OnItemCommand="dgPermitDetails_ItemCommand"
                                                                    OnEditCommand="dgPermitDetails_EditCommand" OnDeleteCommand="dgPermitDetails_DeleteCommand"
                                                                    OnItemDataBound="dgPermitDetails_ItemDataBound" AllowCustomPaging="false">
                                                                    <MasterTableView ClientDataKeyNames=" UWAPermit1, LegID,  PermitID, Country, PermitNumber, ValidDate"
                                                                        DataKeyNames="UWAPermit1" CommandItemDisplay="Top" EditMode="InPlace" AllowFilteringByColumn="false"
                                                                        ShowGroupFooter="false">
                                                                        <PagerStyle Visible="false" />
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn DataField="LegID" HeaderText="LegID" SortExpression="LegID"
                                                                                UniqueName="LegID" Display="false" MaxLength="15">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="PermitID" HeaderText="PermitID" SortExpression="PermitID"
                                                                                UniqueName="PermitID" Display="false" MaxLength="15">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Country" UniqueName="Country" HeaderStyle-Width="80px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("Country") %>' Width="100px"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="tbCountry" runat="server" CssClass="text100" Text='<%#Eval("Country") %>'
                                                                                        MaxLength="30"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="tbCountry"
                                                                                        ErrorMessage="*" ForeColor="Red" Font-Bold="true" />
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Permit No." UniqueName="PermitNumber" HeaderStyle-Width="100px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblPermitNumber" runat="server" Text='<%#Eval("PermitNumber") %>'
                                                                                        Width="120px"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="tbPermitNumber" runat="server" CssClass="text120" Text='<%#Eval("PermitNumber") %>'
                                                                                        MaxLength="20"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="rfvPermitNumber" runat="server" ControlToValidate="tbPermitNumber"
                                                                                        ErrorMessage="*" ForeColor="Red" Font-Bold="true" />
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Valid Date" UniqueName="ValidDate" HeaderStyle-Width="80px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblValidDate" runat="server" Text='<%#Eval("ValidDate","{0:" + Master.DatePicker.DateInput.DateFormat + "}") %>'
                                                                                        Width="100px"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="tbValidDate" runat="server" CssClass="text100" Text='<%#Eval("ValidDate","{0:" + Master.DatePicker.DateInput.DateFormat + "}") %>'
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onBlur="parseDate(this, event);"
                                                                                        onkeydown="return tbDate_OnKeyDown(this, event);" onclick="showPopup(this, event);"></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                                AutoPostBackOnFilter="true">
                                                                                <ItemStyle Width="50px" HorizontalAlign="Justify" />
                                                                                <HeaderStyle Width="30px" />
                                                                            </telerik:GridEditCommandColumn>
                                                                            <telerik:GridButtonColumn ConfirmText="Delete this permit?" ConfirmDialogType="RadWindow"
                                                                                ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                                                                                UniqueName="DeleteColumn">
                                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" Width="30px" />
                                                                                <HeaderStyle Width="20px" />
                                                                            </telerik:GridButtonColumn>
                                                                            <telerik:GridBoundColumn DataField="UWAPermit1" HeaderText="UWAPermitID" SortExpression="UWAPermit1"
                                                                                UniqueName="UWAPermit1" Display="false" MaxLength="15">
                                                                            </telerik:GridBoundColumn>
                                                                        </Columns>
                                                                        <EditFormSettings EditColumn-InsertText="Ïnsert Permit" ColumnNumber="6" CaptionDataField="Country"
                                                                            CaptionFormatString="Edit properties of permit {0}" InsertCaption="New Permit">
                                                                            <FormTableStyle CellSpacing="0" CellPadding="2" Height="110px" BackColor="White" />
                                                                            <EditColumn ButtonType="ImageButton" InsertText="Insert permit" UpdateText="Update record"
                                                                                UniqueName="EditCommandColumn1" CancelText="Cancel edit" AutoPostBackOnFilter="true"
                                                                                Reorderable="true">
                                                                            </EditColumn>
                                                                            <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                                                        </EditFormSettings>
                                                                        <CommandItemSettings ShowRefreshButton="false" AddNewRecordText="Add Permit" />
                                                                    </MasterTableView>
                                                                    <ClientSettings EnablePostBackOnRowClick="false" AllowRowsDragDrop="false" AllowColumnsReorder="false"
                                                                        ReorderColumnsOnClient="true">
                                                                        <Scrolling AllowScroll="false" UseStaticHeaders="false" />
                                                                        <ClientEvents OnRowDblClick="RowDblClick" />
                                                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                                    </ClientSettings>
                                                                </telerik:RadGrid>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlFuel" runat="server">
                                                    <div style="float: left; width: 680px;">
                                                        <div style="float: left; width: 680px;">
                                                            <div style="float: left; width: 400px;">
                                                                <fieldset>
                                                                    <legend>UVair</legend>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkUWAairFuel" runat="server" Text="UVair Fuel" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkUWAairFuelQuote" runat="server" Text="UVair Fuel Quote Required" />
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkPristRequired" runat="server" Text="Prist Required (When Available)" />
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </div>
                                                            <div style="float: right; width: 280px;">
                                                                <fieldset>
                                                                    <legend>Fuel On</legend>
                                                                    <asp:RadioButtonList ID="radlstFuelOn" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1" Selected="True">Arrival</asp:ListItem>
                                                                        <asp:ListItem Value="2">Departure</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                    <div style="float: right; height: 31px;">
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; width: 680px;">
                                                            <fieldset>
                                                                <legend>Special Instructions</legend>
                                                                <asp:TextBox ID="tbFuelSplInstructions" runat="server" TextMode="MultiLine" CssClass="textarea480x50"></asp:TextBox>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
        </div>
        <div style="width: 718px; padding: 5px 0 0 0px;">
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="button" OnClick="btnReset_Click" />
            <asp:Button ID="btnEditTrip" runat="server" Text="Edit Trip" OnClick="btnEditTrip_Click"
                CssClass="button" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
            <asp:Button ID="btnUpdateDefaults" runat="server" Text="Update Defaults" CssClass="button"
                OnClick="btnUpdateDefaultsSubmit_Click" />
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
            <asp:Button ID="btnManualSubmit" runat="server" Text="Manual Submit" CssClass="button"
                OnClick="btnManualSubmit_Click" />
            <asp:Button ID="btnCheckStatus" runat="server" Text="Trip Status" CssClass="button"
                OnClick="btnCheckStatus_Click" />
            <asp:Button ID="btnSubmittoUVTSS" runat="server" Font-Bold="true" Text="Submit to UWA TSS"
                CssClass="button" OnClick="btnSubmittoUVTSS_Click" />
        </div>
        <input type="hidden" runat="server" id="hdnLeg" />
        <input type="hidden" runat="server" id="hdnLegID" />
        <input type="hidden" runat="server" id="hdnPaxSecurityNotes" />
        <input type="hidden" runat="server" id="hdnAircraftSecurityNotes" />
    </div>
    <table id="tblHidden" style="display: none;">
        <tr>
            <td>
                <asp:Button ID="btnUpdateDefaultClearYes" runat="server" Text="Button" OnClick="btnUpdateDefaultClearYes_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
