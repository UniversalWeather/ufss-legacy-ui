﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightPassengerPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightPassengerPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                function OnClientClick(strPanelToExpand) {
                    var PanelBar1 = $find("<%= pnlMain.ClientID %>");
                    var PanelBar2 = $find("<%= pnlAdditionalInfo.ClientID %>");
                    var PanelBar3 = $find("<%= pnlVisa.ClientID %>");
                    var PanelBar4 = $find("<%= Notes.ClientID %>");
                    var PanelBar5 = $find("<%= PaxItems.ClientID %>");
                    var PanelBar6 = $find("<%= pnlPassport.ClientID %>");
                    var PanelBar7 = $find("<%= pnlImage.ClientID %>");
                    PanelBar1.get_items().getItem(0).set_expanded(false);
                    PanelBar2.get_items().getItem(0).set_expanded(false);
                    PanelBar3.get_items().getItem(0).set_expanded(false);
                    PanelBar4.get_items().getItem(0).set_expanded(false);
                    PanelBar5.get_items().getItem(0).set_expanded(false);
                    PanelBar6.get_items().getItem(0).set_expanded(false);
                    PanelBar7.get_items().getItem(0).set_expanded(false);
                    if (strPanelToExpand == "AddInfo") {
                        PanelBar2.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "Visa") {
                        PanelBar3.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "Notes") {
                        PanelBar4.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "Pax Alerts") {
                        PanelBar5.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "Passport") {
                        PanelBar6.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "Image") {
                        PanelBar7.get_items().getItem(0).set_expanded(true);
                    }
                    return false;
                }
                function openWin(radWin) {
                    var url = '';
                    if (radWin == "RadNationalityMasterPopup") {
                        url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbNationality.ClientID%>').value;
                    }
                    else if (radWin == "RadCountryMasterPopup") {
                        url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCityOfResi.ClientID%>').value;
                    }
                    else if (radWin == "RadClientCodeMasterPopup") {
                        url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCde.ClientID%>').value;
                    }
                    else if (radWin == "radAirportPopup") {
                        url = '../Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                    }
                    else if (radWin == "RadFlightPurposeMasterPopup") {
                        url = '../People/FlightPurposePopup.aspx?FlightPurposeCD=' + document.getElementById('<%=tbFlightPurpose.ClientID%>').value;
                    }
                    else if (radWin == "RadDepartmentMasterPopup") {
                        url = '../Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('<%=tbDepartment.ClientID%>').value;
                    }
                    else if (radWin == "RadAuthorizationMasterPopup") {
                        url = '../Company/AuthorizationPopup.aspx?AuthorizationCD=' + document.getElementById('<%=tbAuth.ClientID%>').value + '&deptId=' + document.getElementById('<%=hdnDepartment.ClientID%>').value;
                    }
                    else if (radWin == "radAccountMasterPopup") {
                        url = '../Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbAccountNumber.ClientID%>').value;
                    }
                    else if (radWin == "RadAssociateMasterPopup") {
                        url = '../People/PassengerAssociatePopup.aspx?PassengerRequestorCD=' + document.getElementById('<%=tbAssociated.ClientID%>').value;
                    }
                    else if (radWin == "RadAddlInfo") {
                        url = '../People/PassengerAddInfoPopup.aspx';
                    }
                    else if (radWin == "RadSearchClientCodeMasterPopup") {
                        url = '../Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbSearchClientCode.ClientID%>').value;
                    }
                    else if (radWin == "RadExportData") {
                        url = "../../Reports/ExportReportInformation.aspx?Report=RptDBPassengerRequestorIExport&PassengerCD=" + document.getElementById('<%=tbCode.ClientID%>').value + "&UserCD=UC";
                    }
                    var oWnd = radopen(url, radWin);
                }
                function ShowAddlInfoPopup() {
                    window.radopen("../People/PassengerAddInfoPopup.aspx", "RadAddlInfo");
                    return false;
                }
                function confirmCallBackFn(arg) {
                    if (arg == false) {
                        window.location.href = "PassengerCatalog.aspx";
                    }
                    else {
                        return false;
                    }
                }
                function ValidateEmptyAircraftTextbox(ctrlID, e) {
                    if (ctrlID.value == "") {
                        ctrlID.value = "0.00";
                    }
                }
                function ValidateEmptyTextbox(ctrlID, e) {
                    if (ctrlID.value == "") {
                        ctrlID.value = "0.0";
                    }
                }
                function ValidateEmptyLatTextbox(ctrlID, e) {
                    if (ctrlID.value == "") {
                        ctrlID.value = "0";
                    }
                }
                function ValidateEmptyTextbox(ctrlID, e) {
                    if (ctrlID.value == "") {
                        ctrlID.value = "00.0";
                    }
                }
                function fncClientCheckDate(sender, args) {
                    var DateOfBirth;
                    DateOfBirth = new Date(document.getElementById('<%= ucDateOfBirth.FindControl("tbDate").ClientID %>').value);
                    var Today = new Date();
                    if (DateOfBirth >= Today) {
                        args.IsValid = false;
                        return;
                    }
                    args.IsValid = true;
                }
                var currentTextBox = null;
                var currentDatePicker = null;
                //This method is called to handle the onclick and onfocus client side events for the texbox
                function showPopup(sender, e) {
                    //this is a reference to the texbox which raised the event
                    //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html
                    currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);
                    //this gets a reference to the datepicker, which will be shown, to facilitate
                    //the selection of a date
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    //this variable is used to store a reference to the date picker, which is currently
                    //active
                    currentDatePicker = datePicker;
                    //this method first parses the date, that the user entered or selected, and then
                    //sets it as a selected date to the picker
                    datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));
                    //the code lines below show the calendar, which is used to select a date. The showPopup
                    //function takes three arguments - the x and y coordinates where to show the calendar, as
                    //well as its height, derived from the offsetHeight property of the textbox
                    var position = datePicker.getElementPosition(currentTextBox);
                    datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                }
                //this handler is used to set the text of the TextBox to the value of selected from the popup
                function dateSelected(sender, args) {
                    if (currentTextBox != null) {
                        //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                        //value of the picker
                        currentTextBox.value = args.get_newValue();
                    }
                }
                function ToUpper(ControlName) {
                    var Value = ControlName.value;
                    ControlName.value = Value.toUpperCase();
                }

                function tbDate_OnKeyDown(sender, event) {
                    if (event.keyCode == 9) {
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        datePicker.hidePopup();
                        return true;
                    }
                }
       
            </script>
            <script type="text/javascript">
                function ConfirmClose(WinName) {
                    var oManager = GetRadWindowManager();
                    var oWnd = oManager.GetWindowByName(WinName);
                    //Find the Close button on the page and attach to the
                    //onclick event
                    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                    CloseButton.onclick = function () {
                        CurrentWinName = oWnd.Id;
                        //radconfirm is non-blocking, so you will need to provide a callback function
                        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                    }
                }
                function OnClientCloseNationalityPopup(oWnd, args) {
                    var combo = $find("<%= tbNationality.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbNationality.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=tbNationalityName.ClientID%>").innerHTML = arg.CountryName;
                            document.getElementById("<%=hdnNationality.ClientID%>").value = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbNationality.ClientID%>").value = "";
                            document.getElementById("<%=tbNationalityName.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnNationality.ClientID%>").value = arg.CountryName;
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseCountryPopup(oWnd, args) {
                    var combo = $find("<%= tbCityOfResi.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbCityOfResi.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=tbCityOfResiName.ClientID%>").innerHTML = arg.CountryName;
                            document.getElementById("<%=hdnCityOfResi.ClientID%>").value = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbCityOfResi.ClientID%>").value = "";
                            document.getElementById("<%=tbCityOfResiName.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnCityOfResi.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseClientCodePopup(oWnd, args) {
                    var combo = $find("<%= tbClientCde.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbClientCde.ClientID%>").value = arg.ClientCD
                            document.getElementById("<%=hdnClientCde.ClientID%>").value = arg.ClientID
                        }
                        else {
                            document.getElementById("<%=tbClientCde.ClientID%>").value = "";
                            document.getElementById("<%=hdnClientCde.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseHomeBasePopup(oWnd, args) {
                    var combo = $find("<%= tbHomeBase.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdnHomeBase.ClientID%>").value = arg.ICAO;
                        }
                        else {
                            document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                            document.getElementById("<%=hdnHomeBase.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseFlightPurposePopup(oWnd, args) {
                    var combo = $find("<%= tbFlightPurpose.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbFlightPurpose.ClientID%>").value = arg.FlightPurposeCD;
                            document.getElementById("<%=hdnFlightPurpose.ClientID%>").value = arg.FlightPurposeID;
                        }
                        else {
                            document.getElementById("<%=tbFlightPurpose.ClientID%>").value = "";
                            document.getElementById("<%=hdnFlightPurpose.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseDepartmentPopup(oWnd, args) {
                    var combo = $find("<%= tbDepartment.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                            document.getElementById("<%=tbDepartmentDesc.ClientID%>").value = arg.DepartmentName;
                            document.getElementById("<%=tbAuth.ClientID%>").value = "";
                            document.getElementById("<%=tbAuthDesc.ClientID%>").value = "";
                            document.getElementById("<%=hdnDepartment.ClientID%>").value = arg.DepartmentID;
                            document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                        }
                        else {
                            document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartmentDesc.ClientID%>").value = "";
                            document.getElementById("<%=tbAuth.ClientID%>").value = "";
                            document.getElementById("<%=tbAuthDesc.ClientID%>").value = "";
                            document.getElementById("<%=hdnDepartment.ClientID%>").value = "";
                            document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseAuthorizationPopup(oWnd, args) {
                    var combo = $find("<%= tbAuth.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbAuth.ClientID%>").value = arg.AuthorizationCD;
                            document.getElementById("<%=tbAuthDesc.ClientID%>").value = arg.DeptAuthDescription;
                            document.getElementById("<%=hdnAuth.ClientID%>").value = arg.AuthorizationID;
                            CheckDeptAuthorization();
                        }
                        else {
                            document.getElementById("<%=tbAuth.ClientID%>").value = "";
                            document.getElementById("<%=tbAuthDesc.ClientID%>").value = "";
                            document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseAccountNumberPopup(oWnd, args) {
                    var combo = $find("<%= tbAccountNumber.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbAccountNumber.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%=hdnAccountNumber.ClientID%>").value = arg.AccountNum;
                        }
                        else {
                            document.getElementById("<%=tbAccountNumber.ClientID%>").value = "";
                            document.getElementById("<%=hdnAccountNumber.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseAssociatePopup(oWnd, args) {
                    var combo = $find("<%= tbAssociated.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbAssociated.ClientID%>").value = arg.PassengerRequestorCD;
                            document.getElementById("<%=hdnAssociated.ClientID%>").value = arg.PassengerRequestorID;
                        }
                        else {
                            document.getElementById("<%=tbAssociated.ClientID%>").value = "";
                            document.getElementById("<%=hdnAssociated.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function OnClientCloseSearchClientCodePopup(oWnd, args) {
                    var combo = $find("<%= tbSearchClientCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbSearchClientCode.ClientID%>").value = arg.ClientCD;
                        }
                        else {
                            document.getElementById("<%=tbSearchClientCode.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                    document.getElementById("<%=tbSearchClientCode.ClientID%>").fireEvent("onchange");
                }
                function OnClientVisaCountryPopup(oWnd, args) {
                    var combo = GControlId;
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            GControlId.value = arg.CountryCD;
                            GhdnControlId.value = arg.CountryID;
                        }
                        else {
                            GControlId.value = "";
                            GhdnControlId.value = "";
                            combo.clearSelection();
                        }
                    }
                }

                function OnClientReportPopup(oWnd, args) {
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdnReportParameters.ClientID%>").value = arg.Arg1;
                        }
                        else {
                            document.getElementById("<%=hdnReportParameters.ClientID%>").value = "";
                        }
                    }
                    if ((arg.Arg1 != null) && (arg.Arg1 != "")) {
                        if (document.getElementById("<%=hdnReportFormat.ClientID%>").value == "EXPORT") {
                            url = "../../Reports/ExportReportInformation.aspx";
                            var oWnd = radopen(url, 'RadExportData');
                            return false;
                        }

                    }
                }
                function ShowReports(radWin, ReportFormat, ReportName) {
                    document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                    document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                    url = "../People/PassengerRequestorsPopup.aspx?PassengerRequestorID=1";
                    var oWnd = radopen(url, radWin);
                }
                function GetDimensions(sender, args) {
                    var bounds = sender.getWindowBounds();
                    return;
                }
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }
                // this function is used to the refresh the currency grid
                function refreshGrid(arg) {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
                }
            </script>
            <script type="text/javascript">
                function EnableAssociate(TaxType) {
                    if (TaxType == 'Guest') {
                        document.getElementById('<%=tbAssociated.ClientID%>').disabled = false;
                        document.getElementById('<%=btnAssociated.ClientID%>').disabled = false;
                    }
                    else {
                        document.getElementById('<%=tbAssociated.ClientID%>').disabled = true;
                        document.getElementById('<%=btnAssociated.ClientID%>').disabled = true;
                        document.getElementById('<%=tbAssociated.ClientID%>').value = "";
                    }
                    return true;
                }
                
                var GControlId, GhdnControlId;
                function openWinGrd(radWin, ControlId) {
                    GControlId = ControlId;
                    var ControlCheck = ControlId.id;
                    if (ControlCheck.indexOf("Passport") != -1) {
                        GhdnControlId = ControlCheck.replace("tbPassportCountry", "hdnPassportCountry");
                    }
                    else {
                        GhdnControlId = ControlCheck.replace("tbCountryCD", "hdnCountryID");
                    }
                    var url = '';
                    if (radWin == "RadVisaCountryMasterPopup") {
                        url = '../Company/CountryMasterPopup.aspx?CountryCD=' + ControlId.value;
                    }
                    var oWnd = radopen(url, radWin);
                }            
            </script>
            <script type="text/javascript">
                function File_onchange() {
                    __doPostBack('__Page', 'LoadImage');
                }
                function CheckName() {
                    var nam = document.getElementById('<%=tbImgName.ClientID%>').value;
                    if (nam != "") {
                        document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                    }
                    else {
                        document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                    }
                }
                // this function is related to Image 
                function OpenRadWindow() {
                    var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                    document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                    oWnd.show();
                }
                // this function is related to Image 
                function CloseRadWindow() {
                    var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                    document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                    oWnd.close();
                }
                function uncheckOther(chk) {
                    var status = chk.checked;

                    var checkBoxes = $("input[id*='chkChoice']");

                    $.each(checkBoxes, function () {
                        $(this).attr('checked', false);
                    });

                    chk.checked = status;
                }
                function CheckPassportChoice() {
                    var grid = $find("<%=dgPassport.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var length = MasterTable.get_dataItems().length;
                    var IsPassportChoice = false;
                    var PassportNum;
                    var chkChoice;
                    for (var i = 0; i < length; i++) {
                        PassportNum = MasterTable.get_dataItems()[0].findElement("tbPassportNum").value;
                        chkChoice = MasterTable.get_dataItems()[i].findElement("chkChoice");
                        if (chkChoice.checked == true) {
                            IsPassportChoice = true;
                            PassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                            break;
                        }
                    }
                    if (document.getElementById('<%=hdnSave.ClientID%>').value == "Update" && length > 0) {
                        if (IsPassportChoice == true) {
                            Msg = "The choice passport for this passenger has changed. Would you like to update future tripsheet passenger information with the new passport number [" + PassportNum + "]?";
                        }
                        else {
                            Msg = "There is no choice passport for this Passenger. Would you like to update future tripsheet Passenger information with First Available passport number [" + PassportNum + "]?'";
                        }

                        var IsResult = confirm(Msg, "Passenger/ Requestor");
                        if (IsResult == true) {
                            document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "Yes";
                        }
                        else {
                            document.getElementById('<%=hdnIsPassportChoice.ClientID%>').value = "No";
                        }
                    }
                    return true;
                }
                function CheckPassportDuplicate(txtPassport) {
                    var PassportNumber = txtPassport.value;
                    var grid = $find("<%=dgPassport.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var length = MasterTable.get_dataItems().length;
                    var OthPassportID;
                    var OthPassportNum;
                    for (var i = 0; i < length; i++) {
                        OthPassportID = MasterTable.get_dataItems()[i].findElement("tbPassportNum");
                        OthPassportNum = MasterTable.get_dataItems()[i].findElement("tbPassportNum").value;
                        if (txtPassport.id != OthPassportID.id) {
                            if (PassportNumber == OthPassportNum) {
                                alert("Cannot add duplicate passport.", "Passenger/ Requestor");
                                txtPassport.value = "";
                                txtPassport.focus();
                                return false;
                            }
                        }
                    }
                }
                function CheckVisaDuplicate(txtVisa) {
                    var VisaNumber = txtVisa.value;
                    var grid = $find("<%=dgVisa.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var length = MasterTable.get_dataItems().length;
                    var OthVisaID;
                    var OthVisaNum;
                    for (var i = 0; i < length; i++) {
                        OthVisaID = MasterTable.get_dataItems()[i].findElement("tbVisaNum");
                        OthVisaNum = MasterTable.get_dataItems()[i].findElement("tbVisaNum").value;
                        if (txtVisa.id != OthVisaID.id) {
                            if (VisaNumber == OthVisaNum) {
                                alert("Cannot add duplicate Visa.", "Passenger/ Requestor");
                                txtVisa.value = "";
                                txtVisa.focus();
                                return false;
                            }
                        }
                    }
                }
                function tbDepartment_onchange() {
                    if (document.getElementById('<%=tbDepartment.ClientID%>').value == "") {
                        document.getElementById('<%=tbAuth.ClientID%>').value = "";
                    }
                }
                function SetFocus(control) {
                    var controlid = control.id;
                    if (controlid.indexOf('tbPassportExpiryDT') != -1) {
                        controlid = controlid.replace('tbPassportExpiryDT', 'tbIssueCity');
                    }
                    else if (controlid.indexOf('tbIssueDT') != -1) {
                        controlid = controlid.replace('tbIssueDT', 'tbPassportCountry');
                    }
                    else if (controlid.indexOf('tbExpiryDT') != -1) {
                        controlid = controlid.replace('tbExpiryDT', 'tbIssuePlace');
                    }
                    else if (controlid.indexOf('tbIssueDate') != -1) {
                        controlid = controlid.replace('tbIssueDate', 'tbNotes');
                    }
                    document.getElementById(controlid).focus();
                }
                function ValidateDate(control) {
                    var MinDate = new Date("01/01/1900");
                    var MaxDate = new Date("12/31/2100");
                    var SelectedDate;
                    if (control.value != "") {
                        SelectedDate = new Date(control.value);
                        if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                            alert("Please enter or select Date between 01/01/1900 and 12/31/2100");
                            control.value = "";
                        }
                    }
                    return false;
                }
                function CheckDeptAuthorization() {
                    if (document.getElementById('<%=tbDepartment.ClientID%>').value == "") {
                        alert("Please enter the Department Code before entering the Authorization Code.");
                        document.getElementById('<%=tbAuth.ClientID%>').value = "";
                        document.getElementById('<%=tbDepartment.ClientID%>').focus();
                        return false;
                    }
                }
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadScriptManager runat="server" ID="radScript">
        </telerik:RadScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="divExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" MinDate="01/01/1900"
            MaxDate="12/31/2100" runat="server" OnSelectedDateChanged="RadDatePicker1_SelectedDateChanged">
            <ClientEvents OnDateSelected="dateSelected" />
            <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
            </DateInput>
            <Calendar ID="Calendar1" runat="server">
                <SpecialDays>
                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                        ItemStyle-BorderStyle="Solid">
                    </telerik:RadCalendarDay>
                </SpecialDays>
            </Calendar>
        </telerik:RadDatePicker>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadNationalityMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseNationalityPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadClientCodeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseClientCodePopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadFlightPurposeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseFlightPurposePopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/FlightPurposePopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadDepartmentMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseDepartmentPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadAuthorizationMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseAuthorizationPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseAccountNumberPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadAssociateMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseAssociatePopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerAssociatePopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadAddlInfo" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadSearchClientCodeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseSearchClientCodePopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadVisaCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientVisaCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                    <ContentTemplate>
                        <div>
                            <asp:Image ID="ImgPopup" runat="server" />
                        </div>
                    </ContentTemplate>
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientReportPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <div id="DivExternalForm" runat="server" class="ExternalForm">
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                        <asp:LinkButton ID="lbtnTravelSense" Visible="false" runat="server" Text="Travel Sense"
                            OnClick="lbtnTravelSense_Click" />
                        <asp:Panel ID="pnlSearchPanel" runat="server" Visible="true">
                            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" class="status-list">
                                            <tr>
                                                <td class="tdLabel90">
                                                    <asp:CheckBox ID="chkSearchActiveOnly" Visible="false" runat="server" Text="Active Only"
                                                        OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" AutoPostBack="true" />
                                                </td>
                                                <td class="tdLabel120">
                                                    <asp:CheckBox ID="chkSearchHomebaseOnly" Visible="false" runat="server" Text="Home Base Only"
                                                        OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" AutoPostBack="true" />
                                                </td>
                                                <td class="tdLabel130">
                                                    <asp:CheckBox ID="chkSearchRequestorOnly" Visible="false" runat="server" Text="Requestor Only"
                                                        OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" AutoPostBack="true" />
                                                </td>
                                                <td class="tdLabel80">
                                                    <asp:Label ID="lbSearchClientCode" Visible="false" runat="server" Text="Client Code:" />
                                                </td>
                                                <td class="text120">
                                                    <asp:TextBox ID="tbSearchClientCode" Visible="false" runat="server" CssClass="text80"
                                                        MaxLength="5" OnTextChanged="FilterByClient_OnTextChanged" AutoPostBack="true" />
                                                    <asp:Button ID="btnSearchClientCode" Visible="false" runat="server" CssClass="browse-button"
                                                        OnClientClick="javascript:openWin('RadSearchClientCodeMasterPopup');return false;" />
                                                </td>
                                                <td>
                                                    <asp:CustomValidator ID="cvSearchClientCode" runat="server" ControlToValidate="tbSearchClientCode"
                                                        ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                </td>
                                                <td class="text80" style="display: none;">
                                                    <asp:Button ID="btnFilterClientCode" Visible="false" runat="server" CssClass="ui_nav"
                                                        Text="Search" OnClick="FilterByClient_OnTextChanged" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <telerik:RadGrid ID="dgPassenger" runat="server" AllowSorting="true" OnItemCreated="dgPassenger_ItemCreated"
                            Visible="false" OnNeedDataSource="dgPassenger_BindData" OnItemCommand="dgPassenger_ItemCommand"
                            OnUpdateCommand="dgPassenger_UpdateCommand" OnInsertCommand="dgPassenger_InsertCommand"
                            OnDeleteCommand="dgPassenger_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                            AllowPaging="true" OnSelectedIndexChanged="dgPassenger_SelectedIndexChanged"
                            OnPreRender="dgPassenger_PreRender" OnPageIndexChanged="dgPassenger_PageIndexChanged"
                            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="341px">
                            <MasterTableView DataKeyNames="PassengerRequestorID,PassengerRequestorCD,PassengerName,CustomerID,DepartmentID,PassengerDescription,PhoneNum,IsEmployeeType,
        StandardBilling,AuthorizationID,AuthorizationDescription,Notes,LastName,FirstName,MiddleInitial,DateOfBirth,ClientID,AssociatedWithCD,HomebaseID,IsActive,
        FlightPurposeID,SSN,Title,SalaryLevel,IsPassengerType,LastUpdUID,LastUpdTS,IsScheduledServiceCoord,CompanyName,EmployeeID,FaxNum,EmailAddress,PersonalIDNum,
        IsSpouseDependant,AccountID,CountryID,IsSIFL,PassengerAlert,Gender,AdditionalPhoneNum,IsRequestor,CountryOfResidenceID,PaxScanDoc,TSAStatus,TSADTTM,Addr1,Addr2,
        City,StateName,PostalZipCD,IsDeleted, NationalityName,NationalityCD,ClientCD,ClientDescription,AccountNum,AccountDescription,DepartmentCD,DepartmentName,
        AuthorizationCD,DeptAuthDescription,FlightPurposeCD,FlightPurposeDescription,ResidenceCountryName,ResidenceCountryCD,HomeBaseName,HomeBaseCD,Addr3,OtherEmail,PersonalEmail,OtherPhone,CellPhoneNum2,BusinessFax,PrimaryMobile"
                                CommandItemDisplay="Bottom">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="PAX Code" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="100px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PassengerName" HeaderText="Passengers/Requestors"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                        HeaderStyle-Width="200px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department Code" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="120px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="Department Desc."
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                        HeaderStyle-Width="120px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="100px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="100px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="Active" CurrentFilterFunction="EqualTo"
                                        ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="50px">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn DataField="IsRequestor" HeaderText="Req" CurrentFilterFunction="EqualTo"
                                        ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="50px">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="PassengerRequestorID" HeaderText="PassengerRequestorID"
                                        Display="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" Display="false"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px; float: left; clear: both;">
                                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                            Visible='<%# IsAuthorized(Permission.Database.AddPassengerRequestor)%>'><img style="border:0px;vertical-align:middle;" alt="Add" 
                        src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                            Visible='<%# IsAuthorized(Permission.Database.EditPassengerRequestor)%>' ToolTip="Edit"
                                            CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                            Visible='<%# IsAuthorized(Permission.Database.DeletePassengerRequestor)%>' runat="server"
                                            CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                    </div>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lbIsEnableTSAPX" runat="server" Text=""></asp:Label>
                                    </td>
                                    <%--   <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>--%>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="right">
                                        <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnImage" runat="server" Text="Image" CssClass="ui_nav"
                                                        OnClientClick="javascript:OnClientClick('Image'); return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddlInform" runat="server" Text="Additional Information"
                                                        CssClass="ui_nav" OnClientClick="javascript:OnClientClick('AddInfo'); return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnVisa" runat="server" Text="Visa" CssClass="ui_nav"
                                                        OnClientClick="javascript:OnClientClick('Visa'); return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnPassport" runat="server" Text="Passport" CssClass="ui_nav"
                                                        OnClientClick="javascript:OnClientClick('Passport'); return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnNotes" runat="server" Text="Notes" CssClass="ui_nav"
                                                        OnClientClick="javascript:OnClientClick('Notes'); return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnPaxAlerts" runat="server" Text="PAX Alerts" CssClass="ui_nav"
                                                        OnClientClick="javascript:OnClientClick('Pax Alerts'); return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSaveChangesTop" Enabled="false" runat="server" Text="Save" CssClass="button"
                                                        ValidationGroup="Save" OnClick="btnSaveChangesTop_Click" OnClientClick="javascript:CheckPassportChoice();" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnCancelTop" Enabled="false" runat="server" Text="Cancel" CssClass="button"
                                                        OnClick="Cancel_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <telerik:RadPanelBar ID="pnlMain" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                                runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Maintenance">
                                        <Items>
                                            <telerik:RadPanelItem Value="Maintenance" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%" class="box1">
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td class="tdLabel100">
                                                                            <asp:CheckBox ID="chkRequestor" runat="server" Text="Requestor" />
                                                                        </td>
                                                                        <td class="tdLabel100">
                                                                            <asp:CheckBox ID="chkSchdServCoord" runat="server" Text="Sched Serv Coord." Visible="false"
                                                                                Checked="false" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:CheckBox ID="chkActive1" runat="server" Text="Active" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="tblspace_10">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel150">
                                                                            <span class="mnd_text">PAX Code</span>
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCode" runat="server" MaxLength="5" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                            onBlur="return RemoveSpecialChars(this),CheckTxtBox('code');" Enabled="false"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnCode" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="PAX Code is Required."
                                                                ControlToValidate="tbCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                                                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="PAX Code already exists."
                                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel150" valign="top">
                                                                            <span class="mnd_text">First Name</span>
                                                                        </td>
                                                                        <td class="tdLabel150" valign="top">
                                                                            <asp:TextBox ID="tbFirstName" CssClass="text120" runat="server" MaxLength="20"></asp:TextBox>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ErrorMessage="PAX First Name is Required."
                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbFirstName" ValidationGroup="Save">
                                                                                        </asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            Middle Name
                                                                        </td>
                                                                        <td class="tdLabel150" valign="top">
                                                                            <asp:TextBox ID="tbMiddleName" CssClass="text120" runat="server" MaxLength="20"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel70" valign="top">
                                                                            <span class="mnd_text">Last Name</span>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:TextBox ID="tbLastName" CssClass="text120" runat="server" MaxLength="20"></asp:TextBox>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage="PAX Last Name is Required."
                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbLastName" ValidationGroup="Save">
                                                                                        </asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="tblspace_5">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>
                                                                                    <asp:Label ID="lbContactInfo" runat="server" Text="Contact Information" ForeColor="Black"></asp:Label></legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Company
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbCompany" CssClass="text200" runat="server" MaxLength="60"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Employee ID
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbEmpId" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Home Phone
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbPaxPhone" CssClass="text200" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Business Phone
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbAddlPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Primary Mobile/Cell
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbPrimaryMobile" CssClass="text200" runat="server" MaxLength="25"
                                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Secondary Mobile/Cell
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbSecondaryMobile" runat="server" CssClass="text200" MaxLength="25"
                                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Business Fax
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbBusinessFax" CssClass="text200" runat="server" MaxLength="25"
                                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Other Phone
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbOtherPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Business E-mail
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Home Fax
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbPaxFax" CssClass="text200" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                            onBlur="return RemoveSpecialCharsInPhone(this);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                    </td>
                                                                                                    <td class="tdLabel140">
                                                                                                        <asp:RegularExpressionValidator ID="regEmail" runat="server" ErrorMessage="Invalid email format"
                                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbEmail" ValidationGroup="Save"
                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Personal E-mail
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbPersonalEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Other E-mail
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbOtherEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                                    </td>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:RegularExpressionValidator ID="revPersonalEmail" runat="server" ControlToValidate="tbPersonalEmail"
                                                                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:RegularExpressionValidator ID="revOtherEmail" runat="server" ControlToValidate="tbOtherEmail"
                                                                                                            ValidationGroup="Save" ErrorMessage="Invalid E-mail format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Country Of Residence
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbCityOfResi" CssClass="text170" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            OnTextChanged="tbCityOfResi_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hdnCityOfResi" runat="server" />
                                                                                                        <asp:Button ID="btnCityOfResi" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;" />
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Nationality
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbNationality" CssClass="text170" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            OnTextChanged="tbNationality_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hdnNationality" runat="server" />
                                                                                                        <asp:Button ID="BtnNationality" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadNationalityMasterPopup');return false;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:CustomValidator ID="cvCityOfResi" runat="server" ControlToValidate="tbCityOfResi"
                                                                                                            ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                        <asp:Label ID="tbCityOfResiName" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvNationality" runat="server" ControlToValidate="tbNationality"
                                                                                                            ErrorMessage="Invalid Nationality." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                                        <asp:Label ID="tbNationalityName" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Address Line 1
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbAddress1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Address Line 2
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbAddress2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Address Line 3
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbAddress3" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        City
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbCity" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        State/Province
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Postal
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbPostal" CssClass="text200" runat="server" MaxLength="15"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        PIN No.
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbPinNumber" CssClass="text200" runat="server" MaxLength="40"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Date Of Birth
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <uc:DatePicker ID="ucDateOfBirth" runat="server"></uc:DatePicker>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Gender
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:RadioButton ID="rbMale" runat="server" Text="Male" GroupName="Gender" Checked="true" />
                                                                                                        <asp:RadioButton ID="rbFemale" runat="server" Text="Female" GroupName="Gender" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvDateOfBirth" runat="server" ErrorMessage="Date Of Birth should not be future date."
                                                                                                            ClientValidationFunction="fncClientCheckDate" ValidationGroup="Save" CssClass="alert-text"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Client Code
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbClientCde" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            OnTextChanged="tbClientCde_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hdnClientCde" runat="server" />
                                                                                                        <asp:Button ID="btnClientCde" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadClientCodeMasterPopup');return false;" />
                                                                                                        </button>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Home Base
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            OnTextChanged="tbHomeBase_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hdnHomeBase" runat="server" />
                                                                                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAirportPopup');return false;" />
                                                                                                        </button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:CustomValidator ID="cvClientCde" runat="server" ControlToValidate="tbClientCde"
                                                                                                            ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                                                            ErrorMessage="Invalid Home Base." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Flight Purpose
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="tbFlightPurpose" runat="server" MaxLength="2" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            OnTextChanged="tbFlightPurpose_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hdnFlightPurpose" runat="server" />
                                                                                                        <asp:Button ID="btnFlightPurpose" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadFlightPurposeMasterPopup');return false;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvFlightPurpose" runat="server" ControlToValidate="tbFlightPurpose"
                                                                                                            ErrorMessage="Invalid Flight Purpose Code." Display="Dynamic" CssClass="alert-text"
                                                                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr style="display: none;">
                                                                                                    <td>
                                                                                                        <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>
                                                                                    <asp:Label ID="lbBillingInformation" runat="server" Text="Billing Information" ForeColor="Black"></asp:Label></legend>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140" valign="top">
                                                                                                        Department
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbDepartment" runat="server" MaxLength="8" OnTextChanged="tbDepartment_OnTextChanged"
                                                                                                                        AutoPostBack="true" onchange="tbDepartment_onchange();"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hdnDepartment" runat="server" />
                                                                                                                    <asp:Button ID="btnDepartment" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadDepartmentMasterPopup');return false;" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:CustomValidator ID="cvDepartment" runat="server" ControlToValidate="tbDepartment"
                                                                                                                        ErrorMessage="Invalid Department Code." Display="Dynamic" CssClass="alert-text"
                                                                                                                        ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                                    <asp:Label ID="tbDepartmentDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Authorization
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbAuth" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                        OnTextChanged="tbAuth_OnTextChanged" AutoPostBack="true" onchange="CheckDeptAuthorization();"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hdnAuth" runat="server" />
                                                                                                                    <asp:Button ID="btnAuth" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadAuthorizationMasterPopup');return false;" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:CustomValidator ID="cvAuth" runat="server" ControlToValidate="tbAuth" ErrorMessage="Invalid Department Authorization Code."
                                                                                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                                    <asp:Label ID="tbAuthDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Standard Billing
                                                                                                    </td>
                                                                                                    <td class="tdLabel240">
                                                                                                        <asp:TextBox ID="tbStdBilling" runat="server" MaxLength="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        Account No.
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbAccountNumber" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                        OnTextChanged="tbAccountNumber_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hdnAccountNumber" runat="server" />
                                                                                                                    <asp:Button ID="btnAccount" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAccountMasterPopup');return false;" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:CustomValidator ID="cvAccountNumber" runat="server" ControlToValidate="tbAccountNumber"
                                                                                                                        ErrorMessage="Invalid Account No." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                                        SetFocusOnError="true"></asp:CustomValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td width="53%" valign="top">
                                                                            <fieldset>
                                                                                <legend>
                                                                                    <asp:Label ID="lbTaxInformation" runat="server" Text="Tax Information" ForeColor="Black"></asp:Label></legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td width="100%">
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel60">
                                                                                                        Type
                                                                                                    </td>
                                                                                                    <td class="tdLabel100">
                                                                                                        <asp:RadioButton ID="radType" runat="server" Text="Control" OnCheckedChanged="TaxType_OnCheckedChanged"
                                                                                                            AutoPostBack="true" GroupName="TaxType" />
                                                                                                    </td>
                                                                                                    <td class="tdLabel130">
                                                                                                        <asp:RadioButton ID="radNonControlled" runat="server" Text="Non-Control" OnCheckedChanged="TaxType_OnCheckedChanged"
                                                                                                            AutoPostBack="true" GroupName="TaxType" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:RadioButton ID="radGuest" runat="server" Text="Guest" OnCheckedChanged="TaxType_OnCheckedChanged"
                                                                                                            AutoPostBack="true" GroupName="TaxType" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="100%">
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel140">
                                                                                                        Associated With
                                                                                                    </td>
                                                                                                    <%-- <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>--%>
                                                                                                    <td class="tdLabel120">
                                                                                                        <asp:TextBox ID="tbAssociated" runat="server" Width="80" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            AutoPostBack="true" OnTextChanged="tbAssociated_OnTextChanged"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hdnAssociated" runat="server" />
                                                                                                        <asp:Button ID="btnAssociated" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadAssociateMasterPopup');return false;" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSiflSecurity" runat="server" Text="SIFL Security" />
                                                                                                    </td>
                                                                                                    <%-- </tr>
                                                                                            <tr>--%>
                                                                                                    <td colspan="2">
                                                                                                        <asp:CustomValidator ID="cvAssociated" runat="server" ControlToValidate="tbAssociated"
                                                                                                            ErrorMessage="Invalid Association." Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                    <%--</tr>
                                                                                        </table>
                                                                                    </td>--%>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                        <td width="47%" valign="top">
                                                                            <fieldset>
                                                                                <legend>
                                                                                    <asp:Label ID="lbTravelSenseInformation" runat="server" Text="Travel Sense Information"
                                                                                        ForeColor="Black"> </asp:Label></legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel80">
                                                                                                        T$ense Id
                                                                                                    </td>
                                                                                                    <td class="tdLabel100">
                                                                                                        <asp:TextBox ID="tbTsenseid" CssClass="text80" runat="server" MaxLength="9" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="tdLabel50">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbTitle" CssClass="text80" runat="server" MaxLength="30" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel80">
                                                                                                        Salary Level
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbSalaryLevel" CssClass="text80" runat="server" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:RangeValidator ID="rvSalaryLevel" runat="server" ControlToValidate="tbSalaryLevel"
                                                                                                            Type="double" MinimumValue="1" MaximumValue="9999" ValidationGroup="Save" CssClass="alert-text"
                                                                                                            SetFocusOnError="true" ErrorMessage="Salary Level Must Be Within the Range 1 to 9999."
                                                                                                            Display="Dynamic" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                            runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Image" CssClass="PanelHeaderStyle">
                                                    <ContentTemplate>
                                                        <table cellspacing="0" cellpadding="0" width="100%" class="box1">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table style="width: 100%;">
                                                                                    <tr>
                                                                                        <td class="tdLabel100">
                                                                                            Document Name
                                                                                        </td>
                                                                                        <td style="vertical-align: top">
                                                                                            <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckName();"
                                                                                                ClientIDMode="Static"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel100" valign="top">
                                                                                            Choose
                                                                                        </td>
                                                                                        <td style="vertical-align: top">
                                                                                            <asp:FileUpload ID="fileUL" runat="server" Enabled="false" OnBlur="CheckName();"
                                                                                                ClientIDMode="Static" onchange="javascript:return File_onchange();" />
                                                                                            <asp:Label ID="lblError" CssClass="alert-text" runat="server"></asp:Label>
                                                                                            <asp:HiddenField ID="hdnUrl" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="50%" align="right" valign="top">
                                                                                <table width="70%">
                                                                                    <tr>
                                                                                        <td style="vertical-align: top">
                                                                                            <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                                                                OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text100"
                                                                                                ClientIDMode="AutoID">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td style="vertical-align: top">
                                                                                            <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                                                                                AlternateText="" ClientIDMode="Static" />
                                                                                        </td>
                                                                                        <td style="vertical-align: top">
                                                                                            <asp:Button ID="btndeleteImage" runat="server" Text="Delete" CssClass="button" Enabled="false"
                                                                                                OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadPanelBar ID="pnlAdditionalInfo" Width="100%" ExpandAnimation-Type="None"
                                            CollapseAnimation-Type="None" runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Additional Information">
                                                    <Items>
                                                        <telerik:RadPanelItem Value="AddInfo" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" class="box1">
                                                                    <tr>
                                                                        <td>
                                                                            <telerik:RadGrid ID="dgPassengerAddlInfo" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                                                                Height="245px">
                                                                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                                    DataKeyNames="PassengerAdditionalInfoID,CustomerID,PassengerRequestorID,AdditionalINFOCD,AdditionalINFODescription,AdditionalINFOValue,ClientID,LastUpdUID,LastUptTS,IsDeleted"
                                                                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false">
                                                                                    <Columns>
                                                                                        <telerik:GridBoundColumn DataField="PassengerAdditionalInfoID" UniqueName="PassengerAdditionalInfoID"
                                                                                            HeaderText="Code" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                            ShowFilterIcon="false" AllowFiltering="false" Display="false">
                                                                                        </telerik:GridBoundColumn>
                                                                                        <telerik:GridBoundColumn DataField="AdditionalINFOCD" UniqueName="AdditionalINFOCD"
                                                                                            HeaderText="Code" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                            ShowFilterIcon="false" AllowFiltering="false">
                                                                                        </telerik:GridBoundColumn>
                                                                                        <telerik:GridBoundColumn DataField="AdditionalINFODescription" UniqueName="AdditionalINFODescription"
                                                                                            HeaderText="Description" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                            ShowFilterIcon="false" AllowFiltering="false">
                                                                                        </telerik:GridBoundColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Additional Information" CurrentFilterFunction="Contains"
                                                                                            AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="AdditionalINFOValue"
                                                                                            AllowFiltering="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="tbAddlInfo" runat="server" CssClass="text100" Text='<%# Eval("AdditionalINFOValue") %>'
                                                                                                    MaxLength="25"></asp:TextBox>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                    </Columns>
                                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                </MasterTableView>
                                                                                <ClientSettings>
                                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                    <Selecting AllowRowSelect="true" />
                                                                                </ClientSettings>
                                                                                <GroupingSettings CaseSensitive="false" />
                                                                            </telerik:RadGrid>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAddlInfo" OnClientClick="javascript:return ShowAddlInfoPopup();"
                                                                                            runat="server" CssClass="ui_nav" Text="Add Info." CausesValidation="false" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnDeleteInfo" runat="server" CssClass="ui_nav" Text="Delete Info."
                                                                                            CausesValidation="false" OnClick="DeleteInfo_Click" OnClientClick="if(!confirm('Are you sure you want to delete this ADDITIONAL INFO record?')) return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadPanelBar ID="pnlPassport" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                                            runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Passport">
                                                    <Items>
                                                        <telerik:RadPanelItem Value="Passport" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" class="box1">
                                                                    <tr>
                                                                        <td>
                                                                            PAX Passport Information:
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <telerik:RadGrid ID="dgPassport" runat="server" OnNeedDataSource="Passport_BindData"
                                                                                            OnItemDataBound="dgPassport_ItemDataBound" EnableAJAX="True" AutoGenerateColumns="false"
                                                                                            AllowFilteringByColumn="false" Height="245px" OnPageIndexChanged="dgPassport_PageIndexChanged">
                                                                                            <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                                                DataKeyNames="PassportID,CrewID,PassengerRequestorID,CustomerID,Choice,IssueCity,PassportNum,PassportExpiryDT,CountryID,IsDefaultPassport,
                                                                    PilotLicenseNum,LastUpdUID,LastUpdTS,IssueDT,IsDeleted,CountryCD,CountryName" CommandItemDisplay="None" ShowFooter="false"
                                                                                                AllowFilteringByColumn="false" AllowPaging="false">
                                                                                                <Columns>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Passport No." CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="PassportNUM">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbPassportNum" runat="server" CssClass="text60" Text='<%# Eval("PassportNUM") %>'
                                                                                                                MaxLength="25" onblur="javascript:return CheckPassportDuplicate(this);"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Choice" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IsChoice">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkChoice" runat="server" Checked='<%# Eval("Choice") != null ? Eval("Choice") : false %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="ExpiryDT">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbPassportExpiryDT" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetFocus(this);"
                                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="40" Text='<%# Eval("PassportExpiryDT") %>'></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssueCity">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbIssueCity" runat="server" CssClass="text60" Text='<%# Eval("IssueCity") %>'
                                                                                                                MaxLength="40"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Issue Date" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssueDT">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbIssueDT" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetFocus(this);"
                                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="40" Text='<%# Eval("IssueDT") %>'></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Nation" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CountryCD">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbPassportCountry" runat="server" CssClass="text60" Text='<%# Eval("CountryCD") %>'
                                                                                                                AutoPostBack="true" OnTextChanged="Nation_TextChanged" MaxLength="40" onDblClick="javascript:openWinGrd('RadVisaCountryMasterPopup',this);return false;"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="hdnPassportCountry" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                </Columns>
                                                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                            </MasterTableView>
                                                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                                <Selecting AllowRowSelect="true" />
                                                                                            </ClientSettings>
                                                                                            <GroupingSettings CaseSensitive="false" />
                                                                                        </telerik:RadGrid>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAddPassport" runat="server" Text="Add Passport" OnClick="AddPassport_Click"
                                                                                            CssClass="ui_nav" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnDeletePassport" runat="server" Text="Delete Passport" OnClick="DeletePassport_Click"
                                                                                            CssClass="ui_nav" OnClientClick="if(!confirm('Are you sure you want to delete this PASSPORT record?')) return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadPanelBar ID="pnlVisa" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                                            runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Visa">
                                                    <Items>
                                                        <telerik:RadPanelItem Value="Visa" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" class="box1">
                                                                    <tr>
                                                                        <td>
                                                                            Visa Information:
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <telerik:RadGrid ID="dgVisa" runat="server" OnNeedDataSource="Visa_BindData" EnableAJAX="True"
                                                                                            OnItemDataBound="dgVisa_ItemDataBound" Height="245px" OnPageIndexChanged="dgVisa_PageIndexChanged">
                                                                                            <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                                                DataKeyNames="VisaID,CrewID,PassengerRequestorID,CustomerID,VisaTYPE,VisaNum,LastUpdUID,LastUpdTS,CountryID,ExpiryDT,Notes,IssuePlace,IssueDate,IsDeleted,
                                                                    CountryCD,CountryName" CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false">
                                                                                                <Columns>
                                                                                                    <telerik:GridBoundColumn DataField="VisaID" UniqueName="VisaID" HeaderText="VisaID"
                                                                                                        CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                                                                                                        AllowFiltering="false" Display="false">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Country" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="CountryCD">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbCountryCD" runat="server" CssClass="text60" AutoPostBack="true"
                                                                                                                Text='<%# Eval("CountryCD") %>' OnTextChanged="CountryCD_TextChanged" MaxLength="40"
                                                                                                                onDblClick="javascript:openWinGrd('RadVisaCountryMasterPopup',this);return false;"></asp:TextBox>
                                                                                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Visa No." CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="VisaNum">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbVisaNum" runat="server" CssClass="text60" Text='<%# Eval("VisaNum") %>'
                                                                                                                MaxLength="25" onblur="javascript:return CheckVisaDuplicate(this);"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="ExpiryDT">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbExpiryDT" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetFocus(this);"
                                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="40" Text='<%# Eval("ExpiryDT") %>'></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssuePlace">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbIssuePlace" runat="server" CssClass="text60" Text='<%# Eval("IssuePlace") %>'
                                                                                                                MaxLength="40"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Issue Date" CurrentFilterFunction="Contains"
                                                                                                        AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="IssueDate">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbIssueDate" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event);ValidateDate(this);SetFocus(this);"
                                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" MaxLength="40" Text='<%# Eval("IssueDate") %>'></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Notes" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true"
                                                                                                        ShowFilterIcon="false" UniqueName="Notes">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="tbNotes" runat="server" CssClass="text60" Text='<%# Eval("Notes") %>'
                                                                                                                MaxLength="40"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                </Columns>
                                                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                            </MasterTableView>
                                                                                            <ClientSettings>
                                                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                                <Selecting AllowRowSelect="true" />
                                                                                            </ClientSettings>
                                                                                            <GroupingSettings CaseSensitive="false" />
                                                                                        </telerik:RadGrid>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAddVisa" runat="server" Text="Add Visa" OnClick="AddVisa_Click"
                                                                                            CssClass="ui_nav" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnDeleteVisa" runat="server" Text="Delete Visa" OnClick="DeleteVisa_Click"
                                                                                            CssClass="ui_nav" OnClientClick="if(!confirm('Are you sure you want to delete this VISA record?')) return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadPanelBar ID="Notes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                            runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Notes" CssClass="PanelHeaderCrewRoster">
                                                    <Items>
                                                        <telerik:RadPanelItem Value="Notes" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <telerik:RadPanelBar ID="PaxItems" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                            runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="false" Text="PAX Alerts" CssClass="PanelHeaderCrewRoster">
                                                    <Items>
                                                        <telerik:RadPanelItem Value="PaxItems" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPaxItems" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSaveChanges" runat="server" Text="Save" CssClass="button" ValidationGroup="Save"
                                            OnClick="btnSaveChangesTop_Click" OnClientClick="javascript:CheckPassportChoice();" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="Cancel_Click" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnDate" runat="server" />
                            <asp:HiddenField ID="hdnIsPassportChoice" runat="server" />
                        </asp:Panel>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
