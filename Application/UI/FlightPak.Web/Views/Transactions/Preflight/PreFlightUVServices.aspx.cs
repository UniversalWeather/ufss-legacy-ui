﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PreflightService;
using Telerik.Web.UI;
using System.Globalization;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
//using UWA.Adapters;


namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreFlightUVServices : BaseSecuredPage
    {

        #region "Comments"
        //Defect: 2934 The system shall have the ability to display the Delivery method drop-down options in alphabetical order for Preflight - UWA Services.
        //Fixed By Prabhu 05/02/2014.

        #endregion

        private ExceptionManager exManager;
        public PreflightMain Trip = new PreflightMain();
        public PreflightMain uwalTrip = new PreflightMain();
        // Declaration for Exception
        List<FlightPak.Web.PreflightService.RulesException> ErrorList = new List<FlightPak.Web.PreflightService.RulesException>();
        public UWAL uwal = new UWAL();
        public List<UWAPermit> uwaPermList = new List<UWAPermit>();
        PreflightLeg prefLeg = new PreflightLeg();
        private delegate void SaveSession();

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        //Since the functionality is not implemented These buttons are disabled.
                        btnReset.Enabled = false;
                        btnReset.CssClass = "button-disable";

                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        if (Trip.Mode == TripActionMode.Edit)
                        {
                            btnSave.Enabled = true;
                            btnCancel.Enabled = true;
                            btnEditTrip.Enabled = false;
                            btnSubmittoUVTSS.Enabled = false;
                            btnCheckStatus.Enabled = false;
                            btnManualSubmit.Enabled = false;

                            btnUpdateDefaults.Enabled = false;
                            btnUpdateDefaults.CssClass = "button-disable";

                            btnSave.CssClass = "button";
                            btnCancel.CssClass = "button";
                            btnSubmittoUVTSS.CssClass = "button-disable";
                            btnCheckStatus.CssClass = "button-disable";
                            btnManualSubmit.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            DisableEnableFields(true);
                            imgbtnAircraftSecurity.Enabled = true;
                            imgbtnAircraftSecurity.CssClass = "note-icon";
                            imgbtnCrewPassportVisas.Enabled = true;
                            imgbtnCrewPassportVisas.CssClass = "note-icon";
                            imgbtnPaxPassportVisas.Enabled = true;
                            imgbtnPaxPassportVisas.CssClass = "note-icon";
                            imgbtnPaxSecurity.Enabled = true;
                            imgbtnPaxSecurity.CssClass = "note-icon";
                        }
                        else
                        {
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnEditTrip.Enabled = true;
                            btnSubmittoUVTSS.Enabled = true;
                            btnCheckStatus.Enabled = true;
                            btnManualSubmit.Enabled = true;

                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnSubmittoUVTSS.CssClass = "button";
                            btnCheckStatus.CssClass = "button";
                            btnManualSubmit.CssClass = "button";
                            btnEditTrip.CssClass = "button";
                            btnUpdateDefaults.Enabled = true;
                            btnUpdateDefaults.CssClass = "button";

                            DisableEnableFields(false);
                            imgbtnAircraftSecurity.Enabled = false;
                            imgbtnAircraftSecurity.CssClass = "note-icon-disable";
                            imgbtnCrewPassportVisas.Enabled = false;
                            imgbtnCrewPassportVisas.CssClass = "note-icon-disable";
                            imgbtnPaxPassportVisas.Enabled = false;
                            imgbtnPaxPassportVisas.CssClass = "note-icon-disable";
                            imgbtnPaxSecurity.Enabled = false;
                            imgbtnPaxSecurity.CssClass = "note-icon-disable";
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);

                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlPretrip, pnlPretrip, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlFlightfollowing, pnlFlightfollowing, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlFuel, pnlFuel, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlHandler, pnlHandler, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlLegInfo, pnlLegInfo, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlPermits, pnlPermits, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlPretrip, pnlPretrip, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(RadWindowManager1, pnlPretrip, RadAjaxLoadingPanel1);

                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCrewCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lnkTripID, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstUV, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlPretrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rtsLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstLegsInfo, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPaxSecurity, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnPaxSecurity, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkAircraftSecurity, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnAircraftSecurity, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPaxPassportVisas, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnPaxPassportVisas, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkWideBodyHandling, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkCrewPassportVisas, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnCrewPassportVisas, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkCatering, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstOverflightPermits, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnReset, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaults, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnManualSubmit, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCheckStatus, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSubmittoUVTSS, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaultClearYes, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaultsContinue, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaults, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(RadWindowManager1, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancelDesc, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveDesc, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnYes, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNo, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveManualSubmit, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancelManualSubmit, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCrewCancel, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lnkTripID, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstUV, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlPretrip, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rtsLegs, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstLegsInfo, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPaxSecurity, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnPaxSecurity, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkAircraftSecurity, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnAircraftSecurity, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPaxPassportVisas, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnPaxPassportVisas, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkWideBodyHandling, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkCrewPassportVisas, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(imgbtnCrewPassportVisas, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkCatering, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstOverflightPermits, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnReset, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaults, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnManualSubmit, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCheckStatus, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSubmittoUVTSS, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaultClearYes, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaultsContinue, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnUpdateDefaults, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(RadWindowManager1, DivExternalForm, RadAjaxLoadingPanel1);

                    radwindowCheckStatus.VisibleOnPageLoad = false;
                    SaveSession SavePreflightSession = new SaveSession(_SavePreflightToSession);
                    Master.SaveToSession = SavePreflightSession;
                    //Replicate Save Cancel Delete Buttons in header
                    Master.SaveClick += btnSave_Click;
                    Master.CancelClick += btnCancel_Click;


                    //Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;
                    tbSelectedArrivalHandler.Enabled = false;

                    #region notPostback
                    if (!IsPostBack)
                    {
                        // For Update Defaults
                        chkMainInfo.Checked = true;
                        chkFuel.Checked = true;
                        chkHandle.Checked = true;
                        chkCrewHotel.Checked = true;
                        chkPermits.Checked = true;
                        chkPaxHotel.Checked = true;

                        #region Authorization
                        CheckAutorization(Permission.Preflight.ViewPreflightUWA);

                        if (!IsAuthorized(Permission.Preflight.EditPreflightUWA))
                        {
                            btnSubmittoUVTSS.Visible = false;
                            btnCheckStatus.Visible = false;
                            btnSave.Visible = false;
                            btnManualSubmit.Visible = false;
                            btnUpdateDefaults.Visible = false;
                            btnReset.Visible = false;
                        }
                        else
                        {
                            btnSubmittoUVTSS.Visible = true;
                            btnCheckStatus.Visible = true;
                            btnSave.Visible = true;
                            btnManualSubmit.Visible = true;
                            btnUpdateDefaults.Visible = true;
                            btnReset.Visible = true;
                        }

                        #endregion

                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            loadPageProperties();
                            hdnLeg.Value = "1";
                            //SetLegnumsforTrip(Trip);
                            BindPreflighTabs(Trip, "pageload");
                            rtsLegs.Tabs[0].Selected = true;
                            hdnLeg.Value = (Convert.ToInt16(rtsLegs.Tabs[0].Value) + 1).ToString();
                            hdnLegID.Value = Convert.ToString(Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegID);
                            using (var client = new PreflightServiceClient())
                            {
                                var vUwal = client.GetUWAL(Trip.TripID, false);
                                if (vUwal != null)
                                {
                                    if (vUwal.ReturnFlag)
                                    {
                                        //need to loop thru entity list match the legid and bindit
                                        uwalTrip = vUwal.EntityList[0];






                                        //Code newly Added to store all uwapermitlist
                                        //uwaPermList = uwalTrip.PreflightLegs[0].UWAPermits.ToList();
                                        //Changed as Below

                                        if (uwalTrip.PreflightLegs != null && uwalTrip.PreflightLegs.Count > 0)
                                        {
                                            foreach (PreflightLeg Leg in uwalTrip.PreflightLegs)
                                            {
                                                foreach (UWAPermit uwaPermits in Leg.UWAPermits)
                                                {
                                                    uwaPermList.Add(uwaPermits);
                                                }
                                            }

                                        }


                                        //Code newly Added to store all uwapermitlist


                                        Session["CurrentPreFlightUWAL"] = uwalTrip;
                                        Session["CurrentUWAPermitList"] = uwaPermList;
                                    }
                                }
                            }
                        }
                        if (Session["CurrentPreFlightUWAL"] != null)
                        {
                            if (uwalTrip.PreflightLegs != null && uwalTrip.PreflightLegs.Count() > 0)
                            {
                                //
                                foreach (PreflightLeg prefLeg in uwalTrip.PreflightLegs)
                                {
                                    if (prefLeg.LegID == Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].LegID)
                                    {
                                        LoadLegDetails(prefLeg);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    }
                    if (Session["CurrentPreFlightUWAL"] != null)
                    {
                        uwalTrip = (PreflightMain)Session["CurrentPreFlightUWAL"];


                        foreach (PreflightLeg UWaLeg in uwalTrip.PreflightLegs)
                        {
                            if (UWaLeg.LegID == Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].LegID)
                            {
                                prefLeg = UWaLeg;
                                break;
                            }
                        }

                        //uwaPermList = prefLeg.UWAPermits.ToList();
                    }

                    if (Session["CurrentUWAPermitList"] != null)
                    {
                        uwaPermList = (List<UWAPermit>)Session["CurrentUWAPermitList"];
                    }

                    if (uwalTrip != null && uwalTrip.UWATSSMains != null)
                    {
                        if (uwalTrip.UWATSSMains.Count > 0)
                        {
                            btnCheckStatus.Enabled = true;
                        }
                        else
                        {
                            btnCheckStatus.Enabled = false;
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

            }


        }

        protected void _SavePreflightToSession()
        {
            //if (Session["CurrentPreFlightUWAL"] != null)
            //{
            //    //uwal = (UWAL)Session["CurrentPreFlightUWAL"];

            //    if (Trip.Mode == TripActionMode.Edit)
            //    {
            //        Trip.State = TripEntityState.Modified;

            //        Session["CurrentPreFlightUWAL"] = uwalTrip;
            //    }
            //}
        }

        protected void SetLegnumsforTrip(PreflightMain preflightTrip)
        {
            //if (preflightTrip.PreflightLegs != null)
            //{
            //    int Legnum = 0;
            //    foreach (PreflightLeg Leg in Trip.PreflightLegs)
            //    {
            //        if (Leg.State != TripEntityState.Deleted)
            //        {
            //            Leg.LegNUM = Legnum + 1;
            //            Legnum++;
            //        }
            //        else
            //            Leg.LegNUM = null;
            //    }
            //    Session["CurrentPreFlightTrip"] = Trip;
            //}

        }

        protected void BindPreflighTabs(PreflightMain preFlightTrip, string eventraised)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preFlightTrip, eventraised))
            {
                rtsLegs.Tabs.Clear();

                if (preFlightTrip.PreflightLegs != null && preFlightTrip.PreflightLegs.Count() > 0)
                {
                    List<PreflightLeg> PreflightLegList = preFlightTrip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    int Legid = 0;
                    int counter = 1;
                    foreach (PreflightLeg Leg in PreflightLegList)
                    {
                        if (Leg.State != PreflightService.TripEntityState.Deleted)
                        {
                            RadTab Rt = new RadTab();
                            Rt.Text = "Leg" + Leg.LegNUM.ToString();
                            Rt.Value = preFlightTrip.PreflightLegs.IndexOf(Leg).ToString();


                            rtsLegs.Tabs.Add(Rt);

                            if (Leg.ArriveICAOID != null && Leg.DepartICAOID != null)
                                rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString() + " (" + Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID + ")";
                            else
                                rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString();

                            Legid = Legid + 1;
                            counter = counter + 1;
                        }
                    }
                }
                else
                {
                    RadTab Rt = new RadTab();
                    Rt.Text = "Leg1";
                    Rt.Value = "0";
                    rtsLegs.Tabs.Add(Rt);
                }
                //rtsLegs.Tabs[Convert.ToInt16(hdnLeg.Value) - 1].Selected = true;
                if (rtsLegs.Tabs.Count > 1)
                {
                    rtsLegs.Tabs.FindTabByValue((Convert.ToInt16(hdnLeg.Value) - 1).ToString()).Selected = true;
                    //lbLegNo.Text = "Leg " + (rtsLegs.Tabs.FindTabByValue((Convert.ToInt16(hdnLeg.Value) - 1).ToString()).Index + 1).ToString();
                }
                else
                {
                    rtsLegs.Tabs[0].Selected = true;
                    //lbLegNo.Text = "Leg 1";
                }
            }
        }

        protected void rtsLegs_TabClick(object sender, RadTabStripEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(rtsLegs);

                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                        setFieldValues(uwalTrip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1]);
                        //setFieldValues();
                        hdnLeg.Value = (Convert.ToInt16(e.Tab.Value) + 1).ToString();
                        hdnLegID.Value = Convert.ToString(Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegID);

                        //BindPreflighTabs(Trip, "Tabclick");


                        if (uwalTrip.PreflightLegs != null && uwalTrip.PreflightLegs.Count() > 0)
                        {
                            //
                            foreach (PreflightLeg prefLeg in uwalTrip.PreflightLegs)
                            {
                                if (prefLeg.LegID == Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].LegID)
                                {
                                    uwaPermList = prefLeg.UWAPermits.ToList();
                                    LoadLegDetails(prefLeg);
                                    break;
                                }
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightUWAServices);
                }
            }

        }

        protected void loadPageProperties()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!Page.IsPostBack)
                    {
                        loadLocationDDL();
                        loadDeliveryMethod();
                    }
                    if (radlstUV.Items[0].Selected)
                    {
                        pnlPretrip.Visible = true;
                        pnlFlightfollowing.Visible = false;
                    }
                    else
                    {
                        pnlPretrip.Visible = false;
                        pnlFlightfollowing.Visible = true;
                    }

                    if (radlstLegsInfo.Items[0].Selected)
                    {
                        pnlPermits.Visible = false;
                        pnlFuel.Visible = false;
                        pnlHandler.Visible = true;
                    }
                    else if (radlstLegsInfo.Items[1].Selected)
                    {
                        pnlPermits.Visible = true;
                        pnlFuel.Visible = false;
                        pnlHandler.Visible = false;
                    }
                    else
                    {
                        pnlPermits.Visible = false;
                        pnlHandler.Visible = false;
                        pnlFuel.Visible = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        #region Loading and Setting DropDowns

        protected enum Location
        {
            HANDLER,
            HANGAR,
            HOME,
            OFFICE,
            AIRCRAFT
        }

        protected void loadLocationDDL()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    setDDPretripLocation(ddlstLocation1);
                    setDDPretripLocation(ddlstLocation2);
                    setDDPretripLocation(ddlstLocation3);
                    setDDPretripLocation(ddlstLocation4);
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void setDDPretripLocation(DropDownList ddl)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ddl))
            {
                ddl.DataSource = Enum.GetNames(typeof(Location));
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("", ""));
            }
        }

        protected enum DeliveryMethod
        {
            //Defect: 2934
            AFTN,
            ARINC,
            EMAIL,
            FAX,
            MAILBOX,
            PAGER,
            SITA,
            TELEX

        }

        protected void loadDeliveryMethod()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    setDDPretripDeliveryMethod(ddlstDeliveryMethod1);
                    setDDPretripDeliveryMethod(ddlstDeliveryMethod2);
                    setDDPretripDeliveryMethod(ddlstDeliveryMethod3);
                    setDDPretripDeliveryMethod(ddlstDeliveryMethod4);
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void setDDPretripDeliveryMethod(DropDownList ddl)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ddl))
            {
                ddl.DataSource = Enum.GetNames(typeof(DeliveryMethod));
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("", ""));
            }
        }
        #endregion

        protected void radlstUV_Changed(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (radlstUV.Items[0].Selected)
                    {
                        pnlPretrip.Visible = true;
                        pnlFlightfollowing.Visible = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(ddlstLocation1);
                    }
                    else
                    {
                        pnlPretrip.Visible = false;
                        pnlFlightfollowing.Visible = true;
                        radlstFlightFollowing.Focus();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void radlstLegsInfo_Changed(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (radlstLegsInfo.Items[0].Selected)
                    {
                        pnlPermits.Visible = false;
                        pnlFuel.Visible = false;
                        pnlHandler.Visible = true;
                        chkUniversalArrangesHandling.Checked = true;
                        RadAjaxManager.GetCurrent(Page).FocusControl(chkUniversalArrangesHandling);
                    }
                    else if (radlstLegsInfo.Items[1].Selected)
                    {
                        pnlPermits.Visible = true;
                        pnlFuel.Visible = false;
                        pnlHandler.Visible = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(radlstLandingPermits.ClientID);
                        //radlstLandingPermits.Focus();
                    }
                    else
                    {
                        pnlPermits.Visible = false;
                        pnlHandler.Visible = false;
                        pnlFuel.Visible = true;
                        RadAjaxManager.GetCurrent(Page).FocusControl(chkUWAairFuel);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void radlstOverflightPermits_Changed(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (radlstOverflightPermits.Items[0].Selected)
                    {
                        divPermitDetails.Visible = false;
                    }
                    else
                    {
                        divPermitDetails.Visible = true;
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Page.Validate();
                    if (Page.IsValid)
                    {
                        //BindPreflighTabs(Trip, "SaveButton");

                        foreach (PreflightLeg prefLeg in uwalTrip.PreflightLegs)
                        {
                            if (prefLeg.LegID == Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].LegID)
                            {
                                setFieldValues(prefLeg);
                                //setFieldValues();
                                break;
                            }
                        }

                        using (var client = new PreflightServiceClient())
                        {
                            client.UpdateUWAL(uwalTrip);
                            //_SavePreflightToSession();
                            Session["CurrentPreFlightUWAL"] = uwalTrip;
                            Trip.Mode = TripActionMode.NoChange;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
            Response.Redirect("PreFlightUVServices.aspx?seltab=UWA");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //DisableEnableFields(true);
                    //Trip.Mode = TripActionMode.NoChange;
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    Master.Cancel(Trip);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                ClearFields();
            }
        }

        protected void setFieldValues(PreflightLeg prefLeg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(prefLeg))
            {
                //if (Session["CurrentPreFlightUWAL"] != null)
                //{
                //    uwalTrip = (PreflightMain)Session["CurrentPreFlightUWAL"];

                //    foreach (PreflightLeg prefLeg in uwalTrip.PreflightLegs)
                //    {
                //        if (prefLeg.LegID == Convert.ToInt64(hdnLegID.Value))
                //        {


                //Trip Level
                if (prefLeg.UWALs.Count() == 0)
                {
                    UWAL uwa = new UWAL();
                    prefLeg.UWALs.Add(uwa);
                }
                //int iLegNum = Convert.ToInt32(hdnLeg.Value)-1;

                prefLeg.UWALs[0].LegID = Convert.ToInt64(hdnLegID.Value.ToString());
                prefLeg.UWALs[0].TripID = Trip.TripID;
                prefLeg.UWALs[0].LegNUM = (int)Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM;

                prefLeg.UWALs[0].Pretrip1Location = ddlstLocation1.SelectedValue.ToString();
                prefLeg.UWALs[0].Pretrip1DeliveryMethod = ddlstDeliveryMethod1.SelectedValue.ToString();
                prefLeg.UWALs[0].PretripDelivery1Address = tbDeliveryAddress1.Text.ToString();
                prefLeg.UWALs[0].Pretrip1Attention = tbAttention1.Text.ToString();

                prefLeg.UWALs[0].Pretrip2Location = ddlstLocation2.SelectedValue.ToString();
                prefLeg.UWALs[0].Pretrip2DeliveryMethod = ddlstDeliveryMethod2.SelectedValue.ToString();
                prefLeg.UWALs[0].PretripDelivery2Address = tbDeliveryAddress2.Text.ToString();
                prefLeg.UWALs[0].Pretrip2Attention = tbAttention2.Text.ToString();

                prefLeg.UWALs[0].Pretrip3Location = ddlstLocation3.SelectedValue.ToString();
                prefLeg.UWALs[0].Pretrip3DeliveryMethod = ddlstDeliveryMethod3.SelectedValue.ToString();
                prefLeg.UWALs[0].PretripDelivery3Address = tbDeliveryAddress3.Text.ToString();
                prefLeg.UWALs[0].Pretrip3Attention = tbAttention3.Text.ToString();

                prefLeg.UWALs[0].Pretrip4Location = ddlstLocation4.SelectedValue.ToString();
                prefLeg.UWALs[0].Pretrip4DeliveryMethod = ddlstDeliveryMethod4.SelectedValue.ToString();
                prefLeg.UWALs[0].PretripDelivery4Address = tbDeliveryAddress4.Text.ToString();
                prefLeg.UWALs[0].Pretrip4Attention = tbAttention4.Text.ToString();

                prefLeg.UWALs[0].FlightLegSettings = Convert.ToInt32(radlstFlightFollowing.SelectedValue);

                prefLeg.UWALs[0].IsFax = chkFax.Checked;
                prefLeg.UWALs[0].FaxAttention = tbffattention1.Text.ToString();
                prefLeg.UWALs[0].FaxDeliveryAddress = tbffdeliveryAddress1.Text.ToString();

                prefLeg.UWALs[0].IsEmail = chkEmail.Checked;
                prefLeg.UWALs[0].EmailAttention = tbffattention2.Text.ToString();
                prefLeg.UWALs[0].EmailAddress = tbffdeliveryAddress2.Text.ToString();

                prefLeg.UWALs[0].IsPhone = chkPhone.Checked;
                prefLeg.UWALs[0].PhoneAttention = tbffattention3.Text.ToString();
                prefLeg.UWALs[0].PhoneDeliveryAddress = tbffdeliveryAddress3.Text.ToString();

                prefLeg.UWALs[0].IsSITAAIRINC = chkSITA.Checked;
                prefLeg.UWALs[0].SITAAIRINCDeliveryAttention = tbffattention4.Text.ToString();
                prefLeg.UWALs[0].SITAAIRINCDeliveryAddress = tbffdeliveryAddress4.Text.ToString();

                prefLeg.UWALs[0].NotamLegSettings = Convert.ToInt32(radlstLegSettings.SelectedValue.ToString());
                prefLeg.UWALs[0].NotamTYPE = Convert.ToInt32(radlstType.SelectedValue.ToString());

                prefLeg.UWALs[0].FARAllLegs = Convert.ToInt32(radlstFAR.SelectedValue.ToString());
                prefLeg.UWALs[0].IsSuppressCrewPaxDetailOnAllLegs = chkSuppressCrew.Checked;

                prefLeg.UWALs[0].IsOverflightPermitAllLegs = chkPermitsOverflight.Checked;
                prefLeg.UWALs[0].IsLandingPermitsAllLegs = chkPermitsLanding.Checked;
                prefLeg.UWALs[0].ClientArrangeLandingPermitNUM = tbClientArragnedPermitNo.Text.ToString();

                prefLeg.UWALs[0].IsHandlingAllLegs = chkHandling.Checked;
                prefLeg.UWALs[0].IsArriveDepartDeclFormsAllLeg = chkArrivalDepartureDeclarationForm.Checked;

                prefLeg.UWALs[0].IsPreflightAllLegs = chkPrefileAllLegs.Checked;

                prefLeg.UWALs[0].IsPassengerSecurityAllLegs = chkSecurityPassenger.Checked;
                prefLeg.UWALs[0].IsAircraftSecurityAllLegs = chkSecurityAircraft.Checked;

                prefLeg.UWALs[0].IsPassengerVisaAllLegs = chkVisasPassenger.Checked;
                prefLeg.UWALs[0].IsCrewVisaAllLegs = chkVisasCrew.Checked;

                prefLeg.UWALs[0].IsPassengerHotelAllLegs = chkHotelsPassenger.Checked;
                prefLeg.UWALs[0].IsCrewHotelAllLegs = chkHotelsCrew.Checked;

                prefLeg.UWALs[0].IsUVAirFuelAllLegs = ChkUVairFuel.Checked;
                prefLeg.UWALs[0].IsUVAirQuoteRequestAllLegs = ChkUVairFuelQuote.Checked;

                prefLeg.UWALs[0].IsCustoms = chkCustoms.Checked;
                prefLeg.UWALs[0].IsPaymentCreditForServices = chkServicePayment.Checked;

                prefLeg.UWALs[0].SpecialInstructions = tbFBODepartFBOInfo.Text.ToString();

                prefLeg.UWALs[0].TripRequestBy = tbRequestedby.Text.ToString();

                //Leg Level
                prefLeg.UWALs[0].IsUWAArrangeHandling = chkUniversalArrangesHandling.Checked;
                prefLeg.UWALs[0].IsUWAArrangeCustoms = chkUniversalArrangesHandlingCustomers.Checked;
                prefLeg.UWALs[0].IsHomebaseFlightFollowing = chkHomebaseFlightFollowing.Checked;
                prefLeg.UWALs[0].IsWideBodyHandling = chkWideBodyHandling.Checked;
                prefLeg.UWALs[0].IsPassengerSecurity = chkPaxSecurity.Checked;
                prefLeg.UWALs[0].PassengerSecurityNotes = hdnPaxSecurityNotes.Value;
                prefLeg.UWALs[0].IsAircraftSecurity = chkAircraftSecurity.Checked;
                prefLeg.UWALs[0].AircraftSecurityNote = hdnAircraftSecurityNotes.Value;
                prefLeg.UWALs[0].IsCatering = chkCatering.Checked;
                prefLeg.UWALs[0].CateringDetail = tbCatering.Text;
                prefLeg.UWALs[0].IsCrewVisa = chkCrewPassportVisas.Checked;
                prefLeg.UWALs[0].IsPassengerVisa = chkPaxPassportVisas.Checked;

                prefLeg.UWALs[0].LandingPermitsArrangement = Convert.ToInt32(radlstLandingPermits.SelectedValue);
                prefLeg.UWALs[0].AirportArrivalSlotArrange = Convert.ToInt32(radlstArrive.SelectedValue);
                prefLeg.UWALs[0].AirportDepartureSlotsArrangement = Convert.ToInt32(radlstDepart.SelectedValue);
                prefLeg.UWALs[0].OverflightPermitsArrangement = Convert.ToInt32(radlstOverflightPermits.SelectedValue);
                //Permit Num... text box to be added.

                //Fuel
                prefLeg.UWALs[0].IsUVAirFuel = chkUWAairFuel.Checked;
                prefLeg.UWALs[0].IsUVAirFuelQuoteRequired = chkUWAairFuelQuote.Checked;
                prefLeg.UWALs[0].IsPristRequired = chkPristRequired.Checked;
                prefLeg.UWALs[0].FuelOnArrivalDeparture = Convert.ToInt32(radlstFuelOn.SelectedValue);
                prefLeg.UWALs[0].FuelSpecialInstruction = tbFuelSplInstructions.Text;

                //UserRecord
                prefLeg.UWALs[0].LastUpdUID = UserPrincipal.Identity._name;
                prefLeg.UWALs[0].LastUptTS = DateTime.UtcNow;

                prefLeg.UWASpServs.Clear();

                int iUwalSpServ = 0;

                if (chkWideBodyHandling.Checked == true)
                {
                    if (chkBELTLOADER.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblBELTLOADER.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbBELTLOADER.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkCONTAINERLOADERS.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblCONTAINERLOADERS.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbCONTAINERLOADERS.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkDOLLIES.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblDOLLIES.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbDOLLIES.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkFREIGHTHANDLING.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblFREIGHTHANDLING.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbFREIGHTHANDLING.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkGROUNDSTAIRS.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblGROUNDSTAIRS.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbGROUNDSTAIRS.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkKLOADER.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblKLOADER.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbKLOADER.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkLD3DOLLIES.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblLD3DOLLIES.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbLD3DOLLIES.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkLD6DOLLIES.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblLD6DOLLIES.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbLD6DOLLIES.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                    if (chkLOADINGEQUIPMENT.Checked == true)
                    {
                        UWASpServ uwaSpServ = new UWASpServ();
                        prefLeg.UWASpServs.Add(uwaSpServ);
                        prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblLOADINGEQUIPMENT.Text);
                        prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbLOADINGEQUIPMENT.Text;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                        prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                        iUwalSpServ = iUwalSpServ + 1;

                    }
                }


                if (chkAPMechanic.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblAPMechanic.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbAPMechanic.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ = iUwalSpServ + 1;

                }
                if (chkAIRStartCart.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblAIRStartCart.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbAIRStartCart.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ = iUwalSpServ + 1;
                }
                if (chkCleaning.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblCleaning.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbCleaning.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ = iUwalSpServ + 1;
                }
                if (chkCrewTransportation.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblCrewTransportation.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbCrewTransportation.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ = iUwalSpServ + 1;
                }
                if (chkDishCleaning.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblDishCleaning.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbDishCleaning.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkGupAup.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblGupAup.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbGupAup.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkHangar.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblHangar.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbHangar.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkLadder.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblLadder.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbLadder.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkLaundry.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblLaundry.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbLaundry.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkLavAndWater.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblLavAndWater.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbLavAndWater.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkMiscellaneous.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblMiscellaneous.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbMiscellaneous.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkNavigator.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblNavigator.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbNavigator.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkNavigatorArrangements.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblNavigatorArrangements.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbNavigatorArrangements.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkNewspapers.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblNewspapers.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbNewspapers.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkOvertime.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblOvertime.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbOvertime.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkOxygen.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblOxygen.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbOxygen.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkParking.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblParking.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbParking.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkPaxTransportation.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblPaxTransportation.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbPaxTransportation.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkTowbarTug.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblTowbarTug.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbTowbarTug.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkVipLounge.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblVipLounge.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbVipLounge.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                if (chkWeaponsClearance.Checked == true)
                {
                    UWASpServ uwaSpServ = new UWASpServ();
                    prefLeg.UWASpServs.Add(uwaSpServ);
                    prefLeg.UWASpServs[iUwalSpServ].LegID = prefLeg.LegID;
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServDescription = System.Web.HttpUtility.HtmlDecode(lblWeaponsClearance.Text);
                    prefLeg.UWASpServs[iUwalSpServ].UWASpServInstruction = tbWeaponsClearance.Text;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdUID = UserPrincipal.Identity._name;
                    prefLeg.UWASpServs[iUwalSpServ].LastUpdTS = DateTime.UtcNow;
                    iUwalSpServ++;
                }
                //        }
                //    }
                //}
                //Session["CurrentPreFlightUWAL"] = uwalTrip;
            }
        }

        protected void LoadLegDetails(PreflightLeg prefLeg)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(prefLeg))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {


                    //Clear Values before loading.
                    ClearFieldsLeg();
                    //Trip Level
                    if (uwalTrip.UWATSSMains.Count > 0)
                    {
                        //tbLastSubmitted.Text = Convert.ToDateTime(uwalTrip.UWATSSMains[0].LastUptTS).ToString("MM/dd/yyyy");
                        tbLastSubmitted.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", uwalTrip.UWATSSMains[0].LastUptTS);
                        lnkTripID.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(uwalTrip.UWATSSMains[0].UWATripID));
                        lblTripID.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(uwalTrip.UWATSSMains[0].UWATripID));
                    }

                    if (radlstLegsInfo.Items[0].Selected)
                    {
                        pnlPermits.Visible = false;
                        pnlFuel.Visible = false;
                        pnlHandler.Visible = true;
                        chkUniversalArrangesHandling.Checked = true;
                    }
                    else if (radlstLegsInfo.Items[1].Selected)
                    {
                        pnlPermits.Visible = true;
                        pnlFuel.Visible = false;
                        pnlHandler.Visible = false;
                    }
                    else
                    {
                        pnlPermits.Visible = false;
                        pnlHandler.Visible = false;
                        pnlFuel.Visible = true;
                    }

                    if (radlstOverflightPermits.Items[0].Selected)
                        divPermitDetails.Visible = false;
                    else
                        divPermitDetails.Visible = true;

                    DateTime tempDate = new DateTime();
                    if (Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].DepartureDTTMLocal.ToString() != "")
                    {
                        tempDate = Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].DepartureDTTMLocal.Value;
                        //lblDepartDate.Text = tempDate.ToString("MM/dd/yyyy");
                        lblDepartDate.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", tempDate));
                    }
                    else
                        lblDepartDate.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                    if (Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].HomeArrivalDTTM.ToString() != "")
                    {
                        tempDate = Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].HomeArrivalDTTM.Value;
                        //lblArrivalDate.Text = tempDate.ToString("MM/dd/yyyy");
                        lblArrivalDate.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", tempDate));
                    }
                    else
                        lblArrivalDate.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                    lblDepartIcao.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].Airport1.IcaoID);
                    lblArrivalIcao.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].Airport.IcaoID);

                    if (prefLeg.UWALs.Count > 0)
                    {
                        //Trip Level
                        //int iLegNum = Convert.ToInt32(hdnLeg.Value) - 1;                                                
                        tbRequestedby.Text = (prefLeg.UWALs[0].TripRequestBy == null ? string.Empty : prefLeg.UWALs[0].TripRequestBy.Trim());
                        if (!string.IsNullOrEmpty(prefLeg.UWALs[0].Pretrip1Location))
                        {
                            ddlstLocation1.SelectedValue = prefLeg.UWALs[0].Pretrip1Location.Trim();
                            ddlstDeliveryMethod1.SelectedValue = prefLeg.UWALs[0].Pretrip1DeliveryMethod.Trim();
                            tbDeliveryAddress1.Text = Convert.ToString(prefLeg.UWALs[0].PretripDelivery1Address);
                            tbAttention1.Text = Convert.ToString(prefLeg.UWALs[0].Pretrip1Attention);
                        }
                        if (!string.IsNullOrEmpty(prefLeg.UWALs[0].Pretrip2Location))
                        {
                            ddlstLocation2.SelectedValue = prefLeg.UWALs[0].Pretrip2Location.Trim();
                            ddlstDeliveryMethod2.SelectedValue = prefLeg.UWALs[0].Pretrip2DeliveryMethod.Trim();
                            tbDeliveryAddress2.Text = Convert.ToString(prefLeg.UWALs[0].PretripDelivery2Address);
                            tbAttention2.Text = Convert.ToString(prefLeg.UWALs[0].Pretrip2Attention);
                        }
                        if (!string.IsNullOrEmpty(prefLeg.UWALs[0].Pretrip3Location))
                        {
                            ddlstLocation3.SelectedValue = prefLeg.UWALs[0].Pretrip3Location.Trim();
                            ddlstDeliveryMethod3.SelectedValue = prefLeg.UWALs[0].Pretrip3DeliveryMethod.Trim();
                            tbDeliveryAddress3.Text = Convert.ToString(prefLeg.UWALs[0].PretripDelivery3Address);
                            tbAttention3.Text = Convert.ToString(prefLeg.UWALs[0].Pretrip3Attention);
                        }
                        if (!string.IsNullOrEmpty(prefLeg.UWALs[0].Pretrip4Location))
                        {
                            ddlstLocation4.SelectedValue = prefLeg.UWALs[0].Pretrip4Location.Trim();
                            ddlstDeliveryMethod4.SelectedValue = prefLeg.UWALs[0].Pretrip4DeliveryMethod.Trim();
                            tbDeliveryAddress4.Text = Convert.ToString(prefLeg.UWALs[0].PretripDelivery4Address);
                            tbAttention4.Text = Convert.ToString(prefLeg.UWALs[0].Pretrip4Attention);
                        }
                        radlstFlightFollowing.SelectedValue = prefLeg.UWALs[0].FlightLegSettings.ToString();

                        chkFax.Checked = prefLeg.UWALs[0].IsFax == true ? true : false;
                        tbffattention1.Text = prefLeg.UWALs[0].FaxAttention;
                        tbffdeliveryAddress1.Text = prefLeg.UWALs[0].FaxDeliveryAddress;

                        chkEmail.Checked = prefLeg.UWALs[0].IsEmail == true ? true : false;
                        tbffattention2.Text = Convert.ToString(prefLeg.UWALs[0].EmailAttention);
                        tbffdeliveryAddress2.Text = Convert.ToString(prefLeg.UWALs[0].EmailAddress);

                        chkPhone.Checked = prefLeg.UWALs[0].IsPhone == true ? true : false;
                        tbffattention3.Text = Convert.ToString(prefLeg.UWALs[0].PhoneAttention);
                        tbffdeliveryAddress3.Text = Convert.ToString(prefLeg.UWALs[0].PhoneDeliveryAddress);

                        chkSITA.Checked = prefLeg.UWALs[0].IsSITAAIRINC == true ? true : false;
                        tbffattention4.Text = Convert.ToString(prefLeg.UWALs[0].SITAAIRINCDeliveryAttention);
                        tbffdeliveryAddress4.Text = Convert.ToString(prefLeg.UWALs[0].SITAAIRINCDeliveryAddress);

                        radlstLegSettings.SelectedValue = prefLeg.UWALs[0].NotamLegSettings.ToString();
                        radlstType.SelectedValue = prefLeg.UWALs[0].NotamTYPE.ToString();

                        radlstFAR.SelectedValue = prefLeg.UWALs[0].FARAllLegs.ToString();
                        chkSuppressCrew.Checked = prefLeg.UWALs[0].IsSuppressCrewPaxDetailOnAllLegs == true ? true : false;

                        chkPermitsOverflight.Checked = prefLeg.UWALs[0].IsOverflightPermitAllLegs == true ? true : false;
                        chkPermitsLanding.Checked = prefLeg.UWALs[0].IsLandingPermitsAllLegs == true ? true : false;
                        tbClientArragnedPermitNo.Text = Convert.ToString(prefLeg.UWALs[0].ClientArrangeLandingPermitNUM);

                        chkHandling.Checked = prefLeg.UWALs[0].IsHandlingAllLegs == true ? true : false;
                        chkArrivalDepartureDeclarationForm.Checked = prefLeg.UWALs[0].IsArriveDepartDeclFormsAllLeg == true ? true : false;

                        chkPrefileAllLegs.Checked = prefLeg.UWALs[0].IsPreflightAllLegs == true ? true : false;

                        chkSecurityPassenger.Checked = prefLeg.UWALs[0].IsPassengerSecurityAllLegs == true ? true : false;
                        chkSecurityAircraft.Checked = prefLeg.UWALs[0].IsAircraftSecurityAllLegs == true ? true : false;

                        chkVisasPassenger.Checked = prefLeg.UWALs[0].IsPassengerVisaAllLegs == true ? true : false;
                        chkVisasCrew.Checked = prefLeg.UWALs[0].IsCrewVisaAllLegs == true ? true : false;

                        chkHotelsPassenger.Checked = prefLeg.UWALs[0].IsPassengerHotelAllLegs == true ? true : false;
                        chkHotelsCrew.Checked = prefLeg.UWALs[0].IsCrewHotelAllLegs == true ? true : false;

                        ChkUVairFuel.Checked = prefLeg.UWALs[0].IsUVAirFuelAllLegs == true ? true : false;
                        ChkUVairFuelQuote.Checked = prefLeg.UWALs[0].IsUVAirQuoteRequestAllLegs == true ? true : false;

                        chkCustoms.Checked = prefLeg.UWALs[0].IsCustoms == true ? true : false;
                        chkServicePayment.Checked = prefLeg.UWALs[0].IsPaymentCreditForServices == true ? true : false;

                        tbFBODepartFBOInfo.Text = Convert.ToString(prefLeg.UWALs[0].SpecialInstructions);

                        //Leg Level

                        // lblDepartDate.Text = prefLeg.UWALs[0].DepartICAOID

                        chkUniversalArrangesHandling.Checked = prefLeg.UWALs[0].IsUWAArrangeHandling == true ? true : false;
                        chkUniversalArrangesHandlingCustomers.Checked = prefLeg.UWALs[0].IsUWAArrangeCustoms == true ? true : false;
                        chkHomebaseFlightFollowing.Checked = prefLeg.UWALs[0].IsHomebaseFlightFollowing == true ? true : false;
                        chkWideBodyHandling.Checked = prefLeg.UWALs[0].IsWideBodyHandling == true ? true : false;
                        if (chkWideBodyHandling.Checked)
                            widebody_row_status(true);
                        else
                            widebody_row_status(false);

                        chkPaxSecurity.Checked = prefLeg.UWALs[0].IsPassengerSecurity == true ? true : false;
                        //if (chkPaxPassportVisas.Checked)
                        //    imgbtnPaxSecurity.Enabled = true;
                        //else
                        //    imgbtnPaxSecurity.Enabled = false;
                        hdnPaxSecurityNotes.Value = prefLeg.UWALs[0].PassengerSecurityNotes;
                        chkAircraftSecurity.Checked = prefLeg.UWALs[0].IsAircraftSecurity == true ? true : false;
                        //if (chkAircraftSecurity.Checked)
                        //    imgbtnAircraftSecurity.Enabled = true;
                        //else
                        //    imgbtnAircraftSecurity.Enabled = false;
                        hdnAircraftSecurityNotes.Value = prefLeg.UWALs[0].AircraftSecurityNote;
                        chkCatering.Checked = prefLeg.UWALs[0].IsCatering == true ? true : false;
                        if (chkCatering.Checked)
                            tbCatering.Enabled = true;
                        else
                            tbCatering.Enabled = false;
                        tbCatering.Text = prefLeg.UWALs[0].CateringDetail;
                        chkCrewPassportVisas.Checked = prefLeg.UWALs[0].IsCrewVisa == true ? true : false;
                        chkPaxPassportVisas.Checked = prefLeg.UWALs[0].IsPassengerVisa == true ? true : false;

                        #region UWASpServ

                        if (prefLeg.UWASpServs.Count() > 0)
                        {
                            foreach (UWASpServ uwaSpServ in prefLeg.UWASpServs)
                            {

                                if (uwaSpServ.UWASpServDescription == lblBELTLOADER.Text)
                                {
                                    chkBELTLOADER.Checked = true;
                                    tbBELTLOADER.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblCONTAINERLOADERS.Text)
                                {
                                    chkCONTAINERLOADERS.Checked = true;
                                    tbCONTAINERLOADERS.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblDOLLIES.Text)
                                {
                                    chkDOLLIES.Checked = true;
                                    tbDOLLIES.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblFREIGHTHANDLING.Text)
                                {
                                    chkFREIGHTHANDLING.Checked = true;
                                    tbFREIGHTHANDLING.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblGROUNDSTAIRS.Text)
                                {
                                    chkGROUNDSTAIRS.Checked = true;
                                    tbGROUNDSTAIRS.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblKLOADER.Text)
                                {
                                    chkKLOADER.Checked = true;
                                    tbKLOADER.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblLD3DOLLIES.Text)
                                {
                                    chkLD3DOLLIES.Checked = true;
                                    tbLD3DOLLIES.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblLD6DOLLIES.Text)
                                {
                                    chkLD6DOLLIES.Checked = true;
                                    tbLD6DOLLIES.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblLOADINGEQUIPMENT.Text)
                                {
                                    chkLOADINGEQUIPMENT.Checked = true;
                                    tbLOADINGEQUIPMENT.Text = uwaSpServ.UWASpServInstruction;
                                }

                                else if (uwaSpServ.UWASpServDescription == lblAPMechanic.Text)
                                {
                                    chkAPMechanic.Checked = true;
                                    tbAPMechanic.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblAIRStartCart.Text)
                                {
                                    chkAIRStartCart.Checked = true;
                                    tbAIRStartCart.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblCleaning.Text)
                                {
                                    chkCleaning.Checked = true;
                                    tbCleaning.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblCrewTransportation.Text)
                                {
                                    chkCrewTransportation.Checked = true;
                                    tbCrewTransportation.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblDishCleaning.Text)
                                {
                                    chkDishCleaning.Checked = true;
                                    tbDishCleaning.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblGupAup.Text)
                                {
                                    chkGupAup.Checked = true;
                                    tbGupAup.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblHangar.Text)
                                {
                                    chkHangar.Checked = true;
                                    tbHangar.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblLadder.Text)
                                {
                                    chkLadder.Checked = true;
                                    tbLadder.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblLaundry.Text)
                                {
                                    chkLaundry.Checked = true;
                                    tbLaundry.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblLavAndWater.Text)
                                {
                                    chkLavAndWater.Checked = true;
                                    tbLavAndWater.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblMiscellaneous.Text)
                                {
                                    chkMiscellaneous.Checked = true;
                                    tbMiscellaneous.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblNavigator.Text)
                                {
                                    chkNavigator.Checked = true;
                                    tbNavigator.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblNavigatorArrangements.Text)
                                {
                                    chkNavigatorArrangements.Checked = true;
                                    tbNavigatorArrangements.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblNewspapers.Text)
                                {
                                    chkNewspapers.Checked = true;
                                    tbNewspapers.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblAIRStartCart.Text)
                                {
                                    chkAIRStartCart.Checked = true;
                                    tbAIRStartCart.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblOvertime.Text)
                                {
                                    chkOvertime.Checked = true;
                                    tbOvertime.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblOxygen.Text)
                                {
                                    chkOxygen.Checked = true;
                                    tbOxygen.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblParking.Text)
                                {
                                    chkParking.Checked = true;
                                    tbParking.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblPaxTransportation.Text)
                                {
                                    chkPaxTransportation.Checked = true;
                                    tbPaxTransportation.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblTowbarTug.Text)
                                {
                                    chkTowbarTug.Checked = true;
                                    tbTowbarTug.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblVipLounge.Text)
                                {
                                    chkVipLounge.Checked = true;
                                    tbVipLounge.Text = uwaSpServ.UWASpServInstruction;
                                }
                                else if (uwaSpServ.UWASpServDescription == lblWeaponsClearance.Text)
                                {
                                    chkWeaponsClearance.Checked = true;
                                    tbWeaponsClearance.Text = uwaSpServ.UWASpServInstruction;
                                }
                            }
                        }
                        #endregion

                        //Permits
                        radlstLandingPermits.SelectedValue = prefLeg.UWALs[0].LandingPermitsArrangement.ToString();
                        radlstArrive.SelectedValue = prefLeg.UWALs[0].AirportArrivalSlotArrange.ToString();
                        radlstDepart.SelectedValue = prefLeg.UWALs[0].AirportDepartureSlotsArrangement.ToString();
                        radlstOverflightPermits.SelectedValue = prefLeg.UWALs[0].OverflightPermitsArrangement.ToString();
                        //Permit Num... text box to be added.

                        //Fuel
                        chkUWAairFuel.Checked = prefLeg.UWALs[0].IsUVAirFuel == true ? true : false;
                        chkUWAairFuelQuote.Checked = prefLeg.UWALs[0].IsUVAirFuelQuoteRequired == true ? true : false;
                        chkPristRequired.Checked = prefLeg.UWALs[0].IsPristRequired == true ? true : false;
                        radlstFuelOn.SelectedValue = prefLeg.UWALs[0].FuelOnArrivalDeparture.ToString();
                        tbFuelSplInstructions.Text = Convert.ToString(prefLeg.UWALs[0].FuelSpecialInstruction);
                    }
                    else
                        tbRequestedby.Text = Convert.ToString(UserPrincipal.Identity._firstName) + " " + Convert.ToString(UserPrincipal.Identity._middleName) + " " + Convert.ToString(UserPrincipal.Identity._lastName);
                    //Load Permit Details


                    dgPermitDetails.Rebind();

                    //Load Crew Passport Visa
                    using (var client = new PreflightServiceClient())
                    {
                        var vUwal = client.GetUWACrewPassportVisa(prefLeg.LegID, Convert.ToInt64(prefLeg.CustomerID));
                        if (vUwal != null)
                        {
                            if (vUwal.ReturnFlag)
                            {
                                dgCrewPassportVisa.DataSource = vUwal.EntityList.ToList();
                                dgCrewPassportVisa.DataBind();
                            }
                        }
                    }

                    //Load Passenger Passport Visa
                    using (var client = new PreflightServiceClient())
                    {
                        var vUwal = client.GetUWAPassengerPassportVisa(prefLeg.LegID, Convert.ToInt64(prefLeg.CustomerID));
                        if (vUwal != null)
                        {
                            if (vUwal.ReturnFlag)
                            {
                                dgPassengerPassportVisa.DataSource = vUwal.EntityList.ToList();
                                dgPassengerPassportVisa.DataBind();
                            }
                        }
                    }

                    if (radlstOverflightPermits.Items[0].Selected)
                        divPermitDetails.Visible = false;
                    else
                        divPermitDetails.Visible = true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void DisableEnableFields(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                tbRequestedby.Enabled = status;
                // radlstUV.Enabled = status;

                ddlstLocation1.Enabled = status;
                ddlstDeliveryMethod1.Enabled = status;
                tbDeliveryAddress1.Enabled = status;
                tbAttention1.Enabled = status;

                ddlstLocation2.Enabled = status;
                ddlstDeliveryMethod2.Enabled = status;
                tbDeliveryAddress2.Enabled = status;
                tbAttention2.Enabled = status;

                ddlstLocation3.Enabled = status;
                ddlstDeliveryMethod3.Enabled = status;
                tbDeliveryAddress3.Enabled = status;
                tbAttention3.Enabled = status;

                ddlstLocation4.Enabled = status;
                ddlstDeliveryMethod4.Enabled = status;
                tbDeliveryAddress4.Enabled = status;
                tbAttention4.Enabled = status;

                radlstFlightFollowing.Enabled = status;

                chkFax.Enabled = status;
                tbffattention1.Enabled = status;
                tbffdeliveryAddress1.Enabled = status;

                chkEmail.Enabled = status;
                tbffattention2.Enabled = status;
                tbffdeliveryAddress2.Enabled = status;

                chkPhone.Enabled = status;
                tbffattention3.Enabled = status;
                tbffdeliveryAddress3.Enabled = status;

                chkSITA.Enabled = status;
                tbffattention4.Enabled = status;
                tbffdeliveryAddress4.Enabled = status;

                radlstLegSettings.Enabled = status;
                radlstType.Enabled = status;

                radlstFAR.Enabled = status;
                chkSuppressCrew.Enabled = status;

                chkPermitsOverflight.Enabled = status;
                chkPermitsLanding.Enabled = status;
                tbClientArragnedPermitNo.Enabled = status;

                chkHandling.Enabled = status;
                chkArrivalDepartureDeclarationForm.Enabled = status;

                chkPrefileAllLegs.Enabled = status;

                chkSecurityPassenger.Enabled = status;
                chkSecurityAircraft.Enabled = status;

                chkVisasPassenger.Enabled = status;
                chkVisasCrew.Enabled = status;

                chkHotelsPassenger.Enabled = status;
                chkHotelsCrew.Enabled = status;

                ChkUVairFuel.Enabled = status;
                ChkUVairFuelQuote.Enabled = status;

                chkCustoms.Enabled = status;
                chkServicePayment.Enabled = status;

                tbFBODepartFBOInfo.Enabled = status;

                //Leg Level

                //rtsLegs.Enabled = status;
                //radlstLegsInfo.Enabled = status;

                chkUniversalArrangesHandling.Enabled = status;
                chkUniversalArrangesHandlingCustomers.Enabled = status;
                chkHomebaseFlightFollowing.Enabled = status;
                chkWideBodyHandling.Enabled = status;
                chkPaxSecurity.Enabled = status;
                //if(chkPaxSecurity.Enabled == true && chkPaxSecurity.Checked)
                //    imgbtnPaxSecurity.Enabled = true;
                //else
                //    imgbtnPaxSecurity.Enabled = false;
                chkAircraftSecurity.Enabled = status;
                //if (imgbtnAircraftSecurity.Enabled == true && chkAircraftSecurity.Checked)
                //    imgbtnAircraftSecurity.Enabled = true;
                //else
                //    imgbtnAircraftSecurity.Enabled = false;
                chkPaxPassportVisas.Enabled = status;
                imgbtnPaxPassportVisas.Enabled = status;
                chkCrewPassportVisas.Enabled = status;
                imgbtnCrewPassportVisas.Enabled = status;
                chkCatering.Enabled = status;
                if (chkCatering.Enabled == true && chkCatering.Checked)
                    tbCatering.Enabled = true;
                else
                    tbCatering.Enabled = false;
                chkAPMechanic.Enabled = status;
                tbAPMechanic.Enabled = status;
                chkAIRStartCart.Enabled = status;
                tbAIRStartCart.Enabled = status;
                chkCleaning.Enabled = status;
                tbCleaning.Enabled = status;
                chkCrewTransportation.Enabled = status;
                tbCrewTransportation.Enabled = status;
                chkDishCleaning.Enabled = status;
                tbDishCleaning.Enabled = status;
                chkGupAup.Enabled = status;
                tbGupAup.Enabled = status;
                chkHangar.Enabled = status;
                tbHangar.Enabled = status;
                chkLadder.Enabled = status;
                tbLadder.Enabled = status;
                chkLaundry.Enabled = status;
                tbLaundry.Enabled = status;
                chkLavAndWater.Enabled = status;
                tbLavAndWater.Enabled = status;
                chkMiscellaneous.Enabled = status;
                tbMiscellaneous.Enabled = status;
                chkNavigator.Enabled = status;
                tbNavigator.Enabled = status;
                chkNavigatorArrangements.Enabled = status;
                tbNavigatorArrangements.Enabled = status;
                chkNewspapers.Enabled = status;
                tbNewspapers.Enabled = status;
                chkOvertime.Enabled = status;
                tbOvertime.Enabled = status;
                chkOxygen.Enabled = status;
                tbOxygen.Enabled = status;
                chkParking.Enabled = status;
                tbParking.Enabled = status;
                chkPaxTransportation.Enabled = status;
                tbPaxTransportation.Enabled = status;
                chkTowbarTug.Enabled = status;
                tbTowbarTug.Enabled = status;
                chkVipLounge.Enabled = status;
                tbVipLounge.Enabled = status;
                chkWeaponsClearance.Enabled = status;
                tbWeaponsClearance.Enabled = status;

                dgPermitDetails.Enabled = status;
                radlstLandingPermits.Enabled = status;
                radlstArrive.Enabled = status;
                radlstDepart.Enabled = status;
                radlstOverflightPermits.Enabled = status;

                //Fuel
                chkUWAairFuel.Enabled = status;
                chkUWAairFuelQuote.Enabled = status;
                chkPristRequired.Enabled = status;
                radlstFuelOn.Enabled = status;
                tbFuelSplInstructions.Enabled = status;

                chkBELTLOADER.Enabled = status;
                chkCONTAINERLOADERS.Enabled = status;
                chkDOLLIES.Enabled = status;
                chkFREIGHTHANDLING.Enabled = status;
                chkGROUNDSTAIRS.Enabled = status;
                chkKLOADER.Enabled = status;
                chkLD3DOLLIES.Enabled = status;
                chkLD6DOLLIES.Enabled = status;
                chkLOADINGEQUIPMENT.Enabled = status;

                tbBELTLOADER.Enabled = status;
                tbCONTAINERLOADERS.Enabled = status;
                tbDOLLIES.Enabled = status;
                tbFREIGHTHANDLING.Enabled = status;
                tbGROUNDSTAIRS.Enabled = status;
                tbKLOADER.Enabled = status;
                tbLD3DOLLIES.Enabled = status;
                tbLD6DOLLIES.Enabled = status;
                tbLOADINGEQUIPMENT.Enabled = status;
            }
        }

        protected void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool status = false;

                tbRequestedby.Text = "";
                radlstUV.SelectedValue = "1";

                ddlstLocation1.ClearSelection();
                ddlstDeliveryMethod1.ClearSelection();
                tbDeliveryAddress1.Text = "";
                tbAttention1.Text = "";

                ddlstLocation2.ClearSelection();
                ddlstDeliveryMethod2.ClearSelection();
                tbDeliveryAddress2.Text = "";
                tbAttention2.Text = "";

                ddlstLocation3.ClearSelection();
                ddlstDeliveryMethod3.ClearSelection();
                tbDeliveryAddress3.Text = "";
                tbAttention3.Text = "";

                ddlstLocation4.ClearSelection();
                ddlstDeliveryMethod4.ClearSelection();
                tbDeliveryAddress4.Text = "";
                tbAttention4.Text = "";

                radlstFlightFollowing.SelectedValue = "1";

                chkFax.Checked = status;
                tbffattention1.Text = "";
                tbffdeliveryAddress1.Text = "";

                chkEmail.Checked = status;
                tbffattention2.Text = "";
                tbffdeliveryAddress2.Text = "";

                chkPhone.Checked = status;
                tbffattention3.Text = "";
                tbffdeliveryAddress3.Text = "";

                chkSITA.Checked = status;
                tbffattention4.Text = "";
                tbffdeliveryAddress4.Text = "";

                radlstLegSettings.SelectedValue = "1";
                radlstType.ClearSelection();

                radlstFAR.SelectedValue = "1";
                chkSuppressCrew.Checked = status;

                chkPermitsOverflight.Checked = status;
                chkPermitsLanding.Checked = status;
                tbClientArragnedPermitNo.Text = "";

                chkHandling.Checked = status;
                chkArrivalDepartureDeclarationForm.Checked = status;

                chkPrefileAllLegs.Checked = status;

                chkSecurityPassenger.Checked = status;
                chkAircraftSecurity.Checked = status;

                chkVisasPassenger.Checked = status;
                chkVisasCrew.Checked = status;

                chkHotelsPassenger.Checked = status;
                chkHotelsCrew.Checked = status;

                ChkUVairFuel.Checked = status;
                ChkUVairFuelQuote.Checked = status;

                chkCustoms.Checked = status;
                chkServicePayment.Checked = status;

                tbFBODepartFBOInfo.Text = "";
            }
        }

        protected void ClearFieldsLeg()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Leg Level
                bool status = false;
                radlstLegsInfo.SelectedValue = "1";

                chkUniversalArrangesHandling.Checked = status;
                chkUniversalArrangesHandlingCustomers.Checked = status;
                chkHomebaseFlightFollowing.Checked = status;
                chkWideBodyHandling.Checked = status;
                chkPaxSecurity.Checked = status;

                chkAircraftSecurity.Checked = status;

                chkCatering.Checked = status;
                tbCatering.Text = "";
                tbCatering.Text = "";

                chkPaxPassportVisas.Checked = status;
                chkCrewPassportVisas.Checked = status;

                chkAPMechanic.Checked = status;
                tbAPMechanic.Text = "";
                chkAIRStartCart.Checked = status;
                tbAIRStartCart.Text = "";
                chkCleaning.Checked = status;
                tbCleaning.Text = "";
                chkCrewTransportation.Checked = status;
                tbCrewTransportation.Text = "";
                chkDishCleaning.Checked = status;
                tbDishCleaning.Text = "";
                chkGupAup.Checked = status;
                tbGupAup.Text = "";
                chkHangar.Checked = status;
                tbHangar.Text = "";
                chkLadder.Checked = status;
                tbLadder.Text = "";
                chkLaundry.Checked = status;
                tbLaundry.Text = "";
                chkLavAndWater.Checked = status;
                tbLavAndWater.Text = "";
                chkMiscellaneous.Checked = status;
                tbMiscellaneous.Text = "";
                chkNavigator.Checked = status;
                tbNavigator.Text = "";
                chkNavigatorArrangements.Checked = status;
                tbNavigatorArrangements.Text = "";
                chkNewspapers.Checked = status;
                tbNewspapers.Text = "";
                chkOvertime.Checked = status;
                tbOvertime.Text = "";
                chkOxygen.Checked = status;
                tbOxygen.Text = "";
                chkParking.Checked = status;
                tbParking.Text = "";
                chkPaxTransportation.Checked = status;
                tbPaxTransportation.Text = "";
                chkTowbarTug.Checked = status;
                tbTowbarTug.Text = "";
                chkVipLounge.Checked = status;
                tbVipLounge.Text = "";
                chkWeaponsClearance.Checked = status;
                tbWeaponsClearance.Text = "";

                radlstLandingPermits.SelectedValue = "1";
                radlstArrive.SelectedValue = "1";
                radlstDepart.SelectedValue = "1";
                radlstOverflightPermits.SelectedValue = "1";

                //Fuel
                chkUWAairFuel.Checked = status;
                chkUWAairFuelQuote.Checked = status;
                chkPristRequired.Checked = status;
                radlstFuelOn.SelectedValue = "1";
                tbFuelSplInstructions.Text = "";
                widebody_fields_empty();
                widebody_row_status(false);
            }
        }

        protected void chkWideBodyHandling_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (chkWideBodyHandling.Checked)
                {
                    widebody_row_status(true);
                    widebody_fields_empty();
                    RadAjaxManager.GetCurrent(Page).FocusControl(chkCrewPassportVisas);
                }
                else
                {
                    widebody_row_status(false);
                    widebody_fields_empty();
                    RadAjaxManager.GetCurrent(Page).FocusControl(chkCrewPassportVisas);
                }
            }
        }

        protected void widebody_fields_empty()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                chkBELTLOADER.Checked = false;
                tbBELTLOADER.Text = string.Empty;

                chkCONTAINERLOADERS.Checked = false;
                tbCONTAINERLOADERS.Text = string.Empty;

                chkDOLLIES.Checked = false;
                tbDOLLIES.Text = string.Empty;

                chkFREIGHTHANDLING.Checked = false;
                tbFREIGHTHANDLING.Text = string.Empty;

                chkGROUNDSTAIRS.Checked = false;
                tbGROUNDSTAIRS.Text = string.Empty;

                chkKLOADER.Checked = false;
                tbKLOADER.Text = string.Empty;

                chkLD3DOLLIES.Checked = false;
                tbLD3DOLLIES.Text = string.Empty;

                chkLD6DOLLIES.Checked = false;
                tbLD6DOLLIES.Text = string.Empty;

                chkLOADINGEQUIPMENT.Checked = false;
                tbLOADINGEQUIPMENT.Text = string.Empty;
            }
        }

        protected void widebody_row_status(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                trBELTLOADER.Visible = status;
                trCONTAINERLOADERS.Visible = status;
                trDOLLIES.Visible = status;
                trFREIGHTHANDLING.Visible = status;
                //trGROUNDSTAIRS.Visible = status;
                trKLOADER.Visible = status;
                trLD3DOLLIES.Visible = status;
                trLD6DOLLIES.Visible = status;
                trLOADINGEQUIPMENT.Visible = status;
            }
        }

        protected void chkPaxSecurity_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (chkPaxSecurity.Checked)
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxSecurity);
                        radwindowPopup.VisibleOnPageLoad = true;
                        radwindowPopup.Title = "Passenger Security Detail";
                        tbDescription.Text = hdnPaxSecurityNotes.Value;
                    }
                    else
                    {
                        //    imgbtnPaxSecurity.Enabled = false;
                        hdnPaxSecurityNotes.Value = string.Empty;
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxSecurity);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void chkAircraftSecurity_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (chkAircraftSecurity.Checked)
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnAircraftSecurity);
                        radwindowPopup.VisibleOnPageLoad = true;
                        radwindowPopup.Title = "Aircraft Security Detail";
                        tbDescription.Text = hdnAircraftSecurityNotes.Value;
                    }
                    else
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnAircraftSecurity);
                        //    imgbtnAircraftSecurity.Enabled = false;
                        hdnAircraftSecurityNotes.Value = string.Empty;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void chkCatering_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(chkAPMechanic);
                    if (chkCatering.Checked)
                    {
                        tbCatering.Enabled = true;
                    }
                    else
                    {
                        tbCatering.Enabled = false;
                        tbCatering.Text = "";
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void imgbtnPaxSecurity_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowPopup.VisibleOnPageLoad = true;
                    radwindowPopup.Title = "Passenger Security Detail";
                    tbDescription.Text = hdnPaxSecurityNotes.Value;
                    RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxSecurity);

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void imgbtnAircraftSecurity_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowPopup.VisibleOnPageLoad = true;
                    radwindowPopup.Title = "Aircraft Security Detail";
                    tbDescription.Text = hdnAircraftSecurityNotes.Value;
                    RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnAircraftSecurity);

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnSavePopUp_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (radwindowPopup.Title.Contains("Passenger"))
                    {
                        hdnPaxSecurityNotes.Value = tbDescription.Text;
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxSecurity);
                    }
                    if (radwindowPopup.Title.Contains("Aircraft"))
                    {
                        hdnAircraftSecurityNotes.Value = tbDescription.Text;
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnAircraftSecurity);
                    }
                    radwindowPopup.VisibleOnPageLoad = false;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnCancelPopUp_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowPopup.VisibleOnPageLoad = false;
                    if (radwindowPopup.Title.Contains("Passenger"))
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxSecurity);
                    }
                    if (radwindowPopup.Title.Contains("Aircraft"))
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnAircraftSecurity);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnCancelGridPopUp_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radGridPopUp.VisibleOnPageLoad = false;
                    if (radGridPopUp.Title.Contains("Crew Passport/Visa Details"))
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnCrewPassportVisas);
                    }
                    if (radGridPopUp.Title.Contains("Passenger Passport/Visa Details"))
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxPassportVisas);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void dgPermitDetails_BindData(object source, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
                //{
                //    //Handle methods throguh exception manager with return flag
                //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //    exManager.Process(() =>
                //    {
                //dgPermitDetails.DataSource = uwalTrip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].UWAPermits.ToList();
                //dgPermitDetails.DataSource = uwaPermList.ToList();
                //    }, FlightPak.Common.Constants.Policy.UILayer);

                //}

                //Original Code Changed to 
                //dgPermitDetails.DataSource = uwaPermList.ToList();
                if (Session["CurrentUWAPermitList"] != null)
                    uwaPermList = (List<UWAPermit>)Session["CurrentUWAPermitList"];
                else
                    uwaPermList = new List<UWAPermit>();

                List<UWAPermit> GridList = new List<UWAPermit>();
                if (uwaPermList != null && uwaPermList.Count > 0)
                {
                    GridList = uwaPermList.Where(x => x.LegID == Convert.ToInt64(hdnLegID.Value)).ToList();
                    if (GridList == null)
                        GridList = new List<UWAPermit>();
                }
                dgPermitDetails.DataSource = GridList;

            }
        }

        protected void dgPermitDetails_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;

                    if (item["ValidDate"].Text != "&nbsp;" && item["ValidDate"].Text != "")
                    {
                        DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "ValidDate");
                        item["ValidDate"].Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", date); ;
                    }
                }

                if (e.Item is GridEditableItem && e.Item.IsInEditMode)
                {
                    GridEditableItem item = e.Item as GridEditableItem;
                    TextBox TextBox1 = item.FindControl("tbCountry") as TextBox;
                    RadAjaxManager.GetCurrent(Page).FocusControl(TextBox1);
                }
            }
        }

        protected void dgPermitDetails_DeleteCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //int i = e.Item.RowIndex - 2;
                    //GridDataItem Item = (GridDataItem)dgPermitDetails.Items[0];
                    //int i = Convert.ToInt32(Item.GetDataKeyValue("PermitID"));
                    GridEditableItem editedItem = e.Item as GridEditableItem;
                    int i = Convert.ToInt32(editedItem.GetDataKeyValue("PermitID"));

                    UWAPermit uwaPerEnt = new UWAPermit();
                    List<UWAPermit> currentPermitlist = new List<UWAPermit>();
                    if (uwaPermList != null && uwaPermList.Count > 0)
                    {
                        currentPermitlist = uwaPermList.Where(x => x.LegID == Convert.ToInt64(hdnLegID.Value) && x.PermitID == i).ToList();
                        if (currentPermitlist != null && currentPermitlist.Count > 0)
                        {
                            uwaPerEnt.UWAPermit1 = currentPermitlist.First().UWAPermit1;
                            uwaPerEnt.PermitID = currentPermitlist.First().PermitID;
                            uwaPerEnt.Country = currentPermitlist.First().Country;
                            uwaPerEnt.PermitNumber = currentPermitlist.First().PermitNumber;
                            uwaPerEnt.State = TripEntityState.Deleted;
                            uwaPermList.Remove(currentPermitlist.First());
                        }

                        //uwaPermList.RemoveAt(i);
                        //uwalTrip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].UWAPermits[i].State = TripEntityState.Deleted;

                        //uwalTrip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].UWAPermits[i].State = TripEntityState.Deleted;

                    }
                    uwaPermList.Add(uwaPerEnt);
                    prefLeg.UWAPermits.Clear();
                    prefLeg.UWAPermits.AddRange(uwaPermList);
                    Session["CurrentUWAPermitList"] = uwaPermList;

                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void dgPermitDetails_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    UWAPermit uwaPerEnt = new UWAPermit();
                    GridEditableItem editedItem = e.Item as GridEditableItem;
                    GridEditManager editMan = editedItem.EditManager;
                    foreach (GridColumn column in e.Item.OwnerTableView.RenderColumns)
                    {
                        if (column is IGridEditableColumn)
                        {
                            IGridEditableColumn editableCol = (column as IGridEditableColumn);
                            if (editableCol.IsEditable)
                            {
                                IGridColumnEditor editor = editMan.GetColumnEditor(editableCol);

                                string editorText = "unknown";
                                object editorValue = null;

                                if (editor is GridTemplateColumnEditor)
                                {
                                    foreach (var c in editor.ContainerControl.Controls)
                                    {
                                        if (c is TextBox)
                                        {
                                            editorText = (c as TextBox).Text;
                                            editorValue = (c as TextBox).Text;


                                            uwaPerEnt.LegID = prefLeg.LegID;
                                            int permitcount = 0;
                                            List<UWAPermit> currentPermitlist = new List<UWAPermit>();
                                            if (uwaPermList != null && uwaPermList.Count > 0)
                                            {
                                                currentPermitlist = uwaPermList.Where(x => x.LegID == Convert.ToInt64(hdnLegID.Value)).OrderByDescending(y => y.PermitID).ToList();
                                                if (currentPermitlist != null && currentPermitlist.Count > 0)
                                                {
                                                    if (!string.IsNullOrEmpty(Convert.ToString(currentPermitlist.First().PermitID)))
                                                        permitcount = currentPermitlist.First().PermitID == null ? 0 : Convert.ToInt32(currentPermitlist.First().PermitID);
                                                }
                                            }
                                            uwaPerEnt.PermitID = permitcount + 1;
                                            if (column.UniqueName == "PermitNumber")
                                                uwaPerEnt.PermitNumber = editorText;
                                            else if (column.UniqueName == "Country")
                                                uwaPerEnt.Country = editorText;
                                            else if (column.UniqueName == "ValidDate")
                                            {
                                                //DateTime dat = DateTime.ParseExact(editorText.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                                DateTime dat = DateTime.ParseExact(editorText.ToString(), Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                                uwaPerEnt.ValidDate = dat;
                                            }
                                            uwaPerEnt.State = TripEntityState.Added;

                                        }
                                    }
                                }
                            }
                        }
                    }

                    uwaPermList.Add(uwaPerEnt);
                    prefLeg.UWAPermits.Add(uwaPerEnt);
                    Session["CurrentUWAPermitList"] = uwaPermList;
                    dgPermitDetails.Rebind();
                    RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                }, FlightPak.Common.Constants.Policy.UILayer);

            }


        }

        protected void dgPermitDetails_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    UWAPermit uwaPerEnt = new UWAPermit();
                    GridEditableItem editedItem = e.Item as GridEditableItem;
                    GridEditManager editMan = editedItem.EditManager;
                    foreach (GridColumn column in e.Item.OwnerTableView.RenderColumns)
                    {
                        if (column is IGridEditableColumn)
                        {
                            IGridEditableColumn editableCol = (column as IGridEditableColumn);
                            if (editableCol.IsEditable)
                            {
                                IGridColumnEditor editor = editMan.GetColumnEditor(editableCol);

                                string editorText = "unknown";
                                object editorValue = null;

                                if (editor is GridTemplateColumnEditor)
                                {
                                    foreach (var c in editor.ContainerControl.Controls)
                                    {
                                        if (c is TextBox)
                                        {
                                            editorText = (c as TextBox).Text;
                                            editorValue = (c as TextBox).Text;
                                        }
                                    }
                                }

                                //try
                                //{
                                // int i = e.Item.RowIndex - 2; // header colum + array count                           
                                // GridDataItem Item = (GridDataItem)dgPermitDetails.Items[0];                                   
                                //int i = Convert.ToInt32(Item.GetDataKeyValue("PermitID"));
                                int i = Convert.ToInt32(editedItem.GetDataKeyValue("PermitID"));

                                foreach (UWAPermit Uwapermit in uwaPermList)
                                {
                                    if (Uwapermit.LegID.ToString() == hdnLegID.Value && Uwapermit.PermitID == i)
                                    {
                                        if (column.UniqueName == "PermitNumber")
                                            Uwapermit.PermitNumber = editorText;
                                        else if (column.UniqueName == "Country")
                                            Uwapermit.Country = editorText;
                                        else if (column.UniqueName == "ValidDate")
                                        {
                                            //DateTime dat = DateTime.ParseExact(editorText.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                            DateTime dat = DateTime.ParseExact(editorText.ToString(), Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            Uwapermit.ValidDate = dat;
                                        }
                                        Uwapermit.State = TripEntityState.Added;
                                    }
                                }



                                //uwaPermList[i].PermitID = i + 1;
                                //if (column.UniqueName == "PermitNumber")
                                //    uwaPermList[i].PermitNumber = editorText;
                                //else if (column.UniqueName == "Country")
                                //    uwaPermList[i].Country = editorText;
                                //else if (column.UniqueName == "ValidDate")
                                //{
                                //    DateTime dat = DateTime.ParseExact(editorText.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                //    uwaPermList[i].ValidDate = dat;
                                //}
                                //uwaPermList[i].State = TripEntityState.Added;
                                //}
                                //catch (Exception ex)
                                //{
                                //    dgPermitDetails.Controls.Add(new LiteralControl("Unable to insert into Permits. Reason: " + ex.Message));
                                //    e.Canceled = true;
                                //}
                            }
                        }
                    }
                    uwaPermList.Add(uwaPerEnt);
                    prefLeg.UWAPermits.Clear();
                    prefLeg.UWAPermits.AddRange(uwaPermList);

                    Session["CurrentUWAPermitList"] = uwaPermList;
                    dgPermitDetails.Rebind();
                    RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    e.Item.OwnerTableView.IsItemInserted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void dgPermitDetails_EditCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {

            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            //{
            //    //Handle methods throguh exception manager with return flag
            //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            //    exManager.Process(() =>
            //    {



            // }, FlightPak.Common.Constants.Policy.UILayer);

        }


        protected void dgPermitDetails_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (e.CommandName == "RowClick")
                    {
                        GridDataItem test = (GridDataItem)e.Item;
                        int index = e.Item.ItemIndex;
                        e.Item.Selected = true;
                        //tbCrewName.Text = test.GetDataKeyValue("CrewName").ToString();

                        GridEditFormItem gridEditFormItem = (GridEditFormItem)e.Item;
                        TextBox txtcountry = (TextBox)gridEditFormItem.FindControl("tbCountry");
                        RadAjaxManager.GetCurrent(Page).FocusControl(txtcountry);
                    }

                    if (e.CommandName == "Edit")
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    }

                    if (e.CommandName == "PerformInsert")
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    }

                    if (e.CommandName == "Update")
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    }

                    if (e.CommandName == "Cancel")
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    }

                    if (e.CommandName == "Delete")
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPermitDetails);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void dgPermitDetails_ItemInsert(object source, GridInsertedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                e.KeepInInsertMode = false;
            }
        }

        protected void chkCrewPassportVisas_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (chkCrewPassportVisas.Checked)
                    {
                        radGridPopUp.VisibleOnPageLoad = true;
                        dgCrewPassportVisa.Visible = true;
                        dgPassengerPassportVisa.Visible = false;
                        radGridPopUp.Title = "Crew Passport/Visa Details";
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnCrewPassportVisas);
                    }
                    else
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnCrewPassportVisas);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void imgbtnCrewPassportVisas_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radGridPopUp.VisibleOnPageLoad = true;
                    dgCrewPassportVisa.Visible = true;
                    dgPassengerPassportVisa.Visible = false;
                    radGridPopUp.Title = "Crew Passport/Visa Details";
                    RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnCrewPassportVisas);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void chkPaxPassportVisas_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (chkPaxPassportVisas.Checked)
                    {
                        //using (var client = new PreflightServiceClient())
                        //{
                        //    var vUwal = client.GetUWAPassengerPassportVisa(Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegID, Convert.ToInt64(Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].CustomerID));
                        //    if (vUwal != null)
                        //    {
                        //        if (vUwal.ReturnFlag)
                        //        {
                        //            dgPassengerPassportVisa.DataSource = vUwal.EntityList.ToList();
                        //            dgPassengerPassportVisa.DataBind();
                        //        }
                        //    }
                        //}
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxPassportVisas);
                        radGridPopUp.VisibleOnPageLoad = true;
                        dgCrewPassportVisa.Visible = false;
                        dgPassengerPassportVisa.Visible = true;
                        radGridPopUp.Title = "Passenger Passport/Visa Details";
                    }
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxPassportVisas);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void imgbtnPaxPassportVisas_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //using (var client = new PreflightServiceClient())
                    //{
                    //    var vUwal = client.GetUWAPassengerPassportVisa(Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegID, Convert.ToInt64(Trip.PreflightLegs[Convert.ToInt16(hdnLeg.Value) - 1].CustomerID));
                    //    if (vUwal != null)
                    //    {
                    //        if (vUwal.ReturnFlag)
                    //        {
                    //            dgPassengerPassportVisa.DataSource = vUwal.EntityList.ToList();
                    //            dgPassengerPassportVisa.DataBind();
                    //        }
                    //    }
                    //}
                    RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnPaxPassportVisas);
                    radGridPopUp.VisibleOnPageLoad = true;
                    dgCrewPassportVisa.Visible = false;
                    dgPassengerPassportVisa.Visible = true;
                    radGridPopUp.Title = "Passenger Passport/Visa Details";
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void btnSubmittoUVTSS_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //try
                    //{
                    using (var client = new PreflightServiceClient())
                    {
                        var RetrunValue = client.TSSSubmit(Trip.TripID, "", false);
                        client.Close();

                        if (!RetrunValue.ReturnFlag)
                            RadWindowManager1.RadAlert(RetrunValue.ErrorMessage.ToString().Replace("\n", "<br />"), 300, 200, "FlightPak System", "");
                        else if (RetrunValue.ReturnFlag)
                        {
                            RadWindowManager1.RadAlert("Trip submitted to ATS successfully.   UWA TripID: " + Convert.ToString(RetrunValue.EntityInfo[0]) + "", 300, 200, "FlightPak System", "");
                            lblTripID.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(RetrunValue.EntityInfo[0]));
                            tbLastSubmitted.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", RetrunValue.EntityInfo[1].Substring(0, 10));
                        }
                        else
                            RadWindowManager1.RadAlert("Unable to connect to ATS Service.", 300, 200, "FlightPak System", "");
                    }
                    //}
                    //catch (Exception ex)
                    //{

                    //    RadWindowManager1.RadAlert("Some mandatory fields are missed in this Trip", 350, 150, "FlightPak System", "");
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowTSSSubmitAlert.Visible = false;
                    try
                    {
                        using (var client = new PreflightServiceClient())
                        {
                            {
                                var RetrunValue = client.TSSSubmit(Trip.TripID, "", false);
                                if (!RetrunValue.ReturnFlag)
                                {
                                    RadWindowManager1.RadAlert(RetrunValue.ErrorMessage.ToString().Replace("\n", "<br />"), 300, 200, "FlightPak System", "");
                                }
                                else
                                {
                                    //RadWindowManager1.RadAlert("Trip submitted to ATS successfully.", 300, 200, "FlightPak System", "");
                                    Response.Redirect("PreflightMain.aspx?seltab=PreflightMains");
                                }
                            }
                            client.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                        RadWindowManager1.RadAlert("Unable to connect to ATS Service.", 300, 200, "FlightPak System", "");
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowTSSSubmitAlert.Visible = false;

                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        protected void btnManualSubmit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowManualSubmit.VisibleOnPageLoad = true;
                    radwindowManualSubmit.Title = "Submit Manual Trip Request";

                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        protected void btnCancelManualSubmit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radwindowManualSubmit.VisibleOnPageLoad = false;

                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        protected void btnSaveManualSubmit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //    //Handle methods throguh exception manager with return flag
                //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //    exManager.Process(() =>
                //    {
                if (tbTripManualSubmit.Text != null && tbTripManualSubmit.Text != string.Empty)
                {
                    lblTripManualSubmit.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    //try
                    //{
                    using (var client = new PreflightServiceClient())
                    {
                        {
                            var RetrunValue = client.TSSSubmit(Trip.TripID, tbTripManualSubmit.Text, true);
                            if (!RetrunValue.ReturnFlag)
                            {
                                RadWindowManager1.RadAlert(RetrunValue.ErrorMessage.ToString().Replace("\n", "<br />"), 300, 200, "FlightPak System", "");
                            }
                            else if (RetrunValue.ReturnFlag)
                            {
                                radwindowManualSubmit.VisibleOnPageLoad = false;
                                RadWindowManager1.RadAlert("UWA Trip ID saved successfully.", 300, 200, "FlightPak System", "");
                                lblTripID.Text = System.Web.HttpUtility.HtmlEncode(Convert.ToString(RetrunValue.EntityInfo[0]));
                                tbLastSubmitted.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Convert.ToString(DateTime.Today.Date).Substring(0, 10));
                                //Response.Redirect("PreflightMain.aspx?seltab=PreflightMains");
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Unable to save UWA Trip ID.", 300, 200, "FlightPak System", "");
                            }
                        }
                        client.Close();
                    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    RadWindowManager1.RadAlert("Unable to connect to ATS Service.", 300, 200, "FlightPak System", "");
                    //}
                }
                else
                {
                    lblTripManualSubmit.Text = System.Web.HttpUtility.HtmlEncode("*");
                }
                //    }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        protected void btnCheckStatus_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (uwalTrip.UWATSSMains.Count > 0)
                    {
                        if (uwalTrip.UWATSSMains[0].UWATripID != string.Empty)
                        {

                            try
                            {
                                using (var client = new PreflightServiceClient())
                                {
                                    {
                                        radwindowCheckStatus.VisibleOnPageLoad = true;
                                        var objMain = client.TSSCheckStatus(uwalTrip.UWATSSMains[0].UWATripID);
                                        if (objMain.ReturnFlag)
                                        {
                                            string stringtoShow = string.Empty;

                                            foreach (string str in objMain.EntityInfo)
                                            {
                                                stringtoShow += str + "</br>";

                                            }
                                            divRdWndCheckStatus.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(stringtoShow));
                                        }
                                        else
                                        {
                                            divRdWndCheckStatus.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(objMain.ErrorMessage.ToString().Replace("\n", "<br />")));
                                        }
                                    }
                                    client.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Manually Handled
                                divRdWndCheckStatus.InnerText = System.Web.HttpUtility.HtmlEncode("Unable to connect to ATS Service.");
                                // RadWindowManager1.RadAlert("Unable to connect to ATS Service.", 300, 200, "FlightPak System", "");
                            }
                        }
                        else
                            RadWindowManager1.RadAlert("ATS Trip ID is not generated for this Trip.", 300, 200, "FlightPak System", "");
                    }
                    else
                        RadWindowManager1.RadAlert("This Trip is not submitted to ATS Service.", 300, 200, "FlightPak System", "");
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Master.EditTripEvent();
            }
        }

        #region Update Defaults

        protected void btnUpdateDefaultsSubmit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radUpdateDefaults.VisibleOnPageLoad = true;
                    radUpdateDefaults.Title = "Update Trip Request Defaults";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnUpdateDefaultsContinue_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Main Infos
                    if (chkMainInfo.Checked)
                    {
                        radlstLegSettings.SelectedValue = "3";
                        radlstType.SelectedValue = "1";
                        tbFBODepartFBOInfo.Text = string.Empty;
                        chkHandling.Checked = true;
                        chkArrivalDepartureDeclarationForm.Checked = true;
                        chkPrefileAllLegs.Checked = false;
                        chkVisasPassenger.Checked = false;
                        chkVisasCrew.Checked = false;
                        chkSecurityPassenger.Checked = false;
                        chkSecurityAircraft.Checked = false;
                        chkPermitsOverflight.Checked = true;
                        chkPermitsLanding.Checked = true;
                        chkHotelsPassenger.Checked = true;
                        chkHotelsCrew.Checked = true;
                        ChkUVairFuel.Checked = false;
                        ChkUVairFuelQuote.Checked = false;
                        chkSuppressCrew.Checked = false;
                    }

                    // Leg Info
                    if (chkHandle.Checked)
                    {
                        chkUniversalArrangesHandling.Checked = true;
                        chkUniversalArrangesHandlingCustomers.Checked = false;
                        chkWideBodyHandling.Checked = false;
                        chkHomebaseFlightFollowing.Checked = false;
                    }

                    if (chkFuel.Checked)
                    {
                        radlstFuelOn.SelectedValue = "1";
                        chkPristRequired.Checked = false;
                    }

                    if (Session["CurrentPreFlightUWAL"] != null)
                    {
                        uwalTrip = (PreflightMain)Session["CurrentPreFlightUWAL"];

                        foreach (PreflightLeg prefLeg in uwalTrip.PreflightLegs)
                        {
                            if (prefLeg.UWALs != null && prefLeg.UWALs.Count > 0)
                            {
                                prefLeg.UWALs[0].IsUWAArrangeHandling = true;
                                prefLeg.UWALs[0].IsUWAArrangeCustoms = false;
                                prefLeg.UWALs[0].IsHomebaseFlightFollowing = false;
                                prefLeg.UWALs[0].IsWideBodyHandling = false;
                                prefLeg.UWALs[0].IsPristRequired = false;
                                prefLeg.UWALs[0].FuelOnArrivalDeparture = 1;
                            }

                            if (prefLeg.LegID == Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].LegID)
                            {
                                setFieldValues(prefLeg);
                            }
                        }

                        Session["CurrentPreFlightUWAL"] = uwalTrip;
                    }

                    using (var client = new PreflightServiceClient())
                    {
                        client.UpdateUWAL(uwalTrip);
                        Session["CurrentPreFlightUWAL"] = uwalTrip;
                        Trip.Mode = TripActionMode.NoChange;
                    }

                    radUpdateDefaults.VisibleOnPageLoad = false;
                    RadWindowManager1.RadAlert("Defaults values have been saved.", 300, 100, "FlightPak System", "callBackFnToReload");
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnUpdateDefaultClearYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radlstLegSettings.SelectedValue = "4";
                    radlstType.SelectedValue = "2";
                    tbFBODepartFBOInfo.Text = string.Empty;
                    chkHandling.Checked = false;
                    chkArrivalDepartureDeclarationForm.Checked = false;
                    chkPrefileAllLegs.Checked = false;
                    chkVisasPassenger.Checked = false;
                    chkVisasCrew.Checked = false;
                    chkSecurityPassenger.Checked = false;
                    chkSecurityAircraft.Checked = false;
                    chkPermitsOverflight.Checked = false;
                    chkPermitsLanding.Checked = false;
                    chkHotelsPassenger.Checked = false;
                    chkHotelsCrew.Checked = false;
                    ChkUVairFuel.Checked = false;
                    ChkUVairFuelQuote.Checked = false;
                    chkSuppressCrew.Checked = false;

                    // Leg Info
                    chkUniversalArrangesHandling.Checked = false;
                    chkUniversalArrangesHandlingCustomers.Checked = false;
                    chkWideBodyHandling.Checked = false;
                    chkHomebaseFlightFollowing.Checked = false;
                    radlstFuelOn.SelectedValue = "1";
                    chkPristRequired.Checked = false;

                    if (Session["CurrentPreFlightUWAL"] != null)
                    {
                        uwalTrip = (PreflightMain)Session["CurrentPreFlightUWAL"];

                        foreach (PreflightLeg prefLeg in uwalTrip.PreflightLegs)
                        {
                            if (prefLeg.UWALs != null && prefLeg.UWALs.Count > 0)
                            {
                                prefLeg.UWALs[0].IsUWAArrangeHandling = false;
                                prefLeg.UWALs[0].IsUWAArrangeCustoms = false;
                                prefLeg.UWALs[0].IsHomebaseFlightFollowing = false;
                                prefLeg.UWALs[0].IsWideBodyHandling = false;
                                prefLeg.UWALs[0].IsPristRequired = false;
                                prefLeg.UWALs[0].FuelOnArrivalDeparture = 1;
                            }

                            if (prefLeg.LegID == Trip.PreflightLegs[Convert.ToInt32(hdnLeg.Value) - 1].LegID)
                            {
                                setFieldValues(prefLeg);
                            }
                        }
                        Session["CurrentPreFlightUWAL"] = uwalTrip;
                    }

                    using (var client = new PreflightServiceClient())
                    {
                        client.UpdateUWAL(uwalTrip);
                        Session["CurrentPreFlightUWAL"] = uwalTrip;
                        Trip.Mode = TripActionMode.NoChange;
                    }

                    radUpdateDefaults.VisibleOnPageLoad = false;
                    RadWindowManager1.RadAlert("Defaults values have been cleared.", 300, 100, "FlightPak System", "");
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        protected void btnUpdateDefaultClearNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void btnUpdateDefaultsCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    radUpdateDefaults.VisibleOnPageLoad = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion
    }
}