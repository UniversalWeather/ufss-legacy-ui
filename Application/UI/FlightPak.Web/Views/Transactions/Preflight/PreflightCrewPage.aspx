﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightCrewPage.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightCrewPage" ClientIDMode="AutoID" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager runat="server" ID="radScript">
        </telerik:RadScriptManager>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="../../../Scripts/Common.js"></script>
            <script type="text/javascript">

                // this function is used to display the format when the relevent textbox is empty     .
                function validateEmptyTextbox(ctrlID, e) {

                    if (ctrlID.value == "") {
                        ctrlID.value = "0.00";
                    }
                }
                //this function is used to parse the date entered or selected by the user
                function parseDate(sender, e) {
                    if (currentDatePicker != null) {
                        var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                        var dateInput = currentDatePicker.get_dateInput();
                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                        alert(formattedDate);
                        sender.value = formattedDate;
                    }
                }
                // To Store Image
                function File_onchange() {
                    __doPostBack('__Page', 'LoadImage');
                }

                function CheckName() {
                    var nam = document.getElementById('<%=tbImgName.ClientID%>').value;

                    if (nam != "") {
                        document.getElementById("<%=fileUL.ClientID %>").disabled = false;
                    }
                    else {
                        document.getElementById("<%=fileUL.ClientID %>").disabled = true;
                    }
                }

                // this function is used to allow user to check only one checkbox
                function CheckOne(obj) {
                    var grid = obj.parentNode.parentNode.parentNode;
                    var inputs = grid.getElementsByTagName("input");
                    for (var i = 0; i < inputs.length; i++) {
                        if (inputs[i].type == "checkbox") {
                            if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                                inputs[i].checked = false;
                            }
                        }
                    }
                }

                //this function is used to navigate to pop up screen's with the selected code
                function openWin(radWin) {
                    var url = '';
                    if (radWin == "RadCountryPopup") {
                        url = '../Company/CountryMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbCountry.ClientID%>').value;
                    }
                    else if (radWin == "radAirportPopup") {
                        url = '../Airports/AirportMasterPopup.aspx';
                    }
                    else if (radWin == "RadCrewGroupPopup") {

                        url = "../People/CrewGroupPopup.aspx?CrewGroupCD=" + document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value;
                    }
                    else if (radWin == "RadClientCodePopup") {
                        url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCode.ClientID%>").value;
                    }
                    else if (radWin == "RadFilterClientPopup") {
                        url = "../Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCodeFilter.ClientID%>").value;
                    }
                    else if (radWin == "RadCitizenPopup") {
                        url = '../Company/CountryMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbCountry.ClientID%>').value;
                    }
                    else if (radWin == "RadLicCountryPopup") {
                        url = '../Company/CountryMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbLicCountry.ClientID%>').value;
                    }
                    else if (radWin == "RadAddLicCountryPopup") {
                        url = '../Company/CountryMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbAddLicCountry.ClientID%>').value;
                    }
                    else if (radWin == "RadCountryOfBirthPopup") {
                        url = '../Company/CountryMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbCountryBirth.ClientID%>').value;
                    }
                    else if (radWin == "RadDeptPopup") {
                        url = '../Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('<%=tbDepartment.ClientID%>').value;
                    }
                    else if (radWin == "RadAddlInfo") {
                        url = '../People/CrewRosterAddInfoPopup.aspx';
                    }
                    else if (radWin == "RadAircraftTypePopup") {
                        url = '../Fleet/CrewRosterAircraftTypePopup.aspx';
                    }
                    else if (radWin == "RadCrewCheckListPopup") {
                        url = '../People/CrewCheckListPopup.aspx';
                    }
                    else if (radWin == "RadAicraftType") {
                        url = '../Fleet/CrewRosterAircraftTypePopup.aspx?AircraftTypeCD=' + document.getElementById('<%=tbAircraftType.ClientID%>').value;
                    }
                    else if (radWin == "RadChecklistPopUp") {
                        url = '../People/CrewRosterChecklistSettings.aspx?CrewCD=' + document.getElementById('<%=tbCrewCode.ClientID%>').value + '&CrewChecklistCD =' + document.getElementById('<%=lbCheckListCode.ClientID%>').value;
                    }
                    else if (radWin == "RadExportData") {
                        url = "../../Reports/ExportReportInformation.aspx?Report=RptDBCrewRoaster&CrewCD=" + document.getElementById('<%=tbCrewCode.ClientID%>').value;
                    }
                    else if (radWin == "RadCrewRosterReportsPopup") {
                        url = "../People/CrewRosterReportsPopup.aspx?UserCD=UC&CrewCD=" + document.getElementById('<%=tbCrewCode.ClientID%>').value;
                    }
                    var oWnd = radopen(url, radWin);
                }

                // this function is related to Image 
                function OpenRadWindow() {
                    var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                    document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
                    oWnd.show();
                }

                // this function is related to Image 
                function CloseRadWindow() {
                    var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                    document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                    oWnd.close();
                }
                // this function is used to display the value of selected country code from popup
                function CountryPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnCountryID.ClientID%>").innerHTML = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbCountry.ClientID%>").value = "";
                            document.getElementById("<%=hdnCountryID.ClientID%>").innerHTML = "";
                        }
                    }
                }

                //this handler is used to set the text of the TextBox to the value of selected from the popup


                // this function is used to display the value of selected country code from popup
                function LicCountryPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbLicCountry.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=cvLicCountry.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnLicCountryID.ClientID%>").value = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbLicCountry.ClientID%>").value = "";
                            document.getElementById("<%=hdnLicCountryID.ClientID%>").value = "";
                        }
                    }
                }

                // this function is used to display the value of selected country code from popup
                function AddLicCountryPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbAddLicCountry.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=cvAddLicCountry.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnAddLicCountryID.ClientID%>").value = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbAddLicCountry.ClientID%>").value = "";
                            document.getElementById("<%=hdnAddLicCountryID.ClientID%>").value = "";
                        }
                    }
                }

                // this function is used to display the value of selected country code from popup
                function AddCountryOfBirthPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbCountryBirth.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=cvCountryBirth.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnResidenceCountryID.ClientID%>").value = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbCountryBirth.ClientID%>").value = "";
                            document.getElementById("<%=hdnResidenceCountryID.ClientID%>").value = "";
                        }
                    }
                }

                // this function is used to display the value of selected homebase code from popup
                function HomeBasePopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;

                        }
                        else {
                            document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                            document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                        }
                    }
                }

                //this handler is used to set the text of the TextBox to the value of selected from the popup
                function dateSelected(sender, args) {


                    if (currentTextBox != null) {
                        var step = "Date_TextChanged";

                        if (currentTextBox.value != args.get_newValue()) {

                            currentTextBox.value = args.get_newValue();
                            var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                    }
                }


                function CrewGroupPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value = arg.CrewGroupCD;
                            document.getElementById("<%=hdnCrewGroupID.ClientID%>").value = arg.ClientID;


                            var step = "CrewGroupFilter_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }

                        }
                        else {
                            document.getElementById("<%=tbCrewGroupFilter.ClientID%>").value = "";
                        }
                    }
                }



                // this function is used to display the value of selected client code from popup
                function ClientCodePopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbClientCode.ClientID%>").value = arg.ClientCD;
                            document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnClientID.ClientID%>").value = arg.ClientID;
                        }
                    }
                }

                function ClientCodeFilterPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                            var step = "ClientCodeFilter_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                            //                        document.getElementById("<%=cvClientCode.ClientID%>").innerHTML = "";
                            //                        document.getElementById("<%=hdnClientID.ClientID%>").value = arg.ClientID;
                        }
                    }
                }
                
                // this function is used to display the value of selected country code from popup
                function CitizenPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbCitizenCode.ClientID%>").value = arg.CountryCD;
                            document.getElementById("<%=lbCitizenDesc.ClientID%>").value = arg.CountryName;
                            document.getElementById("<%=cvCitizenCode.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnCitizenshipID.ClientID%>").value = arg.CountryID;
                        }
                        else {
                            document.getElementById("<%=tbCitizenCode.ClientID%>").value = "";
                            document.getElementById("<%=lbCitizenDesc.ClientID%>").value = "";
                            document.getElementById("<%=hdnCitizenshipID.ClientID%>").value = "";
                        }
                    }
                }

                // this function is used to display the value of selected aircrafttype code from popup
                function AicraftTypePopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();

                    var step = "AircraftType_TextChanged";
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");

                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbAircraftType.ClientID%>").value = arg.AircraftCD;
                            document.getElementById("<%=cvAssociateTypeCode.ClientID%>").innerHTML = "";

                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }

                    }
                }

                // this function is used to display the value of selected department code from popup
                function DeptPopupClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                            document.getElementById("<%=cvDepartmentCode.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdnDepartmentID.ClientID%>").value = arg.DepartmentID;
                        }
                        else {
                            document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                            document.getElementById("<%=hdnDepartmentID.ClientID%>").value = "";
                        }
                    }
                }

                // this function is to validate the required field on tab out
                function CheckTxtBox(control) {

                    if (control == 'lastName') {
                        ValidatorEnable(document.getElementById('<%=rfvLastName.ClientID%>'));
                    }
                }



                var currentTextBox = null;
                var currentDatePicker = null;

                //This method is called to handle the onclick and onfocus client side events for the texbox
                function showPopup(sender, e) {
                    //this is a reference to the texbox which raised the event
                    //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                    currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);



                    //this gets a reference to the datepicker, which will be shown, to facilitate
                    //the selection of a date
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    //this variable is used to store a reference to the date picker, which is currently
                    //active
                    currentDatePicker = datePicker;

                    //this method first parses the date, that the user entered or selected, and then
                    //sets it as a selected date to the picker


                    datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                    //the code lines below show the calendar, which is used to select a date. The showPopup
                    //function takes three arguments - the x and y coordinates where to show the calendar, as
                    //well as its height, derived from the offsetHeight property of the textbox
                    var position = datePicker.getElementPosition(currentTextBox);
                    datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                }


                // this function is used to display the crew additional information popup 
                function ShowAddlInfoPopup() {
                    window.radopen("../People/CrewRosterAddInfoPopup.aspx", "RadAddlInfo");
                    return false;
                }

                // this function is used to display the crew checklist settings popup 
                function ShowChecklistSettingsPopup() {
                    window.radopen("../People/CrewRosterChecklistSettings.aspx", "RadChecklistPopUp");
                    return false;

                }

                // this function is used to display the aircraft type popup 
                function ShowAircraftTypePopup() {
                    window.radopen("../Fleet/CrewRosterAircraftTypePopup.aspx", "RadAircraftTypePopup");
                    return false;
                }

                // this function is used to display the crew checklist popup 
                function ShowCrewCheckListPopup() {
                    window.radopen("../People/CrewCheckListPopup.aspx", "RadCrewCheckListPopup");
                    return false;
                }

                // this function is used to the refresh the currency grid
                function refreshGrid(arg) {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
                }

                // this function is used to get the dimensions
                function GetDimensions(sender, args) {
                    var bounds = sender.getWindowBounds();
                    return;
                }

                //this function is used to resize the pop-up window
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                //this function is used to expand the panel on click
                function OnClientClick(strPanelToExpand) {
                    var PanelBar1 = $find("<%= pnlAdditionalInfo.ClientID %>");
                    var PanelBar2 = $find("<%= pnlCurrency.ClientID %>");
                    var PanelBar3 = $find("<%= pnlAdditionalNotes.ClientID %>");
                    var PanelBar4 = $find("<%= pnlMaintenance.ClientID %>");
                    PanelBar1.get_items().getItem(0).set_expanded(false);
                    PanelBar2.get_items().getItem(0).set_expanded(false);
                    PanelBar3.get_items().getItem(0).set_expanded(false);
                    PanelBar4.get_items().getItem(0).set_expanded(false);
                    if (strPanelToExpand == "AdditionalNotes") {
                        PanelBar3.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "Currency") {
                        PanelBar2.get_items().getItem(0).set_expanded(true);
                    }
                    else if (strPanelToExpand == "AdditionalInformation") {
                        PanelBar1.get_items().getItem(0).set_expanded(true);
                    }
                    return false;
                }


                var checkCount = 0

                //maximum number of allowed checked boxes
                var maxChecks = 3

                // this function is used to limit the number of checkboxes to be checked
                function chkcontrol(obj) {

                    //increment/decrement checkCount
                    if (obj.checked) {
                        checkCount = checkCount + 1
                    } else {
                        checkCount = checkCount - 1
                    }

                    //if they checked a 4th box, uncheck the box, then decrement checkcount and pop alert
                    if (checkCount > maxChecks) {
                        obj.checked = false
                        checkCount = checkCount - 1
                        alert('Maximim 3 options can be selected from Additional Info')
                    }
                }

                var GControlId, GhdnControlId;
                function openWinGrd(radWin, ControlId) {
                    GControlId = ControlId;
                    var ControlCheck = ControlId.id;
                    if (ControlCheck.indexOf("Passport") != -1) {
                        GhdnControlId = ControlCheck.replace("tbPassportCountry", "hdnPassportCountry");
                    }
                    else {
                        GhdnControlId = ControlCheck.replace("tbCountryCD", "hdnCountryID");
                    }
                    var url = '';
                    if (radWin == "RadVisaCountryMasterPopup") {
                        url = '../Company/CountryMasterPopup.aspx?CountryCD=' + ControlId.value;
                    }
                    var oWnd = radopen(url, radWin);
                }

                function OnClientVisaCountryPopup(oWnd, args) {
                    var combo = GControlId;
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            GControlId.value = arg.CountryCD;
                            GhdnControlId.value = arg.CountryID;
                        }
                        else {
                            GControlId.value = "";
                            GhdnControlId.value = "";
                            combo.clearSelection();
                        }
                    }
                }

                function ZuluChange() {
                    var radZULU = document.getElementById('<%=radZULU.ClientID%>').checked;
                    var radLocal = document.getElementById('<%=radLocal.ClientID%>').checked;

                    if (radZULU == true) {
                        radalert('E-mail sent will now be in UTC time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');
                    }
                    else { //if (radLocal == true) 
                        radalert('E-mail sent will now be in Local time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');
                    }
                    return false;
                }
            </script>
        </telerik:RadCodeBlock>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr style="display: none;">
                <td>
                    <%--<uc:datepicker id="ucDatePicker" runat="server"></uc:datepicker>--%>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div class="tab-nav-top">
                        <%--  <span class="head-title">Crew Roster</span><span class="tab-nav-icons">--%>
                        <asp:LinkButton ID="lbtnReports" Visible="false" runat="server" OnClientClick="javascript:openWin('RadCrewRosterReportsPopup');return false;"
                            class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" Visible="false" OnClientClick="javascript:openWin('RadExportData');return false;"
                            class="save-icon"></asp:LinkButton>
                        <%--  <a  href="#" class="print-icon"></a><a href="#" class="help-icon"> </a>--%>
                    </div>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td>
                    <div class="status-list">
                        <span>
                            <asp:CheckBox ID="chkActiveOnly" runat="server" Checked="false" Text="Active Only"
                                AutoPostBack="true" Visible="false" OnCheckedChanged="ActiveOnly_CheckedChanged" /></span>
                        <span>
                            <asp:CheckBox ID="chkHomeBaseOnly" runat="server" Checked="false" Text="Home Base Only"
                                AutoPostBack="true" Visible="false" OnCheckedChanged="HomeBaseOnly_CheckedChanged" /></span>
                        <span>
                            <asp:CheckBox ID="chkFixedWingOnly" runat="server" Checked="false" Text="Fixed Wing"
                                AutoPostBack="true" Visible="false" OnCheckedChanged="FixedWingOnly_CheckedChanged" /></span>
                        <span>
                            <asp:CheckBox ID="chkRotaryWingOnly" runat="server" Checked="false" Text="Rotary Wing"
                                AutoPostBack="true" Visible="false" OnCheckedChanged="RotaryWing_CheckedChanged" /></span>
                        <span>
                            <table id="Table1" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lbClientCode" Visible="false">Client Code :</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            ValidationGroup="Save" OnTextChanged="ClientCodeFilter_TextChanged" Visible="false"
                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                    </td>
                                    <td>
                                        <%-- <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadFilterClientPopup');return false;"
                                            CssClass="browse-button" runat="server" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Label runat="server" ID="lbClientCodeFilter" Text="Invalid ClientCode" Visible="false"
                                            CssClass="alert-text"></asp:Label>
                                        <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                            ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </span>
                        <table id="Exception" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label2" Visible="false">Crew Group :</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbCrewGroupFilter" CssClass="text50" runat="server" Visible="false"
                                        OnTextChanged="CrewGroupFilter_TextChanged" AutoPostBack="true" />
                                </td>
                                <td>
                                    <%--  <asp:Button ID="btnCrewGroupFilter" OnClientClick="javascript:openWin('RadCrewGroupPopup');return false;"
                                        CssClass="browse-button" runat="server" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label runat="server" ID="lbCrewGroupCodeFilter" Text="Invalid Crew Group" Visible="false"
                                        CssClass="alert-text"></asp:Label>
                                    <asp:CustomValidator ID="cvCrewGroupCodeFilter" runat="server" ControlToValidate="tbCrewGroupFilter"
                                        ErrorMessage="Invalid Crew Group" Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
            <ClientEvents OnDateSelected="dateSelected" />
            <DateInput ID="DateInput1" runat="server">
            </DateInput>
            <Calendar ID="Calendar1" runat="server">
                <SpecialDays>
                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                        ItemStyle-BorderStyle="Solid">
                    </telerik:RadCalendarDay>
                </SpecialDays>
            </Calendar>
        </telerik:RadDatePicker>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkActiveOnly">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkHomeBaseOnly">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkFixedWingOnly">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkRotaryWingOnly">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSaveChangesTop">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancelTop">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgCrewRoster">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgAircraftAssigned">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAircraftAssigned" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewAddlInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadDatePicker1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="fileUL">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlImg" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="imgFile" />
                        <telerik:AjaxUpdatedControl ControlID="PnlImg" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlImg">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="imgFile" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="PnlImg" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btndeleteImage">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="imgFile" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="PnlImg" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btndeleteImage">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="imgFile" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="PnlImg" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbTotalHrsInType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbTotalHrsDay">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAsOfDt">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnFlightExp">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbTotalHrsNight">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbTotalHrsInstrument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbPICHrsInType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbPICHrsDay">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbPICHrsNight">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbPICHrsInstrument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbSICInTypeHrs">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbSICHrsDay">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbSICHrsNight">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbSICHrsInstrument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbOtherHrsSimulator">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbOtherHrsFltEngineer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbOtherHrsFltInstructor">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbOtherHrsFltAttendant">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRPIC135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRSIC91">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRSIC135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTREngineer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRInstructor">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRAttendant">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRAttendant91">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRAttendant135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRAirman">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRInactive">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkPIC121">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSIC121">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkPIC125">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSIC125">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkAttendent121">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkAttendent125">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbGraceDays">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAlertDays">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbFreq">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAircraftType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnDeleteInfo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewAddlInfo" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbGraceDate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAlertDate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbTotalReqFlightHrs">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbDueNext">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radFreq">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnDeleteChecklist">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnDeleteRating">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnRefreshView">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCurrencyChecklist" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCurrency" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCrewGroupFilter">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="lbCrewGroupCodeFilter" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbClientCodeFilter">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewRoster" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="lbClientCodeFilter" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgTypeRating">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgCrewCheckList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnUpdateFltExperience">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnUpdateAllFltExperience">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRPIC91">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRInactive">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRSIC91">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRPIC135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkTRSIC135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tblTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgTypeRating" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="CountryCD">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgVisa" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgVisa">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgVisa" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnChgBaseDate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbChgBaseDate">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkPIC91">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkPIC135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSIC91">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSIC135">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkInactive">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSetToEndOfMonth">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSetToNextOfMonth">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkSetToEndOfCalenderYear">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ckhDisableDateCalculation">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkOneTimeEvent">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tblCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkCompleted">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkNonConflictedEvent">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkRemoveFromCrewRosterRept">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkRemoveFromChecklistRept">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkDoNotPrintPassDueAlert">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkPrintOneTimeEventCompleted">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCrewCheckList" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCountry">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="cvCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCountryBirth">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbCountryBirth" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="cvCountryBirth" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCitizenCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbCitizenCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="cvCitizenCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbLicCountry">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbLicCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="cvLicCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbAddLicCountry">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbAddLicCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="cvAddLicCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbDepartment">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbDepartment" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="cvDepartmentCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="CountryPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadLicCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="LicCountryPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadAddLicCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="AddLicCountryPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadCountryOfBirthPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="AddCountryOfBirthPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadCrewGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="CrewGroupPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewGroupPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="ClientCodePopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadFilterClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="ClientCodeFilterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadCrewRosterReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Title="Crew Roster Reports" Modal="true"
                    Height="600px" Width="400px" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterReportsPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadCitizenPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="CitizenPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadVisaCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientVisaCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadDeptPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="DeptPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadAddlInfo" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadAircraftTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadCrewCheckListPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadAicraftType" runat="server" OnClientResizeEnd="GetDimensions"
                    Title="Aircraft Type" OnClientClose="AicraftTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/CrewRosterAircraftTypePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadChecklistPopUp" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Title="Select Checklist Settings & Crew Members"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterChecklistSettings.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                    Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                    <ContentTemplate>
                        <div>
                            <asp:Image ID="ImgPopup" runat="server" />
                        </div>
                    </ContentTemplate>
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadGrid ID="dgCrewRoster" Visible="false" runat="server" AllowSorting="true"
            OnNeedDataSource="CrewRoster_BindData" Height="341px" OnItemCreated="CrewRoster_ItemCreated"
            OnItemCommand="CrewRoster_ItemCommand" OnPreRender="CrewRoster_PreRender" OnUpdateCommand="CrewRoster_UpdateCommand"
            OnInsertCommand="CrewRoster_InsertCommand" OnPageIndexChanged="CrewRoster_PageIndexChanged"
            OnDeleteCommand="CrewRoster_DeleteCommand" OnSelectedIndexChanged="CrewRoster_SelectedIndexChanged"
            OnItemDataBound="CrewRoster_ItemDataBound" AutoGene AutoGenerateColumns                                                                                                                                                                                                                                ="false" PageSize="10"
            AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
            <MasterTableView DataKeyNames="CustomerID,CrewID,CrewCD,ClientCD,ClientID,HomeBaseID,HomeBaseCD,CrewTypeCD,DepartmentCD,IcaoID,LastUpdUID,LastUpdTS,NationalityCD,CitizenshipCD,LicenseCountry1,LicenseCountry2,ResidenceCountryCD"
                CommandItemDisplay="Bottom">
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="LastName" SortOrder="Ascending" />
                </SortExpressions>
                <Columns>
                    <telerik:GridBoundColumn DataField="CrewCD" HeaderText="Crew Code" CurrentFilterFunction="Contains"
                        AllowSorting="true" SortExpression="" FilterDelay="4000" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CrewName" HeaderText="Crew Name" CurrentFilterFunction="Contains"
                        AllowSorting="true" SortExpression="" FilterDelay="4000" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" CurrentFilterFunction="Contains"
                        FilterDelay="4000" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="Contains"
                        FilterDelay="4000" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    
                    <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" CurrentFilterFunction="Contains"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CrewCD" HeaderText="CrewCD" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ClientCD" HeaderText="ClientCD" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NationalityCD" HeaderText="NationalityCD" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ResidenceCountryCD" HeaderText="ResidenceCountryCD"
                        CurrentFilterFunction="Contains"  Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CitizenshipCD" HeaderText="CitizenshipCD" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LicenseCountry1" HeaderText="LicenseCountry1"
                        CurrentFilterFunction="Contains"  Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LicenseCountry2" HeaderText="LicenseCountry2"
                        CurrentFilterFunction="Contains"  Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="DepartmentCD" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false"
                        HeaderText="Checklist" HeaderStyle-HorizontalAlign="Center" AllowFiltering="false">
                        <ItemTemplate>
                            <center>
                                <asp:Label ID="lbCheckList" runat="server" Text='<%# Eval("CheckList") %>'></asp:Label>
                            </center>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="CrewID" HeaderText="CrewID" CurrentFilterFunction="Contains"
                         Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddCrewRoster)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                            Visible='<%# IsAuthorized("Permission.Database.EditCrewRoster")%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                            Visible='<%# IsAuthorized("Permission.Database.DeleteCrewRoster")%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <div id="divExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            <div class="mandatory subfix">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnAdditionalNotes" Visible="false" runat="server" CssClass="ui_nav"
                                            Text="Additional Notes" OnClientClick="javascript:OnClientClick('AdditionalNotes');return false;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCurrency" runat="server" Visible="false" CssClass="ui_nav" Text="Currency"
                                            OnClientClick="javascript:OnClientClick('Currency');return false;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAdditionalInformation" runat="server" Visible="false" CssClass="ui_nav"
                                            Text="Additional Information" OnClientClick="javascript:OnClientClick('AdditionalInformation');return false;" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSaveChangesTop" runat="server" Visible="false" CssClass="button"
                                            Text="Save" OnClick="SaveChangesTop_Click" ValidationGroup="Save" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancelTop" runat="server" Visible="false" CssClass="button" Text="Cancel"
                                            CausesValidation="false" OnClick="CancelTop_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <telerik:RadTabStrip ID="tabCrewRoster" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                                MultiPageID="RadMultiPage1" CausesValidation="false" SelectedIndex="0" Align="Justify"
                                Width="100%">
                                <Tabs>
                                    <telerik:RadTab Text="Main Information">
                                    </telerik:RadTab>
                                    <telerik:RadTab Text="Type Ratings">
                                    </telerik:RadTab>
                                    <telerik:RadTab Text="CheckList">
                                    </telerik:RadTab>
                                    <telerik:RadTab Text="Aircraft Assigned">
                                    </telerik:RadTab>
                                    <telerik:RadTab Text="Passports/Visas">
                                    </telerik:RadTab>
                                    <telerik:RadTab Text="E-mail Preferences">
                                    </telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="100%">
                                <telerik:RadPageView ID="MainInfo" runat="server">
                                    <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="None"
                                        CollapseAnimation-Type="None" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem runat="server" Expanded="true" Text="&nbsp">
                                                <Items>
                                                    <telerik:RadPanelItem>
                                                        <ContentTemplate>
                                                            <table width="100%" class="box1">
                                                                <tr>
                                                                    <td align="left">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel100">
                                                                                    <asp:CheckBox ID="chkActiveCrew" runat="server" Checked="true" />Active Crew
                                                                                </td>
                                                                                <td class="tdLabel110">
                                                                                    <asp:CheckBox ID="chkFixedWing" runat="server" Checked="true" />Fixed Wing
                                                                                </td>
                                                                                <td class="tdLabel110">
                                                                                    <asp:CheckBox ID="chkRoteryWing" runat="server" />Rotary Wing
                                                                                </td>
                                                                                <td class="tdLabel150">
                                                                                    <asp:CheckBox ID="chkNoCalenderDisplay" runat="server" Checked="true" />No Calendar
                                                                                    Display
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tblspace_10">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    <span class="mnd_text">Crew Code</span>
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbCrewCode" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        Enabled="false" ValidationGroup="Save" CssClass="text60" AutoPostBack="true"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Home Base
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" ValidationGroup="Save"
                                                                                        OnTextChanged="HomeBase_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                        AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                    <asp:Button ID="btnHomeBase" OnClientClick="javascript:openWin('radAirportPopup');return false;"
                                                                                        CssClass="browse-button" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                                        ErrorMessage="Invalid Home Base" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Client Code
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbClientCode" runat="server" MaxLength="5" ValidationGroup="Save"
                                                                                        OnTextChanged="ClientCode_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                        AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                    <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                                                        CssClass="browse-button" runat="server" />
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Department
                                                                                </td>
                                                                                <td class="tdLabel210">
                                                                                    <asp:TextBox ID="tbDepartment" MaxLength="8" AutoPostBack="true" OnTextChanged="Department_TextChanged"
                                                                                        runat="server" CssClass="text60"></asp:TextBox>
                                                                                    <asp:Button ID="btnDepartment" OnClientClick="javascript:openWin('RadDeptPopup');return false;"
                                                                                        CssClass="browse-button" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:CustomValidator ID="cvClientCode" runat="server" ControlToValidate="tbClientCode"
                                                                                        ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvDepartmentCode" runat="server" ControlToValidate="tbDepartment"
                                                                                        ErrorMessage="Invalid Department" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tblspace_10">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    <span class="mnd_text">First Name</span>
                                                                                </td>
                                                                                <td class="tdLabel150">
                                                                                    <asp:TextBox ID="tbFirstName" runat="server" MaxLength="20" CssClass="text120"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <span>Middle Name</span>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    <asp:TextBox ID="tbMiddleName" runat="server" MaxLength="20" CssClass="text100"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                    <span class="mnd_text">Last Name</span>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbLastName" MaxLength="30" runat="server" CssClass="text120"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel150">
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="tbLastName"
                                                                                        ValidationGroup="Save" ErrorMessage="Last Name is Required" Display="Dynamic"
                                                                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Address 1
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbAddress1" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Address 2
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAddress2" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Address Line 3
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAddress3" runat="server" MaxLength="40" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    City
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbCity" runat="server" MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    State/Province
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbStateProvince" runat="server" MaxLength="10" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Country
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbCountry" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        OnTextChanged="Country_TextChanged" AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                    <asp:Button ID="btnCountryPopup" OnClientClick="javascript:openWin('RadCountryPopup');return false;"
                                                                                        CssClass="browse-button" runat="server" />
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Postal
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbStateProvincePostal" runat="server" MaxLength="15" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                                        ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Gender
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:RadioButton ID="rbMale" runat="server" Text="Male" Checked="true" GroupName="gender" />
                                                                                    <asp:RadioButton ID="rbFemale" runat="server" Text="Female" GroupName="gender" />
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Citizenship
                                                                                </td>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel140">
                                                                                                <asp:TextBox ID="tbCitizenCode" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                    OnTextChanged="CitizenCode_TextChanged" AutoPostBack="true" runat="server" CssClass="text60"></asp:TextBox>
                                                                                                <asp:Button ID="btnCitizenship" OnClientClick="javascript:openWin('RadCitizenPopup');return false;"
                                                                                                    CssClass="browse-button" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbCitizenDesc" runat="server" CssClass="text100"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvCitizenCode" runat="server" ControlToValidate="tbCitizenCode"
                                                                                        ErrorMessage="Invalid CitizenCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Date of Birth
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <%--<uc:DatePicker ID="ucDateOfBirth" runat="server"></uc:DatePicker>--%>
                                                                                    <asp:TextBox ID="tbDateOfBirth" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                        onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                        onBlur="parseDate(this, event);"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    City of Birth
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbCityBirth" MaxLength="30" runat="server" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    State/Prov of Birth
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbStateBirth" runat="server" MaxLength="10" CssClass="text80"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Country of Birth
                                                                                </td>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel140">
                                                                                                <asp:TextBox ID="tbCountryBirth" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                    OnTextChanged="CountryBirth_TextChanged" AutoPostBack="true" CssClass="text60"></asp:TextBox>
                                                                                                <asp:Button ID="btnCountryOfBirth" OnClientClick="javascript:openWin('RadCountryOfBirthPopup');return false;"
                                                                                                    CssClass="browse-button" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvCountryBirth" runat="server" ControlToValidate="tbCountryBirth"
                                                                                        ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tblspace_10">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Home Phone
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbHomePhone" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Business Phone
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Other Phone
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbOtherPhone" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                        MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Pager No.
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPager" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                        MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Primary Mobile/Cell
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbMobilePhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Secondary Mobile/Cell
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbSecondaryMobile" runat="server" MaxLength="25" CssClass="text200"
                                                                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Business E-mail
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbEmail" runat="server" MaxLength="200" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Personal E-mail
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="250" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="tbEmail"
                                                                                        ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ID="revPersonalEmail" runat="server" ControlToValidate="tbPersonalEmail"
                                                                                        ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Other E-mail
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="250" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:RegularExpressionValidator ID="revOtherEmail" runat="server" ControlToValidate="tbOtherEmail"
                                                                                        ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Home Fax
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbFax" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                        CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Business Fax
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbBusinessFax" runat="server" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                        CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tblspace_10">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    SSN
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <asp:TextBox ID="tbSSN" runat="server" MaxLength="11" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Crew Type
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbCrewType" runat="server" MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel140">
                                                                                    Hire Date
                                                                                </td>
                                                                                <td class="tdLabel250">
                                                                                    <%-- <uc:DatePicker ID="ucHireDT" runat="server"></uc:DatePicker>--%>
                                                                                    <asp:TextBox ID="tbHireDT" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                        onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                        onBlur="parseDate(this, event);"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel140">
                                                                                    Termination Date
                                                                                </td>
                                                                                <td>
                                                                                    <%--<uc:DatePicker ID="ucTermDT" runat="server" />--%>
                                                                                    <asp:TextBox ID="tbTermDT" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                        onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                                        onBlur="parseDate(this, event);"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tblspace_10">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <fieldset>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td class="tdLabel70">
                                                                                                                Lic. No.
                                                                                                            </td>
                                                                                                            <td class="tdLabel120">
                                                                                                                <asp:TextBox ID="tbLicNo" runat="server" CssClass="text100" MaxLength="25"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td class="tdLabel80">
                                                                                                                Lic Type
                                                                                                            </td>
                                                                                                            <td class="tdLabel120">
                                                                                                                <asp:TextBox ID="tbLicType" runat="server" MaxLength="30" CssClass="text100"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td class="tdLabel60">
                                                                                                                Exp.Date
                                                                                                            </td>
                                                                                                            <td class="tdLabel90">
                                                                                                                <asp:TextBox ID="tbLicExpDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event)"
                                                                                                                    MaxLength="10"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td class="tdLabel50">
                                                                                                                City
                                                                                                            </td>
                                                                                                            <td class="tdLabel70">
                                                                                                                <asp:TextBox ID="tbLicCity" runat="server" MaxLength="30" CssClass="text50"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td class="tdLabel50">
                                                                                                                Country
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbLicCountry" runat="server" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                    OnTextChanged="LicCountry_TextChanged" AutoPostBack="true" CssClass="text30"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Button ID="btnLicCountry" OnClientClick="javascript:openWin('RadLicCountryPopup');return false;"
                                                                                                                    CssClass="browse-button" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="3">
                                                                                                                <asp:CustomValidator ID="cvLicCountry" runat="server" ControlToValidate="tbLicCountry"
                                                                                                                    ErrorMessage="Invalid LicCountry" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td class="tdLabel70">
                                                                                                                Add'l Lic.
                                                                                                            </td>
                                                                                                            <td class="tdLabel120">
                                                                                                                <asp:TextBox ID="tbAddLic" runat="server" CssClass="text100" MaxLength="25"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td class="tdLabel80">
                                                                                                                Lic Type
                                                                                                            </td>
                                                                                                            <td class="tdLabel120">
                                                                                                                <asp:TextBox ID="tbAddLicType" MaxLength="30" runat="server" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                    OnTextChanged="LicCountry_TextChanged" AutoPostBack="true" CssClass="text100"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td class="tdLabel60">
                                                                                                                Exp.Date
                                                                                                            </td>
                                                                                                            <td class="tdLabel90">
                                                                                                                <asp:TextBox ID="tbAddLicExpDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event)"
                                                                                                                    MaxLength="10"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td class="tdLabel50">
                                                                                                                City
                                                                                                            </td>
                                                                                                            <td class="tdLabel70">
                                                                                                                <asp:TextBox ID="tbAddLicCity" MaxLength="30" runat="server" CssClass="text50"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td class="tdLabel50">
                                                                                                                Country
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAddLicCountry" MaxLength="3" runat="server" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                                    OnTextChanged="AddLicCountry_TextChanged" AutoPostBack="true" CssClass="text30"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Button ID="btnAddLicCountry" OnClientClick="javascript:openWin('RadAddLicCountryPopup');return false;"
                                                                                                                    CssClass="browse-button" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="3" valign="top">
                                                                                                                <asp:CustomValidator ID="cvAddLicCountry" runat="server" ControlToValidate="tbAddLicCountry"
                                                                                                                    ErrorMessage="Invalid LicCountry" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <fieldset>
                                                                                        <legend>Notes</legend>
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-710x40"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </fieldset>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <%--    <tr>
                                                                <td colspan="5">
                                                                    <fieldset>
                                                                        <legend>Photo</legend>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr valign="top">
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPhoto" runat="server" CssClass="text150"></asp:TextBox>
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <asp:Button ID="btnBrowse" runat="server" CssClass="button" Text="Browse" />
                                                                                </td>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Image ID="imgLogo" runat="server" ImageUrl="~/App_Themes/Default/images/no-image.jpg"
                                                                                                    Width="50px" Height="50px" />
                                                                                            </td>
                                                                                            <td valign="top">
                                                                                                <asp:Button ID="btnLogoDelete" runat="server" CssClass="delete-button" Text="" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>--%>
                                                            </table>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="TypeRatings" runat="server" ValidationGroup="Save">
                                    <table id="tblTypeRating" runat="server" width="100%" class="border-box">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel70">
                                                            Type Code :
                                                        </td>
                                                        <td class="tdLabel65">
                                                            <asp:Label ID="lbTypeCode" runat="server" CssClass="input_no_bg"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel70">
                                                            Description :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbDescription" runat="server" CssClass="input_no_bg"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:CheckBox runat="server" ID="chkTRInactive" Text="Inactive" OnCheckedChanged="chkTRInactive_CheckedChanged"
                                                                AutoPostBack="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td width="18%">
                                                            Qualified In Type As
                                                        </td>
                                                        <td width="82%">
                                                            <table class="border-box" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="tdLabel70">
                                                                                    <asp:CheckBox ID="chkTRPIC91" runat="server" Text="PIC-91" AutoPostBack="true" OnCheckedChanged="TRPIC91_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel70">
                                                                                    <asp:CheckBox ID="chkTRSIC91" runat="server" Text="SIC-91" AutoPostBack="true" OnCheckedChanged="TRSIC91_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                    <asp:CheckBox ID="chkTREngineer" runat="server" Text="Engineer" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <asp:CheckBox ID="chkTRAttendant91" runat="server" Text="Attendant-91" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkTRAirman" runat="server" Text="Check Airman" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="tdLabel70">
                                                                                    <asp:CheckBox ID="chkTRPIC135" runat="server" Text="PIC-135" AutoPostBack="true"
                                                                                        OnCheckedChanged="TRPIC135_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel70">
                                                                                    <asp:CheckBox ID="chkTRSIC135" runat="server" Text="SIC-135" AutoPostBack="true"
                                                                                        OnCheckedChanged="TRSIC135_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                    <asp:CheckBox ID="chkTRInstructor" runat="server" Text="Instructor" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <asp:CheckBox ID="chkTRAttendant135" runat="server" Text="Attendant-135" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkTRAttendant" runat="server" Text="Check Attendant" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdLabel70">
                                                                                    <asp:CheckBox ID="chkPIC121" runat="server" Text="PIC-121" AutoPostBack="true" OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel70">
                                                                                    <asp:CheckBox ID="chkSIC121" runat="server" Text="SIC-121" AutoPostBack="true" OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                    <asp:CheckBox ID="chkPIC125" runat="server" Text="PIC-125" AutoPostBack="true" OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <asp:CheckBox ID="chkSIC125" runat="server" Text="SIC-125" AutoPostBack="true" OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkAttendent121" runat="server" Text="Attendant-121" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkAttendent125" runat="server" Text="Attendant-125" AutoPostBack="true"
                                                                                        OnCheckedChanged="TypeRating_CheckedChanged" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="border-box" width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="4" align="center">
                                                            <asp:Panel ID="pnlBeginning" runat="server" ValidationGroup="Save">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            Beginning Flight Experience: As Of Date:
                                                                        </td>
                                                                        <td>
                                                                            <%--<uc:datepicker id="ucAsOfDt" runat="server"></uc:datepicker>--%>
                                                                            <asp:TextBox ID="tbAsOfDt" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                OnTextChanged="AsOfDt_TextChanged" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                                                MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlCurrent" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            Current Flight Experience:
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Total Hrs</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsInType" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revTotalHrsInType" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsInType" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsDay" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revTotalHrsDay" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsDay" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsNight" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revTotalHrsNight" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsNight" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTotalHrsInstrument" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revTotalHrsInstrument" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbTotalHrsInstrument" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>PIC Hrs</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsInType" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revPICHrsInType" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsInType" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsDay" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revPICHrsDay" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsDay" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsNight" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revPICHrsNight" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsNight" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPICHrsInstrument" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revPICHrsInstrument" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbPICHrsInstrument" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>SIC Hrs</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            In Type
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICInTypeHrs" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revInTypeSICHrs" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbSICInTypeHrs" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Day
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICHrsDay" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revSICHrsDay" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbSICHrsDay" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Night
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICHrsNight" runat="server" SelectionOnFocus="SelectAll" CssClass="RadMaskedTextBox62"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revSICHrsNight" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbSICHrsNight" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Instrument
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbSICHrsInstrument" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revSICHrsInstrument" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbSICHrsInstrument" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td>
                                                            <fieldset>
                                                                <legend>Other Hrs</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            Simulator
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsSimulator" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revOtherHrsSimulator" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsSimulator" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Flt.Engineer
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsFltEngineer" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revOtherHrsFltEngineer" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsFltEngineer" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Flt.Instructor
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsFltInstructor" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revOtherHrsFltInstructor" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsFltInstructor" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Flt.Attendant
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOtherHrsFltAttendant" runat="server" SelectionOnFocus="SelectAll"
                                                                                            MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" AutoPostBack="true"
                                                                                            OnTextChanged="Typerating_TextChanged" CssClass="RadMaskedTextBox62" onBlur="return validateEmptyTextbox(this, event)"
                                                                                            ValidationGroup="Save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revOtherHrsFltAttendant" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbOtherHrsFltAttendant" CssClass="alert-text"
                                                                                            ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <telerik:RadGrid ID="dgTypeRating" runat="server" EnableAJAX="True" AutoGenerateColumns="false"
                                                    OnSelectedIndexChanged="TypeRating_SelectedIndexChanged" OnItemDataBound="TypeRating_ItemDataBound"
                                                    Width="768px" Height="341px">
                                                    <MasterTableView DataKeyNames="CustomerID,CrewID,CrewRatingID,AircraftTypeCD,AircraftTypeID,CrewRatingDescription,IsPilotinCommand,IsSecondInCommand,IsQualInType135PIC
                                        ,IsQualInType135SIC,IsEngineer,IsInstructor,IsCheckAttendant,IsAttendantFAR91,IsAttendantFAR135,IsCheckAirman,IsInActive
                                        ,AsOfDT,TimeInType,Day1,Night1,Instrument,Simulator,FlightEngineer,FlightInstructor,FlightAttendant,ClientCD,ClientID
                                        ,SecondInCommandDay,SecondInCommandNight,SecondInCommandInstr,TotalDayHrs,TotalNightHrs,TotalINSTHrs
                                        ,PilotInCommandTypeHrs,TPilotinCommandDayHrs,TPilotInCommandNightHrs,TPilotInCommandINSTHrs,SecondInCommandTypeHrs
                                        ,SecondInCommandDayHrs,SecondInCommandNightHrs,SecondInCommandINSTHrs,OtherSimHrs,OtherFlightEngHrs,OtherFlightInstrutorHrs,OtherFlightAttdHrs,IsPIC121,IsSIC121,IsPIC125,IsSIC125,IsAttendant121,IsAttendant125"
                                                        CommandItemDisplay="None" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                                        <Columns>
                                                            <telerik:GridTemplateColumn HeaderText="Code" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" UniqueName="AircraftTypeCD" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbAircraftTypeCD" runat="server" Text='<%# Eval("AircraftTypeCD") %>'
                                                                        CssClass="tdtext100"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="CrewRatingDescription"
                                                                AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbCrewRatingDescription" runat="server" Text='<%# Eval("CrewRatingDescription") %>'
                                                                        CssClass="tdtext100"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="PIC" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" AllowFiltering="false" UniqueName="IsPilotinCommand">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkIsPilotinCommand" runat="server" Enabled="false" Checked='<%# Eval("IsPilotinCommand") != null ? Eval("IsPilotinCommand") : false %>' />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="SIC" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" AllowFiltering="false" UniqueName="IsSecondInCommand">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkIsSecondInCommand" runat="server" Enabled="false" Checked='<%# Eval("IsSecondInCommand") != null ? Eval("IsSecondInCommand") : false %>' />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Updt. As Of" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalUpdateAsOfDT" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbTotalUpdateAsOfDT" runat="server" Text='<%# Eval("TotalUpdateAsOfDT") %>'
                                                                        CssClass="tdtext100"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot. Time In" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalTimeInTypeHrs" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbTotalTimeInTypeHrs" runat="server" Text='<%# Eval("TotalTimeInTypeHrs") %>'
                                                                        CssClass="tdtext60"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot. Day" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalDayHrs" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbTotalDayHrs" runat="server" Text='<%# Eval("TotalDayHrs") %>' CssClass="tdtext60"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot. Night" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalNightHrs" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbTotalNightHrs" runat="server" Text='<%# Eval("TotalNightHrs") %>'
                                                                        CssClass="tdtext60"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot. Instr" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalINSTHrs" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbTotalINSTHrs" runat="server" Text='<%# Eval("TotalINSTHrs") %>'
                                                                        CssClass="tdtext60"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" CurrentFilterFunction="Contains"
                                                                Display="false" FilterDelay="4000" ShowFilterIcon="false" UniqueName="HiddenCheckboxValues">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkTRPIC135" runat="server" Checked='<%# Eval("IsQualInType135PIC") != null ? Eval("IsQualInType135PIC") : false %>' />
                                                                    <asp:CheckBox ID="chkTRSIC135" runat="server" Checked='<%# Eval("IsQualInType135SIC") != null ? Eval("IsQualInType135SIC") : false %>' />
                                                                    <asp:CheckBox ID="chkTREngineer" runat="server" Checked='<%# Eval("IsEngineer") != null ? Eval("IsEngineer") : false %>' />
                                                                    <asp:CheckBox ID="chkTRInstructor" runat="server" Checked='<%# Eval("IsInstructor") != null ? Eval("IsInstructor") : false %>' />
                                                                    <asp:CheckBox ID="chkTRAttendant" runat="server" Checked='<%# Eval("IsCheckAttendant") != null ? Eval("IsCheckAttendant") : false %>' />
                                                                    <asp:CheckBox ID="chkTRAttendant91" runat="server" Checked='<%# Eval("IsAttendantFAR91") != null ? Eval("IsAttendantFAR91") : false %>' />
                                                                    <asp:CheckBox ID="chkTRAttendant135" runat="server" Checked='<%# Eval("IsAttendantFAR135") != null ? Eval("IsAttendantFAR135") : false %>' />
                                                                    <asp:CheckBox ID="chkTRAirman" runat="server" Checked='<%# Eval("IsCheckAirman") != null ? Eval("IsCheckAirman") : false %>' />
                                                                    <asp:CheckBox ID="chkTRInactive" runat="server" Checked='<%# Eval("IsInActive") != null ? Eval("IsInActive") : false %>' />
                                                                    <asp:CheckBox ID="chkPIC121" runat="server" Checked='<%# Eval("IsPIC121") != null ? Eval("IsPIC121") : false %>' />
                                                                    <asp:CheckBox ID="chkSIC121" runat="server" Checked='<%# Eval("IsSIC121") != null ? Eval("IsSIC121") : false %>' />
                                                                    <asp:CheckBox ID="chkPIC125" runat="server" Checked='<%# Eval("IsPIC125") != null ? Eval("IsPIC125") : false %>' />
                                                                    <asp:CheckBox ID="chkSIC125" runat="server" Checked='<%# Eval("IsSIC125") != null ? Eval("IsSIC125") : false %>' />
                                                                    <asp:CheckBox ID="chkAttendent121" runat="server" Checked='<%# Eval("IsAttendant121") != null ? Eval("IsAttendant121") : false %>' />
                                                                    <asp:CheckBox ID="chkAttendent125" runat="server" Checked='<%# Eval("IsAttendant125") != null ? Eval("IsAttendant125") : false %>' />
                                                                    <asp:Label ID="lblTotalTimeInTypeHrs" runat="server" Text='<%# Eval("TotalTimeInTypeHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lblTotalDayHrs" runat="server" Text='<%# Eval("TotalDayHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lblTotalNightHrs" runat="server" Text='<%# Eval("TotalNightHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lblTotalINSTHrs" runat="server" Text='<%# Eval("TotalINSTHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbPICHrsInType" runat="server" Text='<%# Eval("PilotInCommandTypeHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbPICHrsDay" runat="server" Text='<%# Eval("TPilotinCommandDayHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbPICHrsNight" runat="server" Text='<%# Eval("TPilotInCommandNightHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbPICHrsInstrument" runat="server" Text='<%# Eval("TPilotInCommandINSTHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbSICInTypeHrs" runat="server" Text='<%# Eval("SecondInCommandTypeHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbSICHrsDay" runat="server" Text='<%# Eval("SecondInCommandDayHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbSICHrsNight" runat="server" Text='<%# Eval("SecondInCommandNightHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbSICHrsInstrument" runat="server" Text='<%# Eval("SecondInCommandINSTHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbOtherHrsSimulator" runat="server" Text='<%# Eval("OtherSimHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbOtherHrsFltEngineer" runat="server" Text='<%# Eval("OtherFlightEngHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbOtherHrsFltInstructor" runat="server" Text='<%# Eval("OtherFlightInstrutorHrs") %>'></asp:Label>
                                                                    <asp:Label ID="lbOtherHrsFltAttendant" runat="server" Text='<%# Eval("OtherFlightAttdHrs") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                    <GroupingSettings CaseSensitive="false" />
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td align="right">
                                                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnFlightExp" Text="Modify Beginning Flt. Experience" runat="server"
                                                                CssClass="ui_nav" OnClick="FlightExp_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnUpdateFltExperience" Text="Update Flt.Experience" runat="server"
                                                                OnClick="UpdateFltExperience_Click" CssClass="ui_nav" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnUpdateAllFltExperience" Text="Update All Flt.Experience" runat="server"
                                                                CssClass="ui_nav" OnClick="UpdateAllFltExperience_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddRating" Text="Add Rating" OnClientClick="javascript:return ShowAircraftTypePopup();"
                                                                runat="server" CausesValidation="false" CssClass="ui_nav" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnDeleteRating" Text="Delete Rating" runat="server" CssClass="ui_nav"
                                                                CausesValidation="false" OnClick="DeleteRating_Click" OnClientClick="if(!confirm('Are you sure you want to delete this CREW RATING record?')) return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="CheckList" runat="server">
                                    <table id="tblCrewCheckList" runat="server" class="border-box" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel60">
                                                            Code :
                                                        </td>
                                                        <td class="tdLabel70">
                                                            <asp:Label ID="lbCheckListCode" runat="server" CssClass="text60"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel700">
                                                            Description :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbCheckListDescription" runat="server" CssClass="text60"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:CheckBox ID="chkDisplayInactiveChecklist" runat="server" Text="Active Only Checklist" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" class="border-box">
                                                    <tr>
                                                        <td valign="top">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    Original Date
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbOriginalDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                        OnTextChanged="OriginalDate_TextChanged" AutoPostBack="true" onclick="showPopup(this, event);"
                                                                                        onfocus="showPopup(this, event);" onblur="parseDate(this, event)" MaxLength="10"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    <asp:Button ID="btnChgBaseDate" runat="server" Text="Chg.Base Date" CssClass="button_small_none"
                                                                                        OnClick="ChgBaseDate_Click" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbChgBaseDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                        OnTextChanged="ChgBaseDate_TextChanged" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                                                        AutoPostBack="true" onblur="parseDate(this, event)" MaxLength="10"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    Frequency
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RadioButtonList ID="radFreq" runat="server" OnSelectedIndexChanged="radFreq_SelectedIndexChanged"
                                                                                        RepeatDirection="Horizontal" AutoPostBack="true">
                                                                                        <asp:ListItem Selected="True" Value="1" Text="Months"></asp:ListItem>
                                                                                        <asp:ListItem Value="2" Text="Days"></asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbFreq" runat="server" CssClass="text70" MaxLength="3" AutoPostBack="true"
                                                                                        onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="Freq_TextChanged"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    Previous
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPrevious" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                        AutoPostBack="true" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                                                        onblur="parseDate(this, event)" OnTextChanged="Previous_TextChanged" MaxLength="10"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    Due Next
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbDueNext" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                                        OnTextChanged="DueNext_TextChanged" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                                                        onblur="parseDate(this, event)" MaxLength="10"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        Alert Date
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAlertDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                            OnTextChanged="AlertDate_TextChanged" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                                            onblur="parseDate(this, event)" MaxLength="10"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Alert Days
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAlertDays" runat="server" CssClass="text70" AutoPostBack="true"
                                                                            onKeyPress="return fnAllowNumeric(this, event)" MaxLength="2" OnTextChanged="AlertDays_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Grace Date
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbGraceDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'-/.')"
                                                                            OnTextChanged="GraceDate_TextChanged" onclick="showPopup(this, event);" onfocus="showPopup(this, event);"
                                                                            onblur="parseDate(this, event)" MaxLength="10"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Grace Days
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbGraceDays" runat="server" CssClass="text70" AutoPostBack="true"
                                                                            onKeyPress="return fnAllowNumeric(this, event)" MaxLength="2" OnTextChanged="GraceDays_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Aircraft Type
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAircraftType" runat="server" CssClass="text70" MaxLength="3" OnTextChanged="AircraftType_TextChanged">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnAircraftType" OnClientClick="javascript:openWin('RadAicraftType');return false;"
                                                                                        CssClass="browse-button" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:CustomValidator ID="cvAssociateTypeCode" runat="server" ControlToValidate="tbAircraftType"
                                                                                        ErrorMessage="Invalid Aircraft Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                        SetFocusOnError="true" ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Total Req Flight Hrs
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbTotalReqFlightHrs" runat="server" MaxLength="6" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"
                                                                                        OnTextChanged="TotalReqFlightHrs_TextChanged" CssClass="text70"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ID="revTotalReqFlightHrs" runat="server" Display="Dynamic"
                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbTotalReqFlightHrs" CssClass="alert-text"
                                                                                        ValidationExpression="^[0-9]{0,5}(\.[0-9]{1})?$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkPIC91" runat="server" Text="PIC-91" AutoPostBack="true" OnCheckedChanged="ChecklistchkPIC91_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkPIC135" runat="server" Text="PIC-135" AutoPostBack="true" OnCheckedChanged="ChecklistchkPIC135_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSIC91" runat="server" Text="SIC-91" AutoPostBack="true" OnCheckedChanged="ChecklistchkSIC91_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSIC135" runat="server" Text="SIC-135" AutoPostBack="true" OnCheckedChanged="ChecklistchkSIC135_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" AutoPostBack="true"
                                                                            OnCheckedChanged="ChecklistchkInactive_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSetToEndOfMonth" runat="server" Text="Set To End Of Month" AutoPostBack="true"
                                                                            OnCheckedChanged="SetToEndOfMonth_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSetToNextOfMonth" runat="server" Text="Set To 1st Of Next Month"
                                                                            AutoPostBack="true" OnCheckedChanged="SetToNextOfMonth_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSetToEndOfCalenderYear" runat="server" Text="Set To End Of Calendar Year"
                                                                            AutoPostBack="true" OnCheckedChanged="SetToEndOfCalenderYear_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="ckhDisableDateCalculation" runat="server" Text="Disable Date Calculation"
                                                                            AutoPostBack="true" OnCheckedChanged="ckhDisableDateCalculation_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel120">
                                                                                    <asp:CheckBox ID="chkOneTimeEvent" runat="server" Text="One Time Event" OnCheckedChanged="OneTimeEvent_CheckChanged"
                                                                                        AutoPostBack="true" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkCompleted" runat="server" Enabled="false" Text="Completed" OnCheckedChanged="ChecklistchkCompleted_CheckChanged"
                                                                                        AutoPostBack="true" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkNonConflictedEvent" runat="server" Text="Non-Conflicted Event"
                                                                            AutoPostBack="true" OnCheckedChanged="ChecklistchkNonConflictedEvent_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkRemoveFromCrewRosterRept" runat="server" Text="Remove From Crew Roster Rept"
                                                                            AutoPostBack="true" OnCheckedChanged="ChecklistchkRemoveFromCrewRosterRept_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkRemoveFromChecklistRept" runat="server" Text="Remove From Checklist Rept"
                                                                            AutoPostBack="true" OnCheckedChanged="ChecklistchkRemoveFromChecklistRept_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkDoNotPrintPassDueAlert" runat="server" Text="Do Not Print Pass Due Alert"
                                                                            AutoPostBack="true" OnCheckedChanged="ChecklistchkDoNotPrintPassDueAlert_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkPrintOneTimeEventCompleted" runat="server" Enabled="false" Text="Print One Time Event/Completed"
                                                                            AutoPostBack="true" OnCheckedChanged="ChecklistchkPrintOneTimeEventCompleted_CheckedChanged" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left">
                                                <telerik:RadGrid ID="dgCrewCheckList" runat="server" EnableAJAX="True" AllowFilteringByColumn="false"
                                                    AutoGenerateColumns="false" OnSelectedIndexChanged="CrewCheckList_SelectedIndexChanged"
                                                    OnItemDataBound="CrewCheckList_ItemDataBound" Width="768px" Height="341px">
                                                    <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left"
                                                        DataKeyNames="CustomerID,CrewID,CheckListID,CheckListCD,OriginalDT,PreviousCheckDT,DueDT,AlertDT,GraceDT,BaseMonthDT,IsScheduleCheck,IsEndCalendarYear,IsNextMonth,Frequency,IsPassedDueAlert,IsPrintStatus,TotalREQFlightHrs,IsCompleted,IsSecondInCommandFAR135,IsSecondInCommandFAR91,IsPilotInCommandFAR91,Specific,IsInActive,IsNoChecklistREPT,IsNoCrewCheckListREPTt,IsNoConflictEvent,IsOneTimeEvent,IsStopCALC,IsMonthEnd">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="CheckListCD" HeaderText="Code" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" AllowFiltering="false">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="CrewChecklistDescription">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbCrewChecklistDescription" runat="server" CssClass="text60" Text='<%# Eval("CrewChecklistDescription") %>'
                                                                        MaxLength="40"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="PreviousCheckDT" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="PreviousCheckDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbPreviousCheckDT" runat="server" CssClass="text60" MaxLength="40"
                                                                        Text='<%# Eval("PreviousCheckDT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Due Next" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="DueDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDueDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("DueDT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" UniqueName="AlertDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbAlertDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("AlertDT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" UniqueName="GraceDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbGraceDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("GraceDT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" UniqueName="AlertDays">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbAlertDays" runat="server" CssClass="text60" Text='<%# Eval("AlertDays") %>'
                                                                        MaxLength="40"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" UniqueName="GraceDays">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbGraceDays" runat="server" CssClass="text60" Text='<%# Eval("GraceDays") %>'
                                                                        MaxLength="40"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Freq" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                ShowFilterIcon="false" UniqueName="FrequencyMonth">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbFrequencyMonth" runat="server" CssClass="text60" Text='<%# Eval("FrequencyMonth") %>'
                                                                        MaxLength="40"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Base Date" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="BaseMonthDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbBaseMonthDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("BaseMonthDT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Original Date" CurrentFilterFunction="Contains"
                                                                Display="false" FilterDelay="4000" ShowFilterIcon="false" UniqueName="OriginalDT">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbOriginalDT" runat="server" CssClass="text60" MaxLength="40" Text='<%# Eval("OriginalDT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Aircraft Type" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="AircraftTypeCD">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbAircraftTypeCD" runat="server" CssClass="text60" Text='<%# Eval("AircraftTypeCD") %>'
                                                                        MaxLength="40"></asp:Label>
                                                                    <asp:HiddenField ID="hdnAircraftTypeCD" runat="server" Value='<%# Eval("AircraftID") %>' />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" CurrentFilterFunction="Contains"
                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalREQFlightHrs">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbTotalREQFlightHrs" runat="server" CssClass="text60" Text='<%# Eval("TotalREQFlightHrs") %>'
                                                                        MaxLength="40"></asp:Label>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Tot Req Hrs" Display="false" ShowFilterIcon="false"
                                                                UniqueName="HiddenCheckboxValues">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="RadFreq" runat="server" Text='<%# Eval("Frequency") %>' />
                                                                    <asp:CheckBox ID="chkPassedDueAlert" runat="server" Checked='<%# Eval("IsPassedDueAlert") != null ? Eval("IsPassedDueAlert") : false %>' />
                                                                    <asp:CheckBox ID="chkPilotInCommandFAR91" runat="server" Checked='<%# Eval("IsPilotInCommandFAR91") != null ? Eval("IsPilotInCommandFAR91") : false %>' />
                                                                    <asp:CheckBox ID="chkPilotInCommandFAR135" runat="server" Checked='<%# Eval("IsPilotInCommandFAR135") != null ? Eval("IsPilotInCommandFAR135") : false %>' />
                                                                    <asp:CheckBox ID="chkSecondInCommandFAR91" runat="server" Checked='<%# Eval("IsSecondInCommandFAR91") != null ? Eval("IsSecondInCommandFAR91") : false %>' />
                                                                    <asp:CheckBox ID="chkSecondInCommandFAR135" runat="server" Checked='<%# Eval("IsSecondInCommandFAR135") != null ? Eval("IsSecondInCommandFAR135") : false %>' />
                                                                    <asp:CheckBox ID="chkInActive" runat="server" Checked='<%# Eval("IsInActive") != null ? Eval("IsInActive") : false %>' />
                                                                    <asp:CheckBox ID="chkMonthEnd" runat="server" Checked='<%# Eval("IsMonthEnd") != null ? Eval("IsMonthEnd") : false %>' />
                                                                    <asp:CheckBox ID="chkStopCALC" runat="server" Checked='<%# Eval("IsStopCALC") != null ? Eval("IsStopCALC") : false %>' />
                                                                    <asp:CheckBox ID="chkOneTimeEvent" runat="server" Checked='<%# Eval("IsOneTimeEvent") != null ? Eval("IsOneTimeEvent") : false %>' />
                                                                    <asp:CheckBox ID="chkCompleted" runat="server" Checked='<%# Eval("IsCompleted") != null ? Eval("IsCompleted") : false %>' />
                                                                    <asp:CheckBox ID="chkNoConflictEvent" runat="server" Checked='<%# Eval("IsNoConflictEvent") != null ? Eval("IsNoConflictEvent") : false %>' />
                                                                    <asp:CheckBox ID="chkNoCrewCheckListREPTt" runat="server" Checked='<%# Eval("IsNoCrewCheckListREPTt") != null ? Eval("IsNoCrewCheckListREPTt") : false %>' />
                                                                    <asp:CheckBox ID="chkNoChecklistREPT" runat="server" Checked='<%# Eval("IsNoChecklistREPT") != null ? Eval("IsNoChecklistREPT") : false %>' />
                                                                    <asp:CheckBox ID="chkPrintStatus" runat="server" Checked='<%# Eval("IsPrintStatus") != null ? Eval("IsPrintStatus") : false %>' />
                                                                    <asp:CheckBox ID="chkNextMonth" runat="server" Checked='<%# Eval("IsNextMonth") != null ? Eval("IsNextMonth") : false %>' />
                                                                    <asp:CheckBox ID="chkEndCalendarYear" runat="server" Checked='<%# Eval("IsEndCalendarYear") != null ? Eval("IsEndCalendarYear") : false %>' />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                    <GroupingSettings CaseSensitive="false" />
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td align="right">
                                                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnApplyToAdditionalCrew" runat="server" Text="Apply To Additional Crew"
                                                                OnClientClick="javascript:return ShowChecklistSettingsPopup();" CssClass="button"
                                                                Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddChecklist" runat="server" Text="Add Checklist" OnClientClick="javascript:return ShowCrewCheckListPopup();"
                                                                CausesValidation="false" CssClass="button" Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnDeleteChecklist" runat="server" Text="Delete Checklist" CssClass="button"
                                                                Enabled="false" CausesValidation="false" OnClick="DeleteChecklist_Click" OnClientClick="if(!confirm('Are you sure you want to delete this CHECKLIST record?')) return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="AircraftAssigned" runat="server">
                                    <table width="100%" class="border-box">
                                        <tr>
                                            <td>
                                                Crew Aircraft Assign Information
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="dgAircraftAssigned" runat="server" AllowSorting="true" AllowMultiRowSelection="true"
                                                                OnPageIndexChanged="AircraftAssigned_PageIndexChanged" Visible="true" AutoGenerateColumns="false"
                                                                PageSize="10" AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
                                                                Width="750px" Height="341px" OnNeedDataSource="AircraftAssigned_BindData">
                                                                <MasterTableView DataKeyNames="CrewAircraftAssignedID,FleetID,TailNum,AircraftCD,TypeDescription"
                                                                    CommandItemDisplay="None">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="TailNum">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Assigned" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" AllowFiltering="false" UniqueName="AircraftAssigned">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkAircraftAssigned" runat="server" Enabled="false"/>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Type Code" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="TypeDescription" HeaderText="Type Description"
                                                                            CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="Passports" runat="server">
                                    <table width="100%" class="border-box" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                Visa Information:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="dgVisa" runat="server" OnNeedDataSource="Visa_BindData" EnableAJAX="True"
                                                                OnItemDataBound="Visa_ItemDataBound" Height="341px">
                                                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                    DataKeyNames="VisaID,CrewID,PassengerRequestorID,CustomerID,VisaTYPE,VisaNum,LastUpdUID,LastUpdTS,CountryID,ExpiryDT,Notes,IssuePlace,IssueDate,IsDeleted,CountryCD,CountryName"
                                                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn HeaderText="Country" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="CountryCD">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbCountryCD" runat="server" CssClass="text60" AutoPostBack="true"
                                                                                    onDblClick="javascript:openWinGrd('RadVisaCountryMasterPopup',this);return false;"
                                                                                    Text='<%# Eval("CountryCD") %>' OnTextChanged="CountryCD_TextChanged" MaxLength="40"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Visa No." CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="VisaNum">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbVisaNum" runat="server" CssClass="text60" Text='<%# Eval("VisaNum") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="ExpiryDT">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbExpiryDT" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event)"
                                                                                    MaxLength="10" DataFormatString="{0:MM/dd/yyyy}">
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="IssuePlace">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssuePlace" runat="server" CssClass="text60" Text='<%# Eval("IssuePlace") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="IssueDate" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="IssueDate">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssueDate" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event)"
                                                                                    MaxLength="10" DataFormatString="{0:MM/dd/yyyy}">
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Notes" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                            ShowFilterIcon="false" UniqueName="Notes">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbNotes" runat="server" CssClass="text60" Text='<%# Eval("Notes") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnAddVisa" runat="server" Text="Add Visa" OnClick="AddVisa_Click"
                                                                CssClass="button" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnDeleteVisa" runat="server" Text="Delete Visa" OnClick="DeleteVisa_Click"
                                                                CssClass="button" OnClientClick="if(!confirm('Are you sure you want to delete this VISA record?')) return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Crew Passport Information:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadGrid ID="dgPassport" runat="server" OnNeedDataSource="Passport_BindData"
                                                                OnItemDataBound="Passport_ItemDataBound" EnableAJAX="True" AutoGenerateColumns="false"
                                                                AllowFilteringByColumn="false" Height="341px">
                                                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                    DataKeyNames="PassportID,CrewID,PassengerRequestorID,CustomerID,Choice,IssueCity,PassportNum,PassportExpiryDT,CountryID,IsDefaultPassport,
                                                                    PilotLicenseNum,LastUpdUID,LastUpdTS,IssueDT,IsDeleted,CountryCD,CountryName"
                                                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn HeaderText="Passport No." CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="PassportNum">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbPassportNum" runat="server" CssClass="text60" Text='<%# Eval("PassportNum") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Choice" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="Choice">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkChoice" OnCheckedChanged="chkChoice_CheckedChanged" runat="server" />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Expiration Date" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="PassportExpiryDT">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbPassportExpiryDT" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                    onDblClick="javascript:openWinGrd('RadVisaCountryMasterPopup',this);return false;"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event)"
                                                                                    MaxLength="10">
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Issuing City/Authority" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="IssueCity">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssueCity" runat="server" CssClass="text60" Text='<%# Eval("IssueCity") %>'
                                                                                    MaxLength="40"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="IssueDate" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="IssueDT">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbIssueDT" runat="server" CssClass="text60" onKeyPress="return fnAllowNumericAndChar(this, event,'-/')"
                                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event)"
                                                                                    MaxLength="10">
                                                                                </asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Nation" CurrentFilterFunction="Contains"
                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="PassportCountry">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbPassportCountry" runat="server" CssClass="text60" Text='<%# Eval("CountryCD") %>'
                                                                                    AutoPostBack="true" OnTextChanged="Nation_TextChanged" MaxLength="4" onDblClick="javascript:openWinGrd('RadVisaCountryMasterPopup',this);return false;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnPassportCountry" runat="server" Value='<%# Eval("CountryID") %>' />
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnAddPassport" runat="server" Text="Add Passport" OnClick="AddPassport_Click"
                                                                CssClass="button" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnDeletePassport" runat="server" Text="Delete Passport" OnClick="DeletePassport_Click"
                                                                CssClass="button" OnClientClick="if(!confirm('Are you sure you want to delete this PASSPORT record?')) return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="EmailPref" runat="server">
                                    <table width="100%" class="border-box" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" colspan="2">
                                                <fieldset>
                                                    <legend>E-MAIL PREFERENCE</legend>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="SelectAll"
                                                                                OnCheckedChanged="SelectAll_CheckedChanged" />
                                                                        </td>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>Home</legend>
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel50">
                                                                                                        Date
                                                                                                    </td>
                                                                                                    <td class="tdLabel60">
                                                                                                        <asp:CheckBox ID="chkHomeArrival" runat="server" Text="Arrival" />
                                                                                                    </td>
                                                                                                    <td class="tdLabel80">
                                                                                                        <asp:CheckBox ID="chkHomeDeparture" runat="server" Text="Departure" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel50">
                                                                                                        Time
                                                                                                    </td>
                                                                                                    <td class="tdLabel60">
                                                                                                        <asp:RadioButton ID="radZULU" runat="server" Text="UTC" GroupName="HomeDT" onchange="javascript:return ZuluChange();" />
                                                                                                    </td>
                                                                                                    <td class="tdLabel80">
                                                                                                        <asp:RadioButton ID="radLocal" runat="server" Text="Local" GroupName="HomeDT" onchange="javascript:return ZuluChange();" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_5">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>General</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td class="tdLabel180">
                                                                                <asp:CheckBox ID="chkDepartmentAuthorization" runat="server" Text="Department/Authorization" />
                                                                            </td>
                                                                            <td class="tdLabel150">
                                                                                <asp:CheckBox ID="chkRequestorPhone" runat="server" Text="Requestor/Phone" />
                                                                            </td>
                                                                            <td class="tdLabel120">
                                                                                <asp:CheckBox ID="chkAccountNo" runat="server" Text="Account No" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkCancellationDesc" runat="server" Text="Cancellation Description" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdLabel180">
                                                                                <asp:CheckBox ID="chkStatus" runat="server" Text="Status" />
                                                                            </td>
                                                                            <td class="tdLabel150">
                                                                                <asp:CheckBox ID="chkAirport" runat="server" Text="Airport" />
                                                                            </td>
                                                                            <td class="tdLabel120">
                                                                                <asp:CheckBox ID="chkChecklist" runat="server" Text="Checklist" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkRunway" runat="server" Text="Runway" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_5">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Crew Duty Details</legend>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="tdLabel180">
                                                                                            <asp:CheckBox ID="chkEndDuty" runat="server" Text="End Duty" />
                                                                                        </td>
                                                                                        <td class="tdLabel150">
                                                                                            <asp:CheckBox ID="chkOverride" runat="server" Text="Override" />
                                                                                        </td>
                                                                                        <td class="tdLabel120">
                                                                                            <asp:CheckBox ID="chkCrewRules" runat="server" Text="Crew Rules" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkFAR" runat="server" Text="FAR" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="tdLabel180">
                                                                                            <asp:CheckBox ID="chkDutyHours" runat="server" Text="Duty Hours" />
                                                                                        </td>
                                                                                        <td class="tdLabel150">
                                                                                            <asp:CheckBox ID="chkFlightHours" runat="server" Text="Flight Hours" />
                                                                                        </td>
                                                                                        <td class="tdLabel120">
                                                                                            <asp:CheckBox ID="chkRestHours" runat="server" Text="Rest Hours" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkAssociatedCrew" runat="server" Text="Associated Crew" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_5">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Logistics Details</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <fieldset>
                                                                                    <legend>Crew</legend>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <fieldset>
                                                                                                    <legend>Transport</legend>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:CheckBox ID="chkCrewTransport" runat="server" Text="Departure" />
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CheckBox ID="chkCrewArrival" runat="server" Text="Arrival" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </fieldset>
                                                                                            </td>
                                                                                            <td valign="top" align="right">
                                                                                                <asp:CheckBox ID="chkHotel" runat="server" Text="Hotel" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Passenger</legend>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <fieldset>
                                                                                                    <legend>Transport</legend>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:CheckBox ID="chkPassengerTransport" runat="server" Text="Departure" />
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CheckBox ID="chkPassengerArrival" runat="server" Text="Arrival" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </fieldset>
                                                                                            </td>
                                                                                            <td valign="top" align="right">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkPassengerHotel" runat="server" Text="Hotel" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkPassengerDetails" runat="server" Text="PAX Details" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkPassengerPaxPhone" runat="server" Text="PAX Phone" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>FBO Handler</legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="tdLabel180">
                                                                                                <asp:CheckBox ID="chkDepartureInformation" runat="server" Text="Departure Information" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:CheckBox ID="chkArrivalInformation" runat="server" Text="Arrival Information" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Catering</legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="tdLabel180">
                                                                                                <asp:CheckBox ID="chkDepartureCatering" runat="server" Text="Departure Catering" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:CheckBox ID="chkArrivalCatering" runat="server" Text="Arrival Catering" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_5">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Outbound Instructions</legend>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkNEWSPAPER" runat="server" Text="NEWSPAPER" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkCOFFEE" runat="server" Text="COFFEE" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkJUICE" runat="server" Text="JUICE" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_5">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <fieldset>
                                                    <legend>INVOKE E-MAIL</legend>
                                                    <table width="100%" height="360px">
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>General</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkArrivalDepartureTime" runat="server" Text="Arrival/Departure Time" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkArrivalDepartureICAO" runat="server" Text="Arrival/Departure ICAO" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkAircraft" runat="server" Text="Aircraft" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkGeneralCrew" runat="server" Text="Crew" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkGeneralPassenger" runat="server" Text="PAX" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkOutBoundInstructions" runat="server" Text="OutBound Instructions" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkGeneralChecklist" runat="server" Text="Checklist" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Logistics</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkLogisticsFBO" runat="server" Text="FBO" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkLogisticsHotel" runat="server" Text="Hotel" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkLogisticsTransportation" runat="server" Text="Transportation" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkLogisticsCatering" runat="server" Text="Catering" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <legend>ADDITIONAL INFO (Maximum 3)</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewHotelConfirm" runat="server" Text="Crew Hotel Confirm" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkDeptAirportNotes" runat="server" Text="Departure Airport Notes"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewHotelComments" runat="server" Text="Crew Hotel Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkArrAirportNotes" runat="server" Text="Arrival Airport Notes"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewDeptTransConfirm" runat="server" Text="Crew Departure Transport Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkDeptAirportAlerts" runat="server" Text="Departure Airport Alerts"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewDeptTransComments" runat="server" Text="Crew Departure Transport Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkArrAirportAlerts" runat="server" Text="Arrival Airport Alerts"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewArrTransConfirm" runat="server" Text="Crew Arrival Transport Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkArrFBOConfirm" runat="server" Text="Arrival FBO Confirm" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewArrTransComments" runat="server" Text="Crew Arrival Transport Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkArrFBOComments" runat="server" Text="Arrival FBO Comments" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCrewNotes" runat="server" Text="Crew Notes" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkDeptFBOConfirm" runat="server" Text="Departure FBO Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXHotelConfirm" runat="server" Text="PAX Hotel Confirmation"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkDeptFBOComments" runat="server" Text="Departure FBO Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXHotelComments" runat="server" Text="PAX Hotel Comments" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkDeptCaterConfirm" runat="server" Text="Departure Catering Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXDeptTransConfirm" runat="server" Text="PAX Departure Transport Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkDeptCaterComments" runat="server" Text="Departure Catering Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXDeptTransComments" runat="server" Text="PAX Departure Transport Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkArrCaterConfirm" runat="server" Text="Arrival Catering Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXArrTransConfirm" runat="server" Text="PAX Arrival Transport Confirm"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkArrCaterComments" runat="server" Text="Arrival Catering Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXArrTransComments" runat="server" Text="PAX Arrival Transport Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkTripAlerts" runat="server" Text="Trip Alerts" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkPAXNotes" runat="server" Text="PAX Notes" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkTripNotes" runat="server" Text="Trip Notes" onclick="chkcontrol(this);" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkOutboundInstComments" runat="server" Text="OutBound Instruction Comments"
                                                                                onclick="chkcontrol(this);" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </telerik:RadPageView>
                            </telerik:RadMultiPage>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <telerik:RadPanelBar ID="pnlAdditionalInfo" Width="100%" ExpandAnimation-Type="None"
                                CollapseAnimation-Type="None" runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="false" Text="Additional Information">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <table width="100%" class="box1">
                                                        <tr>
                                                            <td>
                                                                <telerik:RadGrid ID="dgCrewAddlInfo" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                                                    OnItemDataBound="CrewAddlInfo_ItemDataBound">
                                                                    <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                                                        DataKeyNames="CrewInfoXRefID,CrewID,CrewInfoID,InformationDESC,CrewInfoCD,InformationValue,LastUpdUID,LastUpdTS,IsReptFilter"
                                                                        CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false">
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn DataField="CrewInfoCD" UniqueName="CrewInfoCD" HeaderText="Code"
                                                                                CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" AllowFiltering="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="InformationDESC" UniqueName="CrewInformationDescription"
                                                                                HeaderText="Crew Name" CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false"
                                                                                AllowFiltering="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Additional Information" CurrentFilterFunction="Contains"
                                                                                FilterDelay="4000" ShowFilterIcon="false" UniqueName="AddlInfo" AllowFiltering="false">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="tbAddlInfo" runat="server" CssClass="text100" Text='<%# Eval("InformationValue") %>'
                                                                                        MaxLength="40"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Choice" CurrentFilterFunction="Contains"
                                                                                DataField="IsReptFilter" FilterDelay="4000" ShowFilterIcon="false" UniqueName="IsReptFilter"
                                                                                AllowFiltering="false">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkIsReptFilter" Checked='<%# Eval("IsReptFilter") %>' runat="server" />
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                    </MasterTableView>
                                                                    <ClientSettings>
                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                        <Selecting AllowRowSelect="true" />
                                                                    </ClientSettings>
                                                                    <GroupingSettings CaseSensitive="false" />
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="btnAddlInfo" OnClientClick="javascript:return ShowAddlInfoPopup();"
                                                                                runat="server" CssClass="ui_nav" Text="Add Info." CausesValidation="false" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btnDeleteInfo" runat="server" CssClass="ui_nav" Text="Delete Info."
                                                                                CausesValidation="false" OnClick="DeleteInfo_Click" OnClientClick="if(!confirm('Are you sure you want to delete this ADDITIONAL INFO record?')) return false;" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Photo" CssClass="PanelHeaderStyle">
                                        <ContentTemplate>
                                            <table cellspacing="0" cellpadding="0" width="100%" class="box1">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td style="vertical-align: top">
                                                                                <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text60" OnBlur="CheckName();"
                                                                                    ClientIDMode="AutoID"></asp:TextBox>
                                                                            </td>
                                                                            <td style="vertical-align: top">
                                                                                <asp:FileUpload ID="fileUL" runat="server" Enabled="false" OnBlur="CheckName();"
                                                                                    ClientIDMode="AutoID" onchange="javascript:return File_onchange();" />
                                                                                <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
                                                                                <asp:HiddenField ID="hdnUrl" runat="server" />
                                                                            </td>
                                                                            <td style="vertical-align: top">
                                                                                <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                                                    OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text60"
                                                                                    ClientIDMode="AutoID">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="vertical-align: top">
                                                                                <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                                                                    ClientIDMode="AutoID" />
                                                                            </td>
                                                                            <td style="vertical-align: top">
                                                                                <asp:Button ID="btndeleteImage" runat="server" Text="Delete" CssClass="button" Enabled="false"
                                                                                    OnClick="DeleteImage_Click" ClientIDMode="AutoID" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <telerik:RadPanelBar ID="pnlCurrency" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="false" Text="Currency">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <table cellpadding="0" cellspacing="0" class="box1" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table align="left">
                                                                    <tr>
                                                                        <td>
                                                                            Today
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lbToday" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td align="right" width="70%">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnRefreshView" CssClass="ui_nav" runat="server" Text="Refresh View"
                                                                                            OnClick="RefreshView_Click" />
                                                                                    </td>
                                                                                    <td>
                                                                                        Switch Views
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td align="right" width="30%">
                                                                            <telerik:RadTabStrip ID="tabSwitchViews" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                                                                                CausesValidation="false" MultiPageID="RadMultiPage2" SelectedIndex="1" Align="Justify"
                                                                                Width="200px" Style="float: inherit">
                                                                                <Tabs>
                                                                                    <telerik:RadTab Text="Currency">
                                                                                    </telerik:RadTab>
                                                                                    <telerik:RadTab Text="Checklist">
                                                                                    </telerik:RadTab>
                                                                                </Tabs>
                                                                            </telerik:RadTabStrip>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <telerik:RadMultiPage ID="RadMultiPage2" runat="server" SelectedIndex="0" Width="100%">
                                                                    <telerik:RadPageView ID="RadPageView1" runat="server">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <telerik:RadGrid ID="dgCurrency" runat="server" EnableAJAX="True" AllowFilteringByColumn="false"
                                                                                        AutoGenerateColumns="false" Width="756px">
                                                                                        <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                                                                            <Columns>
                                                                                                <telerik:GridBoundColumn CurrentFilterFunction="Contains" FilterDelay="4000" DataField="AircraftCD"
                                                                                                    ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn CurrentFilterFunction="Contains" FilterDelay="4000" DataField="curr2"
                                                                                                    ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn CurrentFilterFunction="Contains" FilterDelay="4000" DataField="curr3"
                                                                                                    ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn CurrentFilterFunction="Contains" FilterDelay="4000" DataField="currTD"
                                                                                                    ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn CurrentFilterFunction="Contains" FilterDelay="4000" DataField="currTN"
                                                                                                    ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Appr" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr4" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Instr" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr5" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Days" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr6" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Days Rest" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" DataField="curr16" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Cal Mon" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr8" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Days" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr9" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Cal. Qtr" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr10" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Cal Qtr Rest" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" DataField="curr17" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Mon" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr11" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Prev 2 Qtrs" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" DataField="curr12" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Mon" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr13" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="Cal yr" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr14" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="365 Days" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr15" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn HeaderText="30 days" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    DataField="curr7" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                            </Columns>
                                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        </MasterTableView>
                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                            <Selecting AllowRowSelect="true" />
                                                                                        </ClientSettings>
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </telerik:RadPageView>
                                                                    <telerik:RadPageView ID="RadPageView2" runat="server">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <telerik:RadGrid ID="dgCurrencyChecklist" runat="server" OnNeedDataSource="CurrencyChecklist_BindData"
                                                                                        OnItemDataBound="CurrencyChecklist_ItemDataBound" EnableAJAX="True" AllowFilteringByColumn="false"
                                                                                        AutoGene AutoGenerateColumns                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ="false" Width="756px">
                                                                                        <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                                                                            <Columns>
                                                                                                <telerik:GridTemplateColumn HeaderText="Description" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="CrewChecklistDescription">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbCrewChecklistDescription" runat="server" CssClass="text60" Text='<%# Eval("CrewChecklistDescription") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Due" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    ShowFilterIcon="false" UniqueName="DueDT">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbDueDT" runat="server" CssClass="text60" Text='<%# Eval("DueDT") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Alert" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    ShowFilterIcon="false" UniqueName="AlertDT">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbAlertDT" runat="server" CssClass="text60" Text='<%# Eval("AlertDT") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Grace" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                                                    ShowFilterIcon="false" UniqueName="GraceDT">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbGraceDT" runat="server" CssClass="text60" Text='<%# Eval("GraceDT") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Aircraft Type" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="AircraftTypeCD">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbAircraftTypeCD" runat="server" CssClass="text60" Text='<%# Eval("AircraftTypeCD") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Aircraft Description" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="AircraftDescription">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbAircraftTypeDesc" runat="server" CssClass="text60" Text='<%# Eval("AircraftDescription") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Current Flt. Hours" CurrentFilterFunction="Contains"
                                                                                                    UniqueName="CurrentFltHrs" FilterDelay="4000" ShowFilterIcon="false">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbCurrentFltHrs" runat="server" CssClass="text60" MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn HeaderText="Total Reqd. Flt. Hours" CurrentFilterFunction="Contains"
                                                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="TotalREQFlightHrs">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lbTotalReqdFltHrs" runat="server" CssClass="text60" Text='<%# Eval("TotalREQFlightHrs") %>'
                                                                                                            MaxLength="40"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                            </Columns>
                                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        </MasterTableView>
                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                            <Selecting AllowRowSelect="true" />
                                                                                        </ClientSettings>
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </telerik:RadPageView>
                                                                </telerik:RadMultiPage>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <telerik:RadPanelBar ID="pnlAdditionalNotes" Width="100%" ExpandAnimation-Type="None"
                                CollapseAnimation-Type="None" runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="false" Text="Additional Notes" CssClass="PanelHeaderCrewRoster">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="note-box">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbAdditionalNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                <tr align="right">
                                    <td>
                                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" CssClass="button" OnClick="SaveChanges_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="false"
                                            CssClass="button" OnClick="Cancel_Click" />
                                    </td>
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnNationalityID" runat="server" />
                                    <asp:HiddenField ID="hdnClientID" runat="server" />
                                    <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                                    <asp:HiddenField ID="hdnDepartmentID" runat="server" />
                                    <asp:HiddenField ID="hdnResidenceCountryID" runat="server" />
                                    <asp:HiddenField ID="hdnLicCountryID" runat="server" />
                                    <asp:HiddenField ID="hdnAddLicCountryID" runat="server" />
                                    <asp:HiddenField ID="hdnCitizenshipID" runat="server" />
                                    <asp:HiddenField ID="hdnCountryID" runat="server" />
                                    <asp:HiddenField ID="hdnCrewID" runat="server" />
                                    <asp:HiddenField ID="hdnCrewGroupID" runat="server" />
                                    <asp:HiddenField ID="hdnTemp" runat="server" />
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
