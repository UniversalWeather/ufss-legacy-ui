﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightLegPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightLegPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Legs</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <style type="text/css">
        .ui-jqgrid .ui-jqgrid-bdiv {
            overflow: hidden !important;
        }
        body{background-color:#fff !important}
        .divGridPanel {
            background-color: #fff !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var jqgridTableId = '#gridPreflightLeg';
            var oArg = new Object();
            var _tripId;
            $(document).ready(function () {
                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page", rows: "size", order: "dir", sort: "sort"
                    }
                });
                _tripId = $.trim(unescape(getQuerystring("TripID", "")));
                $(jqgridTableId).jqGrid({                    
                    url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                    serializeGridData: function (postData) {
                        $(jqgridTableId).jqGrid("clearGridData");
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        postData.apiType = 'fss';
                        postData.method = 'ReadLegDetailsFromTrip';
                        postData.TripId = _tripId;
                        return postData;
                    },
                    height: 230,
                    width: 500,
                    viewrecords: true,
                    rowNum: $("#rowNum").val(),
                    multiselect: true,
                    pager: "#pg_gridPager",
                    colNames: ['LegID', 'LegNUM', 'DepartAir', 'ArrivelAir'],
                    colModel: [
                        { name: 'LegID', index: 'LegID', key: true, hidden: true },
                        { name: 'LegNUM', index: 'LegNUM', width: 90 },
                        { name: 'DepartAir', index: 'DepartAir', width: 190 },
                        { name: 'ArrivelAir', index: 'ArrivelAir', width: 190 }

                    ],
                    ondblClickRow: function (rowId) {
                        var rowData = jQuery(this).getRowData(rowId);
                        returnToParent(rowData);
                    },
                    onSelectRow: function(id) {
                        var rowData = $(this).getRowData(id);
                        var lastSel = rowData['LegID'];//replace name with any column
                        if (id !== lastSel) {
                            $(this).find(".selected").removeClass('selected');
                            $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                            $(this).find('.ui-state-highlight').addClass('selected');
                            lastSel = id;
                        }
                    }                  
                });
                
                $("#btnSubmit").click(function () {
                    var selr = jQuery(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
                    if (selr != null && selr.length > 0) {
                        var selectedLegNum='';
                        var selectedLegId='';
                        for (var i = 0; i < selr.length; i++) {
                            var rowData = $(jqgridTableId).getRowData(selr[i]);
                            selectedLegNum += ','+ rowData["LegNUM"];
                            selectedLegId += ',' + rowData["LegID"];
                        }
                        var oArg = new Object();
                        oArg.LegNUM = selectedLegNum.substring(1);
                        oArg.LegID = selectedLegId.substring(1);

                        oArg.Arg1 = oArg.LegNUM;

                        var oWnd = GetRadWindow();
                        oWnd.close(oArg);
                    } else {
                        showMessageBox('Please select a record.', popupTitle);
                    }
                    return false;
                });
                
                $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

                $("#pagesizebox").insertBefore('.ui-paging-info');
            });
            
            function reloadPageSize() {
                var myGrid = $(jqgridTableId);
                var currentValue = $("#rowNum").val();
                myGrid.setGridParam({ rowNum: currentValue });
                myGrid.trigger('reloadGrid');
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            
            function returnToParent(rowData) {
                var oArg = new Object();
                oArg.LegNUM = rowData["LegNUM"];
                oArg.LegID = rowData["LegID"];

                oArg.Arg1 = oArg.LegNUM;

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <br/>
        <table id="gridPreflightLeg" class="table table-striped table-hover table-bordered"></table>
        <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>                              
                               <div id="pagesizebox">
                               <span>Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                </div> 
                            </div>
        <div class="clear"></div>
        <div style="padding: 5px 5px; text-align: right;">
            <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
        </div>
        <asp:HiddenField ID="hdName" runat="server" />
    </div>
    </form>
</body>
</html>
