﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightAPIS.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightAPIS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>APIS</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">

            function GridCreated(sender, eventArgs) {
                //gets the main table scrollArea HTLM element  
                var scrollArea = document.getElementById(sender.get_element().id + "_GridData");
                var row = sender.get_masterTableView().get_selectedItems()[0];

                //if the position of the selected row is below the viewable grid area  
                if (row) {
                    if ((row.get_element().offsetTop - scrollArea.scrollTop) + row.get_element().offsetHeight + 20 > scrollArea.offsetHeight) {
                        //scroll down to selected row  
                        scrollArea.scrollTop = scrollArea.scrollTop + ((row.get_element().offsetTop - scrollArea.scrollTop) +
                    row.get_element().offsetHeight - scrollArea.offsetHeight) + row.get_element().offsetHeight;
                    }
                    //if the position of the the selected row is above the viewable grid area  
                    else if ((row.get_element().offsetTop - scrollArea.scrollTop) < 0) {
                        //scroll the selected row to the top  
                        scrollArea.scrollTop = row.get_element().offsetTop;
                    }
                }
            }

            function clearhidden() {
                document.getElementById("<%=hdHomeBase.ClientID%>").value = "";
                return true;
            }

            function alertCallBackFn(arg) {
                document.getElementById("<%=dgPreflightLeg.ClientID%>").focus();
            }

            function alertUnableAPISFn(arg) {
                document.getElementById("<%=dgPreflightLeg.ClientID%>").focus();
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbSearchClientCode.ClientID%>').value;
                }
                else if (radWin == "rdHomebasePopup") {
                    url = '../../Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            // this function is used to display the value of selected Client code from popup
            function ClientCidePopupClose(oWnd, args) {
                var combo = $find("<%= tbSearchClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = arg.ClientDescription;
                        document.getElementById("<%=hdClientCodeID.ClientID%>").value = arg.ClientID;
                        if (arg.ClientCD != null)
                            document.getElementById("<%=lbcvClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdClientCodeID.ClientID%>").value = "";
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientHomebaseClose(oWnd, args) {
                //get the transferred arguments               
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        if (arg.HomebaseID.split(',').length > 1) {

                            var lableID = '<%= lblMultiHomebase.ClientID %>';
                            var lable = document.getElementById(lableID);
                            if (lable) {
                                lable.style.display = 'inherit';
                                lable.style.color = "#FF3300";
                            }
                            document.getElementById("<%=lbHomeBase.ClientID%>").innerHTML = "";
                        }
                        else {
                            var lableID = '<%= lblMultiHomebase.ClientID %>';
                            var lable = document.getElementById(lableID);
                            if (lable) {
                                lable.style.display = 'none';
                            }
                            document.getElementById("<%=lbHomeBase.ClientID%>").innerHTML = arg.BaseDescription;
                        }
                        document.getElementById("<%=hdHomeBase.ClientID%>").value = arg.HomebaseID;
                        var temp = arg.HomeBase.split(',');
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = temp[0];
                    }
                    else {
                        document.getElementById("<%=hdHomeBase.ClientID%>").value = "";
                    }
                }
            }

            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to close this window
            //            function CloseAndRebind(arg) {
            //                GetRadWindow().Close();
            //                GetRadWindow().BrowserWindow.location.reload();
            //            }

            function CloseRadWindow(arg) {
                GetRadWindow().Close();

            }
            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);



                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker


                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                    }
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());

                    sender.value = formattedDate;
                }
            }

            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }

            // this function is used to display the value of selected Client code from popup


        </script>
    </telerik:RadCodeBlock>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dgPreflightRetrieveAPIS">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPreflightRetrieveAPIS" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPreflightRetrieveAPIS" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="dgPreflightLeg">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPreflightLeg" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgPreflightRetrieveAPIS">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgPreflightLeg" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnValidateFlightLegs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSubmitUWA">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSubmitCBP">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="box1">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <telerik:RadTabStrip ID="rtPreflightAPIS" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                                        AutoPostBack="true" MultiPageID="RadMultiPage1" SelectedIndex="0" Align="Justify"
                                        Style="float: inherit">
                                        <Tabs>
                                            <telerik:RadTab Value="Search" Text="Search" Selected="true">
                                            </telerik:RadTab>
                                            <telerik:RadTab Value="Exceptions" Text="APIS Exceptions">
                                            </telerik:RadTab>
                                        </Tabs>
                                    </telerik:RadTabStrip>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right">
                        <a href="#" class="print-icon" onclick="window.print();"></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <asp:HiddenField ID="hdClientCodeID" runat="server" />
                                        <asp:HiddenField ID="hdnCountryID" runat="server" />
                                        <table class="border-box">
                                            <tr>
                                                <td valign="top" width="50%">
                                                    <fieldset>
                                                        <legend>Search</legend>
                                                        <table>
                                                            <tr>
                                                                <td class="tdLabel125">
                                                                    <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                                                </td>
                                                                <td class="tdLabel125">
                                                                    <asp:CheckBox ID="ChkExcLog" Text="Exclude Logged" runat="server" />
                                                                </td>
                                                                <td class="tdLabel120">
                                                                    <asp:CheckBox ID="chkSubmittedTrip" Text="Submitted Trip" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel123">
                                                                                Start Departure Date
                                                                            </td>
                                                                            <td class="tdLabel90">
                                                                                <asp:TextBox ID="tbDepDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onclick="showPopup(this, event);"
                                                                                    MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                                                            </td>
                                                                            <td class="tdLabel70">
                                                                                Client Code
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="tbSearchClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                        CssClass="text50" ValidationGroup="Save" onBlur="return RemoveSpecialChars(this)"
                                                                        OnTextChanged="tbSearchClientCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                                        CssClass="browse-button" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" colspan="4">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td colspan="2" class="tdLabel240">
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:Label ID="lbClientCode" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                                                <asp:Label ID="lbcvClientCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td width="40%" valign="top">
                                                    <fieldset>
                                                        <legend>Status</legend>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="tdLabel110">
                                                                    <asp:CheckBox ID="ChkWorksheet" Text="Worksheet" runat="server" />
                                                                </td>
                                                                <td class="tdLabel110">
                                                                    <asp:CheckBox ID="ChkTripsheet" Text="Tripsheet" runat="server" />
                                                                </td>
                                                                <td class="tdLabel110">
                                                                    <asp:CheckBox ID="ChUnFillFilled" Text="Unfulfilled" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel110">
                                                                    <asp:CheckBox ID="ChkCancelled" Text="Canceled" runat="server" />
                                                                </td>
                                                                <td class="tdLabel110">
                                                                    <asp:CheckBox ID="ChkHold" Text="Hold" runat="server" />
                                                                </td>
                                                                <td class="tdLabel110">
                                                                    <asp:CheckBox ID="ChkSchedServ" Text="Sched Serv" runat="server" Visible="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" class="tblspace_15">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td width="10%">
                                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="RetrieveSearch_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                Home Base:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                    OnTextChanged="tbHomeBase_TextChanged" AutoPostBack="true" onBlur="return clearhidden();"></asp:TextBox>
                                                                <asp:HiddenField ID="hdHomeBase" runat="server" />
                                                                <asp:Button runat="server" ToolTip="Search for Home Base" CssClass="browse-button"
                                                                    ID="btnHomeBase" OnClientClick="javascript:openWin('rdHomebasePopup');return false;" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbHomeBase" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                <asp:Label ID="lbcvHomeBase" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                                <asp:Label ID="lblMultiHomebase" runat="server" Text="(Multiple)" ForeColor="Red"
                                                                    Style="display: none;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="preflight-apis">
                                                    <telerik:RadGrid ID="dgPreflightRetrieveAPIS" runat="server" AllowMultiRowSelection="true"
                                                        OnItemDataBound="dgPreflightRetrieveAPIS_ItemDataBound" OnSelectedIndexChanged="dgPreflightRetrieveAPIS_SelectedIndexChanged"
                                                        AllowSorting="true" OnNeedDataSource="dgPreflightRetrieveAPIS_BindData" AutoGenerateColumns="false"
                                                        PageSize="10" AllowPaging="false" Width="920px" Height="341px" ShowFooter="false">
                                                        <MasterTableView AllowPaging="false" CommandItemDisplay="None" ShowFooter="false"
                                                            AllowFilteringByColumn="false" DataKeyNames="TripID">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"
                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                                    HeaderStyle-Width="50px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" AllowFiltering="false"
                                                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="90px" UniqueName="Ddate">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="LogNum" HeaderText="Log" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="40px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="DutyType" HeaderText="Duty" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="RequestorFirstName" HeaderText="UV Req." AllowFiltering="false"
                                                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="80px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="APISException" HeaderText="APIS Excep" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Center" UniqueName="Aexcp">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="APISSubmit" HeaderText="Last Submitted On" AllowFiltering="false"
                                                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="100px" UniqueName="Asubmit">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="APISStatus" HeaderText="APIS Status" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="70px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="TripDescription" HeaderText="Trip Desc" ShowFilterIcon="false"
                                                                    FilterDelay="4000" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                                                    AllowFiltering="false" HeaderStyle-Width="150px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="TripID" HeaderText="Trip ID" Display="false"
                                                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                                    AllowFiltering="false" HeaderStyle-Width="100px">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="Acknowledge" HeaderText="Ack" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="100px" Display="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="WaitList" HeaderText="WL" AutoPostBackOnFilter="true"
                                                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains"
                                                                    HeaderStyle-Width="70px" Display="false">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                            <CommandItemTemplate>
                                                                <div style="padding: 5px 5px; text-align: right;">
                                                                </div>
                                                            </CommandItemTemplate>
                                                        </MasterTableView>
                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="false" />
                                                            <Selecting AllowRowSelect="true" />
                                                            <ClientEvents OnGridCreated="GridCreated" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                    <asp:Label ID="InjectScript" runat="server"></asp:Label>
                                                    <%--<asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table id="tblHidden" style="display: none; position: relative">
                                                        <tr>
                                                            <td>
                                                                <%-- <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
                                                                <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <div class="nav-space">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="preflight-apis">
                                                    <telerik:RadGrid ID="dgPreflightLeg" runat="server" AllowMultiRowSelection="true"
                                                        AllowSorting="false" AutoGenerateColumns="false" PageSize="10" AllowPaging="false"
                                                        OnItemDataBound="dgPreflightLeg_ItemDataBound" Width="920px" Height="341px" ShowFooter="false">
                                                        <MasterTableView AllowPaging="false" ShowFooter="false" CommandItemDisplay="None"
                                                            AllowFilteringByColumn="false" DataKeyNames="LegID,LegNum,TripID,DepartCountry,ArrivalCountry">
                                                            <Columns>
                                                                <telerik:GridClientSelectColumn UniqueName="SelectionID" HeaderText="Select" />
                                                                <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="DepartAir" HeaderText="From" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="To" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Departure Date/Time"
                                                                    DataType="System.DateTime" CurrentFilterFunction="Contains" FilterDelay="4000"
                                                                    ShowFilterIcon="false" UniqueName="LDdate">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrival Date/Time" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false" UniqueName="LAdate">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="ETE" HeaderText="ETE" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="FAR" HeaderText="FAR" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="USAFlightio" HeaderText="USA Flight I/O" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="UWAID" HeaderText="UWA ID" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="CBPConfNumber" HeaderText="CBP Confirmation No."
                                                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="USABorder" HeaderText="USA Border Crossing" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false"
                                                                    HeaderText="End Day" HeaderStyle-HorizontalAlign="Center" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <center>
                                                                            <asp:CheckBox ID="chkCheckList" runat="server" Checked='<%# Eval("DutyEnd") %>' Enabled="false">
                                                                            </asp:CheckBox>
                                                                        </center>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn DataField="DutyErr" HeaderText="Duty Err" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Flight Cost" ItemStyle-CssClass="fc_textbox"
                                                                    UniqueName="FlightCost" CurrentFilterFunction="Contains">
                                                                    <ItemTemplate>
                                                                        <telerik:RadNumericTextBox ID="tbFlightCost" runat="server" Type="Currency" Culture="en-US"
                                                                            DbValue='<%# Bind("FlightCost") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                            ReadOnly="true">
                                                                        </telerik:RadNumericTextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <%-- <telerik:GridBoundColumn DataField="FlightCost" HeaderText="Flight Cost" CurrentFilterFunction="Contains"
                                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>--%>
                                                                <telerik:GridBoundColumn DataField="DepartCountry" HeaderText="From" CurrentFilterFunction="Contains"
                                                                    Display="false" FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="ArrivalCountry" HeaderText="To" CurrentFilterFunction="Contains"
                                                                    Display="false" FilterDelay="4000" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                            <CommandItemTemplate>
                                                                <div style="padding: 5px 5px; text-align: right;">
                                                                </div>
                                                            </CommandItemTemplate>
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="false" />
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                            <tr align="center">
                                                <td colspan="3">
                                                    <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Text="Inbound/Outbound USA Flights with FAR Rules 121,125,135 will not be submitted to APIS/CBP"
                                                        Font-Size="X-Large" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnValidateFlightLegs" Text="Validate Flight Legs" runat="server"
                                                        OnClick="ValidateFlightLegs_Click" CssClass="button" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSubmitUWA" Text="Submit to UWA APIS" runat="server" CssClass="button"
                                                        OnClick="SubmitUWA_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSubmitCBP" Text="Submit To CBP" runat="server" OnClick="SubmitCBP_Click"
                                                        CssClass="button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="border-box">
                                            <tr>
                                                <td class="note-box">
                                                    <%-- <div  width="100%" id ="divAPISException" runat ="server"></div>--%>
                                                    <asp:TextBox ID="tbAPISExceptions" runat="server" TextMode="MultiLine" CssClass="textarea-apis"
                                                        Enabled="true" ReadOnly="true">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
