﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailTrip.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.EmailTrip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-mail Notification</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function confirmEmailCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnEmailYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnEmailNo.ClientID%>').click();
                }
            }

            function alertCallBack(arg) {
                document.getElementById("<%=btnAlert.ClientID%>").click();
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function CloseWindow() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalPopupForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalPopupForm" runat="server">
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <fieldset>
                                    <legend>E-mail Tripsheet to Crew</legend>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <telerik:RadGrid ID="dgCrew" runat="server" OnNeedDataSource="Crew_BindData" EnableAJAX="True"
                                                    AutoGenerateColumns="false" OnItemDataBound="Crew_ItemDataBound" Width="320px"
                                                    Height="300px" AllowMultiRowSelection="false" AllowPaging="false" ShowFooter="false"
                                                    ShowStatusBar="false" CommandItemDisplay="None">
                                                    <MasterTableView DataKeyNames="CrewID" CommandItemDisplay="None" AllowFilteringByColumn="false"
                                                        ShowFooter="false" ItemStyle-HorizontalAlign="Left" AllowPaging="false">
                                                        <Columns>
                                                            <telerik:GridTemplateColumn UniqueName="IsSelect" HeaderText="Select" HeaderStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" OnCheckedChanged="Crew_ToggleRowSelection"
                                                                        AutoPostBack="True" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="Crew_ToggleSelectedState"
                                                                        AutoPostBack="True" />
                                                                </HeaderTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="CrewName" HeaderText="Crew Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbCrewName" runat="server"></asp:Label>
                                                                    <telerik:RadToolTip ID="radCrewToolTip" runat="server" TargetControlID="lbCrewName"
                                                                        RelativeTo="Element" Skin="WebBlue" Position="BottomRight" Animation="Fade" HideDelay="1000"
                                                                        AutoCloseDelay="3500" ContentScrolling="Default">
                                                                    </telerik:RadToolTip>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="CrewEmail" HeaderText="" Display="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbCrewEmail" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="false" AllowRowsDragDrop="false" AllowColumnsReorder="false"
                                                        ReorderColumnsOnClient="true">
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" />
                                                        <ClientEvents />
                                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                    </ClientSettings>
                                                    <GroupingSettings CaseSensitive="false" />
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td valign="top">
                                <fieldset>
                                    <legend>E-mail Itinerary to PAX</legend>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <telerik:RadGrid ID="dgPax" runat="server" OnNeedDataSource="Pax_BindData" EnableAJAX="True"
                                                    AutoGenerateColumns="false" OnItemDataBound="Pax_ItemDataBound" Width="320px"
                                                    Height="300px" AllowMultiRowSelection="false" AllowPaging="false" ShowFooter="false"
                                                    ShowStatusBar="false" CommandItemDisplay="None">
                                                    <MasterTableView DataKeyNames="PassengerID" CommandItemDisplay="None" AllowFilteringByColumn="false"
                                                        ShowFooter="false" ItemStyle-HorizontalAlign="Left" AllowPaging="false">
                                                        <Columns>
                                                            <telerik:GridTemplateColumn UniqueName="IsSelect" HeaderText="Select" HeaderStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" OnCheckedChanged="Pax_ToggleRowSelection"
                                                                        AutoPostBack="True" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="Pax_ToggleSelectedState"
                                                                        AutoPostBack="True" />
                                                                </HeaderTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="PaxName" HeaderText="PAX Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbPaxName" runat="server"></asp:Label>
                                                                    <telerik:RadToolTip ID="radPaxToolTip" runat="server" TargetControlID="lbPaxName"
                                                                        RelativeTo="Element" Skin="WebBlue" Position="BottomRight" Animation="Fade" HideDelay="1000"
                                                                        AutoCloseDelay="3500" ContentScrolling="Default">
                                                                    </telerik:RadToolTip>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="PaxEmail" HeaderText="" Display="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbPaxEmail" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="false" AllowRowsDragDrop="false" AllowColumnsReorder="false"
                                                        ReorderColumnsOnClient="true">
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" />
                                                        <ClientEvents />
                                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                    </ClientSettings>
                                                    <GroupingSettings CaseSensitive="false" />
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                            <td>Enter from E-mail
                                            </td>
                                            </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="tbEmailFrom" runat="server" CssClass="text325" MaxLength="50"> </asp:TextBox><br />
                                        </td>
                                    </tr>
                                            <tr>
                                                <td>E-mail Tripsheet to Other(s)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="tbEmailTripToOthers" runat="server" CssClass="text325" MaxLength="500"
                                                TextMode="MultiLine"></asp:TextBox><br />
                                        </td>
                                    </tr>
                                    <tr>
                                            <td align="left" class="srch_category">E-mail should be comma separated
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            E-mail Itinerary to Other(s)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="tbEmailItineraryToOthers" runat="server" CssClass="text325" MaxLength="500"
                                                TextMode="MultiLine"></asp:TextBox><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="srch_category">
                                            E-mail should be comma separated
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="srch_category">
                                            Report Format
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="left" class="srch_category">
                                            <asp:DropDownList ID="ddlReportFormat" runat="server" Width="250px" >
                                                <asp:ListItem Selected="True" Text="Select" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav-6">
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:Label ID="lbMessage" runat="server" Font-Size="Small"></asp:Label>
                            </td>
                            <td align="right">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left">
                                            <asp:Button ID="btnEmail" runat="server" Text="Send E-mail" OnClick="btnEmail_click"
                                                CssClass="button" />
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:CloseWindow();return false;"
                                                CssClass="button" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="Table1" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnEmailYes" runat="server" Text="Button" OnClick="btnEmailYes_Click" />
                    <asp:Button ID="btnEmailNo" runat="server" Text="Button" OnClick="btnEmailNo_Click" />
                    <asp:Button ID="btnAlert" runat="server" Text="Button" OnClick="btnAlert_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
