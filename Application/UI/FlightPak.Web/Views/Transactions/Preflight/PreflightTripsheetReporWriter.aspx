﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Preflight.Master"
    CodeBehind="PreflightTripsheetReporWriter.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreflightTripsheetReporWriter" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript">

        function openReport(radWin) {
            var url = '';
            //Tripsheet Report Writer
            if (radWin == "Overview") {
                url = '../../Reports/ReportViewerModule.aspx?xmlFilename=PRETSOverview.xml';
            }
            else if (radWin == "Tripsheet") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PreTSTripSheetMain.xml';
            }
            else if (radWin == "Summary") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREPRETSSummary.xml';
            }
            else if (radWin == "Itinerary") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSTripSheetItinerary.xml';
            }
            else if (radWin == "FlightLog") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSFlightLogInformation.xml';
            }
            else if (radWin == "PassengerProf") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSPaxProfile.xml';
            }
            else if (radWin == "PassengerTravelAuth") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSPaxTravelAuthorization.xml';
            }
            else if (radWin == "InternationalData") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSInternationalData.xml';
            }
            else if (radWin == "Alerts") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSTripSheetAlert.xml';
            }
            else if (radWin == "PAERForm") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSPairForm.xml';
            }
            else if (radWin == "GeneralDeclare") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSGeneralDeclaration.xml';
            }
            else if (radWin == "CrewDutyOver") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSCrewDutyOverview.xml';
            }
            else if (radWin == "TripChecklist") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSTripSheetChecklist.xml';
            }
            else if (radWin == "CANPASSForm") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETSCanPassForm.xml';
            }
            else if (radWin == "GeneralDeclareAppen") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREGeneralDeclarationAPDX.xml';
            }
            var oWnd = radopen(url, "RadPRTSReportsPopup");
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
            <telerik:RadWindow ID="RadPRTSReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PreFlight/PreflightTripsheetReporWriter.aspx">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            <table border="1" cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Tripsheet Report Writer</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkOverview" runat="server" Font-Bold="true" Text="Overview"
                                            OnClientClick="javascript:openReport('Overview');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripsheet" runat="server" Font-Bold="true" Text="Tripsheet"
                                            OnClientClick="javascript:openReport('Tripsheet');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkSummary" runat="server" Font-Bold="true" Text="Summary" OnClientClick="javascript:openReport('Summary');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkItinerary" runat="server" Font-Bold="true" Text="Itinerary"
                                            OnClientClick="javascript:openReport('Itinerary');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFlightLog" runat="server" Font-Bold="true" Text="Flight Log"
                                            OnClientClick="javascript:openReport('FlightLog');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengerProf" runat="server" Font-Bold="true" Text="Passenger Profile"
                                            OnClientClick="javascript:openReport('PassengerProf');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengerTravelAuth" runat="server" Font-Bold="true" Text="Passenger Travel Authorization"
                                            OnClientClick="javascript:openReport('PassengerTravelAuth');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkInternationalData" runat="server" Font-Bold="true" Text="International Data"
                                            OnClientClick="javascript:openReport('InternationalData');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAlerts" runat="server" Font-Bold="true" Text="Alerts" OnClientClick="javascript:openReport('Alerts');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPAERForm" runat="server" Font-Bold="true" Text="PAER Form"
                                            OnClientClick="javascript:openReport('PAERForm');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkGeneralDeclare" runat="server" Font-Bold="true" Text="General Declaration"
                                            OnClientClick="javascript:openReport('GeneralDeclare');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewDutyOver" runat="server" Font-Bold="true" Text="Crew Duty Overview"
                                            OnClientClick="javascript:openReport('CrewDutyOver');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripChecklist" runat="server" Font-Bold="true" Text="Trip Checklist"
                                            OnClientClick="javascript:openReport('TripChecklist');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCANPASSForm" runat="server" Font-Bold="true" Text="CANPASS Form"
                                            OnClientClick="javascript:openReport('CANPASSForm');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkGeneralDeclareAppen" runat="server" Font-Bold="true" Text="General Declaration Appendix 1/2"
                                            OnClientClick="javascript:openReport('GeneralDeclareAppen');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
