﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Preflight.Master"
    AutoEventWireup="true" CodeBehind="PreflightReports.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreflightReports" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Preflight.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PostFlightHeadContent" >
    <style type="text/css">
        .art-contentLayout {
            margin-top: 98px !important;
        }        
    </style>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript">

        function openReport(radWin) {
            var url = '';

            //Calender
            if (radWin == "AircraftCalI") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREAircraftCalendarI.xml';
            }
            else if (radWin == "AircraftCalII") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREAircraftCalendarII.xml';
            }
            else if (radWin == "AircraftCalIII") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREAircraftCalendarIII.xml';
            }
            else if (radWin == "AircraftCalIV") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREAircraftCalendarIV.xml';
            }
            else if (radWin == "CrewCalCrew") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRECrewCalendarCrew.xml';
            }
            else if (radWin == "CrewCalDate") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRECrewCalendarDate.xml';
            }
            else if (radWin == "FleetCal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREFleetCalendarAircraft.xml';
            }
            if (radWin == "FleetCalDate") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREFleetCalendarDate.xml';
            }
            else if (radWin == "MonthlyCrew") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREMonthlyCrewCalendar.xml';
            }
            else if (radWin == "MonthlyFleet") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREMonthlyFleetCalendar.xml';
            }
            else if (radWin == "WeeklyCal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREWeeklyCalendar.xml';
            }
            else if (radWin == "WeeklyCalII") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREWeeklyCalendarII.xml';
            }

                //Trip
            else if (radWin == "CorporateITI") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRECorporateItinerary.xml';
            }
            else if (radWin == "DailyBaseActI") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREDailyBaseActivityI.xml';
            }
            else if (radWin == "DailyBaseActII") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREDailyBaseActivityII.xml';
            }
            else if (radWin == "Destination") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREDestination.xml';
            }
            else if (radWin == "FleetSchPax") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREFleetScheduleandPaxInfoDate.xml';
            }
            else if (radWin == "FleetSchAir") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREFleetScheduleAircraft.xml';
            }
            else if (radWin == "FleetSchDate") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREFleetScheduleDate.xml';
            }
            else if (radWin == "FlightFollow") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREFlightFollowingLog.xml';
            }
            else if (radWin == "HoursProj") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREHoursLandingsProjection.xml';
            }
            else if (radWin == "PassengerITI") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREPassengerItinerary.xml';
            }
            else if (radWin == "TripCostQuote") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETripCostQuote.xml';
            }
            else if (radWin == "TripsheetAlert") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETripsheetAlerts.xml';
            }
            else if (radWin == "TripsheetAudit") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETripsheetAudit.xml';
            }
            else if (radWin == "TripsheetValidBranch") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETripExceptions.xml';
            }
            else if (radWin == "TripsheetCancel") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRETripsheetCancellation.xml';
            }
            else if (radWin == "UniversalTrip") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREUVTripForm.xml';
            }

                //Other
            else if (radWin == "CharterQuoteCustProf") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PRECQCustomerProfile.xml';
            }
            else if (radWin == "GADesk") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREGADesk.xml';
            }
            else if (radWin == "StandardServ") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREStandardServiceForm.xml';
            }

                //Aircraft
            else if (radWin == "AircraftStatus") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREAircraftStatus.xml';
            }
            else if (radWin == "ComponentStatus") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREComponentStatus.xml';
            }
            else if (radWin == "MaintenanceAlert") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PreFlight&xmlFilename=PREMaintenanceAlerts.xml';
            }

                //Tripsheet Report Writer
            else if (radWin == "Overview") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=1';
            }
            else if (radWin == "Tripsheet") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=2';
            }
            else if (radWin == "Summary") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=13';
            }
            else if (radWin == "Itinerary") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=3';
            }
            else if (radWin == "FlightLog") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=4';
            }
            else if (radWin == "PassengerProf") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=5';
            }
            else if (radWin == "PassengerTravelAuth") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=7';
            }
            else if (radWin == "InternationalData") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=6';
            }
            else if (radWin == "Alerts") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=8';
            }
            else if (radWin == "PAERForm") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=9';
            }
            else if (radWin == "GeneralDeclare") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=10';
            }
            else if (radWin == "CrewDutyOver") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=11';
            }
            else if (radWin == "TripChecklist") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=12';
            }
            else if (radWin == "CANPASSForm") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=14';
            }
            else if (radWin == "GeneralDeclareAppen") {
                url = '../../Reports/PreflightTripSheetReportViewer.aspx?ReportNumber=16';
            }
            var oWnd = radopen(url, "RadPRReportsPopup");
            
            if (IsMobileSafari()) {
                oWnd.set_autoSize(false);
                oWnd.setSize(800, 600);
                oWnd.set_behaviors(Telerik.Web.UI.WindowBehaviors.Move + Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Maximize);
            }
            
            oWnd.Center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadPRReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Modal="true" AutoSize="True" Behaviors="Close" VisibleStatusbar="false" KeepInScreenBounds="True" NavigateUrl="~/Views/Transactions/PreFlight/PreflightReports.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            <table border="1" cellpadding="0" cellspacing="0" width="250px" align="left" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Calendar</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftCal1" runat="server" Font-Bold="true" Text="Aircraft Calendar I"
                                            OnClientClick="javascript:openReport('AircraftCalI');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftCalII" runat="server" Font-Bold="true" Text="Aircraft Calendar II"
                                            OnClientClick="javascript:openReport('AircraftCalII');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftCalIII" runat="server" Font-Bold="true" Text="Aircraft Calendar III"
                                            OnClientClick="javascript:openReport('AircraftCalIII');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftCalIV" runat="server" Font-Bold="true" Text="Aircraft Calendar IV"
                                            OnClientClick="javascript:openReport('AircraftCalIV');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewCalCrew" runat="server" Font-Bold="true" Text="Crew Calendar/Crew"
                                            OnClientClick="javascript:openReport('CrewCalCrew');return false;">Crew Calendar/Crew</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewCalDate" runat="server" Font-Bold="true" Text="Crew Calendar/Date"
                                            OnClientClick="javascript:openReport('CrewCalDate');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFleetCal" runat="server" Font-Bold="true" Text="Fleet Calendar/Aircraft"
                                            OnClientClick="javascript:openReport('FleetCal');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFleetCalDate" runat="server" Font-Bold="true" Text="Fleet Calendar/Date"
                                            OnClientClick="javascript:openReport('FleetCalDate');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkMonthlyCrew" runat="server" Font-Bold="true" Text="Monthly Crew Calendar"
                                            OnClientClick="javascript:openReport('MonthlyCrew');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkMonthlyFleet" runat="server" Font-Bold="true" Text="Monthly Fleet Calendar"
                                            OnClientClick="javascript:openReport('MonthlyFleet');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkWeeklyCal" runat="server" Font-Bold="true" Text="Weekly Calendar"
                                            OnClientClick="javascript:openReport('WeeklyCal');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkWeeklyCalII" runat="server" Font-Bold="true" Text="Weekly Calendar II"
                                            OnClientClick="javascript:openReport('WeeklyCalII');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table border="1" cellpadding="0" cellspacing="0" width="250px" align="left" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Other</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCharterQuoteCustProf" runat="server" Font-Bold="true" Text="Charter Quote Customer Profile"
                                            OnClientClick="javascript:openReport('CharterQuoteCustProf');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkGADesk" runat="server" Font-Bold="true" Text="GA Desk" OnClientClick="javascript:openReport('GADesk');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkStandardServ" runat="server" Font-Bold="true" Text="Standard Service Form"
                                            OnClientClick="javascript:openReport('StandardServ');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            <table border="1" cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Trip</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCorporateITI" runat="server" Font-Bold="true" Text="Corporate Itinerary"
                                            OnClientClick="javascript:openReport('CorporateITI');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkDailyBaseActI" runat="server" Font-Bold="true" Text="Daily Base Activity I"
                                            OnClientClick="javascript:openReport('DailyBaseActI');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkDailyBaseActII" runat="server" Font-Bold="true" Text="Daily Base Activity II"
                                            OnClientClick="javascript:openReport('DailyBaseActII');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkDestination" runat="server" Font-Bold="true" Text="Destination"
                                            OnClientClick="javascript:openReport('Destination');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFleetSchPax" runat="server" Font-Bold="true" Text="Fleet Schedule and PAX Info/Date"
                                            OnClientClick="javascript:openReport('FleetSchPax');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFleetSchAir" runat="server" Font-Bold="true" Text="Fleet Schedule/Aircraft"
                                            OnClientClick="javascript:openReport('FleetSchAir');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFleetSchDate" runat="server" Font-Bold="true" Text="Fleet Schedule/Date"
                                            OnClientClick="javascript:openReport('FleetSchDate');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFlightFollow" runat="server" Font-Bold="true" Text="Flight Following Log"
                                            OnClientClick="javascript:openReport('FlightFollow');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkHoursProj" runat="server" Font-Bold="true" Text="Hours/Landing Projection"
                                            OnClientClick="javascript:openReport('HoursProj');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengerITI" runat="server" Font-Bold="true" Text="Passenger Itinerary"
                                            OnClientClick="javascript:openReport('PassengerITI');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripCostQuote" runat="server" Font-Bold="true" Text="Trip Cost Quote"
                                            OnClientClick="javascript:openReport('TripCostQuote');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripsheetAlert" runat="server" Font-Bold="true" Text="Tripsheet Alerts"
                                            OnClientClick="javascript:openReport('TripsheetAlert');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripsheetAudit" runat="server" Font-Bold="true" Text="Tripsheet Audit"
                                            OnClientClick="javascript:openReport('TripsheetAudit');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripsheetValidBranch" runat="server" Font-Bold="true" Text="Trip Exceptions"
                                            OnClientClick="javascript:openReport('TripsheetValidBranch');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripsheetCancel" runat="server" Font-Bold="true" Text="Tripsheet Cancellation"
                                            OnClientClick="javascript:openReport('TripsheetCancel');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkUniversalTrip" runat="server" Font-Bold="true" Text="Universal Trip Form"
                                            OnClientClick="javascript:openReport('UniversalTrip');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <table border="1" cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Aircraft</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftStatus" runat="server" Font-Bold="true" Text="Aircraft Status"
                                            OnClientClick="javascript:openReport('AircraftStatus');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkComponentStatus" runat="server" Font-Bold="true" Text="Component Status"
                                            OnClientClick="javascript:openReport('ComponentStatus');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkMaintenanceAlert" runat="server" Font-Bold="true" Text="Maintenance Alerts"
                                            OnClientClick="javascript:openReport('MaintenanceAlert');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </td>
            <td valign="top">
                <table >
                    <tr>
                        <td>
                            <table border="1" cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Tripsheet Report Writer</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkOverview" runat="server" Font-Bold="true" Text="Overview"
                                            OnClientClick="javascript:openReport('Overview');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripsheet" runat="server" Font-Bold="true" Text="Tripsheet"
                                            OnClientClick="javascript:openReport('Tripsheet');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkSummary" runat="server" Font-Bold="true" Text="Summary" OnClientClick="javascript:openReport('Summary');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkItinerary" runat="server" Font-Bold="true" Text="Itinerary"
                                            OnClientClick="javascript:openReport('Itinerary');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFlightLog" runat="server" Font-Bold="true" Text="Flight Log"
                                            OnClientClick="javascript:openReport('FlightLog');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengerProf" runat="server" Font-Bold="true" Text="Passenger Profile"
                                            OnClientClick="javascript:openReport('PassengerProf');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengerTravelAuth" runat="server" Font-Bold="true" Text="Passenger Travel Authorization"
                                            OnClientClick="javascript:openReport('PassengerTravelAuth');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkInternationalData" runat="server" Font-Bold="true" Text="International Data"
                                            OnClientClick="javascript:openReport('InternationalData');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAlerts" runat="server" Font-Bold="true" Text="Alerts" OnClientClick="javascript:openReport('Alerts');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPAERForm" runat="server" Font-Bold="true" Text="PAER Form"
                                            OnClientClick="javascript:openReport('PAERForm');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkGeneralDeclare" runat="server" Font-Bold="true" Text="General Declaration"
                                            OnClientClick="javascript:openReport('GeneralDeclare');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewDutyOver" runat="server" Font-Bold="true" Text="Crew Duty Overview"
                                            OnClientClick="javascript:openReport('CrewDutyOver');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripChecklist" runat="server" Font-Bold="true" Text="Trip Checklist"
                                            OnClientClick="javascript:openReport('TripChecklist');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCANPASSForm" runat="server" Font-Bold="true" Text="CANPASS Form"
                                            OnClientClick="javascript:openReport('CANPASSForm');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkGeneralDeclareAppen" runat="server" Font-Bold="true" Text="General Declaration Appendix 1/2"
                                            OnClientClick="javascript:openReport('GeneralDeclareAppen');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

