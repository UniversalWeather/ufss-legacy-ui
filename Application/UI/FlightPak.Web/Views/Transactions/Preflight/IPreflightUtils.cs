﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public interface IPreflightUtils
    {
        decimal CalculateDistance(bool _IsKilometer, double deplatdeg = 0,
           double deplatmin = 0,
           string lcdeplatdir = "N",
           double deplngdeg = 0,
           double deplngmin = 0,
           string lcdeplngdir = "E",
           double arrlatdeg = 0,
           double arrlatmin = 0,
           string lcArrLatDir = "N",
           double arrlngdeg = 0,
           double arrlngmin = 0,
           string arrlngdir = "E");
        
        Dictionary<string, string> CalculateBias(string PowerSetting, double PowerSettings1TakeOffBias,
            double PowerSettings1LandingBias, double PowerSettings1TrueAirSpeed, double PowerSettings2TakeOffBias,
            double PowerSettings2LandingBias, double PowerSettings2TrueAirSpeed, double PowerSettings3TakeOffBias,
            double PowerSettings3LandingBias, double PowerSettings3TrueAirSpeed, double DepAirportTakeoffBIAS,
            double ArrAirportLandingBIAS, decimal TimeDisplayTenMin, out double dblbias, out double dbllandbias, out double dbltas);
        
        double CalculateWind(double aircraftWindAltitude, int _windReliability,
            double deplatdeg, double deplatmin, string lcDepLatDir,
            double deplngdeg, double deplngmin, string lcDepLngDir, long lnDepartZone,
            double arrlatdeg, double arrlatmin, string lcArrLatDir,
            double arrlngdeg, double arrlngmin, string lcArrLngDir, long lnArrivZone, string localdate, CalculationService.ICalculationService objDstsvc, string Formate = "");

        string GetIcaoEte(double lnwind, double lnLndBias, double lntas, double lnToBias, double lnmiles, string IsFixedRotary,
            decimal TimeDisplayTenMin, decimal ElapseTimeRounding);

    }
}
