﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightChecklist.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightChecklist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body {background-color:#fcfcfc !important; }
    </style>
</head>
<body>
       <%=Styles.Render("~/bundles/jqgrid") %>
        <%=Scripts.Render("~/bundles/jquery") %>
        <%=Scripts.Render("~/bundles/jqgridjs") %>
        <%=Scripts.Render("~/bundles/knockout") %>
        <%=Scripts.Render("~/bundles/sitecommon") %>
        <%=Scripts.Render("~/bundles/preflightChecklist") %>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientIDMode="AutoID">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgLegSummary">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgLegSummary" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
       <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="../../../Scripts/Common.js"></script>
            <script type="text/javascript">
                var oArg = new Object();
                var arg;
                var url = '';
                function openPreflightWin(radWin) {
                   
                    if (radWin == "rdChecklistGroup") {
                        url = '../../Settings/Company/TripManagerPopUp.aspx?CheckGroupCD=' + document.getElementById("tbChecklistGroup").value;
                    }
                    jConfirm('Warning, This Will Replace Your Current Checklist', "Confirmation!", conCallBackFn);
                }

                function conCallBackFn(arg) {
                    if (arg) {
                        var oWnd = radopen(url, "rdChecklistGroup");
                    }
                }

                function ConfirmClose(WinName) {
                    var oManager = GetRadWindowManager();
                    var oWnd = oManager.GetWindowByName(WinName);
                    //Find the Close button on the page and attach to the 
                    //onclick event
                    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                    CloseButton.onclick = function () {
                        CurrentWinName = oWnd.Id;
                        //radconfirm is non-blocking, so you will need to provide a callback function
                        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                    }
                }

                function OnClientCloseChecklistGroup(oWnd, args) {
                    var combo = $find("tbChecklistGroup");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        BindTripChecklistByCheckListGroupID(arg.CheckGroupID, arg.CheckGroupDescription);
                    if (arg) {
                        var step = "tbChecklistGroup_Changed";
                        document.getElementById("tbChecklistGroup").value = arg.CheckGroupDescription;
                        document.getElementById("<%=hdChecklistGroupID.ClientID%>").value = arg.CheckGroupID;
                        document.getElementById("<%=hdCheckGroupCD.ClientID%>").value = arg.CheckGroupCD;

                    }
                    else {
                        document.getElementById("tbChecklistGroup").value = "";
                        document.getElementById("<%=hdChecklistGroupID.ClientID%>").value = "";
                        document.getElementById("<%=hdCheckGroupCD.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

                function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function refreshGrid() {
            }

         
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="rdChecklistGroup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseChecklistGroup" KeepInScreenBounds="true" AutoSize="true"  
                    Modal="true" VisibleStatusbar="false" Behaviors="Close" NavigateUrl="~/Views/Settings/Company/TripManagerPopUp.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" class="divGridPanel" runat="server">
            <table cellpadding="0" cellspacing="0" class="box1">
                <tr>
                    <td>
                        <table id="gridChecklistLegSummary" class="table table-striped table-hover table-bordered"></table>
                    </td>
                </tr>
                <tr>
                    <td>
                            <a id="recordEdit" data-bind="event: { click: btnEdit_Click }" title="Edit" class="edit-icon-grid" href="#"></a>
                   </td>
                </tr>
                <tr>
                    <td align="left">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdLabel150">Change Checklist Group
                                </td>
                                <td>
                                    
                                    <input type="text"  id="tbChecklistGroup" data-bind="value: CheckGroupDescription" disabled="disabled" class="text160" />
                                    <asp:HiddenField runat="server" ID="hdChecklistGroupID" />
                                    <asp:HiddenField runat="server" ID="hdCheckGroupCD" />
                                    <input type="button" data-bind="enable: EditMode" id="btnChecklistGroup"
                                            onclick="javascript: openPreflightWin('rdChecklistGroup'); return false;" />                                
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
                <tr>
                   <td>
                       <table id="dgTripChecklist"></table>
                      
                   </td>
                </tr>
                <tr>
                    <td>
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0" style="min-height: 30px;">
                            <tr>
                                <td class="tdLabel80" align="right">
                                    <input type="button" id="btnCancel" value="Cancel" data-bind="event: { click: btnCancel_Click }, visible: EditMode" class="button" />
                                    <asp:HiddenField ID="hdnLeg" runat="server" />
                                    <asp:HiddenField ID="hdnLegID" runat="server" />
                                    <asp:HiddenField ID="hdnLegNUM" runat="server" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                </td>
                                <td align="right">
                                    <input type="button" id="btnSave" value="Save" data-bind="event: { click: btnSave_Click }, visible: EditMode" class="button" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
