﻿using System;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightAirportInfo : BaseSecuredPage
    {
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["Airportid"] != null)
                        {
                            hdnAirportID.Value = Request.QueryString["Airportid"];
                        }

                        if (hdnAirportID.Value != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objService.GetAirportByAirportID(Convert.ToInt64(hdnAirportID.Value)).EntityList;
                                if (objRetVal != null && objRetVal.Count > 0)
                                {
                                    tbICAO.Text = objRetVal[0].IcaoID;
                                    if (objRetVal[0].CityName != null)
                                        tbCity.Text = objRetVal[0].CityName;
                                    if (objRetVal[0].StateName != null)
                                        tbStateProvince.Text = objRetVal[0].StateName;
                                    if (objRetVal[0].CountryCD != null)
                                        tbCountry.Text = objRetVal[0].CountryCD;
                                    if (objRetVal[0].CountryName != null)
                                        tbCountryDescription.Text = objRetVal[0].CountryName;
                                    if (objRetVal[0].AirportName != null)
                                        tbAirport.Text = objRetVal[0].AirportName;
                                    if (objRetVal[0].TakeoffBIAS != null)
                                        tbTakeoffBias.Text = objRetVal[0].TakeoffBIAS.ToString();
                                    if (objRetVal[0].LandingBIAS != null)
                                        tbLandingBias.Text = objRetVal[0].LandingBIAS.ToString();
                                    if (objRetVal[0].OffsetToGMT != null)
                                        tbUTC.Text = objRetVal[0].OffsetToGMT.ToString();
                                    //Fix for 2411 
                                    if (objRetVal[0].WidthLengthRunway != null)
                                        tbRunwayWidth.Text = objRetVal[0].WidthLengthRunway.ToString();
                                    if (objRetVal[0].LengthWidthRunway != null)
                                        tbRunwayLength.Text = objRetVal[0].LengthWidthRunway.ToString();
                                    if (objRetVal[0].DSTRegionCD != null)
                                        tbDSTRegion.Text = objRetVal[0].DSTRegionCD;
                                    if (objRetVal[0].GeneralNotes != null)
                                        tbNotes.Text = objRetVal[0].GeneralNotes;
                                    if (objRetVal[0].Alerts != null)
                                        tbAlerts.Text = objRetVal[0].Alerts;
                                }
                            }
                        }

                        DisableAirportFields();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        //To Disable fields
        private void DisableAirportFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbICAO.Enabled = false;
                tbCity.Enabled = false;
                tbStateProvince.Enabled = false;
                tbCountry.Enabled = false;
                tbCountryDescription.Enabled = false;
                tbAirport.Enabled = false;
                tbTakeoffBias.Enabled = false;
                tbLandingBias.Enabled = false;
                tbUTC.Enabled = false;
                tbRunwayWidth.Enabled = false;
                tbRunwayLength.Enabled = false;
                tbDSTRegion.Enabled = false;
                tbNotes.Enabled = false;
                tbAlerts.Enabled = false;
            }
        }
    }
}