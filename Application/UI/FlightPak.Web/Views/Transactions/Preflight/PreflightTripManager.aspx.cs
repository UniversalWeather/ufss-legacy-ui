﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.Web.Optimization;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Error;
using FlightPak.Web.Framework.Helpers.Preflight;
using FlightPak.Web.Framework.Helpers.Validators;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.MasterData;
using System.Web.Script.Services;
using System.Web.Services;
using FlightPak.Web.Views.Transactions.CorporateRequest;
using FlightPak.Web.Views.Transactions.PostFlight;
using Newtonsoft.Json;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.PreflightService;
using Omu.ValueInjecter;
using FlightPak.Web.BusinessLite.Preflight;
using System.Collections;
using Newtonsoft.Json.Converters;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using System.Globalization;
using Newtonsoft.Json.Linq;
using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

using FlightPak.Web.CalculationService;
using System.Data;
using PassengerSummary = FlightPak.Web.Framework.Helpers.PassengerSummary;
using System.Web.Security;
using FlightPak.Web.BusinessLite;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    
    public partial class PreflightTripManager : TripManagerBase
    {
        private static ExceptionManager exManager;

        #region TripMain
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightTripViewModel> GetAFreshPreflightTrip()
        {
            FSSOperationResult<PreflightTripViewModel> result = new FSSOperationResult<PreflightTripViewModel>();
            if (!result.IsAuthorized())
                return result;
            UserPrincipalViewModel userPrincipal = getUserPrincipal();
            PreflightTripViewModel pfViewModel = new PreflightTripManagerLite().AddNewTrip(userPrincipal);
            pfViewModel.TripId = 0;
            pfViewModel.PreflightMain = NewTrip(pfViewModel.PreflightMain);
            HttpContext.Current.Session[WebSessionKeys.PreflightException] = null;
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;

            result.Result = pfViewModel;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> GetACurrentTrip()
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;

            Dictionary<string, object> Retobj = new Dictionary<string, object>();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            } else
            {
                pfViewModel.PreflightMain = CreateOrCancelTrip(pfViewModel.PreflightMain);
                pfViewModel.PreflightMain.EditMode = false;
                pfViewModel.TripId = null;
            }
            Retobj.Add("Trip", pfViewModel);
            Retobj.Add("Principal", getUserPrincipal());

            result.Result = Retobj;
            return result;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> GetUpdatedTrip(long homeBaseId)
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;

            Dictionary<string, object> Retobj = new Dictionary<string, object>();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();
            pfViewModel.PreflightMain = CreateOrCancelTrip(pfViewModel.PreflightMain);
            Retobj.Add("Trip", pfViewModel);
            Retobj.Add("Principal", GetUpdatedUserPrincipal(homeBaseId));

            result.Result = Retobj;
            return result;
        }
        public static UserPrincipalViewModel GetUpdatedUserPrincipal(long homeBaseId)
        {
            UserPrincipalViewModel upViewModel = new UserPrincipalViewModel();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetCompanyWithFilters(string.Empty, homeBaseId, true, true);
                List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();
                if (objRetVal.ReturnFlag)
                {
                    CompanyList = objRetVal.EntityList;
                    if (CompanyList != null && CompanyList.Count > 0)
                    {
                        upViewModel._homeBase = CompanyList[0].HomebaseCD;
                        upViewModel._TripMGRDefaultStatus = CompanyList[0].TripMGRDefaultStatus;
                    }
                }
            }
            return upViewModel;
        }
       
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, string>> SetAEditLock(long TripID)
        {
            FSSOperationResult<Dictionary<string, string>> result = new FSSOperationResult<Dictionary<string, string>>();
            if (!result.IsAuthorized())
                return result;

            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();

            Dictionary<string, string> Lock = new Dictionary<string, string>();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    var returnValue = CommonService.Lock(WebSessionKeys.PreflightMainLock, pfViewModel.TripId.Value);
                    if (returnValue.ReturnFlag)
                    {
                        pfViewModel.PreflightMain.EditMode = true;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                    }
                    Lock.Add("IsLocked", returnValue.ReturnFlag.ToString());
                    Lock.Add("LockMsg", returnValue.LockMessage);
                }
            }
            result.Result = Lock;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightTripViewModel> CancelATrip()
        {
            FSSOperationResult<PreflightTripViewModel> result = new FSSOperationResult<PreflightTripViewModel>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (pfViewModel.TripId != 0 && pfViewModel.TripId!=null)
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                    {
                        var returnValue = CommonService.UnLock(WebSessionKeys.PreflightMainLock, pfViewModel.TripId.Value);
                        PreflightMain p = (PreflightMain)HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip];
                        pfViewModel = InjectPreflightMainToPreflightTripViewModel(p, pfViewModel);
                        pfViewModel.PreflightMain.EditMode = false;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                    }
                } else
                {
                    pfViewModel.TripId = 0;
                    pfViewModel.PreflightMain = CreateOrCancelTrip(pfViewModel.PreflightMain);
                    pfViewModel.PreflightMain.EditMode = false;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = null;
                }
            }
            result.Result = pfViewModel;
            return result;
        }

        public static PreflightTripViewModel TripByTripID(long TripID)
        {
            PreflightMain Trip = new PreflightMain();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();

            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
            {
                var objRetVal = PrefSvc.GetTrip(TripID);
                if (objRetVal.ReturnFlag)
                {
                    Trip = objRetVal.EntityList[0];
                    pfViewModel = InjectPreflightMainToPreflightTripViewModel(Trip, pfViewModel);
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                    HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = Trip;
                    pfViewModel.PreflightMain.EditMode = false;
                }
            }
            return pfViewModel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> TripSearch(long TripNum)
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;

            Dictionary<string, object> Retobj = new Dictionary<string, object>();
            PreflightMain Trip = new PreflightMain();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            List<PreflightLegViewModel> pfLegModel = new List<PreflightLegViewModel>();
            pfViewModel.PreflightMain = new PreflightMainViewModel();
            bool isTripSearch = true;

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (pfViewModel.TripId == TripNum)
                {
                    pfViewModel.TripId = Trip.TripID;
                    isTripSearch = false;
                }
            }

            if (isTripSearch)
            {
                using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                {
                    var objRetVal = PrefSvc.GetTripByTripNum(TripNum);
                    if (objRetVal.ReturnFlag)
                    {
                        Trip = objRetVal.EntityList[0];
                        pfViewModel = InjectPreflightMainToPreflightTripViewModel(Trip, pfViewModel);
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                        HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = Trip;
                        pfViewModel.PreflightMain.EditMode = false;
                        Retobj.Add("flag", objRetVal.ReturnFlag);
                    } else
                    {
                        Retobj.Add("flag", objRetVal.ReturnFlag);
                    }
                }
            }
            Retobj.Add("Trip", pfViewModel);
            Retobj.Add("Principal", getUserPrincipal());
            result.Result = Retobj;
            return result;
        }

        private static Dictionary<String, PreflightLegCrewTransportationViewModel> SetPreflightLegTransportationListToVM(PreflightMain Trip)
        {
            Dictionary<String, PreflightLegCrewTransportationViewModel> legsCrewTransportationViewModels = new Dictionary<String, PreflightLegCrewTransportationViewModel>();
            if (Trip == null)
            {
                return legsCrewTransportationViewModels;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        foreach (PreflightLeg leg in Trip.GetLiveLegs())
                        {
                            PreflightTransportList LegDepTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "C" && x.IsDeleted == false && x.IsDepartureTransport == true).FirstOrDefault();
                            PreflightTransportList LegArrTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "C" && x.IsDeleted == false && x.IsArrivalTransport == true).FirstOrDefault();

                            PreflightLegCrewTransportationViewModel transVM = new PreflightLegCrewTransportationViewModel();
                            String departTransCode = "";
                            String arrivalTransCode = "";

                            if (LegDepTansport != null && LegDepTansport.AirportID == leg.DepartICAOID && LegDepTansport.AirportID != null && LegDepTansport.TransportID.HasValue)
                            {
                                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(LegDepTansport.TransportID)).EntityList;
                                    if (objRetVal != null && objRetVal.Count > 0)
                                    {
                                        departTransCode = objRetVal[0].TransportCD;
                                    }
                                }
                                transVM.DepartureAirportId = LegDepTansport.AirportID.Value;
                                transVM.PreflightDepartTransportId = LegDepTansport.PreflightTransportID;
                                transVM.DepartTransportId = LegDepTansport.TransportID.Value;
                                transVM.DepartCode = departTransCode;
                                transVM.DepartName = LegDepTansport.PreflightTransportName;
                                transVM.DepartRate = LegDepTansport.PhoneNum4; //omg!, this is stored in phone number 4 field :(
                                transVM.DepartPhone = LegDepTansport.PhoneNum1;
                                transVM.DepartFax = LegDepTansport.FaxNUM;
                                transVM.DepartComments = LegDepTansport.Comments;
                                transVM.DepartConfirmation = LegDepTansport.ConfirmationStatus;
                                transVM.SelectedDepartTransportStatus = LegDepTansport.Status;
                            }
                            if (LegArrTansport != null && LegArrTansport.AirportID == leg.ArriveICAOID && LegArrTansport.AirportID != null && LegArrTansport.TransportID.HasValue)
                            {
                                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(LegArrTansport.TransportID)).EntityList;
                                    if (objRetVal != null && objRetVal.Count > 0)
                                    {
                                        arrivalTransCode = objRetVal[0].TransportCD;
                                    }
                                }
                                transVM.ArrivalAirportId = LegArrTansport.AirportID.Value;
                                transVM.PreflightArrivalTransportId = LegArrTansport.PreflightTransportID;
                                transVM.ArrivalTransportId = LegArrTansport.TransportID.Value;
                                transVM.ArrivalCode = arrivalTransCode;
                                transVM.ArrivalName = LegArrTansport.PreflightTransportName;
                                transVM.ArrivalRate = LegArrTansport.PhoneNum4;//omg!, this is stored in phone number 4 field :(
                                transVM.ArrivalPhone = LegArrTansport.PhoneNum1;
                                transVM.ArrivalFax = LegArrTansport.FaxNUM;
                                transVM.ArrivalComments = LegArrTansport.Comments;
                                transVM.ArrivalConfirmation = LegArrTansport.ConfirmationStatus;
                                transVM.SelectedArrivalTransportStatus = LegArrTansport.Status;
                            }

                            legsCrewTransportationViewModels[leg.LegNUM.ToString()] = transVM;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
                return legsCrewTransportationViewModels;
            }
        }

        private static PreflightMain SetPreflightLegTransportationListFromVM(PreflightMain Trip, Dictionary<String, PreflightLegCrewTransportationViewModel> legsCrewTransportationViewModels)
        {
            if (Trip == null)
            {
                return null;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        foreach (String strLegNum in legsCrewTransportationViewModels.Keys)
                        {
                            long legNum = Convert.ToInt32(strLegNum);
                            PreflightLegCrewTransportationViewModel transVM = legsCrewTransportationViewModels[strLegNum];

                            PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == legNum).FirstOrDefault();
                            if (leg != null)
                            {
                                PreflightTransportList LegDepTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "C" && x.IsDeleted == false && x.IsDepartureTransport == true).FirstOrDefault();
                                PreflightTransportList LegArrTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "C" && x.IsDeleted == false && x.IsArrivalTransport == true).FirstOrDefault();
                                bool departureNew = false, arrivalNew = false;
                                if (LegDepTansport == null)
                                {
                                    departureNew = true;
                                    LegDepTansport = new PreflightTransportList();
                                    LegDepTansport.State = TripEntityState.NoChange;
                                }
                                if(LegArrTansport == null)
                                {
                                    arrivalNew = true;
                                    LegArrTansport = new PreflightTransportList();
                                    LegDepTansport.State = TripEntityState.NoChange;
                                }
                                if (transVM != null)
                                {
                                    
                                    if (transVM.DepartTransportId != 0)
                                    {

                                        if (LegDepTansport.PreflightTransportID == 0)
                                            LegDepTansport.State = TripEntityState.Added;
                                        else
                                            LegDepTansport.State = TripEntityState.Modified;

                                        LegDepTansport.PreflightTransportID = transVM.PreflightDepartTransportId;
                                        LegDepTansport.Transport.TransportID = transVM.DepartTransportId;
                                        LegDepTansport.Transport.TransportCD = transVM.DepartCode;
                                        LegDepTansport.PreflightTransportName = transVM.DepartName;
                                        LegDepTansport.PhoneNum4 = transVM.DepartRate;
                                        LegDepTansport.PhoneNum1 = transVM.DepartPhone;
                                        LegDepTansport.FaxNUM = transVM.DepartFax;
                                        LegDepTansport.Comments = transVM.DepartComments;
                                        LegDepTansport.ConfirmationStatus = transVM.DepartConfirmation;
                                        LegDepTansport.Status = transVM.SelectedDepartTransportStatus;
                                    }
                                    if (transVM.ArrivalTransportId != 0)
                                    {
                                        if (LegArrTansport.PreflightTransportID == 0)
                                            LegArrTansport.State = TripEntityState.Added;
                                        else
                                            LegArrTansport.State = TripEntityState.Modified;

                                        LegArrTansport.PreflightTransportID = transVM.PreflightArrivalTransportId;
                                        LegArrTansport.Transport.TransportID = transVM.ArrivalTransportId;
                                        LegArrTansport.Transport.TransportCD = transVM.ArrivalCode;
                                        LegArrTansport.PreflightTransportName = transVM.ArrivalName;
                                        LegArrTansport.PhoneNum4 = transVM.ArrivalRate;
                                        LegArrTansport.PhoneNum1 = transVM.ArrivalPhone;
                                        LegArrTansport.FaxNUM = transVM.ArrivalFax;
                                        LegArrTansport.Comments = transVM.ArrivalComments;
                                        LegArrTansport.ConfirmationStatus = transVM.ArrivalConfirmation;
                                        LegArrTansport.Status = transVM.SelectedArrivalTransportStatus;
                                    }
                                }
                                if(arrivalNew)
                                {
                                    leg.PreflightTransportLists.Add(LegArrTansport);
                                }
                                if(departureNew)
                                {
                                    leg.PreflightTransportLists.Add(LegDepTansport);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
                return Trip;
            }
        }

        private static Dictionary<string, List<PreflightLegCrewHotelViewModel>> SetPreflightLegCrewHotelList(PreflightMain Trip)
        {
            Dictionary<string, List<PreflightLegCrewHotelViewModel>> legsCrewHotelsViewModel = new Dictionary<string, List<PreflightLegCrewHotelViewModel>>();
            if (Trip == null)
            {
                return legsCrewHotelsViewModel;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        foreach (PreflightLeg leg in Trip.GetLiveLegs())
                        {
                            List<PreflightLegCrewHotelViewModel> legCrewHotelViewModel = new List<PreflightLegCrewHotelViewModel>();
                            foreach (PreflightHotelList crewHotel in leg.PreflightHotelLists.Where(x => x.IsDeleted == false && x.crewPassengerType == "C"))
                            {
                                PreflightLegCrewHotelViewModel crewHotelViewModel = new PreflightLegCrewHotelViewModel();
                                crewHotelViewModel = (PreflightLegCrewHotelViewModel)crewHotelViewModel.InjectFrom(crewHotel);
                                crewHotelViewModel.LegNUM = leg.LegNUM.Value;
                                if(crewHotel.isDepartureHotel == null)
                                {
                                    crewHotelViewModel.isDepartureHotel = !(crewHotel.isArrivalHotel.HasValue && crewHotel.isArrivalHotel.Value);
                                } 
                                else if(crewHotel.isArrivalHotel == null)
                                {
                                    crewHotelViewModel.isArrivalHotel = !(crewHotel.isDepartureHotel.HasValue && crewHotel.isDepartureHotel.Value);
                                }
                                legCrewHotelViewModel.Add(crewHotelViewModel);
                            }

                            legsCrewHotelsViewModel[leg.LegNUM.ToString()] = legCrewHotelViewModel;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
                return legsCrewHotelsViewModel;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> SavePreflightTrip()
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;
              try
            {
            UserPrincipalViewModel upViewModel = getUserPrincipal();
            Dictionary<string, object> RetObj = new Dictionary<string, object>();
            PreflightMain Trip = new PreflightMain();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if(HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null) {
                        using (PreflightServiceClient Service = new PreflightServiceClient())
                        {
                            PreflightMain PreflightMainUnChanged = (PreflightMain) HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip];
                            PreflightTripViewModel tripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

                            string Msg = string.Empty;
                            bool IsSaved = false;
                            bool IsValidTrip = true;
                            tripVM.PreflightMain.AllowTripToSave = true;

                            if (tripVM.PreflightLegs != null && tripVM.PreflightLegs.Count() > 0)
                            {
                                Trip.PreflightLegs = InjectPreflightLegVMToPreflightLeg(tripVM.PreflightLegs);
                            }

                            Trip.PreviousNUM = 0;
                            Trip = (PreflightMain) Trip.InjectFrom(tripVM.PreflightMain);
                            if (Trip.RecordType == null) Trip.RecordType = "T";

                            if (Trip.TripID > 0)
                            {
                                //Validate the Trip we are sending to update or delete is the same trip we search to prevent any unknown trip edit or delete
                                if (PreflightMainUnChanged != null && Trip.TripID != PreflightMainUnChanged.TripID)
                                {
                                    IsSaved = false;
                                    IsValidTrip = false;
                                    RetObj.Add("Trip", tripVM);
                                    RetObj.Add("IsSaved", IsSaved);
                                    RetObj.Add("Msg", "Invalid Trip, Trip is not Saved");
                                }
                                else
                                {
                                    Trip.State = TripEntityState.Modified;
                                }
                            }
                            else
                            {
                                Trip.State = TripEntityState.Added;
                            }

                            if (IsValidTrip)
                            {
                                if (upViewModel._IsAutoRevisionNum)
                                {
                                    Trip.RevisionNUM = Trip.State == TripEntityState.Added ? 1 : (Trip.RevisionNUM ?? 0) + 1;
                                    tripVM.PreflightMain.RevisionNUM = Trip.RevisionNUM;
                                }
                                Trip.LastUpdUID = upViewModel._name;
                                Trip.LastUpdTS = DateTime.UtcNow;
                                if (Trip.State == TripEntityState.Added)
                                {
                                    Trip.IsTripCopied = false;
                                }
                                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                                {
                                    foreach (string key in tripVM.PreflightLegCrews.Keys)
                                    {
                                        List<PreflightCrewList> PreflightLegCrew = new List<PreflightCrewList>();
                                        PreflightCrewList obj = null;
                                        foreach (PreflightLegCrewViewModel item in tripVM.PreflightLegCrews[key])
                                        {
                                            if ((item.PreflightCrewListID == 0 && item.IsDeleted == false) || item.PreflightCrewListID > 0)
                                            {
                                                obj = (PreflightCrewList)new PreflightCrewList().InjectFrom(item);
                                                if (obj.DutyTYPE != "")
                                                {
                                                    PreflightLegCrew.Add(obj);
                                                }
                                            }
                                        }
                                        PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key) && l.IsDeleted == false).FirstOrDefault();
                                        if (leg != null && obj!=null)
                                            leg.PreflightCrewLists = PreflightLegCrew;
                                    }
                                    foreach (string key in tripVM.PreflightLegCrewLogisticsHotelList.Keys)
                                    {
                                        List<PreflightHotelList> PreflightCrewHotel = new List<PreflightHotelList>();
                                        foreach (PreflightLegCrewHotelViewModel item in tripVM.PreflightLegCrewLogisticsHotelList[key])
                                        {
                                            PreflightHotelList obj = (PreflightHotelList) new PreflightHotelList().InjectFrom(item);
                                            PreflightCrewHotel.Add(obj);
                                        }
                                        PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key) && l.IsDeleted == false).FirstOrDefault();
                                        if (leg != null)
                                            leg.PreflightHotelLists = PreflightCrewHotel;
                                    }
                                    foreach (string key in tripVM.PreflightLegCrewTransportationList.Keys)
                                    {
                                        PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key) && l.IsDeleted == false).FirstOrDefault();
                                        if (leg != null)
                                        {
                                            List<PreflightTransportList> transportation = new List<PreflightTransportList>();
                                            PreflightTransportList depTransport = new PreflightTransportList();
                                            depTransport.LegID = leg.LegID;

                                            PreflightTransportList arrTransport = new PreflightTransportList();
                                            arrTransport.LegID = leg.LegID;

                                            SetAPreflightTransportation(ref depTransport, ref arrTransport, tripVM.PreflightLegCrewTransportationList[key], "C");
                                            if (depTransport.TransportID.HasValue && depTransport.TransportID.Value > 0)
                                                transportation.Add(depTransport);
                                            if (arrTransport.TransportID.HasValue && arrTransport.TransportID.Value > 0)
                                                transportation.Add(arrTransport);
                                            leg.PreflightTransportLists = transportation;
                                        }
                                    }
                                    foreach (PreflightLegViewModel leg in tripVM.PreflightLegs.Where(x => x.IsDeleted == false))
                                    {
                                        if (leg.PreflightTripOutbounds != null && leg.PreflightTripOutbounds.Count > 0)
                                        {
                                            List<PreflightTripOutbound> OutboundList = new List<PreflightTripOutbound>();

                                            foreach (PreflightTripOutboundViewModel item in leg.PreflightTripOutbounds)
                                            {
                                                PreflightTripOutbound obj = (PreflightTripOutbound)new PreflightTripOutbound().InjectFrom(item);
                                                OutboundList.Add(obj);
                                            }
                                            PreflightLeg legObject = Trip.PreflightLegs.Where(l => l.LegNUM == leg.LegNUM && l.IsDeleted == false).FirstOrDefault();
                                            if (legObject != null)
                                                legObject.PreflightTripOutbounds = OutboundList;
                                        }
                                    }
                                    if (tripVM.PreflightLegPreflightLogisticsList != null && tripVM.PreflightLegPreflightLogisticsList.Count > 0)
                                    {
                                        foreach (string key in tripVM.PreflightLegPreflightLogisticsList.Keys)
                                        {
                                            List<PreflightFBOList> FBOList = new List<PreflightFBOList>();
                                            List<PreflightCateringDetail> CateringList = new List<PreflightCateringDetail>();

                                            PreflightFBOList DepFBO = (PreflightFBOList) new PreflightFBOList().InjectFrom(tripVM.PreflightLegPreflightLogisticsList[key].PreflightDepFboListViewModel);
                                            DepFBO.IsDepartureFBO = true; DepFBO.IsArrivalFBO = false;

                                            PreflightFBOList ArrFBO = (PreflightFBOList) new PreflightFBOList().InjectFrom(tripVM.PreflightLegPreflightLogisticsList[key].PreflightArrFboListViewModel);
                                            ArrFBO.IsDepartureFBO = false; ArrFBO.IsArrivalFBO = true;

                                            if (DepFBO.PreflightFBOID > 0)
                                            {
                                                DepFBO.State = TripEntityState.Modified;
                                            }
                                            else
                                            {
                                                DepFBO.State = TripEntityState.Added;
                                            }
                                            if (ArrFBO.PreflightFBOID > 0)
                                            {
                                                ArrFBO.State = TripEntityState.Modified;
                                            }
                                            else
                                            {
                                                ArrFBO.State = TripEntityState.Added;
                                            }

                                            PreflightCateringDetail DepCate = (PreflightCateringDetail) new PreflightCateringDetail().InjectFrom(tripVM.PreflightLegPreflightLogisticsList[key].DepartLogisticsCatering);
                                            PreflightCateringDetail ArrCate = (PreflightCateringDetail) new PreflightCateringDetail().InjectFrom(tripVM.PreflightLegPreflightLogisticsList[key].ArrivalLogisticsCatering);
                                            DepCate.ArriveDepart = "D";
                                            ArrCate.ArriveDepart = "A";

                                            if (DepCate.PreflightCateringID > 0)
                                            {
                                                DepCate.State = TripEntityState.Modified;
                                            }
                                            else
                                            {
                                                DepCate.State = TripEntityState.Added;
                                            }
                                            if (ArrCate.PreflightCateringID > 0)
                                            {
                                                ArrCate.State = TripEntityState.Modified;
                                            }
                                            else
                                            {
                                                ArrCate.State = TripEntityState.Added;
                                            }


                                            if (DepFBO.FBOID > 0 || DepFBO.State== TripEntityState.Added) FBOList.Add(DepFBO);
                                            else if (DepFBO.State == TripEntityState.Modified && DepFBO.PreflightFBOID>0)
                                            {
                                                FBOList.Add(DepFBO);
                                            }
                                            if (ArrFBO.FBOID > 0 || ArrFBO.State == TripEntityState.Added) FBOList.Add(ArrFBO);
                                            else if (ArrFBO.State == TripEntityState.Modified && ArrFBO.PreflightFBOID > 0)
                                            {
                                                FBOList.Add(ArrFBO);
                                            }

                                            if (DepCate.CateringID > 0 || DepCate.State== TripEntityState.Added) CateringList.Add(DepCate);
                                            else if (DepCate.State == TripEntityState.Modified && DepCate.PreflightCateringID > 0)
                                            {
                                                CateringList.Add(DepCate);
                                            }
                                            if (ArrCate.CateringID > 0 || ArrCate.State== TripEntityState.Added) CateringList.Add(ArrCate);
                                            else if (ArrCate.State == TripEntityState.Modified && ArrCate.PreflightCateringID > 0)
                                            {
                                                CateringList.Add(ArrCate);
                                            }


                                            PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key)  && l.IsDeleted == false).FirstOrDefault();
                                            if (leg != null)
                                            {
                                                leg.PreflightFBOLists = FBOList;
                                                leg.PreflightCateringDetails = CateringList;
                                            }
                                        }
                                    }
                                    
                                    foreach (string key in tripVM.PreflightLegPassengers.Keys)
                                    {
                                        PreflightLegViewModel legVM =  tripVM.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key) && l.IsDeleted == false).FirstOrDefault();
                                        List<PreflightPassengerList> PreflightLegPax = new List<PreflightPassengerList>();
                                        foreach (PassengerViewModel item in tripVM.PreflightLegPassengers[key])
                                        {
                                                PreflightPassengerList obj = (PreflightPassengerList)new PreflightPassengerList().InjectFrom(item);
                                                if (obj.FlightPurposeID == 0 )
                                                {
                                                    obj.FlightPurposeID = null;
                                                }
                                                PreflightLegPax.Add(obj);                                            
                                        }
                                        PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key) && l.IsDeleted == false).FirstOrDefault();
                                        if (leg != null)
                                            leg.PreflightPassengerLists = PreflightLegPax;
                                    }

                                    // Saving Pax Transport Viewmodel to Database-Main Trip(EDMX)
                                    Trip = SetPreflightLegPaxTransportationVMToList(Trip, tripVM.PreflightLegPaxTransportationList);

                                    foreach (string key in tripVM.PreflightLegPassengersLogisticsHotelList.Keys)
                                    {
                                        List<PreflightHotelList> PreflightPaxHotel = new List<PreflightHotelList>();
                                        foreach (PreflightLegPassengerHotelViewModel item in tripVM.PreflightLegPassengersLogisticsHotelList[key])
                                        {
                                            PreflightHotelList obj = (PreflightHotelList) new PreflightHotelList().InjectFrom(item);
                                            PreflightPaxHotel.Add(obj);
                                            obj.PAXIDList = new List<long?>();
                                            if (item.IsAllCrewOrPax == true || item.PAXCode.ToLower().Equals("all"))
                                            {
                                                var codes = item.PAXCode.Split(',');
                                                List<long?> ids = tripVM.PreflightLegPassengers[key].Select(y => y.PassengerID).ToList();
                                                obj.PAXIDList.AddRange(ids);
                                            }
                                            else if (item.NoPAX == false && item.PAXCode.Length > 0)
                                            {
                                                var codes = item.PAXCode.Split(',');
                                                List<long?> ids = tripVM.PreflightLegPassengers[key].Where(p => codes.Contains(p.PassengerRequestorCD)).Select(y => y.PassengerID).ToList();
                                                obj.PAXIDList.AddRange(ids);
                                            }
                                        }
                                        PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(key)  && l.IsDeleted == false).FirstOrDefault();
                                        if (leg != null)
                                        {
                                            if (leg.PreflightHotelLists == null)
                                                Trip.PreflightLegs[Convert.ToInt32(key) - 1].PreflightHotelLists = new List<PreflightHotelList>();

                                            leg.PreflightHotelLists.AddRange(PreflightPaxHotel);
                                        }
                                    }
                                }

                                bool isMandatoryEx = ValidateTripForMandatoryExceptions(Trip);
                                if (isMandatoryEx == false)
                                {
                                    var ReturnValue = Service.Add(Trip);
                                    Trip = ReturnValue.EntityInfo;
                                    Service.UpdateCRFromPreflight(Trip);
                                    if (ReturnValue.ReturnFlag)
                                    {
                                        IsSaved = ReturnValue.ReturnFlag;
                                        HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = ReturnValue.EntityList[0];
                                        tripVM = InjectPreflightMainToPreflightTripViewModel(ReturnValue.EntityList[0], tripVM);
                                        tripVM.PreflightMain.EditMode = false;
                                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = tripVM;
                                        Msg = "Trip has been saved successfully";
                                    }
                                    else
                                    {
                                        Msg = "Trip has mandatory exceptions, Trip is not Saved";
                                    }
                                }
                                else
                                {
                                    Msg = "Trip has mandatory exceptions, Trip is not Saved";
                                    IsSaved = false;
                                }
                                RetObj.Add("Trip", tripVM);
                                RetObj.Add("IsSaved", IsSaved);
                                RetObj.Add("Msg", Msg);
                            }
                        }
                    }
              }, FlightPak.Common.Constants.Policy.UILayer);
            }
            result.Result = RetObj;
            }
              catch (Exception ex)
              {
                  if (CheckPrincipalEmpty(ex))
                  {
                      result.Success = false;
                      result.StatusCode = HttpStatusCode.Unauthorized;
                  }
              }
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightTripViewModel> DeleteTrip()
        {
            FSSOperationResult<PreflightTripViewModel> result = new FSSOperationResult<PreflightTripViewModel>();
            if (!result.IsAuthorized())
                return result;

            if (!MiscUtils.IsAuthorized(Permission.Preflight.DeletePreflightLeg))
            {
                result.Success = false;
                result.StatusCode = HttpStatusCode.MethodNotAllowed;
                return result;
            }

            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                PreflightMain PreflightMainUnChanged = (PreflightMain)HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip];

                //Validate the Trip we are sending to update or delete is the same trip we search to prevent any unknown trip edit or delete
                if (pfViewModel.TripId.HasValue && PreflightMainUnChanged != null && pfViewModel.TripId.Value == PreflightMainUnChanged.TripID)
                {
                    using (PreflightServiceClient Service = new PreflightServiceClient())
                    {
                        var ReturnValue = Service.Delete(pfViewModel.TripId.Value);
                        if (ReturnValue.ReturnFlag)
                        {
                            pfViewModel.TripId = 0;
                            pfViewModel.PreflightMain = new PreflightMainViewModel();
                            pfViewModel.PreflightMain = CreateOrCancelTrip(pfViewModel.PreflightMain);
                            pfViewModel.PreflightMain.EditMode = false;
                            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = null;
                            HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = null;
                        }
                    }
                }
            }
            result.Result = pfViewModel;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> TripException(PreflightMainViewModel trip)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            List<PreflightTripException> ExceptionList = new List<PreflightTripException>();
            Hashtable RetObj = new Hashtable();
            ExceptionList = ValidateTrip(trip);
            if (ExceptionList.Count > 0)
            {
                List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                {
                    ExceptionTemplateList = GetExceptionTemplateList();
                    var obj = (from Exp in ExceptionList
                               join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                               select new
                               {
                                   Severity = ExpTempl.Severity,
                                   ExceptionDescription = Exp.ExceptionDescription,
                                   SubModuleID = ExpTempl.SubModuleID,
                                   SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                   DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                               }).ToList();
                    RetObj["Flag"] = "1";
                    RetObj["ExceptionList"] = obj.OrderBy(e=>e.DisplayOrder);
                }
            }
            result.Result = RetObj;
            return result;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool>  UpdateAllLegsCalculation()
        {
            FSSOperationResult<bool> result = new FSSOperationResult<bool>();
            if (!result.IsAuthorized())
                return result;

            var TripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            var liveLegs = TripVM.PreflightLegs.Where(l=>l.IsDeleted==false && l.State != TripEntityState.Deleted).OrderBy(o=>o.LegNUM).ToList();
            var userPrinciple = getUserPrincipal();

            HomebaseAirportDetails hadObj = new HomebaseAirportDetails();
            var homebaseObject = userPrinciple._homeBaseId.HasValue ? DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(userPrinciple._homeBaseId.Value) : DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseCD(userPrinciple._homeBase);

            if (homebaseObject != null)
            {
                hadObj.HomebaseCD = homebaseObject.HomebaseCD;
                hadObj.homeIsDayLightSaving = homebaseObject.HomebaseAirport.IsDayLightSaving;
                hadObj.homeDayLightSavingStartDT = homebaseObject.HomebaseAirport.DayLightSavingStartDT;
                hadObj.homeDayLightSavingEndDT = homebaseObject.HomebaseAirport.DayLightSavingEndDT;
                hadObj.homeDaylLightSavingStartTM = homebaseObject.HomebaseAirport.DaylLightSavingStartTM;
                hadObj.homeDayLightSavingEndTM = homebaseObject.HomebaseAirport.DayLightSavingEndTM;
                hadObj.homeOffsetToGMT = homebaseObject.HomebaseAirport.OffsetToGMT;
                hadObj.HomebaseId = homebaseObject.HomebaseAirport.AirportID.ToString();
                hadObj.HomebaseCD = homebaseObject.HomebaseAirport.IcaoID;
            }

            AircraftDetails aircraftDetailObj = new AircraftDetails();
            if (TripVM.PreflightMain.AircraftID.HasValue)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objPowerSetting = objDstsvc.GetAircraftWithFilters(string.Empty, TripVM.PreflightMain.AircraftID.Value, false);
                    if (objPowerSetting.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                        {
                            var aircraftObj = AircraftList[0];
                            if (aircraftObj != null)
                            {
                                aircraftDetailObj.AirCraftId = aircraftObj.AircraftID;
                                aircraftDetailObj.PowerSettings1TakeOffBias = Convert.ToDouble(aircraftObj.PowerSettings1TakeOffBias);
                                aircraftDetailObj.PowerSettings1LandingBias = Convert.ToDouble(aircraftObj.PowerSettings1LandingBias);
                                aircraftDetailObj.PowerSettings1TrueAirSpeed = Convert.ToDouble(aircraftObj.PowerSettings1TrueAirSpeed);
                                aircraftDetailObj.PowerSettings2TakeOffBias = Convert.ToDouble(aircraftObj.PowerSettings2TakeOffBias);
                                aircraftDetailObj.PowerSettings2LandingBias = Convert.ToDouble(aircraftObj.PowerSettings2LandingBias);
                                aircraftDetailObj.PowerSettings2TrueAirSpeed = Convert.ToDouble(aircraftObj.PowerSettings2TrueAirSpeed);
                                aircraftDetailObj.PowerSettings3TakeOffBias = Convert.ToDouble(aircraftObj.PowerSettings3TakeOffBias);
                                aircraftDetailObj.PowerSettings3LandingBias = Convert.ToDouble(aircraftObj.PowerSettings3LandingBias);
                                aircraftDetailObj.PowerSettings3TrueAirSpeed = Convert.ToDouble(aircraftObj.PowerSettings3TrueAirSpeed);
                                aircraftDetailObj.IsFixedRotary = aircraftObj.IsFixedRotary;
                                aircraftDetailObj.aircraftWindAltitude = Convert.ToDouble(aircraftObj.WindAltitude);
                                aircraftDetailObj.ChargeRate = Convert.ToDouble(aircraftObj.ChargeRate);
                                aircraftDetailObj.ChargeUnit = aircraftObj.ChargeUnit;

                            }
                        }
                    }
                }
            }
            PreflightLegsLite preflightLegLiteObj = new PreflightLegsLite();
            foreach (var legItem in liveLegs)
            {
                DepartAirportDetails departAirportDetObj = new DepartAirportDetails();

                if (legItem.DepartureAirport != null)
                {
                    departAirportDetObj.deplatdeg = Convert.ToDouble(legItem.DepartureAirport.LatitudeDegree == null ? 0 : legItem.DepartureAirport.LatitudeDegree);
                    departAirportDetObj.deplatmin = Convert.ToDouble(legItem.DepartureAirport.LatitudeMinutes == null ? 0 : legItem.DepartureAirport.LatitudeMinutes);
                    departAirportDetObj.lcdeplatdir = string.IsNullOrWhiteSpace(legItem.DepartureAirport.LatitudeNorthSouth) ? "" : legItem.DepartureAirport.LatitudeNorthSouth;
                    departAirportDetObj.deplngdeg = Convert.ToDouble(legItem.DepartureAirport.LongitudeDegrees == null ? 0 : legItem.DepartureAirport.LongitudeDegrees);
                    departAirportDetObj.deplngmin = Convert.ToDouble(legItem.DepartureAirport.LongitudeMinutes == null ? 0 : legItem.DepartureAirport.LongitudeMinutes);
                    departAirportDetObj.lcdeplngdir = string.IsNullOrWhiteSpace(legItem.DepartureAirport.LongitudeEastWest) ? "" : legItem.DepartureAirport.LongitudeEastWest;
                    departAirportDetObj.depIsDayLightSaving = legItem.DepartureAirport.IsDayLightSaving;
                    departAirportDetObj.depDayLightSavingStartDT = legItem.DepartureAirport.DayLightSavingStartDT;
                    departAirportDetObj.depDayLightSavingEndDT = legItem.DepartureAirport.DayLightSavingEndDT;
                    departAirportDetObj.depDaylLightSavingStartTM = legItem.DepartureAirport.DaylLightSavingStartTM == null ? "" : legItem.DepartureAirport.DaylLightSavingStartTM;
                    departAirportDetObj.depDayLightSavingEndTM = legItem.DepartureAirport.DayLightSavingEndTM == null ? "" : legItem.DepartureAirport.DayLightSavingEndTM;
                    departAirportDetObj.depOffsetToGMT = legItem.DepartureAirport.OffsetToGMT;
                    departAirportDetObj.lnDepartWindZone = Convert.ToInt64(legItem.DepartureAirport.WindZone == null ? 0 : legItem.DepartureAirport.WindZone);
                    departAirportDetObj.DepAirportTakeoffBIAS = Convert.ToDouble((legItem.DepartureAirport.TakeoffBIAS == null) ? 0 : legItem.DepartureAirport.TakeoffBIAS);
                    departAirportDetObj.DepID = Convert.ToString(legItem.DepartureAirport.AirportID == 0 || legItem.DepartureAirport.AirportID == null ? 0 : legItem.DepartureAirport.AirportID);
                    departAirportDetObj.DepCD = legItem.DepartureAirport.IcaoID;
                }
                ArrivalAirportDetails arriveAirportDetObj = new ArrivalAirportDetails();
                if (legItem.ArrivalAirport != null)
                {
                    arriveAirportDetObj.arrlatdeg = Convert.ToDouble(legItem.ArrivalAirport.LatitudeDegree == null ? 0 : legItem.ArrivalAirport.LatitudeDegree);
                    arriveAirportDetObj.arrlatmin = Convert.ToDouble(legItem.ArrivalAirport.LatitudeMinutes == null ? 0 : legItem.ArrivalAirport.LatitudeMinutes);
                    arriveAirportDetObj.lcArrLatDir = string.IsNullOrWhiteSpace(legItem.ArrivalAirport.LatitudeNorthSouth) ? "N" : legItem.ArrivalAirport.LatitudeNorthSouth;
                    arriveAirportDetObj.arrlngdeg = Convert.ToDouble(legItem.ArrivalAirport.LongitudeDegrees == null ? 0 : legItem.ArrivalAirport.LongitudeDegrees);
                    arriveAirportDetObj.arrlngmin = Convert.ToDouble(legItem.ArrivalAirport.LongitudeMinutes == null ? 0 : legItem.ArrivalAirport.LongitudeMinutes);
                    arriveAirportDetObj.lcarrlngdir = string.IsNullOrWhiteSpace(legItem.ArrivalAirport.LongitudeEastWest) ? "E" : legItem.ArrivalAirport.LongitudeEastWest;
                    arriveAirportDetObj.arrIsDayLightSaving = legItem.ArrivalAirport.IsDayLightSaving;
                    arriveAirportDetObj.arrDayLightSavingStartDT = legItem.ArrivalAirport.DayLightSavingStartDT;
                    arriveAirportDetObj.arrDayLightSavingEndDT = legItem.ArrivalAirport.DayLightSavingEndDT;
                    arriveAirportDetObj.arrDaylLightSavingStartTM = legItem.ArrivalAirport.DaylLightSavingStartTM == null ? "" : legItem.ArrivalAirport.DaylLightSavingStartTM;
                    arriveAirportDetObj.arrDayLightSavingEndTM = legItem.ArrivalAirport.DayLightSavingEndTM == null ? "" : legItem.ArrivalAirport.DayLightSavingEndTM;
                    arriveAirportDetObj.arrOffsetToGMT = legItem.ArrivalAirport.OffsetToGMT;
                    arriveAirportDetObj.lnArrivWindZone = Convert.ToInt64((legItem.ArrivalAirport.WindZone == null) ? 0 : legItem.ArrivalAirport.WindZone);
                    arriveAirportDetObj.ArrAirportLandingBIAS = Convert.ToDouble((legItem.ArrivalAirport.LandingBIAS == null) ? 0 : legItem.ArrivalAirport.LandingBIAS);
                    arriveAirportDetObj.ArrivalID = Convert.ToString(legItem.ArrivalAirport.AirportID);
                    arriveAirportDetObj.ArrivalCD = legItem.ArrivalAirport.IcaoID;
                }

                aircraftDetailObj.Distance = Convert.ToDecimal(string.IsNullOrWhiteSpace(legItem.StrConvertedDistance) ? "0" : legItem.StrConvertedDistance);
                aircraftDetailObj.Bias = legItem.StrTakeoffBIAS;
                aircraftDetailObj.LandingBias = legItem.StrLandingBias;
                aircraftDetailObj.TAS = Convert.ToString(legItem.TrueAirSpeed);
                aircraftDetailObj.ETE = legItem.StrElapseTM;
                aircraftDetailObj.Wind = Convert.ToDouble(legItem.WindsBoeingTable);
               
                LegDateTime legdateObj = new LegDateTime();
                legdateObj.DepLocalDate = legItem.DepartureLocalDate;
                legdateObj.DepLocalTime = legItem.DepartureLocalTime;
                legdateObj.DepUtcDate = legItem.DepartureGreenwichDate;
                legdateObj.DepUtcTime = legItem.DepartureGreenwichTime;
                legdateObj.DepHomeDate = legItem.HomeDepartureDate;
                legdateObj.DepHomeTime = legItem.HomeDepartureLocal;
                legdateObj.ArrLocalDate = legItem.ArrivalLocalDate;
                legdateObj.ArrLocalTime = legItem.ArrivalLocalTime;
                legdateObj.ArrUtcDate = legItem.ArrivalGreenwichDate;
                legdateObj.ArrUtcTime = legItem.ArrivalGreenwichTime;
                legdateObj.ArrHomeDate = legItem.HomeArrivalDate;
                legdateObj.ArrHomeTime = legItem.HomeArrivalTime;
                legdateObj.Changed = "";
                bool IsAutomaticCalcLegTimes = userPrinciple._IsAutomaticCalcLegTimes;

                bool dutyEnd=legItem.IsDutyEnd.HasValue ? legItem.IsDutyEnd.Value :false;
                PreFlightValidator.PreflightCalculation_Validate_Retrieve(departAirportDetObj, arriveAirportDetObj, hadObj, aircraftDetailObj, legdateObj,
                    (userPrinciple._TimeDisplayTenMin??0), userPrinciple._IsKilometer,
                    Convert.ToInt32(string.IsNullOrWhiteSpace(legItem.StrWindReliability) ? "0" : legItem.StrWindReliability), legItem.DepartureLocalDate, userPrinciple._ApplicationDateFormat,
                    legItem.PowerSetting, userPrinciple._ElapseTMRounding??0, legItem.LegNUM.Value.ToString(),
                    legItem.CrewDutyRulesID, legItem.OverrideValue, dutyEnd, (legItem.IsDepartureConfirmed.HasValue ? legItem.IsDepartureConfirmed.Value:false),
                    Convert.ToDecimal(string.IsNullOrWhiteSpace(legItem.StrConvertedDistance) ? "0" : legItem.StrConvertedDistance), "Airport", IsAutomaticCalcLegTimes);

            }


            result.Success = true;
            return result;

        }


        #endregion 

        #region PREFLIGHT MAIN
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePreflightMain(PreflightMainViewModel pfMain)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            if(pfMain != null && HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfMain.IsPrivate = pfMain.IsPrivate ?? false;
                PreflightTripViewModel pfViewModel = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                pfMain.LastUpdTS = pfViewModel.PreflightMain.LastUpdTS;
                if (pfMain.DispatcherUserName == "")
                    pfMain.DispatcherUserName = null;
                if (Convert.ToString(pfViewModel.PreflightMain.FlightNUM) != Convert.ToString(pfMain.FlightNUM))
                {
                    foreach (var LegsItem in pfViewModel.PreflightLegs)
                    {
                        LegsItem.FlightNUM = string.IsNullOrWhiteSpace(pfMain.FlightNUM) ? null : pfMain.FlightNUM;
                    }
                }

                pfViewModel.PreflightMain = pfMain;
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
            }
            result.Result = true;
            return result;
        }
        #endregion

        #region LEG TAB
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]        
        public static FSSOperationResult<Hashtable> InitializePreflghtLegs(int selectedLegNum = 0)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            Hashtable RetObj = new Hashtable();
            List<PreflightLegViewModel> Legs = new List<PreflightLegViewModel>();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (pfViewModel.PreflightLegs != null && pfViewModel.PreflightLegs.Count > 0 && pfViewModel.PreflightLegs[0].DepartureAirport !=null )
                {
                    Legs = pfViewModel.PreflightLegs.Where(l => l.IsDeleted==false).OrderBy(o => o.LegNUM).ToList();
                    if(pfViewModel.TripId == 0)
                    {
                        foreach(var LegsItem in Legs)
                        {
                            if(string.IsNullOrWhiteSpace(LegsItem.FlightNUM))
                                LegsItem.FlightNUM = pfViewModel.PreflightMain.FlightNUM;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(Legs[0].FlightNUM))
                            Legs[0].FlightNUM = pfViewModel.PreflightMain.FlightNUM;
                    }
                    RetObj["Leg"] = Legs;

                    //if the current selected leg number is not passed then assign the pfViewModel.CurrentLegNUM as the current leg
                    if (selectedLegNum < 1)
                        selectedLegNum = Convert.ToInt32(pfViewModel.CurrentLegNUM);

                    var currentLeg = Legs.FirstOrDefault(l => l.LegNUM == selectedLegNum && l.IsDeleted == false && l.State != TripEntityState.Deleted);
                    RetObj["CurrentLeg"] = currentLeg;
                    RetObj["IsHaveLeg"] = true;
                    RetObj["EditMode"] = pfViewModel.PreflightMain.EditMode;
                }
                else
                {
                    Legs.Add(CreateFirstLeg(pfViewModel.PreflightMain));
                    Legs[0].StrWindReliability = UserPrincipal._WindReliability == null ? "3" : Convert.ToString(UserPrincipal._WindReliability);
                    RetObj["CurrentLeg"] = Legs[0];
                    RetObj["Leg"] = Legs;
                    RetObj["IsHaveLeg"] = false;
                    pfViewModel.PreflightLegs = Legs;
                    RetObj["EditMode"] = pfViewModel.PreflightMain.EditMode;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SaveCurrentLegData(int LegNum, PreflightLegViewModel CurrentLeg)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            try
            {
                PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    PreflightLegViewModel objPreflightLegViewModel = pfViewModel.PreflightLegs.FirstOrDefault(p => p.LegNUM == LegNum && p.IsDeleted == false);
                    CurrentLeg.PreflightTripOutbounds = objPreflightLegViewModel.PreflightTripOutbounds;
                    CurrentLeg.PreflightChecklist = objPreflightLegViewModel.PreflightChecklist;
                    MapOutBoundInstruction(CurrentLeg, objPreflightLegViewModel, true);
                    PreflightTripManagerLite mLite = new PreflightTripManagerLite();
                    CurrentLeg = mLite.SetDefaultPreflightLegChildObjects(CurrentLeg);
                    pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)] = CurrentLeg;
                    pfViewModel.PreflightLegPreflightLogisticsList = GetAFreshLogisticsList(pfViewModel.PreflightLegs, pfViewModel.PreflightLegPreflightLogisticsList);
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                }
                result.Result = true;
            }
            catch (Exception ex)
            {
                if (CheckPrincipalEmpty(ex))
                {
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            return result;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> UpdateLegTimes_ForHomebase(String HomeBaseCD)
        {
            FSSOperationResult<bool> result = new FSSOperationResult<bool>();
            if (!result.IsAuthorized())
                return result;
            UserPrincipalViewModel userPrincipal = getUserPrincipal();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    HomebaseViewModel homebaseVM = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseCD(HomeBaseCD);
                    AirportViewModel hbVM = homebaseVM.HomebaseAirport;
                    if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                    {
                        var pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        foreach (PreflightLegViewModel legVM in pfViewModel.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                        {
                            Dictionary<string, string> RetObj = new Dictionary<string, string>();
                            FlightPak.Web.Views.Transactions.Preflight.PreflightUtils PreUtilities = new FlightPak.Web.Views.Transactions.Preflight.PreflightUtils();
                            RetObj = PreUtilities.CalculateDateTime(true, legVM.DepartureAirport.IsDayLightSaving, legVM.DepartureAirport.DayLightSavingStartDT,
                                legVM.DepartureAirport.DayLightSavingEndDT, legVM.DepartureAirport.DaylLightSavingStartTM, legVM.DepartureAirport.DayLightSavingEndTM, legVM.DepartureAirport.OffsetToGMT,
                                legVM.ArrivalAirport.IsDayLightSaving, legVM.ArrivalAirport.DayLightSavingStartDT, legVM.ArrivalAirport.DayLightSavingEndDT,
                                legVM.ArrivalAirport.DaylLightSavingStartTM, legVM.ArrivalAirport.DayLightSavingEndTM, legVM.ArrivalAirport.OffsetToGMT, hbVM.IsDayLightSaving,
                                hbVM.DayLightSavingStartDT, hbVM.DayLightSavingEndDT, hbVM.DaylLightSavingStartTM, hbVM.DayLightSavingEndTM, hbVM.OffsetToGMT, legVM.DepartureLocalDate, userPrincipal._ApplicationDateFormat, legVM.ArrivalLocalDate,
                                legVM.ArrivalLocalTime, legVM.DepartureAirport.AirportID.ToString(), legVM.ArrivalAirport.AirportID.ToString(), legVM.StrElapseTM, (userPrincipal._TimeDisplayTenMin ?? 1).ToString(),
                                legVM.ArrivalGreenwichTime, legVM.ArrivalGreenwichDate, "", "", legVM.ArrivalGreenwichDate, legVM.ArrivalGreenwichTime, hbVM.AirportID.ToString(), legVM.DepartureLocalTime,
                                legVM.HomeDepartureDate, legVM.HomeDepartureLocal, legVM.HomeArrivalDate, legVM.HomeArrivalTime, legVM.ElapseTM ?? 0, "Local",true);

                            legVM.ArrivalGreenwichDate = RetObj["ArrivalUtcDate"];
                            legVM.ArrivalGreenwichTime = RetObj["ArrivalUtcTime"];
                            legVM.HomeArrivalDate = RetObj["ArrivalHomeDate"];
                            legVM.HomeArrivalTime = RetObj["ArrivalHomeTime"];
                            legVM.ArrivalLocalDate = RetObj["ArrivalLocalDate"];
                            legVM.ArrivalLocalTime = RetObj["ArrivalLocalTime"];

                            legVM.DepartureGreenwichDate = RetObj["DeptUtcDate"];
                            legVM.DepartureGreenwichTime = RetObj["DeptUtcTime"];
                            legVM.HomeDepartureDate = RetObj["DeptHomeDate"];
                            legVM.HomeDepartureLocal = RetObj["DeptHomeTime"];
                            legVM.DepartureLocalDate = RetObj["DeptLocalDate"];
                            legVM.DepartureLocalTime = RetObj["DeptLocalTime"];

                        }
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                    }
                    result.Result = true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

            return result;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegViewModel> SavePrevLegAndReturnCurrentLegData(int LegIndex, PreflightLegViewModel PreviousLegData)
        {
            FSSOperationResult<PreflightLegViewModel> result = new FSSOperationResult<PreflightLegViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightLegViewModel Legs = new PreflightLegViewModel();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                PreflightLegViewModel objPreflightLegViewModel = pfViewModel.PreflightLegs.FirstOrDefault(p => p.LegNUM == PreviousLegData.LegNUM && p.IsDeleted == false);
                MapOutBoundInstruction(PreviousLegData, objPreflightLegViewModel, true);
                PreflightTripManagerLite mLite = new PreflightTripManagerLite();
                PreviousLegData = mLite.SetDefaultPreflightLegChildObjects(PreviousLegData);

                pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)] = PreviousLegData;
                Legs = pfViewModel.PreflightLegs.FirstOrDefault(l => l.LegNUM == LegIndex + 1 && l.IsDeleted == false);
                pfViewModel.PreflightLegPreflightLogisticsList = GetAFreshLogisticsList(pfViewModel.PreflightLegs, pfViewModel.PreflightLegPreflightLogisticsList);
                pfViewModel.CurrentLegNUM = Legs.LegNUM;
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
            }
            result.Result = Legs;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> AuthorizationForLeg()
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable auth=new Hashtable();
            auth["Leg"] = MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightLeg);
            auth["Next"] = MiscUtils.IsAuthorized(Permission.Preflight.ViewTripManager);
            auth["Delete"] = MiscUtils.IsAuthorized(Permission.Preflight.DeleteTripManager);
            auth["Add1"] = auth["Add2"] = auth["Insert1"] = auth["Insert2"] = MiscUtils.IsAuthorized(Permission.Preflight.AddPreflightLeg);
            auth["Delete"] = auth["Delete2"] = MiscUtils.IsAuthorized(Permission.Preflight.DeletePreflightLeg);
            auth["Checklist"] = MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightChecklist);
            auth["Outbound"] = MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightOutboundInstruction);
            auth["LegNotes"] = MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightLegNotes);
            auth["Save"] = auth["Cancel"] = MiscUtils.IsAuthorized(Permission.Preflight.AddPreflightLeg);

            result.Result = auth;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CrewDutyRule_Default_Retrieve(long crewDutyRuleId)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();

            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDutyRule = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetCrewDutyRulesWithFilters(crewDutyRuleId, string.Empty, false);
                if (objRetVal.ReturnFlag)
                {
                    CrewDutyRule = objRetVal.EntityList;
                    if (CrewDutyRule != null && CrewDutyRule.Count > 0)
                    {
                        RetObj["Result"] = "1";
                        RetObj["CrewDutyRule"] = CrewDutyRule;
                    }
                    else
                    {
                        RetObj["Result"] = "0";
                    }
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SetDefaultCategory(long CategoryId)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable RetObj=new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters>();
            using(FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllFlightCategoryWithFilters(0, CategoryId, string.Empty, true);
                if(objRetVal.ReturnFlag)
                {
                    FlightCatagoryLists = objRetVal.EntityList;
                    if(FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                    {
                        RetObj["flag"] = 0;
                        RetObj["Category"] = FlightCatagoryLists[0];
                    }
                    else
                    {
                        RetObj["flag"] = 1;
                    }
                }
                else
                {
                    RetObj["flag"] = 1;
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> GetAAircraft(long aircraftId)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable RetObj=new Hashtable();
            FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
            using(FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objPowerSetting = objDstsvc.GetAircraftWithFilters(string.Empty, aircraftId, false);
                if (objPowerSetting.ReturnFlag)
                {
                    List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                    if(AircraftList != null && AircraftList.Count > 0)
                    {
                        RetObj["Aircraft"] = AircraftList[0];
                        RetObj["flag"] = 0;
                    }
                    else
                    {
                        RetObj["flag"] = 1;
                    }
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> AddNewLeg(int LegNum, PreflightLegViewModel CurrentLeg)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            Hashtable RetObj = new Hashtable();
            PreflightLegViewModel leg = new PreflightLegViewModel();
            if (MiscUtils.IsAuthorized(Permission.Preflight.AddPreflightLeg))
            {
                 PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                if(HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    PreflightLegViewModel objPreflightLegViewModel = pfViewModel.PreflightLegs.FirstOrDefault(p => p.LegNUM == LegNum && p.IsDeleted == false);
                    MapOutBoundInstruction(CurrentLeg, objPreflightLegViewModel, true);
                    PreflightTripManagerLite mLite = new PreflightTripManagerLite();
                    CurrentLeg = mLite.SetDefaultPreflightLegChildObjects(CurrentLeg);

                    pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)] = CurrentLeg;
                    PreflightLegViewModel LastLeg = pfViewModel.PreflightLegs.OrderByDescending(p => p.LegNUM).FirstOrDefault(l => l.IsDeleted == false);
                    leg.LegNUM = GetANewLegNum("ADD");
                    leg.IsPrivate = pfViewModel.PreflightMain.IsPrivate;
                    leg.DepartureAirport = (AirportViewModel)new AirportViewModel().InjectFrom(LastLeg.ArrivalAirport);
                    leg = SetANewLegData(leg, UserPrincipal._IsAutomaticCalcLegTimes);
                    leg.ArrivalAirport = new AirportViewModel();
                    leg.Account = (AccountViewModel)new AccountViewModel().InjectFrom(pfViewModel.PreflightMain.Account);
                    leg.AccountID = pfViewModel.PreflightMain.AccountID;
                    leg.CQCustomer = (CQCustomerViewModel)new CQCustomerViewModel().InjectFrom(pfViewModel.PreflightMain.CQCustomer);
                    leg.CQCustomerID = pfViewModel.PreflightMain.CQCustomerID;
                    leg.Client = (ClientViewModel)new ClientViewModel().InjectFrom(pfViewModel.PreflightMain.Client);
                    leg.ClientID = pfViewModel.PreflightMain.ClientID;
                    leg.Department = (DepartmentViewModel)new DepartmentViewModel().InjectFrom(pfViewModel.PreflightMain.Department);
                    leg.DepartmentID = pfViewModel.PreflightMain.DepartmentID;
                    leg.DepartmentAuthorization = (DepartmentAuthorizationViewModel)new DepartmentAuthorizationViewModel().InjectFrom(pfViewModel.PreflightMain.DepartmentAuthorization);
                    leg.AuthorizationID = pfViewModel.PreflightMain.AuthorizationID;
                    leg.Passenger = (PassengerViewModel)new PassengerViewModel().InjectFrom(pfViewModel.PreflightMain.Passenger);
                    leg = SetADefaultAircraft(leg, pfViewModel.PreflightMain.AircraftID);

                    leg.PassengerRequestorID = pfViewModel.PreflightMain.PassengerRequestorID;
                    leg.IsDeleted = false;
                    leg.State = TripEntityState.Added;
                    leg.FlightPurpose = LastLeg.FlightPurpose;
                    leg.WindReliability = UserPrincipal._WindReliability == null ? 3 : Convert.ToInt16(UserPrincipal._WindReliability);
                    
                    if(string.IsNullOrWhiteSpace(LastLeg.FlightNUM))
                        leg.FlightNUM = String.IsNullOrWhiteSpace(pfViewModel.PreflightMain.FlightNUM) ? "" : pfViewModel.PreflightMain.FlightNUM;
                    else 
                        leg.FlightNUM = LastLeg.FlightNUM;
                    leg.IsDepartureConfirmed = true;
                    leg.IsArrivalConfirmation = false;
                    if (pfViewModel.PreflightMain.Fleet != null)
                    {
                        if (pfViewModel.PreflightMain.Fleet.MaximumPassenger != null && pfViewModel.PreflightMain.Fleet.MaximumPassenger > 0)
                        {
                            leg.SeatTotal = Convert.ToInt32(pfViewModel.PreflightMain.Fleet.MaximumPassenger);
                            leg.ReservationTotal = 0;
                            leg.PassengerTotal = 0;
                            leg.ReservationAvailable = leg.SeatTotal - leg.PassengerTotal;
                            leg.WaitNUM = 0;
                        }
                    }
                    if (UserPrincipal._DefaultFlightCatID != null)
                    {
                        leg.FlightCategoryID = UserPrincipal._DefaultFlightCatID;
                        leg = SetADefaultFlightCategory(leg);
                    }
                    else
                    {
                        leg.FlightCatagory = new FlightCatagoryViewModel();
                    }
                    //CrewDutyRules Setting
                    if (UserPrincipal._CrewDutyID != null)
                    {
                        leg.CrewDutyRulesID = UserPrincipal._CrewDutyID;
                        leg = SetADefaultCrewDutyTypeAndFARRules(leg);
                    }
                    else
                    {
                        if(leg.CrewDutyRulesID != null)
                        {
                            leg.CrewDutyRulesID = pfViewModel.PreflightLegs[LegNum - 1].CrewDutyRulesID;
                            leg = SetADefaultCrewDutyTypeAndFARRules(leg);
                        }
                        else
                        {
                            leg.CrewDutyRule = new CrewDutyRuleViewModel();
                        }
                    }

                    leg.FlightCatagory = LastLeg.FlightCatagory;
                    leg.FlightCategoryID = LastLeg.FlightCategoryID;

                    #region Make sure the previous legs have logistics info
                    pfViewModel.PreflightLegPreflightLogisticsList = GetAFreshLogisticsList(pfViewModel.PreflightLegs, pfViewModel.PreflightLegPreflightLogisticsList);
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                    #endregion Make sure the previous legs have logistics info
                    pfViewModel.PreflightLegs.Add(leg);
                    RetObj["NewLeg"] = leg;
                    RetObj["Legs"] = pfViewModel.PreflightLegs;
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> InsertNewLeg(int CurrentLegNum, PreflightLegViewModel CurrentLeg, bool CopyCurrentLegCrew)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            Hashtable RetObj = new Hashtable();
            PreflightLegViewModel leg = new PreflightLegViewModel();
            if(MiscUtils.IsAuthorized(Permission.Preflight.AddPreflightLeg))
            {
                PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                if(HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

                    #region Make sure the previous legs have logistics info
                    pfViewModel.PreflightLegPreflightLogisticsList = GetAFreshLogisticsList(pfViewModel.PreflightLegs, pfViewModel.PreflightLegPreflightLogisticsList);
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                    #endregion Make sure the previous legs have logistics info

                    PreflightLegViewModel objPreflightLegViewModelCurrentLeg = pfViewModel.PreflightLegs.FirstOrDefault(p => p.LegNUM == CurrentLegNum && p.IsDeleted == false);
                    MapOutBoundInstruction(CurrentLeg, objPreflightLegViewModelCurrentLeg, true);
                    PreflightTripManagerLite mLite = new PreflightTripManagerLite();
                    CurrentLeg = mLite.SetDefaultPreflightLegChildObjects(CurrentLeg);
                    pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModelCurrentLeg)] = CurrentLeg;
                    leg.LegNUM = GetANewLegNum("INSERT", Convert.ToString(CurrentLegNum));
                    leg.IsPrivate = pfViewModel.PreflightMain.IsPrivate;
                    leg = SetANewInsertedLegData(leg, UserPrincipal._IsAutomaticCalcLegTimes);
                    leg.Account = (AccountViewModel)new AccountViewModel().InjectFrom(pfViewModel.PreflightMain.Account);
                    leg.AccountID = pfViewModel.PreflightMain.AccountID;
                    leg.CQCustomer = (CQCustomerViewModel)new CQCustomerViewModel().InjectFrom(pfViewModel.PreflightMain.CQCustomer);
                    leg.CQCustomerID = pfViewModel.PreflightMain.CQCustomerID;
                    leg.Client = (ClientViewModel)new ClientViewModel().InjectFrom(pfViewModel.PreflightMain.Client);
                    leg.ClientID = pfViewModel.PreflightMain.ClientID;
                    leg.Department = (DepartmentViewModel)new DepartmentViewModel().InjectFrom(pfViewModel.PreflightMain.Department);
                    leg.DepartmentID = pfViewModel.PreflightMain.DepartmentID;
                    leg.DepartmentAuthorization = (DepartmentAuthorizationViewModel)new DepartmentAuthorizationViewModel().InjectFrom(pfViewModel.PreflightMain.DepartmentAuthorization);
                    leg.AuthorizationID = pfViewModel.PreflightMain.AuthorizationID;
                    leg.Passenger = (PassengerViewModel)new PassengerViewModel().InjectFrom(pfViewModel.PreflightMain.Passenger);
                    leg.PassengerRequestorID = pfViewModel.PreflightMain.PassengerRequestorID;
                    leg.WindReliability = UserPrincipal._WindReliability == null ? 3 : Convert.ToInt16(UserPrincipal._WindReliability);

                    //CrewDutyRules Setting
                    if (UserPrincipal._CrewDutyID != null)
                    {
                        leg.CrewDutyRulesID = UserPrincipal._CrewDutyID;
                        leg = SetADefaultCrewDutyTypeAndFARRules(leg);
                    }
                    else
                    {
                        if (leg.CrewDutyRulesID != null)
                        {
                            leg.CrewDutyRulesID = pfViewModel.PreflightLegs[Convert.ToInt16(CurrentLegNum) - 1].CrewDutyRulesID;
                            leg = SetADefaultCrewDutyTypeAndFARRules(leg);
                        }
                        else
                        {
                            leg.CrewDutyRule = new CrewDutyRuleViewModel();
                        }
                    }
                    leg.IsDeleted = false;
                    leg.State = TripEntityState.Added;
                   
                    pfViewModel.PreflightLegs.Insert(Convert.ToInt16(CurrentLegNum) - 1, leg);
                    if (CopyCurrentLegCrew && pfViewModel.PreflightLegCrews.Count > 0)
                    {
                        List<PreflightLegCrewViewModel> tmpPreflightLegCrew = new List<PreflightLegCrewViewModel>();
                        foreach (PreflightLegCrewViewModel _preflightLegCrewViewModel in pfViewModel.PreflightLegCrews[Convert.ToString(CurrentLegNum + 1)].Where(f => f.IsDeleted == false).ToList())
                        {
                            PreflightLegCrewViewModel tmp = new PreflightLegCrewViewModel();
                            _preflightLegCrewViewModel.LegNum = Convert.ToString(leg.LegNUM + 1);
                            tmp.InjectFrom(_preflightLegCrewViewModel);
                            tmp.PreflightCrewListID = 0;
                            tmp.LegNum = Convert.ToString(leg.LegNUM);
                            tmp.LegID = leg.LegID;
                            tmp.State = TripEntityState.Added;
                            tmpPreflightLegCrew.Add(tmp);
                        }
                        pfViewModel.PreflightLegCrews[Convert.ToString(leg.LegNUM)] = tmpPreflightLegCrew;
                    }
                    RetObj["NewLeg"] = leg;
                    RetObj["Legs"] = pfViewModel.PreflightLegs.Where(l => l.IsDeleted == false).OrderBy(l => l.LegNUM);
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DeleteLeg(int CurrentLegNum)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            if (MiscUtils.IsAuthorized(Permission.Preflight.DeletePreflightLeg))
            {
                PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip]!=null)
                {
                    pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    if(pfViewModel.PreflightLegs != null && pfViewModel.PreflightLegs.Count() > 1)
                    {
                        int oldLegCount = pfViewModel.PreflightLegs.Where(l=>l.IsDeleted == false).Count();
                        int currentleg = pfViewModel.PreflightLegs.IndexOf(pfViewModel.PreflightLegs[CurrentLegNum - 1]);
                        PreflightLegViewModel objPreflightLegViewModel = pfViewModel.PreflightLegs.FirstOrDefault(p => p.LegNUM == CurrentLegNum && p.IsDeleted == false);
                        long LegNum = (long)pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)].LegNUM;
                        long dummynum = GetANewLegNum("DELETE", Convert.ToString(LegNum));
                        if (pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)].LegID != 0)
                        {
                            removeAllLegChildEntities(pfViewModel, oldLegCount);
                            pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)].IsDeleted = true;
                            pfViewModel.PreflightLegs[pfViewModel.PreflightLegs.IndexOf(objPreflightLegViewModel)].State = TripEntityState.Deleted;
                            
                        }
                        else
                        {
                            PreflightLegViewModel legToBeRemoved = pfViewModel.PreflightLegs.Where(l => l.LegNUM == CurrentLegNum).FirstOrDefault();
                            if (legToBeRemoved != null)
                            {
                                pfViewModel.PreflightLegs.Remove(legToBeRemoved);
                                removeAllLegChildEntities(pfViewModel, oldLegCount);
                            }
                        }

                        PreflightLegViewModel leg = pfViewModel.PreflightLegs.Where(x => x.LegNUM == LegNum && x.IsDeleted == false).FirstOrDefault();
                        if(leg == null)
                            leg = pfViewModel.PreflightLegs.OrderByDescending(l=>l.LegNUM).FirstOrDefault(x=> x.IsDeleted == false);

                        pfViewModel.CurrentLegNUM = leg.LegNUM;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                        RetObj["NewLeg"] = leg;
                        RetObj["Legs"] = pfViewModel.PreflightLegs.Where(l => l.IsDeleted == false);
                        RetObj["Flag"] = true;
                    }
                    else
                    {
                        RetObj["Flag"] = false;
                    }
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> UpdateCurrentLegTripEntityState(int legNum)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            var Trip = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if(Trip != null && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
            {
                PreflightLegViewModel legData = null;
                if(Trip.TripId > 0 && Trip.PreflightLegs[legNum - 1].LegID > 0)
                {
                    legData = Trip.PreflightLegs[legNum - 1];
                    legData.State = TripEntityState.Modified;
                    Trip.PreflightLegs[legNum - 1] = legData;
                }
                else
                {
                    legData = Trip.PreflightLegs[legNum - 1];
                    legData.State = TripEntityState.Added;
                    Trip.PreflightLegs[legNum - 1] = legData;
                }
            }
            result.Result = true;
            return result;
        }
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat= ResponseFormat.Json)]
        public static FSSOperationResult<object> CopyCrewDutyRuleToAllLegs(int LegNUM)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            var Trip = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip != null && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
            {
                var existingLeg = Trip.PreflightLegs.Where(p => p.IsDeleted == false && p.LegNUM == LegNUM).FirstOrDefault();
                if (existingLeg != null)
                {
                    foreach (var leg in Trip.PreflightLegs.Where(p => p.IsDeleted == false))
                    {
                        leg.CrewDutyRulesID = existingLeg.CrewDutyRulesID;
                        if (leg.CrewDutyRule == null)
                            leg.CrewDutyRule = new CrewDutyRuleViewModel();

                        leg.CrewDutyRule.CrewDutyRuleCD = existingLeg.CrewDutyRule.CrewDutyRuleCD;
                        leg.CrewDutyRule.CrewDutyRulesID = (long)existingLeg.CrewDutyRulesID;
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> CopyCrewNotesToAllLeg(int LegNUM ,string crewNotes )
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip != null && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
            {
                var existingLeg = Trip.PreflightLegs.Where(p => p.IsDeleted == false && p.LegNUM == LegNUM).FirstOrDefault();
                if (existingLeg != null)
                {
                    foreach (var leg in Trip.PreflightLegs.Where(p => p.IsDeleted == false))
                    {
                        leg.CrewNotes = crewNotes;
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> CopyPaxNotesToAllLeg(int LegNUM, string paxNotes)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip != null && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
            {
                var existingLeg = Trip.PreflightLegs.Where(p => p.IsDeleted == false && p.LegNUM == LegNUM).FirstOrDefault();
                if (existingLeg != null)
                {
                    foreach (var leg in Trip.PreflightLegs.Where(p => p.IsDeleted == false))
                    {
                        leg.Notes = paxNotes;
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }
        #endregion

        #region PRIVATE METHOD
        public static PreflightMainViewModel SetPreflightMain(PreflightMainViewModel PreflightMain, PreflightMain Trip)
        {
            //Get the Postflight Log ID
            if (Trip.TripID != 0 && Trip.IsLog.HasValue && Trip.IsLog.Value)
            {
                using (PreflightServiceClient SVC = new PreflightServiceClient())
                {
                    var returnVal = SVC.GetLogByTripID((long)Trip.TripID);

                    if (returnVal.ReturnFlag)
                    {
                        if (returnVal.EntityList.Count > 0)
                        {
                            PreflightMain.LogNum = returnVal.EntityList[0].LogNum;
                        }
                    }
                }
            }
            else
            {
                PreflightMain.LogNum = null;
            }
            PreflightMain = Trip != null ? (PreflightMainViewModel)PreflightMain.InjectFrom(Trip) : new PreflightMainViewModel();
            if(Trip != null)
            {
                if (Trip.EstDepartureDT.HasValue)
                PreflightMain.EstDepartureDT = new DateTime(Trip.EstDepartureDT.Value.Ticks, DateTimeKind.Local);
                if (Trip.RequestDT.HasValue)
                    PreflightMain.RequestDT = new DateTime(Trip.RequestDT.Value.Ticks, DateTimeKind.Local);
            }
            PreflightMain.Client = Trip.Client != null ? (ClientViewModel)new ClientViewModel().InjectFrom(Trip.Client) : new ClientViewModel();
            PreflightMain.Fleet = Trip.Fleet != null ? (FleetViewModel)new FleetViewModel().InjectFrom(Trip.Fleet) : new FleetViewModel();
            PreflightMain.Passenger = Trip.Passenger != null ? (PassengerViewModel)new PassengerViewModel().InjectFrom(Trip.Passenger) : new PassengerViewModel();
            PreflightMain.Company = Trip.Company != null ? (CompanyViewModel)new CompanyViewModel().InjectFrom(Trip.Company) : new CompanyViewModel();
            PreflightMain.Account = Trip.Account != null ? (AccountViewModel)new AccountViewModel().InjectFrom(Trip.Account) : new AccountViewModel();
            PreflightMain.Crew = Trip.Crew != null ? (CrewViewModel)new CrewViewModel().InjectFrom(Trip.Crew) : new CrewViewModel();
            PreflightMain.Aircraft = Trip.Aircraft != null ? (AircraftViewModel)new AircraftViewModel().InjectFrom(Trip.Aircraft) : new AircraftViewModel();
            PreflightMain.Department = Trip.Department != null ? (DepartmentViewModel)new DepartmentViewModel().InjectFrom(Trip.Department) : new DepartmentViewModel();
            PreflightMain.DepartmentAuthorization = Trip.DepartmentAuthorization != null ? (DepartmentAuthorizationViewModel)new DepartmentAuthorizationViewModel().InjectFrom(Trip.DepartmentAuthorization) : new DepartmentAuthorizationViewModel();
            PreflightMain.CQCustomer = Trip.CQCustomer != null ? (CQCustomerViewModel)new CQCustomerViewModel().InjectFrom(Trip.CQCustomer) : new CQCustomerViewModel();
            PreflightMain.EmergencyContact = Trip.EmergencyContact != null ? (EmergencyContactViewModel)new EmergencyContactViewModel().InjectFrom(Trip.EmergencyContact) : new EmergencyContactViewModel();
            PreflightMain.UserMaster = Trip.UserMaster != null ? (UserMasterViewModel)new UserMasterViewModel().InjectFrom(Trip.UserMaster) : new UserMasterViewModel();
            return PreflightMain;
        }
        public static List<PreflightLegViewModel> setPreflightLeg(List<PreflightLeg> tripLeg)
        {
            List<PreflightLegViewModel> customLegModel=new List<PreflightLegViewModel>();
            if (tripLeg != null)
            {
                tripLeg = tripLeg.OrderBy(p => p.LegNUM).ToList();
                foreach (PreflightLeg leg in tripLeg)
                {
                    PreflightLegViewModel customleg = new PreflightLegViewModel();
                    customleg = (PreflightLegViewModel)customleg.InjectFrom(leg);
                    customleg.ArrivalAirport = (AirportViewModel)new AirportViewModel().InjectFrom(leg.Airport);
                    customleg.DepartureAirport = (AirportViewModel)new AirportViewModel().InjectFrom(leg.Airport1);
                    customleg.Account = leg.Account != null ? (AccountViewModel)new AccountViewModel().InjectFrom(leg.Account) : new AccountViewModel();
                    customleg.CQCustomer = leg.CQCustomer != null ? (CQCustomerViewModel)new CQCustomerViewModel().InjectFrom(leg.CQCustomer) : new CQCustomerViewModel();
                    customleg.Client = leg.Client != null ? (ClientViewModel)new ClientViewModel().InjectFrom(leg.Client) : new ClientViewModel();
                    customleg.Department = leg.Department != null ? (DepartmentViewModel)new DepartmentViewModel().InjectFrom(leg.Department) : new DepartmentViewModel();
                    customleg.DepartmentAuthorization = leg.DepartmentAuthorization != null ? (DepartmentAuthorizationViewModel)new DepartmentAuthorizationViewModel().InjectFrom(leg.DepartmentAuthorization) : new DepartmentAuthorizationViewModel();
                    customleg.Passenger = leg.Passenger != null ? (PassengerViewModel)new PassengerViewModel().InjectFrom(leg.Passenger) : new PassengerViewModel();
                    customleg.FlightCatagory = leg.FlightCatagory != null ? (FlightCatagoryViewModel)new FlightCatagoryViewModel().InjectFrom(leg.FlightCatagory) : new FlightCatagoryViewModel();
                    customleg.isDeadCategory = leg.FlightCatagory != null ? leg.FlightCatagory.IsDeadorFerryHead??false : false;
                    customleg.CrewDutyRule = leg.CrewDutyRule != null ? (CrewDutyRuleViewModel)new CrewDutyRuleViewModel().InjectFrom(leg.CrewDutyRule) : new CrewDutyRuleViewModel();
                    customleg.PreflightTripOutbounds = new List<PreflightTripOutboundViewModel>();
                    foreach (PreflightTripOutbound legOutbound in leg.PreflightTripOutbounds)
                    {
                        customleg.PreflightTripOutbounds.Add(legOutbound != null ? (PreflightTripOutboundViewModel)new PreflightTripOutboundViewModel().InjectFrom(legOutbound) : new PreflightTripOutboundViewModel());
                    }
                    customleg.PreflightChecklist = new List<PreflightChecklistViewModel>();

                    foreach (PreflightCheckList legChecklist in leg.PreflightCheckLists)
                    {
                        PreflightChecklistViewModel checklist = new PreflightChecklistViewModel();
                        checklist.LegID = legChecklist.LegID;
                        checklist.PreflightCheckListID = legChecklist.PreflightCheckListID;
                        checklist.ComponentCD = legChecklist.ComponentCD;
                        checklist.CheckListID = legChecklist.TripManagerCheckList.CheckListID;
                        checklist.CheckListDescription = legChecklist.TripManagerCheckList.CheckListDescription;
                        checklist.ComponentDescription = legChecklist.ComponentDescription;
                        checklist.IsDeleted = legChecklist.IsDeleted;
                        checklist.IsCompleted = legChecklist.IsCompleted;
                        checklist.CheckGroupCD = legChecklist.TripManagerCheckList.TripManagerCheckListGroup.CheckGroupCD;
                        checklist.CheckGroupID = legChecklist.CheckGroupID;
                        checklist.CheckGroupDescription = legChecklist.TripManagerCheckList.TripManagerCheckListGroup.CheckGroupDescription;

                        customleg.PreflightChecklist.Add(checklist);
                    }

                    customLegModel.Add(customleg);
                }

            }
            return customLegModel;
        }
        public static List<PreflightLeg> InjectPreflightLegVMToPreflightLeg(List<PreflightLegViewModel> LegsVM, bool blnInjectAirports = false)
        {
            List<PreflightLeg> Legs=new List<PreflightLeg>();
            foreach(PreflightLegViewModel LegVM in LegsVM)
            {
                PreflightLeg Leg=(PreflightLeg)new PreflightLeg().InjectFrom(LegVM);
                if(LegVM.IsDeleted == true)
                    Leg.State = TripEntityState.Deleted;
                else
                {
                    if (Leg.LegID > 0)
                    {
                        Leg.State = TripEntityState.Modified;
                    }
                    else
                    {
                        Leg.State = TripEntityState.Added;
                    }
                }
                if (blnInjectAirports)
                {
                    Leg.Airport = (FlightPak.Web.PreflightService.Airport) new FlightPak.Web.PreflightService.Airport().InjectFrom(LegVM.ArrivalAirport);
                    Leg.Airport1 = (FlightPak.Web.PreflightService.Airport) new FlightPak.Web.PreflightService.Airport().InjectFrom(LegVM.DepartureAirport);
                }
                PreflightLegViewModel Nextleg = LegsVM.Where(x => x.LegNUM == (LegVM.LegNUM + 1)).FirstOrDefault();
                if (Nextleg != null)
                {
                    if (Nextleg.DepartureGreenwichDTTM != null)
                        Leg.NextGMTDTTM = Nextleg.DepartureGreenwichDTTM;
                    if (Nextleg.HomeDepartureDTTM != null)
                        Leg.NextHomeDTTM = Nextleg.HomeDepartureDTTM;
                    if (Nextleg.DepartureDTTMLocal != null)
                        Leg.NextLocalDTTM = Nextleg.DepartureDTTMLocal;
                }
                else
                {
                    if (Leg.ArrivalGreenwichDTTM != null)
                        Leg.NextGMTDTTM = Leg.ArrivalGreenwichDTTM;
                    if (Leg.HomeArrivalDTTM != null)
                        Leg.NextHomeDTTM = Leg.HomeArrivalDTTM;
                    if (Leg.ArrivalDTTMLocal != null)
                        Leg.NextLocalDTTM = Leg.ArrivalDTTMLocal;
                }
                if(LegVM.PreflightChecklist != null)
                {
                    List<PreflightCheckList> checkList = LegVM.PreflightChecklist.Where(t => t.CheckListID != null).Select(t => (PreflightCheckList) new PreflightCheckList().InjectFrom(t)).ToList();
                    Leg.PreflightCheckLists = (Leg.IsDeleted) ? checkList.Where(c => c.PreflightCheckListID > 0).ToList() : checkList;
                }
                Legs.Add(Leg);
            }
            return Legs;
        }
        public static Dictionary<string, List<PreflightLegCrewViewModel>> SetPreflightLegsCrews(List<PreflightLeg> tripLeg)
        {
            Dictionary<string, List<PreflightLegCrewViewModel>> customLegCrewModel = new Dictionary<string, List<PreflightLegCrewViewModel>>();
            if (tripLeg != null)
            {
                foreach (PreflightLeg leg in tripLeg)
                {
                    List<PreflightLegCrewViewModel> customlegCrew = new List<PreflightLegCrewViewModel>();
                    foreach (var crew in leg.PreflightCrewLists)
                    {
                        customlegCrew.Add((PreflightLegCrewViewModel)new PreflightLegCrewViewModel().InjectFrom(crew));
                    }
                    customLegCrewModel[leg.LegNUM.ToString()] = customlegCrew;
                }
            }
            return customLegCrewModel;
        }
        private static PreflightLegViewModel CreateFirstLeg(PreflightMainViewModel pfMain)
        {
            PreflightLegViewModel customleg = new PreflightLegViewModel();
            PreflightTripManagerLite managerlite=new PreflightTripManagerLite();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            customleg.LegNUM = 1;
            customleg.DepartureAirport = managerlite.getAirportByIcaoId(pfMain.HomeBaseAirportICAOID);
            customleg.IsPrivate = pfMain.IsPrivate;
            customleg.ArrivalAirport=new AirportViewModel();
            customleg.Account = (AccountViewModel)new AccountViewModel().InjectFrom(pfMain.Account);
            customleg.AccountID = pfMain.AccountID;
            customleg.CQCustomer = (CQCustomerViewModel)new CQCustomerViewModel().InjectFrom(pfMain.CQCustomer);
            customleg.CQCustomerID = pfMain.CQCustomerID;
            customleg.Client = (ClientViewModel)new ClientViewModel().InjectFrom(pfMain.Client);
            customleg.ClientID = pfMain.ClientID;
            customleg.Department = (DepartmentViewModel)new DepartmentViewModel().InjectFrom(pfMain.Department);
            customleg.DepartmentID = pfMain.DepartmentID;
            customleg.DepartmentAuthorization = (DepartmentAuthorizationViewModel)new DepartmentAuthorizationViewModel().InjectFrom(pfMain.DepartmentAuthorization);
            customleg.AuthorizationID = pfMain.AuthorizationID;
            customleg.Passenger = (PassengerViewModel)new PassengerViewModel().InjectFrom(pfMain.Passenger);
            customleg.FlightNUM = pfMain.FlightNUM;
            customleg.PassengerRequestorID = pfMain.PassengerRequestorID;
            customleg = SetADefaultAircraft(customleg,pfMain.AircraftID);
            customleg.State = TripEntityState.Added;
            customleg.FlightPurpose = pfMain.TripDescription;
            customleg.IsDepartureConfirmed = true;
            customleg.IsArrivalConfirmation = false;
            if(pfMain.Fleet != null)
            {
                if(pfMain.Fleet.MaximumPassenger != null && pfMain.Fleet.MaximumPassenger > 0)
                {
                    customleg.SeatTotal = Convert.ToInt32(pfMain.Fleet.MaximumPassenger);
                    customleg.ReservationTotal = 0;
                    customleg.PassengerTotal = 0;
                    customleg.ReservationAvailable = customleg.SeatTotal - customleg.PassengerTotal;
                    customleg.WaitNUM = 0;
                }
            }
            if(UserPrincipal._DefaultFlightCatID != null)
            {
                customleg.FlightCategoryID = UserPrincipal._DefaultFlightCatID;
                customleg = SetADefaultFlightCategory(customleg);
            }
            else
            {
                customleg.FlightCatagory=new FlightCatagoryViewModel();
            }
            if(UserPrincipal._CrewDutyID != null)
            {
                customleg.CrewDutyRulesID = UserPrincipal._CrewDutyID;
                customleg = SetADefaultCrewDutyTypeAndFARRules(customleg);
            }
            else
            {
                customleg.CrewDutyRule=new CrewDutyRuleViewModel();
            }
            using (CalculationService.CalculationServiceClient ObjCalculation = new CalculationService.CalculationServiceClient())
            {
                DateTime? tempDatetime = new DateTime();
                DateTime? dt = pfMain.EstDepartureDT;
                PreflightUtils preUtils = new PreflightUtils();
                Hashtable obj = preUtils.CalculateLegDateTime(dt, ":", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, UserPrincipal._ApplicationDateFormat, customleg.DepartureAirport == null ? "" : Convert.ToString(customleg.DepartureAirport.AirportID), true, ObjCalculation);
                if (obj.Count > 0)
                {
                    if (UserPrincipal._IsAutomaticCalcLegTimes) { 
                    customleg.DepartureDTTMLocal = string.IsNullOrEmpty(obj["DepartureLocalDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0} {1}", obj["DepartureLocalDate"], obj["LocalDepartureTime"]), UserPrincipal._ApplicationDateFormat + " HH:mm");
                    customleg.DepartureGreenwichDTTM = string.IsNullOrEmpty(obj["DepartureUtcDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0} {1}", obj["DepartureUtcDate"], obj["UtcDepartureTime"]), UserPrincipal._ApplicationDateFormat + " HH:mm");
                    customleg.HomeDepartureDTTM = string.IsNullOrEmpty(obj["DepartureHomeDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0} {1}", obj["DepartureHomeDate"], obj["HomeDepartureTime"]), UserPrincipal._ApplicationDateFormat + " HH:mm");
                    }
                    else
                    {
                        customleg.DepartureDTTMLocal = string.IsNullOrEmpty(obj["DepartureLocalDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0}", obj["DepartureLocalDate"]), UserPrincipal._ApplicationDateFormat);
                        customleg.DepartureGreenwichDTTM = string.IsNullOrEmpty(obj["DepartureUtcDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0}", obj["DepartureUtcDate"]), UserPrincipal._ApplicationDateFormat);
                        customleg.HomeDepartureDTTM = string.IsNullOrEmpty(obj["DepartureHomeDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0}", obj["DepartureHomeDate"]), UserPrincipal._ApplicationDateFormat);
                    }
                    customleg.ArrivalDTTMLocal = string.IsNullOrEmpty(obj["DepartureLocalDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0}", obj["DepartureLocalDate"]), UserPrincipal._ApplicationDateFormat);
                    customleg.ArrivalGreenwichDTTM = string.IsNullOrEmpty(obj["DepartureUtcDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0}", obj["DepartureUtcDate"]), UserPrincipal._ApplicationDateFormat);
                    customleg.HomeArrivalDTTM = string.IsNullOrEmpty(obj["DepartureHomeDate"].ToString()) ? (DateTime?)null : tempDatetime.FSSParseDateTime(string.Format("{0}", obj["DepartureHomeDate"]), UserPrincipal._ApplicationDateFormat);
                }
            }
            return customleg;
        }
        public static PreflightMainViewModel NewTrip(PreflightMainViewModel PreflightMain)
        {
            if (PreflightMain.Fleet == null)
            {
                PreflightMain.Fleet = new FleetViewModel();
                PreflightMain.Aircraft = new AircraftViewModel();
                PreflightMain.EmergencyContact = new EmergencyContactViewModel();
            }
            PreflightMain.Client = new ClientViewModel();
            PreflightMain.Passenger = new PassengerViewModel();
            PreflightMain.Account = new AccountViewModel();
            PreflightMain.Crew = new CrewViewModel();
            PreflightMain.Department = new DepartmentViewModel();
            PreflightMain.DepartmentAuthorization = new DepartmentAuthorizationViewModel();
            PreflightMain.CQCustomer = new CQCustomerViewModel();
            PreflightMain.UserMaster = new UserMasterViewModel();
            return PreflightMain;
        }
        public static PreflightMainViewModel CreateOrCancelTrip(PreflightMainViewModel PreflightMain) 
        {
            PreflightMain.EstDepartureDT = null;
            PreflightMain.HomeBaseAirportICAOID = string.Empty;
            PreflightMain.HomebaseAirportID = 0;
            PreflightMain.HomebaseID =null;
            PreflightMain.TripNUM = null;
            PreflightMain.Client = new ClientViewModel();
            PreflightMain.Fleet = new FleetViewModel();
            PreflightMain.Passenger = new PassengerViewModel();
            PreflightMain.Company = new CompanyViewModel();
            PreflightMain.Account = new AccountViewModel();
            PreflightMain.Crew = new CrewViewModel();
            PreflightMain.Aircraft = new AircraftViewModel();
            PreflightMain.Department = new DepartmentViewModel();
            PreflightMain.DepartmentAuthorization = new DepartmentAuthorizationViewModel();
            PreflightMain.CQCustomer = new CQCustomerViewModel();
            PreflightMain.EmergencyContact = new EmergencyContactViewModel();
            PreflightMain.UserMaster = new UserMasterViewModel();
            PreflightMain.RequestDT = null;
            return PreflightMain;
        }
        public static bool IsAuthorized(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]) != null)
            {
                return ((FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]).IsInRole(ModuleName.ToLower());
            }
            return false;
        }

        private static List<PreflightTripException> ValidateTrip(PreflightMainViewModel trip)
        {
            PreflightMain Trip = new PreflightMain();
            PreflightTripViewModel CurrentTrip=new PreflightTripViewModel();
            List<PreflightTripException> oldExceptionList = new List<PreflightTripException>();
            List<PreflightTripException> ExceptionList = new List<PreflightTripException>();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                CurrentTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                Trip = (PreflightMain)Trip.InjectFrom(trip);
                Trip.PreflightLegs = InjectPreflightLegVMToPreflightLeg(CurrentTrip.PreflightLegs);
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {
                    if(CurrentTrip.TripId != 0)
                    {
                        var ObjretVal = Service.GetTripExceptionList(CurrentTrip.TripId.Value);
                        if(ObjretVal.ReturnFlag)
                            HttpContext.Current.Session[WebSessionKeys.PreflightException] = ObjretVal.EntityList;
                        else
                            HttpContext.Current.Session[WebSessionKeys.PreflightException] = null;
                    }
                }
                ValidateTripForMandatoryExceptions(Trip);
                if (HttpContext.Current.Session[WebSessionKeys.PreflightException]!=null) oldExceptionList = (List<PreflightTripException>)HttpContext.Current.Session[WebSessionKeys.PreflightException];

            }
            return oldExceptionList;
        }
        private static List<ExceptionTemplate> GetExceptionTemplateList()
        {
            List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();

            if (HttpContext.Current.Session[WebSessionKeys.PreflightExceptionTemplet] == null)
            {
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {
                    var ObjExTemplRetVal = Service.GetExceptionTemplateList();
                    if (ObjExTemplRetVal.ReturnFlag)
                    {
                        ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                        HttpContext.Current.Session[WebSessionKeys.PreflightExceptionTemplet] = ExceptionTemplateList;
                    }
                }
            }
            else
                ExceptionTemplateList = (List<ExceptionTemplate>)HttpContext.Current.Session[WebSessionKeys.PreflightExceptionTemplet];
            return ExceptionTemplateList;
        }
        private static bool ValidateTripForMandatoryExceptions(PreflightMain Trip)
        {
            bool isMendatoryEx = false;
            List<PreflightTripException> oldExceptionList = new List<PreflightTripException>();
            List<PreflightTripException> ExceptionList = new List<PreflightTripException>();
            using(PreflightServiceClient Service = new PreflightServiceClient())
            {
                var ObjRetval = Service.ValidateTripforMandatoryExceptions(Trip);
                if (ObjRetval.ReturnFlag)
                {
                    if (HttpContext.Current.Session[WebSessionKeys.PreflightException] != null)
                    {
                        oldExceptionList = (List<PreflightTripException>)HttpContext.Current.Session[WebSessionKeys.PreflightException];
                        if (oldExceptionList != null && oldExceptionList.Count > 0)
                        {
                            foreach (PreflightTripException oldMandatoryException in oldExceptionList.ToList())
                            {
                                if (oldMandatoryException.TripExceptionID == 0)
                                    oldExceptionList.Remove(oldMandatoryException);
                            }
                        }
                        ExceptionList = ObjRetval.EntityList;
                        if (ExceptionList != null && ExceptionList.Count > 0)
                        {
                            if (oldExceptionList == null)
                                oldExceptionList = new List<PreflightTripException>();
                            oldExceptionList.AddRange(ExceptionList);
                            isMendatoryEx = true;
                        }
                        HttpContext.Current.Session[WebSessionKeys.PreflightException] = oldExceptionList;
                    }
                    else
                    {
                        ExceptionList = ObjRetval.EntityList;
                        HttpContext.Current.Session[WebSessionKeys.PreflightException] = ExceptionList;
                        if (ExceptionList != null && ExceptionList.Count > 0)
                            isMendatoryEx = true;
                    }
                }
            }
            return isMendatoryEx;
        }
        private static string GetSubmoduleModule(long SubModuleID)
        {
            string Retval = string.Empty;
            switch (SubModuleID)
            {

                case 1: Retval = "Main"; break;
                case 2: Retval = "Leg"; break;
                case 3: Retval = "Crew"; break;
                case 4: Retval = "PAX"; break;
                case 5: Retval = "Logistics"; break;
                case 6: Retval = "Main"; break;
            }
            return Retval;
        }
        private static long GetANewLegNum(string Mode,string SelectedLegNum="")
        {
            int legNum = 0;
            int.TryParse(SelectedLegNum, out legNum);
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                switch(Mode)
                {
                    case "ADD":
                        legNum = pfViewModel.PreflightLegs.Count(l => l.IsDeleted == false) + 1;
                    break;
                    case "INSERT":
                        List<PreflightLegViewModel> PrefLegs = new List<PreflightLegViewModel>();
                        PrefLegs = pfViewModel.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        if(PrefLegs.Count > 0)
                        {
                            foreach (PreflightLegViewModel Leg in PrefLegs)
                            {
                                if (Leg.LegNUM >= legNum)
                                {
                                    Leg.LegNUM = Leg.LegNUM + 1;
                                    if (Leg.LegID > 0)
                                        Leg.State = TripEntityState.Modified;
                                }
                            }
                        }
                        pfViewModel = moveLegEntitiesUpward(pfViewModel, legNum);
                        break;
                    case "DELETE":
                        int previndex = legNum - 1;
                        PrefLegs = new List<PreflightLegViewModel>();
                         PrefLegs = pfViewModel.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        if(PrefLegs.Count > 0)
                        {
                            foreach (PreflightLegViewModel Leg in PrefLegs)
                            {
                                if (Leg.LegNUM > PrefLegs[previndex].LegNUM)
                                {
                                    Leg.LegNUM = Leg.LegNUM - 1;
                                    if(Leg.LegID > 0)
                                        Leg.State = TripEntityState.Modified;
                                }
                            }
                        }
                        pfViewModel = moveLegEntitiesDownward(pfViewModel, (int)legNum);
                        break;
                }
            }
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
            return legNum;
        }
        private static PreflightLegViewModel SetANewLegData(PreflightLegViewModel Newleg, bool calculateLegTimesAutomatically)
        {
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            List<PreflightLegViewModel> Legs=new List<PreflightLegViewModel>();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                Legs = (List<PreflightLegViewModel>) pfViewModel.PreflightLegs;
                int PrevIndex = pfViewModel.PreflightLegs.Count() - 1;

                if (Legs[PrevIndex].ArrivalDTTMLocal != null)
                {
                    DateTime dt = (DateTime) Legs[PrevIndex].ArrivalDTTMLocal;
                    double HrsToAdd = 0;
                    if(Legs[PrevIndex].CheckGroup != null && Legs[PrevIndex].CheckGroup == "DOM")
                        HrsToAdd = UserPrincipal._GroundTM == null ? 0 : (double) UserPrincipal._GroundTM;
                    else
                        HrsToAdd = UserPrincipal._GroundTMIntl == null ? 0 : (double) UserPrincipal._GroundTMIntl;

                    if (calculateLegTimesAutomatically)
                        dt = dt.AddHours(HrsToAdd);
                    else
                        dt = dt.Date;

                    Newleg.DepartureDTTMLocal = dt;
                    Newleg.ArrivalDTTMLocal = dt.Date;

                    if (calculateLegTimesAutomatically)
                    {
                        DateTime ldGmtDep = DateTime.MinValue;
                        DateTime ldHomDep = DateTime.MinValue;
                        using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                        {
                            ldGmtDep = objDstsvc.GetGMT(Newleg.DepartureAirport.AirportID, dt, true, false);
                            Newleg.DepartureGreenwichDTTM = ldGmtDep;
                            Newleg.ArrivalGreenwichDTTM = ldGmtDep.Date;
                            if (pfViewModel.PreflightMain.HomebaseAirportID != null)
                            {
                                Newleg.HomeDepartureDTTM = objDstsvc.GetGMT(pfViewModel.PreflightMain.HomebaseAirportID.Value, ldGmtDep, false, false);
                                if (Newleg.HomeDepartureDTTM != null) Newleg.HomeArrivalDTTM = ((DateTime)Newleg.HomeDepartureDTTM).Date;
                            }
                        }
                    }
                    else
                    {
                        Newleg.DepartureGreenwichDTTM = dt;
                        Newleg.ArrivalGreenwichDTTM = dt.Date;
                        Newleg.HomeDepartureDTTM = dt;
                        Newleg.HomeArrivalDTTM = dt.Date;
                    }
                }

                if(pfViewModel.PreflightMain.FlightNUM != null)
                    Newleg.FlightNUM = pfViewModel.PreflightMain.FlightNUM;

                if(pfViewModel.PreflightMain.Client != null && pfViewModel.PreflightMain.ClientID != 0)
                {
                    Newleg.Client =(ClientViewModel) new ClientViewModel().InjectFrom(pfViewModel.PreflightMain.Client);
                }
                if(pfViewModel.PreflightMain.Fleet != null)
                {
                    if(pfViewModel.PreflightMain.Fleet.MaximumPassenger != null && pfViewModel.PreflightMain.Fleet.MaximumPassenger > 0)
                    {
                        Newleg.SeatTotal = Convert.ToInt32(pfViewModel.PreflightMain.Fleet.MaximumPassenger);
                        Newleg.ReservationTotal = 0;
                        Newleg.PassengerTotal = 0;
                        Newleg.ReservationAvailable = Newleg.SeatTotal - Newleg.PassengerTotal;
                        Newleg.WaitNUM = 0;
                    }
                }
            }
            return Newleg;
        }
        private static PreflightLegViewModel SetANewInsertedLegData(PreflightLegViewModel InsertedLeg, bool calculateLegTimesAutomatically)
        {
             PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            List<PreflightLegViewModel> Legs=new List<PreflightLegViewModel>();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if(HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                PreflightLegViewModel PrevOrNextLeg = new PreflightLegViewModel();
                if(InsertedLeg.LegNUM == 1)
                {
                    InsertedLeg.IsDepartureConfirmed = false;
                    InsertedLeg.IsArrivalConfirmation = true;
                    InsertedLeg.ArrivalAirport=new AirportViewModel();
                    InsertedLeg.DepartureAirport=new AirportViewModel();
                    PrevOrNextLeg = pfViewModel.PreflightLegs.FirstOrDefault(x => x.LegNUM == InsertedLeg.LegNUM + 1);
                    InsertedLeg.ArrivalAirport = (AirportViewModel)new AirportViewModel().InjectFrom(PrevOrNextLeg.DepartureAirport);
                    if (PrevOrNextLeg.FlightCatagory != null && PrevOrNextLeg.FlightCategoryID != 0 && PrevOrNextLeg.FlightCategoryID != null)
                    {
                        InsertedLeg.FlightCatagory = PrevOrNextLeg.FlightCatagory;
                        InsertedLeg.FlightCategoryID = PrevOrNextLeg.FlightCategoryID;
                    }
                    else
                    {
                        InsertedLeg.FlightCatagory = new FlightCatagoryViewModel();
                    }
                    if (PrevOrNextLeg!=null && PrevOrNextLeg.DepartureAirport != null)
                    {
                        InsertedLeg.FlightPurpose = PrevOrNextLeg.FlightPurpose;
                        InsertedLeg.FlightNUM = string.IsNullOrWhiteSpace(PrevOrNextLeg.FlightNUM) ? (String.IsNullOrWhiteSpace(pfViewModel.PreflightMain.FlightNUM) ? "" : pfViewModel.PreflightMain.FlightNUM) : PrevOrNextLeg.FlightNUM;
                        if (PrevOrNextLeg.DepartureDTTMLocal.HasValue)
                        {
                            DateTime dt = (DateTime)PrevOrNextLeg.DepartureDTTMLocal;
                            double HrsToAdd = 0;
                            if (PrevOrNextLeg.CheckGroup != null && PrevOrNextLeg.CheckGroup == "DOM")
                                HrsToAdd = UserPrincipal._GroundTM == null ? 0 : (double)UserPrincipal._GroundTM;
                            else
                                HrsToAdd = UserPrincipal._GroundTMIntl == null ? 0 : (double)UserPrincipal._GroundTMIntl;
                            if (calculateLegTimesAutomatically)
                                dt = dt.AddHours(-1 * HrsToAdd);
                            else
                                dt = dt.Date;

                            InsertedLeg.ArrivalDTTMLocal = dt;
                            InsertedLeg.DepartureDTTMLocal = dt.Date;

                            if (calculateLegTimesAutomatically)
                            {
                                DateTime ldGmtArr = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                {
                                    ldGmtArr = objDstsvc.GetGMT(InsertedLeg.ArrivalAirport.AirportID, dt, true, false);
                                    InsertedLeg.ArrivalGreenwichDTTM = ldGmtArr;
                                    InsertedLeg.DepartureGreenwichDTTM = ldGmtArr.Date;
                                    if (pfViewModel.PreflightMain.HomebaseAirportID != null)
                                    {
                                        InsertedLeg.HomeArrivalDTTM = objDstsvc.GetGMT(pfViewModel.PreflightMain.HomebaseAirportID.Value, ldGmtArr, false, false);
                                        if (InsertedLeg.HomeArrivalDTTM != null)
                                            InsertedLeg.HomeDepartureDTTM = ((DateTime)InsertedLeg.HomeArrivalDTTM).Date;
                                    }
                                }
                            }
                            else
                            {
                                InsertedLeg.ArrivalGreenwichDTTM = dt;
                                InsertedLeg.DepartureGreenwichDTTM = dt.Date;
                                InsertedLeg.HomeArrivalDTTM = dt;
                                InsertedLeg.HomeDepartureDTTM = dt.Date;
                            }
                            if (pfViewModel.PreflightMain.Client != null && pfViewModel.PreflightMain.ClientID != 0)
                            {
                                InsertedLeg.Client = (ClientViewModel)new ClientViewModel().InjectFrom(pfViewModel.PreflightMain.Client);
                            }
                            if (pfViewModel.PreflightMain.Fleet != null)
                            {
                                if (pfViewModel.PreflightMain.Fleet.MaximumPassenger != null && pfViewModel.PreflightMain.Fleet.MaximumPassenger > 0)
                                {
                                    InsertedLeg.SeatTotal = Convert.ToInt32(pfViewModel.PreflightMain.Fleet.MaximumPassenger);
                                    InsertedLeg.ReservationTotal = 0;
                                    InsertedLeg.PassengerTotal = 0;
                                    InsertedLeg.ReservationAvailable = InsertedLeg.SeatTotal - InsertedLeg.PassengerTotal;
                                    InsertedLeg.WaitNUM = 0;
                                }
                            }
                        }
                    }
                }
                else
                {
                    PrevOrNextLeg = pfViewModel.PreflightLegs.FirstOrDefault(x => x.IsDeleted == false && x.LegNUM == InsertedLeg.LegNUM - 1);
                    InsertedLeg.IsDepartureConfirmed = true;
                    InsertedLeg.IsArrivalConfirmation = false;
                    InsertedLeg.ArrivalAirport = new AirportViewModel();
                    InsertedLeg.DepartureAirport = new AirportViewModel();
                    if(PrevOrNextLeg != null && PrevOrNextLeg.ArrivalAirport!=null)
                    {
                        InsertedLeg.FlightPurpose = PrevOrNextLeg.FlightPurpose;
                        InsertedLeg.FlightNUM = string.IsNullOrWhiteSpace(PrevOrNextLeg.FlightNUM) ? (String.IsNullOrWhiteSpace(pfViewModel.PreflightMain.FlightNUM) ? "" : pfViewModel.PreflightMain.FlightNUM) : PrevOrNextLeg.FlightNUM;
                        InsertedLeg.DepartureAirport = (AirportViewModel)new AirportViewModel().InjectFrom(PrevOrNextLeg.ArrivalAirport);
                        InsertedLeg.FlightCatagory = PrevOrNextLeg.FlightCatagory;
                        InsertedLeg.FlightCategoryID = PrevOrNextLeg.FlightCategoryID;
                        if (PrevOrNextLeg.ArrivalDTTMLocal != null)
                        {
                            DateTime dt = (DateTime)PrevOrNextLeg.ArrivalDTTMLocal;
                            double HrsToAdd = 0;
                            if (PrevOrNextLeg.CheckGroup != null && PrevOrNextLeg.CheckGroup == "DOM")
                                HrsToAdd = UserPrincipal._GroundTM == null ? 0 : (double)UserPrincipal._GroundTM;
                            else
                                HrsToAdd = UserPrincipal._GroundTMIntl == null ? 0 : (double)UserPrincipal._GroundTMIntl;
                            if (calculateLegTimesAutomatically)
                                dt = dt.AddHours(HrsToAdd);
                            else
                                dt = dt.Date;

                            InsertedLeg.DepartureDTTMLocal = dt;
                            InsertedLeg.ArrivalDTTMLocal = dt.Date;

                            if (calculateLegTimesAutomatically)
                            {
                                DateTime ldGmtDep = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                {
                                    ldGmtDep = objDstsvc.GetGMT(InsertedLeg.DepartureAirport.AirportID, dt, true, false);
                                    InsertedLeg.DepartureGreenwichDTTM = ldGmtDep;
                                    InsertedLeg.ArrivalGreenwichDTTM = ldGmtDep.Date;
                                    if (pfViewModel.PreflightMain.HomebaseAirportID != null)
                                    {
                                        InsertedLeg.HomeDepartureDTTM = objDstsvc.GetGMT(pfViewModel.PreflightMain.HomebaseAirportID.Value, ldGmtDep, false, false);
                                        if (InsertedLeg.HomeDepartureDTTM != null)
                                            InsertedLeg.HomeArrivalDTTM = ((DateTime)InsertedLeg.HomeDepartureDTTM).Date;
                                    }
                                }
                            }
                            else
                            {
                                InsertedLeg.ArrivalGreenwichDTTM = dt;
                                InsertedLeg.DepartureGreenwichDTTM = dt.Date;
                                InsertedLeg.HomeArrivalDTTM = dt;
                                InsertedLeg.HomeDepartureDTTM = dt.Date;
                            }
                            if (pfViewModel.PreflightMain.FlightNUM != null)
                                InsertedLeg.FlightNUM = pfViewModel.PreflightMain.FlightNUM;

                            if (pfViewModel.PreflightMain.Client != null && pfViewModel.PreflightMain.ClientID != 0)
                            {
                                InsertedLeg.Client = (ClientViewModel)new ClientViewModel().InjectFrom(pfViewModel.PreflightMain.Client);
                            }
                            if (pfViewModel.PreflightMain.Fleet != null)
                            {
                                if (pfViewModel.PreflightMain.Fleet.MaximumPassenger != null && pfViewModel.PreflightMain.Fleet.MaximumPassenger > 0)
                                {
                                    InsertedLeg.SeatTotal = Convert.ToInt32(pfViewModel.PreflightMain.Fleet.MaximumPassenger);
                                    InsertedLeg.ReservationTotal = 0;
                                    InsertedLeg.PassengerTotal = 0;
                                    InsertedLeg.ReservationAvailable = InsertedLeg.SeatTotal - InsertedLeg.PassengerTotal;
                                    InsertedLeg.WaitNUM = 0;
                                }
                            }
                        }
                    }
                }
            }
            return InsertedLeg;
        }
        private static PreflightLegViewModel SetADefaultCrewDutyTypeAndFARRules(PreflightLegViewModel leg)
        {
            FlightPak.Web.FlightPakMasterService.CrewDutyRules CrewDutyRule = new FlightPak.Web.FlightPakMasterService.CrewDutyRules();
            if (leg.CrewDutyRulesID != null)
            {
                using(FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetCrewDutyRulesWithFilters(Convert.ToInt64(leg.CrewDutyRulesID), string.Empty, false);
                    if (objRetVal.ReturnFlag && objRetVal.EntityList.Count > 0)
                    {
                        CrewDutyRule = objRetVal.EntityList.SingleOrDefault();
                        if(CrewDutyRule != null)
                        {
                            leg.CrewDutyRule = (CrewDutyRuleViewModel)new CrewDutyRuleViewModel().InjectFrom(CrewDutyRule);
                            leg.CrewDutyRulesID = CrewDutyRule.CrewDutyRulesID;
                            leg.FedAviationRegNUM = CrewDutyRule.FedAviatRegNum;
                        }
                    }else
                        leg.CrewDutyRule =new CrewDutyRuleViewModel();
                }
            }
            return leg;
        }
        private static PreflightLegViewModel SetADefaultFlightCategory(PreflightLegViewModel leg)
        {
            FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters FlightCatagory = new FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters();
            if(leg.FlightCategoryID != null)
            {
                using(FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetAllFlightCategoryWithFilters(0, Convert.ToInt64(leg.FlightCategoryID), string.Empty, true);
                    if (objRetVal.ReturnFlag && objRetVal.EntityList.Count > 0)
                    {
                        FlightCatagory = objRetVal.EntityList[0];
                        if (FlightCatagory!=null)
                        {
                            leg.FlightCatagory = (FlightCatagoryViewModel) new FlightCatagoryViewModel().InjectFrom(FlightCatagory);
                            leg.FlightCategoryID = FlightCatagory.FlightCategoryID;
                            leg.isDeadCategory = FlightCatagory.IsDeadorFerryHead ?? false;
                        }
                    }
                }
            }
            return leg;
        }
        private static PreflightLegViewModel SetADefaultAircraft(PreflightLegViewModel leg, long? AircraftID)
        {
            if(AircraftID != null)
            {
                using(FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft Aircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftWithFilters(string.Empty, AircraftID.Value, false);
                    if(objPowerSetting.ReturnFlag)
                    {
                        leg.PowerSetting = Aircraft.PowerSetting == null ? "1" : Aircraft.PowerSetting;
                        Aircraft = objPowerSetting.EntityList[0];
                        switch (leg.PowerSetting)
                        {
                            case "1":
                                leg.TrueAirSpeed = Aircraft.PowerSettings1TrueAirSpeed;
                                leg.TakeoffBIAS = Aircraft.PowerSettings1TakeOffBias;
                                leg.LandingBias = Aircraft.PowerSettings1LandingBias;
                                break;
                            case "2":
                                leg.TrueAirSpeed = Aircraft.PowerSettings2TrueAirSpeed;
                                leg.TakeoffBIAS = Aircraft.PowerSettings2TakeOffBias;
                                leg.LandingBias = Aircraft.PowerSettings2LandingBias;
                                break;
                            case "3":
                                leg.TrueAirSpeed = Aircraft.PowerSettings3TrueAirSpeed;
                                leg.TakeoffBIAS = Aircraft.PowerSettings3TakeOffBias;
                                leg.LandingBias = Aircraft.PowerSettings3LandingBias;
                                break;
                            default:
                                leg.TrueAirSpeed = 0;
                                leg.TakeoffBIAS = (decimal?)0.0;
                                leg.LandingBias =(decimal?) 0.0;
                                break;
                        }
                    }
                }
            }
            return leg;
        }

        private static void SetAPreflightTransportation(ref PreflightTransportList depTransport,ref PreflightTransportList arrTransport, PreflightLegCrewTransportationViewModel VMTransport, string CrewPassengerType)
        {
            depTransport.State = VMTransport.PreflightDepartTransportId != 0 ? TripEntityState.Modified : TripEntityState.Added;
            depTransport.PreflightTransportID = VMTransport.PreflightDepartTransportId;
            depTransport.IsDepartureTransport = true;
            depTransport.IsArrivalTransport = false;
            depTransport.AirportID = VMTransport.DepartureAirportId;
            depTransport.PreflightTransportName = VMTransport.DepartName;
            depTransport.TransportID = VMTransport.DepartTransportId;
            depTransport.CrewPassengerType = CrewPassengerType;
            depTransport.Street = "";
            depTransport.CityName = "";
            depTransport.StateName = "";
            depTransport.PostalZipCD = "";
            depTransport.PhoneNum1 = VMTransport.DepartPhone;
            depTransport.FaxNUM = VMTransport.DepartFax;
            depTransport.LastUpdTS = DateTime.UtcNow;
            depTransport.IsDeleted = false;
            depTransport.ConfirmationStatus = VMTransport.DepartConfirmation;
            depTransport.Comments = VMTransport.DepartComments;
            depTransport.IsCompleted = false;
            depTransport.PhoneNum4 = VMTransport.DepartRate;
            depTransport.Status = VMTransport.SelectedDepartTransportStatus;

            arrTransport.State = VMTransport.PreflightArrivalTransportId != 0 ? TripEntityState.Modified : TripEntityState.Added;
            arrTransport.PreflightTransportID = VMTransport.PreflightArrivalTransportId;
            arrTransport.IsArrivalTransport = true;
            arrTransport.IsDepartureTransport = false;
            arrTransport.AirportID = VMTransport.ArrivalAirportId;
            arrTransport.PreflightTransportName = VMTransport.ArrivalName;
            arrTransport.TransportID = VMTransport.ArrivalTransportId;
            arrTransport.CrewPassengerType = CrewPassengerType;
            arrTransport.Street = "";
            arrTransport.CityName = "";
            arrTransport.StateName = "";
            arrTransport.PostalZipCD = "";
            arrTransport.PhoneNum1 = VMTransport.ArrivalPhone;
            arrTransport.FaxNUM = VMTransport.ArrivalFax;
            arrTransport.LastUpdTS = DateTime.UtcNow;
            arrTransport.IsDeleted = false;
            arrTransport.ConfirmationStatus = VMTransport.ArrivalConfirmation;
            arrTransport.Comments = VMTransport.ArrivalComments;
            arrTransport.IsCompleted = false;
            arrTransport.PhoneNum4 = VMTransport.ArrivalRate;
            arrTransport.Status = VMTransport.SelectedArrivalTransportStatus;
        }
        public static PreflightTripViewModel InjectPreflightMainToPreflightTripViewModel(PreflightMain Trip,PreflightTripViewModel tripVM)
        {
            UserPrincipalViewModel upViewModel = getUserPrincipal();
            tripVM = (PreflightTripViewModel)tripVM.InjectFrom(Trip);

            tripVM.PreflightMain.EditMode = Trip.Mode == TripActionMode.Edit;

            tripVM.TripId = Trip.TripID;
            tripVM.PreflightMain.dateFormat = upViewModel._ApplicationDateFormat;
            tripVM.PreflightMain = SetPreflightMain(tripVM.PreflightMain, Trip);

            tripVM.PreflightLegs = setPreflightLeg(Trip.PreflightLegs);

            tripVM.PreflightLegCrewLogisticsHotelList = SetPreflightLegCrewHotelList(Trip);
            tripVM.PreflightLegCrews = SetPreflightLegsCrews(Trip.PreflightLegs);
            tripVM.PreflightLegCrewTransportationList = SetPreflightLegTransportationListToVM(Trip);
            
            tripVM.PreflightLegPaxTransportationList = SetPreflightLegPassengerTransportListToVM(Trip);            
            tripVM.PreflightLegPassengers = setPreflightLegPassengers(tripVM, Trip.PreflightLegs);
            tripVM.PreflightLegPassengersLogisticsHotelList = setPreflightLegPaxHotelList(tripVM, Trip.PreflightLegs);

            tripVM.PreflightLegPreflightLogisticsList = setPreflightLogisticsCateringDetails(tripVM, Trip.PreflightLegs);
            tripVM = new PreflightTripManagerLite().SetADefaultHomeBase(tripVM, tripVM.PreflightMain.HomebaseID.Value);
            return tripVM;
        }
        #endregion

        #region Crew
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable CrewAvailabilityGrid(bool chkCrewAvailabilityActive, bool chkPICSICOnly, bool chkCrewInactive, bool chkTypeRatedOnly, bool chkNoConflictsOnly, bool chkHomeBaseOnly, string tbCrewGroup, string View, string requestedby, bool hdnIsCrewAutoAvailability, string hdnLeg, string airportId, string homeBase, string hdnCrewGroupID, bool Filter, string crewIds)
        {
            Hashtable data = new Hashtable();
            PreflightCrewLite CrewAvailability = new PreflightCrewLite();
           
            if (View == requestedby && View == "Currency")
            {
                if (Filter)
                {
                    var dv = CrewAvailability.Filter_CheckedChanged(chkCrewAvailabilityActive, chkPICSICOnly, chkCrewInactive, chkTypeRatedOnly, chkNoConflictsOnly, chkHomeBaseOnly, tbCrewGroup, hdnIsCrewAutoAvailability, hdnLeg, airportId, hdnCrewGroupID, homeBase);
                    if (dv.Table != null)
                    {
                        var dt = dv.ToTable();
                        if (dt != null)
                        {
                            data["meta"] = new PagingMetaData() { Page = 1, Size = dt.Rows.Count, total_items = dt.Rows.Count };
                            data["results"] = CrewAvailability.GetJson(dv);
                        }
                        else
                        {
                            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                            data["results"] = new List<object>();
                        }
                        return data;
                    }
                }
                else
                {
                    if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                    {
                        var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        if (Convert.ToBoolean(hdnIsCrewAutoAvailability) == true)
                        {
                            Int64 CurrentTripID = 0;
                            Int64 CrewgroupID = 0;
                            Int64 ArriveICAOID = 0;
                            Int64 FleetID = 0;
                            Int64 AircraftID = 0;
                            DateTime ddate = new DateTime();
                            DateTime adate = new DateTime();
                            DateTime maxArrivedate = new DateTime();
                            DateTime minDepartdate = new DateTime();
                            minDepartdate = DateTime.Now;
                            maxArrivedate = DateTime.Now;
                            ddate = DateTime.Now;
                            adate = DateTime.Now;
                            if (Trip.PreflightMain.Fleet != null)
                            {
                                FleetID = Convert.ToInt64(Trip.PreflightMain.FleetID);
                            }
                            if (Trip.PreflightMain.AircraftID != null)
                            {
                                AircraftID = Convert.ToInt64(Trip.PreflightMain.AircraftID);
                            }

                            if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                            {
                                hdnLeg = "1";
                                int Legtotcount = Trip.PreflightLegs.Count;
                                CurrentTripID = Trip.PreflightMain.TripID;
                                DateTime.TryParse(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].DepartureGreenwichDTTM.ToString(), out ddate);
                                DateTime.TryParse(Trip.PreflightLegs[Legtotcount - 1].ArrivalGreenwichDTTM.ToString(), out adate);
                                var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                for (int i = 0; i < Preflegs.Count; i++)
                                {
                                    if (Preflegs[i].ArrivalGreenwichDTTM != null)
                                    {
                                        DateTime.TryParse(Trip.PreflightLegs.Max(x => x.ArrivalGreenwichDTTM).ToString(), out maxArrivedate);
                                    }
                                    
                                    if (Preflegs[i].DepartureGreenwichDTTM != null)
                                    {
                                        DateTime.TryParse(Trip.PreflightLegs.Min(x => x.DepartureGreenwichDTTM).ToString(), out minDepartdate);
                                    }
                                    
                                }

                                if (ddate != DateTime.MinValue && adate != DateTime.MinValue) 
                                {
                                    var dv = CrewAvailability.BindAvailableCrew(ddate, adate, CrewgroupID, Convert.ToInt64(airportId), FleetID, Convert.ToInt64(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].ArriveICAOID), CurrentTripID, minDepartdate, maxArrivedate, AircraftID);
                                    if (dv.Table != null)
                                    {
                                        data["meta"] = new PagingMetaData() { Page = 1, Size = dv.Table.Rows.Count, total_items = dv.Table.Rows.Count };
                                        data["results"] = CrewAvailability.GetJson(dv);
                                        return data;
                                    }
                                }
                                else
                                {
                                    var dv = CrewAvailability.BindAvailableCrew(ddate, adate, CrewgroupID, Convert.ToInt64(airportId), FleetID, Convert.ToInt64(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].ArriveICAOID), CurrentTripID, minDepartdate, maxArrivedate, AircraftID);
                                    if (dv.Table != null)
                                    {
                                        data["meta"] = new PagingMetaData() { Page = 1, Size = dv.Table.Rows.Count, total_items = dv.Table.Rows.Count };
                                        data["results"] = CrewAvailability.GetJson(dv);
                                        return data;
                                    }
                                }
                            }
                            else
                            {
                                var dv = CrewAvailability.BindAvailableCrew(ddate, adate, CrewgroupID, Convert.ToInt64(airportId), FleetID, ArriveICAOID, CurrentTripID, minDepartdate,maxArrivedate, AircraftID);
                                if (dv.Table != null)
                                {
                                    data["meta"] = new PagingMetaData() { Page = 1, Size = dv.Table.Rows.Count, total_items = dv.Table.Rows.Count };
                                    data["results"] = CrewAvailability.GetJson(dv);
                                    return data;
                                }
                            }
                        }
                    }

                }
            }
           
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable CrewSelectionGrid()
        {
            Hashtable data = new Hashtable();
            PreflightCrewLite Crew = new PreflightCrewLite();
            var dv = Crew.LoadCrewfromTrip(true);
            if (dv.Table != null)
            {
                var dt = dv.ToTable();
                if (dt != null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = dt.Rows.Count, total_items = dt.Rows.Count };
                    data["results"] = Crew.GetJson(dv);
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable CrewSummaryGrid()
        {
            Hashtable data = new Hashtable();
            PreflightCrewLite Crew = new PreflightCrewLite();
            var dv = Crew.LoadCrewfromTrip(false);
            if (dv.Table != null)
            {
                var dt = dv.ToTable();

                HttpContext.Current.Session[WebSessionKeys.CrewInSummery] = dt;

                if (dt != null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = dt.Rows.Count, total_items = dt.Rows.Count };
                    data["results"] = Crew.GetJson(dv);
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> TripLegs()
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            UserPrincipalViewModel upViewModel = getUserPrincipal();
            Hashtable data = new Hashtable();
            List<Hashtable> Legs = new List<Hashtable>();
            PreflightCrewLite Crew = new PreflightCrewLite();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    foreach (var Leg in Trip.PreflightLegs.Where(c=>c.State!=TripEntityState.Deleted && c.IsDeleted==false).OrderBy(p => p.LegNUM.Value))
                    {
                        Hashtable LegTemp = new Hashtable();
                        var lblLegDate = "";
                        var lblLegDepart = "";


                        #region Crew Leg header shoud should show local departure date

                        if (Leg.DepartureDTTMLocal != null)
                        {
                            string strDate = String.Format(CultureInfo.InvariantCulture, "{0:" + upViewModel._ApplicationDateFormat + "}", Leg.DepartureDTTMLocal);
                            lblLegDate = System.Web.HttpUtility.HtmlEncode(strDate);
                        }


                        #endregion
                        if (Leg.DepartureAirport != null && Leg.ArrivalAirport != null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode(Convert.ToString(Leg.DepartureAirport.IcaoID) + "-" + Convert.ToString(Leg.ArrivalAirport.IcaoID));
                        }
                        if (Leg.DepartureAirport == null && Leg.ArrivalAirport != null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode(string.Empty + "-" + Convert.ToString(Leg.ArrivalAirport.IcaoID));
                        }
                        if (Leg.DepartureAirport != null && Leg.ArrivalAirport == null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode(Convert.ToString(Leg.DepartureAirport.IcaoID) + "-" + string.Empty);
                        }
                        LegTemp["LegDepart"] = lblLegDepart;
                        LegTemp["LegDate"] = lblLegDate;
                        LegTemp["LegNUM"] = Leg.LegNUM;
                        var CrewList = "";
                        var IsDiscontinue = "";
                        int crewCounter = 1; 
                        if (Trip.PreflightLegCrews.Keys.Contains(Leg.LegNUM.ToString()))
                        {
                            List<PreflightLegCrewViewModel> Crews = Trip.PreflightLegCrews[Leg.LegNUM.ToString()];
                            foreach (var crew in Crews.Where(x => x.IsDeleted != true).OrderBy(p => p.OrderNUM.Value))
                            {
                                if (crewCounter != crew.OrderNUM)
                                    for (int ctr = crewCounter; ctr < crew.OrderNUM; ctr++)
                                    {
                                        IsDiscontinue += "false" + ",";
                                        CrewList += "0" + ",";
                                        crewCounter++;
                                    }
                                CrewList += crew.DutyTYPE + ",";
                                IsDiscontinue += (crew.IsDiscount??false).ToString() + ",";
                                crewCounter++;
                            }

                        }
                           
                        LegTemp["CrewList"] = CrewList;
                        LegTemp["IsDiscontinue"] = IsDiscontinue;
                        Legs.Add(LegTemp);
                    }
                    data["results"] = Legs;
                    result.Result = data;
                    return result;
                }
            }
            data["results"] = new List<object>();
            result.Result = data;
            return result;
        }
        
        static System.Text.RegularExpressions.Regex _htmlRegex = new System.Text.RegularExpressions.Regex("<.*?>", System.Text.RegularExpressions.RegexOptions.Compiled);
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SaveCrewInfoGrid(string GridJson)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable data = new Hashtable();
            GridJson=_htmlRegex.Replace(GridJson, string.Empty);
            if (GridJson != "undefined")
            {
                dynamic Obj = JArray.Parse(GridJson);
                PreflightCrewLite Crew = new PreflightCrewLite();
                Crew.SaveGridtoSession(Obj);
                data["results"] = new List<object>();
                data["warning"] = HttpContext.Current.Session["PreflightCrewConflictMessage"];
                HttpContext.Current.Session["PreflightCrewConflictMessage"] = "";
                result.Result = data;
            }
            return result;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void UpdateRevisionDescription(string RevDesc)
        {
            PreflightTripViewModel pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            pfViewModel.PreflightMain.RevisionDescriptioin = RevDesc;
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SaveCrewSummaryCells(string ColName, string CrewID, string Value, int LegNumber)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable data = new Hashtable();
            DataTable dtNewAssign = new DataTable();
            if ((DataTable)HttpContext.Current.Session[WebSessionKeys.CrewInSummery] != null)
                dtNewAssign = (DataTable)HttpContext.Current.Session[WebSessionKeys.CrewInSummery];
            #region Update Columns
            for (int i = 0; i < dtNewAssign.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtNewAssign.Rows[i]["CrewID"]) == Convert.ToInt64(CrewID) && Convert.ToInt64(dtNewAssign.Rows[i]["Leg"]) == Convert.ToInt64(LegNumber))
                {
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        var modelrow = Trip.PreflightLegCrews[LegNumber.ToString()].Where(p => p.CrewID.Value.ToString() == CrewID).First();

                        if (ColName == "Street")
                        {
                            dtNewAssign.Rows[i]["Street"] = Value;
                            modelrow.Street = Value;
                        }
                        else if (ColName == "Postal")
                        {
                            dtNewAssign.Rows[i]["Postal"] = Value;
                            modelrow.PostalZipCD = Value;
                        }
                        else if (ColName == "City")
                        {
                            dtNewAssign.Rows[i]["City"] = Value;
                            modelrow.CityName = Value;
                        }
                        else if (ColName == "State")
                        {
                            dtNewAssign.Rows[i]["State"] = Value;
                            modelrow.StateName = Value;
                        }
                        else if (ColName == "PassportID")
                        {
                            var hdnPassID = Value;
                            if (!string.IsNullOrEmpty(hdnPassID))
                            {
                                dtNewAssign.Rows[i]["PassportID"] = Convert.ToInt64(hdnPassID);
                                modelrow.PassportID = Convert.ToInt64(hdnPassID); ;
                            }
                        }
                        else if (ColName == "PassportNum")
                        {
                            dtNewAssign.Rows[i]["Passport"] = Value;
                            modelrow.PassportNum = Value;
                        }
                        else if (ColName == "VisaID")
                        {
                            var hdnVisaID = Value;
                            if (!string.IsNullOrEmpty(hdnVisaID))
                            {
                                dtNewAssign.Rows[i]["VisaID"] = Convert.ToInt64(hdnVisaID);
                                modelrow.VisaID = Convert.ToInt64(hdnVisaID);
                            }
                        }
                        if (modelrow.State != TripEntityState.Added && modelrow.State != TripEntityState.Deleted)
                        {
                            modelrow.State = TripEntityState.Modified;    
                        }
                        
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                        break;
                    }
                }
            }
            dtNewAssign.AcceptChanges();

            dtNewAssign.DefaultView.Sort = "OrderNUM ASC"; //,CrewCD ASC,Leg ASC"; // Fix for #8195
            dtNewAssign = dtNewAssign.DefaultView.Table;
            HttpContext.Current.Session[WebSessionKeys.CrewInSummery] = dtNewAssign;
            #endregion
            data["results"] = new List<object>();
            result.Result = data;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SavePaxSummaryCells(string ColName, long PassengerID, string Value, int LegNumber)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable data = new Hashtable();

            List<PassengerViewModel> objPassengerSummaryList = new List<PassengerViewModel>();
            if (HttpContext.Current.Session["SelectedPaxList"] != null)
                objPassengerSummaryList = (List<PassengerViewModel>)HttpContext.Current.Session["SelectedPaxList"];
            #region Update Columns

            foreach (var passengerSummary in objPassengerSummaryList)
            {
                if (passengerSummary.PassengerID == PassengerID && passengerSummary.LegNUM == LegNumber)
                {
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    string CompDateFormat = "MM/dd/yyyy";
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        var modelrow = Trip.PreflightLegPassengers[LegNumber.ToString()].First(p => p.PassengerID == PassengerID);

                        if (ColName == "Street")
                        {
                            passengerSummary.StateName = Value;
                            modelrow.Street = Value;
                        }
                        else if (ColName == "Postal")
                        {
                            passengerSummary.PostalZipCD = Value;
                            modelrow.PostalZipCD = Value;
                        }
                        else if (ColName == "City")
                        {
                            passengerSummary.CityName = Value;
                            modelrow.CityName = Value;
                        }
                        else if (ColName == "State")
                        {
                            passengerSummary.StateName = Value;
                            modelrow.StateName = Value;
                        }
                        else if (ColName == "PassportID")
                        {
                            passengerSummary.PassportID = Convert.ToInt64(Value);
                            modelrow.PassportID = Convert.ToInt64(Value);
                        }
                        else if (ColName == "passportNum")
                        {
                            passengerSummary.PassportNum = Value;
                            modelrow.PassportNum = Value;
                        }
                        else if (ColName == "VisaID")
                        {
                            var hdnVisaID = Value;
                            if (!string.IsNullOrEmpty(hdnVisaID))
                            {
                                passengerSummary.VisaID = Convert.ToInt64(hdnVisaID);
                                modelrow.VisaID = Convert.ToInt64(hdnVisaID);
                            }
                        }
                        else if (ColName == "Billing")
                        {
                            passengerSummary.Billing = Value;
                            modelrow.Billing = Value;
                        }
                        if (modelrow.State != TripEntityState.Added && modelrow.State != TripEntityState.Deleted)
                        {
                            modelrow.State = TripEntityState.Modified;
                        }

                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                        break;
                    }
                }
            }
            objPassengerSummaryList = objPassengerSummaryList.OrderBy(p => p.OrderNUM).ToList();
            HttpContext.Current.Session["SelectedPaxList"] = objPassengerSummaryList;
            #endregion
            data["results"] = new List<object>();
            result.Result = data;
            return result;
        }

        #endregion

        #region Preflight PAX Hotel Logistics Tab

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegPassengerHotelViewModel> GetPreflightLegPaxHotelByLegNumHotelCode(int LegNum, string hotelCode)
        {
            FSSOperationResult<PreflightLegPassengerHotelViewModel> result = new FSSOperationResult<PreflightLegPassengerHotelViewModel>();
            if (!result.IsAuthorized())
                return result;

            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");                
            }
            if(!Trip.PreflightLegPassengersLogisticsHotelList.ContainsKey(LegNum.ToString()))
            {
                result.Result = new PreflightLegPassengerHotelViewModel();
                return result;
            }
           
            List<PreflightLegPassengerHotelViewModel> preflightLegPassengerHotelVMList = Trip.PreflightLegPassengersLogisticsHotelList[LegNum.ToString()];

            if (preflightLegPassengerHotelVMList==null)
            {
                preflightLegPassengerHotelVMList = new List<PreflightLegPassengerHotelViewModel>();
                            
            }
            PreflightLegPassengerHotelViewModel preflightLegPassengerHotelVM = preflightLegPassengerHotelVMList.Where(t => t.HotelCode == hotelCode).FirstOrDefault();
            PreflightLegViewModel nextLegViewModel = Trip.PreflightLegs.Where(t => t.IsDeleted==false && t.LegNUM ==(LegNum+1)).FirstOrDefault();

            if(preflightLegPassengerHotelVM==null)
            {
                preflightLegPassengerHotelVM = new PreflightLegPassengerHotelViewModel();
                preflightLegPassengerHotelVM.isArrivalHotel = true;
                preflightLegPassengerHotelVM.LegNUM = 1;
                preflightLegPassengerHotelVM.State = TripEntityState.Added;
                result.Result = preflightLegPassengerHotelVM;
                return result;
            }            
            PreflightLegViewModel currentLegViewModel = Trip.PreflightLegs.Where(t => t.IsDeleted == false && t.LegNUM == LegNum).FirstOrDefault();            
            PreflightPaxLite paxLite= new PreflightPaxLite();
            preflightLegPassengerHotelVM.DateIn = paxLite.ValidateDateInForHotel(currentLegViewModel,preflightLegPassengerHotelVM);
            preflightLegPassengerHotelVM.DateOut = paxLite.ValidateDateOutForHotel(nextLegViewModel, preflightLegPassengerHotelVM);


            result.Result = preflightLegPassengerHotelVM;
            return result;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePreflightLegPaxHotelByLegNum(PreflightLegPassengerHotelViewModel HotelDetail)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;

            if (HotelDetail.NoPAX == true || HotelDetail.PAXCode==null || HotelDetail.PAXCode.Length==0)
            {
                HotelDetail.PAXCode = "None";                
                HotelDetail.IsAllCrewOrPax = false;
                HotelDetail.NoPAX = true;
            }
            if (HotelDetail.PAXCode.ToLower().Trim().Equals("all"))
            {
                HotelDetail.NoPAX = false;
                HotelDetail.IsAllCrewOrPax = true;
            }
            else
            {                
                HotelDetail.IsAllCrewOrPax = false;
            }

            PreflightTripViewModel preflightTrip= new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {                
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = preflightTrip;
            }else{
                preflightTrip=(PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            List<PreflightLegPassengerHotelViewModel> paxHotelList = preflightTrip.PreflightLegPassengersLogisticsHotelList.Where(t => t.Key == HotelDetail.LegNUM.ToString()).Select(h => h.Value).FirstOrDefault();
            if (paxHotelList == null)
            {
                paxHotelList = new List<PreflightLegPassengerHotelViewModel>();
                preflightTrip.PreflightLegPassengersLogisticsHotelList.Add(HotelDetail.LegNUM.ToString(), paxHotelList);
            }
            if (HotelDetail.PreflightHotelListID == 0)
            {
                HotelDetail.State = TripEntityState.Added;
            }
            else
            {
                HotelDetail.State = TripEntityState.Modified;
            }

            PreflightLegViewModel currentLegViewModel = preflightTrip.PreflightLegs.Where(t => t.IsDeleted == false && t.LegNUM == HotelDetail.LegNUM).FirstOrDefault();
            
            PreflightLegViewModel nextLegViewModel = preflightTrip.PreflightLegs.Where(t => t.IsDeleted == false && t.LegNUM == (HotelDetail.LegNUM + 1)).FirstOrDefault();
            PreflightPaxLite paxLite= new PreflightPaxLite();
            HotelDetail.DateIn = paxLite.ValidateDateInForHotel(currentLegViewModel, HotelDetail);
            HotelDetail.DateOut = paxLite.ValidateDateOutForHotel(nextLegViewModel, HotelDetail);

            if (string.IsNullOrWhiteSpace(HotelDetail.HotelCodeEdit)==true && paxHotelList.Where(t => t.HotelCode == HotelDetail.HotelCode && t.isArrivalHotel == HotelDetail.isArrivalHotel).Count() == 0)
            {
                paxHotelList.Add(HotelDetail);                
            }
            else
            {
                var tt = paxHotelList.Find(t => (t.HotelCode == HotelDetail.HotelCode || t.HotelCode == HotelDetail.HotelCodeEdit) && t.isArrivalHotel == HotelDetail.isArrivalHotel);
                paxHotelList.Remove(tt);
                paxHotelList.Add(HotelDetail);
            }
            result.Result = "1";
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<List<RoomTypeViewModal>> RetrieveRoomType()
        {
            FSSOperationResult<List<RoomTypeViewModal>> result = new FSSOperationResult<List<RoomTypeViewModal>>();
            if (!result.IsAuthorized())
                return result;

            PreflightTripManagerLite pmLite = new PreflightTripManagerLite();
            var data= pmLite.RetrieveRoomType();
            result.Result = data;
            return result;        
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<List<RoomDescriptionViewModel>> RetrieveRoomDescription()
        {
            FSSOperationResult<List<RoomDescriptionViewModel>> result = new FSSOperationResult<List<RoomDescriptionViewModel>>();
            if (!result.IsAuthorized())
                return result;
            var data = DBCatalogsDataLoader.RetrieveRoomDescription();
            result.Result = data;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable InitializePreflightPAXLegEntiryGrid(int LegNUM)
        {
            Hashtable data = new Hashtable();
            PreflightPaxLite Pax = new PreflightPaxLite();            
            var passengerList = Pax.LoadPaxFromPaxSummary(LegNUM);
            data["meta"] = new PagingMetaData() { Page = passengerList.Count/10, Size = 10, total_items = passengerList.Count };
            data["results"]=passengerList;

            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegPassengerTransportViewModel> GetPassengerLegTransportViewModelByLegNum(int LegNum)
        {
            FSSOperationResult<PreflightLegPassengerTransportViewModel> result = new FSSOperationResult<PreflightLegPassengerTransportViewModel>();
            if (!result.IsAuthorized())
                return result;

            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            PreflightLegPassengerTransportViewModel currentPaxTransport = null;
            if (Trip != null && Trip.PreflightLegPaxTransportationList != null)
            {
                if (Trip.PreflightLegPaxTransportationList.ContainsKey(Convert.ToString(LegNum)))
                currentPaxTransport = Trip.PreflightLegPaxTransportationList[Convert.ToString(LegNum)];
            }
          
            if (Framework.Helpers.MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightCrewTransport))
            {
                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (currentPaxTransport != null && string.IsNullOrWhiteSpace(currentPaxTransport.ArrivalCode))
                    {
                        var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(currentPaxTransport.ArriveTransportID)).EntityList;
                        if (objRetVal != null && objRetVal.Count > 0)
                            currentPaxTransport.ArrivalCode = objRetVal[0].TransportCD;
                    }
                    if (currentPaxTransport != null && string.IsNullOrWhiteSpace(currentPaxTransport.DepartCode))
                    {
                        var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(currentPaxTransport.DepartTransportID)).EntityList;
                        if (objRetVal != null && objRetVal.Count > 0)
                            currentPaxTransport.DepartCode = objRetVal[0].TransportCD;

                    }
                                   
                }
            }
            Trip.CurrentLegNUM = LegNum;
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            result.Result = currentPaxTransport;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePaxLogisticsTransport(int legNum, PreflightLegPassengerTransportViewModel paxTransport)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip != null)
            {
                PreflightLegPassengerTransportViewModel passengerTransport = null;
                if (Trip.PreflightLegPaxTransportationList != null && Trip.PreflightLegPaxTransportationList.ContainsKey(Convert.ToString(legNum)))
                    passengerTransport = Trip.PreflightLegPaxTransportationList[Convert.ToString(legNum)];
                else
                    passengerTransport = new PreflightLegPassengerTransportViewModel();

                passengerTransport = paxTransport;
                if (Trip.PreflightLegPaxTransportationList == null)
                    Trip.PreflightLegPaxTransportationList = new Dictionary<string, PreflightLegPassengerTransportViewModel>();
                Trip.PreflightLegPaxTransportationList[Convert.ToString(legNum)] = passengerTransport;
            }
            result.Result = true;
            return result;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable InitializePreflightPAXHotelListGrid(int LegNum)
        {
            Hashtable data = new Hashtable();
            List<PreflightLegPassengerHotelViewModel> paxHotelList = null;

            PreflightTripViewModel preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (preflightTrip==null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            if (preflightTrip.PreflightLegPassengersLogisticsHotelList.ContainsKey(LegNum.ToString()))
                paxHotelList = preflightTrip.PreflightLegPassengersLogisticsHotelList[LegNum.ToString()].Where(t=>t.IsDeleted==false).ToList();

            if(paxHotelList == null)
               paxHotelList = new List<PreflightLegPassengerHotelViewModel>();

           data["meta"] = new PagingMetaData() { Page = paxHotelList.Count / 10, Size = 10, total_items = paxHotelList.Count };
           data["results"] = paxHotelList;

           preflightTrip.CurrentLegNUM = LegNum;
           HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = preflightTrip;
           return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> DeletePreflightLegPaxHotelByLegNum(int legNum, string hotelCode, bool isArrival)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;

            PreflightTripViewModel preflightTrip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = preflightTrip;
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            List<PreflightLegPassengerHotelViewModel> paxHotelList = preflightTrip.PreflightLegPassengersLogisticsHotelList.Where(t => t.Key == legNum.ToString()).Select(h => h.Value).FirstOrDefault();
            if (paxHotelList == null)
            {
                paxHotelList = new List<PreflightLegPassengerHotelViewModel>();
                preflightTrip.PreflightLegPassengersLogisticsHotelList.Add(legNum.ToString(), paxHotelList);
            }
            if (paxHotelList.Where(t => t.HotelCode == hotelCode && t.isArrivalHotel == isArrival && t.IsDeleted==false).Count() > 0)
            {
                foreach (var item in paxHotelList.Where(t => t.HotelCode == hotelCode && t.isArrivalHotel == isArrival).ToList())
                {
                    if (item.PreflightHotelListID != 0)
                    {
                        item.State = TripEntityState.Deleted;
                        item.IsDeleted = true;
                    }
                    else
                    {
                        paxHotelList.RemoveAt(paxHotelList.FindIndex(t => t.HotelCode == hotelCode && t.isArrivalHotel == isArrival));                        
                    }
                }
                preflightTrip.PreflightLegPassengersLogisticsHotelList[legNum.ToString()]= paxHotelList;
            }
            result.Result = "1";
            return result;
        }

        private static Dictionary<string, List<PreflightLegPassengerHotelViewModel>> setPreflightLegPaxHotelList(PreflightTripViewModel pfViewModel, List<PreflightLeg> list)
        {
            Dictionary<string, List<PreflightLegPassengerHotelViewModel>> tripPaxHotelListByLeg = new Dictionary<string, List<PreflightLegPassengerHotelViewModel>>();
            if (list != null)
            {
                foreach (var leg in list)
                {
                    List<PreflightLegPassengerHotelViewModel> hotelList = new List<PreflightLegPassengerHotelViewModel>();

                    tripPaxHotelListByLeg[leg.LegNUM.ToString()] = hotelList;

                    foreach (var hotel in leg.PreflightHotelLists.Where(t => t.crewPassengerType.ToUpper().Trim() == "P" && t.IsDeleted == false))
                    {
                        PreflightLegPassengerHotelViewModel passengerHotel = (PreflightLegPassengerHotelViewModel)new PreflightLegPassengerHotelViewModel().InjectFrom(hotel);
                        passengerHotel.LegNUM = (int)hotel.PreflightLeg.LegNUM;
                        passengerHotel.CountryID = hotel.CountryID;
                        passengerHotel.PAXCode = string.Join(",", hotel.PAXIDList.ToArray());
                        if (hotel.isDepartureHotel == null)
                        {
                            passengerHotel.isDepartureHotel = !(hotel.isArrivalHotel.HasValue && hotel.isArrivalHotel.Value);
                        }
                        else if (hotel.isArrivalHotel == null)
                        {
                            passengerHotel.isArrivalHotel = !(hotel.isDepartureHotel.HasValue && hotel.isDepartureHotel.Value);
                        }
                        if (hotel.IsAllCrewOrPax.HasValue && hotel.IsAllCrewOrPax == true)
                        {
                            passengerHotel.PAXCode = "All";
                        }
                        else if (hotel.PAXIDList != null && hotel.PAXIDList.Count > 0)
                        {

                            string paxCodes = string.Join(",", pfViewModel.PreflightLegPassengers[Convert.ToString(leg.LegNUM)].ToList().Where(p => hotel.PAXIDList.Contains(p.PassengerID)).Select(r => r.PassengerRequestorCD).ToArray());
                            passengerHotel.PAXCode = paxCodes;
                        }else if(hotel.IsAllCrewOrPax==null || hotel.IsAllCrewOrPax == false)
                        {
                            passengerHotel.PAXCode = "None";
                            hotel.IsAllCrewOrPax = false;
                        }
                        passengerHotel.PreflightHotelListID = hotel.PreflightHotelListID;
                        hotelList.Add(passengerHotel);
                    }
                }
            }
            return tripPaxHotelListByLeg;
        }

        private static Dictionary<string, List<PassengerViewModel>> setPreflightLegPassengers(PreflightTripViewModel trip, List<PreflightLeg> leglist)
        {
            Dictionary<string, List<PassengerViewModel>> passengers = new Dictionary<string, List<PassengerViewModel>>();
            if (trip.PreflightLegPassengers == null || trip.PreflightLegPassengers.Count < 0 || leglist==null)
            {
                return passengers;
            }
            foreach (var leg in leglist)
            {
                List<PassengerViewModel> passengerList= new List<PassengerViewModel>();
                foreach (var preflightPassneger in leg.PreflightPassengerLists.Where(t=>t.IsDeleted==false))
                {
                    PassengerViewModel PassengerVM = new PassengerViewModel();
                    PassengerVM =(PassengerViewModel)PassengerVM.InjectFrom(preflightPassneger);
                    passengerList.Add(PassengerVM);
                    using (PreflightServiceClient PrefService = new PreflightServiceClient())
                    {
                        List<GetPassenger> _getPassenger = null;
                        var objRetVal = PrefService.GetPassengerbyIDOrCD(Convert.ToInt64(PassengerVM.PassengerID), string.Empty);
                        if (objRetVal != null)
                        {
                            if (objRetVal.ReturnFlag == true)
                                _getPassenger = objRetVal.EntityList;

                            if (_getPassenger != null && _getPassenger.Count > 0)
                            {
                                var objPaxRetVal = _getPassenger[0];

                                if (objPaxRetVal != null)
                                {
                                    PassengerVM.PassengerRequestorCD = objPaxRetVal.PassengerRequestorCD;
                                    PassengerVM.Notes = objPaxRetVal.Notes;
                                    PassengerVM.PassengerAlert = objPaxRetVal.PassengerAlert;
                                }
                            }
                        }
                    }
                }
                passengers[leg.LegNUM.ToString()] = passengerList;
            }
            return passengers;
        }

        private static Dictionary<String, PreflightLegPassengerTransportViewModel> SetPreflightLegPassengerTransportListToVM(PreflightMain Trip)
        {            
            Dictionary<String, PreflightLegPassengerTransportViewModel> legsPreflightLegPassengerTransportViewModels = new Dictionary<String, PreflightLegPassengerTransportViewModel>();
            if (Trip == null)
            {
                return legsPreflightLegPassengerTransportViewModels;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        foreach (PreflightLeg leg in Trip.GetLiveLegs())
                        {
                            PreflightTransportList LegDepTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsDepartureTransport == true).FirstOrDefault();
                            PreflightTransportList LegArrTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsArrivalTransport == true).FirstOrDefault();

                            PreflightLegPassengerTransportViewModel transVM = new PreflightLegPassengerTransportViewModel();
                            String departTransCode = "";
                            String arrivalTransCode = "";

                            if (LegDepTansport != null && LegDepTansport.AirportID == leg.DepartICAOID && LegDepTansport.AirportID != null && LegDepTansport.TransportID.HasValue)
                            {
                                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(LegDepTansport.TransportID)).EntityList;
                                    if (objRetVal != null && objRetVal.Count > 0)
                                    {
                                        departTransCode = objRetVal[0].TransportCD;
                                    }
                                }
                                transVM.DepartureAirportId = LegDepTansport.AirportID.Value;
                                transVM.PreflightDepartTransportId = LegDepTansport.PreflightTransportID;
                                transVM.DepartTransportID = LegDepTansport.TransportID.Value;
                                transVM.DepartCode = departTransCode;
                                transVM.DepartName = LegDepTansport.PreflightTransportName;
                                transVM.DepartRate = LegDepTansport.PhoneNum4; //omg!, this is stored in phone number 4 field :(
                                transVM.DepartPhone = LegDepTansport.PhoneNum1;
                                transVM.DepartFax = LegDepTansport.FaxNUM;
                                transVM.DepartComments = LegDepTansport.Comments;
                                transVM.DepartConfirmation = LegDepTansport.ConfirmationStatus;
                                transVM.SelectedDepartTransportStatus = LegDepTansport.Status;
                            }
                            if (LegArrTansport != null && LegArrTansport.AirportID == leg.ArriveICAOID && LegArrTansport.AirportID != null && LegArrTansport.TransportID.HasValue)
                            {
                                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(LegArrTansport.TransportID)).EntityList;
                                    if (objRetVal != null && objRetVal.Count > 0)
                                    {
                                        arrivalTransCode = objRetVal[0].TransportCD;
                                    }
                                }
                                transVM.ArrivalAirportId = LegArrTansport.AirportID.Value;
                                transVM.PreflightArrivalTransportId = LegArrTansport.PreflightTransportID;
                                transVM.ArriveTransportID = LegArrTansport.TransportID.Value;
                                transVM.ArrivalCode = arrivalTransCode;
                                transVM.ArrivalName = LegArrTansport.PreflightTransportName;
                                transVM.ArrivalRate = LegArrTansport.PhoneNum4;//omg!, this is stored in phone number 4 field :(
                                transVM.ArrivalPhone = LegArrTansport.PhoneNum1;
                                transVM.ArrivalFax = LegArrTansport.FaxNUM;
                                transVM.ArrivalComments = LegArrTansport.Comments;
                                transVM.ArrivalConfirmation = LegArrTansport.ConfirmationStatus;
                                transVM.SelectedArrivalTransportStatus = LegArrTansport.Status;
                            }

                            legsPreflightLegPassengerTransportViewModels[leg.LegNUM.ToString()] = transVM;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
                return legsPreflightLegPassengerTransportViewModels;
            }
        }

        private static PreflightMain SetPreflightLegPaxTransportationVMToList(PreflightMain Trip, Dictionary<String, PreflightLegPassengerTransportViewModel> legsPreflightLegPassengerTransportViewModels)
        {
            if (Trip == null)
            {
                return null;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        foreach (String strLegNum in legsPreflightLegPassengerTransportViewModels.Keys)
                        {
                            long legNum = 0;
                            long.TryParse(strLegNum, out legNum);
                            PreflightLegPassengerTransportViewModel transVM = legsPreflightLegPassengerTransportViewModels[strLegNum];
                            
                            PreflightLeg leg = Trip.PreflightLegs.Where(l => l.LegNUM == legNum).FirstOrDefault();
                            if (leg != null &&  leg.IsDeleted==false)
                            {
                                if(leg.PreflightTransportLists==null)
                                    leg.PreflightTransportLists= new List<PreflightTransportList>();

                                PreflightTransportList LegDepTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsDepartureTransport == true).FirstOrDefault();
                                PreflightTransportList LegArrTansport = leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsArrivalTransport == true).FirstOrDefault();
                                bool departureNew = false, arrivalNew = false;
                               
                                if (transVM != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(transVM.DepartCode) && transVM.DepartTransportID > 0)
                                    {
                                        if (LegDepTansport == null && (!string.IsNullOrWhiteSpace(transVM.DepartCode)))
                                        {
                                            departureNew = true;
                                            LegDepTansport = new PreflightTransportList();                                            
                                        }
                                        LegDepTansport.State =  transVM.PreflightDepartTransportId > 0 ? TripEntityState.Modified: TripEntityState.Added; 
                                       

                                        LegDepTansport.PreflightTransportID = transVM.PreflightDepartTransportId;
                                        LegDepTansport.TransportID = transVM.DepartTransportID;                                        
                                        LegDepTansport.PreflightTransportName = transVM.DepartName;
                                        LegDepTansport.PhoneNum4 = transVM.DepartRate;
                                        LegDepTansport.PhoneNum1 = transVM.DepartPhone;
                                        LegDepTansport.FaxNUM = transVM.DepartFax;
                                        LegDepTansport.Comments = transVM.DepartComments;
                                        LegDepTansport.IsDepartureTransport = true;
                                        LegDepTansport.IsArrivalTransport = false;
                                        LegDepTansport.AirportID = transVM.DepartureAirportId;
                                        LegDepTansport.CrewPassengerType = "P";
                                        LegDepTansport.ConfirmationStatus = transVM.DepartConfirmation;
                                        LegDepTansport.Status = transVM.SelectedDepartTransportStatus;

                                    }
                                    if (!string.IsNullOrWhiteSpace(transVM.ArrivalCode) && transVM.ArriveTransportID>0)
                                    {
                                        if (LegArrTansport == null)
                                        {
                                            arrivalNew = true;
                                            LegArrTansport = new PreflightTransportList();                                            
                                        }
                                        LegArrTansport.State = transVM.PreflightArrivalTransportId > 0 ? TripEntityState.Modified : TripEntityState.Added; 

                                        LegArrTansport.PreflightTransportID = transVM.PreflightArrivalTransportId;                                        
                                        LegArrTansport.TransportID = transVM.ArriveTransportID;                                        
                                        LegArrTansport.PreflightTransportName = transVM.ArrivalName;
                                        LegArrTansport.PhoneNum4 = transVM.ArrivalRate;
                                        LegArrTansport.PhoneNum1 = transVM.ArrivalPhone;
                                        LegArrTansport.FaxNUM = transVM.ArrivalFax;
                                        LegArrTansport.Comments = transVM.ArrivalComments;
                                        LegArrTansport.ConfirmationStatus = transVM.ArrivalConfirmation;
                                        LegArrTansport.Status = transVM.SelectedArrivalTransportStatus;
                                        LegArrTansport.IsDepartureTransport = false;
                                        LegArrTansport.IsArrivalTransport = true;
                                        LegArrTansport.CrewPassengerType = "P";
                                        LegArrTansport.AirportID = transVM.ArrivalAirportId;

                                    }
                                }
                                if (arrivalNew)
                                {
                                    leg.PreflightTransportLists.Add(LegArrTansport);
                                }
                                if (departureNew)
                                {
                                    leg.PreflightTransportLists.Add(LegDepTansport);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }                
                return Trip;
            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable PaxSelectionGrid()
        {
            Hashtable data = new Hashtable();

            PreflightPaxLite Pax = new PreflightPaxLite();
            var list = Pax.LoadPaxSelectionDataFromTrip()  ;//Pax.LoadPaxfromTrip(true);
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = list.Count(), total_items = list.Count() };
                    data["results"] = list;
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable PaxSummaryGrid()
        {
            Hashtable data = new Hashtable();

            PreflightPaxLite Pax = new PreflightPaxLite();
            var list = Pax.LoadPaxSummaryDataFromTrip()  ;//Pax.LoadPaxfromTrip(false);
            if (list != null)
            {
                HttpContext.Current.Session["SelectedPaxList"] = list;

                if (list.Count() > 0)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = list.Count(), total_items = list.Count() };
                    data["results"] = list;
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable TripPaxSelectionLegs()
        {
            Hashtable data = new Hashtable();
            List<Hashtable> Legs = new List<Hashtable>();
            
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                string CompDateFormat = "MM/dd/yyyy";
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {

                    foreach (var Leg in Trip.PreflightLegs.Where(t=>t.IsDeleted==false).OrderBy(p => p.LegNUM.Value))
                    {
                        Hashtable LegTemp = new Hashtable();
                        var lblLegDate = "";
                        var lblLegDepart = "";


                        #region Crew Leg header shoud should show local departure date

                        if (Leg.DepartureDTTMLocal != null)
                        {
                            string strDate = String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", Leg.DepartureDTTMLocal);
                            lblLegDate = System.Web.HttpUtility.HtmlEncode(strDate);
                        }


                        #endregion
                        if (Leg.DepartureAirport != null && Leg.ArrivalAirport != null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode(Convert.ToString(Leg.DepartureAirport.IcaoID) + "-" + Convert.ToString(Leg.ArrivalAirport.IcaoID));
                        }
                        if (Leg.DepartureAirport == null && Leg.ArrivalAirport != null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode(string.Empty + "-" + Convert.ToString(Leg.ArrivalAirport.IcaoID));
                        }
                        if (Leg.DepartureAirport != null && Leg.ArrivalAirport == null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode(Convert.ToString(Leg.DepartureAirport.IcaoID) + "-" + string.Empty);
                        }

                        LegTemp["isDeadCategory"] = Leg.isDeadCategory;
                        LegTemp["LegDepart"] = lblLegDepart;
                        LegTemp["LegDate"] = lblLegDate;
                        LegTemp["LegNUM"] = Leg.LegNUM;
                        LegTemp["PassengerTotal"] = Leg.PassengerTotal??0;
                        LegTemp["ReservationAvailable"] = (Leg.SeatTotal == null ? 0 : Leg.SeatTotal) - (Leg.PassengerTotal == null ? 0 : Leg.PassengerTotal) ?? 0;
                        LegTemp["PassengerTotalWithPurpose"] = Trip.PreflightLegPassengers.ContainsKey(Leg.LegNUM.ToString()) == true ? Trip.PreflightLegPassengers[Leg.LegNUM.ToString()].Count : 0;

                        var PaxListSelect = new Dictionary<string,object>();

                        if (Trip.PreflightLegPassengers.Keys.Contains(Leg.LegNUM.ToString()))
                        {
                            var objPaxList = new PreflightPaxLite().GetPaxListofGrid();
                            var Paxs = Trip.PreflightLegPassengers[Leg.LegNUM.ToString()].Where(t=>t.IsDeleted==false).ToList();
                            foreach (var pax in Paxs.OrderBy(p => p.OrderNUM.Value))
                            {
                                PaxListSelect[pax.PassengerRequestorCD]= pax.FlightPurposeID;                            
                                var PaxSummaryPax = objPaxList.FirstOrDefault(p => p.LegNUM == Leg.LegNUM && (p.PaxCode == pax.PaxCode || p.PaxCode == pax.PassengerRequestorCD));
                                if (PaxSummaryPax != null)
                                    PaxSummaryPax.FlightPurposeID = pax.FlightPurposeID;
                                
                            }
                        }
                        LegTemp["PaxList"] = PaxListSelect;
                        Legs.Add(LegTemp);
                    }
                    Dictionary<string, string> PurposeList = new Dictionary<string, string>();

                    CacheLite cachedData = new CacheLite();
                    PurposeList= cachedData.GetFlightpurposeListFromCache();
                    if (PurposeList!=null && PurposeList.Count <= 0)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient PurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = PurposeService.GetFlightPurposeList();
                            if (objRetVal != null)
                            {
                                if (objRetVal.ReturnFlag == true && objRetVal.EntityList != null && objRetVal.EntityList.Count > 0)
                                {
                                    PurposeList = objRetVal.EntityList.ToDictionary(p => p.FlightPurposeCD, p => p.FlightPurposeID.ToString());
                                    cachedData.AddFlightPurposeListToCache(PurposeList);
                                }
                            }
                        }
                    }
                    data["PurposeList"] = PurposeList;
                    data["results"] = Legs;
                    return data;
                }
            }
            data["results"] = new List<object>();
            return data;
        }

        #endregion

        #region Preflight Logistics Module
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLogisticsViewModel> GetPreflightLogisticsByLegNum(string legNum)
        {
            FSSOperationResult<PreflightLogisticsViewModel> result = new FSSOperationResult<PreflightLogisticsViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightTripViewModel pTripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            PreflightLogisticsViewModel preflightLogistics = new PreflightLogisticsViewModel();
            if (pTripVM == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
                pTripVM = new PreflightTripViewModel();
            }
            if (pTripVM.PreflightLegPreflightLogisticsList == null || pTripVM.PreflightLegPreflightLogisticsList.Count == 0)
            {
                pTripVM.PreflightLegPreflightLogisticsList = new Dictionary<string, PreflightLogisticsViewModel>();                   
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pTripVM;
            }

            if (pTripVM.PreflightLegPreflightLogisticsList.ContainsKey(legNum) == true)
            {
                preflightLogistics = pTripVM.PreflightLegPreflightLogisticsList[legNum];
                PreflightLegViewModel Leg =pTripVM.PreflightLegs.FirstOrDefault(l => l.LegNUM.Value == Convert.ToInt64(legNum) && l.IsDeleted == false);
                preflightLogistics.FuelVendorText = Leg.LegFuelComment;
                if (preflightLogistics.PreflightDepFboListViewModel.AirportID != Leg.DepartICAOID)
                {
                    preflightLogistics.PreflightDepFboListViewModel = PreflightUtils.SetIsChoiceFBOToLogistics(preflightLogistics.PreflightDepFboListViewModel, Leg.DepartICAOID);
                    preflightLogistics.PreflightDepFboListViewModel.State = preflightLogistics.PreflightDepFboListViewModel.PreflightFBOID > 0 ? TripEntityState.Modified : TripEntityState.Added;
                }

                if (preflightLogistics.PreflightArrFboListViewModel.AirportID != Leg.ArriveICAOID)
                {
                    preflightLogistics.PreflightArrFboListViewModel = PreflightUtils.SetIsChoiceFBOToLogistics(preflightLogistics.PreflightArrFboListViewModel, Leg.ArriveICAOID);
                    preflightLogistics.PreflightArrFboListViewModel.State = preflightLogistics.PreflightArrFboListViewModel.PreflightFBOID > 0 ? TripEntityState.Modified : TripEntityState.Added;
                }
            }
            else
            {
                pTripVM.PreflightLegPreflightLogisticsList = GetAFreshLogisticsList(pTripVM.PreflightLegs, pTripVM.PreflightLegPreflightLogisticsList);
                if (pTripVM.PreflightLegPreflightLogisticsList.ContainsKey(legNum) == true)
                    preflightLogistics = pTripVM.PreflightLegPreflightLogisticsList[legNum]; 
            }
            pTripVM.CurrentLegNUM = Convert.ToInt32(legNum);
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pTripVM;

            preflightLogistics.PreflightLeg = pTripVM.PreflightLegs.FirstOrDefault(y => y.LegNUM.ToString() == legNum && y.IsDeleted==false);
            result.Result = preflightLogistics;
            return result;
        }

        private static Dictionary<string, PreflightLogisticsViewModel> GetAFreshLogisticsList(List<PreflightLegViewModel> Legs, Dictionary<string, PreflightLogisticsViewModel> LogisticsList)
        {
            
            foreach(PreflightLegViewModel leg in Legs.Where(t=>t.IsDeleted==false).ToList())
            {
                if (!LogisticsList.ContainsKey(Convert.ToString(leg.LegNUM)) && leg.DepartICAOID.HasValue && leg.DepartICAOID.Value > 0 && leg.ArriveICAOID.HasValue && leg.ArriveICAOID.Value > 0 )
                {
                    PreflightLogisticsViewModel logistics = new PreflightLogisticsViewModel();

                    logistics.PreflightArrFboListViewModel = new PreflightFBOListViewModel();
                    logistics.PreflightArrFboListViewModel.State = TripEntityState.NoChange;
                    logistics.PreflightArrFboListViewModel.IsDepartureFBO = false;
                    logistics.PreflightArrFboListViewModel.IsArrivalFBO = true;

                    logistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                    logistics.PreflightDepFboListViewModel.State = TripEntityState.NoChange;
                    logistics.PreflightDepFboListViewModel.IsDepartureFBO = true;
                    logistics.PreflightDepFboListViewModel.IsArrivalFBO = false;

                    logistics.ArrivalLogisticsCatering = new PreflightCateringDetailViewModel();
                    logistics.ArrivalLogisticsCatering.ArriveDepart = "A";
                    logistics.ArrivalLogisticsCatering.State = TripEntityState.NoChange;

                    logistics.DepartLogisticsCatering = new PreflightCateringDetailViewModel();
                    logistics.DepartLogisticsCatering.ArriveDepart = "D";
                    logistics.DepartLogisticsCatering.State = TripEntityState.NoChange;

                    #region "Depart FBO"
                    if (Framework.Helpers.MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightFBO))
                    {
                        #region "Add Depart FBO"
                        if (leg.DepartICAOID != null && leg.DepartICAOID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID((long)leg.DepartICAOID)
                                    .EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false)
                                    .ToList();

                            if (ObjRetValGroup != null && ObjRetValGroup.Count > 0)
                            {
                                logistics.PreflightDepFboListViewModel.AirportID = leg.DepartICAOID;
                                logistics.PreflightDepFboListViewModel.FBOCD = ObjRetValGroup[0].FBOCD;
                                logistics.PreflightDepFboListViewModel.FBOID = ObjRetValGroup[0].FBOID;
                                logistics.PreflightDepFboListViewModel.Addr1 = ObjRetValGroup[0].Addr1;
                                logistics.PreflightDepFboListViewModel.Addr2 = ObjRetValGroup[0].Addr2;
                                logistics.PreflightDepFboListViewModel.Addr3 = ObjRetValGroup[0].Addr3;
                                logistics.PreflightDepFboListViewModel.EmailAddress = ObjRetValGroup[0].EmailAddress;
                                logistics.PreflightDepFboListViewModel.UNICOM = ObjRetValGroup[0].UNICOM;
                                logistics.PreflightDepFboListViewModel.ARINC = ObjRetValGroup[0].ARINC;
                                logistics.PreflightDepFboListViewModel.IsArrivalFBO = false;
                                logistics.PreflightDepFboListViewModel.IsDepartureFBO = true;
                                logistics.PreflightDepFboListViewModel.PreflightFBOName = ObjRetValGroup[0].FBOVendor;
                                logistics.PreflightDepFboListViewModel.CityName = ObjRetValGroup[0].CityName;
                                logistics.PreflightDepFboListViewModel.StateName = ObjRetValGroup[0].StateName;
                                logistics.PreflightDepFboListViewModel.PostalZipCD = ObjRetValGroup[0].PostalZipCD;
                                logistics.PreflightDepFboListViewModel.PhoneNum1 = ObjRetValGroup[0].PhoneNUM1;
                                logistics.PreflightDepFboListViewModel.PhoneNum2 = ObjRetValGroup[0].PhoneNUM2;
                                logistics.PreflightDepFboListViewModel.FaxNUM = ObjRetValGroup[0].FaxNum;
                                logistics.PreflightDepFboListViewModel.FBOInformation = ObjRetValGroup[0].FBOVendor + "," + ObjRetValGroup[0].CityName + "," +
                                                    ObjRetValGroup[0].StateName + "," + ObjRetValGroup[0].PostalZipCD + "," +
                                                    ObjRetValGroup[0].PhoneNUM1 + "," + ObjRetValGroup[0].PhoneNUM2 + "," +
                                                    ObjRetValGroup[0].FaxNum + "," + ObjRetValGroup[0].UNICOM + "," +
                                                    ObjRetValGroup[0].ARINC;
                                logistics.PreflightDepFboListViewModel.State = TripEntityState.Added;
                            }
                        }

                        #endregion
                    }
                    #endregion

                    #region "Arrive FBO"
                    if (Framework.Helpers.MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightFBO))
                    {
                        #region "Add Arrival FBO"
                        if (leg.ArriveICAOID != null && leg.ArriveICAOID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup =
                                ObjService.GetAllFBOByAirportID((long)leg.ArriveICAOID)
                                    .EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false)
                                    .ToList();

                            if (ObjRetValGroup != null && ObjRetValGroup.Count > 0)
                            {
                                logistics.PreflightArrFboListViewModel.AirportID = leg.ArriveICAOID;
                                logistics.PreflightArrFboListViewModel.FBOCD = ObjRetValGroup[0].FBOCD;
                                logistics.PreflightArrFboListViewModel.FBOID = ObjRetValGroup[0].FBOID;

                                logistics.PreflightArrFboListViewModel.Addr1 = ObjRetValGroup[0].Addr1;
                                logistics.PreflightArrFboListViewModel.Addr2 = ObjRetValGroup[0].Addr2;
                                logistics.PreflightArrFboListViewModel.Addr3 = ObjRetValGroup[0].Addr3;
                                logistics.PreflightArrFboListViewModel.EmailAddress = ObjRetValGroup[0].EmailAddress;
                                logistics.PreflightArrFboListViewModel.UNICOM = ObjRetValGroup[0].UNICOM;
                                logistics.PreflightArrFboListViewModel.ARINC = ObjRetValGroup[0].ARINC;

                                logistics.PreflightArrFboListViewModel.IsArrivalFBO = true;
                                logistics.PreflightArrFboListViewModel.IsDepartureFBO = false;
                                logistics.PreflightArrFboListViewModel.PreflightFBOName = ObjRetValGroup[0].FBOVendor;
                                logistics.PreflightArrFboListViewModel.CityName = ObjRetValGroup[0].CityName;
                                logistics.PreflightArrFboListViewModel.StateName = ObjRetValGroup[0].StateName;
                                logistics.PreflightArrFboListViewModel.PostalZipCD = ObjRetValGroup[0].PostalZipCD;
                                logistics.PreflightArrFboListViewModel.PhoneNum1 = ObjRetValGroup[0].PhoneNUM1;
                                logistics.PreflightArrFboListViewModel.PhoneNum2 = ObjRetValGroup[0].PhoneNUM2;
                                logistics.PreflightArrFboListViewModel.FaxNUM = ObjRetValGroup[0].FaxNum;
                                logistics.PreflightArrFboListViewModel.FBOInformation = ObjRetValGroup[0].FBOVendor + "," + ObjRetValGroup[0].CityName + "," +
                                                    ObjRetValGroup[0].StateName + "," + ObjRetValGroup[0].PostalZipCD + "," +
                                                    ObjRetValGroup[0].PhoneNUM1 + "," + ObjRetValGroup[0].PhoneNUM2 + "," +
                                                    ObjRetValGroup[0].FaxNum + "," + ObjRetValGroup[0].UNICOM + "," +
                                                    ObjRetValGroup[0].ARINC;
                                logistics.PreflightArrFboListViewModel.State = TripEntityState.Added;
                            }
                        }

                        #endregion

                    }
                    #endregion

                    logistics.FuelVendorText = leg.LegFuelComment;
                    
                    LogisticsList[Convert.ToString(leg.LegNUM)] = logistics;
                }
            }
            return LogisticsList;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePreflightLogisticsModel(PreflightLogisticsViewModel preflightLogistics)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel pTripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (pTripVM == null)
            {
                pTripVM = new PreflightTripViewModel();
            }
            if (pTripVM.PreflightLegPreflightLogisticsList == null)
            {
                pTripVM.PreflightLegPreflightLogisticsList = new Dictionary<string, PreflightLogisticsViewModel>();
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pTripVM;
            }
            if (preflightLogistics.PreflightLeg.LegNUM!=null && pTripVM.PreflightLegPreflightLogisticsList.ContainsKey(preflightLogistics.PreflightLeg.LegNUM.ToString()) == true)
            {
                var curentLeg = pTripVM.PreflightLegs.Where(F => F.LegNUM == preflightLogistics.PreflightLeg.LegNUM).ToList();
                if (curentLeg != null && curentLeg.Count > 0)
                    curentLeg[0].EstFuelQTY = preflightLogistics.PreflightLeg.EstFuelQTY;
                pTripVM.PreflightLegPreflightLogisticsList[preflightLogistics.PreflightLeg.LegNUM.ToString()] = preflightLogistics;
                            var leg = pTripVM.PreflightLegs.Where(p => p.LegNUM == preflightLogistics.PreflightLeg.LegNUM && p.IsDeleted == false).First();
            leg.LegFuelComment = preflightLogistics.FuelVendorText;
            }
            else if(preflightLogistics.PreflightLeg.LegNUM!=null)
            {
                var curentLeg = pTripVM.PreflightLegs.Where(F => F.LegNUM == preflightLogistics.PreflightLeg.LegNUM).ToList();
                if (curentLeg != null && curentLeg.Count > 0)
                    curentLeg[0].EstFuelQTY = preflightLogistics.PreflightLeg.EstFuelQTY;
                pTripVM.PreflightLegPreflightLogisticsList.Add(preflightLogistics.PreflightLeg.LegNUM.ToString(), preflightLogistics);
            }
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pTripVM;
            result.Result = "1";
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetLogisticsFuelInfo(bool isEditMode,bool isUpdateButtonClick,string legNum)
        {
            if (isUpdateButtonClick)
            {
                return UpdateFuelClick(isEditMode, legNum);
            }
            else
            {
                Hashtable data = new Hashtable();
                if (Framework.Helpers.MiscUtils.IsAuthorized(Permission.Preflight.ViewPreflightFuel))
                {
                    PreflightTripViewModel Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    PreflightLogisticsViewModel preflightLogisticsVM;
                    if (Trip.PreflightLegPreflightLogisticsList.ContainsKey(legNum))
                    {
                        preflightLogisticsVM = Trip.PreflightLegPreflightLogisticsList[legNum];
                        Trip.PreflightLegPreflightLogisticsList[legNum] = preflightLogisticsVM;   
                    }
                    else
                    {
                        preflightLogisticsVM = new PreflightLogisticsViewModel();
                        Trip.PreflightLegPreflightLogisticsList.Add(legNum, preflightLogisticsVM);

                    }
                    int lilegNum = 0;
                    int.TryParse(legNum, out lilegNum);
                    preflightLogisticsVM.PreflightLeg = Trip.PreflightLegs[lilegNum - 1];
                    
                    var fuelData = BindFuelData(isEditMode, preflightLogisticsVM.PreflightLeg.DepartICAOID, preflightLogisticsVM.PreflightLeg.DepartureAirport.IcaoID, preflightLogisticsVM.PreflightLeg.ArrivalAirport.IcaoID);
                    data["meta"] = new PagingMetaData() { Page = 1, Size = fuelData.Count, total_items = fuelData.Count };
                    data["results"] = fuelData;
                    return data;
                }
                return data;
            }
        }
        private static List<FlightpakFuelDataViewModel> BindFuelData(bool isEditmode, long? DepartAirportID, string departureIcao, string arriveICAO)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartAirportID))
            {
                bool isUpdateFuel = false;
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip.PreflightMain == null)
                {
                    Trip.PreflightMain = new PreflightMainViewModel();
                    Trip.PreflightLegs = new List<PreflightLegViewModel>();

                }

                using (PreflightServiceClient objService = new PreflightServiceClient())
                {

                    if (Trip != null)
                    {
                        if (Trip.PreflightMain.TripID == 0) // check
                            isUpdateFuel = true;
                        //Check whether Record exists in preflightfuel table for this trip if not call the UWA services to get the Fueldetails for departure Airportid

                        var ReturnValue = objService.GetFlightPakFuelData(Trip.PreflightMain.TripID, (long)DepartAirportID);
                        List<FlightPakFuelData> returnlist = new List<FlightPakFuelData>();
                        if (ReturnValue.ReturnFlag == true)
                        {
                            returnlist = ReturnValue.EntityList;

                            bool checkUWAhitdone = false;
                            if (Trip.PreflightMain.TripID == 0)
                            {
                                List<string> DepartICaoids = new List<string>();
                                List<string> ArriveICaoids = new List<string>();
                                DepartICaoids.Add(departureIcao);
                                ArriveICaoids.Add(arriveICAO);
                                var UWAServiceCall = objService.GetFuelPrices(DepartICaoids,(long) DepartAirportID);
                                ReturnValue = objService.GetFlightPakFuelData(Trip.PreflightMain.TripID, (long)DepartAirportID);
                                returnlist = null;
                                returnlist = new List<FlightPakFuelData>();
                                if (ReturnValue.EntityList != null && ReturnValue.EntityList.Count > 0)
                                {
                                    returnlist = ReturnValue.EntityList;
                                }
                            }
                            else
                            {
                                if (ReturnValue.EntityList == null || (ReturnValue.EntityList != null && ReturnValue.EntityList.Count == 0))
                                {
                                    if (isEditmode)
                                    {
                                        #region new code
                                        if (Trip.PreflightLegs != null)
                                        {
                                            List<PreflightLegViewModel> LegList = Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList();
                                            if (LegList != null)
                                            {
                                                foreach (PreflightLegViewModel Leg in LegList)
                                                {
                                                    List<string> DepartICaoids = new List<string>();
                                                    if (Leg.DepartureAirport!= null)
                                                    {
                                                        DepartICaoids.Add(Leg.DepartureAirport.IcaoID);
                                                        var UWAServiceCall = objService.GetFuelPrices(DepartICaoids, (long)Leg.DepartICAOID);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        objService.UpdatePreflightFuelDetails(Trip.PreflightMain.TripID);
                                    }
                                    ReturnValue = objService.GetFlightPakFuelData((long)Trip.PreflightMain.TripID, (long)DepartAirportID);
                                    returnlist = null;
                                    returnlist = new List<FlightPakFuelData>();
                                    if (ReturnValue.EntityList != null && ReturnValue.EntityList.Count > 0)
                                    {
                                        returnlist = ReturnValue.EntityList;
                                    }
                                }
                            }
                            returnlist = returnlist.ToList().Distinct().ToList();
                            List<FlightpakFuelDataViewModel> fuelList = new List<FlightpakFuelDataViewModel>();
                            //fuelList.InjectFrom(returnlist.OrderByDescending(x => x.EffectiveDT).ToList());
                            returnlist = returnlist.OrderByDescending(x => x.EffectiveDT).ToList();
                            int cnt=0;
                            foreach (var item in returnlist)
                            {
                                FlightpakFuelDataViewModel fvm= new FlightpakFuelDataViewModel();
                                fvm.InjectFrom(item);
                                fvm.Id = cnt; cnt++;
                                fuelList.Add(fvm);                                
                            }
                            return fuelList;              
                        }
                    }
                }
                return null;
            }
        }


       
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable UpdateFuelClick(bool isEditmode,string selectedLegNum)
        {
            Hashtable data = new Hashtable();
            List<FlightpakFuelDataViewModel> listFuelData = new List<FlightpakFuelDataViewModel>();

            using (PreflightServiceClient PrefSvcClient = new PreflightServiceClient())
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip != null)
                {
                        if (isEditmode)
                        {
                            #region new code
                            if (Trip.PreflightLegs != null)
                            {
                                List<PreflightLegViewModel> LegList = Trip.PreflightLegs.Where(p => p.IsDeleted == false).ToList();
                                if (LegList != null)
                                {
                                    foreach (PreflightLegViewModel Leg in LegList)
                                    {
                                        List<string> DepartICaoids = new List<string>();
                                        if (Leg.DepartureAirport != null)
                                        {
                                            DepartICaoids.Add(Leg.DepartureAirport.IcaoID);
                                            var UWAServiceCall = PrefSvcClient.GetFuelPrices(DepartICaoids, (long)Leg.DepartICAOID);
                                        }
                                    }
                                }
                            }
                            #endregion

                            PrefSvcClient.UpdatePreflightFuelDetails(Trip.PreflightMain.TripID);
                        }
                        if (Trip.PreflightLegs != null)
                        {
                            PreflightLogisticsViewModel preflightLogisticsVM = Trip.PreflightLegPreflightLogisticsList.FirstOrDefault().Value;
                            var CurrentLeg=Trip.PreflightLegs.Where(l=>l.LegNUM == Convert.ToInt64(selectedLegNum) && l.IsDeleted==false).FirstOrDefault();
                            if (CurrentLeg != null)
                            {
                                if (preflightLogisticsVM == null)
                                {
                                    preflightLogisticsVM = new PreflightLogisticsViewModel();
                                    preflightLogisticsVM.PreflightLeg = CurrentLeg;
                                    Trip.PreflightLegPreflightLogisticsList[(selectedLegNum).ToString()] = preflightLogisticsVM;
                                }
                                var fuelData = BindFuelData(isEditmode, (long)CurrentLeg.DepartICAOID, CurrentLeg.DepartureAirport.IcaoID, CurrentLeg.ArrivalAirport.IcaoID);

                                listFuelData = fuelData;
                            }
                        }                        
                    
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                }
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = listFuelData.Count, total_items = listFuelData.Count };
            data["results"] = listFuelData;
            return data;
        }
        
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> GetFBOBestPrice(long departureAirportID, decimal gallon)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable data = new Hashtable();
            using (PreflightServiceClient PrefSvcClient = new PreflightServiceClient())
            {
                long tripId = (long)((PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip]).TripId;
                var BestFBOPriceList = PrefSvcClient.GetFBOBestPrice(tripId, departureAirportID, gallon);
                data["ReturnFlag"] = BestFBOPriceList.ReturnFlag;
                data["Count"] = BestFBOPriceList.EntityList.Count;
                if (gallon > 0)
                {
                    data["results"] = BestFBOPriceList.EntityList;
                }
                else
                {
                    data["results"] = BestFBOPriceList.EntityList.Where(x => x.GallonFrom == 0 && x.GallonTO == 0).OrderBy(x => x.UnitPrice).ToList();
                }
                
            }
            result.Result = data;
            return result;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> GetFuelVendorTextForLogisticsFuel(long fuelVendorID, string hdDepartIcaoID, string fboName)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable data = new Hashtable();
            data["flag"] = false;
            using (MasterCatalogServiceClient MasterClient = new MasterCatalogServiceClient())
            {
                var FuelVendorText = MasterClient.GetfuelVendorText(fuelVendorID, Convert.ToDecimal(0), Convert.ToDecimal(0), Convert.ToInt64(hdDepartIcaoID), Convert.ToString(fboName));
                if (FuelVendorText.ReturnFlag)
                {
                    if (FuelVendorText != null && FuelVendorText.EntityList != null && FuelVendorText.EntityList.Count > 0)
                    {
                        data["flag"] = true;
                        data["results"] = FuelVendorText.EntityList[0].FuelVendorText;
                    }
                }
            }
            result.Result = data;
            return result;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> UpdateAllLegsForEstimateGallon(string legNum)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            //Handle methods throguh exception manager with return flag
            var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                PreflightLogisticsViewModel preLogistics = Trip.PreflightLegPreflightLogisticsList[legNum];
                if (Trip != null && Trip.PreflightLegs != null)
                {
                    foreach (PreflightLegViewModel Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                    {
                        Trip.PreflightLegPreflightLogisticsList[Leg.LegNUM.ToString()].FuelAmountType=preLogistics.FuelAmountType;                        
                        Leg.EstFuelQTY = preLogistics.PreflightLeg.EstFuelQTY;                        
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }, FlightPak.Common.Constants.Policy.UILayer);
            result.Result = true;
            return result;
        }

        private static Dictionary<string, PreflightLogisticsViewModel> setPreflightLogisticsCateringDetails(PreflightTripViewModel trip, List<PreflightLeg> tripLeg) 
        {
            Dictionary<string, PreflightLogisticsViewModel> tripPreflightLogistics= new Dictionary<string,PreflightLogisticsViewModel>();
            if (tripLeg != null)
            {
                foreach (var leg in tripLeg)
                {
                    PreflightLogisticsViewModel PreflightLogistics = new PreflightLogisticsViewModel();
                    PreflightLogistics.PreflightLeg = (PreflightLegViewModel)new PreflightLegViewModel().InjectFrom(leg);
                    var DepartFbo = leg.PreflightFBOLists.Where(t => t.IsDepartureFBO == true).FirstOrDefault();
                    if (DepartFbo != null)
                    {
                        PreflightLogistics.PreflightDepFboListViewModel = (PreflightFBOListViewModel)new PreflightFBOListViewModel().InjectFrom(DepartFbo == null ? new PreflightFBOList() : DepartFbo);
                        if (DepartFbo.FBO != null)
                        {
                            PreflightLogistics.PreflightDepFboListViewModel.FBOCD = DepartFbo.FBO.FBOCD;
                            PreflightLogistics.PreflightDepFboListViewModel.UNICOM = DepartFbo.FBO.UNICOM;
                            PreflightLogistics.PreflightDepFboListViewModel.ARINC = DepartFbo.FBO.ARINC;
                            PreflightLogistics.PreflightDepFboListViewModel.Addr1 = DepartFbo.FBO.Addr1;
                            PreflightLogistics.PreflightDepFboListViewModel.Addr2 = DepartFbo.FBO.Addr2;
                            PreflightLogistics.PreflightDepFboListViewModel.Addr3 = DepartFbo.FBO.Addr3;
                            PreflightLogistics.PreflightDepFboListViewModel.CityName = DepartFbo.FBO.CityName;
                            PreflightLogistics.PreflightDepFboListViewModel.StateName = DepartFbo.FBO.StateName;
                            PreflightLogistics.PreflightDepFboListViewModel.EmailAddress = DepartFbo.FBO.EmailAddress;
                            PreflightLogistics.PreflightDepFboListViewModel.PostalZipCD = DepartFbo.FBO.PostalZipCD;
                            PreflightLogistics.PreflightDepFboListViewModel.PhoneNum1 = DepartFbo.FBO.PhoneNUM1;
                            PreflightLogistics.PreflightDepFboListViewModel.PhoneNum2 = DepartFbo.FBO.PhoneNUM2;

                        }
                    }
                    else
                    {
                        PreflightLogistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                    }
                    var ArrivalFbo = leg.PreflightFBOLists.Where(t => t.IsArrivalFBO == true).FirstOrDefault();
                    if (ArrivalFbo != null)
                    {
                        PreflightLogistics.PreflightArrFboListViewModel = (PreflightFBOListViewModel)new PreflightFBOListViewModel().InjectFrom(ArrivalFbo == null ? new PreflightFBOList() : ArrivalFbo);
                        if (ArrivalFbo.FBO != null)
                        {
                            PreflightLogistics.PreflightArrFboListViewModel.FBOCD = ArrivalFbo.FBO.FBOCD;
                            PreflightLogistics.PreflightArrFboListViewModel.UNICOM = ArrivalFbo.FBO.UNICOM;
                            PreflightLogistics.PreflightArrFboListViewModel.ARINC = ArrivalFbo.FBO.ARINC;
                            PreflightLogistics.PreflightArrFboListViewModel.Addr1 = ArrivalFbo.FBO.Addr1;
                            PreflightLogistics.PreflightArrFboListViewModel.Addr2 = ArrivalFbo.FBO.Addr2;
                            PreflightLogistics.PreflightArrFboListViewModel.Addr3 = ArrivalFbo.FBO.Addr3;
                            PreflightLogistics.PreflightArrFboListViewModel.CityName = ArrivalFbo.FBO.CityName;
                            PreflightLogistics.PreflightArrFboListViewModel.StateName = ArrivalFbo.FBO.StateName;
                            PreflightLogistics.PreflightArrFboListViewModel.EmailAddress = ArrivalFbo.FBO.EmailAddress;
                            PreflightLogistics.PreflightArrFboListViewModel.PostalZipCD = ArrivalFbo.FBO.PostalZipCD;
                            PreflightLogistics.PreflightArrFboListViewModel.PhoneNum1 = ArrivalFbo.FBO.PhoneNUM1;
                            PreflightLogistics.PreflightArrFboListViewModel.PhoneNum2 = ArrivalFbo.FBO.PhoneNUM2;

                        }
                    }
                    else
                    {
                        PreflightLogistics.PreflightArrFboListViewModel = new PreflightFBOListViewModel();
                    }
                    var DepartCatering = leg.PreflightCateringDetails.Where(t => t.ArriveDepart == "D").FirstOrDefault();
                    var ArriveCatering = leg.PreflightCateringDetails.Where(t => t.ArriveDepart != "D").FirstOrDefault();


                    PreflightLogistics.DepartLogisticsCatering = (PreflightCateringDetailViewModel)new PreflightCateringDetailViewModel().InjectFrom(DepartCatering == null ? new PreflightCateringDetail() : DepartCatering);
                    PreflightLogistics.ArrivalLogisticsCatering = (PreflightCateringDetailViewModel)new PreflightCateringDetailViewModel().InjectFrom(ArriveCatering == null ? new PreflightCateringDetail() : ArriveCatering);
                    if (DepartCatering != null && DepartCatering.Catering != null)
                    {
                        PreflightLogistics.DepartLogisticsCatering.CateringCD = DepartCatering.Catering.CateringCD;
                        PreflightLogistics.DepartLogisticsCatering.CateringID = DepartCatering.Catering.CateringID;
                        PreflightLogistics.DepartLogisticsCatering.AirportID = (long)DepartCatering.Catering.AirportID;

                    }
                    if (ArriveCatering != null && ArriveCatering.Catering != null)
                    {
                        PreflightLogistics.ArrivalLogisticsCatering.CateringCD = ArriveCatering.Catering.CateringCD;
                        PreflightLogistics.ArrivalLogisticsCatering.CateringID = ArriveCatering.Catering.CateringID;
                        PreflightLogistics.ArrivalLogisticsCatering.AirportID = (long)ArriveCatering.Catering.AirportID;
                    }
                    PreflightLogistics.LegNUM = leg.LegNUM;
                    tripPreflightLogistics[leg.LegNUM.ToString()] = PreflightLogistics;
                }
            }
            return tripPreflightLogistics;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> btnDepartRetainCommentConfirmClick(PreflightLogisticsViewModel preLogistics, bool isYesClick)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable data = new Hashtable();
            data["fbocodemsg"] = "";
            data["open"] = "";
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preLogistics))
            {
                //Handle methods throguh exception manager with return flag
                var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!string.IsNullOrEmpty(preLogistics.PreflightDepFboListViewModel.FBOCD))
                    {
                        #region Search FBO
                        using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(preLogistics.PreflightLeg.DepartureAirport.AirportID.ToString()))
                            {

                                var ServCall = objDstsvc.GetFBOByAirportWithFilters(Convert.ToInt64(preLogistics.PreflightLeg.DepartureAirport.AirportID), 0, preLogistics.PreflightDepFboListViewModel.FBOCD.Trim(), false, false, false);
                                if (ServCall.ReturnFlag && ServCall.EntityList != null && ServCall.EntityList.Count > 0)
                                {
                                    // RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg's Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                    var objRetVal = ServCall.EntityList.ToList();
                                    preLogistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                                    if (!string.IsNullOrEmpty(objRetVal[0].FBOCD))
                                    {
                                        preLogistics.PreflightDepFboListViewModel = (PreflightFBOListViewModel)preLogistics.PreflightDepFboListViewModel.InjectFrom(objRetVal[0]);

                                    }
                                    preLogistics.PreflightDepFboListViewModel = (PreflightFBOListViewModel)preLogistics.PreflightDepFboListViewModel.InjectFrom(objRetVal[0]);

                                    if (!isYesClick)
                                    {
                                        preLogistics.PreflightDepFboListViewModel.ConfirmationStatus = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        preLogistics.PreflightDepFboListViewModel.Comments = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                    }

                                    if (!string.IsNullOrEmpty(objRetVal[0].FBOVendor))
                                        preLogistics.PreflightDepFboListViewModel.PreflightFBOName = System.Web.HttpUtility.HtmlEncode(objRetVal[0].FBOVendor.ToString());

                                    if (!string.IsNullOrEmpty(objRetVal[0].PhoneNUM1))
                                        preLogistics.PreflightDepFboListViewModel.PhoneNum1 = System.Web.HttpUtility.HtmlEncode(objRetVal[0].PhoneNUM1.ToString());

                                    if (!string.IsNullOrEmpty(objRetVal[0].PhoneNUM2))
                                        preLogistics.PreflightDepFboListViewModel.PhoneNum2 = System.Web.HttpUtility.HtmlEncode(objRetVal[0].PhoneNUM2.ToString());

                                    if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                        preLogistics.PreflightDepFboListViewModel.FaxNUM = System.Web.HttpUtility.HtmlEncode(objRetVal[0].FaxNum.ToString());



                                    if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                                    {
                                        
                                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count() > 0)
                                        {
                                            long currentLegnum = (long)Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].LegNUM;
                                            if (currentLegnum != null && currentLegnum > 0)
                                            {
                                                PreflightLegViewModel prevleg = Trip.PreflightLegs.Where(x => x.LegNUM == currentLegnum - 1 && x.IsDeleted == false).FirstOrDefault();
                                                if (prevleg != null)
                                                {
                                                    PreflightLogisticsViewModel previousLogisticsVM = Trip.PreflightLegPreflightLogisticsList[prevleg.LegNUM.ToString()];
                                                    if (Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].DepartICAOID == prevleg.ArriveICAOID)
                                                    {
                                                        if (previousLogisticsVM.PreflightArrFboListViewModel != null)
                                                        {

                                                            if ((previousLogisticsVM.PreflightArrFboListViewModel == null || previousLogisticsVM.PreflightArrFboListViewModel.FBOCD == null) || (previousLogisticsVM.PreflightArrFboListViewModel != null && previousLogisticsVM.PreflightArrFboListViewModel.FBOCD != null && previousLogisticsVM.PreflightArrFboListViewModel.FBOCD != preLogistics.PreflightDepFboListViewModel.FBOCD))
                                                            {
                                                                data["open"] = "confirmPreviousFBOCallBackFnConfirmBoxOpen";//("Do you want to update the prior leg’s Arrival FBO?", "confirmPreviousFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            data["open"] = "confirmPreviousFBOCallBackFnConfirmBoxOpen"; //RadWindowManager1.RadConfirm("Do you want to update the prior leg’s Arrival FBO?", "confirmPreviousFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    data["fbocodemsg"] = System.Web.HttpUtility.HtmlDecode("FBO/Handler Code Does Not Exist");
                                    string fbocd = preLogistics.PreflightDepFboListViewModel.FBOCD;
                                    preLogistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                                    preLogistics.PreflightDepFboListViewModel.FBOCD = fbocd;
                                    preLogistics.PreflightDepFboListViewModel.FBOID = 0;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        data["fbocodemsg"] = "";
                        string fbocd = preLogistics.PreflightDepFboListViewModel.FBOCD;
                        preLogistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                        preLogistics.PreflightDepFboListViewModel.FBOCD = fbocd;
                        preLogistics.PreflightDepFboListViewModel.FBOID = 0;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
            // Update Session Logistics Model
            
            var PLogisticsVM = Trip.PreflightLegPreflightLogisticsList[preLogistics.PreflightLeg.LegNUM.ToString()];
            PLogisticsVM = preLogistics;
            result.Result = data;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> btnArrivalCaterRetainCommentYes_Click(PreflightLogisticsViewModel preLogistics, bool isYesClick)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preLogistics,isYesClick))
            {
                Hashtable data = new Hashtable();
                data["cateringcodemsg"] = "";
                data["open"] = "";
                
                    //Handle methods throguh exception manager with return flag
                    var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!isYesClick)
                        {
                            preLogistics.ArrivalLogisticsCatering.CateringConfirmation = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            preLogistics.ArrivalLogisticsCatering.CateringComments = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                        if (!string.IsNullOrEmpty(preLogistics.ArrivalLogisticsCatering.CateringCD))
                        {

                            preLogistics.ArrivalLogisticsCatering.Cost = 0;
                            preLogistics.ArrivalLogisticsCatering.ContactName = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.ContactPhone = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.ContactFax = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.CateringContactName = string.Empty;                            
                            
                            #region Search Arrive Cater
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {


                                if (!string.IsNullOrEmpty(preLogistics.PreflightLeg.ArriveICAOID.ToString()))
                                {
                                    var ServCall = objDstsvc.GetAllCateringByAirportIDWithFilters((long)preLogistics.PreflightLeg.ArriveICAOID, 0, preLogistics.ArrivalLogisticsCatering.CateringCD.Trim(), false, false);
                                    if (ServCall.ReturnFlag && ServCall.EntityList != null && ServCall.EntityList.Count > 0)
                                    {
                                            var objRetVal = ServCall.EntityList;
                                        
                                            if (!string.IsNullOrEmpty(objRetVal[0].CateringCD))
                                                preLogistics.ArrivalLogisticsCatering.CateringCD= objRetVal[0].CateringCD.ToString();
                                            // hdDeptCaterAirID.Value = ((FlightPak.Web.FlightPakMasterService.Catering)CateringList[0]).AirportID.ToString();
                                            preLogistics.ArrivalLogisticsCatering.CateringID = objRetVal[0].CateringID;
                                            if (objRetVal[0].NegotiatedRate != null)
                                                preLogistics.ArrivalLogisticsCatering.Cost= objRetVal[0].NegotiatedRate;
                                            if (!string.IsNullOrEmpty(objRetVal[0].CateringVendor))
                                                preLogistics.ArrivalLogisticsCatering.ContactName= objRetVal[0].CateringVendor.ToString();
                                            if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                preLogistics.ArrivalLogisticsCatering.ContactPhone= objRetVal[0].PhoneNum.ToString();
                                            if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                preLogistics.ArrivalLogisticsCatering.ContactFax = objRetVal[0].FaxNum.ToString();
                                            if (!string.IsNullOrEmpty(objRetVal[0].ContactName))
                                            {

                                                preLogistics.ArrivalLogisticsCatering.CateringContactName = objRetVal[0].ContactName.ToString();
                                            }
                                    }
                                    else
                                    {

                                        data["cateringcodemsg"] = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                        preLogistics.ArrivalLogisticsCatering.Cost = null;
                                        preLogistics.ArrivalLogisticsCatering.ContactName = string.Empty;
                                        preLogistics.ArrivalLogisticsCatering.ContactPhone = string.Empty;
                                        preLogistics.ArrivalLogisticsCatering.ContactFax = string.Empty;
                                        preLogistics.ArrivalLogisticsCatering.CateringID = null;
                                        preLogistics.ArrivalLogisticsCatering.CateringContactName = string.Empty;                                            
                                       
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            data["cateringcodemsg"] = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.Cost = null;
                            preLogistics.ArrivalLogisticsCatering.ContactName = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.ContactPhone = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.ContactFax = string.Empty;
                            preLogistics.ArrivalLogisticsCatering.CateringID = null;
                            preLogistics.ArrivalLogisticsCatering.CateringContactName = string.Empty;                                            
                                       
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    // Update Session Logistics Model
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    var PLogisticsVM=Trip.PreflightLegPreflightLogisticsList[preLogistics.PreflightLeg.LegNUM.ToString()];
                    PLogisticsVM = preLogistics;
                    result.Result = data;
                    return result;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> btnDepartCaterRetainCommentYes_Click(PreflightLogisticsViewModel preLogistics, bool isYesClick)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preLogistics,isYesClick))
            {
                Hashtable data = new Hashtable();
                data["cateringcodemsg"] = "";
                data["open"] = "";
                
                    //Handle methods throguh exception manager with return flag
                    var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(preLogistics.DepartLogisticsCatering.CateringCD))
                        {
                            preLogistics.DepartLogisticsCatering.Cost= null;
                            preLogistics.DepartLogisticsCatering.ContactName = string.Empty;
                            preLogistics.DepartLogisticsCatering .ContactPhone= string.Empty;
                            preLogistics.DepartLogisticsCatering.ContactFax = string.Empty;
                            preLogistics.DepartLogisticsCatering.CateringContactName = string.Empty;

                            #region Search Catering
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(preLogistics.PreflightLeg.DepartICAOID.ToString()))
                                {
                                    var ServCall = objDstsvc.GetAllCateringByAirportIDWithFilters((long)preLogistics.PreflightLeg.DepartICAOID, 0, preLogistics.DepartLogisticsCatering.CateringCD, false, false);
                                    if (ServCall.ReturnFlag && ServCall.EntityList!=null && ServCall.EntityList.Count>0)
                                    {
                                            var objRetVal = ServCall.EntityList;
                                        
                                            if (!string.IsNullOrEmpty(objRetVal[0].CateringCD))
                                                preLogistics.DepartLogisticsCatering.CateringCD = objRetVal[0].CateringCD.ToString();
                                            // hdDeptCaterAirID.Value = ((FlightPak.Web.FlightPakMasterService.Catering)CateringList[0]).AirportID.ToString();
                                            preLogistics.DepartLogisticsCatering.CateringID= objRetVal[0].CateringID;
                                            if (objRetVal[0].NegotiatedRate != null)
                                                preLogistics.DepartLogisticsCatering.Cost= objRetVal[0].NegotiatedRate;
                                            if (!string.IsNullOrEmpty(objRetVal[0].CateringVendor))
                                                preLogistics.DepartLogisticsCatering.ContactName= objRetVal[0].CateringVendor.ToString();
                                            if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                preLogistics.DepartLogisticsCatering.ContactPhone= objRetVal[0].PhoneNum.ToString();
                                            if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                preLogistics.DepartLogisticsCatering.ContactFax= objRetVal[0].FaxNum.ToString();
                                            if (!string.IsNullOrEmpty(objRetVal[0].ContactName))
                                            {
                                                preLogistics.DepartLogisticsCatering.CateringContactName = objRetVal[0].ContactName.ToString();
                                            }
                                                                               
                                    }
                                    else
                                    {
                                        data["cateringcodemsg"] = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                        preLogistics.DepartLogisticsCatering.Cost = null;
                                        preLogistics.DepartLogisticsCatering.ContactName = string.Empty;
                                        preLogistics.DepartLogisticsCatering.ContactPhone = string.Empty;
                                        preLogistics.DepartLogisticsCatering.ContactFax = string.Empty;
                                        preLogistics.DepartLogisticsCatering.CateringID = null;
                                        preLogistics.DepartLogisticsCatering.CateringContactName = string.Empty;     
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            preLogistics.DepartLogisticsCatering.Cost = null;
                            preLogistics.DepartLogisticsCatering.ContactName = string.Empty;
                            preLogistics.DepartLogisticsCatering.ContactPhone = string.Empty;
                            preLogistics.DepartLogisticsCatering.ContactFax = string.Empty;
                            preLogistics.DepartLogisticsCatering.CateringID = null;
                            preLogistics.DepartLogisticsCatering.CateringContactName = string.Empty;     
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    // Update Session Logistics Model
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    var PLogisticsVM = Trip.PreflightLegPreflightLogisticsList[preLogistics.PreflightLeg.LegNUM.ToString()];
                    PLogisticsVM = preLogistics;
                    result.Result = data;
                    return result;
            }
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> btnArrivalRetainCommentYes_Click(PreflightLogisticsViewModel preLogistics, bool isYesClick)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preLogistics, isYesClick))
            {
                Hashtable data = new Hashtable();
                data["fbocodemsg"] = "";
                data["open"] = "";
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

                    //Handle methods throguh exception manager with return flag
                    var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(preLogistics.PreflightArrFboListViewModel.FBOCD))
                        {
                            #region SearchFBO
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(preLogistics.PreflightLeg.ArriveICAOID.ToString()))
                                {
                                    var ServCall = objDstsvc.GetFBOByAirportWithFilters((long)preLogistics.PreflightLeg.ArriveICAOID, 0, preLogistics.PreflightArrFboListViewModel.FBOCD.Trim(), false, false, false);
                                    if (ServCall.ReturnFlag && ServCall.EntityList != null && ServCall.EntityList.Count > 0)
                                    {

                                        var objRetVal = ServCall.EntityList;
                                            if (!string.IsNullOrEmpty(objRetVal[0].FBOCD))
                                                preLogistics.PreflightArrFboListViewModel.FBOCD= objRetVal[0].FBOCD.ToString();
                                            preLogistics.PreflightArrFboListViewModel.FBOID = objRetVal[0].FBOID;
                                            
                                            
                                            if (!string.IsNullOrEmpty(objRetVal[0].FBOVendor))
                                                preLogistics.PreflightArrFboListViewModel.PreflightFBOName = System.Web.HttpUtility.HtmlEncode(objRetVal[0].FBOVendor.ToString());
                                            

                                            preLogistics.PreflightArrFboListViewModel =(PreflightFBOListViewModel) preLogistics.PreflightArrFboListViewModel.InjectFrom(objRetVal[0]);

                                            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                                            {
                                                

                                                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count() > 0)
                                                {
                                                    long currentLegnum = (long)Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].LegNUM;

                                                    if (currentLegnum != null)
                                                    {
                                                        PreflightLegViewModel nextLeg = Trip.PreflightLegs.Where(x => x.LegNUM == currentLegnum + 1 && x.IsDeleted == false).FirstOrDefault();

                                                        if (nextLeg != null)
                                                        {
                                                            if (Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].ArriveICAOID == nextLeg.DepartICAOID)
                                                            {
                                                                PreflightLogisticsViewModel NextPreLogistics = new PreflightLogisticsViewModel();
                                                                if (!Trip.PreflightLegPreflightLogisticsList.ContainsKey(nextLeg.LegNUM.ToString()))
                                                                    Trip.PreflightLegPreflightLogisticsList.Add(nextLeg.LegNUM.ToString(),NextPreLogistics);

                                                                NextPreLogistics= Trip.PreflightLegPreflightLogisticsList[nextLeg.LegNUM.ToString()];
                                                                if (NextPreLogistics.PreflightDepFboListViewModel!=null)
                                                                {
                                                                    
                                                                    var nextFBO = NextPreLogistics.PreflightDepFboListViewModel;
                                                                    if ((nextFBO == null || (nextFBO != null && nextFBO.FBOCD != null && nextFBO.FBOCD != preLogistics.PreflightArrFboListViewModel.FBOCD)))
                                                                    {
                                                                        data["open"] = "confirmNextFBOCallBackFnConfirmBoxOpen";
                                                                        //RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg’s Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                         data["open"] = "confirmNextFBOCallBackFnConfirmBoxOpen";
                                                                        //RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg’s Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                                                            
                                    }
                                    else
                                    {
                                        data["fbocodemsg"] = System.Web.HttpUtility.HtmlDecode("FBO/Handler Code Does Not Exist");
                                        string fbocd = preLogistics.PreflightDepFboListViewModel.FBOCD;
                                        preLogistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                                        preLogistics.PreflightDepFboListViewModel.FBOCD = fbocd;
                                        preLogistics.PreflightDepFboListViewModel.FBOID = 0;

                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            data["fbocodemsg"] = System.Web.HttpUtility.HtmlDecode("FBO/Handler Code Does Not Exist");
                            string fbocd = preLogistics.PreflightDepFboListViewModel.FBOCD;
                            preLogistics.PreflightDepFboListViewModel = new PreflightFBOListViewModel();
                            preLogistics.PreflightDepFboListViewModel.FBOCD = fbocd;
                            preLogistics.PreflightDepFboListViewModel.FBOID = 0;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    // Update Session Logistics Model

                    var PLogisticsVM = Trip.PreflightLegPreflightLogisticsList[preLogistics.PreflightLeg.LegNUM.ToString()];
                    PLogisticsVM = preLogistics;
                    result.Result = data;
                    return result;
            }
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> btnNextFBOYesClick(PreflightLogisticsViewModel preLogistics)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count() > 0)
                {
                    var legNum = Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].LegNUM;
                    if(legNum != null) {
                        long currentLegnum = (long)legNum;

                        PreflightFBOListViewModel currentLegArrFbo = new PreflightFBOListViewModel();                                        
                        currentLegArrFbo = preLogistics.PreflightArrFboListViewModel;
                        currentLegArrFbo.IsDepartureFBO = false;
                        currentLegArrFbo.IsArrivalFBO = true;

                        PreflightLegViewModel nextLeg = Trip.PreflightLegs.FirstOrDefault(x => x.LegNUM == currentLegnum + 1 && x.IsDeleted == false);
                        if (!Trip.PreflightLegPreflightLogisticsList.ContainsKey(nextLeg.LegNUM.ToString()))
                            Trip.PreflightLegPreflightLogisticsList.Add(nextLeg.LegNUM.ToString(), new ViewModels.PreflightLogisticsViewModel());

                        var nextLegLogistics = Trip.PreflightLegPreflightLogisticsList[nextLeg.LegNUM.ToString()];
                        if (nextLeg != null)
                        {
                            #region fbo

                            if (nextLegLogistics.PreflightDepFboListViewModel != null && nextLegLogistics.PreflightDepFboListViewModel.FBOID != null && nextLegLogistics.PreflightDepFboListViewModel.FBOID!=0)
                            {
                                if (Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].ArriveICAOID == nextLeg.DepartICAOID)
                                {
                                    long preflightfboid = nextLegLogistics.PreflightDepFboListViewModel.PreflightFBOID;
                                    nextLegLogistics.PreflightDepFboListViewModel = SetFBOInformation(currentLegArrFbo, nextLeg.LegID,true);
                                    nextLegLogistics.PreflightDepFboListViewModel.PreflightFBOID = preflightfboid;

                                    if (Trip.TripId != null && Trip.TripId > 0)
                                        nextLegLogistics.PreflightDepFboListViewModel.State = TripEntityState.Modified;
                                    else
                                        nextLegLogistics.PreflightDepFboListViewModel.State = TripEntityState.Added;
                                }
                            }
                            else
                            {
                                if (currentLegArrFbo != null)
                                {
                                    long preflightfboid = nextLegLogistics.PreflightDepFboListViewModel.PreflightFBOID;
                                    nextLegLogistics.PreflightDepFboListViewModel = SetFBOInformation(currentLegArrFbo, nextLeg.LegID,true);
                                    nextLegLogistics.PreflightDepFboListViewModel.PreflightFBOID = preflightfboid;

                                    if (Trip.TripId != null && Trip.TripId > 0)
                                        nextLegLogistics.PreflightDepFboListViewModel.State = TripEntityState.Modified;
                                    else
                                        nextLegLogistics.PreflightDepFboListViewModel.State = TripEntityState.Added;
                                }
                            }
                            #endregion
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> btnPreviousFBOYesClick(PreflightLogisticsViewModel preLogistics)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count() > 0)
                {
                    long currentLegnum = (long)Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].LegNUM;

                    PreflightFBOListViewModel currentLegDepFbo = new PreflightFBOListViewModel();
                    
                    currentLegDepFbo.IsDepartureFBO = true;
                    currentLegDepFbo.IsArrivalFBO = false;
                    
                    currentLegDepFbo.FBOID = preLogistics.PreflightDepFboListViewModel.FBOID;
                    currentLegDepFbo.AirportID = preLogistics.PreflightLeg.DepartureAirport.AirportID;
                    currentLegDepFbo.FBOInformation = System.Web.HttpUtility.HtmlDecode(getCommaSeparatedFBOInformation(preLogistics.PreflightDepFboListViewModel));
                    currentLegDepFbo.PreflightFBOName = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.PreflightFBOName);
                    currentLegDepFbo.CityName = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.CityName);
                    currentLegDepFbo.StateName = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.StateName);
                    currentLegDepFbo.PostalZipCD = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.PostalZipCD);
                    currentLegDepFbo.PhoneNum1 = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.PhoneNum1);
                    currentLegDepFbo.PhoneNum2 = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.PhoneNum2);
                    currentLegDepFbo.IsDepartureFBO = true;
                    currentLegDepFbo.IsArrivalFBO = false;
                    currentLegDepFbo.FBOCD = string.IsNullOrEmpty(preLogistics.PreflightDepFboListViewModel.FBOCD) ? null : preLogistics.PreflightDepFboListViewModel.FBOCD;
                    currentLegDepFbo.FaxNUM = System.Web.HttpUtility.HtmlDecode(preLogistics.PreflightDepFboListViewModel.FaxNUM);                    
                    currentLegDepFbo.Comments = preLogistics.PreflightDepFboListViewModel.Comments;
                    currentLegDepFbo.ConfirmationStatus = preLogistics.PreflightDepFboListViewModel.ConfirmationStatus;
                  
                    PreflightLegViewModel previousLeg = Trip.PreflightLegs.FirstOrDefault(x => x.LegNUM == currentLegnum - 1 && x.IsDeleted == false);

                    if (previousLeg != null)
                    {
                        #region fbo
                        PreflightLogisticsViewModel previousLogistic= Trip.PreflightLegPreflightLogisticsList[previousLeg.LegNUM.ToString()];
                        if (previousLogistic.PreflightArrFboListViewModel != null)
                        {

                            if (Trip.PreflightLegs[Convert.ToInt16(preLogistics.PreflightLeg.LegNUM.ToString()) - 1].DepartICAOID == previousLeg.ArriveICAOID)
                            {
                                    var previousfbo=previousLogistic.PreflightArrFboListViewModel;                                                                        
                                    PreflightFBOListViewModel CopyFbo = new PreflightFBOListViewModel();
                                    CopyFbo=currentLegDepFbo;                                    
                                    CopyFbo.Comments = previousfbo.Comments;
                                    CopyFbo.ConfirmationStatus = previousfbo.ConfirmationStatus;
                                    CopyFbo.IsDepartureFBO = false;
                                    CopyFbo.IsArrivalFBO = true;                                                                        
                                    CopyFbo.LegID = previousLeg.LegID;
                                    previousLogistic.PreflightArrFboListViewModel=CopyFbo;
                                    previousLogistic.PreflightArrFboListViewModel.PreflightFBOID = previousfbo.PreflightFBOID;
                             }
                        }
                        else
                        {
                            if (preLogistics.PreflightDepFboListViewModel  != null)
                            {
                                previousLogistic.PreflightArrFboListViewModel = SetFBOInformation(currentLegDepFbo,previousLeg.LegID,false);
                            }
                        }
                        #endregion
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }
        
        private static PreflightFBOListViewModel SetFBOInformation(PreflightFBOListViewModel CurrentLegFBO,long NextLegId,bool IsDepartOrArrival)
        {
            PreflightFBOListViewModel NewFBO = new PreflightFBOListViewModel();
            NewFBO.FBOID = CurrentLegFBO.FBOID;
            NewFBO.AirportID = CurrentLegFBO.AirportID;
            NewFBO.FBOInformation = System.Web.HttpUtility.HtmlDecode(getCommaSeparatedFBOInformation(CurrentLegFBO));
            NewFBO.PreflightFBOName = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.PreflightFBOName);
            NewFBO.CityName = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.CityName);
            NewFBO.StateName = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.StateName);
            NewFBO.PostalZipCD = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.PostalZipCD);
            NewFBO.PhoneNum1 = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.PhoneNum1);
            NewFBO.PhoneNum2 = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.PhoneNum2);
            NewFBO.FBOCD = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.FBOCD);
            NewFBO.FaxNUM = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.FaxNUM);
            NewFBO.Addr1 = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.Addr1);
            NewFBO.Addr2 = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.Addr2);
            NewFBO.Addr3 = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.Addr3);
            NewFBO.ARINC = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.ARINC);
            NewFBO.UNICOM = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.UNICOM);
            NewFBO.EmailAddress = System.Web.HttpUtility.HtmlDecode(CurrentLegFBO.EmailAddress);

            NewFBO.NoUpdated = CurrentLegFBO.NoUpdated;
            NewFBO.Comments = string.Empty;
            NewFBO.ConfirmationStatus = string.Empty;
            NewFBO.FBOArrangedBy = CurrentLegFBO.FBOArrangedBy != null && CurrentLegFBO.FBOArrangedBy.Value;
            NewFBO.IsCompleted = CurrentLegFBO.IsCompleted;
            if(IsDepartOrArrival)
            {
                NewFBO.IsDepartureFBO = true;
                NewFBO.IsArrivalFBO = false;
            }
            else
            {
                NewFBO.IsDepartureFBO = false;
                NewFBO.IsArrivalFBO = true;    
            }
            NewFBO.LegID = NextLegId;
            return NewFBO;
        }
        private static String getCommaSeparatedFBOInformation(PreflightFBOListViewModel FBOInfo)
        {
            return System.Web.HttpUtility.HtmlDecode(string.Join(",", new List<string> {  FBOInfo.PreflightFBOName, 
                                                                                          FBOInfo.CityName, 
                                                                                          FBOInfo.StateName, 
                                                                                          FBOInfo.PostalZipCD, 
                                                                                          FBOInfo.PhoneNum1, 
                                                                                          FBOInfo.PhoneNum2, 
                                                                                          FBOInfo.FaxNUM, 
                                                                                          FBOInfo.UNICOM, 
                                                                                          FBOInfo.ARINC, 
                                                                                          FBOInfo.Addr1, 
                                                                                          FBOInfo.Addr2, 
                                                                                          FBOInfo.Addr3, 
                                                                                          FBOInfo.Addr3, 
                                                                                          FBOInfo.EmailAddress }));
        }

        private static void SaveLogisticsViewModelIntoMainTrip(PreflightMain mainTrip,PreflightLogisticsViewModel logistics)
        {
            var leg=mainTrip.PreflightLegs.Where(t=>t.LegNUM==logistics.LegNUM && t.IsDeleted==false).FirstOrDefault();
            if (leg == null)
            {
                return;
            }   
            // Arrive FBO Inject To EntityContext
            PreflightFBOList preflightFbo = leg.PreflightFBOLists.Where(f=>f.IsArrivalFBO==true && f.IsDeleted==false).FirstOrDefault();
            if(preflightFbo!=null)
                preflightFbo = (PreflightFBOList)preflightFbo.InjectFrom(logistics.PreflightArrFboListViewModel);

            // Departure FBO Inject To EntityContext
            PreflightFBOList preflightFboDepart = leg.PreflightFBOLists.Where(f => f.IsDepartureFBO == true && f.IsDeleted == false).FirstOrDefault();
            if (preflightFbo != null)
                preflightFboDepart = (PreflightFBOList)preflightFbo.InjectFrom(logistics.PreflightDepFboListViewModel);


            // Arrive Careting Inject To EntityContext
            PreflightCateringDetail arrivalCatering = leg.PreflightCateringDetails.Where(r => r.ArriveDepart == "A" && r.IsDeleted == false).FirstOrDefault();
            if (arrivalCatering != null)
                arrivalCatering = (PreflightCateringDetail)arrivalCatering.InjectFrom(logistics.ArrivalLogisticsCatering);

            PreflightCateringDetail departCatering = leg.PreflightCateringDetails.Where(r => r.ArriveDepart == "D" && r.IsDeleted == false).FirstOrDefault();
            if (departCatering != null)
                departCatering = (PreflightCateringDetail)departCatering.InjectFrom(logistics.DepartLogisticsCatering);

        }

        #endregion

        #region Preflight LegOutBoundDetails
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable TripLegsDetail()
        {
            Hashtable data = new Hashtable();
            PreflightLegViewModel customleg = new PreflightLegViewModel();
            PreflightLegsLite Legs = new PreflightLegsLite();
            var dv = Legs.LoadLegDetailsfromTrip();

            if (dv.Table != null)
            {
                var dt = dv.ToTable();
                if (dt != null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = dt.Rows.Count, total_items = dt.Rows.Count };
                    data["results"] = Legs.GetJson(dv);
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegOutboundInstructionViewModel> GetLegOutboundInstruction(int LegNum)
        {
            FSSOperationResult<PreflightLegOutboundInstructionViewModel> result = new FSSOperationResult<PreflightLegOutboundInstructionViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightLegOutboundInstructionViewModel pflobViewModel = new PreflightLegOutboundInstructionViewModel();

            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                pflobViewModel.EditMode = pfViewModel.PreflightMain.EditMode;
                pflobViewModel.CurrentLegOutBoundData = pfViewModel.PreflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false && x.LegNUM == LegNum).FirstOrDefault();

                if (pflobViewModel.CurrentLegOutBoundData != null && pflobViewModel.CurrentLegOutBoundData.PreflightTripOutbounds != null && pflobViewModel.CurrentLegOutBoundData.PreflightTripOutbounds.Count > 0)
                {
                    foreach (PreflightTripOutboundViewModel prefoutbound in pflobViewModel.CurrentLegOutBoundData.PreflightTripOutbounds)
                    {

                        switch (prefoutbound.OutboundInstructionNUM)
                        {
                            case 1:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction1 = prefoutbound.OutboundDescription;
                                break;
                            case 2:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction2 = prefoutbound.OutboundDescription;
                                break;
                            case 3:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction3 = prefoutbound.OutboundDescription;
                                break;
                            case 4:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction4 = prefoutbound.OutboundDescription;
                                break;
                            case 5:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction5 = prefoutbound.OutboundDescription;
                                break;
                            case 6:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction6 = prefoutbound.OutboundDescription;
                                break;
                            case 7:
                                pflobViewModel.CurrentLegOutBoundData.OutboundInstruction7 = prefoutbound.OutboundDescription;
                                break;
                        }
                    }
                }
            }
            result.Result = pflobViewModel;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> SavePreflightLegOutbound(PreflightLegViewModel preflightLegOutBoundIns)
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (!result.IsAuthorized())
                return result;

            Dictionary<string, object> RetObj = new Dictionary<string, object>();
            PreflightTripViewModel tripObj = new PreflightTripViewModel();
            tripObj.PreflightMain = new PreflightMainViewModel();
            int legNum; string Msg = string.Empty;
            
            if (int.TryParse(preflightLegOutBoundIns.LegNUM.ToString(), out legNum))
            {
                bool IsSaved = false;
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    tripObj = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    var preflightLegObj = tripObj.PreflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false && x.LegNUM == legNum).FirstOrDefault();
                    if (preflightLegObj.PreflightTripOutbounds != null && preflightLegObj.PreflightTripOutbounds.Count > 0)
                    {
                        foreach (PreflightTripOutboundViewModel prefoutbound in preflightLegObj.PreflightTripOutbounds)
                            {
                                if (prefoutbound.PreflightTripOutboundID != 0)
                                    prefoutbound.State = TripEntityState.Modified;
                                else
                                    prefoutbound.State = TripEntityState.Added;

                                prefoutbound.IsDeleted = false;
                                prefoutbound.LastUpdTS = DateTime.UtcNow;
                                prefoutbound.LastUpdUID = UserPrincipal._name;
                                MapOutboundData(prefoutbound.OutboundInstructionNUM, prefoutbound, preflightLegOutBoundIns);

                            }

                        MapOutBoundInstruction(preflightLegObj, preflightLegOutBoundIns, false);
                    }
                    else
                    {
                        preflightLegObj.PreflightTripOutbounds = new List<PreflightTripOutboundViewModel>();
                        PopulateTripOutboundObject(preflightLegObj, preflightLegOutBoundIns, UserPrincipal._name);
                    }
                    IsSaved = true;
                    if (preflightLegObj.State != TripEntityState.Added && preflightLegObj.State != TripEntityState.Deleted)
                        preflightLegObj.State = TripEntityState.Modified;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = tripObj;
                    RetObj.Add("CurrentLegOutBoundData", preflightLegObj);
                    Msg = "Outbound has been added successfully";
                }
                else
                { Msg = "Not able to Add Outbound Instruction"; }
                RetObj.Add("IsSaved", IsSaved);
                RetObj.Add("Msg", Msg);

            }
            result.Result = RetObj;
            return result;
        }

        static void MapOutBoundInstruction(PreflightLegViewModel legObj, PreflightLegViewModel SourcePreflightLegViewModel, bool isOuterCall)
        {
            legObj.OutboundInstruction1 = SourcePreflightLegViewModel.OutboundInstruction1;
            legObj.OutboundInstruction2 = SourcePreflightLegViewModel.OutboundInstruction2;
            legObj.OutboundInstruction3 = SourcePreflightLegViewModel.OutboundInstruction3;
            legObj.OutboundInstruction4 = SourcePreflightLegViewModel.OutboundInstruction4;
            legObj.OutboundInstruction5 = SourcePreflightLegViewModel.OutboundInstruction5;
            legObj.OutboundInstruction6 = SourcePreflightLegViewModel.OutboundInstruction6;
            legObj.OutboundInstruction7 = SourcePreflightLegViewModel.OutboundInstruction7;

            legObj.FuelLoad = SourcePreflightLegViewModel.FuelLoad;
            legObj.CrewFuelLoad = SourcePreflightLegViewModel.CrewFuelLoad;
            legObj.OutbountInstruction = SourcePreflightLegViewModel.OutbountInstruction;

            if (legObj.State == TripEntityState.NoChange && SourcePreflightLegViewModel.State == TripEntityState.Modified)
                legObj.State = TripEntityState.Modified;

            if (isOuterCall && SourcePreflightLegViewModel.PreflightTripOutbounds != null)
                legObj.PreflightTripOutbounds = SourcePreflightLegViewModel.PreflightTripOutbounds;
            
            if (isOuterCall && SourcePreflightLegViewModel.PreflightChecklist != null)
                legObj.PreflightChecklist = SourcePreflightLegViewModel.PreflightChecklist;
        }
        static void PopulateTripOutboundObject(PreflightLegViewModel legObj, PreflightLegViewModel SourcePreflightLegViewModel,string _name)
        {
            for (int outboundIns = 1; outboundIns <= 7; outboundIns++)
            {
                PreflightTripOutboundViewModel prefOutboundObj = new PreflightTripOutboundViewModel();
                prefOutboundObj.LegID = legObj.LegID;
                prefOutboundObj.State = TripEntityState.Added;
                prefOutboundObj.OutboundInstructionNUM = outboundIns;
                prefOutboundObj.IsDeleted = false;
                prefOutboundObj.LastUpdTS = DateTime.UtcNow;
                prefOutboundObj.LastUpdUID = _name;
                MapOutboundData(prefOutboundObj.OutboundInstructionNUM, prefOutboundObj, SourcePreflightLegViewModel);
                if (legObj.PreflightTripOutbounds == null)
                    legObj.PreflightTripOutbounds = new List<PreflightTripOutboundViewModel>();
                legObj.PreflightTripOutbounds.Add(prefOutboundObj);
            }
            MapOutBoundInstruction(legObj, SourcePreflightLegViewModel, false);
        }
        static void MapOutboundData(long? OutboundNum, PreflightTripOutboundViewModel TargetObject, PreflightLegViewModel SourceObject)
        {
            switch (OutboundNum)
            {
                case 1:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction1;
                    break;
                case 2:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction2;
                    break;
                case 3:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction3;
                    break;
                case 4:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction4;
                    break;
                case 5:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction5;
                    break;
                case 6:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction6;
                    break;
                case 7:
                    TargetObject.OutboundDescription = SourceObject.OutboundInstruction7;
                    break;
            }

        }
        #endregion

        #region Crew Hotels
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegCrewHotelViewModel> GetPreflightLegCrewHotelByLegNumHotelCode(int LegNum, string HotelCode)
        {
            FSSOperationResult<PreflightLegCrewHotelViewModel> result = new FSSOperationResult<PreflightLegCrewHotelViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightTripViewModel preflightTrip = new PreflightTripViewModel();
            if(LegNum <= 0)
            {
                result.Result = new PreflightLegCrewHotelViewModel();
                return result;
            }
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                preflightTrip.CurrentLegNUM = LegNum;
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = preflightTrip;
            }
            else
            {
                result.Result = new PreflightLegCrewHotelViewModel();
                return result;
            }

            var preflightLegCrewHotelVM = new PreflightLegCrewHotelViewModel() ;
            List<PreflightLegCrewHotelViewModel> crewHotelList = null;
            if (HotelCode != "")
            {
                if (preflightTrip.PreflightLegCrewLogisticsHotelList.ContainsKey((LegNum).ToString()) == false)
                    preflightTrip.PreflightLegCrewLogisticsHotelList[(LegNum).ToString()] = new List<PreflightLegCrewHotelViewModel>();

                crewHotelList = preflightTrip.PreflightLegCrewLogisticsHotelList[(LegNum).ToString()];
                preflightLegCrewHotelVM = crewHotelList.Where(t => t.HotelCode == HotelCode).FirstOrDefault();

            }
            else
            {
                if (preflightTrip.PreflightLegCrewLogisticsHotelList.ContainsKey(LegNum.ToString()))
                { 
                    crewHotelList = preflightTrip.PreflightLegCrewLogisticsHotelList[(LegNum).ToString()];
                    preflightLegCrewHotelVM = crewHotelList.OrderBy(ch => ch.HotelCode).FirstOrDefault();
                }
            }
            
            if (preflightLegCrewHotelVM != null)
            {
                result.Result = preflightLegCrewHotelVM;
                return result;
            }
            else
            {
                result.Result = new PreflightLegCrewHotelViewModel();
                return result;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable RetrievePreflightCrewHotelListGrid(int LegNum)
        {
            Hashtable data = new Hashtable();
            List<PreflightLegCrewHotelViewModel> crewHotelList = null;

            PreflightTripViewModel preflightTrip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            if (preflightTrip == null )
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }

            List<PreflightLegCrewViewModel> preflightCrews = new List<PreflightLegCrewViewModel>();
            if (preflightTrip.PreflightLegCrews.ContainsKey(LegNum.ToString()))
            {
                preflightCrews = preflightTrip.PreflightLegCrews[LegNum.ToString()];
            }
            if (preflightTrip.PreflightLegCrewLogisticsHotelList.ContainsKey(LegNum.ToString()))
            {
                crewHotelList = preflightTrip.PreflightLegCrewLogisticsHotelList[LegNum.ToString()].Where(tch => tch.IsDeleted==false && tch.State != TripEntityState.Deleted).ToList();
            }
            else
                crewHotelList = new List<PreflightLegCrewHotelViewModel>();
            Dictionary<long, string> crewIDcrewCode = new Dictionary<long, string>();

            foreach(PreflightLegCrewHotelViewModel crHotel in crewHotelList)
                LoadCrewCodesFromID(crHotel.CrewIDList, ref crewIDcrewCode);

            for (int i = 0; i < crewHotelList.Count; i++)
            {
                PreflightLegCrewHotelViewModel chHotelModel = crewHotelList[i];
                if ((chHotelModel.IsAllCrewOrPax.HasValue && chHotelModel.IsAllCrewOrPax.Value) || (preflightCrews != null && chHotelModel.CrewIDList != null && (preflightCrews.Count() <= chHotelModel.CrewIDList.Count())))
                {
                    chHotelModel.CrewCode = "All";
                }
                else if (chHotelModel.CrewIDList!=null && chHotelModel.CrewIDList.Count == 0)
                {
                    chHotelModel.CrewCode = "None";
                }
                else
                {
                    var selCrewValues = (from c in crewIDcrewCode
                                         where chHotelModel.CrewIDList != null && chHotelModel.CrewIDList.Contains(c.Key)
                                         select c.Value).ToArray();
                    chHotelModel.CrewCode = String.Join(",", selCrewValues);
                }
            }

            data["meta"] = new PagingMetaData() { Page = 1, Size = crewHotelList.Count, total_items = crewHotelList.Count };
            data["results"] = crewHotelList.OrderBy( c=> c.HotelCode);

            return data;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegCrewHotelViewModel> AddNewHotel(int legNum, bool isArrival)
        {
            FSSOperationResult<PreflightLegCrewHotelViewModel> result = new FSSOperationResult<PreflightLegCrewHotelViewModel>();
            if (!result.IsAuthorized())
                return result;

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            
            var newCrewHotel = new PreflightLegCrewHotelViewModel();
            newCrewHotel.isArrivalHotel = isArrival;
            newCrewHotel.isDepartureHotel = !isArrival;

            newCrewHotel.RoomDescription = "0";
            if (legNum > 0)
            {
                int legNumIndex = legNum - 1;
                PreflightTripViewModel trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                PreflightLegViewModel currentLeg = trip.PreflightLegs.Where(pl =>pl.LegNUM.HasValue && pl.LegNUM.Value == legNum && pl.IsDeleted == false).FirstOrDefault();

                if (isArrival)
                {
                    if (currentLeg.ArrivalDTTMLocal.HasValue)
                        newCrewHotel.DateIn = new DateTime((currentLeg.ArrivalDTTMLocal.Value.Date).Ticks, DateTimeKind.Local);
                    //else
                    //    newCrewHotel.DateIn = new DateTime((DateTime.Now.Date).Ticks, DateTimeKind.Local);

                    PreflightLegViewModel nextLeg = trip.PreflightLegs.Where(pl => pl.LegNUM.HasValue && pl.LegNUM.Value == legNum + 1 && pl.IsDeleted == false).FirstOrDefault();
                    if (nextLeg != null && nextLeg.DepartureDTTMLocal.HasValue)
                    {
                        newCrewHotel.DateOut = new DateTime((nextLeg.DepartureDTTMLocal.Value.Date).Ticks, DateTimeKind.Local);
                    }
                }
                else
                {
                    if (currentLeg.LegNUM == 1)
                    {
                        newCrewHotel.DateIn = null;
                        newCrewHotel.DateOut = new DateTime((currentLeg.DepartureDTTMLocal.Value.Date).Ticks, DateTimeKind.Local);
                    }
                    else if (currentLeg.LegNUM > 1)
                    {
                        if (currentLeg.DepartureDTTMLocal.HasValue)
                            newCrewHotel.DateOut = new DateTime((currentLeg.DepartureDTTMLocal.Value.Date).Ticks, DateTimeKind.Local);
                        
                        PreflightLegViewModel prevLeg = trip.PreflightLegs.Where(pl => pl.LegNUM.HasValue && pl.LegNUM.Value == legNum - 1 && pl.IsDeleted == false).FirstOrDefault();
                        if (prevLeg != null && prevLeg.ArrivalDTTMLocal.HasValue)
                        {
                            newCrewHotel.DateIn = new DateTime((prevLeg.ArrivalDTTMLocal.Value.Date).Ticks, DateTimeKind.Local);
                        }
                    }

                }

            }
            result.Result = newCrewHotel;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SavePreflightCrewHotel(PreflightLegCrewHotelViewModel crewHotel)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable hashResult = new Hashtable();
            if (crewHotel == null || string.IsNullOrWhiteSpace(crewHotel.HotelCode))
            {
                hashResult["Flag"] = "0";
                hashResult["Error"] = "Invalid Crew Hotel Details received";
                result.Result = hashResult;
                return result;
            }
            if(crewHotel.isArrivalHotel == false || crewHotel.isArrivalHotel == null)
            {
                crewHotel.isDepartureHotel = true;
            }
            PreflightTripViewModel preflightTrip = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            List<PreflightLegCrewHotelViewModel> crewHotelList = null;
            if (!preflightTrip.PreflightLegCrewLogisticsHotelList.ContainsKey((crewHotel.LegNUM).ToString()))
                preflightTrip.PreflightLegCrewLogisticsHotelList[(crewHotel.LegNUM).ToString()] = new List<PreflightLegCrewHotelViewModel>();

            crewHotelList = preflightTrip.PreflightLegCrewLogisticsHotelList[(crewHotel.LegNUM).ToString()];
            if (crewHotelList == null)
                crewHotelList = new List<PreflightLegCrewHotelViewModel>();

            PreflightLegCrewHotelViewModel preflightLegCrewHotelVM = crewHotelList.Where(c => c.HotelCode == crewHotel.HotelCode && c.IsDeleted == false).FirstOrDefault();
            if (preflightLegCrewHotelVM == null && string.IsNullOrWhiteSpace(crewHotel.HotelCodeEdit)==false)
            {
                preflightLegCrewHotelVM = crewHotelList.Where(c => c.HotelCode == crewHotel.HotelCodeEdit).FirstOrDefault();
                if (preflightLegCrewHotelVM != null)
                {
                    crewHotel.State = preflightLegCrewHotelVM.PreflightHotelListID > 0 ? TripEntityState.Modified : TripEntityState.Added;
                    preflightLegCrewHotelVM = (PreflightLegCrewHotelViewModel)preflightLegCrewHotelVM.InjectFrom(crewHotel);
                }
            }
            else if (preflightLegCrewHotelVM == null)
            {
                crewHotel.State = TripEntityState.Added;
                preflightLegCrewHotelVM = crewHotel;
                preflightTrip.PreflightLegCrewLogisticsHotelList[(crewHotel.LegNUM).ToString()].Add(preflightLegCrewHotelVM);
            }
            else
            {
                if (crewHotel.PreflightHotelListID > 0)
                    crewHotel.State = TripEntityState.Modified;
                else
                    crewHotel.State = TripEntityState.Added;
                preflightLegCrewHotelVM = (PreflightLegCrewHotelViewModel)preflightLegCrewHotelVM.InjectFrom(crewHotel);
            }
            if (preflightLegCrewHotelVM != null)
            {
                if (preflightLegCrewHotelVM.PreflightHotelListID > 0)
                    preflightLegCrewHotelVM.State = TripEntityState.Modified;
                else
                    preflightLegCrewHotelVM.State = TripEntityState.Added;
            }           
            preflightTrip.PreflightLegCrewLogisticsHotelList[(crewHotel.LegNUM).ToString()] = crewHotelList;
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = preflightTrip;
            result.Result = hashResult;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DeletePreflightCrewHotel(String CrewHotelCode, int LegNUM, Int64 PreflightHotelListID)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable hashResult = new Hashtable();
            if (CrewHotelCode == null || LegNUM <= 0)
            {
                hashResult["Flag"] = "0";
                hashResult["Error"] = "Invalid Crew Hotel Details received";
                result.Result = hashResult;
                return result;
            }
            PreflightTripViewModel preflightTrip = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            List<PreflightLegCrewHotelViewModel> crewHotelList = null;
            if (!preflightTrip.PreflightLegCrewLogisticsHotelList.ContainsKey(LegNUM.ToString()))
                preflightTrip.PreflightLegCrewLogisticsHotelList[LegNUM.ToString()] = new List<PreflightLegCrewHotelViewModel>();

            crewHotelList = preflightTrip.PreflightLegCrewLogisticsHotelList[(LegNUM).ToString()];
            if (crewHotelList == null)
                crewHotelList = new List<PreflightLegCrewHotelViewModel>();
            PreflightLegCrewHotelViewModel preflightLegCrewHotelVM = null;
            if(PreflightHotelListID <= 0)
                preflightLegCrewHotelVM = crewHotelList.Where(c => c.HotelCode == CrewHotelCode).FirstOrDefault();
            else
                preflightLegCrewHotelVM = crewHotelList.Where(c => c.PreflightHotelListID == PreflightHotelListID).FirstOrDefault();

            if (preflightLegCrewHotelVM != null)
            {
                if (preflightLegCrewHotelVM.PreflightHotelListID > 0)
                {
                    preflightLegCrewHotelVM.State = TripEntityState.Deleted;
                    preflightLegCrewHotelVM.IsDeleted = true;
                }
                else
                {
                    var remSuccess = crewHotelList.Remove(preflightLegCrewHotelVM);
                }
            }

            preflightTrip.PreflightLegCrewLogisticsHotelList[(LegNUM).ToString()] = crewHotelList;
            result.Result = hashResult;
            return result; 
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegCrewTransportationViewModel> GetCrewLegTransportViewModelByLegNum(int LegNum)
        {
            FSSOperationResult<PreflightLegCrewTransportationViewModel> result = new FSSOperationResult<PreflightLegCrewTransportationViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightLegCrewTransportationViewModel crewTransportVM = new PreflightLegCrewTransportationViewModel();

            PreflightTripViewModel preflightTrip = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            crewTransportVM = preflightTrip.PreflightLegCrewTransportationList.ContainsKey(LegNum.ToString()) ? preflightTrip.PreflightLegCrewTransportationList[LegNum.ToString()] : new PreflightLegCrewTransportationViewModel();
            preflightTrip.CurrentLegNUM = LegNum;
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = preflightTrip;
            result.Result = crewTransportVM;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SavePreflightCrewTransportation(PreflightLegCrewTransportationViewModel crewTransportation)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            Hashtable hashResult = new Hashtable();
            hashResult["Flag"] = "0";
            hashResult["Error"] = "Invalid Crew Transportation Details received";
            if (crewTransportation == null)
            {
                result.Result = hashResult;
                return result;
            }
            PreflightTripViewModel preflightTrip = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            if (preflightTrip == null)
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");

            if (preflightTrip.PreflightLegCrewTransportationList == null)
                preflightTrip.PreflightLegCrewTransportationList = new Dictionary<string, PreflightLegCrewTransportationViewModel>();
            
            preflightTrip.PreflightLegCrewTransportationList[crewTransportation.LegNUM.ToString()] = crewTransportation;
            result.Result = hashResult;
            return result;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable LoadPreflightLegCrewHotelSelectedCrew(int LegNum)
        {
            Hashtable data = new Hashtable();
            data["meta"] = new PagingMetaData() { Page = 1, Size = 10, total_items = 0 };
            data["results"] = new List<PreflightLegCrewHotelSelectedCrewViewModel>();

            PreflightTripViewModel preflightTrip = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] == null)
            {
                return data;
            }
            else
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            if(preflightTrip == null || !preflightTrip.PreflightLegCrews.ContainsKey(LegNum.ToString()))
            {
                return data;
            }

            List<PreflightLegCrewViewModel> preflightCrews = preflightTrip.PreflightLegCrews[LegNum.ToString()];

            Dictionary<long, string> crewIDcrewCode = new Dictionary<long, string>();
            foreach(PreflightLegCrewViewModel crew in preflightCrews)
            {
                LoadCrewCodeFromID(ref crewIDcrewCode, crew.CrewID);
            }

            List<PreflightLegCrewHotelSelectedCrewViewModel> crewList = new List<PreflightLegCrewHotelSelectedCrewViewModel>();
            foreach (long crewid in crewIDcrewCode.Keys)
            {
                PreflightLegCrewHotelSelectedCrewViewModel crew = new PreflightLegCrewHotelSelectedCrewViewModel();
                crew.CrewCode = crewIDcrewCode[crewid];
                PreflightLegCrewViewModel crewVM = preflightCrews.Where(c => c.CrewID == crewid).FirstOrDefault();
                
                if (crewVM.IsDeleted)
                {
                    continue;
                }
                
                if (crewVM != null)
                {
                    crew.CrewFirstName = crewVM.CrewFirstName;
                    crew.CrewLastName = crewVM.CrewLastName;
                    crew.CrewMiddleName = crewVM.CrewMiddleName;
                    crew.LegID = crewVM.LegID.HasValue ? crewVM.LegID.Value : 0;
                    crew.LegNUM = LegNum;
                    crew.CrewID = crewid;
                }
                crewList.Add(crew);
            }

            data["meta"] = new PagingMetaData() { Page = crewList.Count / 10, Size = 10, total_items = crewList.Count };
            data["results"] = crewList;

            return data;
        }

        private static Dictionary<long, string> LoadCrewCodesFromID(List<long?> crewHotelIDList, ref Dictionary<long, string> crewIDcrewCode)
        {
            if (crewHotelIDList != null && crewHotelIDList.Count() > 0)
            {
                foreach (long? crewId in crewHotelIDList)
                {
                    LoadCrewCodeFromID(ref crewIDcrewCode, crewId);
                }
            }
            return crewIDcrewCode;
        }

        private static void LoadCrewCodeFromID(ref Dictionary<long, string> crewIDcrewCode, long? crewId)
        {
            if (crewId.HasValue && !crewIDcrewCode.ContainsKey(crewId.Value))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetCrewbyCrewId(crewId.Value);
                    if (objRetVal.ReturnFlag == true)
                    {
                        if (objRetVal.EntityList.Count > 0)
                        {
                            crewIDcrewCode[crewId.Value] = objRetVal.EntityList[0].CrewCD;//crew.CrewCD;
                        }
                    }
                }
            }
        }


        #endregion 


        #region CopyTripClick
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static Hashtable CopyTrip(string tbNewDepartureDate, bool chkIncludeLogistics, bool chkIncludePassengerManifest, bool chkIncludeCrewManifest, bool chkIncludeHistory)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tbNewDepartureDate, chkIncludeLogistics, chkIncludePassengerManifest, chkIncludeCrewManifest))
            {
                Hashtable result = new Hashtable();
                result["Message"] = "";
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbNewDepartureDate))
                        {


                            List<DateTime> dtList = new List<DateTime>();

                            DateTime dateDepartTime = new DateTime();
                            var TripVM = (PreflightTripViewModel)HttpContext.Current.Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip];
                            
                            dateDepartTime = DateTime.ParseExact(tbNewDepartureDate,getUserPrincipal()._ApplicationDateFormat, CultureInfo.InvariantCulture);
                            
                            // Get latst session

                           

                            if (TripVM != null && TripVM.TripId != 0)
                            {

                                using (PreflightServiceClient objPreflightClient = new PreflightServiceClient())
                                {

                                    bool varLogistics = chkIncludeLogistics;
                                    bool varPAXManifest = chkIncludePassengerManifest;
                                    bool varCrewMAnifest = chkIncludeCrewManifest;
                                    bool varHistory = chkIncludeHistory;


                                    dtList.Add(dateDepartTime);

                                    var ObjRetval = objPreflightClient.CopyTripDetails(dateDepartTime, (long)TripVM.TripId, (long)TripVM.PreflightMain.CustomerID, varLogistics, varCrewMAnifest, varPAXManifest, varHistory);
                                    if (ObjRetval.ReturnFlag)
                                    {
                                        var Trip = ObjRetval.EntityInfo;

                                        Trip.Mode = TripActionMode.Edit;
                                        Trip.State = TripEntityState.Modified;
                                        HttpContext.Current.Session["CurrentPreFlightTrip"] = Trip;

                                        var ObjRetvalExp = objPreflightClient.GetTripExceptionList(Trip.TripID);
                                        if (ObjRetvalExp.ReturnFlag)
                                        {
                                            HttpContext.Current.Session["PreflightException"] = ObjRetvalExp.EntityList;
                                        }
                                        else
                                        {
                                            HttpContext.Current.Session.Remove("PreflightException");
                                        }
                                        Trip.AllowTripToNavigate = true;
                                        Trip.AllowTripToSave = true;

                                        result["Message"] = "Trip Copied Successfully.";
                                        result["Title"] = "Copy Alert";
                                        result["callback"] = "CloseCopyTripFn";

                                        var CopiedTripVM= TripSearch((long)Trip.TripNUM).Result;

                                        TripVM = (PreflightTripViewModel)CopiedTripVM["Trip"];
                                        TripVM.PreflightMain.EditMode = true;

                                        //RadWindowManager1.RadAlert("Trip Copied Successfully.", 330, 100, "Copy Alert", "CloseCopyTripFn", null);
                                        //RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                    }
                                }
                            }
                        }
                        else
                        {
                            result["Message"] = "New Depart Date is Mandatory";
                            result["Title"] = "Advance Copy Alert";
                            result["callback"] = "alertCallBackFn";

                            //RadWindowManager1.RadAlert("New Depart Date is Mandatory", 330, 100, "Advance Copy Alert", "alertCallBackFn", null);
                            //RadWindowManager1.RadAlert("New Depart Date is Mandatory", 330, 100, "Advance Copy Alert", null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }

                return result;
            }
        }

        #endregion

        #region LogTripClick
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> IsTripValidForLogging()
        {
            FSSOperationResult<bool> result = new FSSOperationResult<bool>();
            if (!result.IsAuthorized())
                return result;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightTripViewModel preflightTrip = null;
                        if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        }

                        if (preflightTrip != null && preflightTrip.TripId.HasValue && preflightTrip.PreflightMain != null && preflightTrip.PreflightMain.FleetID.HasValue)
                            result.Result = true;
                        else
                            result.Result = false;
                       
                        return result;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
                result.Success = true;
                return result;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> IsTripAlreadyLogged()
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable hasResult = new Hashtable();
                hasResult["Message"] = "";
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMain preflightTrip = null;
                        PreflightTripViewModel preflightTripInSession = null;
                        if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            preflightTripInSession = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        }
                        if (preflightTripInSession == null || !preflightTripInSession.TripId.HasValue)
                        {
                            hasResult["Result"] = "0";
                            hasResult["Message"] = "Please Select a trip to be logged";
                        }
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTripByTripNum(preflightTripInSession.PreflightMain.TripNUM.Value);
                            if (objRetVal.ReturnFlag)
                            {
                                preflightTrip = objRetVal.EntityList[0];
                            }
                        }

                        if (preflightTrip == null || preflightTrip.TripID ==0)
                        {
                            hasResult["Result"] = "0";
                            hasResult["Message"] = "Please Select a trip to be logged";
                        }

                        if (preflightTrip.IsLog.HasValue && preflightTrip.IsLog.Value)
                        {
                            hasResult["Result"] = "2";
                            hasResult["Message"] = "The selected Tripsheet is already logged in Postflight. Selecting YES will remove all data previously entered per leg in this log.";
                        }
                        else
                        {
                            hasResult["Result"] = "1";
                            hasResult["Message"] = "";
                        }
                        return hasResult;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
                result.Result = hasResult;
                return result;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> LogTrip()
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable hasResult = new Hashtable();
                hasResult["Message"] = "";
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightTripViewModel preflightTrip = null;
                        if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        }

                        if (preflightTrip == null || !preflightTrip.TripId.HasValue)
                        {
                            hasResult["Result"] = "0";
                            hasResult["Message"] = "Please Select a trip to be logged";
                        }
                        else
                        {
                            using (PreflightServiceClient objPreflightClient = new PreflightServiceClient())
                            {

                                PreflightSearchRetrieve PostflightMove = new PreflightSearchRetrieve();
                                bool tripmoved = false;
                                tripmoved = PostflightMove.GetTripandConfirm(preflightTrip.TripId.Value);

                                if (tripmoved)
                                {

                                    FlightPak.Web.PostflightService.PostflightMain POLog = (FlightPak.Web.PostflightService.PostflightMain)HttpContext.Current.Session["POSTFLIGHTMAIN"];
                                    if (POLog != null)
                                    {
                                        preflightTrip.PreflightMain.IsLog = true;
                                        hasResult["Result"] = "1";
                                        hasResult["POLogId"] = POLog.LogNum.ToString();
                                        preflightTrip.PreflightMain.LogNum = POLog.LogNum;
                                        //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('Trip Logged into Postflight, Click YES to View Postflight Log No:" + POLog.LogNum.ToString() + "',MasterconfirmCallBackFn, 330, 110,null,'Confirmation!');");
                                    }
                                }
                            }
                        }
                        return hasResult;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
                result.Result = hasResult;
                return result;
            }

        }

        public static void Update_DeletedLogInfo(FlightPak.Web.PostflightService.PostflightMain poLog)
        {
            if (poLog == null)
                return;
            PreflightTripViewModel preflightTrip = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                preflightTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            }

            if (preflightTrip == null || !preflightTrip.TripId.HasValue || poLog.TripID != preflightTrip.TripId )
                return;

            preflightTrip.PreflightMain.IsLog = false;
            preflightTrip.PreflightMain.LogNum = null;
        }
        #endregion

        #region Preflight Leg Checklist
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegChecklistViewModel> GetCheckListByChecklistGroupID(string ChecklistGroupID, string CheckListGroupDesc, int LegNum)
        {
            FSSOperationResult<PreflightLegChecklistViewModel> result = new FSSOperationResult<PreflightLegChecklistViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightLegChecklistViewModel prflChecklistObj = new PreflightLegChecklistViewModel();
            prflChecklistObj.LegNum = LegNum;
            prflChecklistObj.EditMode = true;
            prflChecklistObj.CheckGroupDescription = CheckListGroupDesc;
            List<PreflightChecklistViewModel> checklistObj = new List<PreflightChecklistViewModel>();
            if (!string.IsNullOrEmpty(ChecklistGroupID))
            {

                using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                {

                    var ObjRetVal = ObjService.GetTripManagerChecklistListInfo();

                    List<FlightPakMasterService.GetAllTripManagerCheckList> TripChecklist = new List<FlightPakMasterService.GetAllTripManagerCheckList>();

                    if (ObjRetVal.ReturnFlag == true)
                    {
                        //this should not list any inactive check lists in the UI. Therefore added x.IsInactive==false condition
                        TripChecklist = ObjRetVal.EntityList.Where(x => x.CheckGroupID == Convert.ToInt64(ChecklistGroupID) && x.IsInactive==false).ToList();

                    }
                    foreach (GetAllTripManagerCheckList listObject in TripChecklist)
                    {
                        PreflightChecklistViewModel checkListViewModelObj = new PreflightChecklistViewModel();
                        checkListViewModelObj.CheckGroupCD = listObject.CheckGroupCD;
                        checkListViewModelObj.CheckGroupID = listObject.CheckGroupID;
                        checkListViewModelObj.ComponentCD = listObject.CheckListCD;
                        checkListViewModelObj.CheckGroupDescription = CheckListGroupDesc;
                        checkListViewModelObj.CheckListDescription = listObject.CheckListDescription;
                        checkListViewModelObj.CheckListID = listObject.CheckListID;
                        checkListViewModelObj.IsDeleted = (bool) listObject.IsDeleted;
                        checkListViewModelObj.IsInactive = listObject.IsInactive;
                        checkListViewModelObj.CustomerID = listObject.CustomerID;
                        //checkListViewModelObj.LastUpdTS = listObject.LastUpdTS;
                        checkListViewModelObj.LastUpdUID = listObject.LastUpdUID;

                        checklistObj.Add(checkListViewModelObj);
                    }
                    prflChecklistObj.CurrentLegChecklist = checklistObj;
                }
            }
            result.Result = prflChecklistObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PreflightLegChecklistViewModel> GetChecklist(int LegNum)
        {
            FSSOperationResult<PreflightLegChecklistViewModel> result = new FSSOperationResult<PreflightLegChecklistViewModel>();
            if (!result.IsAuthorized())
                return result;

            PreflightLegChecklistViewModel pfChecklistViewModel = new PreflightLegChecklistViewModel();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                var legObject = pfViewModel.PreflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false && x.LegNUM == LegNum).FirstOrDefault();
                if (legObject.PreflightChecklist != null && legObject.PreflightChecklist.Count > 0)
                    pfChecklistViewModel.CurrentLegChecklist = legObject.PreflightChecklist.Where(x => x.IsDeleted == false).OrderBy(o => o.ComponentCD).ToList();
                else
                    GetDefaultChecklist(pfChecklistViewModel);

                pfChecklistViewModel.EditMode = pfViewModel.PreflightMain.EditMode;
            }
            result.Result = pfChecklistViewModel;
            return result;
        }

        private static void GetDefaultChecklist(PreflightLegChecklistViewModel pfChecklistViewModel)
        {
            UserPrincipalViewModel upViewModel = getUserPrincipal();
            List<PreflightChecklistViewModel> checklistObj = new List<PreflightChecklistViewModel>();
            if (upViewModel._DefaultCheckListGroupID != null)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ObjRetValGroup = ObjService.GetAllTripmanagerChecklistGroupWithFilters((long)upViewModel._DefaultCheckListGroupID, string.Empty, false).EntityList.ToList();
                    string ChecklistGroupCD = string.Empty;
                    string ChecklistGroupDescription = string.Empty;
                    if (ObjRetValGroup.Count > 0)
                    {
                        ChecklistGroupCD = ObjRetValGroup[0].CheckGroupCD;
                        ChecklistGroupDescription = ObjRetValGroup[0].CheckGroupDescription;
                    }

                    var ObjRetVal = ObjService.GetTripManagerChecklistListInfo();
                    List<FlightPakMasterService.GetAllTripManagerCheckList> TripChecklist = new List<FlightPakMasterService.GetAllTripManagerCheckList>();

                    if (ObjRetVal.ReturnFlag == true)
                        TripChecklist = ObjRetVal.EntityList.Where(x => x.CheckGroupID == upViewModel._DefaultCheckListGroupID && x.IsDeleted==false && x.IsInactive==false).ToList();
                    if (TripChecklist.Count > 0)
                    {
                        foreach (GetAllTripManagerCheckList listObject in TripChecklist)
                        {
                            PreflightChecklistViewModel checkListViewModelObj = new PreflightChecklistViewModel();
                            checkListViewModelObj.LegID = 0;
                            checkListViewModelObj.PreflightCheckListID = 0;
                            checkListViewModelObj.ComponentCD = listObject.CheckListCD;
                            checkListViewModelObj.CheckListID = listObject.CheckListID;
                            checkListViewModelObj.CheckListDescription = listObject.CheckListDescription;

                            checkListViewModelObj.ComponentDescription = string.Empty;
                            checkListViewModelObj.IsCompleted = false;
                            checkListViewModelObj.CheckGroupCD = ChecklistGroupCD;
                            checkListViewModelObj.CheckGroupID = listObject.CheckGroupID;
                            checkListViewModelObj.CheckGroupDescription = ChecklistGroupDescription;

                            checklistObj.Add(checkListViewModelObj);
                        }
                    }
                    pfChecklistViewModel.CurrentLegChecklist = checklistObj;
                }
            }
            else
                pfChecklistViewModel.CurrentLegChecklist = checklistObj;
        }

        static void  MarkOldCheckListDelete(List<PreflightChecklistViewModel> preflightChecklistObject)
        {
            foreach (PreflightChecklistViewModel item in preflightChecklistObject.ToList())
            {
                if (item.PreflightCheckListID > 0)
                {
                    item.State = TripEntityState.Deleted;
                    item.IsDeleted = true;
                }
                else                 
                    preflightChecklistObject.Remove(item);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> SavePreflightChecklist(List<PreflightChecklistViewModel> preflightLegCheckList, int LegNum)
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;

            Dictionary<string, object> RetObj = new Dictionary<string, object>();
            PreflightTripViewModel tripObj = new PreflightTripViewModel();
            tripObj.PreflightMain = new PreflightMainViewModel();
            string Msg = string.Empty;
            bool IsSaved = false;
            long? checklistid = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                tripObj = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                var legObject = tripObj.PreflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == LegNum).FirstOrDefault();
                if (legObject != null && tripObj.PreflightLegs.Count > 0)
                {
                    foreach (PreflightChecklistViewModel item in preflightLegCheckList)
                    {
                        if (item.State != TripEntityState.Added && item.State != TripEntityState.Deleted && legObject.State != TripEntityState.Added && legObject.State != TripEntityState.Deleted)
                        {
                            if (item.PreflightCheckListID > 0)
                            {
                                item.State = TripEntityState.Modified;
                                legObject.State = TripEntityState.Modified;
                            }
                            else
                                item.State = TripEntityState.Added;
                        }
                    }
                    if (preflightLegCheckList.Count > 0)
                    {
                        checklistid = preflightLegCheckList[0].CheckGroupID;
                        if (legObject.PreflightChecklist != null && legObject.PreflightChecklist.Count > 0)
                        {
                            var obj = legObject.PreflightChecklist.Where(x => x.CheckGroupID == checklistid && x.IsDeleted == false).Count();
                            if (obj == 0)
                            {
                                MarkOldCheckListDelete(legObject.PreflightChecklist);
                                foreach (PreflightChecklistViewModel item in preflightLegCheckList)
                                {
                                    legObject.PreflightChecklist.Add(item);
                                }
                            }
                            else
                                legObject.PreflightChecklist = preflightLegCheckList;
                        }
                        else
                            legObject.PreflightChecklist = preflightLegCheckList;
                    }
                    else
                        legObject.PreflightChecklist = preflightLegCheckList;

                    IsSaved = true;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = tripObj;
                    Msg = "Checklist has been added successfully";
                }
                else
                {
                    Msg = "Not able to Save Checklist";
                    RetObj.Add("CurrentLegChecklist", legObject);
                    RetObj.Add("IsSaved", IsSaved);
                    RetObj.Add("Msg", Msg);
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetAllTripManagerCheckListGroup(bool IsInActive)
        {
            Hashtable data = new Hashtable();
            PreflightTripManagerLite tripManagerChecklistGroup = new PreflightTripManagerLite();
            var dv = tripManagerChecklistGroup.LoadAllTripManagerCheckListGroup(IsInActive);

            if (dv.Table != null)
            {
                var dt = dv.ToTable();
                if (dt != null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = dt.Rows.Count, total_items = dt.Rows.Count };
                    data["results"] = tripManagerChecklistGroup.GetJson(dv);
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            return data;
        }
        #endregion
            

        #region Pax-Info

        /// <summary>
        /// Check TSA status of passenger. Possible return values are, Status,BackColor and Enabled
        /// </summary>
        /// <param name="passengerRequestorID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CheckTSA(string passengerRequestorID)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            PreflightPaxLite paxLiteManager = new PreflightPaxLite();
            result.Result = paxLiteManager.CheckTSAStatus(passengerRequestorID);
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CheckPaxGroupCode(string paxGroupCode)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            PreflightPaxLite paxLiteManager = new PreflightPaxLite();
            result.Result = paxLiteManager.GetPaxGroupIDByPaxGroupCode(paxGroupCode); ;
            return result;
        }
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> AddNewPax(List<string> passengerRequestorIdentifiers, string isAddOrInsert, int InsertAt,bool? IsPassengerIds)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            PreflightPaxLite Pax = new PreflightPaxLite();
            result.Result = Pax.AddNewPax(passengerRequestorIdentifiers, isAddOrInsert, InsertAt, IsPassengerIds);
            return result;            
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<int> DeletePax(List<string> passengerRequestorCDs)
        {
            FSSOperationResult<int> result = new FSSOperationResult<int>();
            if (result.IsAuthorized() == false)
                return result;

            PreflightPaxLite Pax = new PreflightPaxLite();
            if (passengerRequestorCDs!=null)
                passengerRequestorCDs = passengerRequestorCDs.Select(p => p.Trim()).ToList();

            result.Result = Pax.DeletePax(passengerRequestorCDs);
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SavePaxInfoGrid(string GridJson)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (result.IsAuthorized() == false)
                return result;
            Hashtable data = new Hashtable();
            dynamic Obj = JArray.Parse(GridJson);
            PreflightPaxLite Pax = new PreflightPaxLite();
            Pax.SaveGridtoSession(Obj);
            result.Result = TripPaxSelectionLegs();
            return result;            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePaxInfoOneFlightPurposeByLegNPax(string legNum,string PaxCode,string FlightPurposeID)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightPaxLite Pax = new PreflightPaxLite();
            Pax.SaveLegPAXFlightPurpose(legNum,PaxCode,FlightPurposeID);
            result.Result = true;
            return result;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePaxInfoBlockedSeatsChange(string legNum, string PaxBooked, int BlockedSeatCount)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            if (legNum != null && PaxBooked != null)
            {
                PreflightPaxLite Pax = new PreflightPaxLite();
                Pax.SavePaxInfoBlockedSeatsChange(legNum, PaxBooked, BlockedSeatCount);
                result.Result = true;
            }
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SaveGridtoSessionAfterPaxWarning(string GridJson)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            Hashtable data = new Hashtable();
            dynamic Obj = JArray.Parse(GridJson);
            PreflightPaxLite Pax = new PreflightPaxLite();
            Pax.SaveGridtoSessionAfterPaxWarning(Obj);
            result.Result = true;
            return result;
        }


        #endregion

        #region CrewPassportVisa
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> TripStatus()
        {
            FSSOperationResult<bool> result = new FSSOperationResult<bool>();
            if (!result.IsAuthorized())
                return result;
            bool status = false;
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                status = pfViewModel.PreflightMain.EditMode;
            }
            result.Result = status;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable CrewPassportGrid(string crewID)
        {
            Hashtable data = new Hashtable();

            PreflightCrewLite crew = new PreflightCrewLite();
            var list = crew.GetCrewPassportData(crewID);
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = list.Count(), total_items = list.Count() };
                    data["results"] = list;
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable CrewVisaGrid(string crewID)
        {
            Hashtable data = new Hashtable();

            PreflightCrewLite crew = new PreflightCrewLite();
            var list = crew.GetCrewVisaData(crewID);
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = list.Count(), total_items = list.Count() };
                    data["results"] = list;
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }
        #endregion

        #region PaxPassportVisa
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable PaxPassportGrid(string PassengerID)
        {
            Hashtable data = new Hashtable();

            PreflightPaxLite pax = new PreflightPaxLite();
            var list = pax.GetPaxPassportData(PassengerID);
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = list.Count(), total_items = list.Count() };
                    data["results"] = list;
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable PaxVisaGrid(string PassengerID)
        {
            Hashtable data = new Hashtable();

            PreflightPaxLite pax = new PreflightPaxLite();
            var list = pax.GetPaxVisaData(PassengerID);
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = list.Count(), total_items = list.Count() };
                    data["results"] = list;
                }
                else
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    data["results"] = new List<object>();
                }
                return data;
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = new List<object>();
            return data;
        }
        #endregion

        #region Preflight Copy to All 
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyPrivateToAllLeg(bool isPrivate)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs)
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.IsPrivate = isPrivate;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyRequestorToAllLeg(PassengerViewModel pax, long paxid)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.Passenger = pax;
                    leg.PassengerRequestorID = paxid;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyDepartmentToAllLeg(DepartmentViewModel dept, long deptId)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.Department = dept;
                    leg.DepartmentID = deptId;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyAuthorizationToAllLeg(DepartmentViewModel dept, long deptId, DepartmentAuthorizationViewModel auth, long authId)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.Department = dept;
                    leg.DepartmentID = deptId;
                    leg.DepartmentAuthorization = auth;
                    leg.AuthorizationID = authId;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyCQCustomerToAllLeg(DepartmentViewModel dept, long deptId, CQCustomerViewModel CQCustomer, long CQCustomerId)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.Department = dept;
                    leg.DepartmentID = deptId;
                    leg.CQCustomer = CQCustomer;
                    leg.CQCustomerID = CQCustomerId;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyAccountToAllLeg(AccountViewModel account, long accountId)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.Account = account;
                    leg.AccountID = accountId;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> copyTripPurposeToAllLeg(string TripPurpose)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                foreach (PreflightLegViewModel leg in Trip.PreflightLegs.Where(l => l.IsDeleted == false).ToList())
                {
                    if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                    leg.FlightPurpose = TripPurpose;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> CopyFlightCategoryToAllLeg(string flightCategoryCD, long? flightCategoryID, int legNum)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            if (flightCategoryCD != null)
            { 
                flightCategoryCD = flightCategoryCD.Trim().Equals(String.Empty) ? null: flightCategoryCD;
                PreflightTripViewModel Trip = new PreflightTripViewModel();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    var CurrentLeg = PreflightLegsLite.GetLiveLegs(Trip).Where(x => x.LegNUM == legNum).FirstOrDefault();
                    foreach (PreflightLegViewModel leg in PreflightLegsLite.GetLiveLegs(Trip))
                    {
                        if (leg.LegID > 0) leg.State = TripEntityState.Modified;
                        leg.FlightCategoryID = flightCategoryID;
                        leg.FlightCatagory.FlightCatagoryCD = flightCategoryCD;
                        leg.isDeadCategory = CurrentLeg.isDeadCategory;
                        
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                }
            }
           result.Result = true;
           return result;
        }

        #endregion

        #region Utility Methods
        private static void removeAllLegChildEntities(PreflightTripViewModel tripVM, int legNumber)
        {
            String strLegNumber = legNumber.ToString();
            if(legNumber <= 0 || tripVM == null)
                return;
            PreflightLegViewModel leg = tripVM.PreflightLegs.Where(l => l.LegNUM == legNumber).FirstOrDefault();

            //This below removals for outbound and checklist are not necessary, but still doing it for the sake
            //of logical completion
            if (leg != null)
            {
                leg.PreflightTripOutbounds = null;
                leg.PreflightChecklist = null;
            }

            //Remove all crew info
            if(tripVM.PreflightLegCrews.ContainsKey(strLegNumber))
                tripVM.PreflightLegCrews.Remove(strLegNumber);

            if(tripVM.PreflightLegCrewLogisticsHotelList.ContainsKey(strLegNumber))
                tripVM.PreflightLegCrewLogisticsHotelList.Remove(strLegNumber);

            if(tripVM.PreflightLegCrewTransportationList.ContainsKey(strLegNumber))
                tripVM.PreflightLegCrewTransportationList.Remove(strLegNumber);

            //Remove all passenger related info
            if (tripVM.PreflightLegPassengers.ContainsKey(strLegNumber))
                tripVM.PreflightLegPassengers.Remove(strLegNumber);

            if (tripVM.PreflightLegPassengersLogisticsHotelList.ContainsKey(strLegNumber))
                tripVM.PreflightLegPassengersLogisticsHotelList.Remove(strLegNumber);

            if (tripVM.PreflightLegPaxTransportationList.ContainsKey(strLegNumber))
                tripVM.PreflightLegPaxTransportationList.Remove(strLegNumber);

            if (tripVM.PreflightLegPreflightLogisticsList.ContainsKey(strLegNumber))
                tripVM.PreflightLegPreflightLogisticsList.Remove(strLegNumber);

        }

        private static PreflightTripViewModel moveLegEntitiesUpward(PreflightTripViewModel tripVM, int moveFromLegNumber)
        {
            int maxNewLegCount = tripVM.PreflightLegs.Where(l=>l.IsDeleted==false).Count() + 1;

            for(int legNum = maxNewLegCount; legNum > moveFromLegNumber; legNum--  )
            {
                String strCurLegNum = legNum.ToString();
                String strPrevLegNum = (legNum-1).ToString();

                if (!tripVM.PreflightLegCrews.ContainsKey(strCurLegNum) || tripVM.PreflightLegCrews[strCurLegNum] == null)
                {
                    tripVM.PreflightLegCrews[strCurLegNum] = new List<PreflightLegCrewViewModel>();
                }
                tripVM.PreflightLegCrews[strCurLegNum].Clear();

                if (tripVM.PreflightLegCrews.ContainsKey(strPrevLegNum) && tripVM.PreflightLegCrews[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegCrews[strCurLegNum].AddRange(tripVM.PreflightLegCrews[strPrevLegNum]);
                }

                if (!tripVM.PreflightLegCrewLogisticsHotelList.ContainsKey(strCurLegNum) || tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum] = new List<PreflightLegCrewHotelViewModel>();
                }
                tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum].Clear();

                if (tripVM.PreflightLegCrewLogisticsHotelList.ContainsKey(strPrevLegNum) && tripVM.PreflightLegCrewLogisticsHotelList[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum].AddRange(tripVM.PreflightLegCrewLogisticsHotelList[strPrevLegNum]);
                }

                if (!tripVM.PreflightLegCrewTransportationList.ContainsKey(strCurLegNum) || tripVM.PreflightLegCrewTransportationList[strCurLegNum] == null )
                {
                    tripVM.PreflightLegCrewTransportationList[strCurLegNum] = new PreflightLegCrewTransportationViewModel();
                }
                if (tripVM.PreflightLegCrewTransportationList.ContainsKey(strPrevLegNum) && tripVM.PreflightLegCrewTransportationList[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegCrewTransportationList[strCurLegNum].InjectFrom(tripVM.PreflightLegCrewTransportationList[strPrevLegNum]);
                }

                if (!tripVM.PreflightLegPassengers.ContainsKey(strCurLegNum) || tripVM.PreflightLegPassengers[strCurLegNum] == null) 
                { 
                    tripVM.PreflightLegPassengers[strCurLegNum] = new List<PassengerViewModel>();
                }
                tripVM.PreflightLegPassengers[strCurLegNum].Clear();

                if (tripVM.PreflightLegPassengers.ContainsKey(strPrevLegNum) && tripVM.PreflightLegPassengers[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegPassengers[strCurLegNum].AddRange(tripVM.PreflightLegPassengers[strPrevLegNum]);
                }

                if (!tripVM.PreflightLegPassengersLogisticsHotelList.ContainsKey(strCurLegNum) || tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum] = new List<PreflightLegPassengerHotelViewModel>();
                }
                tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum].Clear();

                if (tripVM.PreflightLegPassengersLogisticsHotelList.ContainsKey(strPrevLegNum) && tripVM.PreflightLegPassengersLogisticsHotelList[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum].AddRange(tripVM.PreflightLegPassengersLogisticsHotelList[strPrevLegNum]);
                }

                if (!tripVM.PreflightLegPaxTransportationList.ContainsKey(strCurLegNum) || tripVM.PreflightLegPaxTransportationList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPaxTransportationList[strCurLegNum] = new PreflightLegPassengerTransportViewModel();
                }

                if (tripVM.PreflightLegPaxTransportationList.ContainsKey(strPrevLegNum) && tripVM.PreflightLegPaxTransportationList[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegPaxTransportationList[strCurLegNum] = (PreflightLegPassengerTransportViewModel)tripVM.PreflightLegPaxTransportationList[strCurLegNum].InjectFrom(tripVM.PreflightLegPaxTransportationList[strPrevLegNum]);
                }

                if (!tripVM.PreflightLegPreflightLogisticsList.ContainsKey(strCurLegNum) || tripVM.PreflightLegPreflightLogisticsList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPreflightLogisticsList[strCurLegNum] = new PreflightLogisticsViewModel();
                }

                if (tripVM.PreflightLegPreflightLogisticsList.ContainsKey(strPrevLegNum) && tripVM.PreflightLegPreflightLogisticsList[strPrevLegNum] != null)
                {
                    tripVM.PreflightLegPreflightLogisticsList[strCurLegNum] = (PreflightLogisticsViewModel)tripVM.PreflightLegPreflightLogisticsList[strCurLegNum].InjectFrom(tripVM.PreflightLegPreflightLogisticsList[strPrevLegNum]);
                }
            }

            //Re-initialize the items in middle leg
            string strMoveFromLegNumber = moveFromLegNumber.ToString();
            if (tripVM.PreflightLegCrews.ContainsKey(strMoveFromLegNumber) && tripVM.PreflightLegCrews[strMoveFromLegNumber] != null)
                tripVM.PreflightLegCrews[strMoveFromLegNumber].Clear();
            else
                tripVM.PreflightLegCrews = new Dictionary<string, List<PreflightLegCrewViewModel>>();
            if (tripVM.PreflightLegCrewLogisticsHotelList.ContainsKey(strMoveFromLegNumber) && tripVM.PreflightLegCrewLogisticsHotelList[strMoveFromLegNumber] != null)
                tripVM.PreflightLegCrewLogisticsHotelList[strMoveFromLegNumber].Clear();
            else
                tripVM.PreflightLegCrewLogisticsHotelList[strMoveFromLegNumber] = new List<PreflightLegCrewHotelViewModel>();
            tripVM.PreflightLegCrewTransportationList[strMoveFromLegNumber] = new PreflightLegCrewTransportationViewModel();

            if (tripVM.PreflightLegPassengers.ContainsKey(strMoveFromLegNumber) && tripVM.PreflightLegPassengers[strMoveFromLegNumber] != null)
                tripVM.PreflightLegPassengers[strMoveFromLegNumber].Clear();
            else
                tripVM.PreflightLegPassengers[strMoveFromLegNumber] = new List<PassengerViewModel>();
            if (tripVM.PreflightLegPassengersLogisticsHotelList.ContainsKey(strMoveFromLegNumber) && tripVM.PreflightLegPassengersLogisticsHotelList[strMoveFromLegNumber] != null)
                tripVM.PreflightLegPassengersLogisticsHotelList[strMoveFromLegNumber].Clear();
            else
                tripVM.PreflightLegPassengersLogisticsHotelList[strMoveFromLegNumber] = new List<PreflightLegPassengerHotelViewModel>();
            tripVM.PreflightLegPaxTransportationList[strMoveFromLegNumber] = new PreflightLegPassengerTransportViewModel();

            if (tripVM.TripId == 0)
                tripVM.PreflightLegPreflightLogisticsList.Remove(strMoveFromLegNumber);
            else
                tripVM.PreflightLegPreflightLogisticsList[strMoveFromLegNumber] = new PreflightLogisticsViewModel();

            return tripVM;
        }

        private static PreflightTripViewModel moveLegEntitiesDownward(PreflightTripViewModel tripVM, int moveTillLegNumber)
        {
            int legMoveLimit = tripVM.PreflightLegs.Count() - 1;

            for (int legNum = moveTillLegNumber; legNum <= legMoveLimit; legNum++)
            {
                String strCurLegNum = legNum.ToString();
                String strNextLegNum = (legNum + 1).ToString();

                if (!tripVM.PreflightLegCrews.ContainsKey(strCurLegNum) || tripVM.PreflightLegCrews[strCurLegNum] == null)
                {
                    tripVM.PreflightLegCrews[strCurLegNum] = new List<PreflightLegCrewViewModel>();
                }
                tripVM.PreflightLegCrews[strCurLegNum].Clear();

                if (tripVM.PreflightLegCrews.ContainsKey(strNextLegNum) && tripVM.PreflightLegCrews[strNextLegNum] != null)
                {
                    tripVM.PreflightLegCrews[strCurLegNum].AddRange(tripVM.PreflightLegCrews[strNextLegNum]);
                }

                if (!tripVM.PreflightLegCrewLogisticsHotelList.ContainsKey(strCurLegNum) || tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum] = new List<PreflightLegCrewHotelViewModel>();
                }
                tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum].Clear();

                if (tripVM.PreflightLegCrewLogisticsHotelList.ContainsKey(strNextLegNum) && tripVM.PreflightLegCrewLogisticsHotelList[strNextLegNum] != null)
                {
                    tripVM.PreflightLegCrewLogisticsHotelList[strCurLegNum].AddRange(tripVM.PreflightLegCrewLogisticsHotelList[strNextLegNum]);
                }

                if (!tripVM.PreflightLegCrewTransportationList.ContainsKey(strCurLegNum) || tripVM.PreflightLegCrewTransportationList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegCrewTransportationList[strCurLegNum] = new PreflightLegCrewTransportationViewModel();
                }
                if (tripVM.PreflightLegCrewTransportationList.ContainsKey(strNextLegNum) && tripVM.PreflightLegCrewTransportationList[strNextLegNum] != null)
                {
                    tripVM.PreflightLegCrewTransportationList[strCurLegNum] = (PreflightLegCrewTransportationViewModel)tripVM.PreflightLegCrewTransportationList[strCurLegNum].InjectFrom(tripVM.PreflightLegCrewTransportationList[strNextLegNum]);
                }

                if (!tripVM.PreflightLegPassengers.ContainsKey(strCurLegNum) || tripVM.PreflightLegPassengers[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPassengers[strCurLegNum] = new List<PassengerViewModel>();
                }
                tripVM.PreflightLegPassengers[strCurLegNum].Clear();

                if (tripVM.PreflightLegPassengers.ContainsKey(strNextLegNum) && tripVM.PreflightLegPassengers[strNextLegNum] != null)
                {
                    tripVM.PreflightLegPassengers[strCurLegNum].AddRange(tripVM.PreflightLegPassengers[strNextLegNum]);
                }

                if (!tripVM.PreflightLegPassengersLogisticsHotelList.ContainsKey(strCurLegNum) || tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum] = new List<PreflightLegPassengerHotelViewModel>();
                }
                tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum].Clear();

                if (tripVM.PreflightLegPassengersLogisticsHotelList.ContainsKey(strNextLegNum) && tripVM.PreflightLegPassengersLogisticsHotelList[strNextLegNum] != null)
                {
                    tripVM.PreflightLegPassengersLogisticsHotelList[strCurLegNum].AddRange(tripVM.PreflightLegPassengersLogisticsHotelList[strNextLegNum]);
                }

                if (!tripVM.PreflightLegPaxTransportationList.ContainsKey(strCurLegNum) || tripVM.PreflightLegPaxTransportationList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPaxTransportationList[strCurLegNum] = new PreflightLegPassengerTransportViewModel();
                }

                if (tripVM.PreflightLegPaxTransportationList.ContainsKey(strNextLegNum) && tripVM.PreflightLegPaxTransportationList[strNextLegNum] != null)
                {
                    tripVM.PreflightLegPaxTransportationList[strCurLegNum] = (PreflightLegPassengerTransportViewModel)tripVM.PreflightLegPaxTransportationList[strCurLegNum].InjectFrom(tripVM.PreflightLegPaxTransportationList[strNextLegNum]);
                }

                if (!tripVM.PreflightLegPreflightLogisticsList.ContainsKey(strCurLegNum) || tripVM.PreflightLegPreflightLogisticsList[strCurLegNum] == null)
                {
                    tripVM.PreflightLegPreflightLogisticsList[strCurLegNum] = new PreflightLogisticsViewModel();
                }

                if (tripVM.PreflightLegPreflightLogisticsList.ContainsKey(strNextLegNum) && tripVM.PreflightLegPreflightLogisticsList[strNextLegNum] != null)
                {
                    tripVM.PreflightLegPreflightLogisticsList[strCurLegNum] = (PreflightLogisticsViewModel)tripVM.PreflightLegPreflightLogisticsList[strCurLegNum].InjectFrom(tripVM.PreflightLegPreflightLogisticsList[strNextLegNum]);
                }
            }
            return tripVM;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat= ResponseFormat.Json)]
        public static FSSOperationResult<object> UpdateFleetInfoAfterTailNumChange(string TailNum, long? FleetID)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (pfViewModel != null)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, String.IsNullOrWhiteSpace(TailNum) ? "" : TailNum.ToUpper().Trim(), FleetID ?? 0);
                    if (objRetVal.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllFleetWithFilters> Fleetlist = new List<FlightPakMasterService.GetAllFleetWithFilters>();
                        Fleetlist = objRetVal.EntityList;
                        bindHelper FleetRetObj = new bindHelper();
                        if (Fleetlist != null && Fleetlist.Count > 0)
                        {

                            if (pfViewModel.PreflightMain.Fleet == null)
                                pfViewModel.PreflightMain.Fleet = new FleetViewModel();

                            pfViewModel.PreflightMain.Fleet = (FleetViewModel)pfViewModel.PreflightMain.Fleet.InjectFrom(Fleetlist[0]);
                            pfViewModel.PreflightMain.AircraftID = Fleetlist[0].AircraftID;
                            if (pfViewModel != null && pfViewModel.PreflightLegs != null && pfViewModel.PreflightLegs.Count > 0)
                            {
                                if (pfViewModel.PreflightMain.Fleet.MaximumPassenger != null && pfViewModel.PreflightMain.Fleet.MaximumPassenger > 0)
                                {
                                    for (int i = 0; i < pfViewModel.PreflightLegs.Where(p => p.IsDeleted == false).ToList().Count; i++)
                                    {
                                        var leg = pfViewModel.PreflightLegs[i];
                                        if (leg != null)
                                        {
                                            leg.SeatTotal = Convert.ToInt32(pfViewModel.PreflightMain.Fleet.MaximumPassenger);
                                            leg.ReservationTotal = leg.ReservationTotal;
                                            leg.PassengerTotal = leg.PassengerTotal ?? 0;
                                            leg.ReservationAvailable = leg.SeatTotal - leg.PassengerTotal;
                                            leg.WaitNUM = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
            }
            result.Result = true;
            return result;
        }

        #endregion

        public static void populateCurrentPreflightTrip(PreflightMain Trip)
        {
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = Trip;
            pfViewModel = InjectPreflightMainToPreflightTripViewModel(Trip, pfViewModel);
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var currentTrip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                //currentTrip.PreflightMain.EditMode = true;
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = currentTrip;
            }
        }
    }
}