﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<!--<![endif]-->
<head runat="server">
    <title>Passenger/Requestor</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <style type="text/css">
        #gridPassenger tr td:first-child {
            text-align: center !important;
        }
    </style>
    <script type="text/javascript">
        var passengerRequestorId = null;
        var passengerName = null;
        var selectedRowData = "";
        var jqgridTableId = '#gridPassenger';
        var selectedRowMultiplePassenger = "";
        var selectedPassengersIDs = new Array();

        var isopenlookup = false;
        var ismultiSelect = false;
        var passengerCD = [];
        var ismultiSelect = true;
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            selectedRowData = decodeURI(getQuerystring("PassengerRequestorCD", ""));
            selectedRowMultiplePassenger = $.trim(decodeURI(getQuerystring("PassengerRequestorCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultiplePassenger);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                passengerCD = selectedRowData.split(',');
            }
            $("#btnSubmit").click(function () {
                var selr = $('#gridPassenger').jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultiplePassenger.length > 0)) {
                    var rowData = $('#gridPassenger').getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please Select a Passenger.', popupTitle);
                }
                return false;
            });

            $("#btnPaxGroupCode").click(function () {
                var widthDoc = $(document).width();
                $("#errorMsg").removeClass().addClass('hideDiv');

                popupwindowSearch("/Views/Transactions/Preflight/PaxGroupPopup.aspx?PassengerGroupCD=" + document.getElementById("paxGroupCD").value, 640, 400, OnClientPaxGroupClose);
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindowForFilterGrid("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", "rdAddWindow", popupTitle, 1100, 700, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $('#gridPassenger').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridPassenger').getRowData(selr);
                passengerRequestorId = rowData['PassengerRequestorID'];
                passengerName = rowData['PassengerName'];
                var isActive = rowData['IsActive'];
                var widthDoc = $(document).width();
                if (isActive == 'Yes') {
                    showMessageBox('Active passenger cannot be deleted.', popupTitle);
                    return false;
                }

                if (passengerRequestorId != undefined && passengerRequestorId != null && passengerRequestorId != '' && passengerRequestorId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please Select a Passenger', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        crossDomain: true,
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Passenger&passengerRequestorId=' + passengerRequestorId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            var msg = "This Passenger(" + passengerName + ")";
                            msg = msg.concat(" does not exist.");
                            if (content.indexOf('404'))
                                showMessageBox(msg, popupTitle);
                        }
                    });
                }
            }

            $("#recordEdit").click(function () {
                $("#divMsg1").removeClass().addClass('hideDiv');
                var recordSelected = $(jqgridTableId).getGridParam("selarrrow");
                if (recordSelected.length > 1) {
                    $("#divMsg1").removeClass().addClass('showDiv');                    
                    setTimeout(function () { $("#divMsg1").removeClass().addClass('hideDiv'); }, 120000);
                    return;
                }
                var selr = $('#gridPassenger').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridPassenger').getRowData(selr);
                passengerRequestorId = rowData['PassengerRequestorID'];
                var widthDoc = $(document).width();
                if (passengerRequestorId != undefined && passengerRequestorId != null && passengerRequestorId != '' && passengerRequestorId != 0) {
                    popupwindow("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=&PassengerRequestorID=" + passengerRequestorId, popupTitle, widthDoc + 100, 700, jqgridTableId);
                    $('#gridPassenger').trigger('reloadGrid');
                }
                else {
                    showMessageBox('Please Select a Passenger', popupTitle);
                }
                return false;
            });

            jQuery("#gridPassenger").jqGrid({

                url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $("#gridPassenger").jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.sendIcaoId = $("#chkHomebaseOnly").is(':checked') ? true : false;
                    postData.apiType = 'fss';
                    postData.method = 'PassengerRequestorPopup';
                    postData.cQCustomerID = 0;
                    postData.passengerId = 0;
                    postData.passengerCD = selectedRowData;
                    postData.activeOnly = $("#chkActiveOnly").is(':checked') ? true : false;
                    postData.requestorOnly = $("#chkRequestorOnly").is(':checked') ? true : false;
                    postData.paxGroupId = function () { return $("#paxGroupId").val(); };
                    postData.clientId = 0;
                    postData.sendCustomerId = "true";
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },

                height: 320,
                width: 900,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                enableclear: true,
                emptyrecords: "No records to view.",
                colNames: ["RowNumber",'PassengerRequestorID', 'Code', 'Passenger/Requestor', 'Department', 'Department Description', 'Phone', 'Home', 'Active', 'Requestor'],
                colModel: [
                    { name: 'RowNumber', index: 'RowNumber', key: true, hidden: true },
                    { name: 'PassengerRequestorID', index: 'PassengerRequestorID', key: true, hidden: true },
                    { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', align: 'left' },
                    { name: 'PassengerName', index: 'PassengerName', width: 220, align: 'left', searchoptions: { sopt: ['cn'] } },
                    { name: 'DepartmentCD', index: 'DepartmentCD', align: 'left' },
                    { name: 'DepartmentName', index: 'DepartmentName', align: 'left', width: 220 },
                    { name: 'PhoneNum', index: 'PhoneNum', align: 'left' },
                    { name: 'HomeBaseCD', index: 'HomeBaseCD' },
                    { name: 'IsActive', index: 'IsActive', align: 'center', editable: true, edittype: 'checkbox', formatter: "checkbox", formatoptions: { disabled: true }, search: false, width: 70 },
                    { name: 'IsRequestor', index: 'IsRequestor', align: 'center', editable: true, edittype: 'checkbox', formatter: "checkbox", formatoptions: { disabled: true }, search: false, width: 50 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultiplePassenger = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultiplePassenger, rowData.PassengerRequestorCD);
                    selectedPassengersIDs = JqgridOnSelectRow(ismultiSelect, status, selectedPassengersIDs.toString(), rowData.PassengerRequestorID);
                    selectedPassengersIDs = selectedPassengersIDs.split(',');
                    
                }, onSelectAll: function (id, status) {
                    selectedRowMultiplePassenger = JqgridSelectAll(selectedRowMultiplePassenger, jqgridTableId, id, status, 'PassengerRequestorCD');
                    selectedPassengersIDs = JqgridSelectAll(selectedPassengersIDs.toString(), jqgridTableId, id, status, 'PassengerRequestorID');
                    selectedPassengersIDs = selectedPassengersIDs.split(',');
                    jQuery("#hdnselectedfccd").val(selectedRowMultiplePassenger);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultiplePassenger, rowid, rowObject.PassengerRequestorCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                    setSelectRow();
                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $("#gridPassenger").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });

        function reloadPassengers() {
            var paxGroupCd = $.trim($("#paxGroupCD").val());
            $("#hdnSearchResults").val("1");
            if (IsNullOrEmpty(paxGroupCd)) {
                $("#paxGroupId").val(0);
                $(jqgridTableId).jqGrid().trigger('reloadGrid');
            }
            else if (!IsNullOrEmpty($("#paxGroupId").val()))
                {
                $(jqgridTableId).jqGrid().trigger('reloadGrid');
                $("#hdnSearchResults").val("");
                }
            selectedRowMultiplePassenger = jQuery("#hdnselectedfccd").val();
        }
        function reloadPassengersSetSelection(id) {
            $("#hdfUpdatedId").val(id);
            isopenlookup = true;
            selectedRowData = id;
            $(jqgridTableId).jqGrid().trigger('reloadGrid');
        }
        function setSelectRow() {
            if ($("#hdfUpdatedId").val() != "") {
                var myGrid = $(jqgridTableId);
                var rowIds = myGrid.jqGrid('getDataIDs');
                for (var i = 0; i < rowIds.length; i++) {
                    var rowData = myGrid.jqGrid('getRowData', rowIds[i]);
                    if (rowData['PassengerRequestorCD'] == $("#hdfUpdatedId").val()) {
                        myGrid.jqGrid('setSelection', rowIds[i]);
                        break;
                    }
                }
                $("#hdfUpdatedId").val("");
            }
        }
        $(document).ready(function () {
            $("#paxGroupCD").bind('blur keydown', function (e) {
                if (e.type === 'keydown' && e.keyCode !== 10 && e.keyCode !== 13)
                    return;
                var paxGroupCd = $.trim($("#paxGroupCD").val());
                var params = JSON.stringify({ paxGroupCode: paxGroupCd });
                if (IsNullOrEmpty(paxGroupCd)) {
                    paxGroupCd = 0;
                }
                if (!IsNullOrEmpty(paxGroupCd))
                {
                    $.ajax({
                        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CheckPaxGroupCode',
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: params,
                        async: true,
                        success: function (data) {
                            var returnResult = verifyReturnedResult(data);
                            if (returnResult == true && data.d.Success == true) {
                                var result = data.d.Result;
                                if (result.Status == true) {
                                    $("#paxGroupId").val(result.ID);
                                    reloadPassengers();
                                    if ($("#hdnSearchResults").val() == "1") {
                                        $(jqgridTableId).jqGrid().trigger('reloadGrid');
                                        $("#hdnSearchResults").val("");
                                    }
                                    $("#errorMsg").removeClass().addClass('hideDiv');
                                } else {
                                    $("#errorMsg").addClass('showDiv');
                                }
                            }                         
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (content.indexOf('404')) {
                                $("#paxGroupId").val(0);
                                $("#errorMsg").addClass('showDiv');
                            }
                        }
                    });
                }
                else {
                    $("#paxGroupId").val(0);
                    $(jqgridTableId).jqGrid().trigger('reloadGrid');
                    $("#errorMsg").removeClass().addClass('hideDiv');
                }

            });
        });


    </script>

    <script type="text/javascript">

        function returnToParent(rowData, one) {
            selectedRowMultiplePassenger = jQuery.trim(selectedRowMultiplePassenger);
            if (selectedRowMultiplePassenger.lastIndexOf(",") == selectedRowMultiplePassenger.length - 1)
                selectedRowMultiplePassenger = selectedRowMultiplePassenger.substr(0, selectedRowMultiplePassenger.length - 1);

            var oArg = new Object();
            var oWnd = GetRadWindow();
            if (one == 0) {
                oArg.PassengerRequestorCD = selectedRowMultiplePassenger;
            }
            else {
                oArg.PassengerRequestorCD = rowData["PassengerRequestorCD"];
                selectedPassengersIDs.push(rowData["PassengerRequestorID"]);
            }
            
            if ($(jqgridTableId).getGridParam("selarrrow").length > 0) {
                var selectedRowNumers = $(jqgridTableId).getGridParam("selarrrow");
                for (var i = 0; i < selectedRowNumers.length; i++) {
                    var rData = $(jqgridTableId).getRowData(selectedRowNumers[i]);
                    if (jQuery.inArray(jQuery.trim(rData["PassengerRequestorID"]), selectedPassengersIDs) == -1) {                        
                        selectedPassengersIDs.push(rData["PassengerRequestorID"]);
                    }
                }
            }
            
            var validPaxIds = new Array();            
            validPaxIds = $.grep(selectedPassengersIDs, function (n) {
                return (n);
            });
            
            oArg.PassengerRequestorID = validPaxIds.toString();
            oArg.PassengerName = rowData["PassengerName"];
            oArg.DepartmentCD = rowData["DepartmentCD"];
            oArg.DepartmentName = rowData["DepartmentName"];
            oArg.AdditionalPhoneNum = rowData["AdditionalPhoneNum"];
            oArg.Arg1 = oArg.PassengerRequestorCD;
            oArg.CallingButton = "PassengerRequestorCD";

            var val = getUrlVarsWithParam(oWnd.GetUrl())["ControlName"];
            if (val === 'RequestorCD') {
                oArg.Arg1 = oArg.PassengerRequestorCD;
                oArg.CallingButton = "RequestorCD";
            }
            if (oArg) {
                oWnd.close(oArg);
            }
        }

        function OnClientPaxGroupClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg !== null) {
                if (arg) {
                    document.getElementById("paxGroupId").value = arg.PassengerGroupID;
                    document.getElementById("paxGroupCD").value = $.trim(arg.PassengerGroupCD);
                    $("#errorMsg").removeClass().addClass('hideDiv');
                    reloadPassengers();
                }
                else {
                    document.getElementById("paxGroupId").value = "";
                    document.getElementById("paxGroupCD").value = "";
                    $("#errorMsg").removeClass().addClass('hideDiv');
                }
            }
        }
        function GetDimensions(sender, args) {
            var bounds = sender.getWindowBounds();
            return;
        }

    </script>

    <style type="text/css">
        .showDiv {
            margin-right: 77px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }

      
    </style>
</head>
<body class="removeBlueBar">
     <input type="hidden" id="hdfUpdatedId" value="" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>        
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="rdPaxGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientPaxGroupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PaxGroupPopup.aspx">
                </telerik:RadWindow>
            </Windows>            
        </telerik:RadWindowManager>

        <div class="jqgrid">
            <input type="hidden" id="hdnselectedfccd" value="" />
            <input type="hidden" id="hdnSearchResults" value="" />
            <div>
                <table class="box1">
                    <tr>
                        <td align="left">
                            <div class="pax_group_left" style="text-align: left; margin-top: 0px">
                                <input id="paxGroupId" type="hidden" value="" />
                                PAX Group
                            <input id="paxGroupCD" type="text" style="width: 65px" value="" />
                                <input type="button" class="browse-button" id="btnPaxGroupCode" value="" name="btnPaxGroupCode" />
                                <input id="btnSearch" class="button" value="Search" type="submit" name="btnSearch" onclick="reloadPassengers(); return false;" />

                            </div>
                            <div class="right_active_request">
                                <input type="checkbox" name="Active" value="Active Only" id="chkActiveOnly" />
                                Active Only
                            
                            <input type="checkbox" name="Request" value="Requestor Only" id="chkRequestorOnly"  />
                                Requestor Only
                            </div>

                            <div id="errorMsg" class="hideDiv">Invalid Pax Group code.!</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridPassenger" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>                                
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
							 <div class="clear">                                 
							 </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>                            
                        </td>
                    </tr>
                    <tr style="min-height:15px">
                        <td>
                            <div id="divMsg1" style="text-align:left" class="hideDiv">Please select one (1) record to edit.</div>                            
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>

</html>
