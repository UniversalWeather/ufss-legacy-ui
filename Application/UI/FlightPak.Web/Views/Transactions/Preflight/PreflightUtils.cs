﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Collections;
using System.Security.Permissions;
using System.Web;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Helpers.Validators;
using FlightPak.Web.Framework.Helpers.Preflight;
using FlightPak.Web.ViewModels;
using System.Web.UI;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public class PreflightUtils : IPreflightUtils
    {
        public FPKConversionHelper Utilities = new FPKConversionHelper();
        /// <summary>
        /// This function will calculate the Arrival and departure datetime based on the Departure Date selected in Preflight tab
        /// </summary>
        public Hashtable CalculateLegDateTime(DateTime? EstDepartureDT, String localTimeWithLiterals, String homeTimeWithLiterals,
            String utcTimeWithLiterals, String arrivalTimeWithLiterals, String arrivalHomeTimeWithLiterals, String arrivalUtcTimeWithLiterals,
            String datePickerDateFormat, String hdnDepartICAOValue, bool? IsAutomaticCalcLegTimes, CalculationService.ICalculationService objDstsvc)
        {
            String strLocalTime, strHomeTime, strUtcTime, strArrivalLocalTime, strArrivalHomeTime, strArrivalUtcTime, strLocalDate, strUtcDate, strHomeDate;
            Hashtable legTimeHash = new Hashtable();

            if (EstDepartureDT != null)
            {
                DateTime? dt = new DateTime();
                DateTime ldGmtDep = new DateTime();
                localTimeWithLiterals = ":";
                // Get and Set Time Fields
                string[] TimeArray = localTimeWithLiterals.Split(':');
                string _hour = "00";
                string _minute = "00";
                if (!string.IsNullOrEmpty(TimeArray[0]))
                    _hour = TimeArray[0];
                if (!string.IsNullOrEmpty(TimeArray[1]))
                    _minute = TimeArray[1];

                int StartHrs = Convert.ToInt16(_hour);
                int StartMts = Convert.ToInt16(_minute);

                strLocalTime = _hour + ":" + _minute;
                strHomeTime = _hour + ":" + _minute;
                strUtcTime = _hour + ":" + _minute;

                strArrivalLocalTime = _hour + ":" + _minute;
                strArrivalHomeTime = _hour + ":" + _minute;
                strArrivalUtcTime = _hour + ":" + _minute;

                strLocalTime = MiscUtils.CalculationTimeAdjustment(localTimeWithLiterals);
                strHomeTime = MiscUtils.CalculationTimeAdjustment(homeTimeWithLiterals);
                strUtcTime = MiscUtils.CalculationTimeAdjustment(utcTimeWithLiterals);

                strArrivalLocalTime = MiscUtils.CalculationTimeAdjustment(arrivalTimeWithLiterals);
                strArrivalHomeTime = MiscUtils.CalculationTimeAdjustment(arrivalHomeTimeWithLiterals);
                strArrivalUtcTime = MiscUtils.CalculationTimeAdjustment(arrivalUtcTimeWithLiterals);

                strLocalDate = EstDepartureDT.FSSParseDateTimeString(datePickerDateFormat);
                strUtcDate = strLocalDate;
                strHomeDate = strLocalDate;

                if (!string.IsNullOrEmpty(hdnDepartICAOValue))
                {
                    if (IsAutomaticCalcLegTimes != null && IsAutomaticCalcLegTimes.Value == true)
                    {
                        dt = dt.FSSParseDateTime(strLocalDate, datePickerDateFormat);

                        dt = dt.Value.AddHours(StartHrs);
                        dt = dt.Value.AddMinutes(StartMts);
                        //GMTDept
                        ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepartICAOValue), dt.Value, true, false);

                        string startHrs = "0000" + ldGmtDep.Hour.ToString();
                        string startMins = "0000" + ldGmtDep.Minute.ToString();
                        strUtcDate = String.Format(CultureInfo.InvariantCulture, "{0:" + datePickerDateFormat + "}", ldGmtDep);
                        strUtcTime = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                    }
                }

                legTimeHash["DepartureLocalDate"]=strLocalDate;
                legTimeHash["DepartureUtcDate"] = strUtcDate;
                legTimeHash["DepartureHomeDate"]=strLocalDate;

                legTimeHash["LocalDepartureTime"]=strLocalTime;
                legTimeHash["UtcDepartureTime"]=strUtcTime;
                legTimeHash["HomeDepartureTime"]=strHomeTime;

                legTimeHash["LocalArrivalTime"]=strArrivalLocalTime;
                legTimeHash["UTCArrivalTime"]=strArrivalUtcTime;
                legTimeHash["HomeArrivalTime"]=strArrivalHomeTime;

            }
            return legTimeHash;
        }

        public decimal CalculateDistance(bool _IsKilometer, double deplatdeg = 0,
           double deplatmin = 0,
           string lcdeplatdir = "N",
           double deplngdeg = 0,
           double deplngmin = 0,
           string lcdeplngdir = "E",
           double arrlatdeg = 0,
           double arrlatmin = 0,
           string lcArrLatDir = "N",
           double arrlngdeg = 0,
           double arrlngmin = 0,
           string arrlngdir = "E")
        {
            double lnMiles = 0;
            double lnDepLat = ((deplatdeg + (deplatmin / 60)) * ((lcdeplatdir.ToUpper() == "N") ? 1 : -1));
            double lnDepLng = ((deplngdeg + (deplngmin / 60)) * ((lcdeplngdir.ToUpper() == "W") ? 1 : -1));
            double lnArrLat = ((arrlatdeg + (arrlatmin / 60)) * ((lcArrLatDir.ToUpper() == "N") ? 1 : -1));
            double lnArrLng = ((arrlngdeg + (arrlngmin / 60)) * ((arrlngdir.ToUpper() == "W") ? 1 : -1));

            double dLat1InRad = lnDepLat * (Math.PI / 180.0);
            double dLong1InRad = lnDepLng * (Math.PI / 180.0);
            double dLat2InRad = lnArrLat * (Math.PI / 180.0);
            double dLong2InRad = lnArrLng * (Math.PI / 180.0);

            double dLongitude = dLong2InRad - dLong1InRad;
            double dLatitude = dLat2InRad - dLat1InRad;

            double lnLongDiff = lnArrLng - lnDepLng;

            double RadlnDepLat = (Math.PI / 180.0) * lnDepLat;
            double RadlnArrLat = (Math.PI / 180.0) * lnArrLat;
            double RadlnlnLongDiff = (Math.PI / 180.0) * lnLongDiff;


            lnMiles = 60 * ((180.0 / Math.PI) * (
                                           Math.Acos(
                                                           Math.Sin(
                                                           RadlnDepLat
                                                           )
                                                           *
                                                           Math.Sin(
                                                           RadlnArrLat
                                                           )
                                                           +
                                                           Math.Cos(
                                                           RadlnDepLat
                                                           )
                                                           *
                                                           Math.Cos(
                                                           RadlnArrLat
                                                           )
                                                           *
                                                           Math.Cos(
                                                           RadlnlnLongDiff
                                                           )
                                                     )
                                                )
                        );

            if (double.IsNaN(lnMiles))
                lnMiles = 0;
            decimal Miles = (decimal)Math.Floor(lnMiles);
            return Miles;
        }

        public Dictionary<string, string> CalculateBias(string PowerSetting, double PowerSettings1TakeOffBias,
            double PowerSettings1LandingBias, double PowerSettings1TrueAirSpeed, double PowerSettings2TakeOffBias,
            double PowerSettings2LandingBias, double PowerSettings2TrueAirSpeed, double PowerSettings3TakeOffBias,
            double PowerSettings3LandingBias, double PowerSettings3TrueAirSpeed, double DepAirportTakeoffBIAS,
            double ArrAirportLandingBIAS, decimal TimeDisplayTenMin,out double dblbias,out double dbllandbias, out double dbltas)
        {
            List<double> BiasList = new List<double>();
            Dictionary<string, string> RetObj = new Dictionary<string, string>();
            string Bias, LandBias, TAS = string.Empty;

            BiasList = CalcBiasTas(PowerSetting, PowerSettings1TakeOffBias, PowerSettings1LandingBias, PowerSettings1TrueAirSpeed, PowerSettings2TakeOffBias, PowerSettings2LandingBias, PowerSettings2TrueAirSpeed, PowerSettings3TakeOffBias, PowerSettings3LandingBias, PowerSettings3TrueAirSpeed, DepAirportTakeoffBIAS, ArrAirportLandingBIAS);
            if (BiasList != null)
            {
                Bias = Utilities.Preflight_ConvertTenthsToMins(BiasList[0].ToString(), TimeDisplayTenMin);
                LandBias = Utilities.Preflight_ConvertTenthsToMins(BiasList[1].ToString(), TimeDisplayTenMin);
                TAS = BiasList[2].ToString();
                dblbias = BiasList[0];
                dbllandbias = BiasList[1];
                dbltas = BiasList[2];
                
            }
            else
            {
                Bias = TenthMinuteFormat.Minute == (TenthMinuteFormat)TimeDisplayTenMin ? "00:00" : "0.0";
                LandBias = TenthMinuteFormat.Minute == (TenthMinuteFormat)TimeDisplayTenMin ? "00:00" : "0.0";
                TAS = "0.0";
                dblbias = 0;
                dbllandbias = 0;
                dbltas = 0;
            }
            if (!string.IsNullOrEmpty(Bias) && Bias.IndexOf('.') < 0 && TimeDisplayTenMin == 1)
                Bias += ".0";
            if (!string.IsNullOrEmpty(LandBias) && LandBias.IndexOf('.') < 0 && TimeDisplayTenMin == 1)
                LandBias += ".0";

            RetObj["Bias"]=Bias;
            RetObj["LandBias"]=LandBias;
            RetObj["TAS"]=TAS;

            return RetObj;
        }

        private List<double> CalcBiasTas(string PowerSetting, double PowerSettings1TakeOffBias, double PowerSettings1LandingBias, double PowerSettings1TrueAirSpeed, double PowerSettings2TakeOffBias, double PowerSettings2LandingBias, double PowerSettings2TrueAirSpeed, double PowerSettings3TakeOffBias, double PowerSettings3LandingBias, double PowerSettings3TrueAirSpeed, double DepAirportTakeoffBIAS, double ArrAirportLandingBIAS)
        {
            double lnToBias = 0.0f;
            double lnLndBias = 0.0f;
            double lnTas = 0.0f;

            switch (PowerSetting)
            {
                case "1":
                    {
                        lnToBias = PowerSettings1TakeOffBias;
                        lnLndBias = PowerSettings1LandingBias;
                        lnTas = PowerSettings1TrueAirSpeed;
                        break;
                    }
                case "2":
                    {
                        lnToBias = PowerSettings2TakeOffBias;
                        lnLndBias = PowerSettings2LandingBias;
                        lnTas = PowerSettings2TrueAirSpeed;
                        break;
                    }

                case "3":
                    {
                        lnToBias = PowerSettings3TakeOffBias;
                        lnLndBias = PowerSettings3LandingBias;
                        lnTas = PowerSettings3TrueAirSpeed;
                        break;
                    }
                default:
                    {
                        lnToBias = 0;
                        lnLndBias = 0;
                        lnTas = 0;
                        break;
                    }
            }

            lnToBias = lnToBias + DepAirportTakeoffBIAS;
            lnLndBias = lnLndBias + ArrAirportLandingBIAS;
            return new List<double>() { lnToBias, lnLndBias, lnTas };
        }
        
        public double CalculateWind(double aircraftWindAltitude, int _windReliability,
            double deplatdeg, double deplatmin, string lcDepLatDir,
            double deplngdeg, double deplngmin, string lcDepLngDir, long lnDepartZone,
            double arrlatdeg, double arrlatmin, string lcArrLatDir,
            double arrlngdeg, double arrlngmin, string lcArrLngDir, long lnArrivZone, string localdate, CalculationService.ICalculationService objDstsvc, string Formate = "")
        {
            DateTime? dt=null;
            double Wind = 0;
            if (localdate != null)
            {
                if (!string.IsNullOrEmpty(localdate))
                {
                    dt = dt.FSSParseDateTime(localdate, Formate);
                    Wind = objDstsvc.GetWindBasedOnParameter(aircraftWindAltitude, _windReliability, GetQtr(dt.Value).ToString(),
                           deplatdeg, deplatmin, lcDepLatDir,
                           deplngdeg, deplngmin, lcDepLngDir, lnDepartZone,
                           arrlatdeg, arrlatmin, lcArrLatDir,
                           arrlngdeg, arrlngmin, lcArrLngDir, lnArrivZone);
                }
            }
            return Wind;
        }

        private int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2)
                    lnqtr = 1;
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                    lnqtr = 2;
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                    lnqtr = 3;
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                    lnqtr = 4;
                else
                    lnqtr = 0;
                return lnqtr;
            }
        }

        public string GetIcaoEte(double lnwind, double lnLndBias, double lntas, double lnToBias, double lnmiles, string IsFixedRotary, 
            decimal TimeDisplayTenMin, decimal ElapseTimeRounding)
        {
            double lnEte = 0;
            string lcLndBias = string.Empty;
            if ((lnwind + lntas) > 0)
            {
                double lnFHr = 0;
                double lnFMin = 0;
                lnEte = Math.Round(lnmiles / (lntas + lnwind), 3);
                if (lnEte == 0 && IsFixedRotary.ToUpper() == "R")
                    lnEte = 0.1;

                string lcEte = Convert.ToString(lnEte);
                int lnPos = lcEte.IndexOf('.');
                double lnHr = 0, lnMin = 0;
                if (lnPos > 0)
                {
                    if (!double.TryParse(lcEte.Substring(0, lnPos), out lnHr))
                        lnHr = 0;
                    if (double.TryParse(lcEte.Substring(lnPos), out lnMin))
                        lnMin = Math.Round(lnMin * 60, 0);
                    else
                        lnMin = 0;
                }
                else
                {
                    if (!double.TryParse(lcEte.ToString(), out lnHr))
                        lnHr = 0;
                }
                lnFHr = lnHr;
                lnFMin = lnMin;
                lnHr = 0;
                lnMin = 0;
                string lcToBias = string.Empty;
                lcToBias = lnToBias.ToString();//Convert.ToString(Math.Round(lnToBias, 3));
                lcToBias.Trim();
                lnPos = lcToBias.IndexOf('.');
                if (lnPos > 0)
                {
                    if (!double.TryParse(lcToBias.Substring(0, lnPos), out lnHr))
                        lnHr = 0;
                    if (double.TryParse(lcToBias.Substring(lnPos), out lnMin)) //ROUND(VAL(SUBSTR(lcToBias,lnPos+1))/1000 * 60,0)
                        lnMin = Math.Round(lnMin * 60, 0);
                    else
                        lnMin = 0;
                }
                else
                {
                    if (!double.TryParse(lcToBias.ToString(), out lnHr))
                        lnHr = 0;
                }
                lnFHr = lnFHr + lnHr;
                lnFMin = lnFMin + lnMin;
                lcLndBias = lnLndBias.ToString();// Convert.ToString(Math.Round(lnLndBias, 3));

                lnHr = 0;
                lnMin = 0;

                lnPos = lcLndBias.IndexOf('.');//AT(".",lcLndBias)
                if (lnPos > 0)
                {
                    if (!double.TryParse(lcLndBias.Substring(0, lnPos), out lnHr)) //VAL(LEFT(lcLndBias,lnPos-1))
                        lnHr = 0;
                    if (double.TryParse(lcLndBias.Substring(lnPos), out lnMin)) //ROUND(VAL(SUBSTR(lcLndBias,lnPos+1))/1000 * 60,0)
                        lnMin = Math.Round(lnMin * 60, 0);
                    else
                        lnMin = 0;
                }
                else
                {
                    if (!double.TryParse(lcLndBias.ToString(), out lnHr)) //VAL(LEFT(lcLndBias,lnPos-1))
                        lnHr = 0;
                }
                lnFHr = lnFHr + lnHr;
                lnFMin = lnFMin + lnMin;
                lnEte = lnFHr + (double)Math.Round((lnFMin / 60), 3);

                lcEte = lnEte.ToString();
                lnPos = lcEte.IndexOf('.');
                string lcHr, lcMin;
                lcHr = lcMin = string.Empty;
                if (lnPos > 0)
                {
                    lcHr = lcEte.Substring(0, lnPos);
                    lcMin = lcEte.Substring(lnPos + 1, (3 > lcEte.Substring(lnPos + 1, lcEte.Length - 1 - lnPos).Length) ? lcEte.Substring(lnPos + 1, lcEte.Length - 1 - lnPos).Length : 3);

                    lcEte = lcHr + "." + lcMin;

                    if (!double.TryParse(lcEte.ToString(), out lnEte))
                        lnEte = 0;
                }
            }
            lnEte= RoundElpTime(lnEte, TimeDisplayTenMin, ElapseTimeRounding);
           string strlnEte= Utilities.Preflight_ConvertTenthsToMins(lnEte.ToString(), TimeDisplayTenMin);
           return Common.Utility.DefaultEteResult(strlnEte);
        }

        public double RoundElpTime(double lnElp_Time, decimal TimeDisplayTenMin,decimal ElapseTimeRounding)
        {
            if (TimeDisplayTenMin == 2)
            {

                decimal ElapseTMRounding = 0;

                //lnEteRound=(double)CompanyLists[0].ElapseTMRounding~lnRound;
                double lnElp_Min = 0.0;
                double lnEteRound = 0.0;

                if (ElapseTimeRounding != null)
                    ElapseTMRounding = (decimal)ElapseTimeRounding;

                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;

                if (lnEteRound > 0)
                {

                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    //lnElp_Min = lnElp_Min % lnEteRound;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);


                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);

                }
            }
            return lnElp_Time;
        }

        public LegDateTime ClearAllDatesBasedonDeptOrArrival(DepartAirportDetails depAirport, ArrivalAirportDetails arrAirport, LegDateTime legdatetime,bool isDepartureConfirmed,string DateFormat)
        {
            Dictionary<string, string> RetObj = new Dictionary<string, string>();
            if (isDepartureConfirmed) 
            {
                if (!string.IsNullOrEmpty(depAirport.DepID))
                {
                    if (!string.IsNullOrEmpty(legdatetime.DepUtcDate) && (legdatetime.DepUtcTime != legdatetime.DepLocalTime))
                    {
                        RetObj=ClearAllDatesBasedonDeptOrArrival(depAirport.depIsDayLightSaving,depAirport.depDayLightSavingStartDT,depAirport.depDayLightSavingEndDT,
                            depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, "UTC", legdatetime.DepUtcDate, legdatetime.DepUtcTime, DateFormat);
                        legdatetime.DepLocalDate = RetObj["Date"];
                        legdatetime.DepLocalTime = RetObj["Time"];

                        legdatetime.ArrHomeDate = string.Empty;
                        legdatetime.ArrHomeTime = "00:00";
                        legdatetime.ArrLocalDate = string.Empty;
                        legdatetime.ArrLocalTime = "00:00";
                        legdatetime.ArrUtcDate = string.Empty;
                        legdatetime.ArrUtcTime = "00:00";
                    }
                    else {
                        if (!string.IsNullOrEmpty(legdatetime.DepLocalDate)) {
                            RetObj = ClearAllDatesBasedonDeptOrArrival(depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                           depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, "LOCAL", legdatetime.DepLocalDate, legdatetime.DepLocalTime, DateFormat);
                            legdatetime.DepUtcDate = RetObj["Date"];
                            legdatetime.DepUtcTime = RetObj["Time"];

                            legdatetime.ArrHomeDate = string.Empty;
                            legdatetime.ArrHomeTime = "00:00";
                            legdatetime.ArrLocalDate = string.Empty;
                            legdatetime.ArrLocalTime = "00:00";
                            legdatetime.ArrUtcDate = string.Empty;
                            legdatetime.ArrUtcTime = "00:00";
                        }
                    }
                }
            }
            else 
            {
                if (!string.IsNullOrEmpty(arrAirport.ArrivalID))
                {
                    if (!string.IsNullOrEmpty(legdatetime.ArrLocalDate) && (legdatetime.ArrUtcTime != legdatetime.ArrLocalTime))
                    {
                        RetObj = ClearAllDatesBasedonDeptOrArrival(arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT, arrAirport.arrDayLightSavingEndDT,
                            arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT, "UTC", legdatetime.ArrUtcDate, legdatetime.ArrUtcTime, DateFormat);
                        legdatetime.ArrLocalDate = RetObj["Date"];
                        legdatetime.ArrLocalTime = RetObj["Time"];

                        legdatetime.DepHomeDate = string.Empty;
                        legdatetime.DepHomeTime = "00:00";
                        legdatetime.DepLocalDate = string.Empty;
                        legdatetime.DepLocalTime = "00:00";
                        legdatetime.DepUtcDate = string.Empty;
                        legdatetime.DepUtcTime = "00:00";

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(legdatetime.ArrLocalDate))
                        {
                            RetObj = ClearAllDatesBasedonDeptOrArrival(arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT, arrAirport.arrDayLightSavingEndDT,
                            arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT, "LOCAL", legdatetime.ArrLocalDate, legdatetime.ArrLocalTime, DateFormat);
                            legdatetime.ArrUtcDate = RetObj["Date"];
                            legdatetime.ArrUtcTime = RetObj["Time"];

                            legdatetime.DepHomeDate = string.Empty;
                            legdatetime.DepHomeTime = "00:00";
                            legdatetime.DepLocalDate = string.Empty;
                            legdatetime.DepLocalTime = "00:00";
                            legdatetime.DepUtcDate = string.Empty;
                            legdatetime.DepUtcTime = "00:00";
                        }
                    }
                }
            }
            return legdatetime;
        }

        private Dictionary<string,string> ClearAllDatesBasedonDeptOrArrival(
            bool? depIsDayLightSaving,
            DateTime? depDayLightSavingStartDT,
            DateTime? depDayLightSavingEndDT,
            string depDaylLightSavingStartTM,
            string depDayLightSavingEndTM,
            decimal? depOffsetToGMT, 
            string CalculationBased,string date,string time,string DateFormat)
        {
            Dictionary<string,string> RetObj=new Dictionary<string, string>();
            if(CalculationBased=="UTC")
            {
                DateTime? UTCDate=new DateTime();
                DateTime? LocalDate=new DateTime();
                UTCDate = UTCDate.FSSParseDateTime(date, DateFormat);
                UTCDate = UTCDate.Value.AddHours(Convert.ToInt16(time.Substring(0, 2)));
                UTCDate = UTCDate.Value.AddMinutes(Convert.ToInt16(time.Substring(3, 2)));
                LocalDate = GetGMT(UTCDate.Value, false, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM, depOffsetToGMT, false);

                RetObj["CalculationBased"]=CalculationBased;
                RetObj["Date"]=LocalDate.FSSParseDateTimeString(DateFormat);
                RetObj["Time"]=Convert.ToString(LocalDate.Value.Hour) + ":" + Convert.ToString(LocalDate.Value.Minute);
            }
            else
            {
                DateTime? LocalDate = new DateTime();
                DateTime? UTCDate = new DateTime();
                LocalDate = LocalDate.FSSParseDateTime(date, DateFormat);
                LocalDate = LocalDate.Value.AddHours(Convert.ToInt16(time.Substring(0, 2)));
                LocalDate = LocalDate.Value.AddMinutes(Convert.ToInt16(time.Substring(3, 2)));
                UTCDate = GetGMT(LocalDate.Value, true, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM, depOffsetToGMT, false);

                RetObj["CalculationBased"]=CalculationBased;
                RetObj["Date"] = UTCDate.FSSParseDateTimeString(DateFormat);
                RetObj["Time"]=Convert.ToString(UTCDate.Value.Hour) + ":" + Convert.ToString(UTCDate.Value.Minute);
            }
            return RetObj;
        }

        private DateTime GetGMT(DateTime ldDtTm, bool IIFindGMT,
            bool? IsDayLightSaving,
            DateTime? DayLightSavingStartDT,
            DateTime? DayLightSavingEndDT,
            string DaylLightSavingStartTM,
            string DayLightSavingEndTM,
            decimal? OffsetToGMT,
            bool IIGmt = false)
        {
            DateTime ldNewDtTm = DateTime.MinValue;
            DateTime ldDstBegtm = DateTime.MinValue;
            DateTime ldDstEndtm = DateTime.MinValue;
            double lnDst = 0;
            try
            {
                if (ldDtTm == null || ldDtTm == DateTime.MinValue)
                {
                    return ldDtTm;
                }
                ldNewDtTm = ldDtTm;
                if (IIGmt == true)
                {
                    return ldDtTm;
                }
                DateTime ldDstBeg1 = DateTime.MinValue;
                DateTime ldDstEnd1 = DateTime.MinValue;
                bool lbIsDayLightSaving = false;
                decimal lnOffsetToGMT = 0;

                if (IsDayLightSaving != null)
                {
                    if (DayLightSavingStartDT != null && DayLightSavingEndDT != null)
                        lbIsDayLightSaving = (bool)IsDayLightSaving;
                }

                if (DayLightSavingStartDT != null)
                {
                    ldDstBeg1 = (DateTime)DayLightSavingStartDT;
                    ldDstBeg1 = new DateTime(ldDtTm.Year, ldDstBeg1.Month, ldDstBeg1.Day);
                    if (!string.IsNullOrEmpty(DaylLightSavingStartTM))
                        ldDstBegtm = Convert.ToDateTime(DaylLightSavingStartTM);
                    else
                        lbIsDayLightSaving = false;

                    ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                    ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                }
                if (DayLightSavingEndDT != null)
                {
                    ldDstEnd1 = (DateTime)DayLightSavingEndDT;
                    ldDstEnd1 = new DateTime(ldDtTm.Year, ldDstEnd1.Month, ldDstEnd1.Day);
                    if (!string.IsNullOrEmpty(DayLightSavingEndTM))
                        ldDstEndtm = Convert.ToDateTime(DayLightSavingEndTM);
                    else
                        lbIsDayLightSaving = false;
                    ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                    ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                    ldDstEnd1 = ldDstEnd1.AddSeconds(-60);
                }

                if (OffsetToGMT != null)
                    lnOffsetToGMT = (decimal)OffsetToGMT;

                if (!IIFindGMT)
                {
                    if (lbIsDayLightSaving)
                    {
                        if (ldDstEnd1.Month < ldDstBeg1.Month)
                        {
                            if (ldDtTm.Month >= ldDstBeg1.Month && ldDtTm.Month <= 12)
                            {
                                ldDstEnd1 = new DateTime(ldDstEnd1.Year + 1, ldDstEnd1.Month, ldDstEnd1.Day);
                                ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                                ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                            }

                            else if (ldDtTm.Month >= 1 && ldDtTm.Month <= ldDstEnd1.Month)
                            {
                                ldDstBeg1 = new DateTime(ldDstBeg1.Year - 1, ldDstBeg1.Month, ldDstBeg1.Day);
                                ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                                ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                            }
                        }

                        if (ldDtTm.Equals(ldDstBeg1))
                        {
                            double ldBegOffSet = (double)(lnOffsetToGMT * 3600) + 3600;
                            ldNewDtTm = ldDtTm.AddSeconds(ldBegOffSet);
                        }
                        else if (ldDtTm.Equals(ldDstEnd1))
                        {
                            double ldEndOffSet = (((double)lnOffsetToGMT * 3600) + (1 * 3600));
                            ldNewDtTm = ldDtTm.AddSeconds(ldEndOffSet);
                        }

                        else if ((ldDstBeg1 < ldDstEnd1) && (ldDtTm >= ldDstBeg1 && ldDtTm <= ldDstEnd1))
                        {
                            double airPortOffSet = (((double)lnOffsetToGMT * 3600) + 3600);
                            ldNewDtTm = ldDtTm.AddSeconds(airPortOffSet);
                        }
                        else
                        {
                            double gmtOffSet = ((double)lnOffsetToGMT * 3600);
                            ldNewDtTm = ldDtTm.AddSeconds(gmtOffSet);
                        }
                    }
                    else
                    {
                        double gmtOffSet = ((double)lnOffsetToGMT * 3600) + lnDst;
                        ldNewDtTm = ldDtTm.AddSeconds(gmtOffSet);
                    }
                }
                else
                {
                    if (lbIsDayLightSaving)
                    {
                        if ((ldDstEnd1.Month) < (ldDstBeg1.Month))
                        {
                            if (ldDtTm.Month >= ldDstBeg1.Month && ldDtTm.Month <= 12)
                            {
                                ldDstEnd1 = new DateTime(ldDstEnd1.Year + 1, ldDstEnd1.Month, ldDstEnd1.Day);
                                ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                                ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                            }
                            else if ((ldDtTm.Month >= 1) && (ldDtTm.Month <= ldDstEnd1.Month))
                            {
                                ldDstBeg1 = new DateTime(ldDstBeg1.Year - 1, ldDstBeg1.Month, ldDstBeg1.Day);
                                ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                                ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                            }
                        }
                        double x5 = (double)lnOffsetToGMT * 3600;
                        DateTime ltLocDstChgBeg = ldDstBeg1.AddSeconds(x5);

                        double x6 = ((double)OffsetToGMT * 3600) + (((double)lnOffsetToGMT != 0) ? (1 * 3600) : 0);
                        DateTime ltLocDstChgEnd = ldDstEnd1.AddSeconds(x6);
                        double x7 = (double)lnOffsetToGMT * 3600;

                        double x8 = (((double)lnOffsetToGMT * 3600) + 3540);

                        if ((ldDtTm >= ltLocDstChgBeg) && (ldDtTm <= ltLocDstChgEnd))
                        {

                            lnDst = 1 * 3600;
                        }
                        if ((ldDtTm >= ldDstBeg1.AddSeconds(x7)) && (ldDtTm <= ldDstBeg1.AddSeconds(x8)))
                        {
                            lnDst = 0;
                        }
                    }
                    double x9 = (((double)lnOffsetToGMT * 3600) + lnDst);
                    ldNewDtTm = ldDtTm.AddSeconds(-(x9));

                }
            }
            catch (Exception e)
            {
                ldNewDtTm = ldDtTm;
            }
            return ldNewDtTm;
        }

        public Dictionary<string, object> CalculateFlightCost(double ChargeRate, string ChargeUnit, PreflightTripViewModel Trip, string AircraftID,FlightPakMasterService.IMasterCatalogService objDstsvc,string LegNum)
        { 
            Dictionary<string,object> RetObj=new Dictionary<string, object>();
            List<PreflightLegViewModel> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
            decimal TotalCost = 0.0M;
            foreach (PreflightLegViewModel Leg in Preflegs)
            {
                decimal lnChrg_Rate = 0.0M;
                decimal lnCost = 0.0M;
                string lcChrg_Unit = string.Empty;
                if (Trip.PreflightMain.FleetID != null && Trip.PreflightMain.FleetID != 0)
                {
                    var objRetval = objDstsvc.GetFleetChargeRateList((long)Trip.PreflightMain.FleetID);
                    if (Leg.DepartureDTTMLocal != null)
                        if (objRetval.ReturnFlag)
                        {
                            List<FlightPakMasterService.FleetChargeRate> FleetchargeRate =(from Fleetchrrate in objRetval.EntityList
                                                                                            where (Fleetchrrate.BeginRateDT <= Leg.ArrivalDTTMLocal && Fleetchrrate.EndRateDT >= Leg.DepartureDTTMLocal)
                                                                                           where Fleetchrrate.FleetID == Trip.PreflightMain.FleetID
                                                                                            select Fleetchrrate).ToList();
                            if (FleetchargeRate.Count > 0)
                            {
                                lnChrg_Rate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal) FleetchargeRate[0].ChargeRate;
                                lcChrg_Unit = FleetchargeRate[0].ChargeUnit;
                            }
                        }
                }
                if (lnChrg_Rate == 0.0M)
                    if (!string.IsNullOrEmpty(AircraftID) && AircraftID != "0")
                    {
                        lnChrg_Rate = (decimal) ChargeRate;
                        lcChrg_Unit = ChargeUnit;
                    }
                decimal dobCost=0;
                switch (lcChrg_Unit)
                {
                    case "N":
                        lnCost = (Leg.Distance == null ? 0 : (decimal) Leg.Distance)*lnChrg_Rate;
                        break;
                    case "K":
                        dobCost = (Leg.Distance == null ? 0 : (decimal) Leg.Distance)*1.852M;
                        lnCost = Math.Floor(dobCost);
                        lnCost = lnCost*lnChrg_Rate;
                        break;
                    case "S":
                        dobCost = (Leg.Distance == null ? 0 : (decimal) Leg.Distance)*1.1508M;
                        lnCost = Math.Floor(dobCost);
                        lnCost = lnCost*lnChrg_Rate;
                        break;
                    case "H":
                        lnCost = (Leg.ElapseTM == null ? 0 : (decimal) Leg.ElapseTM)*lnChrg_Rate;
                        break;
                    default:
                        lnCost = 0.0M;
                        break;
                }
                Leg.FlightCost = lnCost;
                TotalCost = TotalCost + lnCost;
                if (LegNum == (Preflegs.IndexOf(Leg) + 1).ToString())
                    if (Leg.FlightCost != null)
                        RetObj["FlightCost"]=Math.Round((decimal) Leg.FlightCost, 2).ToString();
                    else
                        RetObj["FlightCost"]=0.00;
            }
            Trip.PreflightMain.FlightCost = TotalCost;
            RetObj["Trip"]=Trip;
            return RetObj;
        }

        public Dictionary<string,object> CalculatePreflightFARRules(PreflightTripViewModel Trip, Int16 LegNum,FlightPakMasterService.IMasterCatalogService objectDstsvc)
        {
                Dictionary<string,object> RetObj=new Dictionary<string, object>();
                Dictionary<string,string> FARRules=new Dictionary<string, string>();
                double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty = 0, lnMaxFlt = 0, lnMinRest = 0, lnRestMulti = 0, lnRestPlus = 0, lnOldDuty_Hrs, lnDayBeg = 0, lnDayEnd = 0,lnNeededRest, lnWorkArea;
                bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                DateTime? ltGmtDep, ltGmtArr;
                bool llDutyEnd = false;
                string FARNum = "";
                Int64 lnDuty_RulesID = 0;
            if(Trip.PreflightLegs != null)
            {
                List<PreflightLegViewModel> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                lnEndLegNum = (double) Preflegs[Preflegs.Count - 1].LegNUM;
                lnDuty_Hrs = 0;
                lnRest_Hrs = 0;
                lnFlt_Hrs = 0;
                if(Preflegs[0].DepartureGreenwichDTTM != null)
                {
                    ltGmtArr = (DateTime) Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime) Preflegs[0].DepartureGreenwichDTTM;
                    llDutyBegin = true;
                    double lnOldDuty_hrs = -1;
                    int legcounter = 0;
                    foreach(PreflightLegViewModel Leg in Preflegs)
                    {
                        double lnOverRide = 0;
                        double lnDutyLeg_Num = 0;
                        if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                        {
                            FARNum = "";
                            if(Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID != null)
                                lnDuty_RulesID = (Int64) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID;
                            else
                                lnDuty_RulesID = 0;
                            if(Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd == null)
                                llDutyEnd = false;
                            else
                                llDutyEnd = (bool) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd;
                            if(lnEndLegNum == Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM)
                                llDutyEnd = true;
                            lnOverRide = (double) (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].OverrideValue ?? 0);
                            lnDutyLeg_Num = (double) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM;
                            lnDayBeg = 0;
                            lnDayEnd = 0;
                            lnMaxDuty = 0;
                            lnMaxFlt = 0;
                            lnMinRest = 8;
                            lnRestMulti = 0;
                            lnRestPlus = 0;
                            if(lnDuty_RulesID > 0)
                            {
                                var objRetVal = objectDstsvc.GetCrewDutyRulesWithFilters(lnDuty_RulesID, string.Empty, false);
                                if(objRetVal.ReturnFlag)
                                {
                                    List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = objRetVal.EntityList;

                                    if(CrewDtyRule != null && CrewDtyRule.Count > 0)
                                    {
                                        FARNum = CrewDtyRule[0].FedAviatRegNum;
                                        lnDayBeg = Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM ?? 0);
                                        lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM ?? 0);
                                        lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs ?? 0);
                                        lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs ?? 0);
                                        lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs ?? 0);
                                        lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple ?? 0);
                                        lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs ?? 0);
                                    }
                                }
                            }
                        TimeSpan? Hoursdiff = TimeSpan.MinValue;
                        if(Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DepartureGreenwichDTTM != null && ltGmtArr.HasValue)
                            Hoursdiff = (DateTime) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DepartureGreenwichDTTM - ltGmtArr;
                        if(Leg.ElapseTM != 0 && Leg.ElapseTM != null)
                        {
                            lnFlt_Hrs = lnFlt_Hrs + (double) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ElapseTM;
                            lnDuty_Hrs = lnDuty_Hrs + (double) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ElapseTM + Hoursdiff.Value.TotalHours;
                        }
                        else
                            lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;
                        llRProblem = false;
                        if(llDutyBegin && lnOldDuty_hrs != -1)
                        {
                            lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                            if(lnRestMulti > 0)
                            {
                                //Here is the Multiple and Plus hours usage
                                lnNeededRest = ((lnOldDuty_hrs*lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs*lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                if(lnRest_Hrs < lnNeededRest && lnRest_Hrs > 0)
                                    llRProblem = true;
                            }
                            else
                            {
                                if(lnRest_Hrs < (lnMinRest + lnRestPlus) && lnRest_Hrs > 0)
                                    llRProblem = true;
                            }
                        }
                        else
                        {
                            if(llDutyBegin)
                                lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                        }

                        if(llDutyEnd)
                            lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;

                        if (llDutyEnd == true || lnDutyLeg_Num == 1)
                            lnDuty_Hrs = lnDuty_Hrs + lnOverRide;

                        if(lnRest_Hrs < 0)
                            llRProblem = true;
                        string lcCdAlert = "";
                        if(lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than tripleg.maxflt
                            lcCdAlert = "F"; // Flight time violation and F is stored in tripleg.cdalert as a Flight time error
                        else
                            lcCdAlert = "";
                        if(lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                            lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in tripleg.cdalert as a Duty time error
                        if(llRProblem)
                            lcCdAlert = lcCdAlert + "R"; // Rest time violation and R is stored in tripleg.cdalert as a Rest time error 
                        //if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegID != 0 && Trip.PreflightMain.EditMode)
                        //Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].State = TripEntityState.Modified;
                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].FlightHours = (decimal) lnFlt_Hrs;
                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DutyHours = (decimal) lnDuty_Hrs;
                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].RestHours = (decimal) lnRest_Hrs;
                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyAlert = lcCdAlert;

                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyAlert = lcCdAlert;
                        if(LegNum.ToString() == (Trip.PreflightLegs.IndexOf(Leg) + 1).ToString())
                        {
                            FARRules["Far"] = System.Web.HttpUtility.HtmlEncode(FARNum);
                            FARRules["lcCdAlert"] = lcCdAlert;
                            FARRules["StylelbTotalDuty"] = lcCdAlert.LastIndexOf('D') >= 0 ? "infored" : "infoash";
                            FARRules["StylelbTotalFlight"] = lcCdAlert.LastIndexOf('F') >= 0 ? "infored" : "infoash";
                            FARRules["StylelbRest"] = lcCdAlert.LastIndexOf('R') >= 0 ? "infored" : "infoash";
                            if(lnDuty_Hrs != 0)
                            {
                                FARRules["lbTotalDuty"] = System.Web.HttpUtility.HtmlEncode(Math.Round(lnDuty_Hrs, 1).ToString());
                                if(FARRules["lbTotalDuty"].ToString().IndexOf(".") < 0)
                                    FARRules["lbTotalDuty"] = System.Web.HttpUtility.HtmlEncode(FARRules["lbTotalDuty"] + ".0");
                            }
                            else
                                FARRules["lbTotalDuty"] = System.Web.HttpUtility.HtmlEncode("0.0");

                            if(lnFlt_Hrs != 0)
                            {
                                FARRules["lbTotalFlight"] = System.Web.HttpUtility.HtmlEncode(Math.Round(lnFlt_Hrs, 1).ToString());
                                if(FARRules["lbTotalFlight"].ToString().IndexOf(".") < 0)
                                    FARRules["lbTotalFlight"] = System.Web.HttpUtility.HtmlEncode((FARRules["lbTotalFlight"].ToString() + ".0"));
                            }
                            else
                                FARRules["lbTotalFlight"] = System.Web.HttpUtility.HtmlEncode("0.0");

                            if(lnRest_Hrs != 0)
                            {
                                FARRules["lbRest"] = Math.Round(lnRest_Hrs, 1).ToString();
                                if(FARRules["lbRest"].ToString().IndexOf(".") < 0)
                                    FARRules["lbRest"] = System.Web.HttpUtility.HtmlEncode(FARRules["lbRest"].ToString() + ".0");
                            }
                            else
                                FARRules["lbRest"] = System.Web.HttpUtility.HtmlEncode("0.0");
                        }
                        lnLeg_Num = (double) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM;
                        llDutyEnd = Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd != null && (bool) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd;
                        ltGmtArr = Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ArrivalGreenwichDTTM != null ? (DateTime?) Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ArrivalGreenwichDTTM : null;
                        if(llDutyEnd)
                        {
                            llDutyBegin = true;
                            lnFlt_Hrs = 0;
                        }
                        else
                            llDutyBegin = false;

                        lnOldDuty_hrs = lnDuty_Hrs;
                        legcounter++;
                        if(legcounter < Preflegs.Count)
                            if(llDutyEnd)
                            {
                                lnDuty_Hrs = 0;
                                if(Preflegs[legcounter].DepartureGreenwichDTTM != null && ltGmtArr != null)
                                {
                                    TimeSpan? hrsdiff = (DateTime) Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;
                                    lnRest_Hrs = ((hrsdiff.Value.Days*24) + hrsdiff.Value.Hours + (double) ((double) hrsdiff.Value.Minutes/60)) - lnDayBeg - lnDayEnd;
                                    ltGmtArr = (DateTime) Preflegs[legcounter].DepartureGreenwichDTTM;
                                }
                            }
                            else
                                lnRest_Hrs = 0;
                    }
                  }
                }
            }
            RetObj["Trip"]=Trip;
                RetObj["FARRules"]=FARRules;
          return  RetObj;
        }

        public Dictionary<string, string> CalculateDateTimeNew(
            bool isDepartureConfirmed,
            bool? depIsDayLightSaving,
            DateTime? depDayLightSavingStartDT,
            DateTime? depDayLightSavingEndDT,
            string depDaylLightSavingStartTM,
            string depDayLightSavingEndTM,
            decimal? depOffsetToGMT,
            bool? arrIsDayLightSaving,
            DateTime? arrDayLightSavingStartDT,
            DateTime? arrDayLightSavingEndDT,
            string arrDaylLightSavingStartTM,
            string arrDayLightSavingEndTM,
            decimal? arrOffsetToGMT,
            bool? homeIsDayLightSaving,
            DateTime? homeDayLightSavingStartDT,
            DateTime? homeDayLightSavingEndDT,
            string homeDaylLightSavingStartTM,
            string homeDayLightSavingEndTM,
            decimal? homeOffsetToGMT,
            string strLocalDate,
            string strDateFormat,
            string strArrivalDate,
            string strArrivalTime,
            string strhdDepart,
            string strhdArrival,
            string strETE,
            string strTimeDisplayTenMin,
            string strUtctime,
            string strUtcDate,
            string strtbArrival,
            string strtbDepart,
            string strtbArrivalUtcDate,
            string strrmtArrivalUtctime,
            string strhdHomebaseAirport,
            string strrmtbLocaltime,
            string strtbHomeDate,
            string strrmtHomeTime,
            string strtbArrivalHomeDate,
            string strrmtArrivalHomeTime,
            decimal ElapseTime,
            String changedValue,
            bool IsAutomaticCalcLegTimes
            )
        {
            double x10, x11;
            if (!string.IsNullOrEmpty(strETE))
            {
                string ETEstr = "0";
                if (strTimeDisplayTenMin != null && Convert.ToInt32(strTimeDisplayTenMin) == 2)
                {
                    ETEstr = Utilities.Preflight_ConvertMinstoTenths(strETE, Convert.ToDecimal(strTimeDisplayTenMin));
                }
                else
                    ETEstr = strETE;
                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr), Convert.ToDecimal(strTimeDisplayTenMin), ElapseTime);
                x10 = (tripleg_elp_time * 60 * 60);
                x11 = (tripleg_elp_time * 60 * 60);
            }
            else
            {
                x10 = 0;
                x11 = 0;
            }

            DateTime? DeptUtcDateTime = null, ArrivalUtcDateTime = null;
            DateTime tempLocalDateTime = DateTime.MinValue;
            DateTime ldLocDep = DateTime.MinValue;
            DateTime ldLocArr = DateTime.MinValue;
            DateTime ldHomDep = DateTime.MinValue;
            DateTime ldHomArr = DateTime.MinValue;

            bool result = false;
            TimeSpan ts;
                if (isDepartureConfirmed)
                {
                    switch (changedValue)
                    {
                        case "Local":
                            result = DateTime.TryParseExact(strLocalDate, strDateFormat, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out tempLocalDateTime);
                            ts = DateTime.ParseExact(strrmtbLocaltime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
                            tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                            if (result)
                            {
                                using (
                                    CalculationService.CalculationServiceClient ObjCalculation =
                                        new CalculationService.CalculationServiceClient())
                                {
                                    DeptUtcDateTime = ObjCalculation.GetGMT(Convert.ToInt64(strhdDepart),
                                        tempLocalDateTime, true, false);
                                }
                            }
                            break;
                        case "Home":
                            result = DateTime.TryParseExact(strtbHomeDate, strDateFormat, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out tempLocalDateTime);
                            ts = DateTime.ParseExact(strrmtHomeTime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
                            tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                            if (result)
                            {
                                using (
                                    CalculationService.CalculationServiceClient ObjCalculation =
                                        new CalculationService.CalculationServiceClient())
                                {
                                    DeptUtcDateTime = ObjCalculation.GetGMT(Convert.ToInt64(strhdHomebaseAirport),
                                        tempLocalDateTime, true, false);
                                }
                            }
                            break;
                        case "UTC":
                            result = DateTime.TryParseExact(strUtcDate, strDateFormat, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out tempLocalDateTime);
                            ts = DateTime.ParseExact(strUtctime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
                            tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                            DeptUtcDateTime = tempLocalDateTime;
                            break;
                        default:
                            if (!string.IsNullOrEmpty(strUtcDate))
                            {
                                strUtctime = string.IsNullOrEmpty(strUtctime) ? "00:00" : strUtctime;
                                result = DateTime.TryParseExact(strUtcDate, strDateFormat, CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out tempLocalDateTime);
                                ts = DateTime.ParseExact(strUtctime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
                                tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                                DeptUtcDateTime = tempLocalDateTime;
                            }

                            break;
                    }

                    //Add ETE to Departure UTC Date time and calculate Arrival UTC Time
                    if (!string.IsNullOrEmpty(strhdArrival) && DeptUtcDateTime != null)
                        ArrivalUtcDateTime = DeptUtcDateTime.Value.AddSeconds(x10);

                }
                else
                {
                    switch (changedValue)
                    {
                        case "Local":
                            result = DateTime.TryParseExact(strArrivalDate, strDateFormat, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out tempLocalDateTime);
                            ts = DateTime.ParseExact(strArrivalTime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
                            tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                            if (result)
                            {
                                using (
                                    CalculationService.CalculationServiceClient ObjCalculation =
                                        new CalculationService.CalculationServiceClient())
                                {
                                    ArrivalUtcDateTime = ObjCalculation.GetGMT(Convert.ToInt64(strhdArrival),
                                        tempLocalDateTime, true, false);
                                }
                            }
                            break;
                        case "Home":
                            result = DateTime.TryParseExact(strtbArrivalHomeDate, strDateFormat,
                                CultureInfo.InvariantCulture, DateTimeStyles.None, out tempLocalDateTime);
                            ts =
                                DateTime.ParseExact(strrmtArrivalHomeTime, "HH:mm", CultureInfo.InvariantCulture)
                                    .TimeOfDay;
                            tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                            if (result)
                            {
                                using (
                                    CalculationService.CalculationServiceClient ObjCalculation =
                                        new CalculationService.CalculationServiceClient())
                                {
                                    ArrivalUtcDateTime = ObjCalculation.GetGMT(Convert.ToInt64(strhdHomebaseAirport),
                                        tempLocalDateTime, true, false);
                                }
                            }
                            break;
                        case "UTC":
                            result = DateTime.TryParseExact(strtbArrivalUtcDate, strDateFormat,
                                CultureInfo.InvariantCulture, DateTimeStyles.None, out tempLocalDateTime);
                            ts =
                                DateTime.ParseExact(strrmtArrivalUtctime, "HH:mm", CultureInfo.InvariantCulture)
                                    .TimeOfDay;
                            tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                            ArrivalUtcDateTime = tempLocalDateTime;
                            break;
                        default:
                            if (!string.IsNullOrEmpty(strtbArrivalUtcDate))
                            {
                                strrmtArrivalUtctime = string.IsNullOrEmpty(strrmtArrivalUtctime)
                                    ? "00:00"
                                    : strrmtArrivalUtctime;
                                result = DateTime.TryParseExact(strtbArrivalUtcDate, strDateFormat,
                                    CultureInfo.InvariantCulture, DateTimeStyles.None, out tempLocalDateTime);
                                ts =
                                    DateTime.ParseExact(strrmtArrivalUtctime, "HH:mm", CultureInfo.InvariantCulture)
                                        .TimeOfDay;
                                tempLocalDateTime = tempLocalDateTime.Date.Add(ts);
                                ArrivalUtcDateTime = tempLocalDateTime;
                            }
                            break;
                    }

                    if (!string.IsNullOrEmpty(strhdDepart) && ArrivalUtcDateTime != null)
                        DeptUtcDateTime = ArrivalUtcDateTime.Value.AddSeconds(-x11);

                }

                if (!string.IsNullOrEmpty(strhdDepart) && DeptUtcDateTime != null && DeptUtcDateTime != null)
                {
                    ldLocDep = GetGMT(DeptUtcDateTime.Value, false, depIsDayLightSaving, depDayLightSavingStartDT,
                        depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM, depOffsetToGMT, false);
                }
                if (!string.IsNullOrEmpty(strhdHomebaseAirport) && !string.IsNullOrEmpty(strhdDepart) &&
                    DeptUtcDateTime != null)
                {
                    ldHomDep = GetGMT(DeptUtcDateTime.Value, false, homeIsDayLightSaving, homeDayLightSavingStartDT,
                        homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM, homeOffsetToGMT,
                        false);
                }

                if (!string.IsNullOrEmpty(strhdArrival) && ArrivalUtcDateTime != null)
                {
                    ldLocArr = GetGMT(ArrivalUtcDateTime.Value, false, arrIsDayLightSaving, arrDayLightSavingStartDT,
                        arrDayLightSavingEndDT, arrDaylLightSavingStartTM, arrDayLightSavingEndTM, arrOffsetToGMT, false);
                }

                if (!string.IsNullOrEmpty(strhdHomebaseAirport) && !string.IsNullOrEmpty(strhdArrival) &&
                    ArrivalUtcDateTime != null)
                {
                    ldHomArr = GetGMT(ArrivalUtcDateTime.Value, false, homeIsDayLightSaving, homeDayLightSavingStartDT,
                        homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM, homeOffsetToGMT,
                        false);
                }

                if (!isDateFormatValid(strDateFormat))
                {
                    strDateFormat = "MM/dd/yyyy";
                }

            string strArrivalUtcDate = ArrivalUtcDateTime != null && ArrivalUtcDateTime.Value != DateTime.MinValue ? String.Format(CultureInfo.InvariantCulture, "{0:" + strDateFormat + "}", ArrivalUtcDateTime) : "";
            string strArrivalHomeDate = ldHomArr != DateTime.MinValue ? String.Format(CultureInfo.InvariantCulture, "{0:" + strDateFormat + "}", ldHomArr) : "";
            string strArrivalLocalDate = ldLocArr != DateTime.MinValue ? String.Format(CultureInfo.InvariantCulture, "{0:" + strDateFormat + "}", ldLocArr) : "";

            string strDeptUtcDate = DeptUtcDateTime != null && DeptUtcDateTime.Value != DateTime.MinValue ? String.Format(CultureInfo.InvariantCulture, "{0:" + strDateFormat + "}", DeptUtcDateTime) : "";
            string strDeptHomeDate = ldHomDep != DateTime.MinValue ? String.Format(CultureInfo.InvariantCulture, "{0:" + strDateFormat + "}", ldHomDep) : "";
            string strDeptLocalDate = ldLocDep != DateTime.MinValue ? String.Format(CultureInfo.InvariantCulture, "{0:" + strDateFormat + "}", ldLocDep) : "";

            Dictionary<string, string> RetObj = new Dictionary<string, string>();
            RetObj["ArrivalUtcDate"] = strArrivalUtcDate == "" ? strDeptUtcDate : strArrivalUtcDate;
            RetObj["ArrivalUtcTime"] = ArrivalUtcDateTime != null && ArrivalUtcDateTime.Value != DateTime.MinValue ? ArrivalUtcDateTime.Value.ToString("HH:mm") : "00:00";
            RetObj["ArrivalHomeDate"] = strArrivalHomeDate == "" ? strDeptHomeDate : strArrivalHomeDate;
            RetObj["ArrivalHomeTime"] = ldHomArr != DateTime.MinValue ? ldHomArr.ToString("HH:mm") : "00:00";
            RetObj["ArrivalLocalDate"] = strArrivalLocalDate == "" ? strDeptLocalDate : strArrivalLocalDate;
            RetObj["ArrivalLocalTime"] = ldLocArr != DateTime.MinValue ? ldLocArr.ToString("HH:mm") : "00:00";

            RetObj["DeptUtcDate"] = strDeptUtcDate == "" ? strArrivalUtcDate : strDeptUtcDate;
            RetObj["DeptUtcTime"] = DeptUtcDateTime != null && DeptUtcDateTime.Value != DateTime.MinValue ? DeptUtcDateTime.Value.ToString("HH:mm") : "00:00";
            RetObj["DeptHomeDate"] = strDeptHomeDate == "" ? strArrivalHomeDate : strDeptHomeDate;
            RetObj["DeptHomeTime"] = ldHomDep != DateTime.MinValue ? ldHomDep.ToString("HH:mm") : "00:00";
            RetObj["DeptLocalDate"] = strDeptLocalDate == "" ? strArrivalLocalDate : strDeptLocalDate;
            RetObj["DeptLocalTime"] = ldLocDep != DateTime.MinValue ? ldLocDep.ToString("HH:mm") : "00:00";

            return RetObj;
        }

        public Dictionary<string,string> CalculateDateTime(
            bool isDepartureConfirmed,
            bool? depIsDayLightSaving,
            DateTime? depDayLightSavingStartDT,
            DateTime? depDayLightSavingEndDT,
            string depDaylLightSavingStartTM,
            string depDayLightSavingEndTM,
            decimal? depOffsetToGMT,
            bool? arrIsDayLightSaving,
            DateTime? arrDayLightSavingStartDT,
            DateTime? arrDayLightSavingEndDT,
            string arrDaylLightSavingStartTM,
            string arrDayLightSavingEndTM,
            decimal? arrOffsetToGMT,
            bool? homeIsDayLightSaving,
            DateTime? homeDayLightSavingStartDT,
            DateTime? homeDayLightSavingEndDT,
            string homeDaylLightSavingStartTM,
            string homeDayLightSavingEndTM,
            decimal? homeOffsetToGMT,
            string strLocalDate, //LocalDepartureDate
            string strDateFormat,
            string strArrivalDate,//LocalArrivalDate
            string strArrivalTime,//LocalArrivalTime
            string strhdDepart,
            string strhdArrival,
            string strETE,
            string strTimeDisplayTenMin,
            string strUtctime, //DepartureUTCTime
            string strUtcDate,//DepartureUTCDate
            string strtbArrival,
            string strtbDepart,
            string strtbArrivalUtcDate, // ArrivalUTCDate 
            string strrmtArrivalUtctime, // ArrivalUTCTime
            string strhdHomebaseAirport,
            string strrmtbLocaltime, //LocalDepartureTime
            string strtbHomeDate, //DepartureHomeDate
            string strrmtHomeTime, //DepartureHomeTime
            string strtbArrivalHomeDate,  // ArrivalHomeDate
            string strrmtArrivalHomeTime, // ArrivalHomeTime
            decimal ElapseTime,
            String changedValue,
            bool IsAutomaticCalcLegTimes
            )
        {
            
            Dictionary<string, string> RetObj;
            
            if (!IsAutomaticCalcLegTimes)
            {
                changedValue = changedValue == "" ? "Local" : changedValue;
                if (isDepartureConfirmed)
                {
                    if (changedValue.Contains("Time"))
                    {
                       return CalculateDateTimeNew(isDepartureConfirmed, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM,
                       depOffsetToGMT, arrIsDayLightSaving, arrDayLightSavingStartDT, arrDayLightSavingEndDT, arrDaylLightSavingStartTM, arrDayLightSavingEndTM,
                       arrOffsetToGMT, homeIsDayLightSaving, homeDayLightSavingStartDT, homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM,
                       homeOffsetToGMT, strLocalDate, strDateFormat, strArrivalDate, strArrivalTime, strhdDepart, strhdArrival, strETE, strTimeDisplayTenMin, strUtctime, strUtcDate, strtbArrival,
                       strtbDepart, strtbArrivalUtcDate, strrmtArrivalUtctime, strhdHomebaseAirport, strrmtbLocaltime, strtbHomeDate, strrmtHomeTime, strtbArrivalHomeDate,
                       strrmtArrivalHomeTime, ElapseTime, changedValue.Replace("Time",""), IsAutomaticCalcLegTimes
                       );
                    }
                    else if ((changedValue == "Local" || changedValue == "Home" || changedValue == "UTC") & (strrmtbLocaltime == "00:00" && strUtctime == "00:00" && strrmtHomeTime == "00:00" &&
                                                                                                        strArrivalTime =="00:00" && strrmtArrivalUtctime =="00:00" && strrmtArrivalHomeTime =="00:00"  ))
                    {
                        if (changedValue == "Local")
                        {
                            RetObj = new Dictionary<string, string>();
                            RetObj["ArrivalUtcDate"] = strLocalDate;
                            RetObj["ArrivalUtcTime"] =  "00:00";
                            RetObj["ArrivalHomeDate"] = strLocalDate;
                            RetObj["ArrivalHomeTime"] = "00:00";
                            RetObj["ArrivalLocalDate"] = strLocalDate;
                            RetObj["ArrivalLocalTime"] = "00:00";

                            RetObj["DeptUtcDate"] = strLocalDate;
                            RetObj["DeptUtcTime"] = "00:00";
                            RetObj["DeptHomeDate"] = strLocalDate;
                            RetObj["DeptHomeTime"] = "00:00";
                            RetObj["DeptLocalDate"] = strLocalDate;
                            RetObj["DeptLocalTime"] = "00:00";
                            return RetObj;
                        }
                        else if (changedValue == "Home")
                        {
                            RetObj = new Dictionary<string, string>();
                            RetObj["ArrivalUtcDate"] = strtbHomeDate;
                            RetObj["ArrivalUtcTime"] ="00:00";
                            RetObj["ArrivalHomeDate"] = strtbHomeDate;
                            RetObj["ArrivalHomeTime"] = "00:00";
                            RetObj["ArrivalLocalDate"] = strtbHomeDate;
                            RetObj["ArrivalLocalTime"] = "00:00";

                            RetObj["DeptUtcDate"] = strtbHomeDate;
                            RetObj["DeptUtcTime"] = "00:00";
                            RetObj["DeptHomeDate"] = strtbHomeDate;
                            RetObj["DeptHomeTime"] = "00:00";
                            RetObj["DeptLocalDate"] = strtbHomeDate;
                            RetObj["DeptLocalTime"] = "00:00";
                            return RetObj;
                        }
                        else if (changedValue == "UTC")
                        {
                            RetObj = new Dictionary<string, string>();
                            RetObj["ArrivalUtcDate"] = strUtcDate;
                            RetObj["ArrivalUtcTime"] = "00:00";
                            RetObj["ArrivalHomeDate"] = strUtcDate;
                            RetObj["ArrivalHomeTime"] = "00:00";
                            RetObj["ArrivalLocalDate"] = strUtcDate;
                            RetObj["ArrivalLocalTime"] = "00:00";

                            RetObj["DeptUtcDate"] = strUtcDate;
                            RetObj["DeptUtcTime"] = "00:00";
                            RetObj["DeptHomeDate"] = strUtcDate;
                            RetObj["DeptHomeTime"] = "00:00";
                            RetObj["DeptLocalDate"] = strUtcDate;
                            RetObj["DeptLocalTime"] = "00:00";
                            return RetObj;
                        }
                       // return new Dictionary<string, string>();
                    }

                    return CalculateDateTimeNew(isDepartureConfirmed, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM,
                       depOffsetToGMT, arrIsDayLightSaving, arrDayLightSavingStartDT, arrDayLightSavingEndDT, arrDaylLightSavingStartTM, arrDayLightSavingEndTM,
                       arrOffsetToGMT, homeIsDayLightSaving, homeDayLightSavingStartDT, homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM,
                       homeOffsetToGMT, strLocalDate, strDateFormat, strArrivalDate, strArrivalTime, strhdDepart, strhdArrival, strETE, strTimeDisplayTenMin, strUtctime, strUtcDate, strtbArrival,
                       strtbDepart, strtbArrivalUtcDate, strrmtArrivalUtctime, strhdHomebaseAirport, strrmtbLocaltime, strtbHomeDate, strrmtHomeTime, strtbArrivalHomeDate,
                       strrmtArrivalHomeTime, ElapseTime, changedValue.Replace("Time", ""), IsAutomaticCalcLegTimes
                       );
                }
                else
                {
                    if (changedValue.Contains("Time"))
                    {
                        return CalculateDateTimeNew(isDepartureConfirmed, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM,
                        depOffsetToGMT, arrIsDayLightSaving, arrDayLightSavingStartDT, arrDayLightSavingEndDT, arrDaylLightSavingStartTM, arrDayLightSavingEndTM,
                        arrOffsetToGMT, homeIsDayLightSaving, homeDayLightSavingStartDT, homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM,
                        homeOffsetToGMT, strLocalDate, strDateFormat, strArrivalDate, strArrivalTime, strhdDepart, strhdArrival, strETE, strTimeDisplayTenMin, strUtctime, strUtcDate, strtbArrival,
                        strtbDepart, strtbArrivalUtcDate, strrmtArrivalUtctime, strhdHomebaseAirport, strrmtbLocaltime, strtbHomeDate, strrmtHomeTime, strtbArrivalHomeDate,
                        strrmtArrivalHomeTime, ElapseTime, changedValue.Replace("Time", ""), IsAutomaticCalcLegTimes
                        );
                    }
                    else if ((changedValue == "Local" || changedValue == "Home" || changedValue == "UTC") & (strrmtbLocaltime == "00:00" && strUtctime == "00:00" && strrmtHomeTime == "00:00" &&
                                                                                                        strArrivalTime == "00:00" && strrmtArrivalUtctime == "00:00" && strrmtArrivalHomeTime == "00:00"))
                    {
                        if (changedValue == "Local")
                        {
                            RetObj = new Dictionary<string, string>();
                            RetObj["ArrivalUtcDate"] = strArrivalDate;
                            RetObj["ArrivalUtcTime"] = "00:00";
                            RetObj["ArrivalHomeDate"] = strArrivalDate;
                            RetObj["ArrivalHomeTime"] = "00:00";
                            RetObj["ArrivalLocalDate"] = strArrivalDate;
                            RetObj["ArrivalLocalTime"] = "00:00";

                            RetObj["DeptUtcDate"] = strArrivalDate;
                            RetObj["DeptUtcTime"] = "00:00";
                            RetObj["DeptHomeDate"] = strArrivalDate;
                            RetObj["DeptHomeTime"] = "00:00";
                            RetObj["DeptLocalDate"] = strArrivalDate;
                            RetObj["DeptLocalTime"] = "00:00";
                            return RetObj;
                        }
                        else if (changedValue == "Home")
                        {
                            RetObj = new Dictionary<string, string>();
                            RetObj["ArrivalUtcDate"] = strtbArrivalHomeDate;
                            RetObj["ArrivalUtcTime"] = "00:00";
                            RetObj["ArrivalHomeDate"] = strtbArrivalHomeDate;
                            RetObj["ArrivalHomeTime"] = "00:00";
                            RetObj["ArrivalLocalDate"] = strtbArrivalHomeDate;
                            RetObj["ArrivalLocalTime"] = "00:00";

                            RetObj["DeptUtcDate"] = strtbArrivalHomeDate;
                            RetObj["DeptUtcTime"] = "00:00";
                            RetObj["DeptHomeDate"] = strtbArrivalHomeDate;
                            RetObj["DeptHomeTime"] = "00:00";
                            RetObj["DeptLocalDate"] = strtbArrivalHomeDate;
                            RetObj["DeptLocalTime"] = "00:00";
                            return RetObj;
                        }
                        else if (changedValue == "UTC")
                        {
                            RetObj = new Dictionary<string, string>();
                            RetObj["ArrivalUtcDate"] = strtbArrivalUtcDate;
                            RetObj["ArrivalUtcTime"] = "00:00";
                            RetObj["ArrivalHomeDate"] = strtbArrivalUtcDate;
                            RetObj["ArrivalHomeTime"] = "00:00";
                            RetObj["ArrivalLocalDate"] = strtbArrivalUtcDate;
                            RetObj["ArrivalLocalTime"] = "00:00";

                            RetObj["DeptUtcDate"] = strtbArrivalUtcDate;
                            RetObj["DeptUtcTime"] = "00:00";
                            RetObj["DeptHomeDate"] = strtbArrivalUtcDate;
                            RetObj["DeptHomeTime"] = "00:00";
                            RetObj["DeptLocalDate"] = strtbArrivalUtcDate;
                            RetObj["DeptLocalTime"] = "00:00";
                            return RetObj;
                        }
                        // return new Dictionary<string, string>();
                    }

                    return CalculateDateTimeNew(isDepartureConfirmed, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM,
                        depOffsetToGMT, arrIsDayLightSaving, arrDayLightSavingStartDT, arrDayLightSavingEndDT, arrDaylLightSavingStartTM, arrDayLightSavingEndTM,
                        arrOffsetToGMT, homeIsDayLightSaving, homeDayLightSavingStartDT, homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM,
                        homeOffsetToGMT, strLocalDate, strDateFormat, strArrivalDate, strArrivalTime, strhdDepart, strhdArrival, strETE, strTimeDisplayTenMin, strUtctime, strUtcDate, strtbArrival,
                        strtbDepart, strtbArrivalUtcDate, strrmtArrivalUtctime, strhdHomebaseAirport, strrmtbLocaltime, strtbHomeDate, strrmtHomeTime, strtbArrivalHomeDate,
                        strrmtArrivalHomeTime, ElapseTime, changedValue.Replace("Time", ""), IsAutomaticCalcLegTimes
                        );
                }
            }
            else
            {
               return CalculateDateTimeNew(isDepartureConfirmed,depIsDayLightSaving,depDayLightSavingStartDT,depDayLightSavingEndDT,depDaylLightSavingStartTM,depDayLightSavingEndTM,
                    depOffsetToGMT,arrIsDayLightSaving,arrDayLightSavingStartDT,arrDayLightSavingEndDT,arrDaylLightSavingStartTM,arrDayLightSavingEndTM,
                    arrOffsetToGMT,homeIsDayLightSaving, homeDayLightSavingStartDT,homeDayLightSavingEndDT,homeDaylLightSavingStartTM,homeDayLightSavingEndTM,
                    homeOffsetToGMT,strLocalDate,strDateFormat,strArrivalDate,strArrivalTime,strhdDepart, strhdArrival, strETE, strTimeDisplayTenMin, strUtctime, strUtcDate, strtbArrival,
                    strtbDepart, strtbArrivalUtcDate, strrmtArrivalUtctime,strhdHomebaseAirport, strrmtbLocaltime, strtbHomeDate, strrmtHomeTime, strtbArrivalHomeDate,
                    strrmtArrivalHomeTime, ElapseTime, changedValue.Replace("Time", ""), IsAutomaticCalcLegTimes
                    );
            }
        }



        public string DefaultEteResult(string eteStr)
        {
            if (!string.IsNullOrEmpty(eteStr))
            {
                if (eteStr == "00:00" || eteStr == "0:0" || eteStr == "0:00" || eteStr == "00:0")
                    eteStr = "00:05"; // Set default to 00.05 if the ETE is calculated 0 as per new rule
                else if (eteStr == "00.00" || eteStr == "0.0" || eteStr == "0.00" || eteStr == "00.0" || eteStr == "0")
                    eteStr = "0.1"; // Set default to 1 if the ETE is calculated 0 as per new rule    
            }

            return eteStr;
        }        

        private string timeHelper(string spilit)
        {
            string[] splitedTime = spilit.Split(':');
            string Hrs = "0000" + splitedTime[0].ToString();
            string Mins = "0000" + splitedTime[1].ToString();
            return string.Format("{0}:{1}",Hrs.Substring(Hrs.Length-2,2),Mins.Substring(Mins.Length-2,2));
        }

        public static void ValidatePreflightSession(string selectedTab="")
        {
            PreflightTripViewModel Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip == null)
            {
                HttpContext.Current.Response.Redirect("PreFlightMain.aspx");
            }
            var firstLeg = Trip.PreflightLegs.Where(l=>l.IsDeleted==false && l.DepartICAOID.HasValue && l.DepartICAOID > 0 && l.ArriveICAOID.HasValue && l.ArriveICAOID > 0).OrderBy(l=>l.LegNUM).FirstOrDefault();
            if (selectedTab != "Leg" && (Trip.PreflightLegs == null || Trip.PreflightLegs.Count == 0 || (Trip.PreflightLegs.Count == 1 && firstLeg==null)))
            {
                HttpContext.Current.Response.Redirect("PreflightLegs.aspx?seltab=Legs");
            }


        }
        bool isDateFormatValid(string dateFormate)
        {
            return true;
        }

        public static PreflightFBOListViewModel SetIsChoiceFBOToLogistics(PreflightFBOListViewModel fbo, long? airportId)
        {
            if (airportId != null && airportId > 0)
            {
                using (MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient())
                {
                    var ObjRetValGroup = ObjService.GetAllFBOByAirportID((long)airportId).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();
                    if (ObjRetValGroup == null || ObjRetValGroup.Count == 0)
                    {
                        ObjRetValGroup.Add(new GetAllFBOByAirportID());
                    }
                    fbo.AirportID = airportId;
                    fbo.FBOCD = ObjRetValGroup[0].FBOCD;
                    fbo.FBOID = ObjRetValGroup[0].FBOID == 0 ? (long?)null : ObjRetValGroup[0].FBOID;
                    fbo.Addr1 = ObjRetValGroup[0].Addr1;
                    fbo.Addr2 = ObjRetValGroup[0].Addr2;
                    fbo.Addr3 = ObjRetValGroup[0].Addr3;
                    fbo.EmailAddress = ObjRetValGroup[0].EmailAddress;
                    fbo.UNICOM = ObjRetValGroup[0].UNICOM;
                    fbo.ARINC = ObjRetValGroup[0].ARINC;
                    fbo.PreflightFBOName = ObjRetValGroup[0].FBOVendor;
                    fbo.CityName = ObjRetValGroup[0].CityName;
                    fbo.StateName = ObjRetValGroup[0].StateName;
                    fbo.PostalZipCD = ObjRetValGroup[0].PostalZipCD;
                    fbo.PhoneNum1 = ObjRetValGroup[0].PhoneNUM1;
                    fbo.PhoneNum2 = ObjRetValGroup[0].PhoneNUM2;
                    fbo.FaxNUM = ObjRetValGroup[0].FaxNum;
                    fbo.FBOInformation = ObjRetValGroup[0].FBOVendor + "," + ObjRetValGroup[0].CityName + "," +
                                         ObjRetValGroup[0].StateName + "," + ObjRetValGroup[0].PostalZipCD + "," +
                                         ObjRetValGroup[0].PhoneNUM1 + "," + ObjRetValGroup[0].PhoneNUM2 + "," +
                                         ObjRetValGroup[0].FaxNum + "," + ObjRetValGroup[0].UNICOM + "," +
                                         ObjRetValGroup[0].ARINC;
                }
            }
            return fbo;
        }

        public string ConvertMilesToKilometer(bool? isKilometer, decimal distance)
        {
            if (isKilometer != null && (bool)isKilometer)
                return Convert.ToString(Math.Floor(distance * 1.852M));
            return Convert.ToString(distance);
        }

        public decimal ConvertKilometerToMiles(bool? isKilometer, decimal distance)
        {
            if (isKilometer != null && (bool)isKilometer)
                return Math.Floor(distance / 1.852M);
            return distance;
        }
    }
}