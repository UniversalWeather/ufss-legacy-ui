﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightAirportInfo.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightAirportInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 520px;" class="box1">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                ICAO
                            </td>
                            <td class="tdLabel200">
                                <asp:TextBox ID="tbICAO" runat="server" CssClass="text150"></asp:TextBox>
                                <asp:HiddenField ID="hdnAirportID" runat="server" />
                            </td>
                            <td class="tdLabel100">
                                Takeoff Bias
                            </td>
                            <td>
                                <asp:TextBox ID="tbTakeoffBias" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                City
                            </td>
                            <td class="tdLabel200">
                                <asp:TextBox ID="tbCity" runat="server" CssClass="text150"></asp:TextBox>
                            </td>
                            <td class="tdLabel100">
                                Landing Bias
                            </td>
                            <td>
                                <asp:TextBox ID="tbLandingBias" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                State/Province
                            </td>
                            <td class="tdLabel200">
                                <asp:TextBox ID="tbStateProvince" runat="server" CssClass="text150"></asp:TextBox>
                            </td>
                            <td class="tdLabel100">
                                Country
                            </td>
                            <td>
                                <asp:TextBox ID="tbCountry" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                Country Description
                            </td>
                            <td class="tdLabel200">
                                <asp:TextBox ID="tbCountryDescription" runat="server" CssClass="text150"></asp:TextBox>
                            </td>
                            <td class="tdLabel100">
                                DST Region
                            </td>
                            <td>
                                <asp:TextBox ID="tbDSTRegion" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                Airport
                            </td>
                            <td class="tdLabel200">
                                <asp:TextBox ID="tbAirport" runat="server" CssClass="text150"></asp:TextBox>
                            </td>
                            <td class="tdLabel100">
                                UTC+/-
                            </td>
                            <td>
                                <asp:TextBox ID="tbUTC" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                Runway Length
                            </td>
                            <td class="tdLabel200">
                                <asp:TextBox ID="tbRunwayLength" runat="server" CssClass="text150"></asp:TextBox>
                            </td>
                             <td class="tdLabel100">
                                Runway Width
                            </td>
                            <td>
                                <asp:TextBox ID="tbRunwayWidth" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                Notes
                            </td>
                            <td>
                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea370x50"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="tdLabel120">
                                Alerts
                            </td>
                            <td>
                                <asp:TextBox ID="tbAlerts" runat="server" TextMode="MultiLine" CssClass="textarea370x50"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
