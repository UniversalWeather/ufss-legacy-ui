﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightSIFLPaxInfoPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightSIFLPaxInfoPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="FlightPak.Common.Constants" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Preflight SIFL Passenger/Requestor</title>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgPassengerCatalog.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPassengerCatalog.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "PassengerName");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "EmpType");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorID");
                    var cell5 = MasterTable.getCellByColumnUniqueName(row, "IsPersonalTravel");
                    var cell6 = MasterTable.getCellByColumnUniqueName(row, "LegID");
                    var cell7 = MasterTable.getCellByColumnUniqueName(row, "AssociatedPassengerID");
                    var cell8 = MasterTable.getCellByColumnUniqueName(row, "AssociatedPassengerCD");
                }
                if (selectedRows.length > 0) {
                    oArg.PassengerRequestorCD = cell1.innerHTML;
                    oArg.PassengerName = cell2.innerHTML;
                    oArg.IsEmployeeType = cell3.innerHTML;
                    oArg.PassengerRequestorID = cell4.innerHTML;
                    oArg.IsPersonalTravel = cell5.innerHTML;
                    oArg.LegID = cell6.innerHTML;
                    oArg.AssociatedPassengerID = cell7.innerHTML;
                    oArg.AssociatedPassengerCD = cell8.innerHTML;
                }
                else {
                    oArg.PassengerRequestorCD = "";
                    oArg.PassengerName = "";
                    oArg.IsEmployeeType = "";
                    oArg.PassengerRequestorID = "";
                    oArg.IsPersonalTravel = "";
                    oArg.LegID = "";
                    oArg.AssociatedPassengerID = "";
                    oArg.AssociatedPassengerCD = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgPassengerCatalog.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function GetGridId() {
                return $find("<%= dgPassengerCatalog.ClientID %>");
            }    
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgPassengerCatalog">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPassengerCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPassengerCatalog" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgPassengerCatalog" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" AutoGenerateColumns="false" OnItemCommand="dgPassengerCatalog_ItemCommand"
            OnDeleteCommand="dgPassengerCatalog_DeleteCommand" Height="330px" PageSize="10"
            AllowPaging="true" Width="500px">
            <MasterTableView DataKeyNames="PassengerRequestorID,PassengerRequestorCD,PassengerName,EmpType,IsPersonalTravel"
                CommandItemDisplay="Bottom">
                <Columns>
                    
                    <telerik:GridBoundColumn DataField="EmpType" HeaderText="EmpType" UniqueName="EmpType"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LegID" HeaderText="LegID" UniqueName="LegID"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                        Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LegIndex" HeaderText="Leg #" UniqueName="LegIndex"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="Code" UniqueName="PassengerRequestorCD"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerName" UniqueName="PassengerName" HeaderText="Passenger/Requestor"
                        AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IsPersonalTravel" HeaderText="IsPersonalTravel"
                        UniqueName="IsPersonalTravel" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AssociatedPassengerID" HeaderText="AssociatedPassengerID"
                        UniqueName="AssociatedPassengerID" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AssociatedPassengerCD" HeaderText="AssociatedPassengerCD"
                        UniqueName="AssociatedPassengerCD" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" Display="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerRequestorID" UniqueName="PassengerRequestorID"
                        HeaderText="PassengerRequestorID" AutoPostBackOnFilter="true" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grid_icon">
                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                            CommandName="PerformInsert" Visible='<%# IsAuthorized(Permission.Database.AddPassengerRequestor)%>'></asp:LinkButton>
                        <%-- --%>
                        <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                            CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditPassengerRequestor)%>'
                            OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                        <%-- --%>
                        <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgPassengerCatalog', 'radPaxInfoPopup');"
                            CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# IsAuthorized(Permission.Database.DeletePassengerRequestor)%>'></asp:LinkButton>
                        <%-- CommandName="DeleteSelected" --%>
                    </div>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
        </telerik:RadGrid>
        <asp:HiddenField ID="hdName" runat="server" />
    </div>
    </form>
</body>
</html>
