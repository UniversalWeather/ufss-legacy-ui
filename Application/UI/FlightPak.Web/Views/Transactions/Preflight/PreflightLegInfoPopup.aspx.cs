﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightLegInfoPopup : BaseSecuredPage
    {
        private string LegID;
        public PreflightMain Trip = new PreflightMain();
        string strDepArr = string.Empty;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["LegID"] != null)
                        {
                            LegID = Request.QueryString["LegID"];
                            strDepArr = Request.QueryString["Depart"];
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }
        }


        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgLegInfo_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                    List<PreflightLeg> Dt = new List<PreflightLeg>();
                    if (Trip.PreflightLegs != null)
                    {
                        Dt = Trip.PreflightLegs;

                        if (Dt.Count > 0)
                        {
                            if (strDepArr == "Depart")
                            {
                                dgLegInfo.DataSource = (from u in Dt
                                                        where u.Airport != null && u.Airport1 != null
                                                        select
                                                         new
                                                         {
                                                             LegID = u.LegID,
                                                             LegNum = u.LegNUM,
                                                             DepArr = u.Airport1.IcaoID == null ? string.Empty : u.Airport1.IcaoID + "-" + u.Airport1.CityName,
                                                             AirportID = u.Airport1.AirportID
                                                         });
                            }
                            else if (strDepArr == "Arrive")
                            {
                                dgLegInfo.DataSource = (from u in Dt
                                                        where u.Airport != null && u.Airport1 != null
                                                        select
                                                         new
                                                         {
                                                             LegID = u.LegID,
                                                             LegNum = u.LegNUM,
                                                             DepArr = u.Airport.IcaoID == null ? string.Empty : u.Airport.IcaoID + "-" + u.Airport.CityName,
                                                             AirportID = u.Airport.AirportID
                                                         });
                            }


                        }
                    }
                }
            }
        }


        protected void dgLegInfo_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridHeaderItem)
                {
                    GridHeaderItem header = (GridHeaderItem)e.Item;
                    if (strDepArr == "Arrive")
                        header["DepArr"].Text = "Arr ICAO - City";
                    if (strDepArr == "Depart")
                        header["DepArr"].Text = "Dep ICAO - City";
                }
            }
        }


        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                e.Updated = dgLegInfo;
                SelectItem();
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (LegID != null || LegID != string.Empty)
                {
                    foreach (GridDataItem item in dgLegInfo.MasterTableView.Items)
                    {
                        if (item["LegID"].Text.Trim() == LegID)
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
        }

    }
}