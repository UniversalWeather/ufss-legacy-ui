﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Helpers;
using System.Collections;
using System.Web.UI.WebControls;
using FlightPak.Web.GridHelpers;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PaxInfoPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private long CQcustomerid = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (Request.QueryString["ShowFillPopUp"] != null)
                            {
                                string show = "";
                                show = Request.QueryString["ShowFillPopUp"].ToString().Trim();

                                if (show == "TRUE")
                                {
                                    pnlFlightPurpose.Visible = true;
                                }
                                else
                                {
                                    pnlFlightPurpose.Visible = false;
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["CQcustomerID"]))
                                {
                                    CQcustomerid = Convert.ToInt64(Request.QueryString["CQcustomerID"]);
                                    if (CQcustomerid != 0)
                                    {
                                        chkCustomerID.Visible = true;
                                    }
                                    else
                                    {
                                        chkCustomerID.Visible = false;
                                    }
                                }
                                else
                                {
                                    chkCustomerID.Visible = false;
                                }
                            }
                            else
                            {
                                chkCustomerID.Visible = false;
                                pnlFlightPurpose.Visible = false;
                            }

                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                        if (!Page.IsPostBack)
                        {
                            flightpurposechange();
                            Session["selectedpaxItems"] = null;                          
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void chkFillAllLegs_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                flightpurposechange();
            }
        }

        private void flightpurposechange()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (chkFillAllLegs.Checked == true)
                {
                    tbDefaultBlockedSeat.Enabled = true;
                    btnDefaultBlockedSeat.Enabled = true;
                }
                else
                {
                    tbDefaultBlockedSeat.Enabled = false;
                    btnDefaultBlockedSeat.Enabled = false;
                }
            }
        }

        protected void dgPassengerCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }


        public void DoSearchfilter(bool bind)
        {
            long ClientId = 0;
            long PassengerId = 0;
            string PassengerCD = string.Empty;
            bool ActiveOnly = false;
            bool RequestorOnly = false;
            string ICAOID = string.Empty;
            long PaxGroupId = 0;
            long CQCustomerID = 0;

            using (FlightPakMasterService.MasterCatalogServiceClient Service1 = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (!string.IsNullOrEmpty(tbPaxGroup.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjPaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.PassengerGroup> PaxGroupLists = new List<FlightPak.Web.FlightPakMasterService.PassengerGroup>();
                        var ObjRetValPax = ObjPaxService.GetPaxGroupList();
                        PaxGroupLists = ObjRetValPax.EntityList.Where(x => x.PassengerGroupCD.ToLower().Trim().Equals(tbPaxGroup.Text.ToLower().Trim())).ToList();
                        if (PaxGroupLists.Count > 0)
                        {
                            hdnPaxGroupID.Value = PaxGroupLists[0].PassengerGroupID.ToString();
                        }
                        else
                        {
                            lbcvPaxGroup.Text = System.Web.HttpUtility.HtmlEncode("Invalid Paxgroupcode");
                        }
                    }
                }
                else 
                {
                    lbcvPaxGroup.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    hdnPaxGroupID.Value = string.Empty;
                }
                List<spGetAllPassenger_TelerikPagedGrid_Result> FilterPax = new List<spGetAllPassenger_TelerikPagedGrid_Result>();
                List<FlightPak.Web.FlightPakMasterService.spGetAllPassenger_TelerikPagedGrid_Result> CrewGroupLists = new List<FlightPak.Web.FlightPakMasterService.spGetAllPassenger_TelerikPagedGrid_Result>();

                if (!string.IsNullOrEmpty(hdnPaxGroupID.Value))
                {
                    PaxGroupId = Convert.ToInt64(hdnPaxGroupID.Value);
                }
                if (chkActiveOnly.Checked)
                {
                    ActiveOnly = true;
                }
                if (chkRequestOnly.Checked)
                {
                    RequestorOnly = true;
                }

                PassengerCD = dgPassengerCatalog.MasterTableView.GetColumn("PassengerRequestorCD").CurrentFilterValue;
                string PassengerName = dgPassengerCatalog.MasterTableView.GetColumn("PassengerName").CurrentFilterValue;
                string DepartmentCD = dgPassengerCatalog.MasterTableView.GetColumn("DepartmentCD").CurrentFilterValue;
                string DepartmentName = dgPassengerCatalog.MasterTableView.GetColumn("DepartmentName").CurrentFilterValue;
                string PhoneNum = dgPassengerCatalog.MasterTableView.GetColumn("PhoneNum").CurrentFilterValue;
                string HomeBaseCD = dgPassengerCatalog.MasterTableView.GetColumn("HomeBaseCD").CurrentFilterValue;

                TelerikPaging telerikPaging = TelerikGridHelper.GetPaging(dgPassengerCatalog.CurrentPageIndex, dgPassengerCatalog.PageSize);
                string strSortOrder = TelerikGridHelper.GetSortExpressionString(dgPassengerCatalog.MasterTableView.SortExpressions);
                if (strSortOrder == string.Empty) strSortOrder = "PassengerRequestorCD ASC";

                if (Session["NewPassengerRequestorID"] != null)
                    if (Session["NewPassengerRequestorID"].ToString() != "0")
                    {
                        dgPassengerCatalog.MasterTableView.FilterExpression = string.Empty;
                        PassengerId = (long) Session["NewPassengerRequestorID"];
                        PassengerCD = string.Empty;
                        PassengerName  = string.Empty;
                        DepartmentCD = string.Empty;
                        DepartmentName = string.Empty;
                        PhoneNum = string.Empty;
                        telerikPaging.Start = 1;
                        telerikPaging.End = 10;
                        RequestorOnly = false;
                        PaxGroupId = 0;
                    }

                var ObjRetVal = Service1.GetAllPassengerTelerikPagedGrid(ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId, telerikPaging.Start, telerikPaging.End, strSortOrder, PassengerName, DepartmentCD, DepartmentName, PhoneNum);
                
                if (ObjRetVal.ReturnFlag == true)
                {
                    FilterPax = ObjRetVal.EntityList.ToList();
                    if (FilterPax != null && FilterPax.Count > 0)
                    {
                        if (Session["NewPassengerRequestorID"] != null)
                            if (Session["NewPassengerRequestorID"].ToString() != "0")
                                AddNewPassengerToSelectedList(FilterPax);

                        var FilterPaxFirst = ObjRetVal.EntityList.FirstOrDefault();
                        if (FilterPaxFirst != null)
                        {
                            dgPassengerCatalog.AllowCustomPaging = true;
                            dgPassengerCatalog.VirtualItemCount = Convert.ToInt32(FilterPaxFirst.TotalCount);
                        }
                    }
                }
                dgPassengerCatalog.DataSource = FilterPax;

                if (bind)
                {
                    dgPassengerCatalog.DataBind();
                }

                 if (Session["NewPassengerRequestorID"] != null)
                     if (Session["NewPassengerRequestorID"].ToString() != "0")
                     {
                         Session.Remove("NewPassengerRequestorID");
                         InsertSelectedRow();
                     }
            }
        }

        private void GetPassenger(bool CallBind)
        {
            DoSearchfilter(CallBind);
        }

        protected void chkCustomerID_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void ActiveOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }


        protected void HomeBaseOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void dgPassengerCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string resolvedurl = string.Empty;
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;

                                GridDataItem item = dgPassengerCatalog.SelectedItems[0] as GridDataItem;
                                Session["PassengerRequestorID"] = item["PassengerRequestorID"].Text;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", out resolvedurl);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPassengerCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            string PassengerRequestorID = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient();
                            Passenger oPassenger = new Passenger();
                            GridDataItem item = dgPassengerCatalog.SelectedItems[0] as GridDataItem;
                            PassengerRequestorID = item.GetDataKeyValue("PassengerRequestorID").ToString();
                            oPassenger.PassengerRequestorID = Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString());
                            oPassenger.PassengerRequestorCD = item.GetDataKeyValue("PassengerRequestorCD").ToString();
                            if (item.GetDataKeyValue("IsActive") != null)
                            {
                                if (Convert.ToBoolean(item.GetDataKeyValue("IsActive")) == true)
                                {
                                    string alertMsg = "Active passenger cannot be deleted";
                                    ShowLockAlertPopup(alertMsg, ModuleNameConstants.Database.Passenger);
                                    return;
                                }
                            }
                            oPassenger.IsDeleted = true;
                            var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString()));
                            if (!returnValue.ReturnFlag)
                            {
                                e.Item.Selected = true;
                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PassengerRequestor);
                                return;
                            }
                            PassengerService.DeletePassengerRequestor(oPassenger);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(PassengerRequestorID));

                    }
                }
            }
        }
        protected void dgPassengerCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                                

                    DataTable dtSelectedPax = new DataTable();
                    DataRow oItem;
                    dtSelectedPax.Columns.Add("PassengerRequestorID");
                    dtSelectedPax.Columns.Add("PassengerRequestorCD");
                    dtSelectedPax.Columns.Add("PassengerName");
                    dtSelectedPax.Columns.Add("DepartmentID");
                    dtSelectedPax.Columns.Add("PassengerDescription");
                    dtSelectedPax.Columns.Add("PhoneNum");
                    dtSelectedPax.Columns.Add("HomebaseID");
                    dtSelectedPax.Columns.Add("IsActive");
                    dtSelectedPax.Columns.Add("IsRequestor");
                    dtSelectedPax.Columns.Add("FlightPurposeID");
                    dtSelectedPax.Columns.Add("FlightPurposeCD");
                    List<GetAllPassengerWithFilters> selectedItems = (List<GetAllPassengerWithFilters>)Session["selectedpaxItems"];

                    if (selectedItems != null)
                    {
                        if(selectedItems.Count > 0)
                        {
                            foreach (GetAllPassengerWithFilters paxinfo in selectedItems)
                            {
                                oItem = dtSelectedPax.NewRow();
                                oItem[0] = Convert.ToString(paxinfo.PassengerRequestorID);
                                oItem[1] = Convert.ToString(paxinfo.PassengerRequestorCD);
                                oItem[2] = Convert.ToString(paxinfo.PassengerName);
                                if (paxinfo.DepartmentID != null || paxinfo.DepartmentID != 0)
                                {
                                    oItem[3] = paxinfo.DepartmentID;
                                }
                                else
                                {
                                    oItem[3] = 0;
                                }
                                if (paxinfo.PassengerDescription != null)
                                {
                                    oItem[4] = paxinfo.PassengerDescription.ToString();
                                }
                                else
                                    oItem[4] = string.Empty;
                                if (paxinfo.PhoneNum != null)
                                {
                                    oItem[5] = paxinfo.PhoneNum.ToString();
                                }
                                else
                                    oItem[5] = string.Empty;
                                if (paxinfo.HomebaseID != null)
                                {
                                    oItem[6] = paxinfo.HomebaseID.ToString();
                                }
                                else
                                    oItem[6] = 0;
                                if (paxinfo.IsActive != null)
                                {
                                    oItem[7] = paxinfo.IsActive; // need to check what value is 
                                }
                                else
                                    oItem[7] = false;
                                if (paxinfo.IsRequestor != null) // need to check what value is 
                                {
                                    oItem[8] = paxinfo.IsRequestor;
                                }
                                else
                                    oItem[8] = false;

                                if (tbDefaultBlockedSeat.Visible == true && chkFillAllLegs.Checked == true)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var RetValue = Service.GetAllFlightPurposesWithFilters(0, 0, tbDefaultBlockedSeat.Text, false, false, false).EntityList;
                                        if (RetValue.Count > 0)
                                        {
                                            oItem[9] = RetValue[0].FlightPurposeID.ToString().Trim();
                                            oItem[10] = tbDefaultBlockedSeat.Text;
                                        }
                                        else
                                        {
                                            oItem[9] = string.Empty;
                                            oItem[10] = string.Empty;
                                        }

                                    }

                                }
                                else
                                {
                                    oItem[9] = string.Empty;
                                    oItem[10] = string.Empty;
                                }
                          

                            dtSelectedPax.Rows.Add(oItem);
                        }

                        if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "CQpax")
                        {
                            Session["CQSelectedPax"] = dtSelectedPax;
                        }
                        else
                        {
                            Session["SelectedPax"] = dtSelectedPax;
                        }
                        RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");

                        }
                        else
                        {
                            ShowLockAlertPopup("Please select a Passenger", ModuleNameConstants.Database.Passenger);
                        }
                    }
                    else
                    {
                        ShowLockAlertPopup("Please select a Passenger", ModuleNameConstants.Database.Passenger);
                    }
                }            
        }

        private bool CheckIfExists(string PaxCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaxCode))
            {
                bool ReturnValue = false;
                if (Session["PAXInfo"] != null)
                {
                    List<Passenger> PaxList = (List<Passenger>)Session["PAXInfo"];
                    var PassengerAdditionalInfoValue = (from pax in PaxList
                                                        where pax.PassengerRequestorCD.ToUpper().Trim() == PaxCode.ToUpper().Trim()
                                                        select pax);

                    if (PassengerAdditionalInfoValue.Count() > 0)
                    { ReturnValue = true; }
                }
                return ReturnValue;
            }
        }

        protected void DefaultBlockedSeat_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultBlockedSeat.Text))
                        {
                            CheckAlreadyDefaultBlockedSeatExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        private bool CheckAlreadyDefaultBlockedSeatExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultBlockedSeat.Text != string.Empty) && (tbDefaultBlockedSeat.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllFlightPurposesWithFilters(0, 0, tbDefaultBlockedSeat.Text, false, false, false).EntityList;
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultBlockedSeat.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultBlockedSeat.ClientID);
                            return true;
                        }
                        else
                        {
                            hdnDefaultBlockedSeat.Value = RetValue[0].FlightPurposeID.ToString().Trim();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgPassengerCatalog;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        protected void Filter_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void dgPassengerCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPassengerCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void dgPassengerCatalog_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPassengerCatalog, Page.Session);
            if (Session["selectedpaxItems"] != null)
            {
                List<GetAllPassengerWithFilters> selectedItems = (List<GetAllPassengerWithFilters>)Session["selectedpaxItems"];                 
                int count = 0;
                foreach(GetAllPassengerWithFilters pax in selectedItems)
                {                
                    foreach (GridItem item in dgPassengerCatalog.MasterTableView.Items)
                    {
                        if (item is GridDataItem)
                        {
                            GridDataItem dataItem = (GridDataItem)item;
                            if (pax.PassengerRequestorID  == Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]))
                            {
                                count++;
                                CheckBox cb = (CheckBox)dataItem["CheckBoxTemplateColumn"].Controls[0].FindControl("CheckBox1");
                                cb.Checked = true;
                                dataItem.Selected = true;
                                break;
                            }
                        }
                    }
                }

                GridHeaderItem headerItem = dgPassengerCatalog.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
                if (headerItem != null)
                    (headerItem.FindControl("headerChkbox") as CheckBox).Checked = (count == dgPassengerCatalog.PageSize? true : false);
                
            }
        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            bool checkHeader = true;
            foreach (GridDataItem dataItem in dgPassengerCatalog.MasterTableView.Items)
            {
                if (!(dataItem.FindControl("CheckBox1") as CheckBox).Checked)
                {
                    checkHeader = false;
                    break;
                }
            }
            GridHeaderItem headerItem = dgPassengerCatalog.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            (headerItem.FindControl("headerChkbox") as CheckBox).Checked = checkHeader;

            chk_CheckedChanged(sender, e);
        }
        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            List<GetAllPassengerWithFilters> selectedItems;
            if (Session["selectedpaxItems"] == null)
            {
                selectedItems = new List<GetAllPassengerWithFilters>();                
            }
            else
            {
                selectedItems = (List<GetAllPassengerWithFilters>)Session["selectedpaxItems"];              
            }


            foreach (GridDataItem dataItem in dgPassengerCatalog.MasterTableView.Items)
            {
                (dataItem.FindControl("CheckBox1") as CheckBox).Checked = headerCheckBox.Checked;
                 dataItem.Selected = headerCheckBox.Checked;

                if (headerCheckBox.Checked)
                {
                    if (dataItem is GridDataItem)
                    {
                        GridDataItem dataItemValue = (GridDataItem)dataItem;
                        GetAllPassengerWithFilters paxinfo = new GetAllPassengerWithFilters();
                        paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]); ;
                        paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorCD"]);
                        paxinfo.PassengerName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerName"]);
                        paxinfo.DepartmentID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartmentID"]);
                        paxinfo.PassengerDescription = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerDescription"]);
                        paxinfo.PhoneNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PhoneNum"]);
                        paxinfo.HomebaseID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["HomebaseID"]);
                        paxinfo.IsActive = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsActive"]);
                        paxinfo.IsRequestor = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsRequestor"]);
                        paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeID"]);
                        paxinfo.FlightPurposeCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeCD"]);
                        var exist = selectedItems.Where(X => X.PassengerRequestorID == paxinfo.PassengerRequestorID);
                        if (exist.Count() == 0)
                        {
                            selectedItems.Add(paxinfo);
                            Session["selectedpaxItems"] = selectedItems;
                        }

                    }
                }
                else
                {
                    if (dataItem is GridDataItem)
                    {
                        GridDataItem dataItemValue = (GridDataItem)dataItem;
                        GetAllPassengerWithFilters paxinfo = new GetAllPassengerWithFilters();
                        paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"].ToString()); ;
                        paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorCD"]);
                        paxinfo.PassengerName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerName"]);
                        paxinfo.DepartmentID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartmentID"]);
                        paxinfo.PassengerDescription = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerDescription"]);
                        paxinfo.PhoneNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PhoneNum"]);
                        paxinfo.HomebaseID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["HomebaseID"]);
                        paxinfo.IsActive = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsActive"]);
                        paxinfo.IsRequestor = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsRequestor"]);
                        paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeID"]);
                        paxinfo.FlightPurposeCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeCD"]);
                        int count = 0;
                        foreach (GetAllPassengerWithFilters paxinfovalue in selectedItems.ToList())
                        {
                            if (paxinfovalue.PassengerRequestorID == paxinfo.PassengerRequestorID)
                            {
                                selectedItems.RemoveAt(count);
                                Session["selectedpaxItems"] = selectedItems;
                            }

                            count++;
                        }      
                    }

                }
            }


        }

        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
            
            List<GetAllPassengerWithFilters> selectedItems;
            GridDataItem item = (GridDataItem)(sender as CheckBox).NamingContainer; //item is the row where the checkbox (that invoked the event) resides 
            CheckBox chkbx = (CheckBox)sender;

            if (Session["selectedpaxItems"] == null)
            {
                selectedItems = new List<GetAllPassengerWithFilters>();                
            }
            else
            {
                selectedItems = (List<GetAllPassengerWithFilters>)Session["selectedpaxItems"];              
            }

            if (chkbx.Checked)
            {
                GridDataItem dataItem = (GridDataItem)item;
                GetAllPassengerWithFilters paxinfo = new GetAllPassengerWithFilters();
                paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]); ;
                paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorCD"]);
                paxinfo.PassengerName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerName"]);
                paxinfo.DepartmentID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartmentID"]);
                paxinfo.PassengerDescription = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerDescription"]);
                paxinfo.PhoneNum =  Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PhoneNum"]);
                paxinfo.HomebaseID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["HomebaseID"]);
                paxinfo.IsActive = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsActive"]);
                paxinfo.IsRequestor = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsRequestor"]);
                paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeID"]);
                paxinfo.FlightPurposeCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeCD"]);
                selectedItems.Add(paxinfo);
                Session["selectedpaxItems"] = selectedItems;
            }
            else
            {
                GridDataItem dataItem = (GridDataItem)item;
                GetAllPassengerWithFilters paxinfo = new GetAllPassengerWithFilters();
                paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]); ;
                paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorCD"]);
                paxinfo.PassengerName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerName"]);
                paxinfo.DepartmentID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartmentID"]);
                paxinfo.PassengerDescription = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerDescription"]);
                paxinfo.PhoneNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PhoneNum"]);
                paxinfo.HomebaseID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["HomebaseID"]);
                paxinfo.IsActive = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsActive"]);
                paxinfo.IsRequestor = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsRequestor"]);
                paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeID"]);
                paxinfo.FlightPurposeCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeCD"]);
                int count = 0;
                foreach (GetAllPassengerWithFilters paxinfovalue in selectedItems.ToList())
                {
                    if (paxinfovalue.PassengerRequestorID == paxinfo.PassengerRequestorID)
                    {
                        selectedItems.RemoveAt(count);
                        break;
                    }

                    count++;
                }       
                Session["selectedpaxItems"] = selectedItems;
            }

            // Update database 
        }

        protected void AddNewPassengerToSelectedList(List<spGetAllPassenger_TelerikPagedGrid_Result > paxList)
        {
            List<GetAllPassengerWithFilters> selectedItems;
            if (Session["selectedpaxItems"] == null)
            {
                selectedItems = new List<GetAllPassengerWithFilters>();
            }
            else
            {
                selectedItems = (List<GetAllPassengerWithFilters>)Session["selectedpaxItems"];
            }

            foreach (spGetAllPassenger_TelerikPagedGrid_Result dataItem in paxList.ToList())
            {
                GetAllPassengerWithFilters paxinfo = new GetAllPassengerWithFilters();
                paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.PassengerRequestorID);
                paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.PassengerRequestorCD);
                paxinfo.PassengerName = Convert.ToString(dataItem.PassengerName);
                paxinfo.DepartmentID = Convert.ToInt64(dataItem.DepartmentID);
                paxinfo.PassengerDescription = Convert.ToString(dataItem.PassengerDescription);
                paxinfo.PhoneNum = Convert.ToString(dataItem.PhoneNum);
                paxinfo.HomebaseID = Convert.ToInt64(dataItem.HomebaseID);
                paxinfo.IsActive = Convert.ToBoolean(dataItem.IsActive);
                paxinfo.IsRequestor = Convert.ToBoolean(dataItem.IsRequestor);
                //paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.FlightPurposeID);
                //paxinfo.FlightPurposeCD = Convert.ToString(dataItem.FlightPurposeCD);

                var exist = selectedItems.Where(X => X.PassengerRequestorID == paxinfo.PassengerRequestorID);
                if (exist.Count() == 0)
                {
                    selectedItems.Add(paxinfo);
                    Session["selectedpaxItems"] = selectedItems;
                }

            }
            
        }
    }
}
