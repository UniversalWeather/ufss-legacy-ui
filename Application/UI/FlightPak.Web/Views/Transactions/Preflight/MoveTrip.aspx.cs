﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Omu.ValueInjecter;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using System.Globalization;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Views.Transactions.Preflight;

namespace FlightPak.Web.Views.Settings.PreFlight
{
    public partial class MoveTrip : BaseSecuredPage
    {

        public object row { get; set; }
        private ExceptionManager exManager;
        public PreflightTripViewModel Trip = null;

        /// <summary>
        /// function to load the page and populating the dgviewsource grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Expires = -1;
                        if (!IsPostBack)
                        {
                            if (Session[WebSessionKeys.CurrentPreflightTrip] != null)
                            {
                               Trip = (PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip];
                                Session["ViewSourceTripID"] = Trip.PreflightMain.TripNUM.ToString();
                                List<PreflightLegViewModel> Dt = new List<PreflightLegViewModel>();
                                List<PreflightLegViewModel> Vt = new List<PreflightLegViewModel>();
                                if (Trip.PreflightLegs != null)
                                {
                                    Dt = Trip.PreflightLegs.OrderBy(x => x.LegNUM).ToList();

                                    Session["InitialLegList"] = Dt;

                                    bindViewSourceGrid(Dt);
                                    BindDestinationGrid(Vt);
                                }
                            }
                            DefaultSelection();
                            btnDestinationTrip.CssClass = "browse-button";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }


        private void DefaultSelection()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            Trip = (PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip];
                            Session["ViewSourceTripID"] = Trip.PreflightMain.TripNUM.ToString();
                            List<PreflightLegViewModel> Dt = new List<PreflightLegViewModel>();
                            List<PreflightLegViewModel> Vt = new List<PreflightLegViewModel>();
                            if (Trip.PreflightLegs != null)
                            {
                                Dt = Trip.PreflightLegs.OrderBy(x => x.LegNUM).ToList();

                                Session["InitialLegList"] = Dt;

                                bindViewSourceGrid(Dt);
                                BindDestinationGrid(Vt);
                                if (Trip.PreflightMain.TripNUM != null)
                                {
                                    lblTripno.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.TripNUM.ToString());
                                }
                                if (Trip.PreflightMain.Fleet != null)
                                {
                                    if (Trip.PreflightMain.Fleet.TailNum != null)
                                    {
                                        lblTailno.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Fleet.TailNum.ToString());
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        /// <summary>
        /// function to load data for lower grid on textchange function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DestinationTrip_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDestinationTrip);
                        if (!chkNewTrip.Checked)
                        {
                            if (!string.IsNullOrEmpty(tbDestinationTrip.Text))
                            {
                                Int64 MoveTripNum = Convert.ToInt64(tbDestinationTrip.Text);
                                Trip = (PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip];
                                string Tripnum = Trip.PreflightMain.TripNUM.ToString();

                                if (MoveTripNum != Convert.ToInt64(Tripnum))
                                {
                                    if (Int64.TryParse(tbDestinationTrip.Text, out MoveTripNum))
                                    {
                                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                                        {
                                            var objRetVal = PrefSvc.GetTripByTripNum(MoveTripNum);
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PreflightMain TripTemp = new PreflightMain();
                                                TripTemp = objRetVal.EntityList[0];
                                                PreflightTripViewModel TripDestination = new PreflightTripViewModel();
                                                TripDestination.PreflightMain = new PreflightMainViewModel();
                                                TripDestination.PreflightMain = (PreflightMainViewModel)TripDestination.PreflightMain.InjectFrom(TripTemp);
                                                TripDestination.PreflightLegs = PreflightTripManager.setPreflightLeg(TripTemp.PreflightLegs);

                                                Session["OldTripID"] = TripDestination.PreflightMain.TripID.ToString();
                                                if (TripDestination.PreflightLegs != null && TripDestination.PreflightLegs.Count > 0)
                                                {
                                                    Session["SelectedList"] = TripDestination.PreflightLegs.OrderBy(x => x.LegNUM).ToList();
                                                    BindDestinationGrid((List<PreflightLegViewModel>)Session["SelectedList"]);
                                                }
                                            }
                                            else
                                            {
                                                Session["OldTripID"] = string.Empty;
                                                RadWindowManager1.RadAlert("Invalid Trip Number", 360, 50, "Move Trips", "alertInvalidTripFn", null);
                                                List<PreflightLegViewModel> Vt = new List<PreflightLegViewModel>();
                                                BindDestinationGrid(Vt);
                                                Session.Remove("SelectedList");
                                                tbDestinationTrip.Text = string.Empty;
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDestinationTrip);
                                            }
                                        }
                                    }
                                    RadAjaxManager.GetCurrent(Page).FocusControl(btnDestinationTrip);
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("The source trip and destination trip cannot be the same." + " Please select any other trip.", 360, 50, "Move Trips", "alertNoTripFn", null);
                                    RadAjaxManager.GetCurrent(Page).FocusControl(btnDestinationTrip);
                                    tbDestinationTrip.Text = string.Empty;
                                    List<PreflightLegViewModel> Vt = new List<PreflightLegViewModel>();
                                    BindDestinationGrid(Vt);
                                    Session.Remove("SelectedList");

                                }
                            }
                            else
                            {
                                List<PreflightLegViewModel> Vt = new List<PreflightLegViewModel>();
                                BindDestinationGrid(Vt);
                                Session.Remove("SelectedList");
                                RadAjaxManager.GetCurrent(Page).FocusControl(btnDestinationTrip);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        /// <summary>
        /// Event of Insert button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnInsert_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgViewSource.SelectedItems.Count > 0)
                        {
                            hdnCallVal.Value = "Insert";
                            List<PreflightLeg> DestinationValues = new List<PreflightLeg>();
                            List<PreflightLeg> DestinationNewValues = new List<PreflightLeg>();
                            InsertSelectedRow();
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgDestination);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }

        }

        /// <summary>
        /// Event of Append button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAppend_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgViewSource.SelectedItems.Count > 0)
                        {
                            hdnCallVal.Value = "Append";
                            List<PreflightLeg> DestinationValues = new List<PreflightLeg>();
                            List<PreflightLeg> DestinationNewValues = new List<PreflightLeg>();
                            InsertSelectedRow();
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgDestination);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }


        /// <summary>
        /// function to move data from Upper Grid to Lower Grid
        /// </summary>
        private void InsertSelectedRow()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkNewTrip.Checked == true || (!string.IsNullOrEmpty(tbDestinationTrip.Text.ToString().Trim())))
                        {
                            if (dgViewSource.SelectedItems.Count > 0)
                            {
                                if (dgViewSource.SelectedItems.Count > 0)
                                {
                                    List<PreflightLegViewModel> PreflightLegList = (List<PreflightLegViewModel>)Session["SelectedList"];
                                    List<PreflightLegViewModel> UpperGridLegList = (List<PreflightLegViewModel>)Session["InitialLegList"];
                                    int rowselected = 0;
                                    if (dgDestination.SelectedItems.Count > 0)
                                    {
                                        GridDataItem DestinationGridtem = (GridDataItem)dgDestination.SelectedItems[0];
                                        rowselected = DestinationGridtem.ItemIndex + 1;
                                    }

                                    List<PreflightLegViewModel> LegTobinserted = new List<PreflightLegViewModel>();

                                    foreach (GridDataItem item in dgViewSource.SelectedItems)
                                    {
                                        long SelectedLegnum = 0;
                                        SelectedLegnum = Convert.ToInt32(item["LegNum"].Text);
                                        var firstMatch = UpperGridLegList.First(s => s.LegNUM == SelectedLegnum);

                                        LegTobinserted.Add(firstMatch);
                                        UpperGridLegList.Remove(firstMatch);

                                    }


                                    if (PreflightLegList == null || PreflightLegList.Count == 0)
                                        PreflightLegList = new List<PreflightLegViewModel>();

                                    if (hdnCallVal.Value == "Insert")
                                    {
                                        if (rowselected > 0)
                                            PreflightLegList.InsertRange(rowselected - 1, LegTobinserted);
                                        else
                                            PreflightLegList.AddRange(LegTobinserted);
                                    }
                                    else
                                    {
                                        PreflightLegList.AddRange(LegTobinserted);
                                    }

                                    SetUpperGridNum(ref UpperGridLegList);
                                    SetLegNum(ref PreflightLegList);
                                    Session["InitialLegList"] = UpperGridLegList;
                                    Session["SelectedList"] = PreflightLegList;
                                    bindViewSourceGrid(UpperGridLegList);
                                    dgViewSource.Rebind();
                                    BindDestinationGrid(PreflightLegList);
                                    dgDestination.Rebind();
                                }
                                chkNewTrip.Enabled = false;
                                tbDestinationTrip.Enabled = false;
                                btnDestinationTrip.Enabled = false;
                                btnDestinationTrip.CssClass = "browse-button-disabled";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }


        }

        /// <summary>
        /// Check Change function of chkNewTrip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkNewTrip_CheckedChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<PreflightLegViewModel> Vt = new List<PreflightLegViewModel>();
                        if (chkNewTrip.Checked == true)
                        {
                            tbDestinationTrip.Enabled = false;
                            BindDestinationGrid(Vt);
                            tbDestinationTrip.Text = "";
                            Session.Remove("SelectedList");
                            btnDestinationTrip.Enabled = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(bntInsert);
                            btnDestinationTrip.CssClass = "browse-button-disabled";//disabled
                        }
                        else
                        {
                            tbDestinationTrip.Enabled = true;
                            BindDestinationGrid(Vt);
                            btnDestinationTrip.Enabled = true;
                            btnDestinationTrip.CssClass = "browse-button";//enabled
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDestinationTrip);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }

        }


        protected void ViewSource_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                bindViewSourceGrid((List<PreflightLegViewModel>)Session["InitialLegList"]);
                dgViewSource.ClientSettings.Scrolling.ScrollTop = "0";
            }
        }


        protected void Destination_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                BindDestinationGrid((List<PreflightLegViewModel>)Session["SelectedList"]);
                dgDestination.ClientSettings.Scrolling.ScrollTop = "0";
            }
        }

        /// <summary>
        /// Bind the data fron list to destinationGrid
        /// </summary>
        /// <param name="Dt"></param>
        protected void BindDestinationGrid(List<PreflightLegViewModel> Dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dt))
            {
                var MoveData = (from u in Dt
                                where (u.ArrivalAirport != null && u.DepartureAirport != null && u.IsDeleted == false)
                                select
                                 new
                                 {
                                     LegNum = u.LegNUM,
                                     LegID = u.LegID,
                                     DepartAir = u.ArrivalAirport.IcaoID == null ? string.Empty : u.DepartureAirport.IcaoID,
                                     ArrivalAir = u.DepartureAirport.IcaoID == null ? string.Empty : u.ArrivalAirport.IcaoID,
                                     DepartDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                     ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal,
                                     FlightCost = u.FlightCost.ToString()
                                 }).OrderBy(x => x.LegNum);
                dgDestination.DataSource = MoveData;
                dgDestination.DataBind();
            }
        }


        /// <summary>
        /// Calling the Cancel function on Click of Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }

        }


        /// <summary>
        /// Setting up the LegNumber of the Destination Grid
        /// </summary>
        /// <param name="SelectedList"></param>
        protected void SetLegNum(ref List<PreflightLegViewModel> SelectedList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SelectedList))
            {
                int i = 1;
                foreach (PreflightLegViewModel Leg in SelectedList)
                {
                    Leg.LegNUM = i;
                    i++;
                }
                Session["SelectedList"] = SelectedList;
            }
        }

        protected void SampleSetLegNum(List<PreflightLegViewModel> SelectedList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SelectedList))
            {
                int i = 1;
                foreach (PreflightLegViewModel Leg in SelectedList)
                {
                    Leg.LegNUM = i;
                    i++;
                }
                Session["SelectedList"] = SelectedList;
            }
        }

        /// <summary>
        /// Setting up the LegNumber of the ViewSource Grid
        /// </summary>
        /// <param name="SelectedList"></param>
        protected void SetUpperGridNum(ref List<PreflightLegViewModel> SelectedList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SelectedList))
            {
                int i = 1;
                foreach (PreflightLegViewModel Leg in SelectedList)
                {
                    Leg.LegNUM = i;
                    i++;
                }
                Session["InitialLegList"] = SelectedList;
            }
        }


        /// <summary>
        /// Calling Save function on Save button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            Save();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }

        }

        /// <summary>
        /// Bind the data form list to grid
        /// </summary>
        /// <param name="Dt"></param>
        protected void bindViewSourceGrid(List<PreflightLegViewModel> Dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dt))
            {
                var ViewData = (from u in Dt
                                where (u.ArrivalAirport != null && u.DepartureAirport != null && u.IsDeleted == false)
                                select
                                 new
                                 { 
                                     LegNum = u.LegNUM,
                                     DepartAir = u.ArrivalAirport.IcaoID == null ? string.Empty : u.DepartureAirport.IcaoID,
                                     ArrivalAir = u.DepartureAirport.IcaoID == null ? string.Empty : u.ArrivalAirport.IcaoID,
                                     DepartDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                     ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal,
                                     FlightCost = u.FlightCost.ToString()
                                 });

                dgViewSource.DataSource = ViewData;
                dgViewSource.DataBind();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }


        /// <summary>
        /// Item Databound function to format the Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgViewSource_ItemDataBound(object sender, GridItemEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {

                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["DepartDate"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                                if (date != null)
                                    item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date)) ;
                            }
                            if (item["ArrivalDate"].Text != "&nbsp;")
                            {
                                DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                                if (date1 != null)
                                    item["ArrivalDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date1));
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }


        }


        /// <summary>
        /// Save the trip and their leg information in the database
        /// </summary>
        public void Save()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgDestination.Items.Count > 0)
                {
                    using (PreflightServiceClient Move = new PreflightServiceClient())
                    {
                        List<PreflightLeg> objPreflightLegsSelectedList = new List<PreflightLeg>();
                        List<PreflightLeg> objPreflightLegsInitialList = new List<PreflightLeg>();

                        List<PreflightLegViewModel> objPreflightLegViewModelSelectedList = (List<PreflightLegViewModel>)Session["SelectedList"];
                        List<PreflightLegViewModel> objPreflightLegViewModelInitialList = (List<PreflightLegViewModel>)Session["InitialLegList"];

                        objPreflightLegsSelectedList = PreflightTripManager.InjectPreflightLegVMToPreflightLeg(objPreflightLegViewModelSelectedList, true);
                        objPreflightLegsInitialList = PreflightTripManager.InjectPreflightLegVMToPreflightLeg(objPreflightLegViewModelInitialList, true);
                        
                        if (chkNewTrip.Checked)
                        {
                            Move.MoveTrip(objPreflightLegsSelectedList, objPreflightLegsInitialList, ((PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip]).PreflightMain.TripID, 0);
                        }
                        else
                        {
                            Move.MoveTrip(objPreflightLegsSelectedList, objPreflightLegsInitialList, ((PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip]).PreflightMain.TripID, Convert.ToInt64((Session["OldTripID"].ToString())));
                        }

                        Session.Remove("SelectedList");
                        Session.Remove("InitialLegList");
                        Session.Remove(WebSessionKeys.CurrentPreflightTrip);

                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetValue = PrefSvc.GetTripByTripNum(Convert.ToInt64(Session["ViewSourceTripID"].ToString()));
                            if (objRetValue.ReturnFlag == true)
                            {
                                PreflightMain TripTemp = new PreflightMain();
                                TripTemp = objRetValue.EntityList[0];
                                Trip=new PreflightTripViewModel();
                                Trip = PreflightTripManager.InjectPreflightMainToPreflightTripViewModel(TripTemp,Trip);
                                long TempTripId = Trip.PreflightMain == null ? 0 : Trip.PreflightMain.TripID;
                                if (TempTripId > 0)
                                {
                                    Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                                    var ObjRetvalExp = PrefSvc.GetTripExceptionList(TempTripId);
                                    if (ObjRetvalExp.ReturnFlag)
                                    {
                                        Session["PreflightException"] = ObjRetvalExp.EntityList;
                                    }
                                    else
                                        Session.Remove("PreflightException");
                                }
                            }
                            else
                            {
                                Session.Remove(WebSessionKeys.CurrentPreflightTrip);
                                Session.Remove("PreflightException");
                            }

                            Session.Remove("OldTripID");

                            RadAjaxManager.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                        }
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("No Trips has been Moved.Please Move a Trip", 360, 50, "Move Trips", "alertNoTripFn", null);
                    //string AlertMsg = "radalert('No Trips has been Moved.Please Move a Trip', 360, 50, 'Move Trips');";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
            }
        }

        /// <summary>
        /// Clearing all the Session on Click of Cancel button 
        /// </summary>
        public void Cancel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Session.Remove("SelectedList");
                Session.Remove("InitialLegList");
                RadAjaxManager.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
            }
        }
    }
}

