﻿using System;
using FlightPak.Web.Views.Transactions.Preflight;
using Newtonsoft.Json;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreflightPax : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PreflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                BusinessLite.Preflight.PreflightTripManagerLite.UpdatePreflightTripViewModelAsPerCalenderSelection();
                PreflightUtils.ValidatePreflightSession();
                dynamic preflightPaxInitializer = new { Hotel = new ViewModels.PreflightLegPassengerHotelViewModel(), Transport = new ViewModels.PreflightLegPassengerTransportViewModel() };
                hdnPreflightPaxInitializationObject.Value = JsonConvert.SerializeObject(preflightPaxInitializer);
            }
        }
    }
}