﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.CalculationService;
using FlightPak.Web.PreflightService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    [Serializable()]
    public partial class PreflightSIFL : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public FlightPak.Web.PreflightService.PreflightMain Trip = new FlightPak.Web.PreflightService.PreflightMain();

        public Dictionary<Int64, Int64> PrivatePaxIds { get; set; }
        public Dictionary<Int64, Int64> NonPrivatePaxIds { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            #region Authorization


                            CheckAutorization(Permission.Preflight.ViewTripSIFL);

                            if (!IsAuthorized(Permission.Preflight.AddTripSIFL))
                            {
                                btnUpdateSIFL.Visible = false;
                                btnSave.Visible = false;
                                btnReCalcSIFL.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.DeleteTripSIFL))
                            {
                                btnDelete.Visible = false;
                            }
                            if (!IsAuthorized(Permission.Preflight.EditTripSIFL))
                            {
                                btnUpdateSIFL.Visible = false;
                                btnEdit.Visible = false;
                            }
                            #endregion

                            //hdnLeg.Value = "1";
                            //hdnLegNUM.Value = "1";
                            tbMiles1.Attributes.Add("readonly", "readonly");
                            tbMiles2.Attributes.Add("readonly", "readonly");
                            tbMiles3.Attributes.Add("readonly", "readonly");
                            tbSIFLTotal.Attributes.Add("readonly", "readonly");

                            bool isPersonalTravelAvailableInTrip = false;

                            if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                            {
                                Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];

                                // Get Flight Purpose List from Master when session is null...
                                if (Session["FPurposeList"] == null)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var retVal = Service.GetFlightPurposeList();
                                        if (retVal.ReturnFlag == true && retVal.EntityList.Count > 0)
                                            Session["FPurposeList"] = retVal.EntityList;
                                    }
                                }

                                if (Session["FPurposeList"] != null)
                                {
                                    List<FlightPakMasterService.FlightPurpose> purposeList = (List<FlightPakMasterService.FlightPurpose>)Session["FPurposeList"];

                                    List<string> personalTravelPurposeIds = purposeList.Where(x => x.IsPersonalTravel.HasValue && x.IsPersonalTravel.Value).Select(x => x.FlightPurposeID.ToString()).ToList();
                                    Session["PersonalTravelPurposeIds"] = personalTravelPurposeIds;
                                    if (personalTravelPurposeIds.Count > 0)
                                    {
                                        isPersonalTravelAvailableInTrip = Trip.PreflightLegs.Any(x => x.PreflightPassengerLists.Any(y => personalTravelPurposeIds.Contains(y.FlightPurposeID.Value.ToString())));
                                    }
                                }


                            }
                            if (isPersonalTravelAvailableInTrip)
                            {
                                rtSIFL.Visible = true;

                                using (CalculationServiceClient CalcService = new CalculationServiceClient())
                                {
                                    // Get Fleet Profile Settings
                                    if (Trip.FleetID != null)
                                    {
                                        var ReturnValue = CalcService.GetFleetProfile(Convert.ToInt64(Trip.FleetID));
                                        {
                                            if (ReturnValue.ReturnFlag == true && ReturnValue.EntityList.Count > 0)
                                            {
                                                Session["SIFLFleetProfile"] = ReturnValue.EntityList;
                                            }
                                        }
                                    }

                                    //// Get SIFL Rate based on current date
                                    var siflRate = CalcService.GetSIFLRate(DateTime.Now).EntityList;
                                    Session["SIFLRateKey"] = siflRate;
                                    // Binds the Leginfo grid
                                    BindLegInfo();
                                    // Retrieves the vales fro PrefilghtTripSIFL to dispaly on screen
                                    LoadSifl();
                                }
                            }
                            else
                            {
                                //If there is no passenegr with personal trip
                                pnlMessge.Visible = true;
                                pnlContents.Visible = false;
                            }
                            //DefaultSelection(true);
                            if (dgSifl.MasterTableView.Items.Count > 0)
                            {
                                SelectItem();
                                btnEdit.Enabled = true;
                                btnDelete.Enabled = true;
                                btnEdit.CssClass = "button";
                                btnDelete.CssClass = "button";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method for Employee Selection Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmployeeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbDeparts.Focus();
                        tbAssocPax.Enabled = false;
                        btnAssocPax.Enabled = false;
                        btnAssocPax.CssClass = "browse-button-disabled";
                        if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest)
                        {
                            tbAssocPax.Enabled = true;
                            btnAssocPax.Enabled = true;
                            btnAssocPax.CssClass = "browse-button";

                            if (string.IsNullOrEmpty(tbAssocPax.Text))
                            {
                                ViewState["AlertType"] = "EmpType_Changed";
                                // System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Associated With Passenger Code is Required');", true);

                                Alert_Click(sender, e);
                            }
                        }
                        else
                        {
                            hdnAssocPax.Value = string.Empty;
                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(hdnAssocPax.Value);
                            tbAssocPax.Text = string.Empty;
                        }

                        //lbDepart.Text = System.Web.HttpUtility.HtmlEncode(lbDepart.Text);
                        //lbArrives.Text = System.Web.HttpUtility.HtmlEncode(lbDepart.Text);


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method to Update SIFL Fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateSIFL_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbPax.Text) && !string.IsNullOrEmpty(hdnPax.Value))
                        {
                            if (!string.IsNullOrEmpty(tbDeparts.Text) && !string.IsNullOrEmpty(hdnDeparts.Value))
                            {
                                if (!string.IsNullOrEmpty(tbArrives.Text) && !string.IsNullOrEmpty(hdnArrives.Value))
                                {
                                    bool InvalidPaxID = false;
                                    if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                    {
                                        if (string.IsNullOrEmpty(hdnAssocPax.Value) && string.IsNullOrEmpty(tbAssocPax.Text))
                                        {
                                            RadWindowManager1.RadAlert("Associated With Passenger Code is Required.", 320, 100, "System Messages", "AssocPaxAlert", null);
                                            InvalidPaxID = true;
                                        }
                                    }
                                    if (InvalidPaxID == false)
                                    {
                                        SetProfileSettings(tbPax.Text, hdnPax.Value);
                                        //// FOR NEW, NO NEED TO VALIDATE SINCE IT IS DONE DURING NEW CLICK.
                                        //// FOR EDIT, RECORD, DO VALIDATION BASED ON DEPART ICAOID
                                        //if (GetSiflRateForCurrentDate() > 0) { return; }
                                        StartCalculation(sender, e);
                                        btnSave.Enabled = true;
                                        btnCancel.Enabled = true;
                                        btnSave.CssClass = "button";
                                        btnCancel.CssClass = "button";

                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbStatueMiles);
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Arrival Airport is not seleted.", 300, 100, "System Messages", "alertCallArrAirFn", null);
                                }
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Depart Airport is not seleted.", 300, 100, "System Messages", "alertCallDepAirFn", null);
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Pax is not seleted.", 250, 100, "System Messages", "alertNotPaxFn", null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method to Re-Calculate SIFL Leg
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RecalcSIFLLeg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        #region "Recalculate SIFL"

                        // Declarations
                        decimal siflTotal = 0;
                        decimal distanceRate1 = 0;
                        decimal distanceRate2 = 0;
                        decimal distanceRate3 = 0;
                        decimal distancRateTotal = 0;
                        decimal multiplierTotal = 0;
                        decimal rate1 = 0;
                        decimal rate2 = 0;
                        decimal rate3 = 0;
                        decimal terminalCharge = 0;
                        decimal fareLevelID = 0;
                        decimal miles1 = 0;
                        decimal miles2 = 0;
                        decimal miles3 = 0;
                        decimal statueMiles = 0;
                        string empType = string.Empty;
                        string PaxCode = string.Empty;
                        decimal maxPax = 0;
                        //long? associatedPassengerID = null;
                        //string associatedPassengerCD = string.Empty;
                        // Enable SIFL Fields
                        EnableSIFLFields(false);
                        tbAssocPax.Enabled = false;
                        btnAssocPax.Enabled = false;
                        btnAssocPax.CssClass = "browse-button-disabled";
                        // Set SIFL Rate values from Fare Level
                        List<GetPOSIFLRate> siflRateList = null;

                        // Get Flight Purpose List from Master
                        if (Session["FPurposeList"] == null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var retVal = Service.GetFlightPurposeList();
                                if (retVal.ReturnFlag == true && retVal.EntityList.Count > 0)
                                    Session["FPurposeList"] = retVal.EntityList;
                            }
                        }

                        int privatePaxCount = 0;
                        int nonPrivatePaxCount = 0;
                        NonPrivatePaxIds = new Dictionary<long, long>();
                        if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                        {
                            Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                        }

                        for (int legIndex = 0; legIndex < Trip.PreflightLegs.Count; legIndex++)
                        {
                            if (Trip.PreflightLegs[legIndex].PreflightPassengerLists != null)
                            {
                                if (Trip.PreflightLegs[legIndex].PreflightPassengerLists.Count > 0)
                                {
                                    foreach (PreflightPassengerList Pax in Trip.PreflightLegs[legIndex].PreflightPassengerLists)
                                    {
                                        long PaxId;
                                        long.TryParse(Convert.ToString(Pax.PassengerID), out PaxId);
                                        List<FlightPak.Web.FlightPakMasterService.GetAllPassengerWithFilters> PassengerList = new List<FlightPakMasterService.GetAllPassengerWithFilters>();
                                        FlightPakMasterService.MasterCatalogServiceClient objDstsvc =new FlightPakMasterService.MasterCatalogServiceClient();
                                        var lstPaxList = objDstsvc.GetAllPassengerWithFilters(0, 0, PaxId, string.Empty,false, false, string.Empty, 0);
                                        DateTime? dtPaxDOB = null;
                                        if (lstPaxList.ReturnFlag)
                                        {
                                            PassengerList = lstPaxList.EntityList;
                                            if (PassengerList != null && PassengerList.Count > 0)
                                                dtPaxDOB = PassengerList[0].DateOfBirth;
                                        }
                                        if (dtPaxDOB != null)
                                            dtPaxDOB = dtPaxDOB.Value.AddYears(2);
                                        List<FlightPakMasterService.FlightPurpose> purposeList = (List<FlightPakMasterService.FlightPurpose>)Session["FPurposeList"];
                                        purposeList = purposeList.Where(x => x.FlightPurposeID == Pax.FlightPurposeID && x.IsPersonalTravel == true).ToList();
                                        if (purposeList.Count > 0 && (dtPaxDOB == null || dtPaxDOB < DateTime.Now))
                                            privatePaxCount += 1;
                                        else
                                            nonPrivatePaxCount += 1;

                                    }
                                }
                            }
                            NonPrivatePaxIds.Add(legIndex, nonPrivatePaxCount);
                            privatePaxCount = 0;
                            nonPrivatePaxCount = 0;
                        }

                        using (PreflightServiceClient ServiceClient = new PreflightServiceClient())
                        {
                            // Deletes all the existing records for the trip.
                            var successFlag = ServiceClient.DeleteAllPreflightTripSIFL(Trip.TripID);
                        }
                        // Retain the Existing SIFL Grid values
                        List<PreflightTripSIFL> RecalcSiflList = new List<PreflightTripSIFL>();

                        for (int legIndex = 0; legIndex < Trip.PreflightLegs.Count; legIndex++)
                        {
                            var trip = Trip.PreflightLegs[legIndex];
                            using (var calculationServiceClient = new CalculationServiceClient())
                            {
                                siflRateList = calculationServiceClient.GetSIFLRate(trip.DepartureDTTMLocal.Value).EntityList;
                                if (siflRateList != null && siflRateList.Count > 0)
                                {

                                    var siflRate = siflRateList[0];
                                    // Session["SIFLRateKey"] = siflRateList; 
                                    //var isDepartDateExceedsSiflEndDate = Trip.PreflightLegs.Any(x => x.DepartureGreenwichDTTM > siflRate.EndDT.Value);

                                    var isDepartDateExceedsSiflEndDate = siflRateList.Any(x => (x.StartDT <= trip.DepartureDTTMLocal.Value) && (x.EndDT >= trip.DepartureDTTMLocal.Value));
                                    if (!isDepartDateExceedsSiflEndDate)
                                    {
                                        RadWindowManager1.RadAlert("NO SIFL rates have been set for the current date range being calculated. Departure Date:" + trip.DepartureDTTMLocal.Value.ToShortDateString() + ".", 500, 100, "System Messages", "alertCallBackFn", null);
                                    }

                                    rate1 = (decimal)siflRate.Rate1;
                                    rate2 = (decimal)siflRate.Rate2;
                                    rate3 = (decimal)siflRate.Rate3;
                                    if (siflRateList[0].TerminalCHG != null)
                                    {
                                        terminalCharge = Math.Round(Convert.ToDecimal(siflRate.TerminalCHG), 2);
                                    }

                                    fareLevelID = siflRate.FareLevelID;
                                }
                                else
                                { // when no sifl rate is set in DB catalogue for this Customer...

                                    RadWindowManager1.RadAlert("No SIFL rate has been set for this Customer in Database.", 500, 100, "System Messages", "alertNoCusFn", null);
                                    //RadWindowManager1.RadAlert("No SIFL rate has been set for this Customer in Database.", 500, 100, "System Messages", "");
                                    return;
                                }
                            }




                            if (Trip.PreflightLegs[legIndex].PreflightPassengerLists != null && Trip.PreflightLegs[legIndex].PreflightPassengerLists.Count > 0)
                            {
                                foreach (PreflightPassengerList Pax in Trip.PreflightLegs[legIndex].PreflightPassengerLists)
                                {
                                    if (Session["FPurposeList"] != null)
                                    {
                                        long PaxId;
                                        long.TryParse(Convert.ToString(Pax.PassengerID), out PaxId);
                                        List<FlightPak.Web.FlightPakMasterService.GetAllPassengerWithFilters> PassengerList = new List<FlightPakMasterService.GetAllPassengerWithFilters>();
                                        FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient();
                                        var lstPaxList = objDstsvc.GetAllPassengerWithFilters(0, 0, PaxId, string.Empty, false, false, string.Empty, 0);
                                        DateTime? dtPaxDOB = null;
                                        if (lstPaxList.ReturnFlag)
                                        {
                                            PassengerList = lstPaxList.EntityList;
                                            if (PassengerList != null && PassengerList.Count > 0)
                                                dtPaxDOB = PassengerList[0].DateOfBirth;
                                        }
                                        if (dtPaxDOB != null)
                                            dtPaxDOB = dtPaxDOB.Value.AddYears(2);
                                        List<FlightPakMasterService.FlightPurpose> purposeList = (List<FlightPakMasterService.FlightPurpose>)Session["FPurposeList"];
                                        purposeList = purposeList.Where(x => x.FlightPurposeID == Pax.FlightPurposeID && x.IsPersonalTravel == true).ToList();
                                        if (purposeList.Count > 0 && (dtPaxDOB == null || dtPaxDOB < DateTime.Now))
                                        {
                                            decimal miles = 0;
                                            if (!string.IsNullOrEmpty(Trip.PreflightLegs[legIndex].DepartICAOID.ToString()) && !string.IsNullOrEmpty(Trip.PreflightLegs[legIndex].ArriveICAOID.ToString()))
                                            {
                                                using (CalculationServiceClient CalcService = new CalculationServiceClient())
                                                {
                                                    miles = Convert.ToDecimal(CalcService.GetDistance(Trip.PreflightLegs[legIndex].Airport1.AirportID, Convert.ToInt64(Trip.PreflightLegs[legIndex].ArriveICAOID)));
                                                    // Calculate Statute Miles
                                                    statueMiles = Math.Floor(miles * (decimal)1.15078);
                                                }
                                            }

                                            using (PreflightService.PreflightServiceClient objService = new PreflightServiceClient())
                                            {
                                                var retValue = objService.GetSIFLPassengerEmployeeType(Convert.ToInt64(Pax.PassengerID));
                                                List<GetSIFLPassengerEmployeeType> resultList = new List<GetSIFLPassengerEmployeeType>();
                                                if (retValue.ReturnFlag == true && retValue.EntityList.Count > 0)
                                                {
                                                    foreach (GetSIFLPassengerEmployeeType result in retValue.EntityList)
                                                    {
                                                        if (result.IsEmployeeType == null)
                                                            empType = ModuleNameConstants.Preflight.Employee_Type_NonControlled;
                                                        else
                                                            empType = result.IsEmployeeType;


                                                        PaxCode = result.PassengerRequestorCD;

                                                    }
                                                }


                                                PreflightTripSIFL Sifl = new PreflightTripSIFL();
                                                Sifl.LegID = Trip.PreflightLegs[legIndex].LegID;
                                                Sifl.FareLevelID = (long)fareLevelID;
                                                //temporarilry using SIFL ID
                                                //Sifl.PreflightTripSIFLID = (long)Trip.PreflightLegs[legIndex].LegNUM;
                                                Sifl.PassengerRequestorID = (long)Pax.PassengerID;
                                                Sifl.PassengerName = GetPassengerName(Pax.PassengerLastName, Pax.PassengerFirstName, Pax.PassengerMiddleName);
                                                Sifl.EmployeeTYPE = empType;
                                                long? associatedPassengerID = null;
                                                string associatedPassengerCD = string.Empty;
                                                if (empType == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                                {
                                                    var AssociatedValue = objService.GetAssociatePassengerID(Convert.ToInt64(Pax.PassengerID));
                                                    if (AssociatedValue.ReturnFlag == true && AssociatedValue.EntityList.Count > 0)
                                                    {
                                                        associatedPassengerID = AssociatedValue.EntityList[0].PassengerRequestorID;
                                                        associatedPassengerCD = AssociatedValue.EntityList[0].PassengerRequestorCD;
                                                    }
                                                    else
                                                        Sifl.Passenger = null;

                                                    //Prakash Code -Start 


                                                }



                                                #region Code Change to Get Emptype of AssociatePassenger

                                                if (empType == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                                {
                                                    if (associatedPassengerID != null && associatedPassengerID > 0)
                                                    {
                                                        var AssociatePassretValue = objService.GetSIFLPassengerEmployeeType(associatedPassengerID.Value);

                                                        if (AssociatePassretValue.ReturnFlag == true && AssociatePassretValue.EntityList.Count > 0)
                                                        {
                                                            foreach (GetSIFLPassengerEmployeeType AssociatePassresultList in AssociatePassretValue.EntityList)
                                                            {
                                                                if (AssociatePassresultList.IsEmployeeType == null)
                                                                    empType = ModuleNameConstants.Preflight.Employee_Type_NonControlled;
                                                                else
                                                                    empType = AssociatePassresultList.IsEmployeeType;

                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                                Sifl.AssociatedPassengerID = associatedPassengerID;

                                                bool addSiflInSaveList = true;


                                                List<GetPOFleetProfile> fleetProfileList = (List<GetPOFleetProfile>)Session["SIFLFleetProfile"];
                                                if (fleetProfileList != null && fleetProfileList.Count > 0)
                                                {
                                                    if (empType == ModuleNameConstants.Preflight.Employee_Type_Controlled)
                                                    {
                                                        if (fleetProfileList[0].SIFLMultiControlled != null)
                                                            multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiControlled;
                                                    }
                                                    else if (empType == ModuleNameConstants.Preflight.Employee_Type_NonControlled)
                                                    {
                                                        if (fleetProfileList[0].SIFLMultiNonControled != null)
                                                            multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;
                                                    }
                                                    if (fleetProfileList[0].MaximumPassenger != null)
                                                        maxPax = (decimal)fleetProfileList[0].MaximumPassenger;
                                                }


                                                // if PassengerRequestorID or associatedPassengerID is ISSIFL, then multiplierTotal = MultiSec...
                                                using (FlightPakMasterService.MasterCatalogServiceClient masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var passengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Sifl.PassengerRequestorID.Value).EntityList;
                                                    if (passengers.Count > 0)
                                                    {
                                                        if (passengers.First().IsSIFL == true) // if passenger is SIFL secured, get MultiSec for multiplier, else check for Associated PAX
                                                        {
                                                            if (fleetProfileList != null && fleetProfileList[0].MultiSec != null)
                                                                multiplierTotal = (decimal)fleetProfileList[0].MultiSec;
                                                        }
                                                        else
                                                        {
                                                            if (associatedPassengerID != null)
                                                            {
                                                                var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(associatedPassengerID.Value).EntityList;
                                                                if (associatedPassengers.Count > 0)
                                                                {
                                                                    if (associatedPassengers.First().IsSIFL == true)
                                                                    {
                                                                        if (fleetProfileList != null && fleetProfileList[0].MultiSec != null)
                                                                            multiplierTotal = (decimal)fleetProfileList[0].MultiSec;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }


                                                if (Trip.PreflightLegs[legIndex].Airport1 != null)
                                                    Sifl.DepartICAOID = Trip.PreflightLegs[legIndex].DepartICAOID;

                                                if (Trip.PreflightLegs[legIndex].Airport != null)
                                                    Sifl.ArriveICAOID = Trip.PreflightLegs[legIndex].ArriveICAOID;


                                                long nonPvtCount = NonPrivatePaxIds[legIndex];
                                                if (maxPax > 0)
                                                {
                                                    Sifl.LoadFactor = Math.Round(((nonPvtCount / maxPax) * 100), 1);
                                                    // if loadfactor > 50 and emp type is conTrolled, change type to Non-Controlled / if emptype is Guest, no SIFL calculation done
                                                    if (Sifl.LoadFactor > 50)
                                                    {
                                                        if (Sifl.EmployeeTYPE == ModuleNameConstants.Preflight.Employee_Type_Controlled)
                                                        {
                                                            if (fleetProfileList != null && fleetProfileList[0].SIFLMultiNonControled != null)
                                                                multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;
                                                            Sifl.EmployeeTYPE = ModuleNameConstants.Preflight.Employee_Type_NonControlled;
                                                            rbEmployeeType.SelectedValue = ModuleNameConstants.Preflight.Employee_Type_NonControlled;

                                                        }
                                                        else if (Sifl.EmployeeTYPE == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                                        {
                                                            addSiflInSaveList = false;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Sifl.LoadFactor = Convert.ToDecimal("0.0");
                                                }
                                                Sifl.AircraftMultiplier = multiplierTotal;


                                                if (addSiflInSaveList)
                                                {
                                                    miles1 = 0;
                                                    miles2 = 0;
                                                    miles3 = 0;
                                                    if (statueMiles > 0)
                                                    {

                                                        if (statueMiles >= 1500)
                                                        {
                                                            miles1 = 500;
                                                            miles2 = 1000;
                                                            miles3 = (statueMiles - 1500);
                                                        }
                                                        else if ((statueMiles >= 501) && (statueMiles <= 1500))
                                                        {
                                                            miles1 = 500;
                                                            miles2 = (statueMiles - 500);
                                                        }
                                                        else if ((statueMiles > 0) && (statueMiles <= 500))
                                                            miles1 = statueMiles;
                                                        // Calculate Distance Rate
                                                        distanceRate1 = (rate1 * Convert.ToDecimal(miles1));
                                                        distanceRate2 = (rate2 * Convert.ToDecimal(miles2));
                                                        distanceRate3 = (rate3 * Convert.ToDecimal(miles3));
                                                        distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;

                                                        Sifl.AircraftMultiplier = multiplierTotal;
                                                        multiplierTotal = (multiplierTotal / 100);
                                                        // Calculate SIFL Total                                                    
                                                        siflTotal = Math.Round(((distancRateTotal * multiplierTotal) + terminalCharge), 2);
                                                    }

                                                    Sifl.Rate1 = Math.Round(rate1, 4);
                                                    Sifl.Rate2 = Math.Round(rate2, 4);
                                                    Sifl.Rate3 = Math.Round(rate3, 4);
                                                    Sifl.Distance = statueMiles;
                                                    Sifl.Distance1 = miles1;
                                                    Sifl.Distance2 = miles2;
                                                    Sifl.Distance3 = miles3;
                                                    // Sifl.Distance = Math.Floor(distancRateTotal);
                                                    Sifl.Distance = Sifl.Distance1 + Sifl.Distance2 + Sifl.Distance3;
                                                    Sifl.TeminalCharge = Math.Round(terminalCharge, 2);
                                                    Sifl.AmtTotal = Math.Round(siflTotal, 2);

                                                    RecalcSiflList.Add(Sifl);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        PreflightMain Triptemp = (PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                        var productsByCategory = RecalcSiflList.GroupBy(x => x.PassengerRequestorID).ToList();
                        List<PreflightTripSIFL> RecalcSiflMain = new List<PreflightTripSIFL>();
                        foreach (var PFPAX in productsByCategory)
                        {
                            long PaxID = (long)PFPAX.Key;
                            List<PreflightTripSIFL> RecalcSiflTemp = new List<PreflightTripSIFL>();

                            for (int legIndex = 1; legIndex <= Triptemp.PreflightLegs.Count; legIndex++)
                            {

                                List<PreflightLeg> PFleg_1 = Triptemp.PreflightLegs.Where(x => x.LegNUM == legIndex).ToList();
                                List<PreflightTripSIFL> RecalcSiflList_1 = new List<PreflightTripSIFL>();
                                List<PreflightTripSIFL> RecalcSiflList_2 = new List<PreflightTripSIFL>();
                                RecalcSiflList_1 = RecalcSiflList.Where(x => x.PassengerRequestorID == PaxID && x.LegID == PFleg_1[0].LegID).ToList();
                                if (RecalcSiflList_1.Count == 0)
                                    continue;
                                else if (RecalcSiflTemp.Count == 0 && RecalcSiflList_1.Count > 0)
                                {
                                    RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                                }
                                else if (RecalcSiflTemp.Count > 0 && RecalcSiflList_1.Count > 0)
                                {
                                    long LastLegNum = (long)((FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip]).PreflightLegs.Where(x => x.LegID == (long)RecalcSiflTemp[RecalcSiflTemp.Count - 1].LegID).ToList()[0].LegNUM;
                                    if ((legIndex - LastLegNum) == 1)
                                    {
                                        //13-Mar-2014 - Fixed layover issue. Passed LastLegNum instead of current leg
                                        double ActualLayover = CalculateDifference(Triptemp, Convert.ToInt16(LastLegNum));

                                        // Fix for #2344 - SIFL layover hours
                                        //This field should accept a 0 or be allowed to contain a null value (left blank) if not needed. If this field is set to 0, then the turnaround between legs will be calculated as 10 hours and 1 minute
                                        double siflLayoverHrs = 10.0167;

                                        if (UserPrincipal.Identity._fpSettings._LayoverHrsSIFL != null && UserPrincipal.Identity._fpSettings._LayoverHrsSIFL > 0)
                                            siflLayoverHrs = Convert.ToDouble((int)UserPrincipal.Identity._fpSettings._LayoverHrsSIFL);

                                        //if (ActualLayover <= Convert.ToDouble((int)UserPrincipal.Identity._fpSettings._LayoverHrsSIFL))
                                        if (ActualLayover <= siflLayoverHrs) // Convert.ToDouble((int)UserPrincipal.Identity._fpSettings._LayoverHrsSIFL))
                                        {
                                            //RecalcSiflTemp[RecalcSiflTemp.Count - 1].AmtTotal = RecalcSiflTemp[RecalcSiflTemp.Count - 1].AmtTotal + RecalcSiflList_1[0].AmtTotal;
                                            RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance = RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance + RecalcSiflList_1[0].Distance;
                                            RecalcSiflTemp[RecalcSiflTemp.Count - 1].Airport = RecalcSiflList_1[0].Airport;
                                            //13-Mar-2014 - Fixed issue of change of arrival airport name after log save
                                            RecalcSiflTemp[RecalcSiflTemp.Count - 1].ArriveICAOID = RecalcSiflList_1[0].ArriveICAOID;
                                            //temporarily using POSIFLID for LegNum
                                            //RecalcSiflTemp[RecalcSiflTemp.Count - 1].PreflightTripSIFLID = RecalcSiflList_1[0].PreflightTripSIFLID;
                                            // removed using PreflightTripSIFLID as legnum
                                            RecalcSiflTemp[RecalcSiflTemp.Count - 1].LegID = RecalcSiflList_1[0].LegID;
                                            statueMiles = (decimal)RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance;
                                            miles1 = 0;
                                            miles2 = 0;
                                            miles3 = 0;
                                            if (statueMiles > 0)
                                            {
                                                if (statueMiles >= 1500)
                                                {
                                                    miles1 = 500;
                                                    miles2 = 1000;
                                                    miles3 = (statueMiles - 1500);
                                                }
                                                else if ((statueMiles >= 501) && (statueMiles <= 1500))
                                                {
                                                    miles1 = 500;
                                                    miles2 = (statueMiles - 500);
                                                }
                                                else if ((statueMiles > 0) && (statueMiles <= 500))
                                                {
                                                    miles1 = statueMiles;
                                                }

                                                rate1 = (decimal)RecalcSiflTemp[RecalcSiflTemp.Count - 1].Rate1;
                                                rate2 = (decimal)RecalcSiflTemp[RecalcSiflTemp.Count - 1].Rate2;
                                                rate3 = (decimal)RecalcSiflTemp[RecalcSiflTemp.Count - 1].Rate3;


                                                // Calculate Distance Rate
                                                distanceRate1 = (rate1 * Convert.ToDecimal(miles1));
                                                distanceRate2 = (rate2 * Convert.ToDecimal(miles2));
                                                distanceRate3 = (rate3 * Convert.ToDecimal(miles3));
                                                terminalCharge = (decimal)RecalcSiflTemp[RecalcSiflTemp.Count - 1].TeminalCharge;
                                                //Sifl.AircraftMultiplier = multiplierTotal;
                                                multiplierTotal = ((decimal)RecalcSiflTemp[RecalcSiflTemp.Count - 1].AircraftMultiplier / 100);
                                                distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;
                                                siflTotal = Math.Round(((distancRateTotal * multiplierTotal) + terminalCharge), 2);
                                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance1 = miles1;
                                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance2 = miles2;
                                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance3 = miles3;
                                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].AmtTotal = siflTotal;
                                            }
                                        }
                                        else
                                        {
                                            RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                                        }
                                        //}
                                        //else
                                        //{
                                        //    RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                                        //}


                                    }
                                    else
                                    {
                                        RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                                    }

                                }
                            }

                            RecalcSiflMain.AddRange(RecalcSiflTemp);

                        }
                        //POSIFL used as temp legnum. Now updating to 0
                        // removed using PreflightTripSIFLID as legnum
                        //foreach (var TempSIFL in RecalcSiflMain)
                        //{
                        //    TempSIFL.PreflightTripSIFLID = 0;
                        //}

                        SaveSIFL(RecalcSiflMain);
                        btnSave.Enabled = false;
                        btnCancel.Enabled = false;

                        btnSave.CssClass = "button-disable";
                        btnCancel.CssClass = "button-disable";

                        btnEdit.Enabled = true;
                        btnDelete.Enabled = true;
                        btnEdit.CssClass = "button";
                        btnDelete.CssClass = "button";
                        SelectItem();

                        #endregion

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Handles New Click Event and allows user to add new SIFL Calculation of a Passenger
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void New_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Checks for the SIFL rate existency - for current date
                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];

                        if (GetSiflRateForCurrentDate() > 0) { return; }

                        Session["selectedSIFlID"] = "0";
                        EnableSIFLFields(true);
                        ClearSIFLFields(false);

                        //if (dgSifl.Items.Count == 0)
                        //{
                        //    dgSifl.DataSource = null;
                        //    dgSifl.DataBind();
                        //}

                        btnReCalcSIFL.Enabled = false;
                        btnNew.Enabled = false;
                        btnEdit.Enabled = false;
                        btnDelete.Enabled = false;
                        btnSave.Enabled = false;
                        btnCancel.Enabled = true;
                        btnUpdateSIFL.Enabled = true;

                        btnReCalcSIFL.CssClass = "button-disable";
                        btnNew.CssClass = "button-disable";
                        btnEdit.CssClass = "button-disable";
                        btnDelete.CssClass = "button-disable";
                        btnSave.CssClass = "button-disable";
                        btnCancel.CssClass = "button";
                        btnUpdateSIFL.CssClass = "button";

                        //RadAjaxManager.GetCurrent(Page).FocusControl(tbPax);
                        tbAssocPax.Enabled = false;
                        btnAssocPax.Enabled = false;
                        btnAssocPax.CssClass = "browse-button-disabled";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        private int GetSiflRateForCurrentDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int value = 0;
                using (var calculationServiceClient = new CalculationServiceClient())
                {
                    if (Trip == null)
                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];

                    var siflRateList = calculationServiceClient.GetSIFLRate(Trip.PreflightLegs[0].DepartureDTTMLocal.Value).EntityList;
                    if (siflRateList.Count > 0)
                    {
                        var siflRate = siflRateList[0];
                        Session["SIFLRateKey"] = siflRateList;

                        //var isDepartDateExceedsSiflEndDate = Trip.PreflightLegs.Any(x => x.DepartureGreenwichDTTM > siflRate.EndDT.Value);

                        var isDepartDateExceedsSiflEndDate = siflRateList.Any(x => (x.StartDT <= Trip.PreflightLegs[0].DepartureDTTMLocal.Value) && (x.EndDT >= Trip.PreflightLegs[0].DepartureDTTMLocal.Value));
                        if (!isDepartDateExceedsSiflEndDate) // siflRate.EndDT.Value < DateTime.Now)
                        {
                            RadWindowManager1.RadAlert("NO SIFL rates have been set for the current date range being calculated. Departure Date:" + Trip.PreflightLegs[0].DepartureDTTMLocal.Value.ToShortDateString() + ".", 500, 100, "System Messages", "alertCallBackFn", null);
                        }

                        tbRate1.Text = siflRate.Rate1.Value.ToString();
                        tbRate2.Text = siflRate.Rate2.Value.ToString();
                        tbRate3.Text = siflRate.Rate3.Value.ToString();

                        tbTerminalCharge.Text = Math.Round(siflRate.TerminalCHG.Value, 2).ToString();
                        hdnFareLevel.Value = siflRate.FareLevelID.ToString();
                        return value;
                    }
                    else
                    { // when no sifl rate is set in DB catalogue for this Customer...

                        RadWindowManager1.RadAlert("No SIFL rate has been set for this Customer in Database.", 500, 100, "System Messages", "alertNoCusFn", null);
                        //RadWindowManager1.RadAlert("No SIFL rate has been set for this Customer in Database.", 500, 100, "System Messages", "");
                        return value = 1;
                    }
                }
            }
        }

        /// <summary>
        /// Allows editing of exting records of a selected Passenger from Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgSifl.MasterTableView.Items.Count > 0)
                        {
                            EnableSIFLFields(true);

                            btnReCalcSIFL.Enabled = false;
                            btnNew.Enabled = false;
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;
                            btnSave.Enabled = true;
                            btnCancel.Enabled = true;
                            btnUpdateSIFL.Enabled = true;

                            btnReCalcSIFL.CssClass = "button-disable";
                            btnNew.CssClass = "button-disable";
                            btnEdit.CssClass = "button-disable";
                            btnDelete.CssClass = "button-disable";
                            btnSave.CssClass = "button";
                            btnCancel.CssClass = "button";
                            btnUpdateSIFL.CssClass = "button";

                            if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Controlled || rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_NonControlled)
                            {
                                tbAssocPax.Enabled = false;
                                btnAssocPax.Enabled = false;
                                btnAssocPax.CssClass = "browse-button-disabled";
                            }
                            else
                            {
                                tbAssocPax.Enabled = true;
                                btnAssocPax.Enabled = true;
                                btnAssocPax.CssClass = "browse-button";
                            }

                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPax);
                        }
                        else
                        {
                            EnableSIFLFields(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Deletes the selected of Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {   //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (PreflightServiceClient service = new PreflightServiceClient())
                        {
                            if (dgSifl.SelectedItems.Count > 0)
                            {
                                GridDataItem Item = (GridDataItem)dgSifl.SelectedItems[0];

                                Item.FireCommandEvent("Delete", string.Empty);

                                var returnValue = service.DeletePreflightTripSIFL(Convert.ToInt64(Item.GetDataKeyValue("PreflightTripSIFLID").ToString()));
                                if (returnValue.ReturnFlag == true)
                                {
                                    tbAssocPax.Text = string.Empty;
                                    tbStatueMiles.Text = string.Empty;
                                    tbAircraftMultiplier.Text = string.Empty;
                                    tbArrives.Text = string.Empty;
                                    tbDeparts.Text = string.Empty;
                                    tbAssocPax.Text = string.Empty;
                                    tbPax.Text = string.Empty;
                                    tbRate1.Text = string.Empty;
                                    tbRate2.Text = string.Empty;
                                    tbRate3.Text = string.Empty;
                                    tbSIFLTotal.Text = string.Empty;
                                    tbTerminalCharge.Text = string.Empty;
                                    tbMiles1.Text = string.Empty;
                                    tbMiles2.Text = string.Empty;
                                    tbMiles3.Text = string.Empty;

                                    LoadSifl();

                                }
                                RadAjaxManager.GetCurrent(Page).FocusControl(btnNew);
                            }
                            else
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(btnReCalcSIFL);
                            }
                            if (dgSifl.MasterTableView.Items.Count > 0)
                            {
                                SelectItem();
                                btnEdit.Enabled = true;
                                btnDelete.Enabled = true;
                                btnEdit.CssClass = "button";
                                btnDelete.CssClass = "button";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Saves the SIFL calculation into the databse of a passenger
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            if (string.IsNullOrEmpty(tbArrives.Text))
                            {
                                RadWindowManager1.RadAlert("Arrival ICAO is not selected.", 300, 50, "System Messages", "alertCallArrAirFn", null);
                                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Arrival ICAO is not selected.');", true);
                                return;
                            }

                            if (string.IsNullOrEmpty(tbDeparts.Text))
                            {
                                RadWindowManager1.RadAlert("Depart ICAO is not seleted.", 300, 50, "System Messages", "alertCallDepAirFn", null);
                                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Depart ICAO is not selected.');", true);
                                return;
                            }
                            if (string.IsNullOrEmpty(tbPax.Text))
                            {
                                RadWindowManager1.RadAlert("Pax is not seleted.", 300, 50, "System Messages", "alertNotPaxFn", null);
                                // System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Pax is not selected');", true);
                                return;
                            }

                            if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest && string.IsNullOrEmpty(tbAssocPax.Text))
                            {
                                // ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Associated With Passenger Code is Required.', 275, 100,'System Messages'); return false;});", true);
                                // System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Associated With Passenger Code is Required');", true);
                                RadWindowManager1.RadAlert("Associated With Passenger Code is Required", 320, 50, "System Messages", "AssocPaxAlert", null);
                                return;
                            }
                            List<PreflightTripSIFL> siflList = CreateSIFLList();
                            SaveSIFL(siflList);


                            btnReCalcSIFL.Enabled = true;
                            btnNew.Enabled = true;
                            btnCancel.Enabled = false;
                            btnSave.Enabled = false;

                            btnReCalcSIFL.CssClass = "button";
                            btnNew.CssClass = "button";
                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";

                            if (dgSifl.MasterTableView.Items.Count > 0)
                            {
                                btnEdit.Enabled = true;
                                btnDelete.Enabled = true;
                                btnEdit.CssClass = "button";
                                btnDelete.CssClass = "button";
                                SelectItem();
                            }

                            RadAjaxManager.GetCurrent(Page).FocusControl(btnReCalcSIFL);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Cancels the Extsing flow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnReCalcSIFL);
                        if (dgSifl.SelectedItems.Count > 0)
                        {
                            btnEdit.Enabled = true;
                            btnDelete.Enabled = true;

                            btnEdit.CssClass = "button";
                            btnDelete.CssClass = "button";

                        }
                        else
                        {
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;

                            btnEdit.CssClass = "button-disable";
                            btnDelete.CssClass = "button-disable";
                        }

                        if (Session["PreflightSiflData"] == null)
                        {
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;

                            btnEdit.CssClass = "button-disable";
                            btnDelete.CssClass = "button-disable";
                        }

                        btnNew.Enabled = true;
                        btnReCalcSIFL.Enabled = true;
                        btnCancel.Enabled = false;
                        btnSave.Enabled = false;
                        btnUpdateSIFL.Enabled = false;

                        btnNew.CssClass = "button";
                        btnReCalcSIFL.CssClass = "button";
                        btnCancel.CssClass = "button-disable";
                        btnSave.CssClass = "button-disable";
                        btnUpdateSIFL.CssClass = "button-disable";
                        EnableSIFLFields(false);
                        //ClearSIFLFields(true);
                        if (dgSifl.MasterTableView.Items.Count > 0)
                        {
                            btnEdit.Enabled = true;
                            btnDelete.Enabled = true;
                            btnEdit.CssClass = "button";
                            btnDelete.CssClass = "button";
                            dgSifl.SelectedIndexes.Clear();
                            SelectItem();
                            Sifl_SelectedIndexChanged(sender, e);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Binds the Leginfo Grid
        /// </summary>
        protected void BindLegInfo()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // List for Personal Grid Bind
                List<PreflightSIFLLegInfo> LegInfolList = new List<PreflightSIFLLegInfo>();
                // Get the Maximum Passenger value from Fleet Profile Catalog
                decimal maxPax = 0;
                if (Session["SIFLFleetProfile"] != null)
                {
                    List<GetPOFleetProfile> fleetProfileList = (List<GetPOFleetProfile>)Session["SIFLFleetProfile"];
                    if (fleetProfileList.Count > 0)
                        maxPax = (decimal)fleetProfileList[0].MaximumPassenger;
                }
                // Get Flight Purpose List from Master
                Session.Remove("FPurposeList");

                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var retVal = Service.GetFlightPurposeList();

                    if (retVal.ReturnFlag == true)
                        Session["FPurposeList"] = retVal.EntityList;
                }

                int privatePaxCount = 0;
                int nonPrivatePaxCount = 0;
                NonPrivatePaxIds = new Dictionary<long, long>();

                if (Trip == null)
                {
                    Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                }

                for (int legIndex = 0; legIndex < Trip.PreflightLegs.Count; legIndex++)
                {
                    if (Trip.PreflightLegs[legIndex].PreflightPassengerLists != null)
                    {
                        if (Trip.PreflightLegs[legIndex].PreflightPassengerLists.Count > 0)
                        {
                            foreach (PreflightPassengerList Pax in Trip.PreflightLegs[legIndex].PreflightPassengerLists)
                            {
                                if (Session["FPurposeList"] != null)
                                {
                                    List<FlightPakMasterService.FlightPurpose> purposeList = (List<FlightPakMasterService.FlightPurpose>)Session["FPurposeList"];

                                    //string FPCode = Pax.FlightPurpose.FlightPurposeCD;

                                    purposeList = purposeList.Where(x => x.FlightPurposeID == Pax.FlightPurposeID && x.IsPersonalTravel == true).ToList();
                                    if (purposeList.Count > 0)
                                        privatePaxCount += 1;
                                    else
                                        nonPrivatePaxCount += 1;
                                }
                            }
                        }
                    }
                    NonPrivatePaxIds.Add(legIndex, nonPrivatePaxCount);
                    privatePaxCount = 0;
                    nonPrivatePaxCount = 0;
                }
                for (int legIndex = 0; legIndex < Trip.PreflightLegs.Count; legIndex++)
                {
                    var trip = Trip.PreflightLegs[legIndex];
                    PreflightSIFLLegInfo legInfo = new PreflightSIFLLegInfo();
                    legInfo.LegID = trip.LegID;
                    legInfo.Depart = trip.Airport1.IcaoID;
                    legInfo.Arrive = trip.Airport.IcaoID;
                    legInfo.LegNum = trip.LegNUM.Value;
                    long nonPvtCount = NonPrivatePaxIds[legIndex];
                    if (maxPax > 0)
                    {
                        legInfo.LoadFactor = Math.Round(((nonPvtCount / maxPax) * 100), 1).ToString();

                    }
                    else
                    {
                        legInfo.LoadFactor = "0.0";
                    }
                    var personalTravelPurposeIdsInTrip = (List<string>)Session["PersonalTravelPurposeIds"];
                    bool isPersonalTravelAvailableInLeg = Trip.PreflightLegs[legIndex].PreflightPassengerLists.Any(x => personalTravelPurposeIdsInTrip.Contains(x.FlightPurposeID.Value.ToString()));
                    if (isPersonalTravelAvailableInLeg)
                    {
                        legInfo.Personal = "P";
                    }
                    else
                    {
                        legInfo.Personal = string.Empty;
                    }
                    LegInfolList.Add(legInfo);
                }
                dgSIFLPersonal.DataSource = LegInfolList;//.OrderBy(x => x.LegNum);
                dgSIFLPersonal.DataBind();
            }
        }

        /// <summary>
        /// SIFL Grid On Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Sifl_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EnableSIFLFields(true);
                        GridDataItem Item = (GridDataItem)dgSifl.SelectedItems[0];
                        if (Session["PreflightSiflData"] == null)
                        {
                            return;
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(Item.GetDataKeyValue("AssociatedPassengerID"))))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Item.GetDataKeyValue("AssociatedPassengerID"))).EntityList.FirstOrDefault();
                                if (associatedPassengers != null)
                                {
                                    tbAssocPax.Text = associatedPassengers.PassengerRequestorCD;
                                    lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(associatedPassengers.PassengerName);
                                    hdnAssocPax.Value = Convert.ToString(associatedPassengers.PassengerRequestorID);
                                    hdnAssCode.Value = associatedPassengers.PassengerRequestorCD;
                                    hdnAssocPax.Value = Convert.ToString(associatedPassengers.PassengerRequestorID);

                                }
                            }
                        }
                        else
                        {
                            tbAssocPax.Text = string.Empty;
                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnAssocPax.Value = string.Empty;
                            hdnAssCode.Value = string.Empty;
                            hdnAssocPax.Value = string.Empty;
                        }

                        //if (!string.IsNullOrEmpty(((HiddenField)Item.FindControl("hdnAssocPaxCD")).Value))
                        //{
                        //tbAssocPax.Text = hdnAssCode.Value;
                        //tbAssocPax.Enabled = false;
                        //btnAssocPax.Enabled = false;
                        //btnAssocPax.CssClass = "browse-button-disabled";
                        //}
                        if (Item.GetDataKeyValue("LegID") != null)
                        {
                            hdnPaxLegID.Value = Item.GetDataKeyValue("LegID").ToString();
                        }
                        Session["selectedSIFlID"] = Item.GetDataKeyValue("PreflightTripSIFLID").ToString();
                        tbPax.Text = ((Label)Item.FindControl("lbPaxCode")).Text;
                        hdnPax.Value = ((HiddenField)Item.FindControl("hdnPaxID")).Value;
                        hdnPaxName.Value = Item.GetDataKeyValue("PassengerName").ToString();
                        lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(hdnPaxName.Value);
                        //tbAssocPax.Text = ((HiddenField)Item.FindControl("hdnAssocPaxCD")).Value;
                        //hdnAssocPax.Value = ((HiddenField)Item.FindControl("hdnAssocPaxID")).Value;
                        if (!string.IsNullOrEmpty(((Label)Item.FindControl("lbEmployeeType")).Text))
                            rbEmployeeType.SelectedValue = ((Label)Item.FindControl("lbEmployeeType")).Text.Trim();

                        tbDeparts.Text = ((Label)Item.FindControl("lbDepartCD")).Text;
                        hdnDeparts.Value = ((HiddenField)Item.FindControl("hdnDepartID")).Value;

                        tbArrives.Text = ((Label)Item.FindControl("lbArriveCD")).Text;
                        hdnArrives.Value = ((HiddenField)Item.FindControl("hdnArriveID")).Value;
                        lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(hdnPaxName.Value);


                        using (var masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdnDeparts.Value))
                            {
                                var departureAirports = masterServiceClient.GetAirportByAirportID(Convert.ToInt64(hdnDeparts.Value)).EntityList;
                                if (departureAirports.Count > 0 && departureAirports.First() != null)
                                {
                                    PreflightService.Airport departObj = new PreflightService.Airport() { IcaoID = departureAirports.First().IcaoID, AirportName = departureAirports.First().AirportName };
                                    tbDeparts.Text = departObj.IcaoID;
                                    lbDepart.Text = System.Web.HttpUtility.HtmlEncode(departObj.AirportName);
                                }
                                else
                                {
                                    tbDeparts.Text = "";
                                    lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }
                            }

                            if (!string.IsNullOrEmpty(hdnArrives.Value))
                            {
                                var arrivalAirports = masterServiceClient.GetAirportByAirportID(Convert.ToInt64(hdnArrives.Value)).EntityList;
                                if (arrivalAirports.Count > 0 && arrivalAirports.First() != null)
                                {
                                    PreflightService.Airport arriveObj = new PreflightService.Airport() { IcaoID = arrivalAirports.First().IcaoID, AirportName = arrivalAirports.First().AirportName };
                                    tbArrives.Text = arriveObj.IcaoID;
                                    lbArrives.Text = System.Web.HttpUtility.HtmlEncode(arriveObj.AirportName);
                                }
                                else
                                {
                                    tbArrives.Text = "";
                                    lbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }
                            }
                        }

                        if (Item.GetDataKeyValue("AircraftMultiplier") != null)
                        {
                            tbAircraftMultiplier.Text = Item.GetDataKeyValue("AircraftMultiplier").ToString().Trim();
                        }
                        tbStatueMiles.Text = ((Label)Item.FindControl("lbStatute")).Text;
                        tbMiles1.Text = ((HiddenField)Item.FindControl("hdnDistance1")).Value;
                        tbMiles2.Text = ((HiddenField)Item.FindControl("hdnDistance2")).Value;
                        tbMiles3.Text = ((HiddenField)Item.FindControl("hdnDistance3")).Value;
                        tbTerminalCharge.Text = ((RadNumericTextBox)Item.FindControl("tbTeminalCharge")).Text;
                        tbSIFLTotal.Text = ((RadNumericTextBox)Item.FindControl("tbAmtTotal")).Text;

                        tbRate1.Text = Item.GetDataKeyValue("Rate1").ToString();
                        tbRate2.Text = Item.GetDataKeyValue("Rate2").ToString();
                        tbRate3.Text = Item.GetDataKeyValue("Rate3").ToString();

                        EnableSIFLFields(false);

                        btnEdit.Enabled = true;
                        btnReCalcSIFL.Enabled = true;
                        btnDelete.Enabled = true;

                        btnEdit.CssClass = "button";
                        btnReCalcSIFL.CssClass = "button";
                        btnDelete.CssClass = "button";

                        tbAssocPax.Enabled = false;
                        btnAssocPax.Enabled = false;
                        btnAssocPax.CssClass = "browse-button-disabled";


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        #region "Text Changed Events"


        /// <summary>
        /// Method to Set Profile Settings from Company Profile
        /// </summary>
        /// <param name="paxCode">Pass Passenger Requestor Code</param>
        private void SetProfileSettings(string paxCode, string paxId)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxCode, paxId))
            {
                string employeeType = rbEmployeeType.SelectedValue;//retVal[0].IsEmployeeType;
                if (!string.IsNullOrEmpty(employeeType))
                {
                    rbEmployeeType.SelectedValue = employeeType;
                    if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest && string.IsNullOrEmpty(tbAssocPax.Text))
                    {
                        // ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Associated With Passenger Code is Required.', 275, 100,'System Messages'); return false;});", true);
                        RadWindowManager1.RadAlert("Associated With Passenger Code is Required", 360, 50, "System Messages", "AssocPaxAlert", null);
                        //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Associated With Passenger Code is Required');", true);
                        return;
                    }

                    if (Session["SIFLFleetProfile"] != null)
                    {
                        List<GetPOFleetProfile> fleetProfileList = (List<GetPOFleetProfile>)Session["SIFLFleetProfile"];

                        var sifl = new PreflightTripSIFL();
                        sifl.AssociatedPassengerID = null;
                        sifl.PassengerRequestorID = Convert.ToInt64(paxId);

                        if (employeeType == ModuleNameConstants.Preflight.Employee_Type_Controlled)
                        {
                            tbAircraftMultiplier.Text = fleetProfileList[0].SIFLMultiControlled.ToString();
                        }
                        else if (employeeType == ModuleNameConstants.Preflight.Employee_Type_NonControlled)
                        {
                            tbAircraftMultiplier.Text = fleetProfileList[0].SIFLMultiNonControled.ToString();
                        }
                        else
                        {
                            tbAssocPax.Enabled = true;
                            btnAssocPax.Enabled = true;
                            btnAssocPax.CssClass = "browse-button";

                            if (!string.IsNullOrEmpty(hdnAssocPax.Value))
                            {
                                sifl.AssociatedPassengerID = Convert.ToInt64(hdnAssocPax.Value);

                                //using (FlightPakMasterService.MasterCatalogServiceClient masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                                //{
                                //    var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Convert.ToInt64(hdnAssocPax.Value)).EntityList.FirstOrDefault();
                                //    if (associatedPassengers != null)
                                //    {
                                //        tbAircraftMultiplier.Text = associatedPassengers.PassengerRequestorCD;
                                //    }
                                //}
                            }
                        }

                        var aircraftMultiplier = CalculateMultiplierTotal(0, sifl);
                        tbAircraftMultiplier.Text = aircraftMultiplier.ToString();
                    }
                }
                else
                {
                    if (Session["SIFLFleetProfile"] != null)
                    {
                        List<GetPOFleetProfile> fleetProfileList = (List<GetPOFleetProfile>)Session["SIFLFleetProfile"];
                        tbAircraftMultiplier.Text = fleetProfileList[0].SIFLMultiNonControled.ToString();
                    }
                    rbEmployeeType.ClearSelection();
                }

            }
        }

        /// <summary>
        /// Method to Get SIFL Total
        /// </summary>
        protected double CalculateSIFL(bool isCustom)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isCustom))
            {
                // Declarations
                double siflTotal = 0;
                double distanceRate1 = 0;
                double distanceRate2 = 0;
                double distanceRate3 = 0;
                double distancRateTotal = 0;
                double multiplierTotal = 0;
                double terminalCharge = 0;

                if (Session["SIFLRateKey"] != null)
                {
                    double rate1 = 0;
                    double rate2 = 0;
                    double rate3 = 0;

                    List<GetPOSIFLRate> siflRateList = (List<GetPOSIFLRate>)Session["SIFLRateKey"];
                    if (!isCustom)
                    {
                        if (siflRateList.Count > 0)
                        {
                            tbRate1.Text = siflRateList[0].Rate1.ToString();
                            tbRate2.Text = siflRateList[0].Rate2.ToString();
                            tbRate3.Text = siflRateList[0].Rate3.ToString();

                            rate1 = Convert.ToDouble(tbRate1.Text);
                            rate2 = Convert.ToDouble(tbRate2.Text);
                            rate3 = Convert.ToDouble(tbRate3.Text);

                            if (siflRateList[0].TerminalCHG != null)
                                tbTerminalCharge.Text = Math.Round(Convert.ToDouble(siflRateList[0].TerminalCHG.ToString()), 2).ToString();
                            else
                                tbTerminalCharge.Text = "0.0";
                        }
                    }
                    else
                    {
                        if (rate1 != Convert.ToDouble(tbRate1.Text))
                            rate1 = Convert.ToDouble(tbRate1.Text);
                        if (rate2 != Convert.ToDouble(tbRate2.Text))
                            rate2 = Convert.ToDouble(tbRate2.Text);
                        if (rate3 != Convert.ToDouble(tbRate3.Text))
                            rate3 = Convert.ToDouble(tbRate3.Text);

                        if (terminalCharge != Convert.ToDouble(tbTerminalCharge.Text))
                            terminalCharge = Convert.ToDouble(tbTerminalCharge.Text);
                    }

                    if (tbStatueMiles.Text != null && tbStatueMiles.Text != "")
                    {
                        if (Convert.ToDouble(tbStatueMiles.Text) >= 1500)
                        {
                            tbMiles1.Text = "500";
                            tbMiles2.Text = "1000";
                            tbMiles3.Text = (Convert.ToDouble(tbStatueMiles.Text) - 1500).ToString();
                        }
                        else if ((Convert.ToDouble(tbStatueMiles.Text) >= 501) && (Convert.ToDouble(tbStatueMiles.Text) <= 1500))
                        {
                            tbMiles1.Text = "500";
                            tbMiles2.Text = (Convert.ToDouble(tbStatueMiles.Text) - 500).ToString();
                            tbMiles3.Text = "0";
                        }
                        else if ((Convert.ToDouble(tbStatueMiles.Text) > 0) && (Convert.ToDouble(tbStatueMiles.Text) <= 500))
                        {
                            tbMiles1.Text = tbStatueMiles.Text;
                            tbMiles2.Text = "0";
                            tbMiles3.Text = "0";
                        }
                        else
                        {
                            tbMiles1.Text = "0";
                            tbMiles2.Text = "0";
                            tbMiles3.Text = "0";
                        }

                        // Calculate Distance Rate
                        distanceRate1 = (rate1 * Convert.ToDouble(tbMiles1.Text));
                        distanceRate2 = (rate2 * Convert.ToDouble(tbMiles2.Text));
                        distanceRate3 = (rate3 * Convert.ToDouble(tbMiles3.Text));

                        distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;

                        if (tbAircraftMultiplier.Text == string.Empty)
                            tbAircraftMultiplier.Text = "0.0";

                        multiplierTotal = (Convert.ToDouble(tbAircraftMultiplier.Text) / 100);

                        // Calculate SIFL Total
                        siflTotal = Math.Round((distancRateTotal * multiplierTotal) + Convert.ToDouble(tbTerminalCharge.Text), 2);
                    }
                }
                return siflTotal;
            }
        }



        /// <summary>
        /// For calculation on tab change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void StatueMilesChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbSIFLTotal.Text = CalculateSIFL(true).ToString();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbAircraftMultiplier);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }
        protected void AircraftMultiplierChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbSIFLTotal.Text = CalculateSIFL(true).ToString();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMiles1);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }
        protected void Rate1Changed(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbSIFLTotal.Text = CalculateSIFL(true).ToString();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMiles2);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }
        protected void Rate2Changed(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbSIFLTotal.Text = CalculateSIFL(true).ToString();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMiles3);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }
        protected void Rate3Changed(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbSIFLTotal.Text = CalculateSIFL(true).ToString();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTerminalCharge);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }
        protected void TerminalChargeChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbSIFLTotal.Text = CalculateSIFL(true).ToString();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSIFLTotal);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method to Calculate Miles and Statute Miles
        /// </summary>
        /// <param name="departICAOID">Pass Departure ICAO ID</param>
        /// <param name="arriveICAOID">Pass Arrival ICAO ID</param>
        private void CalculateMiles(string departICAOID, string arriveICAOID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(departICAOID, arriveICAOID))
            {
                double miles = 0;
                double stMiles = 1.15078;

                if (!string.IsNullOrEmpty(departICAOID) && !string.IsNullOrEmpty(arriveICAOID))
                {
                    using (CalculationServiceClient CalcService = new CalculationServiceClient())
                    {
                        miles = CalcService.GetDistance(Convert.ToInt64(departICAOID), Convert.ToInt64(arriveICAOID));

                        // Calculate Statute Miles
                        tbStatueMiles.Text = Math.Floor((miles * stMiles)).ToString();

                        // Calculate SIFL Total
                        tbSIFLTotal.Text = CalculateSIFL(false).ToString();
                    }
                }
            }
        }


        protected void hdnControl_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var button = (Button)sender;
                        if (button.AccessKey == "D")
                        {
                            tbDeparts.Text = string.Empty;
                            lbDepart.Text = System.Web.HttpUtility.HtmlEncode(Request.Form["lbDepart"]);
                        }
                        else if (button.AccessKey == "A")
                        {
                            tbArrives.Text = string.Empty;
                            lbArrives.Text = System.Web.HttpUtility.HtmlEncode(Request.Form["lbArrives"]);
                        }
                        else if (button.AccessKey == "P")
                        {
                            tbPax.Text = string.Empty;
                            lbPaxName.Text = System.Web.HttpUtility.HtmlEncode((Request.Form["lbPaxName"]));
                        }
                        else
                        { // Associated pax
                            tbAssocPax.Text = string.Empty;
                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(Request.Form["lbAssocPaxName"]);
                        }

                        return;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        protected void tbPaxTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string id = Page.Request.Params["__EVENTTARGET"];
                        if (!string.IsNullOrEmpty(id) && id.Contains("btnSave"))
                        {
                            return;
                        }

                        if (!string.IsNullOrEmpty(tbPax.Text.Trim()))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(btnPax);
                            string inputPaxName = tbPax.Text.Trim().ToUpper();
                            if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                            {
                                // get passenger by code
                                using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objScheduler.GetPassengerRequestorByPassengerRequestorCD(inputPaxName).EntityList;

                                    if (objRetVal.Count() > 0)
                                    {
                                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                                        var tripPassengers = Trip.PreflightLegs.SelectMany(x => x.PreflightPassengerLists).ToList();
                                        List<PreflightPassengerList> personalTravelPassengers = new List<PreflightPassengerList>();

                                        var tripPassengerIds = tripPassengers.Select(x => x.PassengerID.ToString()).ToList();
                                        long passengerIdByInputCode = objRetVal.First().PassengerRequestorID;
                                        if (tripPassengerIds.Contains(passengerIdByInputCode.ToString()))
                                        {
                                            var tripPassenger = tripPassengers.Where(x => x.PassengerID == passengerIdByInputCode).First();
                                            tbPax.Text = inputPaxName.ToUpper();
                                            hdnPax.Value = System.Web.HttpUtility.HtmlEncode(tripPassenger.PassengerID.ToString());
                                            lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(GetPassengerName(tripPassenger.PassengerLastName, tripPassenger.PassengerFirstName, tripPassenger.PassengerMiddleName));
                                            hdnPaxName.Value = System.Web.HttpUtility.HtmlEncode(GetPassengerName(tripPassenger.PassengerLastName, tripPassenger.PassengerFirstName, tripPassenger.PassengerMiddleName));
                                            rbEmployeeType.SelectedValue = GetEmployeeTypeOfPassenger();

                                            if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                            {
                                                tbAssocPax.Enabled = true;
                                                btnAssocPax.Enabled = true;
                                                btnAssocPax.CssClass = "browse-button";
                                                using (FlightPakMasterService.MasterCatalogServiceClient masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Convert.ToInt64(hdnPax.Value)).EntityList;
                                                    if (associatedPassengers.Count > 0)
                                                    {
                                                        if (!string.IsNullOrEmpty(associatedPassengers[0].AssociatedWithCD))
                                                            tbAssocPax.Text = associatedPassengers[0].AssociatedWithCD.ToString().ToUpper();
                                                        var objRetValue = objScheduler.GetPassengerRequestorByPassengerRequestorCD(tbAssocPax.Text).EntityList;
                                                        if (objRetValue.Count > 0)
                                                        {
                                                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(objRetValue[0].PassengerName.ToString());
                                                            hdnAssocPax.Value = System.Web.HttpUtility.HtmlEncode(objRetValue[0].PassengerRequestorID.ToString());
                                                        }
                                                    }
                                                }
                                            }

                                            if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Controlled || rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_NonControlled)
                                            {
                                                tbAssocPax.Enabled = false;
                                                btnAssocPax.Enabled = false;
                                                btnAssocPax.CssClass = "browse-button-disabled";
                                            }
                                        }
                                        else
                                        {
                                            tbPax.Focus();
                                            hdnPax.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            hdnPaxName.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            tbPax.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            RadWindowManager1.RadAlert("Pax not in Trip", 250, 100, "System Messages", "alertNotPaxFn", null);
                                        }
                                    }
                                    else
                                    {
                                        tbPax.Focus();
                                        hdnPax.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        hdnPaxName.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        tbPax.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        RadWindowManager1.RadAlert("Pax not in Trip", 250, 100, "System Messages", "alertNotPaxFn", null);
                                    }
                                }
                            }
                            else
                            {
                                tbPax.Focus();
                                hdnPax.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                hdnPaxName.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                tbPax.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                RadWindowManager1.RadAlert("Pax not in Trip", 250, 100, "System Messages", "alertNotPaxFn", null);
                            }
                        }
                        else
                        {
                            tbPax.Focus();
                            hdnPax.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnPaxName.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        protected void tbAssocPaxChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string id = Page.Request.Params["__EVENTTARGET"];
                        if (!string.IsNullOrEmpty(id) && id.Contains("btnSave"))
                        {
                            return;
                        }

                        if (!string.IsNullOrEmpty(tbAssocPax.Text.Trim()))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(btnAssocPax);
                            string inputPaxName = tbAssocPax.Text.Trim().ToUpper();

                            using (FlightPakMasterService.MasterCatalogServiceClient objScheduler = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objScheduler.GetPassengerRequestorByPassengerRequestorCD(inputPaxName).EntityList;

                                if (objRetVal.Count() > 0)
                                {
                                    var passenger = objRetVal.First();
                                    tbAssocPax.Text = inputPaxName.ToUpper();
                                    hdnAssocPax.Value = passenger.PassengerRequestorID.ToString();
                                    lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(passenger.PassengerName);

                                    #region

                                    var associatedPassengers = objScheduler.GetPassengerbyPassengerRequestorID(Convert.ToInt64(hdnAssocPax.Value)).EntityList;

                                    if (associatedPassengers.Count > 0)
                                    {
                                        var associatedPassenger = associatedPassengers.First();

                                        string associatedPaxEmployeeType = associatedPassenger.IsEmployeeType;

                                        if (associatedPaxEmployeeType == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                        {
                                            tbAssocPax.Text = string.Empty;
                                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            tbAssocPax.Text = string.Empty;
                                            RadWindowManager1.RadAlert("Invalid Associated With Passenger Code.", 320, 100, "System Messages", "AssocPaxAlert", null);
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    tbAssocPax.Focus();
                                    hdnAssocPax.Value = string.Empty;
                                    lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(hdnAssocPax.Value);
                                    tbAssocPax.Text = string.Empty;
                                    RadWindowManager1.RadAlert("Invalid Associated With Passenger Code.", 320, 100, "System Messages", "AssocPaxAlert", null);
                                }
                            }

                        }
                        else
                        {
                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        private string GetPassengerName(string lastName, string firstName, string middleInitial)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lastName, firstName, middleInitial))
            {
                StringBuilder name = new StringBuilder();
                if (!string.IsNullOrEmpty(lastName))
                {
                    name.Append(lastName);
                    if (lastName.IndexOf(",") == -1 && (!string.IsNullOrEmpty(firstName) || !string.IsNullOrEmpty(middleInitial)))
                    {
                        name.Append(",");
                    }
                }

                if (!string.IsNullOrEmpty(firstName))
                {
                    name.Append(" ");
                    name.Append(firstName);
                    if (firstName.IndexOf(",") == -1 && !string.IsNullOrEmpty(middleInitial))
                    {
                        name.Append(",");
                    }
                }

                if (!string.IsNullOrEmpty(middleInitial))
                {
                    name.Append(" ");
                    name.Append(middleInitial);
                }
                return name.ToString();
            }

        }

        protected void DepartsTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string id = Page.Request.Params["__EVENTTARGET"];
                        if (!string.IsNullOrEmpty(id) && id.Contains("btnSave"))
                        {
                            return;
                        }

                        if (!string.IsNullOrEmpty(tbDeparts.Text.Trim()))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(btnDeparts);
                            string inputDepartAirport = tbDeparts.Text.Trim().ToUpper();
                            if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                            {
                                Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                                var departAirportsOfTrip = Trip.PreflightLegs.Select(x => x.Airport1.IcaoID).ToList();

                                if (departAirportsOfTrip.Count() > 0)
                                {
                                    var airport = Trip.PreflightLegs.Where(x => x.Airport1.IcaoID == inputDepartAirport).Select(x => x.Airport1).FirstOrDefault();
                                    if (airport != null && airport.AirportID != 0)
                                    {
                                        tbDeparts.Text = inputDepartAirport;
                                        hdnDeparts.Value = airport.AirportID.ToString();
                                        lbDepart.Text = System.Web.HttpUtility.HtmlEncode(airport.AirportName);

                                    }
                                    else
                                    {
                                        tbDeparts.Focus();
                                        hdnDeparts.Value = string.Empty;
                                        lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        tbDeparts.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        RadWindowManager1.RadAlert("Depart ICAO not in Trip.", 300, 100, "System Messages", "alertCallDepAirFn", null);
                                        // RadWindowManager1.RadAlert("Depart ICAO not in Trip.", 250, 100, "System Messages", "DepartAlertOk");
                                    }
                                }
                            }
                        }
                        else
                        {
                            tbDeparts.Focus();
                            hdnDeparts.Value = string.Empty;
                            lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            tbDeparts.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            RadWindowManager1.RadAlert("Depart ICAO is required", 300, 100, "System Messages", "alertCallDepAirFn", null);
                            // RadWindowManager1.RadAlert("Depart ICAO is required.", 250, 100, "System Messages", "DepartAlertOk");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        protected void ArrivesTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string id = Page.Request.Params["__EVENTTARGET"];
                        if (!string.IsNullOrEmpty(id) && id.Contains("btnSave"))
                        {
                            return;
                        }

                        if (!string.IsNullOrEmpty(tbArrives.Text.Trim()))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(btnArrives);
                            string inputArrivalAirport = tbArrives.Text.Trim().ToUpper();
                            if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                            {
                                Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                                var arrivalAirportsOfTrip = Trip.PreflightLegs.Select(x => x.Airport.IcaoID).ToList();

                                if (arrivalAirportsOfTrip.Count() > 0)
                                {
                                    var airport = Trip.PreflightLegs.Where(x => x.Airport.IcaoID == inputArrivalAirport).Select(x => x.Airport).FirstOrDefault();
                                    if (airport != null && airport.AirportID != 0)
                                    {
                                        tbArrives.Text = inputArrivalAirport;
                                        hdnArrives.Value = airport.AirportID.ToString();
                                        lbArrives.Text = System.Web.HttpUtility.HtmlEncode(airport.AirportName);
                                        hdnArriveName.Value = airport.AirportName;
                                    }
                                    else
                                    {
                                        tbArrives.Focus();
                                        hdnArrives.Value = string.Empty;
                                        lbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        hdnArriveName.Value = string.Empty;
                                        tbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        RadWindowManager1.RadAlert("Arrival ICAO not in Trip.", 300, 100, "System Messages", "alertCallArrAirFn", null);
                                        // RadWindowManager1.RadAlert("Arrival ICAO not in Trip.", 250, 100, "System Messages", "ArrivalAlertOk");
                                    }
                                }
                            }
                        }
                        else
                        {
                            tbArrives.Focus();
                            hdnArrives.Value = string.Empty;
                            lbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnArriveName.Value = string.Empty;
                            tbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            RadWindowManager1.RadAlert("Arrival ICAO is required.", 300, 100, "System Messages", "alertCallArrAirFn", null);
                            //RadWindowManager1.RadAlert("Arrival ICAO is required.", 250, 100, "System Messages", "ArrivalAlertOk");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method to validate Arrives Airport Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void StartCalculation(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                        {
                            try
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    lbcvArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    if (!string.IsNullOrEmpty(tbArrives.Text.Trim()))
                                    {
                                        // Call Miles Calculation
                                        if (!string.IsNullOrEmpty(hdnDeparts.Value) && !string.IsNullOrEmpty(hdnArrives.Value))
                                        {
                                            CalculateMiles(hdnDeparts.Value, hdnArrives.Value);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArrives.Text = System.Web.HttpUtility.HtmlEncode("Arrival ICAO not in Trip.");
                                        tbArrives.Focus();
                                        return;
                                    }


                                }, FlightPak.Common.Constants.Policy.UILayer);
                            }
                            catch (Exception ex)
                            {
                                //The exception will be handled, logged and replaced by our custom exception. 
                                ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        #endregion

        /// <summary>
        /// Method to clear SIFL Form Fields
        /// </summary>
        protected void ClearSIFLFields(bool clearRateFields)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(clearRateFields))
            {
                rbEmployeeType.ClearSelection();
                tbPax.Text = string.Empty;
                hdnPax.Value = string.Empty;
                lbcvPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbAssocPax.Text = string.Empty;
                hdnAssocPax.Value = string.Empty;
                lbcvAssoc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbDeparts.Text = string.Empty;
                hdnDeparts.Value = string.Empty;
                lbcvDeparts.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbDepart.Text = string.Empty;
                tbArrives.Text = string.Empty;
                hdnArrives.Value = string.Empty;
                lbcvArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                if (clearRateFields)
                {
                    tbRate1.Text = "0.0";
                    tbRate2.Text = "0.0";
                    tbRate3.Text = "0.0";
                    tbTerminalCharge.Text = "0.0";

                }

                tbMiles1.Text = "0.0";
                tbMiles2.Text = "0.0";
                tbMiles3.Text = "0.0";

                tbSIFLTotal.Text = "0.0";
                tbStatueMiles.Text = "0.0";
                tbAircraftMultiplier.Text = "0.0";

            }
        }

        /// <summary>
        /// To Enable or Disable SIFL Form Fields
        /// </summary>
        /// <param name="isenable">Pass Boolean Values</param>
        protected void EnableSIFLFields(bool isenable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isenable))
            {
                tbPax.Enabled = isenable;
                btnPax.Enabled = isenable;
                rbEmployeeType.Enabled = isenable;
                tbAssocPax.Enabled = isenable;
                btnAssocPax.Enabled = isenable;
                tbDeparts.Enabled = isenable;
                btnDeparts.Enabled = isenable;
                tbArrives.Enabled = isenable;
                btnArrives.Enabled = isenable;
                tbStatueMiles.Enabled = isenable;
                tbAircraftMultiplier.Enabled = isenable;
                tbRate1.Enabled = isenable;
                tbRate2.Enabled = isenable;
                tbRate3.Enabled = isenable;
                tbMiles1.Enabled = isenable;
                tbMiles2.Enabled = isenable;
                tbMiles3.Enabled = isenable;
                tbTerminalCharge.Enabled = isenable;
                tbSIFLTotal.Enabled = isenable;


                // Set Browse Button CSS Class
                if (!isenable)
                {
                    btnDeparts.CssClass = "browse-button-disabled";
                    btnArrives.CssClass = "browse-button-disabled";
                    btnPax.CssClass = "browse-button-disabled";
                    btnAssocPax.CssClass = "browse-button-disabled";
                }
                else
                {
                    btnDeparts.CssClass = "browse-button";
                    btnArrives.CssClass = "browse-button";
                    btnPax.CssClass = "browse-button";
                    btnAssocPax.CssClass = "browse-button";
                }
                btnUpdateSIFL.Enabled = isenable;
            }
        }

        /// <summary>
        /// Creates the Calculated SIFL List to saved to the database
        /// </summary>
        protected List<PreflightTripSIFL> CreateSIFLList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<PreflightTripSIFL> SIFLList = new List<PreflightTripSIFL>();
                // Declarations
                decimal siflTotal = 0;
                decimal distanceRate1 = 0;
                decimal distanceRate2 = 0;
                decimal distanceRate3 = 0;
                decimal distancRateTotal = 0;
                decimal multiplierTotal = 0;
                decimal rate1 = 0;
                decimal rate2 = 0;
                decimal rate3 = 0;
                decimal terminalCharge = 0;
                Int64 fareLevelID = 0;
                decimal miles1 = 0;
                decimal miles2 = 0;
                decimal miles3 = 0;
                decimal statueMiles = 0;
                string empType = string.Empty;
                // Enable SIFL Fields
                EnableSIFLFields(false);
                tbAssocPax.Enabled = false;
                btnAssocPax.Enabled = false;
                btnAssocPax.CssClass = "browse-button-disabled";

                // Set SIFL Rate values from Fare Level
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {
                    if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                    {
                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];

                        var ReturnValue = Service.GetAllPreflightTripSIFL(Trip.TripID);
                        if (ReturnValue.ReturnFlag == true)
                        {
                            var siflRateList = ReturnValue.EntityList.Where(s => s.PreflightTripSIFLID == Convert.ToInt64(Session["selectedSIFlID"])).FirstOrDefault();
                            if (siflRateList != null) // EDIT
                            {

                                fareLevelID = Convert.ToInt64(siflRateList.FareLevelID);

                            }
                            else // NEW
                            {
                                if (!string.IsNullOrEmpty(hdnFareLevel.Value))
                                    fareLevelID = Convert.ToInt64(hdnFareLevel.Value);
                            }

                            if (rate1 != Convert.ToDecimal(tbRate1.Text))
                                rate1 = Convert.ToDecimal(tbRate1.Text);
                            if (rate2 != Convert.ToDecimal(tbRate2.Text))
                                rate2 = Convert.ToDecimal(tbRate2.Text);
                            if (rate3 != Convert.ToDecimal(tbRate3.Text))
                                rate3 = Convert.ToDecimal(tbRate3.Text);

                            if (terminalCharge != Convert.ToDecimal(tbTerminalCharge.Text))
                                terminalCharge = Convert.ToDecimal(tbTerminalCharge.Text);
                        }
                    }
                }

                decimal miles = 0;
                if (!string.IsNullOrEmpty(hdnDeparts.Value) && !string.IsNullOrEmpty(hdnArrives.Value))
                {
                    using (CalculationServiceClient CalcService = new CalculationServiceClient())
                    {
                        miles = Convert.ToDecimal(CalcService.GetDistance(Convert.ToInt64(hdnDeparts.Value), Convert.ToInt64(hdnArrives.Value)));
                        // Calculate Statute Miles
                        statueMiles = Math.Floor(miles * (decimal)1.15078);
                    }
                }

                empType = rbEmployeeType.SelectedValue;

                PreflightTripSIFL Sifl = new PreflightTripSIFL();

                // empType = GetEmployeeTypeOfPassenger();

                Sifl.FareLevelID = (long)fareLevelID;
                if (!string.IsNullOrEmpty(hdnPax.Value))
                    Sifl.PassengerRequestorID = Convert.ToInt64(hdnPax.Value);
                Sifl.PassengerName = hdnPaxName.Value;
                Sifl.EmployeeTYPE = empType;

                if (!string.IsNullOrEmpty(hdnPaxLegID.Value))
                {
                    Sifl.LegID = Convert.ToInt64(hdnPaxLegID.Value);
                }
                else
                {
                    Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                    var passengersInTrip = Trip.PreflightLegs.SelectMany(x => x.PreflightPassengerLists.Where(y => y.PassengerID == Sifl.PassengerRequestorID)).ToList();
                    if (passengersInTrip.Count > 0)
                        hdnPaxLegID.Value = passengersInTrip.Select(x => x.LegID.ToString()).First(); // if manually added sifl, get passenger's first leg id
                    Sifl.PassengerName = System.Web.HttpUtility.HtmlDecode(lbPaxName.Text);

                    Sifl.LegID = Convert.ToInt64(hdnPaxLegID.Value);
                }

                Sifl.PassengerName = Sifl.PassengerName.Length > 63 ? Sifl.PassengerName.Replace(" ", string.Empty) : Sifl.PassengerName; //dummy fix
                Sifl.AssociatedPassengerID = null;
                if (empType == ModuleNameConstants.Preflight.Employee_Type_Guest)
                {
                    if (!string.IsNullOrEmpty(hdnAssocPax.Value))
                    {
                        Sifl.AssociatedPassengerID = Convert.ToInt64(hdnAssocPax.Value);
                    }
                }

                #region Code Change to Get Emptype of AssociatePassenger
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {
                    if (empType == ModuleNameConstants.Preflight.Employee_Type_Guest)
                    {
                        if (Sifl.AssociatedPassengerID != null && Sifl.AssociatedPassengerID > 0)
                        {
                            var AssociatePassretValue = Service.GetSIFLPassengerEmployeeType(Sifl.AssociatedPassengerID.Value);

                            if (AssociatePassretValue.ReturnFlag == true && AssociatePassretValue.EntityList.Count > 0)
                            {
                                foreach (GetSIFLPassengerEmployeeType AssociatePassresultList in AssociatePassretValue.EntityList)
                                {
                                    if (AssociatePassresultList.IsEmployeeType == null)
                                        empType = ModuleNameConstants.Preflight.Employee_Type_NonControlled;
                                    else
                                        empType = AssociatePassresultList.IsEmployeeType;

                                }
                            }
                        }
                    }
                }
                #endregion

                if (!string.IsNullOrEmpty(hdnDeparts.Value))
                {
                    Sifl.DepartICAOID = Convert.ToInt64(hdnDeparts.Value);
                }

                if (!string.IsNullOrEmpty(hdnArrives.Value))
                {
                    Sifl.ArriveICAOID = Convert.ToInt64(hdnArrives.Value);
                }
                //  Sifl.Distance = Math.Floor(distancRateTotal);

                List<PreflightSIFLLegInfo> LegInfolList = RetainLegInfoGrid(dgSIFLPersonal);


                //List<PreflightSIFLLegInfo> AssignedLegInfo = LegInfolList.Where(x => x.LegID == Convert.ToInt64(hdnPaxLegID.Value)).ToList();
                //if (AssignedLegInfo.Count > 0)
                //    Sifl.LoadFactor = Convert.ToDecimal(AssignedLegInfo[0].LoadFactor);
                //else
                Sifl.LoadFactor = Convert.ToDecimal(0.0); // for manual entry, always load factor will be 0

                if (Session["SIFLFleetProfile"] != null)
                {
                    List<GetPOFleetProfile> fleetProfileList = (List<GetPOFleetProfile>)Session["SIFLFleetProfile"];
                    if (empType == ModuleNameConstants.Preflight.Employee_Type_Controlled)
                        multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiControlled;
                    else if (empType == ModuleNameConstants.Preflight.Employee_Type_NonControlled)
                        multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;


                    //#region "Secured"

                    //using (FlightPakMasterService.MasterCatalogServiceClient masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                    //{
                    //    var passengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Sifl.PassengerRequestorID.Value).EntityList;
                    //    if (passengers.Count > 0)
                    //    {
                    //        if (passengers.First().IsSIFL == true) // if passenger is SIFL secured, get MultiSec for multiplier, else check for Associated PAX
                    //        {
                    //            multiplierTotal = (decimal)fleetProfileList[0].MultiSec;
                    //        }
                    //        else
                    //        {
                    //            if (Sifl.AssociatedPassengerID != null)
                    //            {
                    //                var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Sifl.AssociatedPassengerID.Value).EntityList;
                    //                if (associatedPassengers.Count > 0)
                    //                {
                    //                    if (associatedPassengers.First().IsSIFL == true)
                    //                        multiplierTotal = (decimal)fleetProfileList[0].MultiSec;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    //#endregion

                    // if PassengerRequestorID or associatedPassengerID is ISSIFL, then multiplierTotal = MultiSec...
                    multiplierTotal = CalculateMultiplierTotal(multiplierTotal, Sifl);
                }

                if (statueMiles != Convert.ToDecimal(tbStatueMiles.Text))
                {
                    statueMiles = Convert.ToDecimal(tbStatueMiles.Text);
                }
                miles1 = 0;
                miles2 = 0;
                miles3 = 0;
                if (statueMiles > 0)
                {

                    if (statueMiles >= 1500)
                    {
                        miles1 = 500;
                        miles2 = 1000;
                        miles3 = (statueMiles - 1500);
                    }
                    else if ((statueMiles >= 501) && (statueMiles <= 1500))
                    {
                        miles1 = 500;
                        miles2 = (statueMiles - 500);
                    }
                    else if ((statueMiles > 0) && (statueMiles <= 500))
                        miles1 = (statueMiles);
                    // Calculate Distance Rate
                    distanceRate1 = (rate1 * Convert.ToDecimal(miles1));
                    distanceRate2 = (rate2 * Convert.ToDecimal(miles2));
                    distanceRate3 = (rate3 * Convert.ToDecimal(miles3));
                    distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;

                    if (multiplierTotal != Convert.ToDecimal(tbAircraftMultiplier.Text))
                    {
                        multiplierTotal = Convert.ToDecimal(tbAircraftMultiplier.Text);
                    }
                    Sifl.AircraftMultiplier = multiplierTotal;



                    multiplierTotal = (multiplierTotal / 100);
                    // Calculate SIFL Total
                    siflTotal = Math.Round(((distancRateTotal * multiplierTotal) + terminalCharge), 2);
                }


                Sifl.Rate1 = rate1;
                Sifl.Rate2 = rate2;
                Sifl.Rate3 = rate3;
                Sifl.Distance1 = miles1;
                Sifl.Distance2 = miles2;
                Sifl.Distance3 = miles3;
                Sifl.Distance = Sifl.Distance1 + Sifl.Distance2 + Sifl.Distance3;
                Sifl.TeminalCharge = terminalCharge;
                Sifl.AmtTotal = Math.Round(siflTotal, 2);
                SIFLList.Add(Sifl);

                return SIFLList;
            }
        }

        private decimal CalculateMultiplierTotal(decimal multiplierTotal, PreflightTripSIFL Sifl)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(multiplierTotal, Sifl))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (Session["SIFLFleetProfile"] != null)
                    {
                        List<GetPOFleetProfile> fleetProfileList = (List<GetPOFleetProfile>)Session["SIFLFleetProfile"];

                        var passengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Sifl.PassengerRequestorID.Value).EntityList;

                        if (passengers.Count > 0)
                        {
                            if (Sifl.AssociatedPassengerID != null && passengers.First().IsSIFL != true)
                            {
                                var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Sifl.AssociatedPassengerID.Value).EntityList;
                                if (associatedPassengers.Count > 0)
                                {
                                    var associatedPassenger = associatedPassengers.First();

                                    // if PAX is Guest, look for AssocPax empType for mulitplier? - Ans: Yes
                                    if (passengers.First().IsEmployeeType == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                    {
                                        string associatedPaxEmployeeType = associatedPassenger.IsEmployeeType;

                                        if (ModuleNameConstants.Preflight.Employee_Type_Controlled == associatedPaxEmployeeType)
                                        {
                                            multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiControlled;
                                        }
                                        else if (ModuleNameConstants.Preflight.Employee_Type_NonControlled == associatedPaxEmployeeType)
                                        {
                                            multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;
                                        }
                                        else
                                        {
                                            multiplierTotal = 0; //assocPax is also guest...
                                        }
                                    }

                                    if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                    {
                                        string associatedPaxEmployeeType = associatedPassenger.IsEmployeeType;

                                        if (ModuleNameConstants.Preflight.Employee_Type_Controlled == associatedPaxEmployeeType)
                                        {
                                            multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiControlled;
                                        }
                                        else if (ModuleNameConstants.Preflight.Employee_Type_NonControlled == associatedPaxEmployeeType)
                                        {
                                            multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;
                                        }
                                        else
                                        {
                                            multiplierTotal = 0; //assocPax is also guest...
                                        }
                                    }

                                    //Clarification - if PAX is not sifl, look whether assocPax is SIFL ? Ans:- No
                                    if (associatedPassenger.IsSIFL == true)
                                    {
                                        multiplierTotal = (decimal)fleetProfileList[0].MultiSec;
                                    }

                                }
                            }

                            else if (passengers.First().IsSIFL == true) // if passenger is SIFL secured, get MultiSec for multiplier
                            {
                                multiplierTotal = (decimal)fleetProfileList[0].MultiSec;
                            }
                            else
                            {
                                if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Controlled)
                                {
                                    multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiControlled;
                                }
                                else if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_NonControlled)
                                {
                                    multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;
                                }
                                else if (rbEmployeeType.SelectedValue == ModuleNameConstants.Preflight.Employee_Type_Guest)
                                {
                                    if (!string.IsNullOrEmpty(hdnAssocPax.Value))
                                    {
                                        var associatedPassengers = masterServiceClient.GetPassengerbyPassengerRequestorID(Convert.ToInt64(hdnAssocPax.Value)).EntityList;
                                        if (associatedPassengers.Count > 0)
                                        {
                                            var associatedPassenger = associatedPassengers.First();

                                            string associatedPaxEmployeeType = associatedPassenger.IsEmployeeType;

                                            if (ModuleNameConstants.Preflight.Employee_Type_Controlled == associatedPaxEmployeeType)
                                            {
                                                multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiControlled;
                                            }
                                            else if (ModuleNameConstants.Preflight.Employee_Type_NonControlled == associatedPaxEmployeeType)
                                            {
                                                multiplierTotal = (decimal)fleetProfileList[0].SIFLMultiNonControled;
                                            }
                                            else
                                            {
                                                multiplierTotal = 0; //assocPax is also guest...
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return multiplierTotal;
            }
        }

        private string GetEmployeeTypeOfPassenger()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string empType = null;

                using (PreflightService.PreflightServiceClient objService = new PreflightServiceClient())
                {
                    var retValue = objService.GetSIFLPassengerEmployeeType(Convert.ToInt64(hdnPax.Value));
                    if (retValue.ReturnFlag == true && retValue.EntityList.Count > 0)
                    {
                        foreach (GetSIFLPassengerEmployeeType result in retValue.EntityList)
                        {
                            if (result.IsEmployeeType == null)
                                empType = ModuleNameConstants.Preflight.Employee_Type_NonControlled;
                            else
                                empType = result.IsEmployeeType;
                        }
                    }
                    return empType;
                }
            }
        }

        /// <summary>
        /// Save to calculation to database
        /// </summary>
        protected void SaveSIFL(List<PreflightTripSIFL> SIFList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SIFList))
            {
                foreach (PreflightTripSIFL SIFL in SIFList)
                {
                    using (PreflightServiceClient service = new PreflightServiceClient())
                    {
                        Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                        var RetTrip = service.GetHistory(Convert.ToInt64(Trip.CustomerID), Convert.ToInt64(Trip.TripID));
                        StringBuilder HistoryDescription = new System.Text.StringBuilder();
                        string OldDescription = string.Empty;
                        PreflightTripHistory TripHistory = new PreflightTripHistory();

                        var RetValue = service.CheckPreflightTripSIFL(Convert.ToInt64(SIFL.PassengerRequestorID), SIFL.DepartICAOID.Value, SIFL.ArriveICAOID.Value);

                        if (RetValue.ReturnFlag == true && RetValue.EntityList.Count > 0)
                        {
                            SIFL.State = PreflightService.TripEntityState.Modified;
                            SIFL.PreflightTripSIFLID = RetValue.EntityList[0].PreflightTripSIFLID;
                            if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                            {
                                TripHistory.TripHistoryID = RetTrip.EntityList[0].TripHistoryID;
                                TripHistory.TripID = RetTrip.EntityList[0].TripID;
                                TripHistory.TripID = RetTrip.EntityList[0].CustomerID;
                                TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                TripHistory.LastUpdTS = DateTime.UtcNow;
                                TripHistory.RevisionNumber = RetTrip.EntityList[0].RevisionNumber;
                                HistoryDescription.AppendLine(string.Format("SIFL is recalculated from {0} for Passenger {1}", SIFL.AmtTotal, SIFL.PassengerName));
                                OldDescription = RetTrip.EntityList[0].HistoryDescription;
                                TripHistory.HistoryDescription = OldDescription + HistoryDescription.ToString();

                            }
                        }
                        else
                        {
                            SIFL.State = PreflightService.TripEntityState.Added;
                            var NewValue = service.GetPreflightTripSIFLID();
                            if (NewValue.ReturnFlag == true && NewValue.EntityList.Count > 0)
                            {
                                SIFL.PreflightTripSIFLID = Convert.ToInt64(NewValue.EntityList[0].PreflightTripSIFLID);

                                if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                {
                                    TripHistory.TripHistoryID = RetTrip.EntityList[0].TripHistoryID;
                                    TripHistory.TripID = RetTrip.EntityList[0].TripID;
                                    TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                    TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                    TripHistory.LastUpdTS = DateTime.UtcNow;
                                    TripHistory.RevisionNumber = RetTrip.EntityList[0].RevisionNumber;
                                    HistoryDescription.AppendLine(string.Format("SIFL is recalculated from {0} for Passenger {1}", SIFL.AmtTotal, SIFL.PassengerName));
                                    OldDescription = RetTrip.EntityList[0].HistoryDescription;
                                    TripHistory.HistoryDescription = OldDescription + HistoryDescription.ToString();
                                }
                            }
                        }


                        var ReturnValue = service.AddPreflightTripSIFL(SIFL);
                        var ReturnHistory = service.UpdatePreflightTripHistory(TripHistory);
                        //if (ReturnValue.ReturnFlag == true && ReturnValue.EntityList.Count > 0)
                        //{
                        //    LoadSifl();
                        //}

                    }
                }

                LoadSifl();
            }
        }

        /// <summary>
        /// Method to Retain SIFL Personal Grid
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        private List<PreflightSIFLLegInfo> RetainLegInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<PreflightSIFLLegInfo> LegInfoList = new List<PreflightSIFLLegInfo>();

                foreach (GridDataItem Item in dgSIFLPersonal.MasterTableView.Items)
                {
                    PreflightSIFLLegInfo LegInfo = new PreflightSIFLLegInfo();
                    if (Item.GetDataKeyValue("LegID") != null)
                        LegInfo.LegID = Convert.ToInt64(Item.GetDataKeyValue("LegID").ToString());
                    LegInfo.Depart = ((Label)Item["Depart"].FindControl("lbDepart")).Text;
                    LegInfo.Arrive = ((Label)Item["Arrive"].FindControl("lbArrive")).Text;
                    LegInfo.Personal = ((Label)Item["Personal"].FindControl("lbPersonal")).Text;
                    LegInfo.LoadFactor = ((Label)Item["LoadFactor"].FindControl("lbLoadFactor")).Text;
                    LegInfoList.Add(LegInfo);
                }
                return LegInfoList;
            }
        }

        /// <summary>
        /// Hnadles Employee Type Change Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Alert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {   //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ViewState["AlertType"] != null)
                        {
                            switch (ViewState["AlertType"].ToString())
                            {
                                case "EmpType_Changed":
                                    tbAssocPax.Enabled = true;
                                    btnAssocPax.Enabled = true;
                                    tbAssocPax.Focus();
                                    btnAssocPax.CssClass = "browse-button";
                                    break;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// Method to populate at PageLoad if there are existing records
        /// </summary>
        private void LoadSifl()
        {
            decimal AmtTotal = 0;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                {
                    Trip = (FlightPak.Web.PreflightService.PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];

                    using (PreflightServiceClient Service = new PreflightServiceClient())
                    {
                        var ReturnValue = Service.GetAllPreflightTripSIFL(Trip.TripID);

                        string PaxCode = "";
                        string AssociatedPaxCode = "";
                        string DepICAIO = "";
                        string ArrICAIO = "";

                        if (ReturnValue.ReturnFlag == true && ReturnValue.EntityList.Count > 0)
                        {
                            Session["PreflightSiflList"] = ReturnValue.EntityList;
                            List<PreflightTripSIFL> PreflightSIFLList = new List<PreflightTripSIFL>();

                            foreach (GetAllPreflightTripSIFL SIFL in ReturnValue.EntityList)
                            {
                                PreflightTripSIFL PreflightSIFL = new PreflightTripSIFL();

                                PreflightSIFL.PreflightTripSIFLID = SIFL.PreflightTripSIFLID;
                                PreflightSIFL.LegID = SIFL.LegID;
                                PreflightSIFL.PassengerRequestorID = SIFL.PassengerRequestorID;
                                hdnPax.Value = SIFL.PassengerRequestorID.ToString();
                                PreflightSIFL.PassengerName = SIFL.PassengerName;
                                var retValue = Service.GetSIFLPassengerEmployeeType(Convert.ToInt64(PreflightSIFL.PassengerRequestorID));
                                if (retValue.ReturnFlag == true && retValue.EntityList.Count > 0)
                                {
                                    foreach (GetSIFLPassengerEmployeeType result in retValue.EntityList)
                                    {
                                        PaxCode = result.PassengerRequestorCD;
                                    }
                                }
                                PreflightService.Passenger paxObj = new PreflightService.Passenger() { PassengerRequestorCD = PaxCode, PassengerRequestorID = Convert.ToInt64(PreflightSIFL.PassengerRequestorID) };
                                PreflightSIFL.Passenger1 = paxObj;

                                if (SIFL.AssociatedPassengerID != null)
                                {
                                    hdnAssocPax.Value = SIFL.AssociatedPassengerID.ToString();
                                    var AssociatedRetValue = Service.GetSIFLPassengerEmployeeType(Convert.ToInt64(SIFL.AssociatedPassengerID));
                                    if (AssociatedRetValue.ReturnFlag == true && AssociatedRetValue.EntityList.Count > 0)
                                    {
                                        foreach (GetSIFLPassengerEmployeeType result in AssociatedRetValue.EntityList)
                                        {
                                            AssociatedPaxCode = result.PassengerRequestorCD;
                                        }
                                    }
                                }
                                PreflightService.Passenger assocPaxObj = new PreflightService.Passenger() { PassengerRequestorCD = AssociatedPaxCode, PassengerRequestorID = Convert.ToInt64(SIFL.AssociatedPassengerID) };
                                PreflightSIFL.Passenger = assocPaxObj;
                                PreflightSIFL.AssociatedPassengerID = SIFL.AssociatedPassengerID;

                                using (var masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var departureAirports = masterServiceClient.GetAirportByAirportID(SIFL.DepartICAOID.Value).EntityList;
                                    if (departureAirports.Count > 0 && departureAirports.First() != null)
                                    {
                                        PreflightService.Airport departObj = new PreflightService.Airport() { IcaoID = departureAirports.First().IcaoID, AirportName = departureAirports.First().AirportName };
                                        PreflightSIFL.Airport1 = departObj;
                                        PreflightSIFL.DepartICAOID = SIFL.DepartICAOID;
                                        DepICAIO = departureAirports.First().IcaoID;
                                        hdnDeparts.Value = SIFL.DepartICAOID.ToString();
                                    }


                                    var arrivalAirports = masterServiceClient.GetAirportByAirportID(SIFL.ArriveICAOID.Value).EntityList;
                                    if (arrivalAirports.Count > 0 && arrivalAirports.First() != null)
                                    {
                                        PreflightService.Airport arriveObj = new PreflightService.Airport() { IcaoID = arrivalAirports.First().IcaoID, AirportName = arrivalAirports.First().AirportName };
                                        PreflightSIFL.Airport = arriveObj;
                                        PreflightSIFL.ArriveICAOID = SIFL.ArriveICAOID;
                                        ArrICAIO = arrivalAirports.First().IcaoID;
                                        hdnArrives.Value = SIFL.ArriveICAOID.ToString();
                                    }
                                }

                                PreflightSIFL.EmployeeTYPE = SIFL.EmployeeType;
                                PreflightSIFL.Distance = SIFL.Distance;
                                PreflightSIFL.AircraftMultiplier = SIFL.AircraftMultiplier;
                                PreflightSIFL.Distance1 = SIFL.Distance1;
                                PreflightSIFL.Distance2 = SIFL.Distance2;
                                PreflightSIFL.Distance3 = SIFL.Distance3;
                                PreflightSIFL.Rate1 = SIFL.Rate1;
                                PreflightSIFL.Rate2 = SIFL.Rate2;
                                PreflightSIFL.Rate3 = SIFL.Rate3;
                                PreflightSIFL.TeminalCharge = SIFL.TerminalCharge;
                                PreflightSIFL.AmtTotal = SIFL.AmtTotal;
                                PreflightSIFL.LoadFactor = SIFL.LoadFator;
                                PreflightSIFLList.Add(PreflightSIFL);
                                AmtTotal = Convert.ToDecimal(AmtTotal + SIFL.AmtTotal);
                            }

                            dgSifl.DataSource = PreflightSIFLList.OrderBy(x => x.Passenger1.PassengerRequestorCD);
                            dgSifl.DataBind();
                            Session["PreflightSiflData"] = PreflightSIFLList;

                            var retPaxValue = Service.GetSIFLPassengerEmployeeType(Convert.ToInt64(ReturnValue.EntityList[0].PassengerRequestorID));
                            if (retPaxValue.ReturnFlag == true && retPaxValue.EntityList.Count > 0)
                            {
                                foreach (GetSIFLPassengerEmployeeType result in retPaxValue.EntityList)
                                {
                                    tbPax.Text = result.PassengerRequestorCD;
                                    tbPax.Enabled = false;
                                    btnPax.Enabled = false;
                                    btnPax.CssClass = "browse-button-disabled";
                                    lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(ReturnValue.EntityList[0].PassengerName);
                                }
                            }

                            if (ReturnValue.EntityList[0].AssociatedPassengerID != null)
                            {
                                var AssociatedPaxValue = Service.GetSIFLPassengerEmployeeType(Convert.ToInt64(ReturnValue.EntityList[0].AssociatedPassengerID));

                                if (AssociatedPaxValue.ReturnFlag == true && AssociatedPaxValue.EntityList.Count > 0)
                                {
                                    foreach (GetSIFLPassengerEmployeeType result in AssociatedPaxValue.EntityList)
                                    {
                                        tbAssocPax.Text = result.PassengerRequestorCD;
                                        hdnAssocPax.Value = ReturnValue.EntityList[0].AssociatedPassengerID.ToString();

                                        using (var masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var associatePax = masterServiceClient.GetPassengerRequestorByPassengerRequestorCD(result.PassengerRequestorCD).EntityList;

                                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(associatePax.Count > 0 ? associatePax.First().PassengerName : string.Empty);
                                        }
                                    }
                                }
                            }

                            tbAssocPax.Enabled = false;
                            btnAssocPax.Enabled = false;
                            btnAssocPax.CssClass = "browse-button-disabled";

                            //using (var masterServiceClient = new FlightPakMasterService.MasterCatalogServiceClient())
                            //{
                            //    var departureAirports = masterServiceClient.GetAirportByAirportID(ReturnValue.EntityList[0].DepartICAOID.Value).EntityList;
                            //    if (departureAirports.Count > 0 && departureAirports.First() != null)
                            //    {
                            //        PreflightService.Airport departObj = new PreflightService.Airport() { IcaoID = departureAirports.First().IcaoID, AirportName = departureAirports.First().AirportName };
                            //        tbDeparts.Text = departObj.IcaoID;
                            //        lbDepart.Text = departObj.AirportName;

                            //        tbDeparts.Enabled = false;
                            //        btnDeparts.Enabled = false;
                            //        btnDeparts.CssClass = "browse-button-disabled";
                            //    }


                            //    var arrivalAirports = masterServiceClient.GetAirportByAirportID(ReturnValue.EntityList[0].ArriveICAOID.Value).EntityList;
                            //    if (arrivalAirports.Count > 0 && arrivalAirports.First() != null)
                            //    {
                            //        PreflightService.Airport arriveObj = new PreflightService.Airport() { IcaoID = arrivalAirports.First().IcaoID, AirportName = arrivalAirports.First().AirportName };
                            //        tbArrives.Text = arriveObj.IcaoID;
                            //        lbArrives.Text = arriveObj.AirportName;

                            //        tbArrives.Enabled = false;
                            //        btnArrives.Enabled = false;
                            //        btnArrives.CssClass = "browse-button-disabled";
                            //    }
                            //}


                            tbDeparts.Text = PreflightSIFLList[0].Airport1.IcaoID;
                            lbDepart.Text = System.Web.HttpUtility.HtmlEncode(PreflightSIFLList[0].Airport1.AirportName);

                            tbDeparts.Enabled = false;
                            btnDeparts.Enabled = false;
                            btnDeparts.CssClass = "browse-button-disabled";

                            tbArrives.Text = PreflightSIFLList[0].Airport.IcaoID;
                            lbArrives.Text = System.Web.HttpUtility.HtmlEncode(PreflightSIFLList[0].Airport.AirportName);

                            tbArrives.Enabled = false;
                            btnArrives.Enabled = false;
                            btnArrives.CssClass = "browse-button-disabled";


                            rbEmployeeType.SelectedValue = ReturnValue.EntityList[0].EmployeeType.Trim();
                            rbEmployeeType.Enabled = false;

                            tbStatueMiles.Text = ReturnValue.EntityList[0].Distance.ToString();
                            tbStatueMiles.Enabled = false;

                            tbAircraftMultiplier.Text = ReturnValue.EntityList[0].AircraftMultiplier.ToString();
                            tbAircraftMultiplier.Enabled = false;

                            tbMiles1.Text = ReturnValue.EntityList[0].Distance1.ToString();
                            tbMiles1.Enabled = false;

                            tbMiles2.Text = ReturnValue.EntityList[0].Distance2.ToString();
                            tbMiles2.Enabled = false;

                            tbMiles3.Text = ReturnValue.EntityList[0].Distance3.ToString();
                            tbMiles3.Enabled = false;

                            tbRate1.Text = ReturnValue.EntityList[0].Rate1.ToString();
                            tbRate1.Enabled = false;

                            tbRate2.Text = ReturnValue.EntityList[0].Rate2.ToString();
                            tbRate2.Enabled = false;

                            tbRate3.Text = ReturnValue.EntityList[0].Rate3.ToString();
                            tbRate3.Enabled = false;

                            tbTerminalCharge.Text = ReturnValue.EntityList[0].TerminalCharge.ToString();
                            tbTerminalCharge.Enabled = false;

                            tbSIFLTotal.Text = ReturnValue.EntityList[0].AmtTotal.ToString();
                            tbSIFLTotal.Enabled = false;

                            tbGrandTotal.Text = AmtTotal.ToString();

                            btnUpdateSIFL.Enabled = false;
                            btnEdit.Enabled = false;
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDelete.Enabled = false;
                            btnReCalcSIFL.Enabled = true;

                            btnUpdateSIFL.CssClass = "button-disable";
                            btnEdit.CssClass = "button-disable";
                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDelete.CssClass = "button-disable";
                            btnReCalcSIFL.CssClass = "button";
                        }
                        else
                        {
                            EnableSIFLFields(false);
                            btnReCalcSIFL.Enabled = true;
                            btnDelete.Enabled = false;
                            btnEdit.Enabled = false;
                            btnCancel.Enabled = false;
                            btnSave.Enabled = false;

                            btnReCalcSIFL.CssClass = "button";
                            btnDelete.CssClass = "button-disable";
                            btnEdit.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnSave.CssClass = "button-disable";
                            btnUpdateSIFL.CssClass = "button-disable";

                            lbArrives.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbAssocPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            tbRate1.Text = string.Empty;
                            tbRate2.Text = string.Empty;
                            tbRate3.Text = string.Empty;
                            tbTerminalCharge.Text = string.Empty;

                            tbPax.Text = string.Empty;
                            hdnPaxName.Value = string.Empty;
                            tbAssocPax.Text = string.Empty;
                            tbDeparts.Text = string.Empty;
                            tbArrives.Text = string.Empty;
                            tbGrandTotal.Text = string.Empty;
                            //tbStatueMiles.Text = string.Empty;
                            //tbAircraftMultiplier.Text = string.Empty;
                            //tbSIFLTotal.Text = string.Empty;
                            //tbMiles1.Text = string.Empty;
                            //tbMiles2.Text = string.Empty;
                            //tbMiles3.Text = string.Empty;
                            Session.Remove("PreflightSiflData");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Class added for SIFL LegInfo Grid
        /// </summary>
        [Serializable()]
        public class PreflightSIFLLegInfo
        {
            public long LegID { get; set; }
            public string Depart { get; set; }
            public string Arrive { get; set; }
            public string Personal { get; set; }
            public string LoadFactor { get; set; }
            public long LegNum { get; set; }
        }

        protected void btnCancelYes_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void Sifl_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["PreflightSiflData"] != null)
                        {
                            List<PreflightTripSIFL> siflList = (List<PreflightTripSIFL>)Session["PreflightSiflData"];
                            dgSifl.DataSource = siflList.OrderBy(x => x.Passenger1.PassengerRequestorCD);
                            dgSifl.DataBind();
                            dgSifl.ClientSettings.Scrolling.ScrollTop = "0";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        /// <summary>
        /// To highlight the selected item which they have selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgSifl.MasterTableView.Items.Count > 0)
                {
                    dgSifl.SelectedIndexes.Add(0);

                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }

        public double CalculateDifference(PreflightMain Trip, int LegNumber)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip, LegNumber))
            {
                double LegDifference = 0;
                PreflightMain ValidTrip = Trip;

                List<PreflightLeg> LegOfPostflightList = new List<PreflightLeg>();
                PreflightLeg LegOfPostflight = new PreflightLeg();
                LegOfPostflightList = Trip.PreflightLegs.OrderBy(x => x.LegNUM).ToList();
                if (ValidTrip.PreflightLegs.Count > (LegNumber))
                {
                    PreflightLeg currentLeg = LegOfPostflightList[LegNumber - 1];
                    PreflightLeg NextLeg = LegOfPostflightList[LegNumber];

                    DateTime InBoundValue = DateTime.MinValue;
                    DateTime OutBoundValue = DateTime.MinValue;

                    if (currentLeg.ArrivalDTTMLocal != null)
                    {
                        InBoundValue = Convert.ToDateTime(currentLeg.ArrivalDTTMLocal);
                    }
                    if (currentLeg.DepartureDTTMLocal != null)
                    {
                        OutBoundValue = Convert.ToDateTime(NextLeg.DepartureDTTMLocal);

                    }
                    if (InBoundValue != DateTime.MinValue && OutBoundValue != DateTime.MinValue)
                    {
                        if (OutBoundValue > InBoundValue)
                        {
                            TimeSpan DutyDifference = OutBoundValue.Subtract(InBoundValue);
                            LegDifference = Convert.ToDouble(DutyDifference.TotalHours);
                        }
                        else
                        {
                            LegDifference = 0;
                        }
                    }
                    else
                    {
                        LegDifference = 0;
                    }
                }
                else
                {
                    LegDifference = -1;
                }

                return LegDifference;
            }
        }
    }
}