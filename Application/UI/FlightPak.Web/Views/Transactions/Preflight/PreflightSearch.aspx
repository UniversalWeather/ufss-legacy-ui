﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightSearch.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 8 ]> <html lang="en" style="background:#fff;" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" style="background:#fff;" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" style="background:#fff;"  class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" style="background:#fff;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <title>Preflight Search</title>
    <link href="/Scripts/jqgrid/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <style type="text/css">
        .ui-jqgrid .ui-jqgrid-htable th div {
            height: auto !important;
            overflow: hidden;
            padding-right: 4px;
            padding-top: 2px;
            position: relative;
            vertical-align: text-top;
            white-space: normal !important;
        }
       .HideScorll
            {
                overflow-x : hidden !important;
            }

       #btnSubmit, .button{padding:5px 16px!important;}


    </style>
</head>
<body>
<script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.js"></script>
<script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/Scripts/Common.js"></script>
<script type="text/javascript" src="/Scripts/custom-grid-scroll.min.js"></script>
        
<script type="text/javascript" src="/Scripts/knockout/moment.min.js"></script>
        <script type="text/javascript">
            
            var selectedRowData = null;
            var jqgridTableId = '#gridAllTrips';
            var hdnPageRequested = getQuerystring("FromPage", "");
            var hdClientCodeID ="";
            var hdIsTripPrivacy="";
            var hdHomebaseID ="";
            var hdHomeBase = "";
            var MultiSelectFlag = false;
            var AppdateFormat="DD/MM/YYYY";
            var Identity = null;
            var isodateformat = 'YYYY-MM-DD';
            if (hdnPageRequested == "1")
            { MultiSelectFlag = true; }

            if (hdHomeBase.indexOf(",") > 0) {
                document.getElementById("lbHomeBase").innerHTML = "(Multiple)";
                document.getElementById("lbHomeBase").style.color = "red";
            }
            
            $(document).ready(function () {

                UserIdentity(PopupLoad);
                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page", rows: "size", order: "dir", sort: "sort"
                    }
                });
                $("#btnSubmit").click(function () {
                    var selectedrows = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                    if (selectedrows) {
                        var rowData = $(jqgridTableId).getRowData(selectedrows);
                        var lastSel = rowData['TripID'];//replace name with any column
                        var TripNUM = rowData['TripNUM'];
                        $("#hdSelectedTripID").val(lastSel);
                        $("#hdSelectedTripNUM").val(TripNUM);
                        returnToParent();
                    }
                    else { showMessageBox('Please select a trip.', popupTitle); }
                    return false;
                });
            });

            function PopupLoad(usrprencipal) {
                Identity=usrprencipal;
                hdIsTripPrivacy = "false";
                if (!Identity._isSysAdmin) {
                    if (Identity._isTripPrivacy) {
                        hdIsTripPrivacy = "true";
                    }
                }

                if (Identity != null) {
                    hdHomebaseID = Identity._homeBaseId;
                }

                if (Identity._clientId == null)
                    $("#btnClientCode").prop('disabled', false);
                else
                    $("#btnClientCode").prop('disabled', true);
                if (Identity._clientId != null) {
                    hdClientCodeID = Identity._clientId;
                }

                if(Identity._fpSettings._IsDeactivateHomeBaseFilter==false) {
                    $('#chkHomebase').prop('checked',true);
                }
            }


            $(document).ready(function () {
                AppdateFormat = Identity._fpSettings._ApplicationDateFormat;
                $("#tbDepDate").datepicker({
                    changeMonth:true,
                    changeYear:true,
                    dateFormat: AppdateFormat.toLowerCase().replace(/yyyy/g, "yy")
                });

                var estDepart = new Date();
                if (Identity._fpSettings._TripsheetSearchBack != null) {
                    estDepart.setDate(estDepart.getDate() - (parseInt(Identity._fpSettings._TripsheetSearchBack)));
                }
                else {
                    estDepart.setDate(estDepart.getDate() - (30));
                }
                
                $("#tbDepDate").datepicker('setDate', estDepart);
                var isiPad = navigator.userAgent.match(/iPad/i) != null;
                var WidthjqGrid;
                if (isiPad) {
                    WidthjqGrid=950;
                    $("body").css("width", "950px");
                }
                else {
                    WidthjqGrid=1080;
                    $("body").css("width", "1095px");
                    $(".ui-jqgrid-bdiv").addClass("HideScorll");
                }

                function dataFormatter(cellvalue, options, rowObject) {
                    return moment(cellvalue).format(AppdateFormat.toUpperCase());
                }

                jQuery(jqgridTableId).jqGrid({
                    url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    cache: false,
                    async:false,
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
                    serializeGridData: function (postData) {
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        postData.apiType = 'fss';
                        postData.method = 'PreflightMainDetails';

                        var ClientID = 0;
                        if (hdClientCodeID) {
                            ClientID = parseInt(hdClientCodeID);
                        }
                        var tripSheet = "";
                        if ($('#ChkTripsheet').prop("checked")) {
                                tripSheet = "T";
                            }

                            var worksheetstr = "";
                            if ($('#ChkWorksheet').prop("checked")) {
                                worksheetstr = "W";
                            }

                            var hold = "";
                            if ($('#ChkHold').prop("checked")) {
                                hold = "H";
                            }

                            var cancelled = "";
                            if ($('#ChkCancelled').prop("checked")) {
                                cancelled = "X";
                            }

                            var schedServ = "";
                            

                            var Unfulfilled = "";
                            if ($('#ChUnFillFilled').prop("checked")) {
                                Unfulfilled = "U";
                            }

                            var homebaseIDCSVStr = "";
                            if (hdHomeBase) {
                                homebaseIDCSVStr = hdHomeBase;
                            }

                            var EstDepartDate = new Date();
                            if ($('#tbDepDate').val()) {
                                EstDepartDate = moment($('#tbDepDate').val(), AppdateFormat.toUpperCase()).format("MM/DD/YYYY");
                            }
                            else {
                                EstDepartDate = moment(new Date(0)).format("MM/DD/YYYY");
                            }


                            //fix for SUP-334
                            var LoggedFlag;
                            var Logged = 0;
                            Logged = parseInt($('#ddlShow').val());
                            if (Logged == 1) {
                                postData.isLog = false;
                            }
                            else if (Logged == 2) {
                                postData.isLog = true;
                            }
                            else { postData.isLog = undefined; }


                            var showLogged = false;
                            if (Logged == 2) {
                                showLogged = true;
                            }

                            var isPrivateUser = false;
                            if (hdIsTripPrivacy) {
                                isPrivateUser = hdIsTripPrivacy == 'true';
                            }

                            var homebaseID = 0;
                            if ($('#chkHomebase').prop("checked")) {
                                homebaseID = parseInt(hdHomebaseID);
                            }

                            postData.clientID = ClientID;
                            if (homebaseID) { postData.homebaseID = homebaseID; } else { postData.homebaseID =  undefined; }
                            if (tripSheet) { postData.tripSheet = tripSheet; } else { postData.tripSheet = undefined; }
                            if (worksheetstr) { postData.worksheetstr = worksheetstr; } else { postData.worksheetstr = undefined; }
                            if (hold) { postData.hold = hold; } else { postData.hold = undefined; }
                            if (cancelled) { postData.cancelled = cancelled; } else { postData.cancelled = undefined; }
                            if (schedServ) { postData.schedServ = schedServ; } else { postData.schedServ = undefined; }
                            if (Unfulfilled) { postData.Unfulfilled = Unfulfilled; } else { postData.Unfulfilled = undefined; }
                            if (homebaseIDCSVStr) { postData.homebaseIDCSVStr = homebaseIDCSVStr; } else { postData.homebaseIDCSVStr = undefined; }

                            postData.isPrivateUser = isPrivateUser;
                            var dateSplit = EstDepartDate.split("/");
                            var date = new Date(dateSplit[2], parseInt(dateSplit[0]) - 1, dateSplit[1]);
                            postData.EstDepartDate = date.toDateString();
                            postData.showLogged = showLogged;
                            postData.ApplicationDateFormat = AppdateFormat;

                            return postData;
                        },
                        autoheight: true,
                        width: WidthjqGrid,
                        autowidth: false,
                        shrinkToFit: false,
                        viewrecords: true,
                        rowNum: $("#rowNum").val(),
                        multiselect: MultiSelectFlag,
                        pager: "#pg_gridPager",
                        colNames: ['TripID', 'Trip No.', 'Dispatch', 'Departure Date', 'Requestor', 'Trip Description', 'Trip Status', 'Request Date', 'ForeGrndCustomColor', 'BackgroundCustomColor', 'Tail No.', 'C/R', 'Alert', 'Log', 'Duty Exceeded', 'ACK', 'Except', 'Home Base', 'UWA Request On'],
                        colModel: [
                            { name: 'TripID', index: 'TripID', key: true, hidden: true },
                            { name: 'TripNUM', index: 'TripNUM', width: 70, fixed: true },
                            { name: 'DispatchNum', index: 'DispatchNum', width: 60, fixed: true, search: false, searchoptions: { sopt: ['eq'] } },
                            { name: 'EstDepartureDT', index: 'EstDepartureDT', formatter: dataFormatter, width: 70, fixed: true, searchoptions: { sopt: ['bw'] } },
                            { name: 'RequestorFirstName', index: 'RequestorFirstName', width: 70, fixed: true },
                            { name: 'TripDescription', index: 'TripDescription', width: 90, fixed: true },
                            { name: 'TripStatus', index: 'TripStatus', width: 40, fixed: true },
                            { name: 'RequestDT', index: 'RequestDT', formatter: dataFormatter, width: 70, fixed: true, searchoptions: { sopt: ['bw'] } },
                            { name: 'ForeGrndCustomColor', index: 'ForeGrndCustomColor', width: 50, hidden: true },
                            { name: 'BackgroundCustomColor', index: 'BackgroundCustomColor', width: 50, hidden: true },
                            {
                                name: 'TailNum', index: 'TailNum', width: 50, fixed: true, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                                    var style = "";
                                    if (rdata.ForeGrndCustomColor) {
                                        style = 'color:' + rdata.ForeGrndCustomColor + ';';
                                    }
                                    if (rdata.BackgroundCustomColor) {
                                        style = style + 'background-color:' + rdata.BackgroundCustomColor + ' !important';
                                    }
                                    if (style)
                                        return "Style='" + style + "'";
                                    return "";
                                }
                            },
                            { name: 'CR', index: 'CR', width: 50, fixed: true },
                            {
                                name: 'Alert', index: 'Alert', width: 50, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                                    var style = "";
                                    if (cellValue == "!") {
                                        style = 'color:red;';
                                        if (style)
                                            return "Style='" + style + "'";
                                        return "";
                                    }
                                }
                            },
                            {
                                name: 'LogNum', index: 'LogNum', formatter: function (cellvalue, options, rowObject) {
                                 
                                    if (cellvalue == 0)
                                        return "";
                                    if (cellvalue == "0")
                                        return "";
                                    if (cellvalue)
                                    { return cellvalue;}
                                    else {
                                        return "";
                                    }
                                }, width: 50
                            },
                            {
                                name: 'DutyType', index: 'DutyType', width: 60, fixed: true, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                                    var style = "";
                                    if (cellValue == "!") {
                                        style = 'color:red;';
                                        if (style)
                                            return "Style='" + style + "'";
                                        return "";
                                    }
                                }
                            },
                            {
                                name: 'Acknowledge', index: 'Acknowledge', width: 45, fixed: true, formatter: function (cellvalue, options, rowObject) {
                                    if (cellvalue == 0)
                                        return "";
                                    if (cellvalue == "0")
                                        return "";
                                    if (cellvalue)
                                    { }
                                    else {
                                        return "";
                                    }
                                }
                            },
                            {
                                name: 'TripExcep', index: 'TripExcep', width: 60, fixed: true, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                                    var style = "";
                                    if (cellValue == "!") {
                                        style = 'color:red;';
                                        if (style)
                                            return "Style='" + style + "'";
                                        return "";
                                    }
                                }
                            },
                            { name: 'HomebaseCD', index: 'HomebaseCD', width: 50 },
                            {
                                name: 'TripReqLastSubmitDT', index: 'TripReqLastSubmitDT', width: 90, formatter: function (cellvalue, options, rowObject) {
                                    var d=Date(cellvalue);
                                    if ( Object.prototype.toString.call(d) === "[object Date]" ) {
                                        // it is a date
                                        if ( isNaN( d.getTime() ) ) {  // d.valueOf() could also work
                                            // date is not valid
                                            return "";
                                        }
                                        else {
                                            // date is valid
                                            return $.fn.fmatter.date(cellvalue, options);
                                        }
                                    }
                                    else {
                                        // not a date
                                        return "";
                                    }
                                  
                                }, searchoptions: { sopt: ['eq'] }
                            }
                        ],
                        ondblClickRow: function (rowId) {
                            var rowData = jQuery(this).getRowData(rowId);
                            var lastSel = rowData['TripID'];//replace name with any column
                            var TripNUM = rowData['TripNUM'];
                            $("#hdSelectedTripID").val(lastSel);
                            $("#hdSelectedTripNUM").val(TripNUM);
                            returnToParent();
                        },
                        onSelectRow: function (id) {
                            var rowData = $(this).getRowData(id);
                            var lastSel = rowData['TripID'];//replace name with any column

                            if (id !== lastSel) {
                                $(this).find(".selected").removeClass('selected');
                                $('#results_table').jqGrid('resetSelection', lastSel, true);
                                $(this).find('.ui-state-highlight').addClass('selected');
                                lastSel = id;
                            }
                        },
                        gridComplete: function () {
                            var popupContainerID = '#RadWindowWrapper_ctl00_ctl00_MainContent_SettingBodyContent_RadRetrievePopup';
                            var popupTable = '#RadWindowWrapper_ctl00_ctl00_MainContent_SettingBodyContent_RadRetrievePopup > table';
                            $(popupTable, parent.document).css({ height: '' })
                            $(popupContainerID, parent.document).css({ height: $('.preflight-search-box').height() + 50, width: $('.preflight-search-box').width() + 27 })
                            .css({ left: ($(parent.window).width() - $(popupContainerID, parent.document).width()) / 2, top: ($(parent.window).height() - $(popupContainerID, parent.document).height()) / 2 });
                            $('html').css({ backgroundColo: 'white' });
                            ApplyCustomScroll($(".ui-jqgrid-bdiv"));
                        }
                    });
                    $("#pagesizebox").insertBefore('.ui-paging-info');
                    $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

                    if (isiPad == false) {
                        $(".ui-jqgrid-bdiv").addClass("HideScorll");
                    }
                });



                if (hdHomebaseID) {
                    ClientCodeCheck(hdHomebaseID);
                    $("#tbHomeBase").attr('readonly', true);
                }

                // Check Client Code
                function ClientCodeCheckInternal(Control) {

                    var ClientCode = $(Control).val();
                    $("#lbClientCode").text("");
                    $("#tbSearchClientCode").text("");
                    $("#lbcvClientCode").text("");
                    hdClientCodeID="";

                    if (ClientCode) {
                        var Data = ClientCodeCheck(ClientCode);
                        if (Data.ClientDescription)
                            $("#lbClientCode").text(Data.ClientDescription);
                        if (Data.ClientCD)
                            $("#tbSearchClientCode").text(Data.ClientCD);
                        if (Data.Error)
                            $("#lbcvClientCode").text(Data.Error);
                        if (Data.ClientID)
                            hdClientCodeID = Data.ClientID;
                    }
                    else {
                            $("#lbClientCode").text("");
                            $("#tbSearchClientCode").text("");
                            $("#lbcvClientCode").text("");
                            hdClientCodeID = "";
                    }
                }

                // Check HomeBase Code
                function HomeBaseCheckInternal(Control) {

                    var ICAOId = $(Control).val();
                    
                    $("#lbHomeBase").text("");
                    hdHomeBase = "";
                    $("#tbHomeBase").text("");
                    $("#lbcvHomeBase").text("");
                    if (ICAOId) {
                        var Data = HomeBaseCheck(ICAOId);

                        if (Data.BaseDescription)
                            $("#lbHomeBase").text(Data.BaseDescription);
                        if (Data.HomebaseCD)
                            $("#tbHomeBase").text(Data.HomebaseCD);
                        if (Data.Error)
                            $("#lbcvHomeBase").text(Data.Error);
                        if (Data.HomeBaseID)
                            hdHomeBase = Data.HomeBaseID;
                    }
                    else {
                            $("#lbHomeBase").text("");
                            $("#tbHomeBase").text("");
                            $("#lbcvHomeBase").text("");
                            hdHomeBase = "";
                    }
                }


                function returnToParent() {
                    var oArg=new Object();
                    var selectedrows = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                    if (selectedrows.length > 0) {
                            var rowData = $(jqgridTableId).getRowData(selectedrows);
                            oArg.TripID = rowData['TripID'];//replace name with any column
                            oArg.TripNUM = rowData['TripNUM'];
                    }
                    else {
                        oArg.TripNUM = "";
                    }
                    if(oArg.TripNUM!="")
                        oArg.TripNUM=$.trim(oArg.TripNUM);
                    else
                        oArg.TripNUM = "";

                    var oWnd = GetRadWindow();
                    oWnd.close(oArg);
                }

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close(arg);
                GetRadWindow().BrowserWindow.location.reload(false);
            }


            function reloadPageSize() {
                var myGrid = $(jqgridTableId);
                var currentValue = $("#rowNum").val();
                myGrid.setGridParam({ rowNum: currentValue });
                myGrid.trigger('reloadGrid');
            }
            function reloadPage() {
                var myGrid = $(jqgridTableId);
                myGrid.setGridParam({ page: 1 })
                myGrid.trigger('reloadGrid');
            }


        </script>


        <script type="text/javascript">
          

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('tbSearchClientCode').value;
                    var oWnd = popupwindowSearch(url, 640, 500, ClientCidePopupClose);
                }
                else if (radWin == "rdHomebasePopup") {
                    url = '../../Settings/Company/CompanyMasterPopup.aspx?&MultiSelect=yes&HomeBase=' + document.getElementById('tbHomeBase').value;
                    var oWnd = popupwindowSearch(url, 700, 500, OnClientHomebaseClose);
                }



            }

            // this function is used to display the value of selected Client code from popup
            function ClientCidePopupClose(oWnd, args) {
                var combo = $("#tbSearchClientCode");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        if (arg.ClientCD) {
                            document.getElementById("tbSearchClientCode").value = arg.ClientCD;
                            document.getElementById("lbClientCode").innerHTML = arg.ClientDescription;
                            hdClientCodeID = arg.ClientID;
                            if (arg.ClientCD != null)
                                document.getElementById("lbcvClientCode").innerHTML = "";
                        }
                        else { OnClientHomebaseClose(oWnd, args); return true;}
                    }
                    else {
                        document.getElementById("tbSearchClientCode").value = "";
                        hdClientCodeID="";
                        document.getElementById("lbClientCode").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            
            function OnClientHomebaseClose(oWnd, args) {
                //get the transferred arguments               
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        if (arg.HomebaseID) {
                            if (arg.HomebaseID.split(',').length > 1) {

                                var lableID = 'lblMultiHomebase';
                                var lable = document.getElementById(lableID);
                                if (lable) {
                                    lable.style.display = 'inherit';
                                    lable.style.color = "#FF3300";
                                }
                                document.getElementById("lbHomeBase").innerHTML = "";
                            }
                        }
                        else { ClientCidePopupClose(oWnd, args); return true; }
                    }
                    else {
                        var lableID = 'lblMultiHomebase';
                        var lable = document.getElementById(lableID);
                        if (lable) {
                            lable.style.display = 'none';
                        }
                        document.getElementById("lbHomeBase").innerHTML = arg.BaseDescription;
                        $("#lbcvHomeBase").text("");
                    }
                    hdHomeBase = arg.HomebaseID;
                    var temp = arg.HomeBase.split(',');
                    document.getElementById("tbHomeBase").value = temp[0];
                    document.getElementById("lbHomeBase").innerHTML = arg.BaseDescription;
                    $("#lbcvHomeBase").text("");
                }
                else {
                    hdHomeBase = "";
                }
            }
           

        </script>


        <div id="DivExternalForm" >
      
            <input id="hdSelectedTripID" runat="server" type="hidden" />
            <input id="hdSelectedTripNUM" runat="server" type="hidden" />
            <table class="preflight-search-box">
                <tr>
                    <td valign="top" width="50%">
                        <fieldset>
                            <legend>Search</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel180">
                                        <input type="checkbox" id="chkHomebase" />Home Base Only
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel110">Start Depart Date
                                                </td>
                                                <td>
                                                    <input id="tbDepDate" maxlength="10" type="text" class="text70" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel180" valign="top">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="tdLabel75">Home Base
                                                            </td>
                                                            <td valign="top">
                                                                <input type="text" class="text50" id="tbHomeBase" maxlength="4" onchange="HomeBaseCheckInternal(this); fnAllowAlphaNumeric(this, event); return false;"></input>

                                                                <input type="button"  tooltip="Search for Home Base" class="browse-button"
                                                                    id="btnHomeBase" onclick="javascript: openWin('rdHomebasePopup'); return false;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label id="lbHomeBase" class="input_no_bg"></label>
                                                                <label id="lbcvHomeBase" class="alert-text"></label>
                                                                <label id="lblMultiHomebase" style="display: none; color: red;"></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="tdLabel110">Client Code
                                                            </td>
                                                            <td>
                                                                <input type="text" class="text50" id="tbSearchClientCode" maxlength="5" onchange="RemoveSpecialChars(this);ClientCodeCheckInternal(this); fnAllowAlphaNumeric(this, event); return false;" />


                                                                <input type="button"  id="btnClientCode" onclick="javascript: openWin('RadClientCodePopup'); return false;"
                                                                    class="browse-button" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label id="lbClientCode" class="input_no_bg"></label>
                                                                <label id="lbcvClientCode" class="alert-text"></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel180" valign="top"></td>
                                    <td valign="top"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="tdLabel132">Show / Exclude Logged
                                                            </td>
                                                            <td>

                                                                <select id="ddlShow">
                                                                    <option value="0" selected="selected">All</option>
                                                                    <option value="2">Show Logged</option>
                                                                    <option value="1">Exclude Logged</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="display: none">
                                                   <input id="chkIsException" type="checkbox"   />Exception
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td width="40%" valign="top">
                        <fieldset>
                            <legend>Status</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel120">
                                        <input id="ChkWorksheet" type="checkbox" />Worksheet
                                    </td>
                                    <td class="tdLabel120">
                                           <input id="ChkTripsheet" type="checkbox" />Tripsheet
                                    </td>
                                    <td class="tdLabel120">
                                          <input id="ChUnFillFilled" type="checkbox" />Unfulfilled
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel120">
                                         <input id="ChkCancelled" type="checkbox" />Canceled
                                    </td>
                                    <td class="tdLabel120">
                                         <input id="ChkHold" type="checkbox" />Hold
                                    </td>
                                    
                                </tr>
                            </table>
                        </fieldset>
                         <table>
                            <tr>
                                <td>
<input type="button" class="button" value="Search" id="btnSearch" onclick="reloadPage(); return false;" />
                                </td>
                            </tr>
                        </table>
                    </td>



                </tr>
                <tr>
                    <td colspan="3" valign="top">
                     
                        <div class="jqgrid">
                            <div>
                                <table class="box1">
                                    <tr>
                                        <td>
                                            <table id="gridAllTrips" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="grid_icon">
                                                <div role="group" id="pg_gridPager"></div>
                                                <span class="Span">Page Size:</span>
                                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5" />
                                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                            </div>
                                            <div style="padding: 5px 5px; text-align: right;">
                                                <input id="btnSubmit" type="button" class="button okButton" value="OK" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                      
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table id="tblHidden" style="display: none; position: relative">
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
</body>
</html>
