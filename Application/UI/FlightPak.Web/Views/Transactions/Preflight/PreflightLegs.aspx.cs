﻿//For Tracing and Exception Handling

using System;
using System.Collections;
using FlightPak.Web.Views.Transactions.Preflight;
using Newtonsoft.Json;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreflightLegs : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PreflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                BusinessLite.Preflight.PreflightTripManagerLite.UpdatePreflightTripViewModelAsPerCalenderSelection();
                PreflightUtils.ValidatePreflightSession("Leg");
                int selectedLegNum = 0;
                Int32.TryParse(Request.QueryString["legNum"],out selectedLegNum);

                Hashtable legsHashtable = PreflightTripManager.InitializePreflghtLegs(selectedLegNum).Result;
                hdnPreflightLegInitializationObject.Value = JsonConvert.SerializeObject(legsHashtable);
               
            }
        }
    }
}


