﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using System.IO;
using FlightPak.Common;
using FlightPak.Common.Constants;

//For Tracing and Exceptioon Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;


namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightCrewPage : BaseSecuredPage
    {
        #region "Predefined"
        Int64 CountryIDvalue;
        private bool IsEmptyCheck = true;
        private ExceptionManager exManager;
        // Declare Default Date Format
        string DateFormat = string.Empty;
        string CrewRatingAircraftCD = "";
        Int64 CrewRatingCrewID = 0;
        private bool CrewRosterPageNavigated = false;
        private Int64 CrewID;

        public const string DueDT = "DueDT";
        public const string lbDueDT = "lbDueDT";
        public const string AlertDT = "AlertDT";
        public const string lbAlertDT = "lbAlertDT";
        public const string OriginalDT = "OriginalDT";
        public const string lbOriginalDT = "lbOriginalDT";
        public const string PreviousCheckDT = "PreviousCheckDT";
        public const string lbPreviousCheckDT = "lbPreviousCheckDT";
        public const string CurrentFltHrs = "CurrentFltHrs";
        public const string lbCurrentFltHrs = "lbCurrentFltHrs";
        public const string GraceDT = "GraceDT";
        public const string lbGraceDT = "lbGraceDT";
        public const string BaseMonthDT = "BaseMonthDT";
        public const string lbBaseMonthDT = "lbBaseMonthDT";


        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }

        protected FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // To Assign Date Format from User Control, based on Company Profile
                        if (!string.IsNullOrEmpty(Request.QueryString["CrewID"]))
                        {
                            CrewID = Convert.ToInt64(Request.QueryString["CrewID"]);
                        }


                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrewRoster, dgCrewRoster, RadAjaxLoadingPanel1);
                        //store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCrewRoster.ClientID));
                        // Set Logged-in User Name
                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        if (identity != null && identity.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = identity.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                            //((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                            RadDatePicker1.DateInput.DateFormat = DateFormat;
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                            RadDatePicker1.DateInput.DateFormat = DateFormat;
                        }


                        if (identity != null && identity.Identity._clientId != null && identity.Identity._clientId != 0)
                        {

                            tbClientCodeFilter.Enabled = false;
                            tbClientCodeFilter.Text = identity.Identity._clientCd.ToString().Trim();
                            //btnClientCodeFilter.Enabled = false;

                        }
                        else
                        {
                            tbClientCodeFilter.Enabled = true;
                            // btnClientCodeFilter.Enabled = true;
                        }

                        if (!IsPostBack)
                        {
                            // Default Control Appearence
                            pnlBeginning.Visible = false;
                            pnlCurrent.Visible = true;

                            CheckAutorization(Permission.Database.ViewCrewRoster);

                            tabSwitchViews.SelectedIndex = 1;
                            RadPageView2.Selected = true;



                            //// Bind Additional Info. Grid from DB
                            //BindAdditionalInfoGrid(0);

                            ////// Bind Typerating Grid from DB
                            //BindTypeRatingGrid(0);

                            ////// Bind Crew Checklist Grid from DB
                            //BindCrewCheckListGrid(0);

                            //// Bind Aircraft Assigned Grid from DB
                            BindAircraftAssignedGrid(0);
                            BindCrewCurrency(0);
                            //SelectedAircraftAssigned();

                            ////// Bind Crew Visa Grid from DB
                            //BindCrewVisaGrid(0);

                            ////// Bind Crew Passport Grid from DB
                            //BindCrewPassportGrid(string.Empty);

                            // Call the Default Selection Items
                            DefaultSelection(true);
                            CreateDictionayForImgUpload();
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                var objCompany = CompanyService.GetCompanyWithFilters(string.Empty, (long)identity.Identity._homeBaseId,true,true).EntityList;
                                CompanySett.CrewOFullName = objCompany[0].CrewOFullName; //1;
                                CompanySett.CrewOMiddle = objCompany[0].CrewOMiddle;//2;
                                CompanySett.CrewOLast = objCompany[0].CrewOLast;//3;
                                CompanySett.IsEnableTSAPX = objCompany[0].IsEnableTSAPX;
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }



        }

        #region "Image Saving"

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadImage")
                            {
                                LoadImage();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];

                        int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            dicImg.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = dicImg;

                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            Session["DicImgDelete"] = dicImgDelete;

                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in dicImg)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            imgFile.ImageUrl = string.Empty;
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        //public static byte[] ImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}


        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                System.IO.Stream myStream;
                Int32 fileLen;
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        string FileName = fileUL.FileName;
                        fileLen = fileUL.PostedFile.ContentLength;
                        Byte[] Input = new Byte[fileLen];

                        myStream = fileUL.FileContent;
                        myStream.Read(Input, 0, fileLen);
                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(Input));

                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                        if (tbImgName.Text.Trim() != "")
                            FileName = tbImgName.Text.Trim();

                        int count = dicImg.Count(D => D.Key.Equals(FileName));
                        if (count > 0)
                        {
                            dicImg[FileName] = Input;
                        }
                        else
                        {
                            dicImg.Add(FileName, Input);
                            ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(System.Web.HttpUtility.HtmlEncode(FileName), FileName));
                        }
                        tbImgName.Text = System.Web.HttpUtility.HtmlEncode("");
                        btndeleteImage.Enabled = true;
                    }
                }
            }
        }

        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // if(Session["SelectedCrewRosterID"] != null)
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool imgFound = false;
                            imgFile.ImageUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                var ObjRetImg = ObjImgService.GetFileWarehouseList("Crew", Convert.ToInt64(Convert.ToString(Session["SelectedCrewRosterID"]))).EntityList.Where(x => x.UWAFileName.ToLower() == ddlImg.Text.Trim());

                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;
                                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                }
                                tbImgName.Text = ddlImg.Text.Trim();
                                if (!imgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (count > 0)
                                    {
                                        imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    }

                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                Session["DicImg"] = dicImg;

                Dictionary<string, string> dicImgDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = dicImgDelete;
            }
        }

        #endregion

        #region "Crew Code"Generation"

        /// <summary>
        /// Generate the Crew code 
        /// </summary>
        private string GetCrewCode(FlightPakMasterService.Company Company)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Company))
            {
                string CrewCD = "";
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    int Count = 1;
                    while (Count <= 3)
                    {
                        if (Company.CrewOFullName == Count)
                        {
                            if (tbFirstName.Text.Length > 0)
                                CrewCD = CrewCD + tbFirstName.Text.Substring(0, 1);
                        }
                        if (Company.CrewOMiddle == Count)
                        {
                            if (tbMiddleName.Text.Length > 0)
                                CrewCD = CrewCD + tbMiddleName.Text.Substring(0, 1); ;
                        }
                        if (Company.CrewOLast == Count)
                        {
                            if (tbLastName.Text.Length > 0)
                                CrewCD = CrewCD + tbLastName.Text.Substring(0, 1); ;
                        }

                        Count++;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

                return CrewCD;
            }

        }

        #endregion

        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Set as Default Selected Item as First Row
                    dgCrewRoster.SelectedIndexes.Add(0);
                    dgCrewRoster.Rebind();
                    if (dgCrewRoster.MasterTableView.Items.Count > 0)
                    {
                        if (dgCrewRoster.Items[0].GetDataKeyValue("CrewID") != null)
                        {
                            Session["SelectedCrewRosterID"] = dgCrewRoster.Items[0].GetDataKeyValue("CrewID").ToString();
                            ReadOnlyForm();
                            GridEnable(true, true, true);
                        }
                    }
                    EnableHours(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgCrewRoster.Rebind();
                    }
                    if (dgCrewRoster.MasterTableView.Items.Count > 0)
                    {
                        if (dgCrewRoster.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                            Item.Selected = false;
                        }
                        dgCrewRoster.SelectedIndexes.Add(0);
                        if (dgCrewRoster.Items[0].GetDataKeyValue("CrewID") != null)
                        {
                            Session["SelectedCrewRosterID"] = dgCrewRoster.Items[0].GetDataKeyValue("CrewID").ToString();
                        }
                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        GridEnable(true, true, true);
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedCrewRosterID"];
                        string CrewID = Session["SelectedCrewRosterID"].ToString();
                        //   string Code = items.GetDataKeyValue("CrewCD").ToString();

                        foreach (GridDataItem item in dgCrewRoster.MasterTableView.Items)
                        {
                            if (item["CrewID"].Text.Trim() == CrewID.Trim())
                            {
                                item.Selected = true;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #region "Crew Roster Search Grid Events"

        /// <summary>
        /// Bind Crew Roster Data into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (Request.QueryString["CrewID"] != null)
                        //{
                        //    hdnCrewID.Value = Request.QueryString["CrewID"];

                        MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient();
                        //if (!string.IsNullOrEmpty(hdnCrew.Value))
                        //    Crewid = Convert.ToInt64(hdnCrewID.Value);
                        if (CrewID != 0)
                        {
                            var CrewData = CrewRosterService.GetCrewbyCrewId(CrewID);

                            var CrewList = CrewData.EntityList.Where(x => x.IsDeleted == false).ToList();
                            if (CrewData.ReturnFlag == true)
                            {
                                dgCrewRoster.DataSource = CrewList;
                            }

                            Session["PreflightcrewpageCrewList"] = (List<GetCrewByCrewID>)CrewList;
                        }
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton EditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(EditButton, divExternalForm, RadAjaxLoadingPanel1);

                            // To find the insert button in the grid
                            LinkButton InsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(InsertButton, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Crew Roster Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                if (Session["SelectedCrewRosterID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(Convert.ToString((Session["SelectedCrewRosterID"]))));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                            SelectItem();
                                            return;
                                        }
                                    }
                                }
                                hdnTemp.Value = "On";
                                DisplayEditForm();
                                DefaultChecklistSelection();
                                DefaultTypeRatingSelection();
                                GridEnable(false, true, false);
                                ddlImg.Enabled = true;
                                if (imgFile.ImageUrl.Trim() != "")
                                    btndeleteImage.Enabled = true;
                                tbImgName.Enabled = true;
                                fileUL.Enabled = false;
                                SelectItem();

                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                tbCrewCode.ReadOnly = false;
                                tbCrewCode.BackColor = System.Drawing.Color.White;
                                hdnTemp.Value = "On";
                                DisplayInsertForm();
                                DefaultChecklistSelection();
                                DefaultTypeRatingSelection();
                                GridEnable(true, false, false);
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = false;
                                fileUL.Enabled = false;
                                dgCrewRoster.SelectedIndexes.Clear();
                                //tbCrewCode.Focus();


                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Update Command Method for Crew Roster
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            string CrewID = Session["SelectedCrewRosterID"].ToString();
                            string CustomerID = string.Empty, CrewCD = string.Empty;
                            foreach (GridDataItem Item in dgCrewRoster.MasterTableView.Items)
                            {
                                if (Item["CrewID"].Text.Trim() == CrewID.Trim())
                                {
                                    CrewCD = Item["CrewCD"].Text.Trim();
                                    CustomerID = Item["CustomerID"].Text.Trim();
                                    break;
                                }
                            }

                            bool IsValidate = true;
                            if (CheckHomeBaseExist())
                            {
                                cvHomeBase.IsValid = false;
                                tbHomeBase.Focus();
                                IsValidate = false;
                            }
                            if (CheckClientCodeExist())
                            {
                                cvClientCode.IsValid = false;
                                tbClientCode.Focus();
                                IsValidate = false;
                            }
                            if (CheckCountryExist(tbCountry, cvCountry))
                            {
                                cvCountry.IsValid = false;
                                tbCountry.Focus();
                                IsValidate = false;
                            }


                            if (CheckCountryExist(tbCountryBirth, cvCountryBirth))
                            {
                                cvCountryBirth.IsValid = false;
                                tbCountryBirth.Focus();
                                IsValidate = false;
                            }


                            if (CheckCountryExist(tbCitizenCode, cvCitizenCode))
                            {
                                cvCitizenCode.IsValid = false;
                                tbCitizenCode.Focus();
                                IsValidate = false;
                            }


                            if (CheckCountryExist(tbLicCountry, cvLicCountry))
                            {
                                cvLicCountry.IsValid = false;
                                tbLicCountry.Focus();
                                IsValidate = false;
                            }


                            if (CheckCountryExist(tbAddLicCountry, cvAddLicCountry))
                            {
                                cvAddLicCountry.IsValid = false;
                                tbAddLicCountry.Focus();
                                IsValidate = false;
                            }

                            if (IsValidate)
                            {
                                using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                                {

                                    FlightPakMasterService.Crew oCrew = new Crew();
                                    oCrew = GetCrewMainInfo(oCrew);
                                    oCrew = SaveCrewDefinition(oCrew);
                                    oCrew = SaveCrewVisa(oCrew);
                                    oCrew = SaveCrewPassport(oCrew);
                                    oCrew = SaveTypeRatings(oCrew);
                                    oCrew = SaveCrewChecklist(oCrew);
                                    oCrew = SaveAircraftAssigned(oCrew);

                                    //if (Session["CrewAddInfo"] != null)
                                    //    oCrew = SaveCrewDefinition(oCrew);

                                    //if (Session["CrewVisa"] != null)
                                    //    oCrew = SaveCrewVisa(oCrew);

                                    //if (Session["CrewPassport"] != null)
                                    //    oCrew = SaveCrewPassport(oCrew);

                                    //if (Session["CrewTypeRating"] != null)
                                    //    oCrew = SaveTypeRatings(oCrew);

                                    //if (Session["CrewCheckList"] != null)
                                    //    oCrew = SaveCrewChecklist(oCrew);

                                    //if (Session["CrewAircraftAssigned"] != null)
                                    //    oCrew = SaveAircraftAssigned(oCrew);

                                    CrewRosterService.UpdateCrew(oCrew);


                                    #region Image Upload in Edit Mode
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    foreach (var DicItem in dicImg)
                                    {

                                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                            Service.FileWarehouseID = 0;
                                            Service.RecordType = "Crew";
                                            Service.UWAFileName = DicItem.Key;
                                            Service.RecordID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                                            Service.UWAWebpageName = "CrewRoster.aspx";
                                            Service.UWAFilePath = DicItem.Value;
                                            Service.IsDeleted = false;
                                            Service.FileWarehouseID = 0;
                                            objService.AddFWHType(Service);
                                        }
                                    }
                                    #endregion

                                    #region Image Upload in Delete
                                    using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                        Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                        foreach (var DicItem in dicImgDelete)
                                        {
                                            objFWHType.UWAFileName = DicItem.Key;
                                            objFWHType.RecordID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                                            objFWHType.RecordType = "Crew";
                                            objFWHType.IsDeleted = true;
                                            objFWHType.IsDeleted = true;
                                            objFWHTypeService.DeleteFWHType(objFWHType);
                                        }
                                    }
                                    #endregion
                                }
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim()));
                                }
                            }
                            GridEnable(true, true, true);

                            SelectItem();
                            ReadOnlyForm();

                            // Set as Main Info as Default Tab
                            tabCrewRoster.SelectedIndex = 0;
                            RadMultiPage1.SelectedIndex = 0;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void CrewRoster_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewRosterPageNavigated = true;
                        dgCrewRoster.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void CrewRoster_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }


                        else if (CrewRosterPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Insert Command Method for Crew Roster
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;

                        if (CheckHomeBaseExist())
                        {
                            cvHomeBase.IsValid = false;
                            tbHomeBase.Focus();
                            IsValidate = false;

                        }
                        if (CheckClientCodeExist())
                        {
                            cvClientCode.IsValid = false;
                            tbClientCode.Focus();
                            IsValidate = false;
                        }
                        if (CheckCountryExist(tbCountry, cvCountry))
                        {
                            cvCountry.IsValid = false;
                            tbCountry.Focus();
                            IsValidate = false;
                        }


                        if (CheckCountryExist(tbCountryBirth, cvCountryBirth))
                        {
                            cvCountryBirth.IsValid = false;
                            tbCountryBirth.Focus();
                            IsValidate = false;
                        }


                        if (CheckCountryExist(tbCitizenCode, cvCitizenCode))
                        {
                            cvCitizenCode.IsValid = false;
                            tbCitizenCode.Focus();
                            IsValidate = false;
                        }


                        if (CheckCountryExist(tbLicCountry, cvLicCountry))
                        {
                            cvLicCountry.IsValid = false;
                            tbLicCountry.Focus();
                            IsValidate = false;
                        }


                        if (CheckCountryExist(tbAddLicCountry, cvAddLicCountry))
                        {
                            cvAddLicCountry.IsValid = false;
                            tbAddLicCountry.Focus();
                            IsValidate = false;
                        }
                        if (CheckDepartmentCodeExist())
                        {
                            cvDepartmentCode.IsValid = false;
                            tbDepartment.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Crew oCrew = new Crew();

                                oCrew = GetCrewMainInfo(oCrew);
                                oCrew = SaveCrewDefinition(oCrew);
                                oCrew = SaveCrewVisa(oCrew);
                                oCrew = SaveCrewPassport(oCrew);
                                oCrew = SaveTypeRatings(oCrew);
                                oCrew = SaveCrewChecklist(oCrew);
                                oCrew = SaveAircraftAssigned(oCrew);
                                CrewRosterService.AddCrew(oCrew);

                                #region Image Upload in New Mode

                                var InsertedFleetID = CrewRosterService.GetCrewID("");
                                Int64 NewCrewID = 0;
                                if (InsertedFleetID != null)
                                {
                                    foreach (FlightPakMasterService.GetCrewID FltId in InsertedFleetID.EntityList)
                                    {
                                        hdnCrewID.Value = Convert.ToString(FltId.CrewID);
                                    }
                                }



                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                foreach (var DicItem in dicImg)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                        Service.FileWarehouseID = 0;
                                        Service.RecordType = "Crew";
                                        Service.UWAFileName = DicItem.Key;
                                        Service.RecordID = Convert.ToInt64(hdnCrewID.Value);
                                        Service.UWAWebpageName = "CrewRoster.aspx";
                                        Service.UWAFilePath = DicItem.Value;
                                        Service.IsDeleted = false;
                                        Service.FileWarehouseID = 0;
                                        objService.AddFWHType(Service);
                                    }
                                }

                                #endregion
                            }

                            dgCrewRoster.Rebind();
                            GridEnable(true, true, true);
                            DefaultSelection(false);

                            // Set as Main Info as Default Tab
                            tabCrewRoster.SelectedIndex = 0;
                            RadMultiPage1.SelectedIndex = 0;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CrewRoster_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {
                                Crew Crew = new Crew();
                                string CrewID = Session["SelectedCrewRosterID"].ToString();
                                string CrewCD = string.Empty;

                                GridDataItem item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                                Crew.CrewID = Convert.ToInt64(CrewID);
                                Crew.CrewCD = item["CrewCD"].Text;                    // Doubt
                                Crew.MiddleInitial = tbMiddleName.Text;
                                Crew.IsDeleted = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Crew);
                                        return;
                                    }

                                    CrewRosterService.DeleteCrew(Crew);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
                finally
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.Crew, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim()));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = dgCrewRoster.SelectedItems[0] as GridDataItem;
                        if (item["CrewID"].Text.Trim() != "")
                        {
                            Session["SelectedCrewRosterID"] = item["CrewID"].Text;
                            ReadOnlyForm();
                            GridEnable(true, true, true);

                            DefaultChecklistSelection();
                            DefaultTypeRatingSelection();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bound Row styles, based on Checklist information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewRoster_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            Label lbCheckList = (Label)Item.FindControl("lbCheckList");
                            if (lbCheckList.Text.Trim() == "0")
                            {
                                lbCheckList.Text = System.Web.HttpUtility.HtmlEncode("!");
                                lbCheckList.ForeColor = System.Drawing.Color.Red;
                                Item.ForeColor = System.Drawing.Color.Red;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        #endregion

        #region "Crew Roster Additional Info Grid Events"

        /// <summary>
        /// Edit Row styles, based on Crew Roster Additional Info.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewAddlInfo_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("IsReptFilter") != null)
                            {
                                if (Item.GetDataKeyValue("IsReptFilter").ToString() == "True")
                                {
                                    ((CheckBox)Item["IsReptFilter"].FindControl("chkIsReptFilter")).Checked = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }



        /// <summary>
        /// Method to Bind Additional Information Grid, based on Crew Code
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private void BindAdditionalInfoGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // GridDataItem Item = (GridDataItem)Session["SelectedCrewRosterID"];
                    if (Session["CrewAddInfo"] != null)
                    {
                        List<GetCrewDefinition> CrewDefinitionList = (List<GetCrewDefinition>)Session["CrewAddInfo"];
                        dgCrewAddlInfo.DataSource = CrewDefinitionList;
                        dgCrewAddlInfo.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            List<GetCrewDefinition> CrewAdditionalInfoList = new List<GetCrewDefinition>();
                            GetCrewDefinition crew = new GetCrewDefinition();
                            crew.CrewID = CrewID;
                            var CrewDefinitionValue = CrewRosterService.GetCrewDefinitionList(crew);

                            if (CrewDefinitionValue.ReturnFlag == true)
                            {
                                CrewAdditionalInfoList = CrewDefinitionValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewDefinition>();
                                dgCrewAddlInfo.DataSource = CrewAdditionalInfoList;
                                dgCrewAddlInfo.DataBind();
                                Session["CrewAddInfo"] = CrewAdditionalInfoList;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void BindAdditionalInfoGridonEdit(string CrewCode)
        {
            //try
            //{

            //    using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
            //    {
            //        CrewDefinition crew = new CrewDefinition();
            //        crew.CrewID = Convert.ToInt64(CrewCode);
            //        var CrewDefinitionValue = CrewRosterService.GetCrewDefinitionList(crew);

            //        if (CrewDefinitionValue.ReturnFlag == true)
            //        {
            //            dgCrewAddlInfo.DataSource = CrewDefinitionValue.EntityList;
            //            dgCrewAddlInfo.DataBind();
            //            Session["CrewAddInfo"] = (List<CrewDefinition>)CrewDefinitionValue.EntityList.ToList();
            //        }

            //    }
            //}
            //catch (Exception ex) { }
        }
        #endregion

        #region "Crew Roster Aircraft Assigned Grid Events"

        /// <summary>
        /// Method to Bind Aircraft Assigned Grid, based on Crew Code
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private void BindAircraftAssignedGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewAircraftAssigned"] != null)
                    {
                        List<FlightPakMasterService.GetAllCrewAircraftAssign> FleetList = (List<FlightPakMasterService.GetAllCrewAircraftAssign>)Session["CrewAircraftAssigned"];
                        dgAircraftAssigned.DataSource = FleetList;
                        dgAircraftAssigned.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            GetAllCrewAircraftAssign oGetSelectedAircraft = new GetAllCrewAircraftAssign();
                            oGetSelectedAircraft.CrewID = CrewID;
                            var CrewRosterServiceValue = CrewRosterService.GetSelectedAircraftAssignedList(oGetSelectedAircraft);
                            if (CrewRosterServiceValue.ReturnFlag == true)
                            {
                                dgAircraftAssigned.DataSource = CrewRosterServiceValue.EntityList;
                                dgAircraftAssigned.DataBind();
                                Session["CrewAircraftAssigned"] = (List<FlightPakMasterService.GetAllCrewAircraftAssign>)CrewRosterServiceValue.EntityList.ToList();
                            }
                        }
                    }

                    using (MasterCatalogServiceClient AircraftAssignedService = new MasterCatalogServiceClient())
                    {
                        List<CrewAircraftAssigned> AircraftAssignedList = new List<CrewAircraftAssigned>();
                        CrewAircraftAssigned CrewAircraftAssigned = new CrewAircraftAssigned();
                        CrewAircraftAssigned.CrewID = CrewID;
                        var CrewRosterServiceValues = AircraftAssignedService.GetAircraftAssignedList(CrewAircraftAssigned);
                        if (CrewRosterServiceValues.ReturnFlag == true)
                        {
                            AircraftAssignedList = CrewRosterServiceValues.EntityList.ToList();
                            var Results = (from code in AircraftAssignedList
                                           select code);
                            foreach (var V in Results)
                            {
                                foreach (GridDataItem Item in dgAircraftAssigned.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("FleetID") != null)
                                    {
                                        Int64 TailNum = Convert.ToInt64(Item.GetDataKeyValue("FleetID").ToString().Trim());
                                        if (V.FleetID == TailNum)
                                        {
                                            ((CheckBox)(Item["AircraftAssigned"].FindControl("chkAircraftAssigned"))).Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }



        protected void AircraftAssigned_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            if (Session["SelectedCrewRosterID"] != null)
            {
                BindAircraftAssignedGrid(Convert.ToInt64(Session["SelectedCrewRosterID"].ToString()));
            }
            else
            {
                BindAircraftAssignedGrid(0);
            }
            dgAircraftAssigned.ClientSettings.Scrolling.ScrollTop = "0";
        }




        #endregion

        #region "Crew Roster Type Ratings Grid Events"

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TypeRating_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            EnableHours(true);
                            btnFlightExp.Text = "Display Current Flt.Experience";
                            BindSelectedTypeRatingItem();
                        }
                        else
                        {
                            EnableHours(false);
                            btnFlightExp.Text = "Modify Beginning Flt. Experience";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Edit Row styles, based on Crew Rating.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TypeRating_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Convert.ToString(Item.GetDataKeyValue("IsPilotinCommand")) == "True" || Convert.ToString(Item.GetDataKeyValue("IsQualInType135PIC")) == "True")
                            {
                                ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked = true;
                            }
                            if (Convert.ToString(("IsSecondInCommand")) == "True" || Convert.ToString(Item.GetDataKeyValue("IsQualInType135SIC")) == "True")
                            {
                                ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Selected Type Raing Grid Item in the Fields
        /// </summary>
        private void BindSelectedTypeRatingItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //ClearTypeRatingFields();
                    if (dgTypeRating.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgTypeRating.SelectedItems[0];
                        Item.Selected = true;

                        lbTypeCode.Text = System.Web.HttpUtility.HtmlEncode(((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text.Trim());
                        lbDescription.Text = System.Web.HttpUtility.HtmlEncode(((Label)Item["CrewRatingDescription"].FindControl("lbCrewRatingDescription")).Text);

                        //// Quality In Type As
                        //chkTRPIC91.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsPilotinCommand"));
                        //chkTRPIC135.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsQualInType135PIC"));

                        //chkTRSIC91.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsSecondInCommand"));
                        //chkTRSIC135.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsQualInType135SIC"));

                        //chkTREngineer.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsEngineer"));
                        //chkTRInstructor.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInstructor"));
                        //chkTRAttendant.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsCheckAttendant"));
                        //chkTRAttendant91.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendantFAR91"));
                        //chkTRAttendant135.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendantFAR135"));
                        //chkTRAirman.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsCheckAirman"));
                        //chkTRInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive"));


                        ////<new fields>

                        //chkPIC121.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsPIC121"));
                        //chkSIC121.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsSIC121"));
                        //chkPIC125.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsPIC125"));
                        //chkSIC125.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsSIC125"));
                        //chkAttendent121.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendant121"));
                        //chkAttendent125.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendant125"));

                        // Quality In Type As
                        if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                        {
                            chkTRPIC91.Checked = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                        }
                        else
                        {
                            chkTRPIC91.Checked = false;
                        }

                        if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                        {
                            chkTRSIC91.Checked = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                        }
                        else
                        {
                            chkTRSIC91.Checked = false;
                        }



                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                        {
                            chkTRPIC135.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                        }
                        else
                        {
                            chkTRPIC135.Checked = false;
                        }


                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                        {
                            chkTRSIC135.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                        }
                        else
                        {
                            chkTRSIC135.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                        {
                            chkTREngineer.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                        }
                        else
                        {
                            chkTREngineer.Checked = false;
                        }


                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                        {
                            chkTRInstructor.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                        }
                        else
                        {
                            chkTRInstructor.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                        {
                            chkTRAttendant.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                        }
                        else
                        {
                            chkTRAttendant.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                        {
                            chkTRAttendant91.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                        }
                        else
                        {
                            chkTRAttendant91.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                        {
                            chkTRAttendant135.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                        }
                        else
                        {
                            chkTRAttendant135.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                        {
                            chkTRAirman.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                        }
                        else
                        {
                            chkTRAirman.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                        {
                            chkTRInactive.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                        }
                        else
                        {
                            chkTRInactive.Checked = false;
                        }




                        //<new field>
                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                        {
                            chkPIC121.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                        }
                        else
                        {
                            chkPIC121.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                        {
                            chkSIC121.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                        }
                        else
                        {
                            chkSIC121.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                        {
                            chkPIC125.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                        }
                        else
                        {
                            chkPIC125.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                        {
                            chkSIC125.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                        }
                        else
                        {
                            chkSIC125.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                        {
                            chkAttendent121.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                        }
                        else
                        {
                            chkAttendent121.Checked = false;
                        }

                        if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                        {
                            chkAttendent125.Checked = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                        }
                        else
                        {
                            chkAttendent125.Checked = false;
                        }




                        // As of Date

                        if ((pnlBeginning.Visible == true) && (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text)))
                        {
                            //if (DateFormat != null)
                            //{
                            //    tbAsOfDt.Text = String.Format("{0:" + DateFormat + "}", FormatDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()), DateFormat));
                            //}
                            //else
                            //{
                            tbAsOfDt.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim())));
                            //}
                        }






                        // Total Hrs
                        tbTotalHrsInType.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text;
                        tbTotalHrsDay.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text;
                        tbTotalHrsNight.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text;
                        tbTotalHrsInstrument.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text;


                        // PIC Hrs
                        tbPICHrsInType.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text;
                        tbPICHrsDay.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text;
                        tbPICHrsNight.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text;
                        tbPICHrsInstrument.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text;



                        // SIC Hrs
                        tbSICInTypeHrs.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text;
                        tbSICHrsDay.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text;
                        tbSICHrsNight.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text;
                        tbSICHrsInstrument.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text;




                        // Other Hrs
                        tbOtherHrsSimulator.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text;
                        tbOtherHrsFltEngineer.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text;
                        tbOtherHrsFltInstructor.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text;
                        tbOtherHrsFltAttendant.Text = ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text = false ? "0.00" : ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text;


                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }

        /// <summary>
        /// Method to Type Rating Grid based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindTypeRatingGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewTypeRating"] != null)
                    {
                        List<GetCrewRating> GetCrewRatingList = (List<GetCrewRating>)Session["CrewTypeRating"];
                        dgTypeRating.DataSource = GetCrewRatingList;
                        dgTypeRating.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            List<GetCrewRating> CrewRatingList = new List<GetCrewRating>();
                            GetCrewRating crew = new GetCrewRating();
                            crew.CrewID = CrewID;
                            var CrewRatingValue = CrewRosterService.GetCrewRatingList(crew);
                            if (CrewRatingValue.ReturnFlag == true)
                            {
                                CrewRatingList = CrewRatingValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewRating>();
                                dgTypeRating.DataSource = CrewRatingList;
                                dgTypeRating.DataBind();
                                Session["CrewTypeRating"] = CrewRatingList;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion


        #region "Type Rating Textchanged Events & Check Changed Events"



        protected void AsOfDt_TextChanged(object sender, EventArgs e)
        {
            if (dgTypeRating.SelectedItems.Count > 0)
            {
                //GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                //((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text = tbAsOfDt.Text;
            }

        }
        protected void Typerating_TextChanged(object sender, EventArgs e)
        {
            if (dgTypeRating.SelectedItems.Count > 0)
            {
                GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                string text = ((TextBox)sender).ID;

                // Total Hrs
                if (text.ToUpper() == "tbTotalHrsInType".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbTotalHrsInType.Text);
                }
                if (text.ToUpper() == "tbTotalHrsDay".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbTotalHrsDay.Text);
                }
                if (text.ToUpper() == "tbTotalHrsNight".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbTotalHrsNight.Text);
                }
                if (text.ToUpper() == "tbTotalHrsInstrument".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbTotalHrsInstrument.Text);
                }




                // PIC Hrs
                if (text.ToUpper() == "tbPICHrsInType".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text = System.Web.HttpUtility.HtmlEncode(tbPICHrsInType.Text);
                }
                if (text.ToUpper() == "tbPICHrsDay".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text = System.Web.HttpUtility.HtmlEncode(tbPICHrsDay.Text);
                }
                if (text.ToUpper() == "tbPICHrsNight".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text = System.Web.HttpUtility.HtmlEncode(tbPICHrsNight.Text);
                }
                if (text.ToUpper() == "tbPICHrsInstrument".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text = System.Web.HttpUtility.HtmlEncode(tbPICHrsInstrument.Text);
                }


                // SIC Hrs
                if (text.ToUpper() == "tbSICInTypeHrs".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbSICInTypeHrs.Text);
                }
                if (text.ToUpper() == "tbSICHrsDay".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text = System.Web.HttpUtility.HtmlEncode(tbSICHrsDay.Text);
                }
                if (text.ToUpper() == "tbSICHrsNight".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text = System.Web.HttpUtility.HtmlEncode(tbSICHrsNight.Text);
                }
                if (text.ToUpper() == "tbSICHrsInstrument".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text = System.Web.HttpUtility.HtmlEncode(tbSICHrsInstrument.Text);
                }


                // SIC Hrs
                if (text.ToUpper() == "tbOtherHrsSimulator".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text = System.Web.HttpUtility.HtmlEncode(tbOtherHrsSimulator.Text);
                }
                if (text.ToUpper() == "tbOtherHrsFltEngineer".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text = System.Web.HttpUtility.HtmlEncode(tbOtherHrsFltEngineer.Text);
                }
                if (text.ToUpper() == "tbOtherHrsFltInstructor".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text = System.Web.HttpUtility.HtmlEncode(tbOtherHrsFltInstructor.Text);
                }
                if (text.ToUpper() == "tbOtherHrsFltAttendant".ToUpper())
                {
                    ((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text = System.Web.HttpUtility.HtmlEncode(tbOtherHrsFltAttendant.Text);
                }
            }
        }

        protected void TypeRating_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                            string text = ((CheckBox)sender).ID;

                            if (text.ToUpper() == "chkTREngineer".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked = chkTREngineer.Checked;
                            }
                            if (text.ToUpper() == "chkPIC121".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked = chkPIC121.Checked;
                            }
                            if (text.ToUpper() == "chkSIC121".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked = chkSIC121.Checked;
                            }
                            if (text.ToUpper() == "chkPIC125".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked = chkPIC125.Checked;
                            }
                            if (text.ToUpper() == "chkSIC125".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked = chkSIC125.Checked;
                            }
                            if (text.ToUpper() == "chkAttendent121".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked = chkAttendent121.Checked;
                            }
                            if (text.ToUpper() == "chkAttendent125".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked = chkAttendent125.Checked;
                            }
                            if (text.ToUpper() == "chkTRInstructor".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked = chkTRInstructor.Checked;
                            }
                            if (text.ToUpper() == "chkTRAttendant".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked = chkTRAttendant.Checked;
                            }
                            if (text.ToUpper() == "chkTRAttendant91".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked = chkTRAttendant91.Checked;
                            }
                            if (text.ToUpper() == "chkTRAttendant135".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked = chkTRAttendant135.Checked;
                            }
                            if (text.ToUpper() == "chkTRAirman".ToUpper())
                            {
                                ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked = chkTRAirman.Checked;
                            }
                            //if (text.ToUpper() == "chkTRInactive".ToUpper())
                            //{
                            //    ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked = chkTRInactive.Checked;
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        #endregion

        #region "Crew Roster Crew CheckList Grid Events"

        /// <summary>
        /// Method to Enable or Disable Crew Checklist Form Fields
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        protected void EnableCheckListFields(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbOriginalDate.Enabled = Enable;
                    tbChgBaseDate.Enabled = Enable;
                    tbFreq.Enabled = Enable;
                    radFreq.Enabled = Enable;
                    tbPrevious.Enabled = Enable;
                    tbDueNext.Enabled = Enable;
                    tbAlertDate.Enabled = Enable;
                    tbAlertDays.Enabled = Enable;
                    tbGraceDate.Enabled = Enable;
                    tbGraceDays.Enabled = Enable;
                    tbAircraftType.Enabled = Enable;
                    tbTotalReqFlightHrs.Enabled = Enable;
                    chkPIC91.Enabled = Enable;
                    chkPIC135.Enabled = Enable;
                    chkSIC91.Enabled = Enable;
                    chkSIC135.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    chkSetToEndOfMonth.Enabled = Enable;
                    chkSetToNextOfMonth.Enabled = Enable;
                    chkSetToEndOfCalenderYear.Enabled = Enable;
                    ckhDisableDateCalculation.Enabled = Enable;
                    chkOneTimeEvent.Enabled = Enable;
                    //chkCompleted.Enabled = Enable;
                    chkNonConflictedEvent.Enabled = Enable;
                    chkRemoveFromCrewRosterRept.Enabled = Enable;
                    chkRemoveFromChecklistRept.Enabled = Enable;
                    chkDoNotPrintPassDueAlert.Enabled = Enable;
                    //chkPrintOneTimeEventCompleted.Enabled = Enable;
                    chkDisplayInactiveChecklist.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            EnableCheckListFields(true);
                            BindSelectedCrewChecklistItem();
                            Session["CrewCD"] = tbCrewCode.Text; //karthi
                            Session["CrewChecklistCD"] = System.Web.HttpUtility.HtmlDecode(lbCheckListCode.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Edit Row styles, based on Crew Rating.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewCheckList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem Item = (GridDataItem)e.Item;
                                string TestDueDT = ((Label)(Item[DueDT].FindControl(lbDueDT))).Text.Trim();
                                string TestAlertDT = ((Label)(Item[AlertDT].FindControl(lbAlertDT))).Text.Trim();
                                string TestGraceDT = ((Label)(Item[GraceDT].FindControl(lbGraceDT))).Text.Trim();
                                string TestPreviousDT = ((Label)(Item[PreviousCheckDT].FindControl(lbPreviousCheckDT))).Text.Trim();
                                string TestBaseDT = ((Label)(Item[BaseMonthDT].FindControl(lbBaseMonthDT))).Text.Trim();

                                if (TestDueDT != string.Empty)
                                {

                                    ((Label)(Item[DueDT].FindControl(lbDueDT))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestDueDT)));
                                    //Convert.ToDateTime(TestDueDT).ToShortDateString();
                                }

                                if (TestAlertDT != string.Empty)
                                {
                                    ((Label)(Item[AlertDT].FindControl(lbAlertDT))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestAlertDT)));
                                    //Convert.ToDateTime(TestAlertDT).ToShortDateString();
                                }

                                if (TestGraceDT != string.Empty)
                                {
                                    ((Label)(Item[GraceDT].FindControl(lbGraceDT))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestGraceDT)));
                                }

                                if (TestPreviousDT != string.Empty)
                                {
                                    ((Label)(Item[PreviousCheckDT].FindControl(lbPreviousCheckDT))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestPreviousDT)));
                                }

                                if (TestBaseDT != string.Empty)
                                {
                                    ((Label)(Item[BaseMonthDT].FindControl(lbBaseMonthDT))).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(TestBaseDT)));
                                }
                                if ((!string.IsNullOrEmpty(TestDueDT)) && (!string.IsNullOrEmpty(TestAlertDT)) && (!string.IsNullOrEmpty(TestGraceDT)))
                                {
                                    DateTime DateSource = DateTime.MinValue;
                                    DateSource = FormatDate(((Label)(Item[DueDT].FindControl(lbDueDT))).Text, DateFormat);

                                    DateTime DateSource1 = DateTime.MinValue;
                                    DateSource1 = FormatDate(((Label)(Item[AlertDT].FindControl(lbAlertDT))).Text, DateFormat);

                                    DateTime DateSource2 = DateTime.MinValue;
                                    DateSource2 = FormatDate(((Label)(Item[GraceDT].FindControl(lbGraceDT))).Text, DateFormat);

                                    if ((DateSource > System.DateTime.Now) && (DateSource1 > System.DateTime.Now) && (DateSource2 > System.DateTime.Now))
                                    {
                                        Item.BackColor = System.Drawing.Color.White;
                                    }
                                    if ((System.DateTime.Now < DateSource) && (DateSource2 > System.DateTime.Now))
                                    {
                                        Item.BackColor = System.Drawing.Color.Orange;
                                    }
                                    if ((System.DateTime.Now < DateSource) && (DateSource1 > System.DateTime.Now))
                                    {
                                        Item.BackColor = System.Drawing.Color.Yellow;
                                    }
                                    if ((System.DateTime.Now > DateSource) && (DateSource1 < System.DateTime.Now))
                                    {
                                        Item.BackColor = System.Drawing.Color.Red;
                                    }
                                }


                                if (Item.GetDataKeyValue(OriginalDT) != null)
                                {
                                    ((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue(OriginalDT).ToString()))));
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Selected Crew Checklist Grid Item in the Fields
        /// </summary>
        private void BindSelectedCrewChecklistItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    ClearCheckListFields();
                    GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                    Item.Selected = true;
                    if (Item.GetDataKeyValue("CheckListCD") != null)
                    {
                        lbCheckListCode.Text = System.Web.HttpUtility.HtmlEncode(Item.GetDataKeyValue("CheckListCD").ToString());
                    }

                    lbCheckListDescription.Text = System.Web.HttpUtility.HtmlEncode(((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text);
                    if (!string.IsNullOrEmpty(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text))
                    {
                        tbPrevious.Text = String.Format("{0:" + DateFormat + "}", FormatDate(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text, DateFormat));
                    }
                    else
                    {
                        tbPrevious.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item[DueDT].FindControl(lbDueDT)).Text))
                    {
                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", FormatDate(((Label)Item[DueDT].FindControl(lbDueDT)).Text, DateFormat));
                    }
                    else
                    {
                        tbDueNext.Text = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text))
                    {
                        tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", FormatDate(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text, DateFormat));
                    }
                    else
                    {
                        tbAlertDate.Text = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text))
                    {
                        tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", FormatDate(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text, DateFormat));
                    }
                    else
                    {
                        tbGraceDate.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                    {
                        tbAlertDays.Text = ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text;
                    }
                    else
                    {
                        tbAlertDays.Text = "0";
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                    {
                        tbGraceDays.Text = ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text;
                    }
                    else
                    {
                        tbGraceDays.Text = "0";
                    }

                    if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                    {
                        tbFreq.Text = ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text;
                    }
                    else
                    {
                        tbFreq.Text = "0";
                    }
                    if (((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text != string.Empty)
                    {
                        tbChgBaseDate.Text = String.Format("{0:" + DateFormat + "}", FormatDate(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text, DateFormat));
                    }
                    else
                    {
                        tbChgBaseDate.Text = string.Empty;
                    }
                    if (((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text != string.Empty)
                    {
                        tbOriginalDate.Text = String.Format("{0:" + DateFormat + "}", FormatDate(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text, DateFormat));
                    }
                    else
                    {
                        tbOriginalDate.Text = string.Empty;
                    }
                    if (((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text != string.Empty)
                    {
                        tbAircraftType.Text = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text;
                    }
                    else
                    {
                        tbAircraftType.Text = string.Empty;
                    }
                    if (((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text != string.Empty)
                    {
                        tbTotalReqFlightHrs.Text = ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text;
                    }
                    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                    {
                        if (((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text == "1")
                        {
                            radFreq.Items[0].Selected = true;
                        }
                        else
                        {
                            radFreq.Items[1].Selected = true;
                        }
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))) != null)
                    {
                        chkPIC91.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                    }
                    else
                    {
                        chkPIC91.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))) != null)
                    {
                        chkPIC135.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked;
                    }
                    else
                    {
                        chkPIC135.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                    {
                        chkRemoveFromChecklistRept.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                    }
                    else
                    {
                        chkRemoveFromChecklistRept.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                    {
                        chkPrintOneTimeEventCompleted.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                    }
                    else
                    {
                        chkPrintOneTimeEventCompleted.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))) != null)
                    {
                        chkSIC91.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked;
                    }
                    else
                    {
                        chkSIC91.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))) != null)
                    {
                        chkSIC135.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked;
                    }
                    else
                    {
                        chkSIC135.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                    {
                        chkInactive.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                    {
                        chkSetToEndOfMonth.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                    }
                    else
                    {
                        chkSetToEndOfMonth.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                    {
                        ckhDisableDateCalculation.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                    }
                    else
                    {
                        ckhDisableDateCalculation.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                    {
                        chkOneTimeEvent.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                    }
                    else
                    {
                        chkOneTimeEvent.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                    {
                        chkCompleted.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                    }
                    else
                    {
                        chkCompleted.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                    {
                        chkNonConflictedEvent.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                    }
                    else
                    {
                        chkNonConflictedEvent.Checked = false;
                    }

                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))) != null)
                    {
                        chkRemoveFromCrewRosterRept.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked;
                    }
                    else
                    {
                        chkRemoveFromCrewRosterRept.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                    {
                        chkChecklist.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                    }
                    else
                    {
                        chkChecklist.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                    {
                        chkDoNotPrintPassDueAlert.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                    }
                    else
                    {
                        chkDoNotPrintPassDueAlert.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                    {
                        chkSetToNextOfMonth.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                    }
                    else
                    {
                        chkSetToNextOfMonth.Checked = false;
                    }


                    if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                    {
                        chkSetToEndOfCalenderYear.Checked = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                    }
                    else
                    {
                        chkSetToEndOfCalenderYear.Checked = false;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Crew Checklist Grid based on Crew Code
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private void BindCrewCheckListGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CrewCheckList"] != null)
                        {
                            List<GetCrewCheckListDate> CrewCheckList = (List<GetCrewCheckListDate>)Session["CrewCheckList"];
                            dgCrewCheckList.DataSource = CrewCheckList;
                            dgCrewCheckList.DataBind();

                            // Bind Crew Checklist Currency Tab Grid
                            dgCurrencyChecklist.DataSource = CrewCheckList;
                            dgCurrencyChecklist.DataBind();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                            {

                                FlightPakMasterService.GetCrewCheckListDate CrewChecklistDate = new FlightPakMasterService.GetCrewCheckListDate();
                                CrewChecklistDate.CrewID = CrewID;
                                CrewChecklistDate.CheckListCD = "null";
                                CrewChecklistDate.CrewChecklistDescription = "null";

                                var CheckListValue = CrewRosterService.GetCrewCheckListDate(CrewChecklistDate);
                                List<FlightPakMasterService.GetCrewCheckListDate> CrewChecklistDateList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                                if (CheckListValue.ReturnFlag == true)
                                {
                                    CrewChecklistDateList = CheckListValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                    dgCrewCheckList.DataSource = CrewChecklistDateList;
                                    dgCrewCheckList.DataBind();
                                    Session["CrewCheckList"] = CrewChecklistDateList;

                                    // Bind Crew Checklist Currency Tab Grid
                                    dgCurrencyChecklist.DataSource = CheckListValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.GetCrewCheckListDate>();
                                    dgCurrencyChecklist.DataBind();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }



        }



        #endregion

        #region "Crew Roster Visa Grid Events"

        /// <summary>
        /// Bind Crew Additional Information into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Visa_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // BindCrewVisaGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Crew Visa based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindCrewVisaGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewVisa"] != null)
                    {
                        List<GetAllCrewPaxVisa> CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["CrewVisa"];
                        dgVisa.DataSource = CrewPaxVisaList;
                        dgVisa.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                        {
                            CrewPassengerVisa CrewVisa = new CrewPassengerVisa();
                            CrewVisa.CrewID = CrewID;  // Convert.ToInt64(Item.GetDataKeyValue("CrewID"));
                            CrewVisa.PassengerRequestorID = null;
                            var CrewPaxVisaList = Service.GetCrewVisaListInfo(CrewVisa);

                            if (CrewPaxVisaList.ReturnFlag == true)
                            {
                                List<GetAllCrewPaxVisa> CrewPassengerVisaList = new List<GetAllCrewPaxVisa>();
                                CrewPassengerVisaList = CrewPaxVisaList.EntityList;
                                dgVisa.DataSource = CrewPassengerVisaList;
                                dgVisa.DataBind();
                                Session["CrewVisa"] = CrewPassengerVisaList;
                            }
                        }
                    }
                    foreach (GridDataItem DateItem in dgVisa.MasterTableView.Items)
                    {
                        if (!string.IsNullOrEmpty(((TextBox)DateItem["ExpiryDT"].FindControl("tbExpiryDT")).Text))
                        {
                            ((TextBox)DateItem["ExpiryDT"].FindControl("tbExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)DateItem["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                        }
                        if (!string.IsNullOrEmpty(((TextBox)DateItem["IssueDate"].FindControl("tbIssueDate")).Text))
                        {
                            ((TextBox)DateItem["IssueDate"].FindControl("tbIssueDate")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)DateItem["IssueDate"].FindControl("tbIssueDate")).Text);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }

        #endregion

        #region "Crew Roster Passport Grid Events"

        /// <summary>
        /// Bind Crew Passport into the Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Passport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //  BindCrewPassportGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Bind Passport Info into Grid based on Crew Code
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>
        private void BindCrewPassportGrid(Int64 CrewID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["CrewPassport"] != null)
                    {
                        List<GetAllCrewPassport> CrewPassportList = (List<GetAllCrewPassport>)Session["CrewPassport"];
                        dgPassport.DataSource = CrewPassportList;
                        dgPassport.DataBind();
                    }
                    else
                    {
                        using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                        {
                            CrewPassengerPassport CrewPassport = new CrewPassengerPassport();
                            CrewPassport.CrewID = CrewID;
                            CrewPassport.PassengerRequestorID = null;
                            var CrewPaxPassportValue = CrewRosterService.GetCrewPassportListInfo(CrewPassport);

                            if (CrewPaxPassportValue.ReturnFlag == true)
                            {
                                List<GetAllCrewPassport> CrewPassportList = new List<GetAllCrewPassport>();
                                CrewPassportList = CrewPaxPassportValue.EntityList;
                                dgPassport.DataSource = CrewPassportList;
                                dgPassport.DataBind();
                                Session["CrewPassport"] = CrewPassportList;
                            }
                        }
                    }
                    foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
                    {
                        if (!string.IsNullOrEmpty(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text))
                        {
                            ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text))
                        {
                            ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text = String.Format("{0:" + DateFormat + "}", ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion

        #region"Crew Roaster Currency Checklist Grid Events"

        /// <summary>
        /// Bind Currency Checklist based on Crew Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CurrencyChecklist_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // BindCrewCheckListGrid(0);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void CurrencyChecklist_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (!string.IsNullOrEmpty(((Label)Item[DueDT].FindControl(lbDueDT)).Text))
                            {
                                ((Label)Item[DueDT].FindControl(lbDueDT)).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(((Label)Item[DueDT].FindControl(lbDueDT)).Text))));
                            }
                            if (!string.IsNullOrEmpty(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text))
                            {
                                ((Label)Item[AlertDT].FindControl(lbAlertDT)).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text))));
                            }
                            if (!string.IsNullOrEmpty(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text))
                            {
                                ((Label)Item[GraceDT].FindControl(lbGraceDT)).Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text))));
                            }

                            using (FlightPakMasterService.MasterCatalogServiceClient CurrentFltService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                foreach (GridDataItem CheckItem in dgCrewCheckList.Items)
                                {
                                    if (((Label)CheckItem["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text == ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text)
                                    {
                                        e.Item.BackColor = CheckItem.BackColor;
                                        Int64 TempCrewID = 0;
                                        if (Session["SelectedCrewRosterID"] != null)
                                        {
                                            TempCrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                                        }
                                        var CurrentFltValues = CurrentFltService.GetCurrentFltHoursInfo(TempCrewID).EntityList.Where(x => x.CheckListCD.ToString().ToUpper().Trim().Equals(CheckItem.GetDataKeyValue("CheckListCD").ToString().ToUpper().Trim())).ToList();
                                        if (CurrentFltValues != null && CurrentFltValues.Count > 0)
                                        {
                                            if (((FlightPakMasterService.GetAllCurrentFlightHours)CurrentFltValues[0]).FLIGHTHOURS != null)
                                            {
                                                ((Label)Item[CurrentFltHrs].FindControl(lbCurrentFltHrs)).Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.GetAllCurrentFlightHours)CurrentFltValues[0]).FLIGHTHOURS.ToString());
                                            }
                                            else
                                            {
                                                ((Label)Item[CurrentFltHrs].FindControl(lbCurrentFltHrs)).Text = "0.0";
                                            }
                                        }
                                        else
                                        {
                                            ((Label)Item[CurrentFltHrs].FindControl(lbCurrentFltHrs)).Text = "0.0";
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }
        #endregion

        #region"Crew Roaster Currency Grid Events"

        ///// <summary>
        ///// Bind Currency based on Crew Code
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void Currency_BindData(object sender, GridNeedDataSourceEventArgs e)
        //{

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {

        //                if (Session["SelectedCrewRosterID"] != null)
        //                {
        //                    BindCrewCurrency(Convert.ToInt64(Session["SelectedCrewRosterID"].ToString()));
        //                }
        //                else
        //                {
        //                    dgCurrency.Columns[0].HeaderText = "Aircraft Type";
        //                    dgCurrency.Columns[1].HeaderText = "L/D 3";
        //                    dgCurrency.Columns[2].HeaderText = "L/N 3";
        //                    dgCurrency.Columns[3].HeaderText = "T/D 90";
        //                    dgCurrency.Columns[4].HeaderText = "T/N 90";
        //                    dgCurrency.Columns[5].HeaderText = "Appr 3";
        //                    dgCurrency.Columns[6].HeaderText = "Instr 3";
        //                    dgCurrency.Columns[5].HeaderText = "Appr 3";
        //                    dgCurrency.Columns[6].HeaderText = "Instr 3";
        //                    dgCurrency.Columns[7].HeaderText = "Cal Mon.";
        //                    dgCurrency.Columns[8].HeaderText = "90 Days";
        //                    dgCurrency.Columns[9].HeaderText = "Cal Qtr.";
        //                    dgCurrency.Columns[10].HeaderText = "Cal Qtr. Rest";
        //                    dgCurrency.Columns[11].HeaderText = "6 Months";
        //                    dgCurrency.Columns[12].HeaderText = "Prev 2 Qtrs";
        //                    dgCurrency.Columns[13].HeaderText = "12 Months";
        //                    dgCurrency.Columns[14].HeaderText = "Cal. Yr";
        //                    dgCurrency.Columns[15].HeaderText = "365 Days";
        //                    dgCurrency.Columns[16].HeaderText = "30 Days";
        //                    BindCrewCurrency(0);
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);

        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
        //        }
        //    }

        //}

        private void BindCrewCurrency(Int64 CrewID)
        {
            using (MasterCatalogServiceClient CrewrCurrencyService = new MasterCatalogServiceClient())
            {
                GetCompanyWithFilters Company = new GetCompanyWithFilters();
                if (CrewID > 0)
                {
                    var CompanyCurrencyList = CrewrCurrencyService.GetCompanyWithFilters(tbHomeBase.Text.ToString(),0,true,false).EntityList;
                    if (CompanyCurrencyList.Count() != 0 && CompanyCurrencyList != null)
                    {
                        using (MasterCatalogServiceClient CrewRoasterCurrencyService = new MasterCatalogServiceClient())
                        {
                            var CurrencyList = CrewRoasterCurrencyService.GetAllCrewCurrency(CrewID);
                            if (CurrencyList.ReturnFlag == true)
                            {
                                dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).DayLandingMinimum != null)
                                {
                                    dgCurrency.Columns[1].HeaderText = "L/D" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).DayLandingMinimum.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[1].HeaderText = "L/D 3";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).NightllandingMinimum != null)
                                {
                                    dgCurrency.Columns[2].HeaderText = "L/N" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).NightllandingMinimum.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[2].HeaderText = "L/N 3";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).DayLanding != null)
                                {
                                    dgCurrency.Columns[3].HeaderText = "T/D" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).DayLanding.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[3].HeaderText = "T/D 90";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).NightLanding != null)
                                {
                                    dgCurrency.Columns[4].HeaderText = "T/N" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).NightLanding.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[4].HeaderText = "T/N 90";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).ApproachMinimum != null)
                                {
                                    dgCurrency.Columns[5].HeaderText = "Appr" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).ApproachMinimum.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[5].HeaderText = "Appr 3";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).InstrumentMinimum != null)
                                {
                                    dgCurrency.Columns[6].HeaderText = "Instr" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).InstrumentMinimum.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[6].HeaderText = "Instr 3";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day7 != null)
                                {
                                    dgCurrency.Columns[5].HeaderText = "Appr" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day7.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[5].HeaderText = "Appr 3";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).RestDaysMinimum != null)
                                {
                                    dgCurrency.Columns[6].HeaderText = "Instr" + ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).RestDaysMinimum.ToString();
                                }
                                else
                                {
                                    dgCurrency.Columns[6].HeaderText = "Instr 3";
                                }
                                dgCurrency.Columns[7].HeaderText = "Cal Mon.";
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day90 != null)
                                {
                                    dgCurrency.Columns[8].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day90.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[8].HeaderText = "90 Days";
                                }
                                dgCurrency.Columns[9].HeaderText = "Cal Qtr.";
                                dgCurrency.Columns[10].HeaderText = "Cal Qtr. Rest";
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Month6 != null)
                                {
                                    dgCurrency.Columns[11].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Month6.ToString() + "Months";
                                }
                                else
                                {
                                    dgCurrency.Columns[11].HeaderText = "6 Months";
                                }

                                dgCurrency.Columns[12].HeaderText = "Prev 2 Qtrs";

                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Month12 != null)
                                {
                                    dgCurrency.Columns[13].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Month12.ToString() + "Months";
                                }
                                else
                                {
                                    dgCurrency.Columns[13].HeaderText = "12 Months";
                                }
                                dgCurrency.Columns[14].HeaderText = "Cal. Yr";
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day365 != null)
                                {
                                    dgCurrency.Columns[15].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day365.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[15].HeaderText = "365 Days";
                                }
                                if (((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day30 != null)
                                {
                                    dgCurrency.Columns[16].HeaderText = ((FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters)CompanyCurrencyList[0]).Day30.ToString() + "Days";
                                }
                                else
                                {
                                    dgCurrency.Columns[16].HeaderText = "30 Days";
                                }
                                dgCurrency.DataSource = CurrencyList.EntityList;
                                dgCurrency.DataBind();
                            }
                            else
                            {
                                dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                                dgCurrency.Columns[1].HeaderText = "L/D 3";
                                dgCurrency.Columns[2].HeaderText = "L/N 3";
                                dgCurrency.Columns[3].HeaderText = "T/D 90";
                                dgCurrency.Columns[4].HeaderText = "T/N 90";
                                dgCurrency.Columns[5].HeaderText = "Appr 3";
                                dgCurrency.Columns[6].HeaderText = "Instr 3";
                                dgCurrency.Columns[5].HeaderText = "Appr 3";
                                dgCurrency.Columns[6].HeaderText = "Instr 3";
                                dgCurrency.Columns[7].HeaderText = "Cal Mon.";
                                dgCurrency.Columns[8].HeaderText = "90 Days";
                                dgCurrency.Columns[9].HeaderText = "Cal Qtr.";
                                dgCurrency.Columns[10].HeaderText = "Cal Qtr. Rest";
                                dgCurrency.Columns[11].HeaderText = "6 Months";
                                dgCurrency.Columns[12].HeaderText = "Prev 2 Qtrs";
                                dgCurrency.Columns[13].HeaderText = "12 Months";
                                dgCurrency.Columns[14].HeaderText = "Cal. Yr";
                                dgCurrency.Columns[15].HeaderText = "365 Days";
                                dgCurrency.Columns[16].HeaderText = "30 Days";
                                dgCurrency.DataSource = CurrencyList.EntityList;
                                dgCurrency.DataBind();
                            }
                        }
                    }
                    else
                    {
                        dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                        dgCurrency.Columns[1].HeaderText = "L/D 3";
                        dgCurrency.Columns[2].HeaderText = "L/N 3";
                        dgCurrency.Columns[3].HeaderText = "T/D 90";
                        dgCurrency.Columns[4].HeaderText = "T/N 90";
                        dgCurrency.Columns[5].HeaderText = "Appr 3";
                        dgCurrency.Columns[6].HeaderText = "Instr 3";
                        dgCurrency.Columns[5].HeaderText = "Appr 3";
                        dgCurrency.Columns[6].HeaderText = "Instr 3";
                        dgCurrency.Columns[7].HeaderText = "Cal Mon.";
                        dgCurrency.Columns[8].HeaderText = "90 Days";
                        dgCurrency.Columns[9].HeaderText = "Cal Qtr.";
                        dgCurrency.Columns[10].HeaderText = "Cal Qtr. Rest";
                        dgCurrency.Columns[11].HeaderText = "6 Months";
                        dgCurrency.Columns[12].HeaderText = "Prev 2 Qtrs";
                        dgCurrency.Columns[13].HeaderText = "12 Months";
                        dgCurrency.Columns[14].HeaderText = "Cal. Yr";
                        dgCurrency.Columns[15].HeaderText = "365 Days";
                        dgCurrency.Columns[16].HeaderText = "30 Days";
                        using (MasterCatalogServiceClient CrewRoasterCurrencyService = new MasterCatalogServiceClient())
                        {
                            var CurrencyList = CrewRoasterCurrencyService.GetAllCrewCurrency(CrewID);
                            dgCurrency.DataSource = CurrencyList.EntityList;
                            dgCurrency.DataBind();
                        }
                    }
                }
                else
                {
                    dgCurrency.Columns[0].HeaderText = "Aircraft Type";
                    dgCurrency.Columns[1].HeaderText = "L/D 3";
                    dgCurrency.Columns[2].HeaderText = "L/N 3";
                    dgCurrency.Columns[3].HeaderText = "T/D 90";
                    dgCurrency.Columns[4].HeaderText = "T/N 90";
                    dgCurrency.Columns[5].HeaderText = "Appr 3";
                    dgCurrency.Columns[6].HeaderText = "Instr 3";
                    dgCurrency.Columns[5].HeaderText = "Appr 3";
                    dgCurrency.Columns[6].HeaderText = "Instr 3";
                    dgCurrency.Columns[7].HeaderText = "Cal Mon.";
                    dgCurrency.Columns[8].HeaderText = "90 Days";
                    dgCurrency.Columns[9].HeaderText = "Cal Qtr.";
                    dgCurrency.Columns[10].HeaderText = "Cal Qtr. Rest";
                    dgCurrency.Columns[11].HeaderText = "6 Months";
                    dgCurrency.Columns[12].HeaderText = "Prev 2 Qtrs";
                    dgCurrency.Columns[13].HeaderText = "12 Months";
                    dgCurrency.Columns[14].HeaderText = "Cal. Yr";
                    dgCurrency.Columns[15].HeaderText = "365 Days";
                    dgCurrency.Columns[16].HeaderText = "30 Days";
                    using (MasterCatalogServiceClient CrewRoasterCurrencyService = new MasterCatalogServiceClient())
                    {
                        var CurrencyList = CrewRoasterCurrencyService.GetAllCrewCurrency(CrewID);
                        dgCurrency.DataSource = CurrencyList.EntityList;
                        dgCurrency.DataBind();
                    }
                }
            }
        }

        /// <summary>
        /// Method to Bind Crew Currency based on Crew Code
        /// </summary>
        /// <param name="CrewCode"></param>
        //private void BindCrewCurrency(string CrewCode)
        //{
        //    //dgCurrency.DataSource = null;
        //    //dgCurrency.DataBind();
        //}
        #endregion




        #region "Button Events"

        /// <summary>
        /// Command Event Trigger for Save or Update Crew Roster Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            Save();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }




        /// <summary>
        /// Event Trigger for Save or Update Crew Roster Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChangesTop_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Save();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Cancel Crew Roster Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Cancel Crew Roster Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelTop_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Delete Selected Crew Roster Additional Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteInfo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgCrewAddlInfo.SelectedItems[0];
                        Int64 CrewInfoXRefID = Convert.ToInt64(item.GetDataKeyValue("CrewInfoXRefID").ToString());
                        string CrewInfoCD = item.GetDataKeyValue("CrewInfoCD").ToString();
                        List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionList = new List<FlightPakMasterService.GetCrewDefinition>();
                        CrewDefinitionList = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                        for (int Index = 0; Index < CrewDefinitionList.Count; Index++)
                        {
                            if (CrewDefinitionList[Index].CrewInfoCD == CrewInfoCD)
                            {
                                CrewDefinitionList[Index].IsDeleted = true;
                            }
                        }
                        Session["CrewAddInfo"] = CrewDefinitionList;
                        List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewDefinition>();
                        // Retain the Grid Items and bind into List to remove the selected item.
                        CrewDefinitionInfoList = RetainCrewDefinitionGrid(dgCrewAddlInfo);

                        dgCrewAddlInfo.DataSource = CrewDefinitionInfoList.Where(x => x.IsDeleted == false).ToList();
                        dgCrewAddlInfo.DataBind();
                        Session["CrewAddInfo"] = CrewDefinitionList;



                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Delete Selected Crew Roster Rating Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteRating_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgTypeRating.SelectedItems[0];
                            Int64 CrewRatingID = Convert.ToInt64(item.GetDataKeyValue("CrewRatingID").ToString());
                            string AircraftTypeCD = item.GetDataKeyValue("AircraftTypeCD").ToString();
                            List<FlightPakMasterService.GetCrewRating> CrewRatingList = new List<FlightPakMasterService.GetCrewRating>();
                            CrewRatingList = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];
                            for (int Index = 0; Index < CrewRatingList.Count; Index++)
                            {
                                if (CrewRatingList[Index].AircraftTypeCD == AircraftTypeCD)
                                {
                                    CrewRatingList[Index].IsDeleted = true;
                                }
                            }
                            Session["CrewTypeRating"] = CrewRatingList;

                            //List<FlightPakMasterService.GetCrewRating> CrewRatingInfoList = new List<FlightPakMasterService.GetCrewRating>();

                            //CrewRatingInfoList = RetainCrewTypeRatingGrid(dgTypeRating);

                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<GetCrewRating> CrewRatingInfoList = new List<GetCrewRating>();
                            CrewRatingInfoList = RetainCrewTypeRatingGrid(dgTypeRating);
                            for (int Index = 0; Index < CrewRatingInfoList.Count; Index++)
                            {
                                if (CrewRatingInfoList[Index].AircraftTypeCD == AircraftTypeCD)
                                {
                                    CrewRatingInfoList[Index].IsDeleted = true;
                                }
                            }
                            //CrewPaxList.RemoveAll(x => x.VisaNum.ToString().Trim().ToUpper() == VisaNumber.Trim().ToUpper());

                            dgTypeRating.DataSource = CrewRatingInfoList.Where(x => x.IsDeleted == false).ToList();
                            dgTypeRating.DataBind();

                            DefaultTypeRatingSelection();
                            Session["CrewTypeRating"] = CrewRatingList;
                            EnableHours(false);
                            btnFlightExp.Text = "Modify Beginning Flt. Experience";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Delete Selected Crew Checklist Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteChecklist_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                        Int64 ChecklistID = Convert.ToInt64(item.GetDataKeyValue("CheckListID").ToString());
                        string CheckListCD = item.GetDataKeyValue("CheckListCD").ToString();
                        List<FlightPakMasterService.GetCrewCheckListDate> CrewCheckListDateList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                        CrewCheckListDateList = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                        for (int Index = 0; Index < CrewCheckListDateList.Count; Index++)
                        {
                            if (CrewCheckListDateList[Index].CheckListCD == CheckListCD)
                            {
                                CrewCheckListDateList[Index].IsDeleted = true;
                            }
                        }
                        Session["CrewCheckList"] = CrewCheckListDateList;
                        List<FlightPakMasterService.GetCrewCheckListDate> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                        // Retain the Grid Items and bind into List to remove the selected item.
                        CrewDefinitionInfoList = RetainCrewCheckListGrid(dgCrewCheckList);
                        dgCrewCheckList.DataSource = CrewDefinitionInfoList.Where(x => x.IsDeleted == false).ToList();
                        dgCrewCheckList.DataBind();

                        Session["CrewCheckList"] = CrewCheckListDateList;
                        DefaultChecklistSelection();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Method to enable / disable Textbox controls for Flight Experience
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FlightExp_Click(object sender, EventArgs e)
        {
            if (dgTypeRating.SelectedItems.Count > 0)
            {
                if (btnFlightExp.Text == "Modify Beginning Flt. Experience")
                {
                    EnableHours(true);
                    pnlBeginning.Visible = true;
                    pnlCurrent.Visible = false;
                    btnFlightExp.Text = "Display Current Flt.Experience";
                }
                else
                {
                    EnableHours(false);
                    pnlCurrent.Visible = true;
                    pnlBeginning.Visible = false;
                    btnFlightExp.Text = "Modify Beginning Flt. Experience";
                }
            }

        }

        /// <summary>
        /// Method to Add New Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (ValidateVisa())
                        {
                            // Declaring Lists
                            List<GetAllCrewPaxVisa> FinalList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> RetainList = new List<GetAllCrewPaxVisa>();
                            List<GetAllCrewPaxVisa> NewList = new List<GetAllCrewPaxVisa>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewVisaGrid(dgVisa);
                            FinalList.AddRange(RetainList);
                            // Bind Selected New Item and add into Final List

                            GetAllCrewPaxVisa CrewPaxVisa = new GetAllCrewPaxVisa();
                            CrewPaxVisa.VisaID = 0;

                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                CrewPaxVisa.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                            }
                            else { CrewPaxVisa.CrewID = 0; }
                            CrewPaxVisa.PassengerRequestorID = null;
                            NewList.Add(CrewPaxVisa);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["CrewVisa"] = FinalList;      //Commented for testing Delete & add scenario
                            dgVisa.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.Rebind();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to validate Issue Date and Expiry Date in the Visa grid
        /// </summary>
        protected bool ValidateVisa()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string VisaNum = string.Empty;
                    string CountryCode = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    foreach (GridDataItem item in dgVisa.Items)
                    {
                        VisaNum = ((TextBox)item.FindControl("tbVisaNum")).Text;
                        CountryCode = ((TextBox)item.FindControl("tbCountryCD")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDate")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbExpiryDT")).Text;
                        if (String.IsNullOrEmpty(VisaNum))
                        {
                            string alertMsg = "radalert('Visa No. should not be empty', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            DateTime DateStart = new DateTime();
                            DateTime DateEnd = new DateTime();

                            if (!string.IsNullOrEmpty(IssueDate))
                            {
                                if (DateFormat != null)
                                {
                                    DateStart = FormatDate(IssueDate, DateFormat);
                                }
                                else
                                {
                                    DateStart = Convert.ToDateTime(IssueDate);
                                }
                            }
                            if (!string.IsNullOrEmpty(ExpiryDT))
                            {
                                if (DateFormat != null)
                                {
                                    DateEnd = FormatDate(ExpiryDT, DateFormat);
                                }
                                else
                                {
                                    DateEnd = Convert.ToDateTime(ExpiryDT);
                                }
                            }

                            if (!String.IsNullOrEmpty(IssueDate))
                            {
                                if (DateStart >= DateTime.Today)
                                {
                                    string alertMsg = "radalert('Visa Issue Date should not be future date.', 360, 50, 'Crew');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    return false;
                                }
                            }
                            if (DateStart > DateEnd)
                            {
                                string alertMsg = "radalert('Visa Issue Date should be before Expiry Date', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to Delete Visa Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteVisa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgVisa.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgVisa.SelectedItems[0];
                            string VisaNumber = ((TextBox)(Item["VisaNum"].FindControl("tbVisaNum"))).Text;
                            List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                            CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["CrewVisa"];
                            for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                            {
                                if (CrewPaxVisaList[Index].VisaNum == VisaNumber)
                                {
                                    CrewPaxVisaList[Index].IsDeleted = true;
                                }
                            }
                            Session["CrewVisa"] = CrewPaxVisaList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<GetAllCrewPaxVisa> CrewPaxList = new List<GetAllCrewPaxVisa>();
                            CrewPaxList = RetainCrewVisaGrid(dgVisa);
                            for (int Index = 0; Index < CrewPaxList.Count; Index++)
                            {
                                if (CrewPaxList[Index].VisaNum == VisaNumber)
                                {
                                    CrewPaxList[Index].IsDeleted = true;
                                }
                            }
                            //CrewPaxList.RemoveAll(x => x.VisaNum.ToString().Trim().ToUpper() == VisaNumber.Trim().ToUpper());
                            dgVisa.DataSource = CrewPaxList.Where(x => x.IsDeleted != true).ToList();
                            dgVisa.DataBind();
                            Session["CrewVisa"] = CrewPaxVisaList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PassengerRequestor);
                }
            }
        }

        /// <summary>
        /// Method for Add New Passport Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void AddPassport_Click(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                // Declaring Lists
        //                List<GetAllCrewPassport> FinalList = new List<GetAllCrewPassport>();
        //                List<GetAllCrewPassport> RetainList = new List<GetAllCrewPassport>();
        //                List<GetAllCrewPassport> NewList = new List<GetAllCrewPassport>();

        //                // Retain Grid Items and bind into List and add into Final List
        //                RetainList = RetainCrewPassportGrid(dgPassport);
        //                FinalList.AddRange(RetainList);

        //                // Bind Selected New Item and add into Final List
        //                GetAllCrewPassport CrewPaxPassport = new GetAllCrewPassport();
        //                CrewPaxPassport.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
        //                //CrewPaxPassport.Choice = false;
        //                //CrewPaxPassport.IssueCity = "";
        //                //CrewPaxPassport.PassportNum = "";
        //                //CrewPaxPassport.LastUpdUID = "";
        //                ////CrewPaxPassport.CountryID = 0;
        //                //CrewPaxPassport.PassportExpiryDT = null;
        //                //CrewPaxPassport.IssueDT = null;
        //                //CrewPaxPassport.IsDeleted = false;

        //                NewList.Add(CrewPaxPassport);
        //                FinalList.AddRange(NewList);

        //                // Bind final list into Session
        //                Session["CrewPassport"] = FinalList;

        //                dgPassport.DataSource = FinalList;
        //                dgPassport.Rebind();
        //            }, FlightPak.Common.Constants.Policy.UILayer);

        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
        //        }
        //    }
        //}

        protected void AddPassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ValidatePassport())
                        {


                            List<GetAllCrewPassport> FinalList = new List<GetAllCrewPassport>();
                            List<GetAllCrewPassport> RetainList = new List<GetAllCrewPassport>();
                            List<GetAllCrewPassport> NewList = new List<GetAllCrewPassport>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewPassportGrid(dgPassport);
                            FinalList.AddRange(RetainList);


                            GetAllCrewPassport CrewPaxVisa = new GetAllCrewPassport();
                            CrewPaxVisa.PassportID = 0;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                CrewPaxVisa.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                            }
                            else
                            {
                                CrewPaxVisa.CrewID = 0;
                            }
                            CrewPaxVisa.PassengerRequestorID = null;
                            NewList.Add(CrewPaxVisa);
                            FinalList.AddRange(NewList);
                            // Bind final list into Session
                            //Session["CrewVisa"] = FinalList;      //Commented for testing Delete & add scenario
                            dgPassport.DataSource = FinalList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected bool ValidatePassport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    string PassportNum = string.Empty;
                    string IssueDate = string.Empty;
                    string ExpiryDT = string.Empty;
                    foreach (GridDataItem item in dgPassport.Items)
                    {
                        PassportNum = ((TextBox)item.FindControl("tbPassportNum")).Text;
                        IssueDate = ((TextBox)item.FindControl("tbIssueDT")).Text;
                        ExpiryDT = ((TextBox)item.FindControl("tbPassportExpiryDT")).Text;
                        if (String.IsNullOrEmpty(PassportNum))
                        {
                            string alertMsg = "radalert('Passport No. should not be empty', 360, 50, 'Crew');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return false;
                        }
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!(string.IsNullOrEmpty(IssueDate)))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = FormatDate(IssueDate, DateFormat);
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(IssueDate);
                            }
                        }
                        if (!(string.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = FormatDate(ExpiryDT, DateFormat);
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(ExpiryDT);
                            }
                        }
                        if (!String.IsNullOrEmpty(IssueDate))
                        {
                            if (DateStart >= DateTime.Today)
                            {
                                string alertMsg = "radalert('Passport Issue Date should not be future date.', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                        if (!(String.IsNullOrEmpty(IssueDate)) && !(String.IsNullOrEmpty(ExpiryDT)))
                        {
                            if (DateStart > DateEnd)
                            {
                                string alertMsg = "radalert('Passport Issue Date should be before Expiry Date', 360, 50, 'Crew');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                return false;
                            }
                        }
                    }
                    return true;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void DeletePassport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPassport.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgPassport.SelectedItems[0];
                            string PassportNumber = ((TextBox)(Item.FindControl("tbPassportNum"))).Text.Trim();
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> CrewPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            CrewPassportList = (List<FlightPakMasterService.GetAllCrewPassport>)Session["CrewPassport"];
                            for (int Index = 0; Index < CrewPassportList.Count; Index++)
                            {
                                if (CrewPassportList[Index].PassportNum == PassportNumber)
                                {
                                    CrewPassportList[Index].IsDeleted = true;
                                }
                            }
                            Session["CrewPassport"] = CrewPassportList;
                            // Retain the Grid Items and bind into List to remove the selected item.
                            List<FlightPakMasterService.GetAllCrewPassport> PassengerPassportList = new List<FlightPakMasterService.GetAllCrewPassport>();
                            PassengerPassportList = RetainCrewPassportGrid(dgPassport);
                            for (int Index = 0; Index < PassengerPassportList.Count; Index++)
                            {
                                if (PassengerPassportList[Index].PassportNum == PassportNumber)
                                {
                                    PassengerPassportList[Index].IsDeleted = true;
                                }
                            }
                            //PassengerPassportList.RemoveAll(x => x.PassportNum.ToString().Trim().ToUpper() == PassportNumber.Trim().ToUpper());
                            dgPassport.DataSource = PassengerPassportList.Where(x => x.IsDeleted != true).ToList();
                            dgPassport.DataBind();
                            Session["CrewPassport"] = CrewPassportList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to validate country code in the Visa grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryCD_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox tbCountryCD = new TextBox();
                        int RowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgVisa.Items.Count - 1; i += 1)
                        {

                            tbCountryCD.Text = ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Text;

                            if (tbCountryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CountryValue = CountryService.GetCountryWithFilters(tbCountryCD.Text.Trim(),0).EntityList;
                                    if (CountryValue.Count() <= 0)
                                    {
                                        string AlertMsg = "radalert('Invalid Country Code', 360, 50, 'Crew Roaster');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                        ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Text = string.Empty;
                                        ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Focus();
                                    }
                                    else
                                    {

                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)CountryValue.ToList();
                                        ((TextBox)dgVisa.Items[RowIndex].Cells[0].FindControl("tbCountryCD")).Text = CountryList[0].CountryCD;
                                        ((HiddenField)dgVisa.Items[RowIndex].Cells[0].FindControl("hdnCountryID")).Value = CountryList[0].CountryID.ToString();
                                    }
                                }
                            }
                            RowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Method to validate country code in the Passport grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Nation_TextChanged(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TextBox tbCountryCD = new TextBox();
                        int RowIndex = 0;
                        int i = 0;
                        for (i = 0; i <= dgPassport.Items.Count - 1; i += 1)
                        {

                            tbCountryCD.Text = ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Text;

                            if (tbCountryCD.Text != string.Empty)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CountryValue = CountryService.GetCountryWithFilters(tbCountryCD.Text.Trim(),0).EntityList;
                                    if (CountryValue.Count() <= 0)
                                    {
                                        string alertMsg = "radalert('Invalid Country Code', 360, 50, 'Crew Roaster');";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                        ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Text = string.Empty;
                                        ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Focus();
                                    }
                                    else
                                    {
                                        List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                        CountryList = (List<FlightPakMasterService.Country>)CountryValue.ToList();
                                        ((TextBox)dgPassport.Items[RowIndex].Cells[0].FindControl("tbPassportCountry")).Text = CountryList[0].CountryCD;
                                        ((HiddenField)dgPassport.Items[RowIndex].Cells[0].FindControl("hdnPassportCountry")).Value = CountryList[0].CountryID.ToString();
                                    }
                                }
                            }
                            RowIndex += 1;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        /// <summary>
        /// Method to change the other dates based ChgBaseDate Value and show the value in grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChgBaseDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ChgBaseDateDateCalculation();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        private void ChgBaseDateDateCalculation()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        if (ckhDisableDateCalculation.Checked == false)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];

                            ((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);


                            tbDueNext.Text = tbChgBaseDate.Text;
                            tbGraceDate.Text = tbChgBaseDate.Text;
                            tbAlertDate.Text = tbChgBaseDate.Text;

                            ((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text = System.Web.HttpUtility.HtmlEncode(tbOriginalDate.Text);
                            ((Label)Item[DueDT].FindControl(lbDueDT)).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            ((Label)Item[GraceDT].FindControl(lbGraceDT)).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);
                            ((Label)Item[AlertDT].FindControl(lbAlertDT)).Text = System.Web.HttpUtility.HtmlEncode(tbChgBaseDate.Text);



                            ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                            ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);


                            GraceDaysFunction();
                            AlertDaysFunction();


                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }


        protected void ChgBaseDate_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ChgBaseDateDateCalculation();
                        CheckItemInGrid();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }



        protected void OriginalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text = System.Web.HttpUtility.HtmlEncode(tbOriginalDate.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        /// <summary>
        /// Method to change increase day or months to other dates and show the value in grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Freq_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false)
                            {


                                if (tbChgBaseDate.Text.Trim() != string.Empty)
                                {
                                    GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                    if (radFreq.SelectedItem.Text == "Months")
                                    {
                                        int Alerts = 0;
                                        int GraceDay = 0;
                                        int Months = 0;

                                        if (tbFreq.Text.Trim() != "")
                                        {
                                            Months = Convert.ToInt32(tbFreq.Text);
                                        }

                                        if (tbAlertDays.Text.Trim() != "")
                                        {
                                            Alerts = Convert.ToInt32(tbAlertDays.Text);
                                        }


                                        if (tbGraceDays.Text.Trim() != "")
                                        {
                                            GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                        }

                                        DateTime DateSource = DateTime.MinValue;
                                        DateTime DueNext = DateTime.MinValue;
                                        DateTime Alert = DateTime.MinValue;
                                        DateTime Grace = DateTime.MinValue;
                                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);

                                        DateSource = FormatDate(tbChgBaseDate.Text, DateFormat);
                                        DueNext = FormatDate(tbChgBaseDate.Text, DateFormat);
                                        Alert = FormatDate(tbChgBaseDate.Text, DateFormat);
                                        Grace = FormatDate(tbChgBaseDate.Text, DateFormat);

                                        DueNext = DateSource.AddMonths(Months);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);

                                        Alert = DateSource.AddMonths(Months);
                                        Alert = Alert.AddDays(-Alerts);
                                        tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);

                                        Grace = DateSource.AddMonths(Months);
                                        Grace = Alert.AddDays(GraceDay);
                                        tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);
                                    }
                                    else
                                    {
                                        int Alerts = 0;
                                        int GraceDay = 0;

                                        int Days = 0;


                                        if (tbFreq.Text.Trim() != "")
                                        {
                                            Days = Convert.ToInt32(tbFreq.Text);
                                        }

                                        if (tbAlertDays.Text.Trim() != "")
                                        {
                                            Alerts = Convert.ToInt32(tbAlertDays.Text);
                                        }
                                        Alerts = Alerts - Days;

                                        if (tbGraceDays.Text.Trim() != "")
                                        {
                                            GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                        }

                                        GraceDay = GraceDay + Days;

                                        DateTime DateSource = DateTime.MinValue;
                                        DateTime DueNext = DateTime.MinValue;
                                        DateTime Alert = DateTime.MinValue;
                                        DateTime Grace = DateTime.MinValue;

                                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                        DateSource = FormatDate(tbChgBaseDate.Text, DateFormat);
                                        DueNext = FormatDate(tbChgBaseDate.Text, DateFormat);
                                        Alert = FormatDate(tbChgBaseDate.Text, DateFormat);
                                        Grace = FormatDate(tbChgBaseDate.Text, DateFormat);

                                        DueNext = DateSource.AddDays(Days);
                                        tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);

                                        Alert = Alert.AddDays(-Alerts);
                                        tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);


                                        Grace = Alert.AddDays(GraceDay);
                                        tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);


                                    }

                                    ((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text = System.Web.HttpUtility.HtmlEncode(tbFreq.Text);



                                    ((Label)Item[AlertDT].FindControl(lbAlertDT)).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                    ((Label)Item[GraceDT].FindControl(lbGraceDT)).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                                    ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                    ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                    ((Label)Item[DueDT].FindControl(lbDueDT)).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Methods to add days to Alert date and show the value in grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AlertDays_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false)
                            {
                                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                                ((Label)Item[AlertDT].FindControl(lbAlertDT)).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
                                AlertDaysFunction();

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }


        protected void AlertDaysFunction()
        {
            if (tbAlertDate.Text != string.Empty)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                int AlertDays = 0;
                string StartDate = tbDueNext.Text;
                DateTime AlertDate = FormatDate(StartDate, DateFormat);
                if (tbAlertDate.Text != string.Empty)
                {
                    AlertDays = Convert.ToInt32(tbAlertDays.Text);
                }
                tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((AlertDate.AddDays(-AlertDays))));

                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                ((Label)Item[AlertDT].FindControl(lbAlertDT)).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);
            }
        }

        protected void GraceDaysFunction()
        {
            if (tbGraceDate.Text != string.Empty)
            {
                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                int GraceDays = 0;
                string StartDate = tbDueNext.Text;
                DateTime GraceDate = FormatDate(StartDate, DateFormat);
                if (tbGraceDays.Text != string.Empty)
                {
                    GraceDays = Convert.ToInt32(tbGraceDays.Text);
                }
                tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime((GraceDate.AddDays(GraceDays))));

                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                ((Label)Item[GraceDT].FindControl(lbGraceDT)).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);

            }
        }

        /// <summary>
        /// Method to show the Aircraft Type value in Crew Checklist Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AircraftType_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tbAircraftType.Text.Trim()))
                            {
                                GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text = System.Web.HttpUtility.HtmlEncode(tbAircraftType.Text);
                                using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                                {
                                    var CrewRosterServiceValue = CrewRosterService.GetAircraftWithFilters(tbAircraftType.Text.Trim(),0,false).EntityList;
                                    if (CrewRosterServiceValue != null && CrewRosterServiceValue.Count > 0)
                                    {
                                        ((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value = ((FlightPak.Web.FlightPakMasterService.Aircraft)CrewRosterServiceValue[0]).AircraftID.ToString();
                                        ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.Aircraft)CrewRosterServiceValue[0]).AircraftCD.ToString());
                                        tbAircraftType.Text = ((FlightPak.Web.FlightPakMasterService.Aircraft)CrewRosterServiceValue[0]).AircraftCD.ToString();
                                        //RetVal = false;
                                        cvAssociateTypeCode.IsValid = true;
                                    }
                                    else
                                    {
                                        cvAssociateTypeCode.IsValid = false;
                                        tbAircraftType.Focus();
                                        //RetVal = true;
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TotalReqFlightHrs_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tbTotalReqFlightHrs.Text.Trim()))
                            {
                                if (revTotalReqFlightHrs.IsValid)
                                {
                                    GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                                    ((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text = System.Web.HttpUtility.HtmlEncode(tbTotalReqFlightHrs.Text);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Method to show the Changed Alert Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AlertDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {

                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((Label)Item[AlertDT].FindControl(lbAlertDT)).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDate.Text);

                            if (tbDueNext.Text.Trim() != string.Empty && tbAlertDate.Text != string.Empty)
                            {
                                DateTime DueNext = DateTime.MinValue;
                                DateTime Alert = DateTime.MinValue;

                                DueNext = FormatDate(tbDueNext.Text, DateFormat);
                                Alert = FormatDate(tbAlertDate.Text, DateFormat);

                                TimeSpan t = Alert - DueNext;
                                int NrOfDays = t.Days;

                                tbAlertDays.Text = NrOfDays.ToString();
                                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to show the Changed Grace Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GraceDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((Label)Item[GraceDT].FindControl(lbGraceDT)).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);


                            if (tbDueNext.Text.Trim() != string.Empty && tbGraceDate.Text != string.Empty)
                            {
                                DateTime DueNext = DateTime.MinValue;
                                DateTime GraceDate = DateTime.MinValue;




                                DueNext = FormatDate(tbDueNext.Text, DateFormat);
                                GraceDate = FormatDate(tbAlertDate.Text, DateFormat);

                                TimeSpan t = GraceDate - DueNext;
                                int NrOfDays = t.Days;

                                tbAlertDays.Text = NrOfDays.ToString();
                                ((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text = System.Web.HttpUtility.HtmlEncode(tbAlertDays.Text);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to show the Changed Previous Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Previous_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (tbChgBaseDate.Text == string.Empty)
                            {
                                if (ckhDisableDateCalculation.Checked == false)
                                {

                                    //tbDueNext.Text = String.Format("{0:" + DateFormat + "}", tbPrevious.Text);
                                    //tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", tbPrevious.Text);
                                    //tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", tbPrevious.Text);
                                    Label lbPreviousDT = new Label();
                                    //Label lbDueDT = new Label();
                                    //Label lbAlertDT = new Label();
                                    //Label lbGraceDT = new Label();

                                    //lbPDT = ((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT));
                                    //lbDueDT = ((Label)Item[DueDT].FindControl(lbDueDT));
                                    //lbAlertDT = ((Label)Item[AlertDT].FindControl(lbAlertDT));
                                    //lbGraceDT = ((Label)Item[GraceDT].FindControl(lbGraceDT));
                                    //lbBaseMonthDT.Text = tbChgBaseDate.Text;
                                    //lbDueDT.Text = tbDueNext.Text;
                                    //lbAlertDT.Text = tbAlertDate.Text;
                                    //lbGraceDT.Text = tbGraceDate.Text;
                                    ((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                                }
                            }

                            ((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text = System.Web.HttpUtility.HtmlEncode(tbPrevious.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        /// <summary>
        /// Method to show the Changed Previous Date in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DueNext_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            if (tbChgBaseDate.Text != string.Empty)
                            {
                                int GraceDay = 0;
                                int Days = 0;
                                int Alerts = 0;
                                if (ckhDisableDateCalculation.Checked == false)
                                {
                                    if (tbGraceDays.Text.Trim() != "")
                                    {
                                        GraceDay = Convert.ToInt32(tbGraceDays.Text);
                                    }

                                    if (tbAlertDays.Text.Trim() != "")
                                    {
                                        Alerts = Convert.ToInt32(tbAlertDays.Text);
                                    }

                                    if (tbFreq.Text.Trim() != "")
                                    {
                                        Days = Convert.ToInt32(tbFreq.Text);
                                    }

                                    Alerts = Alerts - Days;
                                    GraceDay = GraceDay + Days;

                                    DateTime DateSource = DateTime.MinValue;
                                    DateTime DueNext = DateTime.MinValue;
                                    DateTime Alert = DateTime.MinValue;
                                    DateTime Grace = DateTime.MinValue;

                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);


                                    DueNext = FormatDate(tbDueNext.Text, DateFormat);
                                    Alert = FormatDate(tbDueNext.Text, DateFormat);
                                    Grace = FormatDate(tbDueNext.Text, DateFormat);

                                    tbDueNext.Text = String.Format("{0:" + DateFormat + "}", DueNext);

                                    Alert = Alert.AddDays(-Alerts);
                                    tbAlertDate.Text = String.Format("{0:" + DateFormat + "}", Alert);


                                    Grace = Alert.AddDays(GraceDay);
                                    tbGraceDate.Text = String.Format("{0:" + DateFormat + "}", Grace);

                                    Label lbPreviousDT = new Label();
                                    ((Label)Item[DueDT].FindControl(lbDueDT)).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                                }
                            }

                            ((Label)Item[DueDT].FindControl(lbDueDT)).Text = System.Web.HttpUtility.HtmlEncode(tbDueNext.Text);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to show the Changed Grace Days in the Crew Checklist Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GraceDays_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            if (ckhDisableDateCalculation.Checked == false && dgCrewCheckList.SelectedItems.Count > 0)
                            {
                                GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                                GraceDaysFunction();
                                ((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDays.Text);
                                ((Label)Item[GraceDT].FindControl(lbGraceDT)).Text = System.Web.HttpUtility.HtmlEncode(tbGraceDate.Text);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Method to Update values in the selected list item and bind into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateFltExperience_Click(object sender, EventArgs e)
        {
            // Right now it allow to edit or add new check list for the particula Crew Code, 
            // once Postflight is completed, the record will get from the table and update total hours to the table.


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (dgTypeRating.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgTypeRating.SelectedItems[0] as GridDataItem;
                            Item.Selected = true;

                            // Update selected item values in the List
                            List<GetCrewRating> CrewRateList = (List<GetCrewRating>)Session["CrewTypeRating"];
                            if (Item.GetDataKeyValue("AircraftTypeCD") != null)
                            {
                                // Session["CrewTypeRatingAircraftTypeCD"] = Item.GetDataKeyValue("AircraftTypeCD").ToString();
                                CrewRatingAircraftCD = Item.GetDataKeyValue("AircraftTypeCD").ToString();
                            }
                            if (Item.GetDataKeyValue("CrewID") != null)
                            {
                                //Session["CrewTypeRatingCrewID"] = Item.GetDataKeyValue("CrewID").ToString();
                                CrewRatingCrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString());
                            }
                            //if (Item.GetDataKeyValue("CrewRatingID") != null)
                            //{
                            //    CrewRatingCrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString());
                            //}
                            //else
                            //{
                            //    CrewRatingCrewID = 0;
                            //}
                            CrewRateList.Where(x => x.AircraftTypeCD == Item.GetDataKeyValue("AircraftTypeCD").ToString()).ToList().ForEach(y => SetTypeRating(y, GetCrewRatingItems(Item)));
                            // Bind List into session variable
                            // Session["CrewTypeRating"] = CrewRateList;

                            // Bind List into Type Rating Grid
                            dgTypeRating.DataSource = CrewRateList;
                            dgTypeRating.DataBind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Method to Update values from Form and bind into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateAllFltExperience_Click(object sender, EventArgs e)
        {
            // Right now it allow to edit or add new check list for the particula Crew Code, 
            // once Postflight is completed, the record will get from the table and update total hours to the table.
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.MasterTableView.Items.Count > 0)
                        {


                            List<GetCrewRating> CrewRateList = (List<GetCrewRating>)Session["CrewTypeRating"];
                            foreach (GridDataItem Item in dgTypeRating.MasterTableView.Items)
                            {
                                if (dgTypeRating.SelectedItems.Count > 0)
                                {
                                    GridDataItem SItem = dgTypeRating.SelectedItems[0] as GridDataItem;
                                    Item.Selected = false;
                                }

                                if (Item.GetDataKeyValue("CrewRatingID") != null)
                                {
                                    CrewRatingCrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString());
                                }
                                else
                                {
                                    CrewRatingCrewID = 0;
                                }
                                //  Update selected item values in the List
                                CrewRateList.Where(x => x.AircraftTypeCD.Trim().ToUpper() == Item.GetDataKeyValue("AircraftTypeCD").ToString().Trim().ToUpper()).ToList().ForEach(y => SetTypeRating(y, GetCrewRatingItems(Item)));
                            }
                            // Bind List into session variable
                            Session["CrewTypeRating"] = CrewRateList;

                            // Bind List into Type Rating Grid
                            dgTypeRating.DataSource = CrewRateList;
                            dgTypeRating.DataBind();

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Get Crew Rating Data by passing Grid Item
        /// </summary>
        /// <param name="Item"></param>
        /// <returns>Returns CrewRating Object</returns>
        private GetCrewRating GetCrewRatingItems(GridDataItem Item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Item))
            {
                GetCrewRating CrewRate = new GetCrewRating();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CrewRate.CrewID = Convert.ToInt64(Item.GetDataKeyValue("CrewID").ToString());
                    if (Item.GetDataKeyValue("ClientID") != null)
                    {
                        CrewRate.ClientID = Convert.ToInt64(Item.GetDataKeyValue("ClientID").ToString()); // item.getvalue ll come
                    }
                    if (Item.GetDataKeyValue("AircraftTypeID") != null)
                    {
                        CrewRate.AircraftTypeID = Convert.ToInt64(Item.GetDataKeyValue("AircraftTypeID").ToString());             // Item.GetDataKeyValue("AircraftTypeCD").ToString();
                    }
                    if (Item.GetDataKeyValue("CrewRatingDescription") != null)
                    {
                        CrewRate.CrewRatingDescription = Item.GetDataKeyValue("CrewRatingDescription").ToString();            // Item.GetDataKeyValue("AircraftTypeCD").ToString();
                    }
                    //if (Item.Selected == true)
                    //{
                    // Qualified In Type As
                    CrewRate.IsPilotinCommand = chkTRPIC91.Checked;
                    CrewRate.IsQualInType135PIC = chkTRPIC135.Checked;
                    CrewRate.IsSecondInCommand = chkTRSIC91.Checked;
                    CrewRate.IsQualInType135SIC = chkTRSIC135.Checked;
                    CrewRate.IsEngineer = chkTREngineer.Checked;
                    CrewRate.IsInstructor = chkTRInstructor.Checked;
                    CrewRate.IsCheckAttendant = chkTRAttendant.Checked;
                    CrewRate.IsAttendantFAR91 = chkTRAttendant91.Checked;
                    CrewRate.IsAttendantFAR135 = chkTRAttendant135.Checked;
                    CrewRate.IsCheckAirman = chkTRAirman.Checked;
                    CrewRate.IsInActive = chkTRInactive.Checked;

                    //<new field>
                    CrewRate.IsPIC121 = chkPIC121.Checked;
                    CrewRate.IsSIC121 = chkSIC121.Checked;
                    CrewRate.IsPIC125 = chkPIC125.Checked;
                    CrewRate.IsSIC125 = chkSIC125.Checked;
                    CrewRate.IsAttendant121 = chkAttendent121.Checked;
                    CrewRate.IsAttendant125 = chkAttendent125.Checked;


                    // Beginning Flight Exp. 
                    //TextBox tbAsOfDt = (TextBox)ucAsOfDt.FindControl("tbDate");


                    //FormatDate(tbAsOfDt.Text, DateFormat);


                    // Total Hrs

                    CrewRate.TotalTimeInTypeHrs = Convert.ToDecimal(tbTotalHrsInType.Text = false ? "0.00" : tbTotalHrsInType.Text, CultureInfo.CurrentCulture);
                    CrewRate.TotalDayHrs = Convert.ToDecimal(tbTotalHrsDay.Text = false ? "0.00" : tbTotalHrsDay.Text, CultureInfo.CurrentCulture);
                    CrewRate.TotalNightHrs = Convert.ToDecimal(tbTotalHrsNight.Text = false ? "0.00" : tbTotalHrsNight.Text, CultureInfo.CurrentCulture);
                    CrewRate.TotalINSTHrs = Convert.ToDecimal(tbTotalHrsInstrument.Text = false ? "0.00" : tbTotalHrsInstrument.Text, CultureInfo.CurrentCulture);

                    // PIC Hrs
                    CrewRate.PilotInCommandTypeHrs = Convert.ToDecimal(tbPICHrsInType.Text = false ? "0.00" : tbPICHrsInType.Text, CultureInfo.CurrentCulture);
                    CrewRate.TPilotinCommandDayHrs = Convert.ToDecimal(tbPICHrsDay.Text = false ? "0.00" : tbPICHrsDay.Text, CultureInfo.CurrentCulture);
                    CrewRate.TPilotInCommandNightHrs = Convert.ToDecimal(tbPICHrsNight.Text = false ? "0.00" : tbPICHrsNight.Text, CultureInfo.CurrentCulture);
                    CrewRate.TPilotInCommandINSTHrs = Convert.ToDecimal(tbPICHrsInstrument.Text = false ? "0.00" : tbPICHrsInstrument.Text, CultureInfo.CurrentCulture);

                    // SIC Hrs
                    CrewRate.SecondInCommandTypeHrs = Convert.ToDecimal(tbSICInTypeHrs.Text = false ? "0.00" : tbSICInTypeHrs.Text, CultureInfo.CurrentCulture);
                    CrewRate.SecondInCommandDayHrs = Convert.ToDecimal(tbSICHrsDay.Text = false ? "0.00" : tbSICHrsDay.Text, CultureInfo.CurrentCulture);
                    CrewRate.SecondInCommandNightHrs = Convert.ToDecimal(tbSICHrsNight.Text = false ? "0.00" : tbSICHrsNight.Text, CultureInfo.CurrentCulture);
                    CrewRate.SecondInCommandINSTHrs = Convert.ToDecimal(tbSICHrsInstrument.Text = false ? "0.00" : tbSICHrsInstrument.Text, CultureInfo.CurrentCulture);

                    // Other Hrs
                    CrewRate.OtherSimHrs = Convert.ToDecimal(tbOtherHrsSimulator.Text = false ? "0.00" : tbOtherHrsSimulator.Text, CultureInfo.CurrentCulture);
                    CrewRate.OtherFlightEngHrs = Convert.ToDecimal(tbOtherHrsFltEngineer.Text = false ? "0.00" : tbOtherHrsFltEngineer.Text, CultureInfo.CurrentCulture);
                    CrewRate.OtherFlightInstrutorHrs = Convert.ToDecimal(tbOtherHrsFltInstructor.Text = false ? "0.00" : tbOtherHrsFltInstructor.Text, CultureInfo.CurrentCulture);
                    CrewRate.OtherFlightAttdHrs = Convert.ToDecimal(tbOtherHrsFltAttendant.Text = false ? "0.00" : tbOtherHrsFltAttendant.Text, CultureInfo.CurrentCulture);
                    CrewRate.TotalUpdateAsOfDT = DateTime.Now;
                    CrewRate.AsOfDT = DateTime.Now;
                    CrewRate.IsDeleted = false;
                    //}
                    //else
                    //{
                    //    if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                    //    {
                    //        CrewRate.IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsPilotinCommand = false;
                    //    }

                    //    if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                    //    {
                    //        CrewRate.IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsSecondInCommand = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                    //    {
                    //        CrewRate.IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsQualInType135PIC = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                    //    {
                    //        CrewRate.IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsQualInType135SIC = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                    //    {
                    //        CrewRate.IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsEngineer = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                    //    {
                    //        CrewRate.IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsInstructor = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                    //    {
                    //        CrewRate.IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsCheckAttendant = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                    //    {
                    //        CrewRate.IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsAttendantFAR91 = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                    //    {
                    //        CrewRate.IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsAttendantFAR135 = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                    //    {
                    //        CrewRate.IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsCheckAirman = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                    //    {
                    //        CrewRate.IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsInActive = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                    //    {
                    //        CrewRate.IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsPIC121 = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                    //    {
                    //        CrewRate.IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsSIC121 = false;
                    //    }
                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                    //    {
                    //        CrewRate.IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsPIC125 = false;
                    //    }
                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                    //    {
                    //        CrewRate.IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsSIC125 = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                    //    {
                    //        CrewRate.IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsAttendant121 = false;
                    //    }

                    //    if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                    //    {
                    //        CrewRate.IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                    //    }
                    //    else
                    //    {
                    //        CrewRate.IsAttendant125 = false;
                    //    }
                    //    // Total Hrs
                    //    if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                    //    {
                    //        //if (DateFormat != null)
                    //        //{
                    //        //    CrewRate.TotalUpdateAsOfDT = FormatDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()), DateFormat);
                    //        //}
                    //        //else
                    //        //{
                    //        CrewRate.TotalUpdateAsOfDT = DateTime.Now;
                    //        //Convert.ToDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()));
                    //        //}
                    //    }
                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                    //    {
                    //        CrewRate.TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TotalTimeInTypeHrs = 0;
                    //    }
                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                    //    {
                    //        CrewRate.TotalDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TotalDayHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                    //    {
                    //        CrewRate.TotalNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TotalNightHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                    //    {
                    //        CrewRate.TotalINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TotalINSTHrs = 0;
                    //    }


                    //    // PIC Hrs
                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                    //    {
                    //        CrewRate.PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.PilotInCommandTypeHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                    //    {
                    //        CrewRate.TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TPilotinCommandDayHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                    //    {
                    //        CrewRate.TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TPilotInCommandNightHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                    //    {
                    //        CrewRate.TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.TPilotInCommandINSTHrs = 0;
                    //    }



                    //    // SIC Hrs
                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                    //    {
                    //        CrewRate.SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.SecondInCommandTypeHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                    //    {
                    //        CrewRate.SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.SecondInCommandDayHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                    //    {
                    //        CrewRate.SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.SecondInCommandNightHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                    //    {
                    //        CrewRate.SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.SecondInCommandINSTHrs = 0;
                    //    }


                    //    //if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                    //    //{
                    //    //    //if (DateFormat != null)
                    //    //    //{
                    //    //    //    CrewRate.AsOfDT = FormatDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim(), DateFormat);
                    //    //    //}
                    //    //    //else
                    //    //    //{
                    //    //        CrewRate.AsOfDT = Convert.ToDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim());
                    //    //    //}
                    //    //}
                    //    //else
                    //    //{
                    //    //    CrewRate.AsOfDT = null;
                    //    //}

                    //    // Beginning Flight Exp. 
                    //    //if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                    //    //{
                    //    //    //if (DateFormat != null)
                    //    //    //{
                    //    //    //    CrewRate.AsOfDT = FormatDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim(), DateFormat);
                    //    //    //}
                    //    //    //else
                    //    //    //{
                    //    //        CrewRate.AsOfDT = Convert.ToDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim());

                    //    //    //}
                    //    //}



                    //    // Others Hrs
                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                    //    {

                    //        CrewRate.OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.OtherSimHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                    //    {
                    //        CrewRate.OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.OtherFlightEngHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                    //    {
                    //        CrewRate.OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.OtherFlightInstrutorHrs = 0;
                    //    }

                    //    if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                    //    {
                    //        CrewRate.OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text, CultureInfo.CurrentCulture);
                    //    }
                    //    else
                    //    {
                    //        CrewRate.OtherFlightAttdHrs = 0;
                    //    }

                    //    CrewRate.TotalUpdateAsOfDT = DateTime.Now;
                    //    CrewRate.AsOfDT = DateTime.Now;
                    //}

                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewRate;
            }

        }

        /// <summary>
        /// Method to Assign Updated Crew Checklist class items into Crew Rating Class Objects
        /// </summary>
        /// <param name="CrewRate"></param>
        /// <param name="UpdateCrewRate"></param>
        /// 
        private void SetTypeRating(GetCrewRating CrewRate, GetCrewRating UpdateCrewRate)
        {
            Int64 TotalTimeInTypeHrs = 0;
            Int64 TotalDayHrs = 0;
            Int64 TotalNightHrs = 0;
            Int64 TotalINSTHrs = 0;

            Int64 PilotInCommandTypeHrs = 0;
            Int64 TPilotinCommandDayHrs = 0;
            Int64 TPilotInCommandNightHrs = 0;
            Int64 TPilotInCommandINSTHrs = 0;

            Int64 SecondInCommandTypeHrs = 0;
            Int64 SecondInCommandDayHrs = 0;
            Int64 SecondInCommandNightHrs = 0;
            Int64 SecondInCommandINSTHrs = 0;

            Int64 OtherSimHrs = 0;
            Int64 OtherFlightEngHrs = 0;
            Int64 OtherFlightInstrutorHrs = 0;
            Int64 OtherFlightAttdHrs = 0;


            using (MasterCatalogServiceClient CrewRatingUpdateService = new MasterCatalogServiceClient())
            {
                if (CrewRatingAircraftCD != null && CrewRatingCrewID > 0)
                {
                    //var CrewRatingUpdateList = CrewRatingUpdateService.GetCrewRatingListInfo(CrewRatingAircraftCD, CrewRatingCrewID).EntityList.ToList();
                    //GetFlightLogDataForCrewMember CrewRatingUpdate = new GetFlightLogDataForCrewMember();
                    //if (CrewRatingUpdateList != null && CrewRatingUpdateList.Count > 0)
                    //{
                    //    TotalTimeInTypeHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTTIT);
                    //    TotalDayHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTDAY);
                    //    TotalNightHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTNIGHT);
                    //    TotalINSTHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TOTINSTR);

                    //    PilotInCommandTypeHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPIC);
                    //    TPilotinCommandDayHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPDAY);
                    //    TPilotInCommandNightHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPNIGHT);
                    //    TPilotInCommandINSTHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TPINSTR);

                    //    SecondInCommandTypeHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSIC);
                    //    SecondInCommandDayHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSDAY);
                    //    SecondInCommandNightHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSNIGHT);
                    //    SecondInCommandINSTHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSINSTR);

                    //    OtherSimHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TSIMULATOR);
                    //    OtherFlightEngHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TFLTENGR);
                    //    OtherFlightInstrutorHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TFLTINSTR);
                    //    OtherFlightAttdHrs = Convert.ToInt64(((FlightPakMasterService.GetFlightLogDataForCrewMember)CrewRatingUpdateList[0]).TFLTATTEND);
                    //}
                }

                CrewRate.CrewID = UpdateCrewRate.CrewID;
                CrewRate.ClientID = UpdateCrewRate.ClientID;
                CrewRate.AircraftTypeID = UpdateCrewRate.AircraftTypeID;
                CrewRate.CrewRatingDescription = UpdateCrewRate.CrewRatingDescription;

                // Qualified In Type As
                CrewRate.IsPilotinCommand = UpdateCrewRate.IsPilotinCommand;
                CrewRate.IsQualInType135PIC = UpdateCrewRate.IsQualInType135PIC;
                CrewRate.IsSecondInCommand = UpdateCrewRate.IsSecondInCommand;
                CrewRate.IsQualInType135SIC = UpdateCrewRate.IsQualInType135SIC;
                CrewRate.IsEngineer = UpdateCrewRate.IsEngineer;
                CrewRate.IsInstructor = UpdateCrewRate.IsInstructor;
                CrewRate.IsCheckAttendant = UpdateCrewRate.IsCheckAttendant;
                CrewRate.IsAttendantFAR91 = UpdateCrewRate.IsAttendantFAR91;
                CrewRate.IsAttendantFAR135 = UpdateCrewRate.IsAttendantFAR135;
                CrewRate.IsCheckAirman = UpdateCrewRate.IsCheckAirman;
                CrewRate.IsInActive = UpdateCrewRate.IsInActive;

                //<new fields>
                CrewRate.IsPIC121 = UpdateCrewRate.IsPIC121;
                CrewRate.IsSIC121 = UpdateCrewRate.IsSIC121;
                CrewRate.IsPIC125 = UpdateCrewRate.IsPIC125;
                CrewRate.IsSIC125 = UpdateCrewRate.IsSIC125;
                CrewRate.IsAttendant121 = UpdateCrewRate.IsAttendant121;
                CrewRate.IsAttendant125 = UpdateCrewRate.IsAttendant125;

                CrewRate.AsOfDT = UpdateCrewRate.AsOfDT;

                // Total Hrs
                CrewRate.TotalUpdateAsOfDT = UpdateCrewRate.TotalUpdateAsOfDT;
                CrewRate.TotalTimeInTypeHrs = UpdateCrewRate.TotalTimeInTypeHrs + TotalTimeInTypeHrs;
                CrewRate.TotalDayHrs = UpdateCrewRate.TotalDayHrs + TotalDayHrs;
                CrewRate.TotalNightHrs = UpdateCrewRate.TotalNightHrs + TotalNightHrs;
                CrewRate.TotalINSTHrs = UpdateCrewRate.TotalINSTHrs + TotalINSTHrs;

                // PIC Hrs
                CrewRate.PilotInCommandTypeHrs = UpdateCrewRate.PilotInCommandTypeHrs + PilotInCommandTypeHrs;
                CrewRate.TPilotinCommandDayHrs = UpdateCrewRate.TPilotinCommandDayHrs + TPilotinCommandDayHrs;
                CrewRate.TPilotInCommandNightHrs = UpdateCrewRate.TPilotInCommandNightHrs + TPilotInCommandNightHrs;
                CrewRate.TPilotInCommandINSTHrs = UpdateCrewRate.TPilotInCommandINSTHrs + TPilotInCommandINSTHrs;


                // SIC Hrs
                CrewRate.SecondInCommandTypeHrs = UpdateCrewRate.SecondInCommandTypeHrs + SecondInCommandTypeHrs;
                CrewRate.SecondInCommandDayHrs = UpdateCrewRate.SecondInCommandDayHrs + SecondInCommandDayHrs;
                CrewRate.SecondInCommandNightHrs = UpdateCrewRate.SecondInCommandNightHrs + SecondInCommandNightHrs;
                CrewRate.SecondInCommandINSTHrs = UpdateCrewRate.SecondInCommandINSTHrs + SecondInCommandINSTHrs;


                // Other Hrs
                CrewRate.OtherSimHrs = UpdateCrewRate.OtherSimHrs + OtherSimHrs;
                CrewRate.OtherFlightEngHrs = UpdateCrewRate.OtherFlightEngHrs + OtherFlightEngHrs;
                CrewRate.OtherFlightInstrutorHrs = UpdateCrewRate.OtherFlightInstrutorHrs + OtherFlightInstrutorHrs;
                CrewRate.OtherFlightAttdHrs = UpdateCrewRate.OtherFlightAttdHrs + OtherFlightAttdHrs;
                CrewRate.IsDeleted = false;
            }
        }



        /// <summary>
        /// Enable / Disable Hours in Type Rating Section
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        private void EnableHours(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbTotalHrsInType.Enabled = Enable;
                    tbTotalHrsDay.Enabled = Enable;
                    tbTotalHrsNight.Enabled = Enable;
                    tbTotalHrsInstrument.Enabled = Enable;
                    tbPICHrsInType.Enabled = Enable;
                    tbPICHrsDay.Enabled = Enable;
                    tbPICHrsNight.Enabled = Enable;
                    tbPICHrsInstrument.Enabled = Enable;
                    tbSICInTypeHrs.Enabled = Enable;
                    tbSICHrsDay.Enabled = Enable;
                    tbSICHrsNight.Enabled = Enable;
                    tbSICHrsInstrument.Enabled = Enable;
                    tbOtherHrsSimulator.Enabled = Enable;
                    tbOtherHrsFltEngineer.Enabled = Enable;
                    tbOtherHrsFltInstructor.Enabled = Enable;
                    tbOtherHrsFltAttendant.Enabled = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Clear Type Rating Fields
        /// </summary>
        private void ClearTypeRatingFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbTotalHrsInType.Text = "0.00";
                    tbTotalHrsDay.Text = "0.00";
                    tbTotalHrsNight.Text = "0.00";
                    tbTotalHrsInstrument.Text = "0.00";
                    tbPICHrsInType.Text = "0.00";
                    tbPICHrsDay.Text = "0.00";
                    tbPICHrsNight.Text = "0.00";
                    tbPICHrsInstrument.Text = "0.00";
                    tbSICInTypeHrs.Text = "0.00";
                    tbSICHrsDay.Text = "0.00";
                    tbSICHrsNight.Text = "0.00";
                    tbSICHrsInstrument.Text = "0.00";
                    tbOtherHrsSimulator.Text = "0.00";
                    tbOtherHrsFltEngineer.Text = "0.00";
                    tbOtherHrsFltInstructor.Text = "0.00";
                    tbOtherHrsFltAttendant.Text = "0.00";

                    lbTypeCode.Text = string.Empty;
                    lbDescription.Text = string.Empty;
                    chkTRInactive.Checked = false;
                    chkTRPIC91.Checked = false;
                    chkTRSIC91.Checked = false;
                    chkTREngineer.Checked = false;
                    chkTRAttendant91.Checked = false;
                    chkTRAirman.Checked = false;
                    chkTRPIC135.Checked = false;
                    chkTRSIC135.Checked = false;
                    chkTRInstructor.Checked = false;
                    chkTRAttendant135.Checked = false;
                    chkTRAttendant.Checked = false;

                    chkPIC121.Checked = false;
                    chkSIC121.Checked = false;
                    chkPIC125.Checked = false;
                    chkSIC125.Checked = false;
                    chkAttendent121.Checked = false;
                    chkAttendent125.Checked = false;


                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Clear Check List Fields
        /// </summary>
        private void ClearCheckListFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    lbCheckListCode.Text = string.Empty;
                    lbCheckListDescription.Text = string.Empty;

                    tbFreq.Text = "0";
                    radFreq.Items[0].Selected = false;
                    radFreq.Items[1].Selected = false;
                    tbPrevious.Text = string.Empty;
                    tbDueNext.Text = string.Empty;
                    tbAlertDate.Text = string.Empty;
                    tbAlertDays.Text = string.Empty;
                    tbGraceDate.Text = string.Empty;
                    tbGraceDays.Text = string.Empty;
                    tbAircraftType.Text = string.Empty;
                    tbOriginalDate.Text = string.Empty;
                    tbChgBaseDate.Text = string.Empty;
                    tbTotalReqFlightHrs.Text = string.Empty;
                    chkPIC91.Checked = false;
                    chkPIC135.Checked = false;
                    chkSIC91.Checked = false;
                    chkSIC135.Checked = false;
                    chkInactive.Checked = false;
                    chkSetToEndOfMonth.Checked = false;
                    chkSetToNextOfMonth.Checked = false;
                    chkSetToEndOfCalenderYear.Checked = false;
                    ckhDisableDateCalculation.Checked = false;
                    chkOneTimeEvent.Checked = false;
                    chkCompleted.Checked = false;
                    chkNonConflictedEvent.Checked = false;
                    chkRemoveFromCrewRosterRept.Checked = false;
                    chkRemoveFromChecklistRept.Checked = false;
                    chkDoNotPrintPassDueAlert.Checked = false;
                    chkPrintOneTimeEventCompleted.Checked = false;
                    chkDisplayInactiveChecklist.Checked = false;
                    // btnApplyToAdditionalCrew.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Refresh the Grid based on condition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RefreshView_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tabSwitchViews.SelectedIndex == 1)
                        {
                            List<GetCrewCheckListDate> FinalList = new List<GetCrewCheckListDate>();
                            List<GetCrewCheckListDate> RetainList = new List<GetCrewCheckListDate>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewCheckListGrid(dgCrewCheckList);
                            // Bind final list into Session
                            Session["CrewTypeChecklist"] = RetainList;
                            // Bind final list into Grid
                            dgCurrencyChecklist.DataSource = RetainList;
                            dgCurrencyChecklist.DataBind();
                            // Empty Newly added Session item
                            Session.Remove("CrewTypeChecklist");

                            //
                            //GridDataItem Item = (GridDataItem)Session["SelectedCrewRosterID"];
                            Int64 TempID = 0;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                TempID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                            }
                            BindCrewCurrency(TempID);
                        }
                        else
                        {
                            List<GetCrewRating> FinalList = new List<GetCrewRating>();
                            List<GetCrewRating> RetainList = new List<GetCrewRating>();
                            // Retain Grid Items and bind into List and add into Final List
                            RetainList = RetainCrewTypeRatingGrid(dgCrewCheckList);
                            // Bind final list into Session
                            Session["CrewTypeChecklist"] = RetainList;
                            // Bind final list into Grid
                            dgCurrencyChecklist.DataSource = RetainList;
                            dgCurrencyChecklist.DataBind();
                            // Empty Newly added Session item
                            Session.Remove("CrewTypeChecklist");

                            //
                            // GridDataItem Item = (GridDataItem)Session["SelectedCrewRosterID"];
                            //if (Session["SelectedCrewRosterID"] != null)
                            //{
                            //    BindCrewCurrency(Convert.ToInt64(Session["SelectedCrewRosterID"].ToString()));
                            //}
                            Int64 TempID = 0;
                            if (Session["SelectedCrewRosterID"] != null)
                            {
                                TempID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                            }
                            BindCrewCurrency(TempID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        #endregion



        #region "Validate Data"

        private bool ValidateDate()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {


                    //if ((((TextBox)ucHireDT.FindControl("tbDate")).Text != null) && (((TextBox)ucHireDT.FindControl("tbDate")).Text!=string.Empty) && (((TextBox)ucTermDT.FindControl("tbDate")).Text != null) && ((TextBox)ucTermDT.FindControl("tbDate")).Text != string.Empty)

                    //if (!string.IsNullOrEmpty(((TextBox)ucHireDT.FindControl("tbDate")).Text) && !string.IsNullOrEmpty(((TextBox)ucTermDT.FindControl("tbDate")).Text))
                    //{
                    //    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    //    DateTime DateStart = new DateTime();
                    //    DateTime DateEnd = new DateTime();
                    //    if (!string.IsNullOrEmpty(((TextBox)ucHireDT.FindControl("tbDate")).Text))
                    //    {
                    //        if (DateFormat != null)
                    //        {
                    //            DateStart = FormatDate(((TextBox)ucHireDT.FindControl("tbDate")).Text, DateFormat);
                    //        }
                    //        else
                    //        {
                    //            DateStart = Convert.ToDateTime(((TextBox)ucHireDT.FindControl("tbDate")).Text);
                    //        }
                    //    }
                    //    if (!string.IsNullOrEmpty(((TextBox)ucTermDT.FindControl("tbDate")).Text))
                    //    {
                    //        if (DateFormat != null)
                    //        {
                    //            DateEnd = FormatDate(((TextBox)ucTermDT.FindControl("tbDate")).Text, DateFormat);
                    //        }
                    //        else
                    //        {
                    //            DateEnd = Convert.ToDateTime(((TextBox)ucTermDT.FindControl("tbDate")).Text);
                    //        }
                    //    }
                    //    if (DateStart > DateEnd)
                    //    {
                    //        ReturnVal = true;
                    //    }
                    //}

                    if (!string.IsNullOrEmpty(tbHireDT.Text) && !string.IsNullOrEmpty(tbTermDT.Text))
                    {
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateStart = new DateTime();
                        DateTime DateEnd = new DateTime();
                        if (!string.IsNullOrEmpty(tbHireDT.Text))
                        {
                            if (DateFormat != null)
                            {
                                DateStart = FormatDate(tbHireDT.Text, DateFormat);
                            }
                            else
                            {
                                DateStart = Convert.ToDateTime(tbHireDT.Text);
                            }
                        }
                        if (!string.IsNullOrEmpty(tbTermDT.Text))
                        {
                            if (DateFormat != null)
                            {
                                DateEnd = FormatDate(tbTermDT.Text, DateFormat);
                            }
                            else
                            {
                                DateEnd = Convert.ToDateTime(tbTermDT.Text);
                            }
                        }
                        if (DateStart > DateEnd)
                        {
                            ReturnVal = true;
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnVal;
            }

        }

        private bool ValidateDateofBirth()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    //if (!string.IsNullOrEmpty(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text))
                    //{
                    //    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    //    DateTime DateOfBirth = new DateTime();

                    //    if (!string.IsNullOrEmpty(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text))
                    //    {
                    //        if (DateFormat != null)
                    //        {
                    //            DateOfBirth = FormatDate(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text, DateFormat);
                    //        }
                    //        else
                    //        {
                    //            DateOfBirth = Convert.ToDateTime(((TextBox)ucDateOfBirth.FindControl("tbDate")).Text);
                    //        }
                    //    }
                    //    if (DateOfBirth > System.DateTime.Now)
                    //    {
                    //        ReturnVal = true;
                    //    }
                    //}

                    if (!string.IsNullOrEmpty(tbDateOfBirth.Text))
                    {
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        DateTime DateOfBirth = new DateTime();

                        if (!string.IsNullOrEmpty(tbDateOfBirth.Text))
                        {
                            if (DateFormat != null)
                            {
                                DateOfBirth = FormatDate(tbDateOfBirth.Text, DateFormat);
                            }
                            else
                            {
                                DateOfBirth = Convert.ToDateTime(tbDateOfBirth.Text);
                            }
                        }
                        if (DateOfBirth > System.DateTime.Now)
                        {
                            ReturnVal = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnVal;
            }
        }

        #endregion


        #region "Save & Cancel "

        /// <summary>
        /// Method to Raise Save / Update Event based on Hidden Value
        /// </summary>
        private void Save()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    bool IsValidate = true;
                    if (ValidateDate())
                    {
                        string AlertMsg = "radalert('Termination Date should be lesser than Hire Date', 360, 50, 'Crew Roaster Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        IsValidate = false;
                    }
                    if (ValidateDateofBirth())
                    {
                        string AlertMsg = "radalert('Date of Birth should be lesser than Current Date', 360, 50, 'Crew Roaster Catalog');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        IsValidate = false;
                    }
                    if (IsValidate)
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);

                        }
                        else
                        {
                            (dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        hdnTemp.Value = "Off";
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Cancel Method
        /// </summary>
        private void Cancel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim()));
                        }
                    }
                    Session.Remove("SelectedCrewRosterID");
                    Session.Remove("CrewAddInfo");
                    Session.Remove("CrewTypeRating");
                    Session.Remove("CrewCheckList");
                    Session.Remove("CrewVisa");
                    Session.Remove("CrewPassport");
                    Session.Remove("CrewAircraftAssigned");

                    GridEnable(true, true, true);
                    DefaultSelection(false);
                    hdnTemp.Value = "Off";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion


        #region "Save Grids Data in Database"

        /// <summary>
        /// Method to fetch the Crew Main Info Items
        /// </summary>
        /// <returns>Returns Crew Object</returns>
        private FlightPakMasterService.Crew GetCrewMainInfo(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Main Info Fields
                    oCrew.CrewCD = GetCrewCode(CompanySett);
                    //Crew.CrewCD = tbCrewCode.Text;
                    if (hdnSave.Value == "Update")
                    {
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            oCrew.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                        }
                        //else
                        //{
                        //    oCrew.CrewID = 0;
                        //}
                        oCrew.CustomerID = Convert.ToInt64(((GridDataItem)dgCrewRoster.SelectedItems[0]).GetDataKeyValue("CustomerID").ToString());
                    }
                    if (!string.IsNullOrEmpty(hdnHomeBaseID.Value.ToString()))
                    {
                        oCrew.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }

                    if (!string.IsNullOrEmpty(hdnClientID.Value.ToString()))
                    {
                        oCrew.ClientID = Convert.ToInt64(hdnClientID.Value);
                    }


                    oCrew.LastName = tbLastName.Text;
                    oCrew.FirstName = tbFirstName.Text;
                    oCrew.MiddleInitial = tbMiddleName.Text;
                    oCrew.Addr1 = tbAddress1.Text;
                    oCrew.Addr2 = tbAddress2.Text;
                    oCrew.CityName = tbCity.Text;
                    oCrew.StateName = tbStateProvince.Text;
                    oCrew.PostalZipCD = tbStateProvincePostal.Text;
                    if (!string.IsNullOrEmpty(tbCountry.Text.Trim()))
                    {
                        oCrew.CountryID = Convert.ToInt64(hdnCountryID.Value);
                    }
                    else
                    {
                        oCrew.CountryID = null;
                    }
                    if (rbMale.Checked == true)
                    {
                        oCrew.Gender = "M";
                    }
                    else// if (rbFemale.Checked == true)
                    {
                        oCrew.Gender = "F";
                    }
                    oCrew.IsStatus = chkActiveCrew.Checked;
                    oCrew.IsFixedWing = chkFixedWing.Checked;
                    oCrew.IsRotaryWing = chkRoteryWing.Checked;

                    // TextBox tbDateOfBirth = (TextBox)ucDateOfBirth.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbDateOfBirth.Text.Trim().ToString()))
                    {
                        oCrew.BirthDT = DateTime.ParseExact(tbDateOfBirth.Text, DateFormat, CultureInfo.InvariantCulture);
                    }
                    oCrew.IsNoCalendarDisplay = chkNoCalenderDisplay.Checked;
                    oCrew.SSN = tbSSN.Text.Trim();
                    oCrew.CityOfBirth = tbCityBirth.Text;
                    oCrew.StateofBirth = tbStateBirth.Text;

                    if (!string.IsNullOrEmpty(tbCountryBirth.Text.Trim()))
                    {
                        oCrew.CountryOfBirth = Convert.ToInt64(hdnResidenceCountryID.Value);
                    }
                    else
                    {
                        oCrew.CountryOfBirth = null;
                    }
                    oCrew.PhoneNum = tbHomePhone.Text;
                    oCrew.CellPhoneNum = tbMobilePhone.Text;
                    oCrew.PagerNum = tbPager.Text;
                    oCrew.FaxNum = tbFax.Text;
                    oCrew.EmailAddress = tbEmail.Text;
                    // TextBox tbHireDT = (TextBox)ucHireDT.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbHireDT.Text.Trim().ToString()))
                    {
                        //oCrew.HireDT = DateTime.ParseExact(tbHireDT.Text, DateFormat, CultureInfo.InvariantCulture);

                        oCrew.HireDT = FormatDate(tbHireDT.Text, DateFormat);
                    }
                    // TextBox tbTermDT = (TextBox)ucTermDT.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbTermDT.Text.Trim().ToString()))
                    {
                        //oCrew.TerminationDT = DateTime.ParseExact(tbTermDT.Text, DateFormat, CultureInfo.InvariantCulture);

                        oCrew.TerminationDT = FormatDate(tbTermDT.Text, DateFormat);
                    }

                    if (!string.IsNullOrEmpty(tbCitizenCode.Text.Trim()))
                    {
                        oCrew.Citizenship = Convert.ToInt64(hdnCitizenshipID.Value);
                    }
                    else
                    {
                        oCrew.Citizenship = null;
                    }
                    oCrew.PilotLicense1 = tbLicNo.Text;
                    if (!string.IsNullOrEmpty(tbLicExpDate.Text.Trim().ToString()))
                    {
                        oCrew.License1ExpiryDT = DateTime.ParseExact(tbLicExpDate.Text, DateFormat, CultureInfo.InvariantCulture);
                    }

                    oCrew.License1CityCountry = tbLicCity.Text;
                    oCrew.LicenseCountry1 = tbLicCountry.Text;
                    oCrew.LicenseType1 = tbLicType.Text;
                    oCrew.PilotLicense2 = tbAddLic.Text;
                    if (!string.IsNullOrEmpty(tbAddLicExpDate.Text.Trim().ToString()))
                    {
                        oCrew.License2ExpiryDT = DateTime.ParseExact(tbAddLicExpDate.Text, DateFormat, CultureInfo.InvariantCulture);
                    }
                    oCrew.License2CityCountry = tbAddLicCity.Text;
                    oCrew.LicenseCountry2 = tbAddLicCountry.Text;
                    oCrew.LicenseType2 = tbAddLicType.Text;
                    oCrew.CrewTypeCD = tbCrewType.Text;

                    if (!string.IsNullOrEmpty(hdnDepartmentID.Value.ToString()))
                    {
                        oCrew.DepartmentID = Convert.ToInt64(hdnDepartmentID.Value);   // hidden value ll come here
                    }
                    oCrew.Notes = tbNotes.Text;
                    //tbPhoto.Text="Not in DB";

                    // Type Ratings Fields
                    //oCrew.CrewTypeCD = lbTypeCode.Text;
                    //oCrew.CrewTypeDescription = lbDescription.Text;

                    // Addl Notes Field
                    oCrew.Notes2 = tbAdditionalNotes.Text;

                    //Email Preferences Fields
                    oCrew.IsDepartmentAuthorization = chkDepartmentAuthorization.Checked;
                    oCrew.IsRequestPhoneNum = chkRequestorPhone.Checked;
                    oCrew.IsAccountNum = chkAccountNo.Checked;
                    oCrew.IsCancelDescription = chkCancellationDesc.Checked;
                    oCrew.IsStatusT = chkStatus.Checked;
                    oCrew.IsAirport = chkAirport.Checked;
                    oCrew.IsCheckList = chkChecklist.Checked;
                    oCrew.IsRunway = chkRunway.Checked;
                    oCrew.IsHomeArrivalTM = chkHomeArrival.Checked;
                    oCrew.IsHomeDEPARTTM = chkHomeDeparture.Checked;
                    if (radLocal.Checked == true)
                    {
                        oCrew.BlackBerryTM = "L";
                    }
                    else
                    {
                        oCrew.BlackBerryTM = "Z";
                    }
                    oCrew.IsEndDuty = chkEndDuty.Checked;
                    oCrew.IsOverride = chkOverride.Checked;
                    oCrew.IsCrewRules = chkCrewRules.Checked;
                    oCrew.IsFAR = chkFAR.Checked;
                    oCrew.IsDuty = chkDutyHours.Checked;
                    oCrew.IsFlightHours = chkFlightHours.Checked;
                    oCrew.IsRestHrs = chkRestHours.Checked;
                    oCrew.IsAssociatedCrew = chkAssociatedCrew.Checked;
                    oCrew.Label1 = chkCOFFEE.Checked;
                    oCrew.Label2 = chkNEWSPAPER.Checked;
                    oCrew.Label3 = chkJUICE.Checked;
                    oCrew.IsDepartureFBO = chkDepartureInformation.Checked;
                    oCrew.IsArrivalFBO = chkArrivalInformation.Checked;
                    oCrew.IsCrewDepartTRANS = chkCrewTransport.Checked;
                    oCrew.IsCrewArrivalTRANS = chkCrewArrival.Checked;
                    oCrew.IsHotel = chkHotel.Checked;
                    oCrew.IsPassDepartTRANS = chkPassengerTransport.Checked;
                    oCrew.IsPassArrivalHotel = chkPassengerArrival.Checked;
                    oCrew.IsPassDetails = chkPassengerDetails.Checked;
                    oCrew.IsPassHotel = chkPassengerHotel.Checked;
                    oCrew.IsPassPhoneNum = chkPassengerPaxPhone.Checked;
                    oCrew.IsDepartCatering = chkDepartureCatering.Checked;
                    oCrew.IsArrivalCatering = chkArrivalCatering.Checked;
                    oCrew.IsArrivalDepartTime = chkArrivalDepartureTime.Checked;
                    oCrew.IsAircraft = chkAircraft.Checked;
                    oCrew.IcaoID = chkArrivalDepartureICAO.Checked;
                    oCrew.IsCrew = chkGeneralCrew.Checked;
                    oCrew.IsPassenger = chkGeneralPassenger.Checked;
                    oCrew.IsOutboundINST = chkOutBoundInstructions.Checked;
                    oCrew.IsCheckList1 = chkGeneralChecklist.Checked;
                    oCrew.IsFBO = chkLogisticsFBO.Checked;
                    oCrew.IsHotel = chkLogisticsHotel.Checked;
                    oCrew.IsTransportation = chkLogisticsTransportation.Checked;
                    oCrew.IsCatering = chkLogisticsCatering.Checked;

                    oCrew.IsCrewHotelConfirm = chkCrewHotelConfirm.Checked;
                    oCrew.IsCrewHotelComments = chkCrewHotelComments.Checked;
                    oCrew.IsCrewDepartTRANSConfirm = chkCrewDeptTransConfirm.Checked;
                    oCrew.IsCrewDepartTRANSComments = chkCrewDeptTransComments.Checked;
                    oCrew.IsCrewArrivalConfirm = chkCrewArrTransConfirm.Checked;
                    oCrew.IsCrewArrivalComments = chkCrewArrTransComments.Checked;
                    oCrew.IsCrewNotes = chkCrewNotes.Checked;
                    oCrew.IsPassHotelConfirm = chkPAXHotelConfirm.Checked;
                    oCrew.IsPassHotelComments = chkPAXHotelComments.Checked;
                    oCrew.IsPassDepartTRANSConfirm = chkPAXDeptTransConfirm.Checked;
                    oCrew.IsPassDepartTRANSComments = chkPAXDeptTransComments.Checked;
                    oCrew.IsPassArrivalConfirm = chkPAXArrTransConfirm.Checked;
                    oCrew.IsPassArrivalComments = chkPAXArrTransComments.Checked;
                    oCrew.IsPassNotes = chkPAXNotes.Checked;
                    oCrew.IsOutboundComments = chkOutboundInstComments.Checked;
                    oCrew.IsDepartAirportNotes = chkDeptAirportNotes.Checked;
                    oCrew.IsArrivalAirportNoes = chkArrAirportNotes.Checked;
                    oCrew.IsDepartAirportAlerts = chkDeptAirportAlerts.Checked;
                    oCrew.IsArrivalAirportAlerts = chkArrAirportAlerts.Checked;
                    oCrew.IsFBOArrivalConfirm = chkArrFBOConfirm.Checked;
                    oCrew.IsFBOArrivalComment = chkArrFBOComments.Checked;
                    oCrew.IsFBODepartConfirm = chkDeptFBOConfirm.Checked;
                    oCrew.IsFBODepartComment = chkDeptFBOComments.Checked;
                    oCrew.IsDepartCateringConfirm = chkDeptCaterConfirm.Checked;
                    oCrew.IsDepartCateringComment = chkDeptCaterComments.Checked;
                    oCrew.IsArrivalCateringConfirm = chkArrCaterConfirm.Checked;
                    oCrew.IsArrivalCateringComment = chkArrCaterComments.Checked;
                    oCrew.IsTripAlerts = chkTripAlerts.Checked;
                    oCrew.IsTripNotes = chkTripNotes.Checked;
                    oCrew.IsDeleted = false;
                    // oCrew.LastUpdUID = "UC";
                    oCrew.LastUpdTS = System.DateTime.UtcNow;


                    if (!string.IsNullOrEmpty(tbAddress3.Text))
                    {
                        oCrew.Addr3 = tbAddress3.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                    {
                        oCrew.OtherEmail = tbOtherEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                    {
                        oCrew.PersonalEmail = tbPersonalEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                    {
                        oCrew.OtherPhone = tbOtherPhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbSecondaryMobile.Text))
                    {
                        oCrew.CellPhoneNum2 = tbSecondaryMobile.Text;  // Secondary Mobile / Cell
                    }
                    if (!string.IsNullOrEmpty(tbBusinessFax.Text))
                    {
                        oCrew.BusinessFax = tbBusinessFax.Text;
                    }
                    if (!string.IsNullOrEmpty(tbBusinessPhone.Text))
                    {
                        oCrew.BusinessPhone = tbBusinessPhone.Text;
                    }


                    //<new fields>

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oCrew;
            }
        }
        /// <summary>
        /// Method to Save Crew Definiton Informations
        /// </summary>
        /// <param name="CrewID">Pass Crew Code</param>
        private FlightPakMasterService.Crew SaveCrewDefinition(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {


                    oCrew.CrewDefinition = new List<CrewDefinition>();

                    Int64 Indentity = 0;


                    List<FlightPakMasterService.GetCrewDefinition> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewDefinition>();
                    FlightPakMasterService.CrewDefinition CrewDefinition = new FlightPakMasterService.CrewDefinition();
                    CrewDefinitionInfoList = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewDefinitionInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewDefinition = new CrewDefinition();
                        foreach (GridDataItem Item in dgCrewAddlInfo.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("CrewInfoID").ToString() == CrewDefinitionInfoList[Index].CrewInfoID.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("CrewInfoXRefID").ToString()) != 0)
                                {
                                    CrewDefinition.CrewInfoXRefID = Convert.ToInt64(Item.GetDataKeyValue("CrewInfoXRefID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewDefinition.CrewInfoXRefID = Indentity;
                                    //CrewDefinition.CrewInfoID = Indentity;
                                }
                                CrewDefinition.CrewID = oCrew.CrewID;// CrewDefinitionInfoList[Index].CrewID;
                                CrewDefinition.CrewInfoID = CrewDefinitionInfoList[Index].CrewInfoID;
                                CrewDefinition.CustomerID = oCrew.CustomerID;
                                CrewDefinition.InformationDESC = CrewDefinitionInfoList[Index].InformationDESC;
                                CrewDefinition.InformationValue = ((TextBox)Item["AddlInfo"].FindControl("tbAddlInfo")).Text;//CrewDefinitionInfoList[Index].InformationValue;
                                if (CrewDefinitionInfoList[Index].IsDeleted.HasValue)
                                {
                                    CrewDefinition.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted.Value;
                                }
                                CrewDefinition.IsReptFilter = ((CheckBox)Item["IsReptFilter"].FindControl("chkIsReptFilter")).Checked;
                                CrewDefinition.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                                CrewDefinition.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                                CrewDefinition.IsDeleted = false;
                                IsExist = true;
                                break;
                            }
                        }
                        //IsExist
                        if (IsExist == false)
                        {
                            if (CrewDefinitionInfoList[Index].CrewInfoXRefID != 0)
                            {
                                CrewDefinition.CrewInfoXRefID = CrewDefinitionInfoList[Index].CrewInfoXRefID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewDefinition.CrewInfoID = Indentity;
                            }
                            CrewDefinition.CrewID = CrewDefinitionInfoList[Index].CrewID;
                            CrewDefinition.CrewInfoID = CrewDefinitionInfoList[Index].CrewInfoID;
                            CrewDefinition.CrewInfoXRefID = CrewDefinitionInfoList[Index].CrewInfoXRefID;
                            CrewDefinition.CustomerID = CrewDefinitionInfoList[Index].CustomerID;
                            CrewDefinition.InformationDESC = CrewDefinitionInfoList[Index].InformationDESC;
                            CrewDefinition.InformationValue = CrewDefinitionInfoList[Index].InformationValue;
                            if (CrewDefinitionInfoList[Index].IsDeleted.HasValue)
                            {
                                CrewDefinition.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted.Value;
                            }
                            CrewDefinition.IsReptFilter = CrewDefinitionInfoList[Index].IsReptFilter;
                            CrewDefinition.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                            CrewDefinition.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                        }
                        if (CrewDefinition.CrewInfoXRefID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewDefinition.CrewInfoXRefID = Indentity;
                        }
                        oCrew.CrewDefinition.Add(CrewDefinition);
                    }
                    return oCrew;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oCrew;
            }
        }

        /// <summary>
        /// Method to Save Crew Visa Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>

        //private FlightPakMasterService.Crew SaveCrewVisa(FlightPakMasterService.Crew oCrew)
        private FlightPakMasterService.Crew SaveCrewVisa(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Crew>(() =>
                {
                    // added information items from the Grid to List
                    oCrew.CrewPassengerVisa = new List<FlightPakMasterService.CrewPassengerVisa>();
                    FlightPakMasterService.CrewPassengerVisa CrewPaxVisaInfoListDef = new FlightPakMasterService.CrewPassengerVisa();
                    List<FlightPakMasterService.GetAllCrewPaxVisa> CrewPaxVisaInfoList = new List<FlightPakMasterService.GetAllCrewPaxVisa>();
                    CrewPaxVisaInfoList = (List<GetAllCrewPaxVisa>)Session["CrewVisa"];
                    List<FlightPakMasterService.CrewPassengerVisa> CrewPaxVisaGridInfo = new List<FlightPakMasterService.CrewPassengerVisa>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgVisa.MasterTableView.Items)
                    {
                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                        if (Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) != 0)
                        {
                            CrewPaxVisaInfoListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                        }
                        CrewPaxVisaInfoListDef.CustomerID = oCrew.CustomerID;
                        CrewPaxVisaInfoListDef.CrewID = oCrew.CrewID;// Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                        CrewPaxVisaInfoListDef.PassengerRequestorID = null;
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                        }
                        CrewPaxVisaInfoListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaInfoListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaInfoListDef.VisaNum = string.Empty;
                        }
                        //if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        //{
                        //    CrewPaxVisaInfoListDef.ExpiryDT = DateTime.ParseExact(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat, CultureInfo.InvariantCulture);
                        //}

                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }

                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        //if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        //{
                        //    CrewPaxVisaInfoListDef.IssueDate = DateTime.ParseExact(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat, CultureInfo.InvariantCulture);
                        //}


                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaInfoListDef.IssueDate = FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaInfoListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }

                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaInfoListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaInfoListDef.IsDeleted = false;
                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                    }

                    if (CrewPaxVisaInfoList != null)    //added for testing Delete & add scenario
                    {
                        for (int sIndex = 0; sIndex < CrewPaxVisaInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxVisaGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxVisaInfoList[sIndex].VisaNum == CrewPaxVisaGridInfo[gIndex].VisaNum) || (CrewPaxVisaInfoList[sIndex].VisaID == CrewPaxVisaGridInfo[gIndex].VisaID))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxVisaInfoList[sIndex].VisaNum != null)
                                {
                                    if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                    {
                                        CrewPaxVisaInfoListDef = new CrewPassengerVisa();
                                        CrewPaxVisaInfoListDef.CountryID = CrewPaxVisaInfoList[sIndex].CountryID;
                                        CrewPaxVisaInfoListDef.CrewID = CrewPaxVisaInfoList[sIndex].CrewID;
                                        CrewPaxVisaInfoListDef.CustomerID = CrewPaxVisaInfoList[sIndex].CustomerID;
                                        CrewPaxVisaInfoListDef.ExpiryDT = CrewPaxVisaInfoList[sIndex].ExpiryDT;
                                        CrewPaxVisaInfoListDef.IsDeleted = true;// CrewPaxVisaInfoList[sIndex].IsDeleted;
                                        CrewPaxVisaInfoListDef.IssueDate = CrewPaxVisaInfoList[sIndex].IssueDate;
                                        CrewPaxVisaInfoListDef.IssuePlace = CrewPaxVisaInfoList[sIndex].IssuePlace;
                                        CrewPaxVisaInfoListDef.LastUpdTS = CrewPaxVisaInfoList[sIndex].LastUpdTS;
                                        CrewPaxVisaInfoListDef.LastUpdUID = CrewPaxVisaInfoList[sIndex].LastUpdUID;
                                        CrewPaxVisaInfoListDef.Notes = CrewPaxVisaInfoList[sIndex].Notes;
                                        CrewPaxVisaInfoListDef.PassengerRequestorID = CrewPaxVisaInfoList[sIndex].PassengerRequestorID;
                                        if (CrewPaxVisaInfoList[sIndex].VisaID != 0)
                                        {
                                            CrewPaxVisaInfoListDef.VisaID = CrewPaxVisaInfoList[sIndex].VisaID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxVisaInfoListDef.VisaID = Indentity;
                                        }
                                        CrewPaxVisaInfoListDef.VisaNum = CrewPaxVisaInfoList[sIndex].VisaNum;
                                        CrewPaxVisaInfoListDef.VisaTYPE = CrewPaxVisaInfoList[sIndex].VisaTYPE;
                                        CrewPaxVisaGridInfo.Add(CrewPaxVisaInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oCrew.CrewPassengerVisa = CrewPaxVisaGridInfo;
                    return oCrew;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Save Crew Passport Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>

        private FlightPakMasterService.Crew SaveCrewPassport(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<FlightPakMasterService.Crew>(() =>
                {
                    // added information items from the Grid to List
                    oCrew.CrewPassengerPassport = new List<FlightPakMasterService.CrewPassengerPassport>();
                    FlightPakMasterService.CrewPassengerPassport CrewPaxPassportInfoListDef = new FlightPakMasterService.CrewPassengerPassport();
                    List<FlightPakMasterService.GetAllCrewPassport> CrewPaxPassportInfoList = new List<FlightPakMasterService.GetAllCrewPassport>();
                    CrewPaxPassportInfoList = (List<GetAllCrewPassport>)Session["CrewPassport"];
                    List<FlightPakMasterService.CrewPassengerPassport> CrewPaxPassportGridInfo = new List<FlightPakMasterService.CrewPassengerPassport>();
                    Int64 Indentity = 0;
                    bool IsExist = false;
                    foreach (GridDataItem Item in dgPassport.MasterTableView.Items)
                    {
                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                        if (Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString()) != 0)
                        {
                            CrewPaxPassportInfoListDef.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        }
                        else
                        {
                            Indentity = Indentity - 1;
                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                        }

                        CrewPaxPassportInfoListDef.PassengerRequestorID = null;
                        CrewPaxPassportInfoListDef.CrewID = oCrew.CrewID;
                        CrewPaxPassportInfoListDef.CustomerID = oCrew.CustomerID;
                        if (((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        CrewPaxPassportInfoListDef.Choice = ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked;


                        if (((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat);
                                //CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(String.Format("{0:" + DateFormat + "}", ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text));
                                ////DateTime.Parse(string.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text)); //DateTime.Parse((((TextBox)Item["ExpiryDT"].FindControl("tbPassportExpiryDT")).Text).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }

                        if (((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }


                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxPassportInfoListDef.IssueDT = FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbIssueDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxPassportInfoListDef.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }

                        if (((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPaxPassportInfoListDef.CountryID = Convert.ToInt64(((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value);
                        }
                        CrewPaxPassportInfoListDef.IsDeleted = false;
                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                    }
                    if (CrewPaxPassportInfoList != null)    //added for testing Delete & add scenario
                    {
                        for (int sIndex = 0; sIndex < CrewPaxPassportInfoList.Count; sIndex++)
                        {
                            IsExist = false;
                            for (int gIndex = 0; gIndex < CrewPaxPassportGridInfo.Count; gIndex++)
                            {
                                if ((CrewPaxPassportInfoList[sIndex].PassportNum == CrewPaxPassportGridInfo[gIndex].PassportNum) || (CrewPaxPassportInfoList[sIndex].PassportNum == CrewPaxPassportGridInfo[gIndex].PassportNum))
                                {
                                    IsExist = true;
                                    break;
                                }
                            }
                            if (IsExist == false)
                            {
                                if (CrewPaxPassportInfoList[sIndex].PassportNum != null)
                                {
                                    if (CrewPaxPassportInfoList[sIndex].PassportID != 0)
                                    {
                                        CrewPaxPassportInfoListDef = new CrewPassengerPassport();
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.Choice = CrewPaxPassportInfoList[sIndex].Choice;
                                        CrewPaxPassportInfoListDef.CountryID = CrewPaxPassportInfoList[sIndex].CountryID;
                                        CrewPaxPassportInfoListDef.CrewID = CrewPaxPassportInfoList[sIndex].CrewID;
                                        CrewPaxPassportInfoListDef.CustomerID = CrewPaxPassportInfoList[sIndex].CustomerID;
                                        CrewPaxPassportInfoListDef.IsDefaultPassport = CrewPaxPassportInfoList[sIndex].IsDefaultPassport;
                                        if (CrewPaxPassportInfoList[sIndex].IsDeleted.HasValue)
                                        {
                                            CrewPaxPassportInfoListDef.IsDeleted = CrewPaxPassportInfoList[sIndex].IsDeleted.Value;
                                        }
                                        CrewPaxPassportInfoListDef.IssueCity = CrewPaxPassportInfoList[sIndex].IssueCity;
                                        CrewPaxPassportInfoListDef.IssueDT = CrewPaxPassportInfoList[sIndex].IssueDT;
                                        CrewPaxPassportInfoListDef.LastUpdTS = CrewPaxPassportInfoList[sIndex].LastUpdTS;
                                        CrewPaxPassportInfoListDef.LastUpdUID = CrewPaxPassportInfoList[sIndex].LastUpdUID;
                                        CrewPaxPassportInfoListDef.PassengerRequestorID = CrewPaxPassportInfoList[sIndex].PassengerRequestorID;
                                        CrewPaxPassportInfoListDef.PassportExpiryDT = CrewPaxPassportInfoList[sIndex].PassportExpiryDT;
                                        CrewPaxPassportInfoListDef.PassportNum = CrewPaxPassportInfoList[sIndex].PassportNum;
                                        CrewPaxPassportInfoListDef.PilotLicenseNum = CrewPaxPassportInfoList[sIndex].PilotLicenseNum;
                                        if (CrewPaxPassportInfoList[sIndex].PassportID != 0)
                                        {
                                            CrewPaxPassportInfoListDef.PassportID = CrewPaxPassportInfoList[sIndex].PassportID;
                                        }
                                        else
                                        {
                                            Indentity = Indentity - 1;
                                            CrewPaxPassportInfoListDef.PassportID = Indentity;
                                        }
                                        CrewPaxPassportGridInfo.Add(CrewPaxPassportInfoListDef);
                                    }
                                }
                            }
                        }
                    }
                    oCrew.CrewPassengerPassport = CrewPaxPassportGridInfo;
                    return oCrew;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Save Type Rating Informations
        /// </summary>
        /// <param name="CrewCode">Pass Crew Code</param>


        private FlightPakMasterService.Crew SaveTypeRatings(FlightPakMasterService.Crew oCrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    oCrew.CrewRating = new List<CrewRating>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.GetCrewRating> CrewTypeRatingInfoList = new List<FlightPakMasterService.GetCrewRating>();
                    FlightPakMasterService.CrewRating CrewRating = new FlightPakMasterService.CrewRating();
                    CrewTypeRatingInfoList = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewTypeRatingInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewRating = new CrewRating();
                        foreach (GridDataItem Item in dgTypeRating.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AircraftTypeID").ToString() == CrewTypeRatingInfoList[Index].AircraftTypeID.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString()) != 0)
                                {
                                    CrewRating.CrewRatingID = Convert.ToInt64(Item.GetDataKeyValue("CrewRatingID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewRating.CrewRatingID = Indentity;  // I think CrewRating shud cum here karthik
                                }
                                CrewRating.CrewID = oCrew.CrewID;// CrewDefinitionInfoList[Index].CrewID;
                                if (hdnClientID.Value != string.Empty && hdnClientID.Value != null)
                                {
                                    CrewRating.ClientID = Convert.ToInt64(hdnClientID.Value);
                                }

                                CrewRating.AircraftTypeID = CrewTypeRatingInfoList[Index].AircraftTypeID;
                                CrewRating.CustomerID = oCrew.CustomerID;
                                CrewRating.CrewRatingDescription = Item.GetDataKeyValue("CrewRatingDescription").ToString();

                                //CrewRating.IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                                //CrewRating.IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                                //CrewRating.IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                                //CrewRating.IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                                //CrewRating.IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                                //CrewRating.IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                                //CrewRating.IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                                //CrewRating.IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                                //CrewRating.IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                                //CrewRating.IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                                //CrewRating.IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                                //CrewRating.IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                                //CrewRating.IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                                //CrewRating.IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                                //CrewRating.IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                                //CrewRating.IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                                //CrewRating.IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;

                                if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                                {
                                    CrewRating.IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPilotinCommand = false;
                                }

                                if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                                {
                                    CrewRating.IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSecondInCommand = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                                {
                                    CrewRating.IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsQualInType135PIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                                {
                                    CrewRating.IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsQualInType135SIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                                {
                                    CrewRating.IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsEngineer = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                                {
                                    CrewRating.IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsInstructor = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                                {
                                    CrewRating.IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsCheckAttendant = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                                {
                                    CrewRating.IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendantFAR91 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                                {
                                    CrewRating.IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendantFAR135 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                                {
                                    CrewRating.IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsCheckAirman = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                                {
                                    CrewRating.IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsInActive = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                                {
                                    CrewRating.IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPIC121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                                {
                                    CrewRating.IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSIC121 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                                {
                                    CrewRating.IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsPIC125 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                                {
                                    CrewRating.IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsSIC125 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                                {
                                    CrewRating.IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendant121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                                {
                                    CrewRating.IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                                }
                                else
                                {
                                    CrewRating.IsAttendant125 = false;
                                }


                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                {
                                    CrewRating.TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TotalTimeInTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                {
                                    CrewRating.TotalDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TotalDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                {
                                    CrewRating.TotalNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TotalNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                {
                                    CrewRating.TotalINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TotalINSTHrs = 0;
                                }


                                // PIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                {
                                    CrewRating.PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.PilotInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                {
                                    CrewRating.TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TPilotinCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                {
                                    CrewRating.TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TPilotInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                {
                                    CrewRating.TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.TPilotInCommandINSTHrs = 0;
                                }



                                // SIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                {
                                    CrewRating.SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                {
                                    CrewRating.SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                {
                                    CrewRating.SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                {
                                    CrewRating.SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.SecondInCommandINSTHrs = 0;
                                }




                                // Others Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                {
                                    CrewRating.OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherSimHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                {
                                    CrewRating.OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherFlightEngHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                {
                                    CrewRating.OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherFlightInstrutorHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                {
                                    CrewRating.OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRating.OtherFlightAttdHrs = 0;
                                }

                                //// Qualified In Type As
                                //CrewRating.IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                                //CrewRating.IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                                //CrewRating.IsQualInType135PIC = Convert.ToBoolean(Item.GetDataKeyValue("IsQualInType135PIC"));
                                //CrewRating.IsQualInType135SIC = Convert.ToBoolean(Item.GetDataKeyValue("IsQualInType135SIC"));
                                //CrewRating.IsEngineer = Convert.ToBoolean(Item.GetDataKeyValue("IsEngineer"));
                                //CrewRating.IsInstructor = Convert.ToBoolean(Item.GetDataKeyValue("IsInstructor"));
                                //CrewRating.IsCheckAttendant = Convert.ToBoolean(Item.GetDataKeyValue("IsCheckAttendant"));
                                //CrewRating.IsAttendantFAR91 = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendantFAR91"));
                                //CrewRating.IsAttendantFAR135 = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendantFAR135"));
                                //CrewRating.IsCheckAirman = Convert.ToBoolean(Item.GetDataKeyValue("IsCheckAirman"));
                                //CrewRating.IsInActive = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive"));

                                ////<new field>


                                // As of Date
                                if (pnlBeginning.Visible == true && (((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text != string.Empty) && (((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text != null))
                                {
                                    //if (DateFormat != null)
                                    //{
                                    //    CrewRating.AsOfDT = FormatDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim(), DateFormat);
                                    //}
                                    //else
                                    //{
                                    CrewRating.AsOfDT = Convert.ToDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim());
                                    //}
                                }

                                // Total Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                                {
                                    //if (DateFormat != null)
                                    //{
                                    //    CrewRating.AsOfDT = FormatDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim(), DateFormat);
                                    //}
                                    //else
                                    //{
                                    CrewRating.TotalUpdateAsOfDT = Convert.ToDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim());
                                    //}
                                }
                                //CrewRating.TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["TotalTimeInTypeHrs"].FindControl("lbTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                //CrewRating.TotalDayHrs = Convert.ToDecimal(((Label)Item["TotalDayHrs"].FindControl("lbTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                //CrewRating.TotalNightHrs = Convert.ToDecimal(((Label)Item["TotalNightHrs"].FindControl("lbTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                //CrewRating.TotalINSTHrs = Convert.ToDecimal(((Label)Item["TotalINSTHrs"].FindControl("lbTotalINSTHrs")).Text, CultureInfo.CurrentCulture);

                                //// PIC Hrs
                                //CrewRating.PilotInCommandTypeHrs = Convert.ToDecimal(Item.GetDataKeyValue("PilotInCommandTypeHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.TPilotinCommandDayHrs = Convert.ToDecimal(Item.GetDataKeyValue("TPilotinCommandDayHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.TPilotInCommandNightHrs = Convert.ToDecimal(Item.GetDataKeyValue("TPilotInCommandNightHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.TPilotInCommandINSTHrs = Convert.ToDecimal(Item.GetDataKeyValue("TPilotInCommandINSTHrs").ToString(), CultureInfo.CurrentCulture);

                                //// SIC Hrs
                                //CrewRating.SecondInCommandTypeHrs = Convert.ToDecimal(Item.GetDataKeyValue("SecondInCommandTypeHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.SecondInCommandDayHrs = Convert.ToDecimal(Item.GetDataKeyValue("SecondInCommandDayHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.SecondInCommandNightHrs = Convert.ToDecimal(Item.GetDataKeyValue("SecondInCommandNightHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.SecondInCommandINSTHrs = Convert.ToDecimal(Item.GetDataKeyValue("SecondInCommandINSTHrs").ToString(), CultureInfo.CurrentCulture);

                                //// Other Hrs
                                //CrewRating.OtherSimHrs = Convert.ToDecimal(Item.GetDataKeyValue("OtherSimHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.OtherFlightEngHrs = Convert.ToDecimal(Item.GetDataKeyValue("OtherFlightEngHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.OtherFlightInstrutorHrs = Convert.ToDecimal(Item.GetDataKeyValue("OtherFlightInstrutorHrs").ToString(), CultureInfo.CurrentCulture);
                                //CrewRating.OtherFlightAttdHrs = Convert.ToDecimal(Item.GetDataKeyValue("OtherFlightAttdHrs").ToString(), CultureInfo.CurrentCulture);

                                ////<new field>
                                ////<new fields>
                                //CrewRating.IsPIC121 = Convert.ToBoolean(Item.GetDataKeyValue("IsPIC121"));
                                //CrewRating.IsSIC121 = Convert.ToBoolean(Item.GetDataKeyValue("IsSIC121"));
                                //CrewRating.IsPIC125 = Convert.ToBoolean(Item.GetDataKeyValue("IsPIC125"));
                                //CrewRating.IsSIC125 = Convert.ToBoolean(Item.GetDataKeyValue("IsSIC125"));
                                //CrewRating.IsAttendant121 = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendant121"));
                                //CrewRating.IsAttendant125 = Convert.ToBoolean(Item.GetDataKeyValue("IsAttendant125"));

                                //CrewRating.IsDeleted = CrewTypeRatingInfoList[Index].IsDeleted;
                                CrewRating.LastUpdTS = CrewTypeRatingInfoList[Index].LastUpdTS;
                                CrewRating.LastUpdUID = CrewTypeRatingInfoList[Index].LastUpdUID;
                                CrewRating.IsDeleted = false; // I think CrewRating shud cum here 
                                IsExist = true;
                                break;
                            }
                        }
                        if (IsExist == false)
                        {
                            if (CrewTypeRatingInfoList[Index].CrewRatingID != 0)
                            {
                                CrewRating.CrewRatingID = CrewTypeRatingInfoList[Index].CrewRatingID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewRating.CrewRatingID = Indentity;
                            }
                            CrewRating.CrewID = CrewTypeRatingInfoList[Index].CrewID;
                            CrewRating.AircraftTypeID = CrewTypeRatingInfoList[Index].AircraftTypeID;
                            CrewRating.CrewRatingID = CrewTypeRatingInfoList[Index].CrewRatingID;
                            CrewRating.CustomerID = CrewTypeRatingInfoList[Index].CustomerID;
                            if (CrewTypeRatingInfoList[Index].IsDeleted)
                            {
                                CrewRating.IsDeleted = CrewTypeRatingInfoList[Index].IsDeleted;
                            }
                            //CrewRating.LastUpdTS = CrewTypeRatingInfoList[Index].LastUpdTS;
                            //CrewRating.LastUpdUID = CrewTypeRatingInfoList[Index].LastUpdUID;
                            CrewRating.CrewRatingDescription = CrewTypeRatingInfoList[Index].CrewRatingDescription;
                            CrewRating.AsOfDT = CrewTypeRatingInfoList[Index].AsOfDT;
                            CrewRating.TotalUpdateAsOfDT = CrewTypeRatingInfoList[Index].TotalUpdateAsOfDT;

                            CrewRating.IsPilotinCommand = CrewTypeRatingInfoList[Index].IsPilotinCommand;
                            CrewRating.IsSecondInCommand = CrewTypeRatingInfoList[Index].IsSecondInCommand;
                            CrewRating.IsQualInType135PIC = CrewTypeRatingInfoList[Index].IsQualInType135PIC;
                            CrewRating.IsQualInType135SIC = CrewTypeRatingInfoList[Index].IsQualInType135PIC;

                            CrewRating.IsEngineer = CrewTypeRatingInfoList[Index].IsEngineer;
                            CrewRating.IsInstructor = CrewTypeRatingInfoList[Index].IsInstructor;
                            CrewRating.IsCheckAttendant = CrewTypeRatingInfoList[Index].IsCheckAttendant;
                            CrewRating.IsAttendantFAR91 = CrewTypeRatingInfoList[Index].IsAttendantFAR91;
                            CrewRating.IsAttendantFAR135 = CrewTypeRatingInfoList[Index].IsAttendantFAR135;
                            CrewRating.IsCheckAirman = CrewTypeRatingInfoList[Index].IsCheckAirman;
                            CrewRating.IsInActive = CrewTypeRatingInfoList[Index].IsInActive;

                            CrewRating.IsPIC121 = CrewTypeRatingInfoList[Index].IsPIC121;
                            CrewRating.IsSIC121 = CrewTypeRatingInfoList[Index].IsSIC121;
                            CrewRating.IsPIC125 = CrewTypeRatingInfoList[Index].IsPIC125;
                            CrewRating.IsSIC125 = CrewTypeRatingInfoList[Index].IsSIC125;
                            CrewRating.IsAttendant121 = CrewTypeRatingInfoList[Index].IsAttendant121;
                            CrewRating.IsAttendant125 = CrewTypeRatingInfoList[Index].IsAttendant125;


                            CrewRating.TotalTimeInTypeHrs = CrewTypeRatingInfoList[Index].TotalTimeInTypeHrs;
                            CrewRating.TotalDayHrs = CrewTypeRatingInfoList[Index].TotalDayHrs;
                            CrewRating.TotalNightHrs = CrewTypeRatingInfoList[Index].TotalNightHrs;
                            CrewRating.TotalINSTHrs = CrewTypeRatingInfoList[Index].TotalINSTHrs;

                            // PIC Hrs
                            CrewRating.PilotInCommandTypeHrs = CrewTypeRatingInfoList[Index].PilotInCommandTypeHrs;
                            CrewRating.TPilotinCommandDayHrs = CrewTypeRatingInfoList[Index].TPilotinCommandDayHrs;
                            CrewRating.TPilotInCommandNightHrs = CrewTypeRatingInfoList[Index].TPilotInCommandNightHrs;
                            CrewRating.TPilotInCommandINSTHrs = CrewTypeRatingInfoList[Index].TPilotInCommandINSTHrs;

                            // SIC Hrs
                            CrewRating.SecondInCommandTypeHrs = CrewTypeRatingInfoList[Index].SecondInCommandTypeHrs;
                            CrewRating.SecondInCommandDayHrs = CrewTypeRatingInfoList[Index].SecondInCommandDayHrs;
                            CrewRating.SecondInCommandNightHrs = CrewTypeRatingInfoList[Index].SecondInCommandNightHrs;
                            CrewRating.SecondInCommandINSTHrs = CrewTypeRatingInfoList[Index].SecondInCommandINSTHrs;



                            // Other Hrs
                            CrewRating.OtherSimHrs = CrewTypeRatingInfoList[Index].OtherSimHrs;
                            CrewRating.OtherFlightEngHrs = CrewTypeRatingInfoList[Index].OtherFlightEngHrs;
                            CrewRating.OtherFlightInstrutorHrs = CrewTypeRatingInfoList[Index].OtherFlightInstrutorHrs;
                            CrewRating.OtherFlightAttdHrs = CrewTypeRatingInfoList[Index].OtherFlightAttdHrs;


                        }
                        if (CrewRating.CrewRatingID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewRating.CrewRatingID = Indentity;
                        }
                        oCrew.CrewRating.Add(CrewRating);
                    }
                    return oCrew;
                }, FlightPak.Common.Constants.Policy.UILayer);

                return oCrew;
            }
        }

        /// <summary>
        /// Method to Save Crew Checklist Informations
        /// </summary>
        /// <param name="CrewCode"></param>

        private FlightPakMasterService.Crew SaveCrewChecklist(FlightPakMasterService.Crew oCrew)
        {
            //Soft Delete the previous Crew Checklist Items 
            //belongs to the specified Crew Code.

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    oCrew.CrewCheckListDetail = new List<CrewCheckListDetail>();
                    Int64 Indentity = 0;
                    List<FlightPakMasterService.GetCrewCheckListDate> CrewDefinitionInfoList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                    FlightPakMasterService.CrewCheckListDetail CrewCheckListDetail = new FlightPakMasterService.CrewCheckListDetail();
                    CrewDefinitionInfoList = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                    bool IsExist = false;
                    for (int Index = 0; Index < CrewDefinitionInfoList.Count; Index++)
                    {
                        IsExist = false;
                        CrewCheckListDetail = new CrewCheckListDetail();
                        foreach (GridDataItem Item in dgCrewCheckList.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("CheckListCD").ToString() == CrewDefinitionInfoList[Index].CheckListCD.ToString())
                            {
                                if (Convert.ToInt64(Item.GetDataKeyValue("CheckListID").ToString()) != 0)
                                {
                                    CrewCheckListDetail.CheckListID = Convert.ToInt64(Item.GetDataKeyValue("CheckListID").ToString());
                                }
                                else
                                {
                                    Indentity = Indentity - 1;
                                    CrewCheckListDetail.CheckListID = Indentity; // CrewINFO ID
                                }
                                CrewCheckListDetail.CrewID = oCrew.CrewID;
                                if (CrewDefinitionInfoList[Index].IsDeleted)
                                {
                                    CrewCheckListDetail.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted;
                                }
                                if (Item.GetDataKeyValue("CheckListCD") != null)
                                {
                                    CrewCheckListDetail.CheckListCD = (Item.GetDataKeyValue("CheckListCD").ToString());
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.OriginalDT = FormatDate(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.OriginalDT = Convert.ToDateTime(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.PreviousCheckDT = FormatDate(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.PreviousCheckDT = Convert.ToDateTime(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[DueDT].FindControl(lbDueDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.DueDT = FormatDate(((Label)Item[DueDT].FindControl(lbDueDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.DueDT = Convert.ToDateTime(((Label)Item[DueDT].FindControl(lbDueDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.AlertDT = FormatDate(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.AlertDT = Convert.ToDateTime(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text);
                                    }
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                                {
                                    CrewCheckListDetail.Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                                }

                                if (!string.IsNullOrEmpty(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.GraceDT = FormatDate(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.GraceDT = Convert.ToDateTime(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        CrewCheckListDetail.BaseMonthDT = FormatDate(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        CrewCheckListDetail.BaseMonthDT = Convert.ToDateTime(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                                {
                                    CrewCheckListDetail.GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                                {
                                    CrewCheckListDetail.AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                                {
                                    CrewCheckListDetail.FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text))
                                {
                                    CrewCheckListDetail.AircraftID = Convert.ToInt64(((HiddenField)Item["AircraftTypeCD"].FindControl("hdnAircraftTypeCD")).Value); // ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text;
                                }
                                CrewCheckListDetail.IsPilotInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsPilotInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked;
                                CrewCheckListDetail.IsSecondInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsSecondInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked;
                                CrewCheckListDetail.IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                                CrewCheckListDetail.IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                                CrewCheckListDetail.IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                                CrewCheckListDetail.IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                                CrewCheckListDetail.IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                                CrewCheckListDetail.IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                                CrewCheckListDetail.IsNoCrewCheckListREPTt = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked;
                                CrewCheckListDetail.IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                                CrewCheckListDetail.IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                                CrewCheckListDetail.IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                CrewCheckListDetail.IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                                CrewCheckListDetail.IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                                CrewCheckListDetail.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                                CrewCheckListDetail.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                                CrewCheckListDetail.IsDeleted = false;
                                IsExist = true;
                                break;
                            }
                        }
                        //IsExist
                        if (IsExist == false)
                        {
                            if (CrewDefinitionInfoList[Index].CheckListID != 0)
                            {
                                CrewCheckListDetail.CheckListID = CrewDefinitionInfoList[Index].CheckListID;
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewCheckListDetail.CheckListID = Indentity; //CrewInfoID
                            }
                            CrewCheckListDetail.CrewID = CrewDefinitionInfoList[Index].CrewID;
                            CrewCheckListDetail.CustomerID = CrewDefinitionInfoList[Index].CustomerID;

                            if (CrewDefinitionInfoList[Index].IsDeleted)
                            {
                                CrewCheckListDetail.IsDeleted = CrewDefinitionInfoList[Index].IsDeleted;
                            }
                            CrewCheckListDetail.CheckListCD = CrewDefinitionInfoList[Index].CheckListCD;
                            CrewCheckListDetail.PreviousCheckDT = CrewDefinitionInfoList[Index].PreviousCheckDT;


                            CrewCheckListDetail.Frequency = CrewDefinitionInfoList[Index].Frequency;
                            CrewCheckListDetail.DueDT = CrewDefinitionInfoList[Index].DueDT;
                            CrewCheckListDetail.AlertDT = CrewDefinitionInfoList[Index].AlertDT;
                            CrewCheckListDetail.GraceDT = CrewDefinitionInfoList[Index].GraceDT;
                            CrewCheckListDetail.GraceDays = CrewDefinitionInfoList[Index].GraceDays;
                            CrewCheckListDetail.AlertDays = CrewDefinitionInfoList[Index].AlertDays;
                            CrewCheckListDetail.FrequencyMonth = CrewDefinitionInfoList[Index].FrequencyMonth;
                            CrewCheckListDetail.AircraftID = CrewDefinitionInfoList[Index].AircraftID
                                ;
                            CrewCheckListDetail.BaseMonthDT = CrewDefinitionInfoList[Index].BaseMonthDT;
                            CrewCheckListDetail.OriginalDT = CrewDefinitionInfoList[Index].OriginalDT;
                            CrewCheckListDetail.IsPilotInCommandFAR91 = CrewDefinitionInfoList[Index].IsPilotInCommandFAR91;
                            CrewCheckListDetail.IsPilotInCommandFAR135 = CrewDefinitionInfoList[Index].IsPilotInCommandFAR135;
                            CrewCheckListDetail.IsSecondInCommandFAR91 = CrewDefinitionInfoList[Index].IsSecondInCommandFAR91;
                            CrewCheckListDetail.IsSecondInCommandFAR135 = CrewDefinitionInfoList[Index].IsSecondInCommandFAR135;
                            CrewCheckListDetail.IsInActive = CrewDefinitionInfoList[Index].IsInActive;
                            CrewCheckListDetail.IsMonthEnd = CrewDefinitionInfoList[Index].IsMonthEnd;
                            CrewCheckListDetail.IsStopCALC = CrewDefinitionInfoList[Index].IsStopCALC;
                            CrewCheckListDetail.IsOneTimeEvent = CrewDefinitionInfoList[Index].IsOneTimeEvent;
                            CrewCheckListDetail.IsCompleted = CrewDefinitionInfoList[Index].IsCompleted;
                            CrewCheckListDetail.IsNoConflictEvent = CrewDefinitionInfoList[Index].IsNoConflictEvent;
                            CrewCheckListDetail.IsNoCrewCheckListREPTt = CrewDefinitionInfoList[Index].IsNoCrewCheckListREPTt;
                            CrewCheckListDetail.IsNoChecklistREPT = CrewDefinitionInfoList[Index].IsNoChecklistREPT;
                            CrewCheckListDetail.IsPassedDueAlert = CrewDefinitionInfoList[Index].IsPassedDueAlert;
                            CrewCheckListDetail.IsPrintStatus = CrewDefinitionInfoList[Index].IsPrintStatus;
                            CrewCheckListDetail.IsNextMonth = CrewDefinitionInfoList[Index].IsNextMonth;
                            CrewCheckListDetail.IsEndCalendarYear = CrewDefinitionInfoList[Index].IsEndCalendarYear;
                            CrewCheckListDetail.LastUpdTS = CrewDefinitionInfoList[Index].LastUpdTS;
                            CrewCheckListDetail.LastUpdUID = CrewDefinitionInfoList[Index].LastUpdUID;
                        }
                        if (CrewCheckListDetail.CheckListID == 0)
                        {
                            Indentity = Indentity - 1;
                            CrewCheckListDetail.CheckListID = Indentity; //crewinfoid
                        }
                        oCrew.CrewCheckListDetail.Add(CrewCheckListDetail);
                    }
                    return oCrew;

                }, FlightPak.Common.Constants.Policy.UILayer);
                return oCrew;
            }
        }

        /// <summary>
        /// Method to Save Aircraft Assigned Informations
        /// </summary>
        /// <param name="CrewCode"></param>
        private FlightPakMasterService.Crew SaveAircraftAssigned(FlightPakMasterService.Crew oCrew)
        {

            // Soft Delete the previous Crew Definition Items 
            // belongs to the specified Crew Code.
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCrew))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (hdnSave.Value == "Update")
                    {
                        using (MasterCatalogServiceClient AircraftAssignedService = new MasterCatalogServiceClient())
                        {
                            List<CrewAircraftAssigned> AircraftAssignedList = new List<CrewAircraftAssigned>();
                            CrewAircraftAssigned CrewAircraftAssigned = new CrewAircraftAssigned();
                            CrewAircraftAssigned.CrewID = oCrew.CrewID;
                            CrewAircraftAssigned.CustomerID = oCrew.CustomerID;
                            var CrewRosterServiceValues = AircraftAssignedService.GetAircraftAssignedList(CrewAircraftAssigned).EntityList.ToList();
                            if (CrewRosterServiceValues.Count > 0)
                            {
                                CrewAircraftAssigned.CrewID = oCrew.CrewID;
                                CrewAircraftAssigned.CustomerID = oCrew.CustomerID;
                                CrewAircraftAssigned.IsDeleted = true;
                                using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                                {
                                    CrewRosterService.DeleteCrewAircraftAssigned(CrewAircraftAssigned);
                                }
                            }
                        }

                    }

                    // Save the added information items from the Grid

                    Int64 Indentity = 0;
                    oCrew.CrewAircraftAssigned = new List<CrewAircraftAssigned>();
                    CrewAircraftAssigned CrewAircraftAssigned1;
                    foreach (GridDataItem Item in dgAircraftAssigned.MasterTableView.Items)
                    {
                        CheckBox chkAircraftAssigned = (CheckBox)Item["AircraftAssigned"].FindControl("chkAircraftAssigned");
                        if (chkAircraftAssigned.Checked == true)
                        {
                            CrewAircraftAssigned1 = new CrewAircraftAssigned();
                            if (Item.GetDataKeyValue("CrewAircraftAssignedID") != null)
                            {
                                CrewAircraftAssigned1.CrewAircraftAssignedID = Convert.ToInt64(Item.GetDataKeyValue("CrewAircraftAssignedID").ToString());
                            }
                            else
                            {
                                Indentity = Indentity - 1;
                                CrewAircraftAssigned1.CrewAircraftAssignedID = Indentity;
                            }
                            CrewAircraftAssigned1.CustomerID = oCrew.CustomerID;
                            CrewAircraftAssigned1.CrewID = oCrew.CrewID;  // hidden value ll come here
                            CrewAircraftAssigned1.FleetID = Convert.ToInt64(Convert.ToString(Item.GetDataKeyValue("FleetID")));  // Item.GetDataKeyValue("FleetID").ToString();
                            CrewAircraftAssigned1.IsDeleted = false;
                            oCrew.CrewAircraftAssigned.Add(CrewAircraftAssigned1);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                return oCrew;
            }
        }

        #endregion

        #region "Insert,Edit,Read,Load,Enable & Clear"

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                    // BindDefaultGrids();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Update";
                    //  LoadControlData();
                    EnableForm(true);
                    btnApplyToAdditionalCrew.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    DefaultChecklistSelection();
                    EnableForm(false);
                    btnApplyToAdditionalCrew.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To Clear the Form Fields
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Main Info. Fields
                    chkActiveCrew.Checked = true;
                    chkFixedWing.Checked = true;
                    chkRoteryWing.Checked = false;
                    tbCrewCode.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    tbClientCode.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbAddress1.Text = string.Empty;
                    tbAddress2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbStateProvince.Text = string.Empty;
                    tbStateProvincePostal.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    rbMale.Checked = true;
                    rbFemale.Checked = false;
                    // TextBox tbDateofBirth = (TextBox)ucDateOfBirth.FindControl("tbDate");
                    tbDateOfBirth.Text = string.Empty;
                    chkNoCalenderDisplay.Checked = false;
                    tbSSN.Text = string.Empty;
                    tbCityBirth.Text = string.Empty;
                    tbStateBirth.Text = string.Empty;
                    tbCountryBirth.Text = string.Empty;
                    tbHomePhone.Text = string.Empty;
                    tbMobilePhone.Text = string.Empty;
                    tbPager.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    // TextBox tbHireDT = (TextBox)ucHireDT.FindControl("tbDate");
                    tbHireDT.Text = string.Empty;
                    //  TextBox tbTermDT = (TextBox)ucTermDT.FindControl("tbDate");
                    tbTermDT.Text = string.Empty;
                    tbCitizenCode.Text = string.Empty;
                    lbCitizenDesc.Text = string.Empty;
                    tbLicNo.Text = string.Empty;
                    tbLicExpDate.Text = string.Empty;
                    tbLicCity.Text = string.Empty;
                    tbLicCountry.Text = string.Empty;
                    tbLicType.Text = string.Empty;
                    tbAddLic.Text = string.Empty;
                    tbAddLicExpDate.Text = string.Empty;
                    tbAddLicCity.Text = string.Empty;
                    tbAddLicCountry.Text = string.Empty;
                    tbAddLicType.Text = string.Empty;
                    tbCrewType.Text = string.Empty;
                    tbDepartment.Text = string.Empty;
                    tbNotes.Text = string.Empty;

                    // <new fields>
                    tbAddress3.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbSecondaryMobile.Text = string.Empty;
                    tbBusinessFax.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    // <\new fields>

                    # region Clear Section

                    hdnNationalityID.Value = "";
                    hdnClientID.Value = "";
                    hdnHomeBaseID.Value = "";
                    hdnDepartmentID.Value = "";
                    hdnResidenceCountryID.Value = "";
                    hdnLicCountryID.Value = "";

                    hdnAddLicCountryID.Value = "";
                    hdnCitizenshipID.Value = "";
                    hdnCountryID.Value = "";
                    hdnCrewID.Value = "";  // check
                    hdnCrewGroupID.Value = "";

                    #endregion
                    chkActiveCrew.Checked = false;
                    chkFixedWing.Checked = false;
                    chkRoteryWing.Checked = false;

                    #region "Add Infor Grid"
                    // Addl Info Fields
                    Session.Remove("CrewAddInfo");
                    //dgCrewAddlInfo.MasterTableView.DataSource = "";
                    //dgCrewAddlInfo.MasterTableView.DataMember = "";
                    //dgCrewAddlInfo.DataSource = "";
                    //dgCrewAddlInfo.DataBind();
                    BindAdditionalInfoGrid(0);
                    #endregion

                    #region "Type Rating Grid"
                    // Type Ratings Fields
                    ClearTypeRatingFields();
                    Session.Remove("CrewTypeRating");
                    //dgTypeRating.MasterTableView.DataSource = "";
                    //dgTypeRating.MasterTableView.DataMember = "";
                    //dgTypeRating.DataSource = "";
                    //dgTypeRating.DataBind();
                    BindTypeRatingGrid(0);
                    #endregion

                    #region "Crew Checklist grid"
                    // Checklist Fields
                    Session.Remove("CrewCheckList");
                    ClearCheckListFields();
                    //dgCrewCheckList.MasterTableView.DataSource = "";
                    //dgCrewCheckList.MasterTableView.DataMember = "";
                    //dgCrewCheckList.DataSource = "";
                    //dgCrewCheckList.DataBind();
                    BindCrewCheckListGrid(0);
                    #endregion

                    #region "Aircraft Assign Grid"
                    // Aircraft Assigned Fields 
                    Session.Remove("CrewAircraftAssigned");
                    foreach (GridDataItem Item in dgAircraftAssigned.MasterTableView.Items)
                    {
                        ((CheckBox)Item["AircraftAssigned"].FindControl("chkAircraftAssigned")).Checked = false;
                    }
                    BindAircraftAssignedGrid(0);
                    #endregion

                    #region "Passport Grid"
                    // Passports/Visas Fields
                    Session.Remove("CrewPassport");
                    //dgPassport.MasterTableView.DataSource = "";
                    //dgPassport.MasterTableView.DataMember = "";
                    //dgPassport.DataSource = "";
                    //dgPassport.DataBind();
                    BindCrewPassportGrid(0);
                    #endregion

                    #region "Crew Visa Grid"
                    Session.Remove("CrewVisa");
                    //dgVisa.MasterTableView.DataSource = "";
                    //dgVisa.MasterTableView.DataMember = "";
                    //dgVisa.DataSource = "";
                    //dgVisa.DataBind();
                    BindCrewVisaGrid(0);

                    #endregion

                    #region Image clear
                    CreateDictionayForImgUpload();
                    fileUL.Enabled = false;
                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbNotes.Text = "";
                    tbImgName.Text = "";
                    #endregion

                    // Currency Fields
                    lbToday.Text = DateTime.Now.ToShortDateString();
                    tabSwitchViews.SelectedIndex = 1;
                    BindCrewCurrency(0);
                    // Addl Notes Field
                    tbAdditionalNotes.Text = string.Empty;

                    //Email Preferences Fields
                    chkSelectAll.Checked = false;
                    EnableEmailPreferences(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Enable or Disable form
        /// </summary>
        /// <param name="Enable">Pass Boolean Value</param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Main Info. Fields
                    //  tbCrewCode.Enabled = Enable;
                    tbHomeBase.Enabled = Enable;
                    // Set Logged-in User Name
                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                    if (identity != null && identity.Identity._clientId != null && identity.Identity._clientId != 0)
                    {
                        tbClientCode.Enabled = false;
                        //tbClientCodeFilter.Enabled = false;
                        //need to change
                        tbClientCode.Text = identity.Identity._clientCd.ToString().Trim();
                        // tbClientCodeFilter.Text = identity.Identity._clientCd.ToString().Trim();
                        hdnClientID.Value = identity.Identity._clientId.ToString().Trim();
                        btnClientCode.Enabled = false;
                        // btnClientCodeFilter.Enabled = false;

                    }
                    else
                    {
                        tbClientCode.Enabled = Enable;
                        btnClientCode.Enabled = Enable;
                        //  tbClientCodeFilter.Enabled = Enable;
                        //   btnClientCodeFilter.Enabled = Enable;
                    }
                    tbLastName.Enabled = Enable;
                    tbFirstName.Enabled = Enable;
                    tbMiddleName.Enabled = Enable;
                    tbAddress1.Enabled = Enable;
                    tbAddress2.Enabled = Enable;
                    tbCity.Enabled = Enable;
                    tbStateProvince.Enabled = Enable;
                    tbStateProvincePostal.Enabled = Enable;
                    tbCountry.Enabled = Enable;
                    rbMale.Enabled = Enable;
                    rbFemale.Enabled = Enable;

                    // TextBox tbDateofBirth = (TextBox)ucDateOfBirth.FindControl("tbDate");
                    // tbDateofBirth.Enabled = Enable;

                    tbHireDT.Enabled = Enable;
                    tbDateOfBirth.Enabled = Enable;
                    tbTermDT.Enabled = Enable;

                    chkNoCalenderDisplay.Enabled = Enable;
                    tbSSN.Enabled = Enable;
                    tbCityBirth.Enabled = Enable;
                    tbStateBirth.Enabled = Enable;
                    tbCountryBirth.Enabled = Enable;
                    tbHomePhone.Enabled = Enable;
                    tbMobilePhone.Enabled = Enable;
                    tbPager.Enabled = Enable;
                    tbFax.Enabled = Enable;
                    tbEmail.Enabled = Enable;
                    //TextBox tbHireDT = (TextBox)ucHireDT.FindControl("tbDate");
                    //tbHireDT.Enabled = Enable;
                    //TextBox tbTermDT = (TextBox)ucTermDT.FindControl("tbDate");
                    //tbTermDT.Enabled = Enable;
                    tbCitizenCode.Enabled = Enable;
                    lbCitizenDesc.Enabled = Enable;
                    tbLicNo.Enabled = Enable;
                    tbLicExpDate.Enabled = Enable;
                    tbLicCity.Enabled = Enable;
                    tbLicCountry.Enabled = Enable;
                    tbLicType.Enabled = Enable;
                    tbAddLic.Enabled = Enable;
                    tbAddLicExpDate.Enabled = Enable;
                    tbAddLicCity.Enabled = Enable;
                    tbAddLicCountry.Enabled = Enable;
                    tbAddLicType.Enabled = Enable;
                    tbCrewType.Enabled = Enable;
                    tbDepartment.Enabled = Enable;
                    tbNotes.Enabled = Enable;
                    // tbPhoto.Enabled = Enable;
                    chkActiveCrew.Enabled = Enable;
                    chkFixedWing.Enabled = Enable;
                    chkRoteryWing.Enabled = Enable;


                    //<new field>
                    tbAddress3.Enabled = Enable;
                    tbOtherEmail.Enabled = Enable;
                    tbPersonalEmail.Enabled = Enable;
                    tbOtherPhone.Enabled = Enable;
                    tbSecondaryMobile.Enabled = Enable;
                    tbBusinessFax.Enabled = Enable;
                    tbBusinessPhone.Enabled = Enable;
                    //<\new field>


                    ddlImg.Enabled = Enable;
                    tbImgName.Enabled = Enable;
                    btndeleteImage.Enabled = Enable;
                    fileUL.Enabled = Enable;

                    // Addl Info Fields
                    dgCrewAddlInfo.Enabled = Enable;

                    // Type Ratings Fields
                    lbTypeCode.Enabled = Enable;
                    lbDescription.Enabled = Enable;
                    chkTRInactive.Enabled = Enable;
                    chkTRPIC91.Enabled = Enable;
                    chkTRSIC91.Enabled = Enable;
                    chkTREngineer.Enabled = Enable;
                    chkTRAttendant91.Enabled = Enable;
                    chkTRAirman.Enabled = Enable;
                    chkTRPIC135.Enabled = Enable;
                    chkTRSIC135.Enabled = Enable;
                    chkTRInstructor.Enabled = Enable;
                    chkTRAttendant135.Enabled = Enable;
                    chkTRAttendant.Enabled = Enable;

                    chkPIC121.Enabled = Enable;
                    chkSIC121.Enabled = Enable;
                    chkPIC125.Enabled = Enable;
                    chkSIC125.Enabled = Enable;
                    chkAttendent121.Enabled = Enable;
                    chkAttendent125.Enabled = Enable;

                    EnableHours(Enable);
                    dgTypeRating.Enabled = Enable;

                    // Checklist Fields
                    lbCheckListCode.Enabled = Enable;
                    lbCheckListDescription.Enabled = Enable;

                    EnableCheckListFields(Enable);

                    btnAircraftType.Enabled = Enable;

                    dgCrewCheckList.Enabled = Enable;

                    // Aircraft Assigned Fields
                    //dgAircraftAssigned.Enabled = Enable;

                    // Passports/Visas Fields
                    dgPassport.Enabled = Enable;
                    dgVisa.Enabled = Enable;

                    // Currency Fields
                    lbToday.Enabled = Enable;
                    tabSwitchViews.Enabled = Enable;

                    // Addl Notes Field
                    tbAdditionalNotes.Enabled = Enable;

                    //Email Preferences Fields
                    chkSelectAll.Enabled = Enable;
                    chkDepartmentAuthorization.Enabled = Enable;
                    chkRequestorPhone.Enabled = Enable;
                    chkAccountNo.Enabled = Enable;
                    chkCancellationDesc.Enabled = Enable;
                    chkStatus.Enabled = Enable;
                    chkAirport.Enabled = Enable;
                    chkChecklist.Enabled = Enable;
                    chkRunway.Enabled = Enable;
                    chkHomeArrival.Enabled = Enable;
                    chkHomeDeparture.Enabled = Enable;
                    radZULU.Enabled = Enable;
                    radLocal.Enabled = Enable;
                    chkEndDuty.Enabled = Enable;
                    chkOverride.Enabled = Enable;
                    chkCrewRules.Enabled = Enable;
                    chkFAR.Enabled = Enable;
                    chkDutyHours.Enabled = Enable;
                    chkFlightHours.Enabled = Enable;
                    chkRestHours.Enabled = Enable;
                    chkAssociatedCrew.Enabled = Enable;
                    chkNEWSPAPER.Enabled = Enable;
                    chkCOFFEE.Enabled = Enable;
                    chkJUICE.Enabled = Enable;
                    chkDepartureInformation.Enabled = Enable;
                    chkArrivalInformation.Enabled = Enable;
                    chkCrewTransport.Enabled = Enable;
                    chkCrewArrival.Enabled = Enable;
                    chkHotel.Enabled = Enable;
                    chkPassengerTransport.Enabled = Enable;
                    chkPassengerArrival.Enabled = Enable;
                    chkPassengerDetails.Enabled = Enable;
                    chkPassengerHotel.Enabled = Enable;
                    chkPassengerPaxPhone.Enabled = Enable;
                    chkDepartureCatering.Enabled = Enable;
                    chkArrivalCatering.Enabled = Enable;
                    chkArrivalDepartureTime.Enabled = Enable;
                    chkAircraft.Enabled = Enable;
                    chkArrivalDepartureICAO.Enabled = Enable;
                    chkGeneralCrew.Enabled = Enable;
                    chkGeneralPassenger.Enabled = Enable;
                    chkOutBoundInstructions.Enabled = Enable;
                    chkGeneralChecklist.Enabled = Enable;
                    chkLogisticsFBO.Enabled = Enable;
                    chkLogisticsHotel.Enabled = Enable;
                    chkLogisticsTransportation.Enabled = Enable;
                    chkLogisticsCatering.Enabled = Enable;

                    chkCrewHotelConfirm.Enabled = Enable;
                    chkCrewHotelComments.Enabled = Enable;
                    chkCrewDeptTransConfirm.Enabled = Enable;
                    chkCrewDeptTransComments.Enabled = Enable;
                    chkCrewArrTransConfirm.Enabled = Enable;
                    chkCrewArrTransComments.Enabled = Enable;
                    chkCrewNotes.Enabled = Enable;
                    chkPAXHotelConfirm.Enabled = Enable;
                    chkPAXHotelComments.Enabled = Enable;
                    chkPAXDeptTransConfirm.Enabled = Enable;
                    chkPAXDeptTransComments.Enabled = Enable;
                    chkPAXArrTransConfirm.Enabled = Enable;
                    chkPAXArrTransComments.Enabled = Enable;
                    chkPAXNotes.Enabled = Enable;
                    chkOutboundInstComments.Enabled = Enable;
                    chkDeptAirportNotes.Enabled = Enable;
                    chkArrAirportNotes.Enabled = Enable;
                    chkDeptAirportAlerts.Enabled = Enable;
                    chkArrAirportAlerts.Enabled = Enable;
                    chkArrFBOConfirm.Enabled = Enable;
                    chkArrFBOComments.Enabled = Enable;
                    chkDeptFBOConfirm.Enabled = Enable;
                    chkDeptFBOComments.Enabled = Enable;
                    chkDeptCaterConfirm.Enabled = Enable;
                    chkDeptCaterComments.Enabled = Enable;
                    chkArrCaterConfirm.Enabled = Enable;
                    chkArrCaterComments.Enabled = Enable;
                    chkTripAlerts.Enabled = Enable;
                    chkTripNotes.Enabled = Enable;

                    // Button Controls Enable / Disable

                    btnHomeBase.Enabled = Enable;
                    btnCountryPopup.Enabled = Enable;
                    btnCitizenship.Enabled = Enable;
                    btnLicCountry.Enabled = Enable;
                    btnAddLicCountry.Enabled = Enable;
                    btnCountryOfBirth.Enabled = Enable;
                    //btnBrowse.Enabled = Enable;
                    // btnLogoDelete.Enabled = Enable;
                    btnAddlInfo.Enabled = Enable;
                    btnDeleteInfo.Enabled = Enable;
                    btnDepartment.Enabled = Enable;

                    btnFlightExp.Enabled = Enable;
                    btnUpdateFltExperience.Enabled = Enable;
                    btnUpdateAllFltExperience.Enabled = Enable;
                    btnAddRating.Enabled = Enable;
                    btnDeleteRating.Enabled = Enable;

                    // btnApplyToAdditionalCrew.Enabled = Enable;
                    btnAddChecklist.Enabled = Enable;
                    btnDeleteChecklist.Enabled = Enable;
                    btnChgBaseDate.Enabled = Enable;
                    btnAddVisa.Enabled = Enable;
                    btnDeleteVisa.Enabled = Enable;
                    btnAddPassport.Enabled = Enable;
                    btnDeletePassport.Enabled = Enable;

                    btnSaveChanges.Visible = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChangesTop.Visible = Enable;
                    btnCancelTop.Visible = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }

        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");

                        if (IsAuthorized(Permission.Database.AddCrewRoster))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                            {
                                lbtnInsertCtl.Enabled = true;
                            }
                            else
                            {
                                lbtnInsertCtl.Enabled = false;
                            }
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.DeleteCrewRoster))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                            {
                                lbtnDelCtl.Enabled = true;
                                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                            }
                            else
                            {
                                lbtnDelCtl.Enabled = false;
                                lbtnDelCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Database.EditCrewRoster))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                            {
                                lbtnEditCtl.Enabled = true;
                                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnEditCtl.Enabled = false;
                                lbtnEditCtl.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// To assign the data to controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCrewRosterID"] != null)
                    {
                        // Clear the Existing Session Values
                        Session.Remove("CrewAddInfo");
                        Session.Remove("CrewTypeRating");
                        Session.Remove("CrewCheckList");
                        Session.Remove("CrewPassport");
                        Session.Remove("CrewVisa");
                        Session.Remove("CrewAircraftAssigned");


                        string CrewID = Session["SelectedCrewRosterID"].ToString();
                        string LastUpdUID = string.Empty, LastUpdTS = string.Empty, CustomerID = string.Empty;


                        foreach (GridDataItem Item in dgCrewRoster.MasterTableView.Items)
                        {
                            if (Item["CrewID"].Text.Trim() == CrewID.Trim())
                            {
                                LastUpdUID = Item["LastUpdUID"].Text.Trim().Replace("&nbsp;", "");
                                LastUpdTS = Item["LastUpdTS"].Text.Trim().Replace("&nbsp;", "");
                                CustomerID = Item["CustomerID"].Text.Trim().Replace("&nbsp;", "");
                                Item.Selected = true;
                                break;
                            }
                        }

                        Label lblUser = (Label)dgCrewRoster.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");

                        if (!string.IsNullOrEmpty(LastUpdUID))
                        {
                            lblUser.Text = "Last Updated User: " + LastUpdUID;
                        }
                        else
                        {
                            lblUser.Text = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(LastUpdTS))
                        {
                            lblUser.Text = System.Web.HttpUtility.HtmlEncode(lblUser.Text + " Date: " + GetDate(Convert.ToDateTime(LastUpdTS)));
                        }

                        List<FlightPakMasterService.GetCrewByCrewID> objCrew = new List<FlightPakMasterService.GetCrewByCrewID>();
                        using (FlightPakMasterService.MasterCatalogServiceClient objCrewService = new MasterCatalogServiceClient())
                        {

                            // List<GetAllCrew> lstCrew = (List<GetAllCrew>)Session["CrewList"];

                            //objCrew = objCrewService.GetCrewbyCrewId(Convert.ToInt64(Session["PassengerRequestorID"].ToString())).EntityList;

                            var Results = objCrewService.GetCrewbyCrewId(Convert.ToInt64(Session["SelectedCrewRosterID"].ToString())).EntityList;
                            //var Results = (from Code in lstCrew
                            //               where (Code.CrewID == Convert.ToInt64(CrewID)) && (Code.CustomerID == Convert.ToInt64(CustomerID))
                            //               select Code);

                            if (Results.Count() > 0 && Results != null)
                            {
                                foreach (GetCrewByCrewID Crew in Results)
                                {
                                    // Main Info Fields
                                    chkActiveCrew.Checked = Convert.ToBoolean(Crew.IsStatus, CultureInfo.CurrentCulture);
                                    chkFixedWing.Checked = Convert.ToBoolean(Crew.IsFixedWing, CultureInfo.CurrentCulture);
                                    chkRoteryWing.Checked = Convert.ToBoolean(Crew.IsRotaryWing, CultureInfo.CurrentCulture);
                                    tbCrewCode.Text = Crew.CrewCD;
                                    if (!string.IsNullOrEmpty(Crew.HomeBaseCD))
                                    {
                                        tbHomeBase.Text = Crew.HomeBaseCD;
                                    }
                                    else
                                    {
                                        tbHomeBase.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.ClientCD))
                                    {
                                        tbClientCode.Text = Crew.ClientCD;
                                    }
                                    else
                                    {
                                        tbClientCode.Text = string.Empty;
                                    }

                                    if (Crew.LastName != null)
                                    {
                                        tbLastName.Text = Crew.LastName;
                                    }
                                    else
                                    {
                                        tbLastName.Text = string.Empty;
                                    }

                                    if (Crew.FirstName != null)
                                    {
                                        tbFirstName.Text = Crew.FirstName;
                                    }
                                    else
                                    {
                                        tbFirstName.Text = string.Empty;
                                    }

                                    if (Crew.MiddleInitial != null)
                                    {
                                        tbMiddleName.Text = Crew.MiddleInitial;
                                    }
                                    else
                                    {
                                        tbMiddleName.Text = string.Empty;
                                    }

                                    if (Crew.Addr1 != null)
                                    {
                                        tbAddress1.Text = Crew.Addr1;
                                    }
                                    else
                                    {
                                        tbAddress1.Text = string.Empty;
                                    }

                                    if (Crew.Addr2 != null)
                                    {
                                        tbAddress2.Text = Crew.Addr2;
                                    }
                                    else
                                    {
                                        tbAddress2.Text = string.Empty;
                                    }

                                    if (Crew.CityName != null)
                                    {
                                        tbCity.Text = Crew.CityName;
                                    }
                                    else
                                    {
                                        tbCity.Text = string.Empty;
                                    }

                                    if (Crew.StateName != null)
                                    {
                                        tbStateProvince.Text = Crew.StateName;
                                    }
                                    else
                                    {
                                        tbStateProvince.Text = string.Empty;
                                    }

                                    if (Crew.PostalZipCD != null)
                                    {
                                        tbStateProvincePostal.Text = Crew.PostalZipCD;
                                    }
                                    else
                                    {
                                        tbStateProvincePostal.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.ResidenceCountryCD))  //if (!string.IsNullOrEmpty(Crew.NationalityCD))
                                    {
                                        tbCountry.Text = Crew.ResidenceCountryCD;
                                    }
                                    else
                                    {
                                        tbCountry.Text = string.Empty;
                                    }
                                    if (Crew.Gender != null)
                                    {
                                        if (Crew.Gender.ToString() == "M")
                                        {
                                            rbMale.Checked = true;
                                            rbFemale.Checked = false;
                                        }
                                        else
                                        {
                                            rbMale.Checked = false;
                                            rbFemale.Checked = true;
                                        }
                                    }

                                    //  TextBox tbDateofBirth = (TextBox)ucDateOfBirth.FindControl("tbDate");

                                    //if (!string.IsNullOrEmpty(Crew.BirthDT.ToString()))
                                    if (Crew.BirthDT != null)
                                    {
                                        if ((Crew.BirthDT.ToString() != string.Empty) && (!string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbDateOfBirth.Text = String.Format("{0:" + DateFormat + "}", Crew.BirthDT);
                                        }
                                        else if ((Crew.BirthDT.ToString() != string.Empty) && (string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbDateOfBirth.Text = Convert.ToDateTime(Crew.BirthDT).ToString();
                                        }
                                        else
                                        {
                                            tbDateOfBirth.Text = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        tbDateOfBirth.Text = string.Empty;
                                    }

                                    chkNoCalenderDisplay.Checked = Convert.ToBoolean(Crew.IsNoCalendarDisplay, CultureInfo.CurrentCulture);

                                    if (Crew.SSN != null)
                                    {
                                        tbSSN.Text = Crew.SSN;
                                    }
                                    else
                                    {
                                        tbSSN.Text = string.Empty;
                                    }

                                    if (Crew.CityOfBirth != null)
                                    {
                                        tbCityBirth.Text = Crew.CityOfBirth;
                                    }
                                    else
                                    {
                                        tbCityBirth.Text = string.Empty;
                                    }

                                    if (Crew.StateofBirth != null)
                                    {
                                        tbStateBirth.Text = Crew.StateofBirth;
                                    }
                                    else
                                    {
                                        tbStateBirth.Text = string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(Crew.NationalityCD))
                                    {
                                        tbCountryBirth.Text = Crew.NationalityCD;
                                    }
                                    else
                                    {
                                        tbCountryBirth.Text = string.Empty;
                                    }

                                    if (Crew.PhoneNum != null)
                                    {
                                        tbHomePhone.Text = Crew.PhoneNum;
                                    }
                                    else
                                    {
                                        tbHomePhone.Text = string.Empty;
                                    }

                                    if (Crew.CellPhoneNum != null)
                                    {
                                        tbMobilePhone.Text = Crew.CellPhoneNum;
                                    }
                                    else
                                    {
                                        tbMobilePhone.Text = string.Empty;
                                    }

                                    if (Crew.PagerNum != null)
                                    {
                                        tbPager.Text = Crew.PagerNum;
                                    }
                                    else
                                    {
                                        tbPager.Text = string.Empty;
                                    }
                                    tbFax.Text = Crew.FaxNum;
                                    tbEmail.Text = Crew.EmailAddress;
                                    //  TextBox tbHireDT = (TextBox)ucHireDT.FindControl("tbDate");

                                    if (Crew.HireDT != null)
                                    {
                                        if ((Crew.HireDT.ToString() != string.Empty) && (!string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbHireDT.Text = String.Format("{0:" + DateFormat + "}", Crew.HireDT);
                                        }
                                        else if ((Crew.HireDT.ToString() != string.Empty) && (string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbHireDT.Text = Convert.ToDateTime(Crew.HireDT).ToString();
                                        }
                                        else
                                        {
                                            tbHireDT.Text = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        tbHireDT.Text = string.Empty;
                                    }


                                    // TextBox tbTermDT = (TextBox)ucTermDT.FindControl("tbDate");
                                    if (Crew.TerminationDT != null)
                                    {
                                        if ((Crew.TerminationDT.ToString() != string.Empty) && (!string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbTermDT.Text = String.Format("{0:" + DateFormat + "}", Crew.TerminationDT);
                                        }
                                        else if ((Crew.TerminationDT.ToString() != string.Empty) && (string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbTermDT.Text = Convert.ToDateTime(Crew.TerminationDT).ToString();
                                        }
                                        else
                                        {
                                            tbTermDT.Text = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        tbTermDT.Text = string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(Crew.CitizenshipCD))
                                    {
                                        tbCitizenCode.Text = Crew.CitizenshipCD;
                                    }
                                    else
                                    {
                                        tbCitizenCode.Text = string.Empty;
                                    }
                                    if (Crew.PilotLicense1 != null)
                                    {
                                        tbLicNo.Text = Crew.PilotLicense1;
                                    }
                                    else
                                    {
                                        tbLicNo.Text = string.Empty;
                                    }

                                    if (Crew.License1ExpiryDT != null)
                                    {
                                        if ((Crew.License1ExpiryDT.ToString() != string.Empty) && (!string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbLicExpDate.Text = String.Format("{0:" + DateFormat + "}", Crew.License1ExpiryDT);
                                        }
                                        else if ((Crew.License1ExpiryDT.ToString() != string.Empty) && (string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbLicExpDate.Text = Convert.ToDateTime(Crew.License1ExpiryDT).ToString();
                                        }
                                        else
                                        {
                                            tbLicExpDate.Text = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        tbLicExpDate.Text = string.Empty;
                                    }



                                    if (Crew.License1CityCountry != null)
                                    {
                                        tbLicCity.Text = Crew.License1CityCountry;
                                    }
                                    else
                                    {
                                        tbLicCity.Text = string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(Crew.LicenseCountry1))
                                    {
                                        tbLicCountry.Text = Crew.LicenseCountry1;
                                    }
                                    else
                                    {
                                        tbLicCountry.Text = string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(Crew.LicenseCountry2))
                                    {
                                        tbAddLicCountry.Text = Crew.LicenseCountry2;
                                    }
                                    else
                                    {
                                        tbAddLicCountry.Text = string.Empty;
                                    }

                                    if (Crew.LicenseType1 != null)
                                    {
                                        tbLicType.Text = Crew.LicenseType1;
                                    }
                                    else
                                    {
                                        tbLicType.Text = string.Empty;
                                    }

                                    if (Crew.PilotLicense2 != null)
                                    {
                                        tbAddLic.Text = Crew.PilotLicense2;
                                    }
                                    else
                                    {
                                        tbAddLic.Text = string.Empty;
                                    }

                                    if (Crew.License2ExpiryDT != null)
                                    {
                                        if ((Crew.License2ExpiryDT.ToString() != string.Empty) && (!string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbAddLicExpDate.Text = String.Format("{0:" + DateFormat + "}", Crew.License2ExpiryDT);
                                        }
                                        else if ((Crew.License2ExpiryDT.ToString() != string.Empty) && (string.IsNullOrEmpty(DateFormat)))
                                        {
                                            tbAddLicExpDate.Text = Convert.ToDateTime(Crew.License2ExpiryDT).ToString();
                                        }
                                        else
                                        {
                                            tbAddLicExpDate.Text = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        tbAddLicExpDate.Text = string.Empty;
                                    }



                                    if (Crew.License2CityCountry != null)
                                    {
                                        tbAddLicCity.Text = Crew.License2CityCountry;
                                    }
                                    else
                                    {
                                        tbAddLicCity.Text = string.Empty;
                                    }


                                    //<new fieldS>
                                    if (!string.IsNullOrEmpty(Crew.Addr3))
                                    {
                                        tbAddress3.Text = Crew.Addr3;
                                    }
                                    else
                                    {
                                        tbAddress3.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.OtherEmail))
                                    {
                                        tbOtherEmail.Text = Crew.OtherEmail;
                                    }
                                    else
                                    {
                                        tbOtherEmail.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.PersonalEmail))
                                    {
                                        tbPersonalEmail.Text = Crew.PersonalEmail;
                                    }
                                    else
                                    {
                                        tbPersonalEmail.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.OtherPhone))
                                    {
                                        tbOtherPhone.Text = Crew.OtherPhone;
                                    }
                                    else
                                    {
                                        tbOtherPhone.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.CellPhoneNum2))
                                    {
                                        tbSecondaryMobile.Text = Crew.CellPhoneNum2;
                                    }
                                    else
                                    {
                                        tbSecondaryMobile.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.BusinessFax))
                                    {
                                        tbBusinessFax.Text = Crew.BusinessFax;
                                    }
                                    else
                                    {
                                        tbBusinessFax.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(Crew.BusinessPhone))
                                    {
                                        tbBusinessPhone.Text = Crew.BusinessPhone;
                                    }
                                    else
                                    {
                                        tbBusinessPhone.Text = string.Empty;
                                    }
                                    //<\new fields>

                                    if (Crew.LicenseType2 != null)
                                    {
                                        tbAddLicType.Text = Crew.LicenseType2;
                                    }
                                    else
                                    {
                                        tbAddLicType.Text = string.Empty;
                                    }

                                    if (Crew.CrewTypeCD != null)
                                    {
                                        tbCrewType.Text = Crew.CrewTypeCD;
                                    }
                                    else
                                    {
                                        tbCrewType.Text = string.Empty;
                                    }


                                    if (!string.IsNullOrEmpty(Crew.DepartmentCD))
                                    {
                                        tbDepartment.Text = Crew.DepartmentCD;
                                    }
                                    else
                                    {
                                        tbDepartment.Text = string.Empty;
                                    }

                                    if (Crew.Notes != null)
                                    {
                                        tbNotes.Text = Crew.Notes;
                                    }
                                    else
                                    {
                                        tbNotes.Text = string.Empty;
                                    }

                                    //tbPhoto.Text="Not in DB";

                                    // Addl Info Fields
                                    BindAdditionalInfoGrid(Crew.CrewID);

                                    // Type Ratings Fields
                                    ClearTypeRatingFields();
                                    BindTypeRatingGrid(Crew.CrewID);

                                    // Checklist Fields
                                    ClearCheckListFields();
                                    BindCrewCheckListGrid(Crew.CrewID);

                                    #region Get Image from Database
                                    CreateDictionayForImgUpload();
                                    imgFile.ImageUrl = "";
                                    ImgPopup.ImageUrl = null;
                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var ObjRetImg = ObjImgService.GetFileWarehouseList("Crew", Convert.ToInt64(Session["SelectedCrewRosterID"].ToString())).EntityList;
                                        ddlImg.Items.Clear();
                                        ddlImg.Text = "";
                                        int i = 0;
                                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                        {
                                            Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];

                                            ddlImg.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(fwh.UWAFileName), fwh.UWAFileName));
                                            dicImg.Add(fwh.UWAFileName, fwh.UWAFilePath);
                                            if (i == 0)
                                            {
                                                byte[] picture = fwh.UWAFilePath;
                                                imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                            }
                                            i = i + 1;
                                        }
                                        if (ddlImg.Items.Count > 0)
                                            ddlImg.SelectedIndex = 0;
                                    #endregion

                                        // Aircraft Assigned Fields
                                        BindAircraftAssignedGrid(Crew.CrewID);

                                        // Passports/Visas Fields
                                        BindCrewVisaGrid(Crew.CrewID);
                                        BindCrewPassportGrid(Crew.CrewID);

                                        BindCrewCurrency(Crew.CrewID);

                                        // Addl Notes Field
                                        tbAdditionalNotes.Text = Crew.Notes2;

                                        //Email Preferences Fields
                                        chkDepartmentAuthorization.Checked = Convert.ToBoolean(Crew.IsDepartmentAuthorization, CultureInfo.CurrentCulture);
                                        chkRequestorPhone.Checked = Convert.ToBoolean(Crew.IsRequestPhoneNum, CultureInfo.CurrentCulture);
                                        chkAccountNo.Checked = Convert.ToBoolean(Crew.IsAccountNum, CultureInfo.CurrentCulture);
                                        chkCancellationDesc.Checked = Convert.ToBoolean(Crew.IsCancelDescription, CultureInfo.CurrentCulture);
                                        chkStatus.Checked = Convert.ToBoolean(Crew.IsStatusT, CultureInfo.CurrentCulture);
                                        chkAirport.Checked = Convert.ToBoolean(Crew.IsAirport, CultureInfo.CurrentCulture);
                                        chkChecklist.Checked = Convert.ToBoolean(Crew.IsCheckList, CultureInfo.CurrentCulture);
                                        chkRunway.Checked = Convert.ToBoolean(Crew.IsRunway, CultureInfo.CurrentCulture);
                                        chkHomeArrival.Checked = Convert.ToBoolean(Crew.IsHomeArrivalTM, CultureInfo.CurrentCulture);
                                        chkHomeDeparture.Checked = Convert.ToBoolean(Crew.IsHomeDEPARTTM, CultureInfo.CurrentCulture);
                                        if (Crew.BlackBerryTM.ToString().Trim() == "Z")
                                        {
                                            radZULU.Checked = true;
                                            radLocal.Checked = false;
                                        }
                                        else
                                        {
                                            radLocal.Checked = true;
                                            radZULU.Checked = false;
                                        }
                                        chkEndDuty.Checked = Convert.ToBoolean(Crew.IsEndDuty, CultureInfo.CurrentCulture);
                                        chkOverride.Checked = Convert.ToBoolean(Crew.IsOverride, CultureInfo.CurrentCulture);
                                        chkCrewRules.Checked = Convert.ToBoolean(Crew.IsCrewRules, CultureInfo.CurrentCulture);
                                        chkFAR.Checked = Convert.ToBoolean(Crew.IsFAR, CultureInfo.CurrentCulture);
                                        chkDutyHours.Checked = Convert.ToBoolean(Crew.IsDuty, CultureInfo.CurrentCulture);
                                        chkFlightHours.Checked = Convert.ToBoolean(Crew.IsFlightHours, CultureInfo.CurrentCulture);
                                        chkRestHours.Checked = Convert.ToBoolean(Crew.IsRestHrs, CultureInfo.CurrentCulture);
                                        chkAssociatedCrew.Checked = Convert.ToBoolean(Crew.IsAssociatedCrew, CultureInfo.CurrentCulture);
                                        chkNEWSPAPER.Checked = Convert.ToBoolean(Crew.Label2, CultureInfo.CurrentCulture);
                                        chkCOFFEE.Checked = Convert.ToBoolean(Crew.Label1, CultureInfo.CurrentCulture);
                                        chkJUICE.Checked = Convert.ToBoolean(Crew.Label3, CultureInfo.CurrentCulture);
                                        chkDepartureInformation.Checked = Convert.ToBoolean(Crew.IsDepartureFBO, CultureInfo.CurrentCulture);
                                        chkArrivalInformation.Checked = Convert.ToBoolean(Crew.IsArrivalFBO, CultureInfo.CurrentCulture);
                                        chkCrewTransport.Checked = Convert.ToBoolean(Crew.IsCrewDepartTRANS, CultureInfo.CurrentCulture);
                                        chkCrewArrival.Checked = Convert.ToBoolean(Crew.IsCrewArrivalTRANS, CultureInfo.CurrentCulture);
                                        chkHotel.Checked = Convert.ToBoolean(Crew.IsHotel, CultureInfo.CurrentCulture);
                                        chkPassengerTransport.Checked = Convert.ToBoolean(Crew.IsPassDepartTRANS, CultureInfo.CurrentCulture);
                                        chkPassengerArrival.Checked = Convert.ToBoolean(Crew.IsPassArrivalHotel, CultureInfo.CurrentCulture);
                                        chkPassengerDetails.Checked = Convert.ToBoolean(Crew.IsPassDetails, CultureInfo.CurrentCulture);
                                        chkPassengerHotel.Checked = Convert.ToBoolean(Crew.IsPassHotel, CultureInfo.CurrentCulture);
                                        chkPassengerPaxPhone.Checked = Convert.ToBoolean(Crew.IsPassPhoneNum, CultureInfo.CurrentCulture);
                                        chkDepartureCatering.Checked = Convert.ToBoolean(Crew.IsDepartCatering, CultureInfo.CurrentCulture);
                                        chkArrivalCatering.Checked = Convert.ToBoolean(Crew.IsArrivalCatering, CultureInfo.CurrentCulture);
                                        chkArrivalDepartureTime.Checked = Convert.ToBoolean(Crew.IsArrivalDepartTime, CultureInfo.CurrentCulture);
                                        chkAircraft.Checked = Convert.ToBoolean(Crew.IsAircraft, CultureInfo.CurrentCulture);
                                        chkArrivalDepartureICAO.Checked = Convert.ToBoolean(Crew.IcaoID, CultureInfo.CurrentCulture);
                                        chkGeneralCrew.Checked = Convert.ToBoolean(Crew.IsCrew, CultureInfo.CurrentCulture);
                                        chkGeneralPassenger.Checked = Convert.ToBoolean(Crew.IsPassenger, CultureInfo.CurrentCulture);
                                        chkOutBoundInstructions.Checked = Convert.ToBoolean(Crew.IsOutboundINST, CultureInfo.CurrentCulture);
                                        chkGeneralChecklist.Checked = Convert.ToBoolean(Crew.IsCheckList1, CultureInfo.CurrentCulture);
                                        chkLogisticsFBO.Checked = Convert.ToBoolean(Crew.IsFBO, CultureInfo.CurrentCulture);
                                        chkLogisticsHotel.Checked = Convert.ToBoolean(Crew.IsHotel, CultureInfo.CurrentCulture);
                                        chkLogisticsTransportation.Checked = Convert.ToBoolean(Crew.IsTransportation, CultureInfo.CurrentCulture);
                                        chkLogisticsCatering.Checked = Convert.ToBoolean(Crew.IsCatering, CultureInfo.CurrentCulture);

                                        chkCrewHotelConfirm.Checked = Convert.ToBoolean(Crew.IsCrewHotelConfirm, CultureInfo.CurrentCulture);
                                        chkCrewHotelComments.Checked = Convert.ToBoolean(Crew.IsCrewHotelComments, CultureInfo.CurrentCulture);
                                        chkCrewDeptTransConfirm.Checked = Convert.ToBoolean(Crew.IsCrewDepartTRANSConfirm, CultureInfo.CurrentCulture);
                                        chkCrewDeptTransComments.Checked = Convert.ToBoolean(Crew.IsCrewDepartTRANSComments, CultureInfo.CurrentCulture);
                                        chkCrewArrTransConfirm.Checked = Convert.ToBoolean(Crew.IsCrewArrivalConfirm, CultureInfo.CurrentCulture);
                                        chkCrewArrTransComments.Checked = Convert.ToBoolean(Crew.IsCrewArrivalComments, CultureInfo.CurrentCulture);
                                        chkCrewNotes.Checked = Convert.ToBoolean(Crew.IsCrewNotes, CultureInfo.CurrentCulture);
                                        chkPAXHotelConfirm.Checked = Convert.ToBoolean(Crew.IsPassHotelConfirm, CultureInfo.CurrentCulture);
                                        chkPAXHotelComments.Checked = Convert.ToBoolean(Crew.IsPassHotelComments, CultureInfo.CurrentCulture);
                                        chkPAXDeptTransConfirm.Checked = Convert.ToBoolean(Crew.IsPassDepartTRANSConfirm, CultureInfo.CurrentCulture);
                                        chkPAXDeptTransComments.Checked = Convert.ToBoolean(Crew.IsPassDepartTRANSComments, CultureInfo.CurrentCulture);
                                        chkPAXArrTransConfirm.Checked = Convert.ToBoolean(Crew.IsPassArrivalConfirm, CultureInfo.CurrentCulture);
                                        chkPAXArrTransComments.Checked = Convert.ToBoolean(Crew.IsPassArrivalComments, CultureInfo.CurrentCulture);
                                        chkPAXNotes.Checked = Convert.ToBoolean(Crew.IsPassNotes, CultureInfo.CurrentCulture);
                                        chkOutboundInstComments.Checked = Convert.ToBoolean(Crew.IsOutboundComments, CultureInfo.CurrentCulture);
                                        chkDeptAirportNotes.Checked = Convert.ToBoolean(Crew.IsDepartAirportNotes, CultureInfo.CurrentCulture);
                                        chkArrAirportNotes.Checked = Convert.ToBoolean(Crew.IsArrivalAirportNoes, CultureInfo.CurrentCulture);
                                        chkDeptAirportAlerts.Checked = Convert.ToBoolean(Crew.IsDepartAirportAlerts, CultureInfo.CurrentCulture);
                                        chkArrAirportAlerts.Checked = Convert.ToBoolean(Crew.IsArrivalAirportAlerts, CultureInfo.CurrentCulture);
                                        chkArrFBOConfirm.Checked = Convert.ToBoolean(Crew.IsFBOArrivalConfirm, CultureInfo.CurrentCulture);
                                        chkArrFBOComments.Checked = Convert.ToBoolean(Crew.IsFBOArrivalComment, CultureInfo.CurrentCulture);
                                        chkDeptFBOConfirm.Checked = Convert.ToBoolean(Crew.IsFBODepartConfirm, CultureInfo.CurrentCulture);
                                        chkDeptFBOComments.Checked = Convert.ToBoolean(Crew.IsFBODepartComment, CultureInfo.CurrentCulture);
                                        chkDeptCaterConfirm.Checked = Convert.ToBoolean(Crew.IsDepartCateringConfirm, CultureInfo.CurrentCulture);
                                        chkDeptCaterComments.Checked = Convert.ToBoolean(Crew.IsDepartCateringComment, CultureInfo.CurrentCulture);
                                        chkArrCaterConfirm.Checked = Convert.ToBoolean(Crew.IsArrivalCateringConfirm, CultureInfo.CurrentCulture);
                                        chkArrCaterComments.Checked = Convert.ToBoolean(Crew.IsArrivalCateringComment, CultureInfo.CurrentCulture);
                                        chkTripAlerts.Checked = Convert.ToBoolean(Crew.IsTripAlerts, CultureInfo.CurrentCulture);
                                        chkTripNotes.Checked = Convert.ToBoolean(Crew.IsTripNotes, CultureInfo.CurrentCulture);
                                    }
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    BindAdditionalInfoGrid(0);
                    //    BindTypeRatingGrid(0);
                    //    BindCrewCheckListGrid(0);
                    //    BindAircraftAssignedGrid(0);
                    //    BindCrewVisaGrid(0);
                    //    BindCrewPassportGrid(0);
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Check / Uncheck based on the selected value
        /// </summary>
        /// <param name="Enable">Pass Boolean value</param>
        private void EnableEmailPreferences(bool Enable)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //General Section
                    chkDepartmentAuthorization.Checked = Enable;
                    chkRequestorPhone.Checked = Enable;
                    chkAccountNo.Checked = Enable;
                    chkCancellationDesc.Checked = Enable;
                    chkStatus.Checked = Enable;
                    chkAirport.Checked = Enable;
                    chkRunway.Checked = Enable;
                    chkChecklist.Checked = Enable;

                    //Home Date/Time
                    chkHomeArrival.Checked = Enable;
                    chkHomeDeparture.Checked = Enable;
                    radZULU.Checked = Enable;

                    //Crew Duty Rules
                    chkEndDuty.Checked = Enable;
                    chkOverride.Checked = Enable;
                    chkFAR.Checked = Enable;
                    chkCrewRules.Checked = Enable;
                    chkDutyHours.Checked = Enable;
                    chkRestHours.Checked = Enable;
                    chkFlightHours.Checked = Enable;
                    chkAssociatedCrew.Checked = Enable;

                    //OutBound Instructions
                    chkNEWSPAPER.Checked = Enable;
                    chkCOFFEE.Checked = Enable;
                    chkJUICE.Checked = Enable;

                    //FBO Handler
                    chkDepartureInformation.Checked = Enable;
                    chkArrivalInformation.Checked = Enable;

                    //Crew 
                    chkCrewTransport.Checked = Enable;
                    chkCrewArrival.Checked = Enable;
                    chkHotel.Checked = Enable;

                    //Passenger
                    chkPassengerTransport.Checked = Enable;
                    chkPassengerArrival.Checked = Enable;
                    chkPassengerDetails.Checked = Enable;
                    chkPassengerHotel.Checked = Enable;
                    chkPassengerPaxPhone.Checked = Enable;

                    //Catering
                    chkDepartureCatering.Checked = Enable;
                    chkArrivalCatering.Checked = Enable;

                    //General
                    chkArrivalDepartureTime.Checked = Enable;
                    chkAircraft.Checked = Enable;
                    chkArrivalDepartureICAO.Checked = Enable;
                    chkGeneralCrew.Checked = Enable;
                    chkGeneralPassenger.Checked = Enable;
                    chkOutBoundInstructions.Checked = Enable;
                    chkGeneralChecklist.Checked = Enable;

                    //Logistics
                    chkLogisticsFBO.Checked = Enable;
                    chkLogisticsHotel.Checked = Enable;
                    chkLogisticsTransportation.Checked = Enable;
                    chkLogisticsCatering.Checked = Enable;

                    chkCrewHotelConfirm.Checked = Enable;
                    chkCrewHotelComments.Checked = Enable;
                    chkCrewDeptTransConfirm.Checked = Enable;
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }

        #endregion





        #region "Check Exists Methods"

        private bool CheckCountryExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx, cval))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetCountryWithFilters(txtbx.Text.Trim(),0).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.Country> CountryList = new List<FlightPakMasterService.Country>();
                                CountryList = (List<FlightPakMasterService.Country>)objRetVal.ToList();
                                if (txtbx.ID.Equals("tbCountry"))
                                {
                                    hdnCountryID.Value = CountryList[0].CountryID.ToString();
                                    tbCountry.Text = CountryList[0].CountryCD.ToString();
                                }
                                else if (txtbx.ID.Equals("tbCountryBirth"))
                                {
                                    hdnResidenceCountryID.Value = CountryList[0].CountryID.ToString();
                                    tbCountryBirth.Text = CountryList[0].CountryCD.ToString();
                                }
                                else if (txtbx.ID.Equals("tbCitizenCode"))
                                {
                                    hdnCitizenshipID.Value = CountryList[0].CountryID.ToString();
                                    tbCitizenCode.Text = CountryList[0].CountryCD.ToString();
                                }
                                else if (txtbx.ID.Equals("tbLicCountry"))
                                {
                                    hdnLicCountryID.Value = CountryList[0].CountryID.ToString();
                                    tbLicCountry.Text = CountryList[0].CountryCD.ToString();
                                }
                                else if (txtbx.ID.Equals("tbAddLicCountry"))
                                {
                                    hdnAddLicCountryID.Value = CountryList[0].CountryID.ToString();
                                    tbAddLicCountry.Text = CountryList[0].CountryCD.ToString();
                                }
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To check if valid client code exists or not
        /// </summary>
        /// <returns></returns>
        private bool CheckClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCode.Text != string.Empty) && (tbClientCode.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValue = ClientService.GetClientWithFilters(0,tbClientCode.Text.Trim(),false).EntityList;
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                cvClientCode.IsValid = false;
                                tbClientCode.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                //foreach (FlightPakMasterService.Client cm in ClientValue)
                                //{
                                //    hdnClientID.Value = cm.ClientID.ToString();
                                //}
                                tbClientCode.Text = ClientValue[0].ClientCD;
                                hdnClientID.Value = ClientValue[0].ClientID.ToString();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        private bool CheckClientFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClientCodeFilter.Text != string.Empty) && (tbClientCodeFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValue = ClientService.GetClientWithFilters(0,tbClientCode.Text.Trim(),false).EntityList;
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                //cvClientCodeFilter.IsValid = false;
                                // tbClientCodeFilter.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        private bool CheckCrewGroupFilterCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCrewGroupFilter.Text != string.Empty) && (tbCrewGroupFilter.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ClientValue = ClientService.GetCrewGroupWithFilters(0,tbCrewGroupFilter.Text.Trim(),false).EntityList;
                            if (ClientValue.Count() == 0 || ClientValue == null)
                            {
                                RetVal = true;
                            }
                            else
                            {
                                tbCrewGroupFilter.Text = ClientValue[0].CrewGroupCD;
                                hdnCrewGroupID.Value = ClientValue[0].CrewGroupID.ToString();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        /// To check if valid Department code exists or not
        /// </summary>
        /// <returns></returns>
        private bool CheckDepartmentCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbDepartment.Text != string.Empty) && (tbDepartment.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient DepartmentService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var DepartmentValue = DepartmentService.GetDepartmentByWithFilters(tbDepartment.Text.Trim(),0,0,false).EntityList;
                            if (DepartmentValue.Count() == 0 || DepartmentValue == null)
                            {
                                cvDepartmentCode.IsValid = false;
                                tbDepartment.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                //foreach (FlightPakMasterService.GetAllDeptAuth cm in DepartmentValue)
                                //{
                                //    hdnDepartmentID.Value = cm.DepartmentID.ToString();
                                //}

                                tbDepartment.Text = DepartmentValue[0].DepartmentCD;
                                hdnDepartmentID.Value = DepartmentValue[0].DepartmentID.ToString();

                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                return RetVal;
            }
        }

        /// <summary>
        /// To check if valid homebase code exists or not
        /// </summary>
        /// <returns></returns>
        private bool CheckHomeBaseExist()
        {
            if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient HomeBaseService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var HomeBaseValue = HomeBaseService.GetAirportByAirportICaoID(tbHomeBase.Text.Trim()).EntityList;
                    if (HomeBaseValue.Count() == 0 || HomeBaseValue == null)
                    {
                        cvHomeBase.IsValid = false;
                        tbHomeBase.Focus();
                        return true;
                    }
                    else
                    {
                        //foreach (FlightPakMasterService.GetAllAirport cm in HomeBaseValue)
                        //{
                        //    hdnHomeBaseID.Value = cm.AirportID.ToString();
                        //}
                        tbHomeBase.Text = HomeBaseValue[0].IcaoID.ToUpper();
                        hdnHomeBaseID.Value = HomeBaseValue[0].AirportID.ToString();
                        return false;
                    }
                }
            }
            return false;
        }


        #endregion

        #region "AJAX Events"

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1) || (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1))
                        {
                            e.Updated = dgCrewRoster;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Define the method of IPostBackEventHandler that raises change events.
        /// </summary>
        /// <param name="sourceControl"></param>
        /// <param name="eventArgument"></param>
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Rebind the popup values in the corresponding grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                // Additional Info

                                // Declaring Lists
                                List<GetCrewDefinition> FinalList = new List<GetCrewDefinition>();
                                List<GetCrewDefinition> RetainList = new List<GetCrewDefinition>();
                                List<GetCrewDefinition> NewList = new List<GetCrewDefinition>();

                                // Retain Grid Items and bind into List and add into Final List
                                RetainList = RetainCrewDefinitionGrid(dgCrewAddlInfo);
                                FinalList.AddRange(RetainList);

                                if (Session["CrewNewAddlInfo"] != null)
                                {
                                    // Bind Selected New Item and add into Final List
                                    NewList = (List<GetCrewDefinition>)Session["CrewNewAddlInfo"];
                                    FinalList.AddRange(NewList);
                                }
                                // Bind final list into Session
                                Session["CrewAddInfo"] = FinalList;

                                // Bind final list into Grid
                                dgCrewAddlInfo.DataSource = FinalList.Where(x => x.IsDeleted == false).ToList();
                                dgCrewAddlInfo.DataBind();

                                // Empty Newly added Session item
                                Session.Remove("CrewNewAddlInfo");


                                // Type Rating

                                // Declaring Lists
                                List<GetCrewRating> FinalList1 = new List<GetCrewRating>();
                                List<GetCrewRating> RetainList1 = new List<GetCrewRating>();
                                List<GetCrewRating> NewList1 = new List<GetCrewRating>();

                                // Retain Grid Items and bind into List and add into Final List
                                RetainList1 = RetainCrewTypeRatingGrid(dgTypeRating);
                                FinalList1.AddRange(RetainList1);

                                if (Session["CrewNewTypeRating"] != null)
                                {

                                    // Bind Selected New Item and add into Final List
                                    NewList1 = (List<GetCrewRating>)Session["CrewNewTypeRating"];
                                    FinalList1.AddRange(NewList1);
                                }

                                // Bind final list into Session
                                Session["CrewTypeRating"] = FinalList1;

                                // Bind final list into Grid
                                dgTypeRating.DataSource = FinalList1.Where(x => x.IsDeleted == false).ToList();
                                dgTypeRating.DataBind();

                                // Empty Newly added Session item
                                Session.Remove("CrewNewTypeRating");


                                // Crew Check List

                                // Declaring Lists
                                List<GetCrewCheckListDate> FinalList2 = new List<GetCrewCheckListDate>();
                                List<GetCrewCheckListDate> RetainList2 = new List<GetCrewCheckListDate>();
                                List<GetCrewCheckListDate> NewList2 = new List<GetCrewCheckListDate>();

                                // Retain Grid Items and bind into List and add into Final List
                                RetainList2 = RetainCrewCheckListGrid(dgCrewCheckList);
                                FinalList2.AddRange(RetainList2);

                                if (Session["CrewNewCheckList"] != null)
                                {
                                    // Bind Selected New Item and add into Final List
                                    NewList2 = (List<GetCrewCheckListDate>)Session["CrewNewCheckList"];
                                    FinalList2.AddRange(NewList2);
                                }

                                // Bind final list into Session
                                Session["CrewCheckList"] = FinalList2;

                                // Bind final list into Grid
                                dgCrewCheckList.DataSource = FinalList2.Where(x => x.IsDeleted == false).ToList();
                                dgCrewCheckList.DataBind();

                                Session.Remove("CrewNewCheckList");
                                DefaultChecklistSelection();
                                DefaultTypeRatingSelection();
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        #endregion


        #region "Checkbox Changed Events"


        protected void chkChoice_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPassport.Items.Count > 0)
                        {

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void chkTRInactive_CheckedChanged(object sender, EventArgs e)
        {
            if (dgTypeRating.Items.Count != 0)
            {
                if (dgTypeRating.SelectedItems.Count != 0)
                {
                    GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                    CheckBox chkTRInactive = (CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRInactive");
                    if (chkTRInactive.Checked == true)
                    {

                        chkTRInactive.Checked = true;
                    }
                    else
                    {
                        chkTRInactive.Checked = false;
                    }
                }
                else
                {
                    string AlertMsg = "radalert('Please select a record in the grid.', 360, 50, 'Crew Roster - Type Rating');";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
            }
            else
            {
                string AlertMsg = "radalert('Please add a record in the grid and select it.', 360, 50, 'Crew Roster - Type Rating');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
            }
        }
        /// <summary>
        /// Method to Select All / De-Select All in Email Preferences Section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SelectAll_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkSelectAll.Checked == true)
                        {
                            EnableEmailPreferences(true);
                        }
                        else
                        {
                            EnableEmailPreferences(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }







        /// <summary>
        /// Check Change Event for Trip Pilot in Command 91 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRPIC91_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsPilotinCommand = (CheckBox)SelectedItem["IsPilotinCommand"].FindControl("chkIsPilotinCommand");
                                if (chkTRPIC91.Checked == true)
                                {
                                    chkIsPilotinCommand.Checked = true;
                                }
                                else
                                {
                                    chkIsPilotinCommand.Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Check Change Event for Second in Command 91 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRSIC91_CheckedChanged(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];
                                CheckBox chkIsSecondInCommand = (CheckBox)SelectedItem["IsSecondInCommand"].FindControl("chkIsSecondInCommand");
                                if (chkTRSIC91.Checked == true)
                                {
                                    chkIsSecondInCommand.Checked = true;
                                }
                                else
                                {
                                    chkIsSecondInCommand.Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }

        /// <summary>
        /// Check Change Event for Pilot in Command 135 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRPIC135_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {

                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];


                                CheckBox chkIsPilotinCommand = (CheckBox)SelectedItem["IsPilotinCommand"].FindControl("chkIsPilotinCommand");
                                if (chkTRPIC135.Checked == true)
                                {
                                    chkIsPilotinCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked = true;
                                }
                                else
                                {
                                    chkIsPilotinCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked = false;
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        /// <summary>
        /// Check Change Event for Second in Command 135 Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TRSIC135_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgTypeRating.Items.Count > 0)
                        {
                            if (dgTypeRating.SelectedItems.Count > 0)
                            {
                                GridDataItem SelectedItem = (GridDataItem)dgTypeRating.SelectedItems[0];

                                CheckBox chkIsSecondInCommand = (CheckBox)SelectedItem["IsSecondInCommand"].FindControl("chkIsSecondInCommand");
                                if (chkTRSIC135.Checked == true)
                                {
                                    chkIsSecondInCommand.Checked = true;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked = true;
                                }
                                else
                                {
                                    chkIsSecondInCommand.Checked = false;
                                    ((CheckBox)SelectedItem["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }



        #endregion


        #region "Main Crew Text Changed Events"


        /// <summary>
        /// Method to check hoembase is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text.Trim() != string.Empty)
                        {
                            CheckHomeBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to check clientcode is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClientCode.Text.Trim() != string.Empty)
                        {
                            CheckClientCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to check department is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Department_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbDepartment.Text.Trim() != string.Empty)
                        {
                            CheckDepartmentCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }


        /// <summary>
        /// Method to check countrycode is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Country_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountry.Text != string.Empty)
                        {
                            CheckCountryExist(tbCountry, cvCountry);
                        }
                        else
                        {
                            hdnCountryID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to check country of birth is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryBirth_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountryBirth.Text != string.Empty)
                        {

                            CheckCountryExist(tbCountryBirth, cvCountryBirth);
                        }
                        else
                        {
                            hdnResidenceCountryID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to check citizen code is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CitizenCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCitizenCode.Text != string.Empty)
                        {
                            CheckCountryExist(tbCitizenCode, cvCitizenCode);
                        }
                        else
                        {
                            hdnCitizenshipID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to check LicCountry code is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LicCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist(tbLicCountry, cvLicCountry);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Method to check AddLicCountry code is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddLicCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist(tbAddLicCountry, cvAddLicCountry);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        #endregion


        #region Checklist Check Changed Events
        protected void radFreq_SelectedIndexChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgCrewCheckList.SelectedItems[0] as GridDataItem;
                            if (radFreq.SelectedItem.Value == "1")
                            {
                                ((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text = "1";
                            }
                            else
                            {
                                ((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text = "2";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        protected void ZULU_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (radZULU.Checked == true)
                        {
                            string AlertMsg = "radalert('E-mail sent will now be in UTC time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        }
                        else
                        {
                            string AlertMsg = "radalert('E-mail sent will now be in Local time. E-mail sent prior to this change may be inconsistent.', 360, 50, 'Crew Roster Catalog');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkPIC91_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked = chkPIC91.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkPIC135_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked = chkPIC135.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkSIC91_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked = chkSIC91.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkSIC135_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked = chkSIC135.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkInactive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked = chkInactive.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for Set to end of Month Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetToEndOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToNextOfMonth.Checked = false;
                        chkSetToEndOfCalenderYear.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Check Change Event for Set to Next Month Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetToNextOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToEndOfMonth.Checked = false;
                        chkSetToEndOfCalenderYear.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        /// <summary>
        /// Check Change Event for Set to end of Calender Year Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetToEndOfCalenderYear_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSetToEndOfMonth.Checked = false;
                        chkSetToNextOfMonth.Checked = false;

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ckhDisableDateCalculation_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked = ckhDisableDateCalculation.Checked;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Check Change Event for One Time Event Option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OneTimeEvent_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkOneTimeEvent.Checked == true)
                        {
                            btnChgBaseDate.Enabled = false;
                            chkCompleted.Enabled = true;
                            chkPrintOneTimeEventCompleted.Enabled = true;
                            tbChgBaseDate.Enabled = false;
                            radFreq.Enabled = false;
                            tbFreq.Enabled = false;
                            tbDueNext.Enabled = false;
                            tbAlertDate.Enabled = false;
                            tbAlertDays.Enabled = false;
                            tbGraceDate.Enabled = false;
                            tbGraceDays.Enabled = false;
                            tbTotalReqFlightHrs.Enabled = false;
                        }
                        else if (chkOneTimeEvent.Checked == false)
                        {
                            btnChgBaseDate.Enabled = true;
                            chkCompleted.Enabled = false;
                            chkPrintOneTimeEventCompleted.Enabled = false;
                            tbChgBaseDate.Enabled = true;
                            radFreq.Enabled = true;
                            tbFreq.Enabled = true;
                            tbDueNext.Enabled = true;
                            tbAlertDate.Enabled = true;
                            tbAlertDays.Enabled = true;
                            tbGraceDate.Enabled = true;
                            tbGraceDays.Enabled = true;
                            tbTotalReqFlightHrs.Enabled = true;
                        }

                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked = chkOneTimeEvent.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkCompleted_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked = chkCompleted.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkNonConflictedEvent_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked = chkNonConflictedEvent.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkRemoveFromCrewRosterRept_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked = chkRemoveFromCrewRosterRept.Checked;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkRemoveFromChecklistRept_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked = chkRemoveFromChecklistRept.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkDoNotPrintPassDueAlert_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked = chkDoNotPrintPassDueAlert.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ChecklistchkPrintOneTimeEventCompleted_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCrewCheckList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                            ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked = chkPrintOneTimeEventCompleted.Checked;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CheckItemInGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgCrewCheckList.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = (GridDataItem)dgCrewCheckList.SelectedItems[0];
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked = chkPIC91.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked = chkPIC135.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked = chkSIC91.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked = chkSIC135.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked = chkInactive.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked = chkSetToEndOfMonth.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked = ckhDisableDateCalculation.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked = chkOneTimeEvent.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked = chkCompleted.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked = chkNonConflictedEvent.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked = chkRemoveFromCrewRosterRept.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked = chkRemoveFromChecklistRept.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked = chkPrintOneTimeEventCompleted.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked = chkDoNotPrintPassDueAlert.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked = chkSetToNextOfMonth.Checked;
                        ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked = chkSetToEndOfCalenderYear.Checked;

                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion

        #region Filters

        protected void ActiveOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if ((Service.GetCrewList().ReturnFlag == true) && (chkActiveOnly.Checked == true))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => (x.IsStatus == true) && (x.IsDeleted == false)).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);

                            }
                            if ((Service.GetCrewList().ReturnFlag == true) && (chkActiveOnly.Checked == false))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => x.IsDeleted == false).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void HomeBaseOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if ((chkHomeBaseOnly.Checked == true))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => (x.HomebaseID != null) && (x.IsDeleted == false)).ToList();
                                // dgCrewRoster.Rebind();
                                DefaultSelection(true);

                            }
                            if ((chkHomeBaseOnly.Checked == false))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => x.IsDeleted == false).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        protected void FixedWingOnly_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if ((chkFixedWingOnly.Checked == true))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => (x.IsRotaryWing == true) && (x.IsDeleted == false)).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);
                            }
                            if ((chkFixedWingOnly.Checked == false))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => x.IsDeleted == false).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void RotaryWing_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if ((chkRotaryWingOnly.Checked == true))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => (x.IsRotaryWing == true) && (x.IsDeleted == false)).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);

                            }
                            if ((chkRotaryWingOnly.Checked == false))
                            {
                                dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => x.IsDeleted == false).ToList();
                                //dgCrewRoster.Rebind();
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void CrewGroupFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckCrewGroupFilterCodeExist())
                        {
                            lbCrewGroupCodeFilter.Visible = true;
                            lbCrewGroupCodeFilter.ForeColor = System.Drawing.Color.Red;
                            tbCrewGroupFilter.Focus();
                        }
                        else
                        {
                            lbCrewGroupCodeFilter.Visible = false;
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if ((tbCrewGroupFilter.Text != string.Empty))
                                {
                                    FPPrincipal identityFilter = (FPPrincipal)Session[Session.SessionID];
                                    if (hdnCrewGroupID.Value != string.Empty)
                                    {
                                        dgCrewRoster.DataSource = Service.GetCrewGroupBasedCrewInfo(Convert.ToInt64(hdnCrewGroupID.Value)).EntityList.Where(x => x.IsDeleted == false).ToList();
                                        //dgCrewRoster.Rebind();
                                        DefaultSelection(true);
                                    }

                                }
                                if ((tbCrewGroupFilter.Text == string.Empty))
                                {
                                    dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => x.IsDeleted == false).ToList();
                                    //dgCrewRoster.Rebind();
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (CheckClientFilterCodeExist())
                        {
                            lbClientCodeFilter.Visible = true;
                            lbClientCodeFilter.ForeColor = System.Drawing.Color.Red;
                            tbClientCodeFilter.Focus();
                        }
                        else
                        {
                            lbClientCodeFilter.Visible = false;
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if ((tbClientCodeFilter.Text != string.Empty))
                                {
                                    dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => (x.ClientCD == tbClientCodeFilter.Text.ToString().TrimEnd()) && x.IsDeleted == false).ToList();
                                    //dgCrewRoster.Rebind();
                                    DefaultSelection(true);

                                }
                                if ((tbClientCodeFilter.Text == string.Empty))
                                {
                                    dgCrewRoster.DataSource = Service.GetCrewList().EntityList.Where(x => x.IsDeleted == false).ToList();
                                    // dgCrewRoster.Rebind();
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        #endregion

        #region "Date Format

        private DateTime FormatDate(string Date, string Format)
        {
            string[] DateAndTime = Date.Split(' ');

            string[] SplitDate = new string[3];
            string[] SplitFormat = new string[3];

            if (DateFormat.Contains("/"))
            {
                SplitDate = DateAndTime[0].Split('/');
                SplitFormat = Format.Split('/');
            }
            else if (DateFormat.Contains("-"))
            {
                SplitDate = DateAndTime[0].Split('-');
                SplitFormat = Format.Split('-');
            }
            else if (DateFormat.Contains("."))
            {
                SplitDate = DateAndTime[0].Split('.');
                SplitFormat = Format.Split('.');
            }

            int dd = 0, mm = 0, yyyy = 0;
            for (int Index = 0; Index < SplitFormat.Count(); Index++)
            {
                if (SplitFormat[Index].ToLower().Contains("d"))
                {
                    dd = Convert.ToInt16(SplitDate[Index]);
                }
                if (SplitFormat[Index].ToLower().Contains("m"))
                {
                    mm = Convert.ToInt16(SplitDate[Index]);
                }
                if (SplitFormat[Index].ToLower().Contains("y"))
                {
                    yyyy = Convert.ToInt16(SplitDate[Index]);
                }
            }
            return new DateTime(yyyy, mm, dd);
        }

        #endregion


        #region "Retain Grid Events"
        /// <summary>
        /// Method to Retain Additional Info Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Definition List</returns>
        private List<GetCrewDefinition> RetainCrewDefinitionGrid(RadGrid grid)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetCrewDefinition> CrewDefinitionList = new List<GetCrewDefinition>();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetCrewDefinition>>(() =>
                {
                    List<FlightPakMasterService.GetCrewDefinition> CrewRetainDefinition = new List<FlightPakMasterService.GetCrewDefinition>();
                    CrewRetainDefinition = (List<FlightPakMasterService.GetCrewDefinition>)Session["CrewAddInfo"];
                    List<FlightPakMasterService.GetCrewDefinition> CrewRetainDefinitionList = new List<FlightPakMasterService.GetCrewDefinition>();
                    for (int Index = 0; Index < CrewRetainDefinition.Count; Index++)
                    {
                        foreach (GridDataItem item in grid.MasterTableView.Items)
                        {

                            if (item.GetDataKeyValue("CrewInfoCD").ToString() == CrewRetainDefinition[Index].CrewInfoCD)
                            {
                                CrewRetainDefinition[Index].CrewID = CrewRetainDefinition[Index].CrewID;
                                CrewRetainDefinition[Index].IsReptFilter = ((CheckBox)item["IsReptFilter"].FindControl("chkIsReptFilter")).Checked;
                                CrewRetainDefinition[Index].InformationValue = ((TextBox)item["AddlInfo"].FindControl("tbAddlInfo")).Text;
                                //PassengerAddlInfo[Index].IsDeleted = false;
                                break;
                            }
                        }
                    }
                    return CrewRetainDefinition;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }

        }

        /// <summary>
        /// Method to Retain Type Ratings Grid Items into List
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns>Returns Crew Type Ratings List</returns>
        private List<GetCrewRating> RetainCrewTypeRatingGrid(RadGrid Grid)
        {
            List<GetCrewRating> CrewRateList = new List<GetCrewRating>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Grid))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetCrewRating>>(() =>
                {

                    List<FlightPakMasterService.GetCrewRating> CrewRetainRating = new List<FlightPakMasterService.GetCrewRating>();
                    CrewRetainRating = (List<FlightPakMasterService.GetCrewRating>)Session["CrewTypeRating"];
                    List<FlightPakMasterService.GetCrewRating> CrewRetainDefinitionList = new List<FlightPakMasterService.GetCrewRating>();
                    for (int Index = 0; Index < CrewRetainRating.Count; Index++)
                    {
                        foreach (GridDataItem Item in Grid.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AircraftTypeCD").ToString() == CrewRetainRating[Index].AircraftTypeCD)
                            {
                                if (((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")) != null)
                                {
                                    CrewRetainRating[Index].IsPilotinCommand = ((CheckBox)Item["IsPilotinCommand"].FindControl("chkIsPilotinCommand")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPilotinCommand = false;
                                }

                                if (((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")) != null)
                                {
                                    CrewRetainRating[Index].IsSecondInCommand = ((CheckBox)Item["IsSecondInCommand"].FindControl("chkIsSecondInCommand")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSecondInCommand = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")) != null)
                                {
                                    CrewRetainRating[Index].IsQualInType135PIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRPIC135")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsQualInType135PIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")) != null)
                                {
                                    CrewRetainRating[Index].IsQualInType135SIC = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRSIC135")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsQualInType135SIC = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")) != null)
                                {
                                    CrewRetainRating[Index].IsEngineer = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTREngineer")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsEngineer = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")) != null)
                                {
                                    CrewRetainRating[Index].IsInstructor = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInstructor")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsInstructor = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")) != null)
                                {
                                    CrewRetainRating[Index].IsCheckAttendant = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsCheckAttendant = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendantFAR91 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant91")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendantFAR91 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendantFAR135 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAttendant135")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendantFAR135 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")) != null)
                                {
                                    CrewRetainRating[Index].IsCheckAirman = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRAirman")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsCheckAirman = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")) != null)
                                {
                                    CrewRetainRating[Index].IsInActive = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkTRInactive")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsInActive = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")) != null)
                                {
                                    CrewRetainRating[Index].IsPIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC121")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPIC121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")) != null)
                                {
                                    CrewRetainRating[Index].IsSIC121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC121")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSIC121 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")) != null)
                                {
                                    CrewRetainRating[Index].IsPIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkPIC125")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsPIC125 = false;
                                }
                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")) != null)
                                {
                                    CrewRetainRating[Index].IsSIC125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkSIC125")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsSIC125 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendant121 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent121")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendant121 = false;
                                }

                                if (((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")) != null)
                                {
                                    CrewRetainRating[Index].IsAttendant125 = ((CheckBox)Item["HiddenCheckboxValues"].FindControl("chkAttendent125")).Checked;
                                }
                                else
                                {
                                    CrewRetainRating[Index].IsAttendant125 = false;
                                }
                                // Total Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                                {
                                    //if (DateFormat != null)
                                    //{
                                    //    CrewRetainRating[Index].TotalUpdateAsOfDT = FormatDateTime(String.Format((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()), DateFormat);
                                    //}
                                    //else
                                    //{
                                    CrewRetainRating[Index].TotalUpdateAsOfDT = Convert.ToDateTime((((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim()));
                                    //}
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text))
                                {
                                    CrewRetainRating[Index].TotalTimeInTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalTimeInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalTimeInTypeHrs = 0;
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text))
                                {
                                    CrewRetainRating[Index].TotalDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalDayHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text))
                                {
                                    CrewRetainRating[Index].TotalNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalNightHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text))
                                {
                                    CrewRetainRating[Index].TotalINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lblTotalINSTHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TotalINSTHrs = 0;
                                }


                                // PIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text))
                                {
                                    CrewRetainRating[Index].PilotInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInType")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].PilotInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text))
                                {
                                    CrewRetainRating[Index].TPilotinCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TPilotinCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text))
                                {
                                    CrewRetainRating[Index].TPilotInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TPilotInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text))
                                {
                                    CrewRetainRating[Index].TPilotInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbPICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].TPilotInCommandINSTHrs = 0;
                                }



                                // SIC Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandTypeHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICInTypeHrs")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandTypeHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandDayHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsDay")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandDayHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandNightHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsNight")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandNightHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text))
                                {
                                    CrewRetainRating[Index].SecondInCommandINSTHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbSICHrsInstrument")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].SecondInCommandINSTHrs = 0;
                                }


                                if (!string.IsNullOrEmpty(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text))
                                {
                                    //if (DateFormat != null)
                                    //{
                                    //    CrewRetainRating[Index].AsOfDT = FormatDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim(), DateFormat);
                                    //}
                                    //else
                                    //{
                                    CrewRetainRating[Index].AsOfDT = Convert.ToDateTime(((Label)Item["TotalUpdateAsOfDT"].FindControl("lbTotalUpdateAsOfDT")).Text.Trim());
                                    //}
                                }
                                else
                                {
                                    CrewRetainRating[Index].AsOfDT = null;
                                }



                                // Others Hrs
                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text))
                                {

                                    CrewRetainRating[Index].OtherSimHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsSimulator")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherSimHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text))
                                {
                                    CrewRetainRating[Index].OtherFlightEngHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltEngineer")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherFlightEngHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text))
                                {
                                    CrewRetainRating[Index].OtherFlightInstrutorHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltInstructor")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherFlightInstrutorHrs = 0;
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text))
                                {
                                    CrewRetainRating[Index].OtherFlightAttdHrs = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("lbOtherHrsFltAttendant")).Text, CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    CrewRetainRating[Index].OtherFlightAttdHrs = 0;
                                }
                                break;
                            }
                        }
                    }
                    return CrewRetainRating;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Retain Crew Checklist Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Checklist</returns>
        private List<GetCrewCheckListDate> RetainCrewCheckListGrid(RadGrid grid)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetCrewCheckListDate> CrewCheckListDateList = new List<GetCrewCheckListDate>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<FlightPakMasterService.GetCrewCheckListDate>>(() =>
                {
                    List<FlightPakMasterService.GetCrewCheckListDate> GetCrewCheckListDateRetain = new List<FlightPakMasterService.GetCrewCheckListDate>();
                    GetCrewCheckListDateRetain = (List<FlightPakMasterService.GetCrewCheckListDate>)Session["CrewCheckList"];
                    List<FlightPakMasterService.GetCrewCheckListDate> CrewCheckListRetainList = new List<FlightPakMasterService.GetCrewCheckListDate>();
                    for (int Index = 0; Index < GetCrewCheckListDateRetain.Count; Index++)
                    {
                        foreach (GridDataItem Item in grid.MasterTableView.Items)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (Item.GetDataKeyValue("CheckListCD").ToString() == GetCrewCheckListDateRetain[Index].CheckListCD)
                            {
                                GetCrewCheckListDateRetain[Index].CrewID = GetCrewCheckListDateRetain[Index].CrewID;
                                GetCrewCheckListDateRetain[Index].CrewChecklistDescription = ((Label)Item["CrewChecklistDescription"].FindControl("lbCrewChecklistDescription")).Text;
                                if (!string.IsNullOrEmpty(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].OriginalDT = FormatDate(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].OriginalDT = Convert.ToDateTime(((Label)Item[OriginalDT].FindControl(lbOriginalDT)).Text);
                                    }
                                }


                                if (!string.IsNullOrEmpty(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].PreviousCheckDT = FormatDate(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].PreviousCheckDT = Convert.ToDateTime(((Label)Item[PreviousCheckDT].FindControl(lbPreviousCheckDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[DueDT].FindControl(lbDueDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].DueDT = FormatDate(((Label)Item[DueDT].FindControl(lbDueDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].DueDT = Convert.ToDateTime(((Label)Item[DueDT].FindControl(lbDueDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].AlertDT = FormatDate(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].AlertDT = Convert.ToDateTime(((Label)Item[AlertDT].FindControl(lbAlertDT)).Text);
                                    }
                                }

                                if (!string.IsNullOrEmpty(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].GraceDT = FormatDate(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].GraceDT = Convert.ToDateTime(((Label)Item[GraceDT].FindControl(lbGraceDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].AlertDays = Convert.ToInt32(((Label)Item["AlertDays"].FindControl("lbAlertDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].GraceDays = Convert.ToInt32(((Label)Item["GraceDays"].FindControl("lbGraceDays")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].FrequencyMonth = Convert.ToInt32(((Label)Item["FrequencyMonth"].FindControl("lbFrequencyMonth")).Text);
                                }
                                if (!string.IsNullOrEmpty(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text))
                                {
                                    if (DateFormat != null)
                                    {
                                        GetCrewCheckListDateRetain[Index].BaseMonthDT = FormatDate(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text, DateFormat);
                                    }
                                    else
                                    {
                                        GetCrewCheckListDateRetain[Index].BaseMonthDT = Convert.ToDateTime(((Label)Item[BaseMonthDT].FindControl(lbBaseMonthDT)).Text);
                                    }
                                }
                                if (!string.IsNullOrEmpty(((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].AircraftTypeCD = ((Label)Item["AircraftTypeCD"].FindControl("lbAircraftTypeCD")).Text;
                                    using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
                                    {
                                        var CrewRosterServiceValue = CrewRosterService.GetAircraftWithFilters(GetCrewCheckListDateRetain[Index].AircraftTypeCD.ToUpper().Trim(),0,false).EntityList;
                                        if (CrewRosterServiceValue != null && CrewRosterServiceValue.Count > 0)
                                        {
                                            GetCrewCheckListDateRetain[Index].AircraftID = Convert.ToInt64(((FlightPak.Web.FlightPakMasterService.Aircraft)CrewRosterServiceValue[0]).AircraftID.ToString());
                                        }
                                    }
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].Frequency = Convert.ToDecimal(((Label)Item["HiddenCheckboxValues"].FindControl("RadFreq")).Text);
                                }

                                if (!string.IsNullOrEmpty(((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text))
                                {
                                    GetCrewCheckListDateRetain[Index].TotalREQFlightHrs = Convert.ToDecimal(((Label)Item["TotalREQFlightHrs"].FindControl("lbTotalREQFlightHrs")).Text);
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPilotInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR91"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPilotInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPilotInCommandFAR135"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsSecondInCommandFAR91 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR91"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsSecondInCommandFAR135 = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkSecondInCommandFAR135"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsInActive = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkInActive"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsMonthEnd = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkMonthEnd"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsStopCALC = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkStopCALC"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsOneTimeEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkOneTimeEvent"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsCompleted = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkCompleted"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNoConflictEvent = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoConflictEvent"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNoCrewCheckListREPTt = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoCrewCheckListREPTt"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNoChecklistREPT = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNoChecklistREPT"))).Checked;
                                }

                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPrintStatus = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPrintStatus"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsPassedDueAlert = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkPassedDueAlert"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsNextMonth = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkNextMonth"))).Checked;
                                }
                                if (((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))) != null)
                                {
                                    GetCrewCheckListDateRetain[Index].IsEndCalendarYear = ((CheckBox)(Item["HiddenCheckboxValues"].FindControl("chkEndCalendarYear"))).Checked;
                                }
                                break;
                            }
                        }
                    }
                    return GetCrewCheckListDateRetain;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Retain Crew Passenger Visa Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Returns Crew Pax Visa List</returns>
        //private List<GetAllCrewPaxVisa> RetainCrewVisaGrid(RadGrid grid)
        private List<GetAllCrewPaxVisa> RetainCrewVisaGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<List<GetAllCrewPaxVisa>>(() =>
                {
                    List<GetAllCrewPaxVisa> CrewPaxVisaList = new List<GetAllCrewPaxVisa>();
                    GetAllCrewPaxVisa CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                    //CrewPaxVisaList = (List<GetAllCrewPaxVisa>)Session["PassengerVisa"];
                    //bool IsExist = false;
                    //for (int Index = 0; Index < CrewPaxVisaList.Count; Index++)
                    //{
                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        CrewPaxVisaListDef = new GetAllCrewPaxVisa();
                        //if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum) //Item.GetDataKeyValue("VisaNum")
                        //if ((Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString()) == CrewPaxVisaList[Index].VisaID) && (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum))
                        //if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text == CrewPaxVisaList[Index].VisaNum)
                        //{
                        CrewPaxVisaListDef.VisaID = Convert.ToInt64(Item.GetDataKeyValue("VisaID").ToString());
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            CrewPaxVisaListDef.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString().Trim());
                        }
                        else
                        {
                            CrewPaxVisaListDef.CrewID = 0;
                        }
                        CrewPaxVisaListDef.PassengerRequestorID = null;
                        if (((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value != string.Empty)
                        {
                            CrewPaxVisaListDef.CountryID = Convert.ToInt64(((HiddenField)Item["CountryCD"].FindControl("hdnCountryID")).Value);
                            CrewPaxVisaListDef.CountryCD = ((TextBox)Item["CountryCD"].FindControl("tbCountryCD")).Text;
                        }
                        CrewPaxVisaListDef.VisaTYPE = string.Empty;
                        if (((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text != null)
                        {
                            CrewPaxVisaListDef.VisaNum = ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Text;
                        }
                        else
                        {
                            CrewPaxVisaListDef.VisaNum = string.Empty;
                        }
                        //if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        //{
                        //    DateTime scheduleDateTM = DateTime.MinValue;
                        //    //CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                        //    //CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                        //    scheduleDateTM = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, CultureInfo.InvariantCulture);

                        //    CrewPaxVisaListDef.ExpiryDT = scheduleDateTM;
                        //    //CrewPaxVisaListDef.ExpiryDT = DateTime.ParseExact(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat, CultureInfo.InvariantCulture);
                        //}

                        if (((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.ExpiryDT = FormatDate(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaListDef.ExpiryDT = Convert.ToDateTime(((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text);
                            }
                        }

                        if (((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.IssuePlace = ((TextBox)Item["IssuePlace"].FindControl("tbIssuePlace")).Text;
                        }
                        //if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        //{
                        //    //CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                        //    CrewPaxVisaListDef.IssueDate = DateTime.ParseExact(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat, CultureInfo.InvariantCulture);
                        //}

                        if (((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPaxVisaListDef.IssueDate = FormatDate(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPaxVisaListDef.IssueDate = Convert.ToDateTime(((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text);
                            }
                        }

                        if (((TextBox)Item["Notes"].FindControl("tbNotes")).Text != string.Empty)
                        {
                            CrewPaxVisaListDef.Notes = ((TextBox)Item["Notes"].FindControl("tbNotes")).Text;
                        }
                        CrewPaxVisaListDef.IsDeleted = false;
                        CrewPaxVisaList.Add(CrewPaxVisaListDef);
                        //IsExist = true;
                        //break;
                        //}
                    }
                    //if (IsExist == false)
                    //{
                    //}
                    //}
                    return CrewPaxVisaList;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to Retain Crew Passenger Passport Grid Items into List
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        private List<GetAllCrewPassport> RetainCrewPassportGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<GetAllCrewPassport> CrewPassportList = new List<GetAllCrewPassport>();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    foreach (GridDataItem Item in grid.MasterTableView.Items)
                    {
                        GetAllCrewPassport CrewPassport = new GetAllCrewPassport();
                        if (!string.IsNullOrEmpty(((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text))
                        {
                            CrewPassport.PassportNum = ((TextBox)Item["PassportNum"].FindControl("tbPassportNum")).Text;
                        }
                        CrewPassport.PassportID = Convert.ToInt64(Item.GetDataKeyValue("PassportID").ToString());
                        if (Session["SelectedCrewRosterID"] != null)
                        {
                            CrewPassport.CrewID = Convert.ToInt64(Session["SelectedCrewRosterID"].ToString());
                        }
                        else
                        {
                            CrewPassport.CrewID = 0;
                        }


                        CrewPassport.Choice = ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked;
                        //if (!string.IsNullOrEmpty(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text))
                        //{
                        //    CrewPassport.PassportExpiryDT = DateTime.ParseExact(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat, CultureInfo.InvariantCulture);
                        //    //Convert.ToDateTime(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, CultureInfo.InvariantCulture);
                        //}

                        if (((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassport.PassportExpiryDT = FormatDate(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPassport.PassportExpiryDT = Convert.ToDateTime(((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text);
                            }
                        }

                        if (!string.IsNullOrEmpty(((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text))
                        {
                            CrewPassport.IssueCity = ((TextBox)Item["IssueCity"].FindControl("tbIssueCity")).Text;
                        }
                        //if (!string.IsNullOrEmpty(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text))
                        //{
                        //    CrewPassport.IssueDT = DateTime.ParseExact(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat, CultureInfo.InvariantCulture);
                        //    // Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, CultureInfo.InvariantCulture);
                        //}

                        if (((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text != string.Empty)
                        {
                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (DateFormat != null)
                            {
                                CrewPassport.IssueDT = FormatDate(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text, DateFormat);
                            }
                            else
                            {
                                CrewPassport.IssueDT = Convert.ToDateTime(((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text);
                            }
                        }

                        if (!string.IsNullOrEmpty(((TextBox)Item["PassportCountry"].FindControl("tbPassportCountry")).Text))
                        {
                            CrewPassport.CountryCD = ((TextBox)Item["PassportCountry"].FindControl("tbPassportCountry")).Text;
                        }
                        if (Item.GetDataKeyValue("CountryID") != null)
                        {
                            CrewPassport.CountryID = Convert.ToInt64(Item.GetDataKeyValue("CountryID").ToString());
                        }

                        if (((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value != string.Empty)
                        {
                            CrewPassport.CountryID = Convert.ToInt64(((HiddenField)Item["PassportCountry"].FindControl("hdnPassportCountry")).Value);
                            CrewPassport.CountryCD = ((TextBox)Item["PassportCountry"].FindControl("tbPassportCountry")).Text;
                        }

                        CrewPassport.IsDeleted = false;
                        CrewPassportList.Add(CrewPassport);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CrewPassportList;
            }
        }

        #endregion

        #region "Radgrid Item Databounds"

        protected void Visa_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item.GetDataKeyValue("ExpiryDT") != null)
                            {
                                ((TextBox)Item["ExpiryDT"].FindControl("tbExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("ExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDate") != null)
                            {
                                ((TextBox)Item["IssueDate"].FindControl("tbIssueDate")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDate").ToString())));
                            }
                            if (Item.GetDataKeyValue("VisaID") != null)
                            {
                                if (Item.GetDataKeyValue("VisaID").ToString() == "0")
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = true;
                                }
                                else
                                {
                                    ((TextBox)Item["VisaNum"].FindControl("tbVisaNum")).Enabled = false;
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }


        }
        protected void Passport_ItemDataBound(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item.GetDataKeyValue("PassportExpiryDT") != null)
                            {
                                ((TextBox)Item["PassportExpiryDT"].FindControl("tbPassportExpiryDT")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("IssueDT") != null)
                            {
                                ((TextBox)Item["IssueDT"].FindControl("tbIssueDT")).Text = String.Format("{0:" + DateFormat + "}", (Convert.ToDateTime(Item.GetDataKeyValue("IssueDT").ToString())));
                            }
                            if (Item.GetDataKeyValue("Choice") != null)
                            {
                                ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked = Convert.ToBoolean(Item.GetDataKeyValue("Choice").ToString(), CultureInfo.CurrentCulture);
                            }
                            else
                            {
                                ((CheckBox)Item["Choice"].FindControl("chkChoice")).Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

        }

        #endregion

        private DateTime FormatDateTime(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                string[] TimeFormat = { "hh", "mm", "ss" };
                string[] SplitTime = new string[3];



                if (DateFormat.Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                    SplitFormat = Format.Split('/');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateFormat.Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                    SplitFormat = Format.Split('-');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateFormat.Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                    SplitFormat = Format.Split('.');
                    SplitTime = DateAndTime[1].Split(':');
                }

                int dd = 0, mm = 0, yyyy = 0, hh = 0, MM = 0, ss = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                for (int Index = 0; Index < TimeFormat.Count(); Index++)
                {
                    if (TimeFormat[Index].ToLower().Contains("hh"))
                    {
                        hh = Convert.ToInt32(SplitTime[Index]);
                    }
                    if (TimeFormat[Index].ToLower().Contains("mm"))
                    {
                        MM = Convert.ToInt32(SplitTime[Index]);
                    }
                    if (TimeFormat[Index].ToLower().Contains("ss"))
                    {
                        ss = Convert.ToInt32(SplitTime[Index]);
                    }
                }
                return new DateTime(yyyy, mm, dd, hh, MM, ss);
            }
        }

        private void DefaultTypeRatingSelection()
        {
            if (hdnTemp.Value == "On")
            {
                if (hdnSave.Value == "Update")
                {
                    if (dgTypeRating.Items.Count > 0)
                    {
                        dgTypeRating.SelectedIndexes.Add(0);
                        BindSelectedTypeRatingItem();
                        btnDeleteRating.Enabled = true;
                        EnableHours(true);

                    }
                    else
                    {
                        EnableHours(false);
                        btnDeleteRating.Enabled = false;
                    }
                }
                else
                {
                    if (dgTypeRating.Items.Count > 0)
                    {
                        dgTypeRating.SelectedIndexes.Add(0);
                        BindSelectedTypeRatingItem();
                        btnDeleteRating.Enabled = true;
                        EnableHours(true);
                    }
                    else
                    {
                        EnableHours(false);
                        btnDeleteRating.Enabled = false;
                    }

                }
            }
            else
            {
                if (dgTypeRating.Items.Count > 0)
                {
                    dgTypeRating.SelectedIndexes.Add(0);
                    BindSelectedTypeRatingItem();
                    btnDeleteRating.Enabled = false;
                    EnableHours(false);
                }
                else
                {
                    EnableHours(false);
                    btnDeleteRating.Enabled = false;
                }
            }
        }

        private void DefaultChecklistSelection()
        {
            if (hdnTemp.Value == "On")
            {
                if (hdnSave.Value == "Update")
                {
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                        btnDeleteChecklist.Enabled = true;
                    }
                    else
                    {
                        EnableCheckListFields(false);
                        btnDeleteChecklist.Enabled = false;
                    }
                }
                else
                {
                    if (dgCrewCheckList.Items.Count > 0)
                    {
                        dgCrewCheckList.SelectedIndexes.Add(0);
                        BindSelectedCrewChecklistItem();
                        EnableCheckListFields(true);
                        btnDeleteChecklist.Enabled = true;
                    }
                    else
                    {
                        btnDeleteChecklist.Enabled = false;
                        EnableCheckListFields(false);
                    }
                }
            }
            else
            {
                if (dgCrewCheckList.Items.Count > 0)
                {
                    dgCrewCheckList.SelectedIndexes.Add(0);
                    BindSelectedCrewChecklistItem();
                    EnableCheckListFields(false);
                    btnDeleteChecklist.Enabled = false;
                }
                else
                {
                    btnDeleteChecklist.Enabled = false;
                    EnableCheckListFields(false);
                }
            }

        }

        protected void AircraftAssigned_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<FlightPakMasterService.GetAllCrewAircraftAssign> FleetList = (List<FlightPakMasterService.GetAllCrewAircraftAssign>)Session["CrewAircraftAssigned"];
                        dgAircraftAssigned.DataSource = FleetList;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
    }
}