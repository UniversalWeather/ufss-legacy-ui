﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreFlightSIFLAirport.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreFlightSIFLAirport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="RadScriptManager1" runat="server">
    </asp:ScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            oArg = new Object();
            grid = $find("<%= dgPostFlightSIFLAirport.ClientID %>");

            //this function is used to close this window
            //            function CloseAndRebind(arg) {
            //                GetRadWindow().Close();
            //                //GetRadWindow().BrowserWindow.refreshPage(arg);
            //            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPostFlightSIFLAirport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "LegID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "ICAOID");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "AirportID");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "AirportName");

                }
                if (selectedRows.length > 0) {
                    oArg.LegID = cell1.innerHTML;
                    oArg.ICAOID = cell2.innerHTML;
                    oArg.AirportID = cell3.innerHTML;
                    oArg.AirportName = cell4.innerHTML;
                }
                else {
                    oArg.LegID = "";
                    oArg.ICAOID = "";
                    oArg.AirportID = "";
                    oArg.AirportName = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            //            function Close() {
            //                //GetRadWindow().Close();
            //                alert("Depart ICAO Not In Trip.");
            //                returnToParent();
            //            }
            function alertCallBack() { }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
        </Windows>
    </telerik:RadWindowManager>
    <table cellspacing="0" cellpadding="0" class="box1">
        <tr>
            <td>
                <telerik:RadGrid ID="dgPostFlightSIFLAirport" runat="server" AllowMultiRowSelection="false"
                    AllowSorting="true" AutoGenerateColumns="false" Height="330px" PageSize="10"
                    AllowPaging="false" Width="500px" OnItemCommand="PostFlightSIFLAirport_ItemCommand"
                    OnInsertCommand="PostFlightSIFLAirport_InsertCommand" AllowFilteringByColumn="false">
                    <MasterTableView DataKeyNames="LegID,AirportName" CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="LegID" UniqueName="LegID" HeaderText="Leg #"
                                ShowFilterIcon="false" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ICAOID" UniqueName="ICAOID" HeaderText="" ShowFilterIcon="false"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IcaoCity" UniqueName="IcaoCity" HeaderText=""
                                AllowFiltering="false" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AirportName" UniqueName="AirportName" HeaderText=""
                                ShowFilterIcon="false" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AirportID" UniqueName="AirportID" HeaderText=""
                                ShowFilterIcon="false" Display="false">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; text-align: right;">
                                <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                    Ok</button>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings>
                        <ClientEvents OnRowDblClick="returnToParent" />
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false"></GroupingSettings>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbMessage" runat="server" CssClass="alert-text"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
