﻿
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightOutboundInstruction.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightOutboundInstruction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
        <%=Styles.Render("~/bundles/jqgrid") %>
        <%=Scripts.Render("~/bundles/jquery") %>
        <%=Scripts.Render("~/bundles/jqgridjs") %>
        <%=Scripts.Render("~/bundles/knockout") %>
        <%=Scripts.Render("~/bundles/sitecommon") %>
        <%=Scripts.Render("~/bundles/preflightOutbound") %>
 <form id="form1" runat="server">
       <div id="OuterDiv" class="divGridPanel" runat="server">
            <div>
                <table class="box1">                    
                    <tr>
                        <td>
                            <table id="gridLegSummary" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            <a id="recordEdit" data-bind="event: { click: btnEdit_Click }" title="Edit" class="edit-icon-grid" href="#"></a>
                        </td>
                    </tr>                
                   
                    <tr>
                    <td>
                        <div id="DivExternalForm1" runat="server" class="ExternalForm">
                            <table id="OutboundInstructions" width="100%">
                                <tr>
                                    <td colspan="2">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="mnd_text">Leg &nbsp; 
                                                </td>
                                                <td class="mnd_text">
                                                    <label id="lblLeg" data-bind="text: LegNum"></label>
                                                </td>
                                                <td class="tdLabel50 mnd_text">:
                                                </td>
                                                <td class="tdLabel90 mnd_text">Depart ICAO
                                                </td>
                                                <td class="tdLabel120">
                                                    <label id="lblDepartICAO" data-bind="text: CurrentLegOutBoundData.DepartureAirport.IcaoID" Visible="true"></label>
                                                </td>
                                                <td class="tdLabel90 mnd_text">Arrival ICAO
                                                </td>
                                                <td>
                                                    <label id="lblArrivalICAO" data-bind="text: CurrentLegOutBoundData.ArrivalAirport.IcaoID" Visible="true"></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td class="tdLabel93">
                                                    <label id="lblOutboundInstruction1"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction1"  data-bind="value: CurrentLegOutBoundData.OutboundInstruction1, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction1" class="text315" />                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel93">
                                                    <label id="lblOutboundInstruction2"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction2"  data-bind="value: CurrentLegOutBoundData.OutboundInstruction2, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction2" class="text315" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel93">
                                                    <label id="lblOutboundInstruction3"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction3"  data-bind="value: CurrentLegOutBoundData.OutboundInstruction3, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction3" class="text315" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel93">
                                                    <label id="lblOutboundInstruction4"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction4" data-bind="value: CurrentLegOutBoundData.OutboundInstruction4, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction4"  class="text315" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel93">
                                                    <label id="lblOutboundInstruction5"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction5" data-bind="value: CurrentLegOutBoundData.OutboundInstruction5, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction5" class="text315" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel93">
                                                    <label id="lblOutboundInstruction6"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction6" data-bind="value: CurrentLegOutBoundData.OutboundInstruction6, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction6" class="text315" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel93">
                                                   <label id="lblOutboundInstruction7"/>
                                                </td>
                                                <td>
                                                    <input type="text" id="tbOutboundInstruction7" data-bind="value: CurrentLegOutBoundData.OutboundInstruction7, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutboundInstruction7" class="text315" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Fuel Load
                                    </td>
                                    <td>
                                       <input type="text" id="tbFuelLoad" data-bind="value: CurrentLegOutBoundData.FuelLoad, enable: EditMode, numeric: number, style: {textAlign:'right'}" maxlength="6" class="text80"/>
                                    </td>
                                </tr>
                                <tr>
                                     <td class="tdLabel100" style="vertical-align:top;">Fuel Info
                                    </td>
                                    <td>
                                    <textarea id="tbFuel" data-bind="value: CurrentLegOutBoundData.CrewFuelLoad, enable: EditMode, trimedValue: CurrentLegOutBoundData.CrewFuelLoad" maxlength="500"  class="textarea400x100"></textarea>
                                         </td>
                                   
                                </tr>
                                <tr>
                                    <td class="tdLabel100" style="vertical-align:top;">Comments
                                    </td>
                                    <td>
                                         <textarea id="tbComments" data-bind="value: CurrentLegOutBoundData.OutbountInstruction, enable: EditMode, trimedValue: CurrentLegOutBoundData.OutbountInstruction"  class="textarea400x100"></textarea>
                                       
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="2">
                                         
                                        <table cellpadding="0" cellspacing="0" style="min-height: 30px;" width="100%">
                                            <tr>
                                               <td align="right" style="width: 100%; text-align:right!important;" >
                                                  <input type="button" id="btnCancel" value="Cancel" data-bind="event: { click: btnCancel_Click }, visible: EditMode" class="button" />
                                                    <input type="button" id="btnSave" value="Save" data-bind="event: { click: btnSave_Click }, visible: EditMode" class="button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
