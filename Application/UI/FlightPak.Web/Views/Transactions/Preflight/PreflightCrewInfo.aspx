﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Preflight.Master"
    AutoEventWireup="true" CodeBehind="PreflightCrewInfo.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreflightCrewInfo" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Preflight.Master" %>
<%@ Register Src="~/UserControls/Preflight/Crew/CrewAvailabilityGrid.ascx" TagPrefix="UC" TagName="CrewAvailabilityGrid" %>
<%@ Register Src="~/UserControls/Preflight/Crew/CrewSelection.ascx" TagPrefix="UC" TagName="CrewSelection" %>
<%@ Register Src="~/UserControls/Preflight/Crew/CrewSummary.ascx" TagPrefix="UC" TagName="CrewSummary" %>
<%@ Register Src="~/UserControls/UCPreflightFooterButton.ascx" TagName="Footer" TagPrefix="UCPreflight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadScriptBlock runat="server" ID="scriptBlock">          
        <script type="text/javascript">            
            function pageLoad() {
                function maxLength(field, maxChars) {
                    if (field.value.length >= maxChars) {
                        event.returnValue = false;
                        return false;
                    }
                }
            }
            function InitializeCrewObjectsVM() {
                var schemaElement = document.getElementById('<%=hdnPreflightCrewInitializationSchema.ClientID%>').value;
                var jsonSchema = JSON.parse(schemaElement);
                self.CurrentCrewHotel = ko.mapping.fromJS(jsonSchema.Hotel);
                self.CurrentCrewTransport = ko.mapping.fromJS(jsonSchema.Transport);

                ko.watch(self.CurrentCrewTransport, { depth: 1, tagFields: true }, function (parents, child, item) {
                    if (jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 && ((!IsNullOrEmpty(self.CurrentCrewTransport.DepartCode()) && !IsNullOrEmpty(self.CurrentCrewTransport.DepartName())) || (!IsNullOrEmpty(self.CurrentCrewTransport.ArrivalCode()) && !IsNullOrEmpty(self.CurrentCrewTransport.ArrivalName())))) {
                        setTimeout(function () {
                            if (!isAfterGetTransportCall && self.EditMode() == true) {
                                SaveCrewTransportation();
                            }
                        }, 1000);
                    }
                });
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%= Scripts.Render("~/bundles/preflightcrews") %>
<style type="text/css"> 

    .ui-jqgrid .ui-jqgrid-htable th div {
        height: auto !important;
        overflow: hidden;
        padding-right: 4px;
        padding-top:0px;
        position: relative;
        vertical-align: text-top;
        white-space: normal !important;
    }

    .jqgridTable tr td:first-child {
        text-align: center !important;
    }
    
</style>

    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false" OnClientClose="OnradCrewRosterPopupClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow1" runat="server" Height="800px" Width="1100px" ReloadOnShow="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCrewGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewGroupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewGroupPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMetroPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientMetroClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/MetroMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCtryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCtryClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx" OnClientClose="OnClientTransportClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTransportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTransportClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdArriveTransportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientArriveTransportClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewGroupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewGroupPopup.aspx">
            </telerik:RadWindow>
             <telerik:RadWindow ID="rdCrewPassport" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false" OnClientClose="OnClientVisaClose"
                 Title="Crew Passport Visa">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnTripSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdOutBoundInstructions" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightOutboundInstruction.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdChecklist" runat="server" VisibleOnPageLoad="false" Width="700px" Height="500px"
                Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightChecklist.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="990px" Height="590px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="UserListDialog" runat="server" Title="Editing record" Height="550px"
                Width="960px" ShowContentDuringLoad="false" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCrewPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" ReloadOnShow="false" AutoSize="true" Behaviors="Close"
                VisibleStatusbar="false" Title="Crew Roster">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" style="float: left; width: auto; overflow:auto;">
        <div class="loadingDiv" id="divloading">
        <div class="centerDiv">
            
        </div>        
    </div>
        
        <asp:HiddenField ID="hdnAssignPassLeg" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnCrewCode" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnPurpose" runat="server" />
        <asp:HiddenField ID="hdnNonLogTripSheet" runat="server" />
        <asp:HiddenField ID="hdnIsCrewAutoAvailability" runat="server" />
        <asp:HiddenField ID="hdnPreflightCrewInitializationSchema" runat="server" />
        <input type="hidden" id="hdnCtryID" data-bind="value: self.CurrentCrewHotel.CountryID"  />
        <input type="hidden" id="hdnMetroID" data-bind="value: self.CurrentCrewHotel.MetroID"  />
        <input type="hidden" id="hdnHotelID"  data-bind="value: self.CurrentCrewHotel.HotelID"  />
        <asp:HiddenField ID="hdnHotelAirID" runat="server" />
        <input type="hidden" id="hdnTransportID" data-bind="value: self.CurrentCrewTransport.PreflightDepartTransportId"  />
        <input type="hidden" id="hdnArriveTransportID" data-bind="value: self.CurrentCrewTransport.PreflightArrivalTransportId"  />
        
        <input type="hidden" id="hdnArrivalICao" data-bind="    value: self.CrewHotelArrivalAirportId"  />
        <input type="hidden" id="hdnDepartIcao" data-bind="value: self.CrewHotelDepartureAirportId" />
        <asp:HiddenField ID="hdnCrewSelectedTab" runat="server" />
        <asp:HiddenField ID="hdnCrewLogisticsSelectedTab" runat="server" />
        <div id="tabCrewInfo">
            <ul id="ulTabCrewInfo" class="tabStrip">
                <li id="crewInfo" class="tabStripItem tabStripItemSelected"  onclick="crewTabModuleChange('1');">Crew Info</li>
                <li id="crewLogistics" class="tabStripItem" onclick="crewTabModuleChange('2');">Crew Logistics</li>
            </ul>
        </div>
        <div id="pnlCrew" >
            
            <div runat="server" id="ChecklistOutboundTable" style="padding-top: 8px; padding-bottom: 5px">
                <asp:LinkButton CssClass="link_small" ID="lnkbtnChecklist" Text="Checklist" runat="server"
                    OnClientClick="javascript:openWinCrew('rdChecklist');return false;"></asp:LinkButton>
                |
                <asp:LinkButton CssClass="link_small" ID="lnkbtnOutbound" Text="Outbound Instructions"
                    runat="server" OnClientClick="javascript:openWinCrew('rdOutBoundInstructions');return false;"></asp:LinkButton>
            </div>
         
            <div class="preflight_crew_details" style="float: left; width: 718px; padding: 5px 0 0 0px;">
                <UC:CrewSelection runat="server" ID="CrewSelection" />
                <div style="text-align: center; float: left; width: 718px; padding: 5px 0  5px 0px;">
                    <img src="/App_Themes/Default/images/data_mover_up.png" onclick="ImgbtnSelectedCrewlistButtonClick();return false;" data-bind="enable: EditMode"  alt="Assign crew from crew availability" title="Assign crew from crew availability" />                                  
                </div>
            <%--    OnItemClick="pnlbarAvailableCrew_ItemClick"--%>
                <telerik:RadPanelBar ID="pnlbarAvailableCrew" Width="100%" CssClass="preflight_available_crew_list" ExpandAnimation-Type="None"
                    CollapseAnimation-Type="None" runat="server"  OnClientItemExpand="pnlCrewAvailabilityExpand" OnClientItemCollapse="pnlCrewAvailabilityCollapse" OnClientItemAnimationEnd="pnlCrewAvailabilityAnimationEnd"  >
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Crew Availability">
                            <Items>
                                <telerik:RadPanelItem>
                                  <ContentTemplate>
                                      <UC:CrewAvailabilityGrid runat="server" id="CrewAvailabilityGrid" />
                                  </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
                <div style="text-align: center; float: left; width: 718px; padding: 5px 0  5px 0px;">
                </div>
                <telerik:RadPanelBar ID="pnlCrewSummary" Width="100%" ExpandAnimation-Type="None"
                    CollapseAnimation-Type="None" runat="server"  OnClientItemExpand="LoadCrewSummaryExpand" >
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Crew Summary">
                            <Items>
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <UC:CrewSummary runat="server" id="CrewSummary" />
                                        <asp:HiddenField runat="server" ID="hdnDelete"></asp:HiddenField>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </div>
        </div>
        <div id="pnlCrewLogistics" style="display:none;">
            <div id="rtsLogistics" >
                <div class="ULTab-subTab">
                    <ul  class="tabmenu">
                        <li id="ulHotel" class="tabSelected" onclick="crewLogisticsModuleChange('1');">Hotel</li>
                        <li id="ulTransportation" onclick="crewLogisticsModuleChange('2');">Transportation</li>
                    </ul>
                </div>
            </div>
            <div id="RadMultiPage1" >
                <div id="rdPgeViewHotel">
                    <div id="divHotel">
                        <div class="ULTab-subTab">
                            <ul id="legTabhotel" data-bind="foreach: Legs" class="tabStrip">
                                <li class="tabStripItem" data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: CrewHotelLegtabClick }" >
                                    Leg <span data-bind="text: LegNUM"></span>(<span data-bind="    text: DepartureAirport.IcaoID"></span>-<span data-bind="    text: ArrivalAirport.IcaoID"></span>)
                                </li>
                            </ul>
                        </div>
                            
                            <table cellpadding="0" cellspacing="0" width="100%" class="preflight_crewhotel_grid">
                                <tr>
                                    <td>
                                        <table  id="CrewHotelListGrid" class="table table-striped table-hover table-bordered"></table>
                                    </td>
                                </tr>
                                <tr>                                                            
                                    <td>
                                        <!-- ko if: self.EditMode -->
                                        <div class="ui-state-default ui-jqgrid-pager ui-corner-bottom grid_icon" dir="ltr" style="width: 716px;border:1px solid #aaaaaa;border-width:0 1px 1px!important;padding:3px 0px 4px;">
                                            <div role="group" id="pg_gridPagerCrewHotelSelection">
                                                <a class="add-icon-grid" title="Add"  id="CrewHotelrecordAdd" data-bind="event: { click: CrewHotelAddClick }" ></a>
                                                <a id="CrewHotelrecordEdit" title="Edit" class="edit-icon-grid" data-bind="event: { click: CrewHotelEditClick }" ></a>
                                                <a id="CrewHotelrecordDelete" title="Delete" class="delete-icon-grid" data-bind="event: { click: CrewHotelDeleteClick }" ></a>
                                            </div>
                                            
                                            <div id="CrewHotelListGridpagesizebox">
                                            </div>
                                        </div>                                                                
                                        <!-- /ko -->
                                    </td>                                                            
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td style="display: none">
                                    </td>
                                    <td>
                                    <input type="radio" id="rdArrival" name="rbArriveDepart" onclick="return validateButtonList();" data-bind="    enable: self.EnableHotel, checked: CurrentCrewHotel.isArrivalHotel, checkedValue: true" />
                                    <label for="rdArrival">Arrival</label>
                                    <input type="radio" id="rdDeparture" name="rbArriveDepart" onclick="return validateButtonList();" data-bind="    enable: self.EnableHotel, checked: CurrentCrewHotel.isArrivalHotel, checkedValue: false"/>
                                    <label for="rdDeparture">Departure</label>
                                    </td>
                                    <td align="right">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="button" id="btnCancelHotel" value="Cancel Hotel" data-bind="enable: self.EnableHotel, css: HotelSaveIcon" onclick="    CancelCrewHotel()"/>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnSaveHotel" value="Save Hotel" data-bind="enable: self.EnableHotel, css: HotelSaveIcon" onclick="    SaveCrewHotel()"  />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div style="float: left; width: 713px; padding: 5px 5px 0 0; background: #f6f6f6;">
                                <input type="hidden" ID="hdnLegNumHotel" data-bind="value: self.CurrentCrewHotel.LegNUM" />
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td align="center">
                                        </td>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Status
                                                    </td>
                                                    <td>
                                                        <select id="ddlHotelCompleteds" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.Status">
                                                            <option value="Select">Select</option>
                                                            <option value="Required">Required</option>
                                                            <option value="Not Required">Not Required</option>
                                                            <option value="In Progress">In Progress</option>
                                                            <option value="Change">Change</option>
                                                            <option value="Canceled">Canceled</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="float: left; width: 718px; padding: 5px 0 0 0; background: #f6f6f6;">
                                <table width="100%">
                                    <tr>
                                        <td width="54%" valign="top">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" align="left" style="display: none">
                                                                </td>
                                                                <td align="left" colspan="3" style="display: none">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90 single-line" valign="top">
                                                                    Hotel Code
                                                                </td>
                                                                <td class="tdLabel90">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbHotelCode" maxlength="4" class="text50" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.HotelCode" onchange="return fireOnChange();" style="text-transform: uppercase;"/>
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnCode" onclick="openWinCrew('radHotelPopup'); return false;" data-bind="    enable: self.EnableHotel, css: BrowseBtnCrewHotel" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="left" class="tdLabel30">
                                                                    Rate
                                                                </td>
                                                                <td class="pr_radtextbox_75" align="left">
                                                                    <input type="text" pattern="[0-9]*" style="width:99px;  margin-left: -30px;" id="tbHotelRate" value="0.00" data-bind="enable: self.EnableHotel, CostCurrencyElement: CurrentCrewHotel.Rate" />                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:Label ID="lbHotelCode" runat="server" Visible="true"></asp:Label>
                                                                    <span id="lbcvHotelCode" class="alert-text" ></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90 single-line" valign="top">
                                                                    Hotel Name
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbHotelName" MaxLength="60" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.PreflightHotelName" style="text-transform: uppercase;"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Addr 1:
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbAddr1" MaxLength="100" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.Address1" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Addr 2:
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbAddr2" MaxLength="100" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.Address2" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Addr 3:
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbAddr3" MaxLength="100" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.Address3" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    City:
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbCity" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.CityName" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    State/Prov:
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbState" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.StateProvince" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    Country:
                                                                </td>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbCtry" class="text30"  onchange="return fireOnChangeCountry();" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.CountryCode" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnCtry" onclick="openWinCrew('rdCtryPopup'); return false;"  data-bind="    enable: self.EnableHotel, css: BrowseBtnCrewHotel" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>                                                               
                                                                </td>
                                                                <td class="tdLabel60">
                                                                    Metro:
                                                                </td>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbMetro" maxlength="3" class="text30"  onchange="return fireOnChangeMetro();"  data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.MetroCD" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnMetro" onclick="openWinCrew('rdMetroPopup'); return false;" class="browse-button" data-bind="    enable: self.EnableHotel, css: BrowseBtnCrewHotel"/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <span id="lbcvCtry" class="alert-text"></span>
                                                                </td>
                                                                <td colspan="2">
                                                                    <span id="lbcvMetro" class="alert-text"></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90 single-line" valign="top">
                                                                    Postal Code:
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbPost" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.PostalCode" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Phone
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbHotelPhone" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.PhoneNum1" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel90" valign="top">
                                                                    Fax
                                                                </td>
                                                                <td colspan="3">
                                                                    <input type="text" id="tbHotelFax" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.FaxNUM" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" cellspacing="0" cellpadding="2" width="100%">
                                                            <tr>
                                                                <td class="tdLabel100" colspan="2">
                                                                    <input type="checkbox" id="chkBookNightPrior" data-bind="enable: self.EnableHotel, checked: CurrentCrewHotel.IsBookNight, checkedValue: true"/>
                                                                    <label for="chkBookNightPrior">Book Night Prior</label>
                                                                </td>
                                                                <td align="left" colspan="2">
                                                                    <input type="checkbox" id="chkEarlyCheckin"  data-bind="enable: self.EnableHotel, checked: CurrentCrewHotel.IsEarlyCheckIn, checkedValue: true"/>
                                                                    <label for="chkEarlyCheckin">Early Check-in</label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel80" valign="top">Date In</td>
                                                                <td class="tdLabel80"><input id="tbDateIn" type="text" data-bind="enable: self.EnableHotel, datepicker: CurrentCrewHotel.ClientDateIn" class="textDatePicker" /></td>
                                                                <td valign="top">No. of Rooms</td>
                                                                <td valign="top"><input id="tbNoforooms" type="text" maxlength="2" class="text50" onKeyPress="return fnAllowNumeric(this, event)" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.NoofRooms" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date Out</td>
                                                                <td class="tdLabel80"><input id="tbDateOut" type="text" data-bind="enable: self.EnableHotel, datepicker: CurrentCrewHotel.ClientDateOut"  class="textDatePicker" /></td>
                                                                <td valign="top">No. of Beds</td>
                                                                <td>
                                                                    <input id="tbNoofBeds" type="text" maxlength="3" class="text50" onKeyPress="return fnAllowNumeric(this, event)" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.NoofBeds" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="single-line" valign="top">
                                                                    Type of Room
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <select id="ddltypeofRoomCrewHotel" data-bind="enable: self.EnableHotel, options: self.CrewHotelRooms, optionsValue: 'RoomTypeID', optionsText: 'RoomDescription', value: CurrentCrewHotel.RoomTypeID" class="aspNetDisabled text170">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="single-line" valign="top">
                                                                    Room Desc.
                                                                </td>
                                                                <td colspan="3">
                                                                    <select id="ddlRoomDesc" data-bind="enable: self.EnableHotel, options: self.RoomDescription, optionsValue: 'RoomDescCode', optionsText: 'RoomDescription', value: CurrentCrewHotel.RoomDescription" class="aspNetDisabled text170">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <input type="checkbox" id="chkRequestOnly" data-bind="enable: self.EnableHotel, checked: CurrentCrewHotel.IsRequestOnly, checkedValue: true">
                                                                    <label for="chkRequestOnly">Request Only</label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <input type="checkbox" id="chkSmoking" data-bind="enable: self.EnableHotel, checked: CurrentCrewHotel.IsSmoking, checkedValue: true" />
                                                                    <label for="chkSmoking">Smoking</label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="horizontalLine">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Club Card
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <input id="tbClubCard" MaxLength="100" type="text" class="text228" data-bind="enable: self.EnableHotel, value: CurrentCrewHotel.ClubCard" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Max. Cap Amt
                                                                </td>
                                                                <td align="left" class="pr_radtextbox_100">
                                                                    <span style="width: 160px;" class="riSingle RadInput RadInput_Default" id="ctl00_ctl00_MainContent_SettingBodyContent_tbMaxcapamt_wrapper">
                                                                        <input id="tbMaxcapamt" type="text" MaxLength="10" style="width:90%;text-align: right" value="0.00"
                                                                              data-bind="enable: self.EnableHotel, formatCurrency: CurrentCrewHotel.MaxCapAmount, event: { change: checkMaxcap }" 
                                                                              onfocus="focusMaxcap()" onblur="blurMaxcap()" onkeypress="return fnAllowNumericAndChar(this, event,'.')" />
                                                                    </span>
                                                                </td>
                                                                <td align="left" colspan="2">
                                                                    <input type="checkbox" id="chkRatecap"  data-bind="enable: self.EnableHotel, checked: CurrentCrewHotel.IsRateCap, checkedValue:true">
                                                                    <label for="chkRatecap">Rate Cap</label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="50%" valign="top">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table class="border-box" width="100%" style="height: 391px;">
                                                            <tr>
                                                                <td valign="top">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="checkbox" id="chkNoCrew" data-bind="enable: self.EnableHotel"/>
                                                                                <label for="chkNoCrew">No Crew</label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> 
                                                                                <table id="dgCrewLegHotelSelectionGrid" class="table table-striped table-hover table-bordered jqgridTable"></table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="border-box crewconfirmation_comment" width="100%" >
                                                            <tr>
                                                                <td valign="bottom" colspan="4">
                                                                    Confirmation
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pr_radtextbox_320x40" colspan="4">
                                                                    <span class="riSingle RadInput RadInput_Default" style="width:160px;">
                                                                    <textarea id="tbConfirmation" rows="1" cols="20" class="riTextBox"  data-bind="enable: self.EnableHotel, textinput: CurrentCrewHotel.ConfirmationStatus" >
                                                                    </textarea>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="bottom" colspan="4">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pr_radtextbox_320x40" colspan="4">
                                                                    <span class="riSingle RadInput RadInput_Default" style="width:160px;">
                                                                    <textarea id="tbComments" rows="1" cols="20" class="riTextBox"  data-bind="enable: self.EnableHotel, textinput: CurrentCrewHotel.Comments">
                                                                    </textarea>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    </div>
                </div>
                <div id="radPgeViewTransport" style="display:none;" >
        <input type="hidden" id="hdnTransAirID" data-bind="value: self.CurrentCrewTransport.DepartureAirportId"/>
        <input type="hidden" id="hdnArriveTransAirID" data-bind="value: self.CurrentCrewTransport.ArrivalAirportId"/>
                        <div class="ULTab-subTab">
                            <ul id="legTabTransport" data-bind="foreach: Legs" class="tabStrip">
                                <li data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: CrewTransportLegtabClick }" class="tabStripItem">
                                    Leg <span data-bind="text: LegNUM"></span>(<span data-bind="    text: DepartureAirport.IcaoID"></span>-<span data-bind="text: ArrivalAirport.IcaoID"></span>)
                                </li>
                            </ul>
                        </div>
                        <div style="float: left; width: 718px; padding: 5px 0 0 0; background: #f6f6f6;">
                            <div style="float: left; width: 353px; padding: 5px 5px 5px 0px;">
                                <input type="hidden" id="hdnLegNumTransport" data-bind="enable: self.EditMode, value: CurrentCrewTransport.LegNUM"/>
                                <fieldset>
                                    <legend>Depart Transportation</legend>
                                    <table cellpadding="1" cellspacing="2">
                                        <tr>
                                            <td>
                                                Status
                                            </td>
                                            <td colspan="4">
                                                <asp:CheckBox ID="chkTransCompleted" runat="server" Text="Completed" Visible="false" />
                                                    <select id="ddlDepartTransportCompleteds" data-bind="enable: self.EditMode, value: CurrentCrewTransport.SelectedDepartTransportStatus"  >
                                                        <option value="Select" Selected="selected">Select</option>
                                                        <option value="Required">Required</option>
                                                        <option value="Not Required">Not Required</option>
                                                        <option value="In Progress">In Progress</option>
                                                        <option value="Change">Change</option>
                                                        <option value="Canceled">Canceled</option>
                                                        <option value="Completed">Completed</option>
                                                    </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Code
                                            </td>
                                            <td class="tdLabel50" colspan="2">
                                                <input type="text" id="tbTransCode" class="text50" onchange="return fireOnChangeTransCode();" data-bind="enable: self.EditMode, value: CurrentCrewTransport.DepartCode"/>
                                                <input id="btnTransport" type="button" onclick="openWinCrew('rdTransportPopup'); return false;"  data-bind="css: BrowseBtn"  />
                                            </td>
                                            <td class="tdLabel60" align="center">
                                                Rate
                                            </td>
                                            <td class="pr_radtextbox_55" align="left">
                                                <input pattern="[0-9]*" type="text" id="tbTransRate" maxlength="6" text="0.00"  style="width: 49px;margin-left: -33px;margin-right: -7px;" data-bind="enable: self.EditMode, transportRateElement: CurrentCrewTransport.DepartRate"/>                                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td align="left" colspan="4">
                                                <span id="lbcvTransCode" class="alert-text" style="color:#444444;font-family:Arial;font-size:12px;"></span>
                                                <asp:Label ID="lbTransCode" runat="server" Visible="true" EnableViewState="False" ViewStateMode="Disabled"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="tbTransName" MaxLength="60" class="text228"  data-bind="enable: self.EditMode, value: CurrentCrewTransport.DepartName"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Phone
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="tbTransPhone" MaxLength="25" class="text228" onKeyPress="return fnAllowPhoneFormat(this,event)" data-bind="enable: self.EditMode, value: CurrentCrewTransport.DepartPhone"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Fax
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="tbTransFax" MaxLength="25" class="text228" onKeyPress="return fnAllowPhoneFormat(this,event)" data-bind="enable: self.EditMode, value: CurrentCrewTransport.DepartFax"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Confirmation
                                            </td>
                                            <td colspan="4" class="pr_radtextbox_226">
                                                <textarea id="tbTransConfirmation"  style="width:222px" cols="20" rows="2" data-bind="enable: self.EditMode, value: CurrentCrewTransport.DepartConfirmation">
                                                 </textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Comments
                                            </td>
                                            <td colspan="4" class="pr_radtextbox_226">
                                                <textarea id="tbTransComments" style="width:222px" cols="20" rows="2" data-bind="enable: self.EditMode, value: CurrentCrewTransport.DepartComments">
                                                </textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            <div style="float: left; width: 353px; padding: 5px 5px 5px 0px;">
                                <fieldset>
                                    <legend>Arrive Transportation</legend>
                                    <table cellpadding="1" cellspacing="2">
                                        <tr>
                                            <td>
                                                Status
                                            </td>
                                            <td colspan="4">
                                                    <select id="ddlArriveTransportCompleteds" data-bind="enable: self.EditMode, value: CurrentCrewTransport.SelectedArrivalTransportStatus" >
                                                        <option value="Select" Selected="selected">Select</option>
                                                        <option value="Required">Required</option>
                                                        <option value="Not Required">Not Required</option>
                                                        <option value="In Progress">In Progress</option>
                                                        <option value="Change">Change</option>
                                                        <option value="Canceled">Canceled</option>
                                                        <option value="Completed">Completed</option>
                                                    </select>
                                                <asp:CheckBox ID="chkArriveCompleted" runat="server" Text="Completed" Visible="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Code
                                            </td>
                                            <td class="tdLabel60" colspan="2">
                                                <input type="text" id="tbArriveTransCode" class="text50"  onchange="return fireOnChangeArriveTransCode();" data-bind="enable: self.EditMode, value: CurrentCrewTransport.ArrivalCode" />
                                                <input type="button" id="btnArriveTrans" onclick="openWinCrew('rdArriveTransportPopup'); return false;"  data-bind="css: BrowseBtn"  />
                                            </td>
                                            <td class="tdLabel50" align="center">
                                                Rate
                                            </td>
                                            <td class="pr_radtextbox_55">                                                
                                                <input type="text" pattern="[0-9]*" style="width: 49px;margin-left: -24px;margin-right: -12px;" id="tbArriveRate" maxlength="6" data-bind="enable: self.EditMode, transportRateElement: CurrentCrewTransport.ArrivalRate" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td align="left" colspan="4">
                                                <span id="lbArriveTransCode" style="color:#444444;font-family:Arial;font-size:12px;" ></span>
                                                <span id="lbcvArriveTransCode" class="alert-text" style="color:#444444;font-family:Arial;font-size:12px;"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="tbArriveTransName" MaxLength="60" class="text228" data-bind="enable: self.EditMode, value: CurrentCrewTransport.ArrivalName" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Phone
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="tbArrivePhone" MaxLength="25" class="text228" onKeyPress="return fnAllowPhoneFormat(this,event)"  data-bind="enable: self.EditMode, value: CurrentCrewTransport.ArrivalPhone" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Fax
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="tbArriveFax" MaxLength="25" class="text228" onKeyPress="return fnAllowPhoneFormat(this,event)"  data-bind="enable: self.EditMode, value: CurrentCrewTransport.ArrivalFax" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Confirmation
                                            </td>
                                            <td colspan="4" class="pr_radtextbox_226">
                                                <textarea id="tbArriveConfirmation" style="width:222px" cols="20" rows="2" data-bind="enable: self.EditMode, value: CurrentCrewTransport.ArrivalConfirmation">
                                                </textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                Comments
                                            </td>
                                            <td colspan="4" class="pr_radtextbox_226">
                                                <textarea id="tbArriveComments" style="width:222px" cols="20" rows="2" data-bind="enable: self.EditMode, value: CurrentCrewTransport.ArrivalComments">
                                                </textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                  </div>
            </div>
        </div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right" class="padright_10" style="padding: 11px 0 7px;">
                    <asp:HiddenField ID="hdnLeg" runat="server" />
                        <UCPreflight:Footer ID="PreflightFooter" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
