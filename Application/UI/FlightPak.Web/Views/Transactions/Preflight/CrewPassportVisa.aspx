﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewPassportVisa.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.CrewPassportVisa" %>
<%@ Import Namespace="FlightPak.Web.Views.Transactions.Preflight" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/knockout/knockout.js"></script>
    <script type="text/javascript" src="/Scripts/knockout/knockout.mapping.js"></script>
    <script type="text/javascript" src="/Scripts/knockout/moment.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/Preflight/CrewPassportVisa.js"></script>
    <script type="text/javascript">
        var jqgridPassportId = '#dgPassport';
        var jqgridVisaId = '#dgVisa';
        var CrewID = "";
        var leg = "";
        var crewName = "";

        $(document).ready(function () {

            CrewID = $.trim(decodeURI(getQuerystring("CrewID", "")));
            leg = $.trim(decodeURI(getQuerystring("Leg", "")));
            crewName = $.trim(decodeURI(getQuerystring("CrewName", "")));
            $(document).prop('title', 'Crew Passport Visa' + " - " + crewName);
            crewPassportVisaViewModel = new CrewPassportVisaViewModel(CrewID);         
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" id="hdApplicationDateFormat" value="<%=PreflightTripManager.getUserPrincipal()._ApplicationDateFormat.ToUpper() %>"/>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
               var oArg = new Object();
               var selectedrows = $('#dgPassport').jqGrid('getGridParam', 'selrow');
               if (selectedrows.length > 0) {
                   var rowData = $('#dgPassport').getRowData(selectedrows);
                   oArg.crewID = rowData['CrewID'];
                   oArg.passportNum = rowData['PassportNum'];
                   oArg.PassportID = rowData['PassportID'];
               }
               else {
                   oArg.crewID = "";
                   oArg.passportNum = "";
                   oArg.PassportID = "";
               }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            } 
        
            function openWin(radWin) {
                var url = '';
                if (radWin == "radCrewRosterCRUDPopup") {
                    url = '/Views/Settings/People/CrewRoster.aspx?IsPopup=&IsPassport=true&CrewName=' + $.trim(decodeURI(getQuerystring("CrewName", "")));
                    var oWnd = openWin2(url, radWin);
                }
            }

        </script>

    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table id="dgPassport" style="width: 850px !important;" class="table table-striped table-hover table-bordered"></table>
                    <div class="grid_icon">                                
                          <input type="button" id="lnkInitEdit" class="edit-icon-grid" onclick="    javascript: openWin('radCrewRosterCRUDPopup'); return false;" />                             
                    </div>
                    <div style="padding: 5px 5px; text-align: right;">
                        <input type="button" id="btnSubmitButton" class="button" value="OK" onclick="    returnToParent(); return false;" />                        
                         <%--<asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert" CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>--%>
                    </div>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="nav-space">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="dgVisa" style="width: 850px !important;" class="table table-striped table-hover table-bordered"></table>
                    <div class="grid_icon">  
                         <input type="button" id="LinkButton1" class="edit-icon-grid" onclick="    javascript: openWin('radCrewRosterCRUDPopup'); return false;" />                               
                    </div>
                    <div style="padding: 5px 5px; text-align: right;">
                         <input type="button" id="btnVisaSubmit" class="button"  value="OK" onclick="    returnToParent(); return false;" />                   
                        <%-- <asp:LinkButton ID="LinkButton2" runat="server" ToolTip="Add" CommandName="InitInsert" CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>--%>
                    </div>
                    <br />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
