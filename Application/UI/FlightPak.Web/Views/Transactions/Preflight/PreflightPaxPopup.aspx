﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightPaxPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightPaxPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Passenger</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgPreflightPax.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPreflightPax.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorCD");
                        if (i == 0) {

                            oArg.PassengerRequestorCD = cell1.innerHTML + ",";
                        }

                        else {
                            oArg.PassengerRequestorCD += cell1.innerHTML + ",";

                        }
                    }
                }
                else {
                    oArg.PassengerRequestorCD = "";
                }
                if (oArg.PassengerRequestorCD != "")
                    oArg.PassengerRequestorCD = oArg.PassengerRequestorCD.substring(0, oArg.PassengerRequestorCD.length - 1)
                else
                    oArg.PassengerRequestorCD = "";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgPreflightPax">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPreflightPax" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgPreflightPax" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgPreflightPax" runat="server" AllowMultiRowSelection="true"
            OnItemCreated="dgPreflightPax_ItemCreated" OnNeedDataSource="dgPreflightRetrieve_BindData"
            AllowSorting="true" AutoGenerateColumns="false" Height="330px" PageSize="10"
            AllowPaging="true" Width="500px" OnItemCommand="dgPreflightPax_ItemCommand" OnPreRender="dgPreflightPax_PreRender">
            <MasterTableView DataKeyNames="PassengerID,FirstName,MiddleInitial,LastName,PassengerRequestorCD,LegID"
                CommandItemDisplay="Bottom">
                <Columns>
                    
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" UniqueName="FirstName"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="140px" FilterControlWidth="120px" Display="true" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MiddleInitial" HeaderText="Middle Name" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="140px"
                        FilterControlWidth="120px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="140px"
                        FilterControlWidth="120px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="PAX Code" UniqueName="PassengerRequestorCD"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="80px" FilterControlWidth="60px" Display="true" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LegID" HeaderText="LegID" UniqueName="LegID"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PassengerID" HeaderText="PassengerID" UniqueName="PassengerID"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                        Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grd_ok">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                    <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                        visible="false">
                        Use CTRL key to multi select</div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
        </telerik:RadGrid>
        <asp:HiddenField ID="hdName" runat="server" />
    </div>
    </form>
</body>
</html>
