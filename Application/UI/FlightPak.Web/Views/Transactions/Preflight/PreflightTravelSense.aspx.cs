﻿using System;
using FlightPak.Web.PreflightService;
using System.IO;
using System.Text;
using System.Globalization;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Framework.Constants;
using System.Collections.Generic;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightTravelSense : BaseSecuredPage
    {
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightTripViewModel Trip = (PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip];
//                        PreflightMain Trip = pfViewModel.PreflightMain;


                        string tripNum = string.Empty;
                        string Numlegs = string.Empty;
                        string Descr = string.Empty;
                        string Aircraft = string.Empty;
                        string depart = string.Empty;
                        string departAirport = string.Empty;
                        string arrive = string.Empty;
                        string arriveAirport = string.Empty;
                        string DDate = string.Empty;
                        string DTime = string.Empty;
                        string ADate = string.Empty;
                        string ATime = string.Empty;
                        string Numpax = string.Empty;
                        string Crew = string.Empty;
                        string Refuel = "0";
                        string ETE = string.Empty;

                        if (Trip != null)
                        {
                            if (Trip.PreflightMain.TripNUM != null)
                                tripNum = Trip.PreflightMain.TripNUM.ToString();
                            Numlegs = Trip.PreflightLegs.Count.ToString();
                            if (Trip.PreflightMain.TripDescription != null)
                                Descr = Trip.PreflightMain.TripDescription;
                            if (Trip.PreflightMain.Fleet != null)
                            {
                                if (Trip.PreflightMain.Fleet.TailNum != null)
                                    Aircraft = Trip.PreflightMain.Fleet.TailNum;
                            }

                            Response.ContentType = "text/plain";
                            //veracode fix for 2992
                            //Response.AddHeader("Content-Disposition", "attachment; filename=t$tr" + tripNum + ".t$i");
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode("tStr" + tripNum + ".tSi"));

                            using (StreamWriter writer = new StreamWriter(Response.OutputStream, Encoding.UTF8))
                            {
                                writer.WriteLine("t$tr" + System.Web.HttpUtility.HtmlEncode(tripNum));
                                writer.WriteLine("[TRIP]");
                                writer.WriteLine("Numlegs=" + System.Web.HttpUtility.HtmlEncode(Numlegs));
                                writer.WriteLine("Descr=" + System.Web.HttpUtility.HtmlEncode(Descr));
                                writer.WriteLine("Aircraft=" + System.Web.HttpUtility.HtmlEncode(Aircraft));
                                writer.WriteLine("Crew=" + "");
                                writer.WriteLine("Cost=" + "");
                                writer.WriteLine("");

                                if (Trip.PreflightLegs != null)
                                {
                                    foreach (PreflightLegViewModel leg in Trip.PreflightLegs)
                                    {
                                        if (leg.DepartureAirport != null)
                                        {
                                            if (leg.DepartureAirport.IcaoID != null)
                                                depart = leg.DepartureAirport.IcaoID.ToString();
                                            if (leg.DepartureAirport.AirportName != null)
                                                departAirport = leg.DepartureAirport.AirportName.ToString();
                                        }
                                        if (leg.ArrivalAirport != null)
                                        {
                                            if (leg.ArrivalAirport.IcaoID != null)
                                                arrive = leg.ArrivalAirport.IcaoID.ToString();
                                            if (leg.ArrivalAirport.AirportName != null)
                                                arriveAirport = leg.ArrivalAirport.AirportName.ToString();
                                        }

                                        if (leg.DepartureDTTMLocal != null)
                                        {
                                            DateTime dt = (DateTime)leg.DepartureDTTMLocal;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            DDate = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", leg.DepartureDTTMLocal);
                                            DTime = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        if (leg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime dt = (DateTime)leg.ArrivalDTTMLocal;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            ADate = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", leg.ArrivalDTTMLocal);
                                            ATime = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }

                                        writer.WriteLine("[" + "LEG" + System.Web.HttpUtility.HtmlEncode(leg.LegNUM) + "]");
                                        writer.WriteLine("From=" + System.Web.HttpUtility.HtmlEncode(depart));
                                        writer.WriteLine("FromName=" + System.Web.HttpUtility.HtmlEncode(departAirport));
                                        writer.WriteLine("To=" + System.Web.HttpUtility.HtmlEncode(arrive));
                                        writer.WriteLine("ToName=" + System.Web.HttpUtility.HtmlEncode(arriveAirport));
                                        writer.WriteLine("DDate=" + System.Web.HttpUtility.HtmlEncode(DDate));
                                        writer.WriteLine("DTime=" + System.Web.HttpUtility.HtmlEncode(DTime));
                                        writer.WriteLine("ADate=" + System.Web.HttpUtility.HtmlEncode(ADate));
                                        writer.WriteLine("ATime=" + System.Web.HttpUtility.HtmlEncode(ATime));
                                        Numpax = "0";
                                        
                                        List<PassengerViewModel> preflightLegPassengerList = Trip.PreflightLegPassengers[leg.LegNUM.Value.ToString()];
                                        if (preflightLegPassengerList.Count != 0)
                                        {

                                            string paxCode = string.Empty;
                                            string paxID = string.Empty;
                                            string travelsense = string.Empty;
                                            Numpax = preflightLegPassengerList.Count.ToString();
                                            writer.WriteLine("Numpax=" + Numpax);
                                            int idval = 1;
                                            foreach (PassengerViewModel pax in preflightLegPassengerList)
                                            {
                                                writer.WriteLine("Pax" + idval + "= " + System.Web.HttpUtility.HtmlEncode(pax.PassengerFirstName));
                                                //paxCode = pax.PassengerFirstName;
                                                paxID = pax.PassengerID.ToString();

                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(paxID));
                                                    if (objPaxRetVal.ReturnFlag == true)
                                                    {
                                                        writer.WriteLine("PaxId" + System.Web.HttpUtility.HtmlEncode(idval) + "= " + System.Web.HttpUtility.HtmlEncode(objPaxRetVal.EntityList[0].SSN));
                                                        //travelsense = objPaxRetVal.EntityList[0].SSN.ToString();
                                                    }
                                                }
                                                idval++;
                                            }
                                        }
                                        else
                                        {
                                            writer.WriteLine("Numpax=" + System.Web.HttpUtility.HtmlEncode(Numpax));
                                        }

                                        List<PreflightLegCrewViewModel> preflightCrewList = Trip.PreflightLegCrews[leg.LegNUM.Value.ToString()];
                                        if (preflightCrewList.Count != 0)
                                        {
                                            Crew = preflightCrewList.Count.ToString();
                                        }

                                        //if (leg.Airport != null)
                                        //    Refuel = leg.Airport.ToString();
                                        if (leg.ElapseTM != null)
                                            ETE = leg.ElapseTM.ToString();



                                        writer.WriteLine("Crew=" + System.Web.HttpUtility.HtmlEncode(Crew));
                                        writer.WriteLine("Refuel=" + System.Web.HttpUtility.HtmlEncode(Refuel));
                                        writer.WriteLine("ETE=" + System.Web.HttpUtility.HtmlEncode(ETE));
                                        writer.WriteLine("");
                                    }
                                }
                            }
                            Response.End();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
    }
}