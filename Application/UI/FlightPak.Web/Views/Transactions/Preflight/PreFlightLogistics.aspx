﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Preflight.Master"
    AutoEventWireup="true" CodeBehind="PreFlightLogistics.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreFlightLogistics" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Preflight.Master" %>
<%@ Register Src="~/UserControls/UCPreflightFooterButton.ascx" TagName="Footer" TagPrefix="UCPreflight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%=Scripts.Render("~/bundles/preflightLogistics") %>
        <script type="text/javascript">


            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

           

        </script>
    </telerik:RadCodeBlock>    
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadFBOCodePopupDepart" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientFBOClose1" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadFBOCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    OnClientClose="OnClientFBOClose2"
                    NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCateringPopupDepart" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCaterClose1" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCateringPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCaterClose2"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Logistics/CaterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCateringCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Logistics/CaterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnTripSearchClose"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdOutBoundInstructions" runat="server" OnClientResizeEnd="GetDimensions"
                    VisibleOnPageLoad="false" AutoSize="true" Modal="true" BackColor="#DADADA" VisibleStatusbar="false"
                    Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/PreflightOutboundInstruction.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdChecklist" runat="server" OnClientResizeEnd="GetDimensions" Width="700px" Height="500px" 
                    VisibleOnPageLoad="false" Modal="true" BackColor="#DADADA" VisibleStatusbar="false"
                    Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/PreflightChecklist.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="false"
                    Width="620px" Height="600px" KeepInScreenBounds="true" Modal="true" Behaviors="Close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Width="990px" Height="590px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                    VisibleStatusbar="false" Title="Airport Information">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
                </telerik:RadWindow>
            </Windows>
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
            <AlertTemplate>
                <div class="rwDialogPopup radalert">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                        </a>
                    </div>
                </div>
            </AlertTemplate>
        </telerik:RadWindowManager>
        
        
        <asp:HiddenField runat="server" ID="hdnPreflightLogisticsInitializationObject" ClientIDMode="Static" Value="" />
        <div id="pnlPreFlightlogistics" >
            <input type="hidden" id="hdDeptFBOAirID" data-bind="value: Logistics.PreflightLeg.DepartICAOID" />
            <input type="hidden" id="hdArrFBOAirID" data-bind="value: Logistics.PreflightLeg.ArriveICAOID" />
            <input type="hidden" id="hdDeptCaterAirID" data-bind="value: Logistics.PreflightLeg.DepartICAOID" />
            <input type="hidden" id="hdArrCaterAirID" data-bind="value: Logistics.PreflightLeg.ArriveICAOID" />
            <input type="hidden" id="hdDepartIcaoID" data-bind="value: Logistics.PreflightLeg.DepartICAOID" />
            <input type="hidden" id="hdArriveIcaoID" data-bind="value: Logistics.PreflightLeg.ArriveICAOID" />
            <input type="hidden" value="1" id="hdnSelectedLegNum" />

            <div runat="server" id="ChecklistOutboundTable" style="padding-top: 8px; padding-bottom: 5px">
                <a class="link_small" id="lnkbtnChecklist"
                    onclick="javascript:openWin('rdChecklist');return false;">Checklist</a>                |
                <a class="link_small" id="lnkbtnOutbound" onclick="javascript:openWin('rdOutBoundInstructions');return false;">Outbound Instructions</a>
            </div>

            
            <ul id="legTabLogistics" data-bind="foreach: Legs" class="tabStrip">
                <li class="tabStripItem" data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: LegtabClick }">Leg <span data-bind="    text: LegNUM"></span>(<span data-bind="    text: DepartureAirport.IcaoID"></span>-<span data-bind="    text: ArrivalAirport.IcaoID"></span>)</li>
            </ul>
            
            <table width="100%" cellpadding="0" cellspacing="0" class="nav_bg_preflight">
                <tr>
                    <td class="tdLabel70" align="left">Depart Date
                    </td>
                    <td class="tdLabel140" align="left">
                        <label id="lblDepartDate" data-bind="text: self.DepartLegDateLocal"></label>
                        <label id="lblDepartTime" data-bind=" text: Logistics.PreflightLeg.DepartureLocalTime"></label>
                    </td>
                    <td class="tdLabel80" align="left">Depart ICAO:
                    </td>
                    <td align="left">
                        <label id="lblDepartInfo"></label>
                        <label id="lblIcao" data-bind="text: Logistics.PreflightLeg.DepartureICAO"></label>
                        <label id="lblairportName" data-bind="text: Logistics.PreflightLeg.DepartureAirport.AirportName"></label>
                        <label id="lblcityName" data-bind="text: Logistics.PreflightLeg.DepartureAirport.CityName"></label>
                        <label id="lblstateName" data-bind="text: Logistics.PreflightLeg.DepartureAirport.StateName"></label>
                        <label id="lblcountryName" data-bind="text: Logistics.PreflightLeg.DepartureAirport.CountryName"></label>
                    </td>
                </tr>
                <tr>
                    <td align="left">Arrival Date
                    </td>
                    <td align="left">
                        <label id="lblArrivalDate" data-bind="text: self.ArrivalLegDateLocal"></label>
                        <label id="lblArrivalTime" data-bind=" text: Logistics.PreflightLeg.ArrivalLocalTime"></label>
                    </td>
                    <td align="left">Arrival ICAO:
                    </td>
                    <td align="left">
                        <label id="lblArrivalInfo"></label>
                        <label id="lblArrIcao" data-bind="text: Logistics.PreflightLeg.ArrivaICAO"></label>
                        <label id="lblArrairportName" data-bind="text: Logistics.PreflightLeg.ArrivalAirport.AirportName"></label>
                        <label id="lblArrcityName" data-bind="text: Logistics.PreflightLeg.ArrivalAirport.CityName"></label>
                        <label id="lblArrstateName" data-bind="text: Logistics.PreflightLeg.ArrivalAirport.StateName"></label>
                        <label id="lblArrcountryName" data-bind="text: Logistics.PreflightLeg.ArrivalAirport.CountryName"></label>
                    </td>
                </tr>
            </table>
            
            <a name="FBO"></a>
            <div class="preflight_logistic_handler" style="float: left; width: 718px; padding: 5px 0 0 0px;">
                <telerik:RadPanelBar ID="pnlFBOHandler" Width="100%" ExpandAnimation-Type="None"
                    CollapseAnimation-Type="None" runat="server" OnClientItemExpand="pnlFBOHandlerExpand"
                    OnClientItemCollapse="pnlFBOHandlerCollapse">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="true" Text="FBO/Handler" CssClass="PanelHeaderStyle">
                            <ContentTemplate>
                                <div class="logistic_fieldset_wrapper">
                                    <div class="parts_sections">
                                        <input type="checkbox" id="chkNoUpdate" text="No Update" style="display: none;" data-bind="checked: Logistics.NoUpdated" />
                                        <fieldset>
                                            <legend>Depart-<label id="lblDepartFBOICAO" data-bind="text: Logistics.PreflightLeg.DepartureAirport.IcaoID"></label></legend>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td colspan="2" align="left" style="padding-left: 3px;">
                                                        
                                                    </td>
                                                    <td>Status
                                                    </td>
                                                    <td>
                                                        <select id="ddlDepartFBOCompleteds" data-bind="enable: self.EditMode, value: Logistics.PreflightDepFboListViewModel.Status, event: { change: FBOCommentOrConfirmationChange }">
                                                            <option value="0" selected="selected"><-Select-></option>
                                                            <option value="Required">Required</option>
                                                            <option value="Not Required">Not Required</option>
                                                            <option value="In Progress">In Progress</option>
                                                            <option value="Change">Change</option>
                                                            <option value="Canceled">Canceled</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="nav-space">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Code
                                                    </td>
                                                    <td class="tdLabel110" align="left">
                                                        <table>
                                                            <tr>
                                                                <td><input type="text" id="tbDepartFBOCode" class="text60"
                                                            maxlength="4"
                                                            onkeypress="return fnAllowAlphaNumeric(this, event)" 
                                                            onblur="return RemoveSpecialChars(this)"
                                                            data-bind="enable: self.EditMode, value: Logistics.PreflightDepFboListViewModel.FBOCD, event: { change: DepartFBOChange }">
                                                        </input></td>
                                                                <td><input type="hidden" id="hdDepartFBOID" data-bind="value: Logistics.PreflightDepFboListViewModel.FBOID" /></td>
                                                                <td><input type="button" id="btnDepartFBOCode" onclick="javascript: openFBOWin('DEPART'); return false;" class="browse-button" data-bind="    enable: self.EditMode" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="tdLabel40" align="left">ICAO
                                                    </td>
                                                    <td align="left" class="icao_text">
                                                        <input id="tbDepartFBOICAO" maxlength="4" class="inpt_non_edit text40" data-bind="enable: self.EditMode, value: Logistics.PreflightLeg.DepartureAirport.IcaoID">
                                                        </input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel40" align="left"></td>
                                                    <td colspan="3" align="left">
                                                        <label id="lbcvDepartFBOCode" class="alert-text"></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">FBO Info
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div id="div1" class="pr_fbo_box" runat="server">
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartName">Name:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartName" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.PreflightFBOName"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartAddr1">Addr 1:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartAddr1" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.Addr1"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartAddr2">Addr 2:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartAddr2" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.Addr2"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartAddr3">Addr 3:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartAddr3" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.Addr3"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartPhone1">Phone 1:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartPhone1" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.PhoneNum1"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartPhone2">Phone 2:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartPhone2" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.PhoneNum2"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartFax">Fax:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartFax" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.FaxNUM"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartCity">City:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartCity" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.CityName"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartState">State:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartState" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.StateName"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblDepartEmail">E-mail:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartEmail" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.EmailAddress"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel95">
                                                                    <label id="lblDepartZipPostalCode">Zip/Postal Code:</label></span>
                                                                <span>
                                                                    <label id="lblInputDepartZipPostalCode" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.PostalZipCD"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblUnicom">UNICOM:</label></span>
                                                                <span>
                                                                    <label id="lblInputUnicom" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.UNICOM"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArinc">ARINC:</label></span>
                                                                <span>
                                                                    <label id="lblInputArinc" data-bind="enable: self.EditMode, text: Logistics.PreflightDepFboListViewModel.ARINC"></label>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Confirmation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbDepartFBOConfirmation" rows="2" cols="40" data-bind="enable: self.EditMode, textInput: Logistics.PreflightDepFboListViewModel.ConfirmationStatus, event: { change: FBOCommentOrConfirmationChange }"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Comments
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbDepartFBOComments" rows="2" cols="40" data-bind="enable: self.EditMode, textInput: Logistics.PreflightDepFboListViewModel.Comments, event: { change: FBOCommentOrConfirmationChange }"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                    <div class="parts_sections">
                                        <fieldset>
                                            <legend>Arrive-<label id="lblArriveFBOICAO" data-bind="text: Logistics.PreflightLeg.ArrivalAirport.IcaoID"></label></legend>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td colspan="2" align="left" style="padding-left: 3px;">
                                                        
                                                    </td>
                                                    <td>Status
                                                    </td>
                                                    <td>
                                                        <select id="ddlArriveFBOCompleteds" data-bind="enable: self.EditMode, value: Logistics.PreflightArrFboListViewModel.Status, event: { change: FBOCommentOrConfirmationChange }">
                                                            <option value="0" selected="selected"><-Select-></option>
                                                            <option value="Required">Required</option>
                                                            <option value="Not Required">Not Required</option>
                                                            <option value="In Progress">In Progress</option>
                                                            <option value="Change">Change</option>
                                                            <option value="Canceled">Canceled</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="nav-space">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Code
                                                    </td>
                                                    <td class="tdLabel110" align="left">
                                                        <table>
                                                            <tr>
                                                                <td><input type="text" id="tbArriveFBOCode" class="text60" maxlength="4"
                                                            onkeypress="return fnAllowAlphaNumeric(this, event)" 
                                                            onblur="return RemoveSpecialChars(this)" data-bind="enable: self.EditMode, value: Logistics.PreflightArrFboListViewModel.FBOCD, event: { change: ArrivalFBOChange }">
                                                        </input></td>
                                                                <td><input type="hidden" id="hdArriveFBOID" data-bind="value: Logistics.PreflightArrFboListViewModel.FBOID" /></td>
                                                                <td><input type="button" id="btnArriveFBOCode" onclick="javascript: openFBOWin('ARRIVAL'); return false;" class="browse-button" data-bind="    enable: self.EditMode" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="tdLabel40" align="left">ICAO
                                                    </td>
                                                    <td align="left">
                                                        <input type="text" id="tbArriveFBOICAO" maxlength="4" class="inpt_non_edit text40" data-bind="enable: self.EditMode, value: Logistics.PreflightLeg.ArrivalAirport.IcaoID">
                                                        </input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel40" align="left"></td>
                                                    <td colspan="3" align="left">
                                                        <label id="lbcvArriveFBOCode" class="alert-text"></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">FBO Info
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div id="div2" class="pr_fbo_box" runat="server">
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveName">Name:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveName" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.PreflightFBOName"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveAddr1">Addr 1:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveAddr1" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.Addr1"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveAddr2">Addr 2:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveAddr2" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.Addr2"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveAddr3">Addr 3:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveAddr3" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.Addr3"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArrivePhone1">Phone 1:</label></span>
                                                                <span>
                                                                    <label id="lblInputArrivePhone1" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.PhoneNum1"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArrivePhone2">Phone 2:</label></span>
                                                                <span>
                                                                    <label id="lblInputArrivePhone2" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.PhoneNum2"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveFax">Fax:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveFax" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.FaxNUM"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveCity">City:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveCity" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.CityName"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveState">State:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveState" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.StateName"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">
                                                                    <label id="lblArriveEmail">E-mail:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveEmail" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.EmailAddress"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel95">
                                                                    <label id="lblArriveZipPostalCode">Zip/Postal Code:</label></span>
                                                                <span>
                                                                    <label id="lblInputArriveZipPostalCode" data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.PostalZipCD"></label>
                                                                </span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">UNICOM:</span>
                                                                <span data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.UNICOM"></span>
                                                            </div>
                                                            <div class="fbo_info">
                                                                <span class="tdLabel50">ARINC:</span><span data-bind="enable: self.EditMode, text: Logistics.PreflightArrFboListViewModel.ARINC">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Confirmation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbArriveFBOConfirmation" data-bind="enable: self.EditMode, textInput: Logistics.PreflightArrFboListViewModel.ConfirmationStatus, event: { change: FBOCommentOrConfirmationChange }" cols="40" rows="2"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Comments
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbArriveFBOComments" data-bind="enable: self.EditMode, textInput: Logistics.PreflightArrFboListViewModel.Comments, event: { change: FBOCommentOrConfirmationChange }" cols="40" rows="2"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </div>
            <div class="preflightlogistic_fuelgrid_wrapper" style="float: left; width: 718px; padding: 5px 0 0 0px;">
                <telerik:RadPanelBar ID="pnlFuel" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                    runat="server" OnClientItemExpand="pnlFuelExpand" OnClientItemCollapse="pnlFuelCollapse" OnClientItemAnimationEnd="pnlFuelAnimationEnd">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Fuel" CssClass="PanelHeaderStyle">
                            <ContentTemplate>
                                <table width="100%" cellpadding="0" cellspacing="0" class="box-bg-normal-pad nomargin_logisti preflight_logistic_fuel_wrapper">
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" class="tdLabel340">                                                        
                                                    </td>
                                                    <td align="left">
                                                        <input type="radio" name="faType" id="rdUsGallon" value="0" data-bind="enable: self.EditMode, checked: Logistics.FuelAmountType, event: { click: fuelTypeRadioClick }" />
                                                        <label for="rdUsGallon">U.S. Gallons</label>

                                                        <input type="radio" name="faType" id="rdLiters" value="2" data-bind="enable: self.EditMode, checked: Logistics.FuelAmountType, event: { click: fuelTypeRadioClick }" />
                                                        <label for="rdLiters">Liters</label>

                                                        <input type="radio" name="faType" id="rdImperialGallon" value="1" data-bind="enable: self.EditMode, checked: Logistics.FuelAmountType, event: { click: fuelTypeRadioClick }" />
                                                        <label for="rdImperialGallon">Imperial Gallons</label>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" class="tdLabel100">Departure ICAO
                                                    </td>
                                                    <td class="tdLabel80">
                                                        <input type="text" id="tbDepartureICAO" readonly="readonly" class="text50" data-bind="enable: self.EditMode, value: Logistics.PreflightLeg.DepartureAirport.IcaoID"></input>
                                                    </td>
                                                    <td align="left" class="tdLabel90">Arrival ICAO
                                                    </td>
                                                    <td class="tdLabel80">
                                                        <input type="text" id="tbArriveICAO" readonly="readonly" class="text50" data-bind="enable: self.EditMode, value: Logistics.PreflightLeg.ArrivalAirport.IcaoID"></input>
                                                    </td>
                                                    <td align="left" class="tdLabel150">Estimated
                                                        <label id="lbEstimatedUS" data-bind="text: Logistics.LblEstimateType"></label>
                                                    </td>
                                                    <td align="left">
                                                        <input type="text" id="tbEstimatedGallons" maxlength="6" 
                                                            class="text50" onkeypress="return fnAllowNumeric(this,event)" data-bind="enable: self.EditMode, value: Logistics.PreflightLeg.EstFuelQTY, event: { change: EstFuelQTYChangeCall }"></input>
                                                        <input type="button" style="display:none;margin:4px;" class="browse-button-Validation-Loading" id="btnEstimateGallonsLoading" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="preflight-custom-grid-fuel" align="center">
                                            <%--LogisticsFuelGrid--%>
                                            <div class="jqgrid">
                                                <div>
                                                    <table class="box1">
                                                        <tr>
                                                            <td id="tdOfFuelTable">
                                                                <table id="LogisticsFuelGrid" class="table table-striped table-hover table-bordered preflightlogistic_fuelgrid"></table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div class="nav-space">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" class="pr_radtextbox_310 tdLabel320"> Comments:<br />
                                                        <textarea id="tbNote" rows="2" cols="40" 
                                                            data-bind="enable: self.EditMode, value: Logistics.FuelVendorText"></textarea>
                                                    </td>
                                                    <td align="left">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left">&nbsp;Vendor :
                                                                </td>
                                                                <td align="left">
                                                                    <label id="lbBestVendor" data-bind="enable: self.EditMode, text: Logistics.VendorName"></label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="nav-3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="tdLabel130">&nbsp;Estimated Fuel Cost :
                                                                </td>
                                                                <td align="left" class="fc_textbox">                                                                    
                                                                    <telerik:RadNumericTextBox ID="tbEstimatedCost" runat="server" Type="Currency" Culture="en-US"
                                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" ReadOnly="true" data-bind="enable:self.EditMode,value:Logistics.EstimatedCost">
                                                                    </telerik:RadNumericTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="nav-3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <input type="hidden" value="false" id="hdnUpdateClick" />
                                                                    <input type="button" id="btnUpdateFuel" value="Update Fuel" onclick="return btnUpdateClick();" data-bind="    css: self.ButtonDisableCss, enable: self.EditMode" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <div class="nav-space">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </div>
            <a name="Catering"></a>
            <div class="preflight_logistic_catering" style="float: left; width: 718px; padding: 5px 0 0 0px;">
                <telerik:RadPanelBar ID="pnlCatering" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                    runat="server" OnClientItemExpand="pnlCateringExpand" OnClientItemCollapse="pnlCateringCollapse">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Catering" CssClass="PanelHeaderStyle">
                            <ContentTemplate>
                                <div class="logistic_fieldset_wrapper">
                                    <div class="parts_sections">
                                        <fieldset>
                                            <legend>Depart-<label id="lblDepartCaterICAO" data-bind="enable: self.EditMode, text: Logistics.PreflightLeg.DepartureAirport.IcaoID"></label></legend>
                                            <table width="100%" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td colspan="2" align="left" class="tdLabel200">
                                                        
                                                    </td>
                                                    <td>Status
                                                    </td>
                                                    <td>
                                                        
                                                        <select id="ddlDepartCaterCompleteds" data-bind="enable: self.EditMode, value: Logistics.DepartLogisticsCatering.Status, event: { change: CaterCommentOrConfirmationChange }">
                                                            <option value="0" selected="selected"><-Select-></option>
                                                            <option value="Required">Required</option>
                                                            <option value="Not Required">Not Required</option>
                                                            <option value="In Progress">In Progress</option>
                                                            <option value="Change">Change</option>
                                                            <option value="Canceled">Canceled</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel40" align="left">Code
                                                    </td>
                                                    <td class="tdLabel110" align="left">
                                                        <table>
                                                            <tr>
                                                                <td><input type="text" id="tbDepartCaterCode" class="text60" maxlength="4"
                                                            onkeypress="return fnAllowAlphaNumeric(this, event)" 
                                                            onblur="return RemoveSpecialChars(this)" data-bind="enable: self.EditMode, value: Logistics.DepartLogisticsCatering.CateringCD, event: { change: CateringDepartChange }"></input></td>
                                                                <td><input type="hidden" id="hdDepartCaterID" data-bind="value: Logistics.DepartLogisticsCatering.CateringID" /></td>
                                                                <td><input type="button" id="btnDepartCaterCode" onclick="javascript: openCateringWin('DEPART'); return false;" class="browse-button" data-bind="    enable: self.EditMode" /></td>
                                                            </tr>
                                                        </table>
                                                        
                                                    </td>
                                                    <td class="tdLabel40" align="left">Cost
                                                    </td>
                                                    <td align="left">
                                                        <input id="tbDepartCaterCost"  style="width:60px" maxlength="20" data-bind="enable: self.EditMode, CostCurrencyElement: Logistics.DepartLogisticsCatering.Cost" onKeyPress="return fnAllowNumeric(this, event)" />
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td align="left" colspan="3">
                                                        <label id="lbcvDepartCaterCode" class="alert-text"></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Name
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <input type="text" id="tbDepartCaterName" maxlength="50" class="text250 textfield_medium" data-bind="enable: self.EditMode, value: Logistics.DepartLogisticsCatering.ContactName"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Phone
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <input type="text" id="tbDepartCaterPhone" class="text250 textfield_medium" maxlength="20"
                                                            onkeypress="return fnAllowPhoneFormat(this,event)" data-bind="enable: self.EditMode, value: Logistics.DepartLogisticsCatering.ContactPhone"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Fax
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <input type="text" id="tbDepartCaterFax" class="text250 textfield_medium" maxlength="20"
                                                            onkeypress="return fnAllowPhoneFormat(this,event)" data-bind="enable: self.EditMode, value: Logistics.DepartLogisticsCatering.ContactFax"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="4">Contact Name
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">
                                                        <input type="text" id="tbDepartContactName" maxlength="40" class="text300 textfield_big" data-bind="enable: self.EditMode, value: Logistics.DepartLogisticsCatering.CateringContactName, event: { change: CaterCommentOrConfirmationChange }"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">
                                                        <div class="tblspace_5">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Confirmation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbDepartCaterConfirmation" data-bind="enable: self.EditMode, textInput: Logistics.DepartLogisticsCatering.CateringConfirmation, event: { change: CaterCommentOrConfirmationChange }" rows="2" cols="40"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Comments
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbDepartCaterComments" data-bind="enable: self.EditMode, textInput: Logistics.DepartLogisticsCatering.CateringComments, event: { change: CaterCommentOrConfirmationChange }" rows="2" cols="40"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                    <div class="parts_sections">
                                        <fieldset>
                                            <legend>Arrive-<label id="lblArriveCaterICAO" data-bind="text: Logistics.PreflightLeg.ArrivalAirport.IcaoID"></label></legend>
                                            <table width="100%" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td align="left" colspan="2" class="tdLabel200">
                                                        
                                                    </td>
                                                    <td>Status
                                                    </td>
                                                    <td align="left">
                                                        <select id="ddlArriveCaterCompleteds" data-bind="enable: self.EditMode, value: Logistics.ArrivalLogisticsCatering.Status, event: { change: CaterCommentOrConfirmationChange }">
                                                            <option value="0" selected="selected"><-Select-></option>
                                                            <option value="Required">Required</option>
                                                            <option value="Not Required">Not Required</option>
                                                            <option value="In Progress">In Progress</option>
                                                            <option value="Change">Change</option>
                                                            <option value="Canceled">Canceled</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                        <input type="checkbox" id="chkArriveCaterCompleted" Text="Completed" style="display:none;" data-bind="enable:self.EditMode,checked: Logistics.ArrivalLogisticsCatering.IsCompleted" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel40" align="left">Code
                                                    </td>
                                                    <td class="tdLabel110" align="left">
                                                        <table>
                                                            <tr>
                                                                <td><input type="text" id="tbArriveCaterCode" class="text60" maxlength="4"
                                                            onkeypress="return fnAllowAlphaNumeric(this, event)" 
                                                            onblur="return RemoveSpecialChars(this)" data-bind="enable: self.EditMode, value: Logistics.ArrivalLogisticsCatering.CateringCD, event: { change: CateringArrivalChange }"></input></td>
                                                                <td><input type="hidden" id="hdArriveCaterID"></td>
                                                                <td><input type="button" id="btnArriveCaterCode" onclick="javascript: openCateringWin('ARRIVAL'); return false;" class="browse-button" data-bind="    enable: self.EditMode" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="tdLabel40" align="left">Cost
                                                    </td>
                                                    <td align="left">
                                                        
                                                        <input id="tbArriveCaterCost"  style="width:60px" maxlength="20" data-bind="enable: self.EditMode, CostCurrencyElement: Logistics.ArrivalLogisticsCatering.Cost" onKeyPress="return fnAllowNumeric(this, event)" "/>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"></td>
                                                    <td colspan="3" align="left">
                                                        <label id="lbcvArriveCaterCode" class="alert-text" data-bind="text: Logistics.lbcvArriveCaterCode"></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Name
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <input type="text" id="tbArriveCaterName" maxlength="50" class="text250 textfield_medium" data-bind="enable: self.EditMode, value: Logistics.ArrivalLogisticsCatering.ContactName"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Phone
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <input type="text" id="tbArriveCaterPhone" class="text250 textfield_medium" maxlength="20"
                                                            onkeypress="return fnAllowPhoneFormat(this,event)" data-bind="enable: self.EditMode, value: Logistics.ArrivalLogisticsCatering.ContactPhone"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Fax
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <input type="text" id="tbArriveCaterFax" class="text250 textfield_medium" maxlength="20"
                                                            onkeypress="return fnAllowPhoneFormat(this,event)" data-bind="enable: self.EditMode, value: Logistics.ArrivalLogisticsCatering.ContactFax"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Contact Name
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">
                                                        <input type="text" id="tbArriveContactName" maxlength="40" class="text300 textfield_big" data-bind="enable: self.EditMode, value: Logistics.ArrivalLogisticsCatering.CateringContactName, event: { change: CaterCommentOrConfirmationChange }"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">
                                                        <div class="tblspace_5">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Confirmation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" rows="2" cols="40" id="tbArriveCaterConfirmation" data-bind="enable: self.EditMode, textInput: Logistics.ArrivalLogisticsCatering.CateringConfirmation, event: { change: CaterCommentOrConfirmationChange }"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">Comments
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" class="pr_radtextbox_310">
                                                        <textarea class="textarea_logistic" id="tbArriveCaterComments" data-bind="enable: self.EditMode, textInput: Logistics.ArrivalLogisticsCatering.CateringComments, event: { change: CaterCommentOrConfirmationChange }" rows="2" cols="40"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </div>
        </div>
        <div style="text-align: right; width: 718px; padding: 5px 0 0 0px;">
            
            <table width="718px" cellspacing="6">

                <tr>
                    <td align="right" style="padding: 11px 0 7px;">
                        <UCPreflight:Footer ID="PreflightHeader" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                  
                </td>
            </tr>
        </table>
        <table id="tblHidden1" style="display: none;">
            <tr>
                <td>
                   
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
