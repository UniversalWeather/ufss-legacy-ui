﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using FlightPak.Web.PreflightService;
using System.Globalization;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using Telerik.Web.UI;
using System.ComponentModel;
using FlightPak.Web.Framework.Helpers.Preflight;
using System.IO;
using System.Linq;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.ViewModels;
using System.Configuration;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class EmailTrip : BaseSecuredPage
    {
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            GetSheetReportHeaderList();
                        }

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalPopupForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalPopupForm, DivExternalPopupForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCrew, DivExternalPopupForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPax, DivExternalPopupForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEmail, DivExternalPopupForm, RadAjaxLoadingPanel1);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
            //Necessary to prevent ajax postback errors when Send Email buton is clicked on Email Notification popup window
            var ajaxTimeout = 18000;
            int.TryParse(ConfigurationManager.AppSettings["PreflightEmailSendingAsyncPostbackTimeout"], out ajaxTimeout);
            radScriptManager1.AsyncPostBackTimeout = ajaxTimeout;
        }

        #region CREW

        protected void Crew_BindData(object source, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<PreflightLegCrewViewModel> finalCrewList = new List<PreflightLegCrewViewModel>();
                        if (Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            //PreflightMain Trip = new PreflightMain();
                            var TripVM = (PreflightTripViewModel)Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip];

                            if (TripVM.PreflightLegs != null)
                            {
                                foreach (PreflightLegViewModel leg in TripVM.PreflightLegs)
                                {
                                    var CrewList = TripVM.PreflightLegCrews[Convert.ToString(leg.LegNUM)];
                                    if (CrewList != null)
                                    {
                                        List<PreflightLegCrewViewModel> crewList = CrewList;

                                        foreach (PreflightLegCrewViewModel crew in crewList)
                                        {
                                            if (!finalCrewList.Exists(x => x.CrewID == crew.CrewID))
                                                finalCrewList.Add(crew);
                                        }
                                    }
                                }
                            }
                        }
                        dgCrew.DataSource = finalCrewList;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Crew_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item.GetDataKeyValue("CrewID") != null)
                            {
                                Int64 _crewId = Convert.ToInt64(item.GetDataKeyValue("CrewID").ToString());
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetCrewbyCrewId(_crewId);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        if (objRetVal.EntityList != null && objRetVal.EntityList.Count > 0)
                                        {
                                            Label lbCrewName = (Label)item.FindControl("lbCrewName");
                                            string _crewname = string.Empty;
                                            _crewname = objRetVal.EntityList[0].LastName != null ? objRetVal.EntityList[0].LastName + ", " : string.Empty;
                                            _crewname += objRetVal.EntityList[0].FirstName != null ? objRetVal.EntityList[0].FirstName + " " : string.Empty;
                                            _crewname += objRetVal.EntityList[0].MiddleInitial != null ? objRetVal.EntityList[0].MiddleInitial : string.Empty;

                                            lbCrewName.Text = System.Web.HttpUtility.HtmlEncode(_crewname);

                                            CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                                            Label lbCrewEmail = (Label)item.FindControl("lbCrewEmail");
                                            RadToolTip radCrewToolTip = (RadToolTip)item.FindControl("radCrewToolTip");
                                            if (!string.IsNullOrEmpty(objRetVal.EntityList[0].EmailAddress))
                                            {
                                                chkSelect.Enabled = true;
                                                lbCrewName.ForeColor = System.Drawing.Color.Black;
                                                lbCrewEmail.Text = System.Web.HttpUtility.HtmlEncode(objRetVal.EntityList[0].EmailAddress);
                                                radCrewToolTip.Text = objRetVal.EntityList[0].EmailAddress;
                                            }
                                            else
                                            {
                                                chkSelect.Enabled = false;
                                                lbCrewName.ForeColor = System.Drawing.Color.Red;
                                                radCrewToolTip.Text = "Email Address Not Available!";
                                                radCrewToolTip.ForeColor = System.Drawing.Color.Red;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Crew_ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            foreach (GridDataItem dataItem in dgCrew.MasterTableView.Items)
            {
                if ((dataItem.FindControl("chkSelect") as CheckBox).Enabled)
                {
                    (dataItem.FindControl("chkSelect") as CheckBox).Checked = headerCheckBox.Checked;
                    dataItem.Selected = headerCheckBox.Checked;
                }
                else
                {
                    (dataItem.FindControl("chkSelect") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }
            }
        }

        protected void Crew_ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            bool checkHeader = true;
            foreach (GridDataItem dataItem in dgCrew.MasterTableView.Items)
            {
                if (!(dataItem.FindControl("chkSelect") as CheckBox).Checked)
                {
                    checkHeader = false;
                    break;
                }
            }
            GridHeaderItem headerItem = dgCrew.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            (headerItem.FindControl("chkSelectAll") as CheckBox).Checked = checkHeader;
        }

        #endregion

        #region PAX

        protected void Pax_BindData(object source, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<PassengerViewModel> finalPaxList = new List<PassengerViewModel>();
                        if (Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            PreflightTripViewModel Trip = new PreflightTripViewModel();
                            Trip = (PreflightTripViewModel)Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip];

                            if (Trip.PreflightLegs != null)
                            {
                                foreach (PreflightLegViewModel leg in Trip.PreflightLegs)
                                {
                                    var perflightPassengerList = Trip.PreflightLegPassengers[Convert.ToString(leg.LegNUM)];
                                    if (perflightPassengerList != null)
                                    {
                                        List<PassengerViewModel> paxList = perflightPassengerList;

                                        foreach (PassengerViewModel pax in paxList)
                                        {
                                            if (!finalPaxList.Exists(x => x.PassengerID == pax.PassengerID))
                                                finalPaxList.Add(pax);
                                        }
                                    }
                                }
                            }
                        }
                        dgPax.DataSource = finalPaxList;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Pax_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item.GetDataKeyValue("PassengerID") != null)
                            {
                                Int64 _paxId = Convert.ToInt64(item.GetDataKeyValue("PassengerID").ToString());
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objService.GetPassengerbyPassengerRequestorID(_paxId);
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        if (objRetVal.EntityList != null && objRetVal.EntityList.Count > 0)
                                        {
                                            Label lbPaxName = (Label)item.FindControl("lbPaxName");
                                            string _paxname = string.Empty;
                                            _paxname = objRetVal.EntityList[0].LastName != null ? objRetVal.EntityList[0].LastName + ", " : string.Empty;
                                            _paxname += objRetVal.EntityList[0].FirstName != null ? objRetVal.EntityList[0].FirstName + " " : string.Empty;
                                            _paxname += objRetVal.EntityList[0].MiddleInitial != null ? objRetVal.EntityList[0].MiddleInitial : string.Empty;

                                            lbPaxName.Text = System.Web.HttpUtility.HtmlEncode(_paxname);

                                            CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                                            Label lbPaxEmail = (Label)item.FindControl("lbPaxEmail");
                                            RadToolTip radPaxToolTip = (RadToolTip)item.FindControl("radPaxToolTip");
                                            if (!string.IsNullOrEmpty(objRetVal.EntityList[0].EmailAddress))
                                            {
                                                chkSelect.Enabled = true;
                                                lbPaxName.ForeColor = System.Drawing.Color.Black;
                                                lbPaxEmail.Text = System.Web.HttpUtility.HtmlEncode(objRetVal.EntityList[0].EmailAddress);
                                                radPaxToolTip.Text = objRetVal.EntityList[0].EmailAddress;
                                            }
                                            else
                                            {
                                                chkSelect.Enabled = false;
                                                lbPaxName.ForeColor = System.Drawing.Color.Red;
                                                lbPaxEmail.Text = string.Empty;
                                                radPaxToolTip.Text = "Email Address Not Available!";
                                                radPaxToolTip.ForeColor = System.Drawing.Color.Red;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Pax_ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            foreach (GridDataItem dataItem in dgPax.MasterTableView.Items)
            {
                if ((dataItem.FindControl("chkSelect") as CheckBox).Enabled)
                {
                    (dataItem.FindControl("chkSelect") as CheckBox).Checked = headerCheckBox.Checked;
                    dataItem.Selected = headerCheckBox.Checked;
                }
                else
                {
                    (dataItem.FindControl("chkSelect") as CheckBox).Checked = false;
                    dataItem.Selected = false;
                }
            }
        }

        protected void Pax_ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            bool checkHeader = true;
            foreach (GridDataItem dataItem in dgPax.MasterTableView.Items)
            {
                if (!(dataItem.FindControl("chkSelect") as CheckBox).Checked)
                {
                    checkHeader = false;
                    break;
                }
            }
            GridHeaderItem headerItem = dgPax.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            (headerItem.FindControl("chkSelectAll") as CheckBox).Checked = checkHeader;
        }

        #endregion

        protected void btnEmail_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        string errMsg = string.Empty;

                        if (!string.IsNullOrEmpty(tbEmailTripToOthers.Text))
                        {
                            string[] tripEmails = tbEmailTripToOthers.Text.Split(',');
                            string invalidEmails = string.Empty;

                            if (tripEmails.Length > 0)
                            {
                                foreach (string email in tripEmails)
                                {
                                    bool isValid = Validator.EmailIsValid(email.Trim());

                                    if (!isValid)
                                        invalidEmails += email + ",<br>";
                                }

                                if (!string.IsNullOrEmpty(invalidEmails))
                                {
                                    errMsg = "<b>Invalid E-mail Trip to Other(s)</b> : <br>" + invalidEmails;

                                    //lbTrip.Text = System.Web.HttpUtility.HtmlEncode("Invalid E-mail Trip to Other(s) : \n" + invalidEmails);
                                    //lbTrip.ForeColor = System.Drawing.Color.Red;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(tbEmailItineraryToOthers.Text))
                        {
                            string[] itineraryEmails = tbEmailItineraryToOthers.Text.Split(',');
                            string invalidEmails = string.Empty;

                            if (itineraryEmails.Length > 0)
                            {
                                foreach (string email in itineraryEmails)
                                {
                                    bool isValid = Validator.EmailIsValid(email.Trim());

                                    if (!isValid)
                                        invalidEmails += email + ",<br>";
                                }

                                if (!string.IsNullOrEmpty(invalidEmails))
                                {
                                    if (!string.IsNullOrEmpty(errMsg))
                                        errMsg += "<br>";

                                    errMsg += "<b>Invalid E-mail Itinerary to Other(s)</b> : <br>" + invalidEmails;

                                    //lbItinerary.Text = System.Web.HttpUtility.HtmlEncode("Invalid E-mail Itinerary to Other(s) : \n" + invalidEmails);
                                    //lbItinerary.ForeColor = System.Drawing.Color.Red;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(errMsg))
                        {
                            RadWindowManager1.RadAlert(errMsg, 350, 100, "E-mail Notification", "alertCallBack");
                            //lbMessage.Text = System.Web.HttpUtility.HtmlEncode(errMsg);
                            //lbMessage.CssClass = "alert-text";
                        }
                        else
                        {
                            RadWindowManager1.RadConfirm("Are you sure you want to send an E-mail?", "confirmEmailCallBackFn", 350, 100, null, "E-mail Confirmation");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        private string PrepareEmailList(List<string> CrewEmailList, List<PaxInfo> PaxInfoList, List<string> PaxEmailOthersList)
        {
            string result = string.Empty;
            using (PreflightServiceClient Service = new PreflightServiceClient())
            {

                var TripVM = (PreflightTripViewModel)Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip];

                string FromEmail = tbEmailFrom.Text.Trim();
                if (String.IsNullOrEmpty(FromEmail.Trim()))
                {
                    var userGuid = Session[3];
                    if (userGuid != null && (FlightPak.Web.Framework.Prinicipal.FPPrincipal)(userGuid) != null)
                    {
                        var emailOfLoggedInUser = ((FlightPak.Web.Framework.Prinicipal.FPPrincipal)(userGuid)).Identity._email;
                        FromEmail = emailOfLoggedInUser;
                    }
                }
                if (TripVM != null && TripVM.TripId != 0)
                {
                    try
                    {
                        PreflightMain Trip = Service.GetTrip((long)TripVM.TripId).EntityList.FirstOrDefault();

                        // Tripsheet Details to Crew
                        if (CrewEmailList.Count > 0)
                        {
                            //2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
                            Service.SendTripDetailsWithFromEmail(Trip, CrewEmailList, FromEmail, GenerateHtmlBody(Trip, "Tripsheet"), CreateAttachmentFileBase64Strings(Trip, "Tripsheet"));
                        }

                        // Itinerary Details to Others
                        if (PaxEmailOthersList.Count > 0)
                        {
                            //2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
                            Service.SendItineraryDetailsToOthersWithFromEmail(Trip, PaxEmailOthersList, FromEmail, GenerateHtmlBody(Trip, "Itinerary"), CreateAttachmentFileBase64Strings(Trip, "Itinerary"));
                        }

                        // Itinerary Details to PAX
                        if (PaxInfoList.Count > 0)
                        {
                            foreach (PaxInfo objPax in PaxInfoList)
                            {
                                List<GetPaxItineraryInfo> paxItineraryList = new List<GetPaxItineraryInfo>();
                                paxItineraryList = Service.GetPassengerItinerary(Trip, objPax.PaxId);

                                //2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
                                Service.SendItineraryDetailsToPAXWithFromEmail(Trip, objPax.PaxEmailId, FromEmail, GenerateHtmlBody(Trip, "Itinerary"), CreateAttachmentFileBase64Strings(Trip, "Itinerary"));
                            }
                        }
                        result = "E-mail sent at " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", DateTime.Now);
                        //lbMessage.ForeColor = System.Drawing.Color.Green;
                        //lbMessage.Text = System.Web.HttpUtility.HtmlEncode("E-mail sent at " + DateTime.Now.ToString()); // Mail Sent Successfully..");
                    }
                    catch (Exception ex)
                    {
                        // Manually Handled
                        //lbMessage.CssClass = "alert-text";
                        // This has been modified by Vishwa & Prabhu for Veracode Id: 3035
                        //lbMessage.Text = System.Web.HttpUtility.HtmlEncode("An error occured. Please check email id provided and try again");
                        result = "Error : " + ex.Message;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="BodyType"></param>
        /// <returns></returns>
        protected List<String> CreateAttachmentFileBase64Strings(PreflightMain Trip, string BodyType)
        {
            string strReportPath = string.Empty;
            string strReportName = string.Empty;

            if (BodyType == "Tripsheet")
            {
                strReportPath = @"/PreFlight/RptPRETSTripSheetMain";
                strReportName = "TripsheetReport-TripNo-" + Trip.TripNUM;
            }
            else if (BodyType == "Itinerary")
            {
                strReportPath = @"/PreFlight/RptPRETSTripSheetItinerary";
                strReportName = "ItineraryReport-TripNo-" + Trip.TripNUM;
            }

            List<FlightPak.Web.ReportRenderService.ReportParameter> rptParams;

            FlightPak.Web.ReportRenderService.ReportRenderClient svcReports = new FlightPak.Web.ReportRenderService.ReportRenderClient();

            rptParams = svcReports.GetReportParameters(strReportPath);

            foreach (FlightPak.Web.ReportRenderService.ReportParameter rptParam in rptParams)
            {
                //loops through each parameter and set the parameter value

                if (rptParam.ParamName.ToUpper() == "TRIPNUM")
                {
                    rptParam.ParamValue = Trip.TripNUM.ToString();
                }

                if (rptParam.ParamName.ToUpper() == "USERCD")
                {
                    rptParam.ParamValue = UserPrincipal.Identity._name;
                }

                if (rptParam.ParamName.ToUpper() == "REPORTHEADERID")
                {
                    rptParam.ParamValue = "0";
                    if (ddlReportFormat.Items.Count > 0)
                    {
                        rptParam.ParamValue = ddlReportFormat.SelectedValue;
                    }
                }

                if (rptParam.ParamName.ToUpper() == "REPORTNUMBER" && BodyType == "Tripsheet")
                {
                    rptParam.ParamValue = "2";
                }
                else if (rptParam.ParamName.ToUpper() == "REPORTNUMBER" && BodyType == "Itinerary")
                {
                    rptParam.ParamValue = "3";
                }

                if (rptParam.ParamName.ToUpper() == "USERCUSTOMERID")
                {
                    rptParam.ParamValue = UserPrincipal.Identity._customerID.ToString();
                }

                if (rptParam.ParamName.ToUpper() == "USERHOMEBASEID")
                {
                    rptParam.ParamValue = UserPrincipal.Identity._homeBaseId.ToString();
                }

                if (rptParam.ParamName.ToUpper() == "ISTRIP" || rptParam.ParamName.ToUpper() == "ISCANCELED" || rptParam.ParamName.ToUpper() == "ISHOLD" || rptParam.ParamName.ToUpper() == "ISWORKSHEET" || rptParam.ParamName.ToUpper() == "ISUNFULFILLED" || rptParam.ParamName.ToUpper() == "ISSCHEDULEDSERVICE")
                {
                    rptParam.ParamValue = false.ToString();
                }

                if (rptParam.ParamType.ToString().ToUpper() == "DATETIME" && rptParam.ParamValue == "")
                    rptParam.ParamValue = null;
            }

            Byte[] rptStream = svcReports.GetReportData(strReportPath, "PDF", rptParams);
            svcReports.Close();

            List<String> retAttachmentFileBase64Strings = new List<string>();
            retAttachmentFileBase64Strings.Add(Convert.ToBase64String(rptStream) + "|||" + strReportName);

            return retAttachmentFileBase64Strings;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="BodyType"></param>
        /// <returns></returns>
        protected string GenerateHtmlBody(PreflightMain Trip, string BodyType)
        {
            return string.Format("Please find attached {0} for Trip Number: {1}", BodyType, Trip.TripNUM.ToString());
        }

        protected void btnEmailYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                List<string> CrewEmailList = new List<string>();
                List<PaxInfo> PaxInfoList = new List<PaxInfo>();
                List<string> PaxEmailOthersList = new List<string>();

                #region Send Trip Details To Crew
                foreach (GridDataItem item in dgCrew.Items)
                {
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                    if (chkSelect != null && chkSelect.Checked)
                    {
                        Label lbCrewEmail = (Label)item.FindControl("lbCrewEmail");
                        if (lbCrewEmail != null && !string.IsNullOrEmpty(lbCrewEmail.Text) && lbCrewEmail.Text.Trim() != "&nbsp;")
                        {
                            string _crewEmail = System.Web.HttpUtility.HtmlDecode(lbCrewEmail.Text);
                            CrewEmailList.Add(_crewEmail);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(tbEmailTripToOthers.Text))
                {
                    string[] emails = tbEmailTripToOthers.Text.Split(',');

                    if (emails.Length > 0)
                    {
                        foreach (string email in emails)
                        {
                            bool isValid = Validator.EmailIsValid(email.Trim());

                            if (isValid)
                                CrewEmailList.Add(email);
                        }
                    }
                }
                #endregion

                #region Send Itinery Details To PAX
                foreach (GridDataItem item in dgPax.Items)
                {
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                    if (chkSelect != null && chkSelect.Checked)
                    {
                        if (item.GetDataKeyValue("PassengerID") != null)
                        {
                            Int64 _paxId = Convert.ToInt64(item.GetDataKeyValue("PassengerID"));
                            Label lbPaxEmail = (Label)item.FindControl("lbPaxEmail");
                            if (_paxId > 0 && (lbPaxEmail != null && !string.IsNullOrEmpty(lbPaxEmail.Text) && lbPaxEmail.Text.Trim() != "&nbsp;"))
                            {
                                string _paxEmail = System.Web.HttpUtility.HtmlDecode(lbPaxEmail.Text);
                                PaxInfoList.Add(new PaxInfo() { PaxId = _paxId, PaxEmailId = _paxEmail });
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(tbEmailItineraryToOthers.Text))
                {
                    string[] emails = tbEmailItineraryToOthers.Text.Split(',');

                    if (emails.Length > 0)
                    {
                        foreach (string email in emails)
                        {
                            bool isValid = Validator.EmailIsValid(email.Trim());

                            if (isValid)
                                PaxEmailOthersList.Add(email);
                        }
                    }
                }
                #endregion

                if (CrewEmailList.Count == 0 && PaxEmailOthersList.Count == 0 && PaxInfoList.Count == 0)
                {
                    RadWindowManager1.RadAlert("None of the E-mail section have valid records!", 350, 100, "E-mail Notification", "alertCallBack");
                }
                else
                {
                    string retVal = string.Empty;
                    retVal = PrepareEmailList(CrewEmailList, PaxInfoList, PaxEmailOthersList);
                    RadWindowManager1.RadAlert(retVal, 350, 100, "E-mail Notification", "alertCallBack");

                }
            }
        }

        protected void btnEmailNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
            }
        }

        protected void btnAlert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
            }
        }

        protected void GetSheetReportHeaderList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetTripSheetReportHeader> TripSheetReportHeaderList = new List<GetTripSheetReportHeader>();
                        using (FlightPakMasterService.MasterCatalogServiceClient TripSheetReportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TripSheetReportHeaderInfo = TripSheetReportService.GetTripSheetReportHeaderList();
                            if (TripSheetReportHeaderInfo.ReturnFlag == true)
                            {
                                TripSheetReportHeaderList = TripSheetReportHeaderInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetTripSheetReportHeader>();
                            }

                            if (TripSheetReportHeaderList != null)
                            {
                                ddlReportFormat.Items.Clear();
                                ddlReportFormat.DataSource = TripSheetReportHeaderList;
                                ddlReportFormat.DataTextField = "ReportDescription";
                                ddlReportFormat.DataValueField = "TripSheetReportHeaderID";
                                ddlReportFormat.DataBind();
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
    }

    public static class Validator
    {
        static Regex ValidEmailRegex = CreateValidEmailRegex();

        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }

        internal static bool EmailIsValid(string emailAddress)
        {
            bool isValid = ValidEmailRegex.IsMatch(emailAddress);

            return isValid;
        }
    }

    public class PaxInfo
    {
        public Int64 PaxId { get; set; }
        public string PaxEmailId { get; set; }
    }
}