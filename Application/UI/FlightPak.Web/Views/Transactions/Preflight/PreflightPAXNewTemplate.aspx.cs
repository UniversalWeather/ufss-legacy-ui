﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Collections;
using System.ComponentModel;
using FlightPak.Web.PreflightService;
using FlightPak.Web.FlightPakMasterService;
using System.Globalization;
using FlightPak.Web.Framework.Helpers.Preflight;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Drawing;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightPAXNewTemplate : BaseSecuredPage
    {

        #region comments
        ////Defect :2205 Please add Pax Billing Info fields missing from Preflight Pax Summary area 
        //Descrioption: Please add Pax Billing Info fields missing from Preflight Pax Summary area
        //Fixed By: Prabhu - Looks like this field is already implemented and for some reason it has been asked to hide and now revoking.

        //001: Fix By Prabhu: For Ordering the Pax summary list Jan 17, 2014
        //Defect :2276 Pax flight purpose, All Legs, is not updating properly. This occurs whether a new trip, an existing trip with passengers and add additional or existing trip with no passengers and add initially.Steps: 1) Select existing trip and click Edit 2) Go to PAX tab 3) If no PAX, select from PAX table OR if PAX, highlight one of them 4) Select a different Flight Purpose than the default (displayed) 5) Highlight a second passenger 6) All Legs flight purpose option does not refresh to "Select" 7) Re-select the same Flight Purpose as the 1st PAX 8) Flight Purpose does not change for 2nd PAX
        //Fixed By Prabhu - 05/02/2014. 
        #endregion

        #region Declarations
        public PreflightMain Trip = new PreflightMain();
        private ExceptionManager exManager;
        public int IDcount = 0;
        private delegate void SaveSession();
        private delegate void SavePAXSession();

        string DateFormat = "MM/dd/yyyy";

        // Declaration for Exception
        List<FlightPak.Web.PreflightService.RulesException> ErrorList = new List<FlightPak.Web.PreflightService.RulesException>();
        List<FlightPakMasterService.FlightPurpose> _flightpurpose = null;
        Dictionary<long, bool> LegFlightCategory = new Dictionary<long, bool>();
        List<GetAllFleetWithFilters> _getFleet = null;
        List<GetPassenger> _getPassenger = null;
        private int paxCount = 0;
        private int ACount = 0;
        private static int? maxpassenger;
        public static int Availcount = 0;
        public static int footerCnt = 0;

        private delegate void OutBoundInstructionSaveClick();
        private delegate void OutBoundInstructionCancelClick();

        protected void saveclick()
        {
            rdOutBoundInstructions.VisibleOnPageLoad = false;
        }
        protected void cancelclick()
        {
            rdOutBoundInstructions.VisibleOnPageLoad = false;
        }

        #endregion

        #region Page Function
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                //SaveSession SavePreflightSession = new SaveSession(SavePreflightToSession);
                SaveSession SavePreflightSession = new SaveSession(SavePreflightToSessionOnTabChange);
                Master.SaveToSession = SavePreflightSession;

                SavePAXSession SavePAXSession = new SavePAXSession(SavePreflightToSession);
                Master.SaveToSessionPAXCrew = SavePAXSession;

                Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;
                //Replicate Save Cancel Delete Buttons in header
                Master.SaveClick += btnSave_Click;
                Master.CancelClick += btnCancel_Click;
                Master.DeleteClick += btnDelete_Click;

                RadScriptManager.GetCurrent(Page).AsyncPostBackTimeout = 36000;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        

                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                        CalculateSeatTotalAndFlightCategory();

                        if (!IsPostBack)
                        {
                            if (pnlPaxSummary.Items.Count > 0)
                            {
                                pnlPaxSummary.Items[0].Expanded = false;
                            }


                            Master.ValidateLegExists();
                            RetrieveRoomType();
                            if (Session["AvailablePaxList"] != null)
                                Session.Remove("AvailablePaxList");
                            if (Session["PaxSummaryList"] != null)
                                Session.Remove("PaxSummaryList");

                            #region Authorization
                            CheckAutorization(Permission.Preflight.ViewPreflightPassenger);

                            if (!IsAuthorized(Permission.Preflight.ViewTripManager))
                            {
                                btnNext.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.DeleteTripManager))
                            {
                                btnDeleteTrip.Visible = false;
                            }
                            if (!IsAuthorized(Permission.Preflight.ViewPreflightChecklist))
                            {
                                lnkbtnChecklist.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightChecklist))
                            {
                                lnkbtnChecklist.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightOutboundInstruction))
                            {
                                lnkbtnOutbound.Visible = false;
                            }

                            if (lnkbtnOutbound.Visible == true || lnkbtnChecklist.Visible == true)
                                ChecklistOutboundTable.Visible = true;
                            else
                                ChecklistOutboundTable.Visible = false;
                            #endregion
                            //BindPreflightFromSession();
                            BindFlightPurpose(ddlFlightPurpose, 0, string.Empty);

                            rtsInfoLogistic.Tabs[0].Selected = true;
                            hdnPaxSelectedTab.Value = rtsInfoLogistic.SelectedIndex.ToString();


                            // hdnLeg.Value = "1";




                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                                if (!string.IsNullOrEmpty(Trip.VerifyNUM))
                                    tbTsaVerfiNum.Text = Trip.VerifyNUM;

                                //BindPreflighTabs(Trip, "pageload");

                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                BindPreflighTabs(Trip, "pageload");
                                if (Trip.PreflightLegs != null)
                                {
                                    PreflightLeg Firstleg = new PreflightLeg();
                                    Firstleg = Trip.PreflightLegs.Where(x => x.LegNUM == 1).SingleOrDefault();
                                    if (Firstleg != null)
                                    {
                                        hdnLeg.Value = (Trip.PreflightLegs.IndexOf(Firstleg) + 1).ToString();
                                        hdnLegNumHotel.Value = "1";
                                        rtsLegHotel.Tabs[0].Selected = true;
                                        rbArriveDepart.SelectedValue = "0";
                                        hdnrbArriveDepartPrev.Value = "0";
                                        hdnLegNumTransport.Value = "1";


                                        hdnArrivalICao.Value = Firstleg.ArriveICAOID.ToString();
                                        hdnDepartIcao.Value = Firstleg.DepartICAOID.ToString();
                                    }

                                    LoadHomebaseSettings(!IsPostBack);
                                    LoadPaxfromTrip();
                                }

                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                    //  RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAvailablePax, TableAllLegs, RadAjaxLoadingPanel1);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            if (Trip.Mode == TripActionMode.Edit)
                            {


                                btnSave.Enabled = true;
                                btnCancel.Enabled = true;
                                btnDeleteTrip.Enabled = false;
                                btnEditTrip.Enabled = false;

                                btnSave.CssClass = "button";
                                btnCancel.CssClass = "button";
                                btnDeleteTrip.CssClass = "button-disable";
                                btnEditTrip.CssClass = "button-disable";
                                disableenablefields(true);
                                //if (IsAuthorized(Permission.Preflight.AddPreflightPassengerTransport) || IsAuthorized(Permission.Preflight.EditPreflightPassengerTransport))
                                //{
                                //    DisableTransport(true);
                                //}
                                //else
                                //{
                                //    DisableTransport(false);
                                //}
                                //if (IsAuthorized(Permission.Preflight.AddPreflightPassengerHotel) || IsAuthorized(Permission.Preflight.EditPreflightPassengerHotel))
                                //{
                                //    DisableHotel(true);
                                //}
                                //else
                                //{
                                //    DisableHotel(false);
                                //}
                                if (IsAuthorized(Permission.Preflight.AddPreflightPassenger) || IsAuthorized(Permission.Preflight.EditPreflightPassenger))
                                {
                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        // ((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = true;
                                        if (Trip.PreflightLegs != null)
                                        {
                                            List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                            foreach (PreflightLeg Leg in Preflegs)
                                            {
                                                bool isdeadhead = false;
                                                if (LegFlightCategory.ContainsKey((long)Leg.LegNUM))
                                                    isdeadhead = LegFlightCategory[(long)Leg.LegNUM];
                                                DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Leg.LegNUM).ToString()].FindControl("ddlLeg" + (Leg.LegNUM).ToString());
                                                if (!isdeadhead)
                                                    ddlFP.Enabled = true;
                                                else
                                                    ddlFP.Enabled = false;
                                            }
                                        }
                                    }
                                    foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                                    {
                                        TextBox txtStreet = (TextBox)dataItem.FindControl("txtStreet");
                                        txtStreet.Enabled = true;
                                        TextBox txtPostal = (TextBox)dataItem.FindControl("txtPostal");
                                        txtPostal.Enabled = true;
                                        TextBox txtCity = (TextBox)dataItem.FindControl("txtCity");
                                        txtCity.Enabled = true;
                                        TextBox txtState = (TextBox)dataItem.FindControl("txtState");
                                        txtState.Enabled = true;
                                        TextBox tbPurpose = (TextBox)dataItem.FindControl("tbPurpose");
                                        tbPurpose.Enabled = true;
                                        TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                                        tbPassport.Enabled = false;
                                        TextBox tbBillingCode = (TextBox)dataItem.FindControl("tbBillingCode");
                                        tbBillingCode.Enabled = true;
                                    }
                                }
                                else
                                {
                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        ((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = false;
                                        if (Trip.PreflightLegs != null)
                                        {
                                            for (int i = 0; i < Trip.PreflightLegs.Count; i++)
                                            {
                                                if (Trip.PreflightLegs[i].IsDeleted == false)
                                                {
                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Trip.PreflightLegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Trip.PreflightLegs[i].LegNUM).ToString());
                                                    ddlFP.Enabled = false;
                                                }
                                            }
                                        }
                                    }
                                    foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                                    {
                                        TextBox txtStreet = (TextBox)dataItem.FindControl("txtStreet");
                                        txtStreet.Enabled = false;
                                        TextBox txtPostal = (TextBox)dataItem.FindControl("txtPostal");
                                        txtPostal.Enabled = false;
                                        TextBox txtCity = (TextBox)dataItem.FindControl("txtCity");
                                        txtCity.Enabled = false;
                                        TextBox txtState = (TextBox)dataItem.FindControl("txtState");
                                        txtState.Enabled = false;
                                        TextBox tbPurpose = (TextBox)dataItem.FindControl("tbPurpose");
                                        tbPurpose.Enabled = false;
                                        TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                                        tbPassport.Enabled = false;
                                        TextBox tbBillingCode = (TextBox)dataItem.FindControl("tbBillingCode");
                                        tbBillingCode.Enabled = false;
                                    }
                                }
                            }
                            else
                            {
                                btnSave.Enabled = false;
                                btnCancel.Enabled = false;
                                btnDeleteTrip.Enabled = true;
                                btnEditTrip.Enabled = true;

                                btnSave.CssClass = "button-disable";
                                btnCancel.CssClass = "button-disable";
                                btnDeleteTrip.CssClass = "button";
                                btnEditTrip.CssClass = "button";
                                disableenablefields(false);
                            }
                        }
                        else
                        {
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDeleteTrip.Enabled = false;
                            btnEditTrip.Enabled = false;
                            btnNext.Enabled = false;

                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDeleteTrip.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            btnNext.CssClass = "button-disable";
                            disableenablefields(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        #endregion

        #region common Methods and Events
        private void CalculateSeatTotalAndFlightCategory()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {

                        if (Trip.FleetID != null && Trip.FleetID != 0)
                        {
                            if (_getFleet == null)
                            {
                                _getFleet = FleetService.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, string.Empty, (long)Trip.FleetID).EntityList;
                            }
                            var FleetVal = _getFleet.Where(x => x.FleetID == (long)Trip.FleetID).ToList();
                            if (FleetVal.Count > 0)
                            {
                                if (Trip.PreflightLegs != null)
                                {
                                    foreach (PreflightLeg Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                                    {

                                        #region FleetTotal
                                        if (FleetVal[0].MaximumPassenger != null)
                                        {
                                            Leg.SeatTotal = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);
                                            maxpassenger = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);
                                            hdFleetCount.Value = Leg.SeatTotal.ToString();

                                        }
                                        else
                                        {
                                            Leg.SeatTotal = 0;
                                            hdFleetCount.Value = Leg.SeatTotal.ToString();
                                            maxpassenger = 0;
                                        }
                                        Leg.ReservationAvailable = Leg.SeatTotal - Leg.PassengerTotal;
                                        #endregion

                                        #region FlightCategory
                                        bool isdeadhead = false;
                                        if (Session["LegFlightCategory"] == null)
                                        {
                                            if (Leg.FlightCategoryID != null)
                                            {

                                                using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters>();

                                                    var objRetVal = objFlightCategoryService.GetAllFlightCategoryWithFilters(0, Convert.ToInt64(Leg.FlightCategoryID), string.Empty, true);
                                                    if (objRetVal.ReturnFlag == true)
                                                    {
                                                        FlightCatagoryLists = objRetVal.EntityList;
                                                        if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                            if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                                isdeadhead = true;
                                                    }
                                                }
                                            }
                                            if (!LegFlightCategory.ContainsKey((long)Leg.LegNUM))
                                            {
                                                LegFlightCategory.Add((long)Leg.LegNUM, isdeadhead);
                                            }
                                            Session["LegFlightCategory"] = LegFlightCategory;
                                        }
                                        else
                                            LegFlightCategory = (Dictionary<long, bool>)Session["LegFlightCategory"];

                                        #endregion
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Trip.PreflightLegs != null)
                            {
                                for (int i = 0; i < Trip.PreflightLegs.Count; i++)
                                {
                                    Trip.PreflightLegs[i].SeatTotal = 0;
                                    hdFleetCount.Value = Trip.PreflightLegs[i].SeatTotal.ToString();
                                }
                            }
                            maxpassenger = 0;
                        }
                    }
                }
            }
        }
        private void FillFlightPurpose(List<PassengerInLegs> paxInfoList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int itemCount = 0;
                foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                {
                    int counter = 1;
                    for (int leg = 0; leg < paxInfoList[itemCount].Legs.Count; leg++)
                    {
                        foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                        {
                            if (col.UniqueName == "Leg" + counter.ToString())
                            {
                                // Label lblLeg1p1 = (Label) 
                                DropDownList ddlFlightPurpose = (DropDownList)Item["Leg" + counter.ToString()].FindControl("ddlLeg" + counter.ToString());
                                ddlFlightPurpose.SelectedValue = paxInfoList[itemCount].Legs[leg].FlightPurposeID.ToString();

                                //ddlFlightPurpose.Attributes.Clear();
                                ddlFlightPurpose.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                                ddlFlightPurpose.Attributes.Add(System.Web.HttpUtility.HtmlEncode("previous"), paxInfoList[itemCount].Legs[leg].FlightPurposeID.ToString());
                                //ddlFlightPurpose.Attributes.Add("OnChange", "return CalculateLegCount(this," + counter.ToString() + ",'dropdown');");
                                break;
                            }
                        }
                        counter += 1;
                    }
                    itemCount += 1;
                }
            }
        }
        private void BindDefaultPurpose(List<PassengerInLegs> NewpassengerInLegs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewpassengerInLegs))
            {
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                if (Trip != null)
                {
                    if (Trip.PreflightLegs != null)
                    {
                        List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        for (int newlistcnt = 0; newlistcnt < NewpassengerInLegs.Count; newlistcnt++)
                        {
                            if (NewpassengerInLegs[newlistcnt].Purpose == string.Empty)
                            {
                                for (int i = 1; i <= Preflegs.Count; i++)
                                {
                                    bool isdeadhead = false;

                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");
                                        using (PreflightService.PreflightServiceClient PassengerService = new PreflightServiceClient())
                                        {
                                            //var PassengerList = PassengerService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID));

                                            var objRetValvalue = PassengerService.GetPassengerbyIDOrCD(Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID), string.Empty);
                                            if (objRetValvalue.ReturnFlag == true)
                                                _getPassenger = objRetValvalue.EntityList;

                                            if (_getPassenger != null && _getPassenger.Count > 0)
                                            {
                                                var PassengerList = _getPassenger.Where(x => x.PassengerRequestorID == Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID)).FirstOrDefault();

                                                if (PassengerList != null)
                                                {

                                                    if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID))
                                                    {

                                                        if (LegFlightCategory.ContainsKey((long)Preflegs[i - 1].LegNUM))
                                                        {
                                                            isdeadhead = LegFlightCategory[(long)Preflegs[i - 1].LegNUM];
                                                        }

                                                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i - 1].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i - 1].LegNUM).ToString());
                                                        HiddenField hdnFP = (HiddenField)dataItem["Leg" + (Preflegs[i - 1].LegNUM).ToString()].FindControl("hdnOldPurpose_" + (Preflegs[i - 1].LegNUM).ToString());
                                                        if (isdeadhead == false)
                                                        {
                                                            ddlFP.Enabled = true;
                                                            long Flightpurpose = PassengerList.FlightPurposeID != null ? (long)PassengerList.FlightPurposeID : 0;
                                                            ddlFP.SelectedValue = Flightpurpose.ToString();

                                                            //ddlFP.Attributes.Clear();
                                                            ddlFP.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                                                            ddlFP.Attributes.Add(System.Web.HttpUtility.HtmlEncode("previous"), Flightpurpose.ToString());
                                                            //ddlFP.Attributes.Add("OnChange", "return CalculateLegCount(this," + Preflegs[i - 1].LegNUM + ",'dropdown');");

                                                            hdnFP.Value = PassengerList.FlightPurposeID.ToString();
                                                            //UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                        }
                                                        else
                                                        {
                                                            ddlFP.SelectedValue = "0";
                                                            //ddlFP.Attributes.Clear();
                                                            ddlFP.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                                                            ddlFP.Attributes.Add(System.Web.HttpUtility.HtmlEncode("previous"), "0");
                                                            //ddlFP.Attributes.Add("OnChange", "return CalculateLegCount(this," + Preflegs[i - 1].LegNUM + ",'dropdown');");
                                                            hdnFP.Value = "0";
                                                            //UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                            ddlFP.Enabled = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void BindFlightPurpose(DropDownList ddl, int LegNum, string Code)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ddl, LegNum, Code))
            {
                ddl.Items.Clear();
                using (FlightPakMasterService.MasterCatalogServiceClient PurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (_flightpurpose == null)
                    {
                        var objRetVal = PurposeService.GetFlightPurposeList();
                        if (objRetVal.ReturnFlag == true)
                            _flightpurpose = objRetVal.EntityList;
                    }

                    foreach (FlightPakMasterService.FlightPurpose flightpurpose in _flightpurpose)
                    {
                        ddl.Items.Add(new ListItem(flightpurpose.FlightPurposeCD, flightpurpose.FlightPurposeID.ToString()));
                    }

                    if (ddl.ID == "ddlFlightPurpose")
                    {

                        //Defect : 2276
                        ddl.Items.Insert(0, new ListItem("All Legs", "-1"));
                        ddl.Items.Insert(1, new ListItem("Select", "0"));
                    }
                    else
                        ddl.Items.Insert(0, new ListItem("Select", "0"));

                    if (ddl.ID != "ddlFlightPurpose")
                    {
                        //ddl.Attributes.Clear();
                        ddl.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                        ddl.Attributes.Add(System.Web.HttpUtility.HtmlEncode("previous"), "0");
                        //ddl.Attributes.Add("OnChange", "return CalculateLegCount(this," + LegNum.ToString() + ",'dropdown');");
                    }
                    //if (LegNum != 0)
                    //{
                    //    ddl.Attributes.Add("onChange", "return OnDropDownSelectedIndexChanged(this," + LegNum.ToString() + ", " + Code + " );");
                    //}
                }
            }
        }
        private void disableenablefields(bool status)
        {
            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                lnkPaxRoaster.Visible = status;
                // tbPaxnotes.Enabled = status;
                btnAddremoveColumns.Enabled = status;
                //dgPaxSummary.Enabled = status;
                tbTsaVerfiNum.Enabled = status;
                tbStaus.Enabled = status;
                ddlFlightPurpose.Enabled = status;
                SearchBox.Enabled = status;
                // dgAvailablePax.Enabled = status;

                //tbSpecialInstructions.Enabled = status;

                btnCheckTsa.Enabled = status;

                if (status)
                {
                    btnCheckTsa.CssClass = "button";
                }
                else
                {
                    btnCheckTsa.CssClass = "button-disable";
                }
                foreach (GridNoRecordsItem item in dgAvailablePax.MasterTableView.GetItems(GridItemType.NoRecordsItem))
                {
                    GridCommandItem commandItem = (GridCommandItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    LinkButton lnkButton = (LinkButton)commandItem.FindControl("lnkPaxDelete");
                    lnkButton.Enabled = status;

                    System.Web.UI.HtmlControls.HtmlAnchor insertlink = (System.Web.UI.HtmlControls.HtmlAnchor)commandItem.FindControl("aInsertCrew");
                    if (insertlink != null)
                    {
                        if (status)
                            insertlink.Attributes.Add("onclick", "ShowPaxInfoPopup('insert')");
                        else
                            insertlink.Attributes.Add("onclick", string.Empty);

                    }
                }
                foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                {

                    GridCommandItem commandItem = (GridCommandItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    LinkButton lnkButton = (LinkButton)commandItem.FindControl("lnkPaxDelete");
                    //((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = status;
                    lnkButton.Enabled = status;

                    System.Web.UI.HtmlControls.HtmlAnchor insertlink = (System.Web.UI.HtmlControls.HtmlAnchor)commandItem.FindControl("aInsertCrew");
                    if (insertlink != null)
                    {
                        if (status)
                            insertlink.Attributes.Add("onclick", "ShowPaxInfoPopup('insert')");
                        else
                            insertlink.Attributes.Add("onclick", string.Empty);

                    }

                    if (Trip != null)
                    {
                        if (Trip.PreflightLegs != null)
                        {
                            List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                bool isdeadhead = false;
                                if (LegFlightCategory.ContainsKey((long)Preflegs[i].LegNUM))
                                {
                                    isdeadhead = LegFlightCategory[(long)Preflegs[i].LegNUM];
                                }

                                DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                if (!isdeadhead)
                                {
                                    ddlFP.Enabled = status;
                                }
                                else
                                {
                                    ddlFP.Enabled = false;
                                }
                            }
                        }
                    }
                }

                foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                {
                    if (Trip != null)
                    {
                        if (Trip.PreflightLegs != null)
                        {
                            List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                if (Preflegs[i].IsDeleted == false)
                                {
                                    TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("tbLeg" + (Preflegs[i].LegNUM).ToString());
                                    txtBlockedSeats.Enabled = status;


                                }
                            }
                        }
                    }
                }
                foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                {
                    TextBox txtStreet = (TextBox)dataItem.FindControl("txtStreet");
                    txtStreet.Enabled = status;
                    TextBox txtPostal = (TextBox)dataItem.FindControl("txtPostal");
                    txtPostal.Enabled = status;
                    TextBox txtCity = (TextBox)dataItem.FindControl("txtCity");
                    txtCity.Enabled = status;
                    TextBox txtState = (TextBox)dataItem.FindControl("txtState");
                    txtState.Enabled = status;
                    TextBox tbPurpose = (TextBox)dataItem.FindControl("tbPurpose");
                    tbPurpose.Enabled = status;
                    TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                    tbPassport.Enabled = false;//status;
                    TextBox tbBillingCode = (TextBox)dataItem.FindControl("tbBillingCode");
                    tbBillingCode.Enabled = status;
                }
                disableEnableTransportFields(status);
            }
        }
        private static DataTable ConvertToDataTable<T>(IList<T> list) where T : class
        {
            int count = 1;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DataTable table = CreateDataTable<T>();
                Type objType = typeof(T);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (T item in list)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor property in properties)
                    {
                        if (!CanUseType(property.PropertyType)) continue;
                        row[property.Name] = property.GetValue(item) ?? DBNull.Value;
                    }
                    row["ID"] = count;
                    table.Rows.Add(row);
                    count = count + 1;
                }

                return table;
            }
        }
        private static DataTable CreateDataTable<T>() where T : class
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Type objType = typeof(T);
                DataTable table = new DataTable(objType.Name);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (PropertyDescriptor property in properties)
                {
                    Type propertyType = property.PropertyType;
                    if (!CanUseType(propertyType)) continue;


                    //nullables must use underlying types
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        propertyType = Nullable.GetUnderlyingType(propertyType);
                    //enums also need special treatment
                    if (propertyType.IsEnum)
                        propertyType = Enum.GetUnderlyingType(propertyType);
                    table.Columns.Add(property.Name, propertyType);
                }
                table.Columns.Add("ID");
                return table;
            }
        }
        private static bool CanUseType(Type propertyType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(propertyType))
            {
                //only strings and value types
                if (propertyType.IsArray) return false;
                if (!propertyType.IsValueType && propertyType != typeof(string)) return false;
                return true;
            }
        }
        protected DataTable DeleteDuplicateFromDataTable(DataTable dtDuplicate, string columnName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable hashT = new Hashtable();
                ArrayList arrDuplicate = new ArrayList();
                foreach (DataRow row in dtDuplicate.Rows)
                {
                    if (hashT.Contains(row[columnName]))
                        arrDuplicate.Add(row);
                    else
                        hashT.Add(row[columnName], string.Empty);
                }
                foreach (DataRow row in arrDuplicate)
                    dtDuplicate.Rows.Remove(row);

                return dtDuplicate;
            }
        }
        protected void ResetHomebaseSettings()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnPaxNotes.Value = "false";
            }
        }
        protected void LoadHomebaseSettings(bool InitialPageReq)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InitialPageReq))
            {
                ResetHomebaseSettings();
                hdnPaxNotes.Value = UserPrincipal.Identity._fpSettings._IsPNotes.ToString();
            }
        }
        protected void BindPreflighTabs(PreflightMain preFlightTrip, string eventraised)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                rtsLegHotel.Tabs.Clear();
                rtsLegTransport.Tabs.Clear();

                if (preFlightTrip.PreflightLegs != null && preFlightTrip.PreflightLegs.Count() > 0)
                {
                    List<PreflightLeg> PreflightLegList = preFlightTrip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    foreach (PreflightLeg Leg in PreflightLegList)
                    {
                        if (Leg.State != PreflightService.TripEntityState.Deleted)
                        {
                            RadTab rtHotel = new RadTab();
                            rtHotel.Text = "Leg " + Leg.LegNUM.ToString();
                            rtHotel.Value = Leg.LegNUM.Value.ToString(); // preFlightTrip.PreflightLegs.IndexOf(Leg).ToString();

                            RadTab rtTransport = new RadTab();
                            rtTransport.Text = "Leg " + Leg.LegNUM.ToString();
                            rtTransport.Value = Leg.LegNUM.Value.ToString(); // preFlightTrip.PreflightLegs.IndexOf(Leg).ToString();

                            rtsLegHotel.Tabs.Add(rtHotel);
                            rtsLegTransport.Tabs.Add(rtTransport);

                            if (Leg.ArriveICAOID != null && Leg.DepartICAOID != null)
                            {
                                if (Leg.Airport1 != null && Leg.Airport != null)
                                {
                                    rtsLegHotel.Tabs[(int)Leg.LegNUM.Value - 1].Text = "Leg " + Leg.LegNUM.Value.ToString() + " (" + Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID + ")";
                                    rtsLegTransport.Tabs[(int)Leg.LegNUM.Value - 1].Text = "Leg " + Leg.LegNUM.Value.ToString() + " (" + Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID + ")";
                                }
                                else
                                {
                                    rtsLegHotel.Tabs[(int)Leg.LegNUM.Value - 1].Text = "Leg " + Leg.LegNUM.Value.ToString();
                                    rtsLegTransport.Tabs[(int)Leg.LegNUM.Value - 1].Text = "Leg " + Leg.LegNUM.Value.ToString();
                                }
                            }
                            else
                            {
                                rtsLegHotel.Tabs[(int)Leg.LegNUM.Value - 1].Text = "Leg " + Leg.LegNUM.Value.ToString();
                                rtsLegTransport.Tabs[(int)Leg.LegNUM.Value - 1].Text = "Leg " + Leg.LegNUM.Value.ToString();
                            }
                        }
                    }
                }
                else
                {
                    RadTab Rt = new RadTab();
                    Rt.Text = "Leg 1";
                    Rt.Value = "0";
                    rtsLegHotel.Tabs.Add(Rt);
                }
                if (rtsLegHotel.Tabs.Count > 1)
                {
                    if (eventraised.ToLower() == "pageload")

                        rtsLegHotel.Tabs[0].Selected = true;
                    else
                        rtsLegHotel.Tabs.FindTabByValue((Convert.ToInt16(hdnLeg.Value) - 1).ToString()).Selected = true;
                }
                else
                {
                    rtsLegHotel.Tabs[0].Selected = true;
                }

                if (rtsLegTransport.Tabs.Count > 1)
                {
                    if (eventraised.ToLower() == "pageload")

                        rtsLegTransport.Tabs[0].Selected = true;
                    else
                        rtsLegTransport.Tabs.FindTabByValue((Convert.ToInt16(hdnLeg.Value) - 1).ToString()).Selected = true;
                }
                else
                {
                    rtsLegTransport.Tabs[0].Selected = true;
                }

            }

        }
        protected void Alert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ViewState["AlertType"] != null)
                        {
                            switch (ViewState["AlertType"].ToString())
                            {
                                case "EmpType_Changed":
                                    break;
                                case "dgAvailablePax_DeleteCommand":
                                    break;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            Master.EditTripEvent();
        }
        protected void rtsInfoLogistic_OnTabClick(object sender, RadTabStripEventArgs e)
        {
            if (hdnPaxSelectedTab.Value == "0")
            {

                #region "PAX Info tabs is selected previously so save the details to session"

                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);

                string PaxpurposeErrorstr = string.Empty;
                PaxpurposeErrorstr = PaxPurposeError();

                if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                    RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmInfoTabCallBackFn", 330, 100, null, "Confirmation!");
                else
                {
                    #region Save PAX to session and load the selected tab
                    hdnPaxSelectedTab.Value = rtsInfoLogistic.SelectedIndex.ToString();
                    SavePreflightToSession();
                    if (rtsInfoLogistic.SelectedIndex == 0)
                    {
                        pnlPax.Visible = true;
                        pnlPaxLogistics.Visible = false;
                    }
                    else if (rtsInfoLogistic.SelectedIndex == 1)
                    {
                        pnlPax.Visible = false;
                        pnlPaxLogistics.Visible = true;
                        rtsLogistics.Tabs[0].Selected = true;
                        hdnPaxLogisticsSelectedTab.Value = rtsLogistics.SelectedIndex.ToString();
                        //load hotel details here
                        //dynamically load first leg tab and hotel details here
                        rtsLegHotel.Tabs[0].Selected = true;
                        hdnLegNumHotel.Value = "1";

                        RadMultiPage1.SelectedIndex = 0;
                        BindLegPax(true);
                        BindLegHotel(true);
                        EnableHotelForm(false);
                        rbArriveDepart.SelectedValue = "0";
                        hdnrbArriveDepartPrev.Value = "0";
                        UpdateHiddenAirportIDsBasedOnLegnum(1);
                        ClearHotel();
                        if (dgLegHotel.Items.Count > 0)
                        {
                            dgLegHotel.Items[0].Selected = true;
                            GridDataItem HotelItem = (GridDataItem)dgLegHotel.Items[0];
                            hdnHotelIdentifier.Value = HotelItem.GetDataKeyValue("HotelIdentifier").ToString();
                            LoadHotel();
                            GridEnable(true, true, true);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                #region Crew Logistics tab is selected previously so store hotel/Transport based on the selected tab and load the new tab.

                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (hdnPaxLogisticsSelectedTab.Value == "1") //If previously selected tab is transport then save transport based on the hdnlegnumTranspoert
                    {

                        #region If previously selected tab is transport then save transport based on the hdnLegnumTransport
                        if (IsAuthorized(Permission.Preflight.AddPreflightCrewTransport) || IsAuthorized(Permission.Preflight.EditPreflightCrewTransport))
                        {
                            PreflightLeg LegtoSaveTransport = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();
                            if (LegtoSaveTransport != null)
                            {
                                SaveTransport(LegtoSaveTransport);
                            }
                        }

                        Session["CurrentPreFlightTrip"] = Trip;

                        hdnPaxSelectedTab.Value = rtsInfoLogistic.SelectedIndex.ToString();

                        if (rtsInfoLogistic.SelectedIndex == 0)
                        {
                            LoadPaxfromTrip();
                            pnlPax.Visible = true;
                            pnlPaxLogistics.Visible = false;
                        }
                        else if (rtsInfoLogistic.SelectedIndex == 1)
                        {
                            #region hotel selected
                            pnlPax.Visible = false;
                            pnlPaxLogistics.Visible = true;
                            rtsLogistics.Tabs[0].Selected = true;
                            hdnPaxLogisticsSelectedTab.Value = rtsLogistics.SelectedIndex.ToString();
                            //load hotel details here
                            //dynamically load first leg tab and hotel details here
                            rtsLegHotel.Tabs[0].Selected = true;
                            hdnLegNumHotel.Value = "1";
                    
                            rtsLegHotel.Tabs[0].Selected = true;
                            rbArriveDepart.SelectedValue = "0";
                            hdnrbArriveDepartPrev.Value = "0";
                            UpdateHiddenAirportIDsBasedOnLegnum(1);
                            BindLegPax(true);
                            BindLegHotel(true);
                            EnableHotelForm(false);
                            RadMultiPage1.SelectedIndex = 0;
                            ClearHotel();
                            if (dgLegHotel.Items.Count > 0)
                            {
                                dgLegHotel.Items[0].Selected = true;
                                GridDataItem HotelItem = (GridDataItem)dgLegHotel.Items[0];
                                hdnHotelIdentifier.Value = HotelItem.GetDataKeyValue("HotelIdentifier").ToString();
                                LoadHotel();
                                GridEnable(true, true, true);
                            }
                            #endregion

                        }

                        #endregion
                    }
                    else if (hdnPaxLogisticsSelectedTab.Value == "0") // if previously selected tab is hotel then save hotel
                    {
                        #region if previously selected tab is hotel then save hotel based on hdnlegnumHotel

                        if (btnSaveHotel.Enabled)
                        {
                            rtsInfoLogistic.Tabs[Convert.ToInt16(hdnPaxSelectedTab.Value)].Selected = true;
                            RadWindowManager1.RadAlert("Please save or cancel hotel information", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                        }
                        else
                        {
                            hdnPaxSelectedTab.Value = rtsInfoLogistic.SelectedIndex.ToString();

                            if (rtsInfoLogistic.SelectedIndex == 0)
                            {
                                LoadPaxfromTrip();
                                pnlPax.Visible = true;
                                pnlPaxLogistics.Visible = false;
                            }
                            else if (rtsInfoLogistic.SelectedIndex == 1)
                            {
                                #region hotel selected
                                pnlPax.Visible = false;
                                pnlPaxLogistics.Visible = true;
                                rtsLogistics.Tabs[0].Selected = true;
                                hdnPaxLogisticsSelectedTab.Value = rtsLogistics.SelectedIndex.ToString();
                                //load hotel details here
                                //dynamically load first leg tab and hotel details here
                                rtsLegHotel.Tabs[0].Selected = true;
                                hdnLegNumHotel.Value = "1";

                                rtsLegHotel.Tabs[0].Selected = true;
                                rbArriveDepart.SelectedValue = "0";
                                hdnrbArriveDepartPrev.Value = "0";
                                UpdateHiddenAirportIDsBasedOnLegnum(1);
                                BindLegPax(true);
                                BindLegHotel(true);
                                EnableHotelForm(false);
                                RadMultiPage1.SelectedIndex = 0;
                                ClearHotel();
                                if (dgLegHotel.Items.Count > 0)
                                {
                                    dgLegHotel.Items[0].Selected = true;
                                    GridDataItem HotelItem = (GridDataItem)dgLegHotel.Items[0];
                                    hdnHotelIdentifier.Value = HotelItem.GetDataKeyValue("HotelIdentifier").ToString();
                                    LoadHotel();
                                    GridEnable(true, true, true);
                                }
                                #endregion

                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
        }
        protected void btnInfoTabYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (IsAuthorized(Permission.Preflight.AddPreflightCrew) || IsAuthorized(Permission.Preflight.EditPreflightCrew))
                    {
                        UpdateHeaderInformationBasedOnAvailablePAXGrid();
                        Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        SavePreflightToSession();
                        if (hdnPaxSelectedTab.Value == "0")
                        {
                            hdnPaxSelectedTab.Value = rtsInfoLogistic.SelectedIndex.ToString();
                            rtsInfoLogistic.Tabs[1].Selected = true;
                            pnlPax.Visible = false;
                            pnlPaxLogistics.Visible = true;
                            rtsLogistics.Tabs[0].Selected = true;
                            hdnPaxLogisticsSelectedTab.Value = rtsLogistics.SelectedIndex.ToString();
                            hdnLegNumHotel.Value = "1";

                            RadMultiPage1.SelectedIndex = 0;
                            rtsLegHotel.Tabs[0].Selected = true;
                            BindLegPax(true);
                            BindLegHotel(true);
                            EnableHotelForm(false);
                            rbArriveDepart.SelectedValue = "0";
                            hdnrbArriveDepartPrev.Value = "0";
                        }

                    }


                }
            }
        }
        protected void btnInfoTabNo_Click(object sender, EventArgs e)
        {
            if (hdnPaxSelectedTab.Value == "0")
            {
                rtsInfoLogistic.Tabs[0].Selected = true;
                pnlPax.Visible = true;
                pnlPaxLogistics.Visible = false;
            }
        }
        protected void btnOpenOutboundInstructions_Click(object sender, EventArgs e)
        {
            rdOutBoundInstructions.VisibleOnPageLoad = true;
        }
        #endregion

        #region Pax Available Grid
        private void UpdateHeaderInformationBasedOnAvailablePAXGrid()
        {
            if (Session["CurrentPreFlightTrip"] != null)
            {
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                if (Trip.PreflightLegs != null)
                {
                    GridFooterItem footerItem = (GridFooterItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer)[0];
                    GridHeaderItem headerItem = (GridHeaderItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.Header)[0];
                    List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    for (int legCnt = 0; legCnt < Preflegs.Count; legCnt++)
                    {
                        int NoofPaxwithPurpose = 0;
                        foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                        {
                            DropDownList ddlFP = (DropDownList)dataItem["Leg" + Preflegs[legCnt].LegNUM.ToString()].FindControl("ddlLeg" + Preflegs[legCnt].LegNUM.ToString());
                            if (ddlFP.SelectedValue != "0")
                                NoofPaxwithPurpose++;
                        }

                        string LegNum = "Leg" + Preflegs[legCnt].LegNUM.ToString();
                        string paxNum = "lblLeg" + Preflegs[legCnt].LegNUM.ToString() + "p" + Preflegs[legCnt].LegNUM.ToString();
                        string maxNum = "lblLeg" + Preflegs[legCnt].LegNUM.ToString() + "a" + Preflegs[legCnt].LegNUM.ToString();
                        string hdnPax = "hdnLeg" + Preflegs[legCnt].LegNUM.ToString() + "Pax";
                        string hdnAvail = "hdnLeg" + Preflegs[legCnt].LegNUM.ToString() + "Avail";
                        //string Paxid = dataItem["PassengerRequestorID"].Text;
                        //string PaxCode = lnkPaxCode.Text;
                        //string PaxName = dataItem["Name"].Text;
                        int legnumber = (int)Preflegs[legCnt].LegNUM;


                        footerCnt = 0;

                        TextBox tbLeg = (TextBox)footerItem[LegNum].FindControl("tbLeg" + legnumber);
                        if (!string.IsNullOrEmpty(tbLeg.Text))
                            footerCnt = Convert.ToInt16(tbLeg.Text);


                        int? PasssengerTotal = NoofPaxwithPurpose + footerCnt;


                        Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                        Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 
                        HiddenField hdnLegP = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                        HiddenField hdnLegA = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);
                        hdnLegP.Value = "0";

                        if (!string.IsNullOrEmpty(maxpassenger.ToString()))
                        {
                            hdnLegA.Value = maxpassenger.ToString();
                        }
                        else
                        {
                            hdnLegA.Value = "0";
                        }

                        if (footerCnt != 0)
                        {
                            hdnLegP.Value = (Convert.ToInt16(hdnLegP.Value) + footerCnt + NoofPaxwithPurpose).ToString();
                            hdnLegA.Value = (Convert.ToInt16(hdnLegA.Value) - (footerCnt + NoofPaxwithPurpose)).ToString();
                        }
                        else
                        {
                            hdnLegP.Value = (Convert.ToInt16(hdnLegP.Value) + NoofPaxwithPurpose).ToString();
                            hdnLegA.Value = (Convert.ToInt16(hdnLegA.Value) - NoofPaxwithPurpose).ToString();
                        }

                        paxCount = Convert.ToInt32(hdnLegP.Value);
                        ACount = Convert.ToInt32(hdnLegA.Value);
                        int Total = Convert.ToInt16(PasssengerTotal - NoofPaxwithPurpose);

                        lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                        lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                        hdnLegP.Value = paxCount.ToString();
                        hdnLegA.Value = ACount.ToString();
                        ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                        ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();

                        Preflegs[legCnt].PassengerTotal = PasssengerTotal;
                        Preflegs[legCnt].ReservationAvailable = ACount;

                        ((HiddenField)DivExternalForm.FindControl("hdleg" + Preflegs[legCnt].LegNUM + "PaxCount")).Value = NoofPaxwithPurpose.ToString();// PasssengerTotal.ToString();
                        //((HiddenField)DivExternalForm.FindControl("hdleg" + Preflegs[legCnt].LegNUM + "PaxCount")).Value = PasssengerTotal.ToString();
                        ((HiddenField)DivExternalForm.FindControl("hdleg" + Preflegs[legCnt].LegNUM + "Avail")).Value = hdnLegA.Value;


                    }
                }
                Session["CurrentPreFlightTrip"] = Trip;
            }
        }
        private void SearchPaxAvailable()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    bool alreadyExists = false;
                    List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> OldpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                    List<GetAllPreFlightAvailablePaxList> _list = null;

                    //_list = RetrievePaxList();
                    if (Session["SelectedPax"] != null || !string.IsNullOrEmpty(SearchBox.Text))
                    {
                        DataTable dt = (DataTable)Session["SelectedPax"];
                        if (Session["AvailablePaxList"] != null)
                            OldpassengerInLegs = (List<PassengerInLegs>)Session["AvailablePaxList"];



                        if (!string.IsNullOrEmpty(SearchBox.Text))
                        {
                            Int32 MaxORderNum = 0;
                            if (OldpassengerInLegs != null)
                            {


                                alreadyExists = OldpassengerInLegs.Any(x => x.Code.ToUpper().Trim() == SearchBox.Text.ToUpper().Trim());

                                if (OldpassengerInLegs.Count > 0)
                                {
                                    MaxORderNum = OldpassengerInLegs.Max(x => x.OrderNUM);
                                }
                                MaxORderNum = MaxORderNum + 1;
                            }

                            if (alreadyExists == false)
                            {
                                GetAllPreFlightAvailablePaxList AvailableList = new GetAllPreFlightAvailablePaxList();
                                AvailableList = null;
                                using (PreflightServiceClient prefsevice = new PreflightServiceClient())
                                {
                                    var objretval = prefsevice.GetAllPreflightAvailablePaxListByIDORCD(0, SearchBox.Text.ToLower().Trim());
                                    if (objretval.ReturnFlag)
                                    {
                                        _list = objretval.EntityList;
                                        if (_list != null && _list.Count > 0)
                                            AvailableList = _list[0];
                                    }
                                }
                                if (AvailableList != null)
                                {

                                    PassengerInLegs paxInfo = new PassengerInLegs();
                                    paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                    paxInfo.Code = AvailableList.Code;
                                    paxInfo.Name = AvailableList.Name;
                                    paxInfo.Passport = AvailableList.Passport;
                                    paxInfo.State = AvailableList.State;
                                    paxInfo.Street = AvailableList.Street;
                                    paxInfo.Status = AvailableList.TripStatus;
                                    paxInfo.City = AvailableList.City;
                                    paxInfo.Purpose = "0";
                                    paxInfo.BillingCode = AvailableList.BillingCode;
                                    paxInfo.Notes = AvailableList.Notes;
                                    paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                    paxInfo.PassportID = AvailableList.PassportID.ToString();
                                    paxInfo.VisaID = AvailableList.VisaID.ToString();
                                    paxInfo.Postal = AvailableList.Postal;
                                    //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                                    if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                    {
                                        DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                        if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                            paxInfo.PassportExpiryDT = dtDate.ToString();
                                        else
                                            paxInfo.PassportExpiryDT = string.Empty;
                                    }
                                    paxInfo.Nation = AvailableList.Nation;
                                    paxInfo.OrderNUM = MaxORderNum;
                                    OldpassengerInLegs.Add(paxInfo);
                                    SearchBox.Text = string.Empty;
                                }
                            }
                        }
                        if (dt != null)
                        {
                            for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                            {


                                GetAllPreFlightAvailablePaxList AvailableList = new GetAllPreFlightAvailablePaxList();
                                AvailableList = null;
                                Int32 MaxORderNum = 0;
                                if (OldpassengerInLegs != null)
                                {

                                    alreadyExists = OldpassengerInLegs.Any(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]));
                                    if (OldpassengerInLegs.Count > 0)
                                    {
                                        MaxORderNum = OldpassengerInLegs.Max(x => x.OrderNUM);
                                    }
                                    MaxORderNum = MaxORderNum + 1;
                                }

                                if (alreadyExists == false && string.IsNullOrEmpty(SearchBox.Text))
                                {
                                    using (PreflightServiceClient prefsevice = new PreflightServiceClient())
                                    {
                                        var objretval = prefsevice.GetAllPreflightAvailablePaxListByIDORCD(Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]), string.Empty);
                                        if (objretval.ReturnFlag)
                                        {
                                            _list = objretval.EntityList;
                                            if (_list != null && _list.Count > 0)
                                                AvailableList = _list[0];
                                        }
                                    }
                                    if (AvailableList != null)
                                    {
                                        PassengerInLegs paxInfo = new PassengerInLegs();
                                        paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                        paxInfo.Code = AvailableList.Code;
                                        paxInfo.Name = AvailableList.Name;
                                        paxInfo.Passport = AvailableList.Passport;
                                        paxInfo.State = AvailableList.State;
                                        paxInfo.Street = AvailableList.Street;
                                        paxInfo.Status = AvailableList.TripStatus;
                                        paxInfo.City = AvailableList.City;
                                        paxInfo.Purpose = string.Empty;
                                        paxInfo.BillingCode = AvailableList.BillingCode;
                                        paxInfo.Notes = AvailableList.Notes;
                                        paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                        paxInfo.PassportID = AvailableList.PassportID.ToString();
                                        paxInfo.VisaID = AvailableList.VisaID.ToString();
                                        paxInfo.Postal = AvailableList.Postal;
                                        //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                paxInfo.PassportExpiryDT = dtDate.ToString();
                                            else
                                                paxInfo.PassportExpiryDT = string.Empty;
                                        }
                                        paxInfo.Nation = AvailableList.Nation;
                                        paxInfo.OrderNUM = MaxORderNum;
                                        OldpassengerInLegs.Add(paxInfo);
                                    }
                                }
                            }
                        }




                        if (OldpassengerInLegs != null)
                            FinalpassengerInLegs = NewpassengerInLegs.Concat(OldpassengerInLegs).ToList();

                        Session.Remove("SelectedPax");
                        Session["AvailablePaxList"] = FinalpassengerInLegs;
                        dgAvailablePax.DataSource = FinalpassengerInLegs;
                        dgAvailablePax.DataBind();

                        //BindFlightPurpose(FinalpassengerInLegs);
                        BindDefaultPurpose(OldpassengerInLegs);
                    }
                }
            }
        }
        private void SearchPaxAvailableforInsert()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    bool alreadyExists = false;
                    List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> OldpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                    List<GetAllPreFlightAvailablePaxList> _list = null;

                    //_list = RetrievePaxList();
                    if (Session["SelectedPax"] != null)
                    {
                        DataTable dt = (DataTable)Session["SelectedPax"];
                        if (Session["AvailablePaxList"] != null)
                            OldpassengerInLegs = (List<PassengerInLegs>)Session["AvailablePaxList"];


                        List<PassengerInLegs> FirstPaxList = new List<PassengerInLegs>();
                        List<PassengerInLegs> RestPaxList = new List<PassengerInLegs>();

                        //if (_list != null)
                        //{

                        if (OldpassengerInLegs != null)
                        {

                            Int32 InsertBefore = 1;
                            if (dgAvailablePax.SelectedItems.Count > 0)
                            {
                                InsertBefore = (Int32)((GridDataItem)dgAvailablePax.SelectedItems[0]).GetDataKeyValue("OrderNUM");
                            }

                            foreach (PassengerInLegs Paxinlegs in OldpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                            {
                                if (Paxinlegs.OrderNUM < InsertBefore)
                                    FirstPaxList.Add(Paxinlegs);
                                else
                                    RestPaxList.Add(Paxinlegs);
                            }
                        }

                        if (dt != null)
                        {
                            for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                            {


                                Int32 MaxORderNum = 0;
                                if (FirstPaxList != null)
                                {

                                    alreadyExists = OldpassengerInLegs.Any(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]));
                                    if (FirstPaxList.Count > 0)
                                    {
                                        MaxORderNum = FirstPaxList.Max(x => x.OrderNUM);
                                    }
                                }
                                MaxORderNum = MaxORderNum + 1;
                                if (alreadyExists == false)
                                {
                                    GetAllPreFlightAvailablePaxList AvailableList = new GetAllPreFlightAvailablePaxList();
                                    AvailableList = null;
                                    using (PreflightServiceClient prefsevice = new PreflightServiceClient())
                                    {
                                        var objretval = prefsevice.GetAllPreflightAvailablePaxListByIDORCD(Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]), string.Empty);
                                        if (objretval.ReturnFlag)
                                        {
                                            _list = objretval.EntityList;
                                            if (_list != null && _list.Count > 0)
                                                AvailableList = _list[0];

                                        }
                                    }

                                    if (AvailableList != null)
                                    {
                                        PassengerInLegs paxInfo = new PassengerInLegs();
                                        paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                        paxInfo.Code = AvailableList.Code;
                                        paxInfo.Name = AvailableList.Name;
                                        paxInfo.Passport = AvailableList.Passport;
                                        paxInfo.State = AvailableList.State;
                                        paxInfo.Street = AvailableList.Street;
                                        paxInfo.Status = AvailableList.TripStatus;
                                        paxInfo.City = AvailableList.City;
                                        paxInfo.Purpose = string.Empty;
                                        paxInfo.BillingCode = AvailableList.BillingCode;
                                        paxInfo.Notes = AvailableList.Notes;
                                        paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                        paxInfo.PassportID = AvailableList.PassportID.ToString();
                                        paxInfo.VisaID = AvailableList.VisaID.ToString();
                                        paxInfo.Postal = AvailableList.Postal;
                                        //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                paxInfo.PassportExpiryDT = dtDate.ToString();
                                            else
                                                paxInfo.PassportExpiryDT = string.Empty;
                                        }
                                        paxInfo.Nation = AvailableList.Nation;
                                        paxInfo.OrderNUM = MaxORderNum;
                                        FirstPaxList.Add(paxInfo);
                                    }
                                }
                            }
                        }

                        //}

                        if (FirstPaxList != null)
                        {
                            if (RestPaxList != null)
                            {
                                foreach (PassengerInLegs nextpax in RestPaxList.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    Int32 MaxORderNum = 0;
                                    if (FirstPaxList.Count > 0)
                                        MaxORderNum = FirstPaxList.Max(x => x.OrderNUM);
                                    MaxORderNum++;
                                    nextpax.OrderNUM = MaxORderNum;
                                    FirstPaxList.Add(nextpax);
                                }
                            }
                            List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
                            Oldpassengerlist = (List<PassengerSummary>)Session["PaxSummaryList"];

                            if (Oldpassengerlist != null)
                            {
                                foreach (PassengerInLegs pax in FirstPaxList.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    foreach (PassengerSummary PaxSum in Oldpassengerlist.Where(x => x.PaxID == pax.PassengerRequestorID).ToList())
                                    {
                                        PaxSum.OrderNum = pax.OrderNUM;
                                    }
                                }
                            }
                            Session["PaxSummaryList"] = Oldpassengerlist;
                        }

                        if (OldpassengerInLegs != null)
                            FinalpassengerInLegs = NewpassengerInLegs.Concat(FirstPaxList).ToList();

                        Session.Remove("SelectedPax");
                        Session["AvailablePaxList"] = FinalpassengerInLegs;
                        dgAvailablePax.DataSource = FinalpassengerInLegs;
                        dgAvailablePax.DataBind();

                        //BindFlightPurpose(FinalpassengerInLegs);
                        BindDefaultPurpose(FirstPaxList);
                    }
                }
            }
        }
        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderInformationBasedOnAvailablePAXGrid();
                        SavePreflightToSession();
                        SearchPaxAvailable();
                        UpdateHeaderInformationBasedOnAvailablePAXGrid();

                        if (pnlPaxSummary.Items.Count > 0)
                        {
                            pnlPaxSummary.Items[0].Expanded = false;
                        }

                        SearchBox.Text = string.Empty;
                        RadAjaxManager.GetCurrent(Page).FocusControl(Submit1);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgAvailablePax_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["AvailablePaxList"] != null)
                        {
                            NewpassengerInLegs = (List<PassengerInLegs>)Session["AvailablePaxList"];
                            //SavePreflightToSession(); // check
                            dgAvailablePax.VirtualItemCount = NewpassengerInLegs.Count;
                            dgAvailablePax.DataSource = NewpassengerInLegs;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgAvailablePax_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Int32 FleetCount = 0;
                        if (!string.IsNullOrEmpty(hdFleetCount.Value))
                            FleetCount = Convert.ToInt32(hdFleetCount.Value);
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            if (Trip.PreflightLegs != null)
                            {
                                List<PreflightLeg> Preflightlegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                for (int icount = 1; icount <= Preflightlegs.Count; icount++)
                                {
                                    if (Preflightlegs[icount - 1].IsDeleted == false)
                                    {
                                        (dgAvailablePax.MasterTableView.GetColumn("Leg" + icount) as GridTemplateColumn).Display = true;

                                        if (e.Item is GridDataItem)
                                        {
                                            GridDataItem item = (GridDataItem)e.Item;
                                            DropDownList ddl = new DropDownList();
                                            HiddenField hdn = new HiddenField();

                                            string strCode = item.GetDataKeyValue("PassengerRequestorID").ToString();
                                            ddl.ID = "ddl" + icount;
                                            ddl = (DropDownList)item["leg" + icount].FindControl("ddlLeg" + icount);
                                            BindFlightPurpose(ddl, icount, strCode);
                                            //ddl.Attributes.Clear();
                                            ddl.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                                            ddl.Attributes.Add(System.Web.HttpUtility.HtmlEncode("previous"), "0");
                                            //ddl.Attributes.Add("OnChange", "return CalculateLegCount(this," + icount + ",'dropdown');");
                                        }
                                        if (e.Item is GridHeaderItem)
                                        {

                                            foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                                            {
                                                Label lblpax = (Label)headerItem["Leg" + icount].FindControl("lblLeg" + icount + "p" + icount);// Get the header lable for pax                         
                                                Label lblavailable = (Label)headerItem["Leg" + icount].FindControl("lblLeg" + icount + "a" + icount); // Get the header lable for available 
                                                Label lblLegInfo = (Label)headerItem["Leg" + icount].FindControl("lblLeginfo" + icount);

                                                int? PasssengerTotal = Preflightlegs[icount - 1].PassengerTotal == null ? 0 : Preflightlegs[icount - 1].PassengerTotal;
                                                int? PassengerTotalWithPurpose = Preflightlegs[icount - 1].PreflightPassengerLists == null ? 0 : (Preflightlegs[icount - 1].PreflightPassengerLists.Where(x => x.IsDeleted == false)).ToList().Count;


                                                lblpax.Text = System.Web.HttpUtility.HtmlEncode(Preflightlegs[icount - 1].PassengerTotal == null ? "0" : Preflightlegs[icount - 1].PassengerTotal.ToString());
                                                //maxpassenger = Preflightlegs[icount - 1].ReservationAvailable == null ? 0 : Preflightlegs[icount - 1].SeatTotal - Preflightlegs[icount - 1].PassengerTotal;
                                                lblavailable.Text = System.Web.HttpUtility.HtmlEncode(Preflightlegs[icount - 1].ReservationAvailable == null ? "0" : (Preflightlegs[icount - 1].SeatTotal - Preflightlegs[icount - 1].PassengerTotal).ToString());


                                                //lblpax.Text = PassengerTotalWithPurpose.ToString();
                                                //lblavailable.Text = Preflightlegs[icount - 1].ReservationAvailable == null ? "0" : (Preflightlegs[icount - 1].SeatTotal - PassengerTotalWithPurpose).ToString();

                                                ((HiddenField)DivExternalForm.FindControl("hdleg" + Preflightlegs[icount - 1].LegNUM + "PaxCount")).Value = PassengerTotalWithPurpose.ToString();
                                                ((HiddenField)DivExternalForm.FindControl("hdleg" + Preflightlegs[icount - 1].LegNUM + "Avail")).Value = (FleetCount - PasssengerTotal).ToString();

                                                if (Preflightlegs[icount - 1].Airport1 != null && Preflightlegs[icount - 1].Airport != null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(Preflightlegs[icount - 1].Airport1.IcaoID + "-" + Preflightlegs[icount - 1].Airport.IcaoID);
                                                }
                                                if (Preflightlegs[icount - 1].Airport1 == null && Preflightlegs[icount - 1].Airport != null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty + "-" + Preflightlegs[icount - 1].Airport.IcaoID);
                                                }
                                                if (Preflightlegs[icount - 1].Airport1 != null && Preflightlegs[icount - 1].Airport == null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(Preflightlegs[icount - 1].Airport1.IcaoID + "-" + string.Empty);
                                                }
                                            }
                                        }

                                        if (e.Item is GridFooterItem)
                                        {
                                            foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                                            {
                                                TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + icount].FindControl("tbLeg" + icount);// Get the header lable for pax
                                                HiddenField hdnBlockedSeats = (HiddenField)footerItem["Leg" + icount].FindControl("hdnLeg" + icount);// Get the header lable for pax

                                                int? PasssengerTotal = Preflightlegs[icount - 1].PassengerTotal == null ? 0 : Preflightlegs[icount - 1].PassengerTotal;
                                                int? PassengerTotalWithPurpose = Preflightlegs[icount - 1].PreflightPassengerLists == null ? 0 : (Preflightlegs[icount - 1].PreflightPassengerLists.Where(x => x.IsDeleted == false)).ToList().Count;
                                                if ((PasssengerTotal - PassengerTotalWithPurpose) > 0)
                                                {
                                                    txtBlockedSeats.Text = (PasssengerTotal - PassengerTotalWithPurpose).ToString();
                                                    hdnBlockedSeats.Value = (PasssengerTotal - PassengerTotalWithPurpose).ToString();


                                                    ((HiddenField)DivExternalForm.FindControl("hdleg" + Preflightlegs[icount - 1].LegNUM + "PaxCount")).Value = PassengerTotalWithPurpose.ToString();
                                                    ((HiddenField)DivExternalForm.FindControl("hdleg" + Preflightlegs[icount - 1].LegNUM + "Avail")).Value = (FleetCount - PasssengerTotal).ToString();

                                                }
                                                else
                                                {
                                                    txtBlockedSeats.Text = "0";
                                                    hdnBlockedSeats.Value = "0";
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem dataItem = e.Item as GridDataItem;
                                GridColumn Column = dgAvailablePax.MasterTableView.GetColumn("Notes");
                                string Notes = dataItem["Notes"].Text.Trim();
                                string PassengerAlert = dataItem["PassengerAlert"].Text.Trim();
                                TableCell cellPaxCode = (TableCell)dataItem["PassengerRequestorID"];
                                TableCell cellName = (TableCell)dataItem["Name"];
                                //hdnTSA.Value = cellPaxCode.Text;

                                if (Notes != string.Empty && Notes != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true)
                                {
                                    cellName.ForeColor = System.Drawing.Color.Red;
                                    cellName.ToolTip = "Addl Notes :" + Notes;

                                    if (PassengerAlert != string.Empty && PassengerAlert != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true)
                                    {
                                        cellName.ToolTip = "Addl Notes :" + Notes + Environment.NewLine + "Alerts :" + PassengerAlert;
                                    }
                                }
                                if (PassengerAlert != string.Empty && PassengerAlert != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true && Notes == string.Empty)
                                {
                                    cellName.ForeColor = System.Drawing.Color.Red;
                                    cellName.ToolTip = "Alerts :" + PassengerAlert;
                                }


                                if (Trip.PreflightLegs != null)
                                {
                                    foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                                    {
                                        List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                        for (int i = 0; i < Preflegs.Count; i++)
                                        {

                                            if (Preflegs[i].PreflightPassengerLists != null)
                                            {
                                                for (int j = 0; j < Preflegs[i].PreflightPassengerLists.Count; j++)
                                                {
                                                    if (Convert.ToInt64(cellPaxCode.Text) == Preflegs[i].PreflightPassengerLists[j].PassengerID && col.UniqueName == "Leg" + (Preflegs[i].LegNUM).ToString())
                                                    {
                                                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                                        if (Preflegs[i].PreflightPassengerLists[j].PreflightPassengerListID != 0)
                                                        {

                                                            string strFlightPurposeCd = string.Empty;
                                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                            {
                                                                var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[i].PreflightPassengerLists[j].FlightPurposeID));
                                                                if (objflightRetVal.ReturnFlag == true)
                                                                {
                                                                    if (objflightRetVal.EntityList.Count > 0)
                                                                    {
                                                                        strFlightPurposeCd = objflightRetVal.EntityList[0].FlightPurposeCD.ToString();
                                                                    }
                                                                }
                                                            }
                                                            if (strFlightPurposeCd != string.Empty && !ddlFP.Items.Contains(new ListItem(System.Web.HttpUtility.HtmlEncode(strFlightPurposeCd), Preflegs[i].PreflightPassengerLists[j].FlightPurposeID.ToString())))
                                                            {
                                                                ddlFP.Items.Add(new ListItem(Microsoft.Security.Application.Encoder.HtmlEncode(strFlightPurposeCd), Microsoft.Security.Application.Encoder.HtmlEncode(Preflegs[i].PreflightPassengerLists[j].FlightPurposeID.ToString())));
                                                            }

                                                        }
                                                        ddlFP.SelectedValue = Preflegs[i].PreflightPassengerLists[j].FlightPurposeID.ToString();

                                                        //ddlFP.Attributes.Clear();
                                                        ddlFP.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                                                        ddlFP.Attributes.Add(Microsoft.Security.Application.Encoder.HtmlEncode("previous"), Microsoft.Security.Application.Encoder.HtmlEncode(Preflegs[i].PreflightPassengerLists[j].FlightPurposeID.ToString()));
                                                        //ddlFP.Attributes.Add("OnChange", "return CalculateLegCount(this," + Preflegs[i].LegNUM + ",'dropdown');");
                                                        HiddenField hdnFP = (HiddenField)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("hdnOldPurpose_" + (Preflegs[i].LegNUM).ToString());
                                                        hdnFP.Value = Preflegs[i].PreflightPassengerLists[j].FlightPurposeID.ToString();

                                                        bool isdeadhead = false;
                                                        if (LegFlightCategory.ContainsKey((long)Preflegs[i].LegNUM))
                                                        {
                                                            isdeadhead = LegFlightCategory[(long)Preflegs[i].LegNUM];
                                                        }

                                                        if (isdeadhead)
                                                        {
                                                            ddlFP.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            ddlFP.Enabled = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (dataItem["Name"] != null && dataItem["Name"].Text.Trim() != "")
                                {
                                    string Name = dataItem["Name"].Text;
                                    if (Name[0] == ',')
                                    {
                                        Name = Name.Remove(0, 1);
                                    }
                                    dataItem["Name"].Text = Name;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgAvailablePax_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SavePreflightToSession();
                        foreach (GridDataItem dataItem in dgAvailablePax.Items)
                        {
                            if (dataItem.Selected)
                            {
                                hdnTSA.Value = dataItem["PassengerRequestorID"].Text;
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgAvailablePax_ItemCommand(object sender, GridCommandEventArgs e)
        {
            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.CommandName == "RowClick")
                        {
                            GridDataItem test = (GridDataItem)e.Item;
                            hdnTSA.Value = test["PassengerRequestorID"].Text;
                            if (UserPrincipal.Identity._fpSettings._IsEnableTSA != null)
                            {
                                if (UserPrincipal.Identity._fpSettings._IsEnableTSA == true)
                                {
                                    checkTSA();
                                }
                                else
                                {
                                    tbStaus.Text = string.Empty;
                                    tbStaus.BackColor = Color.White;
                                }
                            }
                            else
                            {
                                tbStaus.Text = string.Empty;
                                tbStaus.BackColor = Color.White;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void FlightPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFlightPurpose.SelectedIndex >= 0)
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    // Added for Set the Selected Value into the Dropdown list
                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                    {
                        try
                        {
                            //Handle methods throguh exception manager with return flag
                            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                            exManager.Process(() =>
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(dgAvailablePax);


                                Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);

                                Session["PaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);

                                List<PassengerInLegs> RetainList = (List<PassengerInLegs>)Session["AvailablePaxList"];
                                int rowCount = 0;
                                foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                {
                                    if (dataItem.Selected && Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == RetainList[rowCount].PassengerRequestorID)
                                    {
                                        if (Trip != null)
                                        {
                                            if (Trip.PreflightLegs != null)
                                            {
                                                List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                                for (int legCnt = 0; legCnt < Preflegs.Count; legCnt++)
                                                {
                                                    bool isdeadhead = false;
                                                    if (LegFlightCategory.ContainsKey((long)Preflegs[legCnt].LegNUM))
                                                    {
                                                        isdeadhead = LegFlightCategory[(long)Preflegs[legCnt].LegNUM];
                                                    }

                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + Preflegs[legCnt].LegNUM.ToString()].FindControl("ddlLeg" + Preflegs[legCnt].LegNUM.ToString());
                                                    if (!isdeadhead)
                                                        ddlFP.SelectedValue = ddlFlightPurpose.SelectedValue;
                                                    else
                                                        ddlFP.SelectedValue = "0";

                                                    //ddlFP.Attributes.Clear();
                                                    ddlFP.Attributes.Remove(System.Web.HttpUtility.HtmlEncode("previous"));
                                                    ddlFP.Attributes.Add(Microsoft.Security.Application.Encoder.HtmlEncode("previous"), Microsoft.Security.Application.Encoder.HtmlEncode(ddlFlightPurpose.SelectedValue));
                                                    //ddlFP.Attributes.Add("OnChange", "return CalculateLegCount(this," + Preflegs[legCnt].LegNUM + ",'dropdown');");

                                                }
                                            }
                                        }
                                    }
                                    rowCount++;
                                }

                                Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();

                                if (pnlPaxSummary.Items.Count > 0)
                                {
                                    pnlPaxSummary.Items[0].Expanded = false;
                                }

                                // Fix for #8221
                                if (ddlFlightPurpose.Items.Count != null && ddlFlightPurpose.Items.Count >= 0)
                                    ddlFlightPurpose.SelectedIndex = 0;

                            }, FlightPak.Common.Constants.Policy.UILayer);
                        }
                        catch (Exception ex)
                        {
                            //The exception will be handled, logged and replaced by our custom exception. 
                            ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                        }
                    }
                }
                if (ddlFlightPurpose.Items.Count != null && ddlFlightPurpose.Items.Count >= 0)
                    ddlFlightPurpose.SelectedIndex = 0;
            }
        }
        #endregion

        #region Ajax Manager Events
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                dgAvailablePax.MasterTableView.SortExpressions.Clear();
                                dgAvailablePax.MasterTableView.GroupByExpressions.Clear();
                                dgAvailablePax.MasterTableView.CurrentPageIndex = dgAvailablePax.MasterTableView.PageCount - 1;
                                dgAvailablePax.Rebind();
                                break;
                            case "RebindAndNavigate":
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                                SavePreflightToSession();
                                SearchPaxAvailable();
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();

                                if (Session["PassSelectedPax"] != null && Session["PaxPassSelectedLeg"] != null)
                                {
                                    RadWindowManager1.RadConfirm("Update Passport Number for All Passenger Legs?", "confirmPaxUpdateAllLegsCallBackFn", 330, 100, null, "Confirmation!");

                                }
                                else
                                {
                                    if (pnlPaxSummary.Items.Count > 0)
                                    {
                                        pnlPaxSummary.Items[0].Expanded = false;
                                    }

                                }
                                break;
                            case "RebindAndNavigateInsert":
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                                SavePreflightToSession();
                                SearchPaxAvailableforInsert();
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();

                                if (pnlPaxSummary.Items.Count > 0)
                                {
                                    pnlPaxSummary.Items[0].Expanded = false;
                                }

                                break;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        #endregion

        #region Trip Events
        protected void SavePreflightToSessionOnTabChange()
        {
            if (hdnPaxSelectedTab.Value == "0")
            {
                #region "Pax Info tabs is selected previously so save the details to session"
                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);

                string PaxpurposeErrorstr = string.Empty;
                PaxpurposeErrorstr = PaxPurposeError();

                if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                    Session["PAXnotAssigned"] = PaxpurposeErrorstr;
                else
                {
                    Session.Remove("PAXnotAssigned");
                    SavePreflightToSession();
                }
                #endregion
            }
            else
            {
                #region PAX Logistics tab is selected previously so store hotel/Transport based on the selected tab and load the new tab.

                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (hdnPaxLogisticsSelectedTab.Value == "1") //If previously selected tab is transport then save transport based on the hdnlegnumTranspoert
                    {

                        #region If previously selected tab is transport then save transport based on the hdnLegnumTransport
                        if (IsAuthorized(Permission.Preflight.AddPreflightCrewTransport) || IsAuthorized(Permission.Preflight.EditPreflightCrewTransport))
                        {
                            PreflightLeg LegtoSaveTransport = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();
                            if (LegtoSaveTransport != null)
                            {
                                SaveTransport(LegtoSaveTransport);
                            }
                        }

                        Session["CurrentPreFlightTrip"] = Trip;
                        #endregion
                    }
                    else if (hdnPaxLogisticsSelectedTab.Value == "0") // if previously selected tab is hotel then save hotel
                    {
                        #region if previously selected tab is hotel then save hotel based on hdnlegnumHotel
                        if (btnSaveHotel.Enabled)
                        {
                            Session["HotelnotSaved"] = "Hotel not saved";
                        }
                        else
                        {
                            Session.Remove("HotelnotSaved");
                        }
                        #endregion
                    }
                }
                #endregion
            }
        }
        protected void SavePreflightToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        if (!string.IsNullOrEmpty(tbTsaVerfiNum.Text))
                            Trip.VerifyNUM = tbTsaVerfiNum.Text;

                        UpdateHeaderInformationBasedOnAvailablePAXGrid(); //check
                        Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        Session["PaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                        UpdatePaxSummaryBasedonAvailableGrid(false);

                        List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["AvailablePaxList"];
                        List<PassengerSummary> passengerSummaryList = (List<PassengerSummary>)Session["PaxSummaryList"]; //RetainPaxSummaryGrid(dgPaxSummary);
                        SavePassengers(passengerList, passengerSummaryList);
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        Session["CurrentPreFlightTrip"] = Trip;

                        Master.BindException();
                    }
                }
            }
        }

        private string PaxPurposeError()
        {
            string desc = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["AvailablePaxList"];
                List<PassengerInLegs> nolegsPAXlist = new List<PassengerInLegs>();


                bool nopurposeassigned = false;
                foreach (PassengerInLegs paxlegs in passengerList)
                {
                    List<PaxLegInfo> legsPAXlist = new List<PaxLegInfo>();

                    if (paxlegs.Legs != null)
                    {
                        legsPAXlist = (from paxleg in paxlegs.Legs
                                       where paxleg.FlightPurposeID != 0
                                       select paxleg).ToList();
                        if (legsPAXlist == null || legsPAXlist.Count == 0)
                            nopurposeassigned = true;
                    }
                }

                if (nopurposeassigned)
                    desc = "Warning - Passenger Not Assigned To Any Legs Will Not Be Saved. Continue Without Saving Your Entry?";
            }
            return desc;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {

                            if (hdnPaxSelectedTab.Value == "0")
                            {
                                #region "Crew Info tabs is selected previously so save the details to session"
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                                Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);

                                string PaxpurposeErrorstr = string.Empty;
                                PaxpurposeErrorstr = PaxPurposeError();

                                if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                                    RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmPaxRemoveCallBackFn", 330, 100, null, "Confirmation!");
                                else
                                {
                                    if (IsAuthorized(Permission.Preflight.AddPreflightPassenger) || IsAuthorized(Permission.Preflight.EditPreflightPassenger))
                                    {

                                        SavePreflightToSession();
                                    }
                                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                    Master.Save(Trip);
                                }
                                #endregion
                            }
                            else
                            {
                                #region PAX Logistics tab is selected previously so store hotel/Transport based on the selected tab and load the new tab.

                                if (Session["CurrentPreFlightTrip"] != null)
                                {
                                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                    if (hdnPaxLogisticsSelectedTab.Value == "1") //If previously selected tab is transport then save transport based on the hdnlegnumTranspoert
                                    {

                                        #region If previously selected tab is transport then save transport based on the hdnLegnumTransport
                                        if (IsAuthorized(Permission.Preflight.AddPreflightCrewTransport) || IsAuthorized(Permission.Preflight.EditPreflightCrewTransport))
                                        {
                                            PreflightLeg LegtoSaveTransport = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();
                                            if (LegtoSaveTransport != null)
                                            {
                                                SaveTransport(LegtoSaveTransport);
                                                Session["CurrentPreFlightTrip"] = Trip;

                                                Master.Save(Trip);
                                            }
                                        }

                                        #endregion
                                    }
                                    else if (hdnPaxLogisticsSelectedTab.Value == "0") // if previously selected tab is hotel then save hotel
                                    {
                                        #region if previously selected tab is hotel then save hotel based on hdnlegnumHotel

                                        if (btnSaveHotel.Enabled)
                                        {
                                            RadWindowManager1.RadAlert("Please save or cancel hotel information", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                                        }
                                        else
                                        {

                                            Session["CurrentPreFlightTrip"] = Trip;
                                            Master.Save(Trip);
                                        }

                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void btnPaxRemoveYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        SavePreflightToSession();
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        Master.Save(Trip);
                    }
                }
            }
        }
        protected void btnPaxRemoveNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnSave);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnPaxSelectedTab.Value == "0")
                        {
                            #region "Crew Info tabs is selected previously so save the details to session"
                            string PaxpurposeErrorstr = string.Empty;
                            PaxpurposeErrorstr = PaxPurposeError();

                            if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                                RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmPaxNextCallBackFn", 330, 100, null, "Confirmation!");
                            else
                            {
                                SavePreflightToSession();
                                Master.Next("Pax");
                            }
                            #endregion
                        }
                        else
                        {
                            #region Crew Logistics tab is selected previously so store hotel/Transport based on the selected tab and load the new tab.

                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (hdnPaxLogisticsSelectedTab.Value == "1") //If previously selected tab is transport then save transport based on the hdnlegnumTranspoert
                                {
                                    #region If previously selected tab is transport then save transport based on the hdnLegnumTransport
                                    if (IsAuthorized(Permission.Preflight.AddPreflightCrewTransport) || IsAuthorized(Permission.Preflight.EditPreflightCrewTransport))
                                    {
                                        PreflightLeg LegtoSaveTransport = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();
                                        if (LegtoSaveTransport != null)
                                        {
                                            SaveTransport(LegtoSaveTransport);
                                        }
                                    }

                                    Session["CurrentPreFlightTrip"] = Trip;
                                    Master.Next("PAX");
                                    #endregion
                                }
                                else if (hdnPaxLogisticsSelectedTab.Value == "0") // if previously selected tab is hotel then save hotel
                                {
                                    #region if previously selected tab is hotel then save hotel based on hdnlegnumHotel

                                    if (btnSaveHotel.Enabled)
                                    {
                                        RadWindowManager1.RadAlert("Please save or cancel hotel information", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                                    }
                                    else
                                    {
                                        Master.Next("PAX");
                                    }
                                    #endregion
                                }
                            }
                            #endregion

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void btnPaxNextYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                SavePreflightToSession();
                Master.Next("Pax");
            }
        }
        protected void btnPaxNextNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnSave);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            Master.Cancel(Trip);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            Master.Delete(Trip.TripID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }


        private void LoadPaxfromTrip()
        {
            bool alreadyExists = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.PreflightLegs != null)
                    {
                        Session.Remove("AvailablePaxList");
                    }
                    if (Session["AvailablePaxList"] == null && Trip.PreflightLegs != null)
                    {
                        // Trip.PreflightLegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        for (int LegCnt = 0; LegCnt < Preflegs.Count; LegCnt++)
                        {
                            if (Preflegs[LegCnt].IsDeleted == false)
                            {
                                if (Preflegs[LegCnt].PreflightPassengerLists != null)
                                {
                                    for (int PaxCnt = 0; PaxCnt < Preflegs[LegCnt].PreflightPassengerLists.Count; PaxCnt++)
                                    {
                                        if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].IsDeleted == false)
                                        {
                                            alreadyExists = NewpassengerInLegs.Any(X => X.PassengerRequestorID == Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID);

                                            if (alreadyExists == false)
                                            {
                                                PassengerInLegs paxInfo = new PassengerInLegs();
                                                paxInfo.Legs = new List<PaxLegInfo>();

                                                NewpassengerInLegs.Add(paxInfo);
                                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                                PaxLeg.LegID = Convert.ToInt64(Preflegs[LegCnt].LegID);

                                                paxInfo.PassengerRequestorID = Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID);





                                                using (PreflightServiceClient PrefService = new PreflightServiceClient())
                                                {

                                                    var objRetVal = PrefService.GetPassengerbyIDOrCD(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID), string.Empty);
                                                    if (objRetVal.ReturnFlag == true)
                                                        _getPassenger = objRetVal.EntityList;

                                                    //var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(dataitem["PassengerRequestorID"].Text));
                                                    if (_getPassenger != null && _getPassenger.Count > 0)
                                                    {
                                                        var objPaxRetVal = _getPassenger[0];

                                                        if (objPaxRetVal != null)
                                                        {
                                                            //var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID));


                                                            paxInfo.Code = objPaxRetVal.PassengerRequestorCD;
                                                            paxInfo.Notes = objPaxRetVal.Notes;
                                                            paxInfo.PassengerAlert = objPaxRetVal.PassengerAlert;
                                                        }
                                                    }
                                                }

                                                paxInfo.Name = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerLastName + ", " + Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerFirstName + " " + Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerMiddleName;
                                                paxInfo.OrderNUM = (Int32)Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].OrderNUM;

                                                using (PreflightServiceClient Service = new PreflightServiceClient())
                                                {

                                                    var objPassRetVal = Service.GetPassengerPassportByPassportID(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassportID));
                                                    if (objPassRetVal.ReturnFlag == true)
                                                    {
                                                        if (objPassRetVal.EntityList.Count > 0)
                                                        {
                                                            paxInfo.PassportID = objPassRetVal.EntityList[0].PassportID.ToString();
                                                            paxInfo.Passport = objPassRetVal.EntityList[0].PassportNum;
                                                        }
                                                    }
                                                }
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].VisaID != null)
                                                    paxInfo.VisaID = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].VisaID.ToString();
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].StateName != null)
                                                    paxInfo.State = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].StateName.ToString();
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Street != null)
                                                    paxInfo.Street = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Street;
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].TripStatus != null)
                                                    paxInfo.Status = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].TripStatus;
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].CityName != null)
                                                    paxInfo.City = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].CityName;
                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].FlightPurposeID));
                                                    // GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeID.Equals(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].FlightPurposeID));
                                                    //if (objflightRetVal.Count() > 0 && objflightRetVal != null)
                                                    if (objflightRetVal.ReturnFlag == true)
                                                    {
                                                        //foreach (FlightPakMasterService.FlightPurpose flight in objflightRetVal)
                                                        //{
                                                        //    if (flight.FlightPurposeID == Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].FlightPurposeID)
                                                        //    {
                                                        paxInfo.Purpose = objflightRetVal.EntityList[0].FlightPurposeID.ToString();
                                                        //flight.FlightPurposeID.ToString();
                                                        //    }
                                                        //}
                                                    }
                                                }
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Billing != null)
                                                    paxInfo.BillingCode = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Billing.ToString().Trim();
                                            }
                                        }
                                    }

                                    for (int PaxCnt = 0; PaxCnt < Preflegs[LegCnt].PreflightPassengerLists.Count; PaxCnt++)
                                    {
                                        if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].IsDeleted == false)
                                        {

                                            PassengerSummary paxSum = new PassengerSummary();
                                            Newpassengerlist.Add(paxSum);

                                            paxSum.PaxID = Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID);
                                            paxSum.OrderNum = (Int32)Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].OrderNUM;

                                            using (PreflightServiceClient Service = new PreflightServiceClient())
                                            {

                                                //var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID));

                                                var objRetVal = Service.GetPassengerbyIDOrCD(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerID), string.Empty);
                                                if (objRetVal.ReturnFlag == true)
                                                    _getPassenger = objRetVal.EntityList;
                                                if (_getPassenger != null && _getPassenger.Count > 0)
                                                {
                                                    paxSum.PaxCode = _getPassenger[0].PassengerRequestorCD;
                                                }

                                                paxSum.PaxName = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerLastName + ", " + Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerFirstName + " " + Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassengerMiddleName;
                                                paxSum.LegId = Preflegs[LegCnt].LegNUM;
                                                if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassportID != null)
                                                    paxSum.PassportID = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassportID.ToString();

                                                var objPassRetVal = Service.GetPassengerPassportByPassportID(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassportID));
                                                if (objPassRetVal.ReturnFlag == true)
                                                {
                                                    if (objPassRetVal.EntityList.Count > 0)
                                                    {
                                                        paxSum.PassportID = objPassRetVal.EntityList[0].PassportID.ToString();
                                                        paxSum.Passport = objPassRetVal.EntityList[0].PassportNum;
                                                        //if (!string.IsNullOrEmpty(objPassRetVal.EntityList[0].PassportExpiryDT.ToString()))
                                                        if (!string.IsNullOrEmpty(Convert.ToString(objPassRetVal.EntityList[0].PassportExpiryDT)))
                                                        {
                                                            DateTime dtDate = Convert.ToDateTime(objPassRetVal.EntityList[0].PassportExpiryDT);
                                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                                paxSum.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                            else
                                                                paxSum.PassportExpiryDT = string.Empty;
                                                        }

                                                        using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            var objCountryVal = CountryService.GetCountryWithFilters(string.Empty, (long)objPassRetVal.EntityList[0].CountryID);
                                                            if (objCountryVal.ReturnFlag == true)
                                                            {
                                                                var objCountryVal1 = objCountryVal.EntityList;
                                                                if (objCountryVal1.Count > 0)
                                                                {
                                                                    paxSum.Nation = objCountryVal1[0].CountryCD;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {

                                                var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].FlightPurposeID));
                                                //.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeID.Equals(Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].FlightPurposeID));
                                                if (objflightRetVal.ReturnFlag == true)
                                                {
                                                    paxSum.Purpose = objflightRetVal.EntityList[0].FlightPurposeID.ToString();
                                                    //flight.FlightPurposeID.ToString();
                                                    //flight.FlightPurposeDescription;
                                                }
                                            }

                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassportID != null)
                                                paxSum.PassportID = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PassportID.ToString();
                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].VisaID != null)
                                                paxSum.VisaID = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].VisaID.ToString();
                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Street != null)
                                                paxSum.Street = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Street.ToString();
                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PostalZipCD != null)
                                                paxSum.Postal = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].PostalZipCD.ToString();
                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].CityName != null)
                                                paxSum.City = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].CityName.ToString();
                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].StateName != null)
                                                paxSum.State = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].StateName.ToString();
                                            if (Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Billing != null)
                                                paxSum.BillingCode = Preflegs[LegCnt].PreflightPassengerLists[PaxCnt].Billing.ToString().Trim();
                                        }
                                    }

                                }
                            }
                        }

                        Newpassengerlist = Newpassengerlist.Where(x => x.LegId != null).OrderBy(x => x.LegId).ToList();

                        Int32 MaxOrderNUM = 0;
                        foreach (PassengerInLegs PaxinLegs in NewpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                        {
                            MaxOrderNUM++;
                            PaxinLegs.OrderNUM = MaxOrderNUM;

                            foreach (PassengerSummary PaxSum in Newpassengerlist.Where(x => x.PaxID == PaxinLegs.PassengerRequestorID).ToList())
                            {
                                PaxSum.OrderNum = PaxinLegs.OrderNUM;
                            }
                        }


                        Session["AvailablePaxList"] = NewpassengerInLegs;
                        dgAvailablePax.DataSource = NewpassengerInLegs;
                        dgAvailablePax.DataBind();
                        Session["PaxSummaryList"] = Newpassengerlist;
                        //001
                        dgPaxSummary.DataSource = Newpassengerlist.OrderBy(x => x.LegId).ThenBy(x => x.OrderNum).ToList();
                        dgPaxSummary.DataBind();

                        Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        Session["PaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                    }
                    else
                    {
                        List<PassengerInLegs> paxInfoList = (List<PassengerInLegs>)Session["AvailablePaxList"];
                        dgAvailablePax.DataSource = paxInfoList;
                        dgAvailablePax.DataBind();
                        FillFlightPurpose(paxInfoList);
                    }
                }
            }
        }
        private List<PassengerInLegs> RetainPAXInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<PassengerInLegs> paxInfoList = new List<PassengerInLegs>();

                if (Session["CurrentPreFlightTrip"] != null)
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                {
                    PassengerInLegs PaxInfo = new PassengerInLegs();
                    LinkButton lbtnCode = (LinkButton)Item["Code"].FindControl("lnkPaxCode");
                    PaxInfo.Code = lbtnCode.Text;
                    PaxInfo.PassengerRequestorID = Convert.ToInt64(Item["PassengerRequestorID"].Text);
                    PaxInfo.Name = Item["Name"].Text;
                    PaxInfo.OrderNUM = (Int32)Item.GetDataKeyValue("OrderNUM");

                    if (Trip != null)
                    {
                        if (Trip.PreflightLegs.Count > 0)
                        {
                            List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            PaxInfo.Legs = new List<PaxLegInfo>();
                            int counter = 1;
                            for (int leg = 0; leg < Preflegs.Count; leg++)
                            {
                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                PaxLeg.LegID = Convert.ToInt64(Preflegs[leg].LegID);
                                if (Preflegs[leg].Airport != null)
                                {
                                    if (Preflegs[leg].Airport.IcaoID != null)
                                        PaxLeg.Depart = Preflegs[leg].Airport.IcaoID;
                                }
                                if (Preflegs[leg].Airport1 != null)
                                {
                                    if (Preflegs[leg].Airport1.IcaoID != null)
                                        PaxLeg.Arrive = Preflegs[leg].Airport1.IcaoID;
                                }

                                foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                                {
                                    if (col.UniqueName == "Leg" + counter.ToString())
                                    {
                                        DropDownList ddlFlightPurpose = (DropDownList)Item["Leg" + counter.ToString()].FindControl("ddlLeg" + counter.ToString());
                                        if (ddlFlightPurpose.SelectedItem != null)
                                            PaxLeg.FlightPurposeCD = ddlFlightPurpose.SelectedItem.ToString();
                                        if (!string.IsNullOrEmpty(ddlFlightPurpose.SelectedValue))
                                            PaxLeg.FlightPurposeID = Convert.ToInt64(ddlFlightPurpose.SelectedValue);
                                        PaxInfo.Purpose = ddlFlightPurpose.SelectedValue;
                                        break;
                                    }
                                }
                                PaxLeg.LegOrder = counter;
                                counter += 1;
                                PaxInfo.Legs.Add(PaxLeg);
                            }
                        }
                    }
                    paxInfoList.Add(PaxInfo);
                }
                return paxInfoList;
            }
        }
        private List<PassengerSummary> RetainPaxSummaryGrid(RadGrid gridPaxSummary)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(gridPaxSummary))
            {
                List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();


                foreach (GridDataItem Item in gridPaxSummary.MasterTableView.Items)
                {
                    long legId = Convert.ToInt64(((Label)Item.FindControl("lblLegID")).Text);
                    long paxID = Convert.ToInt64(((Label)Item.FindControl("lblPassengerRequestorID")).Text);
                    string paxCode = ((Label)Item.FindControl("lblCode")).Text;
                    string paxName = ((Label)Item.FindControl("lblName")).Text;

                    string Street = ((TextBox)Item.FindControl("txtStreet")).Text;
                    string Postal = ((TextBox)Item.FindControl("txtPostal")).Text;
                    string City = ((TextBox)Item.FindControl("txtCity")).Text;
                    string State = ((TextBox)Item.FindControl("txtState")).Text;

                    string Purpose = ((TextBox)Item.FindControl("tbPurpose")).Text;
                    string Passport = ((TextBox)Item.FindControl("tbPassport")).Text;
                    string BillingCode = ((TextBox)Item.FindControl("tbBillingCode")).Text;
                    string VisaID = ((HiddenField)Item.FindControl("hdnVisaID")).Value;
                    string PassportID = ((HiddenField)Item.FindControl("hdnPassID")).Value;
                    string PassportExpiryDT = Item["PassportExpiryDT"].Text;
                    string Nation = Item["Nation"].Text;
                    Int32 OrderNUM = (Int32)Item.GetDataKeyValue("OrderNUM");
                    paxSummaryList.Add(new PassengerSummary()
                    {
                        LegId = legId,
                        PaxID = paxID,
                        PaxName = paxName,
                        PaxCode = paxCode,
                        Street = Street,
                        Postal = Postal,
                        City = City,
                        State = State,
                        Purpose = Purpose,
                        Passport = Passport,
                        BillingCode = BillingCode,
                        IsNonPassenger = false,
                        OrderNum = OrderNUM,
                        WaitList = string.Empty,
                        VisaID = VisaID,
                        PassportID = PassportID,
                        PassportExpiryDT = PassportExpiryDT,
                        Nation = Nation
                    });
                }

                return paxSummaryList;
            }
        }
        public void UpdatePaxSummaryBasedonAvailableGrid(bool bindsummary)
        {
            List<PassengerInLegs> passengerlist = new List<PassengerInLegs>();
            List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
            List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
            List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();

            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            if (Session["AvailablePaxList"] != null)
            {
                passengerlist = (List<PassengerInLegs>)Session["AvailablePaxList"];
            }

            if (Session["PaxSummaryList"] != null)
            {
                Oldpassengerlist = (List<PassengerSummary>)Session["PaxSummaryList"];
            }
            foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
            {


                LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");
                string PaxCode = lnkPaxCode.Text;
                string Paxid = dataItem["PassengerRequestorID"].Text;
                string PassportNumber = string.Empty;
                string PaxPassportID = string.Empty;
                string street = string.Empty;
                string postal = string.Empty;
                string city = string.Empty;
                string state = string.Empty;
                string expirydt = string.Empty;
                string nation = string.Empty;
                int orderNUM = 1;
                // Defect :2205
                string BillingCode = string.Empty;
                if (!string.IsNullOrEmpty(dataItem["BillingCode"].Text) && dataItem["BillingCode"].Text.ToLower() != "&nbsp;")
                    BillingCode = dataItem["BillingCode"].Text.Trim();

                orderNUM = (int)dataItem.GetDataKeyValue("OrderNUM");
                #region "GetPassengerDetails"
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objflightRetVal = objDstsvc.GetPassengerPassportByPassengerRequestorID(Convert.ToInt64(Paxid));
                    if (objflightRetVal.ReturnFlag == true)
                    {
                        if (objflightRetVal.EntityList.Count > 0)
                        {
                            PaxPassportID = objflightRetVal.EntityList[0].PassportID.ToString();
                            PassportNumber = objflightRetVal.EntityList[0].PassportNum;
                            //if (!string.IsNullOrEmpty(objflightRetVal.EntityList[0].PassportExpiryDT.ToString()))
                            if (!string.IsNullOrEmpty(Convert.ToString(objflightRetVal.EntityList[0].PassportExpiryDT)))
                            {
                                DateTime dtDate = Convert.ToDateTime(objflightRetVal.EntityList[0].PassportExpiryDT);
                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                    expirydt = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                else
                                    expirydt = string.Empty;
                            }

                            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objCountryVal = CountryService.GetCountryWithFilters(string.Empty, (long)objflightRetVal.EntityList[0].CountryID);
                                if (objCountryVal.ReturnFlag == true)
                                {
                                    var objCountryVal1 = objCountryVal.EntityList;
                                    if (objCountryVal1.Count > 0)
                                    {
                                        nation = objCountryVal1[0].CountryCD;
                                    }
                                }
                            }
                        }
                    }
                    using (PreflightServiceClient Prefservice = new PreflightServiceClient())
                    {

                        var objRetVal = Prefservice.GetPassengerbyIDOrCD(Convert.ToInt64(Paxid), string.Empty);
                        if (objRetVal.ReturnFlag == true)
                            _getPassenger = objRetVal.EntityList;
                        //var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(dataitem["PassengerRequestorID"].Text));
                        if (_getPassenger != null && _getPassenger.Count > 0)
                        {
                            var objPax = _getPassenger[0];

                            if (objPax != null)
                            {
                                //var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Paxid));

                                street = objPax.Addr1 + objPax.Addr2 + objPax.Addr3;
                                postal = objPax.PostalZipCD;
                                city = objPax.City;
                                state = objPax.StateName;

                            }
                        }
                    }

                }
                #endregion

                if (Trip.PreflightLegs != null)
                {
                    List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    foreach (PreflightLeg Leg in Preflegs)
                    {
                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + Leg.LegNUM.ToString()].FindControl("ddlLeg" + Leg.LegNUM.ToString());


                        string LegNum = "Leg" + Leg.LegNUM.ToString();
                        string paxNum = "lblLeg" + Leg.LegNUM.ToString() + "p" + Leg.LegNUM.ToString();
                        string maxNum = "lblLeg" + Leg.LegNUM.ToString() + "a" + Leg.LegNUM.ToString();
                        string hdnPax = "hdnLeg" + Leg.LegNUM.ToString() + "Pax";
                        string hdnAvail = "hdnLeg" + Leg.LegNUM.ToString() + "Avail";


                        string PaxName = dataItem["Name"].Text;
                        int legnumber = (int)Leg.LegNUM;

                        bool alreadyExists = false;


                        #region GetFooterDetails
                        foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                        {
                            TextBox tbLeg = (TextBox)footerItem[LegNum].FindControl("tbLeg" + legnumber);
                            if (!string.IsNullOrEmpty(tbLeg.Text))
                            {
                                footerCnt = Convert.ToInt16(tbLeg.Text);
                            }
                            else
                            {
                                footerCnt = 0;
                            }
                            break;

                        }
                        #endregion

                        #region PassengerSummaryUpdate
                        int Index = -1;
                        if (Oldpassengerlist != null && Oldpassengerlist.Count > 0)
                        {
                            for (int Cnt = 0; Cnt < Oldpassengerlist.Count; Cnt++)
                            {
                                if (Oldpassengerlist[Cnt].PaxID == Convert.ToInt64(Paxid) && Oldpassengerlist[Cnt].LegId == Convert.ToInt64(legnumber))
                                {
                                    Index = Cnt;
                                    alreadyExists = true;

                                    break;
                                }
                            }
                            // alreadyExists = Oldpassengerlist.Any(x => x.PaxID == Convert.ToInt64(Paxid) && x.LegId == (legCount+1));

                            if (alreadyExists && ddlFP.SelectedIndex == 0)
                            {
                                #region AlreadyExists and flightpurse set to select
                                Oldpassengerlist.RemoveAt(Index);
                                if (Leg.PreflightPassengerLists != null)
                                {
                                    PreflightPassengerList PAXinLeg = Leg.PreflightPassengerLists.Where(x => x.PassengerID == Convert.ToInt64(Paxid) && x.IsDeleted == false).SingleOrDefault();

                                    if (PAXinLeg != null)
                                    {
                                        if (PAXinLeg.PreflightPassengerListID != 0)
                                        {
                                            PAXinLeg.State = TripEntityState.Deleted;
                                            PAXinLeg.IsDeleted = true;

                                            #region hotel needs to be revisit

                                            if (Leg.PreflightHotelLists != null)
                                            {
                                                foreach (PreflightHotelList Leghotel in Leg.PreflightHotelLists.Where(x => x.IsDeleted == false).ToList())
                                                {
                                                    if (Leghotel.PAXIDList != null && Leghotel.PAXIDList.Count > 0)
                                                    {
                                                        if (Leghotel.PreflightHotelListID != 0)
                                                            Leghotel.State = TripEntityState.Modified;
                                                        Leghotel.PAXIDList.Remove(PAXinLeg.PassengerID);
                                                    }
                                                }
                                            }

                                            //if (PAXinLeg.PreflightPassengerHotelLists != null && PAXinLeg.PreflightPassengerHotelLists.Count > 0)
                                            //{
                                            //    foreach (PreflightPassengerHotelList Paxhotel in PAXinLeg.PreflightPassengerHotelLists)
                                            //    {
                                            //        if (Paxhotel.PreflightPassengerHotelListID != 0)
                                            //        {
                                            //            Paxhotel.State = TripEntityState.Deleted;
                                            //            Paxhotel.IsDeleted = true;
                                            //        }
                                            //        else
                                            //        {
                                            //            PAXinLeg.PreflightPassengerHotelLists.Remove(Paxhotel);
                                            //        }

                                            //    }
                                            //}
                                            #endregion
                                        }
                                        else
                                        {
                                            Leg.PreflightPassengerLists.Remove(PAXinLeg);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (!alreadyExists && ddlFP.SelectedIndex != 0)
                            {
                                #region not exist and newlyadded
                                PassengerSummary pax = new PassengerSummary();
                                Newpassengerlist.Add(pax);
                                pax.PaxID = Convert.ToInt64(Paxid);
                                pax.PaxCode = PaxCode;
                                pax.PaxName = PaxName;
                                pax.LegId = Leg.LegNUM;
                                pax.Street = street;
                                pax.Postal = postal;
                                pax.City = city;
                                pax.State = state;
                                pax.Purpose = string.Empty;
                                pax.Passport = PassportNumber;
                                //Defect :2205
                                pax.BillingCode = BillingCode;//string.Empty;
                                pax.OrderNum = orderNUM;
                                pax.VisaID = string.Empty;
                                pax.PassportID = PaxPassportID;
                                pax.Nation = nation;
                                pax.PassportExpiryDT = expirydt;
                                #endregion
                            }
                            else if (alreadyExists && ddlFP.SelectedIndex != 0)
                            {
                                #region AlreadyExists and changed -- Need to check whether to update Passport details
                                for (int Cnts = 0; Cnts < Oldpassengerlist.Count; Cnts++)
                                {
                                    if (Oldpassengerlist[Cnts].PaxID == Convert.ToInt64(Paxid) && Oldpassengerlist[Cnts].LegId == Convert.ToInt64(legnumber))
                                    {
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Passport))
                                            Oldpassengerlist[Cnts].Passport = PassportNumber;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].PassportID))
                                            Oldpassengerlist[Cnts].PassportID = PaxPassportID;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Street))
                                            Oldpassengerlist[Cnts].Street = street;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Postal))
                                            Oldpassengerlist[Cnts].Postal = postal;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].City))
                                            Oldpassengerlist[Cnts].City = city;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].State))
                                            Oldpassengerlist[Cnts].State = state;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Nation))
                                            Oldpassengerlist[Cnts].Nation = nation;
                                        if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].PassportExpiryDT))
                                            Oldpassengerlist[Cnts].PassportExpiryDT = expirydt;

                                        Oldpassengerlist[Cnts].OrderNum = orderNUM;
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (ddlFP.SelectedValue != "0" && Oldpassengerlist == null || (Oldpassengerlist.Count == 0))
                            {
                                PassengerSummary pax = new PassengerSummary();
                                Newpassengerlist.Add(pax);
                                pax.PaxID = Convert.ToInt64(Paxid);
                                pax.PaxCode = PaxCode;
                                pax.PaxName = PaxName;
                                pax.LegId = Leg.LegNUM;
                                pax.Street = street;
                                pax.Postal = postal;
                                pax.City = city;
                                pax.State = state;
                                pax.Purpose = string.Empty;
                                pax.Passport = PassportNumber;
                                //Defect :2205
                                pax.BillingCode = BillingCode; //string.Empty;
                                pax.OrderNum = orderNUM;
                                pax.VisaID = string.Empty;
                                pax.PassportID = PaxPassportID;
                                pax.Nation = nation;
                                pax.PassportExpiryDT = expirydt;
                            }
                        }



                        #endregion

                    }
                }
            }

            if (Oldpassengerlist != null)
                Finalpassengerlist = Newpassengerlist.Concat(Oldpassengerlist).ToList();

            Session["PaxSummaryList"] = Finalpassengerlist;
            if (bindsummary)
            {
                dgPaxSummary.DataSource = Finalpassengerlist.OrderBy(x => x.LegId).ToList().OrderBy(x => x.OrderNum).ToList();
                dgPaxSummary.DataBind();
            }
        }
        private void SavePassengers(List<PassengerInLegs> paxlist, List<PassengerSummary> sumlist)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxlist, sumlist))
            {
                int LegNum = 0;
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];




                    List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    //for (int k = 0; k < sumlist.Count; k++)
                    //{
                    for (int i = 0; i < paxlist.Count; i++)
                    {
                        if (paxlist[i].Legs != null)
                        {
                            for (int j = 0; j < paxlist[i].Legs.Count; j++)
                            {
                                if (!string.IsNullOrEmpty(paxlist[i].Legs[j].FlightPurposeID.ToString()) && paxlist[i].Legs[j].FlightPurposeID != 0)
                                {
                                    #region "Updating PAX"
                                    for (int k = 0; k < sumlist.Count; k++)
                                    {
                                        if (paxlist[i].PassengerRequestorID == sumlist[k].PaxID && sumlist[k].LegId == paxlist[i].Legs[j].LegOrder)
                                        {
                                            LegNum = Convert.ToInt16(paxlist[i].Legs[j].LegOrder);
                                            foreach (PreflightLeg Leg in Preflegs)
                                            {
                                                if (Leg.IsDeleted == false)
                                                {
                                                    if (Leg.LegNUM == LegNum)
                                                    {
                                                        if (Leg.LegID != 0 && Leg.State != TripEntityState.Deleted)
                                                        {
                                                            Leg.State = TripEntityState.Modified;
                                                        }

                                                        Preflegs[LegNum - 1].ReservationTotal = 0;
                                                        Preflegs[LegNum - 1].WaitNUM = 0;
                                                        #region Leg reservation
                                                        if (LegNum == 1)
                                                        {
                                                            string strpax = hdnLeg1p1.Value;
                                                            string stravailable = hdnLeg1a1.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 2)
                                                        {
                                                            string strpax = hdnLeg2p2.Value;
                                                            string stravailable = hdnLeg2a2.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 3)
                                                        {
                                                            string strpax = hdnLeg3p3.Value;
                                                            string stravailable = hdnLeg3a3.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 4)
                                                        {
                                                            string strpax = hdnLeg4p4.Value;
                                                            string stravailable = hdnLeg4a4.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 5)
                                                        {
                                                            string strpax = hdnLeg5p5.Value;
                                                            string stravailable = hdnLeg5a5.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 6)
                                                        {
                                                            string strpax = hdnLeg6p6.Value;
                                                            string stravailable = hdnLeg6a6.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 7)
                                                        {
                                                            string strpax = hdnLeg7p7.Value;
                                                            string stravailable = hdnLeg7a7.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 8)
                                                        {
                                                            string strpax = hdnLeg8p8.Value;
                                                            string stravailable = hdnLeg8a8.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 9)
                                                        {
                                                            string strpax = hdnLeg9p9.Value;
                                                            string stravailable = hdnLeg9a9.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 10)
                                                        {
                                                            string strpax = hdnLeg10p10.Value;
                                                            string stravailable = hdnLeg10a10.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 11)
                                                        {
                                                            string strpax = hdnLeg11p11.Value;
                                                            string stravailable = hdnLeg11a11.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 12)
                                                        {
                                                            string strpax = hdnLeg12p12.Value;
                                                            string stravailable = hdnLeg12a12.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 13)
                                                        {
                                                            string strpax = hdnLeg13p13.Value;
                                                            string stravailable = hdnLeg13a13.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 14)
                                                        {
                                                            string strpax = hdnLeg14p14.Value;
                                                            string stravailable = hdnLeg14a14.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 15)
                                                        {
                                                            string strpax = hdnLeg15p15.Value;
                                                            string stravailable = hdnLeg15a15.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 16)
                                                        {
                                                            string strpax = hdnLeg16p16.Value;
                                                            string stravailable = hdnLeg16a16.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 17)
                                                        {
                                                            string strpax = hdnLeg17p17.Value;
                                                            string stravailable = hdnLeg17a17.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 18)
                                                        {
                                                            string strpax = hdnLeg18p18.Value;
                                                            string stravailable = hdnLeg18a18.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 19)
                                                        {
                                                            string strpax = hdnLeg19p19.Value;
                                                            string stravailable = hdnLeg19a19.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 20)
                                                        {
                                                            string strpax = hdnLeg20p20.Value;
                                                            string stravailable = hdnLeg20a20.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 21)
                                                        {
                                                            string strpax = hdnLeg21p21.Value;
                                                            string stravailable = hdnLeg21a21.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 22)
                                                        {
                                                            string strpax = hdnLeg22p22.Value;
                                                            string stravailable = hdnLeg22a22.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 23)
                                                        {
                                                            string strpax = hdnLeg23p23.Value;
                                                            string stravailable = hdnLeg23a23.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 24)
                                                        {
                                                            string strpax = hdnLeg24p24.Value;
                                                            string stravailable = hdnLeg24a24.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 25)
                                                        {
                                                            string strpax = hdnLeg25p25.Value;
                                                            string stravailable = hdnLeg25a25.Value;
                                                            if (!string.IsNullOrEmpty(strpax))
                                                                Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            if (!string.IsNullOrEmpty(stravailable))
                                                                Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }

                                                        #endregion

                                                        Int64 GridPaxID = 0;
                                                        GridPaxID = Convert.ToInt64(paxlist[i].PassengerRequestorID);

                                                        //if (Leg.LegID > 0)
                                                        //{
                                                        if (Leg.PreflightPassengerLists != null && Leg.PreflightPassengerLists.Count > 0)
                                                        {
                                                            bool PassFound = false;
                                                            foreach (PreflightPassengerList PrefPass in Leg.PreflightPassengerLists)
                                                            {
                                                                if (PrefPass.PassengerID == GridPaxID)
                                                                {
                                                                    #region Passenge Modfiy Mode Already Exists
                                                                    if (PrefPass.PreflightPassengerListID != 0)
                                                                        PrefPass.State = TripEntityState.Modified;
                                                                    else
                                                                        PrefPass.State = TripEntityState.Added;
                                                                    PrefPass.LegID = Leg.LegID;
                                                                    PrefPass.IsDeleted = false;

                                                                    PrefPass.PassengerID = Convert.ToInt64(paxlist[i].PassengerRequestorID);

                                                                    string[] lines = paxlist[i].Name.Split(' ');
                                                                    int NameCount = 0;
                                                                    foreach (string line in lines)
                                                                    {
                                                                        if (NameCount == 0)
                                                                            PrefPass.PassengerLastName = paxlist[i].Name.Split(' ')[0].ToString().Replace(',', ' ').Trim();
                                                                        if (NameCount == 1)
                                                                            PrefPass.PassengerFirstName = paxlist[i].Name.Split(' ')[1].ToString();
                                                                        if (NameCount == 2)
                                                                            PrefPass.PassengerMiddleName = paxlist[i].Name.Split(' ')[2].ToString();
                                                                        NameCount++;
                                                                    }


                                                                    if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                        PrefPass.PassportID = Convert.ToInt64(sumlist[k].PassportID);

                                                                    if (!string.IsNullOrEmpty(sumlist[k].VisaID))
                                                                        PrefPass.VisaID = Convert.ToInt64(sumlist[k].VisaID);

                                                                    PrefPass.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                    PrefPass.PassportNUM = paxlist[i].Passport;
                                                                    //Defect :2205
                                                                    if (!string.IsNullOrEmpty(sumlist[k].BillingCode))
                                                                        PrefPass.Billing = sumlist[k].BillingCode.Trim();
                                                                    PrefPass.OrderNUM = paxlist[i].OrderNUM;
                                                                    PrefPass.IsNonPassenger = false;
                                                                    PrefPass.IsBlocked = false;
                                                                    PrefPass.IsDeleted = false;
                                                                    PrefPass.LastUpdTS = DateTime.UtcNow;
                                                                    PrefPass.StateName = sumlist[k].State;
                                                                    PrefPass.Street = sumlist[k].Street;
                                                                    PrefPass.CityName = sumlist[k].City;
                                                                    PrefPass.PostalZipCD = sumlist[k].Postal;
                                                                    PassFound = true;

                                                                    #endregion
                                                                    break;
                                                                }
                                                            }
                                                            //Add here 
                                                            if (!PassFound)
                                                            {
                                                                #region Passnot found
                                                                //it is a new Pax for this leg
                                                                PreflightPassengerList NewPaxList = new PreflightPassengerList();
                                                                NewPaxList.State = TripEntityState.Added;
                                                                NewPaxList.LegID = Leg.LegID;
                                                                NewPaxList.IsDeleted = false;

                                                                //add the passenger list here
                                                                NewPaxList.PassengerID = Convert.ToInt64(paxlist[i].PassengerRequestorID);

                                                                string[] lines = paxlist[i].Name.Split(' ');
                                                                int NameCount = 0;
                                                                foreach (string line in lines)
                                                                {
                                                                    if (NameCount == 0)
                                                                        NewPaxList.PassengerLastName = paxlist[i].Name.Split(' ')[0].ToString().Replace(',', ' ').Trim();
                                                                    if (NameCount == 1)
                                                                        NewPaxList.PassengerFirstName = paxlist[i].Name.Split(' ')[1].ToString();
                                                                    if (NameCount == 2)
                                                                        NewPaxList.PassengerMiddleName = paxlist[i].Name.Split(' ')[2].ToString();
                                                                    NameCount++;
                                                                }


                                                                if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                    NewPaxList.PassportID = Convert.ToInt64(sumlist[k].PassportID);

                                                                if (!string.IsNullOrEmpty(sumlist[k].VisaID))
                                                                    NewPaxList.VisaID = Convert.ToInt64(sumlist[k].VisaID);

                                                                NewPaxList.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                NewPaxList.PassportNUM = paxlist[i].Passport;
                                                                //Defect :2205
                                                                if (!string.IsNullOrEmpty(sumlist[k].BillingCode))
                                                                    NewPaxList.Billing = sumlist[k].BillingCode.Trim();
                                                                NewPaxList.OrderNUM = paxlist[i].OrderNUM;
                                                                NewPaxList.IsNonPassenger = false;
                                                                NewPaxList.IsBlocked = false;
                                                                NewPaxList.IsDeleted = false;
                                                                NewPaxList.StateName = sumlist[k].State;
                                                                NewPaxList.Street = sumlist[k].Street;
                                                                NewPaxList.CityName = sumlist[k].City;
                                                                NewPaxList.PostalZipCD = sumlist[k].Postal;
                                                                NewPaxList.LastUpdTS = DateTime.UtcNow;


                                                                Leg.PreflightPassengerLists.Add(NewPaxList);

                                                                #endregion
                                                            }
                                                        }
                                                        else
                                                        {
                                                            #region noPassenger in legs
                                                            //it is a new crew for this leg
                                                            if (Leg.PreflightPassengerLists == null)
                                                            {
                                                                Leg.PreflightPassengerLists = new List<PreflightPassengerList>();
                                                            }
                                                            PreflightPassengerList NewPaxList = new PreflightPassengerList();
                                                            NewPaxList.State = TripEntityState.Added;
                                                            NewPaxList.LegID = Leg.LegID;
                                                            NewPaxList.IsDeleted = false;
                                                            //add pax here 
                                                            NewPaxList.PassengerID = Convert.ToInt64(paxlist[i].PassengerRequestorID);

                                                            string[] lines = paxlist[i].Name.Split(' ');
                                                            int NameCount = 0;
                                                            foreach (string line in lines)
                                                            {
                                                                if (NameCount == 0)
                                                                    NewPaxList.PassengerLastName = paxlist[i].Name.Split(' ')[0].ToString().Replace(',', ' ').Trim();
                                                                if (NameCount == 1)
                                                                    NewPaxList.PassengerFirstName = paxlist[i].Name.Split(' ')[1].ToString();
                                                                if (NameCount == 2)
                                                                    NewPaxList.PassengerMiddleName = paxlist[i].Name.Split(' ')[2].ToString();
                                                                NameCount++;
                                                            }

                                                            if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                NewPaxList.PassportID = Convert.ToInt64(sumlist[k].PassportID);

                                                            if (!string.IsNullOrEmpty(sumlist[k].VisaID))
                                                                NewPaxList.VisaID = Convert.ToInt64(sumlist[k].VisaID);

                                                            NewPaxList.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                            NewPaxList.PassportNUM = paxlist[i].Passport;
                                                            //Defect :2205
                                                            if (!string.IsNullOrEmpty(sumlist[k].BillingCode))
                                                                NewPaxList.Billing = sumlist[k].BillingCode.Trim();
                                                            NewPaxList.OrderNUM = paxlist[i].OrderNUM;
                                                            NewPaxList.IsNonPassenger = false;
                                                            NewPaxList.IsBlocked = false;
                                                            NewPaxList.IsDeleted = false;
                                                            NewPaxList.LastUpdTS = DateTime.UtcNow;
                                                            NewPaxList.StateName = sumlist[k].State;
                                                            NewPaxList.Street = sumlist[k].Street;
                                                            NewPaxList.CityName = sumlist[k].City;
                                                            NewPaxList.PostalZipCD = sumlist[k].Postal;
                                                            Leg.PreflightPassengerLists.Add(NewPaxList);

                                                          
                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Tochange flightpurpose during edit when previously set
                                    if (Preflegs != null)
                                    {
                                        for (int legcnt = 0; legcnt < Preflegs.Count; legcnt++)
                                        {
                                            LegNum = Convert.ToInt16(paxlist[i].Legs[j].LegOrder);
                                            if (Preflegs[legcnt].LegNUM == LegNum)
                                            {
                                                if (Preflegs[legcnt].PreflightPassengerLists != null)
                                                {
                                                    for (int passcnt = 0; passcnt < Preflegs[legcnt].PreflightPassengerLists.Count; passcnt++)
                                                    {
                                                        if (Preflegs[legcnt].PreflightPassengerLists[passcnt].PassengerID == paxlist[i].PassengerRequestorID && Preflegs[legcnt].LegNUM == paxlist[i].Legs[j].LegOrder)
                                                        {
                                                            if (Preflegs[legcnt].PreflightPassengerLists[passcnt].PreflightPassengerListID != 0)
                                                            {
                                                                Preflegs[legcnt].PreflightPassengerLists[passcnt].State = TripEntityState.Deleted;
                                                                Preflegs[legcnt].PreflightPassengerLists[passcnt].IsDeleted = true;
                                                                Preflegs[legcnt].PreflightPassengerLists[passcnt].FlightPurposeID = 0;
                                                                #region hotel need to revisit

                                                                if (Preflegs[legcnt].PreflightHotelLists != null)
                                                                {
                                                                    foreach (PreflightHotelList Leghotel in Preflegs[legcnt].PreflightHotelLists.Where(x => x.IsDeleted == false).ToList())
                                                                    {
                                                                        if (Leghotel.PAXIDList != null && Leghotel.PAXIDList.Count > 0)
                                                                        {
                                                                            if (Leghotel.PreflightHotelListID != 0)
                                                                                Leghotel.State = TripEntityState.Modified;
                                                                            Leghotel.PAXIDList.Remove(Preflegs[legcnt].PreflightPassengerLists[passcnt].PassengerID);
                                                                        }
                                                                    }
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }

                    if (paxlist != null && paxlist.Count == 0) // Check imp
                    {
                        if (Preflegs != null)
                        {
                            for (int legscnt = 0; legscnt < Preflegs.Count; legscnt++)
                            {
                                if (Preflegs[legscnt].LegID != 0)
                                {
                                    Preflegs[legscnt].State = TripEntityState.Modified;
                                }

                                Preflegs[legscnt].ReservationTotal = 0;
                                Preflegs[legscnt].WaitNUM = 0;

                                #region blocked count

                                if (legscnt + 1 == 1)
                                {
                                    string strpax = hdnLeg1p1.Value;
                                    string stravailable = hdnLeg1a1.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 2)
                                {
                                    string strpax = hdnLeg2p2.Value;
                                    string stravailable = hdnLeg2a2.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 3)
                                {
                                    string strpax = hdnLeg3p3.Value;
                                    string stravailable = hdnLeg3a3.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 4)
                                {
                                    string strpax = hdnLeg4p4.Value;
                                    string stravailable = hdnLeg4a4.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 5)
                                {
                                    string strpax = hdnLeg5p5.Value;
                                    string stravailable = hdnLeg5a5.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 6)
                                {
                                    string strpax = hdnLeg6p6.Value;
                                    string stravailable = hdnLeg6a6.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 7)
                                {
                                    string strpax = hdnLeg7p7.Value;
                                    string stravailable = hdnLeg7a7.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 8)
                                {
                                    string strpax = hdnLeg8p8.Value;
                                    string stravailable = hdnLeg8a8.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 9)
                                {
                                    string strpax = hdnLeg9p9.Value;
                                    string stravailable = hdnLeg9a9.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 10)
                                {
                                    string strpax = hdnLeg10p10.Value;
                                    string stravailable = hdnLeg10a10.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 11)
                                {
                                    string strpax = hdnLeg11p11.Value;
                                    string stravailable = hdnLeg11a11.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 12)
                                {
                                    string strpax = hdnLeg12p12.Value;
                                    string stravailable = hdnLeg12a12.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 13)
                                {
                                    string strpax = hdnLeg13p13.Value;
                                    string stravailable = hdnLeg13a13.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 14)
                                {
                                    string strpax = hdnLeg14p14.Value;
                                    string stravailable = hdnLeg14a14.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 15)
                                {
                                    string strpax = hdnLeg15p15.Value;
                                    string stravailable = hdnLeg15a15.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 16)
                                {
                                    string strpax = hdnLeg16p16.Value;
                                    string stravailable = hdnLeg16a16.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 17)
                                {
                                    string strpax = hdnLeg17p17.Value;
                                    string stravailable = hdnLeg17a17.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 18)
                                {
                                    string strpax = hdnLeg18p18.Value;
                                    string stravailable = hdnLeg18a18.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 19)
                                {
                                    string strpax = hdnLeg19p19.Value;
                                    string stravailable = hdnLeg19a19.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 20)
                                {
                                    string strpax = hdnLeg20p20.Value;
                                    string stravailable = hdnLeg20a20.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 21)
                                {
                                    string strpax = hdnLeg21p21.Value;
                                    string stravailable = hdnLeg21a21.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 22)
                                {
                                    string strpax = hdnLeg22p22.Value;
                                    string stravailable = hdnLeg22a22.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 23)
                                {
                                    string strpax = hdnLeg23p23.Value;
                                    string stravailable = hdnLeg23a23.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 24)
                                {
                                    string strpax = hdnLeg24p24.Value;
                                    string stravailable = hdnLeg24a24.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 25)
                                {
                                    string strpax = hdnLeg25p25.Value;
                                    string stravailable = hdnLeg25a25.Value;
                                    if (!string.IsNullOrEmpty(strpax))
                                        Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    if (!string.IsNullOrEmpty(stravailable))
                                        Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }


                                #endregion
                            }
                        }

                    }
                    Session["CurrentPreFlightTrip"] = Trip;
                }

            }

        }

        #endregion

        #region Available PAX Delete
        protected void lnkPaxDelete_OnClick(object source, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsAuthorized(Permission.Preflight.DeletePreflightPassenger))
                        {

                            if (pnlPaxSummary.Items.Count > 0)
                            {
                                pnlPaxSummary.Items[0].Expanded = false;
                            }


                            if (dgAvailablePax.SelectedItems.Count > 0)
                            {
                                foreach (GridDataItem dataItem in dgAvailablePax.SelectedItems)
                                {
                                    Session["SelectedPaxitems"] += dataItem["PassengerRequestorID"].Text + ",";
                                }

                                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                                SavePreflightToSession();
                                RadWindowManager1.RadConfirm("Are you sure you want to delete this record", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else
                            {
                                if (dgAvailablePax.Items != null && dgAvailablePax.Items.Count > 0)
                                {
                                    RadWindowManager1.RadConfirm("Please select the record", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                                }
                                else
                                {
                                    RadWindowManager1.RadConfirm("No Passenger is found for deletion,Please add Passenger", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void Paxdelete()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    UpdateHeaderInformationBasedOnAvailablePAXGrid();
                    if (IsAuthorized(Permission.Preflight.DeletePreflightPassenger))
                    {
                        if (dgAvailablePax.SelectedItems.Count == 0)
                            if (Session["SelectedPaxitems"] != null)
                                foreach (GridDataItem dataItem in dgAvailablePax.Items)
                                    if (Session["SelectedPaxitems"].ToString().IndexOf(dataItem["PassengerRequestorID"].Text) >= 0)
                                        dataItem.Selected = true;

                        SavePreflightToSession();
                        List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                        List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();
                        FinalpassengerInLegs = (List<PassengerInLegs>)Session["AvailablePaxList"];
                        Finalpassengerlist = (List<PassengerSummary>)Session["PaxSummaryList"];


                        if (dgAvailablePax.SelectedItems.Count > 0)
                        {
                            foreach (GridDataItem dataItem in dgAvailablePax.SelectedItems)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                Int64 PaxID = Convert.ToInt64(dataItem["PassengerRequestorID"].Text);
                                //Convert.ToInt64((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PassengerRequestorID"]);

                                #region Legs
                                if (Trip.PreflightLegs != null)
                                {
                                    List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                    foreach (PreflightLeg Leg in Preflegs)
                                    {
                                        if (Leg.PreflightPassengerLists != null && Leg.PreflightPassengerLists.Count > 0)
                                        {
                                            List<PreflightPassengerList> PrefPaxlist = Leg.PreflightPassengerLists.Where(x => x.IsDeleted == false && x.PassengerID == PaxID).ToList();

                                            if (PrefPaxlist != null && PrefPaxlist.Count > 0)
                                            {
                                                foreach (PreflightPassengerList PAX in PrefPaxlist)
                                                {
                                                    if (PAX.PreflightPassengerListID > 0)
                                                    {
                                                        PAX.IsDeleted = true;
                                                        PAX.State = TripEntityState.Deleted;

                                                        Leg.PassengerTotal = Leg.PassengerTotal - 1;
                                                        Leg.ReservationAvailable = Leg.SeatTotal - Leg.PassengerTotal + 1;

                                                        Leg.State = TripEntityState.Modified;

                                                        #region hotel needs to revisit

                                                        if (Leg.PreflightHotelLists != null)
                                                        {
                                                            foreach (PreflightHotelList Leghotel in Leg.PreflightHotelLists.Where(x => x.IsDeleted == false).ToList())
                                                            {
                                                                if (Leghotel.PAXIDList != null && Leghotel.PAXIDList.Count > 0)
                                                                {
                                                                    if (Leghotel.PreflightHotelListID != 0)
                                                                        Leghotel.State = TripEntityState.Modified;
                                                                    long? PassengerID = PaxID;
                                                                    Leghotel.PAXIDList.Remove(PassengerID);
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                        Leg.PreflightPassengerLists.Remove(PAX);
                                                }

                                            }
                                        }
                                    }
                                }
                                #endregion

                               
                                for (int FinalAvlCnt = 0; FinalAvlCnt < FinalpassengerInLegs.Count; FinalAvlCnt++)
                                {
                                    if (FinalpassengerInLegs[FinalAvlCnt].PassengerRequestorID == PaxID)
                                    {
                                        FinalpassengerInLegs.Remove(FinalpassengerInLegs[FinalAvlCnt]);
                                    }
                                }

                                for (int FinalAssCnt = 0; FinalAssCnt < Finalpassengerlist.Count; FinalAssCnt++)
                                {
                                    if (Finalpassengerlist[FinalAssCnt].PaxID == PaxID)
                                    {
                                        Finalpassengerlist.Remove(Finalpassengerlist[FinalAssCnt]);
                                        FinalAssCnt = FinalAssCnt - 1;
                                    }
                                }

                                Int32 MAXOrderNUM = 0;
                                foreach (PassengerInLegs Pax in FinalpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    MAXOrderNUM++;
                                    Pax.OrderNUM = MAXOrderNUM;

                                    foreach (PassengerSummary PaxSum in Finalpassengerlist.Where(x => x.PaxID == Pax.PassengerRequestorID).ToList())
                                    {
                                        PaxSum.OrderNum = Pax.OrderNUM;
                                    }
                                }

                                Session.Remove("SelectedPaxitems");
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session["AvailablePaxList"] = FinalpassengerInLegs;
                                if (Session["AvailablePaxList"] != null)
                                {
                                    dgAvailablePax.DataSource = Session["AvailablePaxList"];
                                    dgAvailablePax.DataBind();
                                }
                                UpdateHeaderInformationBasedOnAvailablePAXGrid();
                                Session["PaxSummaryList"] = Finalpassengerlist;
                                if (Session["PaxSummaryList"] != null)
                                {
                                    List<PassengerSummary> paxSummary = new List<PassengerSummary>();
                                    paxSummary = (List<PassengerSummary>)Session["PaxSummaryList"];
                                    //001
                                    dgPaxSummary.DataSource = paxSummary.OrderBy(x => x.LegId).ThenBy(x => x.OrderNum).ToList();
                                    dgPaxSummary.DataBind();
                                }

                            }

                        }
                    }

                }
            }

        }
        protected void btnDeletePaxYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Paxdelete();

                RadAjaxManager.GetCurrent(Page).FocusControl(btnCheckTsa);
            }
        }
        protected void btnDeletePaxNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnCheckTsa);
            // Your code for "No"
        }
        #endregion

        #region Pax Summary Events
        protected void pnlPaxSummary_Click(object sender, RadPanelBarEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (pnlPaxSummary.Items[0].Expanded == true)
                {
                    Session["AvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                    UpdatePaxSummaryBasedonAvailableGrid(true);
                    dgPaxSummary.Rebind();
                }
                else
                {
                    SavePreflightToSession();
                }
            }
        }
        #region Apply Passport to All legs
        protected void btnApplyPAXAllLegsYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Session["ApplyPAXAllLegs"] = true;
                if (Session["PaxSummaryList"] != null)
                {
                    List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
                    paxSummaryList = RetainPaxSummaryGrid(dgPaxSummary); // (List<PassengerSummary>)Session["PaxSummaryList"];
                    dgPaxSummary.VirtualItemCount = paxSummaryList.Count;
                    //001
                    dgPaxSummary.DataSource = paxSummaryList.OrderBy(x => x.LegId).ThenBy(x => x.OrderNum).ToList();
                    dgPaxSummary.DataBind();
                }
                Session.Remove("ApplyPAXAllLegs");
            }
        }
        protected void btnApplyPAXAllLegsNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Session["ApplyPAXAllLegs"] = false;
                if (Session["PaxSummaryList"] != null)
                {
                    List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
                    paxSummaryList = RetainPaxSummaryGrid(dgPaxSummary);  //(List<PassengerSummary>)Session["PaxSummaryList"];
                    dgPaxSummary.VirtualItemCount = paxSummaryList.Count;
                    //001
                    dgPaxSummary.DataSource = paxSummaryList.OrderBy(x => x.LegId).ThenBy(x => x.OrderNum).ToList();
                    dgPaxSummary.DataBind();
                }
                Session.Remove("ApplyPAXAllLegs");
            }
        }
        #endregion
        protected void dgPaxSummary_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                            if (e.CommandName == "RowClicksdf")
                            {
                                GridDataItem test = (GridDataItem)e.Item;
                                int index = e.Item.ItemIndex;
                                e.Item.Selected = true;
                                hdnPassPax.Value = test.GetDataKeyValue("PaxID").ToString();
                                int legid = Convert.ToInt16(test.GetDataKeyValue("LegID"));
                                //lblLegDisplay.Text = "Leg: " + test.GetDataKeyValue("LegID").ToString();

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPaxSummary_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            //if (Trip.Mode == TripActionMode.Edit)
                            //{

                            foreach (GridDataItem dataItem in dgPaxSummary.Items)
                            {
                                if (dataItem.Selected)
                                {
                                    Label lblPassengerRequestorID = (Label)dataItem.FindControl("lblPassengerRequestorID");
                                    hdnPassPax.Value = lblPassengerRequestorID.Text;


                                    hdnPassPax.Value = dataItem.GetDataKeyValue("PaxID").ToString();
                                    int legid = Convert.ToInt16(dataItem.GetDataKeyValue("LegID"));
                                    //lblLegDisplay.Text = "Leg: " + test.GetDataKeyValue("LegID").ToString();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPaxSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridPagerItem)
                        {
                            GridPagerItem pager = (GridPagerItem)e.Item;
                            Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                            lbl.Visible = false;

                            RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                            combo.Visible = false;
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            Label lblPassengerRequestorID = (Label)dataItem.FindControl("lblPassengerRequestorID");
                            Label lblLegID = (Label)dataItem.FindControl("lblLegID");

                            //if (e.Item.Selected)
                            //    hdnPassPax.Value = lblPassengerRequestorID.Text;
                            TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                            HiddenField hdnPassID = (HiddenField)dataItem.FindControl("hdnPassID");



                            HiddenField hdnVisaID = (HiddenField)dataItem.FindControl("hdnVisaID");

                            if (Session["ApplyPAXAllLegs"] != null && (bool)Session["ApplyPAXAllLegs"])
                            {
                                if (hdnPassPax.Value != null && lblPassengerRequestorID.Text == hdnPassPax.Value)
                                {
                                    if (Session["SelectedPassport"] != null)
                                        tbPassport.Text = Session["SelectedPassport"].ToString();
                                    if (Session["SelectedPassportID"] != null)
                                        hdnPassID.Value = Session["SelectedPassportID"].ToString();
                                    if (Session["SelectedVisaID"] != null)
                                        hdnVisaID.Value = Session["SelectedVisaID"].ToString();
                                    if (Session["SelectedPrefPaxPassportExpiryDT"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(Session["SelectedPrefPaxPassportExpiryDT"].ToString()))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(Session["SelectedPrefPaxPassportExpiryDT"]);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                dataItem["PassportExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (DateTime)dtDate));
                                            else
                                                dataItem["PassportExpiryDT"].Text = string.Empty;
                                        }
                                    }
                                    if (Session["SelectedPrefPaxCountryCD"] != null)
                                    {
                                        dataItem["Nation"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(Session["SelectedPrefPaxCountryCD"].ToString());
                                    }

                                    // Fix for UW-1083 (7484) 
                                    //Session.Remove("SelectedPrefPaxCountryCD");
                                    //Session.Remove("SelectedPrefPaxPassportExpiryDT");

                                    if (dataItem["IssueDT"].Text != "&nbsp;")
                                    {
                                        if (dataItem["IssueDT"].Text.Trim() == "01/01/0001" || dataItem["IssueDT"].Text.Trim() == "1/1/0001 12:00:00 AM" || dataItem["IssueDT"].Text.Trim() == "01/01/1900" || dataItem["IssueDT"].Text.IndexOf("1900") >= 0 || dataItem["IssueDT"].ToString() == "01/01/1900 00:00:00" || dataItem["IssueDT"].Text.Trim() == DateTime.MinValue.ToShortDateString())
                                        {
                                            dataItem["IssueDT"].Text = string.Empty;
                                        }
                                    }
                                }

                            }
                            else
                            {

                                if (tbPassport != null && hdnPassPax != null && hdnPassPax.Value != null && lblPassengerRequestorID.Text == hdnPassPax.Value && hdnAssignPassLeg.Value != null && lblLegID.Text == hdnAssignPassLeg.Value)
                                {
                                    if (Session["SelectedPassport"] != null)
                                        tbPassport.Text = Session["SelectedPassport"].ToString();
                                    if (Session["SelectedPassportID"] != null)
                                        hdnPassID.Value = Session["SelectedPassportID"].ToString();
                                    if (Session["SelectedVisaID"] != null)
                                        hdnVisaID.Value = Session["SelectedVisaID"].ToString();
                                    if (Session["SelectedPrefPaxPassportExpiryDT"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(Session["SelectedPrefPaxPassportExpiryDT"].ToString()))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(Session["SelectedPrefPaxPassportExpiryDT"]);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                dataItem["PassportExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (DateTime)dtDate));
                                            else
                                                dataItem["PassportExpiryDT"].Text = string.Empty;
                                        }
                                    }
                                    if (Session["SelectedPrefPaxCountryCD"] != null)
                                    {
                                        dataItem["Nation"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(Session["SelectedPrefPaxCountryCD"].ToString());
                                    }
                                    Session.Remove("SelectedPrefPaxCountryCD");
                                    Session.Remove("SelectedPrefPaxPassportExpiryDT");

                                    if (dataItem["IssueDT"].Text != "&nbsp;")
                                    {
                                        if (dataItem["IssueDT"].Text.Trim() == "01/01/0001" || dataItem["IssueDT"].Text.Trim() == "1/1/0001 12:00:00 AM" || dataItem["IssueDT"].Text.Trim() == "01/01/1900" || dataItem["IssueDT"].Text.IndexOf("1900") >= 0 || dataItem["IssueDT"].ToString() == "01/01/1900 00:00:00" || dataItem["IssueDT"].Text.Trim() == DateTime.MinValue.ToShortDateString())
                                        {
                                            dataItem["IssueDT"].Text = string.Empty;
                                        }
                                    }
                                }
                            }

                            #region "Passport choice"
                            if (!string.IsNullOrEmpty(hdnPassID.Value))
                            {
                                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetPassportbyPassportId(Convert.ToInt64(hdnPassID.Value), false).EntityList;
                                    if (objRetVal != null && objRetVal.Count > 0)
                                    {
                                        if (objRetVal[0].IssueDT != null)
                                            dataItem["IssueDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", (DateTime)objRetVal[0].IssueDT));
                                        else
                                            dataItem["IssueDT"].Text = string.Empty;
                                        CheckBox ChkChoice = (CheckBox)dataItem.FindControl("ChkChoice");

                                        if (objRetVal[0].Choice == true)
                                        {
                                            if (ChkChoice != null)
                                                ChkChoice.Checked = true;
                                            //dataItem["Choice"].Text = "True";
                                        }
                                        else
                                        {
                                            if (ChkChoice != null)
                                                ChkChoice.Checked = false;
                                            //dataItem["Choice"].Text = "False";
                                        }
                                    }
                                }
                            }
                            #endregion

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void dgPaxSummary_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
            try
            {
                if (Session["PaxSummaryList"] != null)
                {
                    paxSummaryList = (List<PassengerSummary>)Session["PaxSummaryList"];
                    dgPaxSummary.VirtualItemCount = paxSummaryList.Count;
                    //001
                    dgPaxSummary.DataSource = paxSummaryList.OrderBy(x => x.LegId).ThenBy(x => x.OrderNum).ToList(); ;
                    // dgPaxSummary.DataBind();
                }
            }
            catch (Exception ex)
            { }
        }
        private void checkTSA()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdnTSA.Value))
                {
                    using (PreflightServiceClient PrefService = new PreflightServiceClient())
                    {

                        var objRetVal = PrefService.GetPassengerbyIDOrCD(Convert.ToInt64(Convert.ToInt64(hdnTSA.Value)), string.Empty);
                        if (objRetVal.ReturnFlag == true)
                            _getPassenger = objRetVal.EntityList;
                        //var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(dataitem["PassengerRequestorID"].Text));
                        if (_getPassenger != null && _getPassenger.Count > 0)
                        {

                            //var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(dataitem["PassengerRequestorID"].Text));
                            var objPaxRetVal = _getPassenger[0];

                            if (objPaxRetVal != null)
                            {
                                //var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(hdnTSA.Value));

                                tbStaus.Enabled = false;
                                if (objPaxRetVal.TSAStatus == "C")
                                {
                                    if (objPaxRetVal.TSADTTM == null)
                                    {
                                        tbStaus.Text = "Cleared";
                                    }
                                    else
                                    {
                                        tbStaus.Text = "Cleared  " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", ((DateTime)objPaxRetVal.TSADTTM));
                                    }
                                    tbStaus.BackColor = System.Drawing.Color.Green;
                                }
                                else if (objPaxRetVal.TSAStatus == "S")
                                {
                                    if (objPaxRetVal.TSADTTM == null)
                                    {
                                        tbStaus.Text = "Selectee";
                                    }
                                    else
                                    {
                                        tbStaus.Text = "Selectee  " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", ((DateTime)objPaxRetVal.TSADTTM));

                                    }
                                    tbStaus.BackColor = Color.Yellow;
                                }
                                else if (objPaxRetVal.TSAStatus == "R")
                                {
                                    if (objPaxRetVal.TSADTTM == null)
                                    {
                                        tbStaus.Text = "Restricted";
                                    }
                                    else
                                    {
                                        tbStaus.Text = "Restricted  " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", ((DateTime)objPaxRetVal.TSADTTM));
                                    }
                                    tbStaus.BackColor = Color.Red;
                                }
                                else
                                {
                                    tbStaus.Text = "N/A";
                                    tbStaus.BackColor = Color.DarkGray;
                                }
                            }

                        }
                    }
                }
            }
        }
        protected void btnCheckTsa_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(dgPaxSummary);

                        if (dgAvailablePax.SelectedItems.Count > 0)
                        {
                            GridDataItem dataItem = (GridDataItem)dgAvailablePax.SelectedItems[0];
                            hdnTSA.Value = dataItem["PassengerRequestorID"].Text;

                        }
                        else
                        {
                            if (dgAvailablePax.Items != null && dgAvailablePax.Items.Count > 0)
                                RadWindowManager1.RadAlert("Please select the record", 330, 100, "Trip Alert!", null, null);

                        }

                        checkTSA();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        #endregion

        #region PAX Logistics - Hotel
        protected void rtsLegHotel_TabClick(object sender, RadTabStripEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveHotel.Enabled)
                        {
                            RadTab rtLegSelprev = rtsLegHotel.FindTabByValue(hdnLegNumHotel.Value);
                            if (rtLegSelprev != null)
                                rtLegSelprev.Selected = true;

                            RadWindowManager1.RadAlert("Please save or cancel hotel information", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                        }
                        else
                        {
                         
                            hdnLegNumHotel.Value = e.Tab.Value;
                            UpdateHiddenAirportIDsBasedOnLegnum(Convert.ToInt64(e.Tab.Value));



                            RadMultiPage1.SelectedIndex = 0;
                            BindLegHotel(true);
                            BindLegPax(true);
                            ClearHotel();
                            if (dgLegHotel.Items.Count > 0)
                            {
                                dgLegHotel.Items[0].Selected = true;
                                GridDataItem HotelItem = (GridDataItem)dgLegHotel.Items[0];
                                hdnHotelIdentifier.Value = HotelItem.GetDataKeyValue("HotelIdentifier").ToString();
                                LoadHotel();
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgLegEntity_BindData(object source, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindLegPax(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgLegEntity_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;



                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void NoPAX_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkNoPAX.Checked)
                        {
                            if (dgLegEntity.Items.Count > 0)
                            {
                                GridHeaderItem hItem = (GridHeaderItem)(dgLegEntity.MasterTableView.GetItems(GridItemType.Header))[0];
                                CheckBox chkSelectHeader = (CheckBox)hItem.FindControl("chkSelectHeader");
                                chkSelectHeader.Checked = false;

                                foreach (GridDataItem item in dgLegEntity.Items)
                                {
                                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                                    chkSelect.Checked = false;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void rbArriveDepart_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindowManager1.RadConfirm("Changing Arrival or Departure will clear hotel information. Do you want to continue?", "confirmrbArrivalDepartCallBackFn", 330, 100, null, "Confirmation!");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgLegHotel_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                GridEnable(false, true, false);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                if (!btnSaveHotel.Enabled)
                                {
                                    EnableHotelForm(true);
                                    ClearHotel();
                                    hdnHotelIdentifier.Value = "0";
                                    GridEnable(true, false, false);
                                }
                                break;
                            case RadGrid.DeleteSelectedCommandName:
                                break;
                            case "RowClick":
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgLegHotel_BindData(object source, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindLegHotel(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgLegHotel_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            Label lblArrDep = (Label)Item.FindControl("lblArrDep");
                            bool isArrival = false;
                            if (Item.GetDataKeyValue("isArrivalHotel") != null)
                                isArrival = Convert.ToBoolean(Item.GetDataKeyValue("isArrivalHotel"));
                            if (isArrival)
                                lblArrDep.Text = "A";
                            else
                                lblArrDep.Text = "D";

                            #region PAXCode
                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (!string.IsNullOrEmpty(hdnLegNumHotel.Value))
                                {
                                    PreflightLeg Leg = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumHotel.Value)).SingleOrDefault();

                                    if (Leg != null && Leg.PreflightHotelLists != null)
                                    {
                                        Int64 hotelidentifier = 0;
                                        if (Item.GetDataKeyValue("HotelIdentifier") != null)
                                            hotelidentifier = Convert.ToInt64(Item.GetDataKeyValue("HotelIdentifier"));

                                        PreflightHotelList Hotel = Leg.PreflightHotelLists.Where(x => x.HotelIdentifier == hotelidentifier && x.IsDeleted == false).SingleOrDefault();

                                        Label lblCDs = (Label)Item.FindControl("lblCDs");
                                        lblCDs.Text = "None";

                                        if (Hotel != null)
                                        {
                                            if (Hotel.PAXIDList != null && Hotel.PAXIDList.Count > 0)
                                            {
                                                string PAXCD = string.Empty;

                                                if (Hotel.PAXIDList.Count >= dgLegEntity.Items.Count)
                                                    PAXCD = "All";
                                                else
                                                {
                                                    foreach (Int64 PAXID in Hotel.PAXIDList)
                                                    {
                                                        #region hotelcrew
                                                        if (Session["PaxSummaryList"] != null)
                                                        {
                                                            List<PassengerSummary> paxSummaryList = (List<PassengerSummary>)Session["PaxSummaryList"];
                                                            if (paxSummaryList != null && paxSummaryList.Count > 0)
                                                            {
                                                                List<PassengerSummary> BindList = paxSummaryList.Where(x => x.LegId == Convert.ToInt64(hdnLegNumHotel.Value) && x.PaxID == PAXID).ToList();
                                                                if (BindList != null && BindList.Count > 0)
                                                                {
                                                                    PAXCD += BindList[0].PaxCode + ", ";
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }

                                                    if (PAXCD != string.Empty)
                                                        PAXCD = PAXCD.Substring(0, PAXCD.Length - 2);
                                                }

                                                lblCDs.Text = Microsoft.Security.Application.Encoder.HtmlEncode(PAXCD) == string.Empty ? "None" : Microsoft.Security.Application.Encoder.HtmlEncode(PAXCD);

                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void btnSaveHotel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(lbcvHotelCode.Text))
                            RadWindowManager1.RadAlert(lbcvHotelCode.Text, 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                        else if (string.IsNullOrEmpty(tbHotelName.Text))
                            RadWindowManager1.RadAlert("Hotel Code should not be Empty!", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                        else if (string.IsNullOrEmpty(tbHotelName.Text) && string.IsNullOrEmpty(hdnHotelID.Value))
                            RadWindowManager1.RadAlert("Hotel Code should not be Empty!", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                        else if (!string.IsNullOrEmpty(tbHotelCode.Text) && CheckHotelExists(tbHotelCode.Text))
                            RadWindowManager1.RadAlert("Hotel Code " + tbHotelCode.Text + " already exists", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                        else
                            SaveHotel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void btnCancelHotel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegHotel.Items.Count > 0)
                            hdnHotelIdentifier.Value = "1";

                        EnableHotelForm(false);
                        LoadHotel();
                        GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void lnkLegHotelDelete_OnClick(object source, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsAuthorized(Permission.Preflight.DeletePreflightCrewHotel))
                        {
                            if (dgLegHotel.SelectedItems.Count > 0)
                            {
                                foreach (GridDataItem dataItem in dgLegHotel.SelectedItems)
                                {
                                    Session["HotelToDelete"] += dataItem.GetDataKeyValue("HotelIdentifier").ToString() + ",";
                                }

                                RadWindowManager1.RadConfirm("Are you sure you want to delete this record?", "confirmDeleteHotelCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else
                            {
                                if (dgLegHotel.Items != null && dgLegHotel.Items.Count > 0)
                                    RadWindowManager1.RadAlert("Please select the record to delete!", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                                else
                                    RadWindowManager1.RadAlert("No hotels found for deletion. Please add hotel!", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void btnDeleteHotelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DeleteHotel();
                        BindLegHotel(true);
                        if (dgLegHotel.Items.Count > 0)
                        {
                            dgLegHotel.Items[0].Selected = true;
                            GridDataItem HotelItem = (GridDataItem)dgLegHotel.Items[0];
                            hdnHotelIdentifier.Value = HotelItem.GetDataKeyValue("HotelIdentifier").ToString();
                            LoadHotel();
                        }
                        else
                        {
                            ClearHotel();
                            hdnHotelIdentifier.Value = "0";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void btnDeleteHotelNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }
        protected void lnkLegHotelEdit_OnClick(object source, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegHotel.Items.Count > 0)
                        {
                            if (dgLegHotel.SelectedItems.Count > 0)
                            {
                                GridDataItem selectedItem = (GridDataItem)dgLegHotel.SelectedItems[0];
                                if (selectedItem != null)
                                {
                                    GridEnable(false, true, false);
                                    hdnHotelIdentifier.Value = selectedItem.GetDataKeyValue("HotelIdentifier").ToString();
                                    selectedItem.Selected = true;
                                    EnableHotelForm(true);
                                }
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Select any one record to edit!", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void dgLegHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem selItem = dgLegHotel.SelectedItems[0] as GridDataItem;
                        hdnHotelIdentifier.Value = selItem.GetDataKeyValue("HotelIdentifier").ToString();
                        LoadHotel();
                        GridEnable(true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }

        private bool CheckHotelExists(string HotelCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HotelCode))
            {
                bool result = false;

                if ((hdnHotelIdentifier.Value == "0") || (hdnHotelIdentifier.Value != "0" && hdnHotelchanged.Value == "true"))
                {
                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        if (!string.IsNullOrEmpty(hdnLegNumHotel.Value))
                        {
                            PreflightLeg Leg = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumHotel.Value)).SingleOrDefault();

                            if (Leg != null && Leg.PreflightHotelLists != null)
                            {
                                bool isArrival = false;
                                bool isDepart = true;
                                if (rbArriveDepart.Items[0].Selected)
                                {
                                    isArrival = true;
                                    isDepart = false;

                                }


                                List<PreflightHotelList> hotelList = new List<PreflightHotelList>();
                                if (hdnHotelIdentifier.Value != "0")
                                    hotelList = Leg.PreflightHotelLists.Where(x => x.IsDeleted == false && x.HotelIdentifier != Convert.ToInt64(hdnHotelIdentifier.Value) && x.isArrivalHotel == isArrival && x.isDepartureHotel == isDepart).ToList();
                                else
                                    hotelList = Leg.PreflightHotelLists.Where(x => x.IsDeleted == false && x.isArrivalHotel == isArrival && x.isDepartureHotel == isDepart).ToList();

                                if (hotelList.Count > 0)
                                {
                                    var retVal = (from hotel in hotelList
                                                  where hotel.HotelCode == HotelCode
                                                  select hotel);

                                    if (retVal.Count() > 0)
                                        result = true;
                                }
                            }
                        }
                    }
                }
                return result;
            }
        }
        private void BindLegPax(bool isRebind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isRebind))
            {
                chkNoPAX.Checked = false;
                if (Session["PaxSummaryList"] != null)
                {
                    List<PassengerSummary> paxSummaryList = (List<PassengerSummary>)Session["PaxSummaryList"];
                    if (paxSummaryList != null && paxSummaryList.Count > 0)
                    {
                        List<PassengerSummary> BindList = paxSummaryList.Where(x => x.LegId == Convert.ToInt64(hdnLegNumHotel.Value)).ToList();
                        if (BindList != null)
                        {
                            dgLegEntity.DataSource = paxSummaryList.Where(x => x.LegId == Convert.ToInt64(hdnLegNumHotel.Value)).ToList();
                            if (isRebind)
                                dgLegEntity.DataBind();
                        }
                        else
                        {
                            dgLegEntity.DataSource = new string[] { };
                            if (isRebind)
                                dgLegEntity.DataBind();
                        }
                    }
                    else
                    {
                        dgLegEntity.DataSource = new string[] { };
                        if (isRebind)
                            dgLegEntity.DataBind();
                    }
                }
                else
                {
                    dgLegEntity.DataSource = new string[] { };
                    if (isRebind)
                        dgLegEntity.DataBind();
                }


                if (dgLegEntity.Items.Count > 0)
                    chkNoPAX.Checked = false;
                else
                    chkNoPAX.Checked = true;
            }
        }
        private void BindLegHotel(bool isRebind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isRebind))
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (!string.IsNullOrEmpty(hdnLegNumHotel.Value))
                    {
                        PreflightLeg Leg = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumHotel.Value)).SingleOrDefault();

                        if (Leg != null)
                        {
                            tbDateIn.Text = string.Empty;
                            hdnArrivalDate.Value = string.Empty;

                            tbDateOut.Text = string.Empty;
                            hdnNextDeptDate.Value = string.Empty;

                            if (Leg.DepartureDTTMLocal != null)
                            {
                                tbDateIn.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.ArrivalDTTMLocal);
                                hdnArrivalDate.Value = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.ArrivalDTTMLocal);
                            }

                            PreflightLeg NextLeg = Trip.PreflightLegs.Where(x => x.LegNUM == (Leg.LegNUM + 1) && x.IsDeleted == false).SingleOrDefault();
                            if (NextLeg != null)
                            {
                                tbDateOut.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", NextLeg.DepartureDTTMLocal);
                                hdnNextDeptDate.Value = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", NextLeg.DepartureDTTMLocal);
                            }
                        }
                        if (Leg != null && Leg.PreflightHotelLists != null)
                        {
                            dgLegHotel.DataSource = Leg.PreflightHotelLists.Where(x => x.IsDeleted == false && x.crewPassengerType == "P").OrderBy(x => x.PreflightHotelName).ToList();
                            if (isRebind)
                                dgLegHotel.DataBind();
                        }
                        else
                        {
                            dgLegHotel.DataSource = new string[] { };
                            if (isRebind)
                                dgLegHotel.DataBind();
                        }
                    }
                    else
                    {
                        dgLegHotel.DataSource = new string[] { };
                        if (isRebind)
                            dgLegHotel.DataBind();
                    }
                }
                else
                {
                    dgLegHotel.DataSource = new string[] { };
                    if (isRebind)
                        dgLegHotel.DataBind();
                }
            }
        }
        private void EnableHotelForm(bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isEnable))
            {
                rbArriveDepart.Enabled = isEnable;
                radlstArrangements.Enabled = isEnable;
                ddlHotelCompleteds.Enabled = isEnable;
                chkUniversalPreferences.Enabled = isEnable;
                tbHotelCode.Enabled = isEnable;
                tbHotelRate.Enabled = isEnable;
                tbHotelName.Enabled = isEnable;
                tbAddr1.Enabled = isEnable;
                tbAddr2.Enabled = isEnable;
                tbAddr3.Enabled = isEnable;
                tbCity.Enabled = isEnable;
                tbState.Enabled = isEnable;
                tbCtry.Enabled = isEnable;
                tbMetro.Enabled = isEnable;
                tbPost.Enabled = isEnable;
                tbHotelPhone.Enabled = isEnable;
                tbHotelFax.Enabled = isEnable;
                chkNoPAX.Enabled = isEnable;
                dgLegEntity.Enabled = isEnable;
                chkEarlyCheckin.Enabled = isEnable;
                chkSmoking.Enabled = isEnable;
                tbDateIn.Enabled = isEnable;
                tbNoforooms.Enabled = isEnable;
                tbDateOut.Enabled = isEnable;
                tbNoofBeds.Enabled = isEnable;
                ddltypeofRoom.Enabled = isEnable;
                ddlRoomDesc.Enabled = isEnable;
                //radRequestOnly.Enabled = isEnable;
                chkRequestOnly.Enabled = isEnable;
                chkBookNightPrior.Enabled = isEnable;
                tbClubCard.Enabled = isEnable;
                tbMaxcapamt.Enabled = isEnable;
                chkRatecap.Enabled = isEnable;
                tbConfirmation.Enabled = isEnable;
                tbComments.Enabled = isEnable;

                btnCancelHotel.Enabled = isEnable;
                btnSaveHotel.Enabled = isEnable;
                btnCode.Enabled = isEnable;
                btnCtry.Enabled = isEnable;
                btnMetro.Enabled = isEnable;

                btnCancelHotel.CssClass = "ui_nav";
                btnSaveHotel.CssClass = "ui_nav";

                btnCode.CssClass = "browse-button";
                btnCtry.CssClass = "browse-button";
                btnMetro.CssClass = "browse-button";

                if (!isEnable)
                {
                    btnCancelHotel.CssClass = "ui_nav_disable";
                    btnSaveHotel.CssClass = "ui_nav_disable";

                    btnCode.CssClass = "browse-button-disabled";
                    btnCtry.CssClass = "browse-button-disabled";
                    btnMetro.CssClass = "browse-button-disabled";
                }
            }
        }

        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LinkButton lbtnInsertCtl = (LinkButton)dgLegHotel.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                        LinkButton lbtnDelCtl = (LinkButton)dgLegHotel.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                        LinkButton lbtnEditCtl = (LinkButton)dgLegHotel.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");

                        if (IsAuthorized(Permission.Preflight.AddPreflightCrewHotel))
                        {
                            lbtnInsertCtl.Visible = true;
                            if (Add)
                                lbtnInsertCtl.Enabled = true;
                            else
                                lbtnInsertCtl.Enabled = false;
                        }
                        else
                        {
                            lbtnInsertCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Preflight.DeletePreflightCrewHotel))
                        {
                            lbtnDelCtl.Visible = true;
                            if (Delete)
                                lbtnDelCtl.Enabled = true;
                            else
                                lbtnDelCtl.Enabled = false;
                        }
                        else
                        {
                            lbtnDelCtl.Visible = false;
                        }

                        if (IsAuthorized(Permission.Preflight.EditPreflightCrewHotel))
                        {
                            lbtnEditCtl.Visible = true;
                            if (Edit)
                                lbtnEditCtl.Enabled = true;
                            else
                                lbtnEditCtl.Enabled = false;
                        }
                        else
                        {
                            lbtnEditCtl.Visible = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void LoadHotel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (IsAuthorized(Permission.Preflight.ViewPreflightCrewHotel))
                {

                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        Int64 LegNum = 0;
                        if (!string.IsNullOrEmpty(hdnLegNumHotel.Value))
                            LegNum = Convert.ToInt64(hdnLegNumHotel.Value);

                        Int64 HotelIdentifier = 0;
                        if (!string.IsNullOrEmpty(hdnHotelIdentifier.Value))
                            HotelIdentifier = Convert.ToInt64(hdnHotelIdentifier.Value);

                        ClearHotel();

                        if (dgLegHotel.Items.Count > 0)
                        {
                            foreach (GridDataItem item in dgLegHotel.Items)
                            {
                                if (item.GetDataKeyValue("HotelIdentifier") != null && item.GetDataKeyValue("HotelIdentifier").ToString() == HotelIdentifier.ToString())
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }

                        PreflightLeg LegToLoad = new PreflightLeg();

                        LegToLoad = Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNum).SingleOrDefault();

                        if (LegToLoad != null)
                        {
                            PreflightHotelList PrefHotel = new PreflightHotelList();
                            if (LegToLoad.PreflightHotelLists != null && HotelIdentifier > 0) // Search for hotel identifier 
                            {
                                PrefHotel = LegToLoad.PreflightHotelLists.Where(x => x.HotelIdentifier == HotelIdentifier && x.IsDeleted == false).SingleOrDefault();
                            }

                            if (PrefHotel != null)
                            {
                                #region Update UI Details
                                tbHotelName.Text = PrefHotel.PreflightHotelName;
                                tbHotelCode.Text = PrefHotel.HotelCode;

                                if (PrefHotel.HotelID != null)
                                    hdnHotelID.Value = PrefHotel.HotelID.ToString();

                                if (PrefHotel.isArrivalHotel != null && (bool)PrefHotel.isArrivalHotel)
                                {
                                    rbArriveDepart.SelectedValue = "0";
                                    hdnrbArriveDepartPrev.Value = "0";
                                }
                                else
                                {
                                    rbArriveDepart.SelectedValue = "1";
                                    hdnrbArriveDepartPrev.Value = "1";
                                }

                                //PrefHotel.RoomType  //Commented for time being
                                //if (PrefHotel.RoomRent != null)
                                //    tbHotelRate.Text = ((decimal)PrefHotel.RoomRent).ToString();

                                tbConfirmation.Text = PrefHotel.RoomConfirm;

                                if (PrefHotel.RoomDescription != string.Empty)
                                    ddlRoomDesc.SelectedValue = PrefHotel.RoomDescription;

                                tbCity.Text = PrefHotel.CityName;
                                tbState.Text = PrefHotel.StateName;
                                tbPost.Text = PrefHotel.PostalZipCD;
                                tbHotelPhone.Text = PrefHotel.PhoneNum1;
                                tbHotelFax.Text = PrefHotel.FaxNUM;
                                //Defaults to none
                                //radlstArrangements.Items[2].Selected = true;
                                radlstArrangements.SelectedValue = "None";

                                if (!string.IsNullOrEmpty(PrefHotel.HotelArrangedBy))
                                {
                                    if (PrefHotel.HotelArrangedBy == "UWA")
                                    {
                                        radlstArrangements.SelectedValue = "UWA";
                                        //radlstArrangements.Items[0].Selected = true;
                                    }
                                    if (PrefHotel.HotelArrangedBy == "Client")
                                    {
                                        radlstArrangements.SelectedValue = "Client";
                                        //radlstArrangements.Items[1].Selected = true;
                                    }
                                    if (PrefHotel.HotelArrangedBy == "None")
                                    {
                                        radlstArrangements.SelectedValue = "None";
                                        //radlstArrangements.Items[2].Selected = true;
                                    }
                                }

                                if (PrefHotel.Rate != null)
                                    tbHotelRate.Text = PrefHotel.Rate.ToString();

                                if (PrefHotel.DateIn != null)
                                {
                                    tbDateIn.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", PrefHotel.DateIn);
                                }

                                if (PrefHotel.DateOut != null)
                                {
                                    tbDateOut.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", PrefHotel.DateOut);
                                }
                                if (PrefHotel.IsSmoking != null)
                                    chkSmoking.Checked = (bool)PrefHotel.IsSmoking;

                                if (PrefHotel.RoomTypeID != null)
                                    ddltypeofRoom.SelectedValue = PrefHotel.RoomTypeID.ToString();

                                if (PrefHotel.NoofBeds != null)
                                    tbNoofBeds.Text = PrefHotel.NoofBeds.ToString();

                                PrefHotel.IsAllCrewOrPax = false;

                                //PrefHotel.IsCompleted	
                                if (PrefHotel.IsEarlyCheckIn != null)
                                    chkEarlyCheckin.Checked = (bool)PrefHotel.IsEarlyCheckIn;

                                //if (PrefHotel.EarlyCheckInOption != null && PrefHotel.EarlyCheckInOption == "R")
                                //    radRequestOnly.Items[0].Selected = true;
                                //else
                                //    radRequestOnly.Items[1].Selected = true;

                                if (PrefHotel.IsRequestOnly != null && PrefHotel.IsRequestOnly.HasValue)
                                    chkRequestOnly.Checked = (bool)PrefHotel.IsRequestOnly;

                                if (PrefHotel.IsBookNight != null && PrefHotel.IsBookNight.HasValue)
                                    chkBookNightPrior.Checked = (bool)PrefHotel.IsBookNight;

                                if (!string.IsNullOrEmpty(PrefHotel.SpecialInstructions))
                                    chkUniversalPreferences.Checked = Convert.ToBoolean(PrefHotel.SpecialInstructions);

                                tbConfirmation.Text = PrefHotel.ConfirmationStatus;

                                tbComments.Text = PrefHotel.Comments;
                                tbAddr1.Text = PrefHotel.Address1;
                                tbAddr2.Text = PrefHotel.Address2;
                                tbAddr3.Text = PrefHotel.Address3;
                                if (PrefHotel.MetroID != null)
                                    hdnMetroID.Value = PrefHotel.MetroID.ToString();

                                PrefHotel.City = tbCity.Text;
                                tbState.Text = PrefHotel.StateProvince;

                                if (PrefHotel.CountryID != null)
                                {
                                    hdnCtryID.Value = PrefHotel.CountryID.ToString();
                                    tbCtry.Text = PrefHotel.CountryCode;
                                }
                                tbPost.Text = PrefHotel.PostalCode;

                                ddlHotelCompleteds.SelectedValue = "0";

                                if (!string.IsNullOrEmpty(PrefHotel.Status))
                                {
                                    string status = PrefHotel.Status.ToString();

                                    if (status == "Required")
                                    {
                                        ddlHotelCompleteds.SelectedValue = "1";
                                    }
                                    if (status == "Not Required")
                                    {
                                        ddlHotelCompleteds.SelectedValue = "2";
                                    }
                                    if (status == "In Progress")
                                    {
                                        ddlHotelCompleteds.SelectedValue = "3";
                                    }
                                    if (status == "Change")
                                    {
                                        ddlHotelCompleteds.SelectedValue = "4";
                                    }
                                    if (status == "Canceled")
                                    {
                                        ddlHotelCompleteds.SelectedValue = "5";
                                    }
                                    if (status == "Completed")
                                    {
                                        ddlHotelCompleteds.SelectedValue = "6";
                                    }
                                }

                                if (PrefHotel.NoofRooms.HasValue)
                                    tbNoforooms.Text = PrefHotel.NoofRooms.Value.ToString();

                                if (!string.IsNullOrEmpty(PrefHotel.ClubCard))
                                    tbClubCard.Text = PrefHotel.ClubCard;

                                if (PrefHotel.MaxCapAmount.HasValue)
                                    tbMaxcapamt.Text = PrefHotel.MaxCapAmount.Value.ToString();

                                if (PrefHotel.IsRateCap != null)
                                    chkRatecap.Checked = PrefHotel.IsRateCap.Value;
                                #endregion

                                chkNoPAX.Checked = true;

                                if (PrefHotel.PAXIDList != null && PrefHotel.PAXIDList.Count > 0)
                                {
                                    foreach (Int64 _PAxId in PrefHotel.PAXIDList.ToList())
                                    {
                                        foreach (GridDataItem item in dgLegEntity.Items)
                                        {
                                            CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                                            if (item.GetDataKeyValue("PaxID").ToString() == _PAxId.ToString())
                                            {
                                                chkSelect.Checked = true;
                                                chkNoPAX.Checked = false;
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        private void SaveHotel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (IsAuthorized(Permission.Preflight.AddPreflightCrewHotel) || IsAuthorized(Permission.Preflight.EditPreflightCrewHotel))
                {
                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        Int64 LegNum = 0;
                        if (!string.IsNullOrEmpty(hdnLegNumHotel.Value))
                            LegNum = Convert.ToInt64(hdnLegNumHotel.Value);

                        Int64 HotelIdentifier = 0;
                        if (!string.IsNullOrEmpty(hdnHotelIdentifier.Value))
                            HotelIdentifier = Convert.ToInt64(hdnHotelIdentifier.Value);


                        PreflightLeg LegToSave = new PreflightLeg();

                        LegToSave = Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNum).SingleOrDefault();

                        if (LegToSave != null)
                        {
                            if (LegToSave.PreflightHotelLists == null)
                                LegToSave.PreflightHotelLists = new List<PreflightHotelList>();

                            PreflightHotelList PrefHotel = new PreflightHotelList();
                            bool hotelfound = false;
                            if (HotelIdentifier > 0) // Search for hotel identifier 
                            {
                                PrefHotel = LegToSave.PreflightHotelLists.Where(x => x.HotelIdentifier == HotelIdentifier && x.IsDeleted == false).SingleOrDefault();
                            }

                            if (HotelIdentifier == 0) // PrefHotel == null) // if hotel not found then it is a new hotel
                            {
                                #region "New Hotel"

                                PrefHotel = new PreflightHotelList();
                                PrefHotel.State = TripEntityState.Added;
                                PrefHotel.IsDeleted = false;
                                Int64 HotelnumtoAssign = 0;

                                if (LegToSave.PreflightHotelLists.Count > 0)
                                {
                                    HotelnumtoAssign = LegToSave.PreflightHotelLists.DefaultIfEmpty().Max(x => x.HotelIdentifier);
                                }
                                HotelnumtoAssign++;
                                PrefHotel.HotelIdentifier = HotelnumtoAssign;
                                hdnHotelIdentifier.Value = HotelnumtoAssign.ToString();

                                #endregion
                            }
                            else
                                hotelfound = true;

                            if (PrefHotel.PreflightHotelListID != 0) // modify an existing hotel 
                            {
                                PrefHotel.IsDeleted = false;
                                PrefHotel.State = TripEntityState.Modified;
                            }
                            else if (PrefHotel.PreflightHotelListID == 0) // modify a newly added hotel 
                                PrefHotel.State = TripEntityState.Added;

                            #region Update UI Details
                            PrefHotel.HotelCode = tbHotelCode.Text;
                            PrefHotel.PreflightHotelName = tbHotelName.Text;
                            PrefHotel.LegID = LegToSave.LegID;
                            if (!string.IsNullOrEmpty(hdnHotelID.Value))
                                PrefHotel.HotelID = Convert.ToInt64(hdnHotelID.Value);

                            if (rbArriveDepart.Items[0].Selected)
                                PrefHotel.AirportID = LegToSave.ArriveICAOID;
                            else
                                PrefHotel.AirportID = LegToSave.DepartICAOID;

                            //PrefHotel.RoomType  //Commented for time being
                            //if (!string.IsNullOrEmpty(tbHotelRate.Text))
                            //    PrefHotel.RoomRent = Convert.ToDecimal(tbHotelRate.Text);

                            PrefHotel.RoomConfirm = tbConfirmation.Text;

                            if (ddlRoomDesc.SelectedItem.Text == "Select")
                                PrefHotel.RoomDescription = string.Empty;
                            else
                                PrefHotel.RoomDescription = ddlRoomDesc.SelectedItem.Text;

                            PrefHotel.Street = string.Empty;
                            PrefHotel.CityName = tbCity.Text;
                            PrefHotel.StateName = tbState.Text;
                            PrefHotel.PostalZipCD = tbPost.Text;
                            PrefHotel.PhoneNum1 = tbHotelPhone.Text;
                            PrefHotel.PhoneNum2 = string.Empty;
                            PrefHotel.PhoneNum3 = string.Empty;
                            PrefHotel.PhoneNum4 = string.Empty;
                            PrefHotel.FaxNUM = tbHotelFax.Text;
                            PrefHotel.HotelArrangedBy = radlstArrangements.SelectedValue.ToString();

                            if (!string.IsNullOrEmpty(tbHotelRate.Text))
                                PrefHotel.Rate = Convert.ToDecimal(tbHotelRate.Text);
                            if (!string.IsNullOrEmpty(tbDateIn.Text))
                            {
                                PrefHotel.DateIn = DateTime.ParseExact(tbDateIn.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            }
                            if (!string.IsNullOrEmpty(tbDateOut.Text))
                            {
                                PrefHotel.DateOut = DateTime.ParseExact(tbDateOut.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            }

                            PrefHotel.IsSmoking = chkSmoking.Checked;
                            if (ddltypeofRoom.SelectedValue != "")
                                PrefHotel.RoomTypeID = Convert.ToInt64(ddltypeofRoom.SelectedValue);
                            if (!string.IsNullOrEmpty(tbNoofBeds.Text))
                                PrefHotel.NoofBeds = Convert.ToDecimal(tbNoofBeds.Text);
                            if (!string.IsNullOrEmpty(tbNoforooms.Text))
                                PrefHotel.NoofRooms = Convert.ToDecimal(tbNoforooms.Text);

                            if (!string.IsNullOrEmpty(tbClubCard.Text))
                                PrefHotel.ClubCard = tbClubCard.Text;

                            if (!string.IsNullOrEmpty(tbMaxcapamt.Text))
                                PrefHotel.MaxCapAmount = Convert.ToDecimal(tbMaxcapamt.Text);

                            PrefHotel.IsRateCap = chkRatecap.Checked;
                            PrefHotel.IsAllCrewOrPax = false;

                            //PrefHotel.IsCompleted	

                            PrefHotel.IsEarlyCheckIn = chkEarlyCheckin.Checked;

                            PrefHotel.IsRequestOnly = chkRequestOnly.Checked;
                            PrefHotel.IsBookNight = chkBookNightPrior.Checked;

                            //if (radRequestOnly.Items[0].Selected)
                            //    PrefHotel.EarlyCheckInOption = radRequestOnly.Items[0].Value;
                            //else
                            //    PrefHotel.EarlyCheckInOption = radRequestOnly.Items[1].Value;

                            PrefHotel.SpecialInstructions = chkUniversalPreferences.Checked.ToString();

                            PrefHotel.ConfirmationStatus = tbConfirmation.Text;
                            PrefHotel.Comments = tbComments.Text;
                            PrefHotel.Address1 = tbAddr1.Text;
                            PrefHotel.Address2 = tbAddr2.Text;
                            PrefHotel.Address3 = tbAddr3.Text;
                            if (!string.IsNullOrEmpty(hdnMetroID.Value))
                                PrefHotel.MetroID = Convert.ToInt64(hdnMetroID.Value);

                            PrefHotel.City = tbCity.Text;
                            PrefHotel.StateProvince = tbState.Text;

                            if (!string.IsNullOrEmpty(hdnCtryID.Value))
                            {
                                PrefHotel.CountryID = Convert.ToInt64(hdnCtryID.Value);
                                PrefHotel.CountryCode = tbCtry.Text;
                            }
                            PrefHotel.PostalCode = tbPost.Text;
                            if (ddlHotelCompleteds.SelectedValue != null && ddlHotelCompleteds.SelectedValue != "")
                            {
                                if (ddlHotelCompleteds.SelectedValue == "1")
                                    PrefHotel.Status = "Required";
                                if (ddlHotelCompleteds.SelectedValue == "2")
                                    PrefHotel.Status = "Not Required";
                                if (ddlHotelCompleteds.SelectedValue == "3")
                                    PrefHotel.Status = "In Progress";
                                if (ddlHotelCompleteds.SelectedValue == "4")
                                    PrefHotel.Status = "Change";
                                if (ddlHotelCompleteds.SelectedValue == "5")
                                    PrefHotel.Status = "Canceled";
                                if (ddlHotelCompleteds.SelectedValue == "6")
                                    PrefHotel.Status = "Completed";
                            }

                            PrefHotel.isArrivalHotel = rbArriveDepart.Items[0].Selected;
                            PrefHotel.isDepartureHotel = rbArriveDepart.Items[1].Selected;
                            PrefHotel.crewPassengerType = "P";

                            PrefHotel.PAXIDList = null;
                            PrefHotel.PAXIDList = new List<long?>();

                            foreach (GridDataItem item in dgLegEntity.Items)
                            {
                                CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                                if (chkSelect != null && chkSelect.Checked)
                                {
                                    PrefHotel.PAXIDList.Add(Convert.ToInt64(item.GetDataKeyValue("PaxID").ToString()));
                                }
                            }

                            #endregion

                            if (!hotelfound)
                                LegToSave.PreflightHotelLists.Add(PrefHotel);
                        }

                        Session["CurrentPreFlightTrip"] = Trip;

                        BindLegHotel(true);
                        LoadHotel();
                        EnableHotelForm(false);
                        GridEnable(true, true, true);
                    }
                }
            }
        }
        private void DeleteHotel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (IsAuthorized(Permission.Preflight.DeletePreflightCrewHotel))
                {
                    if (Session["CurrentPreFlightTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                        Int64 LegNum = 0;
                        if (!string.IsNullOrEmpty(hdnLegNumHotel.Value))
                            LegNum = Convert.ToInt64(hdnLegNumHotel.Value);

                        Int64 HotelIdentifier = 0;
                        //if (!string.IsNullOrEmpty(hdnHotelIdentifier.Value))
                        //    HotelIdentifier = Convert.ToInt64(hdnHotelIdentifier.Value);

                        if (Session["HotelToDelete"] != null)
                        {
                            string[] hotelIds = Session["HotelToDelete"].ToString().Split(',');
                            foreach (string id in hotelIds)
                            {
                                if (!string.IsNullOrEmpty(id))
                                {
                                    HotelIdentifier = Convert.ToInt64(id);
                                    PreflightLeg LegToDeleteHotel = new PreflightLeg();

                                    LegToDeleteHotel = Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNum).SingleOrDefault();

                                    if (LegToDeleteHotel != null)
                                    {
                                        if (LegToDeleteHotel.PreflightHotelLists == null)
                                            LegToDeleteHotel.PreflightHotelLists = new List<PreflightHotelList>();

                                        PreflightHotelList PrefHotel = new PreflightHotelList();

                                        if (HotelIdentifier > 0) // Search for hotel identifier 
                                        {
                                            PrefHotel = LegToDeleteHotel.PreflightHotelLists.Where(x => x.HotelIdentifier == HotelIdentifier && x.IsDeleted == false).SingleOrDefault();
                                        }
                                        if (PrefHotel != null)
                                        {
                                            if (PrefHotel.PreflightHotelListID != 0) // modify an existing hotel 
                                            {
                                                PrefHotel.IsDeleted = true;
                                                PrefHotel.State = TripEntityState.Deleted;
                                            }
                                            else
                                            {
                                                LegToDeleteHotel.PreflightHotelLists.Remove(PrefHotel);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Session["CurrentPreFlightTrip"] = Trip;
                    }
                }
            }
        }
        protected void ClearHotel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                rbArriveDepart.SelectedValue = "0";
                hdnrbArriveDepartPrev.Value = "0";

                tbDateIn.Text = hdnArrivalDate.Value;
                tbDateOut.Text = hdnNextDeptDate.Value;

                tbHotelCode.Text = string.Empty;
                tbHotelRate.Text = string.Empty;
                tbHotelRate.Text = "0.00";
                tbHotelName.Text = string.Empty;
                tbHotelPhone.Text = string.Empty;
                tbHotelFax.Text = string.Empty;
                tbNoforooms.Text = string.Empty;
                chkSmoking.Checked = false;
                //ddltypeofRoom.SelectedValue = "1";
                ddlRoomDesc.SelectedValue = "0";
                tbNoofBeds.Text = string.Empty;
                tbClubCard.Text = string.Empty;
                chkRatecap.Checked = false;
                tbMaxcapamt.Text = string.Empty;
                //chkApplytoallcrew.Checked = false;

                chkEarlyCheckin.Checked = false;
                chkRequestOnly.Checked = false;
                chkBookNightPrior.Checked = false;
                //tbSpecialInstructions.Text = string.Empty;
                tbConfirmation.Text = string.Empty;
                tbComments.Text = string.Empty;
                //radlstDepartArriveList.Items[0].Selected = true;  

                lbHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                hdnHotelAirID.Value = string.Empty;
                hdnHotelID.Value = string.Empty;
                //Defaults to none.
                radlstArrangements.SelectedValue = "None";
                lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("");
                tbAddr1.Text = string.Empty;
                tbAddr2.Text = string.Empty;
                tbAddr3.Text = string.Empty;
                tbMetro.Text = string.Empty;
                tbState.Text = string.Empty;
                tbCity.Text = string.Empty;
                tbPost.Text = string.Empty;
                tbCtry.Text = string.Empty;
                hdnCtryID.Value = string.Empty;
                ddlHotelCompleteds.SelectedValue = "0";

                chkNoPAX.Checked = false;
                if (dgLegEntity.Items.Count > 0 && dgLegEntity.SelectedItems.Count > 0)
                    chkNoPAX.Checked = true;

                GridHeaderItem hItem = (GridHeaderItem)(dgLegEntity.MasterTableView.GetItems(GridItemType.Header))[0];
                CheckBox chkSelectHeader = (CheckBox)hItem.FindControl("chkSelectHeader");
                chkSelectHeader.Checked = false;

                foreach (GridDataItem item in dgLegEntity.Items)
                {
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                    chkSelect.Checked = false;
                }
            }
        }
        private void RetrieveRoomType()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetRoomList();
                    if (objRetVal.ReturnFlag == true)
                    {
                        ddltypeofRoom.DataTextField = "RoomDescription";
                        ddltypeofRoom.DataValueField = "RoomTypeID";
                        ddltypeofRoom.DataSource = objRetVal.EntityList;
                        ddltypeofRoom.DataBind();
                    }
                }
            }
        }
        private void RetrieveHotel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbHotelName.Text = string.Empty;
                tbHotelRate.Text = string.Empty;
                tbHotelPhone.Text = string.Empty;
                tbHotelFax.Text = string.Empty;
                tbAddr1.Text = string.Empty;
                tbAddr2.Text = string.Empty;
                tbAddr3.Text = string.Empty;
                tbCity.Text = string.Empty;
                tbState.Text = string.Empty;
                tbMetro.Text = string.Empty;
                tbCtry.Text = string.Empty;
                tbPost.Text = string.Empty;

                RadAjaxManager.GetCurrent(Page).FocusControl(btnCode);

                if (!string.IsNullOrEmpty(tbHotelCode.Text) && !string.IsNullOrEmpty(hdnHotelID.Value))
                {
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        if (!string.IsNullOrEmpty(hdnHotelID.Value))
                        {
                            var objRetVal = objDstsvc.GetHotelByHotelID(Convert.ToInt64(hdnHotelID.Value)).EntityList;

                            if (objRetVal != null && objRetVal.Count > 0)
                            {
                                hdnHotelchanged.Value = "true";
                                // lbHotelCode.Text = objRetVal[0].Name.ToUpper();
                                lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("");
                                tbHotelCode.Text = objRetVal[0].HotelCD.ToUpper();
                                tbHotelName.Text = objRetVal[0].Name.ToUpper();
                                if (objRetVal[0].NegociatedRate != null)
                                    tbHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                    tbHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                    tbHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].Addr1))
                                    tbAddr1.Text = objRetVal[0].Addr1.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].Addr2))
                                    tbAddr2.Text = objRetVal[0].Addr2.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].Addr3))
                                    tbAddr3.Text = objRetVal[0].Addr3.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].CityName))
                                    tbCity.Text = objRetVal[0].CityName.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].StateName))
                                    tbState.Text = objRetVal[0].StateName.ToString();
                                if (!string.IsNullOrEmpty(objRetVal[0].MetroCD))
                                {
                                    tbMetro.Text = objRetVal[0].MetroCD.ToString();
                                    hdnMetroID.Value = objRetVal[0].MetroID.ToString();
                                }
                                if (!string.IsNullOrEmpty(objRetVal[0].CountryCD))
                                {
                                    tbCtry.Text = objRetVal[0].CountryCD.ToString();
                                    hdnCtryID.Value = objRetVal[0].CountryID.ToString();
                                }
                                if (!string.IsNullOrEmpty(objRetVal[0].PostalZipCD))
                                    tbPost.Text = objRetVal[0].PostalZipCD.ToString();
                            }
                            else
                            {
                                lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                tbHotelName.Text = string.Empty;
                                tbHotelRate.Text = string.Empty;
                                tbHotelPhone.Text = string.Empty;
                                tbHotelFax.Text = string.Empty;
                                tbAddr1.Text = string.Empty;
                                tbAddr2.Text = string.Empty;
                                tbAddr3.Text = string.Empty;
                                tbCity.Text = string.Empty;
                                tbState.Text = string.Empty;
                                tbMetro.Text = string.Empty;
                                tbCtry.Text = string.Empty;
                                hdnMetroID.Value = string.Empty;
                                hdnCtryID.Value = string.Empty;
                                tbPost.Text = string.Empty;
                                tbHotelCode.Focus();

                            }
                        }
                        else
                        {
                            lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                            tbHotelName.Text = string.Empty;
                            tbHotelRate.Text = string.Empty;
                            tbHotelPhone.Text = string.Empty;
                            tbHotelFax.Text = string.Empty;
                            tbAddr1.Text = string.Empty;
                            tbAddr2.Text = string.Empty;
                            tbAddr3.Text = string.Empty;
                            tbCity.Text = string.Empty;
                            tbState.Text = string.Empty;
                            tbMetro.Text = string.Empty;
                            tbCtry.Text = string.Empty;
                            hdnMetroID.Value = string.Empty;
                            hdnCtryID.Value = string.Empty;
                            tbPost.Text = string.Empty;
                            tbHotelCode.Focus();
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(tbHotelCode.Text) && string.IsNullOrEmpty(hdnHotelID.Value))
                {
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        Int64 AirportID = 0;

                        if (rbArriveDepart.Items[0].Selected)
                            AirportID = Convert.ToInt64(hdnArrivalICao.Value);
                        else
                            AirportID = Convert.ToInt64(hdnDepartIcao.Value);

                        var objRetVal = objDstsvc.GetHotelByHotelCode(AirportID, tbHotelCode.Text.Trim()).EntityList;
                        if (objRetVal != null && objRetVal.Count > 0)
                        {
                            hdnHotelchanged.Value = "true";
                            //lbHotelCode.Text = objRetVal[0].Name.ToUpper();
                            lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            tbHotelName.Text = objRetVal[0].Name.ToUpper();
                            hdnHotelID.Value = objRetVal[0].HotelID.ToString();
                            hdnHotelAirID.Value = objRetVal[0].AirportID.ToString();
                            if (objRetVal[0].NegociatedRate != null)
                                tbHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                tbHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                tbHotelFax.Text = objRetVal[0].FaxNum.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].Addr1))
                                tbAddr1.Text = objRetVal[0].Addr1.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].Addr2))
                                tbAddr2.Text = objRetVal[0].Addr2.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].Addr3))
                                tbAddr3.Text = objRetVal[0].Addr3.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].CityName))
                                tbCity.Text = objRetVal[0].CityName.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].StateName))
                                tbState.Text = objRetVal[0].StateName.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].MetroCD))
                            {
                                tbMetro.Text = objRetVal[0].MetroCD.ToString();
                                hdnMetroID.Value = objRetVal[0].MetroID.ToString();
                            }
                            if (!string.IsNullOrEmpty(objRetVal[0].CountryCD))
                            {
                                tbCtry.Text = objRetVal[0].CountryCD.ToString();
                                hdnCtryID.Value = objRetVal[0].CountryID.ToString();
                            }
                            if (!string.IsNullOrEmpty(objRetVal[0].PostalZipCD))
                                tbPost.Text = objRetVal[0].PostalZipCD.ToString();
                        }
                        else
                        {
                            lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                            //tbHotelCode.Text = string.Empty;
                            tbHotelName.Text = string.Empty;
                            tbHotelRate.Text = string.Empty;
                            tbHotelPhone.Text = string.Empty;
                            tbHotelFax.Text = string.Empty;
                            tbAddr1.Text = string.Empty;
                            tbAddr2.Text = string.Empty;
                            tbAddr3.Text = string.Empty;
                            tbCity.Text = string.Empty;
                            tbState.Text = string.Empty;
                            tbMetro.Text = string.Empty;
                            tbCtry.Text = string.Empty;
                            hdnMetroID.Value = string.Empty;
                            hdnCtryID.Value = string.Empty;
                            tbPost.Text = string.Empty;
                            tbHotelCode.Focus();
                        }
                    }
                }
                else
                {
                    hdnHotelID.Value = string.Empty;
                    lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                    tbHotelName.Text = string.Empty;
                    tbHotelRate.Text = string.Empty;
                    tbHotelPhone.Text = string.Empty;
                    tbHotelFax.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbMetro.Text = string.Empty;
                    tbCtry.Text = string.Empty;
                    hdnMetroID.Value = string.Empty;
                    hdnCtryID.Value = string.Empty;
                    tbPost.Text = string.Empty;
                    tbHotelCode.Focus();
                }

            }
        }
        protected void tbCtryCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvCtry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnCtryID.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbCtry.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetVal = ObjService.GetCountryMasterList();
                                if (ObjRetVal.ReturnFlag == true)
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Country> CtryList = new List<FlightPak.Web.FlightPakMasterService.Country>();
                                    CtryList = ObjRetVal.EntityList.Where(x => x.CountryCD.ToLower().Trim() == tbCtry.Text.ToLower().Trim() && x.IsDeleted == false).ToList();
                                    if (CtryList != null && CtryList.Count > 0)
                                    {
                                        hdnCtryID.Value = CtryList[0].CountryID.ToString();
                                        tbCtry.Text = CtryList[0].CountryCD;
                                    }
                                    else
                                    {
                                        lbcvCtry.Text = System.Web.HttpUtility.HtmlEncode("Invalid Country Code");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCtry);
                                    }
                                }
                                else
                                {
                                    lbcvCtry.Text = System.Web.HttpUtility.HtmlEncode("Invalid Country Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCtry);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }

        
        protected void tbMetroCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvMetro.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnMetroID.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbMetro.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetVal = ObjService.GetMetroCityList();
                                if (ObjRetVal.ReturnFlag == true)
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Metro> MetroList = new List<FlightPak.Web.FlightPakMasterService.Metro>();
                                    MetroList = ObjRetVal.EntityList.Where(x => x.MetroCD.ToLower().Trim() == tbMetro.Text.ToLower().Trim() && x.IsDeleted == false).ToList();
                                    if (MetroList != null && MetroList.Count > 0)
                                    {
                                        hdnMetroID.Value = MetroList[0].MetroID.ToString();
                                        tbMetro.Text = MetroList[0].MetroCD;
                                    }
                                    else
                                    {
                                        lbcvMetro.Text = System.Web.HttpUtility.HtmlEncode("Invalid Metro Code");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMetro);
                                    }
                                }
                                else
                                {
                                    lbcvMetro.Text = System.Web.HttpUtility.HtmlEncode("Invalid Metro Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbMetro);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void tbHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCode);
                        //lbHotelCode.Text = string.Empty;
                        lbcvHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnHotelID.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbHotelCode.Text))
                        {
                            RetrieveHotel();
                        }
                        else
                        {
                            lbHotelCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            tbHotelName.Text = string.Empty;
                            tbHotelRate.Text = string.Empty;
                            tbHotelPhone.Text = string.Empty;
                            tbHotelFax.Text = string.Empty;
                            tbAddr1.Text = string.Empty;
                            tbAddr2.Text = string.Empty;
                            tbAddr3.Text = string.Empty;
                            tbCity.Text = string.Empty;
                            tbState.Text = string.Empty;
                            tbMetro.Text = string.Empty;
                            tbCtry.Text = string.Empty;
                            tbPost.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        #endregion

        #region Crew Logistics - Transportation Methods
        protected void radlstArrangements_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // RadAjaxManager.GetCurrent(Page).FocusControl(chkApplytoallcrew);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void rtsLogistics_TabClick(object sender, RadTabStripEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            if (hdnPaxLogisticsSelectedTab.Value == "1") //If previously selected tab is transport then save transport and load first leg of the hotel
                            {
                                hdnPaxLogisticsSelectedTab.Value = e.Tab.Index.ToString();

                                #region If previously selected tab is transport then save transport and load first leg of the hotel

                                if (IsAuthorized(Permission.Preflight.AddPreflightCrewTransport) || IsAuthorized(Permission.Preflight.EditPreflightCrewTransport))
                                {
                                    PreflightLeg LegtoSaveTransport = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();
                                    if (LegtoSaveTransport != null)
                                    {
                                        SaveTransport(LegtoSaveTransport);
                                    }
                                }

                                Session["CurrentPreFlightTrip"] = Trip;

                                // Clear the hotel details and Load hotel based on the selected leg, here it should be first leg
                                ClearHotel();
                                hdnLegNumHotel.Value = "1";

                                RadMultiPage1.SelectedIndex = 0;
                                //rdPgeViewHotel.Visible = true;
                                //radPgeViewTransport.Visible = false;
                                rtsLegHotel.Tabs[0].Selected = true;
                                rbArriveDepart.SelectedValue = "0";
                                hdnrbArriveDepartPrev.Value = "0";
                                BindLegHotel(true);
                                BindLegPax(true);
                                UpdateHiddenAirportIDsBasedOnLegnum(1);
                                if (dgLegHotel.Items.Count > 0)
                                {
                                    dgLegHotel.Items[0].Selected = true;
                                    GridDataItem HotelItem = (GridDataItem)dgLegHotel.Items[0];
                                    hdnHotelIdentifier.Value = HotelItem.GetDataKeyValue("HotelIdentifier").ToString();
                                    LoadHotel();
                                    GridEnable(true, true, true);
                                }
                                rtsLegHotel.Focus();
                                #endregion
                            }
                            else if (hdnPaxLogisticsSelectedTab.Value == "0") // if previously selected tab is hotel then save hotel and load first leg of the transport
                            {
                                #region if previously selected tab is hotel then save hotel and load first leg of the transport

                                if (btnSaveHotel.Enabled)
                                {
                                    rtsLogistics.Tabs[Convert.ToInt16(hdnPaxLogisticsSelectedTab.Value)].Selected = true;
                                    hdnPaxLogisticsSelectedTab.Value = rtsLogistics.SelectedIndex.ToString();
                                    RadTab hotellegtab = rtsLegHotel.FindTabByValue(hdnLegNumHotel.Value);
                                    if (hotellegtab != null)
                                        hotellegtab.Selected = true;
                                    rtsLegHotel.Focus();
                                    RadMultiPage1.SelectedIndex = 0;
                                    //rdPgeViewHotel.Visible = true;
                                    //radPgeViewTransport.Visible = false;
                                    RadWindowManager1.RadAlert("Please save or cancel hotel information", 330, 100, ModuleNameConstants.Preflight.PreflightCrew, "alertPaxCallBackFn");
                                }
                                else
                                {
                                    hdnPaxLogisticsSelectedTab.Value = e.Tab.Index.ToString();
                                    rtsLegTransport.Focus();
                                    //Mark the first leg as the selected leg and load transport for the firstleg
                                    rtsLegTransport.Tabs[0].Selected = true;
                                    hdnLegNumTransport.Value = "1";
                                    RadMultiPage1.SelectedIndex = 1;

                                    //rdPgeViewHotel.Visible = false;
                                    //radPgeViewTransport.Visible = true;
                                    ClearTransport();
                                    PreflightLeg LegtoLoad = Trip.PreflightLegs.Where(x => x.LegNUM == 1).SingleOrDefault();
                                    if (LegtoLoad != null)
                                    {
                                        LoadTransport(LegtoLoad);
                                    }
                                    UpdateHiddenAirportIDsBasedOnLegnum(1);
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void rtsLegTransport_TabClick(object sender, RadTabStripEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            #region Save transport for current leg and load the transport for the selected leg tab
                            if (IsAuthorized(Permission.Preflight.AddPreflightCrewTransport) || IsAuthorized(Permission.Preflight.EditPreflightCrewTransport))
                            {
                                PreflightLeg LegtoSaveTransport = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();
                                if (LegtoSaveTransport != null)
                                {
                                    SaveTransport(LegtoSaveTransport);
                                }
                            }

                            Session["CurrentPreFlightTrip"] = Trip;

                            ClearTransport();

                            hdnLegNumTransport.Value = e.Tab.Value.ToString();
                            UpdateHiddenAirportIDsBasedOnLegnum(Convert.ToInt64(e.Tab.Value));

                            PreflightLeg LegtoLoad = Trip.PreflightLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNumTransport.Value)).SingleOrDefault();

                            if (LegtoLoad != null)
                            {
                                LoadTransport(LegtoLoad);
                            }

                            #endregion

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        private void LoadTransport(PreflightLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (IsAuthorized(Permission.Preflight.ViewPreflightCrewTransport))
                {
                    if (Leg != null)
                    {
                        if (Leg.PreflightTransportLists != null && Leg.PreflightTransportLists.Count > 0)
                        {
                            PreflightTransportList LegDepTansport = Leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsDepartureTransport == true).SingleOrDefault();
                            PreflightTransportList LegArrTansport = Leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsArrivalTransport == true).SingleOrDefault();

                            lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                            if (LegDepTansport != null)
                            {
                                if (LegDepTansport.AirportID == Leg.DepartICAOID && LegDepTansport.AirportID != null)
                                {
                                    #region Departure Transport
                                    hdnTransportID.Value = LegDepTansport.TransportID.ToString();
                                    hdnTransAirID.Value = LegDepTansport.AirportID.ToString();
                                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(LegDepTansport.TransportID)).EntityList;
                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            tbTransCode.Text = objRetVal[0].TransportCD;
                                        }
                                        if (!string.IsNullOrEmpty(LegDepTansport.PreflightTransportName))
                                            tbTransName.Text = LegDepTansport.PreflightTransportName.ToString();
                                        if (!string.IsNullOrEmpty(LegDepTansport.PhoneNum1))
                                            tbTransPhone.Text = LegDepTansport.PhoneNum1.ToString();
                                        if (!string.IsNullOrEmpty(LegDepTansport.FaxNUM))
                                            tbTransFax.Text = LegDepTansport.FaxNUM.ToString();
                                        if (LegDepTansport.ConfirmationStatus != null)
                                            tbTransConfirmation.Text = LegDepTansport.ConfirmationStatus.ToString();
                                        if (LegDepTansport.Comments != null)
                                            tbTransComments.Text = LegDepTansport.Comments.ToString();
                                        if (LegDepTansport.IsCompleted != null)
                                            chkTransCompleted.Checked = Convert.ToBoolean(LegDepTansport.IsCompleted);
                                        if (!string.IsNullOrEmpty(LegDepTansport.PhoneNum4))
                                            tbTransRate.Text = LegDepTansport.PhoneNum4;
                                        if (LegDepTansport.Status != null)
                                        {
                                            string status = LegDepTansport.Status;
                                            if (status == "Required")
                                            {
                                                ddlDepartTransportCompleteds.SelectedValue = "1";
                                            }
                                            if (status == "Not Required")
                                            {
                                                ddlDepartTransportCompleteds.SelectedValue = "2";
                                            }
                                            if (status == "In Progress")
                                            {
                                                ddlDepartTransportCompleteds.SelectedValue = "3";
                                            }
                                            if (status == "Change")
                                            {
                                                ddlDepartTransportCompleteds.SelectedValue = "4";
                                            }
                                            if (status == "Canceled")
                                            {
                                                ddlDepartTransportCompleteds.SelectedValue = "5";
                                            }
                                            if (status == "Completed")
                                            {
                                                ddlDepartTransportCompleteds.SelectedValue = "6";
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }

                            if (LegArrTansport != null)
                            {
                                if (LegArrTansport.AirportID == Leg.ArriveICAOID && LegArrTansport.AirportID != null)
                                {
                                    #region Arrival Transport
                                    hdnArriveTransportID.Value = LegArrTansport.TransportID.ToString();
                                    hdnArriveTransAirID.Value = LegArrTansport.AirportID.ToString();
                                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(LegArrTansport.TransportID)).EntityList;
                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            tbArriveTransCode.Text = objRetVal[0].TransportCD;
                                        }
                                        if (!string.IsNullOrEmpty(LegArrTansport.PreflightTransportName))
                                            tbArriveTransName.Text = LegArrTansport.PreflightTransportName.ToString();
                                        if (!string.IsNullOrEmpty(LegArrTansport.PhoneNum1))
                                            tbArrivePhone.Text = LegArrTansport.PhoneNum1.ToString();
                                        if (!string.IsNullOrEmpty(LegArrTansport.FaxNUM))
                                            tbArriveFax.Text = LegArrTansport.FaxNUM.ToString();
                                        if (LegArrTansport.ConfirmationStatus != null)
                                            tbArriveConfirmation.Text = LegArrTansport.ConfirmationStatus.ToString();
                                        if (LegArrTansport.Comments != null)
                                            tbArriveComments.Text = LegArrTansport.Comments.ToString();
                                        if (LegArrTansport.IsCompleted != null)
                                            chkArriveCompleted.Checked = Convert.ToBoolean(LegArrTansport.IsCompleted);
                                        if (!string.IsNullOrEmpty(LegArrTansport.PhoneNum4))
                                            tbArriveRate.Text = LegArrTansport.PhoneNum4;

                                        if (LegArrTansport.Status != null)
                                        {
                                            string status = LegArrTansport.Status;
                                            if (status == "Required")
                                            {
                                                ddlArriveTransportCompleteds.SelectedValue = "1";
                                            }
                                            if (status == "Not Required")
                                            {
                                                ddlArriveTransportCompleteds.SelectedValue = "2";
                                            }
                                            if (status == "In Progress")
                                            {
                                                ddlArriveTransportCompleteds.SelectedValue = "3";
                                            }
                                            if (status == "Change")
                                            {
                                                ddlArriveTransportCompleteds.SelectedValue = "4";
                                            }
                                            if (status == "Canceled")
                                            {
                                                ddlArriveTransportCompleteds.SelectedValue = "5";
                                            }
                                            if (status == "Completed")
                                            {
                                                ddlArriveTransportCompleteds.SelectedValue = "6";
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }

                        }
                    }
                }
            }
        }
        protected void ClearTransport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbTransCode.Text = string.Empty;
                tbTransRate.Text = "0.00";
                chkTransCompleted.Checked = false;
                tbTransName.Text = string.Empty;
                tbTransPhone.Text = string.Empty;
                tbTransFax.Text = string.Empty;
                tbTransConfirmation.Text = string.Empty;
                tbTransComments.Text = string.Empty;
                lbTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbArriveTransCode.Text = string.Empty;
                tbArriveRate.Text = "0.00";
                chkArriveCompleted.Checked = false;
                tbArriveTransName.Text = string.Empty;
                tbArrivePhone.Text = string.Empty;
                tbArriveFax.Text = string.Empty;
                tbArriveConfirmation.Text = string.Empty;
                tbArriveComments.Text = string.Empty;
                hdnArriveTransAirID.Value = string.Empty;
                hdnArriveTransportID.Value = string.Empty;
                hdnTransAirID.Value = string.Empty;
                hdnTransportID.Value = string.Empty;
                lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                ddlDepartTransportCompleteds.SelectedValue = "0";
                ddlArriveTransportCompleteds.SelectedValue = "0";
            }
        }
        private void RetrieveDepartTransport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbTransCode.Text) && !string.IsNullOrEmpty(hdnTransportID.Value))
                {
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTransport);
                        if (!string.IsNullOrEmpty(hdnTransportID.Value))
                        {
                            var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(hdnTransportID.Value)).EntityList;
                            if (objRetVal != null && objRetVal.Count > 0)
                            {
                                tbTransCode.Text = objRetVal[0].TransportCD.ToUpper();
                                tbTransName.Text = objRetVal[0].TransportationVendor.ToUpper();
                                lbTransCode.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].TransportationVendor.ToUpper());
                                //if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                //    tbTransPhone.Text = objRetVal[0].PhoneNum.ToString();
                                //if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                //    tbTransFax.Text = objRetVal[0].FaxNum.ToString();
                                //if (objRetVal[0].NegotiatedRate.ToString() != null)
                                //    tbTransRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(hdnTransportID.Value))
                                    lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode("Depart Transport Code Does Not Exist");
                                tbTransName.Text = string.Empty;
                                tbTransPhone.Text = string.Empty;
                                tbTransFax.Text = string.Empty;
                                tbTransRate.Text = string.Empty;
                                tbTransCode.Focus();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(hdnTransportID.Value))
                                lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode("Depart Transport Code Does Not Exist");
                            tbTransName.Text = string.Empty;
                            tbTransPhone.Text = string.Empty;
                            tbTransFax.Text = string.Empty;
                            tbTransRate.Text = string.Empty;
                            tbTransCode.Focus();
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(tbTransCode.Text) && string.IsNullOrEmpty(hdnTransportID.Value))
                {
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objDstsvc.GetTransportByTransportCode(Convert.ToInt64(hdnDepartIcao.Value), tbTransCode.Text.Trim()).EntityList;
                        if (objRetVal != null && objRetVal.Count > 0)
                        {
                            tbTransCode.Text = objRetVal[0].TransportCD.ToUpper();
                            tbTransName.Text = objRetVal[0].TransportationVendor.ToUpper();
                            hdnTransportID.Value = objRetVal[0].TransportID.ToString();
                            hdnTransAirID.Value = objRetVal[0].AirportID.ToString();
                            lbTransCode.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].TransportationVendor.ToUpper());
                            if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                tbTransPhone.Text = objRetVal[0].PhoneNum.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                tbTransFax.Text = objRetVal[0].FaxNum.ToString();
                            if (objRetVal[0].NegotiatedRate.ToString() != null)
                                tbTransRate.Text = objRetVal[0].NegotiatedRate.ToString();
                            lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode("");
                        }
                        else
                        {
                            lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode("Depart Transport Code Does Not Exist");
                            tbTransName.Text = string.Empty;
                            tbTransPhone.Text = string.Empty;
                            tbTransFax.Text = string.Empty;
                            tbTransRate.Text = string.Empty;
                            tbTransCode.Focus();
                        }
                    }
                }
                else
                {
                    lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode("Depart Transport Code Does Not Exist");
                    tbTransName.Text = string.Empty;
                    tbTransPhone.Text = string.Empty;
                    tbTransFax.Text = string.Empty;
                    tbTransRate.Text = string.Empty;
                    tbTransCode.Focus();
                }
            }
        }
        private void RetrieveArriveTransport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbArriveTransCode.Text) && !string.IsNullOrEmpty(hdnArriveTransportID.Value))
                {
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveTrans);
                        if (!string.IsNullOrEmpty(hdnArriveTransportID.Value))
                        {
                            var objRetVal = objDstsvc.GetTransportByTransportID(Convert.ToInt64(hdnArriveTransportID.Value)).EntityList;

                            if (objRetVal != null && objRetVal.Count > 0)
                            {
                                tbArriveTransCode.Text = objRetVal[0].TransportCD.ToUpper();
                                tbArriveTransName.Text = objRetVal[0].TransportationVendor.ToUpper();
                                lbArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].TransportationVendor.ToUpper());
                                //if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                //    tbArrivePhone.Text = objRetVal[0].PhoneNum.ToString();
                                //if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                //    tbArriveFax.Text = objRetVal[0].FaxNum.ToString();
                                //if (objRetVal[0].NegotiatedRate.ToString() != null)
                                //    tbArriveRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(hdnArriveTransportID.Value))
                                    lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("Arrival Transport Code Does Not Exist");
                                tbArriveTransName.Text = string.Empty;
                                tbArrivePhone.Text = string.Empty;
                                tbArriveFax.Text = string.Empty;
                                tbArriveRate.Text = string.Empty;
                                tbArriveTransCode.Focus();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(hdnArriveTransportID.Value))
                                lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("Arrival Transport Code Does Not Exist");
                            tbArriveTransName.Text = string.Empty;
                            tbArrivePhone.Text = string.Empty;
                            tbArriveFax.Text = string.Empty;
                            tbArriveRate.Text = string.Empty;
                            tbArriveTransCode.Focus();
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(tbArriveTransCode.Text) && string.IsNullOrEmpty(hdnArriveTransportID.Value))
                {
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objDstsvc.GetTransportByTransportCode(Convert.ToInt64(hdnArrivalICao.Value), tbArriveTransCode.Text.Trim()).EntityList;
                        if (objRetVal != null && objRetVal.Count > 0)
                        {

                            tbArriveTransCode.Text = objRetVal[0].TransportCD.ToUpper();
                            tbArriveTransName.Text = objRetVal[0].TransportationVendor.ToUpper();
                            lbArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].TransportationVendor.ToUpper());
                            hdnArriveTransportID.Value = objRetVal[0].TransportID.ToString();
                            hdnArriveTransAirID.Value = objRetVal[0].AirportID.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                tbArrivePhone.Text = objRetVal[0].PhoneNum.ToString();
                            if (!string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                tbArriveFax.Text = objRetVal[0].FaxNum.ToString();
                            if (objRetVal[0].NegotiatedRate.ToString() != null)
                                tbArriveRate.Text = objRetVal[0].NegotiatedRate.ToString();
                            lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("");
                        }
                        else
                        {
                            lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("Arrival Transport Code Does Not Exist");
                            tbArriveTransName.Text = string.Empty;
                            tbArrivePhone.Text = string.Empty;
                            tbArriveFax.Text = string.Empty;
                            tbArriveRate.Text = string.Empty;
                            tbArriveTransCode.Focus();
                        }
                    }

                }
                else
                {
                    hdnArriveTransportID.Value = "";
                    lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("Arrival Transport Code Does Not Exist");
                    tbArriveTransName.Text = string.Empty;
                    tbArrivePhone.Text = string.Empty;
                    tbArriveFax.Text = string.Empty;
                    tbArriveRate.Text = string.Empty;
                    tbArriveTransCode.Focus();
                }
            }
        }
        private void SaveTransport(PreflightLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Leg.PreflightTransportLists == null)
                    Leg.PreflightTransportLists = new List<PreflightTransportList>();


                PreflightTransportList LegDepTansport = Leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsDepartureTransport == true).SingleOrDefault();
                PreflightTransportList LegArrTansport = Leg.PreflightTransportLists.Where(x => x.CrewPassengerType == "P" && x.IsDeleted == false && x.IsArrivalTransport == true).SingleOrDefault();

                //Depart tansport exists but it is removed from UI so delete this transport from List
                #region Delete Existing Depart transport if it is removed from UI
                if (LegDepTansport != null && (string.IsNullOrEmpty(tbTransCode.Text) && string.IsNullOrEmpty(tbTransName.Text)))
                {

                    if (LegDepTansport.PreflightTransportID != 0)
                    {
                        LegDepTansport.State = TripEntityState.Deleted;
                        LegDepTansport.IsDeleted = true;
                    }
                    else
                    {
                        if (Leg.State == TripEntityState.Added)
                            Leg.PreflightTransportLists.Remove(LegDepTansport);
                    }
                }
                #endregion

                //Arrival tansport exists but it is removed from UI so delete this transport from List
                #region Delete Existing arrival transport if it is removed from UI
                if (LegArrTansport != null && (string.IsNullOrEmpty(tbArriveTransCode.Text) && string.IsNullOrEmpty(tbArriveTransName.Text)))
                {
                    if (LegArrTansport.PreflightTransportID != 0)
                    {
                        LegArrTansport.State = TripEntityState.Deleted;
                        LegArrTansport.IsDeleted = true;
                    }
                    else
                    {
                        if (Leg.State == TripEntityState.Added)
                            Leg.PreflightTransportLists.Remove(LegArrTansport);
                    }
                }
                #endregion

                #region Modify existing Depart transport
                if (LegDepTansport != null && (!string.IsNullOrEmpty(tbTransCode.Text) || !string.IsNullOrEmpty(tbTransName.Text)))
                {

                    if (LegDepTansport.PreflightTransportID != 0)
                        LegDepTansport.State = TripEntityState.Modified;
                    else
                        LegDepTansport.State = TripEntityState.Added;

                    LegDepTansport.PreflightTransportName = tbTransName.Text;
                    if (!string.IsNullOrEmpty(hdnTransportID.Value))
                        LegDepTansport.TransportID = Convert.ToInt64(hdnTransportID.Value);
                    LegDepTansport.IsDepartureTransport = true;
                    //if (!string.IsNullOrEmpty(hdnTransAirID.Value))
                    //    CrewTranportlist[0].AirportID = Convert.ToInt64(hdnTransAirID.Value);
                    LegDepTansport.AirportID = Leg.DepartICAOID;

                    LegDepTansport.CrewPassengerType = "P";
                    LegDepTansport.Street = "";
                    LegDepTansport.CityName = "";
                    LegDepTansport.StateName = "";
                    LegDepTansport.PostalZipCD = "";
                    LegDepTansport.PhoneNum1 = tbTransPhone.Text;
                    LegDepTansport.FaxNUM = tbTransFax.Text;
                    LegDepTansport.LastUpdTS = DateTime.UtcNow;
                    LegDepTansport.IsDeleted = false;
                    LegDepTansport.ConfirmationStatus = tbTransConfirmation.Text;
                    LegDepTansport.Comments = tbTransComments.Text;
                    LegDepTansport.IsCompleted = chkTransCompleted.Checked;
                    LegDepTansport.PhoneNum4 = tbTransRate.Text;

                    if (ddlDepartTransportCompleteds.SelectedValue != null && ddlDepartTransportCompleteds.SelectedValue != "")
                    {
                        if (ddlDepartTransportCompleteds.SelectedValue == "1")
                            LegDepTansport.Status = "Required";
                        if (ddlDepartTransportCompleteds.SelectedValue == "2")
                            LegDepTansport.Status = "Not Required";
                        if (ddlDepartTransportCompleteds.SelectedValue == "3")
                            LegDepTansport.Status = "In Progress";
                        if (ddlDepartTransportCompleteds.SelectedValue == "4")
                            LegDepTansport.Status = "Change";
                        if (ddlDepartTransportCompleteds.SelectedValue == "5")
                            LegDepTansport.Status = "Canceled";
                        if (ddlDepartTransportCompleteds.SelectedValue == "6")
                            LegDepTansport.Status = "Completed";
                    }

                }
                #endregion

                #region Modify Existing Arrival Transport
                if (LegArrTansport != null && (!string.IsNullOrEmpty(tbArriveTransCode.Text) || !string.IsNullOrEmpty(tbArriveTransName.Text)))
                {

                    if (LegArrTansport.PreflightTransportID != 0)
                        LegArrTansport.State = TripEntityState.Modified;
                    else
                        LegArrTansport.State = TripEntityState.Added;

                    LegArrTansport.PreflightTransportName = tbArriveTransName.Text;
                    if (!string.IsNullOrEmpty(hdnArriveTransportID.Value))
                        LegArrTansport.TransportID = Convert.ToInt64(hdnArriveTransportID.Value);

                    LegArrTansport.IsArrivalTransport = true;
                    //if (!string.IsNullOrEmpty(hdnArriveTransAirID.Value))
                    //    LegArrTansport.AirportID = Convert.ToInt64(hdnArriveTransAirID.Value);

                    LegArrTansport.AirportID = Leg.ArriveICAOID;

                    LegArrTansport.CrewPassengerType = "P";
                    LegArrTansport.Street = "";
                    LegArrTansport.CityName = "";
                    LegArrTansport.StateName = "";
                    LegArrTansport.PostalZipCD = "";
                    LegArrTansport.PhoneNum1 = tbArrivePhone.Text;
                    LegArrTansport.FaxNUM = tbArriveFax.Text;
                    LegArrTansport.LastUpdTS = DateTime.UtcNow;
                    LegArrTansport.IsDeleted = false;
                    LegArrTansport.ConfirmationStatus = tbArriveConfirmation.Text;
                    LegArrTansport.Comments = tbArriveComments.Text;
                    LegArrTansport.IsCompleted = chkArriveCompleted.Checked;
                    LegArrTansport.PhoneNum4 = tbArriveRate.Text;

                    if (ddlArriveTransportCompleteds.SelectedValue != null && ddlArriveTransportCompleteds.SelectedValue != "")
                    {
                        if (ddlArriveTransportCompleteds.SelectedValue == "1")
                            LegArrTansport.Status = "Required";
                        if (ddlArriveTransportCompleteds.SelectedValue == "2")
                            LegArrTansport.Status = "Not Required";
                        if (ddlArriveTransportCompleteds.SelectedValue == "3")
                            LegArrTansport.Status = "In Progress";
                        if (ddlArriveTransportCompleteds.SelectedValue == "4")
                            LegArrTansport.Status = "Change";
                        if (ddlArriveTransportCompleteds.SelectedValue == "5")
                            LegArrTansport.Status = "Canceled";
                        if (ddlArriveTransportCompleteds.SelectedValue == "6")
                            LegArrTansport.Status = "Completed";
                    }
                }
                #endregion

                #region Depart Transport does not exists, so create it
                if (LegDepTansport == null && (!string.IsNullOrEmpty(tbTransCode.Text) || !string.IsNullOrEmpty(tbTransName.Text)))
                {

                    LegDepTansport = new PreflightTransportList();
                    LegDepTansport.State = TripEntityState.Added;
                    LegDepTansport.LegID = Leg.LegID;
                    LegDepTansport.IsDepartureTransport = true;
                    LegDepTansport.IsArrivalTransport = false;
                    AddDepartTransport(ref LegDepTansport);
                    LegDepTansport.AirportID = Leg.DepartICAOID;
                    Leg.PreflightTransportLists.Add(LegDepTansport);
                    Leg.DepartAirportChangedUpdateCrew = false;
                }

                #endregion

                #region  Arrival Transport does not exists, so create it
                if (LegArrTansport == null && (!string.IsNullOrEmpty(tbArriveTransCode.Text) || !string.IsNullOrEmpty(tbArriveTransName.Text)))
                {
                    LegArrTansport = new PreflightTransportList();
                    LegArrTansport.State = TripEntityState.Added;
                    LegArrTansport.LegID = Leg.LegID;
                    LegArrTansport.IsDepartureTransport = false;
                    LegArrTansport.IsArrivalTransport = true;
                    AddArriveTransport(ref LegArrTansport);
                    LegArrTansport.AirportID = Leg.ArriveICAOID;
                    Leg.PreflightTransportLists.Add(LegArrTansport);
                    Leg.ArrivalAirportChangedUpdateCrew = false;
                }
                #endregion
            }
        }
        private void UpdateHiddenAirportIDsBasedOnLegnum(Int64 Legnum)
        {
            if (Session["CurrentPreFlightTrip"] != null)
            {
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                if (Trip.PreflightLegs != null)
                {
                    PreflightLeg LegtoLoad = Trip.PreflightLegs.Where(x => x.LegNUM == Legnum && x.IsDeleted == false).SingleOrDefault();
                    if (LegtoLoad != null)
                    {
                        hdnArrivalICao.Value = LegtoLoad.ArriveICAOID.ToString();
                        hdnDepartIcao.Value = LegtoLoad.DepartICAOID.ToString();
                    }
                }
            }
        }
        private void disableEnableTransportFields(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                #region Departure Transport Fields
                chkTransCompleted.Enabled = status;
                ddlDepartTransportCompleteds.Enabled = status;

                tbTransCode.Enabled = status;
                btnTransport.Enabled = status;
                tbTransRate.Enabled = status;

                tbTransName.Enabled = status;
                tbTransPhone.Enabled = status;
                tbTransFax.Enabled = status;
                tbTransConfirmation.Enabled = status;
                tbTransComments.Enabled = status;
                #endregion

                #region Arrival Transport Fields
                chkArriveCompleted.Enabled = status;
                ddlArriveTransportCompleteds.Enabled = status;
                tbArriveTransCode.Enabled = status;
                btnArriveTrans.Enabled = status;
                tbArriveRate.Enabled = status;
                tbArriveTransName.Enabled = status;
                tbArrivePhone.Enabled = status;
                tbArriveFax.Enabled = status;
                tbArriveConfirmation.Enabled = status;
                tbArriveComments.Enabled = status;
                #endregion

                if (status)
                {
                    btnTransport.CssClass = "browse-button";
                    btnArriveTrans.CssClass = "browse-button";
                }
                else
                {
                    btnTransport.CssClass = "browse-button-disabled";
                    btnArriveTrans.CssClass = "browse-button-disabled";
                }

            }
        }
        private void LoadTransportChoice(PreflightLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    bool DepartExists = false;
                    bool ArriveExists = false;
                    // To display the chosen Depart Transport choice for PreflightPax
                    if (Leg != null)
                    {
                        if (Leg.PreflightTransportLists == null || Leg.PreflightTransportLists.Count == 0)
                        {
                            Leg.PreflightTransportLists = new List<PreflightTransportList>();
                        }
                        if (Leg.PreflightTransportLists != null)
                        {
                            if (Leg.PreflightTransportLists.Count > 0)
                            {
                                List<PreflightTransportList> PaxTranportlist = new List<PreflightTransportList>();
                                PaxTranportlist = Leg.PreflightTransportLists.Where(X => X.CrewPassengerType == "P" && X.IsDeleted == false).ToList();
                                if (PaxTranportlist.Count == 1)
                                {
                                    if (PaxTranportlist[0].IsDepartureTransport == true && !Leg.DepartAirportChangedUpdatePAX)
                                    {
                                        DepartExists = true;
                                    }
                                    if (PaxTranportlist[0].IsArrivalTransport == true && !Leg.ArrivalAirportChangedUpdatePAX)
                                    {
                                        ArriveExists = true;
                                    }
                                    if (PaxTranportlist[0].IsDepartureTransport == true && Leg.DepartAirportChangedUpdatePAX)
                                    {
                                        if (PaxTranportlist[0].PreflightTransportID == 0)
                                        {
                                            DepartExists = false;
                                            Leg.PreflightTransportLists.Remove(PaxTranportlist[0]);
                                        }
                                        else
                                        {
                                            DepartExists = false;
                                            PaxTranportlist[0].IsDeleted = true;
                                            PaxTranportlist[0].State = TripEntityState.Deleted;
                                        }
                                        Leg.DepartAirportChangedUpdatePAX = false;
                                    }

                                    if (PaxTranportlist[0].IsArrivalTransport == true && Leg.ArrivalAirportChangedUpdatePAX)
                                    {
                                        if (PaxTranportlist[0].PreflightTransportID == 0)
                                        {
                                            ArriveExists = false;
                                            Leg.PreflightTransportLists.Remove(PaxTranportlist[0]);
                                        }
                                        else
                                        {
                                            ArriveExists = false;
                                            PaxTranportlist[0].IsDeleted = true;
                                            PaxTranportlist[0].State = TripEntityState.Deleted;
                                        }
                                        Leg.ArrivalAirportChangedUpdatePAX = false;
                                    }
                                }
                                if (PaxTranportlist.Count == 2)
                                {
                                    if (!Leg.DepartAirportChangedUpdatePAX)
                                    {
                                        DepartExists = true;
                                    }
                                    if (!Leg.ArrivalAirportChangedUpdatePAX)
                                    {
                                        ArriveExists = true;
                                    }
                                    if (Leg.DepartAirportChangedUpdatePAX)
                                    {
                                        foreach (PreflightTransportList transportList in PaxTranportlist)
                                        {
                                            if (transportList.IsDepartureTransport == true && Leg.DepartAirportChangedUpdatePAX)
                                            {
                                                if (transportList.PreflightTransportID == 0)
                                                {
                                                    DepartExists = false;
                                                    Leg.PreflightTransportLists.Remove(transportList);
                                                }
                                                else
                                                {
                                                    DepartExists = false;
                                                    transportList.IsDeleted = true;
                                                    transportList.State = TripEntityState.Deleted;
                                                }
                                                Leg.DepartAirportChangedUpdatePAX = false;
                                            }
                                            else if (transportList.IsDepartureTransport == true && !Leg.DepartAirportChangedUpdatePAX)
                                            {
                                                DepartExists = true;
                                            }
                                        }
                                    }
                                    if (Leg.ArrivalAirportChangedUpdatePAX)
                                    {
                                        foreach (PreflightTransportList transportList in PaxTranportlist)
                                        {
                                            if (transportList.IsArrivalTransport == true && Leg.ArrivalAirportChangedUpdatePAX)
                                            {
                                                if (transportList.PreflightTransportID == 0)
                                                {
                                                    ArriveExists = false;
                                                    Leg.PreflightTransportLists.Remove(transportList);
                                                }
                                                else
                                                {
                                                    ArriveExists = false;
                                                    transportList.IsDeleted = true;
                                                    transportList.State = TripEntityState.Deleted;
                                                }
                                                Leg.ArrivalAirportChangedUpdatePAX = false;
                                            }
                                            else if (transportList.IsArrivalTransport == true && !Leg.ArrivalAirportChangedUpdatePAX)
                                            {
                                                ArriveExists = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (Leg.DepartICAOID != null && Leg.DepartICAOID > 0 && !DepartExists)
                        {

                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetTransportByAirportID((long)Leg.DepartICAOID).EntityList.Where(x => x.IsChoice == true).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                PreflightTransportList DepTransport = new PreflightTransportList();

                                PreflightService.Transport PrefTransport = new PreflightService.Transport();

                                PrefTransport.TransportID = ObjRetValGroup[0].TransportID;
                                PrefTransport.TransportCD = ObjRetValGroup[0].TransportCD;
                                DepTransport.Transport = PrefTransport;

                                DepTransport.CrewPassengerType = "P";
                                DepTransport.AirportID = Leg.DepartICAOID;//ObjRetValGroup[0].AirportID;
                                DepTransport.IsArrivalTransport = false;
                                DepTransport.IsDepartureTransport = true;
                                DepTransport.TransportID = ObjRetValGroup[0].TransportID;
                                DepTransport.PreflightTransportName = ObjRetValGroup[0].TransportationVendor;
                                DepTransport.PhoneNum1 = ObjRetValGroup[0].PhoneNum;
                                DepTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                                DepTransport.PhoneNum4 = ObjRetValGroup[0].NegotiatedRate.ToString();
                                DepTransport.LastUpdTS = DateTime.UtcNow;
                                DepTransport.IsDeleted = false;
                                DepTransport.State = TripEntityState.Added;
                                Leg.PreflightTransportLists.Add(DepTransport);
                                Leg.DepartAirportChangedUpdatePAX = false;

                            }
                        }

                        // To display the chosen Arrive Transport choice for PreflightPax
                        if (Leg != null && Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0 && !ArriveExists)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetTransportByAirportID((long)Leg.ArriveICAOID).EntityList.Where(x => x.IsChoice == true).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                PreflightTransportList ArrTransport = new PreflightTransportList();

                                PreflightService.Transport PrefTransport = new PreflightService.Transport();

                                PrefTransport.TransportID = ObjRetValGroup[0].TransportID;
                                PrefTransport.TransportCD = ObjRetValGroup[0].TransportCD;
                                ArrTransport.Transport = PrefTransport;

                                ArrTransport.CrewPassengerType = "P";
                                ArrTransport.AirportID = Leg.ArriveICAOID;//ObjRetValGroup[0].AirportID;
                                ArrTransport.IsArrivalTransport = true;
                                ArrTransport.IsDepartureTransport = false;
                                ArrTransport.TransportID = ObjRetValGroup[0].TransportID;
                                ArrTransport.PreflightTransportName = ObjRetValGroup[0].TransportationVendor;
                                ArrTransport.PhoneNum1 = ObjRetValGroup[0].PhoneNum;
                                ArrTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                                ArrTransport.PhoneNum4 = ObjRetValGroup[0].NegotiatedRate.ToString();
                                ArrTransport.LastUpdTS = DateTime.UtcNow;
                                ArrTransport.IsDeleted = false;
                                ArrTransport.State = TripEntityState.Added;
                                Leg.PreflightTransportLists.Add(ArrTransport);
                                Leg.ArrivalAirportChangedUpdatePAX = false;
                            }
                        }
                    }
                }
            }
        }
        protected void AddDepartTransport(ref PreflightTransportList departTrans)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                departTrans.PreflightTransportName = tbTransName.Text;
                if (!string.IsNullOrEmpty(hdnTransportID.Value))
                    departTrans.TransportID = Convert.ToInt64(hdnTransportID.Value);
                departTrans.IsDepartureTransport = true;
                if (!string.IsNullOrEmpty(hdnTransAirID.Value))
                    departTrans.AirportID = Convert.ToInt64(hdnTransAirID.Value);
                departTrans.CrewPassengerType = "P";
                departTrans.Street = "";
                departTrans.CityName = "";
                departTrans.StateName = "";
                departTrans.PostalZipCD = "";
                departTrans.PhoneNum1 = tbTransPhone.Text;
                departTrans.FaxNUM = tbTransFax.Text;
                departTrans.LastUpdTS = DateTime.UtcNow;
                departTrans.IsDeleted = false;
                departTrans.ConfirmationStatus = tbTransConfirmation.Text;
                departTrans.Comments = tbTransComments.Text;
                departTrans.IsCompleted = chkTransCompleted.Checked;
                departTrans.PhoneNum4 = tbTransRate.Text;
                if (ddlDepartTransportCompleteds.SelectedValue != null && ddlDepartTransportCompleteds.SelectedValue != "")
                {
                    if (ddlDepartTransportCompleteds.SelectedValue == "1")
                        departTrans.Status = "Required";
                    if (ddlDepartTransportCompleteds.SelectedValue == "2")
                        departTrans.Status = "Not Required";
                    if (ddlDepartTransportCompleteds.SelectedValue == "3")
                        departTrans.Status = "In Progress";
                    if (ddlDepartTransportCompleteds.SelectedValue == "4")
                        departTrans.Status = "Change";
                    if (ddlDepartTransportCompleteds.SelectedValue == "5")
                        departTrans.Status = "Canceled";
                    if (ddlDepartTransportCompleteds.SelectedValue == "6")
                        departTrans.Status = "Completed";
                }
            }
        }
        protected void AddArriveTransport(ref PreflightTransportList arriveTrans)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                arriveTrans.PreflightTransportName = tbArriveTransName.Text;
                if (!string.IsNullOrEmpty(hdnArriveTransportID.Value))
                    arriveTrans.TransportID = Convert.ToInt64(hdnArriveTransportID.Value);

                arriveTrans.IsArrivalTransport = true;
                if (!string.IsNullOrEmpty(hdnArriveTransAirID.Value))
                    arriveTrans.AirportID = Convert.ToInt64(hdnArriveTransAirID.Value);
                arriveTrans.CrewPassengerType = "P";
                arriveTrans.Street = "";
                arriveTrans.CityName = "";
                arriveTrans.StateName = "";
                arriveTrans.PostalZipCD = "";
                arriveTrans.PhoneNum1 = tbArrivePhone.Text;
                arriveTrans.FaxNUM = tbArriveFax.Text;
                arriveTrans.LastUpdTS = DateTime.UtcNow;
                arriveTrans.IsDeleted = false;
                arriveTrans.ConfirmationStatus = tbArriveConfirmation.Text;
                arriveTrans.Comments = tbArriveComments.Text;
                arriveTrans.IsCompleted = chkArriveCompleted.Checked;
                arriveTrans.PhoneNum4 = tbArriveRate.Text;
                if (ddlArriveTransportCompleteds.SelectedValue != null && ddlArriveTransportCompleteds.SelectedValue != "")
                {
                    if (ddlArriveTransportCompleteds.SelectedValue == "1")
                        arriveTrans.Status = "Required";
                    if (ddlArriveTransportCompleteds.SelectedValue == "2")
                        arriveTrans.Status = "Not Required";
                    if (ddlArriveTransportCompleteds.SelectedValue == "3")
                        arriveTrans.Status = "In Progress";
                    if (ddlArriveTransportCompleteds.SelectedValue == "4")
                        arriveTrans.Status = "Change";
                    if (ddlArriveTransportCompleteds.SelectedValue == "5")
                        arriveTrans.Status = "Canceled";
                    if (ddlArriveTransportCompleteds.SelectedValue == "6")
                        arriveTrans.Status = "Completed";
                }
            }
        }
        protected void tbTransCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTransport);
                        lbTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTransportID.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbTransCode.Text))
                        {
                            RetrieveDepartTransport();
                        }
                        else
                        {
                            lbTransCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            tbTransName.Text = string.Empty;
                            tbTransPhone.Text = string.Empty;
                            tbTransFax.Text = string.Empty;
                            tbTransRate.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void tbArriveTransCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveTrans);
                        lbArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnArriveTransportID.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbArriveTransCode.Text))
                        {
                            RetrieveArriveTransport();
                        }
                        else
                        {
                            lbArriveTransCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            tbArriveTransName.Text = string.Empty;
                            tbArrivePhone.Text = string.Empty;
                            tbArriveFax.Text = string.Empty;
                            tbArriveRate.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        protected void btnTranHotCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ClearHotel();
                        ClearTransport();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
        #endregion

    }
}