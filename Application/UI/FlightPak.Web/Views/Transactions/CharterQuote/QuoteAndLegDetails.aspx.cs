﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.CalculationService;

namespace FlightPak.Web.Views.CharterQuote
{

    public partial class QuoteAndLegDetails : BaseSecuredPage
    {
        #region Declarations
        private ExceptionManager exManager;
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress;
        private bool selectchangefired = false;
        private delegate void SaveSession();

        const decimal statuateConvFactor = 1.1508M;

        public enum Legstate
        {
            Add = 1,
            Insert = 2,
            Delete = 3
        }

        #endregion

        #region Comments
        //Defect: 2764  Changing a date on a leg changes the dates on every other leg as well.
        //       If the departure date or arrival date has been modified for a leg, then the next legs 
        //departure and arrival date should be adjusted automatically if the change is done from the Preflight screen 
        //(if changed from the Leg screen, only the leg will be changed). If the change was initiated from the Preflight screen, 
        //a prompt should be displayed “This will auto update all leg dates?” OK / Cancel
        //__________________________________________
        //The Preflight | Legs date should only be changed for the specific Leg being edited/updated regardless of the End of Duty Day marker. 
        //This will not change the functionality from the Preflight screen - only the Leg screen.
        //Fixed  by Prabhu: Mar 25, 2014.
        #endregion
        /// <summary>
        /// Wire up the SaveCharterQuoteToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveCharterQuoteFileSession = new SaveSession(SaveCharterQuoteToSession);
                        Master.SaveToSession = SaveCharterQuoteFileSession;

                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;

                        Master._DeleteClick += Delete_Click;
                        Master._SaveClick += Save_Click;
                        Master._CancelClick += Cancel_Click;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditFile, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgLegs.ClientID));

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgLegs, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgStandardCharges, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepart, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrival, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbLocalDate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtlocaltime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbUtcDate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtUtctime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbHomeDate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtHomeTime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalDate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtArrivalTime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalUtcDate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtArrivalUtctime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalHomeDate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtArrivalHomeTime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbFeeGroup, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFeeGrpAppend, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFeeGrpOverride, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFeeGrpCancel, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgStandardCharges, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepart, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrival, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbLocalDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtlocaltime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbUtcDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtUtctime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbHomeDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtHomeTime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtArrivalTime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalUtcDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtArrivalUtctime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalHomeDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmtArrivalHomeTime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbFeeGroup, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFeeGrpAppend, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFeeGrpOverride, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFeeGrpCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAddLeg, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnInsertLeg, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteLeg, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDayRoom, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        
                        // Changes for Legnum in CQ Header
                        RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                        Panel pnlFloater = (Panel)contentPlaceHolder.FindControl("pnlFloater");
                        manager.AjaxSettings.AddAjaxSetting(dgLegs, pnlFloater);
                        //manager.AjaxRequest += new RadAjaxControl.AjaxRequestDelegate(manager_AjaxRequest);

                        ResetHomebaseSettings();

                        if (Session[Master.CQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[Master.CQSessionKey];

                            if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                            {
                                Int64 QuoteNumInProgress = 1;
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                if (QuoteInProgress != null)
                                {
                                    FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId objCompany = new GetAllCompanyMasterbyHomeBaseId();

                                    if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                        hdnHomebaseAirport.Value = FileRequest.HomeBaseAirportID.ToString();
                                    else
                                    {
                                        objCompany = Master.GetCompany((long)FileRequest.HomebaseID);
                                        if (objCompany != null)
                                        {

                                            if (objCompany.HomebaseAirportID != null)
                                            {
                                                FileRequest.HomeBaseAirportID = (long)objCompany.HomebaseAirportID;
                                                hdnHomebaseAirport.Value = objCompany.HomebaseAirportID.ToString();
                                                Session[Master.CQSessionKey] = FileRequest;
                                            }
                                        }
                                    }
                                    if (FileRequest.HomeBaseAirportID != null)
                                    {
                                        FlightPakMasterService.GetAllAirport Fleetcompanyairport = new GetAllAirport();
                                        Fleetcompanyairport = Master.GetAirport((long)FileRequest.HomeBaseAirportID);
                                        if (Fleetcompanyairport != null)
                                            hdnCountryID.Value = Fleetcompanyairport.CountryID.ToString();
                                    }

                                    if (QuoteInProgress.AircraftID != null)
                                        hdnAircraft.Value = QuoteInProgress.AircraftID.ToString();
                                    if (FileRequest.Mode != CQRequestActionMode.Edit)
                                    {
                                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).Count() == 0)
                                        {
                                            Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile", true);
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile", true);
                                }
                            }
                        }
                        else
                            Response.Redirect("CharterQuoteRequest.aspx?seltab=File", true);


                        //Company Profile Settings
                        hdTimeDisplayTenMin.Value = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? "1" : UserPrincipal.Identity._fpSettings._TimeDisplayTenMin.ToString();
                        hdShowRequestor.Value = UserPrincipal.Identity._fpSettings._IsReqOnly.ToString();
                        hdHighlightTailno.Value = UserPrincipal.Identity._fpSettings._IsANotes.ToString();
                        hdIsDepartAuthReq.Value = UserPrincipal.Identity._fpSettings._IsDepartAuthDuringReqSelection.ToString();
                        hdSetWindReliability.Value = (UserPrincipal.Identity._fpSettings._WindReliability == null ? 3 : UserPrincipal.Identity._fpSettings._WindReliability).ToString();
                        hdSetCrewRules.Value = UserPrincipal.Identity._fpSettings._IsAutoCreateCrewAvailInfo.ToString();
                        hdHomeCategory.Value = (UserPrincipal.Identity._fpSettings._DefaultFlightCatID == null ? 0 : UserPrincipal.Identity._fpSettings._DefaultFlightCatID).ToString();
                        hdnCompSettingforshowWarn.Value = UserPrincipal.Identity._fpSettings._IsQuoteWarningMessage ? "true" : "false";

                        //Load US Country for DOM/Internation calculation 
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                            var fleetretval = FleetService.GetFleet();
                            if (fleetretval.ReturnFlag)
                            {
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.FleetID != 0 && QuoteInProgress.FleetID != null)
                                    {
                                        GetFleetList = fleetretval.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();

                                        if (GetFleetList != null && GetFleetList.Count > 0)
                                        {
                                            if (QuoteInProgress.Fleet != null)
                                            {
                                                QuoteInProgress.Fleet.MaximumPassenger = GetFleetList[0].MaximumPassenger ?? 0;

                                                if (QuoteInProgress.CQLegs != null)
                                                {
                                                    foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.DAirportID != null && x.AAirportID != null))
                                                    {
                                                        Int32 SeatTotal = 0;
                                                        if (QuoteInProgress.Fleet.MaximumPassenger != null)
                                                            SeatTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);

                                                        if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                                            Leg.State = CQRequestEntityState.Modified;

                                                        if (Leg.PassengerTotal != null)
                                                            Leg.ReservationAvailable = SeatTotal - Leg.PassengerTotal;
                                                        else
                                                            Leg.ReservationAvailable = SeatTotal;
                                                    }
                                                }
                                                Master.SaveQuoteinProgressToFileSession();
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        if (QuoteInProgress != null)
                        {
                            if (!IsPostBack)
                            {
                                #region "Kilometer/Miles"
                                lbMiles.Text = UserPrincipal.Identity._fpSettings._IsKilometer ? System.Web.HttpUtility.HtmlEncode("Kilometers") : System.Web.HttpUtility.HtmlEncode("Miles(N)");
                                #endregion

                                Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                                setWarnMessageToDefault();

                                if (FileRequest.Mode == CQRequestActionMode.Edit)
                                {
                                    if (QuoteInProgress.CQLegs == null || QuoteInProgress.CQLegs.Count == 0)
                                    {
                                        #region "Add New Leg"

                                        QuoteInProgress.CQLegs = new List<CQLeg>();
                                        CQLeg FirstLeg = new CQLeg();
                                        FirstLeg.LegNUM = 1; //Convert.ToInt16(hdnLegNum.Value);
                                        FirstLeg.IsDeleted = false;
                                        FirstLeg.ShowWarningMessage = true;
                                        hdnShowWarnMessage.Value = "true";

                                        if (UserPrincipal.Identity._fpSettings._CQCrewDuty != null)
                                        {
                                            if (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._CQCrewDuty))
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                                    var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                                    if (objRetVal.ReturnFlag)
                                                    {
                                                        CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == UserPrincipal.Identity._fpSettings._CQCrewDuty.Trim().ToUpper()).ToList();
                                                        if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                                        {
                                                            FirstLeg.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                            CharterQuoteService.CrewDutyRule CDrule = new CrewDutyRule();
                                                            CDrule.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                            CDrule.CrewDutyRuleCD = CrewDtyRuleList[0].CrewDutyRuleCD;
                                                            hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();

                                                            //Fix for SUP-380 (3174) Starts
                                                            string DutyDayStart = System.Web.HttpUtility.HtmlEncode("Duty Day Start : 0");
                                                            string DutyDayEnd = System.Web.HttpUtility.HtmlEncode("Duty Day End : 0");
                                                            string MaxDutyHrsAllowed = System.Web.HttpUtility.HtmlEncode("Max Duty Hrs Allowed : 0");
                                                            string MaxFlightHrsAllowed = System.Web.HttpUtility.HtmlEncode("Max Flight Hrs Allowed : 0");
                                                            string MinFixedRest = System.Web.HttpUtility.HtmlEncode("Min Fixed Rest : 0");

                                                            if (CrewDtyRuleList[0].DutyDayBeingTM != null)
                                                            {
                                                                hdnDutyDayStart.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayBeingTM).ToString("0.0"));
                                                                DutyDayStart = "Duty Day Start : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayBeingTM).ToString("0.0"));
                                                            }

                                                            if (CrewDtyRuleList[0].DutyDayEndTM != null)
                                                            {
                                                                hdnDutyDayEnd.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayEndTM).ToString("0.0"));
                                                                DutyDayEnd = "Duty Day End : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayEndTM).ToString("0.0"));
                                                            }

                                                            if (CrewDtyRuleList[0].MaximumDutyHrs != null)
                                                            {
                                                                hdnMaxDutyHrsAllowed.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumDutyHrs).ToString("0.0"));
                                                                MaxDutyHrsAllowed = "Max Duty Hrs Allowed : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumDutyHrs).ToString("0.0"));
                                                            }

                                                            if (CrewDtyRuleList[0].MaximumFlightHrs != null)
                                                            {
                                                                hdnMaxFlightHrsAllowed.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumFlightHrs).ToString("0.0"));
                                                                MaxFlightHrsAllowed = "Max Flight Hrs Allowed : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumFlightHrs).ToString("0.0"));
                                                            }

                                                            if (CrewDtyRuleList[0].MinimumRestHrs != null)
                                                            {
                                                                hdnMinFixedRest.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MinimumRestHrs).ToString("0.0"));
                                                                MinFixedRest = "Min Fixed Rest : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MinimumRestHrs).ToString("0.0"));
                                                            }

                                                            tbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                                            lbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                                            lbCrewRulesCaption.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);

                                                            //Fix for SUP-380 (3174) Ends
                                                            CDrule.CrewDutyRulesDescription = CrewDtyRuleList[0].CrewDutyRulesDescription;
                                                            FirstLeg.FedAviationRegNUM = CrewDtyRuleList[0].FedAviatRegNum ?? string.Empty;
                                                            FirstLeg.CrewDutyRule = CDrule;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        hdSetFarRules.Value = UserPrincipal.Identity._fpSettings._FedAviationRegNum;
                                        Int64 HomebaseID = 0;

                                        #region "Load Homebase Airport to departure"
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTailNum)
                                        {
                                            if (QuoteInProgress.FleetID != null && QuoteInProgress.FleetID > 0)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                                    var objRetVal = objDstsvc.GetFleetProfileList();   //.GetFleetProfileList();
                                                    if (objRetVal.ReturnFlag)
                                                    {

                                                        var fleetdet = objRetVal.EntityList.Where(x => x.FleetID == QuoteInProgress.FleetID).ToList();
                                                        if (fleetdet != null && fleetdet.Count > 0 && fleetdet[0].HomebaseID != null)
                                                            HomebaseID = (Int64)fleetdet[0].HomebaseID;
                                                        if (HomebaseID == 0)
                                                        {
                                                            if (FileRequest.HomebaseID != null && FileRequest.HomebaseID > 0)
                                                                HomebaseID = (Int64)FileRequest.HomebaseID;
                                                        }

                                                        FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = Master.GetCompany(HomebaseID);
                                                        if (Comp != null)
                                                        {
                                                            FlightPakMasterService.GetAllAirport HomebaseAirport = Master.GetAirport((long)Comp.HomebaseAirportID);
                                                            if (HomebaseAirport != null)
                                                            {
                                                                CharterQuoteService.Airport DeptAirport = new CharterQuoteService.Airport();
                                                                FirstLeg.DAirportID = HomebaseAirport.AirportID;
                                                                DeptAirport.IcaoID = HomebaseAirport.IcaoID;
                                                                DeptAirport.AirportID = HomebaseAirport.AirportID;
                                                                DeptAirport.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                                DeptAirport.CityName = HomebaseAirport.CityName != null ? HomebaseAirport.CityName : string.Empty;
                                                                FirstLeg.Airport1 = DeptAirport;
                                                                FirstLeg.ArrivalAirportChanged = true;
                                                                FirstLeg.DepartAirportChanged = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (FileRequest.HomebaseID != null && FileRequest.HomebaseID > 0)
                                                {
                                                    HomebaseID = (Int64)FileRequest.HomebaseID;
                                                    FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = Master.GetCompany(HomebaseID);
                                                    if (Comp != null)
                                                    {
                                                        FlightPakMasterService.GetAllAirport HomebaseAirport = Master.GetAirport((long)Comp.HomebaseAirportID);
                                                        if (HomebaseAirport != null)
                                                        {
                                                            CharterQuoteService.Airport DeptAirport = new CharterQuoteService.Airport();
                                                            FirstLeg.DAirportID = HomebaseAirport.AirportID;
                                                            DeptAirport.IcaoID = HomebaseAirport.IcaoID;
                                                            DeptAirport.AirportID = HomebaseAirport.AirportID;
                                                            DeptAirport.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                            DeptAirport.CityName = HomebaseAirport.CityName != null ? HomebaseAirport.CityName : string.Empty;
                                                            FirstLeg.Airport1 = DeptAirport;
                                                            FirstLeg.ArrivalAirportChanged = true;
                                                            FirstLeg.DepartAirportChanged = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (FileRequest.HomebaseID != null && FileRequest.HomebaseID > 0)
                                            {
                                                HomebaseID = (Int64)FileRequest.HomebaseID;

                                                FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = Master.GetCompany(HomebaseID);
                                                if (Comp != null)
                                                {
                                                    FlightPakMasterService.GetAllAirport HomebaseAirport = Master.GetAirport((long)Comp.HomebaseAirportID);
                                                    if (HomebaseAirport != null)
                                                    {
                                                        CharterQuoteService.Airport DeptAirport = new CharterQuoteService.Airport();
                                                        FirstLeg.DAirportID = HomebaseAirport.AirportID;
                                                        DeptAirport.IcaoID = HomebaseAirport.IcaoID;
                                                        DeptAirport.AirportID = HomebaseAirport.AirportID;
                                                        DeptAirport.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                        DeptAirport.CityName = HomebaseAirport.CityName != null ? HomebaseAirport.CityName : string.Empty;
                                                        FirstLeg.Airport1 = DeptAirport;
                                                        FirstLeg.ArrivalAirportChanged = true;
                                                        FirstLeg.DepartAirportChanged = true;
                                                    }
                                                }
                                            }
                                        }

                                        #endregion

                                        #region "Load File. QuoteInProgress date to leg"

                                        if (FileRequest.EstDepartureDT != null)
                                        {
                                            FirstLeg.DepartureDTTMLocal = FileRequest.EstDepartureDT;
                                            if (FirstLeg.DAirportID != null)
                                            {
                                                CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient();
                                                if (UserPrincipal.Identity._fpSettings._IsAutomaticCalcLegTimes != null && (bool)UserPrincipal.Identity._fpSettings._IsAutomaticCalcLegTimes == true)
                                                {
                                                    //GMTDept
                                                    DateTime ldGmtDep = objDstsvc.GetGMT((long)FirstLeg.DAirportID, (DateTime)FirstLeg.DepartureDTTMLocal, true, false);
                                                    FirstLeg.DepartureGreenwichDTTM = ldGmtDep;
                                                }
                                            }
                                        }

                                        #endregion

                                        setAircraftSettings(ref FirstLeg);
                                        FirstLeg.WindReliability = UserPrincipal.Identity._fpSettings._WindReliability == null ? 3 : (int)UserPrincipal.Identity._fpSettings._WindReliability;

                                        if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                            QuoteInProgress.State = CQRequestEntityState.Modified;

                                        QuoteInProgress.CQLegs.Add(FirstLeg);
                                        #endregion
                                    }
                                }

                                // Set Readonly Class for Charges Section
                                tbTotalCost.ReadOnly = true;
                                tbUsageAdj.ReadOnly = true;
                                tbSegmentFees.ReadOnly = true;
                                tbDiscount.ReadOnly = true;
                                tbLandingFees.ReadOnly = true;
                                tbDiscountAmount.ReadOnly = true;
                                tbTaxes.ReadOnly = true;
                                tbTotalQuote.ReadOnly = true;

                                ControlEnable("TextBox", tbPaxTotal, false, "readonly-text60");
                                ControlEnable("RadNumericTextBox", tbTotalCost, false, "pr_radtextbox_80_readonly");
                                ControlEnable("RadNumericTextBox", tbUsageAdj, false, "pr_radtextbox_50_readonly");
                                ControlEnable("RadNumericTextBox", tbSegmentFees, false, "pr_radtextbox_50_readonly");
                                ControlEnable("TextBox", tbDiscount, false, "readonly-text50");
                                ControlEnable("RadNumericTextBox", tbLandingFees, false, "pr_radtextbox_50_readonly");
                                ControlEnable("RadNumericTextBox", tbDiscountAmount, false, "pr_radtextbox_50_readonly");
                                ControlEnable("RadNumericTextBox", tbTaxes, false, "pr_radtextbox_50_readonly");
                                ControlEnable("RadNumericTextBox", tbTotalQuote, false, "pr_radtextbox_50_readonly");

                                tbAdditionalFees.ForeColor = System.Drawing.Color.Black;
                                tbFlightCharges.ForeColor = System.Drawing.Color.Black;
                                tbSubTotal.ForeColor = System.Drawing.Color.Black;
                                tbMarginAmount.ForeColor = System.Drawing.Color.Black;

                                Master.BindStandardCharges(true, dgStandardCharges);
                            }
                        }
                        else
                        {
                            Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile", false);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ApplyPermissions();

                        if (!IsPostBack)
                        {
                            if (dgLegs.Items.Count > 0)
                            {
                                dgLegs.Items[0].Selected = true;
                                LoadLegDetails();

                                if (string.IsNullOrEmpty(tbDepart.Text))
                                    tbDepart.Focus();
                                else if (string.IsNullOrEmpty(tbArrival.Text))
                                    tbArrival.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void LoadLegDetails()
        {
            if (dgLegs.SelectedItems.Count > 0)
            {
                ClearFields();
                ClearLabels();
                GridDataItem item = (GridDataItem)dgLegs.SelectedItems[0];

                CQLeg cqLeg = new CQLeg();

                cqLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(item["LegNUM"].Text) && x.IsDeleted == false).FirstOrDefault();


                if (cqLeg != null)
                {
                    // Changes for Legnum in CQ Header
                    Master.FloatLegNum.Visible = true;
                    Master.FloatLegNum.Text = System.Web.HttpUtility.HtmlEncode(cqLeg.LegNUM.ToString());

                    hdnLegNum.Value = cqLeg.LegNUM.ToString();
                    RadPanelItem raditem = this.pnlTrip.FindItemByValue("pnlItemTrip");
                    raditem.Text = "Leg " + hdnLegNum.Value.ToString() + " Details";

                    tbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbCrewRulesCaption.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);

                    string DutyDayStart = System.Web.HttpUtility.HtmlEncode("Duty Day Start : 0");
                    string DutyDayEnd = System.Web.HttpUtility.HtmlEncode("Duty Day End : 0");
                    string MaxDutyHrsAllowed = System.Web.HttpUtility.HtmlEncode("Max Duty Hrs Allowed : 0");
                    string MaxFlightHrsAllowed = System.Web.HttpUtility.HtmlEncode("Max Flight Hrs Allowed : 0");
                    string MinFixedRest = System.Web.HttpUtility.HtmlEncode("Min Fixed Rest : 0");

                    #region Authorization - Check for Add/Update access and make save cancel visible

                    if (cqLeg.State == CQRequestEntityState.Modified)
                    {
                        if (!IsAuthorized(Permission.CharterQuote.EditCQLeg))
                        {
                            btnSave.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }

                    if (cqLeg.State == CQRequestEntityState.Added)
                    {
                        if (!IsAuthorized(Permission.CharterQuote.AddCQLeg))
                        {
                            btnSave.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }

                    #endregion

                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqLeg))
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];

                        #region "Leg Details"

                        #region Departs
                        if (cqLeg.Airport1 != null)
                        {
                            tbDepart.Text = cqLeg.Airport1.IcaoID;
                            lbDepart.Text = cqLeg.Airport1.AirportName != null ? System.Web.HttpUtility.HtmlEncode(cqLeg.Airport1.AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnDepart.Value = System.Web.HttpUtility.HtmlEncode(cqLeg.Airport1.AirportID.ToString());
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(cqLeg.Airport1.IcaoID);
                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count > 0)
                                    {

                                        lbDepartsICAO.Text = AirportLists[0].IcaoID == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID);
                                        lbDepartsCity.Text = AirportLists[0].CityName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName);
                                        lbDepartsState.Text = AirportLists[0].StateName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName);
                                        lbDepartsCountry.Text = AirportLists[0].CountryName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName);
                                        lbDepartsAirport.Text = AirportLists[0].AirportName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName);
                                        lbDepartsTakeoffbias.Text = AirportLists[0].TakeoffBIAS != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS.ToString()) : System.Web.HttpUtility.HtmlEncode("0");
                                        lbDepartsLandingbias.Text = AirportLists[0].LandingBIAS != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS.ToString()) : System.Web.HttpUtility.HtmlEncode("0");

                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].DSTRegionCD) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].OffsetToGMT.ToString()) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].LongestRunway.ToString()) : System.Web.HttpUtility.HtmlEncode(string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata ?? string.Empty));

                                        lbDepartIcao.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        lbDepart.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion

                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                        string tooltipStr = string.Empty;

                                        if (!string.IsNullOrEmpty(AirportLists[0].Alerts))
                                        {
                                            tbDepart.ForeColor = Color.Red;
                                            string alertStr = "Alerts : \n";
                                            alertStr += AirportLists[0].Alerts;
                                            tooltipStr = alertStr;
                                        }
                                        else
                                            tbDepart.ForeColor = Color.Black;

                                        if (!string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                        {
                                            string noteStr = string.Empty;
                                            noteStr = !string.IsNullOrEmpty(tooltipStr) ? "\n\nNotes : \n" : "Notes : \n";
                                            noteStr += AirportLists[0].GeneralNotes;
                                            tooltipStr += noteStr;
                                        }

                                        tbDepart.ToolTip = tooltipStr;

                                        #endregion
                                    }
                                }
                            }

                        }
                        else
                        {
                            tbDepart.Text = string.Empty;
                            lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnDepart.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }

                        if (cqLeg.DepartureDTTMLocal != DateTime.MinValue && cqLeg.DepartureDTTMLocal != null)
                        {
                            DateTime dt = (DateTime)cqLeg.DepartureDTTMLocal;
                            string startHrs = "0000" + Convert.ToString(dt.Hour);
                            string startMins = "0000" + Convert.ToString(dt.Minute);
                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", cqLeg.DepartureDTTMLocal);
                            rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        }
                        else
                        {
                            tbLocalDate.Text = "";
                            rmtlocaltime.Text = "0000";
                        }

                        if (cqLeg.DepartureGreenwichDTTM != DateTime.MinValue && cqLeg.DepartureGreenwichDTTM != null)
                        {
                            DateTime dt = (DateTime)cqLeg.DepartureGreenwichDTTM;
                            string startHrs = "0000" + Convert.ToString(dt.Hour);
                            string startMins = "0000" + Convert.ToString(dt.Minute);
                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", cqLeg.DepartureGreenwichDTTM);
                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        }
                        else
                        {
                            tbUtcDate.Text = "";
                            rmtUtctime.Text = "0000";
                        }

                        if (cqLeg.HomeDepartureDTTM != DateTime.MinValue && cqLeg.HomeDepartureDTTM != null)
                        {
                            DateTime dt = (DateTime)cqLeg.HomeDepartureDTTM;
                            string startHrs = "0000" + Convert.ToString(dt.Hour);
                            string startMins = "0000" + Convert.ToString(dt.Minute);
                            tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", cqLeg.HomeDepartureDTTM);
                            rmtHomeTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        }
                        else
                        {
                            tbHomeDate.Text = "";
                            rmtHomeTime.Text = "0000";
                        }

                        #endregion

                        #region Arrives

                        if (cqLeg.Airport != null)
                        {
                            tbArrival.Text = cqLeg.Airport.IcaoID;
                            lbArrival.Text = cqLeg.Airport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(cqLeg.Airport.AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnArrival.Value = System.Web.HttpUtility.HtmlEncode(cqLeg.Airport.AirportID.ToString());
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(cqLeg.Airport.IcaoID);
                                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<GetAllAirport>();

                                if (objRetVal.ReturnFlag)
                                {
                                    getAllAirportList = objRetVal.EntityList;

                                    if (getAllAirportList != null && getAllAirportList.Count > 0)
                                    {

                                        lbArrivesICAO.Text = getAllAirportList[0].IcaoID != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].IcaoID) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbArrivesCity.Text = getAllAirportList[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CityName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbArrivesState.Text = getAllAirportList[0].StateName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].StateName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbArrivesCountry.Text = getAllAirportList[0].CountryName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CountryName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbArrivesAirport.Text = getAllAirportList[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        Decimal TakeOffBias = getAllAirportList[0].TakeoffBIAS != null ? (Math.Round((decimal)getAllAirportList[0].TakeoffBIAS, 1)) : 0;
                                        lbArrivesTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(TakeOffBias.ToString());
                                        Decimal LandingBias = getAllAirportList[0].LandingBIAS != null ? (Math.Round((decimal)getAllAirportList[0].LandingBIAS, 1)) : 0;
                                        lbArrivesLandingbias.Text = System.Web.HttpUtility.HtmlEncode(LandingBias.ToString());

                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (getAllAirportList[0].IcaoID ?? string.Empty)
                                            + "\n" + "City : " + (getAllAirportList[0].CityName ?? string.Empty)
                                            + "\n" + "State/Province : " + (getAllAirportList[0].StateName ?? string.Empty)
                                            + "\n" + "Country : " + (getAllAirportList[0].CountryName ?? string.Empty)
                                            + "\n" + "Airport : " + (getAllAirportList[0].AirportName ?? string.Empty)
                                            + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD ?? string.Empty)
                                            + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);

                                        lbArrive.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        lbArrival.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion

                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                        string tooltipStr = string.Empty;
                                        if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts))
                                        {
                                            tbArrival.ForeColor = Color.Red;
                                            string alertStr = "Alerts : \n";
                                            alertStr += getAllAirportList[0].Alerts;
                                            tooltipStr = alertStr;
                                        }
                                        else
                                            tbArrival.ForeColor = Color.Black;

                                        if (!string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                        {
                                            string noteStr = string.Empty;
                                            noteStr = !string.IsNullOrEmpty(tooltipStr) ? "\n\nNotes : \n" : "Notes : \n";
                                            noteStr += getAllAirportList[0].GeneralNotes;
                                            tooltipStr += noteStr;
                                        }

                                        tbArrival.ToolTip = tooltipStr;
                                        #endregion

                                    }
                                }
                            }
                        }
                        else
                        {
                            tbArrival.Text = string.Empty;
                            lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnArrival.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }

                        if (cqLeg.ArrivalDTTMLocal != DateTime.MinValue && cqLeg.ArrivalDTTMLocal != null)
                        {
                            DateTime dt = (DateTime)cqLeg.ArrivalDTTMLocal;
                            string startHrs = "0000" + Convert.ToString(dt.Hour);
                            string startMins = "0000" + Convert.ToString(dt.Minute);
                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", cqLeg.ArrivalDTTMLocal);
                            rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        }
                        else
                        {
                            tbArrivalDate.Text = "";
                            rmtArrivalTime.Text = "0000";
                        }

                        if (cqLeg.ArrivalGreenwichDTTM != DateTime.MinValue && cqLeg.ArrivalGreenwichDTTM != null)
                        {
                            DateTime dt = (DateTime)cqLeg.ArrivalGreenwichDTTM;
                            string startHrs = "0000" + Convert.ToString(dt.Hour);
                            string startMins = "0000" + Convert.ToString(dt.Minute);
                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", cqLeg.ArrivalGreenwichDTTM);
                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        }
                        else
                        {
                            tbArrivalUtcDate.Text = "";
                            rmtArrivalUtctime.Text = "0000";
                        }

                        if (cqLeg.HomeArrivalDTTM != DateTime.MinValue && cqLeg.HomeArrivalDTTM != null)
                        {
                            DateTime dt = (DateTime)cqLeg.HomeArrivalDTTM;
                            string startHrs = "0000" + Convert.ToString(dt.Hour);
                            string startMins = "0000" + Convert.ToString(dt.Minute);
                            tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", cqLeg.HomeArrivalDTTM);
                            rmtArrivalHomeTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        }
                        else
                        {
                            tbArrivalHomeDate.Text = "";
                            rmtArrivalHomeTime.Text = "0000";
                        }

                        #endregion

                        #region Distance & Time
                        lbDepartureDate.CssClass = "time_anchor";
                        lbArrivalDate.CssClass = "time_anchor";

                        #region Time Anchor Indicator

                        if (cqLeg.IsDepartureConfirmed == null || (bool)cqLeg.IsDepartureConfirmed)
                            lbDepartureDate.CssClass = "time_anchor_active";
                        else
                            lbArrivalDate.CssClass = "time_anchor_active";

                        #endregion

                        tbMiles.Text = "0";
                        hdnMiles.Value = "0";
                        if (cqLeg.Distance != null && cqLeg.Distance != decimal.MinValue)
                        {
                            tbMiles.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)cqLeg.Distance).ToString();
                            hdnMiles.Value = cqLeg.Distance.ToString();
                        }

                        foreach (ListItem Item in rblistWindReliability.Items)
                        {
                            if (Item.Text == cqLeg.WindReliability.ToString())
                                Item.Selected = true;
                        }

                        tbPower.Text = cqLeg.PowerSetting;
                        if (cqLeg.TakeoffBIAS != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = "00:00";

                                tbBias.Text = ConvertTenthsToMins(cqLeg.TakeoffBIAS.ToString());
                            }
                            else
                            {
                                tbBias.Text = "0.0";
                                tbBias.Text = Math.Round((decimal)cqLeg.TakeoffBIAS, 1).ToString();
                            }
                        }
                        tbTAS.Text = cqLeg.TrueAirSpeed.ToString();

                        if (cqLeg.LandingBIAS != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Text = "00:00";
                                tbLandBias.Text = ConvertTenthsToMins((cqLeg.LandingBIAS).ToString());
                            }
                            else
                            {
                                tbLandBias.Text = "0.0";
                                tbLandBias.Text = Math.Round((decimal)cqLeg.LandingBIAS, 1).ToString();
                            }
                        }
                        // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (!string.IsNullOrEmpty(tbBias.Text))
                            {
                                if (tbBias.Text.IndexOf(".") < 0)
                                {
                                    tbBias.Text = tbBias.Text + ".0";
                                }
                            }

                            if (!string.IsNullOrEmpty(tbLandBias.Text))
                            {
                                if (tbLandBias.Text.IndexOf(".") < 0)
                                {
                                    tbLandBias.Text = tbLandBias.Text + ".0";
                                }
                            }
                        }

                        tbWind.Text = cqLeg.WindsBoeingTable.ToString();

                        rblistWindReliability.SelectedValue = cqLeg.WindReliability != null ? cqLeg.WindReliability.ToString() : "3";
                        double ETE = 0.0;
                        if (cqLeg.ElapseTM != null)
                        {
                            ETE = RoundElpTime((double)cqLeg.ElapseTM);
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                tbETE.Text = ConvertTenthsToMins(ETE.ToString());
                            else
                            {
                                tbETE.Text = Math.Round(ETE, 1).ToString();
                                if (tbETE.Text.IndexOf(".") < 0)
                                    tbETE.Text = tbETE.Text + ".0";
                            }

                            tbETE.Text = Common.Utility.DefaultEteResult(tbETE.Text);
                        }
                        tbTaxRate.Text = "0.0";
                        if (cqLeg.TaxRate != null)
                            tbTaxRate.Text = Math.Round((decimal)cqLeg.TaxRate, 1).ToString();

                        tbPaxTotal.Text = "0";
                        ControlEnable("TextBox", tbPaxTotal, false, "readonly-text60");

                        if (cqLeg.CQPassengers == null || cqLeg.CQPassengers.Count(x => x.IsDeleted == false) <= 0)
                        {
                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                                {
                                    ControlEnable("TextBox", tbPaxTotal, true, "text60");
                                }
                            }
                        }

                        if (cqLeg.PassengerTotal != null && cqLeg.PassengerTotal.HasValue)
                            tbPaxTotal.Text = Convert.ToString(cqLeg.PassengerTotal.Value);

                        #endregion

                        #region Duty

                        if (cqLeg.CrewDutyRule != null)
                        {
                            tbCrewRules.Text = cqLeg.CrewDutyRule.CrewDutyRuleCD;
                            hdnCrewRules.Value = System.Web.HttpUtility.HtmlEncode(cqLeg.CrewDutyRulesID.ToString());

                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                if (objRetVal.ReturnFlag)
                                {
                                    CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == tbCrewRules.Text.Trim().ToUpper()).ToList();
                                    if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                    {
                                        if (CrewDtyRuleList[0].DutyDayBeingTM != null)
                                        {
                                            hdnDutyDayStart.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayBeingTM).ToString("0.0"));
                                            DutyDayStart = "Duty Day Start : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayBeingTM).ToString("0.0"));
                                        }

                                        if (CrewDtyRuleList[0].DutyDayEndTM != null)
                                        {
                                            hdnDutyDayEnd.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayEndTM).ToString("0.0"));
                                            DutyDayEnd = "Duty Day End : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayEndTM).ToString("0.0"));
                                        }

                                        if (CrewDtyRuleList[0].MaximumDutyHrs != null)
                                        {
                                            hdnMaxDutyHrsAllowed.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumDutyHrs).ToString("0.0"));
                                            MaxDutyHrsAllowed = "Max Duty Hrs Allowed : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumDutyHrs).ToString("0.0"));
                                        }

                                        if (CrewDtyRuleList[0].MaximumFlightHrs != null)
                                        {
                                            hdnMaxFlightHrsAllowed.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumFlightHrs).ToString("0.0"));
                                            MaxFlightHrsAllowed = "Max Flight Hrs Allowed : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumFlightHrs).ToString("0.0"));
                                        }

                                        if (CrewDtyRuleList[0].MinimumRestHrs != null)
                                        {
                                            hdnMinFixedRest.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MinimumRestHrs).ToString("0.0"));
                                            MinFixedRest = "Min Fixed Rest : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MinimumRestHrs).ToString("0.0"));
                                        }

                                        tbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                        lbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                        lbCrewRulesCaption.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                    }
                                }
                            }
                        }

                        string lcCdAlert = string.Empty;
                        if (cqLeg.CrewDutyAlert != null) // Need the name to change as CrewDutyAlert
                            lcCdAlert = cqLeg.CrewDutyAlert;
                        if (lcCdAlert.LastIndexOf('D') >= 0)
                            SetLabelStyle(lbTotalDuty, "infored", Color.White, Color.Red);
                        else
                            SetLabelStyle(lbTotalDuty, "infoash", Color.Black, Color.Transparent);

                        if (lcCdAlert.LastIndexOf('F') >= 0)
                            SetLabelStyle(lbTotalFlight, "infored",Color.White,Color.Red);
                        else
                            SetLabelStyle(lbTotalFlight, "infoash", Color.Black, Color.Transparent);

                        if (lcCdAlert.LastIndexOf('R') >= 0)
                            SetLabelStyle(lbRest, "infored", Color.White, Color.Red);
                        else
                            SetLabelStyle(lbRest, "infoash", Color.Black, Color.Transparent);

                        chkEndOfDuty.Checked = cqLeg.IsDutyEnd.HasValue && cqLeg.IsDutyEnd.Value;

                        tbOverride.Text = "0.0";

                        if (cqLeg.CQOverRide != null)
                        {
                            if ((decimal)cqLeg.CQOverRide != 0)
                                tbOverride.Text = Math.Round((decimal)cqLeg.CQOverRide, 1).ToString();
                        }

                        if (cqLeg.FedAviationRegNUM != null)
                            lbFar.Text = System.Web.HttpUtility.HtmlEncode(cqLeg.FedAviationRegNUM.ToString());
                        
                        lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                        if (cqLeg.DutyHours != null && cqLeg.DutyHours != 0)
                        {
                            double dutyHours = Math.Round((double)cqLeg.DutyHours, 1);
                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(dutyHours.ToString());

                            if (lbTotalDuty.Text.IndexOf(".") < 0)
                                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(lbTotalDuty.Text + ".0");
                        }
                        else
                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode("0.0");

                        lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                        if (cqLeg.FlightHours != null && cqLeg.FlightHours != 0)
                        {
                            double totalFlightHours = Math.Round((double)cqLeg.FlightHours, 1);
                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(totalFlightHours.ToString());
                            if (lbTotalFlight.Text.IndexOf(".") < 0)
                                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(lbTotalFlight.Text + ".0");
                        }
                        else
                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode("0.0");

                        lbRest.Text = System.Web.HttpUtility.HtmlEncode("0.0");

                        if (cqLeg.RestHours != null && cqLeg.RestHours != 0)
                        {
                            double restHours = Math.Round((double)cqLeg.RestHours, 1);
                            lbRest.Text = System.Web.HttpUtility.HtmlEncode(restHours.ToString());
                            if (lbRest.Text.IndexOf(".") < 0)
                                lbRest.Text = System.Web.HttpUtility.HtmlEncode(lbRest.Text + ".0");
                        }
                        else
                            lbRest.Text = System.Web.HttpUtility.HtmlEncode("0.0");

                        #endregion

                        #region Flight

                        if (cqLeg.ChargeTotal != null)
                        {
                            if ((decimal)cqLeg.ChargeTotal != 0)
                                tbCharges.Text = Math.Round((decimal)cqLeg.ChargeTotal, 2).ToString();
                        }
                        else
                            tbCharges.Text = "0.00";

                        if (cqLeg.ChargeRate != null)
                        {
                            if ((decimal)cqLeg.ChargeRate != 0)
                                tbRate.Text = Math.Round((decimal)cqLeg.ChargeRate, 2).ToString();
                        }
                        else
                            tbRate.Text = "0.00";

                        rbDomestic.SelectedValue = cqLeg.DutyTYPE != null ? cqLeg.DutyTYPE.ToString() : "1";

                        #endregion

                        #region LastPart
                        
                        if (QuoteInProgress.Fleet != null)
                        {
                            if (QuoteInProgress.Fleet.MaximumPassenger != null && QuoteInProgress.Fleet.MaximumPassenger > 0)
                            {
                                if (cqLeg.State == CQRequestEntityState.Added)
                                {
                                    Int32 SeatTotal = 0;
                                    SeatTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);
                                    if (cqLeg.PassengerTotal != null)
                                        cqLeg.ReservationAvailable = SeatTotal - cqLeg.PassengerTotal;
                                    else
                                        cqLeg.ReservationAvailable = SeatTotal;
                                }
                            }
                        }
                        
                        if (!string.IsNullOrEmpty(cqLeg.FlightPurposeDescription))
                            tbPurpose.Text = cqLeg.FlightPurposeDescription;

                        #endregion

                        #region "RON"
                        if (!string.IsNullOrEmpty(cqLeg.RemainOverNightCNT.ToString()))
                            tbRON.Text = cqLeg.RemainOverNightCNT.ToString();
                        if (!string.IsNullOrEmpty(cqLeg.DayRONCNT.ToString()))
                            tbDayRoom.Text = cqLeg.DayRONCNT.ToString();
                        #endregion

                        chkList.Items[0].Selected = cqLeg.IsPositioning.HasValue && cqLeg.IsPositioning.Value;
                        chkList.Items[1].Selected = cqLeg.IsTaxable.HasValue && cqLeg.IsTaxable.Value;

                        #endregion

                        #region "Quote Details"
                        
                        #region "Extra Part"
                        if (QuoteInProgress.StandardCrewDOM != null)
                            tbDomStdCrew.Text = QuoteInProgress.StandardCrewDOM.ToString();

                        if (QuoteInProgress.StandardCrewINTL != null)
                            tbIntlStdCrew.Text = QuoteInProgress.StandardCrewINTL.ToString();

                        if (QuoteInProgress.CostTotal != null)
                            tbTotalCost.Text = QuoteInProgress.CostTotal.ToString();

                        if (QuoteInProgress.PrepayTotal != null)
                            tbTotPrepDeposit.Text = QuoteInProgress.PrepayTotal.ToString();
                        
                        #endregion

                        #region "Charges And Fee"

                        if (QuoteInProgress.FeeGroup != null)
                        {
                            tbFeeGroup.Text = QuoteInProgress.FeeGroup.FeeGroupCD;
                            hdnFeeGroupID.Value = QuoteInProgress.FeeGroup.FeeGroupID.ToString();
                            if (QuoteInProgress.FeeGroup.FeeGroupDescription != null)
                                lbFeeGroupDesc.Text = QuoteInProgress.FeeGroup.FeeGroupDescription != null ? System.Web.HttpUtility.HtmlEncode(QuoteInProgress.FeeGroup.FeeGroupDescription) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }

                        chkTaxDaily.Checked = QuoteInProgress.IsDailyTaxAdj.HasValue && QuoteInProgress.IsDailyTaxAdj.Value;
                        chkTaxLandingFee.Checked = QuoteInProgress.IsLandingFeeTax.HasValue && QuoteInProgress.IsLandingFeeTax.Value;

                        #endregion

                        #region "Charges"

                        if (QuoteInProgress.DaysTotal != null)
                            tbTotDays.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.DaysTotal.ToString()), 2));

                        if (QuoteInProgress.FlightHoursTotal != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                tbFltHrs.Text = Master.ConvertTenthsToMins(QuoteInProgress.FlightHoursTotal.ToString());
                            else
                                tbFltHrs.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.FlightHoursTotal.ToString()), 2));
                        }
                        if (QuoteInProgress.AverageFlightHoursTotal != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                tbAvgHrs.Text = Master.ConvertTenthsToMins(QuoteInProgress.AverageFlightHoursTotal.ToString());
                            else
                                tbAvgHrs.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.AverageFlightHoursTotal.ToString()), 2));
                        }

                        if (QuoteInProgress.AdjAmtTotal != null)
                        {
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                tbAdjustedHrs.Text = Master.ConvertTenthsToMins(QuoteInProgress.AdjAmtTotal.ToString());
                            else
                                tbAdjustedHrs.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.AdjAmtTotal.ToString()), 2));
                        }

                        if (QuoteInProgress.UsageAdjTotal != null)
                            tbUsageAdj.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.UsageAdjTotal), 2));

                        if (QuoteInProgress.SegmentFeeTotal != null)
                            tbSegmentFees.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.SegmentFeeTotal), 2));

                        if (QuoteInProgress.DiscountPercentageTotal != null)
                            tbDiscount.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.DiscountPercentageTotal), 2));

                        if (QuoteInProgress.LandingFeeTotal != null)
                            tbLandingFees.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.LandingFeeTotal), 2));

                        if (QuoteInProgress.DiscountAmtTotal != null)
                            tbDiscountAmount.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.DiscountAmtTotal), 2));

                        if (QuoteInProgress.MarginTotal != null)
                            tbMarginAmount.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.MarginTotal), 2));

                        if (QuoteInProgress.MarginalPercentTotal != null)
                        {
                            tbMargin.ForeColor = System.Drawing.Color.Black;
                            tbMargin.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.MarginalPercentTotal), 2));
                            if (QuoteInProgress.MarginalPercentTotal < 0) tbMargin.ForeColor = System.Drawing.Color.Red;
                        }

                        #region "Margin Color"

                        if (FileRequest.UseCustomFleetCharge != null && (bool)FileRequest.UseCustomFleetCharge)
                        {
                            using (MasterCatalogServiceClient MasterSvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPakMasterService.CQCustomer> GetCQCustomer = new List<FlightPakMasterService.CQCustomer>();
                                var cqcustretval = MasterSvc.GetCQCustomerList();
                                if (cqcustretval.ReturnFlag)
                                {
                                    if (QuoteInProgress.FleetID != 0 && QuoteInProgress.FleetID != null)
                                    {
                                        GetCQCustomer = cqcustretval.EntityList.Where(x => FileRequest.CQCustomerID != null && x.CQCustomerID == (long)FileRequest.CQCustomerID).ToList();

                                        if (GetCQCustomer != null && GetCQCustomer.Count > 0)
                                        {
                                            if (GetCQCustomer[0].MarginalPercentage != null && (decimal)GetCQCustomer[0].MarginalPercentage > 0 && QuoteInProgress.MarginalPercentTotal != null)
                                            {
                                                if (GetCQCustomer[0].MarginalPercentage >= QuoteInProgress.MarginalPercentTotal)
                                                    tbMargin.ForeColor = System.Drawing.Color.Red;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (QuoteInProgress.FleetID != null)
                        {

                            using (MasterCatalogServiceClient MasterSvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                                var fleetretval = MasterSvc.GetFleet();
                                if (fleetretval.ReturnFlag)
                                {
                                    GetFleetList = fleetretval.EntityList.Where(x => QuoteInProgress.FleetID != null && x.FleetID == (long) QuoteInProgress.FleetID).ToList();

                                    if (GetFleetList != null && GetFleetList.Count > 0)
                                    {
                                        if (GetFleetList[0].marginalpercentage != null && (decimal) GetFleetList[0].marginalpercentage > 0 && QuoteInProgress.MarginalPercentTotal != null)
                                        {
                                            if (GetFleetList[0].marginalpercentage >= QuoteInProgress.MarginalPercentTotal)
                                                tbMargin.ForeColor = System.Drawing.Color.Red;
                                        }
                                    }
                                }
                            }
                        }
                        else if (QuoteInProgress.AircraftID != null)
                        {

                            using (MasterCatalogServiceClient MasterSvc = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Aircraft aircraft = new FlightPakMasterService.Aircraft();
                                aircraft = Master.GetAircraft((long)QuoteInProgress.AircraftID);
                                
                                if (aircraft != null && aircraft.MarginalPercentage != null && (decimal) aircraft.MarginalPercentage > 0 && QuoteInProgress.MarginalPercentTotal != null)
                                {
                                    if (aircraft.MarginalPercentage >= QuoteInProgress.MarginalPercentTotal)
                                        tbMargin.ForeColor = System.Drawing.Color.Red;
                                }
                            }
                        }

                        #endregion

                        if (QuoteInProgress.TaxesTotal != null)
                            tbTaxes.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.TaxesTotal), 2));

                        if (QuoteInProgress.AdditionalFeeTotalD != null)
                            tbAdditionalFees.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.AdditionalFeeTotalD), 2));

                        if (QuoteInProgress.FlightChargeTotal != null)
                            tbFlightCharges.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.FlightChargeTotal), 2));

                        if (QuoteInProgress.SubtotalTotal != null)
                            tbSubTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.SubtotalTotal), 2));

                        if (QuoteInProgress.QuoteTotal != null)
                            tbTotalQuote.Text = Convert.ToString(Math.Round(Convert.ToDecimal(QuoteInProgress.QuoteTotal), 2));

                        #endregion

                        #endregion
                    }
                }
                else
                    EnableForm(false);
            }
            else
                EnableForm(false);
        }

        /// <summary>
        /// Set Label Style
        /// </summary>
        /// <param name="lbl"></param>
        /// <param name="className"></param>
        /// <param name="foreColor"></param>
        /// <param name="backColor"></param>
        private void SetLabelStyle(Label lbl, string className, Color foreColor, Color backColor)
        {
            if (lbl != null)
            {
                lbl.CssClass = className;
                lbl.ForeColor = foreColor;
                lbl.BackColor = backColor;
            }
        }

        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                            result = "0" + result;
                        if (timeArray[1].Length == 1)
                            result = result + "0";
                    }
                    else if (int.TryParse(time, out val))
                        result = time;
                }

                return result;
            }
        }

        protected double RoundElpTime(double lnElp_Time)
        {
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {
                decimal ElapseTMRounding = 0;
                double lnElp_Min = 0.0;
                double lnEteRound = 0.0;

                if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                    ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;

                if (lnEteRound > 0)
                {
                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));
                    
                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);
                    
                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);
                }
            }
            return lnElp_Time;
        }

        protected void Legs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.BindLegs(dgLegs, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void PowerSetting_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Power", typeof(Int16));
                        dt.Columns.Add("PowerSetting", typeof(string));
                        dt.Columns.Add("TAS", typeof(decimal));
                        dt.Columns.Add("HrRange", typeof(decimal));
                        dt.Columns.Add("TOBias", typeof(decimal));
                        dt.Columns.Add("LandBias", typeof(decimal));

                        if (QuoteInProgress != null)
                        {
                            if (QuoteInProgress.AircraftID != null)
                            {
                                Int64 AircraftID = (Int64)QuoteInProgress.AircraftID;
                                FlightPakMasterService.Aircraft Aircraft = Master.GetAircraft(AircraftID);
                                if (Aircraft.AircraftDescription != null)
                                    lbAircraft.Text = System.Web.HttpUtility.HtmlEncode(Aircraft.AircraftDescription);

                                dt.Rows.Clear();
                                if (Aircraft != null)
                                {
                                    dt.Rows.Add(1, Aircraft.PowerSettings1Description,
                                        Aircraft.PowerSettings1TrueAirSpeed == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1TrueAirSpeed, 0),
                                        Aircraft.PowerSettings1HourRange == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1HourRange, 1),
                                        Aircraft.PowerSettings1TakeOffBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1),
                                        Aircraft.PowerSettings1LandingBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1));
                                    dt.Rows.Add(2, Aircraft.PowerSettings2Description,
                                        Aircraft.PowerSettings2TrueAirSpeed == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2TrueAirSpeed, 0),
                                        Aircraft.PowerSettings2HourRange == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2HourRange, 1),
                                        Aircraft.PowerSettings2TakeOffBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1),
                                        Aircraft.PowerSettings2LandingBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1));
                                    dt.Rows.Add(3, Aircraft.PowerSettings3Description,
                                        Aircraft.PowerSettings3TrueAirSpeed == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3TrueAirSpeed, 0),
                                        Aircraft.PowerSettings3HourRange == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3HourRange, 1),
                                        Aircraft.PowerSettings3TakeOffBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1),
                                        Aircraft.PowerSettings3LandingBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1));
                                }
                            }
                            else
                            {
                                dt.Rows.Clear();
                                dt.Rows.Add(0, "", 0, 0.0, 0.0, 0.0);
                                dt.Rows.Add(0, "", 0, 0.0, 0.0, 0.0);
                                dt.Rows.Add(0, "", 0, 0.0, 0.0, 0.0);
                            }
                        }
                        else
                        {
                            dt.Rows.Clear();
                            dt.Rows.Add(0, "", 0, 0.0, 0.0, 0.0);
                            dt.Rows.Add(0, "", 0, 0.0, 0.0, 0.0);
                            dt.Rows.Add(0, "", 0, 0.0, 0.0, 0.0);
                        }
                        dgPowerSetting.DataSource = dt;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void Legs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegs.SelectedItems.Count > 0)
                        {
                            SaveCharterQuoteToSession();
                            Master.CalculateQuoteTotal();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));

                            LoadLegDetails();
                            GridEnable(true, false, true);
                            selectchangefired = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void MarkLegItemSelected(Int64 LegNum)
        {
            foreach (GridDataItem Item in dgLegs.Items)
            {
                if (Convert.ToInt64(Item["LegNum"].Text) == LegNum)
                    Item.Selected = true;
            }
        }

        protected void AddOrInsertNewLeg(Int64 LegNum, Legstate AddOrInsert)
        {
            if (QuoteInProgress != null)
            {
                switch (AddOrInsert)
                {
                    case Legstate.Add:
                        {
                            int Legscount = 1;
                            Legscount = dgLegs.Items.Count;
                            CQLeg CurrLeg = new CQLeg();
                            CurrLeg.State = CQRequestEntityState.Added;
                            CurrLeg.IsDeleted = false;
                            CurrLeg.ShowWarningMessage = true;
                            hdnShowWarnMessage.Value = "true";
                            CurrLeg.LegNUM = Legscount + 1;

                            if (QuoteInProgress != null)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                {
                                    CQLeg PrevLeg = new CQLeg();
                                    PrevLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Legscount && x.IsDeleted == false).FirstOrDefault();
                                    if (PrevLeg != null && PrevLeg.Airport != null)
                                    {
                                        CurrLeg.DAirportID = PrevLeg.Airport.AirportID;
                                        CharterQuoteService.Airport DepartAirport = new CharterQuoteService.Airport();
                                        DepartAirport.IcaoID = PrevLeg.Airport.IcaoID;
                                        DepartAirport.AirportID = PrevLeg.Airport.AirportID;
                                        DepartAirport.AirportName = PrevLeg.Airport.AirportName;
                                        CurrLeg.Airport1 = DepartAirport;
                                        CurrLeg.ArrivalAirportChanged = true;
                                        CurrLeg.DepartAirportChanged = true;

                                        #region AutoCalulateLegtimes
                                        if (PrevLeg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime dt = (DateTime)PrevLeg.ArrivalDTTMLocal;

                                            double hourtoadd = 0;
                                            if (PrevLeg.DutyTYPE != null && PrevLeg.DutyTYPE == 1)
                                                hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                            else
                                                hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                            dt = dt.AddHours(hourtoadd);
                                            CurrLeg.DepartureDTTMLocal = dt;

                                            DateTime ldGmtDep = DateTime.MinValue;
                                            DateTime ldHomDep = DateTime.MinValue;
                                            using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                            {
                                                ldGmtDep = objDstsvc.GetGMT((long)CurrLeg.DAirportID, dt, true, false);
                                                CurrLeg.DepartureGreenwichDTTM = ldGmtDep;

                                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                                {
                                                    ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ldGmtDep, false, false);
                                                    CurrLeg.HomeDepartureDTTM = ldHomDep;
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }

                            hdnLegNum.Value = CurrLeg.LegNUM.ToString();
                            if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                QuoteInProgress.State = CQRequestEntityState.Modified;

                            #region set Crew Duty Rule
                            if (UserPrincipal.Identity._fpSettings._CQCrewDuty != null)
                            {
                                if (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._CQCrewDuty))
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                        var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == UserPrincipal.Identity._fpSettings._CQCrewDuty.Trim().ToUpper()).ToList();
                                            if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                            {
                                                CurrLeg.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                CharterQuoteService.CrewDutyRule CDrule = new CrewDutyRule();
                                                CDrule.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                CDrule.CrewDutyRuleCD = CrewDtyRuleList[0].CrewDutyRuleCD;
                                                hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                                CDrule.CrewDutyRulesDescription = CrewDtyRuleList[0].CrewDutyRulesDescription;
                                                CurrLeg.FedAviationRegNUM = CrewDtyRuleList[0].FedAviatRegNum != null ? CrewDtyRuleList[0].FedAviatRegNum : string.Empty;
                                                CurrLeg.CrewDutyRule = CDrule;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            QuoteInProgress.CQLegs.Add(CurrLeg);
                            Master.BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                                dgLegs.Items[dgLegs.Items.Count - 1].Selected = true;
                        }
                        break;
                    case Legstate.Insert:
                        {
                            CQLeg CurrLeg = new CQLeg();
                            CurrLeg.State = CQRequestEntityState.Added;
                            CurrLeg.ShowWarningMessage = true;
                            hdnShowWarnMessage.Value = "true";
                            CurrLeg.IsDeleted = false;
                            CurrLeg.LegNUM = Convert.ToInt16(hdnLegNum.Value.ToString()) < 1 ? 1 : Convert.ToInt16(hdnLegNum.Value.ToString());
                            hdnLegNum.Value = CurrLeg.LegNUM.ToString();

                            if (QuoteInProgress != null)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                {
                                    if (Convert.ToInt16(hdnLegNum.Value.ToString()) > 1)
                                    {
                                        CQLeg PrevLeg = new CQLeg();
                                        PrevLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt16(hdnLegNum.Value.ToString()) - 1 && x.IsDeleted == false).FirstOrDefault();
                                        if (PrevLeg != null && PrevLeg.Airport != null)
                                        {
                                            CurrLeg.DAirportID = PrevLeg.Airport.AirportID;
                                            CharterQuoteService.Airport DepartAirport = new CharterQuoteService.Airport();
                                            DepartAirport.AirportID = PrevLeg.Airport.AirportID;
                                            DepartAirport.IcaoID = PrevLeg.Airport.IcaoID;
                                            DepartAirport.AirportName = PrevLeg.Airport.AirportName;
                                            CurrLeg.Airport1 = DepartAirport;
                                            CurrLeg.ArrivalAirportChanged = true;
                                            CurrLeg.DepartAirportChanged = true;
                                            #region AutoCalulateLegtimes
                                            if (PrevLeg.ArrivalDTTMLocal != null)
                                            {
                                                DateTime dt = (DateTime)PrevLeg.ArrivalDTTMLocal;
                                                double hourtoadd = 0;
                                                if (PrevLeg.DutyTYPE != null && PrevLeg.DutyTYPE == 1)
                                                    hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                                else
                                                    hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                                dt = dt.AddHours(hourtoadd);
                                                CurrLeg.DepartureDTTMLocal = dt;
                                                DateTime ldGmtDep = DateTime.MinValue;
                                                DateTime ldHomDep = DateTime.MinValue;
                                                using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                                {
                                                    ldGmtDep = objDstsvc.GetGMT((long)CurrLeg.DAirportID, dt, true, false);
                                                    CurrLeg.DepartureGreenwichDTTM = ldGmtDep;

                                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                                    {
                                                        ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ldGmtDep, false, false);
                                                        CurrLeg.HomeDepartureDTTM = ldHomDep;
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        CQLeg NextLeg = new CQLeg();
                                        NextLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == (Convert.ToInt16(hdnLegNum.Value.ToString()) + 1) && x.IsDeleted == false).FirstOrDefault();
                                        if (NextLeg != null && NextLeg.Airport1 != null)
                                        {
                                            CurrLeg.DAirportID = NextLeg.Airport1.AirportID;
                                            CharterQuoteService.Airport ArrivalAirport = new CharterQuoteService.Airport();
                                            ArrivalAirport.AirportID = NextLeg.Airport1.AirportID;
                                            ArrivalAirport.IcaoID = NextLeg.Airport1.IcaoID;
                                            ArrivalAirport.AirportName = NextLeg.Airport1.AirportName;
                                            CurrLeg.Airport = ArrivalAirport;
                                            CurrLeg.ArrivalAirportChanged = true;
                                            CurrLeg.DepartAirportChanged = true;

                                            #region "Auto Calculate Leg times"
                                            if (NextLeg.DepartureDTTMLocal != null)
                                            {
                                                DateTime dt = (DateTime)NextLeg.DepartureDTTMLocal;
                                                
                                                double hourtoadd = 0;
                                                if (NextLeg.DutyTYPE != null && NextLeg.DutyTYPE == 1)
                                                    hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                                else
                                                    hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                                dt = dt.AddHours(-1 * hourtoadd);
                                                CurrLeg.ArrivalDTTMLocal = dt;

                                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                                {
                                                    DateTime ldGmtArr = DateTime.MinValue;
                                                    DateTime ldHomArr = DateTime.MinValue;
                                                    using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                                    {
                                                        ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, false);
                                                        CurrLeg.ArrivalGreenwichDTTM = ldGmtArr;
                                                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                                        {
                                                            ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ldGmtArr, false, false);
                                                            CurrLeg.HomeArrivalDTTM = ldHomArr;
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }

                            if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                QuoteInProgress.State = CQRequestEntityState.Modified;

                            #region set Crew Duty Rule
                            if (UserPrincipal.Identity._fpSettings._CQCrewDuty != null)
                            {
                                if (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._CQCrewDuty))
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                        var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == UserPrincipal.Identity._fpSettings._CQCrewDuty.Trim().ToUpper()).ToList();
                                            if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                            {
                                                CurrLeg.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                CharterQuoteService.CrewDutyRule CDrule = new CrewDutyRule();
                                                CDrule.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                CDrule.CrewDutyRuleCD = CrewDtyRuleList[0].CrewDutyRuleCD;
                                                hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                                CDrule.CrewDutyRulesDescription = CrewDtyRuleList[0].CrewDutyRulesDescription;
                                                CurrLeg.FedAviationRegNUM = CrewDtyRuleList[0].FedAviatRegNum != null ? CrewDtyRuleList[0].FedAviatRegNum : string.Empty;
                                                CurrLeg.CrewDutyRule = CDrule;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            QuoteInProgress.CQLegs.Add(CurrLeg);
                            Master.BindLegs(dgLegs, true);
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        break;
                }

                Master.SaveQuoteinProgressToFileSession();
                LoadLegDetails();
            }
        }

        protected void UpdateLegnum(Legstate State)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<CQLeg> CqLegs = new List<CQLeg>();
                CqLegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                switch (State)
                {
                    case Legstate.Add:
                        break;
                    case Legstate.Insert:
                        {
                            if (CqLegs != null && CqLegs.Count > 0)
                            {
                                foreach (CQLeg Leg in CqLegs)
                                {
                                    if (Leg.LegNUM >= Convert.ToInt64(hdnLegNum.Value))
                                    {
                                        Leg.LegNUM = Leg.LegNUM + 1;
                                        if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                            Leg.State = CQRequestEntityState.Modified;
                                    }
                                }
                            }
                        }
                        break;
                    case Legstate.Delete:
                        {
                            Int64 DeletedLegnum = 0;
                            DeletedLegnum = Convert.ToInt64(hdnLegNum.Value);
                            if (DeletedLegnum != CqLegs.Count)
                            {
                                foreach (CQLeg Leg in CqLegs)
                                {
                                    if (Leg.LegNUM > DeletedLegnum)
                                    {
                                        Leg.LegNUM = Leg.LegNUM - 1;
                                        if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                            Leg.State = CQRequestEntityState.Modified;
                                    }
                                }
                            }
                            else
                                hdnLegNum.Value = Convert.ToString(DeletedLegnum - 1);
                        }
                        break;
                }
            }
        }

        protected void DeleteLeg(Int64 LegNum)
        {
            if (QuoteInProgress != null)
            {
                if (QuoteInProgress.CQLegs != null)
                {
                    CQLeg LegToDelete = new CQLeg();
                    if (QuoteInProgress.CQLegs.Count > 0)
                    {
                        LegToDelete = QuoteInProgress.CQLegs.Where(x => x.LegNUM == LegNum && x.IsDeleted == false).FirstOrDefault();

                        if (LegToDelete != null)
                        {
                            UpdateLegnum(Legstate.Delete);
                            if (LegToDelete.CQLegID != 0)
                            {
                                LegToDelete.IsDeleted = true;
                                LegToDelete.State = CQRequestEntityState.Deleted;
                            }
                            else
                                QuoteInProgress.CQLegs.Remove(LegToDelete);
                        }
                    }

                    Master.BindLegs(dgLegs, true);

                    if (dgLegs.Items.Count > 0)
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }

                Master.SaveQuoteinProgressToFileSession();
                Master.CalculateFAR();
                Master.CalculateQuantity();
                Master.CalculateQuoteTotal();

                if (dgLegs.Items.Count > 0)
                    LoadLegDetails();
                else
                    ClearFields();
            }
        }

        protected void Legs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["DepartDate"] != null && !string.IsNullOrEmpty(item["DepartDate"].Text) && item["DepartDate"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                                item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }

                            if (item["ArrivalDate"] != null && !string.IsNullOrEmpty(item["ArrivalDate"].Text) && item["ArrivalDate"].Text != "&nbsp;")
                            {
                                DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                                item["ArrivalDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date1));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void Legs_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void Legs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                break;
                            case RadGrid.DeleteSelectedCommandName:
                                break;
                            case "RowClick":
                                GridDataItem item = (GridDataItem)e.Item;
                                if (!selectchangefired)
                                {
                                    SaveCharterQuoteToSession();
                                    Master.CalculateQuoteTotal();
                                    Master.BindStandardCharges(true, dgStandardCharges);
                                }
                                setWarnMessageToDefault();
                                Master.BindLegs(dgLegs, true);
                                MarkLegItemSelected(Convert.ToInt64(item["LegNUM"].Text));
                                LoadLegDetails();
                                break;
                            default:
                                break;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btnAddLeg_Click(object sender, EventArgs e)
        {
            SaveCharterQuoteToSession();
            Master.CalculateQuoteTotal();
            Master.BindStandardCharges(true, dgStandardCharges);
            Master.BindLegs(dgLegs, true);
            if (dgLegs.Items.Count > 0)
                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));

            LoadLegDetails();
            dgLegs.SelectedIndexes.Clear();
            ClearFields();
            EnableForm(true);
            GridEnable(true, false, false);
            AddOrInsertNewLeg(Convert.ToInt64(hdnLegNum.Value), Legstate.Add);
            setWarnMessageToDefault();
            Master.RadAjaxManagerMaster.FocusControl(tbArrival.ClientID);

            if (string.IsNullOrEmpty(tbDepart.Text))
                tbDepart.Focus();
            else if (string.IsNullOrEmpty(tbArrival.Text))
                tbArrival.Focus();
        }

        protected void btnInsertLeg_Click(object sender, EventArgs e)
        {
            SaveCharterQuoteToSession();
            Master.CalculateQuoteTotal();
            Master.BindStandardCharges(true, dgStandardCharges);
            Master.BindLegs(dgLegs, true);
            if (dgLegs.Items.Count > 0)
                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));

            LoadLegDetails();
            dgLegs.SelectedIndexes.Clear();
            ClearFields();
            EnableForm(true);
            GridEnable(true, false, false);
            UpdateLegnum(Legstate.Insert);
            AddOrInsertNewLeg(Convert.ToInt64(hdnLegNum.Value), Legstate.Insert);
            setWarnMessageToDefault();
            Master.RadAjaxManagerMaster.FocusControl(tbArrival.ClientID);

            if (string.IsNullOrEmpty(tbDepart.Text))
                tbDepart.Focus();
            else if (string.IsNullOrEmpty(tbArrival.Text))
                tbArrival.Focus();
        }

        protected void btnDeleteLeg_Click(object sender, EventArgs e)
        {
            if (dgLegs.Items.Count > 0)
            {
                if (dgLegs.SelectedItems.Count > 0)
                    RadWindowManager1.RadConfirm("Are you sure you want to delete this Leg", "confirmDeleteLegCallBackFn", 330, 100, null, "Delete Leg");
                else
                    RadWindowManager1.RadAlert("Please select the record", 250, 100, "Delete Leg", "", "");
            }
        }

        protected void btndeletelegYes_Click(object sender, EventArgs e)
        {
            SaveCharterQuoteToSession();
            DeleteLeg(Convert.ToInt64(hdnLegNum.Value));
            Master.CalculateQuoteTotal();
            Master.BindStandardCharges(true, dgStandardCharges);
            Master.BindLegs(dgLegs, true);
            if (dgLegs.Items.Count > 0)
                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
            LoadLegDetails();
        }

        protected void btndeletelegNo_Click(object sender, EventArgs e)
        {
            //To Do
        }

        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {
            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Miles * 1.852M, 0);
            else
                return Miles;
        }

        protected decimal ConvertToMilesBasedOnCompanyProfile(decimal Kilometers)
        {
            if (UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Kilometers / 1.852M, 0);
            else
                return Kilometers;
        }

        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "0.0";
                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();
                            if (standardList.Count > 0)
                                result = Convert.ToString(hour + standardList[0].Tenths);
                        }
                    }
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }
        }

        protected string directMinstoTenths(string time)
        {
            string result = "0.0";

            if (time.IndexOf(":") != -1)
            {
                string[] timeArray = time.Split(':');
                decimal hour = Convert.ToDecimal(timeArray[0]);
                decimal minute = Convert.ToDecimal(timeArray[1]);

                decimal decimalOfMin = 0;
                if (minute > 0) // Added conditon to avoid divide by zero error.
                    decimalOfMin = minute / 60;

                result = Convert.ToString(hour + decimalOfMin);
            }
            return result;
        }

        protected bool ValidateTenthMinBasedonCompanyprofile(TextBox cntrlToset)
        {
            bool ValidationSucceeded = true;
            string ValtoCheck = cntrlToset.Text;
            decimal companyprofileSetting = 1;

            companyprofileSetting = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? 1 : (decimal)UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
            if (companyprofileSetting == 1)
            {
                var count = 0;
                var strtext = cntrlToset.Text;
                for (var i = 0; i < strtext.Length; ++i)
                {
                    if (strtext[i] == '.')
                        count++;
                }
                if (count > 1)
                {
                    RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                    ValtoCheck = "0.0";
                    ValidationSucceeded = false;
                }
                if (ValtoCheck.Length > 0)
                {
                    if (ValtoCheck.IndexOf(".") >= 0)
                    {
                        string[] strarr = ValtoCheck.Split('.');
                        if (strarr[0].Length > 2 || strarr[1].Length > 1)
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                            ValtoCheck = "0.0";
                            ValidationSucceeded = false;
                        }
                    }
                    else
                    {
                        if (ValtoCheck.Length > 2)
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                            ValtoCheck = "0.0";
                            ValidationSucceeded = false;
                        }
                        else
                            ValtoCheck = ValtoCheck + ".0";
                    }
                }
            }
            else
            {
                var hourvalue = "00";
                var minvalue = "00";
                if (ValtoCheck.Length > 0)
                {
                    if (ValtoCheck.IndexOf(":") >= 0)
                    {
                        var strarr = ValtoCheck.Split(':');
                        if (strarr[0].Length <= 2)
                        {
                            hourvalue = "00" + strarr[0];
                            hourvalue = hourvalue.Substring(hourvalue.Length - 2, 2);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                            ValtoCheck = "00:00";
                            ValidationSucceeded = false;
                        }

                        if (ValidationSucceeded)
                        {
                            if (strarr[1].Length <= 2)
                            {
                                if (Convert.ToInt16(strarr[1]) > 59)
                                {
                                    RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                    minvalue = hourvalue = "00";
                                    ValidationSucceeded = false;
                                }
                                else
                                {
                                    minvalue = "00" + strarr[1];
                                    minvalue = minvalue.Substring(minvalue.Length - 2, 2);
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                            ValtoCheck = "00:00";
                            ValidationSucceeded = false;
                        }
                    }
                    else
                    {
                        if (ValtoCheck.Length <= 2)
                        {
                            hourvalue = "00" + ValtoCheck;
                            hourvalue = hourvalue.Substring(hourvalue.Length - 2, 2);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                            ValtoCheck = "00:00";
                            ValidationSucceeded = false;
                        }
                    }
                }

                ValtoCheck = hourvalue + ":" + minvalue;
            }

            cntrlToset.Text = ValtoCheck;
            return ValidationSucceeded;
        }

        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });

                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });

                return conversionList;
            }
        }

        protected void ResetHomebaseSettings()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdShowRequestor.Value = "false";
                hdHighlightTailno.Value = "false";
                hdIsDepartAuthReq.Value = "false";
                hdSetFarRules.Value = "";
                hdSetCrewRules.Value = "false";
                hdHomeCategory.Value = "";
            }
        }

        protected void SetWindReliability()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdSetWindReliability.Value))
                    this.rblistWindReliability.SelectedValue = UserPrincipal.Identity._fpSettings._WindReliability == null ? "3" : UserPrincipal.Identity._fpSettings._WindReliability.ToString();
                else
                    rblistWindReliability.SelectedValue = "3";
            }
        }

        protected void setAircraftSettings(ref CQLeg Leg)
        {
            if (QuoteInProgress.AircraftID != null)
                hdnAircraft.Value = QuoteInProgress.AircraftID.ToString();

            Leg.PowerSetting = "1";

            Int64 AircraftID = Convert.ToInt64(hdnAircraft.Value == string.Empty ? "0" : hdnAircraft.Value);
            FlightPakMasterService.Aircraft Aircraft = Master.GetAircraft(AircraftID);
            
            if (Aircraft != null)
            {
                switch (Leg.PowerSetting)
                {
                    case "1":
                        Leg.TrueAirSpeed = Aircraft.PowerSettings1TrueAirSpeed != null ? (decimal)Aircraft.PowerSettings1TrueAirSpeed : 0.0M;
                        Leg.TakeoffBIAS = Aircraft.PowerSettings1TakeOffBias != null ? (decimal)Aircraft.PowerSettings1TakeOffBias : 0.0M;
                        Leg.LandingBIAS = Aircraft.PowerSettings1LandingBias != null ? (decimal)Aircraft.PowerSettings1LandingBias : 0.0M;
                        break;
                    case "2":
                        Leg.TrueAirSpeed = Aircraft.PowerSettings2TrueAirSpeed != null ? (decimal)Aircraft.PowerSettings2TrueAirSpeed : 0.0M;
                        Leg.TakeoffBIAS = Aircraft.PowerSettings2TakeOffBias != null ? (decimal)Aircraft.PowerSettings2TakeOffBias : 0.0M;
                        Leg.LandingBIAS = Aircraft.PowerSettings2LandingBias != null ? (decimal)Aircraft.PowerSettings2LandingBias : 0.0M;
                        break;
                    case "3":
                        Leg.TrueAirSpeed = Aircraft.PowerSettings3TrueAirSpeed != null ? (decimal)Aircraft.PowerSettings3TrueAirSpeed : 0.0M;
                        Leg.TakeoffBIAS = Aircraft.PowerSettings3TakeOffBias != null ? (decimal)Aircraft.PowerSettings3TakeOffBias : 0.0M;
                        Leg.LandingBIAS = Aircraft.PowerSettings3LandingBias != null ? (decimal)Aircraft.PowerSettings3LandingBias : 0.0M;
                        break;
                    default:
                        Leg.TrueAirSpeed = 0.0M;
                        Leg.TakeoffBIAS = 0.0M;
                        Leg.LandingBIAS = 0.0M;
                        break;
                }
            }
        }

        protected void setAircraftSettings()
        {
            if (QuoteInProgress.AircraftID != null)
                hdnAircraft.Value = QuoteInProgress.AircraftID.ToString();

            tbPower.Text = "1";
            tbTAS.Text = "0.0";

            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                tbBias.Text = tbLandBias.Text = "00:00";
            else
                tbBias.Text = tbLandBias.Text = "0.0";
            
            Int64 AircraftID = Convert.ToInt64(hdnAircraft.Value == string.Empty ? "0" : hdnAircraft.Value);
            FlightPakMasterService.Aircraft Aircraft = Master.GetAircraft(AircraftID);
            
            if (Aircraft != null)
            {

                tbPower.Text = Aircraft.PowerSetting ?? "1";

                switch (tbPower.Text)
                {
                    case "1":
                        tbTAS.Text = Aircraft.PowerSettings1TrueAirSpeed != null ? Aircraft.PowerSettings1TrueAirSpeed.ToString() : "0.0";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbBias.Text = Aircraft.PowerSettings1TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString()) : "00:00";
                            tbLandBias.Text = Aircraft.PowerSettings1LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString()) : "00:00";
                        }
                        else
                        {
                            tbBias.Text = Aircraft.PowerSettings1TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString() : "0.0";
                            tbLandBias.Text = Aircraft.PowerSettings1LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString() : "0.0";
                        }
                        break;
                    case "2":
                        tbTAS.Text = Aircraft.PowerSettings2TrueAirSpeed != null ? Aircraft.PowerSettings2TrueAirSpeed.ToString() : "0.0";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbBias.Text = Aircraft.PowerSettings2TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString()) : "00:00";
                            tbLandBias.Text = Aircraft.PowerSettings2LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString()) : "00:00";
                        }
                        else
                        {
                            tbBias.Text = Aircraft.PowerSettings2TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString() : "0.0";
                            tbLandBias.Text = Aircraft.PowerSettings2LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString() : "0.0";
                        }
                        break;
                    case "3":
                        tbTAS.Text = Aircraft.PowerSettings3TrueAirSpeed != null ? Aircraft.PowerSettings3TrueAirSpeed.ToString() : "0";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbBias.Text = Aircraft.PowerSettings3TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString()) : "00:00";
                            tbLandBias.Text = Aircraft.PowerSettings3LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString()) : "00:00";
                        }
                        else
                        {
                            tbBias.Text = Aircraft.PowerSettings3TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString() : "0.0";
                            tbLandBias.Text = Aircraft.PowerSettings3LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString() : "0.0";
                        }
                        break;
                    default:
                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode("Power Setting must be 1, 2, or 3");
                        tbPower.Text = "1";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbLandBias.Text = "00:00";
                            tbBias.Text = "00:00";
                        }
                        else
                        {
                            tbBias.Text = "0.0";
                            tbLandBias.Text = "0.0";
                        }
                        tbTAS.Text = "0.0";
                        break;
                }
                // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(tbBias.Text))
                    {
                        if (tbBias.Text.IndexOf(".") < 0)
                            tbBias.Text = tbBias.Text + ".0";
                    }

                    if (!string.IsNullOrEmpty(tbLandBias.Text))
                    {
                        if (tbLandBias.Text.IndexOf(".") < 0)
                            tbLandBias.Text = tbLandBias.Text + ".0";
                    }
                }
            }

        }

        protected void SetCrewDutyTypeAndFARRules()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdnCrewRules.Value))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                        var objRetVal = objDstsvc.GetCrewDutyRuleList();
                        if (objRetVal.ReturnFlag)
                        {
                            CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRulesID.ToString().ToUpper().Trim().Equals(hdnCrewRules.Value.Trim())).ToList();
                            if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                            {
                                tbCrewRules.Text = CrewDtyRuleList[0].CrewDutyRuleCD == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRuleCD;
                                hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                if (CrewDtyRuleList[0].FedAviatRegNum != null)
                                    this.lbFar.Text = CrewDtyRuleList[0].FedAviatRegNum == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(CrewDtyRuleList[0].FedAviatRegNum.ToString());
                            }
                            else
                            {
                                tbCrewRules.Text = string.Empty;
                                this.lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                hdnCrewRules.Value = string.Empty;
                            }
                        }
                        else
                        {
                            tbCrewRules.Text = string.Empty;
                            this.lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnCrewRules.Value = string.Empty;
                        }
                    }
                }
            }
        }

        private void SetChangedCrewDutyRuleforOtherLegs()
        {
            if (QuoteInProgress != null)
            {
                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                {
                    Int64 Legnum = 0;
                    Legnum = Convert.ToInt64(hdnLegNum.Value);
                    List<CQLeg> PrefLegs = new List<CQLeg>();

                    PrefLegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    //Update previous legs
                    for (int i = (int)Legnum - 1; i >= 0; i--)
                    {
                        CQLeg LegtoUpdate = new CQLeg();
                        LegtoUpdate = (from Leg in PrefLegs
                                       where Leg.LegNUM == i
                                       select Leg).SingleOrDefault();

                        if (LegtoUpdate != null && ((LegtoUpdate.IsDutyEnd != null && !(bool)LegtoUpdate.IsDutyEnd) || LegtoUpdate.IsDutyEnd == null))
                        {
                            if (LegtoUpdate.CQLegID != 0 && LegtoUpdate.State != CQRequestEntityState.Deleted)
                                LegtoUpdate.State = CQRequestEntityState.Modified;

                            CharterQuoteService.CrewDutyRule CrewDtyRule = new CharterQuoteService.CrewDutyRule();
                            if (!string.IsNullOrEmpty(hdnCrewRules.Value) && hdnCrewRules.Value != "0")
                            {
                                Int64 CrewDutyRulesID = 0;
                                if (Int64.TryParse(hdnCrewRules.Value, out CrewDutyRulesID))
                                {
                                    CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                    CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                    CrewDtyRule.CrewDutyRulesDescription = " ";
                                    LegtoUpdate.CrewDutyRule = CrewDtyRule;
                                    LegtoUpdate.CrewDutyRulesID = CrewDutyRulesID;
                                }
                                else
                                {
                                    LegtoUpdate.CrewDutyRule = null;
                                    LegtoUpdate.CrewDutyRulesID = null;
                                }
                            }
                            LegtoUpdate.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(lbFar.Text);
                        }
                        else
                            break;

                    }

                    //Update Next legs
                    if (!chkEndOfDuty.Checked)
                    {
                        for (int i = (int)Legnum + 1; i <= PrefLegs.Count; i++)
                        {
                            CQLeg LegtoUpdate = new CQLeg();
                            LegtoUpdate = (from Leg in PrefLegs
                                           where Leg.LegNUM == i
                                           select Leg).SingleOrDefault();

                            if (LegtoUpdate != null)
                            {
                                if (LegtoUpdate.CQLegID != 0 && LegtoUpdate.State != CQRequestEntityState.Deleted)
                                    LegtoUpdate.State = CQRequestEntityState.Modified;

                                CharterQuoteService.CrewDutyRule CrewDtyRule = new CharterQuoteService.CrewDutyRule();
                                if (!string.IsNullOrEmpty(hdnCrewRules.Value) && hdnCrewRules.Value != "0")
                                {
                                    Int64 CrewDutyRulesID = 0;
                                    if (Int64.TryParse(hdnCrewRules.Value, out CrewDutyRulesID))
                                    {
                                        CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                        CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                        CrewDtyRule.CrewDutyRulesDescription = " ";
                                        LegtoUpdate.CrewDutyRule = CrewDtyRule;
                                        LegtoUpdate.CrewDutyRulesID = CrewDutyRulesID;
                                    }
                                    else
                                    {
                                        LegtoUpdate.CrewDutyRule = null;
                                        LegtoUpdate.CrewDutyRulesID = null;
                                    }
                                }
                                LegtoUpdate.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(lbFar.Text);
                                if (LegtoUpdate.IsDutyEnd != null && (bool)LegtoUpdate.IsDutyEnd)
                                    break;
                            }
                        }
                    }
                }
                Master.SaveQuoteinProgressToFileSession();
            }
        }

        private void ClearAllDatesBasedonDeptOrArrival(bool isDepartureConfirmed)
        {
            if (isDepartureConfirmed)
            {
                if (!string.IsNullOrEmpty(hdnDepart.Value))
                {
                    if (!string.IsNullOrEmpty(tbUtcDate.Text))
                    {
                        #region Calculation based on UTC Departure Date
                        DateTime DeptUTCdt = new DateTime();
                        DateTime ldLocDep = new DateTime();
                        string StartTime = rmtUtctime.Text.Trim();
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        DeptUTCdt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        DeptUTCdt = DeptUTCdt.AddHours(StartHrs);
                        DeptUTCdt = DeptUTCdt.AddMinutes(StartMts);
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            ldLocDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), DeptUTCdt, false, false);
                            
                            string startHrs = "0000" + ldLocDep.Hour.ToString();
                            string startMins = "0000" + ldLocDep.Minute.ToString();
                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldLocDep);
                            rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                            tbHomeDate.Text = string.Empty;
                            rmtHomeTime.Text = "00:00";

                            tbArrivalHomeDate.Text = string.Empty;
                            rmtArrivalHomeTime.Text = "00:00";
                            tbArrivalDate.Text = string.Empty;
                            rmtArrivalTime.Text = "00:00";
                            tbArrivalUtcDate.Text = string.Empty;
                            rmtArrivalUtctime.Text = "00:00";
                        }
                        #endregion
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbLocalDate.Text))
                        {
                            #region calculation based on Local Departure Date
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime dt = new DateTime();
                                DateTime ldGmtDep = new DateTime();
                                string StartTime = rmtlocaltime.Text.Trim();
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                dt = DateTime.ParseExact(tbLocalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                //GMTDept
                                ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), dt, true, false);

                                string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                string startMins = "0000" + ldGmtDep.Minute.ToString();
                                tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                tbHomeDate.Text = string.Empty;
                                rmtHomeTime.Text = "00:00";

                                tbArrivalHomeDate.Text = string.Empty;
                                rmtArrivalHomeTime.Text = "00:00";
                                tbArrivalDate.Text = string.Empty;
                                rmtArrivalTime.Text = "00:00";
                                tbArrivalUtcDate.Text = string.Empty;
                                rmtArrivalUtctime.Text = "00:00";

                            }
                            #endregion
                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(hdnArrival.Value))
                {
                    if (!string.IsNullOrEmpty(tbArrivalUtcDate.Text))
                    {
                        #region Calculation based on Arrival UTC Date
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime ArrUTCdt = new DateTime();
                            DateTime ldLocArr = new DateTime();
                            string StartTime = rmtArrivalUtctime.Text.Trim();
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                            ArrUTCdt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            ArrUTCdt = ArrUTCdt.AddHours(StartHrs);
                            ArrUTCdt = ArrUTCdt.AddMinutes(StartMts);
                            
                            ldLocArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), ArrUTCdt, false, false);
                            string startHrs = "0000" + ldLocArr.Hour.ToString();
                            string startMins = "0000" + ldLocArr.Minute.ToString();
                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldLocArr);
                            rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                            tbArrivalHomeDate.Text = string.Empty;
                            rmtArrivalHomeTime.Text = "00:00";

                            tbHomeDate.Text = string.Empty;
                            rmtHomeTime.Text = "00:00";

                            tbLocalDate.Text = string.Empty;
                            rmtlocaltime.Text = "00:00";
                            tbUtcDate.Text = string.Empty;
                            rmtUtctime.Text = "00:00";
                        }
                        #endregion
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            #region Calculation based on Local Arrival Date
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime dt = new DateTime();
                                DateTime ldGmtArr = new DateTime();
                                string StartTime = rmtArrivalTime.Text.Trim();
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                dt = DateTime.ParseExact(tbArrivalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                //GMTDept
                                ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, false);

                                string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                string startMins = "0000" + ldGmtArr.Minute.ToString();
                                tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                tbArrivalHomeDate.Text = string.Empty;
                                rmtArrivalHomeTime.Text = "00:00";

                                tbHomeDate.Text = string.Empty;
                                rmtHomeTime.Text = "00:00";

                                tbLocalDate.Text = string.Empty;
                                rmtlocaltime.Text = "00:00";
                                tbUtcDate.Text = string.Empty;
                                rmtUtctime.Text = "00:00";
                            }
                            #endregion
                        }
                    }
                }
            }
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl, lnkInsert;
                    insertCtl = (LinkButton)dgLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    lnkInsert = (LinkButton)dgLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInsert");
                    delCtl = (LinkButton)dgLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");

                    // For Fleet Charge Details Grid
                    LinkButton lnkInitInsert = new LinkButton();
                    LinkButton lnkDelete = new LinkButton();

                    //Fix for Index was outside the bounds of the array.
                    if (dgStandardCharges.Items.Count > 0)
                    {
                        lnkInitInsert = (LinkButton)dgStandardCharges.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                        lnkDelete = (LinkButton)dgStandardCharges.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    }

                    if (IsAuthorized(Permission.CharterQuote.AddCQLeg))
                    {
                        insertCtl.Visible = false;
                        lnkInsert.Visible = false;

                        lnkInitInsert.Visible = false;

                        if (add)
                        {
                            insertCtl.Enabled = false;
                            lnkInsert.Enabled = false;

                            lnkInitInsert.Enabled = true;
                            lnkInitInsert.Visible = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                            lnkInsert.Enabled = false;

                            lnkInitInsert.Enabled = false;
                            lnkInitInsert.Visible = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                        lnkInsert.Visible = false;

                        lnkInitInsert.Visible = false;
                    }

                    if (IsAuthorized(Permission.CharterQuote.DeleteCQLeg))
                    {
                        delCtl.Visible = false;

                        lnkDelete.Visible = false;
                        if (delete)
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";

                            lnkDelete.Enabled = true;
                            lnkDelete.Visible = true;
                            lnkDelete.OnClientClick = "javascript:return fnDeleteFleetCharge();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;

                            lnkDelete.Enabled = false;
                            lnkDelete.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                        lnkDelete.Visible = false;
                    }
                    
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected string RounsETEbasedOnCompanyProfileSettings(decimal ETEVal)
        {
            string ETEStr = string.Empty;

            try
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null &&
                        UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        ETEStr = ConvertTenthsToMins(RoundElpTime((double) ETEVal).ToString());
                    }
                    else
                    {
                        ETEStr = Math.Round((decimal) ETEVal, 1).ToString();
                    }

                    ETEStr = Common.Utility.DefaultEteResult(ETEStr);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            catch (Exception ex)
            {
                //The exception will be handled, logged and replaced by our custom exception. 
                ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
            }

            return ETEStr;
        }

        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbAdditionalFees.Text = "0.00";
                tbAdjustedHrs.Text = string.Empty;
                tbArrival.Text = string.Empty;
                tbArrivalDate.Text = string.Empty;
                tbArrivalHomeDate.Text = string.Empty;
                tbArrivalUtcDate.Text = string.Empty;
                tbAvgHrs.Text = string.Empty;
                tbBias.Text = "00:00";
                tbCharges.Text = "0.00";
                tbCrewRules.Text = string.Empty;
                tbDayRoom.Text = string.Empty;
                tbDepart.Text = string.Empty;
                tbDiscount.Text = string.Empty;
                tbDiscountAmount.Text = string.Empty;
                tbDomStdCrew.Text = string.Empty;
                tbETE.Text = string.Empty;
                tbFeeGroup.Text = string.Empty;
                tbFlightCharges.Text = string.Empty;
                tbFltHrs.Text = "0.00";
                tbHomeDate.Text = string.Empty;
                tbIntlStdCrew.Text = string.Empty;
                tbLandBias.Text = "00:00";
                tbLandingFees.Text = string.Empty;
                tbLocalDate.Text = string.Empty;
                tbMargin.Text = string.Empty;
                tbMarginAmount.Text = string.Empty;
                tbMiles.Text = string.Empty;
                tbOverride.Text = string.Empty;
                tbPower.Text = string.Empty;
                tbPurpose.Text = string.Empty;
                tbRate.Text = "0.00";
                tbRON.Text = string.Empty;
                tbSegmentFees.Text = string.Empty;
                tbSubTotal.Text = string.Empty;
                tbTAS.Text = string.Empty;
                tbTaxes.Text = string.Empty;
                tbPaxTotal.Text = "0";
                tbTotalCost.Text = string.Empty;
                tbTotDays.Text = "0.00";
                tbTotPrepDeposit.Text = string.Empty;
                tbUsageAdj.Text = string.Empty;
                tbUtcDate.Text = string.Empty;
                tbWind.Text = string.Empty;
                rmtArrivalTime.Text = "00:00";
                rmtArrivalHomeTime.Text = "00:00";
                rmtArrivalUtctime.Text = "00:00";
                rmtHomeTime.Text = "00:00";
                rmtlocaltime.Text = "00:00";
                rmtUtctime.Text = "00:00";
                rbDomestic.ClearSelection();
                rblistWindReliability.ClearSelection();
                chkList.ClearSelection();
                chkEndOfDuty.Checked = false;
                chkTaxDaily.Checked = false;
                chkTaxLandingFee.Checked = false;
                hdnMiles.Value = "0";

                //Fix for SUP-380 (3174)
                hdnDutyDayStart.Value = "0";
                hdnDutyDayEnd.Value = "0";
                hdnMaxDutyHrsAllowed.Value = "0";
                hdnMaxFlightHrsAllowed.Value = "0";
                hdnMinFixedRest.Value = "0";
                tbCrewRules.ToolTip = string.Empty;
                lbCrewRules.ToolTip = string.Empty;
                lbCrewRulesCaption.ToolTip = string.Empty;
            }
        }

        private void EnableForm(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                tbAdditionalFees.Enabled = status;
                tbAdjustedHrs.Enabled = status;
                IsEditMode.Value = Convert.ToString(status);
                // tbArrival.Enabled = status;
                tbArrivalDate.Enabled = status;
                tbArrivalHomeDate.Enabled = status;
                tbArrivalUtcDate.Enabled = status;
                tbAvgHrs.Enabled = status;
                tbBias.Enabled = status;
                tbCharges.Enabled = status;
                tbCrewRules.Enabled = status;
                tbDayRoom.Enabled = status;
                tbDepart.ReadOnly = !status;
                tbArrival.ReadOnly = !status;
                if (!status)
                {
                    tbDepart.BackColor = System.Drawing.ColorTranslator.FromHtml("#EFEEEC");
                    tbArrival.BackColor = System.Drawing.ColorTranslator.FromHtml("#EFEEEC");
                }
                else
                {
                    tbDepart.BackColor = Color.White;
                    tbArrival.BackColor = Color.White;
                }
                tbTaxRate.BackColor = System.Drawing.ColorTranslator.FromHtml("#EFEEEC");
                // tbDepart.Enabled = status;
                tbDomStdCrew.Enabled = status;
                tbETE.Enabled = status;
                tbFeeGroup.Enabled = status;
                tbFlightCharges.Enabled = status;
                tbFltHrs.Enabled = status;
                tbHomeDate.Enabled = status;
                tbIntlStdCrew.Enabled = status;
                tbLandBias.Enabled = status;
                tbLocalDate.Enabled = status;
                tbMargin.Enabled = status;
                tbMarginAmount.Enabled = status;
                tbMiles.Enabled = status;
                tbOverride.Enabled = status;
                tbPower.Enabled = status;
                tbPurpose.Enabled = status;
                tbRate.Enabled = status;
                tbRON.Enabled = status;
                tbSubTotal.Enabled = status;
                tbTAS.Enabled = status;
                tbTotDays.Enabled = status;
                tbTotPrepDeposit.Enabled = status;
                tbUtcDate.Enabled = status;
                tbWind.Enabled = status;
                lbAdditionalFees.Enabled = status;
                lbAdjustedHours.Enabled = status;
                lbArrival.Enabled = status;
                lbArrive.Enabled = status;
                lbAvgFlightHrs.Enabled = status;
                lbcvArrival.Enabled = status;
                lbcvCrewRules.Enabled = status;
                lbcvDepart.Enabled = status;
                lbcvPower.Enabled = status;
                lbDepart.Enabled = status;
                lbDepartIcao.Enabled = status;
                lbDiscount.Enabled = status;
                lbDiscountAmount.Enabled = status;
                lbDomStdCrew.Enabled = status;
                lbFar.Enabled = status;
                lbFeeGroup.Enabled = status;
                lbFlightCharges.Enabled = status;
                lbflightHrs.Enabled = status;
                lbIntlStdCrew.Enabled = status;
                lbLandingFees.Enabled = status;
                lbMargin.Enabled = status;
                lbMarginAmount.Enabled = status;
                lbRest.Enabled = status;
                lbSegmentFees.Enabled = status;
                lbSubTotal.Enabled = status;
                lbTaxes.Enabled = status;
                lbTotalDuty.Enabled = status;
                lbTotalFlight.Enabled = status;
                lbTotDays.Enabled = status;
                lbTotFixCost.Enabled = status;
                lbTotPrepDeposit.Enabled = status;
                lbTotQuote.Enabled = status;
                lbUsageAdj.Enabled = status;
                rmtArrivalTime.Enabled = status;
                rmtArrivalHomeTime.Enabled = status;
                rmtArrivalUtctime.Enabled = status;
                rmtHomeTime.Enabled = status;
                rmtlocaltime.Enabled = status;
                rmtUtctime.Enabled = status;
                rbDomestic.Enabled = status;
                rblistWindReliability.Enabled = status;
                chkList.Enabled = status;
                chkEndOfDuty.Enabled = status;
                chkTaxDaily.Enabled = status;
                chkTaxLandingFee.Enabled = status;

                btnClosestIcao.Enabled = status;
                btnArrival.Enabled = status;
                btnCrewRules.Enabled = status;
                btnFeeGroup.Enabled = status;
                btnPower.Enabled = status;

                // Override Button Controls
                btnTotalCost.Enabled = status;
                btnUsageAdj.Enabled = status;
                btnSegmentFees.Enabled = status;
                btnDiscount.Enabled = status;
                btnLandingFees.Enabled = status;
                btnDiscountAmount.Enabled = status;
                btnTaxes.Enabled = status;
                btnTotalQuote.Enabled = status;
                tbPaxTotal.Enabled = status;

                if (status)
                {
                    btnClosestIcao.CssClass = "browse-button";
                    btnArrival.CssClass = "browse-button";
                    btnCrewRules.CssClass = "browse-button";
                    btnFeeGroup.CssClass = "browse-button";
                    btnPower.CssClass = "browse-button";

                    // Override Button Controls
                    btnTotalCost.CssClass = "reset-icon";
                    btnUsageAdj.CssClass = "reset-icon";
                    btnSegmentFees.CssClass = "reset-icon";
                    btnDiscount.CssClass = "reset-icon";
                    btnLandingFees.CssClass = "reset-icon";
                    btnDiscountAmount.CssClass = "reset-icon";
                    btnTaxes.CssClass = "reset-icon";
                    btnTotalQuote.CssClass = "reset-icon";

                    ControlEnable("RadNumericTextBox", tbTotalCost, true, "pr_radtextbox_80");
                }
                else
                {
                    btnClosestIcao.CssClass = "browse-button-disabled";
                    btnArrival.CssClass = "browse-button-disabled";
                    btnCrewRules.CssClass = "browse-button-disabled";
                    btnFeeGroup.CssClass = "browse-button-disabled";
                    btnPower.CssClass = "browse-button-disabled";

                    // Override Button Controls
                    btnTotalCost.CssClass = "reset-icon-disable";
                    btnUsageAdj.CssClass = "reset-icon-disable";
                    btnSegmentFees.CssClass = "reset-icon-disable";
                    btnDiscount.CssClass = "reset-icon-disable";
                    btnLandingFees.CssClass = "reset-icon-disable";
                    btnDiscountAmount.CssClass = "reset-icon-disable";
                    btnTaxes.CssClass = "reset-icon-disable";
                    btnTotalQuote.CssClass = "reset-icon-disable";

                    ControlEnable("RadNumericTextBox", tbTotalCost, false, "pr_radtextbox_80_readonly");
                }

                // Override Textbox Controls
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.IsOverrideCost != null && (bool)QuoteInProgress.IsOverrideCost)
                    {
                        if (status)
                        {
                            tbTotalCost.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbTotalCost, true, "pr_radtextbox_80");
                        }
                        btnTotalCost.ToolTip = "Restore Default Value";
                        tbTotalCost.ForeColor = System.Drawing.Color.Red;
                        lbTotFixCost.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbTotalCost.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbTotalCost, false, "pr_radtextbox_80_readonly");
                        btnTotalCost.ToolTip = "Override Default Value";
                        tbTotalCost.ForeColor = System.Drawing.Color.Black;
                        lbTotFixCost.ForeColor = System.Drawing.Color.Black;
                    }

                    if (QuoteInProgress.IsOverrideUsageAdj != null && (bool)QuoteInProgress.IsOverrideUsageAdj)
                    {
                        if (status)
                        {
                            tbUsageAdj.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbUsageAdj, true, "pr_radtextbox_50");
                        }
                        btnUsageAdj.ToolTip = "Restore Default Value";
                        tbUsageAdj.ForeColor = System.Drawing.Color.Red;
                        lbUsageAdj.ForeColor = System.Drawing.Color.Red; //.CssClass = "infored";
                    }
                    else
                    {
                        tbUsageAdj.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbUsageAdj, false, "pr_radtextbox_50_readonly");
                        btnUsageAdj.ToolTip = "Override Default Value";
                        tbUsageAdj.ForeColor = System.Drawing.Color.Black;
                        lbUsageAdj.ForeColor = System.Drawing.Color.Black; //.CssClass = string.Empty;
                    }

                    if (QuoteInProgress.IsOverrideSegmentFee != null && (bool)QuoteInProgress.IsOverrideSegmentFee)
                    {
                        if (status)
                        {
                            tbSegmentFees.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbSegmentFees, true, "pr_radtextbox_50");
                        }
                        btnSegmentFees.ToolTip = "Restore Default Value";
                        tbSegmentFees.ForeColor = System.Drawing.Color.Red;
                        lbSegmentFees.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbSegmentFees.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbSegmentFees, false, "pr_radtextbox_50_readonly");
                        btnSegmentFees.ToolTip = "Override Default Value";
                        tbSegmentFees.ForeColor = System.Drawing.Color.Black;
                        lbSegmentFees.ForeColor = System.Drawing.Color.Black;
                    }

                    if (QuoteInProgress.IsOverrideDiscountPercentage != null && (bool)QuoteInProgress.IsOverrideDiscountPercentage)
                    {
                        if (status)
                        {
                            tbDiscount.ReadOnly = false;
                            ControlEnable("TextBox", tbDiscount, true, "readonly-text50");
                        }
                        btnDiscount.ToolTip = "Restore Default Value";
                        tbDiscount.ForeColor = System.Drawing.Color.Red;
                        lbDiscount.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbDiscount.ReadOnly = true;
                        ControlEnable("TextBox", tbDiscount, false, "readonly-text50");
                        btnDiscount.ToolTip = "Override Default Value";
                        tbDiscount.ForeColor = System.Drawing.Color.Black;
                        lbDiscount.ForeColor = System.Drawing.Color.Black;
                    }

                    if (QuoteInProgress.IsOverrideLandingFee != null && (bool)QuoteInProgress.IsOverrideLandingFee)
                    {
                        if (status)
                        {
                            tbLandingFees.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbLandingFees, true, "pr_radtextbox_50");
                        }
                        btnLandingFees.ToolTip = "Restore Default Value";
                        tbLandingFees.ForeColor = System.Drawing.Color.Red;
                        lbLandingFees.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbLandingFees.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbLandingFees, false, "pr_radtextbox_50_readonly");
                        btnLandingFees.ToolTip = "Override Default Value";
                        tbLandingFees.ForeColor = System.Drawing.Color.Black;
                        lbLandingFees.ForeColor = System.Drawing.Color.Black;
                    }

                    if (QuoteInProgress.IsOverrideDiscountAMT != null && (bool)QuoteInProgress.IsOverrideDiscountAMT)
                    {
                        if (status)
                        {
                            tbDiscountAmount.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbDiscountAmount, true, "pr_radtextbox_50");
                        }
                        btnDiscountAmount.ToolTip = "Restore Default Value";
                        tbDiscountAmount.ForeColor = System.Drawing.Color.Red;
                        lbDiscountAmount.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbDiscountAmount.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbDiscountAmount, false, "pr_radtextbox_50_readonly");
                        btnDiscountAmount.ToolTip = "Override Default Value";
                        tbDiscountAmount.ForeColor = System.Drawing.Color.Black;
                        lbDiscountAmount.ForeColor = System.Drawing.Color.Black;
                    }

                    if (QuoteInProgress.IsOverrideTaxes != null && (bool)QuoteInProgress.IsOverrideTaxes)
                    {
                        if (status)
                        {
                            tbTaxes.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbTaxes, true, "pr_radtextbox_50");
                        }
                        btnTaxes.ToolTip = "Restore Default Value";
                        tbTaxes.ForeColor = System.Drawing.Color.Red;
                        lbTaxes.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbTaxes.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbTaxes, false, "pr_radtextbox_50_readonly");
                        btnTaxes.ToolTip = "Override Default Value";
                        tbTaxes.ForeColor = System.Drawing.Color.Black;
                        lbTaxes.ForeColor = System.Drawing.Color.Black;
                    }

                    if (QuoteInProgress.IsOverrideTotalQuote != null && (bool)QuoteInProgress.IsOverrideTotalQuote)
                    {
                        if (status)
                        {
                            tbTotalQuote.ReadOnly = false;
                            ControlEnable("RadNumericTextBox", tbTotalQuote, true, "pr_radtextbox_50");
                        }
                        btnTotalQuote.ToolTip = "Restore Default Value";
                        tbTotalQuote.ForeColor = System.Drawing.Color.Red;
                        lbTotQuote.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        tbTotalQuote.ReadOnly = true;
                        ControlEnable("RadNumericTextBox", tbTotalQuote, false, "pr_radtextbox_50_readonly");
                        btnTotalQuote.ToolTip = "Override Default Value";
                        tbTotalQuote.ForeColor = System.Drawing.Color.Black;
                        lbTotQuote.ForeColor = System.Drawing.Color.Black;
                    }
                }

                dgStandardCharges.Enabled = status;
            }
        }

        private void ClearLabels()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvPower.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbRest.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            }
        }

        private void validateandrebindmaster()
        {
            if (FileRequest.CQFileID == 0 && FileRequest.State == CQRequestEntityState.Added)
            {
                Master.ValidateQuote();
            }
        }

        private void CommonPreflightCalculation(string EventChange, bool isDepartOrArrival, DateTime? ChangedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EventChange, isDepartOrArrival))
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {

                    if (QuoteInProgress != null)
                    {
                        if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                            SaveCharterQuoteToSession();

                        switch (EventChange)
                        {
                            case "Airport":
                                //Reset Checklist
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateMiles();
                                CalcualteBias();
                                CalculateWind();
                                CalculateETE();
                                ClearAllDatesBasedonDeptOrArrival(isDepartOrArrival);
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "Miles":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "Power":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalcualteBias();
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "Bias":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "Wind":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "ETE":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "WindReliability":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "Date":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateDateTime(isDepartOrArrival, ChangedDate);
                                CalculateWind();
                                CalculateETE();
                                //if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                //{
                                //    CQLeg currLeg = new CQLeg();
                                //    currLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();
                                //    if (currLeg != null)
                                //    {
                                //currLeg.IsDepartureConfirmed = isDepartOrArrival;
                                //currLeg.IsArrivalConfirmation = !isDepartOrArrival;

                                //#region Time Anchor Indicator
                                //lbDepartureDate.CssClass = "time_anchor";
                                //lbArrivalDate.CssClass = "time_anchor";
                                //if (isDepartOrArrival)
                                //    lbDepartureDate.CssClass = "time_anchor_active";
                                //else
                                //    lbArrivalDate.CssClass = "time_anchor_active";
                                //#endregion

                                //    }
                                //}


                                CalculateDateTime(isDepartOrArrival, ChangedDate);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                            case "CrewDutyRule":
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                Master.CalculateQuantity();
                                break;
                        }

                        if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                        {
                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                            {
                                CQLeg currLeg = new CQLeg();
                                currLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();
                                if (currLeg != null)
                                {
                                    currLeg.IsDepartureConfirmed = isDepartOrArrival;
                                    currLeg.IsArrivalConfirmation = !isDepartOrArrival;
                                }
                            }

                            #region Time Anchor Indicator
                            lbDepartureDate.CssClass = "time_anchor";
                            lbArrivalDate.CssClass = "time_anchor";
                            if (isDepartOrArrival)
                                lbDepartureDate.CssClass = "time_anchor_active";
                            else
                                lbArrivalDate.CssClass = "time_anchor_active";
                            #endregion

                            SaveCharterQuoteToSession();
                            Master.CalculateQuoteTotal();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                            {
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            }
                            LoadLegDetails();
                        }
                    }

                }
            }
        }

        private void setWarnMessageToDefault()
        {
            if (QuoteInProgress != null)
            {
                if (QuoteInProgress.CQLegs != null)
                {
                    foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false))
                    {
                        Leg.ShowWarningMessage = true;
                        hdnShowWarnMessage.Value = "true";
                    }
                }
            }
        }

        //private void ShowWarnMessage()
        //{
        //    if (!string.IsNullOrEmpty(hdnLegNum.Value))
        //    {
        //        if (QuoteInProgress != null)
        //        {
        //            if (QuoteInProgress.CQLegs != null)
        //            {
        //                CQLeg CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();

        //                if (CurrLeg != null)
        //                {
        //                    if (!string.IsNullOrEmpty(hdnShowWarnMessage.Value) && hdnShowWarnMessage.Value == "true") //(bool)CurrLeg.ShowWarningMessage && 
        //                    {
        //                        RadWindowManager1.RadConfirm("WARNING - The auto date adjustment feature is active.  Changes madeto any dates or RON values will automatically adjust all other legs onthis itinerary.Press OK to Continue or Cancel to Return.", null, 330, 100, null, "Confirmation!");
        //                        CurrLeg.ShowWarningMessage = false;
        //                        hdnShowWarnMessage.Value = "false";
        //                    }

        //                }
        //            }
        //        }
        //    }
        //}

        private int GetQtr(DateTime Dateval)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from QuoteInProgressLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }

        private void CalculateDateTime(bool isDepartureConfirmed, DateTime? ChangedDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isDepartureConfirmed))
            {
                double x10, x11;
                int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                DateTime Arrivaldt;
                //try
                //{

                if (ChangedDate == null)
                {
                    if (!string.IsNullOrEmpty(tbLocalDate.Text))
                    {
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime dt = new DateTime();
                            //string StartTime = rmtlocaltime.Text.Trim();
                            string StartTime = "0000";
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            if (StartHrs > 23)
                            {
                                RadWindowManager1.RadConfirm("Hour entry must be between 0 and 23", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else if (StartMts > 59)
                            {
                                RadWindowManager1.RadConfirm("Minute entry must be between 0 and 59", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            dt = DateTime.ParseExact(tbLocalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            ChangedDate = dt;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime dt = new DateTime();
                                string StartTime = rmtArrivalTime.Text.Trim();
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                dt = DateTime.ParseExact(tbArrivalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                ChangedDate = dt;
                                isDepartureConfirmed = false;
                            }
                        }
                    }
                }


                if (ChangedDate != null)
                {


                    DateTime EstDepartDate = (DateTime)ChangedDate;
                    //if (QuoteInProgress.EstDepartureDT != null)
                    //{
                    //    EstDepartDate = (DateTime)QuoteInProgress.EstDepartureDT;
                    //}



                    try
                    {

                        //(!string.IsNullOrEmpty(hdnHomebaseAirport.Value) && hdnHomebaseAirport.Value != "0")&& 

                        if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                            && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                            )
                        {

                            if (!string.IsNullOrEmpty(tbETE.Text))
                            {
                                string ETEstr = "0";
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                {
                                    ETEstr = ConvertMinToTenths(tbETE.Text, true, "1");
                                }
                                else
                                    ETEstr = tbETE.Text;


                                double QuoteInProgressleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                x10 = (QuoteInProgressleg_elp_time * 60 * 60);
                                x11 = (QuoteInProgressleg_elp_time * 60 * 60);
                            }
                            else
                            {
                                x10 = 0;
                                x11 = 0;
                            }
                            DateTime DeptUTCdt = DateTime.MinValue;
                            DateTime ArrUTCdt = DateTime.MinValue;

                            if (isDepartureConfirmed)
                            {
                                //DeptUTCDate
                                if (!string.IsNullOrEmpty(rmtUtctime.Text))
                                {
                                    string DeptUTCTimestr = rmtUtctime.Text.Trim();
                                    DeptUTCHrst = Convert.ToInt16(DeptUTCTimestr.Substring(0, 2));
                                    DeptUTCMtstr = Convert.ToInt16(DeptUTCTimestr.Substring(2, 2));
                                    if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                    {
                                        DeptUTCdt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        DeptUTCdt = DeptUTCdt.AddHours(DeptUTCHrst);
                                        DeptUTCdt = DeptUTCdt.AddMinutes(DeptUTCMtstr);
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(this.tbArrival.Text))
                                {
                                    string Timestr = rmtUtctime.Text.Trim();
                                    Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));

                                    Arrivaldt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    Arrivaldt = Arrivaldt.AddHours(Hrst);
                                    Arrivaldt = Arrivaldt.AddMinutes(Mtstr);
                                    Arrivaldt = Arrivaldt.AddSeconds(x10);
                                    ArrUTCdt = Arrivaldt;

                                    tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Arrivaldt);
                                    string startHrs = "0000" + Arrivaldt.Hour.ToString();
                                    string startMins = "0000" + Arrivaldt.Minute.ToString();
                                    rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbArrivalUtcDate.Text = string.Empty;
                                    rmtArrivalUtctime.Text = string.Empty;
                                }
                            }

                            else
                            {
                                //Arrival utc date
                                string ArrUTCTimestr = rmtArrivalUtctime.Text.Trim();
                                int ArrUTCHrst = Convert.ToInt16(ArrUTCTimestr.Substring(0, 2));
                                int ArrUTCMtstr = Convert.ToInt16(ArrUTCTimestr.Substring(2, 2));
                                ArrUTCdt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                ArrUTCdt = ArrUTCdt.AddHours(ArrUTCHrst);
                                ArrUTCdt = ArrUTCdt.AddMinutes(ArrUTCMtstr);

                                if (!string.IsNullOrWhiteSpace(this.tbDepart.Text))
                                {
                                    // tbUtcDate.Text = Convert.ToString(Convert.ToDateTime(this.tbArrivalUtcDate.Text).AddSeconds(-(x11)));

                                    string Timestr = rmtArrivalUtctime.Text.Trim();
                                    ArrivalUTChrs = Convert.ToInt16(Timestr.Substring(0, 2));
                                    ArrivalUTCmts = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(ArrivalUTChrs);
                                    dt = dt.AddMinutes(ArrivalUTCmts);
                                    dt = dt.AddSeconds(-(x11));
                                    DeptUTCdt = dt;
                                    tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dt);
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbUtcDate.Text = string.Empty; //DepartsUTC
                                    rmtUtctime.Text = "";
                                }
                            }




                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime ldLocDep = DateTime.MinValue;
                                DateTime ldLocArr = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {
                                    ldLocDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), DeptUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                {
                                    ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), DeptUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {
                                    ldLocArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), ArrUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                {
                                    ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ArrUTCdt, false, false);
                                }

                                //DateTime ltBlank = EstDepartDate;

                                if (!string.IsNullOrEmpty(this.tbDepart.Text))
                                {
                                    // DateTime Localdt = ldLocDep;
                                    string startHrs = "0000" + ldLocDep.Hour.ToString();
                                    string startMins = "0000" + ldLocDep.Minute.ToString();
                                    tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldLocDep);
                                    rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        // DateTime Homedt = ldHomDep;
                                        string HomedtstartHrs = "0000" + ldHomDep.Hour.ToString();
                                        string HomedtstartMins = "0000" + ldHomDep.Minute.ToString();
                                        tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomDep);
                                        rmtHomeTime.Text = HomedtstartHrs.Substring(HomedtstartHrs.Length - 2, 2) + ":" + HomedtstartMins.Substring(HomedtstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = "0000" + Localdt.Hour.ToString();
                                    string startMins = "0000" + Localdt.Minute.ToString();

                                    tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                    rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    tbUtcDate.Text = tbLocalDate.Text;
                                    rmtUtctime.Text = rmtlocaltime.Text;

                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        tbHomeDate.Text = tbLocalDate.Text;
                                        rmtHomeTime.Text = rmtlocaltime.Text;
                                    }
                                }

                                if (!string.IsNullOrEmpty(this.tbArrival.Text))
                                {
                                    // DateTime dt = ldLocArr;
                                    string startHrs = "0000" + ldLocArr.Hour.ToString();
                                    string startMins = "0000" + ldLocArr.Minute.ToString();
                                    tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldLocArr);
                                    rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        // DateTime HomeArrivaldt = ldHomArr;
                                        string HomeArrivalstartHrs = "0000" + ldHomArr.Hour.ToString();
                                        string HomeArrivalstartMins = "0000" + ldHomArr.Minute.ToString();
                                        tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomArr);
                                        rmtArrivalHomeTime.Text = HomeArrivalstartHrs.Substring(HomeArrivalstartHrs.Length - 2, 2) + ":" + HomeArrivalstartMins.Substring(HomeArrivalstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = "0000" + Localdt.Hour.ToString();
                                    string startMins = "0000" + Localdt.Minute.ToString();

                                    tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                    rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    tbArrivalUtcDate.Text = tbArrivalDate.Text;
                                    rmtArrivalUtctime.Text = rmtArrivalTime.Text;

                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        tbArrivalHomeDate.Text = tbArrivalDate.Text;
                                        rmtArrivalHomeTime.Text = rmtlocaltime.Text;
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Manually handled
                        if (!string.IsNullOrEmpty(this.tbArrivalDate.Text))
                        {
                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmtArrivalTime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                        {

                            if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                            {
                                tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                rmtArrivalHomeTime.Text = "00:00";
                            }
                        }

                        if (!string.IsNullOrEmpty(this.tbArrivalUtcDate.Text))
                        {
                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmtArrivalUtctime.Text = "00:00";
                        }

                        if (!string.IsNullOrEmpty(this.tbLocalDate.Text))
                        {
                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmtlocaltime.Text = "00:00";
                        }

                        if (!string.IsNullOrEmpty(this.tbUtcDate.Text))
                        {
                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmtUtctime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                        {
                            if (!string.IsNullOrEmpty(this.tbHomeDate.Text))
                            {
                                tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                rmtHomeTime.Text = "00:00";
                            }
                        }
                    }

                }

            }

        }

        private void CalculateMiles()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {

                    //CalculationManager CM = new CalculationManager();
                    //Function Miles Calculation
                    double Miles = 0;
                    hdnMiles.Value = "0";
                    tbMiles.Text = "0";
                    if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                    {
                        Miles = objDstsvc.GetDistance(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value));
                        tbMiles.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)Miles).ToString();
                        hdnMiles.Value = Miles.ToString();
                    }
                }
                //}
                //catch (Exception) 
                //{
                //    tbMiles.Text = "0.0";
                //}

            }


        }

        private void CalculateETE()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    double ETE = 0;
                    // lnToBias, lnLndBias, lnTas
                    double Wind = 0;
                    double LandBias = 0;
                    double TAS = 0;
                    double Bias = 0;
                    double Miles = 0;

                    if (!string.IsNullOrEmpty(tbWind.Text))
                    {
                        Wind = Convert.ToDouble(tbWind.Text);
                    }

                    double LandingBias = 0;
                    string LandingBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                    else
                        LandingBiasStr = tbLandBias.Text;

                    if (double.TryParse(LandingBiasStr, out LandingBias))
                        LandBias = LandingBias;
                    else
                        LandBias = 0;

                    double tobias = 0;
                    string TakeOffBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "1");
                    }
                    else
                        TakeOffBiasStr = tbBias.Text;
                    if (double.TryParse(TakeOffBiasStr, out tobias))
                        Bias = tobias;
                    else
                        Bias = 0;

                    //if (!string.IsNullOrEmpty(tbLandBias.Text))
                    //{
                    //    LandBias = Convert.ToDouble(tbLandBias.Text);
                    //}
                    if (!string.IsNullOrEmpty(tbTAS.Text))
                    {
                        TAS = Convert.ToDouble(tbTAS.Text);
                    }

                    //if (!string.IsNullOrEmpty(tbBias.Text))
                    //{
                    //    Bias = Convert.ToDouble(tbBias.Text);
                    //}

                    //if (!string.IsNullOrEmpty(tbMiles.Text))
                    //{
                    //    Miles = Convert.ToDouble(tbMiles.Text);
                    //}

                    if (!string.IsNullOrEmpty(hdnMiles.Value))
                    {
                        Miles = Convert.ToDouble(hdnMiles.Value);
                    }

                    DateTime dt = DateTime.Now;

                    if (!string.IsNullOrEmpty(tbLocalDate.Text))
                    {
                        string StartTime = rmtlocaltime.Text.Trim();
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        dt = DateTime.ParseExact(tbLocalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                    }
                    else
                    {
                        //DateTime EstDeptDt;
                        //if (QuoteInProgress.EstDepartureDT != null)
                        //    EstDeptDt = (DateTime)QuoteInProgress.EstDepartureDT;
                        //else
                        //    EstDeptDt = dt;
                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            string StartTime = rmtArrivalTime.Text.Trim();
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                            dt = DateTime.ParseExact(tbArrivalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                        }
                    }

                    ETE = objDstsvc.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, dt);

                    ETE = RoundElpTime(ETE);

                    if (ETE == 0)
                    {
                        Int64 AircraftID = Convert.ToInt64(hdnAircraft.Value == string.Empty ? "0" : hdnAircraft.Value);
                        FlightPakMasterService.Aircraft Aircraft = Master.GetAircraft(AircraftID);

                        if (Aircraft != null)
                            if (Aircraft.IsFixedRotary != null && Aircraft.IsFixedRotary.ToUpper() == "R")
                                ETE = 0.1;
                    }

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        tbETE.Text = "00:00";
                        tbETE.Text = ConvertTenthsToMins(ETE.ToString());
                    }
                    else
                    {
                        tbETE.Text = "0.0";
                        tbETE.Text = Math.Round(ETE, 1).ToString();
                        if (tbETE.Text.IndexOf(".") < 0)
                            tbETE.Text = tbETE.Text + ".0";
                    }

                    tbETE.Text = Common.Utility.DefaultEteResult(tbETE.Text);

                    //}
                    //catch (Exception)
                    //{
                    //    tbETE.Text = "0.0";
                    //}

                }

            }
        }

        private void CalculateWind()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                if (!string.IsNullOrEmpty(tbLocalDate.Text))
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {
                        //string StartTime = rmtlocaltime.Text.Trim();
                        string StartTime = "0000";
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        DateTime dt = DateTime.ParseExact(tbLocalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                        //Function Wind Calculation
                        double Wind = 0;
                        if (
                            (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") &&
                            (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0") &&
                            (!string.IsNullOrEmpty(hdnAircraft.Value) && hdnAircraft.Value != "0")
                            )
                        {
                            Wind = objDstsvc.GetWind(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt32(rblistWindReliability.SelectedValue), Convert.ToInt64(hdnAircraft.Value), GetQtr(dt).ToString());
                            tbWind.Text = Wind.ToString();
                        }
                        else
                            tbWind.Text = "0.0";
                    }
                }
                else
                    tbWind.Text = "0.0";
                //}
                //catch (Exception)
                //{
                //    tbWind.Text = "0.0";
                //}

            }
        }

        private void CalcualteBias()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    List<double> BiasList = new List<double>();
                    if (tbPower.Text == string.Empty)
                        tbPower.Text = "1";

                    if (
                        (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                        &&
                        (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                        &&
                        (!string.IsNullOrEmpty(hdnAircraft.Value) && hdnAircraft.Value != "0")
                        )
                    {
                        BiasList = objDstsvc.CalcBiasTas(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt64(hdnAircraft.Value), tbPower.Text);

                        if (BiasList != null)
                        {
                            // lnToBias, lnLndBias, lnTas
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = "00:00";
                                tbBias.Text = ConvertTenthsToMins(BiasList[0].ToString());
                            }
                            else
                            {
                                tbBias.Text = "0.0";
                                tbBias.Text = Math.Round((decimal)BiasList[0], 1).ToString();

                            }
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Text = "00:00";
                                tbLandBias.Text = ConvertTenthsToMins(BiasList[1].ToString());
                            }
                            else
                            {
                                tbLandBias.Text = "0.0";
                                tbLandBias.Text = Math.Round((decimal)BiasList[1], 1).ToString();
                            }
                            tbTAS.Text = BiasList[2].ToString();
                        }
                    }
                    else
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbBias.Text = "00:00";
                            tbLandBias.Text = "00:00";
                        }
                        else
                        {
                            tbBias.Text = "0.0";
                            tbLandBias.Text = "0.0";
                        }
                        tbTAS.Text = "0.0";
                    }
                    //}
                    //catch (Exception)
                    //{
                    //    tbBias.Text = "0.0";
                    //    tbLandBias.Text = "0.0"; ;
                    //    tbTAS.Text = "0.0";
                    //    tbPower.Text = "1";
                    //}


                }
                // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(tbBias.Text))
                    {
                        if (tbBias.Text.IndexOf(".") < 0)
                        {
                            tbBias.Text = tbBias.Text + ".0";
                        }

                    }

                    if (!string.IsNullOrEmpty(tbLandBias.Text))
                    {
                        if (tbLandBias.Text.IndexOf(".") < 0)
                        {
                            tbLandBias.Text = tbLandBias.Text + ".0";
                        }
                    }
                }
            }
        }

        private void CalculateQuoteFARRules()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
                    lnNeededRest, lnWorkArea;

                lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0.0;

                bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                llRProblem = llFProblem = llDutyBegin = llDProblem = false;

                DateTime ltGmtDep, ltGmtArr, llDutyend;
                Int64 lnDuty_RulesID = 0;
                string lcDuty_Rules = string.Empty;

                if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                {
                    SaveCharterQuoteToSession();

                }


                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null)
                    {
                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                        lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                        lnDuty_Hrs = 0;
                        lnRest_Hrs = 0;
                        lnFlt_Hrs = 0;
                        //if ((DateTime)Preflegs[0].DepartureGreenwichDTTM != null)
                        //{
                        //    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                        //}
                        //else
                        //{
                        //    ltGmtDep = DateTime.MinValue;
                        //}

                        if (Preflegs[0].DepartureGreenwichDTTM != null)
                        {
                            ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                            ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                            llDutyBegin = true;
                            double lnOldDuty_hrs = -1;

                            int legcounter = 0;
                            foreach (CQLeg Leg in Preflegs)
                            {
                                if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                                {

                                    double dbElapseTime = 0.0;

                                    if (Leg.ElapseTM != null)
                                        dbElapseTime = (double)Leg.ElapseTM;

                                    //if (Leg.CrewDutyRulesID != null)
                                    //{

                                    //    dbCrewDutyRulesID = (Int64)Leg.CrewDutyRulesID;
                                    //}
                                    //if (dbCrewDutyRulesID != 0)
                                    //{
                                    double lnOverRide = 0;
                                    double lnDutyLeg_Num = 0;

                                    bool llDutyEnd = false;
                                    string FARNum = "";

                                    if (Leg.CrewDutyRulesID != null)
                                        lnDuty_RulesID = (Int64)Leg.CrewDutyRulesID;
                                    else
                                        lnDuty_RulesID = 0;
                                    if (Leg.IsDutyEnd == null)
                                        llDutyEnd = false;
                                    else
                                        llDutyEnd = (bool)Leg.IsDutyEnd;

                                    //if it is last leg then  llDutyEnd = true;
                                    if (lnEndLegNum == Leg.LegNUM)
                                        llDutyEnd = true;

                                    //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    //    tbETE.Text = ConvertTenthsToMins(ETE.ToString());
                                    //else
                                    //    tbETE.Text = ETE.ToString();


                                    lnOverRide = (double)(Leg.CQOverRide == null ? 0 : Leg.CQOverRide);



                                    lnDutyLeg_Num = (double)Leg.LegNUM;



                                    using (MasterCatalogServiceClient objectDstsvc = new MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                                      where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                                      select CrewDutyRl).ToList();

                                            if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                            {
                                                FARNum = CrewDtyRule[0].FedAviatRegNum;
                                                lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                                lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                                lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                                lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                                lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                                lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                                lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                            }
                                            else
                                            {
                                                lnDayBeg = 0;
                                                lnDayEnd = 0;
                                                lnMaxDuty = 0;
                                                lnMaxFlt = 0;
                                                lnMinRest = 8;
                                                lnRestMulti = 0;
                                                lnRestPlus = 0;
                                            }
                                        }
                                    }
                                    TimeSpan? Hoursdiff = (DateTime)Leg.DepartureGreenwichDTTM - ltGmtArr;
                                    if (Leg.ElapseTM != 0)
                                    {
                                        lnFlt_Hrs = lnFlt_Hrs + (double)Leg.ElapseTM;
                                        lnDuty_Hrs = lnDuty_Hrs + (double)Leg.ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((QuoteInProgressleg.gmtdep - ltGmtArr),0)/3600,1))
                                    }
                                    else
                                    {

                                        lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((QuoteInProgressleg.gmtdep - ltGmtArr),0)/3600,1))
                                    }

                                    llRProblem = false;

                                    if (llDutyBegin && lnOldDuty_hrs != -1)
                                    {

                                        lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                        if (lnRestMulti > 0)
                                        {

                                            //Here is the Multiple and Plus hours usage
                                            lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                            if (lnRest_Hrs < lnNeededRest && lnRest_Hrs > 0)
                                            {

                                                llRProblem = true;
                                            }
                                        }
                                        else
                                        {
                                            if (lnRest_Hrs < (lnMinRest + lnRestPlus) && lnRest_Hrs > 0)
                                            {

                                                llRProblem = true;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (llDutyBegin)
                                        {
                                            lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                        }
                                    }

                                    if (llDutyEnd)
                                    {
                                        lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                                    }

                                    if (lnRest_Hrs < 0)
                                        llRProblem = true;

                                    string lcCdAlert = "";

                                    if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than QuoteInProgressleg.maxflt
                                        lcCdAlert = "F";// Flight time violation and F is stored in QuoteInProgressleg.cdalert as a Flight time error
                                    else
                                        lcCdAlert = "";



                                    if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                        lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in QuoteInProgressleg.cdalert as a Duty time error

                                    // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;

                                    if (llRProblem)
                                        lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in QuoteInProgressleg.cdalert as a Rest time error 
                                    if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                        Leg.State = CQRequestEntityState.Modified;
                                    Leg.FlightHours = (decimal)lnFlt_Hrs;
                                    Leg.DutyHours = (decimal)lnDuty_Hrs;
                                    Leg.RestHours = (decimal)lnRest_Hrs;

                                    Leg.CrewDutyAlert = lcCdAlert;
                                    //Leg.IsDutyEnd = llDutyEnd;
                                    if (Convert.ToInt64(hdnLegNum.Value) == Leg.LegNUM)
                                    {

                                        lbFar.Text = System.Web.HttpUtility.HtmlEncode(FARNum);

                                        if (lcCdAlert.LastIndexOf('D') >= 0)
                                        {
                                            lbTotalDuty.CssClass = "infored";
                                            //lbTotalDuty.ForeColor = Color.White;
                                            //lbTotalDuty.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbTotalDuty.CssClass = "infoash";
                                            //lbTotalDuty.ForeColor = Color.Black;
                                            //lbTotalDuty.BackColor = Color.Transparent;
                                        }
                                        if (lcCdAlert.LastIndexOf('F') >= 0)
                                        {
                                            lbTotalFlight.CssClass = "infored";
                                            //lbTotalFlight.ForeColor = Color.White;
                                            //lbTotalFlight.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbTotalFlight.CssClass = "infoash";
                                            //lbTotalFlight.ForeColor = Color.Black;
                                            //lbTotalFlight.BackColor = Color.Transparent;
                                        }
                                        if (lcCdAlert.LastIndexOf('R') >= 0)
                                        {
                                            lbRest.CssClass = "infored";
                                            //lbRest.ForeColor = Color.White;
                                            //lbRest.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbRest.CssClass = "infoash";
                                            //lbRest.ForeColor = Color.Black;
                                            //lbRest.BackColor = Color.Transparent;
                                        }

                                        if (lnDuty_Hrs != 0)
                                        {
                                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(Math.Round(lnDuty_Hrs, 1).ToString());
                                            if (lbTotalDuty.Text.IndexOf(".") < 0)
                                            {
                                                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(lbTotalDuty.Text + ".0");
                                            }
                                        }
                                        else
                                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        if (lnFlt_Hrs != 0)
                                        {
                                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(Math.Round(lnFlt_Hrs, 1).ToString());
                                            if (lbTotalFlight.Text.IndexOf(".") < 0)
                                            {
                                                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(lbTotalFlight.Text + ".0");
                                            }
                                        }
                                        else
                                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode("0.0");

                                        if (lnRest_Hrs != 0)
                                        {
                                            lbRest.Text = Math.Round(lnRest_Hrs, 1).ToString();
                                            if (lbRest.Text.IndexOf(".") < 0)
                                            {
                                                lbRest.Text = System.Web.HttpUtility.HtmlEncode(lbRest.Text + ".0");
                                            }
                                        }
                                        else
                                            lbRest.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                    }

                                    lnLeg_Num = (double)Leg.LegNUM;
                                    llDutyEnd = (bool)Leg.IsDutyEnd;
                                    ltGmtArr = (DateTime)Leg.ArrivalGreenwichDTTM;

                                    if ((bool)Leg.IsDutyEnd)
                                    {
                                        llDutyBegin = true;
                                        lnFlt_Hrs = 0;
                                    }
                                    else
                                    {
                                        llDutyBegin = false;
                                    }

                                    lnOldDuty_hrs = lnDuty_Hrs;
                                    //check next leg if available do the below steps
                                    legcounter++;
                                    if (legcounter < Preflegs.Count)
                                    {
                                        if (llDutyEnd)
                                        {
                                            lnDuty_Hrs = 0;

                                            if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                            {
                                                TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                                lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                                //ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                            }
                                        }
                                        else
                                        {
                                            lnRest_Hrs = 0;
                                        }
                                    }
                                    //check next leg if available do the below steps

                                    //}
                                }

                            }
                        }
                    }
                }

                Master.SaveQuoteinProgressToFileSession();
            }
        }

        private void CalculateFlightCost()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                {
                    SaveCharterQuoteToSession();
                }

                if (QuoteInProgress.CQLegs != null)
                {
                    List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    decimal TotalCost = 0.0M;

                    foreach (CQLeg Leg in Preflegs)
                    {

                        decimal lnChrg_Rate = 0.0M;
                        decimal lnCost = 0.0M;
                        string lcChrg_Unit = string.Empty;

                        if (QuoteInProgress.FleetID != null && QuoteInProgress.FleetID != 0)
                        {
                            using (MasterCatalogServiceClient MasterClient = new MasterCatalogServiceClient())
                            {
                                var objRetval = MasterClient.GetFleetChargeRateList((long)QuoteInProgress.FleetID);
                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    if (objRetval.ReturnFlag)
                                    {
                                        List<FlightPakMasterService.FleetChargeRate> FleetchargeRate = (from Fleetchrrate in objRetval.EntityList
                                                                                                        where (Fleetchrrate.BeginRateDT <= Leg.ArrivalDTTMLocal
                                                                                                        && Fleetchrrate.EndRateDT >= Leg.DepartureDTTMLocal)
                                                                                                        where Fleetchrrate.FleetID == QuoteInProgress.FleetID
                                                                                                        select Fleetchrrate).ToList();
                                        if (FleetchargeRate.Count > 0)
                                        {
                                            lnChrg_Rate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal)FleetchargeRate[0].ChargeRate;
                                            lcChrg_Unit = FleetchargeRate[0].ChargeUnit;
                                        }
                                    }
                                }
                            }
                        }

                        if (lnChrg_Rate == 0.0M)
                        {

                            if (!string.IsNullOrEmpty(hdnAircraft.Value) && hdnAircraft.Value != "0")
                            {
                                Int64 AircraftID = Convert.ToInt64(hdnAircraft.Value);
                                FlightPakMasterService.Aircraft Aircraft = Master.GetAircraft(AircraftID);
                                if (Aircraft != null)
                                {
                                    lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                                    lcChrg_Unit = Aircraft.ChargeUnit;
                                }
                            }

                        }

                        switch (lcChrg_Unit)
                        {
                            case "N":
                                {
                                    lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate;
                                    break;
                                }
                            case "K":
                                {
                                    lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.852M;
                                    break;
                                }
                            case "S":
                                {
                                    lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * statuateConvFactor;
                                    break;
                                }
                            case "H":
                                {
                                    lnCost = (Leg.ElapseTM == null ? 0 : (decimal)Leg.ElapseTM) * lnChrg_Rate;
                                    break;
                                }
                            default: lnCost = 0.0M; break;

                        }
                        Leg.ChargeRate = lnChrg_Rate;
                        Leg.ChargeTotal = lnCost;
                        TotalCost = TotalCost + lnCost;
                        if (Convert.ToInt64(hdnLegNum.Value) == Leg.LegNUM)
                        {
                            if (Leg.ChargeTotal != null)
                            {
                                tbCharges.Text = Math.Round((decimal)Leg.ChargeTotal, 2).ToString();
                                tbRate.Text = Math.Round(lnChrg_Rate, 2).ToString();
                            }
                            else
                            {
                                tbCharges.Text = "0.00";
                                tbRate.Text = "0.00";
                            }

                        }
                    }
                    //Total cost needs to be added
                    // QuoteInProgress.FlightCost = TotalCost;


                }
                //}
                //catch (Exception)
                //{
                //    tbCost.Text = "0.0";
                //}

            }



        }

        private DateTime CalculateGMTArr(DateTime gmtdep)
        {
            double Wind = 0;
            double ETE = 0;
            double LandBias = 0;
            double TAS = 0;
            double Bias = 0;
            double Miles = 0;
            DateTime ArrUTCdt = DateTime.MinValue;
            using (CalculationService.CalculationServiceClient CalcService = new CalculationService.CalculationServiceClient())
            {

                //Function Wind Calculation
                if (
                (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                &&
                (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                &&
                (!string.IsNullOrEmpty(hdnAircraft.Value) && hdnAircraft.Value != "0")
                )
                {
                    Wind = CalcService.GetWind(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt32(rblistWindReliability.SelectedValue), Convert.ToInt64(hdnAircraft.Value), GetQtr(gmtdep).ToString());
                }

                double LandingBias = 0;
                string LandingBiasStr = string.Empty;

                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                else
                    LandingBiasStr = tbLandBias.Text;

                if (double.TryParse(LandingBiasStr, out LandingBias))
                    LandBias = LandingBias;
                else
                    LandBias = 0;

                double tobias = 0;
                string TakeOffBiasStr = string.Empty;

                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "1");
                }
                else
                    TakeOffBiasStr = tbBias.Text;
                if (double.TryParse(TakeOffBiasStr, out tobias))
                    Bias = tobias;
                else
                    Bias = 0;

                //if (!string.IsNullOrEmpty(tbLandBias.Text))
                //{
                //    LandBias = Convert.ToDouble(tbLandBias.Text);
                //}
                if (!string.IsNullOrEmpty(tbTAS.Text))
                {
                    TAS = Convert.ToDouble(tbTAS.Text);
                }

                ETE = CalcService.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, gmtdep);

                ETE = RoundElpTime(ETE);
                double x10 = 0;
                double x11 = 0;
                x10 = (ETE * 60 * 60);
                x11 = (ETE * 60 * 60);


                ArrUTCdt = gmtdep;
                ArrUTCdt = ArrUTCdt.AddSeconds(x10);
            }
            return ArrUTCdt;
        }

        private DateTime CalculateGMTDep(DateTime gmtarr)
        {
            double Wind = 0;
            double ETE = 0;
            double LandBias = 0;
            double TAS = 0;
            double Bias = 0;
            double Miles = 0;
            DateTime depUTCdt = DateTime.MinValue;
            using (CalculationService.CalculationServiceClient CalcService = new CalculationService.CalculationServiceClient())
            {

                //Function Wind Calculation
                if (
                (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                &&
                (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                &&
                (!string.IsNullOrEmpty(hdnAircraft.Value) && hdnAircraft.Value != "0")
                )
                {
                    Wind = CalcService.GetWind(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt32(rblistWindReliability.SelectedValue), Convert.ToInt64(hdnAircraft.Value), GetQtr(gmtarr).ToString());
                }

                double LandingBias = 0;
                string LandingBiasStr = string.Empty;

                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                else
                    LandingBiasStr = tbLandBias.Text;

                if (double.TryParse(LandingBiasStr, out LandingBias))
                    LandBias = LandingBias;
                else
                    LandBias = 0;

                double tobias = 0;
                string TakeOffBiasStr = string.Empty;

                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "1");
                }
                else
                    TakeOffBiasStr = tbBias.Text;
                if (double.TryParse(TakeOffBiasStr, out tobias))
                    Bias = tobias;
                else
                    Bias = 0;

                //if (!string.IsNullOrEmpty(tbLandBias.Text))
                //{
                //    LandBias = Convert.ToDouble(tbLandBias.Text);
                //}
                if (!string.IsNullOrEmpty(tbTAS.Text))
                {
                    TAS = Convert.ToDouble(tbTAS.Text);
                }

                ETE = CalcService.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, gmtarr);

                ETE = RoundElpTime(ETE);
                double x10 = 0;
                double x11 = 0;
                x10 = (ETE * 60 * 60);
                x11 = (ETE * 60 * 60);


                depUTCdt = gmtarr;
                depUTCdt = depUTCdt.AddSeconds(-1 * x10);
            }
            return depUTCdt;
        }

        private bool CheckDatechangeValid(DateTime gmtdep, DateTime gmtarr, Int64 LegNUM)
        {
            bool returnval = true;
            if (LegNUM > 1)
            {
                if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                {
                    if (QuoteInProgress != null)
                    {
                        CQLeg CurrLeg = new CQLeg();
                        CQLeg PrevLeg = new CQLeg();
                        CQLeg NextLeg = new CQLeg();

                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM).SingleOrDefault();
                        PrevLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM - 1).SingleOrDefault();
                        NextLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM + 1).SingleOrDefault();

                        if (CurrLeg != null && PrevLeg != null)
                        {
                            if (PrevLeg.ArrivalGreenwichDTTM != null)
                            {
                                double minron = 0;
                                if (UserPrincipal.Identity._fpSettings._MinuteHoursRON != null)
                                    minron = (double)UserPrincipal.Identity._fpSettings._MinuteHoursRON;
                                if (gmtdep <= ((DateTime)PrevLeg.ArrivalGreenwichDTTM).AddHours(minron))
                                {
                                    if (PrevLeg.CQLegID != 0)
                                        PrevLeg.State = CQRequestEntityState.Modified;
                                    PrevLeg.RemainOverNightCNT = 0;
                                }
                                else
                                {
                                    DateTime CurrDate = new DateTime();
                                    DateTime PrevDate = new DateTime();

                                    CurrDate = new DateTime(gmtdep.Year, gmtdep.Month, gmtdep.Day);
                                    PrevDate = new DateTime(((DateTime)PrevLeg.ArrivalDTTMLocal).Year, ((DateTime)PrevLeg.ArrivalDTTMLocal).Month, ((DateTime)PrevLeg.ArrivalDTTMLocal).Day);

                                    TimeSpan daydiff = CurrDate.Subtract(PrevDate);
                                    if (daydiff.Days > 99)
                                        returnval = false;

                                }
                            }
                        }


                        if (CurrLeg != null && NextLeg != null)
                        {
                            if (NextLeg.DepartureGreenwichDTTM != null)
                            {
                                if (gmtarr.AddHours((double)UserPrincipal.Identity._fpSettings._MinuteHoursRON) >= ((DateTime)NextLeg.DepartureGreenwichDTTM))
                                {
                                    tbRON.Text = "0";
                                }
                                else
                                {
                                    DateTime CurrDate = new DateTime();
                                    DateTime NextDate = new DateTime();

                                    CurrDate = new DateTime(gmtarr.Year, gmtarr.Month, gmtarr.Day);
                                    NextDate = new DateTime(((DateTime)NextLeg.DepartureDTTMLocal).Year, ((DateTime)NextLeg.DepartureDTTMLocal).Month, ((DateTime)NextLeg.DepartureDTTMLocal).Day);

                                    TimeSpan daydiff = NextDate.Subtract(CurrDate);
                                    if (daydiff.Days > 99)
                                        returnval = false;

                                }
                            }
                        }


                        Master.SaveQuoteinProgressToFileSession();
                    }
                }
            }
            return returnval;
        }

        #region "text Change events"

        protected void tbDepart_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                        lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbDepart.Text = tbDepart.Text.ToUpper();
                        hdnDepart.Value = string.Empty;
                        lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartIcao.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        bool isRural = false;
                        if (!string.IsNullOrEmpty(tbDepart.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbDepart.Text.ToUpper().Trim());
                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();

                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count > 0)
                                    {
                                        lbDepart.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName != null ? AirportLists[0].AirportName.ToUpper() : string.Empty);
                                        hdnDepart.Value = AirportLists[0].AirportID.ToString();

                                        lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID == null ? string.Empty : AirportLists[0].IcaoID.ToString());
                                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName == null ? string.Empty : AirportLists[0].CityName.ToString());
                                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName == null ? string.Empty : AirportLists[0].StateName.ToString());
                                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName == null ? string.Empty : AirportLists[0].CountryName.ToString());
                                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName == null ? string.Empty : AirportLists[0].AirportName.ToString());
                                        lbDepartsTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0");
                                        lbDepartsLandingbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0");

                                        lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode("");

                                        if (AirportLists[0].IsRural != null && (bool)AirportLists[0].IsRural)
                                            isRural = true;


                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? AirportLists[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? AirportLists[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? AirportLists[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? AirportLists[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? AirportLists[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? AirportLists[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? AirportLists[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? AirportLists[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty);


                                        lbDepartIcao.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        lbDepart.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion


                                        //Choice --- Codes(WANTED IN FUTURE)
                                        if (Session[Master.CQSessionKey] != null)
                                        {

                                            if (QuoteInProgress != null)
                                            {
                                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                                {
                                                    CQLeg cqLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).FirstOrDefault();
                                                    if (cqLeg != null)
                                                    {
                                                        QuoteInProgress.CQLegs[Convert.ToInt16(hdnLegNum.Value) - 1].DepartAirportChanged = true;
                                                    }
                                                }
                                            }
                                            SaveCharterQuoteToSession();
                                        }

                                        rbDomestic.SelectedValue = "1";
                                        if (!string.IsNullOrEmpty(hdnArrival.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnArrival.Value));

                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> ArrAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                ArrAirportLists = objArrRetVal.EntityList;

                                                if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                                {

                                                    if (ArrAirportLists[0].IsRural != null && (bool)ArrAirportLists[0].IsRural)
                                                        isRural = true;

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (ArrAirportLists[0].CountryID == null)
                                                        {
                                                            rbDomestic.SelectedValue = "1";
                                                        }
                                                        else
                                                        {
                                                            if (ArrAirportLists[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != ArrAirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }

                                                                if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //Taxable Company profile Settings
                                        if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 1)
                                        {
                                            chkList.Items[1].Selected = true;

                                        }
                                        else if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                                        {
                                            if (rbDomestic.SelectedValue == "1")
                                                chkList.Items[1].Selected = true;
                                        }
                                        else
                                            chkList.Items[1].Selected = false;


                                        if (chkList.Items[1].Selected)
                                        {

                                            if (QuoteInProgress != null)
                                            {
                                                tbTaxRate.Text = "0.00";
                                                CQLeg Currleg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).SingleOrDefault();
                                                if (Currleg != null)
                                                {
                                                    if (Currleg.State != 0)
                                                        Currleg.State = CQRequestEntityState.Modified;

                                                    Currleg.TaxRate = 0;

                                                    if (isRural)
                                                    {
                                                        if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                            Currleg.TaxRate = UserPrincipal.Identity._fpSettings._RuralTax;
                                                    }
                                                    else
                                                    {
                                                        if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                            Currleg.TaxRate = UserPrincipal.Identity._fpSettings._CQFederalTax;
                                                    }
                                                    tbTaxRate.Text = Math.Round(Currleg.TaxRate.Value, 2).ToString();
                                                }

                                                Master.SaveQuoteinProgressToFileSession();
                                            }
                                        }
                                        //Taxable Company profile Settings


                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                        if (AirportLists[0].AirportID == Convert.ToInt64(hdnDepart.Value))
                                        {
                                            // Fix for UW-1165(8179)
                                            string tooltipStr = string.Empty;

                                            if (!string.IsNullOrEmpty(AirportLists[0].Alerts)) // || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes)) 
                                            {
                                                tbDepart.ForeColor = Color.Red;
                                                string alertStr = "Alerts : \n";
                                                alertStr += AirportLists[0].Alerts;
                                                tooltipStr = alertStr;
                                            }
                                            else
                                                tbDepart.ForeColor = Color.Black;

                                            if (!string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                            {
                                                string noteStr = string.Empty;
                                                if (!string.IsNullOrEmpty(tooltipStr))
                                                    noteStr = "\n\nNotes : \n";
                                                else
                                                    noteStr = "Notes : \n";
                                                noteStr += AirportLists[0].GeneralNotes;
                                                tooltipStr += noteStr;
                                            }

                                            tbDepart.ToolTip = tooltipStr;
                                        }

                                        #endregion
                                        if (tbPower.Text != null)
                                            tbPower.Text = "1";
                                        bool isDepartureConfirmed = true;
                                        if (lbDepartureDate.CssClass == "time_anchor")
                                            isDepartureConfirmed = false;
                                        CommonPreflightCalculation("Airport", isDepartureConfirmed, null);
                                    }
                                    else
                                    {
                                        lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepart);
                                        lbDepartIcao.ToolTip = string.Empty;
                                    }
                                }
                                else
                                {
                                    lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepart);
                                    lbDepartIcao.ToolTip = string.Empty;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbArrive_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArrival);
                        lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesICAO.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbArrival.Text = tbArrival.Text.ToUpper();
                        hdnArrival.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        bool isRural = false;
                        if (!string.IsNullOrEmpty(tbArrival.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbArrival.Text.ToUpper().Trim());
                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();

                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count > 0)
                                    {
                                        lbArrival.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName != null ? AirportLists[0].AirportName.ToUpper() : string.Empty);
                                        hdnArrival.Value = AirportLists[0].AirportID.ToString();

                                        lbArrivesICAO.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID == null ? string.Empty : AirportLists[0].IcaoID.ToString());
                                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName == null ? string.Empty : AirportLists[0].CityName.ToString());
                                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName == null ? string.Empty : AirportLists[0].StateName.ToString());
                                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName == null ? string.Empty : AirportLists[0].CountryName.ToString());
                                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName == null ? string.Empty : AirportLists[0].AirportName.ToString());
                                        lbArrivesTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0");
                                        lbArrivesLandingbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0");

                                        lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode("");

                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? AirportLists[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? AirportLists[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? AirportLists[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? AirportLists[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? AirportLists[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? AirportLists[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? AirportLists[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? AirportLists[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty);


                                        lbArrivesICAO.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        lbArrive.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion

                                        if (AirportLists[0].IsRural != null && (bool)AirportLists[0].IsRural)
                                            isRural = true;


                                        //Choice --- Codes(WANTED IN FUTURE)
                                        if (Session[Master.CQSessionKey] != null)
                                        {

                                            if (QuoteInProgress != null)
                                            {
                                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                                {
                                                    CQLeg cqLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).FirstOrDefault();
                                                    if (cqLeg != null)
                                                    {
                                                        QuoteInProgress.CQLegs[Convert.ToInt16(hdnLegNum.Value) - 1].ArrivalAirportChanged = true;
                                                    }
                                                }
                                            }
                                            SaveCharterQuoteToSession();
                                        }

                                        rbDomestic.SelectedValue = "1";
                                        if (!string.IsNullOrEmpty(hdnDepart.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnDepart.Value));

                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                DepAirportLists = objArrRetVal.EntityList;

                                                if (DepAirportLists != null && DepAirportLists.Count > 0)
                                                {
                                                    if (DepAirportLists[0].IsRural != null && (bool)DepAirportLists[0].IsRural)
                                                        isRural = true;

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (DepAirportLists[0].CountryID == null)
                                                        {
                                                            rbDomestic.SelectedValue = "1";
                                                        }
                                                        else
                                                        {
                                                            if (DepAirportLists[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != DepAirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }
                                                                if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //Taxable Company profile Settings
                                        if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 1)
                                        {
                                            chkList.Items[1].Selected = true;

                                        }
                                        else if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                                        {
                                            if (rbDomestic.SelectedValue == "1")
                                                chkList.Items[1].Selected = true;
                                        }
                                        else
                                            chkList.Items[1].Selected = false;


                                        if (chkList.Items[1].Selected)
                                        {

                                            if (QuoteInProgress != null)
                                            {
                                                tbTaxRate.Text = "0.00";
                                                CQLeg Currleg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).SingleOrDefault();
                                                if (Currleg != null)
                                                {
                                                    if (Currleg.State != 0)
                                                        Currleg.State = CQRequestEntityState.Modified;
                                                    Currleg.TaxRate = 0;
                                                    if (isRural)
                                                    {
                                                        if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                            Currleg.TaxRate = UserPrincipal.Identity._fpSettings._RuralTax;
                                                    }
                                                    else
                                                    {
                                                        if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                            Currleg.TaxRate = UserPrincipal.Identity._fpSettings._CQFederalTax;
                                                    }
                                                    tbTaxRate.Text = Math.Round(Currleg.TaxRate.Value, 2).ToString();
                                                }
                                                Master.SaveQuoteinProgressToFileSession();
                                            }
                                        }
                                        //Taxable Company profile Settings


                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                        if (AirportLists[0].AirportID == Convert.ToInt64(hdnArrival.Value))
                                        {
                                            // Fix for UW-1165(8179)
                                            string tooltipStr = string.Empty;
                                            if (!string.IsNullOrEmpty(AirportLists[0].Alerts)) // || !string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                            {
                                                tbArrival.ForeColor = Color.Red;
                                                string alertStr = "Alerts : \n";
                                                alertStr += AirportLists[0].Alerts;
                                                tooltipStr = alertStr;
                                            }
                                            else
                                                tbArrival.ForeColor = Color.Black;

                                            if (AirportLists[0].GeneralNotes != null)
                                            {
                                                string noteStr = string.Empty;
                                                if (!string.IsNullOrEmpty(tooltipStr))
                                                    noteStr = "\n\nNotes : \n";
                                                else
                                                    noteStr = "Notes : \n";
                                                noteStr += AirportLists[0].GeneralNotes;
                                                tooltipStr += noteStr;
                                            }

                                            tbArrival.ToolTip = tooltipStr;
                                        }

                                        #endregion
                                        if (tbPower.Text != null)
                                            tbPower.Text = "1";
                                        bool isDepartureConfirmed = true;
                                        if (lbDepartureDate.CssClass == "time_anchor")
                                            isDepartureConfirmed = false;

                                        CommonPreflightCalculation("Airport", isDepartureConfirmed, null);
                                    }
                                    else
                                    {
                                        lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                        lbArrive.ToolTip = string.Empty;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrival);
                                    }
                                }
                                else
                                {
                                    lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                    lbArrive.ToolTip = string.Empty;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbArrival);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbMiles_TextChanged(object sender, EventArgs e)
        {
            //DoConvertToMiles

            decimal Miles = 0;
            hdnMiles.Value = "0";
            if (decimal.TryParse(tbMiles.Text, out Miles))
            {
                hdnMiles.Value = ConvertToMilesBasedOnCompanyProfile(Miles).ToString();
            }

            bool isDepartureConfirmed = true;
            if (lbDepartureDate.CssClass == "time_anchor")
                isDepartureConfirmed = false;

            CommonPreflightCalculation("Miles", isDepartureConfirmed, null);
            RadAjaxManager.GetCurrent(Page).FocusControl(tbBias);
        }

        protected void tbWind_TextChanged(object sender, EventArgs e)
        {
            bool isDepartureConfirmed = true;
            if (lbDepartureDate.CssClass == "time_anchor")
                isDepartureConfirmed = false;

            CommonPreflightCalculation("Wind", isDepartureConfirmed, null);
            RadAjaxManager.GetCurrent(Page).FocusControl(tbETE);
        }

        protected void tbPower_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPower);
                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdPower.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbPower.Text))
                        {

                            Int64 AircraftID = Convert.ToInt64(hdnAircraft.Value == string.Empty ? "0" : hdnAircraft.Value);
                            FlightPakMasterService.Aircraft Aircraft = Master.GetAircraft(AircraftID);
                            if (Aircraft != null)
                            {
                                switch (tbPower.Text)
                                {
                                    case "1":
                                        tbTAS.Text = Aircraft.PowerSettings1TrueAirSpeed != null ? Aircraft.PowerSettings1TrueAirSpeed.ToString() : "0";
                                        if (Aircraft.PowerSettings1TakeOffBias != null)
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {

                                                tbBias.Text = "00:00";
                                                tbBias.Text = ConvertTenthsToMins((Aircraft.PowerSettings1TakeOffBias).ToString());
                                            }
                                            else
                                            {
                                                tbBias.Text = "0.0";
                                                tbBias.Text = Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString();
                                            }
                                        tbLandBias.Text = Aircraft.PowerSettings1LandingBias != null ? Aircraft.PowerSettings1LandingBias.ToString() : "0";
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbLandBias.Text = "00:00";
                                            tbLandBias.Text = ConvertTenthsToMins((Aircraft.PowerSettings1LandingBias).ToString());
                                        }
                                        else
                                        {
                                            tbLandBias.Text = "0.0";
                                            tbLandBias.Text = Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString();
                                        }
                                        break;
                                    case "2":
                                        tbTAS.Text = Aircraft.PowerSettings2TrueAirSpeed != null ? Aircraft.PowerSettings2TrueAirSpeed.ToString() : "0";
                                        if (Aircraft.PowerSettings2TakeOffBias != null)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbBias.Text = "00:00";
                                                tbBias.Text = ConvertTenthsToMins((Aircraft.PowerSettings2TakeOffBias).ToString());
                                            }
                                            else
                                            {
                                                tbBias.Text = "0.0";
                                                tbBias.Text = Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString();
                                            }
                                        }
                                        if (Aircraft.PowerSettings2LandingBias != null)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbLandBias.Text = "00:00";
                                                tbLandBias.Text = ConvertTenthsToMins(Aircraft.PowerSettings2LandingBias.ToString());
                                            }
                                            else
                                            {
                                                tbLandBias.Text = "0.0";
                                                tbLandBias.Text = Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString();
                                            }
                                        }
                                        break;
                                    case "3":
                                        tbTAS.Text = Aircraft.PowerSettings3TrueAirSpeed != null ? Aircraft.PowerSettings3TrueAirSpeed.ToString() : "0";
                                        tbBias.Text = Aircraft.PowerSettings3TakeOffBias != null ? Aircraft.PowerSettings3TakeOffBias.ToString() : "0";
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbBias.Text = "00:00";
                                            tbBias.Text = ConvertTenthsToMins(Aircraft.PowerSettings3TakeOffBias.ToString());
                                        }
                                        else
                                        {
                                            tbBias.Text = "0.0";
                                            tbBias.Text = Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString();
                                        }
                                        if (Aircraft.PowerSettings3LandingBias != null)
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbLandBias.Text = "00:00";
                                                tbLandBias.Text = ConvertTenthsToMins(Aircraft.PowerSettings3LandingBias.ToString());
                                            }
                                            else
                                            {
                                                tbLandBias.Text = "0.0";
                                                tbLandBias.Text = Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString();
                                            }
                                        break;
                                    default:
                                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode("Power Setting must be 1, 2, or 3");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPower);
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbLandBias.Text = "00:00";
                                            tbBias.Text = "00:00";
                                        }
                                        else
                                        {
                                            tbLandBias.Text = "0.0";
                                            tbBias.Text = "0.0";
                                        }
                                        tbTAS.Text = "0.0";

                                        break;
                                }
                            }
                            else
                            {
                                switch (tbPower.Text)
                                {
                                    case "1":
                                    case "2":
                                    case "3":
                                        break;
                                    default:
                                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode("Power Setting must be 1, 2, or 3");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPower);
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbLandBias.Text = "00:00";
                                            tbBias.Text = "00:00";
                                        }
                                        else
                                        {
                                            tbLandBias.Text = "0.0";
                                            tbBias.Text = "0.0";
                                        }
                                        tbTAS.Text = "0.0";
                                        break;
                                }
                            }
                            bool isDepartureConfirmed = true;
                            if (lbDepartureDate.CssClass == "time_anchor")
                                isDepartureConfirmed = false;

                            CommonPreflightCalculation("Power", isDepartureConfirmed, null);

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbBias_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        //if (sender is RadMaskedTextBox)
                        //{
                        //    if (((RadMaskedTextBox)sender).ID.ToLower() == "tbbias")
                        //    {
                        //        ValidateTenthMinBasedonCompanyprofile(((RadMaskedTextBox)sender).Text);
                        //        imgbtnTOBias.Focus();
                        //    }
                        //}

                        //if (sender is RadMaskedTextBox)
                        //{
                        //    if (((RadMaskedTextBox)sender).ID.ToLower() == "tblandbias")
                        //    {
                        //        ValidateTenthMinBasedonCompanyprofile(((RadMaskedTextBox)sender).Text);
                        //        imgbtnLandBias.Focus();
                        //    }
                        //}
                        bool validatesucceded = true;
                        if (sender is TextBox)
                        {
                            if (((TextBox)sender).ID.ToLower() == "tbbias")
                            {
                                validatesucceded = ValidateTenthMinBasedonCompanyprofile((TextBox)sender);
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbTAS);
                            }
                            else if (((TextBox)sender).ID.ToLower() == "tblandbias")
                            {
                                validatesucceded = ValidateTenthMinBasedonCompanyprofile((TextBox)sender);
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbWind);
                            }
                            else if (((TextBox)sender).ID.ToLower() == "tbtas")
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbPower);
                            }
                        }
                        if (validatesucceded)
                        {
                            bool isDepartureConfirmed = true;
                            if (lbDepartureDate.CssClass == "time_anchor")
                                isDepartureConfirmed = false;
                            CommonPreflightCalculation("Bias", isDepartureConfirmed, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void tbETE_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool validatesucceded = true;
                        validatesucceded = ValidateTenthMinBasedonCompanyprofile(tbETE);
                        if (validatesucceded)
                        {
                            bool isDepartureConfirmed = true;
                            if (lbDepartureDate.CssClass == "time_anchor")
                                isDepartureConfirmed = false;
                            CommonPreflightCalculation("ETE", isDepartureConfirmed, null);
                        }
                        RadAjaxManager.GetCurrent(Page).FocusControl(rblistWindReliability);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbLocalDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(hdnDepart.Value))
                        {
                            if (!string.IsNullOrEmpty(tbLocalDate.Text))
                            {
                                if (!Master.IsValidDate(tbLocalDate.Text))
                                {
                                    tbLocalDate.Text = string.Empty;
                                    #region restore old date
                                    if (QuoteInProgress != null)
                                    {
                                        if (QuoteInProgress.CQLegs != null)
                                        {
                                            CQLeg CurrLeg = new CQLeg();
                                            CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                            if (CurrLeg != null && CurrLeg.DepartureDTTMLocal != null)
                                            {
                                                DateTime locdt = (DateTime)CurrLeg.DepartureDTTMLocal;
                                                string locstartHrs = "0000" + locdt.Hour.ToString();
                                                string locstartMins = "0000" + locdt.Minute.ToString();
                                                tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.DepartureDTTMLocal);
                                                rmtlocaltime.Text = locstartHrs.Substring(locstartHrs.Length - 2, 2) + ":" + locstartMins.Substring(locstartMins.Length - 2, 2);
                                            }
                                            else
                                            {
                                                tbLocalDate.Text = string.Empty;
                                                rmtlocaltime.Text = "0000";
                                            }

                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Date Change Calculation
                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {


                                        DateTime dt = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        //rmtlocaltime.Text = "0000";
                                        //string StartTime = rmtlocaltime.TextWithLiterals; //.Text.Trim();

                                        // Get and Set Time Fields
                                        string[] TimeArray = rmtlocaltime.TextWithLiterals.Split(':');
                                        string _hour = "00";
                                        string _minute = "00";
                                        if (!string.IsNullOrEmpty(TimeArray[0]))
                                            _hour = TimeArray[0];
                                        if (!string.IsNullOrEmpty(TimeArray[1]))
                                            _minute = TimeArray[1];

                                        int StartHrs = Convert.ToInt16(_hour);
                                        int StartMts = Convert.ToInt16(_minute);

                                        rmtlocaltime.Text = _hour + ":" + _minute;
                                        rmtlocaltime.Text = Master.CalculationTimeAdjustment(rmtlocaltime.TextWithLiterals);

                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtlocaltime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtlocaltime);

                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtlocaltime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtlocaltime);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbLocalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tblocaldate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}

                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), dt, true, false);

                                            string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                            string startMins = "0000" + ldGmtDep.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtArr = CalculateGMTArr(ldGmtDep);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation

                                            if (DateValid)
                                            {
                                                CommonPreflightCalculation("Date", true, dt);
                                                //Ron Calc
                                                Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                Master.CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                Master.CalculateQuoteTotal();
                                                Master.BindStandardCharges(true, dgStandardCharges);
                                                Master.BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadLegDetails();
                                                //Ron Calc

                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tblocaldate")
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtlocaltime);
                                                }
                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtlocaltime")
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbUtcDate);
                                                }
                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.DepartureDTTMLocal != null)
                                                        {
                                                            DateTime locdt = (DateTime)CurrLeg.DepartureDTTMLocal;
                                                            string locstartHrs = "0000" + locdt.Hour.ToString();
                                                            string locstartMins = "0000" + locdt.Minute.ToString();
                                                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.DepartureDTTMLocal);
                                                            rmtlocaltime.Text = locstartHrs.Substring(locstartHrs.Length - 2, 2) + ":" + locstartMins.Substring(locstartMins.Length - 2, 2);
                                                        }
                                                        else
                                                        {
                                                            tbLocalDate.Text = string.Empty;
                                                            rmtlocaltime.Text = "0000";
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbUtcDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(tbUtcDate.Text))
                        {
                            if (!Master.IsValidDate(tbUtcDate.Text))
                            {
                                tbUtcDate.Text = string.Empty;
                                #region restore old values
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.DepartureGreenwichDTTM != null)
                                        {
                                            DateTime dat = (DateTime)CurrLeg.DepartureGreenwichDTTM;
                                            string startHrsval = "0000" + dat.Hour.ToString();
                                            string startMinsval = "0000" + dat.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.DepartureGreenwichDTTM);
                                            rmtUtctime.Text = startHrsval.Substring(startMinsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                        }
                                    }
                                }
                                #endregion

                            }
                            else
                            {
                                #region Date Change Calculation
                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {




                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        //string StartTime = rmtUtctime.Text.Trim();
                                        //int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        //int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                        // Get and Set Time Fields
                                        string[] TimeArray = rmtUtctime.TextWithLiterals.Split(':');
                                        string _hour = "00";
                                        string _minute = "00";
                                        if (!string.IsNullOrEmpty(TimeArray[0]))
                                            _hour = TimeArray[0];
                                        if (!string.IsNullOrEmpty(TimeArray[1]))
                                            _minute = TimeArray[1];

                                        int StartHrs = Convert.ToInt16(_hour);
                                        int StartMts = Convert.ToInt16(_minute);

                                        rmtUtctime.Text = _hour + ":" + _minute;
                                        rmtUtctime.Text = Master.CalculationTimeAdjustment(rmtUtctime.TextWithLiterals);

                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtUtctime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtUtctime);
                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtUtctime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtUtctime);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tbutcdate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), dt, true, true);

                                            string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                            string startMins = "0000" + ldGmtDep.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtArr = CalculateGMTArr(ldGmtDep);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation


                                            if (DateValid)
                                            {



                                                CommonPreflightCalculation("Date", true, dt);

                                                //Ron Calc
                                                Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                Master.CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                Master.CalculateQuoteTotal();
                                                Master.BindStandardCharges(true, dgStandardCharges);
                                                Master.BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadLegDetails();
                                                //Ron Calc
                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tbutcdate")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtUtctime);
                                                    }
                                                }

                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtutctime")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeDate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.DepartureGreenwichDTTM != null)
                                                        {
                                                            DateTime dat = (DateTime)CurrLeg.DepartureGreenwichDTTM;
                                                            string startHrsval = "0000" + dat.Hour.ToString();
                                                            string startMinsval = "0000" + dat.Minute.ToString();
                                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.DepartureGreenwichDTTM);
                                                            rmtUtctime.Text = startHrsval.Substring(startMinsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }



                                }
                                #endregion
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbHomeDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbHomeDate.Text))
                        {
                            if (!Master.IsValidDate(tbHomeDate.Text))
                            {
                                tbHomeDate.Text = string.Empty;
                                #region restore old values
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.HomeDepartureDTTM != null)
                                        {
                                            DateTime dat = (DateTime)CurrLeg.HomeDepartureDTTM;
                                            string startHrsval = "0000" + dat.Hour.ToString();
                                            string startMinsval = "0000" + dat.Minute.ToString();
                                            tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.HomeDepartureDTTM);
                                            rmtHomeTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                        }
                                    }
                                }
                                #endregion

                            }
                            else
                            {
                                #region Date change Calculation
                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                {

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                        {
                                            DateTime dt = new DateTime();
                                            DateTime ldGmtDep = new DateTime();
                                            DateTime ldGmtArr = new DateTime();
                                            //string StartTime = rmtHomeTime.Text.Trim();
                                            //int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                            //int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                            // Get and Set Time Fields
                                            string[] TimeArray = rmtHomeTime.TextWithLiterals.Split(':');
                                            string _hour = "00";
                                            string _minute = "00";
                                            if (!string.IsNullOrEmpty(TimeArray[0]))
                                                _hour = TimeArray[0];
                                            if (!string.IsNullOrEmpty(TimeArray[1]))
                                                _minute = TimeArray[1];

                                            int StartHrs = Convert.ToInt16(_hour);
                                            int StartMts = Convert.ToInt16(_minute);

                                            rmtHomeTime.Text = _hour + ":" + _minute;
                                            rmtHomeTime.Text = Master.CalculationTimeAdjustment(rmtHomeTime.TextWithLiterals);

                                            bool calculate = true;
                                            if (StartHrs > 23)
                                            {
                                                RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                                calculate = false;
                                                rmtHomeTime.Text = "0000";
                                                RadAjaxManager.GetCurrent(Page).FocusControl(rmtHomeTime);
                                            }
                                            else if (StartMts > 59)
                                            {
                                                RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                                calculate = false;
                                                rmtHomeTime.Text = "0000";
                                                RadAjaxManager.GetCurrent(Page).FocusControl(rmtHomeTime);
                                            }

                                            if (calculate)
                                            {
                                                dt = DateTime.ParseExact(tbHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                                //if (sender is TextBox)
                                                //{
                                                //    if (((TextBox)sender).ID.ToLower() == "tbhomedate")
                                                //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                                //}
                                                dt = dt.AddHours(StartHrs);
                                                dt = dt.AddMinutes(StartMts);
                                                //GMTDept
                                                ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), dt, true, false);

                                                bool DateValid = true;
                                                //Company Minron validation
                                                if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                                {
                                                    ldGmtArr = CalculateGMTArr(ldGmtDep);
                                                    DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                //Company minron validation


                                                string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                                string startMins = "0000" + ldGmtDep.Minute.ToString();
                                                tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                                rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                                if (DateValid)
                                                {

                                                    CommonPreflightCalculation("Date", true, dt);

                                                    //Ron Calc
                                                    Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                    //ShowWarnMessage();
                                                    Master.CalculateQuantity();
                                                    SaveCharterQuoteToSession();
                                                    Master.CalculateQuoteTotal();
                                                    Master.BindStandardCharges(true, dgStandardCharges);
                                                    Master.BindLegs(dgLegs, true);
                                                    if (dgLegs.Items.Count > 0)
                                                    {
                                                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                    }
                                                    LoadLegDetails();
                                                    //Ron Calc
                                                    if (sender is TextBox)
                                                    {
                                                        if (((TextBox)sender).ID.ToLower() == "tbhomedate")
                                                        {
                                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtHomeTime);
                                                        }
                                                    }

                                                    if (sender is RadMaskedTextBox)
                                                    {
                                                        if (((RadMaskedTextBox)sender).ID.ToLower() == "rmthometime")
                                                        {
                                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalDate);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);

                                                    if (QuoteInProgress != null)
                                                    {
                                                        if (QuoteInProgress.CQLegs != null)
                                                        {
                                                            CQLeg CurrLeg = new CQLeg();
                                                            CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                            if (CurrLeg != null && CurrLeg.HomeDepartureDTTM != null)
                                                            {
                                                                DateTime dat = (DateTime)CurrLeg.HomeDepartureDTTM;
                                                                string startHrsval = "0000" + dat.Hour.ToString();
                                                                string startMinsval = "0000" + dat.Minute.ToString();
                                                                tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.HomeDepartureDTTM);
                                                                rmtHomeTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbArrivalDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            if (!Master.IsValidDate(tbArrivalDate.Text))
                            {
                                tbArrivalDate.Text = string.Empty;
                                #region Restore old values
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime dat = (DateTime)CurrLeg.ArrivalDTTMLocal;
                                            string startHrsval = "0000" + dat.Hour.ToString();
                                            string startMinsval = "0000" + dat.Minute.ToString();
                                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.ArrivalDTTMLocal);
                                            rmtArrivalTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Date change calculation
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        //string StartTime = rmtArrivalTime.Text.Trim();
                                        //int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        //int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                        // Get and Set Time Fields
                                        string[] TimeArray = rmtArrivalTime.TextWithLiterals.Split(':');
                                        string _hour = "00";
                                        string _minute = "00";
                                        if (!string.IsNullOrEmpty(TimeArray[0]))
                                            _hour = TimeArray[0];
                                        if (!string.IsNullOrEmpty(TimeArray[1]))
                                            _minute = TimeArray[1];

                                        int StartHrs = Convert.ToInt16(_hour);
                                        int StartMts = Convert.ToInt16(_minute);

                                        rmtArrivalTime.Text = _hour + ":" + _minute;
                                        rmtArrivalTime.Text = Master.CalculationTimeAdjustment(rmtArrivalTime.TextWithLiterals);

                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtArrivalTime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalTime);
                                        }
                                        if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtArrivalTime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalTime);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbArrivalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tbarrivaldate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, false);

                                            string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                            string startMins = "0000" + ldGmtArr.Minute.ToString();
                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtDep = CalculateGMTDep(ldGmtArr);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation


                                            if (DateValid)
                                            {
                                                CommonPreflightCalculation("Date", false, dt);

                                                //Ron Calc
                                                Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                Master.CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                Master.CalculateQuoteTotal();
                                                Master.BindStandardCharges(true, dgStandardCharges);
                                                Master.BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadLegDetails();
                                                //Ron Calc

                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tbarrivaldate")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalTime);
                                                    }
                                                }

                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtarrivaltime")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalUtcDate);
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.ArrivalDTTMLocal != null)
                                                        {
                                                            DateTime dat = (DateTime)CurrLeg.ArrivalDTTMLocal;
                                                            string startHrsval = "0000" + dat.Hour.ToString();
                                                            string startMinsval = "0000" + dat.Minute.ToString();
                                                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.ArrivalDTTMLocal);
                                                            rmtArrivalTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbArrivalUtcDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbArrivalUtcDate.Text))
                        {
                            if (!Master.IsValidDate(tbArrivalUtcDate.Text))
                            {
                                tbArrivalUtcDate.Text = string.Empty;
                                #region Restore old values
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.ArrivalGreenwichDTTM != null)
                                        {
                                            DateTime dat = (DateTime)CurrLeg.ArrivalGreenwichDTTM;
                                            string startHrsval = "0000" + dat.Hour.ToString();
                                            string startMinsval = "0000" + dat.Minute.ToString();
                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.ArrivalGreenwichDTTM);
                                            rmtArrivalUtctime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region "Date change Calculation"
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        //string StartTime = rmtArrivalUtctime.Text.Trim();
                                        //int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        //int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                        // Get and Set Time Fields
                                        string[] TimeArray = rmtArrivalUtctime.TextWithLiterals.Split(':');
                                        string _hour = "00";
                                        string _minute = "00";
                                        if (!string.IsNullOrEmpty(TimeArray[0]))
                                            _hour = TimeArray[0];
                                        if (!string.IsNullOrEmpty(TimeArray[1]))
                                            _minute = TimeArray[1];

                                        int StartHrs = Convert.ToInt16(_hour);
                                        int StartMts = Convert.ToInt16(_minute);

                                        rmtArrivalUtctime.Text = _hour + ":" + _minute;
                                        rmtArrivalUtctime.Text = Master.CalculationTimeAdjustment(rmtArrivalUtctime.TextWithLiterals);

                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtArrivalUtctime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalUtctime);
                                        }
                                        if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtArrivalUtctime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalUtctime);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tbarrivalutcdate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, true);

                                            string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                            string startMins = "0000" + ldGmtArr.Minute.ToString();
                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtDep = CalculateGMTDep(ldGmtArr);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation
                                            if (DateValid)
                                            {

                                                CommonPreflightCalculation("Date", false, dt);

                                                //Ron Calc
                                                Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                Master.CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                Master.CalculateQuoteTotal();
                                                Master.BindStandardCharges(true, dgStandardCharges);
                                                Master.BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadLegDetails();
                                                //Ron Calc

                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tbarrivalutcdate")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalUtctime);
                                                    }
                                                }

                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtarrivalutctime")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalHomeDate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.ArrivalGreenwichDTTM != null)
                                                        {
                                                            DateTime dat = (DateTime)CurrLeg.ArrivalGreenwichDTTM;
                                                            string startHrsval = "0000" + dat.Hour.ToString();
                                                            string startMinsval = "0000" + dat.Minute.ToString();
                                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.ArrivalGreenwichDTTM);
                                                            rmtArrivalUtctime.Text = startHrsval.Substring(startHrs.Length - 2, 2) + ":" + startHrsval.Substring(startMinsval.Length - 2, 2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbArrivalHomeDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                        {
                            if (!Master.IsValidDate(tbArrivalHomeDate.Text))
                            {
                                tbArrivalHomeDate.Text = string.Empty;
                                #region restore old values
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.HomeArrivalDTTM != null)
                                        {
                                            DateTime dat = (DateTime)CurrLeg.HomeArrivalDTTM;
                                            string startHrsval = "0000" + dat.Hour.ToString();
                                            string startMinsval = "0000" + dat.Minute.ToString();
                                            tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.HomeArrivalDTTM);
                                            rmtArrivalHomeTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region "Date change calculation"
                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                {

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        //string StartTime = rmtArrivalUtctime.Text.Trim();
                                        //int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        //int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                        // Get and Set Time Fields
                                        string[] TimeArray = rmtArrivalHomeTime.TextWithLiterals.Split(':');
                                        string _hour = "00";
                                        string _minute = "00";
                                        if (!string.IsNullOrEmpty(TimeArray[0]))
                                            _hour = TimeArray[0];
                                        if (!string.IsNullOrEmpty(TimeArray[1]))
                                            _minute = TimeArray[1];

                                        int StartHrs = Convert.ToInt16(_hour);
                                        int StartMts = Convert.ToInt16(_minute);

                                        rmtArrivalHomeTime.Text = _hour + ":" + _minute;
                                        rmtArrivalHomeTime.Text = Master.CalculationTimeAdjustment(rmtArrivalHomeTime.TextWithLiterals);

                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtArrivalHomeTime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalHomeTime);

                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            calculate = false;
                                            rmtArrivalHomeTime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalHomeTime);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbArrivalHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tbarrivalhomedate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), dt, true, false);

                                            string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                            string startMins = "0000" + ldGmtArr.Minute.ToString();
                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtDep = CalculateGMTDep(ldGmtArr);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation

                                            if (DateValid)
                                            {
                                                CommonPreflightCalculation("Date", false, dt);

                                                //Ron Calc
                                                Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                Master.CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                Master.CalculateQuoteTotal();
                                                Master.BindStandardCharges(true, dgStandardCharges);
                                                Master.BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadLegDetails();
                                                //Ron Calc

                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tbarrivalhomedate")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalHomeTime);
                                                    }
                                                    //if (((TextBox)sender).ID.ToLower() == "rmtarrivalhometime")
                                                    //{
                                                    //    RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalHomeTime);
                                                    //}
                                                }

                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtarrivalhometime")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(chkEndOfDuty);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.HomeArrivalDTTM != null)
                                                        {
                                                            DateTime dat = (DateTime)CurrLeg.HomeArrivalDTTM;
                                                            string startHrsval = "0000" + dat.Hour.ToString();
                                                            string startMinsval = "0000" + dat.Minute.ToString();
                                                            tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CurrLeg.HomeArrivalDTTM);
                                                            rmtArrivalHomeTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void tbOverride_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbOverride.Text) && !string.IsNullOrEmpty(hdnCrewRules.Value))
                        {
                            bool isDepartureConfirmed = true;
                            if (lbDepartureDate.CssClass == "time_anchor")
                                isDepartureConfirmed = false;
                            CommonPreflightCalculation("CrewDutyRule", isDepartureConfirmed, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void tbCrewRules_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCrewRules);
                        lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnCrewRules.Value = string.Empty;
                        //Fix for SUP-380 (3174) Starts
                        tbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbCrewRulesCaption.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        string DutyDayStart = System.Web.HttpUtility.HtmlEncode("Duty Day Start : 0");
                        string DutyDayEnd = System.Web.HttpUtility.HtmlEncode("Duty Day End : 0");
                        string MaxDutyHrsAllowed = System.Web.HttpUtility.HtmlEncode("Max Duty Hrs Allowed : 0");
                        string MaxFlightHrsAllowed = System.Web.HttpUtility.HtmlEncode("Max Flight Hrs Allowed : 0");
                        string MinFixedRest = System.Web.HttpUtility.HtmlEncode("Min Fixed Rest : 0");

                        if (!string.IsNullOrEmpty(tbCrewRules.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();

                                var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                if (objRetVal.ReturnFlag)
                                {
                                    CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.ToString().ToUpper().Trim().Equals(tbCrewRules.Text.ToUpper().Trim())).ToList();
                                }
                                if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                {
                                    RadWindowManager1.RadAlert("warning- Changing the rules on this leg will change the rules for the entire duty day", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                    tbCrewRules.Text = CrewDtyRuleList[0].CrewDutyRuleCD;
                                    hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                    if (CrewDtyRuleList[0].FedAviatRegNum != null)
                                        lbFar.Text = System.Web.HttpUtility.HtmlEncode(CrewDtyRuleList[0].FedAviatRegNum);

                                    if (CrewDtyRuleList[0].DutyDayBeingTM != null)
                                    {
                                        hdnDutyDayStart.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayBeingTM).ToString("0.0"));
                                        DutyDayStart = "Duty Day Start : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayBeingTM).ToString("0.0"));
                                    }

                                    if (CrewDtyRuleList[0].DutyDayEndTM != null)
                                    {
                                        hdnDutyDayEnd.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayEndTM).ToString("0.0"));
                                        DutyDayEnd = "Duty Day End : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].DutyDayEndTM).ToString("0.0"));
                                    }

                                    if (CrewDtyRuleList[0].MaximumDutyHrs != null)
                                    {
                                        hdnMaxDutyHrsAllowed.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumDutyHrs).ToString("0.0"));
                                        MaxDutyHrsAllowed = "Max Duty Hrs Allowed : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumDutyHrs).ToString("0.0"));
                                    }

                                    if (CrewDtyRuleList[0].MaximumFlightHrs != null)
                                    {
                                        hdnMaxFlightHrsAllowed.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumFlightHrs).ToString("0.0"));
                                        MaxFlightHrsAllowed = "Max Flight Hrs Allowed : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MaximumFlightHrs).ToString("0.0"));
                                    }

                                    if (CrewDtyRuleList[0].MinimumRestHrs != null)
                                    {
                                        hdnMinFixedRest.Value = System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MinimumRestHrs).ToString("0.0"));
                                        MinFixedRest = "Min Fixed Rest : " + System.Web.HttpUtility.HtmlEncode(Convert.ToDecimal(CrewDtyRuleList[0].MinimumRestHrs).ToString("0.0"));
                                    }

                                    tbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                    lbCrewRules.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);
                                    lbCrewRulesCaption.ToolTip = System.Web.HttpUtility.HtmlEncode(DutyDayStart + "\n" + DutyDayEnd + "\n" + MaxDutyHrsAllowed + "\n" + MaxFlightHrsAllowed + "\n" + MinFixedRest);

                                    //Fix for SUP-380 (3174) Ends
                                    SetChangedCrewDutyRuleforOtherLegs();
                                    bool isDepartureConfirmed = true;
                                    if (lbDepartureDate.CssClass == "time_anchor")
                                        isDepartureConfirmed = false;
                                    CommonPreflightCalculation("CrewDutyRule", isDepartureConfirmed, null);
                                }
                                else
                                {
                                    lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode("Crew Duty Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewRules);
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("warning- Changing the rules on this leg will change the rules for the entire duty day", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void FeeGroup_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnFeeGroup);
                        hdnFeeGroupID.Value = string.Empty;
                        lbFeeGroupMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbFeeGroupDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        if (!string.IsNullOrEmpty(tbFeeGroup.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = Service.GetFeeGroupList().EntityList.Where(x => x.FeeGroupCD.ToString().ToUpper().Trim().Equals(tbFeeGroup.Text.ToUpper().Trim())).ToList(); ;
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    tbFeeGroup.Text = objRetVal[0].FeeGroupCD;
                                    hdnFeeGroupID.Value = objRetVal[0].FeeGroupID.ToString();
                                    if (objRetVal[0].FeeGroupDescription != null)
                                        lbFeeGroupDesc.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].FeeGroupDescription.ToUpper());

                                    RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(@"fnOpenFeeGroupWin('rdFeeGroup');");
                                }
                                else
                                {
                                    lbFeeGroupMsg.Text = System.Web.HttpUtility.HtmlEncode("Invalid Fee Group Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbFeeGroup);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void RON_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                if (!string.IsNullOrEmpty(tbRON.Text))
                {
                    decimal ron = 0.0M;

                    if (decimal.TryParse(tbRON.Text, out ron))
                    {
                        if (ron < 0)
                            ron = 0;
                        else if (ron > 99)
                            RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                        else
                        {
                            SaveCharterQuoteToSession();
                            Master.calculateRON(Convert.ToInt64(hdnLegNum.Value), "RON");
                            //ShowWarnMessage();
                            Master.CalculateQuantity();
                            Master.CalculateQuoteTotal();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                            {
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            }
                            LoadLegDetails();
                        }
                    }
                    else
                        tbRON.Text = "0";

                }


            }
        }

        protected void IntlStdCrew_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                SaveCharterQuoteToSession();
                Master.CalculateQuantity();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void DomStdCrew_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                SaveCharterQuoteToSession();
                Master.CalculateQuantity();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void TotPrepDeposit_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbTotPrepDeposit.Text, 19, 16, 2, "Total Prepaid/Deposit"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void TotalCost_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbTotalCost.Text, 19, 16, 2, "Total Fixed Cost"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void UsageAdj_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbUsageAdj.Text, 19, 16, 2, "Daliy Usage Adj"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void SegmentFees_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbSegmentFees.Text, 19, 16, 2, "Segment Fees"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void Discount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbDiscount.Text, 19, 16, 2, "Discount(%)"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void LandingFees_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbLandingFees.Text, 19, 16, 2, "Landing Fees"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void DiscountAmount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbDiscountAmount.Text, 19, 16, 2, "Discount Amount"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void Taxes_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbTaxes.Text, 19, 16, 2, "Taxes"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void TotalQuote_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Master.IsValidNumericFormat(tbTotalQuote.Text, 19, 16, 2, "Total Quote"))
                {
                    SaveCharterQuoteToSession();
                    Master.CalculateQuantity();
                    Master.CalculateQuoteTotal();
                    Master.BindStandardCharges(true, dgStandardCharges);
                    Master.BindLegs(dgLegs, true);
                    if (dgLegs.Items.Count > 0)
                    {
                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                    }
                    LoadLegDetails();
                }
            }
        }

        protected void tbPaxTotal_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                SaveCharterQuoteToSession();
                Master.CalculateQuantity();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        #endregion

        #region radio, checkbox change or click events
        protected void rblistWindReliability_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool isDepartureConfirmed = true;
                        if (lbDepartureDate.CssClass == "time_anchor")
                            isDepartureConfirmed = false;
                        CommonPreflightCalculation("WindReliability", isDepartureConfirmed, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(rblistWindReliability);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }

        }

        protected void chkList_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var selectedItem = chkList.SelectedItem;

                            if (selectedItem != null && selectedItem.Text.ToLower() == "taxable" && (bool)selectedItem.Selected)
                            {
                                bool isRural = false;
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.GetAllAirport> ArrAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                    var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnArrival.Value));

                                    if (objArrRetVal.ReturnFlag)
                                    {
                                        ArrAirportLists = objArrRetVal.EntityList;

                                        if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                        {

                                            if (ArrAirportLists[0].IsRural != null && (bool)ArrAirportLists[0].IsRural)
                                                isRural = true;

                                            if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                            {
                                                if (ArrAirportLists[0].CountryID == null)
                                                {
                                                    rbDomestic.SelectedValue = "1";
                                                }
                                                else
                                                {
                                                    if (ArrAirportLists[0].CountryID != null)
                                                    {
                                                        if (hdnCountryID.Value != ArrAirportLists[0].CountryID.ToString())
                                                        {
                                                            rbDomestic.SelectedValue = "2";
                                                        }


                                                    }
                                                }
                                            }
                                        }
                                    }
                                }



                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {
                                    var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnDepart.Value));

                                    if (objArrRetVal.ReturnFlag)
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                        AirportLists = objArrRetVal.EntityList;

                                        if (AirportLists != null && AirportLists.Count > 0)
                                        {
                                            if (AirportLists[0].IsRural != null && (bool)AirportLists[0].IsRural)
                                                isRural = true;

                                            if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                            {
                                                if (AirportLists[0].CountryID == null)
                                                {
                                                    rbDomestic.SelectedValue = "1";
                                                }
                                                else
                                                {
                                                    if (AirportLists[0].CountryID != null)
                                                    {
                                                        if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                        {
                                                            rbDomestic.SelectedValue = "2";
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }



                                if (QuoteInProgress != null)
                                {
                                    tbTaxRate.Text = "0.00";
                                    CQLeg Currleg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).SingleOrDefault();
                                    if (Currleg != null)
                                    {
                                        if (Currleg.State != 0)
                                            Currleg.State = CQRequestEntityState.Modified;

                                        if (isRural)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                Currleg.TaxRate = UserPrincipal.Identity._fpSettings._RuralTax;
                                        }
                                        else
                                        {
                                            if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                Currleg.TaxRate = UserPrincipal.Identity._fpSettings._CQFederalTax;
                                        }
                                        tbTaxRate.Text = Math.Round(Currleg.TaxRate.Value, 2).ToString();
                                    }

                                    Master.SaveQuoteinProgressToFileSession();
                                }

                                //Taxable Company profile Settings

                            }
                            else if (selectedItem == null)
                            {
                                if (QuoteInProgress != null)
                                {
                                    CQLeg Currleg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).SingleOrDefault();
                                    if (Currleg != null)
                                    {
                                        if (Currleg.State != 0)
                                            Currleg.State = CQRequestEntityState.Modified;
                                        Currleg.TaxRate = 0;
                                        tbTaxRate.Text = Math.Round(Currleg.TaxRate.Value, 2).ToString();
                                    }
                                    Master.SaveQuoteinProgressToFileSession();
                                }
                            }
                        }
                        SaveCharterQuoteToSession();
                        Master.CalculateQuantity();
                        Master.CalculateQuoteTotal();
                        Master.BindStandardCharges(true, dgStandardCharges);
                        Master.BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadLegDetails();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void TaxDaily_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        Master.CalculateQuantity();
                        Master.CalculateQuoteTotal();
                        Master.BindStandardCharges(true, dgStandardCharges);
                        Master.BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadLegDetails();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void TaxLandingFee_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        Master.CalculateQuantity();
                        Master.CalculateQuoteTotal();
                        Master.BindStandardCharges(true, dgStandardCharges);
                        Master.BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadLegDetails();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void EndOfDuty_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool isDepartureConfirmed = true;
                        if (lbDepartureDate.CssClass == "time_anchor")
                            isDepartureConfirmed = false;
                        CommonPreflightCalculation("CrewDutyRule", isDepartureConfirmed, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPurpose);

                        SaveCharterQuoteToSession();
                        Master.CalculateQuantity();
                        Master.CalculateQuoteTotal();
                        Master.BindStandardCharges(true, dgStandardCharges);
                        Master.BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadLegDetails();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        #endregion

        #region "button click events"

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            SaveCharterQuoteToSession();
                            Master.CalculateQuoteTotal();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                            {
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            }
                            LoadLegDetails();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btPowerSelect_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPowerSetting.SelectedItems.Count > 0)
                        {

                            GridDataItem item = (GridDataItem)dgPowerSetting.SelectedItems[0];
                            //Power,PowerSetting,TAS,HrRange,TOBias,LandBias
                            tbPower.Text = item.GetDataKeyValue("Power").ToString();
                            tbTAS.Text = item.GetDataKeyValue("TAS").ToString();
                            //tbBias.Text = item.GetDataKeyValue("HrRange").ToString();
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = "00:00";
                                tbBias.Text = item.GetDataKeyValue("TOBias").ToString();
                                tbBias.Text = ConvertTenthsToMins(tbBias.Text);
                            }
                            else
                            {
                                tbBias.Text = "0.0";
                                tbBias.Text = Math.Round((decimal)item.GetDataKeyValue("TOBias"), 1).ToString();

                            }
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Text = "00:00";
                                tbLandBias.Text = item.GetDataKeyValue("LandBias").ToString();
                                tbLandBias.Text = ConvertTenthsToMins(tbLandBias.Text);
                            }
                            else
                            {
                                tbLandBias.Text = "0.0";
                                tbLandBias.Text = Math.Round((decimal)item.GetDataKeyValue("LandBias"), 1).ToString();
                            }

                            rdPowerSetting.VisibleOnPageLoad = false;
                            bool isDepartureConfirmed = true;
                            if (lbDepartureDate.CssClass == "time_anchor")
                                isDepartureConfirmed = false;

                            CommonPreflightCalculation("Power", isDepartureConfirmed, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btPowerCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdPowerSetting.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btnCancelTOBias_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdTOBias.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btnOpenTOBiasPopup_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdTOBias.VisibleOnPageLoad = true;
                        lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(tbDepart.Text);
                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> airPortLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                            if (!string.IsNullOrEmpty(hdnDepart.Value))
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnDepart.Value));
                                if (objRetVal.ReturnFlag)
                                {
                                    airPortLists = objRetVal.EntityList;
                                    if (airPortLists.Count() > 0 && airPortLists != null)
                                    {
                                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CityName == null ? string.Empty : airPortLists[0].CityName.ToString());
                                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].StateName == null ? string.Empty : airPortLists[0].StateName.ToString());
                                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CountryName == null ? string.Empty : airPortLists[0].CountryName.ToString());
                                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].AirportName == null ? string.Empty : airPortLists[0].AirportName.ToString());
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btnSaveLandBias_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdLandBias.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btnCancelLandBias_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdLandBias.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void btnOpenLandBiasPopup_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdLandBias.VisibleOnPageLoad = true;
                        lbArrivesICAO.Text = System.Web.HttpUtility.HtmlEncode(tbArrival.Text);
                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> airPortLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                            if (!string.IsNullOrEmpty(hdnArrival.Value))
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnArrival.Value));
                                if (objRetVal.ReturnFlag)
                                {
                                    airPortLists = objRetVal.EntityList;

                                    if (airPortLists[0].IcaoID == lbArrivesICAO.Text)
                                    {
                                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CityName != null ? airPortLists[0].CityName : string.Empty);
                                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].StateName != null ? airPortLists[0].StateName : string.Empty);
                                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CountryName != null ? airPortLists[0].CountryName : string.Empty);
                                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].AirportName != null ? airPortLists[0].AirportName : string.Empty);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        #endregion

        protected void SaveCharterQuoteToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        //IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);

                        if (QuoteInProgress != null && QuoteInProgress.State != CQRequestEntityState.Deleted)
                        {
                            if (QuoteInProgress.CQMainID != 0)
                                QuoteInProgress.State = CQRequestEntityState.Modified;
                            else
                                QuoteInProgress.State = CQRequestEntityState.Added;

                            //if (QuoteInProgress.CQLegs == null || QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).Count() == 0)
                            //{
                            //    CQLeg firstleg = new CQLeg();
                            //    firstleg.LegNUM = 1;
                            //    if (!string.IsNullOrEmpty(hdnArrival.Value))
                            //    {
                            //        firstleg.AAirportID = Convert.ToInt64(hdnArrival.Value);
                            //        firstleg.ArrivalAirportChanged = true;
                            //    }
                            //    if (!string.IsNullOrEmpty(hdnDepart.Value))
                            //    {
                            //        firstleg.DepartAirportChanged = true;
                            //    }

                            //    firstleg.PassengerTotal = 0;
                            //    firstleg.IsDeleted = false;
                            //    firstleg.ShowWarningMessage = true;
                            //    hdnShowWarnMessage.Value = "true";
                            //    if (QuoteInProgress.CQLegs == null)
                            //        QuoteInProgress.CQLegs = new List<CQLeg>();

                            //    if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                            //        QuoteInProgress.State = CQRequestEntityState.Modified;
                            //    QuoteInProgress.CQLegs.Add(firstleg);
                            //}

                            CQLeg cqLeg = new CQLeg();

                            if (string.IsNullOrEmpty(hdnLegNum.Value))
                                hdnLegNum.Value = "1";


                            cqLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();
                            if (cqLeg != null)
                            {

                                bool SaveToSessionFleetDetails = false;
                                bool SaveToSessionLegDetails = false;

                                if (QuoteInProgress.CQFleetChargeDetails != null && QuoteInProgress.CQFleetChargeDetails.Count > 0)
                                    SaveToSessionFleetDetails = true;

                                #region "Leg Details"
                                if (cqLeg != null)
                                {
                                    if (cqLeg.CQLegID == 0)
                                        cqLeg.State = CharterQuoteService.CQRequestEntityState.Added;
                                    else
                                        cqLeg.State = CharterQuoteService.CQRequestEntityState.Modified;

                                    if (cqLeg.State == CQRequestEntityState.Modified)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.EditCQLeg))
                                        {
                                            SaveToSessionLegDetails = true;
                                        }
                                    }
                                    else if (cqLeg.State == CQRequestEntityState.Added)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLeg))
                                        {
                                            SaveToSessionLegDetails = true;
                                        }
                                    }

                                    if (SaveToSessionLegDetails)
                                    {
                                        #region "Leg Details"

                                        #region Departs

                                        CharterQuoteService.Airport DepTArr = new CharterQuoteService.Airport();
                                        if (!string.IsNullOrEmpty(hdnDepart.Value))
                                        {
                                            Int64 AirportID = 0;
                                            if (Int64.TryParse(hdnDepart.Value, out AirportID))
                                            {
                                                DepTArr.AirportID = AirportID;
                                                DepTArr.IcaoID = tbDepart.Text;
                                                DepTArr.AirportName = System.Web.HttpUtility.HtmlDecode(lbDepart.Text);
                                                string[] cityName = lbArrive.ToolTip.Split('\n');
                                                DepTArr.CityName =cityName[1].Split(':')[1];
                                                cqLeg.Airport1 = DepTArr;
                                                cqLeg.DAirportID = AirportID;
                                            }
                                            else
                                            {
                                                cqLeg.Airport1 = null;
                                                cqLeg.DAirportID = null;
                                            }
                                        }
                                        else
                                        {
                                            cqLeg.Airport1 = null;
                                            cqLeg.DAirportID = null;
                                        }


                                        if (!string.IsNullOrEmpty(tbLocalDate.Text))
                                        {
                                            string StartTime = rmtlocaltime.Text.Trim();
                                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                            DateTime dt = DateTime.ParseExact(tbLocalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            cqLeg.DepartureDTTMLocal = dt;

                                        }

                                        if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                        {
                                            string Timestr = rmtUtctime.Text.Trim();
                                            int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                            int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                            DateTime dt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            dt = dt.AddHours(Hrst);
                                            dt = dt.AddMinutes(Mtstr);
                                            cqLeg.DepartureGreenwichDTTM = dt;
                                        }

                                        if (!string.IsNullOrEmpty(tbHomeDate.Text))
                                        {
                                            string Timestr = rmtHomeTime.Text.Trim();
                                            int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                            int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                            DateTime dt = DateTime.ParseExact(tbHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            dt = dt.AddHours(Hrst);
                                            dt = dt.AddMinutes(Mtstr);
                                            cqLeg.HomeDepartureDTTM = dt;
                                        }

                                        #endregion

                                        #region Arrives

                                        CharterQuoteService.Airport ArrvArr = new CharterQuoteService.Airport();
                                        if (!string.IsNullOrEmpty(hdnArrival.Value))
                                        {
                                            Int64 AirportArriveID = 0;
                                            if (Int64.TryParse(hdnArrival.Value, out AirportArriveID))
                                            {
                                                ArrvArr.AirportID = AirportArriveID;
                                                ArrvArr.IcaoID = tbArrival.Text;
                                                ArrvArr.AirportName = System.Web.HttpUtility.HtmlDecode(lbArrival.Text);
                                                string[] cityName = lbDepartIcao.ToolTip.Split('\n');
                                                ArrvArr.CityName = cityName[1].Split(':')[1];
                                                cqLeg.Airport = ArrvArr;
                                                cqLeg.AAirportID = AirportArriveID;
                                            }
                                            else
                                            {
                                                cqLeg.Airport = null;
                                                cqLeg.AAirportID = null;
                                            }
                                        }
                                        else
                                        {
                                            cqLeg.Airport = null;
                                            cqLeg.AAirportID = null;
                                        }

                                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                                        {
                                            string Timestr = rmtArrivalTime.Text.Trim();
                                            int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                            int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                            DateTime dt = DateTime.ParseExact(tbArrivalDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            dt = dt.AddHours(Hrst);
                                            dt = dt.AddMinutes(Mtstr);
                                            cqLeg.ArrivalDTTMLocal = dt;
                                        }

                                        if (!string.IsNullOrEmpty(tbArrivalUtcDate.Text))
                                        {
                                            string Timestr = rmtArrivalUtctime.Text.Trim();
                                            int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                            int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                            DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            dt = dt.AddHours(Hrst);
                                            dt = dt.AddMinutes(Mtstr);
                                            cqLeg.ArrivalGreenwichDTTM = dt;
                                        }

                                        if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                                        {
                                            string Timestr = rmtArrivalHomeTime.Text.Trim();
                                            int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                            int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                            DateTime dt = DateTime.ParseExact(tbArrivalHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            dt = dt.AddHours(Hrst);
                                            dt = dt.AddMinutes(Mtstr);
                                            cqLeg.HomeArrivalDTTM = dt;
                                        }

                                        #endregion

                                        #region Distance & Time

                                        decimal distance = 0;
                                        if (decimal.TryParse(hdnMiles.Value, out distance))
                                            cqLeg.Distance = distance;
                                        else
                                            cqLeg.Distance = 0;

                                        decimal tobias = 0;
                                        string TakeOffBiasStr = string.Empty;

                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "3");
                                        }
                                        else
                                            TakeOffBiasStr = tbBias.Text;
                                        if (decimal.TryParse(TakeOffBiasStr, out tobias))
                                            cqLeg.TakeoffBIAS = tobias;
                                        else
                                            cqLeg.TakeoffBIAS = 0;


                                        decimal toTas = 0;
                                        if (decimal.TryParse(tbTAS.Text, out toTas))
                                            cqLeg.TrueAirSpeed = toTas;
                                        else
                                            cqLeg.TrueAirSpeed = 0;


                                        int windreliability = 0;
                                        if (rblistWindReliability.Items.Count > 0 && rblistWindReliability.SelectedItem != null)
                                        {
                                            if (int.TryParse(rblistWindReliability.SelectedItem.Value, out windreliability))
                                                cqLeg.WindReliability = windreliability;
                                            else
                                                cqLeg.WindReliability = null;
                                        }

                                        cqLeg.PowerSetting = tbPower.Text;


                                        decimal LandingBias = 0;
                                        string LandingBiasStr = string.Empty;

                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                                        }
                                        else
                                            LandingBiasStr = tbLandBias.Text;

                                        if (decimal.TryParse(LandingBiasStr, out LandingBias))
                                        {
                                            cqLeg.LandingBIAS = LandingBias;
                                        }
                                        else
                                        {
                                            cqLeg.LandingBIAS = 0;
                                        }

                                        decimal WindsBoeingTable = 0;
                                        if (decimal.TryParse(tbWind.Text, out WindsBoeingTable))
                                            cqLeg.WindsBoeingTable = WindsBoeingTable;
                                        else
                                            cqLeg.WindsBoeingTable = 0;


                                        cqLeg.ElapseTM = 0;

                                        decimal ElapseTime = 0;
                                        if (!string.IsNullOrEmpty(tbETE.Text))
                                        {
                                            string ETEStr = "0";
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                ETEStr = directMinstoTenths(tbETE.Text);
                                            }
                                            else
                                                ETEStr = tbETE.Text;
                                            if (decimal.TryParse(ETEStr, out ElapseTime))
                                                cqLeg.ElapseTM = ElapseTime;

                                        }

                                        #endregion

                                        #region Duty

                                        CharterQuoteService.CrewDutyRule CrewDtyRule = new CharterQuoteService.CrewDutyRule();
                                        if (!string.IsNullOrEmpty(hdnCrewRules.Value) && hdnCrewRules.Value != "0")
                                        {
                                            Int64 CrewDutyRulesID = 0;
                                            if (Int64.TryParse(hdnCrewRules.Value, out CrewDutyRulesID))
                                            {
                                                CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                                CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                                CrewDtyRule.CrewDutyRulesDescription = " ";
                                                if (!string.IsNullOrEmpty(hdnDutyDayStart.Value))
                                                    CrewDtyRule.DutyDayBeingTM = Convert.ToDecimal(hdnDutyDayStart.Value);

                                                if (!string.IsNullOrEmpty(hdnDutyDayEnd.Value))
                                                    CrewDtyRule.DutyDayEndTM = Convert.ToDecimal(hdnDutyDayEnd.Value);

                                                if (!string.IsNullOrEmpty(hdnMaxDutyHrsAllowed.Value))
                                                    CrewDtyRule.MaximumDutyHrs = Convert.ToDecimal(hdnMaxDutyHrsAllowed.Value);

                                                if (!string.IsNullOrEmpty(hdnMaxFlightHrsAllowed.Value))
                                                    CrewDtyRule.MaximumFlightHrs = Convert.ToDecimal(hdnMaxFlightHrsAllowed.Value);

                                                if (!string.IsNullOrEmpty(hdnMinFixedRest.Value))
                                                    CrewDtyRule.MinimumRestHrs = Convert.ToDecimal(hdnMinFixedRest.Value);

                                                cqLeg.CrewDutyRule = CrewDtyRule;
                                                cqLeg.CrewDutyRulesID = CrewDutyRulesID;
                                            }
                                            else
                                            {
                                                cqLeg.CrewDutyRule = null;
                                                cqLeg.CrewDutyRulesID = null;
                                            }
                                        }
                                        else
                                        {
                                            cqLeg.CrewDutyRule = null;
                                            cqLeg.CrewDutyRulesID = null;
                                        }
                                        cqLeg.IsDutyEnd = chkEndOfDuty.Checked;

                                        decimal Overrideval = 0;
                                        string CQOverRide = "0";

                                        if (!string.IsNullOrEmpty(tbOverride.Text))
                                        {
                                            CQOverRide = tbOverride.Text;
                                        }
                                        if (decimal.TryParse(CQOverRide, out Overrideval))
                                            cqLeg.CQOverRide = Overrideval;
                                        else
                                            cqLeg.CQOverRide = 0;

                                        if (!string.IsNullOrEmpty(lbFar.Text))
                                            cqLeg.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(lbFar.Text);

                                        decimal DutyHours = 0;
                                        string DutyStr = "0";
                                        cqLeg.DutyHours = 0;
                                        if (!string.IsNullOrEmpty(lbTotalDuty.Text))
                                        {
                                            DutyStr = System.Web.HttpUtility.HtmlDecode(lbTotalDuty.Text);

                                            if (decimal.TryParse(DutyStr, out DutyHours))
                                            {

                                                cqLeg.DutyHours = Math.Round(DutyHours, 1);
                                            }
                                        }



                                        decimal FlightHours = 0;
                                        string FlightHourstr = "0";
                                        cqLeg.FlightHours = 0;
                                        if (!string.IsNullOrEmpty(lbTotalFlight.Text))
                                        {
                                            FlightHourstr = System.Web.HttpUtility.HtmlDecode(lbTotalFlight.Text);
                                            if (decimal.TryParse(FlightHourstr, out FlightHours))
                                                cqLeg.FlightHours = Math.Round(FlightHours, 1);
                                        }


                                        decimal RestHours = 0;
                                        string RestHoursrstr = "0";
                                        cqLeg.RestHours = 0;
                                        if (!string.IsNullOrEmpty(lbRest.Text))
                                        {
                                            RestHoursrstr = System.Web.HttpUtility.HtmlDecode(lbRest.Text);
                                            if (decimal.TryParse(RestHoursrstr, out RestHours))
                                                cqLeg.RestHours = Math.Round(RestHours, 2);
                                        }


                                        #endregion

                                        #region FlightDetails

                                        decimal chargetotal = 0;
                                        if (decimal.TryParse(tbCharges.Text, out chargetotal))
                                            cqLeg.ChargeTotal = chargetotal;
                                        else
                                            cqLeg.ChargeTotal = 0;

                                        decimal rate = 0;
                                        if (decimal.TryParse(tbRate.Text, out rate))
                                            cqLeg.ChargeRate = rate;
                                        else
                                            cqLeg.ChargeRate = 0;


                                        decimal taxrate = 0;
                                        if (decimal.TryParse(tbTaxRate.Text, out taxrate))
                                            cqLeg.TaxRate = taxrate;
                                        else
                                            cqLeg.TaxRate = 0;

                                        // Fix for UW-1117 (8052)
                                        decimal paxtotal = 0;
                                        if (decimal.TryParse(tbPaxTotal.Text, out paxtotal))
                                            cqLeg.PassengerTotal = paxtotal;

                                        if (rbDomestic.SelectedValue == "1")
                                            cqLeg.DutyTYPE = 1;
                                        else
                                            cqLeg.DutyTYPE = 2;

                                        #endregion

                                        #region LastPart


                                        if (QuoteInProgress.Fleet != null)
                                        {
                                            if (QuoteInProgress.Fleet.MaximumPassenger != null && QuoteInProgress.Fleet.MaximumPassenger > 0)
                                            {
                                                if (cqLeg.State == CQRequestEntityState.Added)
                                                {
                                                    Int32 SeatTotal = 0;
                                                    SeatTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);

                                                    if (cqLeg.PassengerTotal != null)
                                                        cqLeg.ReservationAvailable = SeatTotal - cqLeg.PassengerTotal;
                                                    else
                                                        cqLeg.ReservationAvailable = SeatTotal;
                                                }
                                            }
                                        }

                                        //if (cqLeg.PassengerTotal != null)
                                        //    tbPax.Text = cqLeg.PassengerTotal.ToString();

                                        if (!string.IsNullOrEmpty(tbPurpose.Text))
                                            cqLeg.FlightPurposeDescription = tbPurpose.Text;

                                        #endregion

                                        #region "RON"
                                        cqLeg.DayRONCNT = 0;
                                        cqLeg.RemainOverNightCNT = 0;

                                        if (!string.IsNullOrEmpty(tbDayRoom.Text))
                                            cqLeg.DayRONCNT = Convert.ToDecimal(tbDayRoom.Text);
                                        if (!string.IsNullOrEmpty(tbRON.Text))
                                            cqLeg.RemainOverNightCNT = Convert.ToInt16(tbRON.Text);

                                        #endregion

                                        #region "POS"
                                        cqLeg.IsPositioning = chkList.Items[0].Selected;
                                        cqLeg.IsTaxable = chkList.Items[1].Selected;
                                        #endregion
                                        #endregion


                                    }

                                }
                                #endregion

                                if (SaveToSessionFleetDetails)
                                {
                                    #region "Quote Details"

                                    #region "Extra Part"

                                    if (!string.IsNullOrEmpty(tbDomStdCrew.Text))
                                        QuoteInProgress.StandardCrewDOM = Convert.ToDecimal(tbDomStdCrew.Text);
                                    else
                                        QuoteInProgress.StandardCrewDOM = 0.0M;
                                    if (!string.IsNullOrEmpty(tbIntlStdCrew.Text))
                                        QuoteInProgress.StandardCrewINTL = Convert.ToDecimal(tbIntlStdCrew.Text);
                                    else
                                        QuoteInProgress.StandardCrewINTL = 0.0M;
                                    if (!string.IsNullOrEmpty(tbTotalCost.Text))
                                        QuoteInProgress.CostTotal = Convert.ToDecimal(tbTotalCost.Text);
                                    else
                                        QuoteInProgress.CostTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbTotPrepDeposit.Text))
                                        QuoteInProgress.PrepayTotal = Convert.ToDecimal(tbTotPrepDeposit.Text);
                                    else
                                        QuoteInProgress.PrepayTotal = 0.0M;

                                    #endregion

                                    #region "Charges And Fee"


                                    CharterQuoteService.FeeGroup objFeeGrp = new CharterQuoteService.FeeGroup();
                                    if (!string.IsNullOrEmpty(hdnFeeGroupID.Value))
                                    {
                                        objFeeGrp.FeeGroupID = Convert.ToInt64(hdnFeeGroupID.Value);
                                        objFeeGrp.FeeGroupCD = tbFeeGroup.Text;
                                        objFeeGrp.FeeGroupDescription = System.Web.HttpUtility.HtmlDecode(lbFeeGroupDesc.Text);
                                        QuoteInProgress.FeeGroup = objFeeGrp;
                                        QuoteInProgress.FeeGroupID = Convert.ToInt64(hdnFeeGroupID.Value);
                                    }
                                    else
                                    {
                                        QuoteInProgress.FeeGroup = null;
                                        QuoteInProgress.FeeGroupID = null;
                                    }

                                    QuoteInProgress.IsDailyTaxAdj = chkTaxDaily.Checked;
                                    QuoteInProgress.IsLandingFeeTax = chkTaxLandingFee.Checked;

                                    #endregion

                                    #region "Charges"

                                    if (!string.IsNullOrEmpty(tbTotDays.Text))
                                        QuoteInProgress.DaysTotal = Convert.ToDecimal(tbTotDays.Text);
                                    else
                                        QuoteInProgress.DaysTotal = 0.0M;

                                    if (!string.IsNullOrEmpty(tbFltHrs.Text))
                                    {
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            QuoteInProgress.FlightHoursTotal = Convert.ToDecimal(ConvertMinToTenths(tbFltHrs.Text, true, "3"));
                                        else
                                            QuoteInProgress.FlightHoursTotal = Convert.ToDecimal(tbFltHrs.Text);
                                    }
                                    else
                                        QuoteInProgress.FlightHoursTotal = 0.0M;

                                    if (!string.IsNullOrEmpty(tbAvgHrs.Text))
                                    {
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            QuoteInProgress.AverageFlightHoursTotal = Convert.ToDecimal(ConvertMinToTenths(tbAvgHrs.Text, true, "3"));
                                        else
                                            QuoteInProgress.AverageFlightHoursTotal = Convert.ToDecimal(tbAvgHrs.Text);
                                    }
                                    else
                                        QuoteInProgress.AverageFlightHoursTotal = 0.0M;

                                    if (!string.IsNullOrEmpty(tbAdjustedHrs.Text))
                                    {
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            QuoteInProgress.AdjAmtTotal = Convert.ToDecimal(ConvertMinToTenths(tbAdjustedHrs.Text, true, "3"));
                                        else
                                            QuoteInProgress.AdjAmtTotal = Convert.ToDecimal(tbAdjustedHrs.Text);
                                    }
                                    else
                                        QuoteInProgress.AdjAmtTotal = 0.0M;

                                    if (!string.IsNullOrEmpty(tbUsageAdj.Text))
                                        QuoteInProgress.UsageAdjTotal = Convert.ToDecimal(tbUsageAdj.Text);
                                    else
                                        QuoteInProgress.UsageAdjTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbSegmentFees.Text))
                                        QuoteInProgress.SegmentFeeTotal = Convert.ToDecimal(tbSegmentFees.Text);
                                    else
                                        QuoteInProgress.SegmentFeeTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbDiscount.Text))
                                        QuoteInProgress.DiscountPercentageTotal = Convert.ToDecimal(tbDiscount.Text);
                                    else
                                        QuoteInProgress.DiscountPercentageTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbLandingFees.Text))
                                        QuoteInProgress.LandingFeeTotal = Convert.ToDecimal(tbLandingFees.Text);
                                    else
                                        QuoteInProgress.LandingFeeTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbDiscountAmount.Text))
                                        QuoteInProgress.DiscountAmtTotal = Convert.ToDecimal(tbDiscountAmount.Text);
                                    else
                                        QuoteInProgress.DiscountAmtTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbMarginAmount.Text))
                                        QuoteInProgress.MarginTotal = Convert.ToDecimal(tbMarginAmount.Text);
                                    else
                                        QuoteInProgress.MarginTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbMargin.Text))
                                        QuoteInProgress.MarginalPercentTotal = Convert.ToDecimal(tbMargin.Text);
                                    else
                                        QuoteInProgress.MarginalPercentTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbTaxes.Text))
                                        QuoteInProgress.TaxesTotal = Convert.ToDecimal(tbTaxes.Text);
                                    else
                                        QuoteInProgress.TaxesTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbAdditionalFees.Text))
                                        QuoteInProgress.AdditionalFeeTotalD = Convert.ToDecimal(tbAdditionalFees.Text);
                                    else
                                        QuoteInProgress.AdditionalFeeTotalD = 0.0M;
                                    if (!string.IsNullOrEmpty(tbFlightCharges.Text))
                                        QuoteInProgress.FlightChargeTotal = Convert.ToDecimal(tbFlightCharges.Text);
                                    else
                                        QuoteInProgress.FlightChargeTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbSubTotal.Text))
                                        QuoteInProgress.SubtotalTotal = Convert.ToDecimal(tbSubTotal.Text);
                                    else
                                        QuoteInProgress.SubtotalTotal = 0.0M;
                                    if (!string.IsNullOrEmpty(tbTotalQuote.Text))
                                        QuoteInProgress.QuoteTotal = Convert.ToDecimal(tbTotalQuote.Text);
                                    else
                                        QuoteInProgress.QuoteTotal = 0.0M;
                                    #endregion

                                    #endregion
                                }
                                Master.SaveQuoteinProgressToFileSession();
                                Master.BindStandardCharges(true, dgStandardCharges);
                                Master.TrackerLegs.Rebind();
                            }
                        }
                    }
                }
            }
        }

        #region Permissions
        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.CharterQuote.ViewCQLeg);
                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                // Check for Field Level Permissions
                if (IsAuthorized(Permission.CharterQuote.ViewCQLeg))
                {
                    // Show read-only form
                    EnableForm(false);

                    GridEnable(false, false, false);

                    // Check Main Page Controls
                    if (IsAuthorized(Permission.CharterQuote.AddCQLeg) || IsAuthorized(Permission.CharterQuote.EditCQLeg))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQLeg))
                    {
                        ControlVisibility("Button", btnDelete, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.EditCQLeg))
                    {
                        ControlVisibility("Button", btnEditFile, true);
                    }
                    // Check Page Controls - Disable/Enable
                    if (Session[Master.CQSessionKey] != null)
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];

                        if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                        {
                            if (dgLegs.Items != null && dgLegs.Items.Count > 0)
                                EnableForm(true);
                            else
                                EnableForm(false);

                            GridEnable(true, true, true);

                            if (IsAuthorized(Permission.CharterQuote.AddCQFile) || IsAuthorized(Permission.CharterQuote.EditCQFile))
                            {
                                ControlEnable("Button", btnAddLeg, true, "ui_nav");
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                            }
                            ControlEnable("Button", btnDelete, false, "button-disable");
                            ControlEnable("Button", btnEditFile, false, "button-disable");
                            ControlEnable("Button", btnInsertLeg, true, "ui_nav");
                            ControlEnable("Button", btnDeleteLeg, true, "ui_nav");
                        }
                        else
                        {
                            ControlEnable("Button", btnAddLeg, false, "ui_nav_disable");
                            ControlEnable("Button", btnInsertLeg, false, "ui_nav_disable");
                            ControlEnable("Button", btnDeleteLeg, false, "ui_nav_disable");
                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDelete, true, "button");
                            ControlEnable("Button", btnEditFile, true, "button");
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnAddLeg, false, "ui_nav_disable");
                        ControlEnable("Button", btnInsertLeg, false, "ui_nav_disable");
                        ControlEnable("Button", btnDeleteLeg, false, "ui_nav_disable");
                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                    }
                }
            }
        }

        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "RadNumericTextBox":
                        RadNumericTextBox rtxtb = (RadNumericTextBox)ctrlName;
                        rtxtb.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            rtxtb.CssClass = cssClass;
                        break;
                    case "TextBox":
                        if (ctrlName != null && ctrlName.GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                        {
                            RadNumericTextBox rtxt = (RadNumericTextBox)ctrlName;
                            rtxt.Enabled = isEnable;
                            if (!string.IsNullOrEmpty(cssClass))
                                rtxt.CssClass = cssClass;
                        }
                        else
                        {
                            TextBox txt = (TextBox)ctrlName;
                            txt.Enabled = isEnable;
                            if (!string.IsNullOrEmpty(cssClass))
                                txt.CssClass = cssClass;
                        }
                        break;
                    default: break;
                }
            }
        }
        #endregion

        #region Button Events

        /// <summary>
        /// To delete the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        Master.Delete((int)FileRequest.FileNUM);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        /// <summary>
        /// To cancel the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        /// <summary>
        /// To save the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lbcvDepart.Text == string.Empty && lbcvArrival.Text == string.Empty && lbcvPower.Text == string.Empty && lbcvCrewRules.Text == string.Empty && lbFeeGroupMsg.Text == string.Empty)
                        {
                            SaveCharterQuoteToSession();
                            Master.CalculateQuoteTotal();

                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            Master.Save(FileRequest);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        /// <summary>
        /// To move next tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Next_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        Master.CalculateQuoteTotal();
                        Master.Next("Quote & Leg Details");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        #endregion

        #region AJAX Events
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.ToString() == "btPowerSelect_Click")
            {
                btPowerSelect_Click(sender, e);
            }
            if (e.Argument.ToString() == "tbDepart_TextChanged")
            {
                tbDepart_TextChanged(sender, e);
            }
            if (e.Argument.ToString() == "tbArrive_TextChanged")
            {
                tbArrive_TextChanged(sender, e);
            }
            if (e.Argument.ToString() == "tbPower_TextChanged")
            {
                tbPower_TextChanged(sender, e);
            }
            if (e.Argument.ToString() == "FeeGroup_TextChanged")
            {
                FeeGroup_TextChanged(sender, e);
            }
            if (e.Argument.ToString() == "tbCrewRules_TextChanged")
            {
                tbCrewRules_TextChanged(sender, e);
            }
        }
        #endregion

        #region Standard Charges and Additional Fees Section

        protected void StandardCharges_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.BindStandardCharges(false, dgStandardCharges);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void StandardCharges_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;

                                Int32 _orderNum = 0;

                                Int64 QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                                {
                                    if (QuoteInProgress.CQFleetChargeDetails.Count == 0)
                                        _orderNum = 4;
                                    else
                                    {
                                        if (QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).Max(x => x.OrderNUM).HasValue)
                                            _orderNum = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).Max(x => x.OrderNUM).Value;

                                        if (_orderNum <= 3)
                                            _orderNum = 4;
                                        else
                                            _orderNum += 1;
                                    }

                                    CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                    fleetCharge.State = CQRequestEntityState.Added;
                                    fleetCharge.CQFlightChargeDescription = string.Empty;
                                    fleetCharge.ChargeUnit = "Fixed";
                                    fleetCharge.NegotiatedChgUnit = 5;
                                    fleetCharge.Quantity = 0.0M;
                                    fleetCharge.BuyDOM = 0.0M;
                                    fleetCharge.SellDOM = 0.0M;
                                    fleetCharge.IsTaxDOM = false;
                                    fleetCharge.IsDiscountDOM = false;
                                    fleetCharge.BuyIntl = 0.0M;
                                    fleetCharge.SellIntl = 0.0M;
                                    fleetCharge.IsTaxIntl = false;
                                    fleetCharge.IsDiscountIntl = false;
                                    fleetCharge.FeeAMT = 0.0M;
                                    fleetCharge.OrderNUM = _orderNum;
                                    fleetCharge.IsDeleted = false;

                                    QuoteInProgress.CQFleetChargeDetails.Add(fleetCharge);

                                    Master.SaveQuoteinProgressToFileSession();
                                    Master.BindStandardCharges(true, dgStandardCharges);
                                    Master.CalculateQuantity();
                                    Master.CalculateQuoteTotal();


                                }

                                break;
                            case RadGrid.DeleteSelectedCommandName:

                                // Updated for UW-838
                                foreach (GridDataItem selItem in dgStandardCharges.Items)
                                {
                                    CheckBox chkSelect = (CheckBox)selItem.FindControl("chkSelect");
                                    if (chkSelect.Checked)
                                    {
                                        //GridDataItem selItem = dgStandardCharges.SelectedItems[0] as GridDataItem;
                                        Int32 orderNum = 0;

                                        if (selItem.GetDataKeyValue("OrderNUM") != null)
                                            orderNum = Convert.ToInt32(selItem.GetDataKeyValue("OrderNUM").ToString());

                                        // Condition to check the Mandatory Charges, Not allow to Delete.
                                        if (orderNum != 0)
                                        {
                                            //if (orderNum == 1 || orderNum == 2 || orderNum == 3)
                                            //    RadWindowManager1.RadAlert(((TextBox)selItem.FindControl("tbDescription")).Text + " is a mandatory charge. Cannot be deleted.", 330, 100, ModuleNameConstants.CharterQuote.CQLeg, null);
                                            //else
                                            DeleteCharge(orderNum);

                                            //Master.BindStandardCharges(true, dgStandardCharges);
                                        }
                                    }
                                }
                                Master.BindStandardCharges(true, dgStandardCharges);
                                break;

                            case "RowClick":
                                //GridDataItem item = (GridDataItem)e.Item;
                                //MarkLegItemSelected(Convert.ToInt64(item["LegNUM"].Text));
                                break;

                            default:
                                break;

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        /// <summary>
        /// Method to Delete Standard Charges
        /// </summary>
        /// <param name="quoteNum"></param>
        protected void DeleteCharge(Int64 orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null && QuoteInProgress.CQFleetChargeDetails.Count > 0)
                {
                    CQFleetChargeDetail ChargeToDelete = new CQFleetChargeDetail();

                    ChargeToDelete = QuoteInProgress.CQFleetChargeDetails.Where(x => x.OrderNUM == orderNum && x.IsDeleted == false).FirstOrDefault();

                    if (ChargeToDelete != null)
                    {
                        if (ChargeToDelete.CQFleetChargeDetailID != 0)
                        {
                            ChargeToDelete.IsDeleted = true;
                            ChargeToDelete.State = CQRequestEntityState.Deleted;
                        }
                        else
                            QuoteInProgress.CQFleetChargeDetails.Remove(ChargeToDelete);

                        Master.SaveQuoteinProgressToFileSession();
                    }
                }
            }
        }

        protected void ConfirmFeeGroupYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

            }
        }

        protected void Description_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as TextBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void Quantity_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as TextBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void BuyDOM_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem;
                if (sender != null && sender.GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                {
                    selItem = (sender as RadNumericTextBox).NamingContainer as GridEditableItem;
                }
                else
                {
                    selItem = (sender as TextBox).NamingContainer as GridEditableItem;
                }
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void SellDOM_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as RadNumericTextBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void TaxDOM_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as CheckBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void DiscountDOM_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as CheckBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void BuyIntl_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem;
                if (sender != null && sender.GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                {
                    selItem = (sender as RadNumericTextBox).NamingContainer as GridEditableItem;
                }
                else
                {
                    selItem = (sender as TextBox).NamingContainer as GridEditableItem;
                }
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void SellIntl_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as RadNumericTextBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void TaxIntl_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as CheckBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void DiscountIntl_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                GridEditableItem selItem = (sender as CheckBox).NamingContainer as GridEditableItem;
                UpdateFleetChargeDetailsByOrderNum(selItem);

                SaveCharterQuoteToSession();
                Master.CalculateQuoteTotal();
                Master.BindStandardCharges(true, dgStandardCharges);
                Master.BindLegs(dgLegs, true);
                if (dgLegs.Items.Count > 0)
                {
                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                }
                LoadLegDetails();
            }
        }

        protected void ChargeUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem selItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        Int32 _orderNum = 0;
                        if (selItem.GetDataKeyValue("OrderNUM") != null)
                            _orderNum = Convert.ToInt32(selItem.GetDataKeyValue("OrderNUM").ToString());

                        if (selItem.FindControl("ddlChargeUnit") != null)
                        {
                            DropDownList ddlChargeUnit = (DropDownList)selItem.FindControl("ddlChargeUnit");

                            if (ddlChargeUnit.SelectedValue == "4" || ddlChargeUnit.SelectedValue == "5" || ddlChargeUnit.SelectedValue == "6")
                            {
                                TextBox tbQuantity = ((TextBox)selItem.FindControl("tbQuantity"));
                                tbQuantity.Text = "0.0";
                            }
                        }

                        UpdateFleetChargeDetailsByOrderNum(selItem);

                        CalculateQuantityByOrderNum(_orderNum);

                        Master.CalculateQuoteTotal();
                        Master.BindStandardCharges(true, dgStandardCharges);
                        Master.BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadLegDetails();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        private void UpdateFleetChargeDetailsByOrderNum(GridEditableItem selItem)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selItem))
            {
                //GridEditableItem selItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                //int index = selItem.ItemIndex;

                Int32 _orderNum = 0;
                string _description = string.Empty;
                string _chargeUnit = string.Empty;
                decimal _chargeUnitVal = 0;
                decimal _quantity = 0;
                decimal _buyDOM = 0;
                decimal _sellDOM = 0;
                bool _isTaxDOM = false;
                bool _isDiscountDOM = false;
                decimal _buyIntl = 0;
                decimal _sellIntl = 0;
                bool _isTaxIntl = false;
                bool _isDiscountIntl = false;
                decimal _feeAMT = 0;

                if (selItem.FindControl("ddlChargeUnit") != null)
                {
                    DropDownList ddlChargeUnit = selItem.FindControl("ddlChargeUnit") as DropDownList;
                    _chargeUnitVal = Convert.ToDecimal(ddlChargeUnit.SelectedValue);
                    _chargeUnit = ddlChargeUnit.SelectedItem.Text;
                }

                if (selItem.GetDataKeyValue("OrderNUM") != null)
                    _orderNum = Convert.ToInt32(selItem.GetDataKeyValue("OrderNUM").ToString());

                if (selItem.FindControl("tbDescription") != null)
                    _description = ((TextBox)selItem.FindControl("tbDescription")).Text;

                if (selItem.FindControl("tbQuantity") != null)
                    _quantity = Math.Round(Convert.ToDecimal(((TextBox)selItem.FindControl("tbQuantity")).Text), 2);

                if (selItem.FindControl("tbBuyDOM") != null)
                {
                    if (selItem.FindControl("tbBuyDOM").GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                        _buyDOM = Math.Round(Convert.ToDecimal(((RadNumericTextBox)selItem.FindControl("tbBuyDOM")).Text), 2);
                    else
                        _buyDOM = Math.Round(Convert.ToDecimal(((TextBox)selItem.FindControl("tbBuyDOM")).Text), 2);
                }

                if (selItem.FindControl("tbSellDOM") != null)

                    if (selItem.FindControl("tbSellDOM").GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                        _sellDOM = Math.Round(Convert.ToDecimal(((RadNumericTextBox)selItem.FindControl("tbSellDOM")).Text), 2);
                    else
                        _sellDOM = Math.Round(Convert.ToDecimal(((TextBox)selItem.FindControl("tbSellDOM")).Text), 2);

                if (selItem.FindControl("chkTaxDOM") != null)
                    _isTaxDOM = ((CheckBox)selItem.FindControl("chkTaxDOM")).Checked;

                if (selItem.FindControl("chkDiscountDOM") != null)
                    _isDiscountDOM = ((CheckBox)selItem.FindControl("chkDiscountDOM")).Checked;

                if (selItem.FindControl("tbBuyIntl") != null)
                {
                    if (selItem.FindControl("tbBuyIntl").GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                        _buyIntl = Math.Round(Convert.ToDecimal(((RadNumericTextBox)selItem.FindControl("tbBuyIntl")).Text), 2);
                    else
                        _buyIntl = Math.Round(Convert.ToDecimal(((TextBox)selItem.FindControl("tbBuyIntl")).Text), 2);
                }

                if (selItem.FindControl("tbSellIntl") != null)
                    if (selItem.FindControl("tbSellIntl").GetType().BaseType.FullName == "Telerik.Web.UI.RadInputControl")
                        _sellIntl = Math.Round(Convert.ToDecimal(((RadNumericTextBox)selItem.FindControl("tbSellIntl")).Text), 2);
                    else
                        _sellIntl = Math.Round(Convert.ToDecimal(((TextBox)selItem.FindControl("tbSellIntl")).Text), 2);
                //_sellIntl = Math.Round(Convert.ToDecimal(((TextBox)selItem.FindControl("tbSellIntl")).Text), 2);

                if (selItem.FindControl("chkTaxIntl") != null)
                    _isTaxIntl = ((CheckBox)selItem.FindControl("chkTaxIntl")).Checked;

                if (selItem.FindControl("chkDiscountIntl") != null)
                    _isDiscountIntl = ((CheckBox)selItem.FindControl("chkDiscountIntl")).Checked;

                if (selItem["FeeAMT"] != null && selItem["FeeAMT"].Text != string.Empty)
                    _feeAMT = Math.Round(Convert.ToDecimal(selItem["FeeAMT"].Text), 2);

                if (QuoteInProgress.CQFleetChargeDetails != null)
                {
                    CQFleetChargeDetail objFleetCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == _orderNum).SingleOrDefault();
                    if (objFleetCharge != null)
                    {
                        if (objFleetCharge.CQFleetChargeDetailID != 0)
                            objFleetCharge.State = CQRequestEntityState.Modified;
                        objFleetCharge.CQFlightChargeDescription = _description;
                        objFleetCharge.ChargeUnit = _chargeUnit;
                        objFleetCharge.NegotiatedChgUnit = _chargeUnitVal;
                        objFleetCharge.Quantity = _quantity;
                        objFleetCharge.BuyDOM = _buyDOM;
                        objFleetCharge.SellDOM = _sellDOM;
                        objFleetCharge.IsTaxDOM = _isTaxDOM;
                        objFleetCharge.IsDiscountDOM = _isDiscountDOM;
                        objFleetCharge.BuyIntl = _buyIntl;
                        objFleetCharge.SellIntl = _sellIntl;
                        objFleetCharge.IsTaxIntl = _isTaxIntl;
                        objFleetCharge.IsDiscountIntl = _isDiscountIntl;
                        objFleetCharge.FeeAMT = _feeAMT;
                        objFleetCharge.OrderNUM = _orderNum;
                        objFleetCharge.IsDeleted = false;

                        Master.SaveQuoteinProgressToFileSession();
                    }
                }
            }
        }

        protected void StandardCharges_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item.FindControl("ddlChargeUnit") != null)
                            {
                                DropDownList ddlChargeUnit = Item.FindControl("ddlChargeUnit") as DropDownList;
                                TextBox tbQuantity = Item.FindControl("tbQuantity") as TextBox;
                                CheckBox chkSelect = Item.FindControl("chkSelect") as CheckBox;

                                tbQuantity.ReadOnly = false;
                                ControlEnable("TextBox", tbQuantity, true, "text60");

                                if (Item.GetDataKeyValue("OrderNUM") != null && (Item.GetDataKeyValue("OrderNUM").ToString() == "1" || Item.GetDataKeyValue("OrderNUM").ToString() == "2"))
                                {
                                    ddlChargeUnit.Items[3].Enabled = false; // Disable % of Daily Usage
                                    ddlChargeUnit.Items[4].Enabled = false; // Disable Fixed
                                    ddlChargeUnit.Items[5].Enabled = false; // Disable Per Hour
                                    ddlChargeUnit.Items[6].Enabled = false; // Disable Per Leg
                                    ddlChargeUnit.Items[7].Enabled = false; // Disable Kilometer

                                    chkSelect.Enabled = false;
                                    tbQuantity.ReadOnly = true;
                                    ControlEnable("TextBox", tbQuantity, false, "inpt_non_edit text60");
                                }
                                else if (Item.GetDataKeyValue("OrderNUM") != null && Item.GetDataKeyValue("OrderNUM").ToString() == "3")
                                {
                                    ddlChargeUnit.Items[0].Enabled = false; // Per Flight Hour
                                    ddlChargeUnit.Items[1].Enabled = false; // Per Nautical Mile
                                    ddlChargeUnit.Items[2].Enabled = false; // Per Statute Mile
                                    ddlChargeUnit.Items[3].Enabled = false; // Disable % of Daily Usage
                                    ddlChargeUnit.Items[5].Enabled = false; // Disable Per Hour
                                    ddlChargeUnit.Items[6].Enabled = false; // Disable Per Leg
                                    ddlChargeUnit.Items[7].Enabled = false; // Disable Kilometer

                                    chkSelect.Enabled = false;
                                    tbQuantity.ReadOnly = true;
                                    ControlEnable("TextBox", tbQuantity, false, "inpt_non_edit text60");
                                }

                                if (Item.GetDataKeyValue("NegotiatedChgUnit") != null && !string.IsNullOrEmpty(Item.GetDataKeyValue("NegotiatedChgUnit").ToString()))
                                {
                                    ddlChargeUnit.SelectedValue = Item.GetDataKeyValue("NegotiatedChgUnit").ToString();

                                    // Set as Readonly for Quantity
                                    if (Item.GetDataKeyValue("NegotiatedChgUnit").ToString() == "1"
                                        || Item.GetDataKeyValue("NegotiatedChgUnit").ToString() == "2"
                                        || Item.GetDataKeyValue("NegotiatedChgUnit").ToString() == "3"
                                        || Item.GetDataKeyValue("NegotiatedChgUnit").ToString() == "7")
                                    {
                                        tbQuantity.ReadOnly = true;
                                        ControlEnable("TextBox", tbQuantity, false, "inpt_non_edit text60");
                                    }
                                }
                            }
                            if (Item.GetDataKeyValue("Quantity") != null)
                            {
                                TextBox tbQuantity = Item.FindControl("tbQuantity") as TextBox;
                                if (tbQuantity != null)
                                    tbQuantity.Text = Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("Quantity")), 2).ToString();
                            }


                            if (Item.GetDataKeyValue("IsTaxDOM") != null)
                            {
                                CheckBox chkTaxDOM = Item.FindControl("chkTaxDOM") as CheckBox;
                                chkTaxDOM.Checked = (bool)Item.GetDataKeyValue("IsTaxDOM");
                            }
                            if (Item.GetDataKeyValue("IsDiscountDOM") != null)
                            {
                                CheckBox chkDiscountDOM = Item.FindControl("chkDiscountDOM") as CheckBox;
                                chkDiscountDOM.Checked = (bool)Item.GetDataKeyValue("IsDiscountDOM");
                            }
                            if (Item.GetDataKeyValue("IsTaxIntl") != null)
                            {
                                CheckBox chkTaxIntl = Item.FindControl("chkTaxIntl") as CheckBox;
                                chkTaxIntl.Checked = (bool)Item.GetDataKeyValue("IsTaxIntl");
                            }
                            if (Item.GetDataKeyValue("IsDiscountIntl") != null)
                            {
                                CheckBox chkDiscountIntl = Item.FindControl("chkDiscountIntl") as CheckBox;
                                chkDiscountIntl.Checked = (bool)Item.GetDataKeyValue("IsDiscountIntl");
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        #endregion

        private void CalculateQuantityByOrderNum(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                {
                    var chargeRate = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == orderNum).FirstOrDefault();

                    if (chargeRate != null && chargeRate.NegotiatedChgUnit != null)
                    {
                        if (chargeRate.CQFleetChargeDetailID != 0)
                            chargeRate.State = CQRequestEntityState.Modified;

                        //Quantity = Sum of ETE where cqfltchg.nchrgunit is 1 
                        if (chargeRate.NegotiatedChgUnit == 1)
                        {
                            chargeRate.Quantity = Master.CalculateSumOfETE((int)chargeRate.OrderNUM);
                        }

                        //Quantity = Sum of Distance where cqfltchg.nchrgunit is 2
                        //Quantity = Sum of Distance multiplied by StatueConvFactor where nchrgunit is 3
                        if (chargeRate.NegotiatedChgUnit == 2)
                        {
                            chargeRate.Quantity = Master.CalculateSumOfDistance((int)chargeRate.OrderNUM);
                        }

                        if (chargeRate.NegotiatedChgUnit == 3)
                        {
                            chargeRate.Quantity = Master.CalculateSumOfDistance((int)chargeRate.OrderNUM) * statuateConvFactor;
                        }

                        //Quantity = Total number of legs where nchrgunit is 7
                        if (chargeRate.NegotiatedChgUnit == 7)
                        {
                            if (QuoteInProgress.CQLegs != null)
                                chargeRate.Quantity = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Count();
                        }

                        //Quantity = Sum of Distance * 1.852M, where cqfltchg.nchrgunit is 8
                        if (chargeRate.NegotiatedChgUnit == 8)
                        {
                            chargeRate.Quantity = Master.CalculateSumOfDistance((int)chargeRate.OrderNUM) * 1.852M;
                        }

                        //Quantity = Sum of RON and Day Room of all legs where ordernum is 3 
                        if (chargeRate.OrderNUM == 3)
                        {
                            chargeRate.Quantity = Master.CalculateSumOfRON();
                        }

                        Master.SaveQuoteinProgressToFileSession();
                    }
                }
            }
        }

        protected void Override_Command(object sender, CommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (QuoteInProgress != null)
                        {
                            if (e.CommandName == "TotalCost")
                            {
                                if (tbTotalCost.Enabled)
                                {
                                    tbTotalCost.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbTotalCost, false, "pr_radtextbox_80_readonly");
                                    btnTotalCost.ToolTip = "Override Default Value";
                                    tbTotalCost.Text = QuoteInProgress.CostTotal.ToString();
                                    tbTotalCost.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideCost = false;
                                }
                                else
                                {
                                    tbTotalCost.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbTotalCost, true, "inpt_override_red_color pr_radtextbox_80");
                                    btnTotalCost.ToolTip = "Restore Default Value";
                                    tbTotalCost.Text = QuoteInProgress.CostTotal.ToString();
                                    tbTotalCost.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideCost = true;
                                }
                            }
                            else if (e.CommandName == "UsageAdj")
                            {
                                if (tbUsageAdj.Enabled)
                                {
                                    tbUsageAdj.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbUsageAdj, false, "pr_radtextbox_50_readonly");
                                    btnUsageAdj.ToolTip = "Override Default Value";
                                    tbUsageAdj.Text = QuoteInProgress.UsageAdjTotal.ToString();
                                    tbUsageAdj.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideUsageAdj = false;
                                }
                                else
                                {
                                    tbUsageAdj.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbUsageAdj, true, "inpt_override_red_color pr_radtextbox_50");
                                    btnUsageAdj.ToolTip = "Restore Default Value";
                                    tbUsageAdj.Text = QuoteInProgress.UsageAdjTotal.ToString();
                                    tbUsageAdj.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideUsageAdj = true;
                                }
                            }
                            else if (e.CommandName == "SegmentFees")
                            {
                                if (tbSegmentFees.Enabled)
                                {
                                    tbSegmentFees.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbSegmentFees, false, "pr_radtextbox_50_readonly");
                                    btnSegmentFees.ToolTip = "Override Default Value";
                                    tbSegmentFees.Text = QuoteInProgress.SegmentFeeTotal.ToString();
                                    tbSegmentFees.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideSegmentFee = false;
                                }
                                else
                                {
                                    tbSegmentFees.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbSegmentFees, true, "inpt_override_red_color pr_radtextbox_50");
                                    btnSegmentFees.ToolTip = "Restore Default Value";
                                    tbSegmentFees.Text = QuoteInProgress.SegmentFeeTotal.ToString();
                                    tbSegmentFees.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideSegmentFee = true;
                                }
                            }
                            else if (e.CommandName == "Discount")
                            {
                                if (tbDiscount.Enabled)
                                {
                                    tbDiscount.ReadOnly = true;
                                    ControlEnable("TextBox", tbDiscount, false, "readonly-text50");
                                    btnDiscount.ToolTip = "Override Default Value";
                                    tbDiscount.Text = QuoteInProgress.DiscountPercentageTotal.ToString();
                                    tbDiscount.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideDiscountPercentage = false;
                                }
                                else
                                {
                                    tbDiscount.ReadOnly = false;
                                    ControlEnable("TextBox", tbDiscount, true, "inpt_override_red_color readonly-text50");
                                    btnDiscount.ToolTip = "Restore Default Value";
                                    tbDiscount.Text = QuoteInProgress.DiscountPercentageTotal.ToString();
                                    tbDiscount.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideDiscountPercentage = true;
                                }
                            }
                            else if (e.CommandName == "LandingFees")
                            {
                                if (tbLandingFees.Enabled)
                                {
                                    tbLandingFees.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbLandingFees, false, "pr_radtextbox_50_readonly");
                                    btnLandingFees.ToolTip = "Override Default Value";
                                    tbLandingFees.Text = QuoteInProgress.LandingFeeTotal.ToString();
                                    tbLandingFees.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideLandingFee = false;
                                }
                                else
                                {
                                    tbLandingFees.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbLandingFees, true, "inpt_override_red_color pr_radtextbox_50");
                                    btnLandingFees.ToolTip = "Restore Default Value";
                                    tbLandingFees.Text = QuoteInProgress.LandingFeeTotal.ToString();
                                    tbLandingFees.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideLandingFee = true;
                                }
                            }
                            else if (e.CommandName == "DiscountAmount")
                            {
                                if (tbDiscountAmount.Enabled)
                                {
                                    tbDiscountAmount.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbDiscountAmount, false, "pr_radtextbox_50_readonly");
                                    btnDiscountAmount.ToolTip = "Override Default Value";
                                    tbDiscountAmount.Text = QuoteInProgress.DiscountAmtTotal.ToString();
                                    tbDiscountAmount.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideDiscountAMT = false;
                                }
                                else
                                {
                                    tbDiscountAmount.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbDiscountAmount, true, "pr_radtextbox_50");
                                    btnDiscountAmount.ToolTip = "Restore Default Value";
                                    tbDiscountAmount.Text = QuoteInProgress.DiscountAmtTotal.ToString();
                                    tbDiscountAmount.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideDiscountAMT = true;
                                }
                            }
                            else if (e.CommandName == "Taxes")
                            {
                                if (tbTaxes.Enabled)
                                {
                                    tbTaxes.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbTaxes, false, "pr_radtextbox_50_readonly");
                                    btnTaxes.ToolTip = "Override Default Value";
                                    tbTaxes.Text = QuoteInProgress.TaxesTotal.ToString();
                                    tbTaxes.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideTaxes = false;
                                }
                                else
                                {
                                    tbTaxes.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbTaxes, true, "inpt_override_red_color pr_radtextbox_50");
                                    btnTaxes.ToolTip = "Restore Default Value";
                                    tbTaxes.Text = QuoteInProgress.TaxesTotal.ToString();
                                    tbTaxes.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideTaxes = true;
                                }
                            }
                            else if (e.CommandName == "TotalQuote")
                            {
                                if (tbTotalQuote.Enabled)
                                {
                                    tbTotalQuote.ReadOnly = true;
                                    ControlEnable("RadNumericTextBox", tbTotalQuote, false, "pr_radtextbox_50_readonly");
                                    btnTotalQuote.ToolTip = "Override Default Value";
                                    tbTotalQuote.Text = QuoteInProgress.QuoteTotal.ToString();
                                    tbTotalQuote.ForeColor = System.Drawing.Color.Black;
                                    QuoteInProgress.IsOverrideTotalQuote = false;
                                }
                                else
                                {
                                    tbTotalQuote.ReadOnly = false;
                                    ControlEnable("RadNumericTextBox", tbTotalQuote, true, "inpt_override_red_color pr_radtextbox_50");
                                    btnTotalQuote.ToolTip = "Restore Default Value";
                                    tbTotalQuote.Text = QuoteInProgress.QuoteTotal.ToString();
                                    tbTotalQuote.ForeColor = System.Drawing.Color.Red;
                                    QuoteInProgress.IsOverrideTotalQuote = true;
                                }
                            }

                            Master.SaveQuoteinProgressToFileSession();

                            Master.CalculateQuantity();
                            Master.CalculateQuoteTotal();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                            {
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            }
                            LoadLegDetails();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditFile_Click(sender, e);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        #region Fee Group Override/Append Event Methods

        protected void FeeGrpOverride_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CharterQuoteService.FeeGroup feegrp = new CharterQuoteService.FeeGroup();
                        feegrp.FeeGroupCD = tbFeeGroup.Text;
                        feegrp.FeeGroupID = Convert.ToInt64(hdnFeeGroupID.Value);
                        feegrp.FeeGroupDescription = System.Web.HttpUtility.HtmlDecode(lbFeeGroupDesc.Text);

                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllFeeScheduleByGroupID> FeeScheduleList = new List<FlightPak.Web.FlightPakMasterService.GetAllFeeScheduleByGroupID>();
                            FeeScheduleList = Service.GetFeeScheduleByGroupID(Convert.ToInt64(hdnFeeGroupID.Value)).EntityList;

                            // Remove Existing Fleet Charge Details, Where Order Number > 3
                            Int64 QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                            if (QuoteInProgress != null)
                            {
                                QuoteInProgress.FeeGroup = feegrp;
                            }

                            if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                            {
                                List<CQFleetChargeDetail> ChargeToDeleteList = new List<CQFleetChargeDetail>();
                                ChargeToDeleteList = QuoteInProgress.CQFleetChargeDetails.Where(x => x.OrderNUM > 3 && x.IsDeleted == false).ToList();

                                if (ChargeToDeleteList != null && ChargeToDeleteList.Count > 0)
                                {
                                    foreach (CQFleetChargeDetail fleetCharge in ChargeToDeleteList)
                                    {
                                        if (fleetCharge.CQFleetChargeDetailID != 0)
                                        {
                                            fleetCharge.IsDeleted = true;
                                            fleetCharge.State = CQRequestEntityState.Deleted;
                                        }
                                        else
                                            QuoteInProgress.CQFleetChargeDetails.Remove(fleetCharge);
                                    }
                                }
                            }

                            if (FeeScheduleList != null && FeeScheduleList.Count > 0)
                            {
                                // Append the Selected Charge Rate
                                int _orderNum = 4;
                                List<CQFleetChargeDetail> chargeList = new List<CQFleetChargeDetail>();
                                foreach (GetAllFeeScheduleByGroupID feeSchedule in FeeScheduleList)
                                {
                                    CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                    fleetCharge.State = CQRequestEntityState.Added;
                                    fleetCharge.CQFlightChargeDescription = feeSchedule.AccountDescription;
                                    fleetCharge.ChargeUnit = "Fixed";
                                    fleetCharge.NegotiatedChgUnit = 5;
                                    fleetCharge.Quantity = 0;
                                    fleetCharge.BuyDOM = feeSchedule.Cost;
                                    fleetCharge.SellDOM = 0.0M;
                                    fleetCharge.IsTaxDOM = feeSchedule.IsTaxable;
                                    fleetCharge.IsDiscountDOM = feeSchedule.IsDiscount;
                                    fleetCharge.BuyIntl = 0.0M;
                                    fleetCharge.SellIntl = 0.0M;
                                    fleetCharge.IsTaxIntl = false;
                                    fleetCharge.IsDiscountIntl = false;
                                    fleetCharge.FeeAMT = 0.0M;
                                    fleetCharge.OrderNUM = _orderNum;
                                    fleetCharge.IsDeleted = false;

                                    chargeList.Add(fleetCharge);

                                    _orderNum += 1;
                                }

                                if (QuoteInProgress.CQFleetChargeDetails == null)
                                    QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();

                                QuoteInProgress.CQFleetChargeDetails.AddRange(chargeList);
                            }
                            Master.SaveQuoteinProgressToFileSession();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.CalculateQuantity();
                            Master.CalculateQuoteTotal();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void FeeGrpAppend_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CharterQuoteService.FeeGroup feegrp = new CharterQuoteService.FeeGroup();
                        feegrp.FeeGroupCD = tbFeeGroup.Text;
                        feegrp.FeeGroupID = Convert.ToInt64(hdnFeeGroupID.Value);
                        feegrp.FeeGroupDescription = System.Web.HttpUtility.HtmlDecode(lbFeeGroupDesc.Text);

                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllFeeScheduleByGroupID> FeeScheduleList = new List<FlightPak.Web.FlightPakMasterService.GetAllFeeScheduleByGroupID>();
                            FeeScheduleList = Service.GetFeeScheduleByGroupID(Convert.ToInt64(hdnFeeGroupID.Value)).EntityList;

                            // Remove Existing Fleet Charge Details, Where Order Number > 3
                            Int64 QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                            if (QuoteInProgress != null)
                            {
                                QuoteInProgress.FeeGroup = feegrp;
                            }

                            if (FeeScheduleList != null && FeeScheduleList.Count > 0)
                            {
                                // Append the Selected Charge Rate
                                int _orderNum = 4;

                                if (QuoteInProgress.CQFleetChargeDetails != null && QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).Count() > 0)
                                {
                                    _orderNum = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).Max(x => x.OrderNUM).Value + 1;
                                }

                                List<CQFleetChargeDetail> chargeList = new List<CQFleetChargeDetail>();
                                foreach (GetAllFeeScheduleByGroupID feeSchedule in FeeScheduleList)
                                {
                                    CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                    fleetCharge.State = CQRequestEntityState.Added;
                                    fleetCharge.CQFlightChargeDescription = feeSchedule.AccountDescription;
                                    fleetCharge.ChargeUnit = "Fixed";
                                    fleetCharge.NegotiatedChgUnit = 5;
                                    fleetCharge.Quantity = 0;
                                    fleetCharge.BuyDOM = feeSchedule.Cost;
                                    fleetCharge.SellDOM = 0.0M;
                                    fleetCharge.IsTaxDOM = feeSchedule.IsTaxable;
                                    fleetCharge.IsDiscountDOM = feeSchedule.IsDiscount;
                                    fleetCharge.BuyIntl = 0.0M;
                                    fleetCharge.SellIntl = 0.0M;
                                    fleetCharge.IsTaxIntl = false;
                                    fleetCharge.IsDiscountIntl = false;
                                    fleetCharge.FeeAMT = 0.0M;
                                    fleetCharge.OrderNUM = _orderNum;
                                    fleetCharge.IsDeleted = false;

                                    chargeList.Add(fleetCharge);

                                    _orderNum += 1;
                                }

                                if (QuoteInProgress.CQFleetChargeDetails == null)
                                    QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();

                                QuoteInProgress.CQFleetChargeDetails.AddRange(chargeList);
                            }

                            Master.SaveQuoteinProgressToFileSession();
                            Master.BindStandardCharges(true, dgStandardCharges);
                            Master.CalculateQuantity();
                            Master.CalculateQuoteTotal();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void FeeGrpCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(@"fnCloseFeeGroupWindow();");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        #endregion

    }
}
