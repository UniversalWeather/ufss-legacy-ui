﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CharterQuote.Master"

AutoEventWireup="true" CodeBehind="CharterQuoteRequest.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.CharterQuoteRequest" %>


<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CharterQuoteHeadContent" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CharterQuoteBodyContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

<script type="text/javascript">


    function confirmAutoUpdateRatesFn(arg) {

        if (arg == true) {

            document.getElementById('<%=btnConfirmAutoUpdateRateYes.ClientID%>').click();

}

else

    document.getElementById('<%=btnConfirmAutoUpdateRateNo.ClientID%>').click();

}


function openWin(url, value, radWin) {

    var oWnd = radopen(url + value, radWin);

}


function OpenCQSearch() {

    var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "RadCQSearch");

}

function openSalesPerson() {

    var oWnd = radopen("/Views/CharterQuote/SalesPersonPopup.aspx?Code=" + document.getElementById("<%=tbSalesPerson.ClientID%>").value, "RadWindow2");

}


function openLeadSource() {

    var oWnd = radopen("/Views/CharterQuote/LeadSourcePopup.aspx?Code=" + document.getElementById("<%=tbLeadSource.ClientID%>").value, "RadWindow3");


}


function openAircraft() {

    var oWnd = radopen("/Views/Settings/Fleet/FleetNewCharterRate.aspx?IsPopup=&FromPage=charter&CQcustomerID=" + document.getElementById("<%=hdnCustomerID.ClientID%>").value + "&CQCD=" + document.getElementById("<%=tbCustomer.ClientID%>").value + "&Description=" + document.getElementById("<%=tbCustomerName.ClientID%>").value, "RadFleetNewCharter");

}


function openCharterQuotCustomer() {

    var oWnd = radopen("/Views/CharterQuote/CharterQuoteCustomerPopup.aspx?Code=" + document.getElementById("<%=tbCustomer.ClientID%>").value, "radCQCPopups");


}

function openHomebase() {

    var oWnd = radopen("/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbHomebase.ClientID%>").value, "radCompanyMasterPopup");

}


function openCustomerAddress() {

    //Response.Redirect("CharterQuoteCustomerContactCatalog.aspx?Screen=CQCustomer&CQCustomerID=" + Server.UrlEncode(CQCustomerID) + "&CQCustomerCode=" + Server.UrlEncode(tbCode.Text) + "&CQCustomerName=" + Server.UrlEncode(tbName.Text));


    var oWnd = radopen("/Views/CharterQuote/CharterQuoteCustomerContactCatalogPopup.aspx?CQCustomerID=" + document.getElementById("<%=hdnCustomerID.ClientID%>").value + "&CQCustomerCode=" + document.getElementById("<%=tbCustomer.ClientID%>").value + "&CQCustomerName=" + document.getElementById("<%=tbCustomerName.ClientID%>").value + "&Code=" + document.getElementById("<%=tbContact.ClientID%>").value, "RadWindow7");

}


function openWin(radWin) {


    var url = '';

    if (radWin == "rdHistory") {

        url = "../../Transactions/History.aspx?FromPage=" + "CharterQuote";

    }


    if (url != '') {

        var oWnd = radopen(url, radWin);

    }

}


function ConfirmClose(WinName) {

    var oManager = GetRadWindowManager();

    var oWnd = oManager.GetWindowByName(WinName);

    //Find the Close button on the page and attach to the

    //onclick event

    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);

    CloseButton.onclick = function () {

        CurrentWinName = oWnd.Id;

        //radconfirm is non-blocking, so you will need to provide a callback function

        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);

    }

}


function OnClientCloseSalesPersonPopup(oWnd, args) {

    var combo = $find("<%= tbSalesPerson.ClientID %>");

    //get the transferred arguments

    var arg = args.get_argument();

    if (arg != null) {

        if (arg) {

            document.getElementById("<%=hdnSalesPerson.ClientID%>").value = arg.SalesPersonID;

    document.getElementById("<%=tbSalesPerson.ClientID%>").value = arg.SalesPersonCD;

    // if (arg.FirstName != null)

    // document.getElementById("<%=lbSalesPersonDesc.ClientID%>").innerHTML = arg.FirstName;


    if (arg.SalesPersonCD != null)

        document.getElementById("<%=lbcvSalesPerson.ClientID%>").value = "";


    var step = "tbSalesPerson_TextChanged";

    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");

if (step) {

    ajaxManager.ajaxRequest(step);

}

}

else {

    document.getElementById("<%=tbSalesPerson.ClientID%>").value = "";

    document.getElementById("<%=lbSalesPersonDesc.ClientID%>").innerHTML = "";

    //combo.clearSelection();

}

}

}


function OnClientCloseLeadSourcePopup(oWnd, args) {

    var combo = $find("<%= tbLeadSource.ClientID %>");

    //get the transferred arguments

    var arg = args.get_argument();

    if (arg != null) {

        if (arg) {

            document.getElementById("<%=hdnLeadSource.ClientID%>").value = arg.LeadSourceID;

    document.getElementById("<%=tbLeadSource.ClientID%>").value = arg.Code;

    if (arg.Description != null && arg.Description != "undefined")

        document.getElementById("<%=lbleadSourc.ClientID%>").innerHTML = arg.Description;

    if (arg.LeadSourceID != null)

        document.getElementById("<%=lbcvLeadSource.ClientID%>").value = "";


    var step = "tbLeadSource_TextChanged";

    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");

if (step) {

    ajaxManager.ajaxRequest(step);

}

}

else {

    document.getElementById("<%=tbLeadSource.ClientID%>").value = "";

    document.getElementById("<%=lbleadSourc.ClientID%>").innerHTML = "";

    //combo.clearSelection();

}

}

}


function OnClientCloseCharterQuotCustomerPopup(oWnd, args) {

    var combo = $find("<%= tbCustomer.ClientID %>");

    //get the transferred arguments

    var arg = args.get_argument();

    if (arg != null) {

        if (arg) {

            document.getElementById("<%=tbCustomer.ClientID%>").value = arg.CQCustomerCD;


    var step = "tbCustomer_TextChanged";

    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");

if (step) {

    ajaxManager.ajaxRequest(step);

}

}

else {

    document.getElementById("<%=tbCustomer.ClientID%>").value = "";

    document.getElementById("<%=tbCustomerName.ClientID%>").value = "";

    document.getElementById("<%=hdnCustomerID.ClientID%>").value = "";

    document.getElementById("<%=tbContact.ClientID%>").value = "";

    document.getElementById("<%=tbPhone.ClientID%>").value = "";

    document.getElementById("<%=tbFax.ClientID%>").value = "";

    document.getElementById("<%=tbContactName.ClientID%>").value = "";

    document.getElementById("<%=hdnContactID.ClientID%>").value = "";

    document.getElementById("<%=tbContact.ClientID%>").disabled = true;

    document.getElementById("<%=btnContact.ClientID%>").disabled = true;

    combo.clearSelection();

}

}

}


function OnClientCloseHomebasePopup(oWnd, args) {

    var combo = $find("<%= tbHomebase.ClientID %>");

    //get the transferred arguments

    var arg = args.get_argument();

    if (arg != null) {

        if (arg) {

            document.getElementById("<%=tbHomebase.ClientID%>").value = arg.HomeBase;

    if (arg.BaseDescription != null)

        document.getElementById("<%=lbHomebase.ClientID%>").innerHTML = arg.BaseDescription;

    document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.HomebaseID;

    var step = "tbHomeBase_TextChanged";

    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");

if (step) {

    ajaxManager.ajaxRequest(step);

}

if (arg.HomeBase != null)

    document.getElementById("<%=lbcvHomeBase.ClientID%>").innerHTML = "";

}

else {

    document.getElementById("<%=tbHomebase.ClientID%>").value = "";

    document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";

    document.getElementById("<%=lbHomebase.ClientID%>").innerHTML = "";

    combo.clearSelection();

}

}

}


function OnClientCloseCustomerAddressPopup(oWnd, args) {
    var combo = $find("<%= tbContact.ClientID %>");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
                document.getElementById("<%=tbContact.ClientID%>").value = arg.CQCustomerContactCD;
                var name = arg.FirstName.replace(/(<([^>]+)>)/ig, "");
                document.getElementById("<%=tbContactName.ClientID%>").value = name.replace("&nbsp;", " ");
                document.getElementById("<%=tbPhone.ClientID%>").value = arg.BusinessPhoneNum;
                document.getElementById("<%=tbFax.ClientID%>").value = arg.BusinessFaxNum;
                var step = "tbContact_TextChanged";
                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }
        else {
            document.getElementById("<%=tbContact.ClientID%>").value = "";
            document.getElementById("<%=tbPhone.ClientID%>").value = "";
            document.getElementById("<%=tbContactName.ClientID%>").value = "";
            document.getElementById("<%=tbFax.ClientID%>").value = "";
            combo.clearSelection();
            }
        }
}


function GetDimensions(sender, args) {

    var bounds = sender.getWindowBounds();

    return;

}

function GetRadWindow() {


    var oWindow = null;

    if (window.radWindow) oWindow = window.radWindow;

    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;

    return oWindow;

}


function RequestStart(sender, args) {

    currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");

    currentUpdatedControl = "<%=DivExternalForm.ClientID%>";

    //show the loading panel over the updated control

    currentLoadingPanel.show(currentUpdatedControl);

}

function ResponseEnd() {

    //hide the loading panel and clean up the global variables

    if (currentLoadingPanel != null)

        currentLoadingPanel.hide(currentUpdatedControl);

    currentUpdatedControl = null;

    currentLoadingPanel = null;

}


function ValidateDate(control) {


    var MinDate = new Date("01/01/1900");

    var MaxDate = new Date("12/31/2100");

    var SelectedDate;

    if (control.value != "") {

        SelectedDate = new Date(control.value);

        if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {

            alert("Please enter/select Date between 01/01/1900 and 12/31/2100");

            control.focus();

            control.value = "";

        }

    }

    return false;

}


function limitnofotext(reftxt, charlength) {


    if (reftxt.value.length > charlength) {


        reftxt.value = reftxt.value.substring(0, charlength);

    }


}


function callBackDepartDate(arg) {

    document.getElementById("<%=tbDepartDate.ClientID%>").focus();

}

function callBackQuoteDate(arg) {

    document.getElementById("<%=tbQuoteDate.ClientID%>").focus();

}


</script>

</telerik:RadCodeBlock>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />

<div id="DivExternalForm" runat="server" class="ExternalForm">

<asp:HiddenField ID="hdnCalculateQuote" runat="server" />

<telerik:RadWindowManager ID="RadWindowManager1" runat="server">

<Windows>

<telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"

KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"

Title="History" NavigateUrl="~/Views/Transactions/History.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"

AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"

NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"

Title="SalesPerson" OnClientClose="OnClientCloseSalesPersonPopup" AutoSize="true"

KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"

NavigateUrl="~/Views/CharterQuote/SalesPersonPopup.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="RadFleetNewCharter" runat="server" OnClientResizeEnd="GetDimensions"

AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"

NavigateUrl="~/Views/Settings/Fleet/FleetNewCharterRate.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"

OnClientClose="OnClientCloseLeadSourcePopup" AutoSize="true" KeepInScreenBounds="true"

Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/LeadSourcePopup.aspx">

</telerik:RadWindow>

<%--<telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"

OnClientClose="OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"

Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">

</telerik:RadWindow>--%>

<telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"

OnClientClose="OnClientCloseCharterQuotCustomerPopup" AutoSize="true" KeepInScreenBounds="true"

Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerPopup.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"

AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"

NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"

OnClientClose="OnClientCloseHomebasePopup" AutoSize="true" KeepInScreenBounds="true"

Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"

OnClientClose="OnClientCloseHomebasePopup" AutoSize="true" KeepInScreenBounds="true"

Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">

</telerik:RadWindow>

<telerik:RadWindow ID="RadWindow7" runat="server" OnClientResizeEnd="GetDimensions"

OnClientClose="OnClientCloseCustomerAddressPopup" AutoSize="true" KeepInScreenBounds="true"

Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerContactCatalogPopup.aspx">

</telerik:RadWindow>

</Windows>

<ConfirmTemplate>

<div class="rwDialogPopup radconfirm">

<div class="rwDialogText">

{1}

</div>

<div>

<a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">

<span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>

<a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">

<span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>

</div>

</div>

</ConfirmTemplate>

<AlertTemplate>

<div class="rwDialogPopup radalert">

<div class="rwDialogText">

{1}

</div>

<div>

<a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">

<span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>

</a>

</div>

</div>

</AlertTemplate>

</telerik:RadWindowManager>

<div style="width: 712px;">

<asp:Panel runat="server" ID="pnlPreflight" Visible="true">

<div style="width: 712px;">

<table>

<tr>

<td style="width: 250px;">

<fieldset>

<legend>Quote Information</legend>

<table cellpadding="1" cellspacing="1">

<tr>

<td class="tdLabel90" valign="top">

<asp:Label ID="lbDepartDate" runat="server" Text="Trip Date" CssClass="mnd_text"></asp:Label>

</td>

<td>

<table cellpadding="0" cellspacing="0">

<tr>

<td class="padright_5">

<asp:TextBox ID="tbDepartDate" runat="server" CssClass="text70" onkeypress="return fnAllowNumericAndChar(this, event,'/')"

AutoPostBack="true" onclick="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"

onchange="parseDate(this, event);" OnTextChanged="DepartDate_TextChanged"></asp:TextBox>

</td>

<td valign="top" class="mask-textbox span"></td>

</tr>

</table>

</td>

</tr>

<tr>

<td class="tdLabel90">

<asp:Label ID="lbQuoteDate" runat="server" Text="Quote Date" CssClass="mnd_text"></asp:Label>

</td>

<td>

<asp:TextBox ID="tbQuoteDate" runat="server" CssClass="text70" onkeypress="return fnAllowNumericAndChar(this, event,'/')"

AutoPostBack="true" onclick="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"

OnTextChanged="QuoteDate_TextChanged" onchange="parseDate(this, event);"></asp:TextBox>

</td>

</tr>

<tr>

<td class="tdLabel90">

<asp:Label ID="lbHomebasefield" runat="server" Text="Home Base" CssClass="mnd_text"></asp:Label>

</td>

<td class="tdLabel100">

<table cellpadding="0" cellspacing="0">

<tr>

<td>

<asp:TextBox ID="tbHomebase" runat="server" CssClass="text70" MaxLength="4" onblur="return RemoveSpecialChars(this)"

OnTextChanged="tbHomeBase_TextChanged" AutoPostBack="true">

</asp:TextBox>

<asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openHomebase();return false;" />

</td>

</tr>

<tr>

<td>

<asp:Label ID="lbHomebase" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>

<asp:Label ID="lbcvHomeBase" runat="server" CssClass="alert-text" Visible="true"></asp:Label>

</td>

</tr>

</table>

</td>

</tr>

<tr>

<td class="tdLabel90">

<asp:Label ID="lbCurrency" runat="server" Text="Currency"></asp:Label>

</td>

<td class="tdLabel100">

<asp:DropDownList ID="ddlCurrency" runat="server" CssClass="text80" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged"

AutoPostBack="true">

</asp:DropDownList>

</td>

</tr>

<tr>

<td colspan="2" class="tblspace_10"></td>

</tr>

<tr>

<td colspan="2">

<asp:Label ID="lbDescription" runat="server" Text="Trip Description"></asp:Label>

</td>

</tr>

<tr>

<td colspan="2">

<asp:TextBox ID="tbDescription" runat="server" MaxLength="40" CssClass="textarea190x30"

TextMode="MultiLine" onkeypress="limitnofotext(this,40)"></asp:TextBox>

</td>

</tr>

<tr>

<td colspan="2"></td>

</tr>

</table>

</fieldset>

</td>

<td style="width: 460px; vertical-align: top;">

<table>

<tr>

<td>

<fieldset>

<legend>Customer Information</legend>

<table cellpadding="1" cellspacing="1">

<tr>

<td class="tdLabel60">

<asp:Label ID="lbCustomer" runat="server" Text="Code"></asp:Label>

</td>



<td>

<asp:TextBox ID="tbCustomer" runat="server" CssClass="text50" MaxLength="5" AutoPostBack="true"

OnTextChanged="tbCustomer_TextChanged"></asp:TextBox>

</td>

<td>

<asp:Button ID="btnCustomer" CssClass="browse-button" runat="server" OnClientClick="javascript:openCharterQuotCustomer();return false;" />

</td>

<td class="text50">

<asp:Button ID="btnTypeCodeSearch" CssClass="charter-rate-button" runat="server"

OnClientClick="javascript:openAircraft();return false;" ToolTip="Custom Charter Rates"

Visible="false" />

</td>

<td align="right">

<asp:Label ID="lbvCustomerName" runat="server" Text="Name&nbsp;&nbsp;"></asp:Label>

</td>

<td>

<asp:TextBox ID="tbCustomerName" runat="server" CssClass="text180" MaxLength="40"></asp:TextBox>

</td>


</tr>

<tr>

<td colspan="5">

<asp:Label ID="Label1" runat="server" CssClass="alert-text" Visible="true"></asp:Label>

</td>

</tr>


<tr>

<td colspan="4">

<asp:Label ID="lbcvCustomerName" runat="server" CssClass="alert-text" Visible="true"></asp:Label>

</td>

</tr>




<tr>

<td colspan="4">

<asp:CheckBox ID="chkUseCustomFleetCharge" runat="server" Text="Use Customer Charter Rates"

AutoPostBack="true" OnCheckedChanged="UseCustomFleetCharge_CheckedChanged" Visible="false" />

</td>

</tr>

<%--<tr>

<td colspan="4" align="left">

<asp:label id="lbNotes" runat="server" text="Notes"></asp:label>

</td>

</tr>--%>

<%--<tr>

<td colspan="4">

<asp:textbox id="tbNotes" runat="server" textmode="MultiLine" cssclass="textarea320x70"

onkeypress='return maxLength(this,"200")' onpaste='return maxLengthPaste(this,"200")'></asp:textbox>

</td>

</tr>--%>

</table>

</fieldset>

</td>

</tr>

<tr>

<td>

<fieldset>

<legend>Contact Information</legend>

<table cellpadding="1" cellspacing="1" class="cqcontactinformation">

<tr>

<td class="tdLabel95" style="display:block;">

<asp:Label ID="lbContact" runat="server" Text="Code"></asp:Label>

</td>

<td>

<table cellpadding="0" cellspacing="0">

<tr>

<td>

<asp:TextBox ID="tbContact" runat="server" CssClass="text50" MaxLength="4" AutoPostBack="true"

OnTextChanged="tbContact_TextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>

</td>

<td>

<asp:Button ID="btnContact" CssClass="browse-button" runat="server" OnClientClick="javascript:openCustomerAddress();return false;" />

</td>

</tr>

</table>

</td>

</tr>

<tr>

<td>

<asp:Label ID="lbContactName" runat="server" Text="Name"></asp:Label>

</td>

<td colspan="3">

<asp:TextBox ID="tbContactName" runat="server" CssClass="text320" MaxLength="40"></asp:TextBox>

</td>

</tr>

<tr>

<td colspan="4">

<asp:Label ID="lbcvContact" runat="server" CssClass="alert-text" Visible="true"></asp:Label>

</td>

</tr>

    <tr>

        <td>

        <asp:Label ID="lbPhone" runat="server" Text="Business Phone"></asp:Label>

        </td>

        <td>

            <asp:TextBox ID="tbPhone" runat="server" style="width:110px;" MaxLength="25"></asp:TextBox>

        </td>

        <td style="display: block; width: 85px; line-height: 24px;">

            <asp:Label ID="lbFax" runat="server" Text="Business Fax" CssClass="fax-class"></asp:Label>

</td>

        <td>

            <asp:TextBox ID="tbFax" runat="server" style="width:111px; width: 107px \9"  MaxLength="25"></asp:TextBox>

</td>

    </tr>

<tr>

<td>

<asp:Label ID="lbEmail" runat="server" Text="Business E-mail"></asp:Label>

</td>

<td colspan="3">

<asp:TextBox ID="tbEmail" runat="server" CssClass="text320" MaxLength="100"></asp:TextBox>

</td>

</tr>

<tr>

<td></td>

<td colspan="3">

<asp:RegularExpressionValidator ID="regEmail" runat="server" ErrorMessage="Invalid E-mail format"

Display="Dynamic" CssClass="alert-text" ControlToValidate="tbEmail" ValidationGroup="Save"

ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />

</td>

</tr>

</table>

</fieldset>

</td>

</tr>



</table>


</td>

</tr>

</table>

<table>

<tr>

<td style="width: 356px;">

<fieldset>

<legend>Source Information</legend>

<table width="100%">

<tr>

<td class="tdLabel80">

<asp:Label ID="lbSalesPerson" runat="server" Text="Salesperson"></asp:Label>

</td>

<td>

<asp:TextBox ID="tbSalesPerson" runat="server" CssClass="text70" MaxLength="5" AutoPostBack="true"

OnTextChanged="tbSalesPerson_TextChanged"></asp:TextBox>

<asp:Button ID="btnSalesPerson" CssClass="browse-button" runat="server" OnClientClick="javascript:openSalesPerson();return false;" />

</td>

</tr>

<tr>

<td colspan="2">

<asp:HiddenField ID="hdnSPFName" runat="server" />

<asp:HiddenField ID="hdnSPLName" runat="server" />

<asp:HiddenField ID="hdnSPMName" runat="server" />

<asp:HiddenField ID="hdnSPBusinessPhone" runat="server" />

<asp:HiddenField ID="hdnSPBusinessEmail" runat="server" />

<asp:Label ID="lbcvSalesPerson" runat="server" CssClass="alert-text" Visible="true"></asp:Label>

</td>

</tr>

<tr>

<td align="left" colspan="2">

<asp:Label ID="lbSalesPersonDesc" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>

</td>

</tr>

<tr>

<td>

<asp:Label ID="lbLeadSource" runat="server" Text="Lead Source"></asp:Label>

</td>

<td>

<asp:TextBox ID="tbLeadSource" runat="server" CssClass="text70" MaxLength="4" AutoPostBack="true"

OnTextChanged="tbLeadSource_TextChanged"></asp:TextBox>

<asp:Button ID="btnLeadSource" CssClass="browse-button" runat="server" OnClientClick="javascript:openLeadSource();return false;" />

</td>

</tr>

<tr>

<td align="left" colspan="2">

<asp:Label ID="lbleadSourc" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>

</td>

</tr>

<tr>

<td colspan="2">

<asp:Label ID="lbcvLeadSource" runat="server" CssClass="alert-text" Visible="true"></asp:Label>

</td>

</tr>

</table>

</fieldset>

</td>

<td style="width: 356px; vertical-align: top;">

<fieldset>

<legend>Credit Information</legend>

<table width="100%">

<tr>

<td valign="top" colspan="2" class="tdLabel120">

<asp:CheckBox ID="chkAppOnFile" runat="server" Text="App On File" />

</td>

<td valign="top" colspan="2">

<asp:CheckBox ID="chkApproval" runat="server" Text="Approval" />

</td>

</tr>

<tr>

<td class="tdLabel35">

<asp:Label ID="lbCredit" runat="server" Text="Credit"></asp:Label>

</td>

<td class="pr_radtextbox_100">

<%--<asp:Label ID="tbCreditData" runat="server" CssClass="alert-text"></asp:Label>--%>

<%--<asp:TextBox ID="tbCreditData" runat="server" CssClass="text80" MaxLength="5"></asp:TextBox>--%>

<telerik:RadNumericTextBox ID="tbCreditData" runat="server" Type="Currency" Culture="en-US"

Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"

ReadOnly="true">

</telerik:RadNumericTextBox>

</td>

<td>

<asp:Label ID="lbDiscount" runat="server" Text="Discount %"></asp:Label>

</td>

<td>

<asp:TextBox ID="tbDiscount" runat="server" CssClass="text60" MaxLength="5" onkeypress="return fnAllowNumericAndChar(this, event,'.')"

AutoPostBack="true" OnTextChanged="Discount_TextChanged"></asp:TextBox>

</td>

</tr>

<tr>

<td colspan="3"></td>

<td>

<%--<asp:RegularExpressionValidator ID="rfvPostedPrice" runat="server" ControlToValidate="tbDiscount"

EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00.00)"

Display="Dynamic" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>

</td>

</tr>

<tr>

<td colspan="4">

<div style="height: 6px;">

</div>

</td>

</tr>

</table>

</fieldset>

</td>

</tr>

<tr>

<td colspan="2">

<fieldset>

<legend>

<asp:Label runat="server" ID="lblAlert" Text="Alert" Font-Bold="true"></asp:Label>

</legend>

<div class="note-box" style="float: left;">

<asp:TextBox ID="tbLogNotes" TextMode="MultiLine" runat="server" CssClass="textarea670x50"></asp:TextBox>

</div>

</fieldset>

</td>

</tr>

</table>

<div style="width: 712px; padding: 0px 0 10px 0;">

<div style="float: right;">

<asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete File" CausesValidation="false"

OnClick="Delete_Click" />

<asp:Button ID="btnEditFile" runat="server" CssClass="button" Text="Edit File" OnClick="Edit_Click" />

<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"

OnClick="Cancel_Click" />

<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"

OnClick="Save_Click" />

<asp:Button ID="btnNext" runat="server" CssClass="button" Text="Next >" CausesValidation="false"

OnClick="Next_Click" />

</div>

</div>

<div style="float: left; width: 712px; padding: 5px 0 10px 0;">

<telerik:RadPanelBar ID="RadPanelBar1" ExpandAnimation-Type="None" CollapseAnimation-Type="None"

runat="server" CssClass="charterquote-panel-bar">

<Items>

<telerik:RadPanelItem runat="server" Expanded="false" Text="Quote Details" Value="pnlItemTrip">

<ContentTemplate>

<table cellpadding="0" cellspacing="0" width="100%">

<tr>

<td class="preflight-custom-grid">

<telerik:RadGrid ID="dgQuote" runat="server" AllowSorting="true" Visible="true" OnNeedDataSource="Quote_BindData"

OnItemDataBound="Quote_ItemDataBound" AutoGenerateColumns="false" PageSize="10"

AllowPaging="false" AllowFilteringByColumn="false" PagerStyle-AlwaysVisible="true"

AllowMultiRowSelection="false">

<MasterTableView DataKeyNames="QuoteID,CQMainID,CustomerID,CQFileID,FileNUM,VendorID,FleetID,AircraftID"

CommandItemDisplay="Bottom" AllowPaging="false" AllowFilteringByColumn="false">

<Columns>

<telerik:GridBoundColumn DataField="QuoteNUM" HeaderText="Q. No." AutoPostBackOnFilter="true"

CurrentFilterFunction="EqualTo" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<telerik:GridBoundColumn DataField="CQSource" HeaderText="Src" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false" UniqueName="CQSource">

</telerik:GridBoundColumn>

<telerik:GridTemplateColumn HeaderText="Vendor" CurrentFilterFunction="Contains"

ShowFilterIcon="false" UniqueName="From" AllowFiltering="false">

<ItemTemplate>

<asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Vendor.Name")%>'></asp:Label>

</ItemTemplate>

</telerik:GridTemplateColumn>

<telerik:GridTemplateColumn HeaderText="Tail No." CurrentFilterFunction="Contains"

ShowFilterIcon="false" UniqueName="From" AllowFiltering="false">

<ItemTemplate>

<asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Fleet.TailNum")%>'></asp:Label>

</ItemTemplate>

</telerik:GridTemplateColumn>

<telerik:GridTemplateColumn HeaderText="Type Code" CurrentFilterFunction="Contains"

ShowFilterIcon="false" UniqueName="From" AllowFiltering="false">

<ItemTemplate>

<asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Aircraft.AircraftCD")%>'></asp:Label>

</ItemTemplate>

</telerik:GridTemplateColumn>

<telerik:GridBoundColumn DataField="FlightHoursTotal" HeaderText="Dur." AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<%--<telerik:GridBoundColumn DataField="" HeaderText="U" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<telerik:GridBoundColumn DataField="" HeaderText="Total Charges" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>--%>

<telerik:GridBoundColumn DataField="IsFinancialWarning" HeaderText="Fin. Warm" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<%-- <telerik:GridBoundColumn DataField="" HeaderText="TS Status" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>--%>

<telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<%-- <telerik:GridBoundColumn DataField="" HeaderText="Except." AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<telerik:GridBoundColumn DataField="" HeaderText="Quote Status" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>--%>

<telerik:GridBoundColumn DataField="LastAcceptDT" HeaderText="Accepted" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>

<%--

<telerik:GridBoundColumn DataField="" HeaderText="Log Number" AutoPostBackOnFilter="true"

CurrentFilterFunction="Contains" ShowFilterIcon="false">

</telerik:GridBoundColumn>--%>

</Columns>

<CommandItemTemplate>

<div>

<asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>

</div>

</CommandItemTemplate>

</MasterTableView>

<GroupingSettings CaseSensitive="false" />

</telerik:RadGrid>

</td>

</tr>

</table>

</ContentTemplate>

</telerik:RadPanelItem>

</Items>

</telerik:RadPanelBar>

</div>

</div>

</asp:Panel>

</div>

<asp:HiddenField ID="hdnSave" runat="server" />

<asp:HiddenField ID="hdnHomeBaseID" runat="server" />

<asp:HiddenField ID="hdnExchangeRateID" runat="server" />

<asp:HiddenField ID="hdnCustomerID" runat="server" />

<asp:HiddenField ID="hdnContactID" runat="server" />

<asp:HiddenField ID="hdnSalesPerson" runat="server" />

<asp:HiddenField ID="hdnLeadSource" runat="server" />

<asp:HiddenField ID="hdHomeBaseAirportID" runat="server" />

<asp:HiddenField ID="hdnCalculateFile" runat="server" />

</div>

<table id="tblHidden" style="display: none;">

<tr>

<td>

<asp:Button ID="btnConfirmAutoUpdateRateYes" runat="server" Text="Button" OnClick="ConfirmAutoUpdateRateYes_Click" />

<asp:Button ID="btnConfirmAutoUpdateRateNo" runat="server" Text="Button" OnClick="ConfirmAutoUpdateRateNo_Click" />

</td>

</tr>

</table>

</asp:Content>