﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CharterQuote.Master"
    AutoEventWireup="true" CodeBehind="CharterQuoteLogistics.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.CharterQuoteLogistics" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CharterQuoteBodyContent" runat="server">
    <style type="text/css">
        .rgDataDiv{ height: auto !important;}
    </style>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            var Popup = '';
            var TransPopup = '';

            function openWinFBO(type) {
                var url = '';
                Popup = type;
                if (type == "DEPART") {
                    url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('<%=hdDepartIcaoID.ClientID%>').value + '&FBOCD=' + document.getElementById('<%=tbDepartFBO.ClientID%>').value;
                    var oWnd = radopen(url, 'RadFBOCodePopup');
                    oWnd.add_close(OnClientFBOClose);
                }
                else {
                    var icaoId = document.getElementById('<%=hdArriveIcaoID.ClientID%>').value;
                    if (icaoId) {
                        url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&FBOCD=' + document.getElementById('<%=tbArriveFBO.ClientID%>').value;
                        var oWnd = radopen(url, 'RadFBOCodePopup');
                        oWnd.add_close(OnClientFBOClose);
                    } else {
                        showMessageArrivalIcaoRequired();
                    }
                }
            }

            function showMessageArrivalIcaoRequired() {
                radalert('Please enter an Arrival ICAO in the Quote & Leg Details screen.', 300, 120, popupTitle);
            }

            function openWinTransport(type) {
                var url = '';
                TransPopup = type;
                var icaoId = document.getElementById('<%=hdArriveIcaoID.ClientID%>').value;
                if (icaoId) {
                    if (type == "PAX") {
                        url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&TransportCD=' + document.getElementById('<%=tbPaxTransportCode.ClientID%>').value;
                        var oWnd = radopen(url, 'radTransportCodePopup');
                        //oWnd.add_close(OnClientTransportClose);
                    } else if (type == "CREW") {
                        url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&TransportCD=' + document.getElementById('<%=tbCrewTransportCode.ClientID%>').value;
                            var oWnd = radopen(url, 'radTransportCodePopup');
                        //oWnd.add_close(OnClientTransportClose);
                        }
                } else {
                    showMessageArrivalIcaoRequired();
                }
            }

            function OnClientTransportClose(oWnd, args) {

                if (TransPopup == 'PAX') {
                    var combo = $find("<%= tbPaxTransportCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=hdPaxTransAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdPaxTransportID.ClientID%>").value = arg.TransportID;
                            document.getElementById("<%=tbPaxTransportCode.ClientID%>").value = arg.TransportCD;
                            document.getElementById("<%=tbPaxTransportName.ClientID%>").value = arg.TransportationVendor;
                            if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportPhone.ClientID%>").value = arg.PhoneNUM;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportFax.ClientID%>").value = arg.FaxNUM;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportRate.ClientID%>").value = arg.NegotiatedRate;
                            if (arg.CateringCD != null)
                                document.getElementById("<%=lbcvPaxTransportCode.ClientID%>").innerHTML = "";
                            var step = "PaxTransportCode_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdPaxTransAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdPaxTransportID.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportCode.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportName.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportFax.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportRate.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                else if (TransPopup == 'CREW') {
                    var combo = $find("<%= tbCrewTransportCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=hdCrewTransAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdCrewTransportID.ClientID%>").value = arg.TransportID;
                            document.getElementById("<%=tbCrewTransportCode.ClientID%>").value = arg.TransportCD;
                            document.getElementById("<%=tbCrewTransportName.ClientID%>").value = arg.TransportationVendor;
                            if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportPhone.ClientID%>").value = arg.PhoneNUM;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportFax.ClientID%>").value = arg.FaxNUM;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportRate.ClientID%>").value = arg.NegotiatedRate;
                            if (arg.CateringCD != null)
                                document.getElementById("<%=lbcvCrewTransportCode.ClientID%>").innerHTML = "";
                            var step = "CrewTransportCode_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdCrewTransAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdCrewTransportID.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportCode.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportName.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportFax.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportRate.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }

            function openWin(radWin) {
                var icaoId = document.getElementById('<%=hdArriveIcaoID.ClientID%>').value;
                var url = '';

                if (radWin == "rdPaxHotelPopup") {
                    if (icaoId) {
                        url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&HotelCD=' + document.getElementById('<%=tbPaxHotelCode.ClientID%>').value;;
                    }else {
                        url = ''; 
                        showMessageArrivalIcaoRequired();
                    }
                }
                if (radWin == "rdCrewHotelPopup") {
                    if (icaoId) {
                        url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&HotelCD=' + document.getElementById('<%=tbCrewHotelCode.ClientID%>').value;;
                    } else {
                        url = '';
                        showMessageArrivalIcaoRequired();
                    }
                }

                if (radWin == "rdMaintHotelPopup") {
                    if (icaoId) {
                        url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&HotelCD=' + document.getElementById('<%=tbMaintHotelCode.ClientID%>').value;;
                    } else {
                        url = '';
                        showMessageArrivalIcaoRequired();
                    }
                }

                if (radWin == "radCateringPopup") {
                    var CateringCD = document.getElementById('<%=tbMaintCateringCode.ClientID%>').value;
                    url = '/Views/Settings/Logistics/CaterPopup.aspx?IcaoID=' + document.getElementById('<%=hdDepartIcaoID.ClientID%>').value + '&CateringCD=' + CateringCD;
                }

                if (radWin == "rdHistory") {
                    url = "../../Transactions/History.aspx?FromPage=" + "CharterQuote";
                }

                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }

            function OpenCQSearch() {
                var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "RadCQSearch");
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientFBOClose(oWnd, args) {

                if (Popup == 'DEPART') {
                    var combo = $find("<%= tbDepartFBO.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=hdDepartFBOAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdDepartFBOID.ClientID%>").value = arg.FBOID;
                            document.getElementById("<%=tbDepartFBO.ClientID%>").value = arg.FBOCD;
                            document.getElementById("<%=tbDepartFBOName.ClientID%>").value = arg.FBOVendor;
                            if (arg.PhoneNUM1 != null && arg.PhoneNUM1 != "&nbsp;")
                                document.getElementById("<%=tbDepartFBOPhone.ClientID%>").value = arg.PhoneNUM1;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbDepartFBOFax.ClientID%>").value = arg.FaxNum;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbDepartFBOEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.FBOID != null)
                                document.getElementById("<%=lbcvDepartFBOCode.ClientID%>").innerHTML = "";
                            var step = "DepartFBO_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdDepartFBOAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdDepartFBOID.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBO.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOName.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOFax.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                else {
                    var combo = $find("<%= tbArriveFBO.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg != null) {
                        if (arg) {
                            document.getElementById("<%=hdArriveFBOAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdArriveFBOID.ClientID%>").value = arg.FBOID;
                            document.getElementById("<%=tbArriveFBO.ClientID%>").value = arg.FBOCD;
                            document.getElementById("<%=tbArriveFBOName.ClientID%>").value = arg.FBOVendor;
                            if (arg.PhoneNUM1 != null && arg.PhoneNUM1 != "&nbsp;")
                                document.getElementById("<%=tbArriveFBOPhone.ClientID%>").value = arg.PhoneNUM1;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbArriveFBOFax.ClientID%>").value = arg.FaxNum;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbArriveFBOEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.FBOCD != null)
                                document.getElementById("<%=lbcvArriveFBOCode.ClientID%>").innerHTML = "";
                            var step = "ArriveFBO_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdArriveFBOAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdArriveFBOID.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBO.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOName.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOFax.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }

            function OnClientCaterClose(oWnd, args) {
                var combo = $find("<%= tbMaintCateringCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=hdMaintCatAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdMaintCateringID.ClientID%>").value = arg.CateringID;
                        document.getElementById("<%=tbMaintCateringCode.ClientID%>").value = arg.CateringCD;
                        document.getElementById("<%=tbMaintCateringName.ClientID%>").value = arg.CateringVendor;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbMaintCateringPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbMaintCateringFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbMaintCateringEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbMaintCateringRate.ClientID%>").value = arg.NegotiatedRate;
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvMaintCateringCode.ClientID%>").innerHTML = "";
                        var step = "MaintCatering_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdMaintCatAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdMaintCateringID.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintCateringCode.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintCateringName.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintCateringPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintCateringFax.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintCateringRate.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintCateringEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientPaxTransportClose(oWnd, args) {

            }

            function OnClientCrewTransportClose(oWnd, args) {

            }

            function OnClientPaxHotelClose(oWnd, args) {
                var combo = $find("<%= tbPaxHotelCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=hdPaxHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdPaxHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbPaxHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbPaxHotelName.ClientID%>").value = arg.Name;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelRate.ClientID%>").value = arg.NegociatedRate;
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvPaxHotelCode.ClientID%>").innerHTML = "";
                        var step = "PaxHotelCode_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdPaxHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdPaxHotelID.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelFax.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCrewHotelClose(oWnd, args) {
                var combo = $find("<%= tbCrewHotelCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=hdCrewHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdCrewHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbCrewHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbCrewHotelName.ClientID%>").value = arg.Name;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelRate.ClientID%>").value = arg.NegociatedRate;
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvCrewHotelCode.ClientID%>").innerHTML = "";
                        var step = "CrewHotelCode_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdCrewHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdCrewHotelID.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelFax.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientMaintHotelClose(oWnd, args) {
                var combo = $find("<%= tbMaintHotelCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=hdMaintHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdMaintHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbMaintHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbMaintHotelName.ClientID%>").value = arg.Name;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelRate.ClientID%>").value = arg.NegociatedRate;
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvMaintHotelCode.ClientID%>").innerHTML = "";
                        var step = "MaintHotelCode_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdMaintHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdMaintHotelID.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelFax.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated controlk
                currentLoadingPanel.show(currentUpdatedControl);
            }

            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function confirmNextFBOCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnNextFBOYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnNextFBONo.ClientID%>').click();
                }
            }

            function confirmPreviousFBOCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnPreviousFBOYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnPreviousFBONo.ClientID%>').click();
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFBOCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCateringPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCaterClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/CaterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCateringCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx" OnClientClose="OnClientTransportClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPaxHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientPaxHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCrewHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMaintHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientMaintHotelClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Visible="true" runat="server"
        Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div style="width: 712px;">
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgLegs" runat="server" AllowSorting="false" Visible="true" OnItemDataBound="Legs_ItemDataBound"
                            OnNeedDataSource="Legs_BindData" OnSelectedIndexChanged="Legs_SelectedIndexChanged"
                            OnItemCommand="Legs_ItemCommand" AutoGenerateColumns="false" AllowPaging="false"
                            AllowFilteringByColumn="false" PagerStyle-AlwaysVisible="false"
                            Width="704px">
                            <MasterTableView DataKeyNames="LegNum,DepartAirportID,ArrivalAirportID" CommandItemDisplay="None"
                                AllowPaging="false" AllowFilteringByColumn="false">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" ShowFilterIcon="false"
                                        HeaderStyle-Width="40px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DepartAir" HeaderText="Dep ICAO" ShowFilterIcon="false"
                                        HeaderStyle-Width="70px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="Arr ICAO" ShowFilterIcon="false"
                                        HeaderStyle-Width="70px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ETE" HeaderText="ETE" HeaderStyle-Width="50px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Dep Date/Time" DataType="System.DateTime"
                                        HeaderStyle-Width="130px" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TA" HeaderText="TA" HeaderStyle-Width="40px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arr Date/Time" HeaderStyle-Width="130px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="POS" HeaderText="POS" HeaderStyle-Width="60px">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn DataField="Tax" HeaderText="Tax" HeaderStyle-Width="40px">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="TaxRate" HeaderText="Tax Rate" HeaderStyle-Width="65px">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn HeaderText="Tax Rate" FilterDelay="4000" ItemStyle-CssClass="fc_textbox"
                                        HeaderStyle-Width="80px" UniqueName="TaxRate" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbTaxRate" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("TaxRate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Rate" ItemStyle-CssClass="fc_textbox" ItemStyle-Width="80px"
                                        HeaderStyle-Width="80px" UniqueName="Rate" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbRate" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("Rate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%-- <telerik:GridBoundColumn DataField="Charges" HeaderText="Charges" HeaderStyle-Width="60px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Charges" ItemStyle-CssClass="fc_textbox"
                                        ItemStyle-Width="80px" HeaderStyle-Width="80px" UniqueName="Charges" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbcharges" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("Charges") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="RON" HeaderText="RON" HeaderStyle-Width="50px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DayRoom" HeaderText="Day Room" HeaderStyle-Width="70px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaxTotal" HeaderText="PAX Total" HeaderStyle-Width="70px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CD" HeaderText="CD" HeaderStyle-Width="30px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Distance" HeaderText="Distance" ShowFilterIcon="false"
                                        HeaderStyle-Width="60px">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <div runat="server" style="float: left; width: 351px;">
                            <fieldset>
                                <legend>Departure FBO</legend>
                                <table>
                                    <tr>
                                        <td class="tdLabel50">
                                            <asp:Label ID="lbDepartFBO" runat="server" Text="Code"></asp:Label>
                                        </td>
                                        <td class="tdLabel250">
                                            <asp:TextBox ID="tbDepartFBO" runat="server" CssClass="text40" OnTextChanged="DepartFBO_TextChanged"
                                                onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdDepartFBOID" runat="server" />
                                            <asp:Button ID="btnDepartFBO" OnClientClick="javascript:openWinFBO('DEPART');return false;"
                                                runat="server" CssClass="browse-button" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lbcvDepartFBOCode" CssClass="alert-text" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbDepartFBOName" runat="server" Text="Name"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbDepartFBOName" BackColor="Gray" ForeColor="White" runat="server"
                                                ReadOnly="true" CssClass="text230"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbDepartFBOPhone" runat="server" Text="Phone">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbDepartFBOPhone" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbDepartFBOFax" runat="server" Text="Fax">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbDepartFBOFax" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbDepartFBOEmail" runat="server" Text="E-mail">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbDepartFBOEmail" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                        <div runat="server" style="float: right; width: 351px;">
                            <fieldset>
                                <legend>Arrival FBO</legend>
                                <table>
                                    <tr>
                                        <td class="tdLabel50">
                                            <asp:Label ID="lbArriveFBO" runat="server" Text="Code"></asp:Label>
                                        </td>
                                        <td class="tdLabel250">
                                            <asp:TextBox ID="tbArriveFBO" runat="server" CssClass="text40" OnTextChanged="ArriveFBO_TextChanged"
                                                onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true"></asp:TextBox>
                                            <asp:Button ID="btnArriveFBO" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinFBO('ARRIVAL');return false;" />
                                            <asp:HiddenField ID="hdArriveFBOID" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lbcvArriveFBOCode" CssClass="alert-text" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbArriveFBOName" runat="server" Text="Name"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbArriveFBOName" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbArriveFBOPhone" runat="server" Text="Phone">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbArriveFBOPhone" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbArriveFBOFax" runat="server" Text="Fax">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbArriveFBOFax" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbArriveFBOEmail" runat="server" Text="E-mail">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbArriveFBOEmail" runat="server" ReadOnly="true" CssClass="text230"
                                                BackColor="Gray" ForeColor="White">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td style="width: 356px;">
                        <fieldset>
                            <legend>PAX Hotel</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbPaxHotelCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel145">
                                        <asp:TextBox ID="tbPaxHotelCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="PaxHotelCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnPaxHotelCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdPaxHotelPopup');return false;" />
                                        <asp:HiddenField ID="hdPaxHotelID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbPaxHotelRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadNumericTextBox ID="tbPaxHotelRate" runat="server" Type="Currency" Culture="en-US"
                                            MaxLength="17" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                            AutoPostBack="true" OnTextChanged="tbPaxHotelRate_TextChanged">
                                        </telerik:RadNumericTextBox>
                                        <%--<asp:TextBox ID="tbPaxHotelRate" runat="server" MaxLength="17" OnTextChanged="tbPaxHotelRate_TextChanged"
                                            AutoPostBack="true" CssClass="text40">
                                        </asp:TextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvPaxHotelCode" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelnName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelName" runat="server" MaxLength="40" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelPhone" runat="server" MaxLength="25" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelFax" runat="server" MaxLength="25" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelEmail" runat="server" MaxLength="250" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="3">
                                        <asp:RegularExpressionValidator ID="regEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbPaxHotelEmail" ValidationGroup="Save"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 356px;">
                        <fieldset>
                            <legend>PAX Transportation</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbPaxTransportCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel145">
                                        <asp:TextBox ID="tbPaxTransportCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="PaxTransportCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnPaxTransportCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinTransport('PAX');return false;" />
                                        <asp:HiddenField ID="hdPaxTransportID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbPaxTransportRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <%--<asp:TextBox ID="tbPaxTransportRate" MaxLength="6" runat="server" OnTextChanged="tbPaxTransportRate_TextChanged"
                                            AutoPostBack="true" CssClass="text40">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbPaxTransportRate" runat="server" Type="Currency"
                                            Culture="en-US" MaxLength="6" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                            AutoPostBack="true" OnTextChanged="tbPaxTransportRate_TextChanged">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvPaxTransportCode" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportName" MaxLength="40" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportPhone" MaxLength="25" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportFax" MaxLength="25" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportEmail" runat="server" MaxLength="250" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="3">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbPaxTransportEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td style="width: 356px;">
                        <fieldset>
                            <legend>Crew Hotel</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbCrewHotelCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel145">
                                        <asp:TextBox ID="tbCrewHotelCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="CrewHotelCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnCrewHotelCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdCrewHotelPopup');return false;" />
                                        <asp:HiddenField ID="hdCrewHotelID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbCrewHotelRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <%--<asp:TextBox ID="tbCrewHotelRate" MaxLength="17" runat="server" OnTextChanged="tbCrewHotelRate_TextChanged"
                                            CssClass="text40" AutoPostBack="true">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbCrewHotelRate" runat="server" Type="Currency" Culture="en-US"
                                            MaxLength="17" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                            AutoPostBack="true" OnTextChanged="tbCrewHotelRate_TextChanged">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvCrewHotelCode" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelName" runat="server" MaxLength="40" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelPhone" runat="server" MaxLength="25" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelFax" runat="server" MaxLength="25" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelEmail" runat="server" MaxLength="250" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="3">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbCrewHotelEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 356px;">
                        <fieldset>
                            <legend>Crew Transportation</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbCrewTransportCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel145">
                                        <asp:TextBox ID="tbCrewTransportCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="CrewTransportCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnCrewTransportCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinTransport('CREW');return false;" />
                                        <asp:HiddenField ID="hdCrewTransportID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbCrewTransportRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <%--<asp:TextBox ID="tbCrewTransportRate" runat="server" OnTextChanged="tbCrewTransportRate_TextChanged"
                                            CssClass="text40" AutoPostBack="true">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbCrewTransportRate" runat="server" Type="Currency"
                                            Culture="en-US" MaxLength="17" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                            AutoPostBack="true" OnTextChanged="tbCrewTransportRate_TextChanged">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvCrewTransportCode" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportName" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportPhone" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportFax" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportEmail" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="3">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbCrewTransportEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td style="width: 356px;">
                        <fieldset>
                            <legend>Maintenance Hotel</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbMaintHotelCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel145">
                                        <asp:TextBox ID="tbMaintHotelCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="MaintHotelCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnMaintHotelCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdMaintHotelPopup');return false;" />
                                        <asp:HiddenField ID="hdMaintHotelID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbMaintHotelRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <%-- <asp:TextBox ID="tbMaintHotelRate" MaxLength="17" runat="server" OnTextChanged="tbMaintHotelRate_TextChanged"
                                            CssClass="text40" AutoPostBack="true">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbMaintHotelRate" runat="server" Type="Currency" Culture="en-US"
                                            MaxLength="17" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                            AutoPostBack="true" OnTextChanged="tbMaintHotelRate_TextChanged">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvMaintHotelCode" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintHotelName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintHotelName" runat="server" MaxLength="40" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintHotelPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintHotelPhone" runat="server" MaxLength="25" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintHotelFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintHotelFax" runat="server" MaxLength="25" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintHotelEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintHotelEmail" runat="server" MaxLength="250" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="3">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbMaintHotelEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 356px;">
                        <fieldset>
                            <legend>Departure Catering</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbMaintCateringCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel145">
                                        <asp:TextBox ID="tbMaintCateringCode" runat="server" CssClass="text40" OnTextChanged="MaintCatering_TextChanged"
                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true">
                                        </asp:TextBox>
                                        <asp:Button ID="btnMaintCateringCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('radCateringPopup');return false;" />
                                        <asp:HiddenField ID="hdMaintCateringID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbMaintCateringRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadNumericTextBox ID="tbMaintCateringRate" runat="server" Type="Currency"
                                            Culture="en-US" MaxLength="6" Value="0.00" NumberFormat-DecimalSeparator="."
                                            EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                        <%-- <asp:TextBox ID="tbMaintCateringRate" MaxLength="6" runat="server" CssClass="text40"
                                            AutoPostBack="true">
                                        </asp:TextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvMaintCateringCode" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintCateringName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintCateringName" MaxLength="40" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintCateringPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintCateringPhone" MaxLength="25" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintCateringFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintCateringFax" MaxLength="25" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintCateringEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbMaintCateringEmail" MaxLength="250" runat="server" CssClass="text230">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="3">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbMaintCateringEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server">
                            <Items>
                                <telerik:RadPanelItem Value="radpnlItemNotes" runat="server" Expanded="false" Text="PAX Notes">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <asp:TextBox ID="tbPaxNotes" CssClass="textarea690x80" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete File" OnClick="Delete_Click" />
                        <asp:Button ID="btnEditFile" runat="server" CssClass="button" Text="Edit File" OnClick="Edit_Click" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="Cancel_Click" />
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                            OnClick="Save_Click" />
                        <asp:Button ID="btnNext" runat="server" CssClass="button" Text="Next >" OnClick="Next_Click" />
                        <asp:HiddenField ID="hdDepartFBOAirID" runat="server" />
                        <asp:HiddenField ID="hdArriveFBOAirID" runat="server" />
                        <asp:HiddenField ID="hdMaintCatAirID" runat="server" />
                        <asp:HiddenField ID="hdPaxTransAirID" runat="server" />
                        <asp:HiddenField ID="hdCrewTransAirID" runat="server" />
                        <asp:HiddenField ID="hdPaxHotelAirID" runat="server" />
                        <asp:HiddenField ID="hdCrewHotelAirID" runat="server" />
                        <asp:HiddenField ID="hdMaintHotelAirID" runat="server" />
                        <asp:HiddenField ID="hdDepartIcaoID" runat="server" />
                        <asp:HiddenField ID="hdArriveIcaoID" runat="server" />
                        <asp:HiddenField ID="hdnLegNum" runat="server" />
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlQuote" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server" CssClass="charterquote-panel-bar" Visible="false">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Quote Details" Value="pnlItemTrip">
                                    <ContentTemplate>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="preflight-custom-grid">
                                                    <telerik:RadGrid ID="dgQuote" runat="server" AllowSorting="true" Visible="true" OnNeedDataSource="Quote_BindData"
                                                        OnItemDataBound="Quote_ItemDataBound" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowFilteringByColumn="false" PagerStyle-AlwaysVisible="true"
                                                        AllowMultiRowSelection="false">
                                                        <MasterTableView DataKeyNames="QuoteID,CQMainID,CustomerID,CQFileID,FileNUM,VendorID,FleetID,AircraftID"
                                                            CommandItemDisplay="Bottom" AllowPaging="false" AllowFilteringByColumn="false">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="QuoteNUM" HeaderText="Q. No." AutoPostBackOnFilter="true"
                                                                    CurrentFilterFunction="EqualTo" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="CQSource" HeaderText="Src" AutoPostBackOnFilter="true"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" UniqueName="CQSource">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Vendor" CurrentFilterFunction="Contains"
                                                                    ShowFilterIcon="false" UniqueName="From" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Vendor.Name")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Tail No." CurrentFilterFunction="Contains"
                                                                    ShowFilterIcon="false" UniqueName="From" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Fleet.TailNum")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Type Code" CurrentFilterFunction="Contains"
                                                                    ShowFilterIcon="false" UniqueName="From" AllowFiltering="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Aircraft.AircraftCD")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn DataField="FlightHoursTotal" HeaderText="Dur." AutoPostBackOnFilter="true"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <%--<telerik:GridBoundColumn DataField="" HeaderText="U" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="" HeaderText="Total Charges" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>--%>
                                                                <telerik:GridBoundColumn DataField="IsFinancialWarning" HeaderText="Fin. Warm" AutoPostBackOnFilter="true"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <%--                            <telerik:GridBoundColumn DataField="" HeaderText="TS Status" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>--%>
                                                                <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <%--                            <telerik:GridBoundColumn DataField="" HeaderText="Except." AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="" HeaderText="Quote Status" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>--%>
                                                                <telerik:GridBoundColumn DataField="LastAcceptDT" HeaderText="Accepted" AutoPostBackOnFilter="true"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                                                </telerik:GridBoundColumn>
                                                                <%--
                            <telerik:GridBoundColumn DataField="" HeaderText="Log Number" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>--%>
                                                            </Columns>
                                                            <CommandItemTemplate>
                                                                <div>
                                                                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                                                </div>
                                                            </CommandItemTemplate>
                                                        </MasterTableView>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table id="tblHidden" style="display: none;">
                <tr>
                    <td>
                        <asp:Button ID="btnNextFBOYes" runat="server" Text="Button" OnClick="btnNextFBOYes_Click" />
                        <asp:Button ID="btnNextFBONo" runat="server" Text="Button" OnClick="btnNextFBONo_Click" />
                        <asp:Button ID="btnPreviousFBOYes" runat="server" Text="Button" OnClick="btnPreviousFBOYes_Click" />
                        <asp:Button ID="btnPreviousFBONo" runat="server" Text="Button" OnClick="btnPreviousFBONo_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
