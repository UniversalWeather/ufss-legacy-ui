﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Site.Master"
    AutoEventWireup="true" CodeBehind="ExpressQuote.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.ExpressQuote" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            document.getElementById("anchorCharter").setAttribute("class", "selector-active");
        </script>
        <script type="text/javascript">

            function ShowWarningMessage(uicontrol, sender, e) {
                if (document.getElementById("<%=hdnCompSettingforshowWarn.ClientID%>").value == "true") {
                    if (document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value == "true") {

                        function callBackFn(confirmed) {
                            if (confirmed) {
                                document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "false";
                                if (uicontrol == "date")
                                    showPopup(sender, e);
                            }
                        }

                        var msg = 'WARNING - The auto date adjustment feature is active.  Changes made to any dates or RON values will automatically adjust all other legs onthis itinerary.Press YES to Continue or NO to Return.';
                        radconfirm(msg, callBackFn, 330, 100, '', 'Express Quote');
                        //return false;

                        //                        if (confirm()) {
                        //                            document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "false";
                        //                            if (uicontrol == "date")
                        //                                showPopup(sender, e);
                        //                        }
                        //                        else {
                        //                            document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "true";
                        //                            return false;
                        //                        }
                    }
                    else {
                        if (uicontrol == "date")
                            showPopup(sender, e);
                    }
                }
                else {
                    if (uicontrol == "date")
                        showPopup(sender, e);
                }
            }



            function alertCrewCallBackFn(arg) {
                if (document.getElementById('<%=hdnTempHoursTxtbox.ClientID%>').value == "tbHrs") {
                    document.getElementById('<%=tbHrs.ClientID%>').value = "";
                    document.getElementById('<%=tbHrs.ClientID%>').focus();
                }
            }

            function OpenDepartAirportCatalog(AirportID) {
                var oWnd = radopen('../../Settings/Airports/AirportCatalog.aspx?IsPopup=View&AirportID=' + AirportID, "RadAirportCatalog");
                return false;
            }

            function OpenArrivalAirportCatalog(AirportID) {
                var oWnd = radopen('../../Settings/Airports/AirportCatalog.aspx?IsPopup=View&AirportID=' + AirportID, "RadAirportCatalog");
                return false;
            }


            function OpenCQSearch() {
                var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx?IsExpressQuote=true', "RadCQSearch");
            }
            function openFleetNewCharter() {
                var oWnd = radopen("/Views/Settings/Fleet/FleetNewCharterRate.aspx?IsPopup=&FromPage=charter&FleetID=" + document.getElementById("<%=hdnTailNum.ClientID%>").value + "&TailNum=" + document.getElementById("<%=tbTailNum.ClientID%>").value, "RadFleetNewCharter");
            }
            function openWin(url, value, radWin) {
                var oWnd = radopen(url + value, radWin);
            }

            function openWinAirport(type) {
                var url = '';
                if (type == "DEPART") {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbDepart.ClientID%>').value;
                    var oWnd = radopen(url, 'rdAirportPopup');
                    oWnd.add_close(OnClientICAOClose1);
                }
                else {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbArrival.ClientID%>').value;
                    var oWnd = radopen(url, 'rdAirportPopup');
                    oWnd.add_close(OnClientICAOClose2);
                }

            }
            function openWin(radWin) {
                var url = '';
                if (radWin == "rdDepartAirport") {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbDepart.ClientID%>').value;
                }
                else if (radWin == "rdArriveAirport") {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbArrival.ClientID%>').value;
                }
                else if (radWin == "rdPower") {
                    url = '../../Settings/Fleet/AircraftPopup.aspx?PowerSetting=' + document.getElementById('<%=tbPower.ClientID%>').value;
                }
                else if (radWin == "rdCrewRules") {
                    url = "../../Settings/People/CrewDutyRulesPopup.aspx?CrewDutyRuleCD=" + document.getElementById('<%=tbCrewRules.ClientID%>').value;
                }
                else if (radWin == "rdVendor") {
                    url = '../../Settings/Logistics/VendorMasterPopUp.aspx?VendorCD=' + document.getElementById('<%=tbVendor.ClientID%>').value;
                }
                else if (radWin == "rdType") {
                    if (document.getElementById("<%=hdnTailNum.ClientID%>").value == "") {
                        url = "../../Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + document.getElementById('<%=tbTypeCode.ClientID%>').value + "&tailNoAirCraftID=" + document.getElementById("<%=hdnTypeCode.ClientID%>").value;
                    }
                }
                else if (radWin == "rdTailNum") {
                    if (document.getElementById("<%=hdnVendor.ClientID%>").value == "") {
                        url = "../../Settings/Fleet/FleetProfilePopup.aspx?TailNum=" + document.getElementById('<%=tbTailNum.ClientID%>').value;
                    }
                    else {
                        if (document.getElementById("<%=hdnVendor.ClientID%>").value != "") {
                            url = "../../Settings/Fleet/FleetProfilePopup.aspx?Vendor=" + document.getElementById('<%=hdnVendor.ClientID%>').value + "&FromPage=" + "expressquote";
                        }
                    }
                }
                else if (radWin == "radCQCPopups") {
                    url = '../../CharterQuote/CharterQuoteCustomerPopup.aspx?Code=' + document.getElementById('<%=tbCustomer.ClientID%>').value;
                }
                else if (radWin == "rdSalesPerson") {
                    url = '../../CharterQuote/SalesPersonPopup.aspx?Code=' + document.getElementById('<%=tbSalesPerson.ClientID%>').value;
                }
                else if (radWin == "rdLeadSource") {
                    url = '../../CharterQuote/LeadSourcePopup.aspx?Code=' + document.getElementById('<%=tbLeadSource.ClientID%>').value;
                }

    if (url != "") {
        var oWnd = radopen(url, radWin);
    }

    else {
        if (radWin == "rdTOBias")
            var oWnd = radopen("", radWin);
        if (radWin == "rdLandBias")
            var oWnd = radopen("", radWin);
    }
}

function ConfirmClose(WinName) {
    var oManager = GetRadWindowManager();
    var oWnd = oManager.GetWindowByName(WinName);
    //Find the Close button on the page and attach to the
    //onclick event
    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
    CloseButton.onclick = function () {
        CurrentWinName = oWnd.Id;
        //radconfirm is non-blocking, so you will need to provide a callback function
        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
    }
}

function OnClientICAOClose1(oWnd, args) {
    var combo = $find("<%= tbDepart.ClientID %>");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            document.getElementById("<%=tbDepart.ClientID%>").value = arg.ICAO.toUpperCase();
            document.getElementById("<%=lbDepart.ClientID%>").innerHTML = arg.AirportName.toUpperCase();
            document.getElementById("<%=hdnDepart.ClientID%>").value = arg.AirportID;
            if (arg.ICAO != null)
                document.getElementById("<%=lbcvDepart.ClientID%>").innerHTML = "";
            var step = "tbDepart_TextChanged";
            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
            if (step) {
                ajaxManager.ajaxRequest(step);
            }
        }
        else {
            document.getElementById("<%=tbDepart.ClientID%>").value = "";
            document.getElementById("<%=lbDepart.ClientID%>").innerHTML = "";
            document.getElementById("<%=hdnDepart.ClientID%>").value = "";

        }
    }
}


            function OnClientICAOClose2(oWnd, args) {
                oWnd.remove_close(OnClientICAOClose2);
    var combo = $find("<%= tbArrival.ClientID %>");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            document.getElementById("<%=tbArrival.ClientID%>").value = arg.ICAO.toUpperCase();
            document.getElementById("<%=lbArrival.ClientID%>").innerHTML = arg.AirportName.toUpperCase();
            document.getElementById("<%=hdnArrival.ClientID%>").value = arg.AirportID;
            if (arg.ICAO != null)
                document.getElementById("<%=lbcvArrival.ClientID%>").innerHTML = "";
                        var step = "tbArrive_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbArrival.ClientID%>").value = "";
            document.getElementById("<%=lbArrival.ClientID%>").innerHTML = "";
            document.getElementById("<%=hdnArrival.ClientID%>").value = "";

        }
    }
}

function OnClientPowerClose(oWnd, args) {
    var combo = $find("<%= tbPower.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbPower.ClientID%>").value = arg.PowerSetting;
                        document.getElementById("<%=hdPower.ClientID%>").value = arg.AircraftID;
                        if (arg.PowerSetting != null)
                            document.getElementById("<%=lbcvPower.ClientID%>").innerHTML = "";
                        var step = "tbPower_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbPower.ClientID%>").value = "";
                        document.getElementById("<%=hdPower.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCrewRuleClose(oWnd, args) {
                var combo = $find("<%= tbCrewRules.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbCrewRules.ClientID%>").value = arg.CrewDutyRuleCD;
                        document.getElementById("<%=lbFar.ClientID%>").innerHTML = arg.FedAviatRegNum;
                        document.getElementById("<%=hdnCrewRules.ClientID%>").value = arg.CrewDutyRulesID;
                        if (arg.CrewDutyRuleCD != null)
                            document.getElementById("<%=lbcvCrewRules.ClientID%>").innerText = "";
                        var step = "tbCrewRules_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbCrewRules.ClientID%>").value = "";
                        document.getElementById("<%=lbFar.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCrewRules.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTailNoClose(oWnd, args) {
                var combo = $find("<%= tbTailNum.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNum.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=hdnTailNum.ClientID%>").value = arg.FleetId;
                        if (arg.TailNum != null)
                            document.getElementById("<%=lbcvTailNum.ClientID%>").innerHTML = "";
                        var step = "TailNum_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTailNum.ClientID%>").value = "";
                        document.getElementById("<%=hdnTailNum.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTypeClose(oWnd, args) {
                var combo = $find("<%= tbTypeCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=lbTypeCodeDes.ClientID%>").innerHTML = arg.AircraftDescription;
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = arg.AircraftID;
                        if (arg.AircraftID != null)
                            document.getElementById("<%=lbcvTypeCode.ClientID%>").innerHTML = "";
                        var step = "TypeCode_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = "";
                        document.getElementById("<%=lbTypeCodeDes.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientVendorClose(oWnd, args) {
                var combo = $find("<%= tbVendor.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbVendor.ClientID%>").value = arg.VendorCD;
                        document.getElementById("<%=hdnVendor.ClientID%>").value = arg.VendorID;
                        if (arg.Name != null) {
                            document.getElementById("<%=lbVendorDesc.ClientID%>").innerHTML = arg.Name;
                        }
                        var step = "Vendor_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                        if (arg.VendorCD != null)
                            document.getElementById("<%=lbcvVendor.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbVendor.ClientID%>").value = "";
                        document.getElementById("<%=lbVendorDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnVendor.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseCharterQuotCustomerPopup(oWnd, args) {
                var combo = $find("<%= tbCustomer.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbCustomer.ClientID%>").value = arg.CQCustomerCD;
                        document.getElementById("<%=hdnCustomerID.ClientID%>").value = arg.CQCustomerID;
                        if (arg.Name != null) {
                            document.getElementById("<%=tbCustomerName.ClientID%>").innerHTML = arg.CQCustomerName;
                        }
                        var step = "tbCustomer_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                        if (arg.CQCustomerCD != null)
                            document.getElementById("<%=lbcvCustomerName.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCustomer.ClientID%>").value = "";
                        document.getElementById("<%=tbCustomerName.ClientID%>").value = "";
                        document.getElementById("<%=hdnCustomerID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseSalesPersonPopup(oWnd, args) {
                var combo = $find("<%= tbSalesPerson.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbSalesPerson.ClientID%>").value = arg.SalesPersonCD;
                        //document.getElementById("<%=lbSalesPersonDesc.ClientID%>").innerHTML = arg.FirstName;
                        document.getElementById("<%=hdnSalesPerson.ClientID%>").value = arg.SalesPersonID;
                        var step = "tbSalesPerson_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.Code != null)
                            document.getElementById("<%=lbcvSalesPerson.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbSalesPerson.ClientID%>").value = "";
                        document.getElementById("<%=lbSalesPersonDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnSalesPerson.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseLeadSourcePopup(oWnd, args) {
                var combo = $find("<%= tbLeadSource.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbLeadSource.ClientID%>").value = arg.Code;
                        document.getElementById("<%=lbleadSource.ClientID%>").innerHTML = arg.Description;
                        document.getElementById("<%=hdnLeadSource.ClientID%>").value = arg.LeadSourceID;
                        var step = "tbLeadSource_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                        if (arg.Code != null)
                            document.getElementById("<%=lbcvLeadSource.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbLeadSource.ClientID%>").value = "";
                        document.getElementById("<%=lbleadSource.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnLeadSource.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        alert("Please enter/select Date between 01/01/1900 and 12/31/2100");
                        control.value = "";
                    }
                }
                return false;
            }

        </script>
        <script type="text/javascript">

            // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates
            function confirmAutoUpdateRatesFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnConfirmAutoUpdateRateYes.ClientID%>').click();
                }
            }

            function limitnofotext(reftxt, charlength) {

                if (reftxt.value.length > charlength) {

                    reftxt.value = reftxt.value.substring(0, charlength);
                }

            }

            function CloseAndRebindSelect() {
                window.location.reload();
            }

            function CloseAndRebindPage() {
                window.location.reload();
            }

            function confirmTransferCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnTransferFileYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnTransferFileNo.ClientID%>').click();
                }
            }

            function confirmDeleteCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeleteFileYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeleteFileNo.ClientID%>').click();
                }
            }

            function confirmCancelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }

            function callBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find('<%= dgLegs.ClientID %>');
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }

            function SaveCloseAndRebindPage() {
                var oWnd = radalert("Express Quote file has been saved successfully", 360, 110, "Express Quote");
                oWnd.add_close(function () { window.location.reload(); });
            }
            function SaveCloseAndRebindExpressPage() {
                var oWnd = radalert("Express Quote file has been moved successfully", 360, 110, "Express Quote");
                oWnd.add_close(function () { window.location.reload(); });
            }

            function confirmCallBackfnVendor(arg) {
                document.getElementById("<%=tbVendor.ClientID%>").focus();
            }

            function confirmCallBackfnTail(arg) {
                document.getElementById("<%=tbTailNum.ClientID%>").focus();
            }

            function confirmCallBackfnTypeCode(arg) {
                document.getElementById("<%=tbTypeCode.ClientID%>").focus();
            }

            function confirmCallBackfnDescription(arg) {
                document.getElementById("<%=tbDescription.ClientID%>").focus();
            }

            function CheckTenthorHourMinValuebias(myfield) {
                var postbackval = true;
                var CompanyProfileSetting = "";
                companyprofileSetting = document.getElementById('<%= hdTimeDisplayTenMin.ClientID%>').value;
                if (companyprofileSetting == 1) {
                    var count = 0;
                    var strarr = myfield.value;
                    for (var i = 0; i < strarr.length; ++i) {
                        if (strarr[i] == ".")
                            count++;
                    }
                    if (count > 1) {
                        var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                        myfield.value = "0.0";
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                        //myfield.value = "0.0";
                        myfield.focus();
                        postbackval = false;
                    }
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(":") >= 0) {
                            var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                            myfield.value = "0.0";
                            ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                            //myfield.value = "0.0";
                            myfield.focus();
                            postbackval = false;
                        }
                        else if (myfield.value.indexOf(".") >= 0) {
                            var strarr = myfield.value.split(".");

                            if (strarr[0].length > 2) {
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                myfield.value = "0.0";
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                                //myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }

                            if (strarr[1].length > 1) {
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                myfield.value = "0.0";
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                                //myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            if (postbackval) {
                                if (strarr[0].length == 0)
                                    myfield.value = "0" + myfield.value;
                            }
                            if (postbackval) {
                                if (strarr[1].length == 0)
                                    myfield.value = myfield.value + "0";
                            }
                        }
                        else {
                            if (myfield.value.length > 2) {
                                //alert("Invalid format, Required format is NN.N ");
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                myfield.value = "0.0";
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                                //myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                myfield.value = myfield.value + ".0";
                            }
                        }
                }


            }
            else {
                var hourvalue = "00";
                var minvalue = "00";
                var count = 0;
                if (myfield.value.indexOf(".") >= 0) {
                    var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                    ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                    postbackval = false;
                }
                    //check for colon
                else if (myfield.value.indexOf(":") >= 0) {

                    var strarr = myfield.value.split(":");

                    if (strarr.length > 2) {
                        var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                        postbackval = false;
                    }


                    // hour validation
                    if (strarr[0].length <= 2) {
                        hourvalue = "00" + strarr[0];
                        hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                    }
                    else {
                        //alert("Invalid format, Required format is HH:MM");
                        var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                        myfield.value = "00:00";
                        myfield.focus();
                        postbackval = false;

                    }
                    // hour validation
                    if (postbackval) {
                        //minute Validation
                        if (strarr[1].length <= 2) {
                            if (Math.abs(strarr[1]) > 59) {
                                var ErrorMsg = radalert("Minute entry must be between 0 and 59", 330, 100);
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                                //                                    alert("Minute entry must be between 0 and 59");
                                hourvalue = "00";
                                minvalue = "00";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                minvalue = "00" + strarr[1];
                                minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                            }
                        }
                        //minute Validation
                    }
                    //check for colon
                }
                else {
                    if (myfield.value.length <= 2) {
                        hourvalue = "00" + myfield.value;
                        hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                    }
                    else {
                        var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                        myfield.value = "00:00";
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbBias.ClientID%>").select(); })
                            //                            alert("Invalid format, Required format is HH:MM");

                            myfield.focus();
                            postbackval = false;
                        }
                    }

                myfield.value = hourvalue + ":" + minvalue;
            }

            if (postbackval) {
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                ajaxManager.ajaxRequest("tbBias_TextChanged");
            }

            return postbackval;
        }

        function CheckTenthorHourMinValueLandbias(myfield) {
            var postbackval = true;
            var CompanyProfileSetting = "";
            companyprofileSetting = document.getElementById('<%= hdTimeDisplayTenMin.ClientID%>').value;
            if (companyprofileSetting == 1) {
                var count = 0;
                var strarr = myfield.value;
                for (var i = 0; i < strarr.length; ++i) {
                    if (strarr[i] == ".")
                        count++;
                }
                if (count > 1) {
                    //                        alert("Invalid format, Required format is NN.N ");
                    var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                    ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                    myfield.value = "0.0";
                    myfield.focus();
                    postbackval = false;
                }
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(":") >= 0) {
                        //                            alert("Invalid format, Required format is NN.N");
                        var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                        myfield.value = "0.0";
                        myfield.focus();
                        postbackval = false;
                    }
                    else if (myfield.value.indexOf(".") >= 0) {
                        var strarr = myfield.value.split(".");

                        if (strarr[0].length > 2) {
                            //                                alert("Invalid format, Required format is NN.N ");
                            var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                            ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }

                            if (strarr[1].length > 1) {
                                //                                alert("Invalid format, Required format is NN.N ");
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            if (postbackval) {
                                if (strarr[0].length == 0)
                                    myfield.value = "0" + myfield.value;
                            }
                            if (postbackval) {
                                if (strarr[1].length == 0)
                                    myfield.value = myfield.value + "0";
                            }
                        }
                        else {
                            if (myfield.value.length > 2) {
                                //                                alert("Invalid format, Required format is NN.N ");
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                myfield.value = myfield.value + ".0";
                            }
                        }
                }


            }
            else {
                var hourvalue = "00";
                var minvalue = "00";
                var count = 0;
                if (myfield.value.indexOf(".") >= 0) {
                    var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                    ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                    postbackval = false;
                }
                    //check for colon
                else if (myfield.value.indexOf(":") >= 0) {

                    var strarr = myfield.value.split(":");
                    // hour validation
                    if (strarr[0].length <= 2) {
                        hourvalue = "00" + strarr[0];
                        hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                    }
                    else {
                        //  alert("Invalid format, Required format is HH:MM");
                        var ErrorMsg = radalert("Invalid format, Required format is HH:MM.");
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                        myfield.value = "00:00";
                        myfield.focus();
                        postbackval = false;

                    }
                    // hour validation
                    if (postbackval) {
                        //minute Validation
                        if (strarr[1].length <= 2) {
                            if (Math.abs(strarr[1]) > 59) {
                                var ErrorMsg = radalert("Minute entry must be between 0 and 59.", 330, 100);
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                                hourvalue = "00";
                                minvalue = "00";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                minvalue = "00" + strarr[1];
                                minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                            }
                        }
                        //minute Validation
                    }
                    //check for colon
                }
                else {
                    if (myfield.value.length <= 2) {
                        hourvalue = "00" + myfield.value;
                        hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                    }
                    else {
                        var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbLandBias.ClientID%>").select(); })
                        //                            alert("Invalid format, Required format is HH:MM");
                        myfield.value = "00:00";
                        myfield.focus();
                        postbackval = false;
                    }
                }

            myfield.value = hourvalue + ":" + minvalue;
        }

        if (postbackval) {
            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                ajaxManager.ajaxRequest("tbLandBias_TextChanged");
            }

            return postbackval;
        }

        function CheckTenthorHourMinValueEte(myfield) {
            var postbackval = true;
            var CompanyProfileSetting = "";
            companyprofileSetting = document.getElementById('<%= hdTimeDisplayTenMin.ClientID%>').value;
            if (companyprofileSetting == 1) {
                var count = 0;
                var strarr = myfield.value;
                for (var i = 0; i < strarr.length; ++i) {
                    if (strarr[i] == ".")
                        count++;
                }
                if (count > 1) {
                    var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                    myfield.value = "0.0";
                    ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                    //                        alert("Invalid format, Required format is NN.N ");
                    //                        myfield.value = "0.0";
                    //                        myfield.focus();
                    postbackval = false;
                }
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(":") >= 0) {
                        var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                        myfield.value = "0.0";
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                        //                            alert("Invalid format, Required format is NN.N");
                        //                            myfield.value = "0.0";
                        //                            myfield.focus();
                        postbackval = false;
                    }
                    else if (myfield.value.indexOf(".") >= 0) {
                        var strarr = myfield.value.split(".");

                        if (strarr[0].length > 2) {
                            var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                            myfield.value = "0.0";
                            ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                                //                                alert("Invalid format, Required format is NN.N ");
                                //                                myfield.value = "0.0";
                                //                                myfield.focus();
                                postbackval = false;
                            }

                            if (strarr[1].length > 1) {
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                myfield.value = "0.0";
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                                //                                myfield.value = "0.0";
                                //                                myfield.focus();
                                postbackval = false;
                            }
                            if (postbackval) {
                                if (strarr[0].length == 0)
                                    myfield.value = "0" + myfield.value;
                            }
                            if (postbackval) {
                                if (strarr[1].length == 0)
                                    myfield.value = myfield.value + "0";
                            }
                        }
                        else {
                            if (myfield.value.length > 2) {
                                var ErrorMsg = radalert("Invalid format, Required format is NN.N", 330, 100);
                                myfield.value = "0.0";
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                                //                                alert("Invalid format, Required format is NN.N ");

                                //                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                myfield.value = myfield.value + ".0";
                            }
                        }
                }


            }
            else {
                var hourvalue = "00";
                var minvalue = "00";
                var count = 0;
                if (myfield.value.indexOf(".") >= 0) {
                    var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                    ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                    postbackval = false;
                }
                    //check for colon
                else if (myfield.value.indexOf(":") >= 0) {

                    var strarr = myfield.value.split(":");
                    // hour validation
                    if (strarr[0].length <= 2) {
                        hourvalue = "00" + strarr[0];
                        hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                    }
                    else {
                        var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                        //                            myfield.value = "00:00";
                        //                            myfield.focus();
                        postbackval = false;

                    }
                    // hour validation
                    if (postbackval) {
                        //minute Validation
                        if (strarr[1].length <= 2) {
                            if (Math.abs(strarr[1]) > 59) {
                                var ErrorMsg = radalert("Minute entry must be between 0 and 59", 330, 100);
                                ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                                //                                    alert("Minute entry must be between 0 and 59");
                                hourvalue = "00";
                                minvalue = "00";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                minvalue = "00" + strarr[1];
                                minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                            }
                        }
                        //minute Validation
                    }
                    //check for colon
                }
                else {
                    if (myfield.value.length <= 2) {
                        hourvalue = "00" + myfield.value;
                        hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                    }
                    else {

                        var ErrorMsg = radalert("Invalid format, Required format is HH:MM.", 330, 100);
                        ErrorMsg.add_close(function () { document.getElementById("<%=tbETE.ClientID%>").select(); })
                        //                            myfield.value = "00:00";
                        //                            myfield.focus();
                        postbackval = false;
                    }
                }

            myfield.value = hourvalue + ":" + minvalue;

        }

            myfield.value = DefaultEteResult(myfield.value, companyprofileSetting);

        if (postbackval) {
            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                ajaxManager.ajaxRequest("tbETE_TextChanged");
            }

            return postbackval;
        }


        function fnAllowNumericAndOneDecimal(fieldname, myfield, e) {
            var key;
            var keychar;
            var allowedString = "0123456789" + ".";

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;
            keychar = String.fromCharCode(key);

            if (keychar == ".") {

                if (myfield.value.indexOf(keychar) > -1) {
                    return false;
                }
            }

            if (fieldname == "override") {
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") < myfield.value.length - 1) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length >= 2)
                                return false;
                            if (strarr[1].length > 0 && strarr[1].length == 1)
                                return false;
                        }
                    }
                    else {
                        if (myfield.value.length >= 2 && keychar != ".")
                            return false;
                    }
                }
            }
            // control keys
            if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                return true;
                // numeric values and allow slash("/")
            else if ((allowedString).indexOf(keychar) > -1)
                return true;
            else
                return false;
        }

        </script>
        <script type="text/javascript">
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html
                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker

                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    var step = "Date_TextChanged";
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function hide() {
                var datePicker = $find("<%= RadDatePicker1.ClientID%>");
                datePicker.hidePopup();
                return true;
            }


            function tbDate_OnKeyDown(sender, event) {

                var step = "Date_TextChanged";
                if (event.keyCode == 9) {
                    parseDate(sender, event);
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                    if (step) {
                        ajaxManager.ajaxRequest(step);
                    }
                    return true;
                }

            }

            function ontbDateKeyPress(sender, e) {

                // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                var dateSeparator = null;
                var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                if (dotSeparator != -1) { dateSeparator = '.'; }
                else if (slashSeparator != -1) { dateSeparator = '/'; }
                else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                else {
                    alert("Invalid Date");
                }
                // allow dot, slash and hypen characters... if any other character, throw alert
                return fnAllowNumericAndChar($find("<%=RadDatePicker1.ClientID %>"), e, dateSeparator);
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }
            function OnClientFleetCharterClose(oWnd, args) {
                //alert('Close');
                var step = "TailNum_TextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }

            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadFleetNewCharter" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFleetCharterClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetNewCharterRate.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Width="450px" Height="400px"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAirportCatalog" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartAirport" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientICAOClose1" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Width="450px" Height="400px" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdArriveAirport" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientICAOClose2" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPower" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientPowerClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPowerSetting" runat="server" VisibleOnPageLoad="false" Height="190px"
                Width="400px" Modal="true" BackColor="#DADADA" Behaviors="Close" VisibleStatusbar="false"
                Title="Aircraft Power Settings">
                <ContentTemplate>
                    <table width="100%" class="box1">
                        <tr>
                            <td>Aircraft:
                                <asp:Label ID="lbAircraft" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="preflight-custom-grid">
                                <telerik:RadGrid ID="dgPowerSetting" OnNeedDataSource="dgPowerSetting_BindData" runat="server"
                                    AllowSorting="true" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                                    PageSize="5" Width="100%">
                                    <MasterTableView CommandItemDisplay="None" AllowSorting="false" DataKeyNames="Power,PowerSetting,TAS,HrRange,TOBias,LandBias"
                                        AllowFilteringByColumn="false">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="PowerSetting" HeaderText="PowerSetting" CurrentFilterFunction="Contains"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TAS" HeaderText="TAS" CurrentFilterFunction="Contains"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HrRange" HeaderText="Hr.Range" CurrentFilterFunction="Contains"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TOBias" HeaderText="TOBias" CurrentFilterFunction="Contains"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LandBias" HeaderText="LandBias" CurrentFilterFunction="Contains"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        <PagerStyle AlwaysVisible="false" />
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="true" />
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="bottom">
                                <asp:Button ID="btPowerSelect" runat="server" CssClass="button" Text="Select" OnClick="btPowerSelect_Click" />
                                <asp:Button ID="btPowerCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btPowerCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCrewRules" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewRuleClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyRulesPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTOBias" runat="server" VisibleOnPageLoad="false" Width="600px"
                Height="180px" Behaviors="Close" Modal="true" VisibleStatusbar="false" Title="Airport Bias">
                <ContentTemplate>
                    <table class="box1">
                        <tr>
                            <td class="tdLabel80">ICAO
                            </td>
                            <td class="tdLabel120">City
                            </td>
                            <td class="tdLabel80">State
                            </td>
                            <td class="tdLabel150">Country
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbDepartsICAO" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbDepartsCity" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbDepartsState" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbDepartsCountry" runat="server" Visible="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">Airport
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lbDepartsAirport" runat="server" Visible="true" CssClass="text150"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                                    <tr>
                                        <td class="tdLabel100">Takeoff Bias
                                        </td>
                                        <td class="tdLabel40">
                                            <asp:Label ID="lbDepartsTakeoffbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                        <td class="tdLabel100">Landing Bias
                                        </td>
                                        <td>
                                            <asp:Label ID="lbDepartsLandingbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdLandBias" runat="server" VisibleOnPageLoad="false" Width="600px"
                Height="180px" Behaviors="Close" Modal="true" VisibleStatusbar="false" Title="Airport Bias">
                <ContentTemplate>
                    <table class="box1">
                        <tr>
                            <td class="tdLabel80">ICAO
                            </td>
                            <td class="tdLabel120">City
                            </td>
                            <td class="tdLabel80">State
                            </td>
                            <td class="tdLabel150">Country
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbArrivesICAO" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbArrivesCity" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbArrivesState" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbArrivesCountry" runat="server" Visible="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">Airport
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lbArrivesAirport" runat="server" Visible="true" CssClass="text150"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                                    <tr>
                                        <td class="tdLabel100">Takeoff Bias
                                        </td>
                                        <td class="tdLabel40">
                                            <asp:Label ID="lbArrivesTakeoffbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                        <td class="tdLabel100">Landing Bias
                                        </td>
                                        <td>
                                            <asp:Label ID="lbArrivesLandingbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTailNum" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdType" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientTypeClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdVendor" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientVendorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/VendorMasterPopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSalesPerson" runat="server" OnClientResizeEnd="GetDimensions"
                Title="SalesPerson" OnClientClose="OnClientCloseSalesPersonPopup" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/CharterQuote/SalesPersonPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdLeadSource" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseLeadSourcePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/LeadSourcePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCharterQuotCustomerPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadDatePicker1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="New_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Edit_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Delete_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Save_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Cancel_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnTransferTOCQ_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Source_SelectedIndexChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Vendor_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="TailNum_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="TypeCode_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbDepart_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbArrive_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbLocalDate_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbArrivalDate_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Search_Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rbDomestic_SelectedIndexChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="chkPOS_CheckChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="chkTaxable_CheckChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbMiles_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbETE_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbRON_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbDayRoom_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbLegRate_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbPaxCount_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="TaxRate_TextChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <ClientEvents OnRequestStart="RequestStart" OnResponseEnd="ResponseEnd" />
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="account-body">
        <table width="980px">
            <tr>
                <td colspan="2">
                    <table width="724px">
                        <tr>
                            <td class="tab-nav-top">
                                <span class="head-title">Express Quote</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="nav-3"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnNewFile" runat="server" CssClass="button" Text="New" OnClick="New_Click" />
                                <asp:Button ID="btnEditFile" runat="server" CssClass="button" Text="Edit" OnClick="Edit_Click" />
                                <asp:Button ID="btnDeleteFile" runat="server" CssClass="button" Text="Delete" OnClick="Delete_Click" />
                            </td>
                            <td class="tdLabel530" align="right" valign="middle">
                                <div class="mandatory">
                                    <span>Bold</span> Indicates required field
                                </div>
                            </td>
                            <td align="right">
                                <asp:LinkButton ID="lnkReport" runat="server" CssClass="print_preview_icon" ToolTip="Express Quote Report Writer"
                                    OnClick="lnkReport_Click" CausesValidation="false"></asp:LinkButton>
                            </td>
                            <td align="right">
                                <a href="../../Help/ViewHelp.aspx?Screen=ExpressQuoteHelp" class="help-icon" target="_blank"
                                    title="Help"></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="border-box-CharterQuote" width="700px">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" CssClass="SearchBox_sc_crew" AutoPostBack="true" ID="tbbtSearch"
                                                OnTextChanged="Search_Click"></asp:TextBox>
                                        </td>
                                        <td class="tdLabel30">
                                            <asp:Button runat="server" CssClass="searchsubmitbutton" ID="btnSearch" OnClick="Search_Click" />
                                        </td>
                                        <td class="tdLabel100" align="left">
                                            <asp:LinkButton ID="lnkPaxRoaster" runat="server" CausesValidation="false" OnClientClick="javascript:OpenCQSearch();return false;"
                                                Text="See All Files" CssClass="link_small"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="tdLabel70">File No.
                                                    </td>
                                                    <td class="tdLabel120">
                                                        <asp:TextBox ID="tbFileNumber" ReadOnly="true" BackColor="Gray" ForeColor="White"
                                                            runat="server" CssClass="text80"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel85">Quote No.
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbQuoteNumber" ReadOnly="true" BackColor="Gray" ForeColor="White"
                                                            runat="server" CssClass="text80"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left">
                                            <asp:Label CssClass="alert-text" runat="server" ID="lbTripSearchMessage" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <div style="width: 385px;">
                                                <fieldset>
                                                    <legend>Source</legend>
                                                    <div style="width: 365px; height: 192px; padding: 0px 0 0px 0;">
                                                        <table cellpadding="1" cellspacing="1" width="100%">
                                                            <tr>
                                                                <td colspan="3" align="left" valign="top">
                                                                    <asp:RadioButtonList ID="rblSource" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Source_SelectedIndexChanged"
                                                                        AutoPostBack="true">
                                                                        <asp:ListItem Value="1">Vendor</asp:ListItem>
                                                                        <asp:ListItem Value="2" Selected="True">Inside</asp:ListItem>
                                                                        <asp:ListItem Value="3">Type</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" class="tdLabel90">
                                                                    <asp:Label ID="lbVendor" runat="server" Text="Vendor"></asp:Label>
                                                                </td>
                                                                <td valign="top" class="tdLabel120">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbVendor" runat="server" CssClass="text70" MaxLength="5" AutoPostBack="true"
                                                                                    OnTextChanged="Vendor_TextChanged"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnVendor" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnVendor" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdVendor');return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbVendorDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td align="left" colspan="2" valign="top">
                                                                    <asp:Label ID="lbcvVendor" CssClass="alert-text" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="lbTailNumber" runat="server" Text="Tail No."></asp:Label>
                                                                </td>
                                                                <td valign="top" colspan="2">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbTailNum" runat="server" CssClass="text70" MaxLength="9" AutoPostBack="true"
                                                                                    OnTextChanged="TailNum_TextChanged"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnTailNum" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnTailNumber" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdTailNum');return false;" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnTailNumberSearch" CssClass="charter-rate-button" runat="server"
                                                                                    OnClientClick="javascript:openFleetNewCharter();return false;" ToolTip="Fleet Charter Rates" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td align="left" colspan="2" valign="top">
                                                                    <asp:Label ID="lbcvTailNum" CssClass="alert-text" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="lbTypeCode" runat="server" Text="Type Code"></asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbTypeCode" runat="server" CssClass="text70" MaxLength="10" AutoPostBack="true"
                                                                                    OnTextChanged="TypeCode_TextChanged"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdnTypeCode" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnTypeCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdType');return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbTypeCodeDes" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td align="left" colspan="2" valign="top">
                                                                    <asp:Label ID="lbcvTypeCode" runat="server" class="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">Description
                                                                </td>
                                                                <td valign="top" colspan="2">
                                                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="40" CssClass="text200"
                                                                        onblur="limitnofotext(this,40)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 320px;">
                                                <fieldset>
                                                    <legend>Charges</legend>
                                                    <table>
                                                        <tr>
                                                            <td class="tdLabel140">
                                                                <asp:Label ID="lbFltHrs" runat="server" Text="Flight Hours"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel50">
                                                                <asp:TextBox ID="tbHrs" runat="server" CssClass="text70" ReadOnly="true" BackColor="Gray"
                                                                    ForeColor="White"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbRON" runat="server" Text="RON Crew Charge"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_150">
                                                                <telerik:RadNumericTextBox ID="tbRONCrewCharge" runat="server" Type="Currency" Culture="en-US"
                                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" MaxLength="17"
                                                                    ReadOnly="true" BackColor="Gray" ForeColor="White" />
                                                                <%-- <asp:TextBox ID="tbRONCrewCharge" runat="server" CssClass="text120" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White"></asp:TextBox>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbLandingFees" runat="server" Text="Landing Fees"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_150">
                                                                <telerik:RadNumericTextBox ID="tbLandingFees" runat="server" Type="Currency" Culture="en-US"
                                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" MaxLength="17"
                                                                    ReadOnly="true" BackColor="Gray" ForeColor="White" />
                                                                <%-- <asp:TextBox ID="tbLandingFees" runat="server" CssClass="text120" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White"></asp:TextBox>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbDailyUseAdj" runat="server" Text="Daily Usage Adjustment"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_150">
                                                                <telerik:RadNumericTextBox ID="tbDailyUseAdj" runat="server" Type="Currency" Culture="en-US"
                                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White" />
                                                                <%--<asp:TextBox ID="tbDailyUseAdj" runat="server" CssClass="text120" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White"></asp:TextBox>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbFlightCharges" runat="server" Text="Flight Charges"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_150">
                                                                <telerik:RadNumericTextBox ID="tbFlightCharges" runat="server" Type="Currency" Culture="en-US"
                                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White" />
                                                                <%-- <asp:TextBox ID="tbFlightCharges" runat="server" CssClass="text120" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White"></asp:TextBox>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbSegFeesChrg" runat="server" Text="Segment Fees & Taxes"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_150">
                                                                <telerik:RadNumericTextBox ID="tbSegFeesChrg" runat="server" Type="Currency" Culture="en-US"
                                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White" />
                                                                <%-- <asp:TextBox ID="tbSegFeesChrg" runat="server" CssClass="text120" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White"></asp:TextBox>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                                <div style="padding: 5px 0 0 0;">
                                                    <fieldset>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="tdLabel149">
                                                                    <asp:Label ID="lbTotQuote" runat="server" Text="Total Quote"></asp:Label>
                                                                </td>
                                                                <td class="pr_radtextbox_150">
                                                                    <telerik:RadNumericTextBox ID="tbTotalQuote" runat="server" Type="Currency" Culture="en-US"
                                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" ReadOnly="true" Style="background-color: gray !important; color: white;" />
                                                                    <%--<asp:TextBox ID="tbTotalQuote" runat="server" CssClass="text120" ReadOnly="true"
                                                                        BackColor="Gray" ForeColor="White" Text="0.00"></asp:TextBox>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="width: 385px;">
                                                <fieldset>
                                                    <legend>Source Information</legend>
                                                    <table>
                                                        <tr>
                                                            <td class="tdLabel90">Salesperson
                                                            </td>
                                                            <td align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbSalesPerson" runat="server" CssClass="text70" MaxLength="5" AutoPostBack="true"
                                                                                OnTextChanged="tbSalesPerson_TextChanged"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdnSalesPerson" runat="server" />
                                                                            <asp:HiddenField ID="hdnFirstName" runat="server" />
                                                                            <asp:HiddenField ID="hdnLastName" runat="server" />
                                                                            <asp:HiddenField ID="hdnMiddleName" runat="server" />
                                                                            <asp:HiddenField ID="hdnSPBusinessPhone" runat="server" />
                                                                            <asp:HiddenField ID="hdnSPBusinessEmail" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btnSalesPerson" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdSalesPerson');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbSalesPersonDesc" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbcvSalesPerson" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Lead Source
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbLeadSource" runat="server" CssClass="text70" MaxLength="4" AutoPostBack="true"
                                                                                OnTextChanged="tbLeadSource_TextChanged"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdnLeadSource" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btnLeadSource" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('rdLeadSource');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbleadSource" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbcvLeadSource" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="width: 320px;">
                                                <fieldset>
                                                    <legend>Customer Information</legend>
                                                    <table>
                                                        <tr>
                                                            <td>Customer
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCustomer" runat="server" CssClass="text90" MaxLength="5" AutoPostBack="true"
                                                                                OnTextChanged="tbCustomer_TextChanged"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdnCustomerID" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btnCustomer" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('radCQCPopups');return false;" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <asp:Label ID="lbcvCustomerName" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel140">Customer Name
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbCustomerName" runat="server" CssClass="text110" ReadOnly="true"
                                                                    BackColor="Gray" ForeColor="White"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-6"></td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="dgLegs" runat="server" AllowSorting="false" Visible="true" OnNeedDataSource="Legs_BindData"
                                    OnItemCommand="Legs_ItemCommand" AutoGenerateColumns="false" PageSize="10" OnSelectedIndexChanged="Legs_SelectedIndexChanged"
                                    OnItemCreated="Legs_ItemCreated" AllowPaging="false" AllowFilteringByColumn="true"
                                    PagerStyle-AlwaysVisible="true" Height="341px" Width="710px" OnDeleteCommand="Legs_DeleteCommand">
                                    <MasterTableView DataKeyNames="LegNum" CommandItemDisplay="Bottom" AllowPaging="false"
                                        AllowFilteringByColumn="false" AllowSorting="false">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" ShowFilterIcon="false"
                                                HeaderStyle-Width="40px">
                                            </telerik:GridBoundColumn>
                                            <%-- <telerik:GridBoundColumn DataField="DepartAir" HeaderText="Departure" ShowFilterIcon="false"
                                                HeaderStyle-Width="70px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="Arrival" ShowFilterIcon="false"
                                                HeaderStyle-Width="70px">
                                            </telerik:GridBoundColumn>--%>
                                            <telerik:GridTemplateColumn HeaderText="Departs" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                UniqueName="DepartAir">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDepartAir" runat="server" Text='<%# Eval("DepartAir") %>'
                                                        CssClass="tdtext100" CausesValidation="false" OnClientClick='<%#Eval("DepartAirportID", "return OpenDepartAirportCatalog(\"{0}\")")%>'>                                               
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Arrives" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                UniqueName="ArrivalAir">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkArrivalAir" runat="server" Text='<%# Eval("ArrivalAir") %>'
                                                        CssClass="tdtext100" CausesValidation="false" OnClientClick='<%#Eval("ArrivalAirportID", "return   OpenArrivalAirportCatalog(\"{0}\")")%>'>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Departure Date" DataType="System.DateTime"
                                                HeaderStyle-Width="130px" ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ETE" HeaderText="ETE" HeaderStyle-Width="50px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Distance" HeaderText="Distance" ShowFilterIcon="false"
                                                HeaderStyle-Width="80px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrival Date" HeaderStyle-Width="130px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RON" HeaderText="RON" HeaderStyle-Width="50px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DayRoom" HeaderText="Day Room" HeaderStyle-Width="70px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PaxTotal" HeaderText="PAX Count" HeaderStyle-Width="70px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>
                                            <%--   <telerik:GridBoundColumn DataField="Charges" HeaderText="Flight Charges" HeaderStyle-Width="100px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>--%>
                                            <telerik:GridTemplateColumn HeaderText="Flight Charges" FilterDelay="4000" ItemStyle-CssClass="fc_textbox"
                                                HeaderStyle-Width="100px" UniqueName="Charges" CurrentFilterFunction="Contains">
                                                <ItemTemplate>
                                                    <telerik:RadNumericTextBox ID="tbFlightcharge" runat="server" Type="Currency" Culture="en-US"
                                                        DbValue='<%# Bind("Charges") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                        ReadOnly="true">
                                                    </telerik:RadNumericTextBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridCheckBoxColumn DataField="Tax" HeaderText="Tax" HeaderStyle-Width="40px">
                                            </telerik:GridCheckBoxColumn>
                                            <%-- <telerik:GridBoundColumn DataField="TaxRate" HeaderText="Tax Rate" HeaderStyle-Width="80px">
                                            </telerik:GridBoundColumn>--%>
                                            <telerik:GridTemplateColumn HeaderText="Tax Rate" FilterDelay="4000" ItemStyle-CssClass="fc_textbox"
                                                HeaderStyle-Width="80px" UniqueName="TaxRate" CurrentFilterFunction="Contains">
                                                <ItemTemplate>
                                                    <telerik:RadNumericTextBox ID="tbTaxRate" runat="server" Type="Currency" Culture="en-US"
                                                        DbValue='<%# Bind("TaxRate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                        ReadOnly="true">
                                                    </telerik:RadNumericTextBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridCheckBoxColumn DataField="Intl" HeaderText="International" HeaderStyle-Width="90px">
                                            </telerik:GridCheckBoxColumn>
                                            <telerik:GridCheckBoxColumn DataField="POS" HeaderText="POS" HeaderStyle-Width="40px">
                                            </telerik:GridCheckBoxColumn>
                                            <%-- <telerik:GridBoundColumn DataField="Rate" HeaderText="Leg Rates" HeaderStyle-Width="90px"
                                                ShowFilterIcon="false">
                                            </telerik:GridBoundColumn>--%>
                                            <telerik:GridTemplateColumn HeaderText="Leg Rates" ItemStyle-CssClass="fc_textbox"
                                                HeaderStyle-Width="50px" UniqueName="LegRates" CurrentFilterFunction="Contains">
                                                <ItemTemplate>
                                                    <telerik:RadNumericTextBox ID="tbRate" runat="server" Type="Currency" Culture="en-US"
                                                        DbValue='<%# Bind("Rate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                        ReadOnly="true">
                                                    </telerik:RadNumericTextBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="CD" HeaderText="CD" HeaderStyle-Width="30px"
                                                ShowFilterIcon="false" Display="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TA" HeaderText="TA" HeaderStyle-Width="40px"
                                                ShowFilterIcon="false" Display="false">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemTemplate>
                                            <div class="grid_icon">
                                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add Leg" CssClass="add-icon-grid"
                                                    CommandName="InitInsert"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkInsert" runat="server" ToolTip="Insert Leg" CssClass="insert-icon-grid"
                                                    CommandName="PerformInsert"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDelete();"
                                                    CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete Leg"></asp:LinkButton>
                                            </div>
                                            <div>
                                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                            </div>
                                        </CommandItemTemplate>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                    <GroupingSettings CaseSensitive="false" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-3"></td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadPanelBar ID="pnlTrip" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                    runat="server" Width="100%">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Leg Details" Value="pnlItemTrip">
                                            <ContentTemplate>
                                                <div class="CharterQuote-boxsc">
                                                    <div style="width: 707px; padding: 10px 0px 5px 0px;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel100">
                                                                                <span class="mnd_text">
                                                                                    <asp:Label ID="lbDepartIcao" runat="server" Text="Departure ICAO"></asp:Label></span>
                                                                            </td>
                                                                            <td class="tdLabel150">
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbDepart" runat="server" CssClass="text60" MaxLength="4" OnTextChanged="tbDepart_TextChanged"
                                                                                                AutoPostBack="true">
                                                                                            </asp:TextBox>
                                                                                            <asp:HiddenField ID="hdnDepart" runat="server" />
                                                                                            <asp:HiddenField ID="hdnHomebaseAirport" runat="server" />
                                                                                            <asp:HiddenField ID="hdnCountryID" runat="server" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button runat="server" ID="btnClosestIcao" CssClass="browse-button" OnClientClick="javascript:openWinAirport('DEPART');return false;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" colspan="2">
                                                                                <asp:Label ID="lbDepart" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                <asp:Label ID="lbcvDepart" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="mnd_text">
                                                                                    <asp:Label ID="lbArrive" runat="server" Text="Arrival ICAO"></asp:Label></span>
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbArrival" AutoPostBack="true" runat="server" MaxLength="4" CssClass="text60"
                                                                                                OnTextChanged="tbArrive_TextChanged">
                                                                                            </asp:TextBox>
                                                                                            <asp:HiddenField ID="hdnArrival" runat="server" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button runat="server" ID="btnArrival" CssClass="browse-button" OnClientClick="javascript:openWinAirport('ARRIVAL');return false;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" colspan="2">
                                                                                <asp:Label ID="lbArrival" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                <asp:Label ID="lbcvArrival" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <fieldset>
                                                                        <asp:RadioButtonList ID="rbDomestic" runat="server" RepeatDirection="Vertical" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="rbDomestic_SelectedIndexChanged">
                                                                            <asp:ListItem Value="1" Selected="True">Domestic</asp:ListItem>
                                                                            <asp:ListItem Value="2">International</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </fieldset>
                                                                </td>
                                                                <td>
                                                                    <fieldset>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkPOS" runat="server" Text="POS" AutoPostBack="true" OnCheckedChanged="chkPOS_CheckChanged" />
                                                                                </td>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkTaxable" runat="server" Text="Taxable" AutoPostBack="true" OnCheckedChanged="chkTaxable_CheckChanged" />
                                                                                    </td>
                                                                                </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="width: 707px; padding: 0px 0px 10px 0px;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel90">
                                                                                <span class="mnd_text">Departure</span>
                                                                            </td>
                                                                            <td valign="top" class="tdLabel140" align="left">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2" class="mnd_text">Local
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top" class="tdLabel80">
                                                                                            <asp:TextBox ID="tbLocalDate" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onclick="ShowWarningMessage('date',this,event);" OnTextChanged="tbLocalDate_TextChanged"
                                                                                                AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70"
                                                                                                runat="server" onchange="parseDate(this, event);"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtlocaltime" runat="server" OnTextChanged="tbLocalDate_TextChanged"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" AutoPostBack="true" SelectionOnFocus="SelectAll"
                                                                                                Mask="<0..23>:<0..59>">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span class="mnd_text">Arrival</span>
                                                                            </td>
                                                                            <td align="left" valign="top">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2" class="mnd_text">Local
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:TextBox ID="tbArrivalDate" OnTextChanged="tbArrivalDate_TextChanged" AutoPostBack="true"
                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onchange="parseDate(this, event);" onclick="ShowWarningMessage('date',this,event);"
                                                                                                runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtArrivalTime" AutoPostBack="true" OnTextChanged="tbArrivalDate_TextChanged"
                                                                                                runat="server" SelectionOnFocus="SelectAll" Mask="<0..23>:<0..59>" onfocus="ShowWarningMessage('time',this,event);">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <fieldset>
                                                                        <legend>Distance And Time</legend>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="tdLabel80">
                                                                                    <asp:Label runat="Server" ID="lbMiles" Text="Miles(N)"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbMiles" OnTextChanged="tbMiles_TextChanged" MaxLength="5" onfocus="this.select();"
                                                                                        onKeyPress="return fnAllowNumericAndOneDecimal('miles',this, event)" AutoPostBack="true"
                                                                                        CssClass="text60" runat="server"></asp:TextBox>
                                                                                    <asp:HiddenField runat="server" ID="hdnMiles" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>ETE
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbETE" AutoPostBack="true" CssClass="text60" onfocus="this.select();"
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,': .')" onchange="javascript:return CheckTenthorHourMinValueEte(this);"
                                                                                        OnTextChanged="tbETE_TextChanged" MaxLength="5" runat="server"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <div style="height: 18px;">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                                <td>
                                                                    <fieldset>
                                                                        <legend>RON</legend>
                                                                        <table>
                                                                            <tr>
                                                                                <td valign="top" class="tdLabel90">RON
                                                                                </td>
                                                                                <td valign="top">
                                                                                    <asp:TextBox ID="tbRON" runat="server" CssClass="text60" MaxLength="3" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                        onfocus="ShowWarningMessage('ron',this,event);" AutoPostBack="true" OnTextChanged="tbRON_TextChanged"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">Day Room
                                                                                </td>
                                                                                <td valign="top">
                                                                                    <asp:TextBox ID="tbDayRoom" runat="server" onKeyPress="return fnAllowNumeric(this,event,'')"
                                                                                        MaxLength="2" CssClass="text60" AutoPostBack="true" OnTextChanged="tbDayRoom_TextChanged">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <div style="height: 18px;">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="width: 707px; padding: 0px 0px 10px 0px;">
                                                        <table>
                                                            <tr>
                                                                <td class="tdLabel90">Leg Rates
                                                                </td>
                                                                <td class="pr_radtextbox_85">
                                                                    <telerik:RadNumericTextBox ID="tbLegRate" runat="server" Type="Currency" Culture="en-US"
                                                                        MaxLength="17" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right"
                                                                        AutoPostBack="true" OnTextChanged="tbLegRate_TextChanged">
                                                                    </telerik:RadNumericTextBox>
                                                                    <%-- <asp:TextBox ID="tbLegRate" MaxLength="17" CssClass="text112" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"
                                                                        runat="server" AutoPostBack="true" OnTextChanged="tbLegRate_TextChanged"></asp:TextBox>--%>
                                                                </td>
                                                                <td class="tdLabel30"></td>
                                                                <td class="tdLabel102">Passenger Count
                                                                </td>
                                                                <td class="tdLabel80">
                                                                    <asp:TextBox ID="tbPaxCount" MaxLength="3" onKeyPress="return fnAllowNumeric(this,event,'')"
                                                                        runat="server" CssClass="text48" AutoPostBack="true" OnTextChanged="tbPaxCount_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td class="tdLabel60">Tax Rate
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbTaxRate" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"
                                                                        runat="server" CssClass="text80" AutoPostBack="true" OnTextChanged="TaxRate_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbCharges" MaxLength="17" onKeyPress="return fnAllowNumericAndChar(this,event,'.')"
                                                                        CssClass="text112" runat="server" Visible="false"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <%--<div>Codes for Calculation only</div>--%>
                                                    <div style="width: 707px;">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbUtcDate" CssClass="text70" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                                        AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                        onclick="showPopup(this, event);" onchange="parseDate(this, event); " runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="cq_leg_radmask">
                                                                    <telerik:RadMaskedTextBox Visible="false" Display="false" ID="rmtUtctime" AutoPostBack="true" runat="server"
                                                                        SelectionOnFocus="SelectAll" Mask="<0..23>:<0..59>">
                                                                    </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbHomeDate" AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onclick="showPopup(this, event)"
                                                                        CssClass="text70" onchange="parseDate(this, event);" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="cq_leg_radmask">
                                                                    <telerik:RadMaskedTextBox Visible="false" Display="false" ID="rmtHomeTime" AutoPostBack="true" runat="server"
                                                                        SelectionOnFocus="SelectAll" Mask="<0..23>:<0..59>">
                                                                    </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbArrivalUtcDate" AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                                        CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                        onchange="parseDate(this, event);" onclick="showPopup(this, event);" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="cq_leg_radmask">
                                                                    <telerik:RadMaskedTextBox Visible="false" Display="false" ID="rmtArrivalUtctime" AutoPostBack="true"
                                                                        runat="server" SelectionOnFocus="SelectAll" Mask="<0..23>:<0..59>">
                                                                    </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbArrivalHomeDate" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                                        AutoPostBack="true" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                        onchange="parseDate(this, event);" onclick="showPopup(this, event)" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="cq_leg_radmask">
                                                                    <telerik:RadMaskedTextBox Visible="false" Display="false" ID="rmtArrivalHomeTime" AutoPostBack="true"
                                                                        runat="server" SelectionOnFocus="SelectAll" Mask="<0..23>:<0..59>">
                                                                    </telerik:RadMaskedTextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="width: 707px;">
                                                        <asp:TextBox Visible="false" ID="tbPurpose" MaxLength="40" runat="server" CssClass="text420"
                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                    </div>
                                                    <div style="width: 704px;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <!--<OnTextChanged="tbBias_TextChanged"-->
                                                                    <asp:TextBox Visible="false" ID="tbBias" onfocus="this.select();" AutoPostBack="true"
                                                                        CssClass="text40" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"
                                                                        OnTextChanged="tbBias_TextChanged" onchange="javascript:return CheckTenthorHourMinValuebias(this);"
                                                                        MaxLength="5" OnClick="this.select();" runat="server">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton Visible="false" ID="imgbtnTOBias" ToolTip="Display Airport Bias"
                                                                        runat="server" OnClientClick="javascript:openWin('rdTOBias');return false;" CssClass="calc-icon" />
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbTAS" OnTextChanged="tbBias_TextChanged" onfocus="this.select();"
                                                                        MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)" AutoPostBack="true"
                                                                        CssClass="text60" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbPower" CssClass="text60" MaxLength="1" runat="server"
                                                                        AutoPostBack="true" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdPower" runat="server" />
                                                                    <asp:Button Visible="false" runat="server" ToolTip="Display Aircraft Power Settings"
                                                                        ID="btnPower" CssClass="browse-button" OnClientClick="javascript:openWin('rdPowerSetting');return false;" />
                                                                </td>
                                                                <td>
                                                                    <!--OnTextChanged="tbBias_TextChanged"-->
                                                                    <asp:TextBox Visible="false" ID="tbLandBias" AutoPostBack="true" CssClass="text40"
                                                                        onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,': .')"
                                                                        onchange="javascript:return CheckTenthorHourMinValueLandbias(this);" OnTextChanged="tbBias_TextChanged"
                                                                        MaxLength="5" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton Visible="false" ID="imgbtnLandingBias" ToolTip="Display Airport Bias"
                                                                        runat="server" OnClientClick="javascript:openWin('rdTOBias');return false;" CssClass="calc-icon" />
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbWind" OnTextChanged="tbWind_TextChanged" onfocus="this.select();"
                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'-')" onchange="javascript:return  checkNegativeValue(this);"
                                                                        MaxLength="5" AutoPostBack="true" CssClass="text60" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbcvPower" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:RadioButtonList Visible="false" ID="rblistWindReliability" OnSelectedIndexChanged="rblistWindReliability_SelectedIndexChanged"
                                                                        AutoPostBack="true" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1">50%</asp:ListItem>
                                                                        <asp:ListItem Value="2">75%</asp:ListItem>
                                                                        <asp:ListItem Value="3" Selected="True">85%</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="width: 707px;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbCrewRules" runat="server" CssClass="text90" MaxLength="4"
                                                                        OnTextChanged="tbCrewRules_TextChanged" AutoPostBack="true" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                        onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnCrewRules" runat="server" />
                                                                    <asp:Button Visible="false" runat="server" ID="btnCrewRules" CssClass="browse-button"
                                                                        OnClientClick="javascript:openWin('rdCrewRules');return false;" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbcvCrewRules" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox Visible="false" ID="tbOverride" AutoPostBack="true" runat="server" MaxLength="4"
                                                                        CssClass="text60" onchange="javascript:return checknumericValue(this);" Text="0.0"
                                                                        OnTextChanged="tbOverride_TextChanged">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox Visible="false" ID="chkEndOfDuty" runat="server" Text="End Of duty day" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbFar" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label Visible="false" ID="lbTotalFlight" MaxLength="5" runat="server" Text="0.0"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label Visible="false" ID="lbTotalDuty" MaxLength="5" runat="server" Text="0.0"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label Visible="false" ID="lbRest" MaxLength="5" runat="server" Text="0.0"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-3"></td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" class="tdLabel450">
                                            <asp:Button ID="btnTransferTOCQ" runat="server" CssClass="button" Text="Transfer To Charter Quote Manager"
                                                OnClick="btnTransferTOCQ_Click" />
                                        </td>
                                        <td align="right">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnNew" runat="server" CssClass="button" Text="New" OnClick="New_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" OnClick="Edit_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" OnClick="Delete_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="Save_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="Cancel_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="hdTimeDisplayTenMin" runat="server" />
                                <asp:HiddenField ID="hdShowRequestor" runat="server" />
                                <asp:HiddenField ID="hdHighlightTailno" runat="server" />
                                <asp:HiddenField ID="hdIsDepartAuthReq" runat="server" />
                                <asp:HiddenField ID="hdSetFarRules" runat="server" />
                                <asp:HiddenField ID="hdSetCrewRules" runat="server" />
                                <asp:HiddenField ID="hdSetWindReliability" runat="server" />
                                <asp:HiddenField ID="hdHomeCategory" runat="server" />
                                <asp:HiddenField ID="hdHomeChkListGroup" runat="server" />
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:HiddenField ID="hdnLegNum" runat="server" />
                                <asp:HiddenField ID="hdMaximumPassenger" Value="0" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" class="exception_box">
                    <telerik:RadPanelBar ID="pnlException" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                        runat="server">
                        <CollapseAnimation Type="None"></CollapseAnimation>
                        <Items>
                            <telerik:RadPanelItem Text="Exceptions" Font-Bold="true" Value="pnlItemException">
                                <ContentTemplate>
                                    <telerik:RadGrid ID="dgException" OnNeedDataSource="Exception_BindData" OnItemDataBound="Exception_ItemDataBound"
                                        runat="server" AllowSorting="true" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                                        ShowFooter="false" AllowPaging="false" GroupingEnabled="true" ShowGroupPanel="false"
                                        Width="247px" ShowStatusBar="false" PageSize="500">
                                        <MasterTableView CommandItemDisplay="None" DataKeyNames="Severity" AllowFilteringByColumn="false"
                                            ShowHeader="false" ShowFooter="false" AllowPaging="false">
                                            <GroupByExpressions>
                                                <telerik:GridGroupByExpression>
                                                    <SelectFields>
                                                        <telerik:GridGroupByField FieldAlias="Group" FieldName="SubModuleGroup"></telerik:GridGroupByField>
                                                    </SelectFields>
                                                    <GroupByFields>
                                                        <telerik:GridGroupByField FieldName="SubModuleID" SortOrder="None"></telerik:GridGroupByField>
                                                    </GroupByFields>
                                                </telerik:GridGroupByExpression>
                                            </GroupByExpressions>
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" UniqueName="Severity">
                                                    <ItemTemplate>
                                                        <center>
                                                            <asp:Image ID="imgSeverity" runat="server" />
                                                        </center>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="ExceptionDescription" HeaderText="Exception">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                            <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" SaveScrollPosition="True"></Scrolling>
                                            <Selecting AllowRowSelect="false" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                        <ExpandAnimation Type="None"></ExpandAnimation>
                    </telerik:RadPanelBar>
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:HiddenField ID="hdnTempHoursTxtbox" runat="server" />
                    <asp:HiddenField ID="hdnShowWarnMessage" runat="server" />
                    <asp:HiddenField ID="hdnCompSettingforshowWarn" runat="server" />
                    <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="CancelYes_Click" />
                    <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="CancelNo_Click" />
                    <asp:Button ID="btnDeleteFileYes" runat="server" Text="Button" OnClick="DeleteFileYes_Click" />
                    <asp:Button ID="btnDeleteFileNo" runat="server" Text="Button" OnClick="DeleteFileNo_Click" />
                    <asp:Label ID="lblContent" runat="server" Text="ExpressQuote" Visible="false"></asp:Label>
                    <asp:Button ID="btnTransferFileYes" runat="server" Text="Button" OnClick="TransferFileYes_Click" />
                    <asp:Button ID="btnTransferFileNo" runat="server" Text="Button" OnClick="TransferFileNo_Click" />
                    <asp:Button ID="btnConfirmAutoUpdateRateYes" runat="server" Text="Button" OnClick="ConfirmAutoUpdateRateYes_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
