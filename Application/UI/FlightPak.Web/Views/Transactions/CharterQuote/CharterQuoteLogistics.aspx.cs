﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class CharterQuoteLogistics : BaseSecuredPage
    {
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        private ExceptionManager exManager;
        private bool selectchangefired = false;
        private delegate void SaveSession();

        /// <summary>
        /// Wire up the SaveCharterQuoteToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveCharterQuoteFileSession = new SaveSession(SaveCharterQuoteToSession);
                        Master.SaveToSession = SaveCharterQuoteFileSession;

                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;

                        Master._DeleteClick += Delete_Click;
                        Master._SaveClick += Save_Click;
                        Master._CancelClick += Cancel_Click;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgLegs.ClientID));
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditFile, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        // Changes for Legnum in CQ Header
                        RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
                        Panel pnlFloater = (Panel)contentPlaceHolder.FindControl("pnlFloater");
                        manager.AjaxSettings.AddAjaxSetting(dgLegs, pnlFloater);

                        if (!IsPostBack)
                        {
                            hdnLegNum.Value = "1";

                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];

                                Int64 QuoteNumInProgress = 1;
                                if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                    QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                if (FileRequest.CQMains != null)
                                {
                                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                                    if (QuoteInProgress != null)
                                    {
                                        if (QuoteInProgress != null && QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).Count() > 0)
                                        {
                                            Master.BindLegs(dgLegs, true);
                                            if (dgLegs.Items.Count > 0)
                                            {
                                                dgLegs.Items[0].Selected = true;
                                            }
                                            LoadLegDetails();
                                        }
                                        else
                                            Response.Redirect("QuoteAndLegDetails.aspx?seltab=QuoteAndLeg");
                                    }
                                    else
                                        Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile");
                                }
                                else
                                    Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile");
                            }
                            else
                                Response.Redirect("CharterQuoteRequest.aspx?seltab=File");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    ApplyPermissions();
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void LoadLegDetails()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgLegs.SelectedItems.Count > 0)
                {
                    ClearFields();

                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                    if (FileRequest != null && FileRequest.CQMains != null)
                    {
                        Int64 QuoteNumInProgress = 1;
                        if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null)
                        {
                            GridDataItem item = (GridDataItem)dgLegs.SelectedItems[0];
                            CQLeg cqLeg = new CQLeg();

                            cqLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt16(item["LegNUM"].Text) && x.IsDeleted == false).FirstOrDefault();

                            if (cqLeg != null)
                            {
                                // Changes for Legnum in CQ Header
                                Master.FloatLegNum.Visible = true;
                                Master.FloatLegNum.Text = System.Web.HttpUtility.HtmlEncode(cqLeg.LegNUM.ToString());

                                hdnLegNum.Value = cqLeg.LegNUM.ToString();

                                if (cqLeg.DAirportID != null)
                                {
                                    hdDepartIcaoID.Value = cqLeg.DAirportID.ToString();
                                }

                                if (cqLeg.AAirportID != null)
                                {
                                    hdArriveIcaoID.Value = cqLeg.AAirportID.ToString();
                                }

                                CQchoice(cqLeg);

                                if (cqLeg.CQFBOLists != null && cqLeg.CQFBOLists.Count > 0)
                                {
                                    foreach (CQFBOList cqfbo in cqLeg.CQFBOLists)
                                    {
                                        UpdateScreenDetailsFromCQFBO(cqfbo);
                                    }
                                }

                                if (cqLeg.CQCateringLists != null && cqLeg.CQCateringLists.Count > 0)
                                {
                                    foreach (CQCateringList cqcatering in cqLeg.CQCateringLists)
                                    {
                                        UpdateScreenDetailsFromCQCatering(cqcatering);
                                    }
                                }

                                if (cqLeg.CQTransportLists != null && cqLeg.CQTransportLists.Count > 0)
                                {
                                    foreach (CQTransportList cqtransport in cqLeg.CQTransportLists)
                                    {
                                        UpdateScreenDetailsFromCQTransport(cqtransport);
                                    }
                                }

                                if (cqLeg.CQHotelLists != null && cqLeg.CQHotelLists.Count > 0)
                                {
                                    foreach (CQHotelList cqhotel in cqLeg.CQHotelLists)
                                    {
                                        UpdateScreenDetailsFromCQHotel(cqhotel);
                                    }
                                }

                                if (!string.IsNullOrEmpty(cqLeg.Notes))
                                {
                                    tbPaxNotes.Text = cqLeg.Notes.ToString();
                                    RadPanelItem radpnlitem = new RadPanelItem();
                                    radpnlitem = pnlNotes.FindItemByValue("radpnlItemNotes");
                                    radpnlitem.ForeColor = Color.Red;
                                    radpnlitem.Expanded = true;
                                }
                                else
                                {
                                    tbPaxNotes.Text = string.Empty;
                                    RadPanelItem radpnlitem = new RadPanelItem();
                                    radpnlitem = pnlNotes.FindItemByValue("radpnlItemNotes");
                                    radpnlitem.Expanded = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        #region MasterCatalog Popup text box change events

        /// <summary>
        /// To check unique Depart FBO Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DepartFBO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartFBO);
                        tbDepartFBOName.Text = string.Empty;
                        tbDepartFBOPhone.Text = string.Empty;
                        tbDepartFBOFax.Text = string.Empty;
                        tbDepartFBOEmail.Text = string.Empty;
                        lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdDepartFBOID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbDepartFBO.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdDepartIcaoID.Value);
                                    var ServCall = objDstsvc.GetFBOByAirportWithFilters(AirportID, 0, tbDepartFBO.Text.Trim(), false, false, false);
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList;

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            // RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg's Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                            if (!string.IsNullOrEmpty(objRetVal[0].FBOCD))
                                                tbDepartFBO.Text = objRetVal[0].FBOCD.ToString();
                                            hdDepartFBOID.Value = objRetVal[0].FBOID.ToString();

                                            if (objRetVal[0].FBOVendor != null && !string.IsNullOrEmpty(objRetVal[0].FBOVendor))
                                                tbDepartFBOName.Text = objRetVal[0].FBOVendor.ToString();
                                            if (objRetVal[0].PhoneNUM1 != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNUM1))
                                                tbDepartFBOPhone.Text = objRetVal[0].PhoneNUM1.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbDepartFBOFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbDepartFBOEmail.Text = objRetVal[0].ContactEmail.ToString();

                                            #region Previous Fbo Alert

                                            FileRequest = (CQFile)Session[Master.CQSessionKey];

                                            if (FileRequest.Mode == CQRequestActionMode.Edit)
                                            {

                                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                                                    {
                                                        CQLeg PrevLeg = new CQLeg();
                                                        PrevLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == (Convert.ToInt16(hdnLegNum.Value) - 1) && x.IsDeleted == false).FirstOrDefault();

                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt16(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();

                                                        if (PrevLeg != null && CurrLeg != null)
                                                        {
                                                            if (PrevLeg.CQLegID != 0)
                                                                PrevLeg.State = CQRequestEntityState.Modified;
                                                            else
                                                                PrevLeg.State = CQRequestEntityState.Added;

                                                            if (CurrLeg.DAirportID == PrevLeg.AAirportID)
                                                            {
                                                                CQFBOList PrevArrFBO = null;
                                                                if (PrevLeg.CQFBOLists != null && PrevLeg.CQFBOLists.Count > 0)
                                                                {
                                                                    PrevArrFBO = PrevLeg.CQFBOLists.Where(x => x.IsDeleted == false && x.RecordType == "FA").FirstOrDefault();

                                                                    if (PrevArrFBO == null || (PrevArrFBO != null && PrevArrFBO.FBOID != null && PrevArrFBO.FBOID.ToString().Trim() != hdDepartFBOID.Value.ToString().Trim()))
                                                                        RadWindowManager1.RadConfirm("Do you want to update the prior leg’s Arrival FBO?", "confirmPreviousFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                                }
                                                                else
                                                                    RadWindowManager1.RadConfirm("Do you want to update the prior leg’s Arrival FBO?", "confirmPreviousFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                            }
                                                        }
                                                    }
                                                }
                                                Session[Master.CQSessionKey] = FileRequest;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartFBO);
                                        }
                                    }
                                    else
                                    {
                                        lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartFBO);
                                    }
                                }
                                else
                                {
                                    lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartFBO);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Arrive FBO Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ArriveFBO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveFBO);
                        tbArriveFBOName.Text = string.Empty;
                        tbArriveFBOPhone.Text = string.Empty;
                        tbArriveFBOFax.Text = string.Empty;
                        tbArriveFBOEmail.Text = string.Empty;
                        lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdArriveFBOID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbArriveFBO.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                                    var ServCall = objDstsvc.GetFBOByAirportWithFilters(AirportID, 0, tbArriveFBO.Text.Trim(), false, false, false);
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList;

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            // RadWindowManager1.RadConfirm("Do you want to update the next leg’s Arriveure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                            if (!string.IsNullOrEmpty(objRetVal[0].FBOCD))
                                                tbArriveFBO.Text = objRetVal[0].FBOCD.ToString();
                                            hdArriveFBOID.Value = objRetVal[0].FBOID.ToString();

                                            if (objRetVal[0].FBOVendor != null && !string.IsNullOrEmpty(objRetVal[0].FBOVendor))
                                                tbArriveFBOName.Text = objRetVal[0].FBOVendor.ToString();
                                            if (objRetVal[0].PhoneNUM1 != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNUM1))
                                                tbArriveFBOPhone.Text = objRetVal[0].PhoneNUM1.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbArriveFBOFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbArriveFBOEmail.Text = objRetVal[0].ContactEmail.ToString();


                                            #region Next FBO Change
                                            FileRequest = (CQFile)Session[Master.CQSessionKey];

                                            if (FileRequest.Mode == CQRequestActionMode.Edit)
                                            {

                                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                                                    {
                                                        CQLeg NextLeg = new CQLeg();
                                                        NextLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == (Convert.ToInt16(hdnLegNum.Value) + 1) && x.IsDeleted == false).FirstOrDefault();

                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt16(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();

                                                        if (NextLeg != null && CurrLeg != null)
                                                        {
                                                            if (NextLeg.CQLegID != 0)
                                                                NextLeg.State = CQRequestEntityState.Modified;
                                                            else
                                                                NextLeg.State = CQRequestEntityState.Added;

                                                            if (CurrLeg.AAirportID == NextLeg.DAirportID)
                                                            {
                                                                CQFBOList nextDepFBO = null;
                                                                if (NextLeg.CQFBOLists != null && NextLeg.CQFBOLists.Count > 0)
                                                                {
                                                                    nextDepFBO = NextLeg.CQFBOLists.Where(x => x.IsDeleted == false && x.RecordType == "FD").FirstOrDefault();

                                                                    if (nextDepFBO == null || (nextDepFBO != null && nextDepFBO.FBOID != null && nextDepFBO.FBOID.ToString().Trim() != hdArriveFBOID.Value.ToString().Trim()))
                                                                        RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg’s Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                                }
                                                                else
                                                                    RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg’s Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                                            }
                                                        }
                                                    }
                                                }
                                                Session[Master.CQSessionKey] = FileRequest;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveFBO);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveFBO);
                                    }
                                }
                                else
                                {
                                    lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveFBO);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Maintainence Catering Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MaintCatering_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnMaintCateringCode);
                        tbMaintCateringName.Text = string.Empty;
                        tbMaintCateringPhone.Text = string.Empty;
                        tbMaintCateringFax.Text = string.Empty;
                        tbMaintCateringRate.Text = string.Empty;
                        tbMaintCateringEmail.Text = string.Empty;
                        lbcvMaintCateringCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdMaintCateringID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbMaintCateringCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdDepartIcaoID.Value);
                                    var ServCall = objDstsvc.GetAllCateringByAirportIDWithFilters(AirportID, 0, tbMaintCateringCode.Text.Trim(), false, false);
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList;

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].CateringCD != null && !string.IsNullOrEmpty(objRetVal[0].CateringCD))
                                                tbMaintCateringCode.Text = objRetVal[0].CateringCD.ToString();
                                            hdMaintCateringID.Value = objRetVal[0].CateringID.ToString();

                                            if (objRetVal[0].CateringVendor != null && !string.IsNullOrEmpty(objRetVal[0].CateringVendor))
                                                tbMaintCateringName.Text = objRetVal[0].CateringVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbMaintCateringPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbMaintCateringFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbMaintCateringEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbMaintCateringRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbMaintCateringRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvMaintCateringCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintCateringCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintCateringCode);
                                    }
                                }
                                else
                                {
                                    lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintCateringCode);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Pax Transport Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaxTransportCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPaxTransportCode);
                        tbPaxTransportName.Text = string.Empty;
                        tbPaxTransportPhone.Text = string.Empty;
                        tbPaxTransportFax.Text = string.Empty;
                        tbPaxTransportRate.Text = string.Empty;
                        tbPaxTransportEmail.Text = string.Empty;
                        lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdPaxTransportID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbPaxTransportCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                                    var ServCall = objDstsvc.GetTransportByAirportIDWithFilters(AirportID, 0, tbPaxTransportCode.Text.Trim(), false, false);
                                    //.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList; //.Where(x => x.TransportCD == tbPaxTransportCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].TransportCD != null && !string.IsNullOrEmpty(objRetVal[0].TransportCD))
                                                tbPaxTransportCode.Text = objRetVal[0].TransportCD.ToString();
                                            hdPaxTransportID.Value = objRetVal[0].TransportID.ToString();

                                            if (objRetVal[0].TransportationVendor != null && !string.IsNullOrEmpty(objRetVal[0].TransportationVendor))
                                                tbPaxTransportName.Text = objRetVal[0].TransportationVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbPaxTransportPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbPaxTransportFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbPaxTransportEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbPaxTransportRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbPaxTransportRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxTransportCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxTransportCode);
                                    }
                                }
                                else
                                {
                                    lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxTransportCode);
                                }
                            }
                        }
                        else
                        {
                            hdPaxTransportID.Value = "";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Crew Transport Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewTransportCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCrewTransportCode);
                        tbCrewTransportName.Text = string.Empty;
                        tbCrewTransportPhone.Text = string.Empty;
                        tbCrewTransportFax.Text = string.Empty;
                        tbCrewTransportRate.Text = string.Empty;
                        tbCrewTransportEmail.Text = string.Empty;
                        lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdCrewTransportID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbCrewTransportCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                                    var ServCall = objDstsvc.GetTransportByAirportIDWithFilters(AirportID, 0, tbCrewTransportCode.Text.Trim(), false, false);

                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList; //.Where(x => x.TransportCD == tbCrewTransportCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].TransportCD != null && !string.IsNullOrEmpty(objRetVal[0].TransportCD))
                                                tbCrewTransportCode.Text = objRetVal[0].TransportCD.ToString();
                                            hdCrewTransportID.Value = objRetVal[0].TransportID.ToString();

                                            if (objRetVal[0].TransportationVendor != null && !string.IsNullOrEmpty(objRetVal[0].TransportationVendor))
                                                tbCrewTransportName.Text = objRetVal[0].TransportationVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbCrewTransportPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbCrewTransportFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbCrewTransportEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbCrewTransportRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbCrewTransportRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewTransportCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewTransportCode);
                                    }
                                }
                                else
                                {
                                    lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewTransportCode);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Pax Hotel Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaxHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPaxHotelCode);
                        tbPaxHotelName.Text = string.Empty;
                        tbPaxHotelPhone.Text = string.Empty;
                        tbPaxHotelFax.Text = string.Empty;
                        tbPaxHotelRate.Text = string.Empty;
                        tbPaxHotelEmail.Text = string.Empty;
                        lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdPaxHotelID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbPaxHotelCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                                    var ServCall = objDstsvc.GetHotelsByAirportIDWithFilters(AirportID, 0, tbPaxHotelCode.Text.Trim(), false, false, false);
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList;

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].HotelCD != null && !string.IsNullOrEmpty(objRetVal[0].HotelCD))
                                                tbPaxHotelCode.Text = objRetVal[0].HotelCD.ToString();
                                            hdPaxHotelID.Value = objRetVal[0].HotelID.ToString();

                                            if (objRetVal[0].Name != null && !string.IsNullOrEmpty(objRetVal[0].Name))
                                                tbPaxHotelName.Text = objRetVal[0].Name.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbPaxHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbPaxHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbPaxHotelEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegociatedRate != null)
                                                tbPaxHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                            else
                                                tbPaxHotelRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxHotelCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxHotelCode);
                                    }
                                }
                                else
                                {
                                    lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxHotelCode);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Crew Hotel Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCrewHotelCode);
                        tbCrewHotelName.Text = string.Empty;
                        tbCrewHotelPhone.Text = string.Empty;
                        tbCrewHotelFax.Text = string.Empty;
                        tbCrewHotelRate.Text = string.Empty;
                        tbCrewHotelEmail.Text = string.Empty;
                        lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdCrewHotelID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbCrewHotelCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                                    var ServCall = objDstsvc.GetHotelsByAirportIDWithFilters(AirportID, 0, tbCrewHotelCode.Text.Trim(), false, false, false);
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList; //.Where(x => x.HotelCD == tbCrewHotelCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].HotelCD != null && !string.IsNullOrEmpty(objRetVal[0].HotelCD))
                                                tbCrewHotelCode.Text = objRetVal[0].HotelCD.ToString();
                                            hdCrewHotelID.Value = objRetVal[0].HotelID.ToString();

                                            if (objRetVal[0].Name != null && !string.IsNullOrEmpty(objRetVal[0].Name))
                                                tbCrewHotelName.Text = objRetVal[0].Name.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbCrewHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbCrewHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbCrewHotelEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegociatedRate != null)
                                                tbCrewHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                            else
                                                tbCrewHotelRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewHotelCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewHotelCode);
                                    }
                                }
                                else
                                {
                                    lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewHotelCode);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Maint Hotel Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MaintHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnMaintHotelCode);
                        tbMaintHotelName.Text = string.Empty;
                        tbMaintHotelPhone.Text = string.Empty;
                        tbMaintHotelFax.Text = string.Empty;
                        tbMaintHotelRate.Text = string.Empty;
                        tbMaintHotelEmail.Text = string.Empty;
                        lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdMaintHotelID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbMaintHotelCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    //Fix for #3023
                                    long AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                                    var ServCall = objDstsvc.GetHotelsByAirportIDWithFilters(AirportID, 0, tbMaintHotelCode.Text.Trim(), false, false, false);
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList; 

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].HotelCD != null && !string.IsNullOrEmpty(objRetVal[0].HotelCD))
                                                tbMaintHotelCode.Text = objRetVal[0].HotelCD.ToString();
                                            hdMaintHotelID.Value = objRetVal[0].HotelID.ToString();

                                            if (objRetVal[0].Name != null && !string.IsNullOrEmpty(objRetVal[0].Name))
                                                tbMaintHotelName.Text = objRetVal[0].Name.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbMaintHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbMaintHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbMaintHotelEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegociatedRate != null)
                                                tbMaintHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                            else
                                                tbMaintHotelRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintHotelCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintHotelCode);
                                    }
                                }
                                else
                                {
                                    lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintHotelCode);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }


        protected void tbPaxHotelRate_TextChanged(Object sender, EventArgs e)
        {
            Master.IsValidNumericFormat(tbPaxHotelRate.Text, 17, 14, 2, "Passenger Hotel- Negotiated Price");
            if (Master.IsValidNumericFormat(tbPaxHotelRate.Text, 17, 14, 2, "Passenger Hotel- Negotiated Price") == false)
            {
                tbPaxHotelRate.Text = string.Empty;
            }
        }

        protected void tbPaxTransportRate_TextChanged(Object sender, EventArgs e)
        {
            Master.IsValidNumericFormat(tbPaxTransportRate.Text, 6, 3, 2, "Passenger Transport- Negotiated Price");
            if (Master.IsValidNumericFormat(tbPaxTransportRate.Text, 6, 3, 2, "Passenger Transport- Negotiated Price") == false)
            {
                tbPaxTransportRate.Text = string.Empty;
            }
        }

        protected void tbCrewHotelRate_TextChanged(Object sender, EventArgs e)
        {
            Master.IsValidNumericFormat(tbCrewHotelRate.Text, 17, 14, 2, "Crew Hotel- Negotiated Price");
            if (Master.IsValidNumericFormat(tbCrewHotelRate.Text, 17, 14, 2, "Crew Hotel- Negotiated Price") == false)
            {
                tbCrewHotelRate.Text = string.Empty;
            }
        }

        protected void tbCrewTransportRate_TextChanged(Object sender, EventArgs e)
        {
            Master.IsValidNumericFormat(tbCrewTransportRate.Text, 6, 3, 2, "Crew Transport- Negotiated Price");
            if (Master.IsValidNumericFormat(tbCrewTransportRate.Text, 6, 3, 2, "Crew Transport- Negotiated Price") == false)
            {
                tbCrewTransportRate.Text = string.Empty;
            }
        }

        protected void tbMaintHotelRate_TextChanged(Object sender, EventArgs e)
        {
            Master.IsValidNumericFormat(tbMaintHotelRate.Text, 17, 14, 2, "Maintenance Hotel- Negotiated Price");
            if (Master.IsValidNumericFormat(tbMaintHotelRate.Text, 17, 14, 2, "Maintenance Hotel- Negotiated Price") == false)
            {
                tbMaintHotelRate.Text = string.Empty;
            }
        }

        #endregion

        protected void Legs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.BindLegs(dgLegs, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void Legs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["DepartDate"] != null && !string.IsNullOrEmpty(item["DepartDate"].Text) && item["DepartDate"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                                if (date != null)
                                    item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));

                            }

                            if (item["ArrivalDate"] != null && !string.IsNullOrEmpty(item["ArrivalDate"].Text) && item["ArrivalDate"].Text != "&nbsp;")
                            {
                                DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                                if (date1 != null)
                                    item["ArrivalDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date1));

                            }
                        }

                        if (dgLegs.MasterTableView.Items.Count > 0)
                        {
                            dgLegs.MasterTableView.Items[0].Selected = true;

                            GridDataItem dataItem = (GridDataItem)dgLegs.SelectedItems[0];
                            if (dataItem.GetDataKeyValue("DepartAirportID") != null)
                                hdDepartIcaoID.Value = dataItem.GetDataKeyValue("DepartAirportID").ToString();
                            if (dataItem.GetDataKeyValue("ArrivalAirportID") != null)
                                hdArriveIcaoID.Value = dataItem.GetDataKeyValue("ArrivalAirportID").ToString();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void Legs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                break;

                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                break;

                            case RadGrid.DeleteSelectedCommandName:
                                break;

                            case "RowClick":
                                GridDataItem Item = (GridDataItem)dgLegs.SelectedItems[0];
                                hdDepartIcaoID.Value = Item.GetDataKeyValue("DepartAirportID").ToString();
                                hdArriveIcaoID.Value = Item.GetDataKeyValue("ArrivalAirportID").ToString();
                                if (!selectchangefired)
                                {
                                    SaveCharterQuoteToSession();
                                }

                                break;

                            default:
                                break;

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void Legs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegs.SelectedItems.Count > 0)
                        {
                            SaveCharterQuoteToSession();
                            LoadLegDetails();
                            selectchangefired = true;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbDepartFBO.Text = string.Empty;
                tbDepartFBOEmail.Text = string.Empty;
                tbDepartFBOFax.Text = string.Empty;
                tbDepartFBOName.Text = string.Empty;
                tbDepartFBOPhone.Text = string.Empty;

                tbArriveFBO.Text = string.Empty;
                tbArriveFBOEmail.Text = string.Empty;
                tbArriveFBOFax.Text = string.Empty;
                tbArriveFBOName.Text = string.Empty;
                tbArriveFBOPhone.Text = string.Empty;

                tbCrewHotelCode.Text = string.Empty;
                tbCrewHotelName.Text = string.Empty;
                tbCrewHotelPhone.Text = string.Empty;
                tbCrewHotelRate.Text = string.Empty;
                tbCrewHotelFax.Text = string.Empty;
                tbCrewHotelEmail.Text = string.Empty;

                tbCrewTransportCode.Text = string.Empty;
                tbCrewTransportName.Text = string.Empty;
                tbCrewTransportPhone.Text = string.Empty;
                tbCrewTransportRate.Text = string.Empty;
                tbCrewTransportFax.Text = string.Empty;
                tbCrewTransportEmail.Text = string.Empty;

                tbPaxHotelCode.Text = string.Empty;
                tbPaxHotelName.Text = string.Empty;
                tbPaxHotelPhone.Text = string.Empty;
                tbPaxHotelRate.Text = string.Empty;
                tbPaxHotelFax.Text = string.Empty;
                tbPaxHotelEmail.Text = string.Empty;

                tbPaxTransportCode.Text = string.Empty;
                tbPaxTransportName.Text = string.Empty;
                tbPaxTransportPhone.Text = string.Empty;
                tbPaxTransportRate.Text = string.Empty;
                tbPaxTransportFax.Text = string.Empty;
                tbPaxTransportEmail.Text = string.Empty;

                tbPaxNotes.Text = string.Empty;

                tbMaintHotelCode.Text = string.Empty;
                tbMaintHotelName.Text = string.Empty;
                tbMaintHotelPhone.Text = string.Empty;
                tbMaintHotelRate.Text = string.Empty;
                tbMaintHotelFax.Text = string.Empty;
                tbMaintHotelEmail.Text = string.Empty;

                tbMaintCateringCode.Text = string.Empty;
                tbMaintCateringFax.Text = string.Empty;
                tbMaintCateringName.Text = string.Empty;
                tbMaintCateringPhone.Text = string.Empty;
                tbMaintCateringRate.Text = string.Empty;
                tbMaintCateringFax.Text = string.Empty;
                tbMaintCateringEmail.Text = string.Empty;

                // Clear Label Validation Messages
                lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvMaintCateringCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            }
        }

        protected void SaveCharterQuoteToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null)
                        {
                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                            {
                                CQLeg cqLeg = new CQLeg();
                                cqLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt16(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();

                                if (cqLeg != null)
                                {
                                    if (cqLeg.CQLegID != 0)
                                        cqLeg.State = CQRequestEntityState.Modified;
                                    else
                                        cqLeg.State = CQRequestEntityState.Added;

                                    hdnLegNum.Value = cqLeg.LegNUM.ToString();

                                    #region "Depart FBO and Arrive FBO"

                                    CQFBOList cqDepartFBOList = null;
                                    CQFBOList cqArriveFBOList = null;

                                    if (cqLeg.CQFBOLists != null && cqLeg.CQFBOLists.Count > 0)
                                        cqDepartFBOList = cqLeg.CQFBOLists.Where(x => x.IsDeleted == false && x.RecordType == "FD").FirstOrDefault();

                                    if (cqLeg.CQFBOLists != null && cqLeg.CQFBOLists.Count > 0)
                                        cqArriveFBOList = cqLeg.CQFBOLists.Where(x => x.IsDeleted == false && x.RecordType == "FA").FirstOrDefault();

                                    if (cqDepartFBOList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbDepartFBO.Text))
                                            {
                                                CQFBOList departFBOList = new CQFBOList();
                                                departFBOList.State = CQRequestEntityState.Added;
                                                departFBOList.RecordType = "FD";
                                                UpdateScreenDetailstoCQFBO(ref departFBOList, departFBOList.RecordType.ToString());
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQFBOLists == null)
                                                    cqLeg.CQFBOLists = new List<CQFBOList>();
                                                cqLeg.CQFBOLists.Add(departFBOList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbDepartFBO.Text))
                                        {
                                            if (cqDepartFBOList.CQFBOListID != 0)
                                            {
                                                cqDepartFBOList.IsDeleted = true;
                                                cqDepartFBOList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQFBOLists.Remove(cqDepartFBOList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqDepartFBOList.State != CQRequestEntityState.Deleted && cqDepartFBOList.State != CQRequestEntityState.Added)
                                                cqDepartFBOList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQFBO(ref cqDepartFBOList, cqDepartFBOList.RecordType.ToString());
                                            //cqLeg.CQFBOLists[0] = cqDepartFBOList;
                                        }
                                    }

                                    if (cqArriveFBOList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbArriveFBO.Text))
                                            {
                                                CQFBOList arriveFBOList = new CQFBOList();
                                                arriveFBOList.State = CQRequestEntityState.Added;
                                                arriveFBOList.RecordType = "FA";
                                                UpdateScreenDetailstoCQFBO(ref arriveFBOList, arriveFBOList.RecordType.ToString());
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQFBOLists == null)
                                                    cqLeg.CQFBOLists = new List<CQFBOList>();
                                                cqLeg.CQFBOLists.Add(arriveFBOList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbArriveFBO.Text))
                                        {
                                            if (cqArriveFBOList.CQFBOListID != 0)
                                            {
                                                cqArriveFBOList.IsDeleted = true;
                                                cqArriveFBOList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQFBOLists.Remove(cqArriveFBOList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqArriveFBOList.State != CQRequestEntityState.Deleted && cqArriveFBOList.State != CQRequestEntityState.Added)
                                                cqArriveFBOList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQFBO(ref cqArriveFBOList, cqArriveFBOList.RecordType.ToString());
                                            //cqLeg.CQFBOLists[1] = cqArriveFBOList;
                                        }
                                    }

                                    #endregion

                                    #region "Maintenance Catering"

                                    CQCateringList cqCateringList = null;

                                    if (cqLeg.CQCateringLists != null && cqLeg.CQCateringLists.Count > 0)
                                        cqCateringList = cqLeg.CQCateringLists.Where(x => x.IsDeleted == false && x.RecordType == "CM").FirstOrDefault();

                                    if (cqCateringList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbMaintCateringCode.Text))
                                            {
                                                CQCateringList maintCatering = new CQCateringList();
                                                maintCatering.State = CQRequestEntityState.Added;
                                                UpdateScreenDetailstoCQCatering(ref maintCatering);
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQCateringLists == null)
                                                    cqLeg.CQCateringLists = new List<CQCateringList>();
                                                cqLeg.CQCateringLists.Add(maintCatering);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbMaintCateringCode.Text))
                                        {
                                            if (cqCateringList.CQCateringListID != 0)
                                            {
                                                cqCateringList.IsDeleted = true;
                                                cqCateringList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQCateringLists.Remove(cqCateringList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqCateringList.State != CQRequestEntityState.Added && cqCateringList.State != CQRequestEntityState.Deleted)
                                                cqCateringList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQCatering(ref cqCateringList);
                                            // cqLeg.CQCateringLists[0] = cqCateringList;
                                        }
                                    }

                                    #endregion

                                    #region "Pax Transport and Crew Transport"

                                    CQTransportList cqPaxTransportList = null;
                                    CQTransportList cqCrewTransportList = null;

                                    if (cqLeg.CQTransportLists != null && cqLeg.CQTransportLists.Count > 0)
                                        cqPaxTransportList = cqLeg.CQTransportLists.Where(x => x.IsDeleted == false && x.RecordType == "TP").FirstOrDefault();

                                    if (cqLeg.CQTransportLists != null && cqLeg.CQTransportLists.Count > 0)
                                        cqCrewTransportList = cqLeg.CQTransportLists.Where(x => x.IsDeleted == false && x.RecordType == "TC").FirstOrDefault();

                                    if (cqPaxTransportList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbPaxTransportCode.Text))
                                            {
                                                CQTransportList paxTransportList = new CQTransportList();
                                                paxTransportList.State = CQRequestEntityState.Added;
                                                paxTransportList.RecordType = "TP";
                                                UpdateScreenDetailstoCQTransport(ref paxTransportList, paxTransportList.RecordType);
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQTransportLists == null)
                                                    cqLeg.CQTransportLists = new List<CQTransportList>();
                                                cqLeg.CQTransportLists.Add(paxTransportList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbPaxTransportCode.Text))
                                        {
                                            if (cqPaxTransportList.CQTransportListID != 0)
                                            {
                                                cqPaxTransportList.IsDeleted = true;
                                                cqPaxTransportList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQTransportLists.Remove(cqPaxTransportList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqPaxTransportList.State != CQRequestEntityState.Added && cqPaxTransportList.State != CQRequestEntityState.Deleted)
                                                cqPaxTransportList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQTransport(ref cqPaxTransportList, cqPaxTransportList.RecordType);
                                            //cqLeg.CQTransportLists[0] = cqPaxTransportList;
                                        }
                                    }

                                    if (cqCrewTransportList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbCrewTransportCode.Text))
                                            {
                                                CQTransportList crewTransportList = new CQTransportList();
                                                crewTransportList.State = CQRequestEntityState.Added;
                                                crewTransportList.RecordType = "TC";
                                                UpdateScreenDetailstoCQTransport(ref crewTransportList, crewTransportList.RecordType);
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQTransportLists == null)
                                                    cqLeg.CQTransportLists = new List<CQTransportList>();
                                                cqLeg.CQTransportLists.Add(crewTransportList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbCrewTransportCode.Text))
                                        {
                                            if (cqCrewTransportList.CQTransportListID != 0)
                                            {
                                                cqCrewTransportList.IsDeleted = true;
                                                cqCrewTransportList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQTransportLists.Remove(cqCrewTransportList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqCrewTransportList.State != CQRequestEntityState.Added && cqCrewTransportList.State != CQRequestEntityState.Deleted)
                                                cqCrewTransportList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQTransport(ref cqCrewTransportList, cqCrewTransportList.RecordType);
                                            // cqLeg.CQTransportLists[1] = cqCrewTransportList;
                                        }
                                    }

                                    #endregion

                                    #region "Pax Hotel , Crew Hotel and Maintenance Hotel"

                                    CQHotelList cqPaxHotelList = null;
                                    CQHotelList cqCrewHotelList = null;
                                    CQHotelList cqMaintHotelList = null;

                                    if (cqLeg.CQHotelLists != null && cqLeg.CQHotelLists.Count > 0)
                                        cqPaxHotelList = cqLeg.CQHotelLists.Where(x => x.IsDeleted == false && x.RecordType == "HP").FirstOrDefault();

                                    if (cqLeg.CQHotelLists != null && cqLeg.CQHotelLists.Count > 0)
                                        cqCrewHotelList = cqLeg.CQHotelLists.Where(x => x.IsDeleted == false && x.RecordType == "HC").FirstOrDefault();

                                    if (cqLeg.CQHotelLists != null && cqLeg.CQHotelLists.Count > 0)
                                        cqMaintHotelList = cqLeg.CQHotelLists.Where(x => x.IsDeleted == false && x.RecordType == "HM").FirstOrDefault();

                                    if (cqPaxHotelList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbPaxHotelCode.Text))
                                            {
                                                CQHotelList paxHotelList = new CQHotelList();
                                                paxHotelList.State = CQRequestEntityState.Added;
                                                paxHotelList.RecordType = "HP";
                                                UpdateScreenDetailstoCQHotel(ref paxHotelList, paxHotelList.RecordType);
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQHotelLists == null)
                                                    cqLeg.CQHotelLists = new List<CQHotelList>();
                                                cqLeg.CQHotelLists.Add(paxHotelList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbPaxHotelCode.Text))
                                        {
                                            if (cqPaxHotelList.CQHotelListID != 0)
                                            {
                                                cqPaxHotelList.IsDeleted = true;
                                                cqPaxHotelList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQHotelLists.Remove(cqPaxHotelList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqPaxHotelList.State != CQRequestEntityState.Added && cqPaxHotelList.State != CQRequestEntityState.Deleted)
                                                cqPaxHotelList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQHotel(ref cqPaxHotelList, cqPaxHotelList.RecordType);
                                            //cqLeg.CQHotelLists[0] = cqPaxHotelList;
                                        }
                                    }

                                    if (cqCrewHotelList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbCrewHotelCode.Text))
                                            {
                                                CQHotelList CrewHotelList = new CQHotelList();
                                                CrewHotelList.State = CQRequestEntityState.Added;
                                                CrewHotelList.RecordType = "HC";
                                                UpdateScreenDetailstoCQHotel(ref CrewHotelList, CrewHotelList.RecordType);
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQHotelLists == null)
                                                    cqLeg.CQHotelLists = new List<CQHotelList>();
                                                cqLeg.CQHotelLists.Add(CrewHotelList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbCrewHotelCode.Text))
                                        {
                                            if (cqCrewHotelList.CQHotelListID != 0)
                                            {
                                                cqCrewHotelList.IsDeleted = true;
                                                cqCrewHotelList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQHotelLists.Remove(cqCrewHotelList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqCrewHotelList.State != CQRequestEntityState.Added && cqCrewHotelList.State != CQRequestEntityState.Deleted)
                                                cqCrewHotelList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQHotel(ref cqCrewHotelList, cqCrewHotelList.RecordType);
                                            //cqLeg.CQHotelLists[1] = cqCrewHotelList;
                                        }
                                    }

                                    if (cqMaintHotelList == null)
                                    {
                                        if (IsAuthorized(Permission.CharterQuote.AddCQLogistics))
                                        {
                                            if (!string.IsNullOrEmpty(tbMaintHotelCode.Text))
                                            {
                                                CQHotelList MaintHotelList = new CQHotelList();
                                                MaintHotelList.State = CQRequestEntityState.Added;
                                                MaintHotelList.RecordType = "HM";
                                                UpdateScreenDetailstoCQHotel(ref MaintHotelList, MaintHotelList.RecordType);
                                                if (cqLeg.CQLegID != 0 && cqLeg.State != CQRequestEntityState.Deleted)
                                                    cqLeg.State = CQRequestEntityState.Modified;
                                                if (cqLeg.CQHotelLists == null)
                                                    cqLeg.CQHotelLists = new List<CQHotelList>();
                                                cqLeg.CQHotelLists.Add(MaintHotelList);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(tbMaintHotelCode.Text))
                                        {
                                            if (cqMaintHotelList.CQHotelListID != 0)
                                            {
                                                cqMaintHotelList.IsDeleted = true;
                                                cqMaintHotelList.State = CQRequestEntityState.Deleted;
                                            }
                                            else
                                                cqLeg.CQHotelLists.Remove(cqMaintHotelList);
                                        }

                                        if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                                        {
                                            if (cqMaintHotelList.State != CQRequestEntityState.Added && cqMaintHotelList.State != CQRequestEntityState.Deleted)
                                                cqMaintHotelList.State = CQRequestEntityState.Modified;
                                            UpdateScreenDetailstoCQHotel(ref cqMaintHotelList, cqMaintHotelList.RecordType);
                                            //cqLeg.CQHotelLists[2] = cqMaintHotelList;
                                        }
                                    }

                                    #endregion

                                    cqLeg.Notes = string.Empty;
                                    if (!string.IsNullOrEmpty(tbPaxNotes.Text))
                                        cqLeg.Notes = tbPaxNotes.Text;
                                }
                            }
                        }

                        Session[Master.CQSessionKey] = FileRequest;
                    }
                }
            }
        }

        private void UpdateScreenDetailstoCQFBO(ref CQFBOList cqfbo, string arriveDepart)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqfbo, arriveDepart))
            {
                if (arriveDepart.Equals("FD"))
                {
                    if (!string.IsNullOrEmpty(hdDepartFBOID.Value))
                        cqfbo.FBOID = Convert.ToInt64(hdDepartFBOID.Value);
                    else
                        cqfbo.FBOID = null;

                    if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                        cqfbo.AirportID = Convert.ToInt64(hdDepartIcaoID.Value);
                    else
                        cqfbo.AirportID = null;

                    cqfbo.RecordType = "FD";
                    cqfbo.LastUpdTS = DateTime.UtcNow;
                    cqfbo.IsDeleted = false;
                }
                else if (arriveDepart.Equals("FA"))
                {
                    if (!string.IsNullOrEmpty(hdArriveFBOID.Value))
                        cqfbo.FBOID = Convert.ToInt64(hdArriveFBOID.Value);
                    else
                        cqfbo.FBOID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        cqfbo.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        cqfbo.AirportID = null;

                    cqfbo.RecordType = "FA";
                    cqfbo.LastUpdTS = DateTime.UtcNow;
                    cqfbo.IsDeleted = false;
                }
            }
        }

        private void UpdateScreenDetailsFromCQFBO(CQFBOList cqfbo)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqfbo))
            {
                if (cqfbo.RecordType == "FD")
                {
                    if (hdDepartIcaoID.Value == cqfbo.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqfbo.FBOID.ToString()))
                        {
                            hdDepartFBOID.Value = cqfbo.FBOID.ToString();
                        }
                        else
                        {
                            hdDepartFBOID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                            {
                                var FBOValue = FBOService.GetAllFBOByAirportID(Convert.ToInt64(hdDepartIcaoID.Value));
                                if (FBOValue.ReturnFlag == true)
                                {
                                    if (FBOValue.EntityList.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(hdDepartFBOID.Value))
                                        {
                                            var FBOValue1 = FBOValue.EntityList.Where(x => x.FBOID == Convert.ToInt64(hdDepartFBOID.Value)).ToList();
                                            if (FBOValue1.Count > 0)
                                            {
                                                if (FBOValue1[0].FBOCD != null && !string.IsNullOrEmpty(FBOValue1[0].FBOCD.ToString()))
                                                    tbDepartFBO.Text = FBOValue1[0].FBOCD;
                                                if (FBOValue1[0].FBOVendor != null && !string.IsNullOrEmpty(FBOValue1[0].FBOVendor.ToString()))
                                                    tbDepartFBOName.Text = FBOValue1[0].FBOVendor.ToString();
                                                if (FBOValue1[0].PhoneNUM1 != null && !string.IsNullOrEmpty(FBOValue1[0].PhoneNUM1.ToString()))
                                                    tbDepartFBOPhone.Text = FBOValue1[0].PhoneNUM1.ToString();
                                                if (FBOValue1[0].FaxNum != null && !string.IsNullOrEmpty(FBOValue1[0].FaxNum.ToString()))
                                                    tbDepartFBOFax.Text = FBOValue1[0].FaxNum.ToString();
                                                if (FBOValue1[0].ContactEmail != null && !string.IsNullOrEmpty(FBOValue1[0].ContactEmail.ToString()))
                                                    tbDepartFBOEmail.Text = FBOValue1[0].ContactEmail.ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (cqfbo.RecordType == "FA")
                {
                    if (hdArriveIcaoID.Value == cqfbo.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqfbo.FBOID.ToString()))
                        {
                            hdArriveFBOID.Value = cqfbo.FBOID.ToString();
                        }
                        else
                        {
                            hdArriveFBOID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            {
                                var FBOValue = FBOService.GetAllFBOByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                if (FBOValue.ReturnFlag == true)
                                {
                                    if (FBOValue.EntityList.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(hdArriveFBOID.Value))
                                        {
                                            var FBOValue1 = FBOValue.EntityList.Where(x => x.FBOID == Convert.ToInt64(hdArriveFBOID.Value)).ToList();
                                            if (FBOValue1.Count > 0)
                                            {
                                                if (FBOValue1[0].FBOCD != null && !string.IsNullOrEmpty(FBOValue1[0].FBOCD.ToString()))
                                                    tbArriveFBO.Text = FBOValue1[0].FBOCD;
                                                if (FBOValue1[0].FBOVendor != null && !string.IsNullOrEmpty(FBOValue1[0].FBOVendor.ToString()))
                                                    tbArriveFBOName.Text = FBOValue1[0].FBOVendor.ToString();
                                                if (FBOValue1[0].PhoneNUM1 != null && !string.IsNullOrEmpty(FBOValue1[0].PhoneNUM1.ToString()))
                                                    tbArriveFBOPhone.Text = FBOValue1[0].PhoneNUM1.ToString();
                                                if (FBOValue1[0].FaxNum != null && !string.IsNullOrEmpty(FBOValue1[0].FaxNum.ToString()))
                                                    tbArriveFBOFax.Text = FBOValue1[0].FaxNum.ToString();
                                                if (FBOValue1[0].ContactEmail != null && !string.IsNullOrEmpty(FBOValue1[0].ContactEmail.ToString()))
                                                    tbArriveFBOEmail.Text = FBOValue1[0].ContactEmail.ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void UpdateScreenDetailstoCQCatering(ref CQCateringList cqcatering)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqcatering))
            {
                if (!string.IsNullOrEmpty(hdMaintCateringID.Value))
                {
                    if (!string.IsNullOrEmpty(hdMaintCateringID.Value))
                        cqcatering.CateringID = Convert.ToInt64(hdMaintCateringID.Value);
                    else
                        cqcatering.CateringID = null;

                    if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                        cqcatering.AirportID = Convert.ToInt64(hdDepartIcaoID.Value);
                    else
                        cqcatering.AirportID = null;

                    cqcatering.RecordType = "CM";
                    cqcatering.LastUpdTS = DateTime.UtcNow;
                    cqcatering.IsDeleted = false;
                    cqcatering.CQCateringListDescription = tbMaintCateringName.Text;
                    cqcatering.PhoneNUM = tbMaintCateringPhone.Text;
                    cqcatering.FaxNUM = tbMaintCateringFax.Text;
                    cqcatering.Email = tbMaintCateringEmail.Text;
                    if (!string.IsNullOrEmpty(tbMaintCateringRate.Text))
                        cqcatering.Rate = Convert.ToDecimal(tbMaintCateringRate.Text);
                }
                else
                {
                    cqcatering.CateringID = null;
                    cqcatering.CQCateringListDescription = string.Empty;
                    cqcatering.PhoneNUM = string.Empty;
                    cqcatering.FaxNUM = string.Empty;
                    cqcatering.Email = string.Empty;
                    cqcatering.Rate = null;
                }
            }
        }

        private void UpdateScreenDetailsFromCQCatering(CQCateringList cqcatering)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqcatering))
            {
                if (hdDepartIcaoID.Value == cqcatering.AirportID.ToString())
                {
                    if (!string.IsNullOrEmpty(cqcatering.CateringID.ToString()))
                    {
                        hdMaintCateringID.Value = cqcatering.CateringID.ToString();
                    }
                    else
                    {
                        hdMaintCateringID.Value = string.Empty;
                    }
                    using (FlightPakMasterService.MasterCatalogServiceClient CateringService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CateringValue = CateringService.GetAllCateringByAirportID(Convert.ToInt64(hdDepartIcaoID.Value));
                        if (CateringValue.ReturnFlag == true)
                        {
                            if (CateringValue.EntityList.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(hdMaintCateringID.Value))
                                {
                                    var CateringValue1 = CateringValue.EntityList.Where(x => x.CateringID == Convert.ToInt64(hdMaintCateringID.Value)).ToList();
                                    if (CateringValue1.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(CateringValue1[0].CateringCD.ToString()))
                                            tbMaintCateringCode.Text = CateringValue1[0].CateringCD;
                                        //if (CateringValue1[0].CateringVendor != null && !string.IsNullOrEmpty(CateringValue1[0].CateringVendor.ToString()))
                                        //    tbMaintCateringName.Text = CateringValue1[0].CateringVendor.ToString();
                                        //if (CateringValue1[0].PhoneNum != null && !string.IsNullOrEmpty(CateringValue1[0].PhoneNum.ToString()))
                                        //    tbMaintCateringPhone.Text = CateringValue1[0].PhoneNum.ToString();
                                        //if (CateringValue1[0].FaxNum != null && !string.IsNullOrEmpty(CateringValue1[0].FaxNum.ToString()))
                                        //    tbMaintCateringFax.Text = CateringValue1[0].FaxNum.ToString();
                                        //if (CateringValue1[0].NegotiatedRate != null && !string.IsNullOrEmpty(CateringValue1[0].NegotiatedRate.ToString()))
                                        //    tbMaintCateringRate.Text = CateringValue1[0].NegotiatedRate.ToString();
                                        //if (CateringValue1[0].ContactEmail != null && !string.IsNullOrEmpty(CateringValue1[0].ContactEmail.ToString()))
                                        //    tbMaintCateringEmail.Text = CateringValue1[0].ContactEmail.ToString();
                                    }
                                }
                            }
                        }
                    }
                    if (cqcatering.CQCateringListDescription != null && !string.IsNullOrEmpty(cqcatering.CQCateringListDescription.ToString()))
                        tbMaintCateringName.Text = cqcatering.CQCateringListDescription.ToString();
                    if (cqcatering.PhoneNUM != null && !string.IsNullOrEmpty(cqcatering.PhoneNUM.ToString()))
                        tbMaintCateringPhone.Text = cqcatering.PhoneNUM.ToString();
                    if (cqcatering.FaxNUM != null && !string.IsNullOrEmpty(cqcatering.FaxNUM.ToString()))
                        tbMaintCateringFax.Text = cqcatering.FaxNUM.ToString();
                    if (cqcatering.Rate != null && !string.IsNullOrEmpty(cqcatering.Rate.ToString()))
                        tbMaintCateringRate.Text = cqcatering.Rate.ToString();
                    if (cqcatering.Email != null && !string.IsNullOrEmpty(cqcatering.Email.ToString()))
                        tbMaintCateringEmail.Text = cqcatering.Email.ToString();
                }
            }
        }

        private void UpdateScreenDetailstoCQTransport(ref CQTransportList cqtransport, string paxcrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqtransport, paxcrew))
            {
                if (paxcrew.Equals("TP"))
                {
                    if (!string.IsNullOrEmpty(hdPaxTransportID.Value))
                    {
                        if (!string.IsNullOrEmpty(hdPaxTransportID.Value))
                            cqtransport.TransportID = Convert.ToInt64(hdPaxTransportID.Value);
                        else
                            cqtransport.TransportID = null;

                        if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            cqtransport.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                        else
                            cqtransport.AirportID = null;

                        cqtransport.RecordType = "TP";
                        cqtransport.LastUpdTS = DateTime.UtcNow;
                        cqtransport.IsDeleted = false;
                        cqtransport.CQTransportListDescription = tbPaxTransportName.Text;
                        cqtransport.PhoneNUM = tbPaxTransportPhone.Text;
                        cqtransport.FaxNUM = tbPaxTransportFax.Text;
                        cqtransport.Email = tbPaxTransportEmail.Text;
                        if (!string.IsNullOrEmpty(tbPaxTransportRate.Text))
                            cqtransport.Rate = Convert.ToDecimal(tbPaxTransportRate.Text);
                    }
                    else
                    {
                        cqtransport.TransportID = null;
                        cqtransport.CQTransportListDescription = string.Empty;
                        cqtransport.PhoneNUM = string.Empty;
                        cqtransport.FaxNUM = string.Empty;
                        cqtransport.Email = string.Empty;
                        cqtransport.Rate = null;
                    }
                }
                else if (paxcrew.Equals("TC"))
                {
                    if (!string.IsNullOrEmpty(hdCrewTransportID.Value))
                    {
                        if (!string.IsNullOrEmpty(hdCrewTransportID.Value))
                            cqtransport.TransportID = Convert.ToInt64(hdCrewTransportID.Value);
                        else
                            cqtransport.TransportID = null;

                        if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            cqtransport.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                        else
                            cqtransport.AirportID = null;

                        cqtransport.RecordType = "TC";
                        cqtransport.LastUpdTS = DateTime.UtcNow;
                        cqtransport.IsDeleted = false;
                        cqtransport.CQTransportListDescription = tbCrewTransportName.Text;
                        cqtransport.PhoneNUM = tbCrewTransportPhone.Text;
                        cqtransport.FaxNUM = tbCrewTransportFax.Text;
                        cqtransport.Email = tbCrewTransportEmail.Text;
                        if (!string.IsNullOrEmpty(tbCrewTransportRate.Text))
                            cqtransport.Rate = Convert.ToDecimal(tbCrewTransportRate.Text);
                    }
                    else
                    {
                        cqtransport.TransportID = null;
                        cqtransport.CQTransportListDescription = string.Empty;
                        cqtransport.PhoneNUM = string.Empty;
                        cqtransport.FaxNUM = string.Empty;
                        cqtransport.Email = string.Empty;
                        cqtransport.Rate = null;
                    }
                }
            }
        }

        private void UpdateScreenDetailsFromCQTransport(CQTransportList cqtransport)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqtransport))
            {
                if (cqtransport.RecordType == "TP")
                {
                    if (hdArriveIcaoID.Value == cqtransport.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqtransport.TransportID.ToString()))
                        {
                            hdPaxTransportID.Value = cqtransport.TransportID.ToString();
                        }
                        else
                        {
                            hdPaxTransportID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TransportValue = TransportService.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (TransportValue.ReturnFlag == true)
                            {
                                if (TransportValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdPaxTransportID.Value))
                                    {
                                        var TransportValue1 = TransportValue.EntityList.Where(x => x.TransportID == Convert.ToInt64(hdPaxTransportID.Value)).ToList();
                                        if (TransportValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(TransportValue1[0].TransportCD.ToString()))
                                                tbPaxTransportCode.Text = TransportValue1[0].TransportCD;
                                            //if (!string.IsNullOrEmpty(TransportValue1[0].TransportationVendor.ToString()))
                                            //    tbPaxTransportName.Text = TransportValue1[0].TransportationVendor.ToString();
                                            //if (TransportValue1[0].PhoneNum != null && !string.IsNullOrEmpty(TransportValue1[0].PhoneNum.ToString()))
                                            //    tbPaxTransportPhone.Text = TransportValue1[0].PhoneNum.ToString();
                                            //if (TransportValue1[0].FaxNum != null && !string.IsNullOrEmpty(TransportValue1[0].FaxNum.ToString()))
                                            //    tbPaxTransportFax.Text = TransportValue1[0].FaxNum.ToString();
                                            //if (TransportValue1[0].ContactEmail != null && !string.IsNullOrEmpty(TransportValue1[0].ContactEmail.ToString()))
                                            //    tbPaxTransportEmail.Text = TransportValue1[0].ContactEmail.ToString();
                                            //if (TransportValue1[0].NegotiatedRate != null && !string.IsNullOrEmpty(TransportValue1[0].NegotiatedRate.ToString()))
                                            //    tbPaxTransportRate.Text = TransportValue1[0].NegotiatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                        if (cqtransport.CQTransportListDescription != null && !string.IsNullOrEmpty(cqtransport.CQTransportListDescription))
                            tbPaxTransportName.Text = cqtransport.CQTransportListDescription.ToString();
                        if (cqtransport.PhoneNUM != null && !string.IsNullOrEmpty(cqtransport.PhoneNUM.ToString()))
                            tbPaxTransportPhone.Text = cqtransport.PhoneNUM.ToString();
                        if (cqtransport.FaxNUM != null && !string.IsNullOrEmpty(cqtransport.FaxNUM.ToString()))
                            tbPaxTransportFax.Text = cqtransport.FaxNUM.ToString();
                        if (cqtransport.Email != null && !string.IsNullOrEmpty(cqtransport.Email.ToString()))
                            tbPaxTransportEmail.Text = cqtransport.Email.ToString();
                        if (cqtransport.Rate != null && !string.IsNullOrEmpty(cqtransport.Rate.ToString()))
                            tbPaxTransportRate.Text = cqtransport.Rate.ToString();
                    }
                }
                else if (cqtransport.RecordType == "TC")
                {
                    if (hdArriveIcaoID.Value == cqtransport.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqtransport.TransportID.ToString()))
                        {
                            hdCrewTransportID.Value = cqtransport.TransportID.ToString();
                        }
                        else
                        {
                            hdCrewTransportID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TransportValue = TransportService.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (TransportValue.ReturnFlag == true)
                            {
                                if (TransportValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdCrewTransportID.Value))
                                    {
                                        var TransportValue1 = TransportValue.EntityList.Where(x => x.TransportID == Convert.ToInt64(hdCrewTransportID.Value)).ToList();
                                        if (TransportValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(TransportValue1[0].TransportCD.ToString()))
                                                tbCrewTransportCode.Text = TransportValue1[0].TransportCD;
                                            //if (!string.IsNullOrEmpty(TransportValue1[0].TransportationVendor.ToString()))
                                            //    tbCrewTransportName.Text = TransportValue1[0].TransportationVendor.ToString();
                                            //if (TransportValue1[0].PhoneNum != null && !string.IsNullOrEmpty(TransportValue1[0].PhoneNum.ToString()))
                                            //    tbCrewTransportPhone.Text = TransportValue1[0].PhoneNum.ToString();
                                            //if (TransportValue1[0].FaxNum != null && !string.IsNullOrEmpty(TransportValue1[0].FaxNum.ToString()))
                                            //    tbCrewTransportFax.Text = TransportValue1[0].FaxNum.ToString();
                                            //if (TransportValue1[0].ContactEmail != null && !string.IsNullOrEmpty(TransportValue1[0].ContactEmail.ToString()))
                                            //    tbCrewTransportEmail.Text = TransportValue1[0].ContactEmail.ToString();
                                            //if (TransportValue1[0].NegotiatedRate != null && !string.IsNullOrEmpty(TransportValue1[0].NegotiatedRate.ToString()))
                                            //    tbCrewTransportRate.Text = TransportValue1[0].NegotiatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                        if (cqtransport.CQTransportListDescription != null && !string.IsNullOrEmpty(cqtransport.CQTransportListDescription))
                            tbCrewTransportName.Text = cqtransport.CQTransportListDescription.ToString();
                        if (cqtransport.PhoneNUM != null && !string.IsNullOrEmpty(cqtransport.PhoneNUM.ToString()))
                            tbCrewTransportPhone.Text = cqtransport.PhoneNUM.ToString();
                        if (cqtransport.FaxNUM != null && !string.IsNullOrEmpty(cqtransport.FaxNUM.ToString()))
                            tbCrewTransportFax.Text = cqtransport.FaxNUM.ToString();
                        if (cqtransport.Email != null && !string.IsNullOrEmpty(cqtransport.Email.ToString()))
                            tbCrewTransportEmail.Text = cqtransport.Email.ToString();
                        if (cqtransport.Rate != null && !string.IsNullOrEmpty(cqtransport.Rate.ToString()))
                            tbCrewTransportRate.Text = cqtransport.Rate.ToString();
                    }
                }
            }
        }

        private void UpdateScreenDetailstoCQHotel(ref CQHotelList cqhotel, string paxcrewmaint)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqhotel, paxcrewmaint))
            {
                if (paxcrewmaint.Equals("HP"))
                {
                    if (!string.IsNullOrEmpty(hdPaxHotelID.Value))
                    {
                        if (!string.IsNullOrEmpty(hdPaxHotelID.Value))
                            cqhotel.HotelID = Convert.ToInt64(hdPaxHotelID.Value);
                        else
                            cqhotel.HotelID = null;

                        if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            cqhotel.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                        else
                            cqhotel.AirportID = null;

                        cqhotel.RecordType = "HP";
                        cqhotel.LastUpdTS = DateTime.UtcNow;
                        cqhotel.IsDeleted = false;
                        cqhotel.CQHotelListDescription = tbPaxHotelName.Text;
                        cqhotel.PhoneNUM = tbPaxHotelPhone.Text;
                        cqhotel.FaxNUM = tbPaxHotelFax.Text;
                        cqhotel.Email = tbPaxHotelEmail.Text;
                        if (!string.IsNullOrEmpty(tbPaxHotelRate.Text))
                            cqhotel.Rate = Convert.ToDecimal(tbPaxHotelRate.Text);
                    }
                    else
                    {
                        cqhotel.HotelID = null;
                        cqhotel.CQHotelListDescription = string.Empty;
                        cqhotel.PhoneNUM = string.Empty;
                        cqhotel.FaxNUM = string.Empty;
                        cqhotel.Email = string.Empty;
                        cqhotel.Rate = null;
                    }
                }
                else if (paxcrewmaint.Equals("HC"))
                {
                    if (!string.IsNullOrEmpty(hdCrewHotelID.Value))
                    {
                        if (!string.IsNullOrEmpty(hdCrewHotelID.Value))
                            cqhotel.HotelID = Convert.ToInt64(hdCrewHotelID.Value);
                        else
                            cqhotel.HotelID = null;

                        if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            cqhotel.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                        else
                            cqhotel.AirportID = null;

                        cqhotel.RecordType = "HC";
                        cqhotel.LastUpdTS = DateTime.UtcNow;
                        cqhotel.IsDeleted = false;
                        cqhotel.CQHotelListDescription = tbCrewHotelName.Text;
                        cqhotel.PhoneNUM = tbCrewHotelPhone.Text;
                        cqhotel.FaxNUM = tbCrewHotelFax.Text;
                        cqhotel.Email = tbCrewHotelEmail.Text;
                        if (!string.IsNullOrEmpty(tbCrewHotelRate.Text))
                            cqhotel.Rate = Convert.ToDecimal(tbCrewHotelRate.Text);
                    }
                    else
                    {
                        cqhotel.HotelID = null;
                        cqhotel.CQHotelListDescription = string.Empty;
                        cqhotel.PhoneNUM = string.Empty;
                        cqhotel.FaxNUM = string.Empty;
                        cqhotel.Email = string.Empty;
                        cqhotel.Rate = null;
                    }
                }
                else if (paxcrewmaint.Equals("HM"))
                {
                    if (!string.IsNullOrEmpty(hdMaintHotelID.Value))
                    {
                        if (!string.IsNullOrEmpty(hdMaintHotelID.Value))
                            cqhotel.HotelID = Convert.ToInt64(hdMaintHotelID.Value);
                        else
                            cqhotel.HotelID = null;

                        if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            cqhotel.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                        else
                            cqhotel.AirportID = null;

                        cqhotel.RecordType = "HM";
                        cqhotel.LastUpdTS = DateTime.UtcNow;
                        cqhotel.IsDeleted = false;
                        cqhotel.CQHotelListDescription = tbMaintHotelName.Text;
                        cqhotel.PhoneNUM = tbMaintHotelPhone.Text;
                        cqhotel.FaxNUM = tbMaintHotelFax.Text;
                        cqhotel.Email = tbMaintHotelEmail.Text;
                        if (!string.IsNullOrEmpty(tbMaintHotelRate.Text))
                            cqhotel.Rate = Convert.ToDecimal(tbMaintHotelRate.Text);
                    }
                    else
                    {
                        cqhotel.HotelID = null;
                        cqhotel.CQHotelListDescription = string.Empty;
                        cqhotel.PhoneNUM = string.Empty;
                        cqhotel.FaxNUM = string.Empty;
                        cqhotel.Email = string.Empty;
                        cqhotel.Rate = null;
                    }
                }
            }
        }

        private void UpdateScreenDetailsFromCQHotel(CQHotelList cqhotel)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqhotel))
            {
                if (cqhotel.RecordType == "HP")
                {
                    if (hdArriveIcaoID.Value == cqhotel.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqhotel.HotelID.ToString()))
                        {
                            hdPaxHotelID.Value = cqhotel.HotelID.ToString();
                        }
                        else
                        {
                            hdPaxHotelID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (HotelValue.ReturnFlag == true)
                            {
                                if (HotelValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdPaxHotelID.Value))
                                    {
                                        var HotelValue1 = HotelValue.EntityList.Where(x => x.HotelID == Convert.ToInt64(hdPaxHotelID.Value)).ToList();
                                        if (HotelValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(HotelValue1[0].HotelCD.ToString()))
                                                tbPaxHotelCode.Text = HotelValue1[0].HotelCD;
                                            //if (!string.IsNullOrEmpty(HotelValue1[0].Name.ToString()))
                                            //    tbPaxHotelName.Text = HotelValue1[0].Name.ToString();
                                            //if (HotelValue1[0].PhoneNum != null && !string.IsNullOrEmpty(HotelValue1[0].PhoneNum.ToString()))
                                            //    tbPaxHotelPhone.Text = HotelValue1[0].PhoneNum.ToString();
                                            //if (HotelValue1[0].FaxNum != null && !string.IsNullOrEmpty(HotelValue1[0].FaxNum.ToString()))
                                            //    tbPaxHotelFax.Text = HotelValue1[0].FaxNum.ToString();
                                            //if (HotelValue1[0].ContactEmail != null && !string.IsNullOrEmpty(HotelValue1[0].ContactEmail.ToString()))
                                            //    tbPaxHotelEmail.Text = HotelValue1[0].ContactEmail.ToString();
                                            //if (HotelValue1[0].NegociatedRate != null && !string.IsNullOrEmpty(HotelValue1[0].NegociatedRate.ToString()))
                                            //    tbPaxHotelRate.Text = HotelValue1[0].NegociatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                        if (cqhotel.CQHotelListDescription != null && !string.IsNullOrEmpty(cqhotel.CQHotelListDescription))
                            tbPaxHotelName.Text = cqhotel.CQHotelListDescription.ToString();
                        if (cqhotel.PhoneNUM != null && !string.IsNullOrEmpty(cqhotel.PhoneNUM.ToString()))
                            tbPaxHotelPhone.Text = cqhotel.PhoneNUM.ToString();
                        if (cqhotel.FaxNUM != null && !string.IsNullOrEmpty(cqhotel.FaxNUM.ToString()))
                            tbPaxHotelFax.Text = cqhotel.FaxNUM.ToString();
                        if (cqhotel.Email != null && !string.IsNullOrEmpty(cqhotel.Email.ToString()))
                            tbPaxHotelEmail.Text = cqhotel.Email.ToString();
                        if (cqhotel.Rate != null && !string.IsNullOrEmpty(cqhotel.Rate.ToString()))
                            tbPaxHotelRate.Text = cqhotel.Rate.ToString();
                    }
                }
                else if (cqhotel.RecordType == "HC")
                {
                    if (hdArriveIcaoID.Value == cqhotel.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqhotel.HotelID.ToString()))
                        {
                            hdCrewHotelID.Value = cqhotel.HotelID.ToString();
                        }
                        else
                        {
                            hdCrewHotelID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (HotelValue.ReturnFlag == true)
                            {
                                if (HotelValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdCrewHotelID.Value))
                                    {
                                        var HotelValue1 = HotelValue.EntityList.Where(x => x.HotelID == Convert.ToInt64(hdCrewHotelID.Value)).ToList();
                                        if (HotelValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(HotelValue1[0].HotelCD.ToString()))
                                                tbCrewHotelCode.Text = HotelValue1[0].HotelCD;
                                            //if (!string.IsNullOrEmpty(HotelValue1[0].Name.ToString()))
                                            //    tbCrewHotelName.Text = HotelValue1[0].Name.ToString();
                                            //if (HotelValue1[0].PhoneNum != null && !string.IsNullOrEmpty(HotelValue1[0].PhoneNum.ToString()))
                                            //    tbCrewHotelPhone.Text = HotelValue1[0].PhoneNum.ToString();
                                            //if (HotelValue1[0].FaxNum != null && !string.IsNullOrEmpty(HotelValue1[0].FaxNum.ToString()))
                                            //    tbCrewHotelFax.Text = HotelValue1[0].FaxNum.ToString();
                                            //if (HotelValue1[0].ContactEmail != null && !string.IsNullOrEmpty(HotelValue1[0].ContactEmail.ToString()))
                                            //    tbCrewHotelEmail.Text = HotelValue1[0].ContactEmail.ToString();
                                            //if (HotelValue1[0].NegociatedRate != null && !string.IsNullOrEmpty(HotelValue1[0].NegociatedRate.ToString()))
                                            //    tbCrewHotelRate.Text = HotelValue1[0].NegociatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                        if (cqhotel.CQHotelListDescription != null && !string.IsNullOrEmpty(cqhotel.CQHotelListDescription))
                            tbCrewHotelName.Text = cqhotel.CQHotelListDescription.ToString();
                        if (cqhotel.PhoneNUM != null && !string.IsNullOrEmpty(cqhotel.PhoneNUM.ToString()))
                            tbCrewHotelPhone.Text = cqhotel.PhoneNUM.ToString();
                        if (cqhotel.FaxNUM != null && !string.IsNullOrEmpty(cqhotel.FaxNUM.ToString()))
                            tbCrewHotelFax.Text = cqhotel.FaxNUM.ToString();
                        if (cqhotel.Email != null && !string.IsNullOrEmpty(cqhotel.Email.ToString()))
                            tbCrewHotelEmail.Text = cqhotel.Email.ToString();
                        if (cqhotel.Rate != null && !string.IsNullOrEmpty(cqhotel.Rate.ToString()))
                            tbCrewHotelRate.Text = cqhotel.Rate.ToString();
                    }
                }
                else if (cqhotel.RecordType == "HM")
                {
                    if (hdArriveIcaoID.Value == cqhotel.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(cqhotel.HotelID.ToString()))
                        {
                            hdMaintHotelID.Value = cqhotel.HotelID.ToString();
                        }
                        else
                        {
                            hdMaintHotelID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (HotelValue.ReturnFlag == true)
                            {
                                if (HotelValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdMaintHotelID.Value))
                                    {
                                        var HotelValue1 = HotelValue.EntityList.Where(x => x.HotelID == Convert.ToInt64(hdMaintHotelID.Value)).ToList();
                                        if (HotelValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(HotelValue1[0].HotelCD.ToString()))
                                                tbMaintHotelCode.Text = HotelValue1[0].HotelCD;
                                            //if (!string.IsNullOrEmpty(HotelValue1[0].Name.ToString()))
                                            //    tbMaintHotelName.Text = HotelValue1[0].Name.ToString();
                                            //if (HotelValue1[0].PhoneNum != null && !string.IsNullOrEmpty(HotelValue1[0].PhoneNum.ToString()))
                                            //    tbMaintHotelPhone.Text = HotelValue1[0].PhoneNum.ToString();
                                            //if (HotelValue1[0].FaxNum != null && !string.IsNullOrEmpty(HotelValue1[0].FaxNum.ToString()))
                                            //    tbMaintHotelFax.Text = HotelValue1[0].FaxNum.ToString();
                                            //if (HotelValue1[0].ContactEmail != null && !string.IsNullOrEmpty(HotelValue1[0].ContactEmail.ToString()))
                                            //    tbMaintHotelEmail.Text = HotelValue1[0].ContactEmail.ToString();
                                            //if (HotelValue1[0].NegociatedRate != null && !string.IsNullOrEmpty(HotelValue1[0].NegociatedRate.ToString()))
                                            //    tbMaintHotelRate.Text = HotelValue1[0].NegociatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                        if (cqhotel.CQHotelListDescription != null && !string.IsNullOrEmpty(cqhotel.CQHotelListDescription))
                            tbMaintHotelName.Text = cqhotel.CQHotelListDescription.ToString();
                        if (cqhotel.PhoneNUM != null && !string.IsNullOrEmpty(cqhotel.PhoneNUM.ToString()))
                            tbMaintHotelPhone.Text = cqhotel.PhoneNUM.ToString();
                        if (cqhotel.FaxNUM != null && !string.IsNullOrEmpty(cqhotel.FaxNUM.ToString()))
                            tbMaintHotelFax.Text = cqhotel.FaxNUM.ToString();
                        if (cqhotel.Email != null && !string.IsNullOrEmpty(cqhotel.Email.ToString()))
                            tbMaintHotelEmail.Text = cqhotel.Email.ToString();
                        if (cqhotel.Rate != null && !string.IsNullOrEmpty(cqhotel.Rate.ToString()))
                            tbMaintHotelRate.Text = cqhotel.Rate.ToString();
                    }
                }
            }
        }

        private void CQchoice(CQLeg quoteLeg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(quoteLeg))
            {
                if (quoteLeg.DepartAirportChanged)
                {
                    #region "Depart FBO"
                    if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    {
                        if (quoteLeg.CQFBOLists != null && quoteLeg.CQFBOLists.Count() > 0)
                        {
                            CQFBOList Departfbo = (from fbo in quoteLeg.CQFBOLists.ToList()
                                                   where fbo.IsDeleted == false && fbo.RecordType == "FD"
                                                   select fbo).SingleOrDefault();

                            if (Departfbo != null)
                            {
                                if (Departfbo.CQFBOListID == 0)
                                    quoteLeg.CQFBOLists.Remove(Departfbo);
                                else
                                {

                                    Departfbo.IsDeleted = true;
                                    Departfbo.State = CQRequestEntityState.Deleted;
                                }
                            }
                        }

                        #region "Add Depart FBO"


                        if (quoteLeg.DAirportID != null && quoteLeg.DAirportID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID(Convert.ToInt64(quoteLeg.DAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                CQFBOList Fbo = new CQFBOList();

                                CharterQuoteService.FBO DepFbo = new CharterQuoteService.FBO();

                                DepFbo.FBOID = ObjRetValGroup[0].FBOID;
                                DepFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                Fbo.FBO = DepFbo;
                                Fbo.AirportID = quoteLeg.DAirportID;
                                Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                Fbo.RecordType = "FD";
                                Fbo.LastUpdTS = DateTime.UtcNow;
                                Fbo.IsDeleted = false;
                                Fbo.State = CQRequestEntityState.Added;
                                if (quoteLeg.CQFBOLists == null)
                                    quoteLeg.CQFBOLists = new List<CQFBOList>();
                                quoteLeg.CQFBOLists.Add(Fbo);
                            }
                        }
                        #endregion

                    }
                    #endregion
                    //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                    #region "Maintanence Catering"
                    //if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    //{
                    //    if (quoteLeg.CQCateringLists != null && quoteLeg.CQCateringLists.Count() > 0)
                    //    {
                    //        CQCateringList MaintCatering = (from catering in quoteLeg.CQCateringLists.ToList()
                    //                                        where catering.IsDeleted == false && catering.RecordType == "CM"
                    //                                        select catering).SingleOrDefault();

                    //        if (MaintCatering != null)
                    //        {
                    //            if (MaintCatering.CQCateringListID == 0)
                    //                quoteLeg.CQCateringLists.Remove(MaintCatering);
                    //            else
                    //            {

                    //                MaintCatering.IsDeleted = true;
                    //                MaintCatering.State = CQRequestEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Maintanence Catering"


                    //    if (quoteLeg.DAirportID != null && quoteLeg.DAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetAllCateringByAirportID(Convert.ToInt64(quoteLeg.DAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            CQCateringList catering = new CQCateringList();

                    //            CharterQuoteService.Catering maintCatering = new CharterQuoteService.Catering();

                    //            maintCatering.CateringID = ObjRetValGroup[0].CateringID;
                    //            maintCatering.CateringCD = ObjRetValGroup[0].CateringCD;
                    //            catering.Catering = maintCatering;

                    //            catering.AirportID = quoteLeg.DAirportID;
                    //            catering.CateringID = ObjRetValGroup[0].CateringID;
                    //            if (ObjRetValGroup[0].CateringVendor != null)
                    //                catering.CQCateringListDescription = ObjRetValGroup[0].CateringVendor;
                    //            if (ObjRetValGroup[0].PhoneNum != null)
                    //                catering.PhoneNUM = ObjRetValGroup[0].PhoneNum;
                    //            if (ObjRetValGroup[0].FaxNum != null)
                    //                catering.FaxNUM = ObjRetValGroup[0].FaxNum;
                    //            if (ObjRetValGroup[0].ContactEmail != null)
                    //                catering.Email = ObjRetValGroup[0].ContactEmail;
                    //            if (ObjRetValGroup[0].NegotiatedRate != null)
                    //                catering.Rate = (decimal)ObjRetValGroup[0].NegotiatedRate;
                    //            catering.RecordType = "CM";
                    //            catering.LastUpdTS = DateTime.UtcNow;
                    //            catering.IsDeleted = false;
                    //            catering.State = CQRequestEntityState.Added;
                    //            if (quoteLeg.CQCateringLists == null)
                    //                quoteLeg.CQCateringLists = new List<CQCateringList>();
                    //            quoteLeg.CQCateringLists.Add(catering);
                    //        }
                    //    }
                    //    #endregion

                    //}
                    #endregion

                    quoteLeg.DepartAirportChanged = false;
                }

                if (quoteLeg.ArrivalAirportChanged)
                {
                    #region "Arrive FBO"
                    if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    {
                        if (quoteLeg.CQFBOLists != null && quoteLeg.CQFBOLists.Count() > 0)
                        {
                            CQFBOList Arrivefbo = (from fbo in quoteLeg.CQFBOLists.ToList()
                                                   where fbo.IsDeleted == false && fbo.RecordType == "FA"
                                                   select fbo).SingleOrDefault();

                            if (Arrivefbo != null)
                            {
                                if (Arrivefbo.CQFBOListID == 0)
                                    quoteLeg.CQFBOLists.Remove(Arrivefbo);
                                else
                                {

                                    Arrivefbo.IsDeleted = true;
                                    Arrivefbo.State = CQRequestEntityState.Deleted;
                                }
                            }
                        }

                        #region "Add Arrive FBO"


                        if (quoteLeg.AAirportID != null && quoteLeg.AAirportID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID(Convert.ToInt64(quoteLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                CQFBOList Fbo = new CQFBOList();

                                CharterQuoteService.FBO PrefFbo = new CharterQuoteService.FBO();

                                PrefFbo.FBOID = ObjRetValGroup[0].FBOID;
                                PrefFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                Fbo.FBO = PrefFbo;
                                Fbo.AirportID = quoteLeg.AAirportID;
                                Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                Fbo.RecordType = "FA";
                                Fbo.LastUpdTS = DateTime.UtcNow;
                                Fbo.IsDeleted = false;
                                Fbo.State = CQRequestEntityState.Added;
                                if (quoteLeg.CQFBOLists == null)
                                    quoteLeg.CQFBOLists = new List<CQFBOList>();
                                quoteLeg.CQFBOLists.Add(Fbo);
                            }
                        }
                        #endregion

                    }
                    #endregion

                    //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                    #region "Transport"
                    //if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    //{
                    //    if (quoteLeg.CQTransportLists != null && quoteLeg.CQTransportLists.Count() > 0)
                    //    {
                    //        CQTransportList paxTrans = (from paxtransport in quoteLeg.CQTransportLists.ToList()
                    //                                    where paxtransport.IsDeleted == false && paxtransport.RecordType == "TP"
                    //                                    select paxtransport).SingleOrDefault();

                    //        CQTransportList crewTrans = (from paxtransport in quoteLeg.CQTransportLists.ToList()
                    //                                     where paxtransport.IsDeleted == false && paxtransport.RecordType == "TC"
                    //                                     select paxtransport).SingleOrDefault();

                    //        if (paxTrans != null)
                    //        {
                    //            if (paxTrans.CQTransportListID == 0)
                    //                quoteLeg.CQTransportLists.Remove(paxTrans);
                    //            else
                    //            {
                    //                paxTrans.IsDeleted = true;
                    //                paxTrans.State = CQRequestEntityState.Deleted;
                    //            }
                    //        }

                    //        if (crewTrans != null)
                    //        {
                    //            if (crewTrans.CQTransportListID == 0)
                    //                quoteLeg.CQTransportLists.Remove(crewTrans);
                    //            else
                    //            {
                    //                crewTrans.IsDeleted = true;
                    //                crewTrans.State = CQRequestEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Transport for Pax and Crew"

                    //    if (quoteLeg.AAirportID != null && quoteLeg.AAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetTransportByAirportID(Convert.ToInt64(quoteLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            #region "Pax"
                    //            CQTransportList PaxTransport = new CQTransportList();

                    //            CharterQuoteService.Transport paxTrans = new CharterQuoteService.Transport();

                    //            paxTrans.TransportID = ObjRetValGroup[0].TransportID;
                    //            paxTrans.TransportCD = ObjRetValGroup[0].TransportCD;
                    //            PaxTransport.Transport = paxTrans;

                    //            PaxTransport.AirportID = quoteLeg.AAirportID;
                    //            PaxTransport.TransportID = ObjRetValGroup[0].TransportID;
                    //            if (ObjRetValGroup[0].TransportationVendor != null)
                    //                PaxTransport.CQTransportListDescription = ObjRetValGroup[0].TransportationVendor;
                    //            if (ObjRetValGroup[0].PhoneNum != null)
                    //                PaxTransport.PhoneNUM = ObjRetValGroup[0].PhoneNum;
                    //            if (ObjRetValGroup[0].FaxNum != null)
                    //                PaxTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                    //            if (ObjRetValGroup[0].ContactEmail != null)
                    //                PaxTransport.Email = ObjRetValGroup[0].ContactEmail;
                    //            if (ObjRetValGroup[0].NegotiatedRate != null)
                    //                PaxTransport.Rate = (decimal)ObjRetValGroup[0].NegotiatedRate;
                    //            PaxTransport.RecordType = "TP";
                    //            PaxTransport.LastUpdTS = DateTime.UtcNow;
                    //            PaxTransport.IsDeleted = false;
                    //            PaxTransport.State = CQRequestEntityState.Added;
                    //            if (quoteLeg.CQTransportLists == null)
                    //                quoteLeg.CQTransportLists = new List<CQTransportList>();
                    //            quoteLeg.CQTransportLists.Add(PaxTransport);
                    //            #endregion

                    //            #region "Crew"
                    //            CQTransportList CrewTransport = new CQTransportList();

                    //            CharterQuoteService.Transport crewTrans = new CharterQuoteService.Transport();

                    //            crewTrans.TransportID = ObjRetValGroup[0].TransportID;
                    //            crewTrans.TransportCD = ObjRetValGroup[0].TransportCD;
                    //            CrewTransport.Transport = crewTrans;

                    //            CrewTransport.AirportID = quoteLeg.AAirportID;
                    //            CrewTransport.TransportID = ObjRetValGroup[0].TransportID;
                    //            if (ObjRetValGroup[0].TransportationVendor != null)
                    //                CrewTransport.CQTransportListDescription = ObjRetValGroup[0].TransportationVendor;
                    //            if (ObjRetValGroup[0].PhoneNum != null)
                    //                CrewTransport.PhoneNUM = ObjRetValGroup[0].PhoneNum;
                    //            if (ObjRetValGroup[0].FaxNum != null)
                    //                CrewTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                    //            if (ObjRetValGroup[0].ContactEmail != null)
                    //                CrewTransport.Email = ObjRetValGroup[0].ContactEmail;
                    //            if (ObjRetValGroup[0].NegotiatedRate != null)
                    //                CrewTransport.Rate = (decimal)ObjRetValGroup[0].NegotiatedRate;
                    //            CrewTransport.RecordType = "TC";
                    //            CrewTransport.LastUpdTS = DateTime.UtcNow;
                    //            CrewTransport.IsDeleted = false;
                    //            CrewTransport.State = CQRequestEntityState.Added;
                    //            if (quoteLeg.CQTransportLists == null)
                    //                quoteLeg.CQTransportLists = new List<CQTransportList>();
                    //            quoteLeg.CQTransportLists.Add(CrewTransport);
                    //            #endregion
                    //        }
                    //    }
                    //    #endregion
                    //}
                    #endregion

                    quoteLeg.ArrivalAirportChanged = false;
                }

                Session[Master.CQSessionKey] = FileRequest;
            }
        }

        #region Button Events

        /// <summary>
        /// To delete the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        Master.Delete((int)FileRequest.FileNUM);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To cancel the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To save the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            //if (lbcvArriveFBOCode.Text == string.Empty && lbcvCrewHotelCode.Text == string.Empty && lbcvCrewTransportCode.Text == string.Empty && lbcvDepartFBOCode.Text == string.Empty && lbcvMaintCateringCode.Text == string.Empty && lbcvMaintHotelCode.Text == string.Empty && lbcvPaxHotelCode.Text == string.Empty && lbcvPaxTransportCode.Text == string.Empty)
                            //{
                            SaveCharterQuoteToSession();
                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            Master.Save(FileRequest);
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        /// <summary>
        /// To move next tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Next_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        Master.Next("Logistics");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }

        protected void btnNextFBOYes_Click(object sender, EventArgs e)
        {
            if (Session[Master.CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[Master.CQSessionKey];

                if (FileRequest.Mode == CQRequestActionMode.Edit)
                {

                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                    if (QuoteInProgress != null)
                    {
                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                        {
                            CQLeg NextLeg = new CQLeg();
                            NextLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == (Convert.ToInt16(hdnLegNum.Value) + 1) && x.IsDeleted == false).FirstOrDefault();

                            if (NextLeg != null)
                            {
                                if (NextLeg.CQLegID != 0)
                                    NextLeg.State = CQRequestEntityState.Modified;
                                else
                                    NextLeg.State = CQRequestEntityState.Added;
                                NextLeg.DepartAirportChanged = false;
                                bool NextFBOFound = false;

                                CQFBOList nextDepFBO = null;
                                if (NextLeg.CQFBOLists != null)
                                {
                                    #region Next Leg Dept FBO Modify
                                    nextDepFBO = NextLeg.CQFBOLists.Where(x => x.IsDeleted == false && x.RecordType == "FD").FirstOrDefault();

                                    if ((nextDepFBO != null))
                                    {
                                        NextFBOFound = true;
                                        nextDepFBO.State = CQRequestEntityState.Modified;
                                        if (!string.IsNullOrEmpty(hdArriveFBOID.Value))
                                            nextDepFBO.FBOID = Convert.ToInt64(hdArriveFBOID.Value);
                                        else
                                            nextDepFBO.FBOID = null;
                                        nextDepFBO.AirportID = NextLeg.DAirportID;
                                        nextDepFBO.RecordType = "FD";
                                        nextDepFBO.LastUpdTS = DateTime.UtcNow;
                                        nextDepFBO.IsDeleted = false;
                                    }
                                    #endregion
                                }

                                if (!NextFBOFound)
                                {
                                    #region Add New Depart FBO to next leg
                                    if (NextLeg.CQFBOLists == null)
                                        NextLeg.CQFBOLists = new List<CQFBOList>();

                                    nextDepFBO = new CQFBOList();
                                    nextDepFBO.State = CQRequestEntityState.Added;

                                    if (!string.IsNullOrEmpty(hdArriveFBOID.Value))
                                        nextDepFBO.FBOID = Convert.ToInt64(hdArriveFBOID.Value);
                                    else
                                        nextDepFBO.FBOID = null;
                                    nextDepFBO.AirportID = NextLeg.DAirportID;
                                    nextDepFBO.RecordType = "FD";
                                    nextDepFBO.LastUpdTS = DateTime.UtcNow;
                                    nextDepFBO.IsDeleted = false;
                                    NextLeg.CQFBOLists.Add(nextDepFBO);
                                    #endregion
                                }
                            }
                        }
                    }
                }

                Session[Master.CQSessionKey] = FileRequest;
            }
            RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveFBO);
        }

        protected void btnNextFBONo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveFBO);
            // Code for no copy of FBO for Next Leg
        }

        protected void btnPreviousFBOYes_Click(object sender, EventArgs e)
        {
            if (Session[Master.CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[Master.CQSessionKey];

                if (FileRequest.Mode == CQRequestActionMode.Edit)
                {

                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                    if (QuoteInProgress != null)
                    {
                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                        {
                            CQLeg PreLeg = new CQLeg();
                            PreLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == (Convert.ToInt16(hdnLegNum.Value) - 1) && x.IsDeleted == false).FirstOrDefault();

                            if (PreLeg != null)
                            {
                                if (PreLeg.CQLegID != 0)
                                    PreLeg.State = CQRequestEntityState.Modified;
                                else
                                    PreLeg.State = CQRequestEntityState.Added;

                                PreLeg.ArrivalAirportChanged = false;
                                bool PrevFBOFound = false;

                                CQFBOList PrevArrFBO = null;
                                if (PreLeg.CQFBOLists != null)
                                {
                                    #region Prev Leg Dept FBO Modify
                                    PrevArrFBO = PreLeg.CQFBOLists.Where(x => x.IsDeleted == false && x.RecordType == "FA").FirstOrDefault();

                                    if ((PrevArrFBO != null))
                                    {
                                        PrevFBOFound = true;
                                        PrevArrFBO.State = CQRequestEntityState.Modified;

                                        if (!string.IsNullOrEmpty(hdDepartFBOID.Value))
                                            PrevArrFBO.FBOID = Convert.ToInt64(hdDepartFBOID.Value);
                                        else
                                            PrevArrFBO.FBOID = null;
                                        PrevArrFBO.AirportID = PreLeg.AAirportID;
                                        PrevArrFBO.RecordType = "FA";
                                        PrevArrFBO.LastUpdTS = DateTime.UtcNow;
                                        PrevArrFBO.IsDeleted = false;
                                    }
                                    #endregion
                                }

                                if (!PrevFBOFound)
                                {
                                    #region Add New Depart FBO to Prev leg
                                    if (PreLeg.CQFBOLists == null)
                                        PreLeg.CQFBOLists = new List<CQFBOList>();

                                    PrevArrFBO = new CQFBOList();
                                    PrevArrFBO.State = CQRequestEntityState.Added;

                                    if (!string.IsNullOrEmpty(hdDepartFBOID.Value))
                                        PrevArrFBO.FBOID = Convert.ToInt64(hdDepartFBOID.Value);
                                    else
                                        PrevArrFBO.FBOID = null;
                                    PrevArrFBO.AirportID = PreLeg.AAirportID;
                                    PrevArrFBO.RecordType = "FA";
                                    PrevArrFBO.LastUpdTS = DateTime.UtcNow;
                                    PrevArrFBO.IsDeleted = false;
                                    PreLeg.CQFBOLists.Add(PrevArrFBO);
                                    #endregion
                                }
                            }
                        }
                    }
                }

                Session[Master.CQSessionKey] = FileRequest;
            }
            RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartFBO);
        }

        protected void btnPreviousFBONo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartFBO);
            // Code for no copy of FBO for Previous Leg
        }

        #endregion

        #region AJAX Events
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            //if (e.Argument.ToString() == "DepartFBO_TextChanged")
            //    DepartFBO_TextChanged(sender, e);
            //if (e.Argument.ToString() == "ArriveFBO_TextChanged")
            //    ArriveFBO_TextChanged(sender, e);
            if (e.Argument.ToString() == "MaintCatering_TextChanged")
                MaintCatering_TextChanged(sender, e);
            if (e.Argument.ToString() == "PaxTransportCode_TextChanged")
                PaxTransportCode_TextChanged(sender, e);
            if (e.Argument.ToString() == "CrewTransportCode_TextChanged")
                CrewTransportCode_TextChanged(sender, e);
            if (e.Argument.ToString() == "PaxHotelCode_TextChanged")
                PaxHotelCode_TextChanged(sender, e);
            if (e.Argument.ToString() == "CrewHotelCode_TextChanged")
                CrewHotelCode_TextChanged(sender, e);
            if (e.Argument.ToString() == "MaintHotelCode_TextChanged")
                MaintHotelCode_TextChanged(sender, e);
        }
        #endregion

        #region Permissions

        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.CharterQuote.ViewCQLogistics);
                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                // Check for Field Level Permissions
                if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                {
                    // Show read-only form
                    EnableForm(false);

                    // Check Page Controls - Visibility
                    if (IsAuthorized(Permission.CharterQuote.AddCQLogistics) || IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQLogistics))
                    {
                        ControlVisibility("Button", btnDelete, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                    {
                        ControlVisibility("Button", btnEditFile, true);
                    }
                    // Check Page Controls - Disable/Enable
                    if (Session[Master.CQSessionKey] != null)
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];

                        if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                        {
                            EnableForm(true);

                            if (IsAuthorized(Permission.CharterQuote.AddCQLogistics) || IsAuthorized(Permission.CharterQuote.EditCQLogistics))
                            {
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                            }
                            ControlEnable("Button", btnDelete, false, "button-disable");
                            ControlEnable("Button", btnEditFile, false, "button-disable");
                        }
                        else
                        {
                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDelete, true, "button");
                            ControlEnable("Button", btnEditFile, true, "button");
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                    }
                }
            }
        }

        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            txt.CssClass = cssClass;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method to Enable and Disable Form
        /// </summary>
        /// <param name="isEnable">Boolean</param>
        protected void EnableForm(bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isEnable))
            {
                tbDepartFBO.Enabled = isEnable;
                tbDepartFBOEmail.Enabled = isEnable;
                tbDepartFBOFax.Enabled = isEnable;
                tbDepartFBOName.Enabled = isEnable;
                tbDepartFBOPhone.Enabled = isEnable;

                tbArriveFBO.Enabled = isEnable;
                tbArriveFBOEmail.Enabled = isEnable;
                tbArriveFBOFax.Enabled = isEnable;
                tbArriveFBOName.Enabled = isEnable;
                tbArriveFBOPhone.Enabled = isEnable;

                tbCrewHotelCode.Enabled = isEnable;
                tbCrewHotelName.Enabled = isEnable;
                tbCrewHotelPhone.Enabled = isEnable;
                tbCrewHotelRate.Enabled = isEnable;
                tbCrewHotelFax.Enabled = isEnable;
                tbCrewHotelEmail.Enabled = isEnable;

                tbCrewTransportCode.Enabled = isEnable;
                tbCrewTransportName.Enabled = isEnable;
                tbCrewTransportPhone.Enabled = isEnable;
                tbCrewTransportRate.Enabled = isEnable;
                tbCrewTransportFax.Enabled = isEnable;
                tbCrewTransportEmail.Enabled = isEnable;

                tbPaxHotelCode.Enabled = isEnable;
                tbPaxHotelName.Enabled = isEnable;
                tbPaxHotelPhone.Enabled = isEnable;
                tbPaxHotelRate.Enabled = isEnable;
                tbPaxHotelFax.Enabled = isEnable;
                tbPaxHotelEmail.Enabled = isEnable;

                tbPaxTransportCode.Enabled = isEnable;
                tbPaxTransportName.Enabled = isEnable;
                tbPaxTransportPhone.Enabled = isEnable;
                tbPaxTransportRate.Enabled = isEnable;
                tbPaxTransportFax.Enabled = isEnable;
                tbPaxTransportEmail.Enabled = isEnable;

                tbPaxNotes.Enabled = isEnable;

                tbMaintHotelCode.Enabled = isEnable;
                tbMaintHotelName.Enabled = isEnable;
                tbMaintHotelPhone.Enabled = isEnable;
                tbMaintHotelRate.Enabled = isEnable;
                tbMaintHotelFax.Enabled = isEnable;
                tbMaintHotelEmail.Enabled = isEnable;

                tbMaintCateringCode.Enabled = isEnable;
                tbMaintCateringFax.Enabled = isEnable;
                tbMaintCateringName.Enabled = isEnable;
                tbMaintCateringPhone.Enabled = isEnable;
                tbMaintCateringRate.Enabled = isEnable;
                tbMaintCateringFax.Enabled = isEnable;
                tbMaintCateringEmail.Enabled = isEnable;

                //btnAddColumns.Enabled = isEnable;
                //btnCalender.Enabled = isEnable;
                btnArriveFBO.Enabled = isEnable;
                btnCrewHotelCode.Enabled = isEnable;
                btnCrewTransportCode.Enabled = isEnable;
                btnDepartFBO.Enabled = isEnable;
                btnMaintCateringCode.Enabled = isEnable;
                btnMaintHotelCode.Enabled = isEnable;
                btnPaxHotelCode.Enabled = isEnable;
                btnPaxTransportCode.Enabled = isEnable;

                if (isEnable)
                {
                    btnArriveFBO.CssClass = "browse-button";
                    btnCrewHotelCode.CssClass = "browse-button";
                    btnCrewTransportCode.CssClass = "browse-button";
                    btnDepartFBO.CssClass = "browse-button";
                    btnMaintCateringCode.CssClass = "browse-button";
                    btnMaintHotelCode.CssClass = "browse-button";
                    btnPaxHotelCode.CssClass = "browse-button";
                    btnPaxTransportCode.CssClass = "browse-button";
                }
                else
                {
                    btnArriveFBO.CssClass = "browse-button-disabled";
                    btnCrewHotelCode.CssClass = "browse-button-disabled";
                    btnCrewTransportCode.CssClass = "browse-button-disabled";
                    btnDepartFBO.CssClass = "browse-button-disabled";
                    btnMaintCateringCode.CssClass = "browse-button-disabled";
                    btnMaintHotelCode.CssClass = "browse-button-disabled";
                    btnPaxHotelCode.CssClass = "browse-button-disabled";
                    btnPaxTransportCode.CssClass = "browse-button-disabled";
                }
            }
        }

        #endregion

        #region Quote Grid Events
        protected void Quote_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            List<CQMain> quoteList = new List<CQMain>();
                            quoteList = FileRequest.CQMains.Where(x => x.IsDeleted == false).OrderBy(x => x.QuoteNUM).ToList();
                            dgQuote.DataSource = quoteList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }
        protected void Quote_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            // Set Charter Quote Source Field in Grid
                            if (Item["CQSource"].Text == "1")
                                Item["CQSource"].Text = "V";
                            else if (Item["CQSource"].Text == "2")
                                Item["CQSource"].Text = "I";
                            else if (Item["CQSource"].Text == "3")
                                Item["CQSource"].Text = "T";
                            // Format Last Accepted Date Field in Grid
                            if (Item["LastAcceptDT"] != null && !string.IsNullOrEmpty(Item["LastAcceptDT"].Text) && Item["LastAcceptDT"].Text != "&nbsp;")
                                Item["LastAcceptDT"].Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Convert.ToDateTime(Item["LastAcceptDT"].Text));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }
        #endregion

        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditFile_Click(sender, e);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLogistics);
                }
            }
        }
    }
}
