﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class CharterQuoteRequest : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public CQFile FileRequest = new CQFile();
        private delegate void SaveSession();
        public CQMain QuoteInProgress;
        /// <summary>
        /// Wire up the SaveCharterQuoteToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.CharterQuote))
            {
                Response.Redirect("~/Views/Home.aspx?m=CharterQuote");
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveCharterQuoteFileSession = new SaveSession(SaveCharterQuoteToSession);
                        Master.SaveToSession = SaveCharterQuoteFileSession;
                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;

                        Master._DeleteClick += Delete_Click;
                        Master._SaveClick += Save_Click;
                        Master._CancelClick += Cancel_Click;

                        if (Session["SearchItemPrimaryKeyValue"] != null)
                        {
                            using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                            {
                                long FileID = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"].ToString());
                                var objRetVal = CqSvc.GetCQRequestByID(FileID, false);
                                if (objRetVal.ReturnFlag)
                                {
                                    CQFile FileReq = new CQFile();

                                    Session.Remove("CQFILE");
                                    Session.Remove("CQEXCEPTION");
                                    Session.Remove("CQQUOTEONFILEDICIMG");
                                    Session.Remove("CQCQQuoteOnFileBase64");

                                    FileReq = objRetVal.EntityList[0];
                                    FileReq.Mode = CQRequestActionMode.NoChange;
                                    FileReq.State = CQRequestEntityState.NoChange;

                                    if (FileReq.CQMains != null)
                                    {
                                        foreach (CQMain quote in FileReq.CQMains)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                {
                                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                    {
                                                        CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                        objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                        objImageFile.RecordType = "CQQuoteOnFile";
                                                        objImageFile.UWAFileName = fwh.UWAFileName;
                                                        objImageFile.RecordID = quote.CQMainID;
                                                        objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                        objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                        objImageFile.IsDeleted = false;
                                                        if (quote.ImageList == null)
                                                            quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                        quote.ImageList.Add(objImageFile);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    Session["CQFILE"] = FileReq;
                                }
                            }
                            Session.Remove("SearchItemPrimaryKeyValue");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        /// To Call methods during onload
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditFile, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                    if (!IsPostBack)
                    {
                        hdnCalculateFile.Value = string.Empty;
                        LoadCurrency();
                        BindCharterQuoteFileFromSession();
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    ApplyPermissions();
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        #region Permissions
        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.CharterQuote.ViewCQFile);
                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                // Check for Field Level Permissions
                if (IsAuthorized(Permission.CharterQuote.ViewCQFile))
                {
                    // Show read-only form
                    EnableForm(false);
                    // Check Main Page Controls
                    if (IsAuthorized(Permission.CharterQuote.AddCQFile) || IsAuthorized(Permission.CharterQuote.EditCQFile))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQFile))
                    {
                        ControlVisibility("Button", btnDelete, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.EditCQFile))
                    {
                        ControlVisibility("Button", btnDelete, true);
                    }
                    // Check Page Controls - Disable/Enable
                    if (Session[Master.CQSessionKey] != null)
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                        {
                            EnableForm(true);
                            if (IsAuthorized(Permission.CharterQuote.AddCQFile) || IsAuthorized(Permission.CharterQuote.EditCQFile))
                            {
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                            }
                            ControlEnable("Button", btnDelete, false, "button-disable");
                            ControlEnable("Button", btnEditFile, false, "button-disable");
                        }
                        else
                        {
                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDelete, true, "button");
                            ControlEnable("Button", btnEditFile, true, "button");
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                    }
                }
            }
        }
        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }
        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "LinkButton":
                        LinkButton lnkbtn = (LinkButton)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            lnkbtn.CssClass = cssClass;
                        lnkbtn.Enabled = isEnable;
                        break;
                    default: break;
                }
            }
        }
        /// <summary>
        /// To Disable and Enable Controls
        /// </summary>
        /// <param name="status"></param>
        private void EnableForm(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                if (FileRequest != null && FileRequest.Mode == CQRequestActionMode.Edit && FileRequest.CQFileID != 0)
                {
                    tbDepartDate.Enabled = false;
                }
                else
                {
                    tbDepartDate.Enabled = status;
                }
                tbQuoteDate.Enabled = status;
                tbHomebase.Enabled = status;
                ddlCurrency.Enabled = status;
                tbDescription.Enabled = status;
                tbCustomer.Enabled = status;
                tbCustomerName.Enabled = status;
                if (status == true && (!string.IsNullOrEmpty(tbCustomer.Text)) && (string.IsNullOrEmpty(lbcvCustomerName.Text)))
                {
                    tbContact.Enabled = status;
                    btnContact.Enabled = status;
                    if (status)
                        btnContact.CssClass = "browse-button";
                    else
                        btnContact.CssClass = "browse-button-disabled";
                }
                else
                {
                    tbContact.Enabled = false;
                    btnContact.Enabled = false;
                    btnContact.CssClass = "browse-button-disabled";
                }

                tbContactName.Enabled = status;
                tbPhone.Enabled = status;
                tbFax.Enabled = status;
                tbEmail.Enabled = status;
                tbSalesPerson.Enabled = status;
                tbLeadSource.Enabled = status;
                chkAppOnFile.Enabled = status;
                chkApproval.Enabled = status;
                //chkUseCustomFleetCharge.Enabled = status;
                tbCreditData.Enabled = false;
                tbDiscount.Enabled = status;
                tbLogNotes.Enabled = status;
                lbSalesPersonDesc.Enabled = status;
                lbleadSourc.Enabled = status;
                lbDiscount.Enabled = status;
                ///Other Buttons
                btnSave.Enabled = status;
                btnCancel.Enabled = status;
                btnDelete.Enabled = status;
                btnEditFile.Enabled = status;
                ///For Popup Buttons
                btnHomeBase.Enabled = status;
                btnSalesPerson.Enabled = status;
                btnLeadSource.Enabled = status;
                btnCustomer.Enabled = status;
                btnTypeCodeSearch.Enabled = status;

                if (status)
                {
                    btnHomeBase.CssClass = "browse-button";//enabled
                    btnSalesPerson.CssClass = "browse-button";
                    btnLeadSource.CssClass = "browse-button";
                    btnCustomer.CssClass = "browse-button";

                    if (!string.IsNullOrEmpty(tbCustomer.Text))
                        btnTypeCodeSearch.CssClass = "charter-rate-button";
                    else
                    {
                        btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                        btnTypeCodeSearch.Enabled = false;
                    }
                }
                else
                {
                    btnHomeBase.CssClass = "browse-button-disabled";//disabled
                    btnSalesPerson.CssClass = "browse-button-disabled";
                    btnLeadSource.CssClass = "browse-button-disabled";
                    btnCustomer.CssClass = "browse-button-disabled";
                    btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                }
            }
        }
        #endregion
        /// <summary>
        /// Save values to session
        /// </summary>
        protected void SaveCharterQuoteToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                    if (FileRequest != null && FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        if (FileRequest.CQFileID == 0)
                            FileRequest.State = CharterQuoteService.CQRequestEntityState.Added;
                        else
                            FileRequest.State = CharterQuoteService.CQRequestEntityState.Modified;
                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        if (!string.IsNullOrEmpty(tbDepartDate.Text))
                            FileRequest.EstDepartureDT = DateTime.ParseExact(tbDepartDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        else
                            FileRequest.EstDepartureDT = null;
                        if (!string.IsNullOrEmpty(tbQuoteDate.Text))
                            FileRequest.QuoteDT = DateTime.ParseExact(tbQuoteDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        else
                            FileRequest.QuoteDT = null;
                        CharterQuoteService.Company objHomebase = new CharterQuoteService.Company();
                        if (!string.IsNullOrWhiteSpace(tbHomebase.Text))
                        {
                            Int64 HomebaseID = 0;
                            if (Int64.TryParse(hdnHomeBaseID.Value, out HomebaseID))
                            {
                                objHomebase.HomebaseID = HomebaseID;
                                if (!string.IsNullOrEmpty(lbHomebase.Text))
                                    objHomebase.BaseDescription = System.Web.HttpUtility.HtmlDecode(lbHomebase.Text);
                                FileRequest.HomebaseID = HomebaseID;
                                FileRequest.HomeBaseAirportICAOID = tbHomebase.Text;
                                if (!string.IsNullOrEmpty(hdHomeBaseAirportID.Value))
                                    FileRequest.HomeBaseAirportID = Convert.ToInt64(hdHomeBaseAirportID.Value);
                                FileRequest.Company = objHomebase;
                            }
                        }
                        else
                        {
                            FileRequest.HomebaseID = null;
                            FileRequest.Company = null;
                        }
                        if (!string.IsNullOrEmpty(tbDescription.Text))
                            FileRequest.CQFileDescription = tbDescription.Text;
                        else
                            FileRequest.CQFileDescription = null;
                        //While binding get the CustomerID base on the cqCustomer
                        CharterQuoteService.CQCustomer objCQCustomer = new CharterQuoteService.CQCustomer();
                        if (!string.IsNullOrWhiteSpace(tbCustomerName.Text))
                        {
                            Int64 CustomerID = 0;
                            if (Int64.TryParse(hdnCustomerID.Value, out CustomerID))
                            {
                                objCQCustomer.CQCustomerID = Convert.ToInt64(hdnCustomerID.Value);
                                objCQCustomer.CQCustomerName = tbCustomerName.Text;
                                objCQCustomer.CQCustomerCD = tbCustomer.Text;
                                if (!string.IsNullOrEmpty(tbCreditData.Text))
                                    objCQCustomer.Credit = Convert.ToDecimal(tbCreditData.Text);
                                if (!string.IsNullOrEmpty(tbDiscount.Text))
                                    objCQCustomer.DiscountPercentage = Convert.ToDecimal(tbDiscount.Text);
                                objCQCustomer.IsApplicationFiled = chkAppOnFile.Checked;
                                objCQCustomer.IsApproved = chkApproval.Checked;
                                FileRequest.CQCustomer = objCQCustomer;
                                FileRequest.CQCustomerID = CustomerID;
                            }
                        }
                        else
                        {
                            FileRequest.CQCustomer = null;
                            FileRequest.CQCustomerID = null;
                        }

                        //While binding get the ConatctId base on the CqContact
                        CharterQuoteService.CQCustomerContact objCQContact = new CharterQuoteService.CQCustomerContact();
                        if (!string.IsNullOrWhiteSpace(tbContactName.Text))
                        {
                            Int64 ContactID = 0;
                            if (Int64.TryParse(hdnContactID.Value, out ContactID))
                            {
                                objCQContact.CQCustomerContactID = Convert.ToInt64(hdnContactID.Value);
                                objCQContact.CQCustomerContactID = ContactID;
                                if (!string.IsNullOrEmpty(tbContact.Text))
                                    objCQContact.CQCustomerContactCD = Convert.ToInt32(tbContact.Text);
                                objCQContact.ContactName = tbContactName.Text;
                                objCQContact.BusinessPhoneNum = tbPhone.Text;
                                objCQContact.BusinessFaxNum = tbFax.Text;
                                objCQContact.BusinessEmailID = tbEmail.Text;
                                FileRequest.CQCustomerContact = objCQContact;
                                FileRequest.CQCustomerContactID = ContactID;
                                FileRequest.CQCustomerContactName = tbContactName.Text;
                            }
                        }
                        else
                        {
                            FileRequest.CQCustomerContact = null;
                            FileRequest.CQCustomerContactID = null;
                        }
                        //While binding get the SalesPerson base on the salesperson
                        CharterQuoteService.SalesPerson CqSalesPerson = new CharterQuoteService.SalesPerson();
                        if (!string.IsNullOrWhiteSpace(tbSalesPerson.Text))
                        {
                            Int64 SalesID = 0;
                            if (Int64.TryParse(hdnSalesPerson.Value, out SalesID))
                            {
                                CqSalesPerson.SalesPersonID = Convert.ToInt64(hdnSalesPerson.Value);
                                CqSalesPerson.SalesPersonCD = tbSalesPerson.Text;
                                CqSalesPerson.FirstName = hdnSPFName.Value;
                                CqSalesPerson.LastName = hdnSPLName.Value;
                                CqSalesPerson.MiddleName = hdnSPMName.Value;
                                CqSalesPerson.PhoneNUM = hdnSPBusinessPhone.Value; //Fix for SUP-381 (3194)
                                CqSalesPerson.EmailAddress = hdnSPBusinessEmail.Value; //Fix for SUP-381 (3194)
                                FileRequest.SalesPersonID = SalesID;
                                FileRequest.SalesPerson = CqSalesPerson;
                            }
                        }
                        else
                        {
                            FileRequest.SalesPerson = null;
                            FileRequest.SalesPersonID = null;
                        }
                        //While binding get the LeadSource base on the LeadSource
                        CharterQuoteService.LeadSource CqLeadSource = new CharterQuoteService.LeadSource();
                        if (!string.IsNullOrWhiteSpace(tbLeadSource.Text))
                        {
                            Int64 LeadSourceID = 0;
                            if (Int64.TryParse(hdnLeadSource.Value, out LeadSourceID))
                            {
                                CqLeadSource.LeadSourceID = Convert.ToInt64(hdnLeadSource.Value);
                                CqLeadSource.LeadSourceCD = tbLeadSource.Text;
                                CqLeadSource.LeadSourceDescription = System.Web.HttpUtility.HtmlDecode(lbleadSourc.Text);
                                FileRequest.LeadSourceID = LeadSourceID;
                                FileRequest.LeadSource = CqLeadSource;
                            }
                        }
                        else
                        {
                            FileRequest.LeadSource = null;
                            FileRequest.LeadSourceID = null;
                        }
                        //While binding get the Exchangeid base on the exchangerate
                        CharterQuoteService.ExchangeRate CqExchangeRate = new CharterQuoteService.ExchangeRate();
                        if (!string.IsNullOrWhiteSpace(ddlCurrency.SelectedValue))
                        {
                            Int64 ExchangeID = 0;
                            if (Int64.TryParse(hdnExchangeRateID.Value, out ExchangeID))
                            {
                                CqExchangeRate.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                                CqExchangeRate.ExchRate = Convert.ToDecimal(ddlCurrency.SelectedValue);
                                CqExchangeRate.ExchRateCD = ddlCurrency.SelectedItem.ToString();
                                CqExchangeRate.ExchRateDescription = " ";
                                FileRequest.ExchangeRateID = ExchangeID;
                                FileRequest.ExchangeRate1 = CqExchangeRate;
                                FileRequest.ExchangeRate = CqExchangeRate.ExchRate;
                            }
                        }
                        else
                        {
                            FileRequest.ExchangeRate1 = null;
                            FileRequest.ExchangeRateID = null;
                        }
                        FileRequest.CQCustomerName = null;
                        if (!string.IsNullOrEmpty(tbCustomerName.Text))
                            FileRequest.CQCustomerName = tbCustomerName.Text;
                        else
                            FileRequest.CQCustomerName = null;
                        //FileRequest.CQCustomerContactName = null;
                        //if (!string.IsNullOrEmpty(tbContactName.Text))
                        //    FileRequest.CQCustomerContactName = tbContactName.Text;
                        //else
                        //    FileRequest.CQCustomerContactName = null;
                        FileRequest.PhoneNUM = null;
                        if (!string.IsNullOrEmpty(tbPhone.Text))
                            FileRequest.PhoneNUM = tbPhone.Text;
                        else
                            FileRequest.PhoneNUM = null;
                        FileRequest.FaxNUM = null;
                        if (!string.IsNullOrEmpty(tbFax.Text))
                            FileRequest.FaxNUM = tbFax.Text;
                        else
                            FileRequest.FaxNUM = null;
                        FileRequest.EmailID = null;
                        if (!string.IsNullOrEmpty(tbEmail.Text))
                            FileRequest.EmailID = tbEmail.Text;
                        else
                            FileRequest.EmailID = null;
                        if (chkAppOnFile.Checked)
                            FileRequest.IsApplicationFiled = true;
                        else
                            FileRequest.IsApplicationFiled = false;
                        if (chkApproval.Checked)
                            FileRequest.IsApproved = true;
                        else
                            FileRequest.IsApproved = false;
                        FileRequest.DiscountPercentage = 0;
                        if (!string.IsNullOrEmpty(tbDiscount.Text))
                            FileRequest.DiscountPercentage = Convert.ToDecimal(tbDiscount.Text);
                        else
                            FileRequest.DiscountPercentage = null;
                        FileRequest.Credit = 0;
                        if (!string.IsNullOrEmpty(tbCreditData.Text))
                            FileRequest.Credit = Convert.ToDecimal(tbCreditData.Text);
                        else
                            FileRequest.Credit = null;
                        FileRequest.Notes = null;
                        if (!string.IsNullOrEmpty(tbLogNotes.Text))
                            FileRequest.Notes = tbLogNotes.Text;
                        else
                            FileRequest.Notes = null;
                        FileRequest.ExpressQuote = false;

                        FileRequest.UseCustomFleetCharge = chkUseCustomFleetCharge.Checked;
                    }
                    Session[Master.CQSessionKey] = FileRequest;

                    if (hdnCalculateFile.Value == "true")
                        Master.DoFileCalculation();
                }
            }
        }
        /// <summary>
        /// Bind values to fields from session
        /// </summary>
        protected void BindCharterQuoteFileFromSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                    ClearFields();
                    if (FileRequest != null)
                    {
                        if (FileRequest.EstDepartureDT != DateTime.MinValue && FileRequest.EstDepartureDT != null)
                            tbDepartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", FileRequest.EstDepartureDT);
                        else
                            tbDepartDate.Text = string.Empty;
                        if (FileRequest.QuoteDT != DateTime.MinValue && FileRequest.QuoteDT != null)
                            tbQuoteDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", FileRequest.QuoteDT);
                        else
                            tbQuoteDate.Text = string.Empty;
                        ///Company Details
                        if (FileRequest.Company != null)
                        {
                            if (FileRequest.HomebaseID > 0 && string.IsNullOrEmpty(FileRequest.HomeBaseAirportICAOID))
                            {
                                FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId TripCompany = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                                TripCompany = Master.GetCompany((long)FileRequest.HomebaseID);
                                if (TripCompany != null)
                                {
                                    FileRequest.HomeBaseAirportICAOID = TripCompany.HomebaseCD;
                                    FileRequest.HomeBaseAirportID = (long)TripCompany.HomebaseAirportID;
                                }
                            }
                            if (FileRequest.Company.BaseDescription != null)
                                lbHomebase.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.Company.BaseDescription.ToUpper());

                            hdnHomeBaseID.Value = FileRequest.HomebaseID.ToString();
                            tbHomebase.Text = FileRequest.HomeBaseAirportICAOID;
                            hdHomeBaseAirportID.Value = FileRequest.HomeBaseAirportID.ToString();
                        }
                        tbDescription.Text = FileRequest.CQFileDescription;
                        if (FileRequest.CQCustomer != null)
                        {
                            hdnCustomerID.Value = FileRequest.CQCustomer.CQCustomerID.ToString();
                            tbCustomer.Text = FileRequest.CQCustomer.CQCustomerCD;
                            tbCustomerName.Text = FileRequest.CQCustomer.CQCustomerName;
                            if (FileRequest.CQCustomer.Credit != null)
                                tbCreditData.Text = FileRequest.CQCustomer.Credit.ToString();
                        }

                        if (!string.IsNullOrEmpty(tbCustomer.Text))
                        {
                            btnTypeCodeSearch.Enabled = true;
                            btnTypeCodeSearch.CssClass = "charter-rate-button";
                        }
                        else
                        {
                            btnTypeCodeSearch.Enabled = false;
                            btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                        }

                        if (FileRequest.CQCustomerContact != null)
                        {
                            hdnContactID.Value = FileRequest.CQCustomerContact.CQCustomerContactID.ToString();
                            if (FileRequest.CQCustomerContact.CQCustomerContactCD != null)
                                tbContact.Text = FileRequest.CQCustomerContact.CQCustomerContactCD.ToString();
                            if (FileRequest.CQCustomerContact.ContactName != null)
                                tbContactName.Text = FileRequest.CQCustomerContact.ContactName;
                            if (FileRequest.CQCustomerContact.BusinessPhoneNum != null)
                                tbPhone.Text = FileRequest.CQCustomerContact.BusinessPhoneNum;
                            if (FileRequest.CQCustomerContact.BusinessFaxNum != null)
                                tbFax.Text = FileRequest.CQCustomerContact.BusinessFaxNum;
                            if (FileRequest.CQCustomerContact.BusinessEmailID != null)
                                tbEmail.Text = FileRequest.CQCustomerContact.BusinessEmailID;
                        }

                        if (FileRequest.SalesPerson != null)
                        {
                            hdnSalesPerson.Value = FileRequest.SalesPerson.SalesPersonID.ToString();
                            tbSalesPerson.Text = FileRequest.SalesPerson.SalesPersonCD.ToString();
                            hdnSPFName.Value = FileRequest.SalesPerson.FirstName != null ? FileRequest.SalesPerson.FirstName.ToString() : string.Empty;
                            hdnSPLName.Value = FileRequest.SalesPerson.LastName != null ? FileRequest.SalesPerson.LastName.ToString() : string.Empty;
                            hdnSPMName.Value = FileRequest.SalesPerson.MiddleName != null ? FileRequest.SalesPerson.MiddleName.ToString() : string.Empty;
                            hdnSPBusinessEmail.Value = FileRequest.SalesPerson.PhoneNUM != null ? FileRequest.SalesPerson.PhoneNUM.ToString() : string.Empty;
                            hdnSPBusinessPhone.Value = FileRequest.SalesPerson.EmailAddress != null ? FileRequest.SalesPerson.EmailAddress.ToString() : string.Empty;

                            string firstname = string.Empty;
                            string lastname = string.Empty;
                            string middlename = string.Empty;
                            string businessemail = string.Empty;
                            string businessphone = string.Empty;
                            string salesPersonName = string.Empty;

                            if (FileRequest.SalesPerson.FirstName != null)
                                firstname = FileRequest.SalesPerson.FirstName != null ? ", " + FileRequest.SalesPerson.FirstName.ToString() : string.Empty;
                            if (FileRequest.SalesPerson.LastName != null)
                                lastname = FileRequest.SalesPerson.LastName != null ? FileRequest.SalesPerson.LastName.ToString() : string.Empty;
                            if (FileRequest.SalesPerson.MiddleName != null)
                                middlename = FileRequest.SalesPerson.MiddleName != null ? " " + FileRequest.SalesPerson.MiddleName.ToString() : string.Empty;

                            salesPersonName = lastname + firstname + middlename;

                            if (FileRequest.SalesPerson.PhoneNUM != null)
                                businessphone = "Business Phone : " + FileRequest.SalesPerson.PhoneNUM;

                            if (FileRequest.SalesPerson.EmailAddress != null)
                                businessemail = "Business E-Mail : " + FileRequest.SalesPerson.EmailAddress;

                            lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(salesPersonName.ToUpper());

                            //Fix for SUP-381 (3194)
                            tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(businessphone + "\n" + businessemail);
                        }
                        if (FileRequest.LeadSource != null)
                        {
                            hdnLeadSource.Value = FileRequest.LeadSource.LeadSourceID.ToString();
                            tbLeadSource.Text = FileRequest.LeadSource.LeadSourceCD;
                            if (FileRequest.LeadSource.LeadSourceDescription != null)
                                lbleadSourc.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.LeadSource.LeadSourceDescription.ToString().ToUpper());
                        }
                        if (FileRequest.ExchangeRate1 != null)
                        {
                            if (FileRequest.ExchangeRate1.ExchangeRateID != null)
                                hdnExchangeRateID.Value = FileRequest.ExchangeRate1.ExchangeRateID.ToString();
                            if (ddlCurrency.Items.Count > 0)
                                ddlCurrency.SelectedValue = FileRequest.ExchangeRate1.ExchRate.ToString();
                        }
                        if (FileRequest.CQCustomerName != null)
                        {
                            tbCustomerName.Text = FileRequest.CQCustomerName;
                        }
                        if (FileRequest.CQCustomerContactName != null)
                        {
                            tbContactName.Text = FileRequest.CQCustomerContactName;
                        }
                        //if (FileRequest.PhoneNUM != null)
                        //{
                        //    tbPhone.Text = FileRequest.PhoneNUM;
                        //}
                        //if (FileRequest.FaxNUM != null)
                        //{
                        //    tbFax.Text = FileRequest.FaxNUM;
                        //}
                        //if (FileRequest.EmailID != null)
                        //{
                        //    tbEmail.Text = FileRequest.EmailID;
                        //}

                        if (FileRequest.IsApplicationFiled != null)
                        {
                            chkAppOnFile.Checked = (bool)FileRequest.IsApplicationFiled;
                        }
                        if (FileRequest.IsApproved != null)
                        {
                            chkApproval.Checked = (bool)FileRequest.IsApproved;
                        }
                        if (FileRequest.Credit != null)
                            tbCreditData.Text = FileRequest.Credit.ToString();
                        if (FileRequest.DiscountPercentage != null)
                            tbDiscount.Text = FileRequest.DiscountPercentage.ToString();

                        if (FileRequest.Notes != null && !string.IsNullOrEmpty(FileRequest.Notes))
                            //lblAlert.ForeColor = System.Drawing.Color.Red;
                            lblAlert.CssClass = "alert-text-bold";
                        else
                            lblAlert.CssClass = "text";
                        //lblAlert.ForeColor = System.Drawing.Color.Black;

                        tbLogNotes.Text = FileRequest.Notes;

                        if (FileRequest.UseCustomFleetCharge != null)
                            chkUseCustomFleetCharge.Checked = (bool)FileRequest.UseCustomFleetCharge;

                        if (!string.IsNullOrEmpty(hdnCustomerID.Value))
                        {
                            List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == Convert.ToInt64(hdnCustomerID.Value)).ToList();

                                if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                                {
                                    if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified))
                                        chkUseCustomFleetCharge.Enabled = true;
                                    else
                                        chkUseCustomFleetCharge.Enabled = false;
                                }
                                else
                                    chkUseCustomFleetCharge.Enabled = false;
                            }
                        }
                        else
                            chkUseCustomFleetCharge.Enabled = false;
                    }
                }
                else
                {
                    EnableForm(false);
                    chkUseCustomFleetCharge.Enabled = false;
                }
            }
        }
        /// <summary>
        /// To Load Currency from ExchangeRate
        /// </summary>
        private void LoadCurrency()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = objMasterService.GetExchangeRate().EntityList.ToList();
                            if (RetValue != null && RetValue.Count() > 0)
                            {
                                ddlCurrency.DataSource = RetValue;
                                ddlCurrency.DataTextField = "ExchRateCD";
                                ddlCurrency.DataValueField = "ExchRate";
                                ddlCurrency.DataBind();
                                ddlCurrency.Items.Insert(0, String.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        #region Quote Grid Events
        protected void Quote_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            List<CQMain> quoteList = new List<CQMain>();
                            quoteList = FileRequest.CQMains.Where(x => x.IsDeleted == false).OrderBy(x => x.QuoteNUM).ToList();
                            dgQuote.DataSource = quoteList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void Quote_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            // Set Charter Quote Source Field in Grid
                            if (Item["CQSource"].Text == "1")
                                Item["CQSource"].Text = "V";
                            else if (Item["CQSource"].Text == "2")
                                Item["CQSource"].Text = "I";
                            else if (Item["CQSource"].Text == "3")
                                Item["CQSource"].Text = "T";
                            // Format Last Accepted Date Field in Grid
                            if (Item["LastAcceptDT"] != null && !string.IsNullOrEmpty(Item["LastAcceptDT"].Text) && Item["LastAcceptDT"].Text != "&nbsp;")
                                Item["LastAcceptDT"].Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Convert.ToDateTime(Item["LastAcceptDT"].Text));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        #endregion
        /// <summary>
        /// To Clear Fields
        /// </summary>
        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbDepartDate.Text = string.Empty;
                tbQuoteDate.Text = string.Empty;
                tbHomebase.Text = string.Empty;
                if (ddlCurrency.Items.Count > 0)
                    ddlCurrency.SelectedIndex = 0;
                tbDescription.Text = string.Empty;
                tbCustomer.Text = string.Empty;
                tbCustomerName.Text = string.Empty;
                tbContact.Text = string.Empty;
                tbContactName.Text = string.Empty;
                tbPhone.Text = string.Empty;
                tbFax.Text = string.Empty;
                tbEmail.Text = string.Empty;
                tbSalesPerson.Text = string.Empty;
                tbLeadSource.Text = string.Empty;
                chkAppOnFile.Checked = false;
                chkApproval.Checked = false;
                tbDiscount.Text = string.Empty;
                tbLogNotes.Text = string.Empty;
                lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbleadSourc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                hdnSPFName.Value = string.Empty;
                hdnSPLName.Value = string.Empty;
                hdnSPMName.Value = string.Empty;

                //Fix for SUP-381 (3194)
                tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                hdnSPBusinessPhone.Value = string.Empty;
                hdnSPBusinessEmail.Value = string.Empty;
            }
        }
        #region "Events"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.ToString() == "tbCustomer_TextChanged")
                tbCustomer_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbContact_TextChanged")
                tbContact_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbSalesPerson_TextChanged")
                tbSalesPerson_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbLeadSource_TextChanged")
                tbLeadSource_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbHomeBase_TextChanged")
                tbHomeBase_TextChanged(sender, e);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string ExchangeCd = ddlCurrency.SelectedItem.ToString();
                        if (!string.IsNullOrEmpty(ExchangeCd))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                CharterQuoteService.ExchangeRate CqExchangeRate = new CharterQuoteService.ExchangeRate();
                                var objRetVal = objMastersvc.GetExchangeRate();
                                List<FlightPakMasterService.ExchangeRate> ExchangeRateList = new List<FlightPakMasterService.ExchangeRate>();
                                if (objRetVal.ReturnFlag)
                                {
                                    ExchangeRateList = objRetVal.EntityList.Where(x => x.ExchRateCD.ToUpper().Trim() == ExchangeCd.ToUpper().Trim()).ToList();
                                    if (ExchangeRateList != null && ExchangeRateList.Count > 0)
                                    {
                                        hdnExchangeRateID.Value = ExchangeRateList[0].ExchangeRateID.ToString();
                                        //CqExchangeRate.ExchangeRateID = ExchangeRateList[0].ExchangeRateID;
                                        //CqExchangeRate.ExchRate = Convert.ToDecimal(ddlCurrency.SelectedValue);
                                        //CqExchangeRate.ExchRateCD = ddlCurrency.SelectedItem.ToString();
                                        //FileRequest.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                                        //FileRequest.ExchangeRate1 = CqExchangeRate;
                                        //FileRequest.ExchangeRate = CqExchangeRate.ExchRate;
                                    }
                                }
                            }
                        }
                        else
                        {
                            FileRequest.ExchangeRate = null;
                            hdnExchangeRateID.Value = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        #endregion
        #region MastercatalogPopup Text Box Change events

        /// <summary>
        /// To Validate HomeBase Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnHomeBase);
                        hdnHomeBaseID.Value = string.Empty;
                        lbHomebase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        if (!string.IsNullOrEmpty(tbHomebase.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                var objRetValue = objDstsvc.GetAirportByAirportICaoID(tbHomebase.Text.ToUpper().Trim());
                                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<FlightPakMasterService.GetAllAirport>();
                                if (objRetVal.ReturnFlag)
                                {
                                    CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomebase.Text.ToUpper().Trim())).ToList();
                                    if (CompanyList != null && CompanyList.Count > 0)
                                    {
                                        hdnCalculateFile.Value = "true";

                                        if (CompanyList[0].BaseDescription != null)
                                            lbHomebase.Text = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription.ToUpper());
                                        hdnHomeBaseID.Value = CompanyList[0].HomebaseID.ToString();
                                        tbHomebase.Text = CompanyList[0].HomebaseCD;
                                        hdHomeBaseAirportID.Value = CompanyList[0].HomebaseAirportID.ToString();

                                        #region Tooltip for AirportDetails
                                        if (objRetValue.ReturnFlag)
                                        {
                                            getAllAirportList = objRetValue.EntityList;
                                            if (getAllAirportList[0] != null && getAllAirportList.Count > 0)
                                            {
                                                string builder = string.Empty;
                                                builder = "ICAO : " + (getAllAirportList[0].IcaoID != null ? getAllAirportList[0].IcaoID : string.Empty)
                                                    + "\n" + "City : " + (getAllAirportList[0].CityName != null ? getAllAirportList[0].CityName : string.Empty)
                                                    + "\n" + "State/Province : " + (getAllAirportList[0].StateName != null ? getAllAirportList[0].StateName : string.Empty)
                                                    + "\n" + "Country : " + (getAllAirportList[0].CountryName != null ? getAllAirportList[0].CountryName : string.Empty)
                                                    + "\n" + "Airport : " + (getAllAirportList[0].AirportName != null ? getAllAirportList[0].AirportName : string.Empty)
                                                    + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD != null ? getAllAirportList[0].DSTRegionCD : string.Empty)
                                                    + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                                    + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                                    + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);
                                                //lbArriveIcao.ToolTip = builder;
                                                lbHomebase.ToolTip = builder;
                                            }
                                        }
                                        #endregion
                                        Master.DoLegCalculationsForDomIntlTaxable();
                                    }
                                    else
                                    {
                                        lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomebase);
                                    }
                                }
                                else
                                {
                                    lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbHomebase);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void DepartDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!Master.IsValidDate(tbDepartDate.Text))
                        {
                            tbDepartDate.Text = string.Empty;

                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                if (FileRequest != null && FileRequest.EstDepartureDT != null)
                                {
                                    DateTime dat = (DateTime)FileRequest.EstDepartureDT;
                                    tbDepartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dat);
                                }
                            }

                            Master.RadAjaxManagerMaster.FocusControl(tbDepartDate.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }
        protected void QuoteDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!Master.IsValidDate(tbQuoteDate.Text))
                        {
                            tbQuoteDate.Text = string.Empty;
                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                if (FileRequest != null && FileRequest.QuoteDT != null)
                                {
                                    DateTime dat = (DateTime)FileRequest.QuoteDT;
                                    tbQuoteDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dat);
                                }
                            }
                            Master.RadAjaxManagerMaster.FocusControl(tbQuoteDate.ClientID);
                        }

                        //if (!string.IsNullOrEmpty(tbQuoteDate.Text))
                        //{
                        //    DateTime Departdate = DateTime.ParseExact(tbQuoteDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        //    if (Departdate.Year < 1900 || Departdate.Year > 2100)
                        //    {
                        //        tbQuoteDate.Text = string.Empty;
                        //        RadWindowManager1.RadAlert("Please enter/select Date between 01/01/1900 and 12/31/2100", 250, 100, ModuleNameConstants.CharterQuote.CQFile, "callBackQuoteDate");
                        //    }
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }
        /// <summary>
        /// To validate Charterquote customer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCustomer_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvCustomerName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbCreditData.Text = string.Empty;
                        tbContact.Text = string.Empty;
                        tbContactName.Text = string.Empty;
                        tbPhone.Text = string.Empty;
                        tbFax.Text = string.Empty;
                        tbEmail.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbCustomer.Text))
                        {
                            tbContact.Enabled = true;
                            btnContact.Enabled = true;
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPak.Web.FlightPakMasterService.CQCustomer();
                                objCQCustomer.CQCustomerCD = tbCustomer.Text.Trim();
                                var RetValue = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvCustomerName.Text = System.Web.HttpUtility.HtmlEncode("Invalid Customer Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomer);
                                    tbContact.Enabled = false;
                                    btnContact.Enabled = false;
                                    hdnCustomerID.Value = string.Empty;
                                    hdnContactID.Value = string.Empty;
                                    //tbCustomer.Text = string.Empty;
                                    tbCustomerName.Text = string.Empty;
                                    tbContact.Text = string.Empty;
                                    tbContactName.Text = string.Empty;
                                    tbPhone.Text = string.Empty;
                                    tbFax.Text = string.Empty;
                                    tbEmail.Text = string.Empty;
                                    chkAppOnFile.Checked = false;
                                    chkApproval.Checked = false;
                                    tbDiscount.Text = string.Empty;
                                }
                                else
                                {
                                    FlightPak.Web.FlightPakMasterService.CQCustomerContact objCQCustomerContact = new FlightPak.Web.FlightPakMasterService.CQCustomerContact();
                                    if (RetValue != null && RetValue.Count > 0)
                                    {
                                        hdnCalculateFile.Value = "true";

                                        objCQCustomerContact.CQCustomerID = RetValue[0].CQCustomerID;
                                        hdnCustomerID.Value = RetValue[0].CQCustomerID.ToString();
                                        //tbCustomer.Text = RetValue[0].CQCustomerCD.ToString();
                                        tbCustomerName.Text = RetValue[0].CQCustomerName;
                                        if (RetValue[0].Credit != null)
                                            tbCreditData.Text = RetValue[0].Credit.ToString();
                                        if (RetValue[0].IsApplicationFiled != null)
                                            chkAppOnFile.Checked = (bool)RetValue[0].IsApplicationFiled;
                                        if (RetValue[0].IsApproved != null)
                                            chkApproval.Checked = (bool)RetValue[0].IsApproved;
                                        if (RetValue[0].DiscountPercentage != null)
                                            tbDiscount.Text = RetValue[0].DiscountPercentage.ToString();

                                        // To check if the customer has New Fleet Charter Rate
                                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();
                                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == RetValue[0].CQCustomerID).ToList();

                                            if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                                                chkUseCustomFleetCharge.Enabled = true;
                                            else
                                            {
                                                chkUseCustomFleetCharge.Enabled = false;
                                                chkUseCustomFleetCharge.Checked = false;
                                            }
                                        }
                                    }
                                    var ReturnValue = objMasterService.GetCQCustomerContactByCQCustomerID(objCQCustomerContact).EntityList;
                                    if (ReturnValue.Count() != null && ReturnValue.Count() > 0)
                                    {
                                        List<GetCQCustomerContactByCQCustomerID> choicecqCustomerContactList = new List<GetCQCustomerContactByCQCustomerID>();
                                        choicecqCustomerContactList = ReturnValue.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                        if (choicecqCustomerContactList != null && choicecqCustomerContactList.Count > 0)
                                        {
                                            hdnCustomerID.Value = choicecqCustomerContactList[0].CQCustomerID.ToString();
                                            hdnContactID.Value = choicecqCustomerContactList[0].CQCustomerContactID.ToString();
                                            tbContact.Text = choicecqCustomerContactList[0].CQCustomerContactCD.ToString();
                                            //Fix for #2935
                                            if (!string.IsNullOrEmpty(choicecqCustomerContactList[0].FirstName) && !string.IsNullOrEmpty(choicecqCustomerContactList[0].LastName))
                                                tbContactName.Text = string.Format("{0} {1}", choicecqCustomerContactList[0].FirstName, choicecqCustomerContactList[0].LastName);
                                            else
                                                tbContactName.Text = choicecqCustomerContactList[0].FirstName;

                                            if (choicecqCustomerContactList[0].BusinessPhoneNum != null && choicecqCustomerContactList[0].BusinessPhoneNum != "&nbsp;")
                                                tbPhone.Text = choicecqCustomerContactList[0].BusinessPhoneNum;
                                            if (choicecqCustomerContactList[0].BusinessFaxNum != null && choicecqCustomerContactList[0].BusinessFaxNum != "&nbsp;")
                                                tbFax.Text = choicecqCustomerContactList[0].BusinessFaxNum;
                                            if (choicecqCustomerContactList[0].BusinessEmailID != null && choicecqCustomerContactList[0].BusinessEmailID != "&nbsp;")
                                                tbEmail.Text = choicecqCustomerContactList[0].BusinessEmailID;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //tbCustomer.Text = string.Empty;
                            tbCustomerName.Text = string.Empty;
                            tbContact.Enabled = false;
                            btnContact.Enabled = false;
                            tbContact.Text = string.Empty;
                            tbContactName.Text = string.Empty;
                            chkAppOnFile.Checked = false;
                            chkApproval.Checked = false;
                            tbDiscount.Text = string.Empty;
                            tbPhone.Text = string.Empty;
                            tbFax.Text = string.Empty;
                            tbEmail.Text = string.Empty;
                        }

                        if (!string.IsNullOrEmpty(tbCustomer.Text))
                        {
                            btnTypeCodeSearch.Enabled = true;
                            btnTypeCodeSearch.CssClass = "charter-rate-button";
                        }
                        else
                        {
                            btnTypeCodeSearch.Enabled = false;
                            btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        /// To validate Charterquote Contact Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbContact_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbContactName.Text = string.Empty;
                        tbPhone.Text = string.Empty;
                        tbFax.Text = string.Empty;
                        tbEmail.Text = string.Empty;
                        lbcvContact.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        if (!string.IsNullOrEmpty(tbContact.Text))
                        {
                            Int32 contactID = 0;
                            if (Int32.TryParse(tbContact.Text, out contactID))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ReturnValue = objMasterService.GetCQCustomerContactList().EntityList.Where(x => x.CQCustomerID == Convert.ToInt64(hdnCustomerID.Value) && x.IsDeleted == false && x.CQCustomerContactCD == Convert.ToInt32(tbContact.Text)).ToList();
                                    if (ReturnValue != null && ReturnValue.Count > 0)
                                    {
                                        hdnContactID.Value = ReturnValue[0].CQCustomerContactID.ToString();
                                        //Fix for #2935
                                        if (!string.IsNullOrEmpty(ReturnValue[0].FirstName) && !string.IsNullOrEmpty(ReturnValue[0].LastName))
                                            tbContactName.Text = string.Format("{0} {1}", ReturnValue[0].FirstName, ReturnValue[0].LastName);
                                        else
                                            tbContactName.Text = ReturnValue[0].FirstName;

                                        if (ReturnValue[0].BusinessPhoneNum != null && ReturnValue[0].BusinessPhoneNum != "&nbsp;")
                                            tbPhone.Text = ReturnValue[0].BusinessPhoneNum;
                                        if (ReturnValue[0].BusinessFaxNum != null && ReturnValue[0].BusinessFaxNum != "&nbsp;")
                                            tbFax.Text = ReturnValue[0].BusinessFaxNum;
                                        if (ReturnValue[0].BusinessEmailID != null && ReturnValue[0].BusinessEmailID != "&nbsp;")
                                            tbEmail.Text = ReturnValue[0].BusinessEmailID;
                                    }
                                    else
                                    {
                                        lbcvContact.Text = System.Web.HttpUtility.HtmlEncode("Invalid Contact Code.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbContact);
                                    }
                                }
                            }
                            else
                            {
                                lbcvContact.Text = System.Web.HttpUtility.HtmlEncode("Invalid Contact Code.");
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbContact);
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        /// To Validate SalesPerson Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbSalesPerson_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSalesPerson.Value = string.Empty;
                        lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        string firstname = string.Empty;
                        string lastname = string.Empty;
                        string middlename = string.Empty;
                        string businessphone = string.Empty;
                        string businessemail = string.Empty;
                        string salesPersonName = string.Empty;
                        string salesPersonTooltip = string.Empty;

                        if (!string.IsNullOrEmpty(tbSalesPerson.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetSalesPersonList().EntityList.Where(x => x.SalesPersonCD != null && x.SalesPersonCD.Trim().ToUpper().Equals(tbSalesPerson.Text.ToString().ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode("Invalid Sales Person Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbSalesPerson);
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                    SalesPersonList = (List<FlightPakMasterService.GetSalesPerson>)RetValue.ToList();

                                    if (SalesPersonList != null && SalesPersonList.Count > 0)
                                    {
                                        hdnSalesPerson.Value = SalesPersonList[0].SalesPersonID.ToString();

                                        if (!string.IsNullOrEmpty(SalesPersonList[0].FirstName))
                                        {
                                            hdnSPFName.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].FirstName.ToString());
                                            firstname = ", " + System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].FirstName.ToString());
                                        }

                                        if (!string.IsNullOrEmpty(SalesPersonList[0].LastName))
                                        {
                                            hdnSPLName.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].LastName.ToString());
                                            lastname = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].LastName.ToString());
                                        }

                                        if (!string.IsNullOrEmpty(SalesPersonList[0].MiddleName))
                                        {
                                            hdnSPMName.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].MiddleName.ToString());
                                            middlename = " " + System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].MiddleName.ToString());
                                        }

                                        if (!string.IsNullOrEmpty(SalesPersonList[0].PhoneNUM))
                                        {
                                            hdnSPBusinessPhone.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].PhoneNUM.ToString());
                                            businessphone = "Business Phone : " + System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].PhoneNUM.ToString());
                                        }


                                        if (!string.IsNullOrEmpty(SalesPersonList[0].EmailAddress))
                                        {
                                            hdnSPBusinessEmail.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].EmailAddress.ToString());
                                            businessemail = "Business E-Mail : " + System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].EmailAddress.ToString());
                                        }

                                        salesPersonName = lastname + firstname + middlename;
                                        lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(salesPersonName.ToUpper());

                                        //Fix for SUP-381 (3194)
                                        tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(businessphone + "\n" + businessemail);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        ///  To Validate LeadSource Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbLeadSource_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnLeadSource.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbleadSourc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbLeadSource.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetLeadSourceList().EntityList.Where(x => x.LeadSourceCD != null && x.LeadSourceCD.Trim().ToUpper().Equals(tbLeadSource.Text.ToString().Replace("amp;", "").ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode("Invalid Lead Source Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbLeadSource);
                                }
                                else
                                {
                                    List<FlightPakMasterService.LeadSource> LeadSourceList = new List<FlightPakMasterService.LeadSource>();
                                    LeadSourceList = (List<FlightPakMasterService.LeadSource>)RetValue.ToList();
                                    tbLeadSource.Text = LeadSourceList[0].LeadSourceCD.ToString();
                                    hdnLeadSource.Value = LeadSourceList[0].LeadSourceID.ToString();
                                    if (LeadSourceList[0].LeadSourceDescription != null)
                                        lbleadSourc.Text = System.Web.HttpUtility.HtmlEncode(LeadSourceList[0].LeadSourceDescription.ToString().ToUpper());
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void Discount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDiscount.Text))
                        {
                            bool correctFormat = true;
                            int count = 0;
                            string discount = tbDiscount.Text;
                            for (int i = 0; i < discount.Length; ++i)
                            {
                                if (discount[i].ToString() == ".")
                                    count++;
                            }
                            if (count > 1)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQFile, null);
                                tbDiscount.Text = "00.00";
                                correctFormat = false;
                            }
                            if (discount.IndexOf(".") >= 0)
                            {
                                string[] strarr = discount.Split('.');
                                if (strarr[0].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQFile, null);
                                    tbDiscount.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (strarr[1].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQFile, null);
                                    tbDiscount.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[0].Length == 0)
                                        discount = "00" + discount;
                                    tbDiscount.Text = discount;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[1].Length == 0)
                                        discount = discount + "00";
                                    if (strarr[1].Length == 1)
                                        discount = discount + "0";
                                    tbDiscount.Text = discount;
                                }
                            }
                            else
                            {
                                if (discount.Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQFile, null);
                                    tbDiscount.Text = "00.00";
                                    correctFormat = false;
                                }
                                else
                                {
                                    tbDiscount.Text = discount + ".00";
                                    hdnCalculateFile.Value = "true";
                                }
                            }
                        }
                        //hdnCalculateFile.Value = "true";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }


        #endregion
        #region Button Events
        /// <summary>
        /// To delete the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        Master.Delete((int)FileRequest.FileNUM);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        /// To cancel the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        /// To save the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            //if (lbcvHomeBase.Text == string.Empty && lbcvCustomerName.Text == string.Empty && lbcvContact.Text == string.Empty && lbcvLeadSource.Text == string.Empty && lbcvSalesPerson.Text == string.Empty)
                            //{
                            SaveCharterQuoteToSession();
                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            Master.Save(FileRequest);
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        /// <summary>
        /// To move next tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Next_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        Master.Next("File");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        #endregion

        protected void UseCustomFleetCharge_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
                        {
                            Int64 QuoteNumInProgress = 1;
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                            if (QuoteInProgress != null)
                            {
                                hdnCalculateFile.Value = "true";

                                if (chkUseCustomFleetCharge.Checked)
                                {
                                    //if (UserPrincipal.Identity._fpSettings._IsQuoteDeactivateAutoUpdateRates != null && (bool)UserPrincipal.Identity._fpSettings._IsQuoteDeactivateAutoUpdateRates)
                                    //{
                                    if (QuoteInProgress.CQFleetChargeDetails != null && QuoteInProgress.CQFleetChargeDetails.Count > 0)
                                    {
                                        RadWindowManager1.RadConfirm("The Quote will be updated based on Customer Charter Rates. Do you want to update?", "confirmAutoUpdateRatesFn", 330, 100, null, "Confirmation!");
                                    }
                                    //}
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("The Quote will be updated based on Tail Number / Aircraft Type Code Charter Rates.", 330, 100, ModuleNameConstants.CharterQuote.CQFile, string.Empty);
                                    Master.BindFleetChargeDetails((long)QuoteInProgress.FleetID, (long)QuoteInProgress.AircraftID, 0, false);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void ConfirmAutoUpdateRateYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Master.BindFleetChargeDetails(0, 0, !string.IsNullOrEmpty(hdnCustomerID.Value)? Convert.ToInt64(hdnCustomerID.Value):0, true);
                hdnCalculateFile.Value = "true";
            }
        }

        protected void ConfirmAutoUpdateRateNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                chkUseCustomFleetCharge.Checked = false;
                hdnCalculateFile.Value = "true";
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditFile_Click(sender, e);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
    }
}
