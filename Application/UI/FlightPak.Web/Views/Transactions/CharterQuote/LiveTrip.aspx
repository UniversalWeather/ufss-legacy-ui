﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LiveTrip.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CharterQuote.LiveTrip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Live Trip</title>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">
            function Close() {
                GetRadWindow().Close();
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function callBackFn(arg) {

                if (arg == true) {
                    document.getElementById('<%=btnTransfer.ClientID%>').click();
                }
                else {
                    GetRadWindow().Close();
                    return false;
                }
            }
        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnOk">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnTransfer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <div style="width: 330px; padding: 10px 0px 5px 0px; float: left">
            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" RepeatColumns="2"
                ID="radlstStatus" Width="250px">
                <asp:ListItem Value="W">Worksheet</asp:ListItem>
                <asp:ListItem Value="T" Selected="True">Tripsheet</asp:ListItem>
                <asp:ListItem Value="U">Unfulfilled</asp:ListItem>
                <asp:ListItem Value="X">Canceled</asp:ListItem>
                <asp:ListItem Value="H">Hold</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div style="width: 330px; height: 110px; float: right;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:Button ID="btnOk" runat="server" CssClass="button" Text="Ok" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnTransfer" runat="server" OnClick="btnTransferTOCQ_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
