﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewChaterRates.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CharterQuote.ViewChaterRates" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin(radWin) {
                var url = '';
                if (radWin == "rdTailNum") {
                    url = "../../Settings/Fleet/FleetProfilePopup.aspx?TailNumber=" + document.getElementById('<%=tbTail.ClientID%>').value + "&FromPage=" + "CompareCharterRates";
                }
                else if (radWin == "rdType") {
                    if (document.getElementById("<%=hdnTailNo.ClientID%>").value == "") {
                        url = "../../Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + document.getElementById('<%=tbTypeCode.ClientID%>').value + "&tailNoAirCraftID=" + document.getElementById("<%=hdnTypeCode.ClientID%>").value + "&FromPage=" + "CompareCharterRates";
                    }
                }

                if (url != "")
                    var oWnd = radopen(url, radWin);
            }

            function OnClientTailNoClose(oWnd, args) {
                var combo = $find("<%= tbTail.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTail.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=hdnTailNo.ClientID%>").value = arg.FleetIds;
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = arg.AirCraftTypeCodes;
                        var step = "Tail_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTail.ClientID%>").value = "";
                        document.getElementById("<%=hdnTailNo.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTypeClose(oWnd, args) {
                var combo = $find("<%= tbTypeCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = arg.AircraftID;

                        var step = "TypeCode_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }
        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdTailNum" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdType" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientTypeClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="radbtnlstCharterRates_SelectedIndexChanged">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnTailNum">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btntypeCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div id="DivExternalForm" runat="server">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td class="nav-3">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tdLabel50">
                                File No.
                            </td>
                            <td class="tdLabel110">
                                <asp:TextBox ID="tbFileNo" runat="server" ReadOnly="true" CssClass="text80" BackColor="Gray"
                                    ForeColor="White"></asp:TextBox>
                            </td>
                            <td class="tdLabel65">
                                Quote No.
                            </td>
                            <td>
                                <asp:TextBox ID="tbQuoteNo" runat="server" ReadOnly="true" CssClass="text80" BackColor="Gray"
                                    ForeColor="White"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav-6">
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Compare Charter Rates By</legend>
                                    <asp:RadioButtonList ID="radbtnlstCharterRates" runat="server" RepeatDirection="Horizontal"
                                        OnSelectedIndexChanged="radbtnlstCharterRates_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Selected="True" Value="1">Tail No.</asp:ListItem>
                                        <asp:ListItem Value="2">Type Code</asp:ListItem>
                                    </asp:RadioButtonList>
                                </fieldset>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            Tail No.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbTail" runat="server" CssClass="tdLabel420 nonedit_text"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnTailNum" runat="server" CssClass="browse-button" ToolTip="Search for Tail No."
                                                OnClientClick="javascript:openWin('rdTailNum');return false;" />
                                            <asp:HiddenField ID="hdnTailNo" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Type Code
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbTypeCode" runat="server" CssClass="tdLabel420 nonedit_text"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btntypeCode" runat="server" CssClass="browse-button" ToolTip="Search for Type Code"
                                                OnClientClick="javascript:openWin('rdType');return false;" />
                                            <asp:HiddenField ID="hdnTypeCode" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Reset" OnClick="Reset_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav-20">
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <table cellpadding="1" cellspacing="1" width="100%">
                            <tr class="GridViewAlternatingRowStyle">
                                <td class="tdLabel200 GridViewHeaderStyle">
                                    <asp:Label ID="lbTailNum" runat="server" Text="Tail No." Style="color: #fff; font-weight: bold;"></asp:Label>
                                </td>
                                <td class="tdLabel130">
                                    <asp:Label ID="lbSelectedTail" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum1" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum2" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum3" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum4" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum5" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum6" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum7" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum8" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum9" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td class="tdLabel110">
                                    <asp:Label ID="lbtailNum10" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                            </tr>
                            <tr class="GridViewRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbAirCraft" runat="server" Text="Type Code" Style="color: #fff; font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbSelectedTypeCode" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode1" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode2" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode3" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode4" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode5" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode6" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode7" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode8" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode9" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbaircraftCode10" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                            </tr>
                            <tr class="GridViewAlternatingRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbTypeCodeDesc" runat="server" Text="Type Code Description" Style="color: #fff;
                                        font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbselectedDescription" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription1" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription2" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription3" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription4" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription5" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription6" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription7" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription8" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription9" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                                <td>
                                    <asp:Label ID="lbdescription10" runat="server" CssClass="tdLabel100 mnd_text" />
                                </td>
                            </tr>
                            <tr class="GridViewRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbFlightCharges" runat="server" Text="Flight Charges" Style="color: #fff;
                                        font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbSelectedFlightCharges" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges1" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges2" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges3" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges4" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges5" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges6" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges7" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges8" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges9" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbFlightCharges10" runat="server" CssClass="tdLabel100" />
                                </td>
                            </tr>
                            <tr class="GridViewAlternatingRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbAdditionalfees" runat="server" Text="Additional Fees" Style="color: #fff;
                                        font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbSelectedAdditionalfees" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees1" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees2" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees3" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees4" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees5" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees6" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees7" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees8" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees9" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbAdditionalfees10" runat="server" CssClass="tdLabel100" />
                                </td>
                            </tr>
                            <tr class="GridViewRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbCost" runat="server" Text="Total Quote" Style="color: #fff; font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbSelectedCost" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost1" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost2" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost3" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost4" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost5" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost6" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost7" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost8" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost9" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbCost10" runat="server" CssClass="tdLabel100" />
                                </td>
                            </tr>
                            <tr class="GridViewAlternatingRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbMargin" runat="server" Text="Margin" Style="color: #fff; font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbSelectedMargin" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin1" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin2" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin3" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin4" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin5" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin6" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin7" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin8" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin9" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMargin10" runat="server" CssClass="tdLabel100" />
                                </td>
                            </tr>
                            <tr class="GridViewRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbMinimumMargin" runat="server" Text="Minimum Margin" Style="color: #fff;
                                        font-weight: bold;"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbSelectedMinimumMargin" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin1" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin2" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin3" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin4" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin5" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin6" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin7" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin8" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin9" runat="server" CssClass="tdLabel100" />
                                </td>
                                <td>
                                    <asp:Label ID="lbMinimumMargin10" runat="server" CssClass="tdLabel100" />
                                </td>
                            </tr>
                            <tr class="GridViewAlternatingRowStyle">
                                <td class="GridViewHeaderStyle">
                                    <asp:Label ID="lbDeparturedateFirstLeg" runat="server" Text="Departure Date 1st Leg"
                                        Style="color: #fff; font-weight: bold;"></asp:Label>
                                </td>
                                <td colspan="11">
                                    <asp:Label ID="lbSelectedDeparturedateLeg" runat="server" CssClass="tdLabel100" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
