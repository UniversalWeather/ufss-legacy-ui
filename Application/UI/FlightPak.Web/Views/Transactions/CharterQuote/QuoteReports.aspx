﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CharterQuote.Master"
    AutoEventWireup="true" CodeBehind="QuoteReports.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.QuoteReports" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CharterQuoteHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CharterQuoteBodyContent" runat="server">
   
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }

            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function lnkUniversalLocator_OnClientClick() {
                var url = '<% =ResolveClientUrl("~/Views/Utilities/UniversalLocator.aspx") %>';
                window.open(url, 'UniversalLocator', 'width=998,height=550,toolbar=no, location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=' + (screen.height - 550) / 2 + ',left=' + (screen.width - 998) / 2);
                return false;
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(url, value, radWin) {
                var oWnd = radopen(url + value, radWin);
            }

            function OpenCQSearch() {
                var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "RadCQSearch");
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function confirmCallBackQuoteFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnQuoteRefreshYes.ClientID%>').click();
            }
            else {
                return false;
            }
        }
        function confirmCallBackInvoiceFn(arg) {
            if (arg == true) {
                document.getElementById('<%=btnInvoiceRefreshYes.ClientID%>').click();
                }
                else {
                    return false;
                }
            }

            function OpenCQRadWindow(arg) {
                //alert(document.getElementById('<%=ddlQuoteImage.ClientID %>').value);
                var url = '';
                //var oWnd = $find("<%=RadCQImagePopup.ClientID%>");
                if (arg == "1")
                    url = "ImagePreview.aspx?FileWarehouseId=" + document.getElementById('<%=ddlQuoteImage.ClientID %>').value;
                else if (arg == "2")
                    url = "ImagePreview.aspx?FileWarehouseId=" + document.getElementById('<%=ddlQuoteImage2.ClientID %>').value;
                else if (arg == "3")
                    url = "ImagePreview.aspx?FileWarehouseId=" + document.getElementById('<%=ddlQuoteImage3.ClientID %>').value;
                else if (arg == "4")
                    url = "ImagePreview.aspx?FileWarehouseId=" + document.getElementById('<%=ddlQuoteImage4.ClientID %>').value;
    if (url != "")
        var oWnd = radopen(url, "RadCQImagePopup");
                //oWnd.show();
}

function openWin(radWin) {
    var url = '';
    if (radWin == "rdMultipleQuote") {
        url = "../../Transactions/CharterQuote/ViewMultipleQuote.aspx?";
    }

    if (radWin == "rdHistory") {
        url = "../../Transactions/History.aspx?FromPage=" + "CharterQuote";
    }
    if (url != "")
        var oWnd = radopen(url, radWin);
}
        </script>
    </telerik:RadCodeBlock>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Visible="true" runat="server"
        Skin="Sunset" />
    <telerik:RadAjaxManagerProxy ID="RadAjaxManager2" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ddlQuoteImage">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="imgQuote1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlQuoteImage2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="imgQuote2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlQuoteImage3">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="imgQuote3" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlQuoteImage4">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="imgQuote4" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnQuoteSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnInvoiceSaveNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCQImagePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" Title="Preview"
                NavigateUrl="ImagePreview.aspx" Width="600px" VisibleStatusbar="false" Height="500px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMultipleQuote" runat="server" OnClientResizeEnd="GetDimensions"
                Width="300px" Height="300px" KeepInScreenBounds="true" Modal="true" Behaviors="Close"
                VisibleStatusbar="false" NavigateUrl="../../Transactions/CharterQuote/ViewMultipleQuote.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div style="width: 712px;">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadTabStrip ID="TabCharterQuote" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                            OnTabClick="TabCharterQuote_Clicked" MultiPageID="RadMultiPage1" SelectedIndex="0"
                            Align="Justify" Width="712px" Style="float: inherit">
                            <Tabs>
                                <telerik:RadTab Text="Quote Report" Value="QR" TabIndex="0">
                                </telerik:RadTab>
                                <telerik:RadTab Text="Invoice Report" Value="IR" TabIndex="1">
                                </telerik:RadTab>
                            </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="tlk_checkbox">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <telerik:RadPanelBar ID="pnlbarQuoteReport" Width="100%" ExpandAnimation-Type="None"
                                                CollapseAnimation-Type="none" runat="server">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                                        <Items>
                                                            <telerik:RadPanelItem>
                                                                <ContentTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Report Info</legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td align="left" colspan="2">
                                                                                                <asp:Label ID="lbCharterCompanyInfo" runat="server" Text="Charter Company Information"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lbCustomerAddressInformation" runat="server" Text="Customer Address Information"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="left">
                                                                                                <asp:TextBox ID="tbCharterCompanyInfo" runat="server" CssClass="textarea320x75" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbCustomerAddressInformation" runat="server" CssClass="textarea320x75"
                                                                                                    TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="left">
                                                                                                <asp:Label ID="lbCustomHeaderInfo" runat="server" Text="Custom Header Information"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lbCustomerFooter" runat="server" Text="Custom Footer Information"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="left">
                                                                                                <asp:TextBox ID="tbHeaderInfo" runat="server" CssClass="textarea320x40" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbCustomerFooter" runat="server" CssClass="textarea320x40" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">Custom Footer
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <asp:DropDownList ID="ddlQuoteCustomFooter" runat="server" CssClass="text170" AutoPostBack="true"
                                                                                                    Height="20px" OnSelectedIndexChanged="ddlQuoteCustomFooter_SelectedIndexChanged">
                                                                                                </asp:DropDownList>
                                                                                                <asp:ImageButton runat="server" ID="imgbtnQuoteCustomFooter" ImageUrl="~/App_Themes/Default/images/reset_icon.png"
                                                                                                    OnClick="btnQuoteCustomFooter_Click" ToolTip="Refresh Customer Footer" />
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <%--<asp:Button ID="btnQuoteCustomFooter" runat="server" CssClass="button" Text="Refresh Customer Footer"
                                                                                        ValidationGroup="Save" OnClick = "btnQuoteCustomFooter_Click" />--%>
                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Image Info</legend>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <table cellpadding="3" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkQuoteImage1" runat="server" />
                                                                                                        </td>
                                                                                                        <td>Aircraft Image1
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlQuoteImage" runat="server" CssClass="text150" ClientIDMode="Static">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:ImageButton ID="imgQuote1" runat="server" ImageUrl="~/App_Themes/Default/images/browse_button.png"
                                                                                                                OnClientClick="javascript:OpenCQRadWindow('1');return false;" ToolTip="Preview" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkQuoteImage2" runat="server" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lbQuoteImage2" runat="server" Text="Aircraft Image2"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlQuoteImage2" runat="server" CssClass="text150" ClientIDMode="Static">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:ImageButton ID="imgQuote2" runat="server" ImageUrl="~/App_Themes/Default/images/browse_button.png"
                                                                                                                OnClientClick="javascript:OpenCQRadWindow('2');return false;" ToolTip="Preview" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <table cellpadding="3" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkQuoteImage3" runat="server" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lbQuoteImage3" runat="server" Text="Aircraft Image3"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlQuoteImage3" runat="server" CssClass="text150" ClientIDMode="Static">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:ImageButton ID="imgQuote3" runat="server" ImageUrl="~/App_Themes/Default/images/browse_button.png"
                                                                                                                OnClientClick="javascript:OpenCQRadWindow('3');return false;" ToolTip="Preview" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="chkQuoteImage4" runat="server" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lbQuoteImage4" runat="server" Text="Aircraft Image4"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlQuoteImage4" runat="server" CssClass="text150">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:ImageButton ID="imgQuote4" runat="server" ImageUrl="~/App_Themes/Default/images/browse_button.png"
                                                                                                                OnClientClick="javascript:OpenCQRadWindow('4');return false;" ToolTip="Preview" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td class="tdLabel83">
                                                                                                            <asp:Label ID="Label6" runat="server" Text="Image Position"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="tdLabel60">
                                                                                                            <asp:RadioButton ID="radNone" runat="server" Text="None" GroupName="ImagePosition" />
                                                                                                        </td>
                                                                                                        <td class="tdLabel99">
                                                                                                            <asp:RadioButton ID="radEndOfReport" runat="server" Text="End Of Report" GroupName="ImagePosition" />
                                                                                                        </td>
                                                                                                        <td class="tdLabel100">
                                                                                                            <asp:RadioButton ID="radTailNum" runat="server" Text="After Tail No." GroupName="ImagePosition" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:RadioButton ID="radSepPage" runat="server" Text="Separate Page" GroupName="ImagePosition" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkPrint" runat="server" Text="Print Quote Detail" OnCheckedChanged="chkQuotePrint_CheckedChanged"
                                                                                    AutoPostBack="true" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <telerik:RadGrid ID="dgQuoteLeg" runat="server" Visible="true" AutoGenerateColumns="false"
                                                                                    PagerStyle-AlwaysVisible="false" OnNeedDataSource="Leg_BindData" Height="160px"
                                                                                    Width="700px" OnItemDataBound="Leg_ItemDataBound">
                                                                                    <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" DataKeyNames="DepartAirportID,ArrivalAirportID,LegNum">
                                                                                        <Columns>
                                                                                            <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Print" UniqueName="Print">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkPrint" Checked='<%# Eval("isPrint") %>' runat="server" />
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Departure Date" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="DepartAir" HeaderText="From" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="To" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Quoted" ItemStyle-CssClass="fc_textbox" UniqueName="Quoted"
                                                                                                CurrentFilterFunction="Contains" ShowFilterIcon="false" AllowFiltering="false">
                                                                                                <ItemTemplate>
                                                                                                    <telerik:RadNumericTextBox ID="tbQuoted" runat="server" Type="Currency" Culture="en-US"
                                                                                                        DbValue='<%# Bind("QuoteTotal") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                                        ReadOnly="true">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <%-- <telerik:GridBoundColumn DataField="QuoteTotal" HeaderText="Quoted" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>--%>
                                                                                            <telerik:GridBoundColumn DataField="FlightHoursTotal" HeaderText="Flight Hours" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="Miles" HeaderText="Miles" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="PaxTotal" HeaderText="PAX" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                        </Columns>
                                                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                                                                    </MasterTableView>
                                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                                        <Scrolling AllowScroll="true" />
                                                                                        <Selecting AllowRowSelect="true" />
                                                                                    </ClientSettings>
                                                                                </telerik:RadGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </telerik:RadPanelItem>
                                                        </Items>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                            <telerik:RadPanelBar ID="RadPanelBar2" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                                runat="server" CssClass="charterLogReport-panel-bar">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Standard and Additional Fees"
                                                        Value="pnlItemTrip">
                                                        <ContentTemplate>
                                                            <div class="border-box-CharterQuote" style="float: left;">
                                                                <div style="float: left;">
                                                                    <asp:CheckBox ID="chkQuoteAddPrintSummary" runat="server" Text="Print Additional Fees"
                                                                        AutoPostBack="true" OnCheckedChanged="chkQuoteAddPrintSummary_CheckedChanged" />
                                                                </div>
                                                                <div class="charterquote-custom-grid tfoot" style="float: left;">
                                                                    <telerik:RadGrid ID="dgQuoteAddFee" runat="server" AllowSorting="true" Visible="true"
                                                                        OnItemDataBound="QuoteAddFee_ItemDataBound" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                                                                        OnNeedDataSource="QuoteAddFee_BindData" MasterTableView-NoMasterRecordsText="No records found"
                                                                        Height="160px" Width="695px">
                                                                        <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" DataKeyNames="OrderNUM">
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Print" UniqueName="Print">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkPrint" runat="server" Checked='<%# Eval("IsPrintable") %>' />
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Description" UniqueName="CQQuoteFeeDescription">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtdesc" runat="server" Text='<%# Eval("CQQuoteFeeDescription") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Quoted" ItemStyle-CssClass="fc_textbox" UniqueName="Quoted"
                                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AllowFiltering="false">
                                                                                    <ItemTemplate>
                                                                                        <telerik:RadNumericTextBox ID="tbQuotedAmount" runat="server" Type="Currency" Culture="en-US"
                                                                                            DbValue='<%# Bind("QuoteAmount") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                            ReadOnly="true">
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <%-- <telerik:GridBoundColumn DataField="QuoteAmount" HeaderText="Quoted" CurrentFilterFunction="Contains"
                                                                                    ShowFilterIcon="false">
                                                                                </telerik:GridBoundColumn>--%>
                                                                            </Columns>
                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                            <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                                                        </MasterTableView>
                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                            <Scrolling AllowScroll="true" />
                                                                            <Selecting AllowRowSelect="true" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                            <telerik:RadPanelBar ID="RadPanelBar3" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                                runat="server" CssClass="charterLogReport-panel-bar">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Quote Summary" Value="pnlItemTrip">
                                                        <ContentTemplate>
                                                            <div class="border-box-CharterQuote" style="float: left;">
                                                                <div style="float: left;">
                                                                    <asp:CheckBox ID="chkQuoteSummary" runat="server" Text="Print Summary" AutoPostBack="true"
                                                                        OnCheckedChanged="chkQuoteSummary_CheckedChanged" />
                                                                </div>
                                                                <br />
                                                                <div style="float: left; width: 100%;">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr class="GridViewHeaderStyle">
                                                                            <th>Print
                                                                            </th>
                                                                            <th>Description
                                                                            </th>
                                                                            <th>Quoted
                                                                            </th>
                                                                        </tr>
                                                                        <tr class="GridViewRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkDailyUsageAdjustment" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbDailyDesc" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%--<asp:Label ID="lbDailyQuoteAmount" runat="server"></asp:Label>--%>
                                                                                <telerik:RadNumericTextBox ID="tbDailyQuoteAmount" runat="server" Type="Currency"
                                                                                    Culture="en-US" MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="."
                                                                                    EnabledStyle-HorizontalAlign="Left" ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewAlternatingRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkAdditionalFee" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbAdditionalFee" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%-- <asp:Label ID="lbAdditionalFee" runat="server"></asp:Label>--%>
                                                                                <telerik:RadNumericTextBox ID="tbAdditionalFeee" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="GridViewRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkLandingFee" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbLandingFee" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <telerik:RadNumericTextBox ID="tbLandingFeee" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="GridViewAlternatingRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkSegmentFee" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbSegmentFee" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%-- <asp:Label ID="lbSegmentFee" runat="server"></asp:Label>--%>
                                                                                <telerik:RadNumericTextBox ID="tbSegmentFeee" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkFlightCharge" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbFlightCharge" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%-- <asp:Label ID="lbFlightCharge" runat="server"></asp:Label>--%>
                                                                                <telerik:RadNumericTextBox ID="tbFlightChargee" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewAlternatingRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkSubTotal" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbSubTotal" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%-- <asp:Label ID="lbSubtotal" runat="server"></asp:Label>--%>
                                                                                <telerik:RadNumericTextBox ID="tbSubtotall" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkDiscountAmount" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbDiscountAmount" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%-- <asp:Label ID="lbDiscountAmount" runat="server" />--%>
                                                                                <telerik:RadNumericTextBox ID="tbDiscountAmountt" runat="server" Type="Currency"
                                                                                    Culture="en-US" MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="."
                                                                                    EnabledStyle-HorizontalAlign="Left" ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewAlternatingRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTaxes" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbTax" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%--   <asp:Label ID="lbTax" runat="server" />--%>
                                                                                <telerik:RadNumericTextBox ID="tbTaxx" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkTotalQuote" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbTotalQuote" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%-- <asp:Label ID="lbTotalQuote" runat="server" />--%>
                                                                                <telerik:RadNumericTextBox ID="tbTotalQuotee" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewAlternatingRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkPrepaid" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbPrepaid" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%--  <asp:Label ID="lbPrepaid" runat="server" />--%>
                                                                                <telerik:RadNumericTextBox ID="tbPrepaidd" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="GridViewRowStyle">
                                                                            <td>
                                                                                <asp:CheckBox ID="chkRemainingAmt" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbRemainingAmt" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                            <td class="fc_textbox_130">
                                                                                <%--  <asp:Label ID="lbRemainingamt" runat="server" />--%>
                                                                                <telerik:RadNumericTextBox ID="tbRemainingamtt" runat="server" Type="Currency" Culture="en-US"
                                                                                    MaxLength="17" CssClass="fc_textbox" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                    ReadOnly="true">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="lnkQuoteReportBottom" runat="server" CssClass="print_preview_icon_CQ"
                                                ToolTip="Preview Quote Report" OnClick="lnkQuoteReportBottom_Click" CausesValidation="false"></asp:LinkButton>
                                        </td>
                                        <td align="right">
                                            <asp:ImageButton runat="server" ID="btnQuoteRefresh" ImageUrl="~/App_Themes/Default/images/reset_icon.png"
                                                OnClick="btnQuoteRefresh_Click" ToolTip="Refresh" Visible="false" />
                                            <asp:Button ID="btnQuoteCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnQuoteCancel_click" />
                                            <asp:Button ID="btnQuoteSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                                                OnClick="btnQuoteSave_click" />
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadPanelBar ID="RadPanelBar1" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                                                runat="server">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                                                        <Items>
                                                            <telerik:RadPanelItem>
                                                                <ContentTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Report Info</legend>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td align="left" colspan="2">
                                                                                                <asp:Label ID="lbInvoiceCharCompanyInfo" runat="server" Text="Charter Company Information"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lbInvoiceMyCharCompanyInfo" runat="server" Text="Customer Address Information"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="left">
                                                                                                <asp:TextBox ID="tbInvoiceCompanyInfo" runat="server" CssClass="textarea320x75" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbMyCharterCustomer" runat="server" CssClass="textarea320x75" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="left">
                                                                                                <asp:Label ID="lbInvoiceHeaderInformation" runat="server" Text="Custom Header Message"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lbInvoiceCustomerFooterInfo" runat="server" Text="Custom Footer Information"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" align="left">
                                                                                                <asp:TextBox ID="tbInvoiceHeaderInfo" runat="server" CssClass="textarea320x40" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbInvoiceFooterInfo" runat="server" CssClass="textarea320x40" TextMode="MultiLine"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">Custom Footer
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" colspan="2">
                                                                                                <asp:DropDownList ID="ddlInvoiceMyCustomerFooter" runat="server" CssClass="text170"
                                                                                                    AutoPostBack="true" Height="20px" OnSelectedIndexChanged="ddlInvoiceMyCustomerFooter_SelectedIndexChanged">
                                                                                                </asp:DropDownList>
                                                                                                <asp:ImageButton runat="server" ID="imgInvoiceRefreshFooter" ImageUrl="~/App_Themes/Default/images/reset_icon.png"
                                                                                                    OnClick="btnInvoiceCustomFooter_Click" ToolTip="Refresh Customer Footer" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <%--  <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <fieldset>
                                                                        <legend>Image Info</legend>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                Aircraft Image
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:DropDownList ID="ddlInvoiceAircraftImage" runat="server" CssClass="text150">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="tdLabel83">
                                                                                                <asp:Label ID="lbInvoiceIamgePosition" runat="server" Text="Image Position"></asp:Label>
                                                                                            </td>
                                                                                            <td class="tdLabel50">
                                                                                                <asp:RadioButton ID="rbInvoiceNone" runat="server" Text="None" GroupName="ImagePosition" />
                                                                                            </td>
                                                                                            <td class="tdLabel99">
                                                                                                <asp:RadioButton ID="rbInvoiceEndofReport" runat="server" Text="End Of Report" GroupName="ImagePosition" />
                                                                                            </td>
                                                                                            <td class="tdLabel120">
                                                                                                <asp:RadioButton ID="rbInvoiceAfterTail" runat="server" Text="After Tail Number"
                                                                                                    GroupName="ImagePosition" />
                                                                                            </td>
                                                                                            <td class="tdLabel99">
                                                                                                <asp:RadioButton ID="rbInvoiceSeperate" runat="server" Text="Separte Page" GroupName="ImagePosition" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkInvoicePrintDetail" runat="server" Text="Print Invoice Detail"
                                                                                    OnCheckedChanged="chkInvoicePrintDetail_CheckedChanged" AutoPostBack="true" />
                                                                                <asp:CheckBox ID="chkInvoicePrintQuoteNo" runat="server" Text="Print Quote No./Description" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <telerik:RadGrid ID="dgInvoiceLeg" runat="server" AllowSorting="true" Visible="true"
                                                                                    OnItemDataBound="InvoiceLeg_ItemDataBound" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                                                                                    PageSize="5" OnNeedDataSource="InvoiceLeg_BindData" Height="160px" Width="700px">
                                                                                    <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" PageSize="5"
                                                                                        NoMasterRecordsText="No records found" DataKeyNames="DepartAirportID,ArrivalAirportID,LegNum">
                                                                                        <Columns>
                                                                                            <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridTemplateColumn HeaderText="Print" UniqueName="Print">
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkPrint" Checked='<%# Eval("isPrint") %>' runat="server" />
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Departure Date" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="DepartAir" HeaderText="From" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="To" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <%--  <telerik:GridBoundColumn DataField="QuoteTotal" HeaderText="Quoted" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>--%>
                                                                                            <telerik:GridTemplateColumn HeaderText="Quoted" ItemStyle-CssClass="fc_textbox" HeaderStyle-Width="80px"
                                                                                                UniqueName="Quoted" CurrentFilterFunction="Contains" ShowFilterIcon="false" AllowFiltering="false">
                                                                                                <ItemTemplate>
                                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceQuotedRate" runat="server" Type="Currency"
                                                                                                        Culture="en-US" DbValue='<%# Bind("QuoteTotal") %>' NumberFormat-DecimalSeparator="."
                                                                                                        EnabledStyle-HorizontalAlign="Left" ReadOnly="true">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </ItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridBoundColumn DataField="FlightHoursTotal" HeaderText="Flight Hours" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="Miles" HeaderText="Miles" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false" AllowFiltering="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                            <telerik:GridBoundColumn DataField="PaxTotal" HeaderText="PAX" CurrentFilterFunction="Contains"
                                                                                                ShowFilterIcon="false">
                                                                                            </telerik:GridBoundColumn>
                                                                                        </Columns>
                                                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                                                                    </MasterTableView>
                                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                                        <Scrolling AllowScroll="true" />
                                                                                        <Selecting AllowRowSelect="true" />
                                                                                    </ClientSettings>
                                                                                </telerik:RadGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </telerik:RadPanelItem>
                                                        </Items>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                            <telerik:RadPanelBar ID="RadPanelBar4" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                                runat="server" CssClass="charterLogReport-panel-bar">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Standard and Additional Fees"
                                                        Value="pnlItemTrip">
                                                        <ContentTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="tdLabel160" valign="middle">
                                                                                    <asp:CheckBox ID="chkInvoicePrintAdditionalFees" runat="server" Text="Print Additional Fees"
                                                                                        AutoPostBack="true" OnCheckedChanged="chkInvoicePrintAdditionalFees_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel180" valign="middle">
                                                                                    <asp:CheckBox ID="chkInvoiceAdditionalFeesTaxable" runat="server" Text="Additional Fees Taxable"
                                                                                        AutoPostBack="true" OnCheckedChanged="chkInvoiceAdditionalFeesTaxable_CheckedChanged" />
                                                                                </td>
                                                                                <td class="tdLabel180" valign="middle">Tax Rate:
                                                                                    <%--<telerik:RadNumericTextBox ID="tbInvocieAdditionalFeeTaxRate" runat="server" Type="Currency"
                                                                                        Culture="en-US" MaxLength="7" AutoPostBack="true" OnTextChanged="tbInvocieAdditionalFeeTaxRate_TextChanged"
                                                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                                                                    </telerik:RadNumericTextBox>--%>
                                                                                    <asp:TextBox ID="tbInvocieAdditionalFeeTaxRate" runat="server" Text="" CssClass="tdLabel30"
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="7" AutoPostBack="true"
                                                                                        OnTextChanged="tbInvocieAdditionalFeeTaxRate_TextChanged"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadGrid ID="dgInvoiceAddFee" runat="server" AllowSorting="true" Visible="true"
                                                                            OnItemDataBound="dgInvoiceAddFee_ItemDataBound" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                                                                            PageSize="5" OnNeedDataSource="InvoiceAddFee_BindData" Height="160px" Width="695px">
                                                                            <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" PageSize="5"
                                                                                NoMasterRecordsText="No records found" DataKeyNames="OrderNUM">
                                                                                <Columns>
                                                                                    <telerik:GridTemplateColumn HeaderText="Print" UniqueName="Print">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkInvoiceAddfeePrint" runat="server" Checked='<%# Eval("IsPrintable") %>' />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                    <telerik:GridTemplateColumn HeaderText="Description" UniqueName="CQQuoteFeeDescription">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="tbInvoiceAdditioanalDescription" runat="server" Text='<%# Eval("CQInvoiceFeeDescription") %>'></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                    <%--<telerik:GridBoundColumn DataField="QuoteAmount" HeaderText="Quoted" CurrentFilterFunction="Contains"
                                                                                        ShowFilterIcon="false">
                                                                                    </telerik:GridBoundColumn>--%>
                                                                                    <telerik:GridTemplateColumn HeaderText="Quoted" ItemStyle-CssClass="fc_textbox" HeaderStyle-Width="80px"
                                                                                        UniqueName="QuoteAmount" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                                                        AllowFiltering="false">
                                                                                        <ItemTemplate>
                                                                                            <telerik:RadNumericTextBox ID="tbInvoiceQuotedAmount" runat="server" Type="Currency"
                                                                                                Culture="en-US" DbValue='<%# Bind("QuoteAmount") %>' NumberFormat-DecimalSeparator="."
                                                                                                EnabledStyle-HorizontalAlign="Left" ReadOnly="true">
                                                                                            </telerik:RadNumericTextBox>
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                    <telerik:GridTemplateColumn HeaderText="Invoiced" HeaderStyle-Width="80px" UniqueName="InvoiceAmount"
                                                                                        CurrentFilterFunction="Contains" ItemStyle-CssClass="pr_radtextbox_65" ShowFilterIcon="false"
                                                                                        AllowFiltering="false">
                                                                                        <ItemTemplate>
                                                                                            <telerik:RadNumericTextBox ID="tbInvoiceAdditionalFeeInvocieAmt" runat="server" Type="Currency"
                                                                                                Culture="en-US" DbValue='<%# Bind("InvoiceAmt") %>' NumberFormat-DecimalSeparator="."
                                                                                                EnabledStyle-HorizontalAlign="Left" MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceAdditionalFeeInvocieAmt_TextChanged">
                                                                                            </telerik:RadNumericTextBox>
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                    <%--<telerik:GridTemplateColumn HeaderText="Invoiced">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="tbInvoiceAdditionalFeeInvocieAmt" runat="server" Text='<%# Eval("InvoiceAmt") %>'
                                                                                                MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceAdditionalFeeInvocieAmt_TextChanged"
                                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>--%>
                                                                                    <telerik:GridTemplateColumn HeaderText="Taxable" UniqueName="Print">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkInvoiceTaxable" runat="server" Checked='<%# Eval("IsTaxable") %>' />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                    <telerik:GridTemplateColumn HeaderText="Discount" UniqueName="Print">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkInvoiceDiscount" runat="server" Checked='<%# Eval("IsDiscount") %>' />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                </Columns>
                                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                                                            </MasterTableView>
                                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                                <Scrolling AllowScroll="true" />
                                                                                <Selecting AllowRowSelect="true" />
                                                                            </ClientSettings>
                                                                        </telerik:RadGrid>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                            <telerik:RadPanelBar ID="RadPanelBar5" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                                runat="server" CssClass="charterLogReport-panel-bar">
                                                <Items>
                                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Invoice Summary" Value="pnlItemTrip">
                                                        <ContentTemplate>
                                                            <div class="border-box-CharterQuote" style="float: left;">
                                                                <div style="float: left;">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel130" valign="middle">
                                                                                <asp:CheckBox ID="chkÍnvoicePrintSummary" runat="server" Text="Print Summary" AutoPostBack="true"
                                                                                    OnCheckedChanged="chkÍnvoicePrintSummary_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel180" valign="middle">
                                                                                <asp:CheckBox ID="chkInvoiceSummaryFeesTaxable" runat="server" Text="Summary Fees Taxable"
                                                                                    AutoPostBack="true" OnCheckedChanged="chkInvoiceSummaryFeesTaxable_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel130" valign="middle">
                                                                                <asp:CheckBox ID="chkInvoiceManualTaxEntry" AutoPostBack="true" runat="server" Text="Manual Tax Entry"
                                                                                    OnCheckedChanged="chkInvoiceManualTaxEntry_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel180" valign="middle">
                                                                                <asp:CheckBox ID="chkManualDiscountEntry" runat="server" AutoPostBack="true" Text="Manual Discount Entry"
                                                                                    OnCheckedChanged="chkManualDiscountEntry_CheckedChanged" />
                                                                            </td>
                                                                            <td class="tdLabel110" valign="middle">Tax Rate:
                                                                                <asp:TextBox ID="tbInvoiceTaxRate" runat="server" CssClass="tdLabel30" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                    AutoPostBack="true" OnTextChanged="tbInvocieAdditionalFeeTaxRate_TextChanged"></asp:TextBox>
                                                                                <%--<telerik:RadNumericTextBox ID="tbInvoiceTaxRate" runat="server" Type="Currency" Culture="en-US"
                                                                                    AutoPostBack="true" OnTextChanged="tbInvoiceAdditionalFeeInvocieAmt_TextChanged"
                                                                                    NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right" MaxLength="7">
                                                                                </telerik:RadNumericTextBox>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div class="border-box-CharterQuote" style="float: left;">
                                                                    <div style="float: left;">
                                                                    </div>
                                                                    <div>
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr class="GridViewHeaderStyle">
                                                                                <th>Print
                                                                                </th>
                                                                                <th>Description
                                                                                </th>
                                                                                <th>Quote Amount
                                                                                </th>
                                                                                <th>Invoice Amount
                                                                                </th>
                                                                                <th>Taxable
                                                                                </th>
                                                                                <th>Discount
                                                                                </th>
                                                                            </tr>
                                                                            <tr class="GridViewRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceDailyUsageAdjustmentPrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceDailyUsageAdjustment" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <%-- <asp:Label ID="lbInvoicedailyusageQuoteAmt" runat="server"></asp:Label>--%>
                                                                                    <telerik:RadNumericTextBox ID="tbInvoicedailyusageQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="17" CssClass="fc_textbox" ReadOnly="true" />
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <%--<asp:TextBox ID="tbInvoicedailyusageInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoicedailyusageInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                    <telerik:RadNumericTextBox ID="tbInvoicedailyusageInvoiceAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoicedailyusageInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkDailyusageTaxable" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkDailyusageDiscount" runat="server" Enabled="false" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewAlternatingRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceAdditionalfeePrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceAdditionalFeeDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <%--<asp:Label ID="lbInvoiceAdditionalfeeQuoteAmt" runat="server"></asp:Label>--%>
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceAdditionalfeeQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceAdditionalfeeInvoiceAmt" Enabled="false"
                                                                                        runat="server" Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="."
                                                                                        EnabledStyle-HorizontalAlign="Left" MaxLength="16" OnTextChanged="tbInvoiceAdditionalfeeInvoiceAmt_TextChanged"
                                                                                        AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvoiceAdditionalfeeInvoiceAmt" runat="server" Enabled="false"
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="16" AutoPostBack="true"
                                                                                        OnTextChanged="tbInvoiceAdditionalfeeInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceAdditionalfeeInvoiceTaxable" runat="server" Enabled="false" />
                                                                                </td>
                                                                                <td>
                                                                                    <%--<asp:CheckBox ID="chkInvoiceAdditionalfeeInvoiceDiscount" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceSegmentFeePrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceSegmentFeeDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceSegmentFeeQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%-- <asp:Label ID="lbInvoiceSegmentFeeQuoteAmt" runat="server"></asp:Label>--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceSegmentFeeInvoiceAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceSegmentFeeInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%--  <asp:TextBox ID="tbInvoiceSegmentFeeInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceSegmentFeeInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceSegmentFeetaxable" runat="server" OnCheckedChanged="chkInvoiceSegmentFeetaxable_CheckedChanged"
                                                                                        AutoPostBack="true" />
                                                                                </td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr class="GridViewAlternatingRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceRonCrewChargePrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceRonCrewChargeDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceRonCrewChargeQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%--<asp:Label ID="lbInvoiceRonCrewChargeQuoteAmt" runat="server"></asp:Label>--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvocieRonCrewChargeInvoiceAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvocieRonCrewChargeInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvocieRonCrewChargeInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvocieRonCrewChargeInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceRonCrewChargeTaxable" runat="server" OnCheckedChanged="chkInvoiceRonCrewChargeTaxable_CheckedChanged"
                                                                                        AutoPostBack="true" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceRonCrewChargeDiscount" runat="server" OnCheckedChanged="chkInvoiceRonCrewChargeDiscount_CheckedChanged"
                                                                                        AutoPostBack="true" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceFlightCharge" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceFlightChargeDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceFlightChargeQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%--<asp:Label ID="lbInvoiceFlightChargeQuoteAmt" runat="server"></asp:Label>--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceFlightChargeInvocieAmt" Enabled="false" runat="server"
                                                                                        Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceFlightChargeInvocieAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%-- <asp:TextBox ID="tbInvoiceFlightChargeInvocieAmt" Enabled="false" runat="server"
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="16" AutoPostBack="true"
                                                                                        OnTextChanged="tbInvoiceFlightChargeInvocieAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceFlightChargeTaxable" runat="server" Enabled="false" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceFlightChargeDiscount" runat="server" OnCheckedChanged="chkInvoiceFlightChargeDiscount_CheckedChanged"
                                                                                        AutoPostBack="true" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewAlternatingRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceSubtotalPrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceSubtotalDecription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceSubtotalQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%-- <asp:Label ID="lbInvoiceSubtotalQuoteAmt" runat="server" />--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceSubtotalInvoiceAmt" Enabled="false" runat="server"
                                                                                        Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceSubtotalInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvoiceSubtotalInvoiceAmt" runat="server" Enabled="false" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceSubtotalInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="chkInvoiceSubtotalTaxable" runat="server" />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="chkInvoiceSubtotalDiscount" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceDiscountPrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceDiscountdescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <%-- <asp:Label ID="lbInvoiceDiscountQuoteAmt" runat="server" />--%>
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceDiscountQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceDiscountInvoiceAmt" Enabled="false" runat="server"
                                                                                        Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceDiscountInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvoiceDiscountInvoiceAmt" runat="server" Enabled="false" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceDiscountInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="chkInvoiceDiscountTaxable" runat="server" />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="chkInvoiceDiscountDiscount" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewAlternatingRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceTaxesPrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceTaxesDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTaxesQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%-- <asp:Label ID="lbInvoiceTaxesQuoteAmt" runat="server" />--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTaxesInvoiceAmt" Enabled="false" runat="server"
                                                                                        Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceTaxesInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvoiceTaxesInvoiceAmt" runat="server" Enabled="false" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceTaxesInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%--<asp:CheckBox ID="CheckBox2" runat="server" />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="CheckBox5" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceTotalInvoicePrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceTotalInvoiceDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTotalInvoiceQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%--  <asp:Label ID="lbInvoiceTotalInvoiceQuoteAmt" runat="server" />--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTotalInvoiceInvoiceAmt" Enabled="false" runat="server"
                                                                                        Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceTotalInvoiceInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvoiceTotalInvoiceInvoiceAmt" runat="server" Enabled="false"
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="16" AutoPostBack="true"
                                                                                        OnTextChanged="tbInvoiceTotalInvoiceInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%--<asp:CheckBox ID="CheckBox2" runat="server" />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="CheckBox5" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewAlternatingRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceTotalPrepayPrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceTotalPrepayDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTotalPrepayQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%-- <asp:Label ID="lbInvoiceTotalPrepayQuoteAmt" runat="server" />--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTotalPrepayInvoiceAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        MaxLength="16" OnTextChanged="tbInvoiceTotalPrepayInvoiceAmt_TextChanged" AutoPostBack="true" />
                                                                                    <%-- <asp:TextBox ID="tbInvoiceTotalPrepayInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceTotalPrepayInvoiceAmt_TextChanged"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%--<asp:CheckBox ID="CheckBox2" runat="server" />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="CheckBox5" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="GridViewRowStyle">
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkInvoiceTotalRemainingPrint" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbInvoiceTotalRemainingDescription" runat="server" Width="200px"></asp:TextBox>
                                                                                </td>
                                                                                <td class="fc_textbox_130">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTotalRemainingQuoteAmt" runat="server" Type="Currency"
                                                                                        Culture="en-US" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                        CssClass="fc_textbox" ReadOnly="true" />
                                                                                    <%-- <asp:Label ID="lbInvoiceTotalRemainingQuoteAmt" runat="server" />--%>
                                                                                </td>
                                                                                <td class="pr_radtextbox_150">
                                                                                    <telerik:RadNumericTextBox ID="tbInvoiceTotalRemainingInvoiceAmt" Enabled="false"
                                                                                        runat="server" Type="Currency" Culture="en-US" NumberFormat-DecimalSeparator="."
                                                                                        EnabledStyle-HorizontalAlign="Left" MaxLength="16" OnTextChanged="tbInvoiceTotalRemainingInvoiceAmt_TextChanged"
                                                                                        AutoPostBack="true" />
                                                                                    <%--<asp:TextBox ID="tbInvoiceTotalRemainingInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceTotalRemainingInvoiceAmt_TextChanged"
                                                                                        Enabled="false"></asp:TextBox>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%--<asp:CheckBox ID="CheckBox2" runat="server" />--%>
                                                                                </td>
                                                                                <td>
                                                                                    <%-- <asp:CheckBox ID="CheckBox5" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelBar>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:LinkButton ID="lnkInvoiceReportBottom" runat="server" CssClass="print_preview_icon_CQ"
                                                ToolTip="Preview Invoice Report" OnClick="lnkInvoiceReportBottom_Click" CausesValidation="false"></asp:LinkButton>
                                        </td>
                                        <td align="left">
                                            <asp:LinkButton ID="lnkMultipleQuote" runat="server" Text="Print Multiple Quote"
                                                ToolTip="Print Multiple Quote" OnClientClick="javascript:openWin('rdMultipleQuote');return false;"></asp:LinkButton>
                                        </td>
                                        <td align="right">
                                            <asp:ImageButton runat="server" ID="btnInvoiceRefresh" ImageUrl="~/App_Themes/Default/images/reset_icon.png"
                                                OnClick="InvoiceRefresh_Click" ToolTip="Refresh" Visible="false" />
                                            <asp:Button ID="btnInvoiceCancel" runat="server" CssClass="button" Text="Cancel"
                                                OnClick="btnInvoiceCancel_Click" CausesValidation="false" />
                                            <asp:Button ID="btnInvoiceSaveNew" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                                                OnClick="btnInvoiceSave_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </td>
                </tr>
            </table>
            <table id="tblHidden" style="display: none;">
                <tr>
                    <td>
                        <asp:Button ID="btnQuoteRefreshYes" runat="server" Text="Button" OnClick="QuoteRefreshYes_Click" />
                        <asp:Button ID="btnInvoiceRefreshYes" runat="server" Text="Button" OnClick="InvoiceRefreshYes_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- Un-used fields commented -->
    <!--
    <tr class="GridViewRowStyle" style="display: none">
        <td>
            <asp:CheckBox ID="chkInvoiceLandingfeePrint" runat="server" />
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceLandingFeeDescription" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lbInvoiceLandingfeeQuoteAmt" runat="server"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceLandingfeeInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceLandingfeeInvoiceAmt_TextChanged"></asp:TextBox>
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceLandingfeeInvoiceTaxable" runat="server" />
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceLandingfeeInvoiceDiscount" runat="server" />
        </td>
    </tr>
    <tr class="GridViewAlternatingRowStyle" style="display: none">
        <td>
            <asp:CheckBox ID="chkInvoiceAddCrewChargePrint" runat="server" />
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceAddrewChargeDescription" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lbInvoiceAddCrewChargeQuoteAmt" runat="server"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbInvocieAddCrewChargeInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvocieAddCrewChargeInvoiceAmt_TextChanged"></asp:TextBox>
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceAddCrewChargeTaxable" runat="server" />
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceAddCrewChargeDiscount" runat="server" />
        </td>
    </tr>
    <tr class="GridViewRowStyle" style="display: none">
        <td>
            <asp:CheckBox ID="chkInvoiceAdditionalRonCrewChargePrint" runat="server" />
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceAdditionalRonCrewChargeDescription" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lbInvoiceAdditionalRonCrewChargeQuoteAmt" runat="server"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceAdditionalRonCrewChargeInvoiceAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceAdditionalRonCrewChargeInvoiceAmt_TextChanged"></asp:TextBox>
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceAdditionalRonCrewChargeTaxable" runat="server" />
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceAdditionalRonCrewChargeDiscount" runat="server" />
        </td>
    </tr>
    <tr class="GridViewAlternatingRowStyle" style="display: none">
        <td>
            <asp:CheckBox ID="chkInvoiceWaitCharge" runat="server" />
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceWaitChargeDescription" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lbInvoiceWaitChargeQuoteAmt" runat="server"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="tbInvoiceWaitChargeInvocieAmt" runat="server" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                MaxLength="16" AutoPostBack="true" OnTextChanged="tbInvoiceWaitChargeInvocieAmt_TextChanged"></asp:TextBox>
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceWaitChargeTaxable" runat="server" />
        </td>
        <td>
            <asp:CheckBox ID="chkInvoiceWaitChargeDiscount" runat="server" />
        </td>
    </tr>
    -->
</asp:Content>
