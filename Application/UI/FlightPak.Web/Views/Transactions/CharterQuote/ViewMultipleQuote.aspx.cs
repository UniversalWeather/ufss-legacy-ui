﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.CharterQuoteService;

namespace FlightPak.Web.Views.Transactions.CharterQuote
{
    public partial class ViewMultipleQuote : BaseSecuredPage
    {
        public string CQSessionKey = "CQFILE";
        public bool IsSummary = true;
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        int NoOfQuotes = 1;
        public string QuoteNums = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FileRequest = (CQFile)Session[CQSessionKey];
                if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count() > 0)
                {
                    CharterQuoteService.CQInvoiceMain cqs = new CharterQuoteService.CQInvoiceMain();
                    if (FileRequest.CQMains.Count() > 0)
                    {
                        foreach (CQMain cqMain in FileRequest.CQMains)
                        {
                            using (CharterQuoteService.CharterQuoteServiceClient CQService = new CharterQuoteService.CharterQuoteServiceClient())
                            {
                                var objRetVal = CQService.GetCQInvoiceMainByCQMainID(cqMain.CQMainID);
                                if (objRetVal.EntityList.Count == null || (objRetVal.EntityList.Count() == 0 && objRetVal.EntityList.Count != null))
                                {
                                    LoadInvoiceCompanyDetail(ref cqs, cqMain.CQMainID, (int)cqMain.QuoteNUM);
                                }
                            }
                        }
                    }
                }

            }
            if (Session[CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[CQSessionKey];

                if (FileRequest != null)
                {
                    if (FileRequest.FileNUM != null && FileRequest.FileNUM != 0)
                        lbFileNo.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.FileNUM.ToString());
                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        NoOfQuotes = FileRequest.CQMains.Count;

                        for (int i = 1; i <= NoOfQuotes; i++)
                        {
                            chklstquote.Items.Add("Quote No." + i);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// To Load Quote CompanyDetails
        /// </summary>
        /// <param name="cqs"></param>
        private void LoadInvoiceCompanyDetail(ref CharterQuoteService.CQInvoiceMain cqs, Int64 CQMainID, Int32 QuoteNUM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqs))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if (FileRequest != null && FileRequest.HomebaseID != null)
                    {
                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId((long)FileRequest.HomebaseID);
                        if (CompanyList.EntityList.Count() != null && CompanyList.EntityList.Count() > 0)
                        {
                            cqs.State = CQRequestEntityState.Added;
                            cqs.Mode = CQRequestActionMode.Edit;
                            cqs.CQMainID = CQMainID;
                            cqs.CharterCompanyInformation = CompanyList.EntityList[0].ChtQuoteCompanyNameINV;
                            cqs.InvoiceHeader = CompanyList.EntityList[0].ChtQuoteHeaderINV;
                            cqs.InvoiceFooter = CompanyList.EntityList[0].ChtQuoteFooterINV;

                            if (CompanyList.EntityList[0].IsQuoteDetailPrint != null)
                                cqs.IsPrintDetail = CompanyList.EntityList[0].IsQuoteDetailPrint;
                            if (CompanyList.EntityList[0].IsQuotePrintSum != null)
                                cqs.IsPrintSum = CompanyList.EntityList[0].IsQuotePrintSum;
                            if (CompanyList.EntityList[0].isQuotePrintFees != null)
                                cqs.IsPrintFees = CompanyList.EntityList[0].isQuotePrintFees;
                            if (CompanyList.EntityList[0].CQFederalTax != null)
                                cqs.InvoiceAdditionalFeeTax = CompanyList.EntityList[0].CQFederalTax;
                            else
                                cqs.InvoiceAdditionalFeeTax = 0;

                            #region Bind Customer Address Info for Quote

                            if (FileRequest.CQCustomerID != null && FileRequest.CQCustomerID > 0)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.CQCustomer custObj = new FlightPakMasterService.CQCustomer();
                                    custObj.CQCustomerID = Convert.ToInt64(FileRequest.CQCustomerID);

                                    var ReturnValue = objMasterService.GetCQCustomerByCQCustomerID(custObj);

                                    if (ReturnValue != null && ReturnValue.EntityList.Count > 0)
                                    {
                                        string custAddr = string.Empty;
                                        if (ReturnValue.EntityList[0].CQCustomerName != null)
                                            custAddr = ReturnValue.EntityList[0].CQCustomerName + "\r\n";
                                        if (ReturnValue.EntityList[0].BillingAddr1 != null)
                                            custAddr += ReturnValue.EntityList[0].BillingAddr1.ToString().Trim() + "\r\n";
                                        if (ReturnValue.EntityList[0].BillingAddr2 != null)
                                            custAddr += ReturnValue.EntityList[0].BillingAddr2.ToString().Trim() + "\r\n";
                                        if (ReturnValue.EntityList[0].BillingAddr3 != null)
                                            custAddr += ReturnValue.EntityList[0].BillingAddr3.ToString().Trim() + "\r\n";

                                        if (ReturnValue.EntityList[0].BillingCity != null)
                                            custAddr += ReturnValue.EntityList[0].BillingCity.ToString().Trim() + ", ";
                                        if (ReturnValue.EntityList[0].BillingState != null)
                                            custAddr += ReturnValue.EntityList[0].BillingState.ToString().Trim() + ", ";
                                        if (ReturnValue.EntityList[0].BillingZip != null)
                                            custAddr += ReturnValue.EntityList[0].BillingZip.ToString().Trim() + ", ";
                                        if (ReturnValue.EntityList[0].CountryCD != null)
                                            custAddr += ReturnValue.EntityList[0].CountryCD.ToString().Trim();

                                        cqs.CustomerAddressInformation = custAddr;
                                    }
                                }
                            }

                            #endregion

                            IsSummary = true;
                            cqs.CQInvoiceSummaries = new List<CQInvoiceSummary>();
                            CQInvoiceSummary cqInvoiceSummary = new CQInvoiceSummary();
                            cqInvoiceSummary.State = CQRequestEntityState.Added;
                            cqInvoiceSummary.IsPrintUsage = CompanyList.EntityList[0].IsQuotePrepaidMinimUsageFeeAmt;
                            cqInvoiceSummary.UsageAdjDescription = CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt;
                            //Additional Fees
                            cqInvoiceSummary.IsPrintAdditonalFees = CompanyList.EntityList[0].IsQuotePrintAdditionalFee;
                            cqInvoiceSummary.DescriptionAdditionalFee = CompanyList.EntityList[0].QuoteAdditionalFeeDefault;
                            //LAnding fee
                            cqInvoiceSummary.IsPrintLandingFees = CompanyList.EntityList[0].IsQuoteFlightLandingFeePrint;
                            cqInvoiceSummary.LandingFeeDescription = CompanyList.EntityList[0].QuoteLandingFeeDefault;
                            //Segment Fees 
                            cqInvoiceSummary.IsPrintSegmentFee = CompanyList.EntityList[0].IsQuotePrepaidSegementFee;
                            cqInvoiceSummary.SegmentFeeDescription = CompanyList.EntityList[0].QuoteSegmentFeeDeposit;
                            //std crew
                            cqInvoiceSummary.IsPrintStdCrew = CompanyList.EntityList[0].IsQuoteStdCrewRonPrepaid;
                            cqInvoiceSummary.StdCrewRONDescription = CompanyList.EntityList[0].QuoteStdCrewRONDeposit;
                            //Flight Charge
                            cqInvoiceSummary.IsPrintFlightChg = CompanyList.EntityList[0].IsQuoteFlightChargePrepaid;
                            cqInvoiceSummary.FlightChgDescription = CompanyList.EntityList[0].DepostFlightCharge;
                            //Subtotal
                            cqInvoiceSummary.IsPrintSubtotal = CompanyList.EntityList[0].IsQuoteSubtotalPrint;
                            cqInvoiceSummary.SubtotalDescription = CompanyList.EntityList[0].QuoteSubtotal;
                            //Discount Amount
                            cqInvoiceSummary.IsPrintDiscountAmt = CompanyList.EntityList[0].IsQuoteDispcountAmtPrint;
                            cqInvoiceSummary.DescriptionDiscountAmt = CompanyList.EntityList[0].DiscountAmountDefault;
                            //Taxes
                            cqInvoiceSummary.IsPrintTaxes = CompanyList.EntityList[0].IsQuoteTaxesPrint;
                            cqInvoiceSummary.TaxesDescription = CompanyList.EntityList[0].QuoteTaxesDefault;
                            //Total Invoice
                            cqInvoiceSummary.IsInvoiceTax = CompanyList.EntityList[0].IsQuoteInvoicePrint;
                            cqInvoiceSummary.InvoiceDescription = CompanyList.EntityList[0].DefaultInvoice;
                            //Total Prepay
                            cqInvoiceSummary.IsPrintPrepay = CompanyList.EntityList[0].IsQuotePrepayPrint;
                            cqInvoiceSummary.PrepayDescription = CompanyList.EntityList[0].QuotePrepayDefault;
                            //Total Remaining Invoice
                            cqInvoiceSummary.IsPrintRemaingAMT = CompanyList.EntityList[0].IsQuoteRemainingAmtPrint;
                            cqInvoiceSummary.RemainingAmtDescription = CompanyList.EntityList[0].QuoteRemainingAmountDefault;
                            cqs.CQInvoiceSummaries.Add(cqInvoiceSummary);

                            LoadInvoiceAdditionalFeeandSummary(ref cqs, CQMainID, QuoteNUM);
                        }
                    }
                }
            }
        }


        /// To Load AdditionalFeeSummary
        /// </summary>
        /// <param name="InvoiceData"></param>
        private void LoadInvoiceAdditionalFeeandSummary(ref CharterQuoteService.CQInvoiceMain InvoiceData, long CQMainID, Int32 QuoteNUM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InvoiceData))
            {
                using (CharterQuoteServiceClient CQService = new CharterQuoteServiceClient())
                {
                    if (Session[CQSessionKey] != null)
                    {
                        Int64 QuoteNumInProgress = 1;
                        FileRequest = (CQFile)Session[CQSessionKey];
                        QuoteNumInProgress = QuoteNUM;
                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                        if (QuoteInProgress != null)
                        {
                            InvoiceData.CQMainID = QuoteInProgress.CQMainID;
                            if (IsSummary)
                            {
                                InvoiceData.CQInvoiceSummaries = new List<CQInvoiceSummary>();
                                CQInvoiceSummary cqInvoiceSummary = new CQInvoiceSummary();
                                cqInvoiceSummary.State = CQRequestEntityState.Added;
                                cqInvoiceSummary.IsPrintUsage = QuoteInProgress.IsPrintUsageAdj;
                                cqInvoiceSummary.UsageAdjDescription = QuoteInProgress.UsageAdjDescription;
                                cqInvoiceSummary.UsageAdjTotal = QuoteInProgress.UsageAdjTotal;
                                cqInvoiceSummary.QuoteUsageAdj = QuoteInProgress.UsageAdjTotal;
                                cqInvoiceSummary.IsUsageAdjTax = QuoteInProgress.IsDailyTaxAdj;

                                //Additional Fees
                                cqInvoiceSummary.IsPrintAdditonalFees = QuoteInProgress.IsPrintAdditonalFees;
                                cqInvoiceSummary.DescriptionAdditionalFee = QuoteInProgress.AdditionalFeeDescription;
                                cqInvoiceSummary.AdditionalFees = QuoteInProgress.AdditionalFeeTotalD;
                                cqInvoiceSummary.AdditionalFeeTotal = QuoteInProgress.AdditionalFeeTotalD;

                                //LAnding fee
                                cqInvoiceSummary.IsPrintLandingFees = QuoteInProgress.IsLandingFeeTax;
                                cqInvoiceSummary.LandingFeeDescription = QuoteInProgress.LandingFeeDescription;
                                cqInvoiceSummary.QuoteLandingFee = QuoteInProgress.LandingFeeTotal;
                                cqInvoiceSummary.LandingFeeTotal = QuoteInProgress.LandingFeeTotal;
                                cqInvoiceSummary.IsLandingFeeTax = QuoteInProgress.IsLandingFeeTax;

                                //Segment Fees                      
                                cqInvoiceSummary.IsPrintSegmentFee = QuoteInProgress.IsPrintSegmentFee;
                                cqInvoiceSummary.SegmentFeeDescription = QuoteInProgress.SegmentFeeDescription;
                                cqInvoiceSummary.QuoteSegementFee = QuoteInProgress.SegmentFeeTotal;
                                cqInvoiceSummary.SegmentFeeTotal = QuoteInProgress.SegmentFeeTotal;

                                //Additional Crew Ron Charge                 
                                cqInvoiceSummary.IsPrintAdditionalCrew = QuoteInProgress.IsPrintAdditionalCrew;
                                cqInvoiceSummary.AdditionalCrewDescription = QuoteInProgress.AdditionalCrewDescription;
                                cqInvoiceSummary.AdditionalCrewRONTotal = QuoteInProgress.AdditionalCrewRONTotal1;
                                cqInvoiceSummary.AdditionalCrewTotal = QuoteInProgress.AdditionalCrewRONTotal1;

                                //Ron Crew charge                       
                                cqInvoiceSummary.IsPrintStdCrew = QuoteInProgress.IsPrintStdCrew;
                                cqInvoiceSummary.StdCrewRONDescription = QuoteInProgress.StdCrewROMDescription;
                                cqInvoiceSummary.StdCrewRONTotal = QuoteInProgress.StdCrewRONTotal;
                                cqInvoiceSummary.QuoteStdCrewRON = QuoteInProgress.StdCrewRONTotal;

                                ////Wait Charge
                                cqInvoiceSummary.IsWaitingChg = QuoteInProgress.IsPrintWaitingChg;
                                cqInvoiceSummary.WaitChgdescription = QuoteInProgress.WaitChgDescription;
                                cqInvoiceSummary.QuoteWaitingChg = QuoteInProgress.WaitChgTotal;
                                cqInvoiceSummary.WaitChgTotal = QuoteInProgress.WaitChgTotal;

                                //Flight Charge
                                cqInvoiceSummary.IsPrintFlightChg = QuoteInProgress.IsPrintFlightChg;
                                cqInvoiceSummary.FlightChgDescription = QuoteInProgress.FlightChgDescription;
                                cqInvoiceSummary.FlightChargeTotal = QuoteInProgress.FlightChargeTotal;
                                cqInvoiceSummary.FlightCharge = QuoteInProgress.FlightChargeTotal;

                                //Subtotal
                                cqInvoiceSummary.IsPrintSubtotal = QuoteInProgress.IsPrintSubtotal;
                                cqInvoiceSummary.SubtotalDescription = QuoteInProgress.SubtotalDescription;
                                cqInvoiceSummary.QuoteSubtotal = QuoteInProgress.SubtotalTotal;
                                cqInvoiceSummary.SubtotalTotal = QuoteInProgress.SubtotalTotal;

                                //Discount Amount
                                cqInvoiceSummary.IsPrintDiscountAmt = QuoteInProgress.IsPrintDiscountAmt;
                                cqInvoiceSummary.DescriptionDiscountAmt = QuoteInProgress.DescriptionDiscountAmt;
                                cqInvoiceSummary.QuoteDiscountAmount = QuoteInProgress.DiscountAmtTotal;
                                cqInvoiceSummary.DiscountAmtTotal = QuoteInProgress.DiscountAmtTotal;

                                //Taxes
                                cqInvoiceSummary.IsPrintTaxes = QuoteInProgress.IsPrintTaxes;
                                cqInvoiceSummary.TaxesDescription = QuoteInProgress.TaxesDescription;
                                cqInvoiceSummary.QuoteTaxes = QuoteInProgress.TaxesTotal;
                                cqInvoiceSummary.TaxesTotal = QuoteInProgress.TaxesTotal;

                                //Total Invoice
                                cqInvoiceSummary.IsInvoiceTax = QuoteInProgress.IsPrintInvoice;
                                cqInvoiceSummary.InvoiceDescription = QuoteInProgress.InvoiceDescription;
                                cqInvoiceSummary.QuoteInvoice = QuoteInProgress.QuoteTotal;
                                cqInvoiceSummary.InvoiceTotal = QuoteInProgress.QuoteTotal;

                                //Total Prepay
                                cqInvoiceSummary.IsPrintPrepay = QuoteInProgress.IsPrintPay;
                                cqInvoiceSummary.PrepayDescription = QuoteInProgress.PrepayDescription;
                                cqInvoiceSummary.PrepayTotal = QuoteInProgress.PrepayTotal;
                                cqInvoiceSummary.QuotePrepay = QuoteInProgress.PrepayTotal;

                                //Total Remaining Invoice
                                cqInvoiceSummary.IsPrintRemaingAMT = QuoteInProgress.IsPrintRemaingAMT;
                                cqInvoiceSummary.RemainingAmtDescription = QuoteInProgress.RemainingAmtDescription;
                                cqInvoiceSummary.RemainingAmtTotal = QuoteInProgress.RemainingAmtTotal;
                                cqInvoiceSummary.QuoteRemainingAmt = QuoteInProgress.RemainingAmtTotal;


                                cqInvoiceSummary.UsageAdjDescription = "DAILY USAGE ADJUSTMENT:";
                                cqInvoiceSummary.DescriptionAdditionalFee = "ADDITIONAL FEES:";
                                cqInvoiceSummary.LandingFeeDescription = "LANDING FEES:";
                                cqInvoiceSummary.SegmentFeeDescription = "SEGMENT FEES:";
                                cqInvoiceSummary.StdCrewRONDescription = "RON CREW CHARGE:";
                                cqInvoiceSummary.FlightChgDescription = "FLIGHT CHARGE:";
                                cqInvoiceSummary.SubtotalDescription = "SUBTOTAL:";
                                cqInvoiceSummary.DescriptionDiscountAmt = "DISCOUNT AMOUNT:";
                                cqInvoiceSummary.TaxesDescription = "TAXES:";
                                cqInvoiceSummary.InvoiceDescription = "TOTAL INVOICE:";
                                cqInvoiceSummary.PrepayDescription = "TOTAL PREPAY:";
                                cqInvoiceSummary.RemainingAmtDescription = "TOTAL REMAINING INVOICE:";

                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    if (FileRequest.HomebaseID != null)
                                    {
                                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId((long)FileRequest.HomebaseID);
                                        if (CompanyList != null && CompanyList.EntityList != null && CompanyList.EntityList.Count() > 0)
                                        {
                                            cqInvoiceSummary.IsPrintUsage = CompanyList.EntityList[0].IsQuotePrepaidMinimUsageFeeAmt;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt))
                                                cqInvoiceSummary.UsageAdjDescription = CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt;


                                            //Additional Fees
                                            cqInvoiceSummary.IsPrintAdditonalFees = CompanyList.EntityList[0].IsQuotePrintAdditionalFee;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteAdditionalFeeDefault))
                                                cqInvoiceSummary.DescriptionAdditionalFee = CompanyList.EntityList[0].QuoteAdditionalFeeDefault;


                                            //LAnding fee                           
                                            cqInvoiceSummary.IsPrintLandingFees = CompanyList.EntityList[0].IsQuoteFlightLandingFeePrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteLandingFeeDefault))
                                                cqInvoiceSummary.LandingFeeDescription = CompanyList.EntityList[0].QuoteLandingFeeDefault;

                                            //Segment Fees 
                                            cqInvoiceSummary.IsPrintSegmentFee = CompanyList.EntityList[0].IsQuotePrepaidSegementFee;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSegmentFeeDeposit))
                                                cqInvoiceSummary.SegmentFeeDescription = CompanyList.EntityList[0].QuoteSegmentFeeDeposit;

                                            //std crew
                                            cqInvoiceSummary.IsPrintStdCrew = CompanyList.EntityList[0].IsQuoteStdCrewRonPrepaid;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteStdCrewRONDeposit))
                                                cqInvoiceSummary.StdCrewRONDescription = CompanyList.EntityList[0].QuoteStdCrewRONDeposit;

                                            //Flight Charge
                                            cqInvoiceSummary.IsPrintFlightChg = CompanyList.EntityList[0].IsQuoteFlightChargePrepaid;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].DepostFlightCharge))
                                                cqInvoiceSummary.FlightChgDescription = CompanyList.EntityList[0].DepostFlightCharge;

                                            //Subtotal
                                            cqInvoiceSummary.IsPrintSubtotal = CompanyList.EntityList[0].IsQuoteSubtotalPrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSubtotal))
                                                cqInvoiceSummary.SubtotalDescription = CompanyList.EntityList[0].QuoteSubtotal;


                                            //Discount Amount
                                            cqInvoiceSummary.IsPrintDiscountAmt = CompanyList.EntityList[0].IsQuoteDispcountAmtPrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].DiscountAmountDefault))
                                                cqInvoiceSummary.DescriptionDiscountAmt = CompanyList.EntityList[0].DiscountAmountDefault;


                                            //Taxes
                                            cqInvoiceSummary.IsPrintTaxes = CompanyList.EntityList[0].IsQuoteTaxesPrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteTaxesDefault))
                                                cqInvoiceSummary.TaxesDescription = CompanyList.EntityList[0].QuoteTaxesDefault;


                                            //Total Invoice
                                            cqInvoiceSummary.IsInvoiceTax = CompanyList.EntityList[0].IsQuoteInvoicePrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].DefaultInvoice))
                                                cqInvoiceSummary.InvoiceDescription = CompanyList.EntityList[0].DefaultInvoice;


                                            //Total Prepay                            
                                            cqInvoiceSummary.IsPrintPrepay = CompanyList.EntityList[0].IsQuotePrepayPrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuotePrepayDefault))
                                                cqInvoiceSummary.PrepayDescription = CompanyList.EntityList[0].QuotePrepayDefault;


                                            //Total Remaining Invoice
                                            cqInvoiceSummary.IsPrintRemaingAMT = CompanyList.EntityList[0].IsQuoteRemainingAmtPrint;
                                            if (!string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteRemainingAmountDefault))
                                                cqInvoiceSummary.RemainingAmtDescription = CompanyList.EntityList[0].QuoteRemainingAmountDefault;


                                        }
                                    }
                                }
                                InvoiceData.CQInvoiceSummaries.Add(cqInvoiceSummary);

                            }
                            else
                            {
                                if (InvoiceData.CQInvoiceSummaries != null && InvoiceData.CQInvoiceSummaries.Where(x => x.IsDeleted == false).Count() > 0)
                                {
                                    CQInvoiceSummary cqInvoiceSummary = new CQInvoiceSummary();
                                    cqInvoiceSummary = InvoiceData.CQInvoiceSummaries.Where(x => x.IsDeleted == false).ToList()[0];
                                    if (cqInvoiceSummary != null)
                                    {
                                        if (cqInvoiceSummary.CQInvoiceSummaryID != 0 && cqInvoiceSummary.State != CQRequestEntityState.Deleted)
                                            cqInvoiceSummary.State = CQRequestEntityState.Modified;
                                        cqInvoiceSummary.UsageAdjTotal = QuoteInProgress.UsageAdjTotal;
                                        cqInvoiceSummary.QuoteUsageAdj = QuoteInProgress.UsageAdjTotal;

                                        //Additional Fees
                                        cqInvoiceSummary.AdditionalFees = QuoteInProgress.AdditionalFeeTotalD;
                                        cqInvoiceSummary.AdditionalFeeTotal = QuoteInProgress.AdditionalFeeTotalD;

                                        //LAnding fee
                                        cqInvoiceSummary.QuoteLandingFee = QuoteInProgress.LandingFeeTotal;
                                        cqInvoiceSummary.LandingFeeTotal = QuoteInProgress.LandingFeeTotal;
                                        //invoiceQuoteSummary.IsLandingFeeDiscount = chkInvoiceLandingfeeInvoiceDiscount.Checked;

                                        //Segment Fees                      
                                        cqInvoiceSummary.QuoteSegementFee = QuoteInProgress.SegmentFeeTotal;
                                        cqInvoiceSummary.SegmentFeeTotal = QuoteInProgress.SegmentFeeTotal;

                                        //Additional Crew  Charge
                                        cqInvoiceSummary.AdditionalCrewRONTotal = QuoteInProgress.AdditionalCrewRONTotal1;
                                        cqInvoiceSummary.AdditionalCrewTotal = QuoteInProgress.AdditionalCrewRONTotal1;

                                        //Ron Crew charge                       
                                        cqInvoiceSummary.QuoteStdCrewRON = QuoteInProgress.StdCrewRONTotal;
                                        cqInvoiceSummary.StdCrewRONTotal = QuoteInProgress.StdCrewRONTotal;

                                        ////Wait Charge
                                        cqInvoiceSummary.QuoteWaitingChg = QuoteInProgress.WaitChgTotal;
                                        cqInvoiceSummary.WaitChgTotal = QuoteInProgress.WaitChgTotal;

                                        //Flight Charge
                                        cqInvoiceSummary.FlightChargeTotal = QuoteInProgress.FlightChargeTotal;
                                        cqInvoiceSummary.FlightCharge = QuoteInProgress.FlightChargeTotal;

                                        //Subtotal
                                        cqInvoiceSummary.QuoteSubtotal = QuoteInProgress.QuoteTotal;
                                        cqInvoiceSummary.SubtotalTotal = QuoteInProgress.SubtotalTotal;

                                        //Discount Amount
                                        cqInvoiceSummary.QuoteDiscountAmount = QuoteInProgress.DiscountAmtTotal;
                                        cqInvoiceSummary.DiscountAmtTotal = QuoteInProgress.DiscountAmtTotal;

                                        //Taxes
                                        cqInvoiceSummary.QuoteTaxes = QuoteInProgress.TaxesTotal;
                                        cqInvoiceSummary.TaxesTotal = QuoteInProgress.TaxesTotal;

                                        //Total Invoice
                                        cqInvoiceSummary.QuoteInvoice = QuoteInProgress.QuoteTotal;
                                        cqInvoiceSummary.InvoiceTotal = QuoteInProgress.QuoteTotal;

                                        //Total Prepay
                                        cqInvoiceSummary.PrepayTotal = QuoteInProgress.PrepayTotal;
                                        cqInvoiceSummary.QuotePrepay = QuoteInProgress.PrepayTotal;

                                        //Total Remaining Invoice
                                        cqInvoiceSummary.QuoteRemainingAmt = QuoteInProgress.RemainingAmtTotal;
                                        cqInvoiceSummary.RemainingAmtTotal = QuoteInProgress.RemainingAmtTotal;
                                    }
                                }
                            }
                            // QuoteInProgress.CQInvoiceMains[0].CQInvoiceSummaries.Add(cqInvoiceSummary);
                            if (QuoteInProgress.CQFleetChargeDetails != null)
                            {
                                if (InvoiceData != null || InvoiceData.CQInvoiceAdditionalFees == null)
                                    InvoiceData.CQInvoiceAdditionalFees = new List<CQInvoiceAdditionalFee>();
                                foreach (CQFleetChargeDetail fltCharge in QuoteInProgress.CQFleetChargeDetails)
                                {
                                    CQInvoiceAdditionalFee addfee = new CQInvoiceAdditionalFee();
                                    addfee.State = CQRequestEntityState.Added;
                                    //addfee.CQInvoiceMainID = fltCharge.ma
                                    addfee.IsDeleted = false;
                                    addfee.OrderNUM = fltCharge.OrderNUM != null ? fltCharge.OrderNUM : 0;
                                    addfee.CQInvoiceFeeDescription = fltCharge.CQFlightChargeDescription;
                                    addfee.QuoteAmount = fltCharge.FeeAMT != null ? fltCharge.FeeAMT : 0;
                                    addfee.InvoiceAmt = fltCharge.FeeAMT != null ? fltCharge.FeeAMT : 0;
                                    addfee.Quantity = fltCharge.Quantity != null ? fltCharge.Quantity : 0;
                                    addfee.ChargeUnit = fltCharge.ChargeUnit;
                                    addfee.Rate = (fltCharge.FeeAMT == null || (fltCharge.Quantity == null || fltCharge.Quantity == 0)) ? 0 : fltCharge.FeeAMT / fltCharge.Quantity;
                                    addfee.IsTaxable = fltCharge.IsTaxDOM != null ? fltCharge.IsTaxDOM : false;
                                    addfee.IsDiscount = fltCharge.IsDiscountDOM != null ? fltCharge.IsDiscountDOM : false;
                                    addfee.IsPrintable = InvoiceData.IsPrintFees != null ? (bool)InvoiceData.IsPrintFees : false;  //UserPrincipal.Identity._fpSettings._isQuotePrintFees != null ? (bool)UserPrincipal.Identity._fpSettings._isQuotePrintFees : false;
                                    InvoiceData.CQInvoiceAdditionalFees.Add(addfee);
                                }
                            }
                            if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                            {
                                if (InvoiceData.CQInvoiceQuoteDetails == null)
                                    InvoiceData.CQInvoiceQuoteDetails = new List<CQInvoiceQuoteDetail>();
                                else
                                {
                                    foreach (CQInvoiceQuoteDetail CqInvdet in InvoiceData.CQInvoiceQuoteDetails.ToList())
                                    {
                                        if (CqInvdet.CQInvoiceQuoteDetailID != 0)
                                        {
                                            CqInvdet.State = CQRequestEntityState.Deleted;
                                            CqInvdet.IsDeleted = true;
                                        }
                                        else
                                        {
                                            InvoiceData.CQInvoiceQuoteDetails.Remove(CqInvdet);
                                        }
                                    }
                                }
                                foreach (CQLeg Leg in QuoteInProgress.CQLegs)
                                {
                                    CQInvoiceQuoteDetail addQuoteDetail = new CQInvoiceQuoteDetail();
                                    addQuoteDetail.State = CQRequestEntityState.Added;
                                    addQuoteDetail.IsDeleted = false;
                                    addQuoteDetail.LegNum = Leg.LegNUM != null ? Leg.LegNUM : 0;
                                    addQuoteDetail.DepartureDTTMLocal = Leg.DepartureDTTMLocal;
                                    addQuoteDetail.ArrivalDTTMLocal = Leg.ArrivalDTTMLocal;
                                    addQuoteDetail.Airport1 = Leg.Airport1;
                                    addQuoteDetail.Airport = Leg.Airport;
                                    addQuoteDetail.DAirportID = Leg.DAirportID != null ? Leg.DAirportID : 0;
                                    addQuoteDetail.AAirportID = Leg.AAirportID != null ? Leg.AAirportID : 0;
                                    addQuoteDetail.PassengerTotal = Leg.PassengerTotal != null ? Leg.PassengerTotal : 0;
                                    addQuoteDetail.FlightHours = Leg.ElapseTM != null ? Leg.ElapseTM : 0;
                                    addQuoteDetail.Distance = Leg.Distance != null ? Leg.Distance : 0;
                                    addQuoteDetail.FlightCharge = Leg.ChargeTotal != null ? Leg.ChargeTotal : 0;
                                    addQuoteDetail.IsPrintable = InvoiceData.IsPrintDetail != null ? (bool)InvoiceData.IsPrintDetail : false; //UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint : false;
                                    addQuoteDetail.TaxRate = InvoiceData.InvoiceAdditionalFeeTax;
                                    InvoiceData.CQInvoiceQuoteDetails.Add(addQuoteDetail);
                                }
                            }
                        }
                    }
                    ClearInvoiceChildProperties(ref InvoiceData);
                    CQService.SaveInvoiceReport(InvoiceData);
                }
            }
        }


        private void ClearInvoiceChildProperties(ref CQInvoiceMain cqInvoiceMain)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqInvoiceMain))
            {
                if (cqInvoiceMain.CQMessageID != null) cqInvoiceMain.CQMessage = null;
                if (cqInvoiceMain.CQMainID != null) cqInvoiceMain.CQMain = null;
                if (cqInvoiceMain.CustomerID != null) cqInvoiceMain.Customer = null;
                if (cqInvoiceMain.LastUpdUID != null) cqInvoiceMain.UserMaster = null;
                #region CQMain Properties

                #endregion
                #region CQQuoteDetails Properties
                if (cqInvoiceMain.CQInvoiceQuoteDetails != null)
                {
                    foreach (CQInvoiceQuoteDetail QuoteDet in cqInvoiceMain.CQInvoiceQuoteDetails)
                    {
                        if (QuoteDet.DAirportID != null) QuoteDet.Airport1 = null;
                        if (QuoteDet.AAirportID != null) QuoteDet.Airport = null;
                    }
                }
                #endregion
            }
        }
        protected void lnkInvoiceReport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session[CQSessionKey] != null)
                {
                    Int64 QuoteNumInProgress = 1;
                    FileRequest = (CQFile)Session[CQSessionKey];
                    if (FileRequest.QuoteNumInProgress == null)
                        FileRequest.QuoteNumInProgress = QuoteNumInProgress;
                    else
                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                    if (NoOfQuotes > 0)
                    {
                        for (int i = 1; i <= NoOfQuotes; i++)
                        {
                            ListItem lstItem = new ListItem();
                            lstItem = chklstquote.Items.FindByText("Quote No." + i);
                            if (lstItem.Selected == true)
                            {
                                QuoteNums += i + ",";
                            }
                        }
                        QuoteNums = QuoteNums.Substring(0, QuoteNums.Length - 1);
                    }
                    ShowReport("RptCQInvoice", QuoteNums, FileRequest.CQFileID);
                }
            }
        }

        private void ShowReport(string reportName, string quoteNum, Int64 cqFileID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(reportName, quoteNum, cqFileID))
            {
                Session["TabSelect"] = "Charter";
                Session["REPORT"] = reportName;
                Session["FORMAT"] = "PDF";
                Session["PARAMETER"] = "QuoteNUM=" + quoteNum.ToString() + ";CQFileID=" + cqFileID.ToString() + ";UserCD=" + UserPrincipal.Identity._name;
                Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
            }
        }
    }
}