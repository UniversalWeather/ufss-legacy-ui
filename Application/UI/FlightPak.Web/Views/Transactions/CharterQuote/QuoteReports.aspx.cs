﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.UI.WebControls;
using FlightPak.Web.AdminService;
using Telerik.Web.UI;
using FlightPak.Web.CharterQuoteService;
using System.Text.RegularExpressions;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Globalization;
using System.Text;
using Omu.ValueInjecter;
using Microsoft.Security.Application;
using FlightPak.Common.Constants;

namespace FlightPak.Web.Views.CharterQuote
{
    [ScriptService]
    public partial class QuoteReports : BaseSecuredPage
    {
        #region Declarations
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        private ExceptionManager exManager;
        private delegate void SaveSession();
        public bool IsSummary = true;
        public bool IsQuoteSummary = true;
        const decimal statuateConvFactor = 1.1508M;

        private const string DailyUsageAdjustment = "DAILY USAGE ADJUSTMENT:";
        private const string AdditionalFees = "ADDITIONAL FEES:";
        private const string LandingFees = "LANDING FEES:";
        private const string SegmentFees = "SEGMENT FEES:";
        private const string FlightCharge = "FLIGHT CHARGE:";
        private const string SubTotal = "SUBTOTAL:";
        private const string DiscountAmount = "DISCOUNT AMOUNT:";
        private const string Taxes = "TAXES";
        private const string TotalQuote = "TOTAL QUOTE";
        private const string Prepaid = "PREPAID";
        private const string RemainingAmountDue = "REMAINING AMOUNT DUE:";

        private const string RonCrewCharge = "RON CREW CHARGE:";
        private const string TotalInvoice = "TOTAL INVOICE:";
        private const string TotalPrepay = "TOTAL PREPAY:";
        private const string TotalRemainingInvoice = "TOTAL REMAINING INVOICE:";

        private const string AdditionalRonCrewCharge = "ADDITIONAL RON CREW CHARGE:";
        private const string WaitCharge = "WAIT CHARGE:";
        private const string InvoiceReportInvoicedAmount = "Invoice Report - Invoiced Amount";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.CharterQuoteReports))
            {
                Response.Redirect("~/Views/Home.aspx?m=CharterQuoteReports");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(TabCharterQuote, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        if (Session["CQQuoteReport"] == null || (Session["CQQuoteReport"] != null && Session["CQQuoteReport"].ToString() == "QR"))
                        {
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPrint, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkQuoteAddPrintSummary, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkQuoteSummary, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lnkQuoteReportBottom, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnQuoteCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnQuoteSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(ddlQuoteCustomFooter, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        }
                        else
                        {
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvoiceTotalPrepayInvoiceAmt, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvoiceTaxesInvoiceAmt, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvoiceDiscountInvoiceAmt, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvoicedailyusageInvoiceAmt, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvoiceSegmentFeeInvoiceAmt, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvocieRonCrewChargeInvoiceAmt, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnInvoiceSaveNew, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnInvoiceCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkÍnvoicePrintSummary, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkInvoicePrintAdditionalFees, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkInvoicePrintDetail, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkInvoiceAdditionalFeesTaxable, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkInvoiceSummaryFeesTaxable, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkInvoiceManualTaxEntry, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkManualDiscountEntry, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvoiceTaxRate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbInvocieAdditionalFeeTaxRate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgInvoiceAddFee, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(ddlInvoiceMyCustomerFooter, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                            //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lnkInvoiceReportBottom, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                            
                        }

                        if (Session[Master.CQSessionKey] != null)
                        {
                            Int64 QuoteNumInProgress = 1;

                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                        }

                        if (!IsPostBack)
                        {
                            chkQuoteImage2.Visible = false;
                            lbQuoteImage2.Visible = false;
                            ddlQuoteImage2.Visible = false;
                            imgQuote2.Visible = false;

                            // Fix for SUP-183 (7281)
                            chkQuoteImage3.Visible = false;
                            lbQuoteImage3.Visible = false;
                            ddlQuoteImage3.Visible = false;
                            imgQuote3.Visible = false;

                            chkQuoteImage4.Visible = false;
                            lbQuoteImage4.Visible = false;
                            ddlQuoteImage4.Visible = false;
                            imgQuote4.Visible = false;

                            if (UserPrincipal.Identity._fpSettings._ImageCnt.HasValue)
                            {
                                if (UserPrincipal.Identity._fpSettings._ImageCnt == 2)
                                {
                                    chkQuoteImage2.Visible = true;
                                    lbQuoteImage2.Visible = true;
                                    ddlQuoteImage2.Visible = true;
                                    imgQuote2.Visible = true;
                                }
                                else if (UserPrincipal.Identity._fpSettings._ImageCnt == 3)
                                {
                                    chkQuoteImage2.Visible = true;
                                    lbQuoteImage2.Visible = true;
                                    ddlQuoteImage2.Visible = true;
                                    imgQuote2.Visible = true;

                                    chkQuoteImage3.Visible = true;
                                    lbQuoteImage3.Visible = true;
                                    ddlQuoteImage3.Visible = true;
                                    imgQuote3.Visible = true;
                                }
                                else if (UserPrincipal.Identity._fpSettings._ImageCnt == 4)
                                {
                                    chkQuoteImage2.Visible = true;
                                    lbQuoteImage2.Visible = true;
                                    ddlQuoteImage2.Visible = true;
                                    imgQuote2.Visible = true;

                                    chkQuoteImage3.Visible = true;
                                    lbQuoteImage3.Visible = true;
                                    ddlQuoteImage3.Visible = true;
                                    imgQuote3.Visible = true;

                                    chkQuoteImage4.Visible = true;
                                    lbQuoteImage4.Visible = true;
                                    ddlQuoteImage4.Visible = true;
                                    imgQuote4.Visible = true;
                                }
                            }

                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count() > 0)
                            {
                                Int64 QuoteNumInProgress = 1;
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                    QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                ///Load Image
                                using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    if (QuoteInProgress != null && QuoteInProgress.FleetID.HasValue)
                                    {
                                        Int64 FleetId = (long)QuoteInProgress.FleetID;
                                        var ReturnImg = ImgService.GetFileWarehouseList("Fleet", FleetId).EntityList;
                                        int i = 0;
                                        if (ReturnImg != null)
                                        {
                                            ddlQuoteImage.Items.Clear();
                                            ddlQuoteImage2.Items.Clear();
                                            // Fix for SUP-183 (7281)
                                            ddlQuoteImage3.Items.Clear();
                                            ddlQuoteImage4.Items.Clear();
                                            foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImg)
                                            {
                                                string UWAFilename = string.Empty;
                                                long FileWarehouseID = 0;

                                                if (FWH.UWAFileName != null)
                                                    UWAFilename = FWH.UWAFileName.ToString();

                                                if (FWH.FileWarehouseID != null)
                                                    FileWarehouseID = FWH.FileWarehouseID;

                                                if (!string.IsNullOrEmpty(UWAFilename))
                                                {
                                                    if (ddlQuoteImage3 != null)
                                                        ddlQuoteImage3.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(UWAFilename), FileWarehouseID.ToString()));

                                                    if (ddlQuoteImage4 != null)
                                                        ddlQuoteImage4.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(UWAFilename), FileWarehouseID.ToString()));

                                                    ddlQuoteImage.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(UWAFilename), FileWarehouseID.ToString()));

                                                    if (ddlQuoteImage2 != null)
                                                        ddlQuoteImage2.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(UWAFilename), FileWarehouseID.ToString()));

                                                }
                                                i = i + 1;
                                            }
                                        }
                                    }
                                }

                                imgQuote1.Enabled = false;
                                imgQuote2.Enabled = false;
                                imgQuote3.Enabled = false;
                                imgQuote4.Enabled = false;

                                imgQuote1.ImageUrl = "~/App_Themes/Default/images/browse_button_disable.png";
                                imgQuote2.ImageUrl = "~/App_Themes/Default/images/browse_button_disable.png";
                                imgQuote3.ImageUrl = "~/App_Themes/Default/images/browse_button_disable.png";
                                imgQuote4.ImageUrl = "~/App_Themes/Default/images/browse_button_disable.png";

                                imgQuote1.ToolTip = "No Preview Available!";
                                imgQuote2.ToolTip = "No Preview Available!";
                                imgQuote3.ToolTip = "No Preview Available!";
                                imgQuote4.ToolTip = "No Preview Available!";

                                if (ddlQuoteImage.Items.Count > 0)
                                {
                                    imgQuote1.ImageUrl = "~/App_Themes/Default/images/browse_button.png";
                                    imgQuote1.Enabled = true;
                                    imgQuote1.ToolTip = "Preview";
                                }

                                if (ddlQuoteImage2.Items.Count > 0)
                                {
                                    imgQuote2.ImageUrl = "~/App_Themes/Default/images/browse_button.png";
                                    imgQuote2.Enabled = true;
                                    imgQuote2.ToolTip = "Preview";
                                }

                                if (ddlQuoteImage3.Items.Count > 0)
                                {
                                    imgQuote3.ImageUrl = "~/App_Themes/Default/images/browse_button.png";
                                    imgQuote3.Enabled = true;
                                    imgQuote3.ToolTip = "Preview";
                                }

                                if (ddlQuoteImage4.Items.Count > 0)
                                {
                                    imgQuote4.ImageUrl = "~/App_Themes/Default/images/browse_button.png";
                                    imgQuote4.Enabled = true;
                                    imgQuote4.ToolTip = "Preview";
                                }

                                if (Session["CQQuoteReport"] == null || (Session["CQQuoteReport"] != null && Session["CQQuoteReport"].ToString() == "QR"))
                                {
                                    #region "QuoteReport"

                                    Session.Remove("QuoteReportMain");

                                    CharterQuoteService.CQQuoteMain cqs = new CharterQuoteService.CQQuoteMain();
                                    if (QuoteInProgress != null && QuoteInProgress.CQMainID > 0)
                                    {
                                        using (CharterQuoteService.CharterQuoteServiceClient CQService = new CharterQuoteService.CharterQuoteServiceClient())
                                        {
                                            var objRetVal = CQService.GetCQQuoteMainByCQMainID(QuoteInProgress.CQMainID);
                                            if (objRetVal != null && objRetVal.EntityList != null && objRetVal.EntityList.Count() > 0)
                                            {
                                                cqs = (CQQuoteMain)objRetVal.EntityList[0];

                                                // Fix for UW-853 (7388)
                                                bool isRefreshNeed = false;

                                                Session["QuoteReportMain"] = cqs;

                                                if (cqs.CQQuoteAdditionalFees == null || (cqs.CQQuoteAdditionalFees != null && cqs.CQQuoteAdditionalFees.Count == 0))
                                                {
                                                    isRefreshNeed = true;
                                                }

                                                if (cqs.CQQuoteDetails == null || (cqs.CQQuoteDetails != null && cqs.CQQuoteDetails.Count == 0))
                                                {
                                                    isRefreshNeed = true;
                                                }

                                                BindQuoteAddFee(cqs, true);
                                                BindLegs(dgQuoteLeg, true);
                                            }
                                            else
                                            {
                                                #region Bind Customer Address Info for Quote

                                                if (FileRequest.CQCustomerID.HasValue && FileRequest.CQCustomerID > 0)
                                                {
                                                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        FlightPakMasterService.CQCustomer custObj = new FlightPakMasterService.CQCustomer();
                                                        custObj.CQCustomerID = Convert.ToInt64(FileRequest.CQCustomerID);

                                                        var ReturnValue = objMasterService.GetCQCustomerByCQCustomerID(custObj);

                                                        if (ReturnValue != null && ReturnValue.EntityList.Count > 0)
                                                        {
                                                            StringBuilder custAddr = new StringBuilder();

                                                            if (ReturnValue.EntityList[0].CQCustomerName != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].CQCustomerName);
                                                            if (ReturnValue.EntityList[0].BillingAddr1 != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr1.ToString().Trim());
                                                            if (ReturnValue.EntityList[0].BillingAddr2 != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr2.ToString().Trim());
                                                            if (ReturnValue.EntityList[0].BillingAddr3 != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr3.ToString().Trim());

                                                            if (ReturnValue.EntityList[0].BillingCity != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].BillingCity.ToString().Trim() + ", ");
                                                            if (ReturnValue.EntityList[0].BillingState != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].BillingState.ToString().Trim() + ", ");
                                                            if (ReturnValue.EntityList[0].BillingZip != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].BillingZip.ToString().Trim() + ", ");
                                                            if (ReturnValue.EntityList[0].CountryCD != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].CountryCD.ToString().Trim());

                                                            cqs.QuoteInformation = custAddr.ToString();
                                                        }
                                                    }
                                                }

                                                #endregion

                                                LoadQuoteCompanyDetail(ref cqs);
                                            }
                                        }
                                    }
                                    else
                                        EmptyQuoteDetails();


                                    #endregion

                                    QuoteRefreshYes_Click(null, null);
                                }
                                else
                                {
                                    #region "Invoice Report"

                                    chkDailyusageDiscount.Enabled = false;
                                    TabCharterQuote.Tabs[1].Selected = true;
                                    RadPageView pageview = (RadPageView)RadMultiPage1.FindPageViewByID("RadPageView2");
                                    pageview.Selected = true;
                                    Session.Remove("InvoiceReportMain");

                                    CharterQuoteService.CQInvoiceMain cqs = new CharterQuoteService.CQInvoiceMain();
                                    if (QuoteInProgress != null && QuoteInProgress.CQMainID > 0)
                                    {
                                        using (CharterQuoteService.CharterQuoteServiceClient CQService = new CharterQuoteService.CharterQuoteServiceClient())
                                        {
                                            var objRetVal = CQService.GetCQInvoiceMainByCQMainID(QuoteInProgress.CQMainID);
                                            if (objRetVal != null && objRetVal.EntityList != null && objRetVal.EntityList.Count() > 0)
                                            {
                                                cqs = (CQInvoiceMain)objRetVal.EntityList[0];

                                                // Fix for UW-853 (7388)
                                                bool isRefreshNeed = false;

                                                Session["InvoiceReportMain"] = cqs;
                                                if (cqs.CQInvoiceAdditionalFees == null || (cqs.CQInvoiceAdditionalFees != null && cqs.CQInvoiceAdditionalFees.Count == 0))
                                                {
                                                    isRefreshNeed = true;
                                                }

                                                if (cqs.CQInvoiceQuoteDetails == null || (cqs.CQInvoiceQuoteDetails != null && cqs.CQInvoiceQuoteDetails.Count == 0))
                                                {
                                                    isRefreshNeed = true;
                                                }

                                                if (cqs.CQInvoiceSummaries == null || (cqs.CQInvoiceSummaries != null && cqs.CQInvoiceSummaries.Count == 0))
                                                {
                                                    isRefreshNeed = true;
                                                }

                                                BindInvoiceAddFee(cqs, true);

                                            }
                                            else
                                            {
                                                #region Bind Customer Address Info for Invoice

                                                if (FileRequest.CQCustomerID.HasValue && FileRequest.CQCustomerID > 0)
                                                {
                                                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        FlightPakMasterService.CQCustomer custObj = new FlightPakMasterService.CQCustomer();
                                                        custObj.CQCustomerID = Convert.ToInt64(FileRequest.CQCustomerID);

                                                        var ReturnValue = objMasterService.GetCQCustomerByCQCustomerID(custObj);

                                                        if (ReturnValue != null && ReturnValue.EntityList.Count > 0)
                                                        {
                                                            StringBuilder custAddr = new StringBuilder();

                                                            if (ReturnValue.EntityList[0].CQCustomerName != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].CQCustomerName);
                                                            if (ReturnValue.EntityList[0].BillingAddr1 != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr1.ToString().Trim());
                                                            if (ReturnValue.EntityList[0].BillingAddr2 != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr2.ToString().Trim());
                                                            if (ReturnValue.EntityList[0].BillingAddr3 != null)
                                                                custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr3.ToString().Trim());

                                                            if (ReturnValue.EntityList[0].BillingCity != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].BillingCity.ToString().Trim() + ", ");
                                                            if (ReturnValue.EntityList[0].BillingState != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].BillingState.ToString().Trim() + ", ");
                                                            if (ReturnValue.EntityList[0].BillingZip != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].BillingZip.ToString().Trim() + ", ");
                                                            if (ReturnValue.EntityList[0].CountryCD != null)
                                                                custAddr.Append(ReturnValue.EntityList[0].CountryCD.ToString().Trim());
                                                           
                                                            if(string.IsNullOrEmpty(cqs.CustomerAddressInformation))
                                                                cqs.CustomerAddressInformation = custAddr.ToString();
                                                        }
                                                    }
                                                }

                                                #endregion

                                                LoadInvoiceCompanyDetail(ref cqs);
                                            }
                                        }
                                    }
                                    else
                                        EmptyInvoiceDetails();

                                    #endregion

                                    InvoiceRefreshYes_Click(null, null);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //ApplyPermissions();
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void TabCharterQuote_Clicked(object sender, RadTabStripEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["CQQuoteReport"] = e.Tab.Value;
                        Response.Redirect(Request.RawUrl, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        #region QuoteReport
        /// <summary>
        /// To Load Quote CompanyDetails
        /// </summary>
        /// <param name="cqs"></param>
        private void LoadQuoteCompanyDetail(ref CQQuoteMain cqs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqs))
            {
                IsQuoteSummary = false;
                LoadQuoteAdditionalFeeandSummary(ref cqs);
                LoadQuoteData(cqs);
                Session["QuoteReportMain"] = cqs;
                BindLegs(dgQuoteLeg, true);
                BindQuoteAddFee(cqs, true);

            }
        }

        /// <summary>
        /// To Load AdditionalFeeSummary
        /// </summary>
        /// <param name="QuoteData"></param>
        private void LoadQuoteAdditionalFeeandSummary(ref CharterQuoteService.CQQuoteMain QuoteData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(QuoteData))
            {
                //using (CharterQuoteServiceClient CQService = new CharterQuoteServiceClient())
                //{
                    #region Data
                    if (Session[Master.CQSessionKey] != null)
                    {
                        Int64 QuoteNumInProgress = 1;
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null)
                        {
                            CQMain cqMain = new CQMain();
                            cqMain.State = CQRequestEntityState.Modified;
                            cqMain.CQMainID = QuoteInProgress.CQMainID;
                            cqMain.CustomerID = QuoteInProgress.CustomerID;
                            cqMain.CQFileID = QuoteInProgress.CQFileID;
                            cqMain.FileNUM = QuoteInProgress.FileNUM;
                            cqMain.QuoteNUM = QuoteInProgress.QuoteNUM;
                            cqMain.QuoteID = QuoteInProgress.QuoteID;
                            cqMain.CQSource = QuoteInProgress.CQSource;
                            cqMain.VendorID = QuoteInProgress.VendorID;
                            cqMain.FleetID = QuoteInProgress.FleetID;
                            cqMain.AircraftID = QuoteInProgress.AircraftID;
                            cqMain.IsCustomerCQ = QuoteInProgress.IsCustomerCQ;
                            cqMain.FederalTax = QuoteInProgress.FederalTax;
                            cqMain.QuoteRuralTax = QuoteInProgress.QuoteRuralTax;
                            cqMain.IsTaxable = QuoteInProgress.IsTaxable;
                            cqMain.PassengerRequestorID = QuoteInProgress.PassengerRequestorID;
                            cqMain.RequestorName = QuoteInProgress.RequestorName;
                            cqMain.RequestorPhoneNUM = QuoteInProgress.RequestorPhoneNUM;
                            cqMain.IsSubmitted = QuoteInProgress.IsSubmitted;
                            cqMain.LastSubmitDT = QuoteInProgress.LastSubmitDT;
                            cqMain.IsAccepted = QuoteInProgress.IsAccepted;
                            cqMain.LastAcceptDT = QuoteInProgress.LastAcceptDT;
                            cqMain.RejectDT = QuoteInProgress.RejectDT;
                            cqMain.Comments = QuoteInProgress.Comments;
                            cqMain.IsFinancialWarning = QuoteInProgress.IsFinancialWarning;
                            cqMain.LiveTrip = QuoteInProgress.LiveTrip;
                            cqMain.MinimumDayUsageTotal = QuoteInProgress.MinimumDayUsageTotal;
                            cqMain.BillingCrewRONTotal = QuoteInProgress.BillingCrewRONTotal;
                            cqMain.AdditionalCrewTotal = QuoteInProgress.AdditionalCrewTotal;
                            cqMain.AdditionalCrewRONTotal1 = QuoteInProgress.AdditionalCrewRONTotal1;
                            cqMain.StdCrewRONTotal = QuoteInProgress.StdCrewRONTotal;
                            cqMain.AdditionalCrewRONTotal2 = QuoteInProgress.AdditionalCrewRONTotal2;
                            cqMain.WaitTMTotal = QuoteInProgress.WaitTMTotal;
                            cqMain.WaitChgTotal = QuoteInProgress.WaitChgTotal;
                            cqMain.HomebaseID = QuoteInProgress.HomebaseID;
                            cqMain.ClientID = QuoteInProgress.ClientID;
                            cqMain.CQLostBusinessID = QuoteInProgress.CQLostBusinessID;
                            cqMain.LastUpdUID = QuoteInProgress.LastUpdUID;
                            cqMain.LastUpdTS = QuoteInProgress.LastUpdTS;
                            cqMain.IsDeleted = QuoteInProgress.IsDeleted;
                            cqMain.StandardCrewDOM = QuoteInProgress.StandardCrewDOM;
                            cqMain.StandardCrewINTL = QuoteInProgress.StandardCrewINTL;
                            cqMain.DaysTotal = QuoteInProgress.DaysTotal;
                            cqMain.FlightHoursTotal = QuoteInProgress.FlightHoursTotal;
                            cqMain.AverageFlightHoursTotal = QuoteInProgress.AverageFlightHoursTotal;
                            cqMain.AdjAmtTotal = QuoteInProgress.AdjAmtTotal;
                            cqMain.UsageAdjTotal = QuoteInProgress.UsageAdjTotal;
                            cqMain.IsOverrideUsageAdj = QuoteInProgress.IsOverrideUsageAdj;
                            cqMain.SegmentFeeTotal = QuoteInProgress.SegmentFeeTotal;
                            cqMain.IsOverrideSegmentFee = QuoteInProgress.IsOverrideSegmentFee;
                            cqMain.LandingFeeTotal = QuoteInProgress.LandingFeeTotal;
                            cqMain.IsOverrideLandingFee = QuoteInProgress.IsOverrideLandingFee;
                            cqMain.AdditionalFeeTotalD = QuoteInProgress.AdditionalFeeTotalD;
                            cqMain.FlightChargeTotal = QuoteInProgress.FlightChargeTotal;
                            cqMain.SubtotalTotal = QuoteInProgress.SubtotalTotal;
                            cqMain.DiscountPercentageTotal = QuoteInProgress.DiscountPercentageTotal;
                            cqMain.IsOverrideDiscountPercentage = QuoteInProgress.IsOverrideDiscountPercentage;
                            cqMain.DiscountAmtTotal = QuoteInProgress.DiscountAmtTotal;
                            cqMain.IsOverrideDiscountAMT = QuoteInProgress.IsOverrideDiscountAMT;
                            cqMain.TaxesTotal = QuoteInProgress.TaxesTotal;
                            cqMain.IsOverrideTaxes = QuoteInProgress.IsOverrideTaxes;
                            cqMain.MarginalPercentTotal = QuoteInProgress.MarginalPercentTotal;
                            cqMain.MarginTotal = QuoteInProgress.MarginTotal;
                            cqMain.QuoteTotal = QuoteInProgress.QuoteTotal;
                            cqMain.IsOverrideTotalQuote = QuoteInProgress.IsOverrideTotalQuote;
                            cqMain.CostTotal = QuoteInProgress.CostTotal;
                            cqMain.IsOverrideCost = QuoteInProgress.IsOverrideCost;
                            cqMain.PrepayTotal = QuoteInProgress.PrepayTotal;
                            cqMain.RemainingAmtTotal = QuoteInProgress.RemainingAmtTotal;
                            cqMain.TripID = QuoteInProgress.TripID;
                            cqMain.CustomUsageAdj = QuoteInProgress.UsageAdjTotal;
                            cqMain.CustomAdditionalFees = QuoteInProgress.AdditionalFeeTotalD;
                            cqMain.CustomLandingFee = QuoteInProgress.LandingFeeTotal;
                            cqMain.CustomSegmentFee = QuoteInProgress.SegmentFeeTotal;
                            cqMain.CustomStdCrewRON = QuoteInProgress.StdCrewRONTotal;
                            cqMain.CustomAdditionalCrew = QuoteInProgress.AdditionalCrewTotal;
                            //cqMain.CustomAdditionalCrewRON = QuoteInProgress.add; //Doubt Field
                            cqMain.CustomWaitingChg = QuoteInProgress.WaitChgTotal;
                            cqMain.CustomFlightChg = QuoteInProgress.FlightChargeTotal;
                            cqMain.CustomSubTotal = QuoteInProgress.SubtotalTotal;
                            cqMain.CustomDiscountAmt = QuoteInProgress.DiscountAmtTotal;
                            cqMain.CustomTaxes = QuoteInProgress.TaxesTotal;
                            cqMain.CustomTotalQuote = QuoteInProgress.QuoteTotal;
                            cqMain.CustomPrepay = QuoteInProgress.PrepayTotal;
                            cqMain.CustomRemainingAmt = QuoteInProgress.RemainingAmtTotal;
                            if (IsQuoteSummary)
                            {
                                cqMain.IsPrintUsageAdj = QuoteInProgress.IsPrintUsageAdj;
                                cqMain.UsageAdjDescription = QuoteInProgress.UsageAdjDescription;
                                cqMain.IsPrintAdditonalFees = QuoteInProgress.IsPrintAdditonalFees;
                                cqMain.AdditionalFeeDescription = QuoteInProgress.AdditionalFeeDescription;
                                cqMain.AdditionalFeesTax = QuoteInProgress.AdditionalFeesTax;
                                cqMain.PositioningDescriptionINTL = QuoteInProgress.PositioningDescriptionINTL;
                                cqMain.IsAdditionalFees = QuoteInProgress.IsAdditionalFees;
                                cqMain.IsPrintLandingFees = QuoteInProgress.IsPrintLandingFees;
                                cqMain.LandingFeeDescription = QuoteInProgress.LandingFeeDescription;
                                cqMain.IsPrintSegmentFee = QuoteInProgress.IsPrintSegmentFee;
                                cqMain.SegmentFeeDescription = QuoteInProgress.SegmentFeeDescription;
                                cqMain.IsPrintStdCrew = QuoteInProgress.IsPrintStdCrew;
                                cqMain.StdCrewROMDescription = QuoteInProgress.StdCrewROMDescription;
                                cqMain.IsPrintAdditionalCrew = QuoteInProgress.IsPrintAdditionalCrew;
                                cqMain.AdditionalCrewDescription = QuoteInProgress.AdditionalCrewDescription;
                                cqMain.IsAdditionalCrewRON = QuoteInProgress.IsAdditionalCrewRON;
                                cqMain.AdditionalDescriptionCrewRON = QuoteInProgress.AdditionalDescriptionCrewRON;
                                cqMain.IsPrintWaitingChg = QuoteInProgress.IsPrintWaitingChg;
                                cqMain.WaitChgDescription = QuoteInProgress.WaitChgDescription;
                                cqMain.IsPrintFlightChg = QuoteInProgress.IsPrintFlightChg;
                                cqMain.FlightChgDescription = QuoteInProgress.FlightChgDescription;
                                cqMain.IsPrintSubtotal = QuoteInProgress.IsPrintSubtotal;
                                cqMain.SubtotalDescription = QuoteInProgress.SubtotalDescription;
                                cqMain.IsPrintDiscountAmt = QuoteInProgress.IsPrintDiscountAmt;
                                cqMain.DescriptionDiscountAmt = QuoteInProgress.DescriptionDiscountAmt;
                                cqMain.IsPrintTaxes = QuoteInProgress.IsPrintTaxes;
                                cqMain.TaxesDescription = QuoteInProgress.TaxesDescription;
                                cqMain.IsPrintInvoice = QuoteInProgress.IsPrintInvoice;
                                cqMain.InvoiceDescription = QuoteInProgress.InvoiceDescription;
                                cqMain.ReportQuote = QuoteInProgress.ReportQuote;
                                cqMain.IsPrintPay = QuoteInProgress.IsPrintPay;
                                cqMain.PrepayDescription = QuoteInProgress.PrepayDescription;
                                cqMain.IsPrintRemaingAMT = QuoteInProgress.IsPrintRemaingAMT;
                                cqMain.RemainingAmtDescription = QuoteInProgress.RemainingAmtDescription;
                                cqMain.IsInActive = QuoteInProgress.IsInActive;
                                cqMain.IsDailyTaxAdj = QuoteInProgress.IsDailyTaxAdj;
                                cqMain.IsLandingFeeTax = QuoteInProgress.IsLandingFeeTax;
                                cqMain.FeeGroupID = QuoteInProgress.FeeGroupID;
                                cqMain.IsPrintTotalQuote = QuoteInProgress.IsPrintTotalQuote;
                                cqMain.TotalQuoteDescription = QuoteInProgress.TotalQuoteDescription;
                            }
                            else
                            {

                                cqMain.UsageAdjDescription = DailyUsageAdjustment;
                                cqMain.AdditionalFeeDescription = AdditionalFees;
                                cqMain.LandingFeeDescription = LandingFees;
                                cqMain.SegmentFeeDescription = SegmentFees;
                                cqMain.FlightChgDescription = FlightCharge;
                                cqMain.SubtotalDescription = SubTotal;
                                cqMain.DescriptionDiscountAmt = DiscountAmount;
                                cqMain.TaxesDescription = Taxes;
                                cqMain.TotalQuoteDescription = TotalQuote;
                                cqMain.PrepayDescription = Prepaid;
                                cqMain.RemainingAmtDescription = RemainingAmountDue;

                                if (FileRequest.HomebaseID.HasValue)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId((long)FileRequest.HomebaseID);
                                        if (CompanyList != null && CompanyList.EntityList != null && CompanyList.EntityList.Count() > 0)
                                        {

                                            QuoteData.CompanyInformation = string.IsNullOrEmpty(QuoteData.CompanyInformation) ? CompanyList.EntityList[0].ChtQuoteCompany : QuoteData.CompanyInformation;
                                            QuoteData.ReportHeader = string.IsNullOrEmpty(QuoteData.ReportHeader) ? CompanyList.EntityList[0].ChtQouteHeaderDetailRpt : QuoteData.ReportHeader;
                                            QuoteData.ReportFooter = string.IsNullOrEmpty(QuoteData.ReportFooter) ? CompanyList.EntityList[0].ChtQouteFooterDetailRpt : QuoteData.ReportFooter;

                                            QuoteData.IsPrintDetail = (!QuoteData.IsPrintDetail.HasValue ? CompanyList.EntityList[0].IsChangeQueueDetail : QuoteData.IsPrintDetail) ?? false;
                                            QuoteData.IsPrintFees = (!QuoteData.IsPrintFees.HasValue ? CompanyList.EntityList[0].IsQuoteChangeQueueFees : QuoteData.IsPrintFees) ?? false;
                                            QuoteData.IsPrintSum = (!QuoteData.IsPrintSum.HasValue ? CompanyList.EntityList[0].IsQuoteChangeQueueSum : QuoteData.IsPrintSum) ?? false;

                                            QuoteData.ImagePosition = (QuoteData.ImagePosition == 0 ? CompanyList.EntityList[0].ImagePosition : QuoteData.ImagePosition) ?? 0;
                                            
                                            cqMain.UsageAdjDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteMinimumUse2FeeDepositAdj) ? CompanyList.EntityList[0].QuoteMinimumUse2FeeDepositAdj : DailyUsageAdjustment;

                                            if (CompanyList.EntityList[0].IsQuotePrepaidMinimumUsage2FeeAdj.HasValue)
                                                cqMain.IsPrintUsageAdj = CompanyList.EntityList[0].IsQuotePrepaidMinimumUsage2FeeAdj;

                                            cqMain.AdditionalFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Additional2FeeDefault) ? CompanyList.EntityList[0].Additional2FeeDefault : AdditionalFees;

                                            if (CompanyList.EntityList[0].IsQuoteAdditional2FeePrint.HasValue)
                                                cqMain.IsPrintAdditonalFees = CompanyList.EntityList[0].IsQuoteAdditional2FeePrint;

                                            cqMain.LandingFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteLandingFeeDefault) ? CompanyList.EntityList[0].QuoteLandingFeeDefault : LandingFees;

                                            if (CompanyList.EntityList[0].IsQuoteFlight2LandingFeePrint.HasValue)
                                                cqMain.IsPrintLandingFees = CompanyList.EntityList[0].IsQuoteFlight2LandingFeePrint;

                                            cqMain.SegmentFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSegment2FeeDeposit) ? CompanyList.EntityList[0].QuoteSegment2FeeDeposit : SegmentFees;

                                            if (CompanyList.EntityList[0].IsQuotePrepaidSegement2Fee.HasValue)
                                                cqMain.IsPrintSegmentFee = CompanyList.EntityList[0].IsQuotePrepaidSegement2Fee;

                                            if (CompanyList.EntityList[0].IsQuoteFlight2ChargePrepaid.HasValue)
                                                cqMain.IsPrintFlightChg = CompanyList.EntityList[0].IsQuoteFlight2ChargePrepaid;

                                            cqMain.FlightChgDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Deposit2FlightCharge) ? CompanyList.EntityList[0].Deposit2FlightCharge : FlightCharge;

                                            if (CompanyList.EntityList[0].IsQuoteSubtotal2Print.HasValue)
                                                cqMain.IsPrintSubtotal = CompanyList.EntityList[0].IsQuoteSubtotal2Print;

                                            cqMain.SubtotalDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Quote2Subtotal) ? CompanyList.EntityList[0].Quote2Subtotal : SubTotal;

                                            if (CompanyList.EntityList[0].IsQuoteDiscount2AmountPrepaid.HasValue)
                                                cqMain.IsPrintDiscountAmt = CompanyList.EntityList[0].IsQuoteDiscount2AmountPrepaid;

                                            cqMain.DescriptionDiscountAmt = !string.IsNullOrEmpty(CompanyList.EntityList[0].Discount2AmountDeposit) ? CompanyList.EntityList[0].Discount2AmountDeposit : DiscountAmount;

                                            if (CompanyList.EntityList[0].IsQuoteTaxes2Print.HasValue)
                                                cqMain.IsPrintTaxes = CompanyList.EntityList[0].IsQuoteTaxes2Print;
                                            cqMain.TaxesDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Quote2TaxesDefault) ? CompanyList.EntityList[0].Quote2TaxesDefault : Taxes;
                                            if (CompanyList.EntityList[0].IsQuoteInvoice2Print.HasValue)
                                                cqMain.IsPrintTotalQuote = CompanyList.EntityList[0].IsQuoteInvoice2Print;
                                            cqMain.TotalQuoteDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteInvoice2Default) ? CompanyList.EntityList[0].QuoteInvoice2Default : TotalQuote;
                                            if (CompanyList.EntityList[0].IsQuote2PrepayPrint.HasValue)
                                                cqMain.IsPrintPay = CompanyList.EntityList[0].IsQuote2PrepayPrint;
                                            cqMain.PrepayDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuotePrepay2Default) ? CompanyList.EntityList[0].QuotePrepay2Default : Prepaid;
                                            if (CompanyList.EntityList[0].IsQuoteRemaining2AmtPrint.HasValue)
                                                cqMain.IsPrintRemaingAMT = CompanyList.EntityList[0].IsQuoteRemaining2AmtPrint;
                                            cqMain.RemainingAmtDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteRemaining2AmountDefault) ? CompanyList.EntityList[0].QuoteRemaining2AmountDefault : RemainingAmountDue;
                                        }
                                    }
                                }

                                // Append the Sales Person's Name and Phone at Company Information Property for Quote Report.
                                string salesPerson = string.Empty;
                                if (FileRequest.SalesPersonID.HasValue)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var RetValue = objMasterService.GetSalesPersonList().EntityList.Where(x => x.SalesPersonID == FileRequest.SalesPersonID);
                                        if (RetValue != null && RetValue.Count() > 0)
                                        {
                                            List<FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                            SalesPersonList = (List<FlightPakMasterService.GetSalesPerson>)RetValue.ToList();

                                            if (SalesPersonList[0].Name != null && !string.IsNullOrEmpty(SalesPersonList[0].Name))
                                                salesPerson = Environment.NewLine + "REPRESENTATIVE: " + SalesPersonList[0].Name;
                                            if (SalesPersonList[0].PhoneNUM != null && !string.IsNullOrEmpty(SalesPersonList[0].PhoneNUM))
                                                salesPerson += Environment.NewLine + "Phone No: " + SalesPersonList[0].PhoneNUM;

                                            QuoteData.CompanyInformation += salesPerson;
                                        }
                                    }
                                }
                            }

                            QuoteData.CQMain = cqMain;
                            QuoteData.CQMainID = cqMain.CQMainID;

                            if (QuoteInProgress.CQFleetChargeDetails != null)
                            {
                                if (QuoteData.CQQuoteAdditionalFees == null)
                                    QuoteData.CQQuoteAdditionalFees = new List<CQQuoteAdditionalFee>();
                                else
                                {
                                    foreach (CQQuoteAdditionalFee quotefee in QuoteData.CQQuoteAdditionalFees.ToList())
                                    {
                                        if (quotefee.CQQuoteAdditionalFeesID != 0)
                                        {
                                            quotefee.IsDeleted = true;
                                            quotefee.State = CQRequestEntityState.Deleted;
                                        }
                                        else
                                            QuoteData.CQQuoteAdditionalFees.Remove(quotefee);
                                    }
                                }

                                foreach (CQFleetChargeDetail fltCharge in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                                {
                                    CQQuoteAdditionalFee quotefee = QuoteData.CQQuoteAdditionalFees.Where(x => x.IsDeleted == true && x.OrderNUM == fltCharge.OrderNUM).FirstOrDefault();
                                    CQQuoteAdditionalFee addfee = new CQQuoteAdditionalFee();
                                    addfee.State = CQRequestEntityState.Added;
                                    addfee.IsDeleted = false;
                                    addfee.OrderNUM = fltCharge.OrderNUM;
                                    addfee.CQQuoteFeeDescription = fltCharge.CQFlightChargeDescription;
                                    addfee.QuoteAmount = fltCharge.FeeAMT;
                                    addfee.IsPrintable = QuoteData.IsPrintSum.HasValue ? (bool)QuoteData.IsPrintSum : false; // UserPrincipal.Identity._fpSettings._IsQuotePrintSum != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuotePrintSum : false;
                                    addfee.Quantity = fltCharge.Quantity;
                                    addfee.ChargeUnit = fltCharge.ChargeUnit;
                                    if (quotefee != null)
                                    {
                                        addfee.IsPrintable = quotefee.IsPrintable.HasValue ? (bool)quotefee.IsPrintable : (QuoteData.IsPrintSum.HasValue ? (bool)QuoteData.IsPrintSum : false);
                                        addfee.CQQuoteFeeDescription = (fltCharge.CQFlightChargeDescription != quotefee.CQQuoteFeeDescription) ? quotefee.CQQuoteFeeDescription : fltCharge.CQFlightChargeDescription;
                                    }
                                    QuoteData.CQQuoteAdditionalFees.Add(addfee);
                                }
                            }

                            if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                            {
                                if (QuoteData.CQQuoteDetails == null)
                                    QuoteData.CQQuoteDetails = new List<CQQuoteDetail>();
                                else
                                {
                                    foreach (CQQuoteDetail quoteDet in QuoteData.CQQuoteDetails.ToList())
                                    {
                                        if (quoteDet.CQQuoteDetailID != 0)
                                        {
                                            quoteDet.IsDeleted = true;
                                            quoteDet.State = CQRequestEntityState.Deleted;
                                        }
                                        else
                                            QuoteData.CQQuoteDetails.Remove(quoteDet);
                                    }
                                }

                                foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).ToList())
                                {
                                    CQQuoteDetail quoteDet = QuoteData.CQQuoteDetails.Where(x => x.IsDeleted == true && x.CQLegID == Leg.CQLegID).FirstOrDefault();
                                    CQQuoteDetail addQuoteDetail = new CQQuoteDetail();
                                    addQuoteDetail.State = CQRequestEntityState.Added;
                                    addQuoteDetail.IsDeleted = false;
                                    addQuoteDetail.LegNum = Leg.LegNUM;
                                    addQuoteDetail.DepartureDTTMLocal = Leg.DepartureDTTMLocal;
                                    addQuoteDetail.ArrivalDTTMLocal = Leg.ArrivalDTTMLocal;
                                    addQuoteDetail.Airport1 = Leg.Airport1;
                                    addQuoteDetail.Airport = Leg.Airport;
                                    addQuoteDetail.DAirportID = Leg.DAirportID;
                                    addQuoteDetail.AAirportID = Leg.AAirportID;
                                    addQuoteDetail.PassengerTotal = Leg.PassengerTotal;
                                    addQuoteDetail.FlightHours = Leg.ElapseTM;
                                    addQuoteDetail.Distance = Leg.Distance;
                                    addQuoteDetail.FlightCharge = Leg.ChargeTotal;
                                    addQuoteDetail.IsTaxable = Leg.IsTaxable.HasValue ? Leg.IsTaxable : false;
                                    addQuoteDetail.IsPrintable = QuoteData.IsPrintDetail.HasValue ? (bool)QuoteData.IsPrintDetail : false;// UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint : false;
                                    if (quoteDet != null)
                                    {
                                        addQuoteDetail.IsPrintable = quoteDet.IsPrintable.HasValue ? (bool)quoteDet.IsPrintable : (QuoteData.IsPrintDetail.HasValue ? (bool)QuoteData.IsPrintDetail : false);
                                    }
                                    QuoteData.CQQuoteDetails.Add(addQuoteDetail);
                                }
                            }
                        }
                    }
                    #endregion
                //}
            }
        }

        /// <summary>
        /// To Empty Quote Details
        /// </summary>
        private void EmptyQuoteDetails()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                dgQuoteAddFee.DataSource = string.Empty;
                dgQuoteAddFee.DataBind();
                dgQuoteLeg.DataSource = string.Empty;
                dgQuoteLeg.DataBind();
                tbCharterCompanyInfo.Text = string.Empty;
                tbCharterCompanyInfo.Enabled = false;
                tbCustomerAddressInformation.Text = string.Empty;
                tbCustomerAddressInformation.Enabled = false;
                tbHeaderInfo.Text = string.Empty;
                tbHeaderInfo.Enabled = false;
                ddlQuoteCustomFooter.SelectedIndex = 0;
                tbCustomerFooter.Text = string.Empty;
                tbCustomerFooter.Enabled = false;
                chkPrint.Checked = false;
                chkPrint.Enabled = false;
                radNone.Checked = false;
                radNone.Enabled = false;
                radEndOfReport.Checked = false;
                radEndOfReport.Enabled = false;
                radTailNum.Checked = false;
                radTailNum.Enabled = false;
                radSepPage.Checked = false;
                radSepPage.Enabled = false;
                ddlQuoteImage.SelectedIndex = 0;
                chkQuoteImage1.Checked = false;

                // Fix for SUP-183 (7281)
                if (UserPrincipal.Identity._fpSettings._ImageCnt.HasValue)
                {
                    if (UserPrincipal.Identity._fpSettings._ImageCnt == 2)
                    { 
                        ddlQuoteImage2.SelectedIndex = 0;
                        chkQuoteImage2.Checked = false;
                    }

                    else if (UserPrincipal.Identity._fpSettings._ImageCnt == 3)
                    {
                        ddlQuoteImage2.SelectedIndex = 0;
                        ddlQuoteImage3.SelectedIndex = 0;
                        chkQuoteImage2.Checked = false;
                        chkQuoteImage3.Checked = false;
                    }
                    else if (UserPrincipal.Identity._fpSettings._ImageCnt == 4)
                    {
                        ddlQuoteImage2.SelectedIndex = 0;
                        ddlQuoteImage3.SelectedIndex = 0;
                        ddlQuoteImage4.SelectedIndex = 0;
                        chkQuoteImage2.Checked = false;
                        chkQuoteImage3.Checked = false;
                        chkQuoteImage4.Checked = false;
                    }
                }
                chkQuoteAddPrintSummary.Checked = false;
                chkQuoteAddPrintSummary.Enabled = false;
                chkDailyUsageAdjustment.Checked = false;
                chkDailyUsageAdjustment.Enabled = false;
                chkAdditionalFee.Checked = false;
                chkAdditionalFee.Enabled = false;

                chkLandingFee.Checked = false;
                chkLandingFee.Enabled = false;

                chkSegmentFee.Checked = false;
                chkSegmentFee.Enabled = false;
                chkFlightCharge.Checked = false;
                chkFlightCharge.Enabled = false;
                chkSubTotal.Checked = false;
                chkSubTotal.Enabled = false;
                chkDiscountAmount.Checked = false;
                chkDiscountAmount.Enabled = false;
                chkTaxes.Checked = false;
                chkTaxes.Enabled = false;
                chkTotalQuote.Checked = false;
                chkTotalQuote.Enabled = false;
                chkPrepaid.Checked = false;
                chkPrepaid.Enabled = false;
                chkRemainingAmt.Checked = false;
                chkRemainingAmt.Enabled = false;
                tbDailyDesc.Text = string.Empty;
                tbDailyDesc.Enabled = false;
                tbDailyQuoteAmount.Text = string.Empty;
                tbAdditionalFee.Text = string.Empty;
                tbAdditionalFee.Enabled = false;
                tbAdditionalFeee.Text = string.Empty;
                tbSegmentFee.Text = string.Empty;
                tbSegmentFee.Enabled = false;
                tbSegmentFeee.Text = string.Empty;
                tbFlightCharge.Text = string.Empty;
                tbFlightCharge.Enabled = false;
                tbFlightChargee.Text = string.Empty;
                tbSubTotal.Text = string.Empty;
                tbSubTotal.Enabled = false;
                tbSubtotall.Text = string.Empty;
                tbDiscountAmount.Text = string.Empty;
                tbDiscountAmount.Enabled = false;
                tbDiscountAmountt.Text = string.Empty;
                tbTax.Text = string.Empty;
                tbTax.Enabled = false;
                tbTaxx.Text = string.Empty;
                tbTotalQuote.Text = string.Empty;
                tbTotalQuote.Enabled = false;
                tbTotalQuotee.Text = string.Empty;
                tbPrepaid.Text = string.Empty;
                tbPrepaid.Enabled = false;
                tbPrepaidd.Text = string.Empty;
                tbRemainingAmt.Text = string.Empty;
                tbRemainingAmt.Enabled = false;
                tbRemainingamtt.Text = string.Empty;

            }
        }

        /// <summary>
        /// Quote Summary Check Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkQuoteSummary_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkQuoteSummary.Checked == true)
                        {
                            chkDailyUsageAdjustment.Checked = true;
                            chkAdditionalFee.Checked = true;
                            chkLandingFee.Checked = true;
                            chkSegmentFee.Checked = true;
                            chkFlightCharge.Checked = true;
                            chkSubTotal.Checked = true;
                            chkDiscountAmount.Checked = true;
                            chkTaxes.Checked = true;
                            chkTotalQuote.Checked = true;
                            chkPrepaid.Checked = true;
                            chkRemainingAmt.Checked = true;
                        }
                        else
                        {
                            chkDailyUsageAdjustment.Checked = false;
                            chkAdditionalFee.Checked = false;
                            chkLandingFee.Checked = false;
                            chkSegmentFee.Checked = false;
                            chkFlightCharge.Checked = false;
                            chkSubTotal.Checked = false;
                            chkDiscountAmount.Checked = false;
                            chkTaxes.Checked = false;
                            chkTotalQuote.Checked = false;
                            chkPrepaid.Checked = false;
                            chkRemainingAmt.Checked = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// To Load Quote Data
        /// </summary>
        /// <param name="QuoteData"></param>
        private void LoadQuoteData(CharterQuoteService.CQQuoteMain QuoteData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(QuoteData))
            {
                FileRequest = (CQFile)Session[Master.CQSessionKey];
                if (QuoteData != null && QuoteData.CQMain != null)
                {
                    tbCharterCompanyInfo.Text = string.IsNullOrEmpty(QuoteData.CompanyInformation) ? string.Empty : QuoteData.CompanyInformation;

                    tbCustomerAddressInformation.Text = string.IsNullOrEmpty(QuoteData.QuoteInformation) ? string.Empty : QuoteData.QuoteInformation;

                    tbHeaderInfo.Text = string.IsNullOrEmpty(QuoteData.ReportHeader) ? string.Empty : QuoteData.ReportHeader;
                    tbCustomerFooter.Text = string.IsNullOrEmpty(QuoteData.ReportFooter) ? string.Empty : QuoteData.ReportFooter;
                    
                    if (QuoteData.ReportFooter != null)
                    {
                        if (FileRequest != null && FileRequest.HomebaseID.HasValue)
                        {
                            ddlQuoteCustomFooter.Items.Clear();
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetCQMessage("Q", (long)FileRequest.HomebaseID).EntityList;
                                int i = 0;
                                if (RetValue != null && RetValue.Count > 0)
                                {
                                    foreach (FlightPakMasterService.CQMessage fwh in RetValue)
                                    {
                                        string encodedDescription = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.MessageDescription); 
                                        string encodedID = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.CQMessageID.ToString());

                                        ddlQuoteCustomFooter.Items.Insert(i, new ListItem((encodedDescription),encodedID));
                                        i = i + 1;
                                    }
                                }
                            }
                        }
                        if (FileRequest != null && FileRequest.Company != null && FileRequest.Company.ChtQouteFooterDetailRpt != null)
                        {
                            string Item = FileRequest.Company.ChtQouteFooterDetailRpt;
                            if (ddlQuoteCustomFooter.Items.Count > 0)
                            {
                                if (ddlQuoteCustomFooter.Items.FindByText(Item) != null)
                                {
                                    ddlQuoteCustomFooter.Items.FindByText(Item).Selected = true;
                                    tbCustomerFooter.Text = string.IsNullOrEmpty(tbCustomerFooter.Text) ? ddlQuoteCustomFooter.SelectedItem.ToString() : tbCustomerFooter.Text;
                                }
                            }
                        }
                    }

                    chkPrint.Checked = QuoteData.IsPrintDetail.HasValue && QuoteData.IsPrintDetail.Value;

                    chkQuoteAddPrintSummary.Checked = QuoteData.IsPrintFees.HasValue && QuoteData.IsPrintFees.Value;

                    chkQuoteSummary.Checked = QuoteData.IsPrintSum.HasValue && QuoteData.IsPrintSum.Value;
                    
                    if (QuoteData.ImagePosition.HasValue)
                    {
                        radNone.Checked = (QuoteData.ImagePosition == 1);

                        radEndOfReport.Checked = (QuoteData.ImagePosition == 2);

                        radTailNum.Checked = (QuoteData.ImagePosition == 3);

                        radSepPage.Checked = (QuoteData.ImagePosition == 4);
                    }

                    SetQuoteSummaryStatus(chkQuoteSummary.Checked);
                    
                    chkDailyUsageAdjustment.Checked = QuoteData.CQMain.IsPrintUsageAdj.HasValue && QuoteData.CQMain.IsPrintUsageAdj.Value;

                    tbDailyDesc.Text = string.IsNullOrEmpty(QuoteData.CQMain.UsageAdjDescription) ? string.Empty : QuoteData.CQMain.UsageAdjDescription;

                    tbDailyQuoteAmount.Text = (QuoteData.CQMain.CustomUsageAdj.HasValue) ? QuoteData.CQMain.CustomUsageAdj.ToString() : string.Empty;

                    chkAdditionalFee.Checked = QuoteData.CQMain.IsPrintAdditonalFees.HasValue && QuoteData.CQMain.IsPrintAdditonalFees.Value;

                    tbAdditionalFee.Text = string.IsNullOrEmpty(QuoteData.CQMain.AdditionalFeeDescription) ? string.Empty : QuoteData.CQMain.AdditionalFeeDescription;

                    tbAdditionalFeee.Text = (QuoteData.CQMain.CustomAdditionalFees.HasValue) ? QuoteData.CQMain.CustomAdditionalFees.ToString() : string.Empty;

                    chkLandingFee.Checked = QuoteData.CQMain.IsPrintLandingFees.HasValue && QuoteData.CQMain.IsPrintLandingFees.Value;

                    tbLandingFee.Text = string.IsNullOrEmpty(QuoteData.CQMain.LandingFeeDescription) ? string.Empty : QuoteData.CQMain.LandingFeeDescription;

                    tbLandingFeee.Text = (QuoteData.CQMain.CustomLandingFee.HasValue) ? QuoteData.CQMain.CustomLandingFee.ToString() : string.Empty;

                    chkSegmentFee.Checked = QuoteData.CQMain.IsPrintSegmentFee.HasValue && QuoteData.CQMain.IsPrintSegmentFee.Value;

                    tbSegmentFee.Text = string.IsNullOrEmpty(QuoteData.CQMain.SegmentFeeDescription) ? string.Empty : QuoteData.CQMain.SegmentFeeDescription;

                    tbSegmentFeee.Text = (QuoteData.CQMain.CustomSegmentFee.HasValue) ? QuoteData.CQMain.CustomSegmentFee.ToString() : string.Empty;

                    chkFlightCharge.Checked = QuoteData.CQMain.IsPrintFlightChg.HasValue && QuoteData.CQMain.IsPrintFlightChg.Value;

                    tbFlightCharge.Text = string.IsNullOrEmpty(QuoteData.CQMain.FlightChgDescription) ? string.Empty : QuoteData.CQMain.FlightChgDescription;

                    tbFlightChargee.Text = (QuoteData.CQMain.CustomFlightChg.HasValue) ? QuoteData.CQMain.CustomFlightChg.ToString() : string.Empty;

                    chkSubTotal.Checked = QuoteData.CQMain.IsPrintSubtotal.HasValue && QuoteData.CQMain.IsPrintSubtotal.Value;

                    tbSubTotal.Text = string.IsNullOrEmpty(QuoteData.CQMain.SubtotalDescription) ? string.Empty : QuoteData.CQMain.SubtotalDescription;

                    tbSubtotall.Text = (QuoteData.CQMain.CustomSubTotal.HasValue) ? QuoteData.CQMain.CustomSubTotal.ToString() : string.Empty;

                    chkDiscountAmount.Checked = QuoteData.CQMain.IsPrintDiscountAmt.HasValue && QuoteData.CQMain.IsPrintDiscountAmt.Value;

                    tbDiscountAmount.Text = string.IsNullOrEmpty(QuoteData.CQMain.DescriptionDiscountAmt) ? string.Empty : QuoteData.CQMain.DescriptionDiscountAmt;

                    tbDiscountAmountt.Text = (QuoteData.CQMain.CustomDiscountAmt.HasValue) ? QuoteData.CQMain.CustomDiscountAmt.ToString() : string.Empty;

                    chkTaxes.Checked = QuoteData.CQMain.IsPrintTaxes.HasValue && QuoteData.CQMain.IsPrintTaxes.Value;

                    tbTax.Text = string.IsNullOrEmpty(QuoteData.CQMain.TaxesDescription) ? string.Empty : QuoteData.CQMain.TaxesDescription;

                    tbTaxx.Text = (QuoteData.CQMain.CustomTaxes.HasValue) ? QuoteData.CQMain.CustomTaxes.ToString() : string.Empty;

                    chkTotalQuote.Checked = QuoteData.CQMain.IsPrintTotalQuote.HasValue && QuoteData.CQMain.IsPrintTotalQuote.Value;

                    tbTotalQuote.Text = string.IsNullOrEmpty(QuoteData.CQMain.TotalQuoteDescription) ? string.Empty : QuoteData.CQMain.TotalQuoteDescription;

                    tbTotalQuotee.Text = (QuoteData.CQMain.CustomTotalQuote.HasValue) ? QuoteData.CQMain.CustomTotalQuote.ToString() : string.Empty;

                    chkPrepaid.Checked = QuoteData.CQMain.IsPrintPay.HasValue && QuoteData.CQMain.IsPrintPay.Value;

                    tbPrepaid.Text = string.IsNullOrEmpty(QuoteData.CQMain.PrepayDescription) ? string.Empty : QuoteData.CQMain.PrepayDescription;

                    tbPrepaidd.Text = (QuoteData.CQMain.CustomPrepay.HasValue) ? QuoteData.CQMain.CustomPrepay.ToString() : string.Empty;

                    chkRemainingAmt.Checked = QuoteData.CQMain.IsPrintRemaingAMT.HasValue && QuoteData.CQMain.IsPrintRemaingAMT.Value;

                    tbRemainingAmt.Text = string.IsNullOrEmpty(QuoteData.CQMain.RemainingAmtDescription) ? string.Empty : QuoteData.CQMain.RemainingAmtDescription;

                    tbRemainingamtt.Text = (QuoteData.CQMain.CustomRemainingAmt.HasValue) ? QuoteData.CQMain.CustomRemainingAmt.ToString() : string.Empty;
                    
                    BindLegs(dgQuoteLeg, true);

                    BindQuoteAddFee(QuoteData, true);

                    if (ddlQuoteImage.Items.Count > 0 && QuoteData.FileWarehouseID.HasValue)
                    {
                            ddlQuoteImage.SelectedValue = QuoteData.FileWarehouseID.ToString();
                            chkQuoteImage1.Checked = true;
                    }

                    if (ddlQuoteImage2.Items.Count > 0 && QuoteData.FileWarehouseID2.HasValue)
                    {
                            ddlQuoteImage2.SelectedValue = QuoteData.FileWarehouseID2.ToString();
                            chkQuoteImage2.Checked = true;
                    }

                    if (ddlQuoteImage3.Items.Count > 0 && QuoteData.FileWarehouseID3.HasValue)
                    {
                            ddlQuoteImage3.SelectedValue = QuoteData.FileWarehouseID3.ToString();
                            chkQuoteImage3.Checked = true;
                    }

                    if (ddlQuoteImage4.Items.Count > 0 && QuoteData.FileWarehouseID4.HasValue)
                    {
                            ddlQuoteImage4.SelectedValue = QuoteData.FileWarehouseID4.ToString();
                            chkQuoteImage4.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Set Quote Summary status to check or uncheck
        /// </summary>
        /// <param name="IsChecked">True Or False</param>
        private void SetQuoteSummaryStatus(bool IsChecked)
        {
            chkDailyUsageAdjustment.Checked = IsChecked;
            chkAdditionalFee.Checked = IsChecked;
            chkLandingFee.Checked = IsChecked;
            chkSegmentFee.Checked = IsChecked;
            chkFlightCharge.Checked = IsChecked;
            chkSubTotal.Checked = IsChecked;
            chkDiscountAmount.Checked = IsChecked;
            chkTaxes.Checked = IsChecked;
            chkTotalQuote.Checked = IsChecked;
            chkPrepaid.Checked = IsChecked;
            chkRemainingAmt.Checked = IsChecked;
        }

        /// <summary>
        /// To Bind Quote Add Fee
        /// </summary>
        /// <param name="QuoteData"></param>
        /// <param name="isBind"></param>
        private void BindQuoteAddFee(CharterQuoteService.CQQuoteMain QuoteData, bool isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(QuoteData, isBind))
            {
                if (QuoteData != null && QuoteData.CQQuoteAdditionalFees != null && QuoteData.CQQuoteAdditionalFees.Count() > 0)
                {
                    dgQuoteAddFee.DataSource = QuoteData.CQQuoteAdditionalFees.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM).ToList();
                }
                else
                {
                    dgQuoteAddFee.DataSource = string.Empty;
                }
                if (isBind)
                    dgQuoteAddFee.Rebind();
            }
        }

        /// <summary>
        /// To Bind Leg Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Leg_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindLegs(dgQuoteLeg, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void Leg_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["DepartDate"] != null && !string.IsNullOrEmpty(item["DepartDate"].Text) && item["DepartDate"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                                if (date != null)
                                    item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }

                            CheckBox chk = (CheckBox)item.FindControl("chkPrint");
                            if (chk != null)
                            {
                                if (chk.Checked)
                                    chk.Checked = true;
                                else
                                    chk.Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void QuoteAddFee_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["QuoteReportMain"] != null)
                        {
                            CharterQuoteService.CQQuoteMain cqs = (CQQuoteMain)Session["QuoteReportMain"];
                            BindQuoteAddFee(cqs, false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void QuoteAddFee_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            CheckBox chk = (CheckBox)item.FindControl("chkPrint");
                            if (chk != null)
                            {
                                if (chk.Checked)
                                    chk.Checked = true;
                                else
                                    chk.Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// Print summary check Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkQuoteAddPrintSummary_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["QuoteReportMain"] != null)
                        {
                            CharterQuoteService.CQQuoteMain cqs = (CQQuoteMain)Session["QuoteReportMain"];

                            BindQuoteAddFee(cqs, true);

                            foreach (GridDataItem item in dgQuoteAddFee.Items)
                            {
                                CheckBox chk = (CheckBox)item.FindControl("chkPrint");
                                if (chk != null)
                                    chk.Checked = chkQuoteAddPrintSummary.Checked;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void QuoteSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            CheckBox chkQuoteSummaryPrint = (CheckBox)item.FindControl("chkPrint");
                            if (chkQuoteSummaryPrint != null)
                            {
                                if (chkQuoteSummaryPrint.Checked)
                                    chkQuoteSummaryPrint.Checked = true;
                                else
                                    chkQuoteSummaryPrint.Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// To Clear Child Properties
        /// </summary>
        /// <param name="cqQuoteMain"></param>
        private void ClearChildProperties(ref CQQuoteMain cqQuoteMain)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqQuoteMain))
            {
                #region CQMain Properties
                if (cqQuoteMain.CQMain != null)
                {
                    //#region CQMain Properties
                    if (cqQuoteMain.CQMain.VendorID.HasValue) cqQuoteMain.CQMain.Vendor = null;
                    if (cqQuoteMain.CQMain.FleetID.HasValue) cqQuoteMain.CQMain.Fleet = null;
                    if (cqQuoteMain.CQMain.AircraftID.HasValue) cqQuoteMain.CQMain.Aircraft = null;
                    if (cqQuoteMain.CQMain.PassengerRequestorID.HasValue) cqQuoteMain.CQMain.Passenger = null;
                    if (cqQuoteMain.CQMain.HomebaseID.HasValue) cqQuoteMain.CQMain.Company = null;
                    if (cqQuoteMain.CQMain.ClientID.HasValue) cqQuoteMain.CQMain.Client = null;
                    if (cqQuoteMain.CQMain.CQLostBusinessID.HasValue) cqQuoteMain.CQMain.CQLostBusiness = null;
                    if (cqQuoteMain.CQMain.FeeGroupID.HasValue) cqQuoteMain.CQMain.FeeGroup = null;
                    cqQuoteMain.CQMain.CQLegs = null;
                    cqQuoteMain.CQMain.CQFleetChargeDetails = null;
                    cqQuoteMain.CQMain.CQFleetCharterRates = null;
                    cqQuoteMain.CQMain.CQHistories = null;
                    cqQuoteMain.CQMain.CQInvoiceMains = null;
                    cqQuoteMain.CQMain.CQQuoteMains = null;
                    //#endregion
                }
                #endregion
                #region CQQuoteDetails Properties
                if (cqQuoteMain.CQQuoteDetails != null)
                {
                    foreach (CQQuoteDetail QuoteDet in cqQuoteMain.CQQuoteDetails)
                    {
                        if (QuoteDet.DAirportID.HasValue) QuoteDet.Airport1 = null;
                        if (QuoteDet.AAirportID.HasValue) QuoteDet.Airport = null;
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// To Save Quote Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnQuoteSave_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            SaveQuoteReport();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        public void SaveQuoteReport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["QuoteReportMain"] != null)
                {
                    CharterQuoteService.CQQuoteMain CQQuoteMain = (CQQuoteMain)Session["QuoteReportMain"];
                    if (CQQuoteMain.CQQuoteMainID != 0)
                        CQQuoteMain.State = CQRequestEntityState.Modified;
                    else
                        CQQuoteMain.State = CQRequestEntityState.Added;
                    CQQuoteMain.Mode = CQRequestActionMode.Edit;

                    CQQuoteMain.QuoteInformation = string.Empty;
                    if (!string.IsNullOrEmpty(tbCustomerAddressInformation.Text))
                        CQQuoteMain.QuoteInformation = tbCustomerAddressInformation.Text;

                    CQQuoteMain.CompanyInformation = string.Empty;
                    if (!string.IsNullOrEmpty(tbCharterCompanyInfo.Text))
                        CQQuoteMain.CompanyInformation = tbCharterCompanyInfo.Text;

                    CQQuoteMain.ReportHeader = string.Empty;
                    if (!string.IsNullOrEmpty(tbHeaderInfo.Text))
                        CQQuoteMain.ReportHeader = tbHeaderInfo.Text;

                    CQQuoteMain.ReportFooter = string.Empty;
                    if (!string.IsNullOrEmpty(tbCustomerFooter.Text))
                        CQQuoteMain.ReportFooter = tbCustomerFooter.Text;
                    CQQuoteMain.IsPrintDetail = chkPrint.Checked;
                    CQQuoteMain.IsPrintFees = chkQuoteAddPrintSummary.Checked;
                    CQQuoteMain.IsPrintSum = chkQuoteSummary.Checked;
                    if (radNone.Checked == true)
                        CQQuoteMain.ImagePosition = 1;
                    else if (radEndOfReport.Checked == true)
                        CQQuoteMain.ImagePosition = 2;
                    else if (radTailNum.Checked == true)
                        CQQuoteMain.ImagePosition = 3;
                    else if (radSepPage.Checked == true)
                        CQQuoteMain.ImagePosition = 4;
                    else
                        CQQuoteMain.ImagePosition = 0;
                    Int64 HomeBaseId = 0;
                    if (FileRequest != null && FileRequest.HomebaseID.HasValue)
                    {
                        HomeBaseId = (long)FileRequest.HomebaseID;
                    }
                    if (!string.IsNullOrEmpty(ddlQuoteImage.SelectedValue))
                        if (chkQuoteImage1.Checked)
                        { CQQuoteMain.FileWarehouseID = Convert.ToInt64(ddlQuoteImage.SelectedValue); }
                        else { CQQuoteMain.FileWarehouseID = null; }

                    if (UserPrincipal.Identity._fpSettings._ImageCnt.HasValue)
                    {
                        if (UserPrincipal.Identity._fpSettings._ImageCnt == 2)
                        {
                            if (ddlQuoteImage2 != null && !string.IsNullOrEmpty(ddlQuoteImage2.SelectedValue))
                                if (chkQuoteImage2.Checked)
                                { CQQuoteMain.FileWarehouseID2 = Convert.ToInt64(ddlQuoteImage2.SelectedValue); }
                                else { CQQuoteMain.FileWarehouseID2 = null; }

                        }
                        else if (UserPrincipal.Identity._fpSettings._ImageCnt == 3) // Fix for SUP-183 (7281)
                        {
                            if (ddlQuoteImage2 != null && !string.IsNullOrEmpty(ddlQuoteImage2.SelectedValue))
                                if (chkQuoteImage2.Checked)
                                { CQQuoteMain.FileWarehouseID2 = Convert.ToInt64(ddlQuoteImage2.SelectedValue); }
                                else { CQQuoteMain.FileWarehouseID2 = null; }


                            if (ddlQuoteImage3 != null && !string.IsNullOrEmpty(ddlQuoteImage3.SelectedValue))
                                if (chkQuoteImage3.Checked)
                                { CQQuoteMain.FileWarehouseID3 = Convert.ToInt64(ddlQuoteImage3.SelectedValue); }
                                else { CQQuoteMain.FileWarehouseID3 = null; }
                        }
                        else if (UserPrincipal.Identity._fpSettings._ImageCnt == 4)
                        {
                            if (ddlQuoteImage2 != null && !string.IsNullOrEmpty(ddlQuoteImage2.SelectedValue))
                                if (chkQuoteImage2.Checked)
                                { CQQuoteMain.FileWarehouseID2 = Convert.ToInt64(ddlQuoteImage2.SelectedValue); }
                                else { CQQuoteMain.FileWarehouseID2 = null; }


                            if (ddlQuoteImage3 != null && !string.IsNullOrEmpty(ddlQuoteImage3.SelectedValue))
                                if (chkQuoteImage3.Checked)
                                { CQQuoteMain.FileWarehouseID3 = Convert.ToInt64(ddlQuoteImage3.SelectedValue); }
                                else { CQQuoteMain.FileWarehouseID3 = null; }

                            if (ddlQuoteImage4 != null && !string.IsNullOrEmpty(ddlQuoteImage4.SelectedValue))
                                if (chkQuoteImage4.Checked)
                                { CQQuoteMain.FileWarehouseID4 = Convert.ToInt64(ddlQuoteImage4.SelectedValue); }
                                else { CQQuoteMain.FileWarehouseID4 = null; }
                        }
                    }

                    // Leg Info
                    if (CQQuoteMain.CQQuoteDetails != null)
                    {
                        foreach (GridDataItem item in dgQuoteLeg.Items)
                        {
                            Int64 LegNum = (long)item.GetDataKeyValue("LegNum");
                            CQQuoteDetail quotedet = new CQQuoteDetail();
                            quotedet = CQQuoteMain.CQQuoteDetails.Where(x => x.LegNum == LegNum && x.IsDeleted == false).SingleOrDefault();
                            if (quotedet != null)
                            {
                                if (quotedet.CQQuoteDetailID != 0)
                                    quotedet.State = CQRequestEntityState.Modified;
                                else
                                    quotedet.State = CQRequestEntityState.Added;
                                CheckBox chkAddFeePrint = (CheckBox)item.FindControl("chkPrint");
                                quotedet.IsPrintable = chkAddFeePrint.Checked;
                            }
                        }
                    }

                    //Additionalfee
                    if (CQQuoteMain.CQQuoteAdditionalFees != null)
                    {
                        foreach (GridDataItem item in dgQuoteAddFee.Items)
                        {
                            if (item.GetDataKeyValue("OrderNUM") != null)
                            {
                                int OrderNum = (int)item.GetDataKeyValue("OrderNUM");
                                CQQuoteAdditionalFee quoteAddfee = new CQQuoteAdditionalFee();
                                quoteAddfee = CQQuoteMain.CQQuoteAdditionalFees.Where(x => x.OrderNUM == OrderNum && x.IsDeleted == false).FirstOrDefault();
                                if (quoteAddfee != null)
                                {
                                    if (quoteAddfee.CQQuoteAdditionalFeesID > 0)
                                        quoteAddfee.State = CQRequestEntityState.Modified;
                                    else
                                        quoteAddfee.State = CQRequestEntityState.Added;
                                    CheckBox chkAddFeePrint = (CheckBox)item.FindControl("chkPrint");
                                    quoteAddfee.IsPrintable = chkAddFeePrint.Checked;
                                    TextBox txtdesc = (TextBox)item.FindControl("txtdesc");
                                    if (!string.IsNullOrEmpty(txtdesc.Text))
                                        quoteAddfee.CQQuoteFeeDescription = txtdesc.Text;
                                    else
                                        quoteAddfee.CQQuoteFeeDescription = string.Empty;
                                }
                            }
                        }
                    }
                    ///QuoteSummary
                    if (CQQuoteMain.CQMain != null)
                    {
                        CQQuoteMain.CQMain.State = CQRequestEntityState.Modified;
                        CQQuoteMain.CQMain.IsPrintUsageAdj = chkDailyUsageAdjustment.Checked;
                        CQQuoteMain.CQMain.UsageAdjDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbDailyDesc.Text))
                            CQQuoteMain.CQMain.UsageAdjDescription = tbDailyDesc.Text;

                        CQQuoteMain.CQMain.IsPrintAdditonalFees = chkAdditionalFee.Checked;
                        CQQuoteMain.CQMain.AdditionalFeeDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbAdditionalFee.Text))
                            CQQuoteMain.CQMain.AdditionalFeeDescription = tbAdditionalFee.Text;

                        CQQuoteMain.CQMain.IsPrintLandingFees = chkLandingFee.Checked;
                        CQQuoteMain.CQMain.LandingFeeDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbLandingFee.Text))
                            CQQuoteMain.CQMain.LandingFeeDescription = tbLandingFee.Text;

                        CQQuoteMain.CQMain.IsPrintSegmentFee = chkSegmentFee.Checked;
                        CQQuoteMain.CQMain.SegmentFeeDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbSegmentFee.Text))
                            CQQuoteMain.CQMain.SegmentFeeDescription = tbSegmentFee.Text;
                        CQQuoteMain.CQMain.IsPrintFlightChg = chkFlightCharge.Checked;
                        CQQuoteMain.CQMain.FlightChgDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbFlightCharge.Text))
                            CQQuoteMain.CQMain.FlightChgDescription = tbFlightCharge.Text;
                        CQQuoteMain.CQMain.IsPrintSubtotal = chkSubTotal.Checked;
                        CQQuoteMain.CQMain.SubtotalDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbSubTotal.Text))
                            CQQuoteMain.CQMain.SubtotalDescription = tbSubTotal.Text;
                        CQQuoteMain.CQMain.IsPrintDiscountAmt = chkDiscountAmount.Checked;
                        CQQuoteMain.CQMain.DescriptionDiscountAmt = string.Empty;
                        if (!string.IsNullOrEmpty(tbDiscountAmount.Text))
                            CQQuoteMain.CQMain.DescriptionDiscountAmt = tbDiscountAmount.Text;
                        CQQuoteMain.CQMain.IsPrintTaxes = chkTaxes.Checked;
                        CQQuoteMain.CQMain.TaxesDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbTax.Text))
                            CQQuoteMain.CQMain.TaxesDescription = tbTax.Text;
                        CQQuoteMain.CQMain.IsPrintTotalQuote = chkTotalQuote.Checked;
                        CQQuoteMain.CQMain.TotalQuoteDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbTotalQuote.Text))
                            CQQuoteMain.CQMain.TotalQuoteDescription = tbTotalQuote.Text;
                        CQQuoteMain.CQMain.IsPrintPay = chkPrepaid.Checked;
                        CQQuoteMain.CQMain.PrepayDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbPrepaid.Text))
                            CQQuoteMain.CQMain.PrepayDescription = tbPrepaid.Text;
                        CQQuoteMain.CQMain.IsPrintRemaingAMT = chkRemainingAmt.Checked;
                        CQQuoteMain.CQMain.RemainingAmtDescription = string.Empty;
                        if (!string.IsNullOrEmpty(tbRemainingAmt.Text))
                            CQQuoteMain.CQMain.RemainingAmtDescription = tbRemainingAmt.Text;
                    }
                    //QuoteSummary
                    using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                    {
                        ClearChildProperties(ref CQQuoteMain);
                        Service.SaveQuoteReport(CQQuoteMain);
                        var objRetVal = Service.GetCQQuoteMainByCQMainID((long)CQQuoteMain.CQMainID);
                        if (objRetVal != null && objRetVal.EntityList.Count() > 0)
                        {
                            CharterQuoteService.CQQuoteMain cqs = new CharterQuoteService.CQQuoteMain();
                            cqs = (CQQuoteMain)objRetVal.EntityList[0];
                            LoadQuoteData(cqs);
                            Session["QuoteReportMain"] = cqs;
                            BindLegs(dgQuoteLeg, true);
                            BindQuoteAddFee(cqs, true);

                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                //Method will be changed once manager completed
                                long vFileNo = 0;
                                if (FileRequest.FileNUM.HasValue)
                                {
                                    vFileNo = (long)FileRequest.FileNUM;
                                }
                                var objRetVal1 = Service.GetCQRequestByFileNum(vFileNo, false);

                                if (objRetVal1 != null && objRetVal1.ReturnFlag)
                                {
                                    FileRequest = objRetVal1.EntityList[0];
                                    if (FileRequest.CQMains != null)
                                    {
                                        foreach (CQMain quote in FileRequest.CQMains.Where(x => x.IsDeleted == false))
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);
                                                if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                {
                                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                    {
                                                        CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                        objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                        objImageFile.RecordType = "CQQuoteOnFile";
                                                        objImageFile.UWAFileName = fwh.UWAFileName;
                                                        objImageFile.RecordID = quote.CQMainID;
                                                        objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                        objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                        objImageFile.IsDeleted = false;
                                                        if (quote.ImageList == null)
                                                            quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                        quote.ImageList.Add(objImageFile);
                                                    }
                                                }
                                            }
                                        }
                                        FileRequest.QuoteNumInProgress = (int)cqs.CQMain.QuoteNUM;
                                        Session[Master.CQSessionKey] = FileRequest;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To Bind Legs
        /// </summary>
        /// <param name="objGrid"></param>
        /// <param name="isBind"></param>
        public void BindLegs(RadGrid objGrid, Boolean isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objGrid, isBind))
            {
                objGrid.DataSource = new string[] { };
                if (isBind)
                    objGrid.Rebind();
                CQQuoteMain QuoteReportMain = new CQQuoteMain();
                if (Session["QuoteReportMain"] != null)
                {
                    QuoteReportMain = (CQQuoteMain)Session["QuoteReportMain"];
                    if (QuoteReportMain != null && QuoteReportMain.CQQuoteDetails != null)
                    {
                        List<CQQuoteDetail> LegList = new List<CQQuoteDetail>();
                        LegList = QuoteReportMain.CQQuoteDetails.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNum).ToList();
                        if (LegList.Count > 0)
                        {
                            objGrid.DataSource = (from u in LegList
                                                  where (u.IsDeleted == false)
                                                  select
                                                   new
                                                   {
                                                       LegNum = u.LegNum,
                                                       isPrint = u.IsPrintable.HasValue ? (bool)u.IsPrintable : false,
                                                       DepartDate = (!u.DepartureDTTMLocal.HasValue) ? null : u.DepartureDTTMLocal,
                                                       ArrivalDate = (!u.ArrivalDTTMLocal.HasValue) ? null : u.ArrivalDTTMLocal,
                                                       DepartAir = u.Airport1 != null ? u.Airport1.IcaoID == null ? string.Empty : u.Airport1.IcaoID : string.Empty,
                                                       ArrivalAir = u.Airport != null ? u.Airport.IcaoID == null ? string.Empty : u.Airport.IcaoID : string.Empty,
                                                       DepartAirportID = u.DAirportID,
                                                       ArrivalAirportID = u.AAirportID,
                                                       PaxTotal = u.PassengerTotal.HasValue ? u.PassengerTotal.ToString() : "0",
                                                       FlightHoursTotal = RounsETEbasedOnCompanyProfileSettings(u.FlightHours != null ? (decimal)u.FlightHours : 0),
                                                       Miles = u.Distance.HasValue ? u.Distance.Value.ToString() : "0",
                                                       QuoteTotal = u.FlightCharge.HasValue ? u.FlightCharge.Value.ToString() : "0"
                                                   }).OrderBy(x => x.LegNum);
                            if (isBind)
                                objGrid.Rebind();
                        }
                    }
                }
            }
        }

        protected string RounsETEbasedOnCompanyProfileSettings(decimal ETEVal)
        {
            string ETEStr = string.Empty;

            try
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin.HasValue &&
                        UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        ETEStr = ConvertTenthsToMins(RoundElpTime((double) ETEVal).ToString());
                    }
                    else
                    {
                        ETEStr = Math.Round((decimal) ETEVal, 1).ToString();
                    }

                    ETEStr = Common.Utility.DefaultEteResult(ETEStr);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            catch (Exception ex)
            {
                //The exception will be handled, logged and replaced by our custom exception. 
                ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
            }

            return ETEStr;
        }


        protected double RoundElpTime(double lnElp_Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lnElp_Time))
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin.HasValue && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {

                    decimal ElapseTMRounding = 0;
                    double lnElp_Min = 0.0;
                    double lnEteRound = 0.0;

                    if (UserPrincipal.Identity._fpSettings._ElapseTMRounding.HasValue)
                        ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                    if (ElapseTMRounding == 1)
                        lnEteRound = 0;
                    else if (ElapseTMRounding == 2)
                        lnEteRound = 5;
                    else if (ElapseTMRounding == 3)
                        lnEteRound = 10;
                    else if (ElapseTMRounding == 4)
                        lnEteRound = 15;
                    else if (ElapseTMRounding == 5)
                        lnEteRound = 30;
                    else if (ElapseTMRounding == 6)
                        lnEteRound = 60;

                    if (lnEteRound > 0)
                    {

                        lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                        //lnElp_Min = lnElp_Min % lnEteRound;
                        if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                            lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                        else
                            lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                        if (lnElp_Min > 0 && lnElp_Min < 60)
                            lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                        if (lnElp_Min == 0)
                            lnElp_Time = Math.Floor(lnElp_Time);


                        if (lnElp_Min == 60)
                            lnElp_Time = Math.Ceiling(lnElp_Time);

                    }
                }
                return lnElp_Time;
            }
        }

        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[1].Length == 1)
                            result = result + "0";
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// To Cancel Quote Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnQuoteCancel_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect(Request.RawUrl, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// To Refresh Custom footer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnQuoteCustomFooter_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null && FileRequest.HomebaseID.HasValue)
                        {
                            ddlQuoteCustomFooter.Items.Clear();
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetCQMessage("Q", (long)FileRequest.HomebaseID).EntityList;
                                int i = 0;
                                if (RetValue != null && RetValue.Count > 0)
                                {
                                    foreach (FlightPakMasterService.CQMessage fwh in RetValue)
                                    {
                                        string encodedDescription = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.MessageDescription);
                                        string encodedID = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.CQMessageID.ToString());

                                        ddlQuoteCustomFooter.Items.Insert(i, new ListItem((encodedDescription), encodedID));
                                        i = i + 1;
                                    }
                                }
                            }
                        }
                        if (FileRequest != null && FileRequest.Company != null && FileRequest.Company.ChtQouteFooterDetailRpt != null)
                        {
                            string Item = FileRequest.Company.ChtQouteFooterDetailRpt;
                            if (ddlQuoteCustomFooter.Items.Count > 0)
                            {
                                if (ddlQuoteCustomFooter.Items.FindByText(Item) != null)
                                {
                                    ddlQuoteCustomFooter.Items.FindByText(Item).Selected = true;
                                    tbCustomerFooter.Text = string.IsNullOrEmpty(tbCustomerFooter.Text) ? ddlQuoteCustomFooter.SelectedItem.ToString() : tbCustomerFooter.Text;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// To Refresh Quote
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnQuoteRefresh_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindowManager1.RadConfirm("Refresh will restore Quote to original values. Continue with the Refresh Process?", "confirmCallBackQuoteFn", 500, 30, null, "Confirmation!");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// Method to Refresh the Quote after confirmation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void QuoteRefreshYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CharterQuoteService.CQQuoteMain cqs = new CharterQuoteService.CQQuoteMain();
                        //LoadQuoteCompanyDetail(ref cqs);

                        if (Session["QuoteReportMain"] != null)
                        {
                            cqs = (CQQuoteMain)Session["QuoteReportMain"];
                            if (cqs.CQQuoteMainID != 0)
                            {
                                if (Session[Master.CQSessionKey] != null)
                                {
                                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                                    int QuoteNumInProgress = 1;
                                    QuoteNumInProgress = (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0) ? Convert.ToInt32(FileRequest.QuoteNumInProgress) : QuoteNumInProgress;
                                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                    #region Bind Customer Address Info for Quote, When Refresh Clicked

                                    if (FileRequest.CQCustomerID.HasValue && FileRequest.CQCustomerID > 0)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.CQCustomer custObj = new FlightPakMasterService.CQCustomer();
                                            custObj.CQCustomerID = Convert.ToInt64(FileRequest.CQCustomerID);

                                            var ReturnValue = objMasterService.GetCQCustomerByCQCustomerID(custObj);

                                            if (ReturnValue != null && ReturnValue.EntityList.Count > 0)
                                            {
                                                StringBuilder custAddr = new StringBuilder();

                                                if (ReturnValue.EntityList[0].CQCustomerName != null)
                                                    custAddr.AppendLine(ReturnValue.EntityList[0].CQCustomerName);
                                                if (ReturnValue.EntityList[0].BillingAddr1 != null)
                                                    custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr1.ToString().Trim());
                                                if (ReturnValue.EntityList[0].BillingAddr2 != null)
                                                    custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr2.ToString().Trim());
                                                if (ReturnValue.EntityList[0].BillingAddr3 != null)
                                                    custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr3.ToString().Trim());

                                                if (ReturnValue.EntityList[0].BillingCity != null)
                                                    custAddr.Append(ReturnValue.EntityList[0].BillingCity.ToString().Trim() + ", ");
                                                if (ReturnValue.EntityList[0].BillingState != null)
                                                    custAddr.Append(ReturnValue.EntityList[0].BillingState.ToString().Trim() + ", ");
                                                if (ReturnValue.EntityList[0].BillingZip != null)
                                                    custAddr.Append(ReturnValue.EntityList[0].BillingZip.ToString().Trim() + ", ");
                                                if (ReturnValue.EntityList[0].CountryCD != null)
                                                    custAddr.Append(ReturnValue.EntityList[0].CountryCD.ToString().Trim());
                                                
                                                if(string.IsNullOrEmpty(cqs.QuoteInformation))
                                                    cqs.QuoteInformation = custAddr.ToString();
                                            }
                                        }
                                    }

                                    #endregion

                                    if (QuoteInProgress != null)
                                    {
                                        #region CQMain
                                        CQMain cqMain = new CQMain();
                                        cqMain.InjectFrom(QuoteInProgress);
                                        cqMain.State = CQRequestEntityState.Modified;
                                        
                                        cqMain.CustomUsageAdj = QuoteInProgress.UsageAdjTotal;
                                        cqMain.CustomAdditionalFees = QuoteInProgress.AdditionalFeeTotalD;
                                        cqMain.CustomLandingFee = QuoteInProgress.LandingFeeTotal;
                                        cqMain.CustomSegmentFee = QuoteInProgress.SegmentFeeTotal;
                                        cqMain.CustomStdCrewRON = QuoteInProgress.StdCrewRONTotal;
                                        cqMain.CustomAdditionalCrew = QuoteInProgress.AdditionalCrewTotal;
                                        //cqMain.CustomAdditionalCrewRON = QuoteInProgress.AdditionalCrewRONTotal1; //Doubt Field
                                        cqMain.CustomWaitingChg = QuoteInProgress.WaitChgTotal;
                                        cqMain.CustomFlightChg = QuoteInProgress.FlightChargeTotal;
                                        cqMain.CustomSubTotal = QuoteInProgress.SubtotalTotal;
                                        cqMain.CustomDiscountAmt = QuoteInProgress.DiscountAmtTotal;
                                        cqMain.CustomTaxes = QuoteInProgress.TaxesTotal;
                                        cqMain.CustomTotalQuote = QuoteInProgress.QuoteTotal;
                                        cqMain.CustomPrepay = QuoteInProgress.PrepayTotal;
                                        cqMain.CustomRemainingAmt = QuoteInProgress.RemainingAmtTotal;

                                        cqMain.UsageAdjDescription = string.IsNullOrEmpty(cqMain.UsageAdjDescription) ? DailyUsageAdjustment : cqMain.UsageAdjDescription;
                                        cqMain.AdditionalFeeDescription = string.IsNullOrEmpty(cqMain.AdditionalFeeDescription) ? AdditionalFees : cqMain.AdditionalFeeDescription;
                                        cqMain.LandingFeeDescription = string.IsNullOrEmpty(cqMain.LandingFeeDescription) ? LandingFees : cqMain.LandingFeeDescription;
                                        cqMain.SegmentFeeDescription = string.IsNullOrEmpty(cqMain.SegmentFeeDescription) ? SegmentFees : cqMain.SegmentFeeDescription;
                                        cqMain.FlightChgDescription = string.IsNullOrEmpty(cqMain.FlightChgDescription) ? FlightCharge : cqMain.FlightChgDescription;
                                        cqMain.SubtotalDescription = string.IsNullOrEmpty(cqMain.SubtotalDescription) ? SubTotal : cqMain.SubtotalDescription;
                                        cqMain.DescriptionDiscountAmt = string.IsNullOrEmpty(cqMain.DescriptionDiscountAmt) ? DiscountAmount : cqMain.DescriptionDiscountAmt;
                                        cqMain.TaxesDescription = string.IsNullOrEmpty(cqMain.TaxesDescription) ? Taxes : cqMain.TaxesDescription;
                                        cqMain.TotalQuoteDescription = string.IsNullOrEmpty(cqMain.TotalQuoteDescription) ? TotalQuote : cqMain.TotalQuoteDescription;
                                        cqMain.PrepayDescription = string.IsNullOrEmpty(cqMain.PrepayDescription) ? Prepaid : cqMain.PrepayDescription;
                                        cqMain.RemainingAmtDescription = string.IsNullOrEmpty(cqMain.RemainingAmtDescription) ? RemainingAmountDue : cqMain.RemainingAmtDescription;

                                        #region Load Company Quote Details using Homebase ID
                                        if (FileRequest.HomebaseID.HasValue)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var CompanyList = FPKMasterService.GetListInfoByHomeBaseId((long)FileRequest.HomebaseID);
                                                if (CompanyList != null && CompanyList.EntityList != null && CompanyList.EntityList.Count() > 0)
                                                {

                                                    cqs.CompanyInformation = string.IsNullOrEmpty(cqs.CompanyInformation) ? CompanyList.EntityList[0].ChtQuoteCompany : cqs.CompanyInformation;
                                                    cqs.ReportHeader = string.IsNullOrEmpty(cqs.ReportHeader) ? CompanyList.EntityList[0].ChtQouteHeaderDetailRpt : cqs.ReportHeader;
                                                    cqs.ReportFooter = string.IsNullOrEmpty(cqs.ReportFooter) ? CompanyList.EntityList[0].ChtQouteFooterDetailRpt : cqs.ReportFooter;

                                                    cqs.IsPrintDetail = (!cqs.IsPrintDetail.HasValue ? (CompanyList.EntityList[0].IsChangeQueueDetail ?? false) : cqs.IsPrintDetail);
                                                    cqs.IsPrintFees = (!cqs.IsPrintFees.HasValue ? (CompanyList.EntityList[0].IsQuoteChangeQueueFees ?? false) : cqs.IsPrintFees);
                                                    cqs.IsPrintSum = (!cqs.IsPrintSum.HasValue ? (CompanyList.EntityList[0].IsQuoteChangeQueueSum ?? false) : cqs.IsPrintSum);
                                                    cqs.ImagePosition = (cqs.ImagePosition == 0 ? (CompanyList.EntityList[0].ImagePosition ?? 0) : cqs.ImagePosition);

                                                    cqMain.UsageAdjDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteMinimumUse2FeeDepositAdj) ? (cqMain.UsageAdjDescription.Trim() == CompanyList.EntityList[0].QuoteMinimumUse2FeeDepositAdj.Trim() ? CompanyList.EntityList[0].QuoteMinimumUse2FeeDepositAdj : cqMain.UsageAdjDescription) : DailyUsageAdjustment;
                                                    cqMain.IsPrintUsageAdj = (!cqMain.IsPrintUsageAdj.HasValue ? (CompanyList.EntityList[0].IsQuotePrepaidMinimumUsage2FeeAdj ?? false) : cqMain.IsPrintUsageAdj);

                                                    cqMain.AdditionalFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Additional2FeeDefault) ? (cqMain.AdditionalFeeDescription.Trim() == CompanyList.EntityList[0].Additional2FeeDefault.Trim() ? CompanyList.EntityList[0].Additional2FeeDefault : cqMain.AdditionalFeeDescription) : AdditionalFees;
                                                    cqMain.IsPrintAdditonalFees = (!cqMain.IsPrintAdditonalFees.HasValue ? (CompanyList.EntityList[0].IsQuoteAdditional2FeePrint ?? false) : cqMain.IsPrintAdditonalFees);

                                                    cqMain.SegmentFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSegment2FeeDeposit) ? (cqMain.SegmentFeeDescription.Trim() == CompanyList.EntityList[0].QuoteSegment2FeeDeposit ? CompanyList.EntityList[0].QuoteSegment2FeeDeposit : cqMain.SegmentFeeDescription) : SegmentFees;
                                                    cqMain.IsPrintSegmentFee = (!cqMain.IsPrintSegmentFee.HasValue ? (CompanyList.EntityList[0].IsQuotePrepaidSegement2Fee ?? false) : cqMain.IsPrintSegmentFee);

                                                    cqMain.LandingFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteLandingFeeDefault) ? (cqMain.LandingFeeDescription == CompanyList.EntityList[0].QuoteLandingFeeDefault ? CompanyList.EntityList[0].QuoteLandingFeeDefault : cqMain.LandingFeeDescription) : LandingFees;
                                                    cqMain.IsPrintLandingFees = (!cqMain.IsPrintLandingFees.HasValue ? (CompanyList.EntityList[0].IsQuoteFlight2LandingFeePrint ?? false) : cqMain.IsPrintLandingFees);

                                                    cqMain.FlightChgDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Deposit2FlightCharge) ? (cqMain.FlightChgDescription == CompanyList.EntityList[0].Deposit2FlightCharge ? CompanyList.EntityList[0].Deposit2FlightCharge : cqMain.FlightChgDescription) : FlightCharge;
                                                    cqMain.IsPrintFlightChg = (!cqMain.IsPrintFlightChg.HasValue ? (CompanyList.EntityList[0].IsQuoteFlight2ChargePrepaid ?? false) : cqMain.IsPrintFlightChg);

                                                    cqMain.SubtotalDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Quote2Subtotal) ? (cqMain.SubtotalDescription == CompanyList.EntityList[0].Quote2Subtotal ? CompanyList.EntityList[0].Quote2Subtotal : cqMain.SubtotalDescription) : SubTotal;
                                                    cqMain.IsPrintSubtotal = (!cqMain.IsPrintSubtotal.HasValue ? (CompanyList.EntityList[0].IsQuoteSubtotal2Print ?? false) : cqMain.IsPrintSubtotal);

                                                    cqMain.DescriptionDiscountAmt = !string.IsNullOrEmpty(CompanyList.EntityList[0].Discount2AmountDeposit) ? (cqMain.DescriptionDiscountAmt == CompanyList.EntityList[0].Discount2AmountDeposit ? CompanyList.EntityList[0].Discount2AmountDeposit : cqMain.DescriptionDiscountAmt) : DiscountAmount;
                                                    cqMain.IsPrintDiscountAmt = (!cqMain.IsPrintDiscountAmt.HasValue ? (CompanyList.EntityList[0].IsQuoteDiscount2AmountPrepaid ?? false) : cqMain.IsPrintDiscountAmt);

                                                    cqMain.TaxesDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].Quote2TaxesDefault) ? (cqMain.TaxesDescription == CompanyList.EntityList[0].Quote2TaxesDefault ? CompanyList.EntityList[0].Quote2TaxesDefault : cqMain.TaxesDescription) : Taxes;
                                                    cqMain.IsPrintTaxes = (!cqMain.IsPrintTaxes.HasValue ? CompanyList.EntityList[0].IsQuoteTaxes2Print : cqMain.IsPrintTaxes);

                                                    cqMain.TotalQuoteDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteInvoice2Default) ? (cqMain.TotalQuoteDescription == CompanyList.EntityList[0].QuoteInvoice2Default ? CompanyList.EntityList[0].QuoteInvoice2Default : cqMain.TotalQuoteDescription) : TotalQuote;
                                                    cqMain.IsPrintTotalQuote = (!cqMain.IsPrintTotalQuote.HasValue ? CompanyList.EntityList[0].IsQuoteInvoice2Print : cqMain.IsPrintTotalQuote);

                                                    cqMain.PrepayDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuotePrepay2Default) ? (cqMain.PrepayDescription == CompanyList.EntityList[0].QuotePrepay2Default ? CompanyList.EntityList[0].QuotePrepay2Default : cqMain.PrepayDescription) : Prepaid;
                                                    cqMain.IsPrintPay = (!cqMain.IsPrintPay.HasValue ? CompanyList.EntityList[0].IsQuote2PrepayPrint : cqMain.IsPrintPay);

                                                    cqMain.RemainingAmtDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteRemaining2AmountDefault) ? (cqMain.RemainingAmtDescription == CompanyList.EntityList[0].QuoteRemaining2AmountDefault ? CompanyList.EntityList[0].QuoteRemaining2AmountDefault : cqMain.RemainingAmtDescription) : RemainingAmountDue;
                                                    cqMain.IsPrintRemaingAMT = (!cqMain.IsPrintRemaingAMT.HasValue ? CompanyList.EntityList[0].IsQuoteRemaining2AmtPrint : cqMain.IsPrintRemaingAMT);
                                                    
                                                }
                                            }
                                        }
                                        #endregion

                                        // Append the Sales Person's Name and Phone at Company Information Property for Quote Report, When Refresh button clicked.
                                        
                                        if (FileRequest.SalesPersonID != null)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var RetValue = objMasterService.GetSalesPersonList().EntityList.Where(x => x.SalesPersonID == FileRequest.SalesPersonID);
                                                if (RetValue != null && RetValue.Count() > 0)
                                                {
                                                    List<FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                                    SalesPersonList = (List<FlightPakMasterService.GetSalesPerson>)RetValue.ToList();

                                                    if (string.IsNullOrEmpty(cqs.CompanyInformation) || (cqs.CompanyInformation.Contains("REPRESENTATIVE: ") == false && SalesPersonList[0].Name != null && !string.IsNullOrEmpty(SalesPersonList[0].Name)))
                                                        cqs.CompanyInformation += Environment.NewLine + "REPRESENTATIVE: " + SalesPersonList[0].Name;

                                                    if (string.IsNullOrEmpty(cqs.CompanyInformation) || (cqs.CompanyInformation.Contains("Phone No: ") == false && SalesPersonList[0].PhoneNUM != null && !string.IsNullOrEmpty(SalesPersonList[0].PhoneNUM)))
                                                        cqs.CompanyInformation += Environment.NewLine + "Phone No: " + SalesPersonList[0].PhoneNUM;

                                                }
                                            }
                                        }

                                        cqs.CQMain = cqMain;
                                        cqs.CQMainID = cqMain.CQMainID;
                                        #endregion

                                        #region AdditionalFees
                                        if (QuoteInProgress.CQFleetChargeDetails != null)
                                        {
                                            if (cqs.CQQuoteAdditionalFees == null)
                                                cqs.CQQuoteAdditionalFees = new List<CQQuoteAdditionalFee>();
                                            else
                                            {
                                                foreach (CQQuoteAdditionalFee quotefee in cqs.CQQuoteAdditionalFees.ToList())
                                                {
                                                    if (quotefee.CQQuoteAdditionalFeesID != 0)
                                                    {
                                                        quotefee.IsDeleted = true;
                                                        quotefee.State = CQRequestEntityState.Deleted;
                                                    }
                                                    else
                                                        cqs.CQQuoteAdditionalFees.Remove(quotefee);
                                                }
                                            }

                                            foreach (CQFleetChargeDetail fltCharge in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                CQQuoteAdditionalFee quotefee = cqs.CQQuoteAdditionalFees.Where(x => x.IsDeleted == true && x.OrderNUM == fltCharge.OrderNUM).FirstOrDefault();
                                                CQQuoteAdditionalFee addfee = new CQQuoteAdditionalFee();
                                                addfee.State = CQRequestEntityState.Added;
                                                addfee.IsDeleted = false;
                                                addfee.OrderNUM = fltCharge.OrderNUM;
                                                addfee.CQQuoteFeeDescription = fltCharge.CQFlightChargeDescription;
                                                addfee.QuoteAmount = fltCharge.FeeAMT;
                                                addfee.Quantity = fltCharge.Quantity;
                                                addfee.ChargeUnit = fltCharge.ChargeUnit;
                                                addfee.IsPrintable = cqs.IsPrintSum != null ? (bool)cqs.IsPrintSum : false;// UserPrincipal.Identity._fpSettings._IsQuotePrintSum != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuotePrintSum : false;
                                                if (quotefee != null)
                                                {
                                                    addfee.IsPrintable = quotefee.IsPrintable != null ? (bool)quotefee.IsPrintable : false;
                                                    addfee.CQQuoteFeeDescription = (fltCharge.CQFlightChargeDescription != quotefee.CQQuoteFeeDescription) ? quotefee.CQQuoteFeeDescription : fltCharge.CQFlightChargeDescription;
                                                }
                                                cqs.CQQuoteAdditionalFees.Add(addfee);
                                            }
                                        }
                                        #endregion

                                        #region QuoteDetail
                                        if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                                        {
                                            if (cqs.CQQuoteDetails == null)
                                                cqs.CQQuoteDetails = new List<CQQuoteDetail>();
                                            else
                                            {
                                                foreach (CQQuoteDetail quotedet in cqs.CQQuoteDetails.Where(x => x.IsDeleted == false).ToList())
                                                {
                                                    if (quotedet.CQQuoteDetailID != 0)
                                                    {
                                                        quotedet.IsDeleted = true;
                                                        quotedet.State = CQRequestEntityState.Deleted;
                                                    }
                                                    else
                                                        cqs.CQQuoteDetails.Remove(quotedet);
                                                }
                                            }

                                            foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                CQQuoteDetail quotedet = cqs.CQQuoteDetails.Where(x => x.IsDeleted == true && x.CQLegID == Leg.CQLegID).FirstOrDefault();
                                                CQQuoteDetail addQuoteDetail = new CQQuoteDetail();
                                                addQuoteDetail.State = CQRequestEntityState.Added;
                                                addQuoteDetail.IsDeleted = false;
                                                addQuoteDetail.CQLegID = Leg.CQLegID;
                                                addQuoteDetail.LegNum = Leg.LegNUM;
                                                addQuoteDetail.DepartureDTTMLocal = Leg.DepartureDTTMLocal;
                                                addQuoteDetail.ArrivalDTTMLocal = Leg.ArrivalDTTMLocal;
                                                addQuoteDetail.Airport1 = Leg.Airport1;
                                                addQuoteDetail.Airport = Leg.Airport;
                                                addQuoteDetail.DAirportID = Leg.DAirportID;
                                                addQuoteDetail.AAirportID = Leg.AAirportID;
                                                addQuoteDetail.PassengerTotal = Leg.PassengerTotal;
                                                addQuoteDetail.FlightHours = Leg.ElapseTM;
                                                addQuoteDetail.Distance = Leg.Distance;
                                                addQuoteDetail.FlightCharge = Leg.ChargeTotal;
                                                addQuoteDetail.IsTaxable = Leg.IsTaxable != null ? Leg.IsTaxable : false;
                                                addQuoteDetail.IsPrintable = cqs.IsPrintDetail != null ? (bool)cqs.IsPrintDetail : false;  // UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint : false;
                                                if (quotedet != null)
                                                {
                                                    addQuoteDetail.IsPrintable = quotedet.IsPrintable != null ? (bool)quotedet.IsPrintable : (cqs.IsPrintDetail != null ? (bool)cqs.IsPrintDetail : false);
                                                }

                                                cqs.CQQuoteDetails.Add(addQuoteDetail);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                LoadQuoteData(cqs);
                            }
                            else
                            {
                                LoadQuoteCompanyDetail(ref cqs);
                            }
                        }
                        else
                        {
                            LoadQuoteCompanyDetail(ref cqs);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        #endregion

        #region InvoiceReport
        private void EmptyInvoiceDetails()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbInvoiceCompanyInfo.Text = string.Empty;
                tbInvoiceCompanyInfo.Enabled = false;
                tbMyCharterCustomer.Text = string.Empty;
                tbMyCharterCustomer.Enabled = false;
                tbInvoiceHeaderInfo.Text = string.Empty;
                tbInvoiceHeaderInfo.Enabled = false;
                tbInvoiceFooterInfo.Text = string.Empty;
                tbInvoiceFooterInfo.Enabled = false;
                chkInvoicePrintDetail.Checked = false;
                chkInvoicePrintDetail.Enabled = false;
                chkInvoicePrintQuoteNo.Checked = false;
                chkInvoicePrintQuoteNo.Enabled = false;
                dgInvoiceLeg.DataSource = string.Empty;
                dgInvoiceLeg.DataBind();
                dgInvoiceAddFee.DataSource = string.Empty;
                dgInvoiceAddFee.DataBind();
                chkInvoicePrintAdditionalFees.Checked = false;
                chkInvoicePrintAdditionalFees.Enabled = false;
                chkInvoiceAdditionalFeesTaxable.Checked = false;
                chkInvoiceAdditionalFeesTaxable.Enabled = false;
                tbInvocieAdditionalFeeTaxRate.Text = string.Empty;
                tbInvocieAdditionalFeeTaxRate.Enabled = false;
                chkÍnvoicePrintSummary.Checked = false;
                chkÍnvoicePrintSummary.Enabled = false;
                chkInvoiceSummaryFeesTaxable.Checked = false;
                chkÍnvoicePrintSummary.Enabled = false;
                chkInvoiceManualTaxEntry.Checked = false;
                chkInvoiceManualTaxEntry.Enabled = false;
                chkManualDiscountEntry.Checked = false;
                chkManualDiscountEntry.Enabled = false;
                tbInvoiceTaxRate.Text = string.Empty;
                tbInvoiceTaxRate.Enabled = false;
                chkInvoiceDailyUsageAdjustmentPrint.Checked = false;
                chkInvoiceDailyUsageAdjustmentPrint.Enabled = false;
                tbInvoiceDailyUsageAdjustment.Text = string.Empty;
                tbInvoicedailyusageQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                chkDailyusageTaxable.Checked = false;
                chkDailyusageTaxable.Enabled = false;
                chkDailyusageDiscount.Checked = false;
                chkDailyusageDiscount.Enabled = false;
                chkInvoiceAdditionalfeePrint.Checked = false;
                chkInvoiceAdditionalfeePrint.Enabled = false;
                tbInvoiceAdditionalFeeDescription.Text = string.Empty;
                tbInvoiceAdditionalFeeDescription.Enabled = false;
                tbInvoiceAdditionalfeeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceAdditionalfeeInvoiceAmt.Text = string.Empty;
                tbInvoiceAdditionalfeeInvoiceAmt.Enabled = false;
                chkInvoiceAdditionalfeeInvoiceTaxable.Checked = false;
                chkInvoiceAdditionalfeeInvoiceTaxable.Enabled = false;
                chkInvoiceSegmentFeePrint.Checked = false;
                chkInvoiceSegmentFeePrint.Enabled = false;
                tbInvoiceSegmentFeeDescription.Text = string.Empty;
                tbInvoiceSegmentFeeDescription.Enabled = false;
                tbInvoiceSegmentFeeQuoteAmt.Enabled = false;
                tbInvoiceSegmentFeeInvoiceAmt.Text = string.Empty;
                tbInvoiceSegmentFeeInvoiceAmt.Enabled = false;
                chkInvoiceSegmentFeetaxable.Checked = false;
                chkInvoiceSegmentFeetaxable.Enabled = false;
                chkInvoiceRonCrewChargePrint.Enabled = false;
                chkInvoiceRonCrewChargePrint.Checked = false;
                tbInvoiceRonCrewChargeDescription.Enabled = false;
                tbInvoiceRonCrewChargeDescription.Text = string.Empty;
                tbInvoiceRonCrewChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvocieRonCrewChargeInvoiceAmt.Enabled = false;
                tbInvocieRonCrewChargeInvoiceAmt.Text = string.Empty;
                chkInvoiceRonCrewChargeTaxable.Enabled = false;
                chkInvoiceRonCrewChargeTaxable.Checked = false;
                chkInvoiceRonCrewChargeDiscount.Enabled = false;
                chkInvoiceRonCrewChargeDiscount.Checked = false;
                chkInvoiceFlightCharge.Checked = false;
                chkInvoiceFlightCharge.Enabled = false;
                tbInvoiceFlightChargeDescription.Enabled = false;
                tbInvoiceFlightChargeDescription.Text = string.Empty;
                tbInvoiceFlightChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceFlightChargeInvocieAmt.Enabled = false;
                tbInvoiceFlightChargeInvocieAmt.Text = string.Empty;
                chkInvoiceFlightChargeDiscount.Enabled = false;
                chkInvoiceFlightChargeDiscount.Checked = false;
                chkInvoiceSubtotalPrint.Checked = false;
                chkInvoiceSubtotalPrint.Enabled = false;
                tbInvoiceSubtotalDecription.Text = string.Empty;
                tbInvoiceSubtotalDecription.Enabled = false;
                tbInvoiceSubtotalQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceSubtotalInvoiceAmt.Text = string.Empty;
                tbInvoiceSubtotalInvoiceAmt.Enabled = false;
                chkInvoiceDiscountPrint.Enabled = false;
                chkInvoiceDiscountPrint.Checked = false;
                tbInvoiceDiscountdescription.Enabled = false;
                tbInvoiceDiscountdescription.Text = string.Empty;
                tbInvoiceDiscountQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceDiscountInvoiceAmt.Text = string.Empty;
                tbInvoiceDiscountInvoiceAmt.Enabled = false;
                chkInvoiceTaxesPrint.Checked = false;
                chkInvoiceTaxesPrint.Enabled = false;
                tbInvoiceTaxesDescription.Text = string.Empty;
                tbInvoiceTaxesDescription.Enabled = false;
                tbInvoiceTaxesQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceTaxesInvoiceAmt.Text = string.Empty;
                tbInvoiceTaxesInvoiceAmt.Enabled = false;
                chkInvoiceTotalInvoicePrint.Checked = false;
                chkInvoiceTotalInvoicePrint.Enabled = false;
                tbInvoiceTotalInvoiceDescription.Enabled = false;
                tbInvoiceTotalInvoiceDescription.Text = string.Empty;
                tbInvoiceTotalInvoiceQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceTotalInvoiceInvoiceAmt.Enabled = false;
                tbInvoiceTotalInvoiceInvoiceAmt.Text = string.Empty;
                chkInvoiceTotalPrepayPrint.Checked = false;
                chkInvoiceTotalPrepayPrint.Enabled = false;
                tbInvoiceTotalPrepayDescription.Enabled = false;
                tbInvoiceTotalPrepayDescription.Text = string.Empty;
                tbInvoiceTotalPrepayQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceTotalPrepayInvoiceAmt.Enabled = false;
                tbInvoiceTotalPrepayInvoiceAmt.Text = string.Empty;
                chkInvoiceTotalRemainingPrint.Enabled = false;
                chkInvoiceTotalRemainingPrint.Checked = false;
                tbInvoiceTotalRemainingDescription.Enabled = false;
                tbInvoiceTotalRemainingDescription.Text = string.Empty;
                tbInvoiceTotalRemainingQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                tbInvoiceTotalRemainingInvoiceAmt.Enabled = false;
                tbInvoiceTotalRemainingInvoiceAmt.Text = string.Empty;
                //Landing fees
                chkInvoiceLandingfeePrint.Checked = false;
                chkInvoiceLandingfeePrint.Enabled = false;
                tbInvoiceLandingFeeDescription.Text = string.Empty;
                tbInvoiceLandingFeeDescription.Enabled = false;
                lbInvoiceLandingfeeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbInvoiceLandingfeeQuoteAmt.Enabled = false;
                tbInvoiceLandingfeeInvoiceAmt.Text = string.Empty;
                tbInvoiceLandingfeeInvoiceAmt.Enabled = false;
                chkInvoiceLandingfeeInvoiceTaxable.Checked = false;
                chkInvoiceLandingfeeInvoiceTaxable.Enabled = false;
                chkInvoiceLandingfeeInvoiceDiscount.Checked = false;
                chkInvoiceLandingfeeInvoiceDiscount.Enabled = false;
                //Additional Crew 
                chkInvoiceAddCrewChargePrint.Checked = false;
                chkInvoiceAddCrewChargePrint.Enabled = false;
                tbInvoiceAddrewChargeDescription.Text = string.Empty;
                tbInvoiceAddrewChargeDescription.Enabled = false;
                lbInvoiceAddCrewChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbInvoiceAddCrewChargeQuoteAmt.Enabled = false;
                tbInvocieAddCrewChargeInvoiceAmt.Text = string.Empty;
                tbInvocieAddCrewChargeInvoiceAmt.Enabled = false;
                tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text = string.Empty;
                tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Enabled = false;
                chkInvoiceAddCrewChargeTaxable.Checked = false;
                chkInvoiceAddCrewChargeTaxable.Enabled = false;
                chkInvoiceAddCrewChargeDiscount.Checked = false;
                chkInvoiceAddCrewChargeDiscount.Enabled = false;
                //Additional Crew Ron 
                chkInvoiceAdditionalRonCrewChargePrint.Checked = false;
                chkInvoiceAdditionalRonCrewChargePrint.Enabled = false;
                tbInvoiceAdditionalRonCrewChargeDescription.Text = string.Empty;
                tbInvoiceAdditionalRonCrewChargeDescription.Enabled = false;
                lbInvoiceAdditionalRonCrewChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbInvoiceAdditionalRonCrewChargeQuoteAmt.Enabled = false;
                tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text = string.Empty;
                tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Enabled = false;
                chkInvoiceAdditionalRonCrewChargeTaxable.Checked = false;
                chkInvoiceAdditionalRonCrewChargeTaxable.Enabled = false;
                chkInvoiceAdditionalRonCrewChargeDiscount.Checked = false;
                chkInvoiceAdditionalRonCrewChargeDiscount.Enabled = false;
                ///Waiting charge
                chkInvoiceWaitCharge.Checked = false;
                chkInvoiceWaitCharge.Enabled = false;
                tbInvoiceWaitChargeDescription.Text = string.Empty;
                tbInvoiceWaitChargeDescription.Enabled = false;
                lbInvoiceWaitChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbInvoiceWaitChargeQuoteAmt.Enabled = false;
                tbInvoiceWaitChargeInvocieAmt.Text = string.Empty;
                tbInvoiceWaitChargeInvocieAmt.Enabled = false;
                chkInvoiceWaitChargeTaxable.Checked = false;
                chkInvoiceWaitChargeTaxable.Enabled = false;
                chkInvoiceWaitChargeDiscount.Checked = false;
                chkInvoiceWaitChargeDiscount.Enabled = false;
                //Usage Adjustment
                tbInvoicedailyusageInvoiceAmt.Text = string.Empty;
                tbInvoicedailyusageInvoiceAmt.Enabled = false;
                tbInvoiceDailyUsageAdjustment.Text = string.Empty;
                tbInvoiceDailyUsageAdjustment.Enabled = false;
            }
        }

        protected void btnInvoiceCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect(Request.RawUrl, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkQuotePrint_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        foreach (GridDataItem item in dgQuoteLeg.Items)
                        {
                            CheckBox chk = (CheckBox)item.FindControl("chkPrint");
                            if (chk != null)
                            {
                                if (chkPrint.Checked)
                                    chk.Checked = true;
                                else
                                    chk.Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkInvoicePrintDetail_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        foreach (GridDataItem item in dgInvoiceLeg.Items)
                        {
                            CheckBox chk = (CheckBox)item.FindControl("chkPrint");
                            if (chk != null)
                            {
                                chk.Checked = chkInvoicePrintDetail.Checked;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkInvoicePrintAdditionalFees_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["InvoiceReportMain"] != null)
                        {
                            CharterQuoteService.CQInvoiceMain cqs = (CQInvoiceMain)Session["InvoiceReportMain"];

                            foreach (GridDataItem item in dgInvoiceAddFee.Items)
                            {
                                CheckBox chk = (CheckBox)item.FindControl("chkInvoiceAddfeePrint");
                                if (chk != null)
                                {
                                    chk.Checked = chkInvoicePrintAdditionalFees.Checked;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkInvoiceAdditionalFeesTaxable_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["InvoiceReportMain"] != null)
                        {
                            CharterQuoteService.CQInvoiceMain cqs = (CQInvoiceMain)Session["InvoiceReportMain"];
                            
                            foreach (GridDataItem item in dgInvoiceAddFee.Items)
                            {
                                CheckBox chk = (CheckBox)item.FindControl("chkInvoiceTaxable");
                                if (chk != null)
                                {
                                    chk.Checked = chkInvoiceAdditionalFeesTaxable.Checked;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkÍnvoicePrintSummary_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SetInvoicePrintSummaryStatus(chkÍnvoicePrintSummary.Checked);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// Set Invoice Print Summary Status
        /// </summary>
        /// <param name="IsChecked"></param>
        private void SetInvoicePrintSummaryStatus(bool IsChecked)
        {
            chkInvoiceDailyUsageAdjustmentPrint.Checked = IsChecked;
            chkInvoiceAdditionalfeePrint.Checked = IsChecked;
            chkInvoiceSegmentFeePrint.Checked = IsChecked;
            chkInvoiceRonCrewChargePrint.Checked = IsChecked;
            chkInvoiceFlightCharge.Checked = IsChecked;
            chkInvoiceSubtotalPrint.Checked = IsChecked;
            chkInvoiceDiscountPrint.Checked = IsChecked;
            chkInvoiceTaxesPrint.Checked = IsChecked;
            chkInvoiceTotalInvoicePrint.Checked = IsChecked;
            chkInvoiceTotalPrepayPrint.Checked = IsChecked;
            chkInvoiceTotalRemainingPrint.Checked = IsChecked;
        }

        protected void chkInvoiceSummaryFeesTaxable_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SetInvoiceSummaryFeesTaxableStatus(chkInvoiceSummaryFeesTaxable.Checked);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// Set Invoice Summary Fees Taxable checkboxes status
        /// </summary>
        /// <param name="IsChecked"></param>
        private void SetInvoiceSummaryFeesTaxableStatus(bool IsChecked)
        {
            chkDailyusageTaxable.Checked = IsChecked;
            chkInvoiceAdditionalfeeInvoiceTaxable.Checked = IsChecked;
            chkInvoiceRonCrewChargeTaxable.Checked = IsChecked;
        }

        protected void chkInvoiceManualTaxEntry_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbInvoiceTaxesInvoiceAmt.Enabled = chkInvoiceManualTaxEntry.Checked == true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }
        protected void chkManualDiscountEntry_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbInvoiceDiscountInvoiceAmt.Enabled = chkManualDiscountEntry.Checked == true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void InvoiceAddFee_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["InvoiceReportMain"] != null)
                        {
                            CharterQuoteService.CQInvoiceMain cqs = (CQInvoiceMain)Session["InvoiceReportMain"];
                            BindInvoiceAddFee(cqs, false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        public void BindInvoiceLegs(RadGrid objGrid, Boolean isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objGrid, isBind))
            {
                objGrid.DataSource = new string[] { };
                if (isBind)
                    objGrid.Rebind();
                CQInvoiceMain InvoiceReportMain = new CQInvoiceMain();
                if (Session["InvoiceReportMain"] != null)
                {
                    InvoiceReportMain = (CQInvoiceMain)Session["InvoiceReportMain"];
                    if (InvoiceReportMain != null && InvoiceReportMain.CQInvoiceQuoteDetails != null)
                    {
                        List<CQInvoiceQuoteDetail> LegList = new List<CQInvoiceQuoteDetail>();
                        LegList = InvoiceReportMain.CQInvoiceQuoteDetails.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNum).ToList();
                        if (LegList.Count > 0)
                        {
                            objGrid.DataSource = (from u in LegList
                                                  where (u.IsDeleted == false)
                                                  select
                                                   new
                                                   {
                                                       LegNum = u.LegNum,
                                                       isPrint = u.IsPrintable != null ? (bool)u.IsPrintable : false,
                                                       DepartDate = (!u.DepartureDTTMLocal .HasValue) ? null : u.DepartureDTTMLocal,
                                                       ArrivalDate = (!u.ArrivalDTTMLocal.HasValue)? null : u.ArrivalDTTMLocal,
                                                       DepartAir = u.Airport1 != null ? u.Airport1.IcaoID == null ? string.Empty : u.Airport1.IcaoID : string.Empty,
                                                       ArrivalAir = u.Airport != null ? u.Airport.IcaoID == null ? string.Empty : u.Airport.IcaoID : string.Empty,
                                                       DepartAirportID = u.DAirportID,
                                                       ArrivalAirportID = u.AAirportID,
                                                       PaxTotal = u.PassengerTotal.HasValue? u.PassengerTotal.ToString() : "0",
                                                       FlightHoursTotal = RounsETEbasedOnCompanyProfileSettings(u.FlightHours.HasValue ? (decimal)u.FlightHours : 0),
                                                       Miles = u.Distance.HasValue ? u.Distance.Value.ToString() : "0",
                                                       QuoteTotal = u.FlightCharge.HasValue ? u.FlightCharge.Value.ToString() : "0"
                                                   }).OrderBy(x => x.LegNum);
                            if (isBind)
                                objGrid.Rebind();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To Bind Leg Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InvoiceLeg_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindInvoiceLegs(dgInvoiceLeg, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void InvoiceLeg_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["DepartDate"] != null && !string.IsNullOrEmpty(item["DepartDate"].Text) && item["DepartDate"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                                if (date != null)
                                    item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }
                            CheckBox chk = (CheckBox)item.FindControl("chkPrint");
                            if (chk != null)
                            {
                                if (chk.Checked)
                                    chk.Checked = true;
                                else
                                    chk.Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void dgInvoiceAddFee_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            CheckBox chk = (CheckBox)item.FindControl("chkInvoiceAddfeePrint");
                            if (chk != null)
                            {
                                if (chk.Checked)
                                    chk.Checked = true;
                                else
                                    chk.Checked = false;
                            }

                            CheckBox check = (CheckBox)item.FindControl("chkInvoiceTaxable");
                            if (check != null)
                            {
                                if (check.Checked)
                                    check.Checked = true;
                                else
                                    check.Checked = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// To Load Quote CompanyDetails
        /// </summary>
        /// <param name="cqs"></param>
        private void LoadInvoiceCompanyDetail(ref CharterQuoteService.CQInvoiceMain cqs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqs))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    CQInvoiceSummary cqInvoiceSummary = new CQInvoiceSummary();
                    cqInvoiceSummary.State = CQRequestEntityState.Added;

                    cqInvoiceSummary.UsageAdjDescription = DailyUsageAdjustment;
                    cqInvoiceSummary.DescriptionAdditionalFee = AdditionalFees;
                    cqInvoiceSummary.LandingFeeDescription = LandingFees;
                    cqInvoiceSummary.SegmentFeeDescription = SegmentFees;
                    cqInvoiceSummary.StdCrewRONDescription = RonCrewCharge;
                    cqInvoiceSummary.FlightChgDescription = FlightCharge;
                    cqInvoiceSummary.SubtotalDescription = SubTotal;
                    cqInvoiceSummary.DescriptionDiscountAmt = DiscountAmount;
                    cqInvoiceSummary.TaxesDescription = Taxes;
                    cqInvoiceSummary.InvoiceDescription = TotalInvoice;
                    cqInvoiceSummary.PrepayDescription = TotalPrepay;
                    cqInvoiceSummary.RemainingAmtDescription = TotalRemainingInvoice;

                    if (FileRequest.HomebaseID != null)
                    {
                        var CompanyList = FPKMasterService.GetListInfoByHomeBaseId((long)FileRequest.HomebaseID);
                        if (CompanyList != null && CompanyList.EntityList != null && CompanyList.EntityList.Count() > 0)
                        {
                            cqs.CharterCompanyInformation = CompanyList.EntityList[0].ChtQuoteCompanyNameINV;
                            cqs.InvoiceHeader = CompanyList.EntityList[0].ChtQuoteHeaderINV;
                            cqs.InvoiceFooter = CompanyList.EntityList[0].ChtQuoteFooterINV;

                            cqs.IsPrintDetail =((!cqs.IsPrintDetail.HasValue) ? (CompanyList.EntityList[0].IsQuoteDetailPrint.HasValue && CompanyList.EntityList[0].IsQuoteDetailPrint.Value) : cqs.IsPrintDetail);
                            cqs.IsPrintSum = ((!cqs.IsPrintSum.HasValue) ? (CompanyList.EntityList[0].IsQuotePrintSum.HasValue && CompanyList.EntityList[0].IsQuotePrintSum.Value) : cqs.IsPrintSum);
                            cqs.IsPrintFees = ((!cqs.IsPrintFees.HasValue) ? (CompanyList.EntityList[0].isQuotePrintFees.HasValue && CompanyList.EntityList[0].isQuotePrintFees.Value) : cqs.IsPrintFees);

                            cqs.InvoiceAdditionalFeeTax = (cqs.InvoiceAdditionalFeeTax > 0 ? cqs.InvoiceAdditionalFeeTax : (CompanyList.EntityList[0].CQFederalTax ?? 0));

                            IsSummary = false;
                            cqs.CQInvoiceSummaries = new List<CQInvoiceSummary>();

                            cqInvoiceSummary.IsPrintUsage = CompanyList.EntityList[0].IsQuotePrepaidMinimUsageFeeAmt.HasValue && CompanyList.EntityList[0].IsQuotePrepaidMinimUsageFeeAmt.Value;
                            
                            cqInvoiceSummary.UsageAdjDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt) ? CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt : DailyUsageAdjustment;

                            //Additional Fees
                            cqInvoiceSummary.IsPrintAdditonalFees = CompanyList.EntityList[0].IsQuotePrintAdditionalFee;
                            cqInvoiceSummary.DescriptionAdditionalFee = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteAdditionalFeeDefault) ? CompanyList.EntityList[0].QuoteAdditionalFeeDefault : AdditionalFees;

                            //LAnding fee                           
                            cqInvoiceSummary.IsPrintLandingFees = CompanyList.EntityList[0].IsQuoteFlightLandingFeePrint;
                            cqInvoiceSummary.LandingFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteLandingFeeDefault) ? CompanyList.EntityList[0].QuoteLandingFeeDefault : LandingFees;

                            //Segment Fees 
                            cqInvoiceSummary.IsPrintSegmentFee = CompanyList.EntityList[0].IsQuotePrepaidSegementFee;
                            cqInvoiceSummary.SegmentFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSegmentFeeDeposit) ? CompanyList.EntityList[0].QuoteSegmentFeeDeposit : SegmentFees;

                            //std crew
                            cqInvoiceSummary.IsPrintStdCrew = CompanyList.EntityList[0].IsQuoteStdCrewRonPrepaid;
                            cqInvoiceSummary.StdCrewRONDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteStdCrewRONDeposit) ? CompanyList.EntityList[0].QuoteStdCrewRONDeposit : RonCrewCharge;

                            //Flight Charge
                            cqInvoiceSummary.IsPrintFlightChg = CompanyList.EntityList[0].IsQuoteFlightChargePrepaid;
                            cqInvoiceSummary.FlightChgDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].DepostFlightCharge) ? CompanyList.EntityList[0].DepostFlightCharge : FlightCharge;

                            //Subtotal
                            cqInvoiceSummary.IsPrintSubtotal = CompanyList.EntityList[0].IsQuoteSubtotalPrint;
                            cqInvoiceSummary.SubtotalDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSubtotal) ? CompanyList.EntityList[0].QuoteSubtotal : SubTotal;

                            //Discount Amount
                            cqInvoiceSummary.IsPrintDiscountAmt = CompanyList.EntityList[0].IsQuoteDispcountAmtPrint;
                            cqInvoiceSummary.DescriptionDiscountAmt = !string.IsNullOrEmpty(CompanyList.EntityList[0].DiscountAmountDefault) ? CompanyList.EntityList[0].DiscountAmountDefault : DiscountAmount;

                            //Taxes
                            cqInvoiceSummary.IsPrintTaxes = CompanyList.EntityList[0].IsQuoteTaxesPrint;
                            cqInvoiceSummary.TaxesDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteTaxesDefault) ? CompanyList.EntityList[0].QuoteTaxesDefault : Taxes;

                            //Total Invoice
                            cqInvoiceSummary.IsInvoiceTax = CompanyList.EntityList[0].IsQuoteInvoicePrint;
                            cqInvoiceSummary.InvoiceDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].DefaultInvoice) ? CompanyList.EntityList[0].DefaultInvoice : TotalInvoice;
                            
                            //Total Prepay                            
                            cqInvoiceSummary.IsPrintPrepay = CompanyList.EntityList[0].IsQuotePrepayPrint;
                            cqInvoiceSummary.PrepayDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuotePrepayDefault) ? CompanyList.EntityList[0].QuotePrepayDefault : TotalPrepay;

                            //Total Remaining Invoice
                            cqInvoiceSummary.IsPrintRemaingAMT = CompanyList.EntityList[0].IsQuoteRemainingAmtPrint;
                            cqInvoiceSummary.RemainingAmtDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteRemainingAmountDefault) ? CompanyList.EntityList[0].QuoteRemainingAmountDefault : TotalRemainingInvoice;

                            cqs.CQInvoiceSummaries.Add(cqInvoiceSummary);
                        }

                        LoadInvoiceAdditionalFeeandSummary(ref cqs);
                        Session["InvoiceReportMain"] = cqs;
                        LoadInvoiceData(cqs);
                        BindInvoiceAddFee(cqs, true);
                    }
                }
            }
        }

        /// <summary>
        /// To Load AdditionalFeeSummary
        /// </summary>
        /// <param name="InvoiceData"></param>
        private void LoadInvoiceAdditionalFeeandSummary(ref CharterQuoteService.CQInvoiceMain InvoiceData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InvoiceData))
            {
                using (CharterQuoteServiceClient CQService = new CharterQuoteServiceClient())
                {
                    if (Session[Master.CQSessionKey] != null)
                    {
                        Int64 QuoteNumInProgress = 1;
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null)
                        {
                            InvoiceData.CQMainID = QuoteInProgress.CQMainID;

                            if (IsSummary)
                            {
                                InvoiceData.CQInvoiceSummaries = new List<CQInvoiceSummary>();
                                CQInvoiceSummary cqInvoiceSummary = new CQInvoiceSummary();
                                cqInvoiceSummary.State = CQRequestEntityState.Added;
                                cqInvoiceSummary.IsPrintUsage = QuoteInProgress.IsPrintUsageAdj;
                                cqInvoiceSummary.UsageAdjDescription = QuoteInProgress.UsageAdjDescription;
                                cqInvoiceSummary.UsageAdjTotal = QuoteInProgress.UsageAdjTotal;
                                cqInvoiceSummary.QuoteUsageAdj = QuoteInProgress.UsageAdjTotal;
                                cqInvoiceSummary.IsUsageAdjTax = QuoteInProgress.IsDailyTaxAdj;

                                //Additional Fees
                                cqInvoiceSummary.IsPrintAdditonalFees = QuoteInProgress.IsPrintAdditonalFees;
                                cqInvoiceSummary.DescriptionAdditionalFee = QuoteInProgress.AdditionalFeeDescription;
                                cqInvoiceSummary.AdditionalFees = QuoteInProgress.AdditionalFeeTotalD;
                                cqInvoiceSummary.AdditionalFeeTotal = QuoteInProgress.AdditionalFeeTotalD;

                                //Landing fee
                                cqInvoiceSummary.IsPrintLandingFees = QuoteInProgress.IsLandingFeeTax;
                                cqInvoiceSummary.LandingFeeDescription = QuoteInProgress.LandingFeeDescription;
                                cqInvoiceSummary.QuoteLandingFee = QuoteInProgress.LandingFeeTotal;
                                cqInvoiceSummary.LandingFeeTotal = QuoteInProgress.LandingFeeTotal;
                                cqInvoiceSummary.IsLandingFeeTax = QuoteInProgress.IsLandingFeeTax;

                                //Segment Fees                      
                                cqInvoiceSummary.IsPrintSegmentFee = QuoteInProgress.IsPrintSegmentFee;
                                cqInvoiceSummary.SegmentFeeDescription = QuoteInProgress.SegmentFeeDescription;
                                cqInvoiceSummary.QuoteSegementFee = QuoteInProgress.SegmentFeeTotal;
                                cqInvoiceSummary.SegmentFeeTotal = QuoteInProgress.SegmentFeeTotal;

                                //Additional Crew  Charge
                                cqInvoiceSummary.IsPrintAdditionalCrew = QuoteInProgress.IsPrintAdditionalCrew;
                                cqInvoiceSummary.AdditionalCrewDescription = QuoteInProgress.AdditionalCrewDescription;
                                cqInvoiceSummary.AdditionalCrewRONTotal = QuoteInProgress.AdditionalCrewRONTotal1;
                                cqInvoiceSummary.AdditionalCrewTotal = QuoteInProgress.AdditionalCrewRONTotal1;

                                //Ron Crew charge
                                cqInvoiceSummary.IsPrintStdCrew = QuoteInProgress.IsPrintStdCrew;
                                cqInvoiceSummary.StdCrewRONDescription = QuoteInProgress.StdCrewROMDescription;
                                cqInvoiceSummary.StdCrewRONTotal = QuoteInProgress.StdCrewRONTotal;
                                cqInvoiceSummary.QuoteStdCrewRON = QuoteInProgress.StdCrewRONTotal;

                                //Wait Charge
                                cqInvoiceSummary.IsWaitingChg = QuoteInProgress.IsPrintWaitingChg;
                                cqInvoiceSummary.WaitChgdescription = QuoteInProgress.WaitChgDescription;
                                cqInvoiceSummary.QuoteWaitingChg = QuoteInProgress.WaitChgTotal;
                                cqInvoiceSummary.WaitChgTotal = QuoteInProgress.WaitChgTotal;

                                //Flight Charge
                                cqInvoiceSummary.IsPrintFlightChg = QuoteInProgress.IsPrintFlightChg;
                                cqInvoiceSummary.FlightChgDescription = QuoteInProgress.FlightChgDescription;
                                cqInvoiceSummary.FlightChargeTotal = QuoteInProgress.FlightChargeTotal;
                                cqInvoiceSummary.FlightCharge = QuoteInProgress.FlightChargeTotal;

                                //Subtotal
                                cqInvoiceSummary.IsPrintSubtotal = QuoteInProgress.IsPrintSubtotal;
                                cqInvoiceSummary.SubtotalDescription = QuoteInProgress.SubtotalDescription;
                                cqInvoiceSummary.QuoteSubtotal = QuoteInProgress.SubtotalTotal;
                                cqInvoiceSummary.SubtotalTotal = QuoteInProgress.SubtotalTotal;

                                //Discount Amount
                                cqInvoiceSummary.IsPrintDiscountAmt = QuoteInProgress.IsPrintDiscountAmt;
                                cqInvoiceSummary.DescriptionDiscountAmt = QuoteInProgress.DescriptionDiscountAmt;
                                cqInvoiceSummary.QuoteDiscountAmount = QuoteInProgress.DiscountAmtTotal;
                                cqInvoiceSummary.DiscountAmtTotal = QuoteInProgress.DiscountAmtTotal;

                                //Taxes
                                cqInvoiceSummary.IsPrintTaxes = QuoteInProgress.IsPrintTaxes;
                                cqInvoiceSummary.TaxesDescription = QuoteInProgress.TaxesDescription;
                                cqInvoiceSummary.QuoteTaxes = QuoteInProgress.TaxesTotal;
                                cqInvoiceSummary.TaxesTotal = QuoteInProgress.TaxesTotal;

                                //Total Invoice
                                cqInvoiceSummary.IsInvoiceTax = QuoteInProgress.IsPrintInvoice;
                                cqInvoiceSummary.InvoiceDescription = QuoteInProgress.InvoiceDescription;
                                cqInvoiceSummary.QuoteInvoice = QuoteInProgress.QuoteTotal;
                                cqInvoiceSummary.InvoiceTotal = QuoteInProgress.QuoteTotal;

                                //Total Prepay
                                cqInvoiceSummary.IsPrintPrepay = QuoteInProgress.IsPrintPay;
                                cqInvoiceSummary.PrepayDescription = QuoteInProgress.PrepayDescription;
                                cqInvoiceSummary.QuotePrepay = QuoteInProgress.PrepayTotal;
                                cqInvoiceSummary.PrepayTotal = QuoteInProgress.PrepayTotal;

                                //Total Remaining Invoice
                                cqInvoiceSummary.IsPrintRemaingAMT = QuoteInProgress.IsPrintRemaingAMT;
                                cqInvoiceSummary.RemainingAmtDescription = QuoteInProgress.RemainingAmtDescription;
                                cqInvoiceSummary.QuoteRemainingAmt = QuoteInProgress.RemainingAmtTotal;
                                cqInvoiceSummary.RemainingAmtTotal = QuoteInProgress.RemainingAmtTotal;

                                InvoiceData.CQInvoiceSummaries.Add(cqInvoiceSummary);
                            }
                            else
                            {
                                if (InvoiceData.CQInvoiceSummaries != null && InvoiceData.CQInvoiceSummaries.Where(x => x.IsDeleted == false).Count() > 0)
                                {
                                    CQInvoiceSummary cqInvoiceSummary = new CQInvoiceSummary();
                                    cqInvoiceSummary = InvoiceData.CQInvoiceSummaries.Where(x => x.IsDeleted == false).ToList()[0];
                                    if (cqInvoiceSummary != null)
                                    {
                                        if (cqInvoiceSummary.CQInvoiceSummaryID != 0 && cqInvoiceSummary.State != CQRequestEntityState.Deleted)
                                            cqInvoiceSummary.State = CQRequestEntityState.Modified;
                                        cqInvoiceSummary.UsageAdjTotal = QuoteInProgress.UsageAdjTotal;
                                        cqInvoiceSummary.QuoteUsageAdj = QuoteInProgress.UsageAdjTotal;
                                        //Additional Fees
                                        cqInvoiceSummary.AdditionalFees = QuoteInProgress.AdditionalFeeTotalD;
                                        cqInvoiceSummary.AdditionalFeeTotal = QuoteInProgress.AdditionalFeeTotalD;
                                        //LAnding fee
                                        cqInvoiceSummary.QuoteLandingFee = QuoteInProgress.LandingFeeTotal;
                                        cqInvoiceSummary.LandingFeeTotal = QuoteInProgress.LandingFeeTotal;

                                        //Segment Fees                      
                                        cqInvoiceSummary.QuoteSegementFee = QuoteInProgress.SegmentFeeTotal;
                                        cqInvoiceSummary.SegmentFeeTotal = QuoteInProgress.SegmentFeeTotal;

                                        //Ron Crew charge                       
                                        cqInvoiceSummary.StdCrewRONTotal = QuoteInProgress.StdCrewRONTotal;
                                        cqInvoiceSummary.AdditionalCrewRONTotal = QuoteInProgress.AdditionalCrewRONTotal1;

                                        ////Wait Charge
                                        cqInvoiceSummary.QuoteWaitingChg = QuoteInProgress.WaitChgTotal;
                                        cqInvoiceSummary.WaitChgTotal = QuoteInProgress.WaitChgTotal;

                                        //Flight Charge
                                        cqInvoiceSummary.FlightChargeTotal = QuoteInProgress.FlightChargeTotal;
                                        cqInvoiceSummary.FlightCharge = QuoteInProgress.FlightChargeTotal;

                                        //Subtotal
                                        cqInvoiceSummary.QuoteSubtotal = QuoteInProgress.SubtotalTotal;
                                        cqInvoiceSummary.SubtotalTotal = QuoteInProgress.SubtotalTotal;

                                        //Discount Amount
                                        cqInvoiceSummary.QuoteDiscountAmount = QuoteInProgress.DiscountAmtTotal;
                                        cqInvoiceSummary.DiscountAmtTotal = QuoteInProgress.DiscountAmtTotal;

                                        //Taxes
                                        cqInvoiceSummary.QuoteTaxes = QuoteInProgress.TaxesTotal;
                                        cqInvoiceSummary.TaxesTotal = QuoteInProgress.TaxesTotal;

                                        //Total Invoice
                                        cqInvoiceSummary.QuoteInvoice = QuoteInProgress.QuoteTotal;
                                        cqInvoiceSummary.InvoiceTotal = QuoteInProgress.QuoteTotal;

                                        //Total Prepay
                                        cqInvoiceSummary.QuotePrepay = QuoteInProgress.PrepayTotal;
                                        cqInvoiceSummary.PrepayTotal = QuoteInProgress.PrepayTotal;

                                        //Total Remaining Invoice
                                        cqInvoiceSummary.QuoteRemainingAmt = QuoteInProgress.RemainingAmtTotal;
                                        cqInvoiceSummary.RemainingAmtTotal = QuoteInProgress.RemainingAmtTotal;
                                    }
                                }
                            }

                            if (InvoiceData.CQInvoiceAdditionalFees != null)
                            {
                                foreach (CQInvoiceAdditionalFee addlFee in InvoiceData.CQInvoiceAdditionalFees.ToList())
                                {
                                    if (addlFee.CQInvoiceAdditionalFeesID != 0)
                                    {
                                        addlFee.IsDeleted = true;
                                        addlFee.State = CQRequestEntityState.Deleted;
                                    }
                                    else
                                    {
                                        InvoiceData.CQInvoiceAdditionalFees.Remove(addlFee);
                                    }
                                }
                            }

                            if (QuoteInProgress.CQFleetChargeDetails != null)
                            {
                                if (InvoiceData.CQInvoiceAdditionalFees == null)
                                    InvoiceData.CQInvoiceAdditionalFees = new List<CQInvoiceAdditionalFee>();

                                foreach (CQFleetChargeDetail fltCharge in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                                {
                                    CQInvoiceAdditionalFee addlFee = InvoiceData.CQInvoiceAdditionalFees.Where(x => x.IsDeleted == false).FirstOrDefault();
                                    CQInvoiceAdditionalFee addfee = new CQInvoiceAdditionalFee();
                                    addfee.State = CQRequestEntityState.Added;
                                    addfee.IsDeleted = false;
                                    addfee.OrderNUM = fltCharge.OrderNUM.HasValue ? fltCharge.OrderNUM : 0;
                                    addfee.CQInvoiceFeeDescription = fltCharge.CQFlightChargeDescription;
                                    addfee.QuoteAmount = fltCharge.FeeAMT.HasValue ? fltCharge.FeeAMT : 0;
                                    addfee.InvoiceAmt = fltCharge.FeeAMT.HasValue ? fltCharge.FeeAMT : 0;
                                    addfee.Quantity = fltCharge.Quantity.HasValue ? fltCharge.Quantity : 0;
                                    addfee.ChargeUnit = fltCharge.ChargeUnit;
                                    addfee.Rate = ((!fltCharge.FeeAMT.HasValue) || ((!fltCharge.Quantity.HasValue) || fltCharge.Quantity == 0)) ? 0 : fltCharge.FeeAMT / fltCharge.Quantity;
                                    addfee.IsTaxable = fltCharge.IsTaxDOM.HasValue ? fltCharge.IsTaxDOM : false;
                                    addfee.IsDiscount = fltCharge.IsDiscountDOM.HasValue ? fltCharge.IsDiscountDOM : false;
                                    addfee.IsPrintable = InvoiceData.IsPrintFees.HasValue ? (bool)InvoiceData.IsPrintFees : false; // UserPrincipal.Identity._fpSettings._isQuotePrintFees != null ? (bool)UserPrincipal.Identity._fpSettings._isQuotePrintFees : false;
                                    if (addlFee != null)
                                    {
                                        addfee.InvoiceAmt = (!addlFee.InvoiceAmt.HasValue) ? (fltCharge.FeeAMT.HasValue ? fltCharge.FeeAMT : 0) : addlFee.InvoiceAmt;
                                    }
                                    InvoiceData.CQInvoiceAdditionalFees.Add(addfee);
                                }
                            }

                            if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                            {
                                if (InvoiceData.CQInvoiceQuoteDetails == null)
                                    InvoiceData.CQInvoiceQuoteDetails = new List<CQInvoiceQuoteDetail>();
                                else
                                {
                                    foreach (CQInvoiceQuoteDetail CqInvdet in InvoiceData.CQInvoiceQuoteDetails.ToList())
                                    {
                                        if (CqInvdet.CQInvoiceQuoteDetailID != 0)
                                        {
                                            CqInvdet.State = CQRequestEntityState.Deleted;
                                            CqInvdet.IsDeleted = true;
                                        }
                                        else
                                        {
                                            InvoiceData.CQInvoiceQuoteDetails.Remove(CqInvdet);
                                        }
                                    }
                                }

                                foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).ToList())
                                {
                                    CQInvoiceQuoteDetail addQuoteDetail = new CQInvoiceQuoteDetail();
                                    addQuoteDetail.State = CQRequestEntityState.Added;
                                    addQuoteDetail.IsDeleted = false;
                                    addQuoteDetail.LegNum = Leg.LegNUM.HasValue ? Leg.LegNUM : 0;
                                    addQuoteDetail.DepartureDTTMLocal = Leg.DepartureDTTMLocal;
                                    addQuoteDetail.ArrivalDTTMLocal = Leg.ArrivalDTTMLocal;
                                    addQuoteDetail.Airport1 = Leg.Airport1;
                                    addQuoteDetail.Airport = Leg.Airport;
                                    addQuoteDetail.DAirportID = Leg.DAirportID.HasValue ? Leg.DAirportID : 0;
                                    addQuoteDetail.AAirportID = Leg.AAirportID.HasValue ? Leg.AAirportID : 0;
                                    addQuoteDetail.PassengerTotal = Leg.PassengerTotal.HasValue ? Leg.PassengerTotal : 0;
                                    addQuoteDetail.FlightHours = Leg.ElapseTM.HasValue ? Leg.ElapseTM : 0;
                                    addQuoteDetail.Distance = Leg.Distance.HasValue ? Leg.Distance : 0;
                                    addQuoteDetail.FlightCharge = Leg.ChargeTotal.HasValue ? Leg.ChargeTotal : 0;
                                    addQuoteDetail.IsPrintable = InvoiceData.IsPrintDetail.HasValue ? (bool)InvoiceData.IsPrintDetail : false; // UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint : false;
                                    addQuoteDetail.TaxRate = InvoiceData.InvoiceAdditionalFeeTax.HasValue ? InvoiceData.InvoiceAdditionalFeeTax : 0; // UserPrincipal.Identity._fpSettings._CQFederalTax != null ? UserPrincipal.Identity._fpSettings._CQFederalTax : 0;
                                    InvoiceData.CQInvoiceQuoteDetails.Add(addQuoteDetail);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To Bind Quote Add Fee
        /// </summary>
        /// <param name="InvoiceData"></param>
        /// <param name="isBind"></param>
        private void BindInvoiceAddFee(CharterQuoteService.CQInvoiceMain InvoiceData, bool isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InvoiceData, isBind))
            {
                if (InvoiceData != null && InvoiceData.CQInvoiceAdditionalFees != null && InvoiceData.CQInvoiceAdditionalFees.Count() > 0)
                {
                    dgInvoiceAddFee.DataSource = InvoiceData.CQInvoiceAdditionalFees.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM).ToList();
                }
                else
                {
                    dgInvoiceAddFee.DataSource = string.Empty;
                }
                if (isBind)
                    dgInvoiceAddFee.Rebind();
            }
        }

        /// <summary>
        /// To Load Quote Data
        /// </summary>
        /// <param name="QuoteData"></param>
        private void LoadInvoiceData(CharterQuoteService.CQInvoiceMain InvoiceData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InvoiceData))
            {
                FileRequest = (CQFile)Session[Master.CQSessionKey];
                if (InvoiceData != null)
                {
                    tbInvoiceCompanyInfo.Text = InvoiceData.CharterCompanyInformation ?? string.Empty;
                    tbMyCharterCustomer.Text = InvoiceData.CustomerAddressInformation ?? string.Empty;
                    tbInvoiceHeaderInfo.Text = InvoiceData.InvoiceHeader ?? string.Empty;
                    tbInvoiceFooterInfo.Text = InvoiceData.InvoiceFooter ?? string.Empty;

                    if (InvoiceData.InvoiceFooter != null)
                    {
                        if (FileRequest != null && FileRequest.HomebaseID.HasValue)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetCQMessage("I", (long)FileRequest.HomebaseID).EntityList;
                                if (RetValue != null && RetValue.Count > 0)
                                {
                                    ddlInvoiceMyCustomerFooter.Items.Clear();
                                    int i = 0;
                                    foreach (FlightPakMasterService.CQMessage fwh in RetValue)
                                    {
                                        string encodedDescription = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.MessageDescription);
                                        string encodedID = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.CQMessageID.ToString());

                                        ddlInvoiceMyCustomerFooter.Items.Insert(i, new ListItem((encodedDescription), encodedID));
                                        i = i + 1;
                                    }
                                }
                            }
                        }
                        if (FileRequest != null && FileRequest.Company != null && FileRequest.Company.ChtQuoteFooterINV != null)
                        {
                            string Item = FileRequest.Company.ChtQuoteFooterINV;
                            if (ddlInvoiceMyCustomerFooter.Items.FindByText(Item) != null)
                            {
                                ddlInvoiceMyCustomerFooter.Items.FindByText(Item).Selected = true;
                                tbInvoiceFooterInfo.Text = string.IsNullOrEmpty(tbInvoiceFooterInfo.Text) ? ddlInvoiceMyCustomerFooter.SelectedItem.ToString() : tbInvoiceFooterInfo.Text;
                            }
                        }
                    }

                    //Print Invocie Detai l
                    if (InvoiceData.IsPrintDetail.HasValue)
                    {
                        chkInvoicePrintDetail.Checked = (bool)InvoiceData.IsPrintDetail;
                    }

                    if (InvoiceData.IsPrintQuoteNUM.HasValue)
                    {
                        chkInvoicePrintQuoteNo.Checked = (bool)InvoiceData.IsPrintQuoteNUM;
                    }
                    if (InvoiceData.IsPrintFees.HasValue)
                    {
                        chkInvoicePrintAdditionalFees.Checked = (bool)InvoiceData.IsPrintFees;
                    }
                    if (InvoiceData.IsInvoiceAdditionalFeeTax.HasValue)
                    {
                        chkInvoiceAdditionalFeesTaxable.Checked = (bool)InvoiceData.IsInvoiceAdditionalFeeTax;
                    }
                    if (InvoiceData.IsPrintSum.HasValue)
                    {
                        chkÍnvoicePrintSummary.Checked = (bool)InvoiceData.IsPrintSum;
                    }
                    if (InvoiceData.InvoiceAdditionalFeeTax.HasValue)
                    {
                        tbInvocieAdditionalFeeTaxRate.Text = InvoiceData.InvoiceAdditionalFeeTax.ToString();
                    }

                    if (InvoiceData.IsManualDiscount.HasValue)
                    {
                        chkManualDiscountEntry.Checked = (bool)InvoiceData.IsManualDiscount;
                    }
                    if (InvoiceData.IsManualTax.HasValue)
                    {
                        chkInvoiceManualTaxEntry.Checked = (bool)InvoiceData.IsManualTax;
                    }
                    if (InvoiceData.IsSumTax.HasValue)
                    {
                        chkInvoiceSummaryFeesTaxable.Checked = (bool)InvoiceData.IsSumTax;
                    }
                    //tbInvoiceTaxRate
                    if (InvoiceData.CQInvoiceQuoteDetails != null && InvoiceData.CQInvoiceQuoteDetails.Where(x => x.IsDeleted == false).ToList().Count() > 0 && InvoiceData.CQInvoiceQuoteDetails.Where(x => x.IsDeleted == false).ToList()[0].TaxRate != null)
                    {
                        tbInvoiceTaxRate.Text = InvoiceData.CQInvoiceQuoteDetails.Where(x => x.IsDeleted == false).ToList()[0].TaxRate.ToString();
                    }

                    SetInvoicePrintSummaryStatus(chkÍnvoicePrintSummary.Checked);

                    SetInvoiceSummaryFeesTaxableStatus(chkInvoiceSummaryFeesTaxable.Checked);
                    
                    if (InvoiceData.CQInvoiceSummaries != null && InvoiceData.CQInvoiceSummaries.Where(x => x.IsDeleted == false).Count() > 0)
                    {
                        CQInvoiceSummary objInvoiceSum = InvoiceData.CQInvoiceSummaries.Where(x => x.IsDeleted == false).ToList()[0];

                        if (objInvoiceSum.IsPrintUsage.HasValue)
                        {
                            chkInvoiceDailyUsageAdjustmentPrint.Checked = (bool)objInvoiceSum.IsPrintUsage;
                        }
                        tbInvoiceDailyUsageAdjustment.Text = objInvoiceSum.UsageAdjDescription ?? string.Empty;
                        tbInvoicedailyusageInvoiceAmt.Text = objInvoiceSum.UsageAdjTotal.HasValue ? objInvoiceSum.UsageAdjTotal.ToString() : string.Empty;
                        tbInvoicedailyusageQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteUsageAdj.HasValue ? objInvoiceSum.QuoteUsageAdj.ToString() : string.Empty);
                        if (objInvoiceSum.IsUsageAdjTax.HasValue)
                        {
                            chkDailyusageTaxable.Checked = (bool)objInvoiceSum.IsUsageAdjTax;
                        }

                        //Additional Fees
                        if (objInvoiceSum.IsPrintAdditonalFees.HasValue)
                        {
                            chkInvoiceAdditionalfeePrint.Checked = (bool)objInvoiceSum.IsPrintAdditonalFees;
                        }
                        tbInvoiceAdditionalFeeDescription.Text = objInvoiceSum.DescriptionAdditionalFee ?? string.Empty;
                        tbInvoiceAdditionalfeeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.AdditionalFees.HasValue ? objInvoiceSum.AdditionalFees.ToString() : string.Empty);
                        tbInvoiceAdditionalfeeInvoiceAmt.Text = objInvoiceSum.AdditionalFeeTotal.HasValue ? objInvoiceSum.AdditionalFeeTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsAdditionalFeesTax.HasValue)
                        {
                            chkInvoiceAdditionalfeeInvoiceTaxable.Checked = (bool)objInvoiceSum.IsAdditionalFeesTax;
                        }
                        //LAnding fee
                        if (objInvoiceSum.IsPrintLandingFees.HasValue)
                        {
                            chkInvoiceLandingfeePrint.Checked = (bool)objInvoiceSum.IsPrintLandingFees;
                        }
                        tbInvoiceLandingFeeDescription.Text = objInvoiceSum.LandingFeeDescription != null ? objInvoiceSum.LandingFeeDescription.ToString() : string.Empty;
                        lbInvoiceLandingfeeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteLandingFee.HasValue ? objInvoiceSum.QuoteLandingFee.ToString() : string.Empty);
                        tbInvoiceLandingfeeInvoiceAmt.Text = objInvoiceSum.LandingFeeTotal.HasValue ? objInvoiceSum.LandingFeeTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsLandingFeeTax.HasValue)
                        {
                            chkInvoiceLandingfeeInvoiceTaxable.Checked = (bool)objInvoiceSum.IsLandingFeeTax;
                        }
                        if (objInvoiceSum.IsLandingFeeDiscount.HasValue)
                        {
                            chkInvoiceLandingfeeInvoiceDiscount.Checked = (bool)objInvoiceSum.IsLandingFeeDiscount;
                        }
                        //Segment Fees 
                        if (objInvoiceSum.IsPrintSegmentFee.HasValue)
                        {
                            chkInvoiceSegmentFeePrint.Checked = (bool)objInvoiceSum.IsPrintSegmentFee;
                        }
                        tbInvoiceSegmentFeeDescription.Text = objInvoiceSum.SegmentFeeDescription != null ? objInvoiceSum.SegmentFeeDescription.ToString() : string.Empty;
                        tbInvoiceSegmentFeeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteSegementFee.HasValue ? objInvoiceSum.QuoteSegementFee.ToString() : string.Empty);
                        tbInvoiceSegmentFeeInvoiceAmt.Text = objInvoiceSum.SegmentFeeTotal.HasValue ? objInvoiceSum.SegmentFeeTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsSegmentFeeTax.HasValue)
                        {
                            chkInvoiceSegmentFeetaxable.Checked = (bool)objInvoiceSum.IsSegmentFeeTax;
                        }

                        //Ron Crew charge     
                        if (objInvoiceSum.IsPrintStdCrew.HasValue)
                        {
                            chkInvoiceRonCrewChargePrint.Checked = (bool)objInvoiceSum.IsPrintStdCrew;
                        }
                        tbInvoiceRonCrewChargeDescription.Text = objInvoiceSum.StdCrewRONDescription != null ? objInvoiceSum.StdCrewRONDescription.ToString() : string.Empty;
                        tbInvoiceRonCrewChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteStdCrewRON.HasValue ? objInvoiceSum.QuoteStdCrewRON.ToString() : string.Empty);
                        tbInvocieRonCrewChargeInvoiceAmt.Text = objInvoiceSum.StdCrewRONTotal.HasValue ? objInvoiceSum.StdCrewRONTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsStdCrewRONTax.HasValue)
                        {
                            chkInvoiceRonCrewChargeTaxable.Checked = (bool)objInvoiceSum.IsStdCrewRONTax;
                        }
                        if (objInvoiceSum.IsStdCrewRONDiscount.HasValue)
                        {
                            chkInvoiceRonCrewChargeDiscount.Checked = (bool)objInvoiceSum.IsStdCrewRONDiscount;
                            chkDailyusageDiscount.Checked = (bool)objInvoiceSum.IsStdCrewRONDiscount;
                        }
                        //Additional Crew  Charge
                        if (objInvoiceSum.IsPrintAdditionalCrew.HasValue)
                        {
                            chkInvoiceAddCrewChargePrint.Checked = (bool)objInvoiceSum.IsPrintAdditionalCrew;
                        }
                        tbInvoiceAddrewChargeDescription.Text = objInvoiceSum.AdditionalCrewDescription != null ? objInvoiceSum.AdditionalCrewDescription.ToString() : string.Empty;
                        lbInvoiceAddCrewChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.CQAdditionalCrew.HasValue ? objInvoiceSum.CQAdditionalCrew.ToString() : string.Empty);
                        tbInvocieAddCrewChargeInvoiceAmt.Text = objInvoiceSum.AdditionalCrewTotal.HasValue ? objInvoiceSum.AdditionalCrewTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsAdditionalCrewTax.HasValue)
                        {
                            chkInvoiceAddCrewChargeTaxable.Checked = (bool)objInvoiceSum.IsAdditionalCrewTax;
                        }
                        if (objInvoiceSum.AdditionalCrewDiscount.HasValue)
                        {
                            chkInvoiceAddCrewChargeDiscount.Checked = (bool)objInvoiceSum.AdditionalCrewDiscount;
                        }
                        //Additional Crew Ron Charge
                        if (objInvoiceSum.IsAdditionalCrewRON.HasValue)
                        {
                            chkInvoiceAdditionalRonCrewChargePrint.Checked = (bool)objInvoiceSum.IsAdditionalCrewRON;
                        }
                        tbInvoiceAdditionalRonCrewChargeDescription.Text = objInvoiceSum.AdditionalCrewRON != null ? objInvoiceSum.AdditionalCrewRON.ToString() : string.Empty;
                        lbInvoiceAdditionalRonCrewChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.AdditionalRONCrew.HasValue ? objInvoiceSum.AdditionalRONCrew.ToString() : string.Empty);
                        tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text = objInvoiceSum.AdditionalCrewRONTotal.HasValue ? objInvoiceSum.AdditionalCrewRONTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsAdditionalCrewRONTax.HasValue)
                        {
                            chkInvoiceAdditionalRonCrewChargeTaxable.Checked = (bool)objInvoiceSum.IsAdditionalCrewRONTax;
                        }
                        if (objInvoiceSum.IsAdditionalCrewRONDiscount.HasValue)
                        {
                            chkInvoiceAdditionalRonCrewChargeDiscount.Checked = (bool)objInvoiceSum.IsAdditionalCrewRONDiscount;
                        }

                        //Wait Charge
                        if (objInvoiceSum.IsWaitingChg.HasValue)
                        {
                            chkInvoiceWaitCharge.Checked = (bool)objInvoiceSum.IsWaitingChg;
                        }
                        tbInvoiceWaitChargeDescription.Text = objInvoiceSum.WaitChgdescription != null ? objInvoiceSum.WaitChgdescription.ToString() : string.Empty;
                        lbInvoiceWaitChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteWaitingChg.HasValue ? objInvoiceSum.QuoteWaitingChg.ToString() : string.Empty);
                        tbInvoiceWaitChargeInvocieAmt.Text = objInvoiceSum.WaitChgTotal.HasValue ? objInvoiceSum.WaitChgTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsWaitingChgTax.HasValue)
                        {
                            chkInvoiceWaitChargeTaxable.Checked = (bool)objInvoiceSum.IsWaitingChgTax;
                        }
                        if (objInvoiceSum.IsWaitingChgDiscount.HasValue)
                        {
                            chkInvoiceWaitChargeDiscount.Checked = (bool)objInvoiceSum.IsWaitingChgDiscount;
                        }
                        //Flight Charge
                        if (objInvoiceSum.IsPrintFlightChg.HasValue)
                        {
                            chkInvoiceFlightCharge.Checked = (bool)objInvoiceSum.IsPrintFlightChg;
                        }
                        tbInvoiceFlightChargeDescription.Text = objInvoiceSum.FlightChgDescription != null ? objInvoiceSum.FlightChgDescription.ToString() : string.Empty;
                        tbInvoiceFlightChargeQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.FlightCharge.HasValue ? objInvoiceSum.FlightCharge.ToString() : string.Empty);
                        tbInvoiceFlightChargeInvocieAmt.Text = objInvoiceSum.FlightChargeTotal.HasValue ? objInvoiceSum.FlightChargeTotal.ToString() : string.Empty;
                        if (objInvoiceSum.IsFlightChgDiscount.HasValue)
                        {
                            chkInvoiceFlightChargeDiscount.Checked = (bool)objInvoiceSum.IsFlightChgDiscount;
                            chkDailyusageDiscount.Checked = (bool)objInvoiceSum.IsFlightChgDiscount;
                        }

                        //Subtotal
                        if (objInvoiceSum.IsPrintSubtotal.HasValue)
                        {
                            chkInvoiceSubtotalPrint.Checked = (bool)objInvoiceSum.IsPrintSubtotal;
                        }
                        tbInvoiceSubtotalDecription.Text = objInvoiceSum.SubtotalDescription != null ? objInvoiceSum.SubtotalDescription.ToString() : string.Empty;
                        tbInvoiceSubtotalQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteSubtotal.HasValue ? objInvoiceSum.QuoteSubtotal.ToString() : string.Empty);
                        tbInvoiceSubtotalInvoiceAmt.Text = objInvoiceSum.SubtotalTotal.HasValue ? objInvoiceSum.SubtotalTotal.ToString() : string.Empty;

                        //Discount Amount
                        if (objInvoiceSum.IsPrintDiscountAmt.HasValue)
                        {
                            chkInvoiceDiscountPrint.Checked = (bool)objInvoiceSum.IsPrintDiscountAmt;
                        }
                        tbInvoiceDiscountdescription.Text = objInvoiceSum.DescriptionDiscountAmt != null ? objInvoiceSum.DescriptionDiscountAmt.ToString() : string.Empty;
                        tbInvoiceDiscountQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteDiscountAmount.HasValue ? objInvoiceSum.QuoteDiscountAmount.ToString() : string.Empty);
                        tbInvoiceDiscountdescription.Text = objInvoiceSum.DescriptionDiscountAmt != null ? objInvoiceSum.DescriptionDiscountAmt.ToString() : string.Empty;
                        tbInvoiceDiscountInvoiceAmt.Text = objInvoiceSum.DiscountAmtTotal.HasValue ? objInvoiceSum.DiscountAmtTotal.ToString() : string.Empty;
                        tbInvoiceDiscountInvoiceAmt.Enabled = chkManualDiscountEntry.Checked == true;
                        //Taxes
                        if (objInvoiceSum.IsPrintTaxes.HasValue)
                        {
                            chkInvoiceTaxesPrint.Checked = (bool)objInvoiceSum.IsPrintTaxes;
                        }
                        tbInvoiceTaxesDescription.Text = objInvoiceSum.TaxesDescription != null ? objInvoiceSum.TaxesDescription.ToString() : string.Empty;
                        tbInvoiceTaxesQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteTaxes.HasValue ? objInvoiceSum.QuoteTaxes.ToString() : string.Empty);
                        tbInvoiceTaxesInvoiceAmt.Text = objInvoiceSum.TaxesTotal.HasValue ? objInvoiceSum.TaxesTotal.ToString() : string.Empty;
                        tbInvoiceTaxesInvoiceAmt.Enabled = chkInvoiceManualTaxEntry.Checked == true;
                        //Total Invoice
                        if (objInvoiceSum.IsInvoiceTax.HasValue)
                        {
                            chkInvoiceTotalInvoicePrint.Checked = (bool)objInvoiceSum.IsInvoiceTax;
                        }
                        tbInvoiceTotalInvoiceDescription.Text = objInvoiceSum.InvoiceDescription != null ? objInvoiceSum.InvoiceDescription.ToString() : string.Empty;
                        tbInvoiceTotalInvoiceQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteInvoice.HasValue ? objInvoiceSum.QuoteInvoice.ToString() : string.Empty);
                        tbInvoiceTotalInvoiceInvoiceAmt.Text = objInvoiceSum.InvoiceTotal.HasValue ? objInvoiceSum.InvoiceTotal.ToString() : string.Empty;
                        //Total Prepay
                        if (objInvoiceSum.IsPrintPrepay.HasValue)
                        {
                            chkInvoiceTotalPrepayPrint.Checked = (bool)objInvoiceSum.IsPrintPrepay;
                        }
                        tbInvoiceTotalPrepayDescription.Text = objInvoiceSum.PrepayDescription != null ? objInvoiceSum.PrepayDescription.ToString() : string.Empty;
                        tbInvoiceTotalPrepayQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuotePrepay.HasValue ? objInvoiceSum.QuotePrepay.ToString() : string.Empty);
                        tbInvoiceTotalPrepayInvoiceAmt.Text = objInvoiceSum.PrepayTotal.HasValue ? objInvoiceSum.PrepayTotal.ToString() : string.Empty;
                        //Total Remaining Invoice
                        if (objInvoiceSum.IsPrintRemaingAMT.HasValue)
                        {
                            chkInvoiceTotalRemainingPrint.Checked = (bool)objInvoiceSum.IsPrintRemaingAMT;
                        }
                        tbInvoiceTotalRemainingDescription.Text = objInvoiceSum.RemainingAmtDescription != null ? objInvoiceSum.RemainingAmtDescription.ToString() : string.Empty;
                        tbInvoiceTotalRemainingQuoteAmt.Text = System.Web.HttpUtility.HtmlEncode(objInvoiceSum.QuoteRemainingAmt.HasValue ? objInvoiceSum.QuoteRemainingAmt.ToString() : string.Empty);
                        tbInvoiceTotalRemainingInvoiceAmt.Text = objInvoiceSum.RemainingAmtTotal.HasValue ? objInvoiceSum.RemainingAmtTotal.ToString() : string.Empty;
                    }
                    BindInvoiceAddFee(InvoiceData, true);
                    BindInvoiceLegs(dgInvoiceLeg, true);
                }
            }
        }

        protected void InvoiceRefreshYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CharterQuoteService.CQInvoiceMain cqs = new CharterQuoteService.CQInvoiceMain();
                        if (Session["InvoiceReportMain"] != null)
                        {
                            cqs = (CQInvoiceMain)Session["InvoiceReportMain"];
                            if (cqs.CQInvoiceMainID != 0)
                            {
                                if ((CQFile)Session[Master.CQSessionKey] != null)
                                {
                                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                                    int QuoteNumInProgress = 1;

                                    QuoteNumInProgress = (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0) ? Convert.ToInt32(FileRequest.QuoteNumInProgress) : QuoteNumInProgress;
                                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                                    if (QuoteInProgress != null)
                                    {
                                        cqs.State = CQRequestEntityState.Modified;
                                        cqs.CQMainID = QuoteInProgress.CQMainID;

                                        cqs.IsInvoiceAdditionalFeeTax = ((!cqs.IsInvoiceAdditionalFeeTax.HasValue) ? QuoteInProgress.IsPrintAdditonalFees : cqs.IsInvoiceAdditionalFeeTax);

                                        #region Bind Customer Address Info for Invoice, When Refresh Button Clicked

                                        if (FileRequest.CQCustomerID.HasValue && FileRequest.CQCustomerID > 0)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.CQCustomer custObj = new FlightPakMasterService.CQCustomer();
                                                custObj.CQCustomerID = Convert.ToInt64(FileRequest.CQCustomerID);

                                                var ReturnValue = objMasterService.GetCQCustomerByCQCustomerID(custObj);

                                                if (ReturnValue != null && ReturnValue.EntityList.Count > 0)
                                                {
                                                    StringBuilder custAddr = new StringBuilder();
                                                    if (ReturnValue.EntityList[0].CQCustomerName != null)
                                                        custAddr.AppendLine(ReturnValue.EntityList[0].CQCustomerName);
                                                    if (ReturnValue.EntityList[0].BillingAddr1 != null)
                                                        custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr1.ToString().Trim());
                                                    if (ReturnValue.EntityList[0].BillingAddr2 != null)
                                                        custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr2.ToString().Trim());
                                                    if (ReturnValue.EntityList[0].BillingAddr3 != null)
                                                        custAddr.AppendLine(ReturnValue.EntityList[0].BillingAddr3.ToString().Trim());

                                                    if (ReturnValue.EntityList[0].BillingCity != null)
                                                        custAddr.Append(ReturnValue.EntityList[0].BillingCity.ToString().Trim() + ", ");
                                                    if (ReturnValue.EntityList[0].BillingState != null)
                                                        custAddr.Append(ReturnValue.EntityList[0].BillingState.ToString().Trim() + ", ");
                                                    if (ReturnValue.EntityList[0].BillingZip != null)
                                                        custAddr.Append(ReturnValue.EntityList[0].BillingZip.ToString().Trim() + ", ");
                                                    if (ReturnValue.EntityList[0].CountryCD != null)
                                                        custAddr.Append(ReturnValue.EntityList[0].CountryCD.ToString().Trim());

                                                    if(string.IsNullOrEmpty(cqs.CustomerAddressInformation))
                                                        cqs.CustomerAddressInformation = custAddr.ToString();
                                                }
                                            }
                                        }

                                        #endregion

                                        #region Invoice Summary

                                        CQInvoiceSummary invoiceSummary = new CQInvoiceSummary();
                                        if (cqs.CQInvoiceSummaries != null && cqs.CQInvoiceSummaries.Count > 0)
                                        {
                                            invoiceSummary = cqs.CQInvoiceSummaries[0];
                                            if (invoiceSummary.CQInvoiceSummaryID != 0)
                                                invoiceSummary.State = CQRequestEntityState.Modified;
                                        }
                                        else
                                        {
                                            cqs.CQInvoiceSummaries = new List<CQInvoiceSummary>();
                                            invoiceSummary.State = CQRequestEntityState.Added;
                                        }


                                        //Usage Adjustment Total
                                        invoiceSummary.IsPrintUsage = invoiceSummary.IsPrintUsage.HasValue ? invoiceSummary.IsPrintUsage : QuoteInProgress.IsPrintUsageAdj;
                                        invoiceSummary.IsUsageAdjTax = invoiceSummary.IsUsageAdjTax.HasValue ? invoiceSummary.IsUsageAdjTax : QuoteInProgress.IsDailyTaxAdj;
                                        invoiceSummary.UsageAdjTotal = invoiceSummary.UsageAdjTotal > 0 ? invoiceSummary.UsageAdjTotal : QuoteInProgress.UsageAdjTotal;
                                        invoiceSummary.QuoteUsageAdj = invoiceSummary.QuoteUsageAdj > 0 ? invoiceSummary.QuoteUsageAdj : QuoteInProgress.UsageAdjTotal;
                                        invoiceSummary.UsageAdjDescription = !string.IsNullOrEmpty(QuoteInProgress.UsageAdjDescription) ? QuoteInProgress.UsageAdjDescription : DailyUsageAdjustment;

                                        //Additional Fees
                                        invoiceSummary.IsPrintAdditonalFees = invoiceSummary.IsPrintAdditonalFees.HasValue ? invoiceSummary.IsPrintAdditonalFees : QuoteInProgress.IsPrintAdditonalFees;
                                        invoiceSummary.AdditionalFees = invoiceSummary.AdditionalFees > 0 ? invoiceSummary.AdditionalFees : QuoteInProgress.AdditionalFeeTotalD;
                                        invoiceSummary.AdditionalFeeTotal = invoiceSummary.AdditionalFeeTotal > 0 ? invoiceSummary.AdditionalFeeTotal : QuoteInProgress.AdditionalFeeTotalD;
                                        invoiceSummary.DescriptionAdditionalFee = !string.IsNullOrEmpty(QuoteInProgress.AdditionalFeeDescription) ? QuoteInProgress.AdditionalFeeDescription : AdditionalFees;

                                        //Landing fee
                                        invoiceSummary.IsLandingFeeTax = invoiceSummary.IsLandingFeeTax!=null ? invoiceSummary.IsLandingFeeTax : QuoteInProgress.IsLandingFeeTax;
                                        invoiceSummary.IsPrintLandingFees = invoiceSummary.IsPrintLandingFees.HasValue ? invoiceSummary.IsPrintLandingFees : QuoteInProgress.IsLandingFeeTax;
                                        invoiceSummary.QuoteLandingFee = invoiceSummary.QuoteLandingFee > 0 ? invoiceSummary.QuoteLandingFee : QuoteInProgress.LandingFeeTotal;
                                        invoiceSummary.LandingFeeTotal = invoiceSummary.LandingFeeTotal > 0 ? invoiceSummary.LandingFeeTotal : QuoteInProgress.LandingFeeTotal;
                                        invoiceSummary.LandingFeeDescription = !string.IsNullOrEmpty(QuoteInProgress.LandingFeeDescription) ? QuoteInProgress.LandingFeeDescription : LandingFees;

                                        //Segment Fees                      
                                        invoiceSummary.IsPrintSegmentFee = invoiceSummary.IsPrintSegmentFee.HasValue ? invoiceSummary.IsPrintSegmentFee : QuoteInProgress.IsPrintSegmentFee;
                                        invoiceSummary.QuoteSegementFee = invoiceSummary.QuoteSegementFee > 0 ? invoiceSummary.QuoteSegementFee : QuoteInProgress.SegmentFeeTotal;
                                        invoiceSummary.SegmentFeeTotal = invoiceSummary.SegmentFeeTotal > 0 ? invoiceSummary.SegmentFeeTotal : QuoteInProgress.SegmentFeeTotal;
                                        invoiceSummary.SegmentFeeDescription = !string.IsNullOrEmpty(QuoteInProgress.SegmentFeeDescription) ? QuoteInProgress.SegmentFeeDescription : SegmentFees;

                                        //Additional Crew charge                       
                                        invoiceSummary.IsPrintAdditionalCrew = invoiceSummary.IsPrintAdditionalCrew.HasValue ? invoiceSummary.IsPrintAdditionalCrew : QuoteInProgress.IsPrintAdditionalCrew;
                                        invoiceSummary.AdditionalCrewRONTotal = invoiceSummary.AdditionalCrewRONTotal > 0 ? invoiceSummary.AdditionalCrewRONTotal : QuoteInProgress.AdditionalCrewRONTotal1;
                                        invoiceSummary.AdditionalCrewRON = !string.IsNullOrEmpty(QuoteInProgress.AdditionalDescriptionCrewRON) ? QuoteInProgress.AdditionalDescriptionCrewRON : AdditionalRonCrewCharge;

                                        //Ron Crew charge
                                        invoiceSummary.IsPrintStdCrew = invoiceSummary.IsPrintStdCrew.HasValue ? invoiceSummary.IsPrintStdCrew : QuoteInProgress.IsPrintStdCrew;
                                        invoiceSummary.StdCrewRONTotal = invoiceSummary.StdCrewRONTotal > 0 ? invoiceSummary.StdCrewRONTotal : QuoteInProgress.StdCrewRONTotal;
                                        invoiceSummary.QuoteStdCrewRON = invoiceSummary.QuoteStdCrewRON > 0 ? invoiceSummary.QuoteStdCrewRON : QuoteInProgress.StdCrewRONTotal;
                                        invoiceSummary.StdCrewRONDescription = !string.IsNullOrEmpty(QuoteInProgress.StdCrewROMDescription) ? QuoteInProgress.StdCrewROMDescription : RonCrewCharge;

                                        //Wait Charge
                                        invoiceSummary.IsWaitingChg = invoiceSummary.IsWaitingChg.HasValue ? invoiceSummary.IsWaitingChg : QuoteInProgress.IsPrintWaitingChg;
                                        invoiceSummary.QuoteWaitingChg = invoiceSummary.QuoteWaitingChg > 0 ? invoiceSummary.QuoteWaitingChg : QuoteInProgress.WaitChgTotal;
                                        invoiceSummary.WaitChgTotal = invoiceSummary.WaitChgTotal > 0 ? invoiceSummary.WaitChgTotal : QuoteInProgress.WaitChgTotal;
                                        invoiceSummary.WaitChgdescription = !string.IsNullOrEmpty(QuoteInProgress.WaitChgDescription) ? QuoteInProgress.WaitChgDescription : WaitCharge;

                                        //Flight Charge
                                        invoiceSummary.IsPrintFlightChg = invoiceSummary.IsPrintFlightChg.HasValue ? invoiceSummary.IsPrintFlightChg : QuoteInProgress.IsPrintFlightChg;
                                        invoiceSummary.FlightChargeTotal = invoiceSummary.FlightChargeTotal > 0 ? invoiceSummary.FlightChargeTotal : QuoteInProgress.FlightChargeTotal;
                                        invoiceSummary.FlightCharge = invoiceSummary.FlightCharge > 0 ? invoiceSummary.FlightCharge : QuoteInProgress.FlightChargeTotal;
                                        invoiceSummary.FlightChgDescription = !string.IsNullOrEmpty(QuoteInProgress.FlightChgDescription) ? QuoteInProgress.FlightChgDescription : FlightCharge;

                                        //Subtotal
                                        invoiceSummary.IsPrintSubtotal = invoiceSummary.IsPrintSubtotal.HasValue ? invoiceSummary.IsPrintSubtotal : QuoteInProgress.IsPrintSubtotal;
                                        invoiceSummary.QuoteSubtotal = invoiceSummary.QuoteSubtotal > 0 ? invoiceSummary.QuoteSubtotal : QuoteInProgress.SubtotalTotal;
                                        invoiceSummary.SubtotalTotal = invoiceSummary.SubtotalTotal > 0 ? invoiceSummary.SubtotalTotal : QuoteInProgress.SubtotalTotal;
                                        invoiceSummary.SubtotalDescription = !string.IsNullOrEmpty(QuoteInProgress.SubtotalDescription) ? QuoteInProgress.SubtotalDescription : SubTotal;

                                        //Discount Amount
                                        invoiceSummary.IsPrintDiscountAmt = invoiceSummary.IsPrintDiscountAmt.HasValue ? invoiceSummary.IsPrintDiscountAmt : QuoteInProgress.IsPrintDiscountAmt;
                                        invoiceSummary.QuoteDiscountAmount = invoiceSummary.QuoteDiscountAmount > 0 ? invoiceSummary.QuoteDiscountAmount : QuoteInProgress.DiscountAmtTotal;
                                        invoiceSummary.DiscountAmtTotal = invoiceSummary.DiscountAmtTotal > 0 ? invoiceSummary.DiscountAmtTotal : QuoteInProgress.DiscountAmtTotal;
                                        invoiceSummary.DescriptionDiscountAmt = !string.IsNullOrEmpty(QuoteInProgress.DescriptionDiscountAmt) ? QuoteInProgress.DescriptionDiscountAmt : DiscountAmount;

                                        //Taxes
                                        invoiceSummary.IsPrintTaxes = invoiceSummary.IsPrintTaxes.HasValue ? invoiceSummary.IsPrintTaxes : QuoteInProgress.IsPrintTaxes;
                                        invoiceSummary.QuoteTaxes = invoiceSummary.QuoteTaxes > 0 ? invoiceSummary.QuoteTaxes : QuoteInProgress.TaxesTotal;
                                        invoiceSummary.TaxesTotal = invoiceSummary.TaxesTotal > 0 ? invoiceSummary.TaxesTotal : QuoteInProgress.TaxesTotal;
                                        invoiceSummary.TaxesDescription = !string.IsNullOrEmpty(QuoteInProgress.TaxesDescription) ? QuoteInProgress.TaxesDescription : Taxes;

                                        //Total Invoice
                                        invoiceSummary.IsInvoiceTax = invoiceSummary.IsInvoiceTax.HasValue ? invoiceSummary.IsInvoiceTax : QuoteInProgress.IsPrintInvoice;
                                        invoiceSummary.QuoteInvoice = invoiceSummary.QuoteInvoice > 0 ? invoiceSummary.QuoteInvoice : QuoteInProgress.QuoteTotal;
                                        invoiceSummary.InvoiceTotal = invoiceSummary.InvoiceTotal > 0 ? invoiceSummary.InvoiceTotal : QuoteInProgress.QuoteTotal;
                                        invoiceSummary.InvoiceDescription = !string.IsNullOrEmpty(QuoteInProgress.InvoiceDescription) ? QuoteInProgress.InvoiceDescription : TotalInvoice;

                                        //Total Prepay
                                        invoiceSummary.IsPrintPrepay = invoiceSummary.IsPrintPrepay.HasValue ? invoiceSummary.IsPrintPrepay : QuoteInProgress.IsPrintPay;
                                        invoiceSummary.PrepayTotal = invoiceSummary.PrepayTotal > 0 ? invoiceSummary.PrepayTotal : QuoteInProgress.PrepayTotal;
                                        invoiceSummary.QuotePrepay = invoiceSummary.QuotePrepay > 0 ? invoiceSummary.QuotePrepay : QuoteInProgress.PrepayTotal;
                                        invoiceSummary.PrepayDescription = !string.IsNullOrEmpty(QuoteInProgress.PrepayDescription) ? QuoteInProgress.PrepayDescription : TotalPrepay;

                                        //Total Remaining Invoice
                                        invoiceSummary.IsPrintRemaingAMT = invoiceSummary.IsPrintRemaingAMT.HasValue ? invoiceSummary.IsPrintRemaingAMT : QuoteInProgress.IsPrintRemaingAMT;
                                        invoiceSummary.QuoteRemainingAmt = invoiceSummary.QuoteRemainingAmt > 0 ? invoiceSummary.QuoteRemainingAmt : QuoteInProgress.RemainingAmtTotal;
                                        invoiceSummary.RemainingAmtTotal = invoiceSummary.RemainingAmtTotal > 0 ? invoiceSummary.RemainingAmtTotal : QuoteInProgress.RemainingAmtTotal;
                                        invoiceSummary.RemainingAmtDescription = !string.IsNullOrEmpty(QuoteInProgress.RemainingAmtDescription) ? QuoteInProgress.RemainingAmtDescription : TotalRemainingInvoice;

                                        if (cqs.CQInvoiceSummaries == null || (cqs.CQInvoiceSummaries != null && cqs.CQInvoiceSummaries.Count == 0))
                                        {
                                            if (invoiceSummary.CQInvoiceSummaryID == 0)
                                            {
                                                cqs.CQInvoiceSummaries.Add(invoiceSummary);
                                            }
                                        }
                                        #endregion

                                        #region Load Company Invoice Details using Homebase ID
                                        if (FileRequest.HomebaseID.HasValue)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var CompanyList = FPKMasterService.GetListInfoByHomeBaseId((long)FileRequest.HomebaseID);

                                                if (CompanyList != null && CompanyList.EntityList != null && CompanyList.EntityList.Count() > 0)
                                                {
                                                    cqs.CharterCompanyInformation = string.IsNullOrEmpty(cqs.CharterCompanyInformation) ? CompanyList.EntityList[0].ChtQuoteCompanyNameINV : cqs.CharterCompanyInformation;
                                                    cqs.InvoiceHeader = string.IsNullOrEmpty(cqs.InvoiceHeader) ? CompanyList.EntityList[0].ChtQuoteHeaderINV : cqs.InvoiceHeader;
                                                    cqs.InvoiceFooter = string.IsNullOrEmpty(cqs.InvoiceFooter) ? CompanyList.EntityList[0].ChtQuoteFooterINV : cqs.InvoiceFooter;
                                                    cqs.IsPrintDetail = (!cqs.IsPrintDetail.HasValue) ? (CompanyList.EntityList[0].IsQuoteDetailPrint ?? false) : cqs.IsPrintDetail;
                                                    cqs.IsPrintSum = (!cqs.IsPrintSum.HasValue) ? (CompanyList.EntityList[0].IsQuotePrintSum ?? false) : cqs.IsPrintSum;
                                                    cqs.IsPrintFees = (!cqs.IsPrintFees.HasValue) ? (CompanyList.EntityList[0].isQuotePrintFees ?? false) : cqs.IsPrintFees;

                                                    cqs.InvoiceAdditionalFeeTax = (cqs.InvoiceAdditionalFeeTax > 0 ? cqs.InvoiceAdditionalFeeTax : (CompanyList.EntityList[0].CQFederalTax ?? 0));

                                                    //DAILY USAGE ADJUSTMENT:
                                                    invoiceSummary.IsPrintUsage = (!invoiceSummary.IsPrintUsage.HasValue) ? (CompanyList.EntityList[0].IsQuotePrepaidMinimUsageFeeAmt ?? false) : invoiceSummary.IsPrintUsage;
                                                    invoiceSummary.UsageAdjDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt) ? (invoiceSummary.UsageAdjDescription == CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt ? CompanyList.EntityList[0].QuoteMinimumUseFeeDepositAmt : invoiceSummary.UsageAdjDescription) : DailyUsageAdjustment;

                                                    //Additional Fees
                                                    invoiceSummary.IsPrintAdditonalFees = (!invoiceSummary.IsPrintAdditonalFees.HasValue) ? CompanyList.EntityList[0].IsQuotePrintAdditionalFee : invoiceSummary.IsPrintAdditonalFees;
                                                    invoiceSummary.DescriptionAdditionalFee = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteAdditionalFeeDefault) ? (invoiceSummary.DescriptionAdditionalFee == CompanyList.EntityList[0].QuoteAdditionalFeeDefault ? CompanyList.EntityList[0].QuoteAdditionalFeeDefault : invoiceSummary.DescriptionAdditionalFee) : AdditionalFees;

                                                    //LAnding fee                           
                                                    invoiceSummary.IsPrintLandingFees = (!invoiceSummary.IsPrintLandingFees.HasValue) ? CompanyList.EntityList[0].IsQuoteFlightLandingFeePrint : invoiceSummary.IsPrintLandingFees;
                                                    invoiceSummary.LandingFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteLandingFeeDefault) ? (invoiceSummary.LandingFeeDescription == CompanyList.EntityList[0].QuoteLandingFeeDefault ? CompanyList.EntityList[0].QuoteLandingFeeDefault : invoiceSummary.LandingFeeDescription) : LandingFees;

                                                    //Segment Fees 
                                                    invoiceSummary.IsPrintSegmentFee = (!invoiceSummary.IsPrintSegmentFee.HasValue) ? CompanyList.EntityList[0].IsQuotePrepaidSegementFee : invoiceSummary.IsPrintSegmentFee;
                                                    invoiceSummary.SegmentFeeDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSegmentFeeDeposit) ? (invoiceSummary.SegmentFeeDescription == CompanyList.EntityList[0].QuoteSegmentFeeDeposit ? CompanyList.EntityList[0].QuoteSegmentFeeDeposit : invoiceSummary.SegmentFeeDescription) : SegmentFees;
                                                    
                                                    //std crew
                                                    invoiceSummary.IsPrintStdCrew = (!invoiceSummary.IsPrintStdCrew.HasValue) ? CompanyList.EntityList[0].IsQuoteStdCrewRonPrepaid : invoiceSummary.IsPrintStdCrew;
                                                    invoiceSummary.StdCrewRONDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteStdCrewRONDeposit) ? (invoiceSummary.StdCrewRONDescription == CompanyList.EntityList[0].QuoteStdCrewRONDeposit ? CompanyList.EntityList[0].QuoteStdCrewRONDeposit : invoiceSummary.StdCrewRONDescription) : RonCrewCharge;
                                                    
                                                    //Flight Charge
                                                    invoiceSummary.IsPrintFlightChg = (!invoiceSummary.IsPrintFlightChg.HasValue) ? CompanyList.EntityList[0].IsQuoteFlightChargePrepaid : invoiceSummary.IsPrintFlightChg;
                                                    invoiceSummary.FlightChgDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].DepostFlightCharge) ? (invoiceSummary.FlightChgDescription == CompanyList.EntityList[0].DepostFlightCharge ? CompanyList.EntityList[0].DepostFlightCharge : invoiceSummary.FlightChgDescription) : FlightCharge;
                                                    
                                                    //Subtotal
                                                    invoiceSummary.IsPrintSubtotal = (!invoiceSummary.IsPrintSubtotal.HasValue) ? CompanyList.EntityList[0].IsQuoteSubtotalPrint : invoiceSummary.IsPrintSubtotal;
                                                    invoiceSummary.SubtotalDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteSubtotal) ? (invoiceSummary.SubtotalDescription == CompanyList.EntityList[0].QuoteSubtotal ? CompanyList.EntityList[0].QuoteSubtotal : invoiceSummary.SubtotalDescription) : SubTotal;
                                                    
                                                    //Discount Amount
                                                    invoiceSummary.IsPrintDiscountAmt = (!invoiceSummary.IsPrintDiscountAmt.HasValue) ? CompanyList.EntityList[0].IsQuoteDispcountAmtPrint : invoiceSummary.IsPrintDiscountAmt;
                                                    invoiceSummary.DescriptionDiscountAmt = !string.IsNullOrEmpty(CompanyList.EntityList[0].DiscountAmountDefault) ? (invoiceSummary.DescriptionDiscountAmt == CompanyList.EntityList[0].DiscountAmountDefault ? CompanyList.EntityList[0].DiscountAmountDefault : invoiceSummary.DescriptionDiscountAmt) : DiscountAmount;
                                                    
                                                    //Taxes
                                                    invoiceSummary.IsPrintTaxes = (!invoiceSummary.IsPrintTaxes.HasValue) ? CompanyList.EntityList[0].IsQuoteTaxesPrint : invoiceSummary.IsPrintTaxes;
                                                    invoiceSummary.TaxesDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteTaxesDefault) ? (invoiceSummary.TaxesDescription == CompanyList.EntityList[0].QuoteTaxesDefault ? CompanyList.EntityList[0].QuoteTaxesDefault : invoiceSummary.TaxesDescription) : Taxes;
                                                    
                                                    //Total Invoice
                                                    invoiceSummary.IsInvoiceTax = (!invoiceSummary.IsInvoiceTax.HasValue) ? CompanyList.EntityList[0].IsQuoteInvoicePrint : invoiceSummary.IsInvoiceTax;
                                                    invoiceSummary.InvoiceDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].DefaultInvoice) ? (invoiceSummary.InvoiceDescription == CompanyList.EntityList[0].DefaultInvoice ? CompanyList.EntityList[0].DefaultInvoice : invoiceSummary.InvoiceDescription) : TotalInvoice;
                                                    
                                                    //Total Prepay                            
                                                    invoiceSummary.IsPrintPrepay = (!invoiceSummary.IsPrintPrepay.HasValue) ? CompanyList.EntityList[0].IsQuotePrepayPrint : invoiceSummary.IsPrintPrepay;
                                                    invoiceSummary.PrepayDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuotePrepayDefault) ? (invoiceSummary.PrepayDescription == CompanyList.EntityList[0].QuotePrepayDefault ? CompanyList.EntityList[0].QuotePrepayDefault : invoiceSummary.PrepayDescription) : TotalPrepay;
                                                    
                                                    //Total Remaining Invoice
                                                    invoiceSummary.IsPrintRemaingAMT = (!invoiceSummary.IsPrintRemaingAMT.HasValue) ? CompanyList.EntityList[0].IsQuoteRemainingAmtPrint : invoiceSummary.IsPrintRemaingAMT;
                                                    invoiceSummary.RemainingAmtDescription = !string.IsNullOrEmpty(CompanyList.EntityList[0].QuoteRemainingAmountDefault) ? (invoiceSummary.RemainingAmtDescription == CompanyList.EntityList[0].QuoteRemainingAmountDefault ? CompanyList.EntityList[0].QuoteRemainingAmountDefault : invoiceSummary.RemainingAmtDescription) : TotalRemainingInvoice;
                                                    
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Invoice Details
                                        if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                                        {
                                            if (cqs.CQInvoiceQuoteDetails == null)
                                                cqs.CQInvoiceQuoteDetails = new List<CQInvoiceQuoteDetail>();
                                            else
                                            {
                                                foreach (CQInvoiceQuoteDetail quotedet in cqs.CQInvoiceQuoteDetails.ToList())
                                                {
                                                    if (quotedet.CQInvoiceQuoteDetailID != 0)
                                                    {
                                                        quotedet.IsDeleted = true;
                                                        quotedet.State = CQRequestEntityState.Deleted;
                                                    }
                                                    else
                                                        cqs.CQInvoiceQuoteDetails.Remove(quotedet);
                                                }
                                            }

                                            foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                CQInvoiceQuoteDetail quotedet = cqs.CQInvoiceQuoteDetails.Where(x => x.IsDeleted == true && x.CQLegID == Leg.CQLegID).FirstOrDefault();
                                                CQInvoiceQuoteDetail addInvoiceDetail = new CQInvoiceQuoteDetail();
                                                addInvoiceDetail.State = CQRequestEntityState.Added;
                                                addInvoiceDetail.IsDeleted = false;
                                                addInvoiceDetail.CQLegID = Leg.CQLegID;
                                                addInvoiceDetail.LegNum = Leg.LegNUM;
                                                addInvoiceDetail.DepartureDTTMLocal = Leg.DepartureDTTMLocal;
                                                addInvoiceDetail.ArrivalDTTMLocal = Leg.ArrivalDTTMLocal;
                                                addInvoiceDetail.Airport1 = Leg.Airport1;
                                                addInvoiceDetail.Airport = Leg.Airport;
                                                addInvoiceDetail.DAirportID = Leg.DAirportID;
                                                addInvoiceDetail.AAirportID = Leg.AAirportID;
                                                addInvoiceDetail.PassengerTotal = Leg.PassengerTotal;
                                                addInvoiceDetail.FlightHours = Leg.ElapseTM;
                                                addInvoiceDetail.Distance = Leg.Distance;
                                                addInvoiceDetail.FlightCharge = Leg.ChargeTotal;
                                                addInvoiceDetail.IsPrintable = cqs.IsPrintDetail.HasValue ? (bool)cqs.IsPrintDetail : false; // UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint != null ? (bool)UserPrincipal.Identity._fpSettings._IsQuoteDetailPrint : false;
                                                addInvoiceDetail.TaxRate = cqs.InvoiceAdditionalFeeTax.HasValue ? cqs.InvoiceAdditionalFeeTax : 0; // UserPrincipal.Identity._fpSettings._CQFederalTax != null ? UserPrincipal.Identity._fpSettings._CQFederalTax : 0;
                                                if(quotedet!=null){
                                                    addInvoiceDetail.IsPrintable = ((!quotedet.IsPrintable.HasValue) ? (cqs.IsPrintDetail.HasValue ? (bool)cqs.IsPrintDetail : false) : quotedet.IsPrintable);
                                                }

                                                cqs.CQInvoiceQuoteDetails.Add(addInvoiceDetail);
                                            }
                                        }
                                        #endregion

                                        #region AdditionalFees

                                        if (cqs.CQInvoiceAdditionalFees != null)
                                        {
                                            foreach (CQInvoiceAdditionalFee invoicefee in cqs.CQInvoiceAdditionalFees.ToList())
                                            {
                                                if (invoicefee.CQInvoiceAdditionalFeesID != 0)
                                                {
                                                    invoicefee.IsDeleted = true;
                                                    invoicefee.State = CQRequestEntityState.Deleted;
                                                }
                                                else
                                                    cqs.CQInvoiceAdditionalFees.Remove(invoicefee);
                                            }
                                        }

                                        if (QuoteInProgress.CQFleetChargeDetails != null)
                                        {
                                            if (cqs.CQInvoiceAdditionalFees == null)
                                                cqs.CQInvoiceAdditionalFees = new List<CQInvoiceAdditionalFee>();

                                            foreach (CQFleetChargeDetail fltCharge in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                CQInvoiceAdditionalFee invoicefee = cqs.CQInvoiceAdditionalFees.Where(x => x.IsDeleted == true && x.OrderNUM == fltCharge.OrderNUM).FirstOrDefault();
                                                CQInvoiceAdditionalFee addfee = new CQInvoiceAdditionalFee();
                                                addfee.State = CQRequestEntityState.Added;
                                                addfee.IsDeleted = false;
                                                addfee.OrderNUM = fltCharge.OrderNUM;
                                                addfee.CQInvoiceFeeDescription = fltCharge.CQFlightChargeDescription;
                                                addfee.QuoteAmount = fltCharge.FeeAMT;
                                                addfee.InvoiceAmt = fltCharge.FeeAMT;
                                                addfee.Quantity = fltCharge.Quantity;
                                                addfee.ChargeUnit = fltCharge.ChargeUnit;
                                                addfee.Rate = ((!fltCharge.FeeAMT.HasValue) || ((!fltCharge.Quantity.HasValue) || fltCharge.Quantity == 0)) ? 0 : fltCharge.FeeAMT / fltCharge.Quantity;
                                                addfee.IsTaxable = fltCharge.IsTaxDOM;
                                                addfee.IsDiscount = fltCharge.IsDiscountDOM;
                                                addfee.IsPrintable = cqs.IsPrintFees.HasValue ? (bool)cqs.IsPrintFees : false; // UserPrincipal.Identity._fpSettings._isQuotePrintFees != null ? (bool)UserPrincipal.Identity._fpSettings._isQuotePrintFees : false;

                                                if (invoicefee != null)
                                                {
                                                    addfee.IsPrintable = (!invoicefee.IsPrintable.HasValue) ? (cqs.IsPrintFees.HasValue ? (bool)cqs.IsPrintFees : false) : invoicefee.IsPrintable;
                                                    addfee.CQInvoiceFeeDescription = addfee.CQInvoiceFeeDescription;
                                                    addfee.InvoiceAmt = (!invoicefee.InvoiceAmt.HasValue) ? fltCharge.FeeAMT : invoicefee.InvoiceAmt;
                                                    addfee.IsTaxable = (!invoicefee.IsTaxable.HasValue) ? fltCharge.IsTaxDOM : invoicefee.IsTaxable;
                                                    addfee.IsDiscount = (!invoicefee.IsDiscount.HasValue) ? fltCharge.IsDiscountDOM : invoicefee.IsDiscount;
                                                }

                                                cqs.CQInvoiceAdditionalFees.Add(addfee);
                                            }
                                        }
                                        #endregion


                                    }
                                }
                                Session["InvoiceReportMain"] = cqs;
                                LoadInvoiceData(cqs);
                            }
                            else
                                LoadInvoiceCompanyDetail(ref cqs);
                        }
                        else
                            LoadInvoiceCompanyDetail(ref cqs);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void InvoiceRefresh_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindowManager1.RadConfirm("Refresh will restore invoice to original Quote values. Continue with the Refresh Process?", "confirmCallBackInvoiceFn", 500, 30, null, "Confirmation");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnInvoiceCustomFooter_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null && FileRequest.HomebaseID.HasValue)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetCQMessage("I", (long)FileRequest.HomebaseID).EntityList;
                                if (RetValue != null && RetValue.Count > 0)
                                {
                                    ddlInvoiceMyCustomerFooter.Items.Clear();
                                    int i = 0;
                                    foreach (FlightPakMasterService.CQMessage fwh in RetValue)
                                    {
                                        string encodedDescription = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.MessageDescription);
                                        string encodedID = Microsoft.Security.Application.Encoder.HtmlEncode(fwh.CQMessageID.ToString());

                                        ddlInvoiceMyCustomerFooter.Items.Insert(i, new ListItem((encodedDescription), encodedID));
                                        i = i + 1;
                                    }
                                }
                            }
                        }
                        if (FileRequest != null && FileRequest.Company != null && FileRequest.Company.ChtQuoteFooterINV != null)
                        {
                            string Item = FileRequest.Company.ChtQuoteFooterINV;
                            if (ddlInvoiceMyCustomerFooter.Items.FindByText(Item) != null)
                            {
                                ddlInvoiceMyCustomerFooter.Items.FindByText(Item).Selected = true;
                                tbInvoiceFooterInfo.Text = string.IsNullOrEmpty(tbInvoiceFooterInfo.Text) ? ddlInvoiceMyCustomerFooter.SelectedItem.ToString() : tbInvoiceFooterInfo.Text;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void btnInvoiceSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            SaveInvoiceReport();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        public void SaveInvoiceReport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                CharterQuoteService.CQInvoiceMain CQInvoiceMain = null;
                if (Session["InvoiceReportMain"] != null)
                {
                    CQInvoiceMain = (CQInvoiceMain)Session["InvoiceReportMain"];

                    CQInvoiceMain.State = CQInvoiceMain.CQInvoiceMainID != 0 ? CQRequestEntityState.Modified : CQRequestEntityState.Added;

                    CQInvoiceMain.Mode = CQRequestActionMode.Edit;

                    CQInvoiceMain.CharterCompanyInformation = !string.IsNullOrEmpty(tbInvoiceCompanyInfo.Text) ? tbInvoiceCompanyInfo.Text : string.Empty;

                    CQInvoiceMain.CustomerAddressInformation = !string.IsNullOrEmpty(tbMyCharterCustomer.Text) ? tbMyCharterCustomer.Text : string.Empty;

                    CQInvoiceMain.InvoiceHeader = !string.IsNullOrEmpty(tbInvoiceHeaderInfo.Text) ? tbInvoiceHeaderInfo.Text : string.Empty;

                    CQInvoiceMain.InvoiceFooter = !string.IsNullOrEmpty(tbInvoiceFooterInfo.Text) ? tbInvoiceFooterInfo.Text : string.Empty;
                    
                    //Legs
                    if (CQInvoiceMain.CQInvoiceQuoteDetails != null)
                    {
                        foreach (GridDataItem item in dgInvoiceLeg.Items)
                        {
                            Int64 LegNum = (long)item.GetDataKeyValue("LegNum");
                            CQInvoiceQuoteDetail invoicedet = new CQInvoiceQuoteDetail();
                            invoicedet = CQInvoiceMain.CQInvoiceQuoteDetails.Where(x => x.LegNum == LegNum && x.IsDeleted == false).SingleOrDefault();
                            if (invoicedet != null)
                            {
                                if (invoicedet.CQInvoiceQuoteDetailID != 0)
                                    invoicedet.State = CQRequestEntityState.Modified;
                                else
                                    invoicedet.State = CQRequestEntityState.Added;
                                CheckBox chkAddFeePrint = (CheckBox)item.FindControl("chkPrint");
                                invoicedet.IsPrintable = chkAddFeePrint.Checked;
                                if (!string.IsNullOrEmpty(tbInvoiceTaxRate.Text))
                                {
                                    invoicedet.TaxRate = Convert.ToDecimal(tbInvoiceTaxRate.Text);
                                }
                            }
                        }
                    }

                    CQInvoiceMain.InvoiceAdditionalFeeTax = !string.IsNullOrEmpty(tbInvocieAdditionalFeeTaxRate.Text) ? Convert.ToDecimal(tbInvocieAdditionalFeeTaxRate.Text) : 0;

                    //Additional fees
                    if (CQInvoiceMain.CQInvoiceAdditionalFees != null)
                    {
                        foreach (GridDataItem item in dgInvoiceAddFee.Items)
                        {
                            int OrderNum = (int)item.GetDataKeyValue("OrderNUM");
                            CQInvoiceAdditionalFee invoiceAddfee = new CQInvoiceAdditionalFee();
                            invoiceAddfee = CQInvoiceMain.CQInvoiceAdditionalFees.Where(x => x.OrderNUM == OrderNum && x.IsDeleted == false).SingleOrDefault();
                            if (invoiceAddfee != null)
                            {
                                invoiceAddfee.State = invoiceAddfee.CQInvoiceAdditionalFeesID != 0 ? CQRequestEntityState.Modified : CQRequestEntityState.Added;

                                CheckBox chkAddFeePrint = (CheckBox)item.FindControl("chkInvoiceAddfeePrint");
                                invoiceAddfee.IsPrintable = chkAddFeePrint != null && chkAddFeePrint.Checked;

                                TextBox tbInvoiceAdditioanalDescription = (TextBox)item.FindControl("tbInvoiceAdditioanalDescription");
                                if (tbInvoiceAdditioanalDescription != null && !string.IsNullOrEmpty(tbInvoiceAdditioanalDescription.Text))
                                    invoiceAddfee.CQInvoiceFeeDescription = tbInvoiceAdditioanalDescription.Text;
                                else
                                    invoiceAddfee.CQInvoiceFeeDescription = string.Empty;

                                RadNumericTextBox tbInvoiceAmt = (RadNumericTextBox)item.FindControl("tbInvoiceAdditionalFeeInvocieAmt");
                                invoiceAddfee.InvoiceAmt = tbInvoiceAmt != null ? Convert.ToDecimal(tbInvoiceAmt.Text) : 0;

                                CheckBox chkInvoiceTaxable = (CheckBox)item.FindControl("chkInvoiceTaxable");
                                invoiceAddfee.IsTaxable = chkInvoiceTaxable != null && chkInvoiceTaxable.Checked;
                                
                                CheckBox chkInvoiceDiscount = (CheckBox)item.FindControl("chkInvoiceDiscount");
                                invoiceAddfee.IsDiscount = chkInvoiceDiscount != null && chkInvoiceDiscount.Checked;

                            }
                        }
                    }
                    ///Invoice Summary
                    CQInvoiceMain.IsManualDiscount = chkManualDiscountEntry.Checked;
                    CQInvoiceMain.IsManualTax = chkInvoiceManualTaxEntry.Checked;
                    CQInvoiceMain.IsSumTax = chkInvoiceSummaryFeesTaxable.Checked;
                    CQInvoiceMain.IsPrintSum = chkÍnvoicePrintSummary.Checked;
                    CQInvoiceMain.IsPrintDetail = chkInvoicePrintDetail.Checked;
                    CQInvoiceMain.IsPrintQuoteNUM = chkInvoicePrintQuoteNo.Checked;
                    CQInvoiceMain.IsPrintFees = chkInvoicePrintAdditionalFees.Checked;
                    CQInvoiceMain.IsInvoiceAdditionalFeeTax = chkInvoiceAdditionalFeesTaxable.Checked;

                    if (!string.IsNullOrEmpty(tbInvocieAdditionalFeeTaxRate.Text))
                        CQInvoiceMain.InvoiceAdditionalFeeTax = Convert.ToDecimal(tbInvocieAdditionalFeeTaxRate.Text);

                    CQInvoiceSummary invoiceQuoteSummary = new CQInvoiceSummary();

                    if (CQInvoiceMain.CQInvoiceSummaries != null)
                        invoiceQuoteSummary = CQInvoiceMain.CQInvoiceSummaries[0];
                    else
                        CQInvoiceMain.CQInvoiceSummaries.Add(invoiceQuoteSummary);

                    invoiceQuoteSummary.State = invoiceQuoteSummary.CQInvoiceSummaryID != 0 ? CQRequestEntityState.Modified : CQRequestEntityState.Added;
                    invoiceQuoteSummary.IsPrintUsage = chkInvoiceDailyUsageAdjustmentPrint.Checked;
                    invoiceQuoteSummary.UsageAdjDescription = !string.IsNullOrEmpty(tbInvoiceDailyUsageAdjustment.Text) ? tbInvoiceDailyUsageAdjustment.Text : string.Empty;
                    invoiceQuoteSummary.UsageAdjTotal = !string.IsNullOrEmpty(tbInvoicedailyusageInvoiceAmt.Text) ? Convert.ToDecimal(tbInvoicedailyusageInvoiceAmt.Text) : 0;
                    invoiceQuoteSummary.QuoteUsageAdj = !string.IsNullOrEmpty(tbInvoicedailyusageQuoteAmt.Text) ? Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoicedailyusageQuoteAmt.Text)) : 0;
                    invoiceQuoteSummary.IsUsageAdjTax = chkDailyusageTaxable.Checked;

                    //Additional Fees
                    invoiceQuoteSummary.IsPrintAdditonalFees = chkInvoiceAdditionalfeePrint.Checked;
                    invoiceQuoteSummary.DescriptionAdditionalFee = !string.IsNullOrEmpty(tbInvoiceAdditionalFeeDescription.Text) ? tbInvoiceAdditionalFeeDescription.Text : string.Empty;
                    invoiceQuoteSummary.AdditionalFees = !string.IsNullOrEmpty(tbInvoiceAdditionalfeeQuoteAmt.Text) ? Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceAdditionalfeeQuoteAmt.Text)) : 0;
                    invoiceQuoteSummary.AdditionalFeeTotal = !string.IsNullOrEmpty(tbInvoiceAdditionalfeeInvoiceAmt.Text) ? Convert.ToDecimal(tbInvoiceAdditionalfeeInvoiceAmt.Text) : 0;
                    invoiceQuoteSummary.IsAdditionalFeesTax = chkInvoiceAdditionalfeeInvoiceTaxable.Checked;

                    //LAnding fee
                    invoiceQuoteSummary.IsPrintLandingFees = chkInvoiceLandingfeePrint.Checked;
                    invoiceQuoteSummary.LandingFeeDescription = !string.IsNullOrEmpty(tbInvoiceLandingFeeDescription.Text) ? tbInvoiceLandingFeeDescription.Text : string.Empty;
                    invoiceQuoteSummary.QuoteLandingFee = !string.IsNullOrEmpty(lbInvoiceLandingfeeQuoteAmt.Text) ? Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(lbInvoiceLandingfeeQuoteAmt.Text)) : 0;
                    invoiceQuoteSummary.LandingFeeTotal = !string.IsNullOrEmpty(tbInvoiceLandingfeeInvoiceAmt.Text) ? Convert.ToDecimal(tbInvoiceLandingfeeInvoiceAmt.Text) : 0;
                    invoiceQuoteSummary.IsLandingFeeTax = chkInvoiceLandingfeeInvoiceTaxable.Checked;
                    invoiceQuoteSummary.IsLandingFeeDiscount = chkInvoiceLandingfeeInvoiceDiscount.Checked;

                    //Segment Fees                      
                    invoiceQuoteSummary.IsPrintSegmentFee = chkInvoiceSegmentFeePrint.Checked;
                    invoiceQuoteSummary.SegmentFeeDescription = !string.IsNullOrEmpty(tbInvoiceSegmentFeeDescription.Text) ? tbInvoiceSegmentFeeDescription.Text : string.Empty;
                    invoiceQuoteSummary.QuoteSegementFee = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceSegmentFeeQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteSegementFee = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceSegmentFeeQuoteAmt.Text));
                    invoiceQuoteSummary.SegmentFeeTotal = 0;

                    if (!string.IsNullOrEmpty(tbInvoiceSegmentFeeInvoiceAmt.Text))
                        invoiceQuoteSummary.SegmentFeeTotal = Convert.ToDecimal(tbInvoiceSegmentFeeInvoiceAmt.Text);
                    invoiceQuoteSummary.IsSegmentFeeTax = chkInvoiceSegmentFeetaxable.Checked;

                    //Ron Crew charge                       
                    invoiceQuoteSummary.IsPrintStdCrew = chkInvoiceRonCrewChargePrint.Checked;
                    invoiceQuoteSummary.StdCrewRONDescription = tbInvoiceRonCrewChargeDescription.Text;
                    invoiceQuoteSummary.QuoteStdCrewRON = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceRonCrewChargeQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteStdCrewRON = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceRonCrewChargeQuoteAmt.Text));
                    invoiceQuoteSummary.StdCrewRONTotal = 0;

                    if (!string.IsNullOrEmpty(tbInvocieRonCrewChargeInvoiceAmt.Text))
                        invoiceQuoteSummary.StdCrewRONTotal = Convert.ToDecimal(tbInvocieRonCrewChargeInvoiceAmt.Text);
                    invoiceQuoteSummary.IsStdCrewRONTax = chkInvoiceRonCrewChargeTaxable.Checked;

                    invoiceQuoteSummary.IsStdCrewRONDiscount = chkInvoiceRonCrewChargeDiscount.Checked;
                    //Additional Crew  Charge
                    invoiceQuoteSummary.IsPrintAdditionalCrew = chkInvoiceAddCrewChargePrint.Checked;
                    invoiceQuoteSummary.AdditionalCrewDescription = !string.IsNullOrEmpty(tbInvoiceAddrewChargeDescription.Text) ? tbInvoiceAddrewChargeDescription.Text : string.Empty;
                    invoiceQuoteSummary.CQAdditionalCrew = !string.IsNullOrEmpty(lbInvoiceAddCrewChargeQuoteAmt.Text) ? Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(lbInvoiceAddCrewChargeQuoteAmt.Text)) : 0;
                    invoiceQuoteSummary.AdditionalCrewTotal = !string.IsNullOrEmpty(tbInvocieAddCrewChargeInvoiceAmt.Text) ? Convert.ToDecimal(tbInvocieAddCrewChargeInvoiceAmt.Text) : 0;
                    invoiceQuoteSummary.IsAdditionalCrewTax = chkInvoiceAddCrewChargeTaxable.Checked;
                    invoiceQuoteSummary.AdditionalCrewDiscount = chkInvoiceAddCrewChargeDiscount.Checked;

                    //Additional Crew Ron Charge
                    invoiceQuoteSummary.IsAdditionalCrewRON = chkInvoiceAdditionalRonCrewChargePrint.Checked;
                    invoiceQuoteSummary.AdditionalCrewRON = !string.IsNullOrEmpty(tbInvoiceAdditionalRonCrewChargeDescription.Text) ? tbInvoiceAdditionalRonCrewChargeDescription.Text : string.Empty;
                    invoiceQuoteSummary.AdditionalRONCrew = 0;
                    if (!string.IsNullOrEmpty(lbInvoiceAdditionalRonCrewChargeQuoteAmt.Text))
                        invoiceQuoteSummary.AdditionalRONCrew = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(lbInvoiceAdditionalRonCrewChargeQuoteAmt.Text));
                    invoiceQuoteSummary.AdditionalCrewRONTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text))
                        invoiceQuoteSummary.AdditionalCrewRONTotal = Convert.ToDecimal(tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text);
                    invoiceQuoteSummary.IsAdditionalCrewRONTax = chkInvoiceAdditionalRonCrewChargeTaxable.Checked;
                    invoiceQuoteSummary.IsAdditionalCrewRONDiscount = chkInvoiceAdditionalRonCrewChargeDiscount.Checked;

                    //Wait Charge
                    invoiceQuoteSummary.IsWaitingChg = chkInvoiceWaitCharge.Checked;
                    invoiceQuoteSummary.WaitChgdescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceWaitChargeDescription.Text))
                        invoiceQuoteSummary.WaitChgdescription = tbInvoiceWaitChargeDescription.Text;
                    if (!string.IsNullOrEmpty(lbInvoiceWaitChargeQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteWaitingChg = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(lbInvoiceWaitChargeQuoteAmt.Text));
                    if (!string.IsNullOrEmpty(tbInvoiceWaitChargeInvocieAmt.Text))
                        invoiceQuoteSummary.WaitChgTotal = Convert.ToDecimal(tbInvoiceWaitChargeInvocieAmt.Text);
                    invoiceQuoteSummary.IsWaitingChgTax = chkInvoiceWaitChargeTaxable.Checked;
                    invoiceQuoteSummary.IsWaitingChgDiscount = chkInvoiceWaitChargeDiscount.Checked;
                    //Flight Charge
                    invoiceQuoteSummary.IsPrintFlightChg = chkInvoiceFlightCharge.Checked;
                    invoiceQuoteSummary.FlightChgDescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceFlightChargeDescription.Text))
                        invoiceQuoteSummary.FlightChgDescription = tbInvoiceFlightChargeDescription.Text;
                    invoiceQuoteSummary.FlightChargeTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceFlightChargeInvocieAmt.Text))
                        invoiceQuoteSummary.FlightChargeTotal = Convert.ToDecimal(tbInvoiceFlightChargeInvocieAmt.Text);
                    invoiceQuoteSummary.FlightCharge = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceFlightChargeQuoteAmt.Text))
                        invoiceQuoteSummary.FlightCharge = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceFlightChargeQuoteAmt.Text));
                    invoiceQuoteSummary.IsFlightChgDiscount = chkInvoiceFlightChargeDiscount.Checked;
                    //Subtotal
                    invoiceQuoteSummary.IsPrintSubtotal = chkInvoiceSubtotalPrint.Checked;
                    invoiceQuoteSummary.SubtotalDescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceSubtotalDecription.Text))
                        invoiceQuoteSummary.SubtotalDescription = tbInvoiceSubtotalDecription.Text;
                    invoiceQuoteSummary.QuoteSubtotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceSubtotalQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteSubtotal = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceSubtotalQuoteAmt.Text));
                    invoiceQuoteSummary.SubtotalTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceSubtotalInvoiceAmt.Text))
                        invoiceQuoteSummary.SubtotalTotal = Convert.ToDecimal(tbInvoiceSubtotalInvoiceAmt.Text);

                    //Discount Amount
                    invoiceQuoteSummary.IsPrintDiscountAmt = chkInvoiceDiscountPrint.Checked;
                    invoiceQuoteSummary.DescriptionDiscountAmt = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceDiscountdescription.Text))
                        invoiceQuoteSummary.DescriptionDiscountAmt = tbInvoiceDiscountdescription.Text;
                    invoiceQuoteSummary.QuoteDiscountAmount = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceDiscountQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteDiscountAmount = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceDiscountQuoteAmt.Text)); // QuoteInProgress.CustomDiscountAmt;
                    invoiceQuoteSummary.DiscountAmtTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceDiscountInvoiceAmt.Text))
                        invoiceQuoteSummary.DiscountAmtTotal = Convert.ToDecimal(tbInvoiceDiscountInvoiceAmt.Text);
                    //Taxes
                    invoiceQuoteSummary.IsPrintTaxes = chkInvoiceTaxesPrint.Checked;
                    invoiceQuoteSummary.TaxesDescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceTaxesDescription.Text))
                        invoiceQuoteSummary.TaxesDescription = tbInvoiceTaxesDescription.Text;
                    invoiceQuoteSummary.QuoteTaxes = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTaxesQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteTaxes = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceTaxesQuoteAmt.Text));
                    invoiceQuoteSummary.TaxesTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTaxesInvoiceAmt.Text))
                        invoiceQuoteSummary.TaxesTotal = Convert.ToDecimal(tbInvoiceTaxesInvoiceAmt.Text);
                    //Total Invoice
                    invoiceQuoteSummary.IsInvoiceTax = chkInvoiceTotalInvoicePrint.Checked;
                    invoiceQuoteSummary.InvoiceDescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalInvoiceDescription.Text))
                        invoiceQuoteSummary.InvoiceDescription = tbInvoiceTotalInvoiceDescription.Text;
                    invoiceQuoteSummary.QuoteInvoice = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTaxesQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteInvoice = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceTaxesQuoteAmt.Text));
                    invoiceQuoteSummary.InvoiceTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalInvoiceInvoiceAmt.Text)) // tbInvoiceTaxesInvoiceAmt.Text))
                        invoiceQuoteSummary.InvoiceTotal = Convert.ToDecimal(tbInvoiceTotalInvoiceInvoiceAmt.Text);
                    //Total Prepay
                    invoiceQuoteSummary.IsPrintPrepay = chkInvoiceTotalPrepayPrint.Checked;
                    invoiceQuoteSummary.PrepayDescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalPrepayDescription.Text))
                        invoiceQuoteSummary.PrepayDescription = tbInvoiceTotalPrepayDescription.Text;
                    invoiceQuoteSummary.QuotePrepay = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTaxesQuoteAmt.Text))
                        invoiceQuoteSummary.QuotePrepay = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceTaxesQuoteAmt.Text));
                    invoiceQuoteSummary.PrepayTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalPrepayInvoiceAmt.Text))
                        invoiceQuoteSummary.PrepayTotal = Convert.ToDecimal(tbInvoiceTotalPrepayInvoiceAmt.Text);
                    //Total Remaining Invoice
                    invoiceQuoteSummary.IsPrintRemaingAMT = chkInvoiceTotalRemainingPrint.Checked;
                    invoiceQuoteSummary.RemainingAmtDescription = string.Empty;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalRemainingDescription.Text))
                        invoiceQuoteSummary.RemainingAmtDescription = tbInvoiceTotalRemainingDescription.Text;
                    invoiceQuoteSummary.QuoteRemainingAmt = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalRemainingQuoteAmt.Text))
                        invoiceQuoteSummary.QuoteRemainingAmt = Convert.ToDecimal(System.Web.HttpUtility.HtmlDecode(tbInvoiceTotalRemainingQuoteAmt.Text));
                    invoiceQuoteSummary.RemainingAmtTotal = 0;
                    if (!string.IsNullOrEmpty(tbInvoiceTotalRemainingInvoiceAmt.Text))
                        invoiceQuoteSummary.RemainingAmtTotal = Convert.ToDecimal(tbInvoiceTotalRemainingInvoiceAmt.Text);

                    using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                    {
                        ClearInvoiceChildProperties(ref CQInvoiceMain);
                        Service.SaveInvoiceReport(CQInvoiceMain);
                        var objRetVal = Service.GetCQInvoiceMainByCQMainID((long)CQInvoiceMain.CQMainID);
                        if (objRetVal != null && objRetVal.EntityList.Count() > 0)
                        {
                            CharterQuoteService.CQInvoiceMain cqs = new CharterQuoteService.CQInvoiceMain();
                            cqs = (CQInvoiceMain)objRetVal.EntityList[0];
                            Session["InvoiceReportMain"] = cqs;
                            LoadInvoiceData(cqs);
                            BindInvoiceAddFee(cqs, true);
                        }
                    }
                }
            }
        }

        #endregion

        #region Permissions
        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }
        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            txt.CssClass = cssClass;
                        break;
                    default: break;
                }
            }
        }
        #endregion

        public string ProcessMyDataItem(object myValue)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(myValue))
            {
                return myValue == null ? "0 value" : myValue.ToString();
            }
        }

        /// <summary>
        /// Method to Clear Child Properties
        /// </summary>
        /// <param name="cqQuoteMain"></param>
        private void ClearInvoiceChildProperties(ref CQInvoiceMain cqInvoiceMain)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqInvoiceMain))
            {
                if (cqInvoiceMain.CQMessageID.HasValue) cqInvoiceMain.CQMessage = null;
                if (cqInvoiceMain.CQMainID.HasValue) cqInvoiceMain.CQMain = null;
                if (cqInvoiceMain.CustomerID.HasValue) cqInvoiceMain.Customer = null;
                if (cqInvoiceMain.LastUpdUID != null) cqInvoiceMain.UserMaster = null;

                #region CQQuoteDetails Properties
                if (cqInvoiceMain.CQInvoiceQuoteDetails != null)
                {
                    foreach (CQInvoiceQuoteDetail QuoteDet in cqInvoiceMain.CQInvoiceQuoteDetails)
                    {
                        if (QuoteDet.DAirportID.HasValue) QuoteDet.Airport1 = null;
                        if (QuoteDet.AAirportID.HasValue) QuoteDet.Airport = null;
                    }
                }
                #endregion
            }
        }

        protected void chkInvoiceRonCrewChargeDiscount_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkDailyusageDiscount.Checked = chkInvoiceRonCrewChargeDiscount.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkInvoiceFlightChargeDiscount_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkDailyusageDiscount.Checked = chkInvoiceFlightChargeDiscount.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkInvoiceSegmentFeetaxable_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkDailyusageTaxable.Checked = chkInvoiceSegmentFeetaxable.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void chkInvoiceRonCrewChargeTaxable_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkDailyusageTaxable.Checked = chkInvoiceRonCrewChargeTaxable.Checked;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceAdditionalFeeInvocieAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Int32 _orderNum = 0;
                        GridEditableItem item = (sender as RadNumericTextBox).NamingContainer as GridEditableItem;
                        if (item != null)
                        {
                            if (item.GetDataKeyValue("OrderNUM") != null)
                                _orderNum = Convert.ToInt32(item.GetDataKeyValue("OrderNUM").ToString());
                            RadNumericTextBox tbInvoiceAmt = new RadNumericTextBox();
                            tbInvoiceAmt.Text = ((RadNumericTextBox)item.FindControl("tbInvoiceAdditionalFeeInvocieAmt")).Text;
                            if (!string.IsNullOrEmpty(tbInvoiceAmt.Text))
                            {
                                bool IsValid = true;
                                IsValid = Master.IsValidNumericFormat(tbInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                                if (!IsValid)
                                {
                                    tbInvoiceAmt.Text = string.Empty;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceAmt);
                                }
                                else
                                    CalculateQuoteTotal(QuoteInProgress);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvocieAdditionalFeeTaxRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbInvocieAdditionalFeeTaxRate.Text))
                        {
                            bool IsValid = true;
                            IsValid = Master.IsValidNumericFormat(tbInvocieAdditionalFeeTaxRate.Text, 7, 4, 2, "Invoice Report - Tax Rate");
                            if (!IsValid)
                            {
                                tbInvocieAdditionalFeeTaxRate.Text = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbInvocieAdditionalFeeTaxRate);
                            }
                        }
                        if (!string.IsNullOrEmpty(tbInvoiceTaxRate.Text))
                        {
                            bool IsValid = true;
                            IsValid = Master.IsValidNumericFormat(tbInvoiceTaxRate.Text, 7, 4, 2, "Invoice Report - Tax Rate");
                            if (!IsValid)
                            {
                                tbInvoiceTaxRate.Text = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceTaxRate);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        #region Invoice Summary CheckFormat
        protected void tbInvoicedailyusageInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoicedailyusageInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoicedailyusageInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoicedailyusageInvoiceAmt);
                        }
                        else
                            CalculateQuoteTotal(QuoteInProgress);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceAdditionalfeeInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceAdditionalfeeInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceAdditionalfeeInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceAdditionalfeeInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceLandingfeeInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceLandingfeeInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceLandingfeeInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceLandingfeeInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceSegmentFeeInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceSegmentFeeInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceSegmentFeeInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceSegmentFeeInvoiceAmt);
                        }
                        else
                            CalculateQuoteTotal(QuoteInProgress);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvocieRonCrewChargeInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvocieRonCrewChargeInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvocieRonCrewChargeInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvocieRonCrewChargeInvoiceAmt);
                        }
                        else
                            CalculateQuoteTotal(QuoteInProgress);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvocieAddCrewChargeInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvocieAddCrewChargeInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvocieAddCrewChargeInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvocieAddCrewChargeInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceAdditionalRonCrewChargeInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceAdditionalRonCrewChargeInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceAdditionalRonCrewChargeInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceWaitChargeInvocieAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceWaitChargeInvocieAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceWaitChargeInvocieAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceWaitChargeInvocieAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceFlightChargeInvocieAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceFlightChargeInvocieAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceFlightChargeInvocieAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceFlightChargeInvocieAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceSubtotalInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceSubtotalInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceSubtotalInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceSubtotalInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceDiscountInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceDiscountInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceDiscountInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceDiscountInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceTaxesInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceTaxesInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceTaxesInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceTaxesInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceTotalInvoiceInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceTotalInvoiceInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceTotalInvoiceInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceTotalInvoiceInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceTotalPrepayInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceTotalPrepayInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceTotalPrepayInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceTotalPrepayInvoiceAmt);
                        }
                        else
                            CalculateQuoteTotal(QuoteInProgress);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void tbInvoiceTotalRemainingInvoiceAmt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsValid = true;
                        IsValid = Master.IsValidNumericFormat(tbInvoiceTotalRemainingInvoiceAmt.Text, 16, 13, 2, InvoiceReportInvoicedAmount);
                        if (!IsValid)
                        {
                            tbInvoiceTotalRemainingInvoiceAmt.Text = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbInvoiceTotalRemainingInvoiceAmt);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }
        #endregion

        #region Quote Calculation Methods
        public void CalculateQuoteTotal(CQMain QuoteInProgress)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(QuoteInProgress))
            {
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnTotFltHrs = 0;
                decimal lnTotDays = 0;
                decimal lnTotalDays = 0;
                decimal lnMinDays = 0;
                decimal lnMinDailyUsage = 0;
                decimal lnTotLndDsc = 0;
                decimal lnDiscRate = 0;
                decimal lnTotCqDisc = 0;
                decimal lnTotBuyAmt = 0;
                decimal lnTotAdjAmt = 0;
                decimal lnTotTaxAmt = 0;
                decimal lnTotCrwRon = 0;
                decimal lnTotUseAdj = 0;
                decimal lnTotlndfee = 0;
                decimal totchrg = 0;
                decimal lnTotDiscAmt = 0;
                decimal totsub = 0;
                decimal lnTotCost = 0;
                decimal lnTotMargin = 0;
                decimal lnTotMarPct = 0;
                decimal lnTotSegFee = 0;
                string lcFleetType = string.Empty;
                DateTime locarrdt, locdepdt;
                if (QuoteInProgress != null)
                {
                    lnTaxRate = QuoteInProgress.FederalTax.HasValue ? (decimal)QuoteInProgress.FederalTax : 0;
                    lnExRate = (FileRequest.ExchangeRate.HasValue && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;
                    if (!QuoteInProgress.SubtotalTotal.HasValue)
                        QuoteInProgress.SubtotalTotal = 0.0M;
                    if (!QuoteInProgress.QuoteTotal.HasValue)
                        QuoteInProgress.QuoteTotal = 0.0M;
                    if (!QuoteInProgress.CostTotal.HasValue)
                        QuoteInProgress.CostTotal = 0.0M;
                    if (!QuoteInProgress.UsageAdjTotal.HasValue)
                        QuoteInProgress.UsageAdjTotal = 0.0M;
                    if (!QuoteInProgress.DiscountAmtTotal.HasValue)
                        QuoteInProgress.DiscountAmtTotal = 0.0M;
                    if (!QuoteInProgress.IsOverrideCost.HasValue) QuoteInProgress.IsOverrideCost = false;
                    if (!QuoteInProgress.IsOverrideDiscountAMT.HasValue) QuoteInProgress.IsOverrideDiscountAMT = false;
                    if (!QuoteInProgress.IsOverrideDiscountPercentage.HasValue) QuoteInProgress.IsOverrideDiscountPercentage = false;
                    if (!QuoteInProgress.IsOverrideLandingFee.HasValue) QuoteInProgress.IsOverrideLandingFee = false;
                    if (!QuoteInProgress.IsOverrideSegmentFee.HasValue) QuoteInProgress.IsOverrideSegmentFee = false;
                    if (!QuoteInProgress.IsOverrideTaxes.HasValue) QuoteInProgress.IsOverrideTaxes = false;
                    if (!QuoteInProgress.IsOverrideTotalQuote.HasValue) QuoteInProgress.IsOverrideTotalQuote = false;
                    if (!QuoteInProgress.IsOverrideUsageAdj.HasValue) QuoteInProgress.IsOverrideUsageAdj = false;
                    if (!QuoteInProgress.IsTaxable.HasValue) QuoteInProgress.IsTaxable = false;
                    if (!QuoteInProgress.IsDailyTaxAdj.HasValue) QuoteInProgress.IsDailyTaxAdj = false;
                    if (!QuoteInProgress.IsLandingFeeTax.HasValue) QuoteInProgress.IsLandingFeeTax = false;
                    if (QuoteInProgress.FleetID.HasValue)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                            var objRetVal = objDstsvc.GetFleetProfileList();
                            if (objRetVal.ReturnFlag)
                            {
                                Fleetlist = objRetVal.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID && x.IsDeleted == false).ToList();
                                if (Fleetlist != null && Fleetlist.Count > 0)
                                {
                                    lcFleetType = Fleetlist[0].FleetSize;
                                    if (Fleetlist[0].MinimumDay != null)
                                        lnMinDays = (decimal)Fleetlist[0].MinimumDay;
                                }
                            }
                        }
                        lnTotFltHrs = CalculateSumOfETE(4); // any no greater than three to get all values of ete.
                        locdepdt = CalculateMinDeparttDate();
                        locarrdt = CalculateMaxArrivalDate();
                        totchrg = CalculateSumOfTotalCharge();
                        if (locarrdt != DateTime.MinValue && locdepdt != DateTime.MinValue)
                        {
                            TimeSpan daydiff = locarrdt.Subtract(locdepdt);
                            lnTotalDays = daydiff.Days + 1;
                        }
                        lnTotDays = lnTotalDays;
                        if (lnTotalDays < 1)
                            lnTotalDays = 1;
                        lnMinDailyUsage = lnTotalDays * lnMinDays;
                        lnTotAdjAmt = Math.Round(lnMinDailyUsage - lnTotFltHrs, 2);
                        if (lnTotAdjAmt < 0)
                            lnTotAdjAmt = 0;
                        lnTotLndDsc = 0;
                        lnDiscRate = 0;
                        lnTotCqDisc = 0;
                        lnTotBuyAmt = 0;
                        lnTotSegFee = CalculateSegFee();
                        
                        if ((!string.IsNullOrEmpty(tbInvoiceSegmentFeeInvoiceAmt.Text) && !string.IsNullOrEmpty(lnTotSegFee.ToString())) && tbInvoiceSegmentFeeInvoiceAmt.Text != lnTotSegFee.ToString())
                            lnTotSegFee = Convert.ToDecimal(tbInvoiceSegmentFeeInvoiceAmt.Text);
                        if (FileRequest.DiscountPercentage.HasValue)
                            lnDiscRate = (decimal)FileRequest.DiscountPercentage;
                        if (QuoteInProgress.IsOverrideDiscountPercentage.HasValue && (bool)QuoteInProgress.IsOverrideDiscountPercentage) // Override Discount % in quote 
                            lnDiscRate = QuoteInProgress.DiscountPercentageTotal.HasValue ? (decimal)QuoteInProgress.DiscountPercentageTotal : 0;
                        else
                            QuoteInProgress.DiscountPercentageTotal = lnDiscRate;
                        if (QuoteInProgress.CQFleetChargeDetails != null)
                        {
                            #region Initializing to 0
                            foreach (CQFleetChargeDetail cqFleetChrgDet in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                            {
                                if (cqFleetChrgDet.CQFleetChargeDetailID != 0)
                                    cqFleetChrgDet.State = CQRequestEntityState.Modified;
                                cqFleetChrgDet.FeeAMT = 0;
                                if (!cqFleetChrgDet.Quantity.HasValue)
                                    cqFleetChrgDet.Quantity = 0;
                                if (!cqFleetChrgDet.SellDOM.HasValue)
                                    cqFleetChrgDet.SellDOM = 0;
                                if (!cqFleetChrgDet.IsDiscountDOM.HasValue)
                                    cqFleetChrgDet.IsDiscountDOM = false;
                                if (!cqFleetChrgDet.IsTaxDOM.HasValue)
                                    cqFleetChrgDet.IsTaxDOM = false;
                                if (!cqFleetChrgDet.BuyDOM.HasValue)
                                    cqFleetChrgDet.BuyDOM = 0;
                                if (!cqFleetChrgDet.SellIntl.HasValue)
                                    cqFleetChrgDet.SellIntl = 0;
                                if (!cqFleetChrgDet.IsDiscountIntl.HasValue)
                                    cqFleetChrgDet.IsDiscountIntl = false;
                                if (!cqFleetChrgDet.IsTaxIntl.HasValue)
                                    cqFleetChrgDet.IsTaxIntl = false;
                                if (!cqFleetChrgDet.BuyIntl.HasValue)
                                    cqFleetChrgDet.BuyIntl = 0;
                            }
                            lnTotCqDisc = 0;
                            lnTotTaxAmt = 0;
                            lnTotBuyAmt = 0;
                            lnTotlndfee = 0;
                            totsub = 0;
                            lnTotCrwRon = 0;
                            #endregion
                            #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition
                            foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3 && x.NegotiatedChgUnit.HasValue && x.NegotiatedChgUnit == 5))
                            {
                                decimal chargeRateDisc = 0.0M;
                                if (chargeRate.CQFleetChargeDetailID != 0)
                                    chargeRate.State = CQRequestEntityState.Modified;
                                //chargeRate.FeeAMT = chargeRate.Quantity * chargeRate.SellDOM;
                                chargeRate.FeeAMT = GetInvoiceFeeAmountByOrderNum(Convert.ToInt32(chargeRate.OrderNUM)); //GET THE TEXTBOX VALUE FOR THE ORDERNUM
                                // Calculate Discount
                                if ((bool)chargeRate.IsDiscountDOM)
                                {
                                    chargeRateDisc = Math.Round((decimal)chargeRate.FeeAMT * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }
                                // Calculate Tax
                                if ((bool)chargeRate.IsTaxDOM && (bool)QuoteInProgress.IsTaxable && chargeRate.FeeAMT.HasValue)
                                {
                                    decimal taxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        taxAmount = Math.Round(((decimal)chargeRate.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        taxAmount = Math.Round((decimal)chargeRate.FeeAMT * (decimal)lnTaxRate / 100, 2);
                                    lnTotTaxAmt += taxAmount;
                                }
                            }
                            #endregion
                            #region Landing Fee
                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                            {
                                foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue))
                                {
                                    if (Leg.AAirportID.HasValue && Leg.AAirportID != 0)
                                    {
                                        decimal lnLegLndngFee = 0;
                                        FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirpot = GetAirport((long)Leg.AAirportID);
                                        if (ArrAirpot != null)
                                        {
                                            switch (lcFleetType)
                                            {
                                                case "S":
                                                    lnLegLndngFee = ArrAirpot.LandingFeeSmall != null ? (decimal)ArrAirpot.LandingFeeSmall : 0;
                                                    break;
                                                case "M":
                                                    lnLegLndngFee = ArrAirpot.LandingFeeMedium != null ? (decimal)ArrAirpot.LandingFeeMedium : 0;
                                                    break;
                                                case "L":
                                                    lnLegLndngFee = ArrAirpot.LandingFeeLarge != null ? (decimal)ArrAirpot.LandingFeeLarge : 0;
                                                    break;
                                            }
                                            lnTotlndfee += lnLegLndngFee;
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region Calculate Fee Amount for all the Standard Charges and Additional Fees, Where OrderNum = 1
                            var StandardCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();
                            if (StandardCharge != null && QuoteInProgress.CQLegs != null)
                            {
                                if (StandardCharge.CQFleetChargeDetailID != 0)
                                    StandardCharge.State = CQRequestEntityState.Modified;
                                decimal buyAmount = 0.0M;
                                foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.IsPositioning.HasValue && x.IsPositioning == false))
                                {
                                    StandardCharge.FeeAMT = GetInvoiceFeeAmountByOrderNum(Convert.ToInt32(StandardCharge.OrderNUM)); //GET TEXTBOX VALUE FOR ORDER NUM 1
                                    lnTotBuyAmt += buyAmount;
                                    // Calculate Discount
                                    decimal chargeRateDisc = 0.0M;
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsDiscountDOM : StandardCharge.IsDiscountIntl))
                                    {
                                        chargeRateDisc = Math.Round((decimal)StandardCharge.FeeAMT * lnDiscRate / 100, 2);
                                        lnTotCqDisc += chargeRateDisc;
                                    }
                                    // Calculate Tax
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsTaxDOM : StandardCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && StandardCharge.FeeAMT.HasValue)
                                    {
                                        decimal taxAmount = 0.0M;
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                            taxAmount = Math.Round(((decimal)StandardCharge.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                        else
                                            taxAmount = Math.Round((decimal)StandardCharge.FeeAMT * lnTaxRate / 100, 2);
                                        lnTotTaxAmt += taxAmount;
                                    }
                                }
                            }
                            #endregion
                            #region Calculate Fee Amount for all Positioning Charges, Where OrderNum = 2
                            var PositionCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 2).FirstOrDefault();
                            if (PositionCharge != null && QuoteInProgress.CQLegs != null)
                            {
                                if (PositionCharge.CQFleetChargeDetailID != 0)
                                    PositionCharge.State = CQRequestEntityState.Modified;
                                decimal buyAmount = 0.0M;
                                foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.IsPositioning.HasValue && x.IsPositioning == true))
                                {
                                    decimal lnFeeAmt = 0;

                                    PositionCharge.FeeAMT = GetInvoiceFeeAmountByOrderNum(Convert.ToInt32(PositionCharge.OrderNUM)); //get textbox value for the order num
                                    // Calculate Discount
                                    decimal chargeRateDisc = 0.0M;
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsDiscountDOM : PositionCharge.IsDiscountIntl))
                                    {
                                        chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                        lnTotCqDisc += chargeRateDisc;
                                    }
                                    // Calculate Tax
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsTaxDOM : PositionCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && PositionCharge.FeeAMT.HasValue)
                                    {
                                        decimal taxAmount = 0.0M;
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                            taxAmount = Math.Round(((decimal)PositionCharge.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                        else
                                            taxAmount = Math.Round((decimal)PositionCharge.FeeAMT * lnTaxRate / 100, 2);
                                        lnTotTaxAmt += taxAmount;
                                    }
                                }
                            }
                            #endregion
                            #region Calculate Standard Crew RON Charge, Where OrderNum = 3
                            var RonCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 3).FirstOrDefault();
                            if (RonCharge != null && QuoteInProgress.CQLegs != null)
                            {
                                if (RonCharge.CQFleetChargeDetailID != 0)
                                    RonCharge.State = CQRequestEntityState.Modified;
                                decimal domRonQty = 0;
                                decimal intlRonQty = 0;
                                decimal domFeeAmt = 0;
                                decimal intlFeeAmt = 0;
                                decimal domBuyAmt = 0;
                                decimal intlBuyAmt = 0;
                                
                                RonCharge.FeeAMT = GetInvoiceFeeAmountByOrderNum(Convert.ToInt32(RonCharge.OrderNUM)); // get textbox value for the ordernum 3
                                lnTotCrwRon += (decimal)RonCharge.FeeAMT;
                                decimal chargeRateDisc = 0.0M;
                                if (RonCharge.IsDiscountIntl.HasValue && (bool)RonCharge.IsDiscountIntl)
                                {
                                    chargeRateDisc = Math.Round((decimal)RonCharge.FeeAMT * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }
                                // Calculate Tax
                                if ((bool)RonCharge.IsTaxDOM && (QuoteInProgress.IsTaxable.HasValue && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT.HasValue)
                                {
                                    decimal domtaxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        domtaxAmount = Math.Round(((decimal)RonCharge.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        domtaxAmount = Math.Round((decimal)RonCharge.FeeAMT * lnTaxRate / 100, 2);
                                    lnTotTaxAmt += domtaxAmount;
                                }
                            }
                            #endregion
                            #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3) Condition
                            foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3))
                            {
                                if (chargeRate.CQFleetChargeDetailID != 0)
                                    chargeRate.State = CQRequestEntityState.Modified;
                                decimal buyAmount = 0.0M;
                                if (QuoteInProgress.CQLegs != null)
                                {
                                    foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue))
                                    {
                                        chargeRate.FeeAMT += GetInvoiceFeeAmountByOrderNum(Convert.ToInt32(chargeRate.OrderNUM)); // GET TEXT BOX VALUE FOR THE ORDER NUM 
                                        decimal chargeRateDisc = 0.0M;
                                        if (chargeRate.NegotiatedChgUnit != 5)
                                        {
                                            // Calculate Discount
                                            if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl))
                                            {
                                                chargeRateDisc = Math.Round((decimal)chargeRate.FeeAMT * lnDiscRate / 100, 2);
                                                lnTotCqDisc += chargeRateDisc;
                                            }
                                            // Calculate Tax
                                            if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl) && (bool)QuoteInProgress.IsTaxable && chargeRate.FeeAMT.HasValue)
                                            {
                                                decimal taxAmount = 0.0M;
                                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                                    taxAmount = Math.Round(((decimal)chargeRate.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                                else
                                                    taxAmount = Math.Round((decimal)chargeRate.FeeAMT * (decimal)lnTaxRate / 100, 2);
                                                lnTotTaxAmt += taxAmount;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region Calculate Adjusted hours TotAdjAmt for first leg and ordernum 1
                            if (QuoteInProgress.CQLegs != null)
                            {
                                CQLeg FirstLeg = new CQLeg();
                                FirstLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.LegNUM == 1).SingleOrDefault();
                                lnTotUseAdj = 0;
                                if (FirstLeg != null)
                                {
                                    if (FirstLeg.DutyTYPE == 1)
                                    {
                                        var StandardChargeDet = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();
                                        if (StandardChargeDet != null)
                                        {
                                            if (StandardChargeDet.CQFleetChargeDetailID != 0)
                                                StandardChargeDet.State = CQRequestEntityState.Modified;
                                            decimal buyAmount = 0.0M;
                                            if (QuoteInProgress.IsOverrideUsageAdj.HasValue)
                                                lnTotUseAdj = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.SellDOM : (decimal)StandardChargeDet.SellIntl), 2);
                                            
                                            if ((!string.IsNullOrEmpty(tbInvoicedailyusageInvoiceAmt.Text) && !string.IsNullOrEmpty(lnTotUseAdj.ToString())) && tbInvoicedailyusageInvoiceAmt.Text != lnTotUseAdj.ToString())
                                                lnTotUseAdj = Convert.ToDecimal(tbInvoicedailyusageInvoiceAmt.Text);
                                            // Calculate Discount
                                            decimal chargeRateDisc = 0.0M;
                                            if ((bool)(((decimal)FirstLeg.DutyTYPE == 1) ? StandardChargeDet.IsDiscountDOM : StandardChargeDet.IsDiscountIntl))
                                            {
                                                chargeRateDisc = Math.Round((decimal)lnTotUseAdj * lnDiscRate / 100, 2);
                                                lnTotCqDisc += chargeRateDisc;
                                            }
                                            // Calculate Tax
                                            if (QuoteInProgress.IsDailyTaxAdj.HasValue && QuoteInProgress.IsTaxable.HasValue && (bool)QuoteInProgress.IsDailyTaxAdj && (bool)QuoteInProgress.IsTaxable)
                                            {
                                                decimal taxAmount = 0.0M;
                                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                                    taxAmount = Math.Round(((decimal)lnTotUseAdj - (decimal)chargeRateDisc) * (decimal)QuoteInProgress.FederalTax / 100, 2);
                                                else
                                                    taxAmount = Math.Round((decimal)lnTotUseAdj * (decimal)QuoteInProgress.FederalTax / 100, 2);
                                                lnTotTaxAmt += taxAmount;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                            totchrg = CalculateSumOfTotalCharge();
                            if (QuoteInProgress.IsOverrideLandingFee.HasValue)
                                lnTotlndfee = (bool)QuoteInProgress.IsOverrideLandingFee ? (decimal)QuoteInProgress.LandingFeeTotal : lnTotlndfee;
                            if (QuoteInProgress.IsLandingFeeTax.HasValue && (bool)QuoteInProgress.IsLandingFeeTax)
                                lnTotTaxAmt += Math.Round((lnTotlndfee * lnTaxRate) / 100, 2);
                            //Sum of fee amount where ordernum > 3
                            decimal totQuoteChrg = 0;
                            totQuoteChrg = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3 && x.FeeAMT.HasValue).Sum(x => x.FeeAMT).Value;
                            QuoteInProgress.SubtotalTotal = totQuoteChrg + totchrg;
                            QuoteInProgress.SegmentFeeTotal = lnTotSegFee;
                            QuoteInProgress.UsageAdjTotal = lnTotUseAdj;
                            QuoteInProgress.LandingFeeTotal = lnTotlndfee;
                            QuoteInProgress.SubtotalTotal = QuoteInProgress.SubtotalTotal + QuoteInProgress.UsageAdjTotal +
                                    QuoteInProgress.SegmentFeeTotal + lnTotCrwRon + QuoteInProgress.LandingFeeTotal;
                            lnTotDiscAmt = lnTotCqDisc + lnTotLndDsc;
                            if (QuoteInProgress.IsOverrideDiscountAMT.HasValue && !(bool)QuoteInProgress.IsOverrideDiscountAMT)
                                QuoteInProgress.DiscountAmtTotal = lnTotDiscAmt;
                            else
                            {
                                if (QuoteInProgress.DiscountAmtTotal.HasValue)
                                    lnTotDiscAmt = (decimal)QuoteInProgress.DiscountAmtTotal;
                                decimal subTotal = 0;
                                subTotal = ((decimal)QuoteInProgress.SubtotalTotal - ((decimal)QuoteInProgress.LandingFeeTotal + (decimal)QuoteInProgress.SegmentFeeTotal));
                                // Condtion for Attempted to divide by zero Error.
                                if (lnTotDiscAmt != 0 && subTotal != 0)
                                    lnDiscRate = ((lnTotDiscAmt * 100) / subTotal);
                                lnDiscRate = Math.Round(lnDiscRate, 2);
                                QuoteInProgress.DiscountPercentageTotal = lnDiscRate;
                            }
                            if (QuoteInProgress.IsOverrideTaxes.HasValue && !(bool)QuoteInProgress.IsOverrideTaxes)
                                QuoteInProgress.TaxesTotal = lnTotTaxAmt;
                            else if (QuoteInProgress.TaxesTotal.HasValue)
                                lnTotTaxAmt = (decimal)QuoteInProgress.TaxesTotal;
                            if (QuoteInProgress.IsOverrideTotalQuote.HasValue)
                                QuoteInProgress.QuoteTotal = (bool)QuoteInProgress.IsOverrideTotalQuote ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt);
                            QuoteInProgress.FlightHoursTotal = lnTotFltHrs;
                            QuoteInProgress.DaysTotal = lnTotDays;
                            QuoteInProgress.AverageFlightHoursTotal = lnTotDays == 0 ? 0 : Math.Round(lnTotFltHrs / lnTotDays, 2);
                            QuoteInProgress.AdditionalFeeTotalD = totQuoteChrg;
                            QuoteInProgress.StdCrewRONTotal = lnTotCrwRon;
                            if (QuoteInProgress.IsOverrideUsageAdj.HasValue)
                                QuoteInProgress.UsageAdjTotal = !(bool)QuoteInProgress.IsOverrideUsageAdj ? lnTotUseAdj : (decimal)QuoteInProgress.UsageAdjTotal;
                            QuoteInProgress.AdjAmtTotal = lnTotAdjAmt;
                            if (!QuoteInProgress.PrepayTotal.HasValue)
                                QuoteInProgress.PrepayTotal = 0;
                            //PM25032013 load prepay total from prepay text box
                            if (!string.IsNullOrEmpty(tbInvoiceTotalPrepayInvoiceAmt.Text) && tbInvoiceTotalPrepayInvoiceAmt.Text != QuoteInProgress.PrepayTotal.ToString())
                                QuoteInProgress.PrepayTotal = Convert.ToDecimal(tbInvoiceTotalPrepayInvoiceAmt.Text);
                            if (QuoteInProgress.IsOverrideTotalQuote.HasValue)
                                QuoteInProgress.RemainingAmtTotal = ((bool)QuoteInProgress.IsOverrideTotalQuote ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt)) - (decimal)QuoteInProgress.PrepayTotal;
                            QuoteInProgress.FlightChargeTotal = totchrg;
                            if (QuoteInProgress.IsOverrideCost.HasValue && (bool)QuoteInProgress.IsOverrideCost)
                                lnTotCost = (decimal)QuoteInProgress.CostTotal;
                            else
                                QuoteInProgress.CostTotal = lnTotBuyAmt;
                            //Margin Calculation
                            lnTotMargin = (decimal)QuoteInProgress.SubtotalTotal - (decimal)QuoteInProgress.CostTotal - (decimal)QuoteInProgress.DiscountAmtTotal;
                            lnTotMarPct = (decimal)QuoteInProgress.SubtotalTotal == 0 ? 0 : Math.Round(lnTotMargin / (decimal)QuoteInProgress.SubtotalTotal, 2);
                            QuoteInProgress.MarginTotal = lnTotMargin;
                            QuoteInProgress.MarginalPercentTotal = lnTotMarPct;
                            //set calculated value in the ui actual fields
                            tbInvoicedailyusageInvoiceAmt.Text = QuoteInProgress.UsageAdjTotal.HasValue ? QuoteInProgress.UsageAdjTotal.ToString() : string.Empty;
                            //LAnding fee
                            tbInvoiceLandingfeeInvoiceAmt.Text = QuoteInProgress.LandingFeeTotal.HasValue ? QuoteInProgress.LandingFeeTotal.ToString() : string.Empty;
                            //Segment Fees 
                            tbInvoiceSegmentFeeInvoiceAmt.Text = QuoteInProgress.SegmentFeeTotal.HasValue ? QuoteInProgress.SegmentFeeTotal.ToString() : string.Empty;
                            //Ron Crew charge     
                            tbInvocieRonCrewChargeInvoiceAmt.Text = QuoteInProgress.StdCrewRONTotal.HasValue ? QuoteInProgress.StdCrewRONTotal.ToString() : string.Empty;
                            //Additional Crew  Charge
                            tbInvocieAddCrewChargeInvoiceAmt.Text = QuoteInProgress.AdditionalCrewTotal.HasValue ? QuoteInProgress.AdditionalCrewTotal.ToString() : string.Empty;

                            //Wait Charge
                            tbInvoiceWaitChargeInvocieAmt.Text = QuoteInProgress.WaitChgTotal.HasValue ? QuoteInProgress.WaitChgTotal.ToString() : string.Empty;
                            //Flight Charge
                            tbInvoiceFlightChargeInvocieAmt.Text = QuoteInProgress.FlightChargeTotal.HasValue ? QuoteInProgress.FlightChargeTotal.ToString() : string.Empty;
                            //Subtotal
                            tbInvoiceSubtotalInvoiceAmt.Text = QuoteInProgress.SubtotalTotal.HasValue ? QuoteInProgress.SubtotalTotal.ToString() : string.Empty;
                            tbInvoiceDiscountInvoiceAmt.Text = QuoteInProgress.DiscountAmtTotal.HasValue ? QuoteInProgress.DiscountAmtTotal.ToString() : string.Empty;
                            //Taxes
                            tbInvoiceTaxesInvoiceAmt.Text = QuoteInProgress.TaxesTotal.HasValue ? QuoteInProgress.TaxesTotal.ToString() : string.Empty;
                            //Total Invoice
                            tbInvoiceTotalInvoiceInvoiceAmt.Text = QuoteInProgress.QuoteTotal.HasValue ? QuoteInProgress.QuoteTotal.ToString() : string.Empty;
                            //Total Prepay
                            tbInvoiceTotalPrepayInvoiceAmt.Text = QuoteInProgress.PrepayTotal.HasValue ? QuoteInProgress.PrepayTotal.ToString() : string.Empty;
                            //Total Remaining Invoice
                            tbInvoiceTotalRemainingInvoiceAmt.Text = QuoteInProgress.RemainingAmtTotal.HasValue ? QuoteInProgress.RemainingAmtTotal.ToString() : string.Empty;
                        }
                    }

                    if (Session[Master.CQSessionKey] != null)
                    {
                        int quotenum = (int)QuoteInProgress.QuoteNUM;
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        //Method will be changed once manager completed
                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                        {
                            var objRetVal1 = Service.GetCQRequestByFileNum((long)FileRequest.FileNUM, false);
                            if (objRetVal1.ReturnFlag)
                            {
                                FileRequest = objRetVal1.EntityList[0];
                                if (FileRequest.CQMains != null)
                                {
                                    foreach (CQMain quote in FileRequest.CQMains.Where(x => x.IsDeleted == false))
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);
                                            if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                            {
                                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                {
                                                    CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                    objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                    objImageFile.RecordType = "CQQuoteOnFile";
                                                    objImageFile.UWAFileName = fwh.UWAFileName;
                                                    objImageFile.RecordID = quote.CQMainID;
                                                    objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                    objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                    objImageFile.IsDeleted = false;
                                                    if (quote.ImageList == null)
                                                        quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                    quote.ImageList.Add(objImageFile);
                                                }
                                            }
                                        }
                                    }
                                    FileRequest.QuoteNumInProgress = quotenum;
                                    Session[Master.CQSessionKey] = FileRequest;
                                }
                            }
                        }
                    }
                }
            }
        }

        public decimal CalculateSumOfETE(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    if (orderNum == 1)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.IsPositioning == false && x.ElapseTM.HasValue);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                    else if (orderNum == 2)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.IsPositioning == true && x.ElapseTM.HasValue);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                    else
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.ElapseTM.HasValue);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                }
                return result;
            }
        }

        public DateTime CalculateMinDeparttDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.DepartureDTTMLocal.HasValue);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Min(x => x.DepartureDTTMLocal).Value;
                }
                return result;
            }
        }

        public DateTime CalculateMaxArrivalDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.ArrivalDTTMLocal.HasValue);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Max(x => x.ArrivalDTTMLocal).Value;
                }
                return result;
            }
        }

        public decimal CalculateSumOfTotalCharge()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.ChargeTotal).Value;
                }
                return result;
            }
        }

        public decimal CalculateSumOfDistance(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    if (orderNum == 1)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.IsPositioning == false).Sum(x => x.Distance).Value;
                    }
                    else if (orderNum == 2)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.IsPositioning == true).Sum(x => x.Distance).Value;
                    }
                    else
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue).Sum(x => x.Distance).Value;
                    }
                }
                return result;
            }
        }

        public decimal CalculateSumOfRON()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue && x.DayRONCNT.HasValue && x.RemainOverNightCNT.HasValue);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                }
                return result;
            }
        }

        public decimal CalculateSegFee()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTotSegFee = 0;
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnCqSegAk = 0;
                decimal lnCqSegHi = 0;
                decimal lnIntlSegFeeRate = 0;
                decimal lnDomSegFeeRate = 0;
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                    {
                        lnTaxRate = QuoteInProgress.FederalTax.HasValue ? (decimal)QuoteInProgress.FederalTax : 0.0M;
                        lnExRate = (FileRequest.ExchangeRate.HasValue && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;
                        FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId company = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                        if (FileRequest.HomebaseID.HasValue)
                            company = Master.GetCompany((long)FileRequest.HomebaseID);
                        else
                        {
                            if (UserPrincipal.Identity._homeBaseId.HasValue)
                                company = Master.GetCompany((long)UserPrincipal.Identity._homeBaseId);
                        }
                        if (company != null)
                        {
                            if (company.SegmentFeeAlaska.HasValue)
                                lnCqSegAk = (decimal)company.SegmentFeeAlaska * lnExRate;
                            if (company.SegementFeeHawaii.HasValue)
                                lnCqSegHi = (decimal)company.SegementFeeHawaii * lnExRate;
                            if (company.ChtQouteIntlSegCHG.HasValue)
                                lnIntlSegFeeRate = (decimal)company.ChtQouteIntlSegCHG * lnExRate;
                            if (company.ChtQouteDOMSegCHG.HasValue)
                                lnDomSegFeeRate = (decimal)company.ChtQouteDOMSegCHG * lnExRate;
                        }
                        foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => (x.IsDeleted == false && x.DAirportID.HasValue && x.AAirportID.HasValue)).ToList())
                        {
                            FlightPak.Web.FlightPakMasterService.GetAllAirport DepartAirport = GetAirport((long)Leg.DAirportID);
                            FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirport = GetAirport((long)Leg.AAirportID);
                            decimal lnDomSegFee = 0;
                            decimal lnIntlSegFee = 0;
                            decimal lnPax_Total = 0;
                            if (DepartAirport != null && ArrAirport != null)
                            {
                                bool DepartRural = false;
                                bool ArrRural = false;
                                if (DepartAirport.IsRural.HasValue)
                                    DepartRural = (bool)DepartAirport.IsRural;
                                if (ArrAirport.IsRural.HasValue)
                                    ArrRural = (bool)ArrAirport.IsRural;
                                if (!ArrRural && !DepartRural)
                                {
                                    string lcCqSegState = string.Empty;
                                    if (DepartAirport.StateName != null && ArrAirport.StateName != null)
                                    {
                                        lcCqSegState = GetStateSegFee(DepartAirport.StateName, ArrAirport.StateName);
                                    }
                                    if ((bool)QuoteInProgress.IsTaxable && (bool)Leg.IsTaxable)
                                    {
                                        switch (lcCqSegState)
                                        {
                                            case "AK":
                                                lnDomSegFeeRate = lnCqSegAk;
                                                break;
                                            case "HI":
                                                lnDomSegFeeRate = lnCqSegHi;
                                                break;
                                        }
                                        lnPax_Total = Leg.PassengerTotal.HasValue ? (decimal)Leg.PassengerTotal : 0;
                                        if (Leg.DutyTYPE == 1)
                                            lnDomSegFee = lnDomSegFeeRate * lnExRate * lnPax_Total;
                                        else if (Leg.DutyTYPE == 2)
                                            lnIntlSegFee = lnIntlSegFeeRate * lnExRate * lnPax_Total;
                                        lnTotSegFee += lnDomSegFee + lnIntlSegFee;
                                    }
                                }
                            }
                        }
                    }
                }
                return lnTotSegFee;
            }
        }

        public void CalculateQuantity()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                {
                    foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false))
                    {
                        if (chargeRate.NegotiatedChgUnit != null)
                        {
                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;
                            //Quantity = Sum of ETE where cqfltchg.nchrgunit is 1 
                            if (chargeRate.NegotiatedChgUnit == 1 && chargeRate.OrderNUM.HasValue)
                            {
                                chargeRate.Quantity = CalculateSumOfETE((int)chargeRate.OrderNUM);
                            }
                            //Quantity = Sum of Distance where cqfltchg.nchrgunit is 2
                            //Quantity = Sum of Distance multiplied by StatueConvFactor where nchrgunit is 3
                            if (chargeRate.NegotiatedChgUnit == 2 && chargeRate.OrderNUM.HasValue)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM);
                            }
                            if (chargeRate.NegotiatedChgUnit == 3 && chargeRate.OrderNUM.HasValue)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * statuateConvFactor;
                            }
                            //Quantity = Total number of legs where nchrgunit is 7
                            if (chargeRate.NegotiatedChgUnit == 7)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                    chargeRate.Quantity = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID.HasValue && x.DAirportID.HasValue).Count();
                            }
                            //Quantity = Sum of Distance * 1.852M, where cqfltchg.nchrgunit is 8
                            if (chargeRate.NegotiatedChgUnit == 8 && chargeRate.OrderNUM.HasValue)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * 1.852M;
                            }
                            //Quantity = Sum of RON and Day Room of all legs where ordernum is 3 
                            if (chargeRate.OrderNUM == 3)
                            {
                                chargeRate.Quantity = CalculateSumOfRON();
                            }
                        }
                    }
                    SaveQuoteinProgressToFileSession();
                }
            }
        }

        public string GetStateSegFee(string DepartState, string ArrivalState)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartState, ArrivalState))
            {
                string segState = string.Empty;
                if (!string.IsNullOrEmpty(DepartState) && (DepartState == "AK" || DepartState == "HI"))
                    segState = DepartState;
                if (!string.IsNullOrEmpty(DepartState) && (DepartState == "AK" || DepartState == "HI"))
                    segState = ArrivalState;
                return segState;
            }
        }

        public FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 AirportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.GetAllAirport Airportmaster = new FlightPak.Web.FlightPakMasterService.GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(AirportID);
                    if (objAirport.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;
                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;
                }
            }
        }

        public FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);
                    if (objPowerSetting.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;
                }
            }
        }

        public void SaveQuoteinProgressToFileSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                    CQMain QuoteToUpdate = new CQMain();
                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        QuoteToUpdate = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                        QuoteToUpdate = QuoteInProgress;
                    }
                    Session[Master.CQSessionKey] = FileRequest;
                }
            }
        }

        /// <summary>
        /// Method to Bind Standard Charges
        /// </summary>
        /// <param name="isRebind"></param>
        /// <param name="dgStandardCharges"></param>
        public void BindStandardCharges(bool isRebind, RadGrid dgStandardCharges)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isRebind, dgStandardCharges))
            {
                dgStandardCharges.DataSource = new string[] { };
                if (isRebind)
                    dgStandardCharges.Rebind();
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        Int64 QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                        if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                        {
                            dgStandardCharges.DataSource = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM).ToList();
                            if (isRebind)
                                dgStandardCharges.Rebind();
                        }
                        else
                        {
                            List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();
                            if (QuoteInProgress != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    // Get New Charter Rates from Fleet Profile Catalog
                                    if (QuoteInProgress != null && QuoteInProgress.FleetID.HasValue)
                                    {
                                        FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID.HasValue && x.FleetID == Convert.ToInt64(QuoteInProgress.FleetID) && (!x.AircraftTypeID.HasValue) && x.IsDeleted == false).ToList();
                                    }
                                    // Get New Charter Rates from Aircraft Type Catalog, if the Source is selected as Type and Tail Number is Null/Empty
                                    else if ((!QuoteInProgress.FleetID.HasValue) && QuoteInProgress.AircraftID.HasValue)
                                    {
                                        FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => (!x.FleetID.HasValue) && (x.AircraftTypeID.HasValue && x.AircraftTypeID == Convert.ToInt64(QuoteInProgress.AircraftID)) && x.IsDeleted == false).ToList();
                                    }
                                    if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                                    {
                                        QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();
                                        List<CQFleetChargeDetail> chargeList = new List<CQFleetChargeDetail>();
                                        foreach (FlightPakMasterService.GetAllFleetNewCharterRate chargeRate in FleetChargeRateList)
                                        {
                                            CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                            fleetCharge.State = CQRequestEntityState.Added;
                                            fleetCharge.CQFlightChargeDescription = chargeRate.FleetNewCharterRateDescription;
                                            fleetCharge.ChargeUnit = chargeRate.ChargeUnit;
                                            fleetCharge.NegotiatedChgUnit = chargeRate.NegotiatedChgUnit;
                                            fleetCharge.Quantity = 0;
                                            fleetCharge.BuyDOM = chargeRate.BuyDOM;
                                            fleetCharge.SellDOM = chargeRate.SellDOM;
                                            fleetCharge.IsTaxDOM = chargeRate.IsTaxDOM;
                                            fleetCharge.IsDiscountDOM = chargeRate.IsDiscountDOM;
                                            fleetCharge.BuyIntl = chargeRate.BuyIntl;
                                            fleetCharge.SellIntl = chargeRate.SellIntl;
                                            fleetCharge.IsTaxIntl = chargeRate.IsTaxIntl;
                                            fleetCharge.IsDiscountIntl = chargeRate.IsDiscountIntl;
                                            fleetCharge.FeeAMT = 0;
                                            fleetCharge.OrderNUM = chargeRate.OrderNum;
                                            fleetCharge.IsDeleted = false;
                                            chargeList.Add(fleetCharge);
                                        }
                                        QuoteInProgress.CQFleetChargeDetails = chargeList;
                                        CalculateQuantity();
                                        dgStandardCharges.DataSource = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM).ToList();
                                    }
                                }
                                SaveQuoteinProgressToFileSession();
                                if (isRebind)
                                    dgStandardCharges.Rebind();
                            }
                        }
                    }
                }
            }
        }
        #endregion

        private decimal GetInvoiceFeeAmountByOrderNum(Int32 orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0;
                foreach (GridDataItem Item in dgInvoiceAddFee.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("OrderNUM") != null)
                    {
                        if (Convert.ToInt32(Item.GetDataKeyValue("OrderNUM").ToString()) == orderNum)
                        {
                            if (Item.FindControl("tbInvoiceAdditionalFeeInvocieAmt") != null)
                            {
                                result = Convert.ToDecimal(((RadNumericTextBox)Item.FindControl("tbInvoiceAdditionalFeeInvocieAmt")).Text);
                            }
                        }
                    }
                }
                return result;
            }
        }

        #region Preview Report
        protected void lnkQuoteReportBottom_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[Master.CQSessionKey] != null)
                        {
                            SaveQuoteReport();
                            Int64 QuoteNumInProgress = 1;

                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                            ShowReport("RptCQQuote", QuoteNumInProgress, FileRequest.CQFileID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        protected void lnkInvoiceReportBottom_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[Master.CQSessionKey] != null)
                        {
                            SaveInvoiceReport();
                            Int64 QuoteNumInProgress = 1;

                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                            ShowReport("RptCQInvoice", QuoteNumInProgress, FileRequest.CQFileID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQReports);
                }
            }
        }

        private void ShowReport(string reportName, Int64 quoteNum, Int64 cqFileID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(reportName, quoteNum, cqFileID))
            {
                Session["TabSelect"] = "Charter";
                Session["REPORT"] = reportName;
                Session["FORMAT"] = "PDF";
                Session["PARAMETER"] = "QuoteNUM=" + quoteNum.ToString() + ";CQFileID=" + cqFileID.ToString() + ";UserCD=" + UserPrincipal.Identity._name;
                Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
            }
        }

        #endregion

        protected void ddlQuoteCustomFooter_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                tbCustomerFooter.Text = ddlQuoteCustomFooter.SelectedItem.Text.ToString();
            }
        }

        protected void ddlInvoiceMyCustomerFooter_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                tbInvoiceFooterInfo.Text = ddlInvoiceMyCustomerFooter.SelectedItem.Text.ToString();
            }
        }



        /// <summary>
        /// Method to Get FileWarehouse By ID
        /// </summary>
        /// <param name="WareHouseID">Pass FileWarehouseID</param>
        /// <returns>Returns FileWarehouse Object</returns>
        private GetFileWarehouseByID GetFileWarehouseByID(Int64 FileWarehouseID)
        {
            using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
            {
                var objRetVal = Service.GetFileWarehouseByID(FileWarehouseID).EntityList;
                return objRetVal[0];
            }
        }


        private void SetImage(Int64 FileWarehouseId, ref Image img)
        {
            if (FileWarehouseId > 0)
            {
                CharterQuoteService.GetFileWarehouseByID objWarehouse = new CharterQuoteService.GetFileWarehouseByID();
                objWarehouse = GetFileWarehouseByID(FileWarehouseId);

                byte[] picture = objWarehouse.UWAFilePath;
                string filename = objWarehouse.UWAFileName;

                int iIndex = filename.Trim().IndexOf('.');
                string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
                string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
                Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                if (regMatch.Success)
                {
                    img.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                    img.AlternateText = filename;
                }
                else
                {
                    img.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                    img.AlternateText = "no-image.jpg";
                }
            }
            else
            {
                img.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                img.AlternateText = "no-image.jpg";
            }
        }
    }
}
