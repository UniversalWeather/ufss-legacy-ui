﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.CharterQuoteService;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Views.Transactions.CharterQuote
{
    public partial class ImagePreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["FileWarehouseId"] != null && !string.IsNullOrEmpty(Request.QueryString["FileWarehouseId"].ToString()))
                {
                    Int64 _warehouseId = 0;
                    _warehouseId = Convert.ToInt64(Request.QueryString["FileWarehouseId"].ToString());

                    SetImage(_warehouseId);
                }
            }
        }

        /// <summary>
        /// Method to Get FileWarehouse By ID
        /// </summary>
        /// <param name="WareHouseID">Pass FileWarehouseID</param>
        /// <returns>Returns FileWarehouse Object</returns>
        private GetFileWarehouseByID GetFileWarehouseByID(Int64 FileWarehouseID)
        {
            using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
            {
                var objRetVal = Service.GetFileWarehouseByID(FileWarehouseID).EntityList;
                return objRetVal[0];
            }
        }


        private void SetImage(Int64 FileWarehouseId)
        {
            if (FileWarehouseId > 0)
            {
                CharterQuoteService.GetFileWarehouseByID objWarehouse = new CharterQuoteService.GetFileWarehouseByID();
                objWarehouse = GetFileWarehouseByID(FileWarehouseId);

                byte[] picture = objWarehouse.UWAFilePath;
                string filename = objWarehouse.UWAFileName;

                int iIndex = filename.Trim().IndexOf('.');
                string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
                string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
                Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                if (regMatch.Success)
                {
                    //if (hdnBrowserName.Value == "Chrome")
                    //{
                    imgPopup.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                    imgPopup.AlternateText = filename;
                    //}
                    //else
                    //{
                    //    Session["CQQuoteOnFileBase64"] = picture;
                    //    img.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                    //    img.AlternateText = filename;
                    //}
                }
                else
                {
                    imgPopup.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                    imgPopup.AlternateText = "no-image.jpg";
                    //Session["CQQuoteOnFileBase64"] = picture;
                }
            }
            else
            {
                imgPopup.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgPopup.AlternateText = "no-image.jpg";
            }

        }
    }


}