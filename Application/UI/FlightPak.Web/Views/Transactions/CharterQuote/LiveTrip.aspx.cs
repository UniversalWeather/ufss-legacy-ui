﻿using System;
using System.Linq;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Web.CharterQuoteService;
using System.Globalization;
using FlightPak.Common.Constants;
using System.Collections.Generic;
using Telerik.Web.UI;

namespace FlightPak.Web.Views.Transactions.CharterQuote
{
    public partial class LiveTrip : BaseSecuredPage
    {
        #region Declarations
        public string CQSessionKey = "CQFILE";
        public string CQSelectedQuoteNum = "CQSELECTEDQUOTENUM"; // Maintain for selected Quote Number after save scenario.
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        Boolean AutoDispatch = false;
        Boolean IsRevision = false;
        private ExceptionManager exManager;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            btnOk.Enabled = true;
                            btnOk.Text = "Ok";
                            btnOk.CssClass = "button";
                        }

                        RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
                        manager.AjaxSettings.AddAjaxSetting(btnOk, DivExternalForm);
                        manager.AjaxSettings.AddAjaxSetting(btnTransfer, DivExternalForm);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// To tranfer records to Preflight
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTransferTOCQ_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Int64 CQQuoteId = 0;
                        FileRequest = (CQFile)Session[CQSessionKey];
                        if (FileRequest != null)
                        {
                            Int64 QuoteNumInProgress = 1;
                            if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                            QuoteInProgress = FileRequest.CQMains.Where(x => x.IsDeleted == false && x.QuoteNUM == QuoteNumInProgress).SingleOrDefault();


                            if (QuoteInProgress != null)
                            {
                                CQQuoteId = ((Int64)QuoteInProgress.CQMainID);
                                if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._IsAutoDispat != null)
                                {
                                    AutoDispatch = UserPrincipal.Identity._fpSettings._IsAutoDispat;
                                }
                                if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._IsAutoRevisionNum != null)
                                {
                                    IsRevision = UserPrincipal.Identity._fpSettings._IsAutoRevisionNum;
                                }

                                string Status = radlstStatus.SelectedValue.ToString();
                                using (PreflightService.PreflightServiceClient PreService = new PreflightService.PreflightServiceClient())
                                {
                                    if (FileRequest.CQFileID != null && CQQuoteId != null)
                                    {
                                        var objRetval = PreService.CopyCQToPreflight(FileRequest.CQFileID, CQQuoteId, AutoDispatch, Status, IsRevision);
                                        if (objRetval.ReturnFlag)
                                        {
                                            if (objRetval.EntityInfo != null)
                                            {
                                                PreflightService.PreflightMain Trip = (PreflightService.PreflightMain)objRetval.EntityInfo;
                                                Trip.Mode = PreflightService.TripActionMode.Edit;
                                                Trip.State = PreflightService.TripEntityState.Modified;

                                                if (Trip.PreflightLegs != null)
                                                {
                                                    foreach (PreflightService.PreflightLeg Leg in Trip.PreflightLegs)
                                                    {
                                                        Leg.State = PreflightService.TripEntityState.Modified;
                                                    }
                                                }

                                                DoLegCalculationsForTrip(ref Trip);
                                                SaveTrip(ref Trip);

                                            }
                                            Session["LiveTrip"] = "yes";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "Close();", true);
                                        }
                                        else
                                            Session["LiveTrip"] = "no";
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected double RoundElpTime(double lnElp_Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lnElp_Time))
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {

                    decimal ElapseTMRounding = 0;

                    //lnEteRound=(double)CompanyLists[0].ElapseTMRounding~lnRound;
                    double lnElp_Min = 0.0;
                    double lnEteRound = 0.0;

                    if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                        ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                    if (ElapseTMRounding == 1)
                        lnEteRound = 0;
                    else if (ElapseTMRounding == 2)
                        lnEteRound = 5;
                    else if (ElapseTMRounding == 3)
                        lnEteRound = 10;
                    else if (ElapseTMRounding == 4)
                        lnEteRound = 15;
                    else if (ElapseTMRounding == 5)
                        lnEteRound = 30;
                    else if (ElapseTMRounding == 6)
                        lnEteRound = 60;

                    if (lnEteRound > 0)
                    {

                        lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                        //lnElp_Min = lnElp_Min % lnEteRound;
                        if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                            lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                        else
                            lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                        if (lnElp_Min > 0 && lnElp_Min < 60)
                            lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                        if (lnElp_Min == 0)
                            lnElp_Time = Math.Floor(lnElp_Time);


                        if (lnElp_Min == 60)
                            lnElp_Time = Math.Ceiling(lnElp_Time);

                    }
                }
                return lnElp_Time;
            }
        }
        public void SaveTrip(ref PreflightService.PreflightMain PreflightTrip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PreflightTrip))
            {
                using (PreflightService.PreflightServiceClient Service = new PreflightService.PreflightServiceClient())
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, PreflightTrip.TripID);
                    }

                    PreflightTrip.AllowTripToSave = true;

                    if (PreflightTrip.PreflightLegs != null)
                    {
                        DateTime LegMinDate = DateTime.MinValue;

                        foreach (PreflightService.PreflightLeg Leg in PreflightTrip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList())
                        {
                            PreflightService.PreflightLeg Nextleg = new PreflightService.PreflightLeg();
                            Nextleg = PreflightTrip.PreflightLegs.Where(x => x.LegNUM == (Leg.LegNUM + 1)).FirstOrDefault();

                            if (Nextleg != null)
                            {
                                if (Nextleg.DepartureGreenwichDTTM != null)
                                    Leg.NextGMTDTTM = Nextleg.DepartureGreenwichDTTM;
                                if (Nextleg.HomeDepartureDTTM != null)
                                    Leg.NextHomeDTTM = Nextleg.HomeDepartureDTTM;
                                if (Nextleg.DepartureDTTMLocal != null)
                                    Leg.NextLocalDTTM = Nextleg.DepartureDTTMLocal;
                            }
                            else
                            {
                                if (Leg.ArrivalGreenwichDTTM != null)
                                    Leg.NextGMTDTTM = Leg.ArrivalGreenwichDTTM;
                                if (Leg.HomeArrivalDTTM != null)
                                    Leg.NextHomeDTTM = Leg.HomeArrivalDTTM;
                                if (Leg.ArrivalDTTMLocal != null)
                                    Leg.NextLocalDTTM = Leg.ArrivalDTTMLocal;
                            }
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                if (LegMinDate == DateTime.MinValue)
                                    LegMinDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (LegMinDate > (DateTime)Leg.DepartureDTTMLocal)
                                    LegMinDate = (DateTime)Leg.DepartureDTTMLocal;
                            }
                        }
                        if (LegMinDate != DateTime.MinValue)
                        {
                            string dtstr = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LegMinDate);
                            DateTime dt = DateTime.ParseExact(dtstr, UserPrincipal.Identity._fpSettings._ApplicationDateFormat, CultureInfo.InvariantCulture);
                            PreflightTrip.EstDepartureDT = dt;
                        }
                        //PreflightTrip.EstDepartureDT = DateTime.ParseExact(LegMinDate, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                    }

                    if (PreflightTrip.AllowTripToSave)
                    {
                        //RemoveTripNavigationProp(ref trip);


                        if ((bool)UserPrincipal.Identity._fpSettings._IsAutoRevisionNum)
                        {

                            if (PreflightTrip.State == PreflightService.TripEntityState.Added)
                                PreflightTrip.RevisionNUM = 1;
                            else
                                PreflightTrip.RevisionNUM = (PreflightTrip.RevisionNUM == null ? 0 : PreflightTrip.RevisionNUM) + 1;
                        }


                        if (UserPrincipal != null)
                            PreflightTrip.LastUpdUID = UserPrincipal.Identity._name;
                        PreflightTrip.LastUpdTS = DateTime.UtcNow;

                        if (PreflightTrip.Account != null) PreflightTrip.Account = null;
                        if (PreflightTrip.Aircraft != null) PreflightTrip.Aircraft = null;
                        if (PreflightTrip.Client != null) PreflightTrip.Client = null;
                        if (PreflightTrip.Crew != null) PreflightTrip.Crew = null;
                        if (PreflightTrip.Company != null) PreflightTrip.Company = null;
                        if (PreflightTrip.Department != null) PreflightTrip.Department = null;
                        if (PreflightTrip.DepartmentAuthorization != null) PreflightTrip.DepartmentAuthorization = null;
                        if (PreflightTrip.EmergencyContact != null) PreflightTrip.EmergencyContact = null;
                        if (PreflightTrip.Fleet != null) PreflightTrip.Fleet = null;
                        if (PreflightTrip.Passenger != null) PreflightTrip.Passenger = null;
                        if (PreflightTrip.UserMaster != null) PreflightTrip.UserMaster = null;
                        if (PreflightTrip.UserMaster1 != null) PreflightTrip.UserMaster1 = null;
                        if (PreflightTrip.RecordType == null) PreflightTrip.RecordType = "T";
                        if (PreflightTrip.LastUpdTS == null) PreflightTrip.LastUpdTS = DateTime.UtcNow;


                        #region Legs

                        if (PreflightTrip.PreflightLegs != null)
                        {


                            foreach (PreflightService.PreflightLeg Leg in PreflightTrip.PreflightLegs)
                            {

                                if (PreflightTrip.EstDepartureDT == null)
                                {
                                    if (Leg.LegNUM == 1 && Leg.DepartureDTTMLocal != null)
                                        PreflightTrip.EstDepartureDT = Leg.DepartureDTTMLocal;
                                }
                                //Leg.TripID = PreflightTrip.TripID;
                                //Leg.PreflightMain = PreflightTrip;ate
                                if (Leg.Account != null) Leg.Account = null;
                                if (Leg.Airport != null) Leg.Airport = null;
                                if (Leg.Airport1 != null) Leg.Airport1 = null;
                                if (Leg.Client != null) Leg.Client = null;
                                if (Leg.FlightCatagory != null) Leg.FlightCatagory = null;
                                if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                                if (Leg.Department != null) Leg.Department = null;
                                if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                                if (Leg.DepartmentAuthorization != null) Leg.DepartmentAuthorization = null;
                                if (Leg.FBO != null) Leg.FBO = null;
                                if (Leg.Passenger != null) Leg.Passenger = null;
                                if (Leg.UserMaster != null) Leg.UserMaster = null;

                                if ((bool)Leg.IsDeleted)
                                {
                                    if (Leg.PreflightCrewLists != null) Leg.PreflightCrewLists.Clear();
                                    if (Leg.PreflightCateringDetails != null) Leg.PreflightCateringDetails.Clear();
                                    if (Leg.PreflightCateringLists != null) Leg.PreflightCateringLists.Clear();
                                    if (Leg.PreflightCheckLists != null) Leg.PreflightCheckLists.Clear();
                                    if (Leg.PreflightFBOLists != null) Leg.PreflightFBOLists.Clear();
                                    if (Leg.PreflightPassengerLists != null) Leg.PreflightPassengerLists.Clear();
                                    if (Leg.PreflightTransportLists != null) Leg.PreflightTransportLists.Clear();
                                    if (Leg.PreflightTripOutbounds != null) Leg.PreflightTripOutbounds.Clear();
                                    if (Leg.PreflightTripSIFLs != null) Leg.PreflightTripSIFLs.Clear();
                                    if (Leg.UWALs != null) Leg.UWALs.Clear();
                                    if (Leg.UWALTsPers != null) Leg.UWALTsPers.Clear();
                                    if (Leg.UWAPermits != null) Leg.UWAPermits.Clear();
                                    if (Leg.UWASpServs != null) Leg.UWASpServs.Clear();
                                    if (Leg.UWATSSLegs != null) Leg.UWATSSLegs.Clear();

                                }
                                else
                                {
                                    if (Leg.PreflightCrewLists != null)
                                    {
                                        foreach (PreflightService.PreflightCrewList Crew in Leg.PreflightCrewLists)
                                        {
                                            //Crew.LegID = Leg.LegID;
                                            //Crew.PreflightLeg = Leg;
                                            if (Crew.PreflightLeg != null) Crew.PreflightLeg = null;
                                            if (Crew.Crew != null) Crew.Crew = null;
                                            if (Crew.UserMaster != null) Crew.UserMaster = null;
                                            //if (Crew.PreflightHotelXrefList != null) Crew.PreflightHotelXrefList = null;

                                            if (Crew.PreflightCrewHotelLists != null)
                                            {
                                                foreach (PreflightService.PreflightCrewHotelList Hotel in Crew.PreflightCrewHotelLists)
                                                {
                                                    if (Hotel.Airport != null) Hotel.Airport = null;
                                                    if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                                    if (Hotel.Hotel != null) Hotel.Hotel = null;
                                                }
                                            }


                                        }
                                    }

                                    if (Leg.PreflightPassengerLists != null)
                                    {
                                        foreach (PreflightService.PreflightPassengerList PAX in Leg.PreflightPassengerLists)
                                        {

                                            if (PAX.PreflightLeg != null) PAX.PreflightLeg = null;
                                            if (PAX.Passenger != null) PAX.Passenger = null;
                                            if (PAX.UserMaster != null) PAX.UserMaster = null;

                                            if (PAX.PreflightPassengerHotelLists != null)
                                            {
                                                foreach (PreflightService.PreflightPassengerHotelList Hotel in PAX.PreflightPassengerHotelLists)
                                                {
                                                    if (Hotel.Airport != null) Hotel.Airport = null;
                                                    if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                                    if (Hotel.Hotel != null) Hotel.Hotel = null;
                                                }
                                            }
                                        }
                                    }

                                    if (Leg.PreflightTransportLists != null)
                                    {
                                        foreach (PreflightService.PreflightTransportList Transport in Leg.PreflightTransportLists)
                                        {
                                            //Transport.LegID = Leg.LegID;
                                            //Transport.PreflightLeg = Leg;
                                            if (Transport.Airport != null) Transport.Airport = null;
                                            if (Transport.UserMaster != null) Transport.UserMaster = null;
                                            if (Transport.Transport != null) Transport.Transport = null;

                                        }
                                    }

                                    //if (Leg.PreflightHotelXrefLists != null)
                                    //{

                                    //    foreach (PreflightHotelXrefList Hotel in Leg.PreflightHotelXrefLists)
                                    //    {
                                    //        //Hotel.LegID = Leg.LegID;
                                    //        //Hotel.PreflightLeg = Leg;
                                    //        if (Hotel.Airport != null) Hotel.Airport = null;
                                    //        if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                    //        if (Hotel.Hotel != null) Hotel.Hotel = null;

                                    //    }
                                    //}


                                    if (Leg.PreflightFBOLists != null)
                                    {
                                        foreach (PreflightService.PreflightFBOList FBO in Leg.PreflightFBOLists)
                                        {
                                            if (FBO.Airport != null) FBO.Airport = null;
                                            if (FBO.UserMaster != null) FBO.UserMaster = null;
                                            if (FBO.PreflightLeg != null) FBO.PreflightLeg = null;
                                            if (FBO.FBO != null) FBO.FBO = null;
                                        }
                                    }

                                    if (Leg.PreflightCateringDetails != null)
                                    {
                                        foreach (PreflightService.PreflightCateringDetail Catering in Leg.PreflightCateringDetails)
                                        {
                                            if (Catering.Airport != null) Catering.Airport = null;
                                            if (Catering.UserMaster != null) Catering.UserMaster = null;
                                            if (Catering.PreflightLeg != null) Catering.PreflightLeg = null;
                                            if (Catering.Catering != null) Catering.Catering = null;
                                        }
                                    }


                                    if (Leg.PreflightTripOutbounds != null)
                                    {
                                        foreach (PreflightService.PreflightTripOutbound outboundInstruction in Leg.PreflightTripOutbounds)
                                        {
                                            if (outboundInstruction.UserMaster != null) outboundInstruction.UserMaster = null;
                                            if (outboundInstruction.PreflightLeg != null) outboundInstruction.PreflightLeg = null;

                                        }
                                    }

                                    if (Leg.PreflightCheckLists != null)
                                    {
                                        foreach (PreflightService.PreflightCheckList checklist in Leg.PreflightCheckLists)
                                        {
                                            if (checklist.UserMaster != null) checklist.UserMaster = null;
                                            if (checklist.PreflightLeg != null) checklist.PreflightLeg = null;
                                            if (checklist.TripManagerCheckList != null) checklist.TripManagerCheckList = null;
                                        }
                                    }

                                }
                            }
                        }
                        #endregion
                        var ReturnValue = Service.Add(PreflightTrip);
                    }
                }
            }
        }
        private int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dateval))
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }
        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = (from aircraft in objPowerSetting.EntityList
                        //                                                                    where aircraft.AircraftID == AircraftID
                        //                                                                    select aircraft).ToList();

                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;


                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;

                }
            }
        }
        public void DoLegCalculationsForTrip(ref PreflightService.PreflightMain Trip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {

                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        List<PreflightService.PreflightLeg> PrefLeg = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        foreach (PreflightService.PreflightLeg Leg in PrefLeg)
                        {

                            #region Miles
                            double Miles = 0;
                            if (Leg.DepartICAOID > 0 && Leg.ArriveICAOID > 0)
                            {
                                Miles = objDstsvc.GetDistance((long)Leg.DepartICAOID, (long)Leg.ArriveICAOID);

                            }
                            Leg.Distance = (decimal)Miles;
                            #endregion
                            #region Bias

                            List<double> BiasList = new List<double>();

                            if (Leg.PowerSetting == null)
                                Leg.PowerSetting = "1";

                            if (Trip.AircraftID != null && Trip.AircraftID != 0)
                            {
                                FlightPakMasterService.Aircraft TripAircraft = new FlightPakMasterService.Aircraft();
                                TripAircraft = GetAircraft((long)Trip.AircraftID);
                                Leg.PowerSetting = TripAircraft.PowerSetting != null ? TripAircraft.PowerSetting : "1";

                            }

                            if (
                                (Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                                &&
                                (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                                &&
                                (Trip.AircraftID != null && Trip.AircraftID > 0)
                                )
                            {
                                BiasList = objDstsvc.CalcBiasTas((long)Leg.DepartICAOID, (long)Leg.ArriveICAOID, (long)Trip.AircraftID, Leg.PowerSetting);
                                if (BiasList != null)
                                {
                                    // lnToBias, lnLndBias, lnTas
                                    Leg.TakeoffBIAS = (decimal)BiasList[0];
                                    Leg.LandingBias = (decimal)BiasList[1];
                                    Leg.TrueAirSpeed = (decimal)BiasList[2];
                                }
                            }
                            else
                            {
                                Leg.TakeoffBIAS = 0.0M;
                                Leg.LandingBias = 0.0M; ;
                                Leg.TrueAirSpeed = 0.0M;
                            }
                            #endregion
                            #region "Wind"
                            Leg.WindsBoeingTable = 0.0M;
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;
                                //Function Wind Calculation
                                double Wind = 0;
                                if (
                                (Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                                &&
                                (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                                &&
                                (Trip.AircraftID != null && Trip.AircraftID > 0)
                                )
                                {
                                    Wind = objDstsvc.GetWind((long)Leg.DepartICAOID, (long)Leg.ArriveICAOID, Convert.ToInt32(Leg.WindReliability), (long)Trip.AircraftID, GetQtr(dt).ToString());
                                    Leg.WindsBoeingTable = (decimal)Wind;
                                }
                            }
                            #endregion
                            #region "ETE"
                            double ETE = 0;
                            // lnToBias, lnLndBias, lnTas

                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;


                                ETE = objDstsvc.GetIcaoEte((double)Leg.WindsBoeingTable, (double)Leg.LandingBias, (double)Leg.TrueAirSpeed, (double)Leg.TakeoffBIAS, (double)Leg.Distance, dt);

                                ETE = RoundElpTime(ETE);
                            }

                            Leg.ElapseTM = (decimal)ETE;

                            #endregion
                            #region CalculateDatetime

                            double x10, x11;
                            // int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                            // DateTime Arrivaldt;
                            //try
                            //{


                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime ChangedDate;

                                ChangedDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (ChangedDate != null)
                                {


                                    DateTime EstDepartDate = (DateTime)ChangedDate;
                                    //if (Trip.EstDepartureDT != null)
                                    //{
                                    //    EstDepartDate = (DateTime)Trip.EstDepartureDT;
                                    //}



                                    try
                                    {

                                        //(Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)&&
                                        if ((Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                                            && (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                                            )
                                        {

                                            if (!string.IsNullOrEmpty(Leg.ElapseTM.ToString()))
                                            {
                                                string ETEstr = "0";

                                                ETEstr = Leg.ElapseTM.ToString();


                                                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                                x10 = (tripleg_elp_time * 60 * 60);
                                                x11 = (tripleg_elp_time * 60 * 60);
                                            }
                                            else
                                            {
                                                x10 = 0;
                                                x11 = 0;
                                            }
                                            DateTime DeptUTCdt;
                                            DateTime ArrUTCdt;

                                            DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;
                                            ArrUTCdt = DeptUTCdt;
                                            ArrUTCdt = ArrUTCdt.AddSeconds(x10);
                                            DateTime ldLocDep = DateTime.MinValue;
                                            DateTime ldLocArr = DateTime.MinValue;
                                            DateTime ldHomDep = DateTime.MinValue;
                                            DateTime ldHomArr = DateTime.MinValue;
                                            ldLocDep = objDstsvc.GetGMT((long)Leg.DepartICAOID, DeptUTCdt, false, false);



                                            if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                ldHomDep = objDstsvc.GetGMT((long)Trip.HomeBaseAirportID, DeptUTCdt, false, false);


                                            ldLocArr = objDstsvc.GetGMT((long)Leg.ArriveICAOID, ArrUTCdt, false, false);

                                            if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                ldHomArr = objDstsvc.GetGMT((long)Trip.HomeBaseAirportID, ArrUTCdt, false, false);

                                            //DateTime ltBlank = EstDepartDate;

                                            if (Leg.DepartICAOID > 0)
                                            {

                                                Leg.DepartureDTTMLocal = ldLocDep;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = ldHomDep;

                                                Leg.DepartureGreenwichDTTM = DeptUTCdt;

                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;

                                                Leg.DepartureDTTMLocal = Localdt;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = Localdt;
                                                Leg.DepartureGreenwichDTTM = Localdt;

                                            }

                                            if (Leg.ArriveICAOID > 0)
                                            {
                                                // DateTime dt = ldLocArr;

                                                Leg.ArrivalDTTMLocal = ldLocArr;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = ldHomArr;
                                                Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;

                                                Leg.ArrivalDTTMLocal = Localdt;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = Localdt;
                                                Leg.ArrivalGreenwichDTTM = Localdt;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //Manually Handled
                                        if (Leg.ArrivalDTTMLocal == null)
                                        {
                                            Leg.ArrivalDTTMLocal = EstDepartDate;
                                        }
                                        if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                            if (Leg.HomeArrivalDTTM == null)
                                            {
                                                Leg.HomeArrivalDTTM = EstDepartDate;
                                            }

                                        if (Leg.ArrivalGreenwichDTTM == null)
                                        {
                                            Leg.ArrivalGreenwichDTTM = EstDepartDate;
                                        }

                                        if (Leg.DepartureDTTMLocal == null)
                                        {
                                            Leg.DepartureDTTMLocal = EstDepartDate;
                                        }

                                        if (Leg.DepartureGreenwichDTTM == null)
                                        {
                                            Leg.DepartureGreenwichDTTM = EstDepartDate;
                                        }
                                        if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                            if (Leg.HomeDepartureDTTM == null)
                                            {
                                                Leg.HomeDepartureDTTM = EstDepartDate;
                                            }
                                    }

                                }
                            }
                            #endregion
                        }

                        #region "PreflightFARRule"


                        double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
                           lnNeededRest, lnWorkArea;

                        lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0;

                        bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                        llRProblem = llFProblem = llDutyBegin = llDProblem = false;

                        DateTime ltGmtDep, ltGmtArr, llDutyend;
                        Int64 lnDuty_RulesID = 0;
                        string lcDuty_Rules = string.Empty;


                        if (Trip.PreflightLegs != null)
                        {
                            List<PreflightService.PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                            lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                            lnDuty_Hrs = 0;
                            lnRest_Hrs = 0;
                            lnFlt_Hrs = 0;
                            //if ((DateTime)Preflegs[0].DepartureGreenwichDTTM != null)
                            //{
                            //    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                            //}
                            //else
                            //{
                            //    ltGmtDep = DateTime.MinValue;
                            //}

                            if (Preflegs[0].DepartureGreenwichDTTM != null)
                            {
                                ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                                ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                                llDutyBegin = true;
                                double lnOldDuty_hrs = -1;

                                int legcounter = 0;
                                foreach (PreflightService.PreflightLeg Leg in Preflegs)
                                {
                                    if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                                    {

                                        double dbElapseTime = 0.0;
                                        long dbCrewDutyRulesID = 0;

                                        if (Leg.ElapseTM != null)
                                            dbElapseTime = (double)Leg.ElapseTM;

                                        //if (Leg.CrewDutyRulesID != null)
                                        //{

                                        //    dbCrewDutyRulesID = (Int64)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID;
                                        //}
                                        //if (dbCrewDutyRulesID != 0)
                                        //{
                                        double lnOverRide = 0;
                                        double lnDutyLeg_Num = 0;

                                        bool llDutyEnd = false;
                                        string FARNum = "";

                                        if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID != null)
                                            lnDuty_RulesID = (Int64)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID;
                                        else
                                            lnDuty_RulesID = 0;
                                        if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd == null)
                                            llDutyEnd = false;
                                        else
                                            llDutyEnd = (bool)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd;

                                        //if it is last leg then  llDutyEnd = true;
                                        if (lnEndLegNum == Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM)
                                            llDutyEnd = true;


                                        lnOverRide = (double)(Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].OverrideValue == null ? 0 : Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].OverrideValue);



                                        lnDutyLeg_Num = (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM;



                                        FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient objectDstsvc = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                                        var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                                      where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                                      select CrewDutyRl).ToList();

                                            if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                            {
                                                FARNum = CrewDtyRule[0].FedAviatRegNum;
                                                lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                                lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                                lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                                lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                                lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                                lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                                lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                            }
                                            else
                                            {
                                                lnDayBeg = 0;
                                                lnDayEnd = 0;
                                                lnMaxDuty = 0;
                                                lnMaxFlt = 0;
                                                lnMinRest = 8;
                                                lnRestMulti = 0;
                                                lnRestPlus = 0;
                                            }
                                        }
                                        TimeSpan? Hoursdiff = (DateTime)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DepartureGreenwichDTTM - ltGmtArr;
                                        if (Leg.ElapseTM != 0)
                                        {
                                            lnFlt_Hrs = lnFlt_Hrs + (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ElapseTM;
                                            lnDuty_Hrs = lnDuty_Hrs + (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                                        }
                                        else
                                        {

                                            lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                                        }

                                        llRProblem = false;

                                        if (llDutyBegin && lnOldDuty_hrs != -1)
                                        {

                                            lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                            if (lnRestMulti > 0)
                                            {

                                                //Here is the Multiple and Plus hours usage
                                                lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                                if (lnRest_Hrs < lnNeededRest)
                                                {

                                                    llRProblem = true;
                                                }
                                            }
                                            else
                                            {
                                                if (lnRest_Hrs < (lnMinRest + lnRestPlus))
                                                {

                                                    llRProblem = true;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            if (llDutyBegin)
                                            {
                                                lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                            }
                                        }

                                        if (llDutyEnd)
                                        {
                                            lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                                        }

                                        string lcCdAlert = "";

                                        if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than tripleg.maxflt
                                            lcCdAlert = "F";// Flight time violation and F is stored in tripleg.cdalert as a Flight time error
                                        else
                                            lcCdAlert = "";



                                        if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                            lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in tripleg.cdalert as a Duty time error

                                        // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;

                                        if (llRProblem)
                                            lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in tripleg.cdalert as a Rest time error 

                                        if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegID != 0 && Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].State != PreflightService.TripEntityState.Deleted)
                                            Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].State = PreflightService.TripEntityState.Modified;
                                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].FlightHours = (decimal)lnFlt_Hrs;
                                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DutyHours = (decimal)lnDuty_Hrs;
                                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].RestHours = (decimal)lnRest_Hrs;
                                        Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyAlert = lcCdAlert;
                                        //Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd = llDutyEnd;


                                        lnLeg_Num = (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM;

                                        llDutyEnd = false;
                                        if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd != null)
                                            llDutyEnd = (bool)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd;

                                        ltGmtArr = (DateTime)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ArrivalGreenwichDTTM;

                                        if ((bool)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd)
                                        {
                                            llDutyBegin = true;
                                            lnFlt_Hrs = 0;
                                        }
                                        else
                                        {
                                            llDutyBegin = false;
                                        }

                                        lnOldDuty_hrs = lnDuty_Hrs;
                                        //check next leg if available do the below steps
                                        legcounter++;
                                        if (legcounter < Preflegs.Count)
                                        {
                                            if (llDutyEnd)
                                            {
                                                lnDuty_Hrs = 0;

                                                if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                                {
                                                    TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                                    lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                                    //ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                    ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                }
                                            }
                                            else
                                            {
                                                lnRest_Hrs = 0;
                                            }
                                        }
                                        //check next leg if available do the below steps

                                        //}
                                    }

                                }
                            }
                        }
                        //}
                        //catch (Exception)
                        //{
                        //    lbTotalDuty.Text = "0.0";
                        //    lbTotalFlight.Text = "0.0";
                        //    lbRest.Text = "0.0";
                        //}

                        #endregion

                        #region FlightCost
                        if (Trip.PreflightLegs != null)
                        {
                            List<PreflightService.PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            decimal TotalCost = 0.0M;

                            foreach (PreflightService.PreflightLeg Leg in Preflegs)
                            {

                                decimal lnChrg_Rate = 0.0M;
                                decimal lnCost = 0.0M;
                                string lcChrg_Unit = string.Empty;

                                if (Trip.FleetID != null && Trip.FleetID != 0)
                                {
                                    Int64 FleetID = Convert.ToInt64(Trip.FleetID);
                                    FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient MasterClient = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                                    var objRetval = MasterClient.GetFleetChargeRateList((long)Trip.FleetID);
                                    if (Leg.DepartureDTTMLocal != null)
                                    {
                                        if (objRetval.ReturnFlag)
                                        {
                                            List<FlightPakMasterService.FleetChargeRate> FleetchargeRate = (from Fleetchrrate in objRetval.EntityList
                                                                                                            where (Fleetchrrate.BeginRateDT <= Leg.ArrivalDTTMLocal
                                                                                                            && Fleetchrrate.EndRateDT >= Leg.DepartureDTTMLocal)
                                                                                                            where Fleetchrrate.FleetID == FleetID
                                                                                                            select Fleetchrrate).ToList();
                                            if (FleetchargeRate.Count > 0)
                                            {
                                                lnChrg_Rate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal)FleetchargeRate[0].ChargeRate;
                                                lcChrg_Unit = FleetchargeRate[0].ChargeUnit;
                                            }
                                        }
                                    }
                                }

                                if (lnChrg_Rate == 0.0M)
                                {
                                    if (Trip.AircraftID != null && Trip.AircraftID > 0)
                                    {
                                        Int64 AircraftID = Convert.ToInt64(Trip.AircraftID);
                                        FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                                        if (Aircraft != null)
                                        {
                                            lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                                            lcChrg_Unit = Aircraft.ChargeUnit;
                                        }
                                    }
                                }

                                switch (lcChrg_Unit)
                                {
                                    case "N":
                                        {
                                            lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate;
                                            break;
                                        }
                                    case "K":
                                        {
                                            lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.852M;
                                            break;
                                        }
                                    case "S":
                                        {
                                            lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.1508M;
                                            break;
                                        }
                                    case "H":
                                        {
                                            lnCost = (Leg.ElapseTM == null ? 0 : (decimal)Leg.ElapseTM) * lnChrg_Rate;
                                            break;
                                        }
                                    default: lnCost = 0.0M; break;

                                }
                                Leg.FlightCost = lnCost;
                                TotalCost = TotalCost + lnCost;
                            }

                            Trip.FlightCost = TotalCost;
                        }
                        #endregion
                    }

                }
            }
        }
        /// <summary>
        /// To show popup based on tripid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (Session[CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[CQSessionKey];
                Session["LiveTrip"] = "no";

                if (FileRequest != null)
                {
                    btnOk.Enabled = false;
                    btnOk.Text = "Processing...";
                    btnOk.CssClass = "button-disable";

                    Int64 QuoteNumInProgress = 1;
                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                    //long QuoteId = Convert.ToInt64(Session[CQSelectedQuoteNum].ToString());

                    QuoteInProgress = FileRequest.CQMains.Where(x => x.IsDeleted == false && x.QuoteNUM == QuoteNumInProgress).SingleOrDefault();

                    if (QuoteInProgress != null)
                    {
                        if (QuoteInProgress.TripID != null && QuoteInProgress.TripID > 0)
                            RadWindowManager1.RadConfirm("Warning, Quote Has Already Been Submitted As A Live Trip.Resubmitting The Quote Will Delete Previous Tripsheet Records. Continue Submitting The Quote", "callBackFn", 330, 160, null, ModuleNameConstants.CharterQuote.CQOnFile);
                        else
                            RadWindowManager1.RadConfirm("Submit Quote To PreFlight?", "callBackFn", 300, 120, null, ModuleNameConstants.CharterQuote.CQOnFile);
                    }
                }
            }
        }
    }
}