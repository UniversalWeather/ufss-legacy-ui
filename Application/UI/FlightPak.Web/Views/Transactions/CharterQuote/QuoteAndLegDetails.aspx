﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CharterQuote.Master"
    AutoEventWireup="true" CodeBehind="QuoteAndLegDetails.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.QuoteAndLegDetails"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CharterQuoteHeadContent" runat="server">
    <style type="text/css">
        .rgDataDiv{ height: auto !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CharterQuoteBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function confirmDeleteLegCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btndeletelegYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btndeletelegNo.ClientID%>').click();
                }
            }

            function fnOpenFeeGroupWin(radWin) {
                var oWnd = radopen("", radWin);
            }

            function fnCloseFeeGroupWindow() {
                var oWindow = $find("<%=rdFeeGroup.ClientID %>");
                oWindow.argument = null;
                oWindow.close();
            }

            function fnDeleteFleetCharge() {
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var grid = $find("<%= dgStandardCharges.ClientID %>");
                var masterTable = grid.get_masterTableView();
                var msg = 'Are you sure you want to delete this flight charge?';
                var isChecked = false;

                if (masterTable.get_dataItems().length > 0) {

                    for (var i = 0; i < masterTable.get_dataItems().length; i++) {
                        var gridItemElement = masterTable.get_dataItems()[i].findElement("chkSelect");
                        if (gridItemElement.checked) {
                            //gridItemElement.checked = true;
                            isChecked = true;
                            //btn.value = "UnCheck";
                        }
                    }

                    //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                    if (isChecked) {
                        radconfirm(msg, deleteFleetChargeCallBackFn, 330, 100, '', 'Delete');
                    }
                    else {
                        var oWnd = radalert('Select atleast any one record to Delete!', 330, 100, "Delete", "");
                        oWnd.get_popupElement().style.zIndex = 100000;
                    }
                    return false;
                }
                else {
                    var oWnd = radalert('Record should not be empty!', 330, 100, "Delete", "");
                    oWnd.get_popupElement().style.zIndex = 100000;
                    return false;
                }
            }

            function deleteFleetChargeCallBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find("<%= dgStandardCharges.ClientID %>");
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }

            function ShowAirportInfoPopup(Airportid) {
                window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
                return false;
            }

            function OpenCQSearch() {
                var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "RadCQSearch");
            }

            function openWinAirport(type) {
                var url = '';
                if (type == "DEPART") {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbDepart.ClientID%>').value;
                    var oWnd = radopen(url, 'rdAirportPopup');
                    oWnd.add_close(OnClientICAOClose1);
                }
                else {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbArrival.ClientID%>').value;
                    var oWnd = radopen(url, 'rdAirportPopup');
                    oWnd.add_close(OnClientICAOClose2);
                }
            }
            function OpenDepartAirportCatalog(AirportID) {
                var oWnd = radopen('../../Settings/Airports/AirportCatalog.aspx?IsPopup=View&AirportID=' + AirportID, "RadAirportCatalog");
                return false;
            }
            function OpenArrivalAirportCatalog(AirportID) {
                var oWnd = radopen('../../Settings/Airports/AirportCatalog.aspx?IsPopup=View&AirportID=' + AirportID, "RadAirportCatalog");
                return false;
            }
            function openWin(radWin) {
                var url = '';
                if (radWin == "rdDepartAirport") {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbDepart.ClientID%>').value;
                }
                else if (radWin == "rdArriveAirport") {
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbArrival.ClientID%>').value;
                }
                else if (radWin == "rdPowerSetting") {
                    url = '../../Settings/Fleet/AircraftPopup.aspx?PowerSetting=' + document.getElementById('<%=tbPower.ClientID%>').value;
                }
                else if (radWin == "rdCrewRules") {
                    url = "../../Settings/People/CrewDutyRulesPopup.aspx?CrewDutyRuleCD=" + document.getElementById('<%=tbCrewRules.ClientID%>').value;
                }
                else if (radWin == "radFeeGroupPopup") {
                    //radconfirm("Warning, This Will Overwrite Existing Additional Fees. Overwrite Additional Fees?", confirmFeeGroupCallBackFn, 330, 100, '', 'Confirmation!');
                    url = "/Views/Settings/Company/FeeScheduleGroupPopup.aspx?Code=" + document.getElementById("<%=tbFeeGroup.ClientID%>").value;
                }
                else if (radWin == "rdHistory") {
                    url = "../../Transactions/History.aspx?FromPage=" + "CharterQuote";
                }

                if (url != "") {
                    var oWnd = radopen(url, radWin);
                }
                else {
                    if (radWin == "rdTOBias")
                        var oWnd = radopen("", radWin);
                    else if (radWin == "rdLandBias")
                        var oWnd = radopen("", radWin);
                    //                    else if (radWin == "rdFeeGroup")
                    //                        var oWnd = radopen("", radWin);
                }
            }

            function confirmFeeGroupCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnConfirmFeeGroupYes.ClientID%>').click();
                    var oWnd = radopen("/Views/Settings/Company/FeeScheduleGroupPopup.aspx?Code=" + document.getElementById("<%=tbFeeGroup.ClientID%>").value, "rdFeeGroup");
                }
            }


            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientICAOClose1(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbDepart.ClientID%>").value = arg.ICAO.toUpperCase();
                        document.getElementById("<%=lbDepart.ClientID%>").innerHTML = arg.AirportName.toUpperCase();
                        document.getElementById("<%=hdnDepart.ClientID%>").value = arg.AirportID;
                        if (arg.ICAO != null)
                            document.getElementById("<%=lbcvDepart.ClientID%>").innerHTML = "";
                        var step = "tbDepart_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbDepart.ClientID%>").value = "";
                        document.getElementById("<%=lbDepart.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnDepart.ClientID%>").value = "";

                    }
                }
            }

            function ShowWarningMessage(uicontrol, sender, e) {
                if (document.getElementById("<%=hdnCompSettingforshowWarn.ClientID%>").value == "true") {
                    if (document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value == "true") {

                        function callBackFn(confirmed) {
                            if (confirmed) {
                                document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "false";
                                if (uicontrol == "date")
                                    showPopup(sender, e);
                            }
                        }

                        var msg = 'WARNING - The auto date adjustment feature is active.  Changes made to any dates or RON values will automatically adjust all other legs onthis itinerary.Press YES to Continue or NO to Return.';
                        radconfirm(msg, callBackFn, 330, 100, '', 'Quote And Leg');
                        //return false;

                        //                        if (confirm()) {
                        //                            document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "false";
                        //                            if (uicontrol == "date")
                        //                                showPopup(sender, e);
                        //                        }
                        //                        else {
                        //                            document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "true";
                        //                            return false;
                        //                        }
                    }
                    else {
                        if (uicontrol == "date")
                            showPopup(sender, e);
                    }
                }
                else {
                    if (uicontrol == "date")
                        showPopup(sender, e);
                }
            }

            function callBackFn(confirmed) {
                if (confirmed) {
                    document.getElementById("<%=hdnShowWarnMessage.ClientID%>").value = "false";
                    if (uicontrol == "date")
                        showPopup(sender, e);
                }
            }

            function OnClientICAOClose2(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbArrival.ClientID%>").value = arg.ICAO.toUpperCase();
                        document.getElementById("<%=lbArrival.ClientID%>").innerHTML = arg.AirportName.toUpperCase();
                        document.getElementById("<%=hdnArrival.ClientID%>").value = arg.AirportID;
                        if (arg.ICAO != null)
                            document.getElementById("<%=lbcvArrival.ClientID%>").innerHTML = "";
                        var step = "tbArrive_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbArrival.ClientID%>").value = "";
                        document.getElementById("<%=lbArrival.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnArrival.ClientID%>").value = "";

                    }
                }
            }

            function OnClientPowerClose(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbPower.ClientID%>").value = arg.PowerSetting;
                        document.getElementById("<%=hdPower.ClientID%>").value = arg.AircraftID;
                        if (arg.PowerSetting != null)
                            document.getElementById("<%=lbcvPower.ClientID%>").innerHTML = "";
                        var step = "tbPower_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbPower.ClientID%>").value = "";
                        document.getElementById("<%=hdPower.ClientID%>").value = "";
                        document.getElementById("<%=lbcvPower.ClientID%>").innerHTML = "";

                    }
                }
            }

            function OnClientCrewRuleClose(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbCrewRules.ClientID%>").value = arg.CrewDutyRuleCD;
                        document.getElementById("<%=lbFar.ClientID%>").innerHTML = arg.FedAviatRegNum;
                        document.getElementById("<%=hdnCrewRules.ClientID%>").value = arg.CrewDutyRulesID;
                        if (arg.CrewDutyRuleCD != null)
                            document.getElementById("<%=lbcvCrewRules.ClientID%>").innerHTML = "";
                        var step = "tbCrewRules_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbCrewRules.ClientID%>").value = "";
                        document.getElementById("<%=lbFar.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCrewRules.ClientID%>").value = "";
                    }
                }
            }

            function OnClientFeeScheduleGroupClose(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbFeeGroup.ClientID%>").value = arg.FeeGroupCD;
                        document.getElementById("<%=hdnFeeGroupID.ClientID%>").value = arg.FeeGroupID;
                        if (arg.FeeGroupDescription != null)
                            document.getElementById("<%=lbFeeGroupDesc.ClientID%>").innerHTML = arg.FeeGroupDescription;
                        document.getElementById("<%=lbFeeGroupMsg.ClientID%>").innerHTML = "";

                        var step = "FeeGroup_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbFeeGroup.ClientID%>").value = "";
                        document.getElementById("<%=hdnFeeGroupID.ClientID%>").value = "";
                        document.getElementById("<%=lbFeeGroupMsg.ClientID%>").innerHTML = "";
                        document.getElementById("<%=lbFeeGroupDesc.ClientID%>").innerHTML = "";
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                currentLoadingPanel.hide(currentUpdatedControl);
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }

            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        alert("Please enter/select Date between 01/01/1900 and 12/31/2100");
                        control.value = "";
                    }
                }
                return false;
            }

            function RowDblClick() {
                var masterTable = $find("<%= dgPowerSetting.ClientID %>").get_masterTableView();
                var step = "btPowerSelect_Click";
                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
                //return false;
            }

        </script>
        <script type="text/javascript">

            function checknumericValue(myfield) {
                var postbackval = true;
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") > 0) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length > 2) {
                                alert("Invalid format, Required format is NN.N ");
                                myfield.value = "0";
                                myfield.focus();
                                postbackval = false;

                            }
                            if (strarr[1].length > 1) {
                                alert("Invalid format, Required format is NN.N ");
                                myfield.value = "0";
                                myfield.focus();
                                postbackval = false;
                            }
                        }
                    }
                    else {
                        if (myfield.value.length > 2) {
                            alert("Invalid format, Required format is NN.N ");
                            myfield.value = "0";
                            myfield.focus();
                            postbackval = false;
                        }
                    }
                }



                //                if (postbackval == true) {
                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                ajaxManager.ajaxRequest("tbOverride_TextChanged");
                return postbackval;
                //                }

            }


            function CheckTenthorHourMinValuebias(myfield) {
                var postbackval = true;
                var CompanyProfileSetting = "";
                companyprofileSetting = document.getElementById('<%= hdTimeDisplayTenMin.ClientID%>').value;
                if (companyprofileSetting == 1) {
                    var count = 0;
                    var strarr = myfield.value;
                    for (var i = 0; i < strarr.length; ++i) {
                        if (strarr[i] == ".")
                            count++;
                    }
                    if (count > 1) {
                        radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                        myfield.value = "0.0";
                        myfield.focus();
                        postbackval = false;
                    }
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(":") >= 0) {
                            radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                            myfield.value = "0.0";
                            myfield.focus();
                            postbackval = false;
                        }
                        else if (myfield.value.indexOf(".") >= 0) {
                            var strarr = myfield.value.split(".");

                            if (strarr[0].length > 2) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }

                            if (strarr[1].length > 1) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            if (postbackval) {
                                if (strarr[0].length == 0)
                                    myfield.value = "0" + myfield.value;
                            }
                            if (postbackval) {
                                if (strarr[1].length == 0)
                                    myfield.value = myfield.value + "0";
                            }
                        }
                        else {
                            if (myfield.value.length > 2) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                myfield.value = myfield.value + ".0";
                            }
                        }
                    }


                }
                else {
                    var hourvalue = "00";
                    var minvalue = "00";
                    var count = 0;
                    if (myfield.value.indexOf(".") >= 0) {
                        radalertCommonError("Invalid format, Required format is HH:MM.", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                        postbackval = false;
                    }
                    //check for colon
                    else if (myfield.value.indexOf(":") >= 0) {

                        var strarr = myfield.value.split(":");

                        if (strarr.length > 2) {
                            radalertCommonError("Invalid format, Required format is HH:MM.", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                            postbackval = false;
                        }


                        // hour validation
                        if (strarr[0].length <= 2) {
                            hourvalue = "00" + strarr[0];
                            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                        }
                        else {
                            radalertCommonError("Invalid format, Required format is HH:MM.", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                            myfield.value = "00:00";
                            myfield.focus();
                            postbackval = false;

                        }
                        // hour validation
                        if (postbackval) {
                            //minute Validation
                            if (strarr[1].length <= 2) {
                                if (Math.abs(strarr[1]) > 59) {
                                    radalertCommonError("Minute entry must be between 0 and 59", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                                    hourvalue = "00";
                                    minvalue = "00";
                                    myfield.focus();
                                    postbackval = false;
                                }
                                else {
                                    minvalue = "00" + strarr[1];
                                    minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                                }
                            }
                            //minute Validation
                        }
                        //check for colon
                    }
                    else {
                        if (myfield.value.length <= 2) {
                            hourvalue = "00" + myfield.value;
                            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                        }
                        else {
                            radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbBias.ClientID%>"))
                            myfield.value = "00:00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }

                    myfield.value = hourvalue + ":" + minvalue;
                }

                if (postbackval) {
                    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                    ajaxManager.ajaxRequest("tbBias_TextChanged");
                }

                return postbackval;
            }

            function CheckTenthorHourMinValueLandbias(myfield) {
                var postbackval = true;
                var CompanyProfileSetting = "";
                companyprofileSetting = document.getElementById('<%= hdTimeDisplayTenMin.ClientID%>').value;
                if (companyprofileSetting == 1) {
                    var count = 0;
                    var strarr = myfield.value;
                    for (var i = 0; i < strarr.length; ++i) {
                        if (strarr[i] == ".")
                            count++;
                    }
                    if (count > 1) {
                        radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"))
                        myfield.value = "0.0";
                        myfield.focus();
                        postbackval = false;
                    }
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(":") >= 0) {
                            radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                            myfield.value = "0.0";
                            myfield.focus();
                            postbackval = false;
                        }
                        else if (myfield.value.indexOf(".") >= 0) {
                            var strarr = myfield.value.split(".");

                            if (strarr[0].length > 2) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }

                            if (strarr[1].length > 1) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            if (postbackval) {
                                if (strarr[0].length == 0)
                                    myfield.value = "0" + myfield.value;
                            }
                            if (postbackval) {
                                if (strarr[1].length == 0)
                                    myfield.value = myfield.value + "0";
                            }
                        }
                        else {
                            if (myfield.value.length > 2) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                                myfield.value = "0.0";
                                myfield.focus();
                                postbackval = false;
                            }
                            else {
                                myfield.value = myfield.value + ".0";
                            }
                        }
                    }


                }
                else {
                    var hourvalue = "00";
                    var minvalue = "00";
                    var count = 0;
                    if (myfield.value.indexOf(".") >= 0) {
                        radalertCommonError("Invalid format, Required format is HH:MM.", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                        postbackval = false;
                    }
                    //check for colon
                    else if (myfield.value.indexOf(":") >= 0) {

                        var strarr = myfield.value.split(":");
                        // hour validation
                        if (strarr[0].length <= 2) {
                            hourvalue = "00" + strarr[0];
                            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                        }
                        else {
                            radalertCommonError("Invalid format, Required format is HH:MM.", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                            myfield.value = "00:00";
                            myfield.focus();
                            postbackval = false;

                        }
                        // hour validation
                        if (postbackval) {
                            //minute Validation
                            if (strarr[1].length <= 2) {
                                if (Math.abs(strarr[1]) > 59) {
                                    radalertCommonError("Minute entry must be between 0 and 59.", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                                    hourvalue = "00";
                                    minvalue = "00";
                                    myfield.focus();
                                    postbackval = false;
                                }
                                else {
                                    minvalue = "00" + strarr[1];
                                    minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                                }
                            }
                            //minute Validation
                        }
                        //check for colon
                    }
                    else {
                        if (myfield.value.length <= 2) {
                            hourvalue = "00" + myfield.value;
                            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                        }
                        else {
                            radalertCommonError("Invalid format, Required format is HH:MM.", 330, 100, 100000, document.getElementById("<%=tbLandBias.ClientID%>"));
                            myfield.value = "00:00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }

                    myfield.value = hourvalue + ":" + minvalue;
                }

                if (postbackval) {
                    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                    ajaxManager.ajaxRequest("tbLandBias_TextChanged");
                }

                return postbackval;
            }

            function CheckTenthorHourMinValueEte(myfield) {
                var postbackval = true;
                var CompanyProfileSetting = "";
                companyprofileSetting = document.getElementById('<%= hdTimeDisplayTenMin.ClientID%>').value;
                if (companyprofileSetting == 1) {
                    var count = 0;
                    var strarr = myfield.value;
                    for (var i = 0; i < strarr.length; ++i) {
                        if (strarr[i] == ".")
                            count++;
                    }
                    if (count > 1) {
                        radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                        myfield.value = "0.0";
                        postbackval = false;
                    }
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(":") >= 0) {
                            radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                            myfield.value = "0.0";
                            postbackval = false;
                        }
                        else if (myfield.value.indexOf(".") >= 0) {
                            var strarr = myfield.value.split(".");

                            if (strarr[0].length > 2) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                                myfield.value = "0.0";
                                postbackval = false;
                            }

                            if (strarr[1].length > 1) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                                myfield.value = "0.0";
                                postbackval = false;
                            }
                            if (postbackval) {
                                if (strarr[0].length == 0)
                                    myfield.value = "0" + myfield.value;
                            }
                            if (postbackval) {
                                if (strarr[1].length == 0)
                                    myfield.value = myfield.value + "0";
                            }
                        }
                        else {
                            if (myfield.value.length > 2) {
                                radalertCommonError("Invalid format, Required format is NN.N", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                                myfield.value = "0.0";
                                postbackval = false;
                            }
                            else {
                                myfield.value = myfield.value + ".0";
                            }
                        }
                    }


                }
                else {
                    var hourvalue = "00";
                    var minvalue = "00";
                    var count = 0;
                    if (myfield.value.indexOf(".") >= 0) {
                        radalertCommonError("Invalid format, Required format is HH.MM.", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                        postbackval = false;
                    }
                    //check for colon
                    else if (myfield.value.indexOf(":") >= 0) {

                        var strarr = myfield.value.split(":");
                        // hour validation
                        if (strarr[0].length <= 2) {
                            hourvalue = "00" + strarr[0];
                            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                        }
                        else {
                            radalertCommonError("Invalid format, Required format is HH.MM.", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                            postbackval = false;

                        }
                        // hour validation
                        if (postbackval) {
                            //minute Validation
                            if (strarr[1].length <= 2) {
                                if (Math.abs(strarr[1]) > 59) {
                                    radalertCommonError("Minute entry must be between 0 and 59", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                                    hourvalue = "00";
                                    minvalue = "00";
                                    myfield.focus();
                                    postbackval = false;
                                }
                                else {
                                    minvalue = "00" + strarr[1];
                                    minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                                }
                            }
                            //minute Validation
                        }
                        //check for colon
                    }
                    else {
                        if (myfield.value.length <= 2) {
                            hourvalue = "00" + myfield.value;
                            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
                        }
                        else {
                            radalertCommonError("Invalid format, Required format is HH.MM.", 330, 100, 100000, document.getElementById("<%=tbETE.ClientID%>"));
                            postbackval = false;
                        }
                    }

                    myfield.value = hourvalue + ":" + minvalue;

                }

                myfield.value = DefaultEteResult(myfield.value, companyprofileSetting);

                if (postbackval) {
                    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                    ajaxManager.ajaxRequest("tbETE_TextChanged");
                }

                return postbackval;
            }

            function fnAllowNumericAndOneDecimal(fieldname, myfield, e) {
                var key;
                var keychar;
                var allowedString = "0123456789" + ".";

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);

                if (keychar == ".") {

                    if (myfield.value.indexOf(keychar) > -1) {
                        return false;
                    }
                }

                if (fieldname == "override") {
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(".") >= 0) {
                            if (myfield.value.indexOf(".") < myfield.value.length - 1) {
                                var strarr = myfield.value.split(".");
                                if (strarr[0].length >= 2)
                                    return false;
                                if (strarr[1].length > 0 && strarr[1].length == 1)
                                    return false;
                            }
                        }
                        else {
                            if (myfield.value.length >= 2 && keychar != ".")
                                return false;
                        }
                    }
                }
                // control keys
                if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    return true;
                // numeric values and allow slash("/")
                else if ((allowedString).indexOf(keychar) > -1)
                    return true;
                else
                    return false;
            }

            function checkNegativeValue(myfield) {
                var postbackval = true;

                if (myfield.value.length > 0) {

                    if (myfield.value.indexOf("-") > 0) {
                        postbackval = false;
                        alert("Invalid format, Required format is (-)### ");
                        myfield.value = "0.0";
                    }
                    else {
                        var strarr = myfield.value.split("-");
                        if (strarr.length > 2) {
                            postbackval = false;
                            alert("Invalid format, Required format is (-)### ");
                            myfield.value = "0.0";
                        }
                    }

                }
                if (postbackval) {
                    var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                    ajaxManager.ajaxRequest("tbWind_TextChanged");
                }
                return postbackval;
            }
        </script>
        <script type="text/javascript">
            //To display plane image at right place
            isLegDetailsExpanded = true;
            isQuoteDetailsExpanded = true;

            function pnlTripExpand() {
                isLegDetailsExpanded = true;
            }
            function pnlTripCollapse() {
                isLegDetailsExpanded = false;
            }
            function pnlQuoteDetailsExpand() {
                isQuoteDetailsExpanded = true;
            }
            function pnlQuoteDetailsCollapse() {
                isQuoteDetailsExpanded = false;
            }


            $(document).ready(function showPlaneImage() {
                $('body').on('mouseenter', '.JqFixedTooltip', function () {
                    if ($(this).val() == "5") {
                        var pos = $(this).position();
                        $("#tooltipFixedChargeUnit").css({ top: pos.top + 20, left: pos.left + 20 });
                        $("#tooltipFixedChargeUnit").show();
                    }
                });
                $('body').on('mouseleave', '.JqFixedTooltip', function () {
                    $("#tooltipFixedChargeUnit").hide();
                });
                $("body").on('mouseenter', '#FixedTooltipDisabled', function () {
                    var IsEditMode = $("#<%=IsEditMode.ClientID%>").val();
                    if (IsEditMode == "True") {
                        $(this).remove();
                    } else {
                        if ($(this).parent().find(".JqFixedTooltip").val() == 5) {
                            var pos = $(this).position();
                            $("#tooltipFixedChargeUnit").css({ top: pos.top + 20, left: pos.left + 20 });
                            $("#tooltipFixedChargeUnit").show();
                        }
                    }
                });
                $('body').on('mouseleave', '#FixedTooltipDisabled', function () {
                    $("#tooltipFixedChargeUnit").hide();
                });
                //Top new/delete file/edit file/cancel/save Button
                $('#ctl00_ctl00_MainContent_btnNewFile, #ctl00_ctl00_MainContent_btnDeleteFile,'
                    + '#ctl00_ctl00_MainContent_btnEditFile, #ctl00_ctl00_MainContent_btnCancelFile'
                    + '#ctl00_ctl00_MainContent_btnSaveFile').on('click', setPlaneImage);

                //Code for Add leg/ Insert Leg/ Delete Leg
                $('#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnAddLeg,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnInsertLeg,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnDeleteLeg').on('click', setPlaneImage);

                //Grid row and sorting
                $('.rgRow, th.rgHeader').on('click', setPlaneImage);

                //Confirmation
                $('.rwPopupButton').on('click', setPlaneImage);

                //Charges and Total Fixed Cost
                $('#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnUsageAdj,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnSegmentFees,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnLandingFees,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnDiscount,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnDiscountAmount,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnTaxes,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnTotalQuote,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_pnlQuoteDetails_i0_btnTotalCost').on('click', setPlaneImage);

                // Bottom edit/delete/save/cancle/next
                $('#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnEditFile,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnDelete,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnSave,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnCancel,'
                    + '#ctl00_ctl00_MainContent_CharterQuoteBodyContent_btnNext').on('click', setPlaneImage);
            });

            function setPlaneImage() {

                var scrollTop = $(window).scrollTop();
                var topMargin = 0;

                if (isLegDetailsExpanded) {
                    topMargin -= 250;
                }
                if (isQuoteDetailsExpanded) {
                    topMargin -= 240;
                }

                topMargin += scrollTop - 180; //-180 is value when all are collapsed
                $('div.raDiv').attr('style', 'margin-top: ' + topMargin + 'px !important');
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFeeGroupCRUDPopup" runat="server" Height="300px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAirportCatalog" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Width="450px" Height="400px"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDepartAirport" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientICAOClose1" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Width="450px" Height="400px" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdArriveAirport" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientICAOClose2" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPower" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientPowerClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPowerSetting" runat="server" VisibleOnPageLoad="false" Height="230px"
                Width="500px" Modal="true" BackColor="#DADADA" Behaviors="Close" VisibleStatusbar="false"
                Title="Aircraft Power Settings">
                <ContentTemplate>
                    <table width="100%" class="box1">
                        <tr>
                            <td>
                                Aircraft:
                                <asp:Label ID="lbAircraft" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="preflight-custom-grid">
                                <telerik:RadGrid ID="dgPowerSetting" OnNeedDataSource="PowerSetting_BindData" runat="server"
                                    AllowSorting="true" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                                    PageSize="5" Width="100%">
                                    <MasterTableView CommandItemDisplay="None" AllowSorting="false" DataKeyNames="Power,PowerSetting,TAS,HrRange,TOBias,LandBias"
                                        AllowFilteringByColumn="false">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="PowerSetting" HeaderText="Power Setting" CurrentFilterFunction="Contains"
                                                FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="120px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TAS" HeaderText="TAS" CurrentFilterFunction="Contains"
                                                FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="40px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HrRange" HeaderText="Hr. Range" CurrentFilterFunction="Contains"
                                                FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="70px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TOBias" HeaderText="TO Bias" CurrentFilterFunction="Contains"
                                                FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="60px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LandBias" HeaderText="Land Bias" CurrentFilterFunction="Contains"
                                                FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="70px">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        <PagerStyle AlwaysVisible="false" />
                                    </MasterTableView>
                                    <ClientSettings>
                                        <ClientEvents OnRowDblClick="RowDblClick" />
                                        <Scrolling AllowScroll="true" />
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="bottom">
                                <asp:Button ID="btPowerSelect" runat="server" CssClass="button" Text="Select" OnClick="btPowerSelect_Click" />
                                <asp:Button ID="btPowerCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btPowerCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCrewRules" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCrewRuleClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyRulesPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFeeGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFeeScheduleGroupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/FeeScheduleGroupPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFeeGroup" runat="server" Height="120px" Width="400px" Modal="true"
                BackColor="#DADADA" Behaviors="Close" VisibleStatusbar="false" Title="Fee Group - Confirmation!"
                VisibleOnPageLoad="false">
                <ContentTemplate>
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td align="left">
                                    <div style="margin-top: 5px;">
                                        Warning, This Will Overwrite/Append Existing Additional Fees!
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnFeeGrpOverride" runat="server" Text="Override" OnClick="FeeGrpOverride_Click"
                                                    CssClass="button" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnFeeGrpAppend" runat="server" Text="Append" OnClick="FeeGrpAppend_Click"
                                                    CssClass="button" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnFeeGrpCancel" runat="server" Text="Cancel" OnClick="FeeGrpCancel_Click"
                                                    CssClass="button" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTOBias" runat="server" VisibleOnPageLoad="false" Width="600px"
                Height="180px" Behaviors="Close" Modal="true" VisibleStatusbar="false" Title="Airport Bias">
                <ContentTemplate>
                    <table class="box1">
                        <tr>
                            <td class="tdLabel80">
                                ICAO
                            </td>
                            <td class="tdLabel120">
                                City
                            </td>
                            <td class="tdLabel80">
                                State
                            </td>
                            <td class="tdLabel150">
                                Country
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbDepartsICAO" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbDepartsCity" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbDepartsState" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbDepartsCountry" runat="server" Visible="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                Airport
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lbDepartsAirport" runat="server" Visible="true" CssClass="text150"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                                    <tr>
                                        <td class="tdLabel100">
                                            Takeoff Bias
                                        </td>
                                        <td class="tdLabel40">
                                            <asp:Label ID="lbDepartsTakeoffbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                        <td class="tdLabel100">
                                            Landing Bias
                                        </td>
                                        <td>
                                            <asp:Label ID="lbDepartsLandingbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdLandBias" runat="server" VisibleOnPageLoad="false" Width="600px"
                Height="180px" Behaviors="Close" Modal="true" VisibleStatusbar="false" Title="Airport Bias">
                <ContentTemplate>
                    <table class="box1">
                        <tr>
                            <td class="tdLabel80">
                                ICAO
                            </td>
                            <td class="tdLabel120">
                                City
                            </td>
                            <td class="tdLabel80">
                                State
                            </td>
                            <td class="tdLabel150">
                                Country
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbArrivesICAO" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbArrivesCity" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbArrivesState" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbArrivesCountry" runat="server" Visible="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                Airport
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lbArrivesAirport" runat="server" Visible="true" CssClass="text150"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                                    <tr>
                                        <td class="tdLabel100">
                                            Takeoff Bias
                                        </td>
                                        <td class="tdLabel40">
                                            <asp:Label ID="lbArrivesTakeoffbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                        <td class="tdLabel100">
                                            Landing Bias
                                        </td>
                                        <td>
                                            <asp:Label ID="lbArrivesLandingbias" runat="server" Text="0:00" Visible="true" CssClass="text90"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div style="width: 712px;">
            <asp:HiddenField ID="hdTimeDisplayTenMin" runat="server" />
            <asp:HiddenField ID="hdShowRequestor" runat="server" />
            <asp:HiddenField ID="hdnCompSettingforshowWarn" runat="server" />
            <asp:HiddenField ID="hdnShowWarnMessage" runat="server" />
            <asp:HiddenField ID="hdHighlightTailno" runat="server" />
            <asp:HiddenField ID="hdIsDepartAuthReq" runat="server" />
            <asp:HiddenField ID="hdSetFarRules" runat="server" />
            <asp:HiddenField ID="hdSetCrewRules" runat="server" />
            <asp:HiddenField ID="hdSetWindReliability" runat="server" />
            <asp:HiddenField ID="hdHomeCategory" runat="server" />
            <asp:HiddenField ID="hdHomeChkListGroup" runat="server" />
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:HiddenField ID="hdnLegNum" runat="server" />
            <asp:HiddenField ID="IsEditMode" runat="server"/>
            <table width="100%">
                <tr>
                    <td align="right">
                         <div style="float: right; text-align: right; width: 314px; padding-top: 5px;">
                            <asp:Button ID="btnAddLeg" runat="server" CssClass="ui_nav" ToolTip="Add New Leg" Text="Add Leg" OnClick="btnAddLeg_Click" />
                            <asp:Button ID="btnInsertLeg" runat="server" CssClass="ui_nav" Text="Insert Leg" ToolTip="Insert New Leg Prior to the Highlighted Leg" OnClick="btnInsertLeg_Click" />
                            <asp:Button ID="btnDeleteLeg" runat="server" CssClass="ui_nav" Text="Delete Leg" ToolTip="Delete Existing Leg" OnClick="btnDeleteLeg_Click" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgLegs" runat="server" AllowSorting="true" Visible="true" OnNeedDataSource="Legs_BindData"
                            OnItemCommand="Legs_ItemCommand" OnDeleteCommand="Legs_DeleteCommand" AutoGenerateColumns="false"
                            OnSelectedIndexChanged="Legs_SelectedIndexChanged" AllowPaging="false" AllowFilteringByColumn="false"
                            PagerStyle-AlwaysVisible="true" Width="710px" OnItemDataBound="Legs_ItemDataBound">
                            <MasterTableView DataKeyNames="LegNum" CommandItemDisplay="Bottom" AllowPaging="false"
                                AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" ShowFilterIcon="false"
                                        HeaderStyle-Width="40px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Departs" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                        UniqueName="DepartAir">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDepartAir" runat="server" Text='<%# Eval("DepartAir") %>'
                                                CssClass="tdtext100" CausesValidation="false" OnClientClick='<%#Eval("DepartAirportID", "return OpenDepartAirportCatalog(\"{0}\")")%>'>                                               
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Arrives" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                        UniqueName="ArrivalAir">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkArrivalAir" runat="server" Text='<%# Eval("ArrivalAir") %>'
                                                CssClass="tdtext100" CausesValidation="false" OnClientClick='<%#Eval("ArrivalAirportID", "return   OpenArrivalAirportCatalog(\"{0}\")")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="ETE" HeaderText="ETE" HeaderStyle-Width="50px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Depart Date" UniqueName="DepartDate"
                                        HeaderStyle-Width="115px" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TA" HeaderText="TA" HeaderStyle-Width="40px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrive Date" HeaderStyle-Width="115px"
                                        UniqueName="ArrivalDate" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="POS" HeaderText="POS" HeaderStyle-Width="60px">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn DataField="Tax" HeaderText="Tax" HeaderStyle-Width="40px">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="TaxRate" HeaderText="Tax Rate" HeaderStyle-Width="80px">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="Rate" HeaderText="Rate" HeaderStyle-Width="50px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>--%>
                                    <%-- <telerik:GridTemplateColumn HeaderText="Tax Rate" ItemStyle-CssClass="fc_textbox"
                                        HeaderStyle-Width="100px" UniqueName="TaxRate" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbTaxRate" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("TaxRate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Rate" ItemStyle-CssClass="fc_textbox_130"
                                        HeaderStyle-Width="100px" UniqueName="Rate" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbRate" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("Rate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Charges" ItemStyle-CssClass="fc_textbox_130"
                                        HeaderStyle-Width="100px" UniqueName="Charges" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbCharges" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("Charges") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%-- <telerik:GridBoundColumn DataField="Charges" HeaderText="Charges" HeaderStyle-Width="60px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="RON" HeaderText="RON" HeaderStyle-Width="50px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DayRoom" HeaderText="DayRoom" HeaderStyle-Width="70px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaxTotal" HeaderText="PaxTotal" HeaderStyle-Width="70px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CD" HeaderText="CD" HeaderStyle-Width="30px"
                                        ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Distance" HeaderText="Distance" ShowFilterIcon="false"
                                        HeaderStyle-Width="70px">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemTemplate>
                                    <div class="grid_icon">
                                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add Leg" CssClass="add-icon-grid"
                                            CommandName="InitInsert" Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                            ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit" Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkInsert" runat="server" ToolTip="Insert Leg" CssClass="insert-icon-grid"
                                            CommandName="PerformInsert" Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDelete();"
                                            CommandName="DeleteSelected" CssClass="delete-icon-grid" Visible="false" ToolTip="Delete Leg"></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                    </div>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlTrip" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server" Width="712px" OnClientItemExpand="pnlTripExpand" OnClientItemCollapse="pnlTripCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Leg Details" Value="pnlItemTrip">
                                    <ContentTemplate>
                                        <div class="CharterQuote-boxsc" style="float: left;">
                                            <table width="100%">
                                                <tr>
                                                    <td width="80%">
                                                        <table>
                                                            <tr>
                                                                <td class="tdLabel100">
                                                                    <asp:Label ID="lbDepartIcao" runat="server" Font-Bold="true" Text="Departure ICAO"></asp:Label>
                                                                </td>
                                                                <td class="tdLabel212">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbDepart" runat="server" CssClass="text60" MaxLength="4" OnTextChanged="tbDepart_TextChanged"
                                                                                    AutoPostBack="true">
                                                                                </asp:TextBox>
                                                                                <asp:HiddenField ID="hdnDepart" runat="server" />
                                                                                <asp:HiddenField ID="hdnAircraft" runat="server" />
                                                                                <asp:HiddenField ID="hdnHomebaseAirport" runat="server" />
                                                                                <asp:HiddenField ID="hdnCountryID" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button runat="server" ID="btnClosestIcao" CssClass="browse-button" OnClientClick="javascript:openWinAirport('DEPART');return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel85">
                                                                    Rate
                                                                </td>
                                                                <td class="pr_radtextbox_100">
                                                                    <telerik:RadNumericTextBox ID="tbRate" runat="server" Type="Currency" Culture="en-US"
                                                                        MaxLength="12" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left">
                                                                    </telerik:RadNumericTextBox>
                                                                    <%-- <asp:TextBox ID="tbRate" MaxLength="12" CssClass="text112" runat="server"></asp:TextBox>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="4">
                                                                    <asp:Label ID="lbDepart" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                    <asp:Label ID="lbcvDepart" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbArrive" runat="server" Font-Bold="true" Text="Arrival ICAO"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="tbArrival" AutoPostBack="true" runat="server" MaxLength="4" CssClass="text60"
                                                                                    OnTextChanged="tbArrive_TextChanged">
                                                                                </asp:TextBox>
                                                                                <asp:HiddenField ID="hdnArrival" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button runat="server" ID="btnArrival" CssClass="browse-button" OnClientClick="javascript:openWinAirport('ARRIVAL');return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    Charges
                                                                </td>
                                                                <td class="pr_radtextbox_100">
                                                                    <%-- <asp:TextBox ID="tbCharges" MaxLength="9" onKeyPress="return fnAllowNumericAndChar(this,event,'-,+,(,),#, ,') "
                                                                        CssClass="text112" runat="server"></asp:TextBox>--%>
                                                                    <telerik:RadNumericTextBox ID="tbCharges" runat="server" Type="Currency" Culture="en-US"
                                                                        MaxLength="9" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left">
                                                                    </telerik:RadNumericTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="4">
                                                                    <asp:Label ID="lbArrival" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                    <asp:Label ID="lbcvArrival" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="20%" valign="top">
                                                        <asp:RadioButtonList ID="rbDomestic" runat="server" OnSelectedIndexChanged="RON_TextChanged"
                                                            AutoPostBack="true">
                                                            <asp:ListItem Value="1" Selected="True">Domestic</asp:ListItem>
                                                            <asp:ListItem Value="2">International</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td width="77%">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel100">
                                                                                <asp:Label ID="lbDepartureDate" runat="server" CssClass="time_anchor_active" Text="Departure"></asp:Label>
                                                                            </td>
                                                                            <td valign="top" class="tdLabel140" align="left">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2" class="mnd_text">
                                                                                            Local
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top" class="tdLabel80">
                                                                                            <asp:TextBox ID="tbLocalDate" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onclick="ShowWarningMessage('date',this,event);" OnTextChanged="tbLocalDate_TextChanged"
                                                                                                AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70"
                                                                                                runat="server" onchange="parseDate(this, event);"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtlocaltime" runat="server" OnTextChanged="tbLocalDate_TextChanged"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" AutoPostBack="true" SelectionOnFocus="SelectAll"
                                                                                                Mask="##:##">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td valign="top" class="tdLabel140">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2">
                                                                                            <b>UTC</b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:TextBox ID="tbUtcDate" CssClass="text70" OnTextChanged="tbUtcDate_TextChanged"
                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onclick="ShowWarningMessage('date',this,event);" onchange="parseDate(this, event); "
                                                                                                runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtUtctime" OnTextChanged="tbUtcDate_TextChanged" AutoPostBack="true"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" runat="server" SelectionOnFocus="SelectAll"
                                                                                                Mask="##:##">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td valign="top" class="tdLabel140">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2">
                                                                                            <b>Home</b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:TextBox ID="tbHomeDate" OnTextChanged="tbHomeDate_TextChanged" AutoPostBack="true"
                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onclick="ShowWarningMessage('date',this,event);" CssClass="text70" onchange="parseDate(this, event);"
                                                                                                runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtHomeTime" AutoPostBack="true" OnTextChanged="tbHomeDate_TextChanged"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" runat="server" SelectionOnFocus="SelectAll"
                                                                                                Mask="##:##">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel100">
                                                                                <asp:Label ID="lbArrivalDate" runat="server" CssClass="time_anchor" Text="Arrival"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" class="tdLabel140">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2" class="mnd_text">
                                                                                            Local
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:TextBox ID="tbArrivalDate" OnTextChanged="tbArrivalDate_TextChanged" AutoPostBack="true"
                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onchange="parseDate(this, event);" onclick="ShowWarningMessage('date',this,event);"
                                                                                                runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtArrivalTime" AutoPostBack="true" OnTextChanged="tbArrivalDate_TextChanged"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" runat="server" SelectionOnFocus="SelectAll"
                                                                                                Mask="##:##">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td align="left" valign="top" class="tdLabel140">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2" class="mnd_text">
                                                                                            UTC
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:TextBox ID="tbArrivalUtcDate" OnTextChanged="tbArrivalUtcDate_TextChanged" AutoPostBack="true"
                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                onchange="parseDate(this, event);" onclick="ShowWarningMessage('date',this,event);"
                                                                                                runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtArrivalUtctime" OnTextChanged="tbArrivalUtcDate_TextChanged"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" AutoPostBack="true" runat="server"
                                                                                                SelectionOnFocus="SelectAll" Mask="##:##">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td align="left" valign="top" class="tdLabel140">
                                                                                <table class="charterquote-box" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left" colspan="2" class="mnd_text">
                                                                                            Home
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:TextBox ID="tbArrivalHomeDate" OnTextChanged="tbArrivalHomeDate_TextChanged"
                                                                                                onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" CssClass="text70"
                                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onchange="parseDate(this, event);"
                                                                                                onclick="ShowWarningMessage('date',this,event);" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="cq_leg_radmask">
                                                                                            <telerik:RadMaskedTextBox ID="rmtArrivalHomeTime" OnTextChanged="tbArrivalHomeDate_TextChanged"
                                                                                                onfocus="ShowWarningMessage('time',this,event);" AutoPostBack="true" runat="server"
                                                                                                SelectionOnFocus="SelectAll" Mask="##:##">
                                                                                            </telerik:RadMaskedTextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="23%" valign="top">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:CheckBoxList ID="chkList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkList_SelectedIndexChanged"
                                                                        RepeatDirection="Vertical">
                                                                        <asp:ListItem>POS</asp:ListItem>
                                                                        <asp:ListItem>Taxable</asp:ListItem>
                                                                    </asp:CheckBoxList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    &nbsp;<asp:CheckBox ID="chkEndOfDuty" runat="server" Text="End Of Duty Day" AutoPostBack="true"
                                                                        OnCheckedChanged="EndOfDuty_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel61">
                                                                    Tax Rate
                                                                </td>
                                                                <td>
                                                                    <%-- <telerik:RadNumericTextBox ID="tbTaxRate" runat="server" Type="Currency" Culture="en-US"
                                                                        NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right" ReadOnly="true">
                                                                    </telerik:RadNumericTextBox>--%>
                                                                    <asp:TextBox ID="tbTaxRate" ReadOnly="true" CssClass="text60" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        Purpose
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbPurpose" MaxLength="40" runat="server" CssClass="text420" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                    </td>
                                                    <td class="tdLabel61">
                                                        &nbsp;&nbsp;Pax Total
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbPaxTotal" CssClass="text60" runat="server" MaxLength="3" AutoPostBack="true" onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="tbPaxTotal_TextChanged"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <fieldset>
                                                            <legend>Distance And Time</legend>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    <asp:Label runat="Server" ID="lbMiles" Text="Miles(N)"></asp:Label>
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <asp:TextBox ID="tbMiles" OnTextChanged="tbMiles_TextChanged" MaxLength="5" onfocus="this.select();"
                                                                                        onKeyPress="return fnAllowNumericAndOneDecimal('miles',this, event)" AutoPostBack="true"
                                                                                        CssClass="text60" runat="server"></asp:TextBox>
                                                                                    <asp:HiddenField runat="server" ID="hdnMiles" />
                                                                                </td>
                                                                                <td class="tdLabel60">
                                                                                    T/O Bias
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="tdLabel50">
                                                                                                <!--<OnTextChanged="tbBias_TextChanged"-->
                                                                                                <asp:TextBox ID="tbBias" onfocus="this.select();" AutoPostBack="true" CssClass="text40"
                                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,': .')" OnTextChanged="tbBias_TextChanged"
                                                                                                    onchange="javascript:return CheckTenthorHourMinValuebias(this);" MaxLength="5"
                                                                                                    OnClick="this.select();" runat="server">
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <img class="calc-icon" onclick="javascript:openWin('rdTOBias');return false;" alt="Display Airport or Aircraft Bias" />
                                                                                                <%--                                                                                                <asp:ImageButton ID="imgbtnTOBias" ToolTip="Display Airport or Aircraft Bias" runat="server"
                                                                                                    OnClientClick="javascript:openWin('rdTOBias');return false;" CssClass="calc-icon"
                                                                                                    TabIndex="9999" />--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                    TAS
                                                                                </td>
                                                                                <td class="tdLabel70">
                                                                                    <asp:TextBox ID="tbTAS" OnTextChanged="tbBias_TextChanged" onfocus="this.select();"
                                                                                        MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)" AutoPostBack="true"
                                                                                        CssClass="text60" runat="server"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    Power Setting
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="tbPower" CssClass="text60" MaxLength="1" runat="server" AutoPostBack="true"
                                                                                                    onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdPower" runat="server" />
                                                                                                <asp:Button runat="server" ToolTip="Display Aircraft Power Settings" ID="btnPower"
                                                                                                    CssClass="browse-button" OnClientClick="javascript:openWin('rdPowerSetting');return false;" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td class="tdLabel60">
                                                                                    Land Bias
                                                                                </td>
                                                                                <td class="tdLabel100">
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel50">
                                                                                                <!--OnTextChanged="tbBias_TextChanged"-->
                                                                                                <asp:TextBox ID="tbLandBias" AutoPostBack="true" CssClass="text40" onfocus="this.select();"
                                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,': .')" onchange="javascript:return CheckTenthorHourMinValueLandbias(this);"
                                                                                                    OnTextChanged="tbBias_TextChanged" MaxLength="5" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <img class="calc-icon" onclick="javascript:openWin('rdLandBias');return false;" alt="Display Airport or Aircraft Bias" />
                                                                                                <%--                                                                                                <asp:ImageButton ID="imgbtnLandingBias" ToolTip="Display Airport or Aircraft Bias"
                                                                                                    runat="server" OnClientClick="javascript:openWin('rdTOBias');return false;" CssClass="calc-icon"
                                                                                                    TabIndex="9998" />--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td class="tdLabel80">
                                                                                    Wind Factor
                                                                                </td>
                                                                                <td class="tdLabel90">
                                                                                    <asp:TextBox ID="tbWind" OnTextChanged="tbWind_TextChanged" onfocus="this.select();"
                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'-')" onchange="javascript:return  checkNegativeValue(this);"
                                                                                        MaxLength="5" AutoPostBack="true" CssClass="text60" runat="server"></asp:TextBox>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td align="left" class="tdLabel50">
                                                                                                ETE
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <!--OnTextChanged="tbETE_TextChanged"-->
                                                                                                <asp:TextBox ID="tbETE" AutoPostBack="true" CssClass="text40" onfocus="this.select();"
                                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,': .')" onchange="javascript:return CheckTenthorHourMinValueEte(this);"
                                                                                                    OnTextChanged="tbETE_TextChanged" MaxLength="5" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lbcvPower" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td align="left" class="tdLabel90">
                                                                                    Wind Reliability
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:RadioButtonList ID="rblistWindReliability" OnSelectedIndexChanged="rblistWindReliability_SelectedIndexChanged"
                                                                                        AutoPostBack="true" runat="server" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="1">50%</asp:ListItem>
                                                                                        <asp:ListItem Value="2">75%</asp:ListItem>
                                                                                        <asp:ListItem Value="3" Selected="True">85%</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 500px;">
                                                        <fieldset>
                                                            <legend>Duty</legend>
                                                            <table>
                                                                <tr>
                                                                    <td width="60%" valign="top">
                                                                        <table>
                                                                            <tr>
                                                                                <td valign="top" class="tdLabel70">
                                                                                    <asp:Label ID="lbCrewRulesCaption" runat="server" Text="Crew Rules"></asp:Label>
                                                                                </td>
                                                                                <td valign="top" class="tdLabel150" colspan="2">
                                                                                    <asp:TextBox ID="tbCrewRules" runat="server" CssClass="text90" MaxLength="4" OnTextChanged="tbCrewRules_TextChanged"
                                                                                        AutoPostBack="true" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnCrewRules" runat="server" />
                                                                                    <asp:HiddenField ID="hdnDutyDayStart" runat="server" />
                                                                                    <asp:HiddenField ID="hdnDutyDayEnd" runat="server" />
                                                                                    <asp:HiddenField ID="hdnMaxDutyHrsAllowed" runat="server" />
                                                                                    <asp:HiddenField ID="hdnMaxFlightHrsAllowed" runat="server" />
                                                                                    <asp:HiddenField ID="hdnMinFixedRest" runat="server" />
                                                                                    <asp:Button runat="server" ID="btnCrewRules" CssClass="browse-button" OnClientClick="javascript:openWin('rdCrewRules');return false;" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <asp:Label ID="lbCrewRules" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                    <asp:Label ID="lbcvCrewRules" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    Override
                                                                                </td>
                                                                                <td valign="top">
                                                                                    <asp:TextBox ID="tbOverride" AutoPostBack="true" runat="server" MaxLength="4" CssClass="text60"
                                                                                        onchange="javascript:return checknumericValue(this);" Text="0.0" OnTextChanged="tbOverride_TextChanged">
                                                                                    </asp:TextBox>
                                                                                </td>
                                                                                <td valign="top">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="40%" valign="top">
                                                                        <div class="chtrquote-box-noedit" style="float: right;">
                                                                            <table>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        FAR
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:Label ID="lbFar" runat="server" Visible="true"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        Total Flight
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:Label ID="lbTotalFlight" MaxLength="5" runat="server" Text="0.0"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        Total Duty
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:Label ID="lbTotalDuty" MaxLength="5" runat="server" Text="0.0"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        Rest
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:Label ID="lbRest" MaxLength="5" runat="server" Text="0.0"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 200px;">
                                                        <fieldset>
                                                            <legend>RON</legend>
                                                            <table>
                                                                <tr>
                                                                    <td valign="top" class="tdLabel90">
                                                                        RON
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbRON" runat="server" CssClass="text60" MaxLength="4" AutoPostBack="true"
                                                                            onfocus="ShowWarningMessage('ron',this,event);" onKeyPress="return fnAllowNumeric(this, event)"
                                                                            OnTextChanged="RON_TextChanged"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        Day Room
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbDayRoom" AutoPostBack="true" runat="server" MaxLength="2" CssClass="text60"
                                                                            onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="RON_TextChanged">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <div style="height: 29px;">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td>
                        <telerik:RadPanelBar ID="pnlQuoteDetails" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                            runat="server" Width="712px" OnClientItemExpand="pnlQuoteDetailsExpand" OnClientItemCollapse="pnlQuoteDetailsCollapse">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Quote Details" Value="pnlItemTrip">
                                    <ContentTemplate>
                                        <div class="CharterQuote-boxsc">
                                            <div style="padding: 5px 0px 10px 0px;">
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td class="tdLabel120">
                                                                <asp:Label ID="lbDomStdCrew" runat="server" Text="Dom. std. Crew"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <asp:TextBox ID="tbDomStdCrew" runat="server" CssClass="text220" MaxLength="2" AutoPostBack="true"
                                                                    onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="DomStdCrew_TextChanged"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel120">
                                                                <asp:Label ID="lbIntlStdCrew" runat="server" Text="Intl std. Crew"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbIntlStdCrew" runat="server" CssClass="text180" MaxLength="2" AutoPostBack="true"
                                                                    onKeyPress="return fnAllowNumeric(this, event)" OnTextChanged="IntlStdCrew_TextChanged"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel120">
                                                                <asp:Label ID="lbTotFixCost" runat="server" Text="Total Fixed Cost"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="pr_radtextbox_200">
                                                                            <telerik:RadNumericTextBox ID="tbTotalCost" runat="server" Type="Currency" Culture="en-US"
                                                                                MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"  CssClass="text200"
                                                                                AutoPostBack="true" OnTextChanged="TotalCost_TextChanged">
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btnTotalCost" ToolTip="Override Default Value" CssClass="reset-icon"
                                                                                runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="TotalCost" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel120">
                                                                <asp:Label ID="lbTotPrepDeposit" runat="server" Text="Tot Prepaid/Deposit"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_200">
                                                                <telerik:RadNumericTextBox ID="tbTotPrepDeposit" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left" CssClass="text180"
                                                                    AutoPostBack="true" OnTextChanged="TotPrepDeposit_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <%--<asp:TextBox ID="tbTotPrepDeposit" runat="server" CssClass="text50" MaxLength="19"
                                                                    AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                    OnTextChanged="TotPrepDeposit_TextChanged"></asp:TextBox>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="padding: 0px 0px 5px 0px;">
                                                    <fieldset>
                                                        <legend>Standard Charges and Additional Fees</legend>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="tdLabel60">
                                                                                <asp:Label ID="lbFeeGroup" runat="server" Text="Fee Group"></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:TextBox ID="tbFeeGroup" runat="server" CssClass="text60" MaxLength="10" AutoPostBack="true"
                                                                                    OnTextChanged="FeeGroup_TextChanged"></asp:TextBox>
                                                                                <asp:Button ID="btnFeeGroup" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('radFeeGroupPopup');return false;" />
                                                                                <asp:HiddenField ID="hdnFeeGroupID" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:Label ID="lbFeeGroupDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                <asp:Label ID="lbFeeGroupMsg" CssClass="alert-text" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:CheckBox ID="chkTaxDaily" runat="server" Text="Tax Daily Usage Adj" OnCheckedChanged="TaxDaily_CheckedChanged"
                                                                        AutoPostBack="true" Width="200" />
                                                                    <asp:CheckBox ID="chkTaxLandingFee" runat="server" Text="Tax Landing Fee" OnCheckedChanged="TaxLandingFee_CheckedChanged"
                                                                        AutoPostBack="true" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </div>
                                                <div style="overflow: auto;">
                                                    <div id="tooltipFixedChargeUnit" class="tooltipCss" style="display: none;color:red;font-weight: bold;width:360px;border-color:gray">Fixed charge unit calculates Quantity multiplied by Dom Sell price.</div>
                                                    <div style="width: 1000px;">
                                                    <fieldset>
                                                        <legend>Services</legend>
                                                        <telerik:RadGrid ID="dgStandardCharges" runat="server" AllowSorting="true" Visible="true"
                                                            AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false" PageSize="500"
                                                            OnItemCommand="StandardCharges_ItemCommand" OnItemDataBound="StandardCharges_ItemDataBound">
                                                            <MasterTableView CommandItemDisplay="Bottom" AllowFilteringByColumn="false" DataKeyNames="CQFleetChargeDetailID,CQMainID,OrderNUM,NegotiatedChgUnit,IsTaxDOM,IsDiscountDOM,IsTaxIntl,IsDiscountIntl,Quantity">
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn HeaderText="Select" CurrentFilterFunction="Contains"
                                                                        FilterDelay="4000" ShowFilterIcon="false" UniqueName="Select" AllowFiltering="false"
                                                                        HeaderStyle-Width="60px">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Description" UniqueName="Description">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbDescription" runat="server" Text='<%#Eval("CQFlightChargeDescription")%>'
                                                                                CssClass="text120" MaxLength="40" OnTextChanged="Description_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn UniqueName="ChargeUnit" DataField="NegotiatedChgUnit"
                                                                        HeaderText="Charge Unit">
                                                                        <ItemTemplate>
                                                                            <div id="FixedTooltipDisabled" style="position: absolute;width: 120px;background-color:transparent;height:18px">
                                                                             </div>
                                                                            <asp:DropDownList CssClass="text120 JqFixedTooltip" Font-Size="XX-Small" ID="ddlChargeUnit" runat="server"
                                                                                OnSelectedIndexChanged="ChargeUnit_SelectedIndexChanged" AutoPostBack="true">
                                                                                <asp:ListItem Text="Per Flight Hour" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Per Nautical Mile" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="Per Statute Mile" Value="3"></asp:ListItem>
                                                                                <asp:ListItem Text="% of Daily Usage" Value="4"></asp:ListItem>
                                                                                <asp:ListItem Text="Fixed" Value="5"></asp:ListItem>
                                                                                <asp:ListItem Text="Per Hour" Value="6"></asp:ListItem>
                                                                                <asp:ListItem Text="Per Leg" Value="7"></asp:ListItem>
                                                                                <asp:ListItem Text="Kilometer" Value="8"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Quantity" UniqueName="Quantity">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbQuantity" runat="server" Text='<%#Eval("Quantity")%>' MaxLength="9"
                                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'.')" OnTextChanged="Quantity_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Dom. Fixed Costs/Buy" UniqueName="BuyDOM">
                                                                        <ItemTemplate>
                                                                            <%-- <asp:TextBox ID="tbBuyDOM" runat="server" Text='<%#Eval("BuyDOM")%>' CssClass="text40"
                                                                                MaxLength="17" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" OnTextChanged="BuyDOM_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>--%>
                                                                            <telerik:RadNumericTextBox ID="tbBuyDOM" runat="server" Type="Currency" Culture="en-US"
                                                                                DbValue='<%# Bind("BuyDOM") %>' MaxLength="17" NumberFormat-DecimalSeparator="."
                                                                                EnabledStyle-HorizontalAlign="Left" OnTextChanged="BuyDOM_TextChanged" AutoPostBack="true">
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <%-- <telerik:GridTemplateColumn HeaderText="Dom. Sell" UniqueName="SellDOM">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbSellDOM" runat="server" Text='<%#Eval("SellDOM")%>' CssClass="text40"
                                                                                MaxLength="17" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" OnTextChanged="SellDOM_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>--%>
                                                                    <telerik:GridTemplateColumn HeaderText="Dom. Sell" FilterDelay="4000" UniqueName="SellDOM"
                                                                        CurrentFilterFunction="Contains">
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="tbSellDOM" runat="server" Type="Currency" Culture="en-US"
                                                                                DbValue='<%# Bind("SellDOM") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                MaxLength="17" OnTextChanged="SellDOM_TextChanged" AutoPostBack="true">
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Tax" UniqueName="IsTaxDOM">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkTaxDOM" runat="server" OnCheckedChanged="TaxDOM_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Disc" UniqueName="IsDiscountDOM">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkDiscountDOM" runat="server" OnCheckedChanged="DiscountDOM_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Intl. Fixed Costs/Buy" UniqueName="BuyIntl">
                                                                        <ItemTemplate>
                                                                            <%-- <asp:TextBox ID="tbBuyIntl" runat="server" Text='<%#Eval("BuyIntl")%>' CssClass="text40"
                                                                                MaxLength="17" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" OnTextChanged="BuyIntl_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>--%>
                                                                            <telerik:RadNumericTextBox ID="tbBuyIntl" runat="server" Type="Currency" Culture="en-US"
                                                                                DbValue='<%# Bind("BuyIntl") %>' MaxLength="17" NumberFormat-DecimalSeparator="."
                                                                                EnabledStyle-HorizontalAlign="Left" OnTextChanged="BuyIntl_TextChanged" AutoPostBack="true">
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <%-- <telerik:GridTemplateColumn HeaderText="Intl. Sell" UniqueName="SellIntl">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="tbSellIntl" runat="server" Text='<%#Eval("SellIntl")%>' CssClass="text40"
                                                                                MaxLength="17" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" OnTextChanged="SellIntl_TextChanged"
                                                                                AutoPostBack="true"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>--%>
                                                                    <telerik:GridTemplateColumn HeaderText="Intl. Sell" FilterDelay="4000" UniqueName="SellIntl"
                                                                        CurrentFilterFunction="Contains">
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="tbSellIntl" runat="server" Type="Currency" Culture="en-US"
                                                                                DbValue='<%# Bind("SellIntl") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                                MaxLength="17" OnTextChanged="SellIntl_TextChanged" AutoPostBack="true">
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Tax" UniqueName="IsTaxIntl">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkTaxIntl" runat="server" OnCheckedChanged="TaxIntl_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="Disc" UniqueName="IsDiscountIntl">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkDiscountIntl" runat="server" OnCheckedChanged="DiscountIntl_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <%-- <telerik:GridBoundColumn DataField="FeeAMT" HeaderText="Fee Amount" ShowFilterIcon="false">
                                                                    </telerik:GridBoundColumn>--%>
                                                                    <telerik:GridTemplateColumn HeaderText="Fee Amount" UniqueName="FeeAMT" ItemStyle-CssClass="fc_textbox">
                                                                        <ItemTemplate>
                                                                            <telerik:RadNumericTextBox ID="tbFeeAMT" runat="server" Type="Currency" Culture="en-US"
                                                                                DbValue='<%# Bind("FeeAMT") %>' MaxLength="17" NumberFormat-DecimalSeparator="."
                                                                                EnabledStyle-HorizontalAlign="Left" ReadOnly="true">
                                                                            </telerik:RadNumericTextBox>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                                                <CommandItemTemplate>
                                                                    <div class="grid_icon">
                                                                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                                                            CommandName="InitInsert"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteSelected" CssClass="delete-icon-grid"
                                                                            OnClientClick="javascript:return fnDeleteFleetCharge();" ToolTip="Delete"></asp:LinkButton>
                                                                    </div>
                                                                </CommandItemTemplate>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="false">
                                                                <Scrolling AllowScroll="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="padding: 5px 3px 0px 0px;">
                                                <fieldset>
                                                    <legend>Charges</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbTotDays" runat="server" Text="Total Days"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel230">
                                                                <asp:TextBox ID="tbTotDays" runat="server" ReadOnly="true" CssClass="text172 charter_readonly_textbox"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbflightHrs" runat="server" Text="Flight Hours"></asp:Label>
                                                            </td>
                                                            <td class="tdLabel230">
                                                                <asp:TextBox ID="tbFltHrs" runat="server" ReadOnly="true" CssClass="text172 charter_readonly_textbox"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbAvgFlightHrs" runat="server" Text="Avg Flt Hrs/Days"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbAvgHrs" runat="server" ReadOnly="true" CssClass="text172 charter_readonly_textbox"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbAdjustedHours" runat="server" Text="Adjusted Hours"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbAdjustedHrs" runat="server" ReadOnly="true" CssClass="text172 charter_readonly_textbox"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbUsageAdj" runat="server" Text="Daily Usage Adj"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <telerik:RadNumericTextBox ID="tbUsageAdj" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    AutoPostBack="true" OnTextChanged="UsageAdj_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <%--<asp:TextBox ID="tbUsageAdj" runat="server" MaxLength="19" AutoPostBack="true" OnTextChanged="UsageAdj_TextChanged"
                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                                <asp:Button ID="btnUsageAdj" ToolTip="Override Default Value" CssClass="reset-icon" style="float: right;"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="UsageAdj" />
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbSegmentFees" runat="server" Text="Segment Fees"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <telerik:RadNumericTextBox ID="tbSegmentFees" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    AutoPostBack="true" OnTextChanged="SegmentFees_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <%-- <asp:TextBox ID="tbSegmentFees" runat="server" MaxLength="19" AutoPostBack="true"
                                                                    OnTextChanged="SegmentFees_TextChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                                <asp:Button ID="btnSegmentFees" ToolTip="Override Default Value" CssClass="reset-icon" style="float: right;"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="SegmentFees" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbLandingFees" runat="server" Text="Landing Fees"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <telerik:RadNumericTextBox ID="tbLandingFees" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    AutoPostBack="true" OnTextChanged="LandingFees_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <%-- <asp:TextBox ID="tbLandingFees" runat="server" MaxLength="19" AutoPostBack="true"
                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')" OnTextChanged="LandingFees_TextChanged"></asp:TextBox>--%>
                                                                <asp:Button ID="btnLandingFees" ToolTip="Override Default Value" CssClass="reset-icon" style="float: right;"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="LandingFees" />
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbAdditionalFees" runat="server" Text="Additional Fees"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <%-- <asp:TextBox ID="tbAdditionalFees" runat="server" ReadOnly="true" CssClass="text80 charter_readonly_textbox"></asp:TextBox>--%>
                                                                <telerik:RadNumericTextBox ID="tbAdditionalFees" runat="server" Type="Currency" Culture="en-US"
                                                                    ReadOnly="true" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    CssClass="text180 charter_readonly_textbox">
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbFlightCharges" runat="server" Text="Flight Charges"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <%--<asp:TextBox ID="tbFlightCharges" runat="server" ReadOnly="true" CssClass="text80 charter_readonly_textbox"></asp:TextBox>--%>
                                                                <telerik:RadNumericTextBox ID="tbFlightCharges" runat="server" Type="Currency" Culture="en-US"
                                                                    ReadOnly="true" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    CssClass="text200 charter_readonly_textbox">
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbSubTotal" runat="server" Text="Sub Total"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <%-- <asp:TextBox ID="tbSubTotal" runat="server" ReadOnly="true" CssClass="text80 charter_readonly_textbox"></asp:TextBox>--%>
                                                                <telerik:RadNumericTextBox ID="tbSubTotal" runat="server" Type="Currency" Culture="en-US"
                                                                    ToolTip="This value includes STD Crew RON Charge" ReadOnly="true" NumberFormat-DecimalSeparator="."
                                                                    EnabledStyle-HorizontalAlign="Left" CssClass="text180 charter_readonly_textbox">
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbDiscount" runat="server" Text="Discount (%)"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <%-- <telerik:RadNumericTextBox ID="tbDiscount" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="5" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    AutoPostBack="true" OnTextChanged="Discount_TextChanged">
                                                                </telerik:RadNumericTextBox>--%>
                                                                <asp:TextBox ID="tbDiscount" runat="server" MaxLength="5" AutoPostBack="true" OnTextChanged="Discount_TextChanged"
                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                                <asp:Button ID="btnDiscount" ToolTip="Override Default Value" CssClass="reset-icon text180" style="float: right;"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="Discount" />
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbDiscountAmount" runat="server" Text="Discount Amount"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <telerik:RadNumericTextBox ID="tbDiscountAmount" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    AutoPostBack="true" OnTextChanged="DiscountAmount_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <%--<asp:TextBox ID="tbDiscountAmount" runat="server" MaxLength="19" AutoPostBack="true"
                                                                    OnTextChanged="DiscountAmount_TextChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                                <asp:Button ID="btnDiscountAmount" ToolTip="Override Default Value" CssClass="reset-icon" style="float: right"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="DiscountAmount" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbTaxes" runat="server" Text="Taxes"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <%-- <asp:TextBox ID="tbTaxes" runat="server" MaxLength="19" AutoPostBack="true" OnTextChanged="Taxes_TextChanged"
                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                                <telerik:RadNumericTextBox ID="tbTaxes" runat="server" Type="Currency" Culture="en-US"
                                                                    MaxLength="19" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    AutoPostBack="true" OnTextChanged="Taxes_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:Button ID="btnTaxes" ToolTip="Override Default Value" CssClass="reset-icon" style="float: right;"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="Taxes" />
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbMargin" runat="server" Text="Margin (%)"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbMargin" runat="server" ReadOnly="true" CssClass="text172 charter_readonly_textbox"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel100">
                                                                <asp:Label ID="lbMarginAmount" runat="server" Text="Margin Amount"></asp:Label>
                                                            </td>
                                                            <td  class="pr_radtextbox_180">
                                                                <telerik:RadNumericTextBox ID="tbMarginAmount" runat="server" Type="Currency" Culture="en-US"
                                                                    ReadOnly="true" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                    CssClass="text180 charter_readonly_textbox">
                                                                </telerik:RadNumericTextBox>
                                                                <%--<asp:TextBox ID="tbMarginAmount" runat="server" ReadOnly="true" CssClass="text80 charter_readonly_textbox"></asp:TextBox>--%>
                                                            </td>
                                                            <td class="tdLabel200">
                                                                <asp:Label ID="lbTotQuote" runat="server" Text="Total Quote"></asp:Label>
                                                            </td>
                                                            <td class="pr_radtextbox_180">
                                                                <%-- <asp:TextBox ID="tbTotalQuote" runat="server" MaxLength="19" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                    AutoPostBack="true" OnTextChanged="TotalQuote_TextChanged"></asp:TextBox>--%>
                                                                <telerik:RadNumericTextBox ID="tbTotalQuote" MaxLength="19" runat="server" Type="Currency"
                                                                    Culture="en-US" AutoPostBack="true" OnTextChanged="TotalQuote_TextChanged" NumberFormat-DecimalSeparator="."
                                                                    EnabledStyle-HorizontalAlign="Left">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:Button ID="btnTotalQuote" ToolTip="Override Default Value" CssClass="reset-icon" style="float: right;"
                                                                    runat="server" Text="Override Default Value" OnCommand="Override_Command" CommandName="TotalQuote" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="height: 20px;" runat="server">
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete File" CausesValidation="false"
                            OnClick="Delete_Click" />
                        <asp:Button ID="btnEditFile" runat="server" CssClass="button" Text="Edit File" OnClick="Edit_Click" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                            OnClick="Cancel_Click" />
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                            OnClick="Save_Click" TabIndex="0" />
                        <asp:Button ID="btnNext" runat="server" CssClass="button" Text="Next >" CausesValidation="false"
                            OnClick="Next_Click" TabIndex="1" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnConfirmFeeGroupYes" runat="server" Text="Button" OnClick="ConfirmFeeGroupYes_Click" />
                    <asp:Button ID="btndeletelegYes" runat="server" Text="Button" OnClick="btndeletelegYes_Click" />
                    <asp:Button ID="btndeletelegNo" runat="server" Text="Button" OnClick="btndeletelegNo_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
