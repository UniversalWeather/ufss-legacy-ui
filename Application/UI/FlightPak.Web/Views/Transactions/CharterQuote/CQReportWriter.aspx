﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CharterQuote.Master"
    AutoEventWireup="true" CodeBehind="CQReportWriter.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.CQReportWriter" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CharterQuoteHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CharterQuoteBodyContent" runat="server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Visible="true" runat="server"
        Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openReport(radWin) {
                var url = '';
                //CQ Report Writer
                if (radWin == "AirProfile") {
                    url = '../../Reports/CQReportViewerModule.aspx?xmlFilename=CQAircraftProfile.xml';
                }
                else if (radWin == "Itinerary") {
                    url = '../../Reports/CQReportViewerModule.aspx?xmlFilename=CQItinerary.xml';
                }
                else if (radWin == "NotesAlerts") {
                    url = '../../Reports/CQReportViewerModule.aspx?xmlFilename=CQNotesAlerts.xml';
                }
                else if (radWin == "PassProfile") {
                    url = '../../Reports/CQReportViewerModule.aspx?xmlFilename=CQPaxProfile.xml';
                }
                else if (radWin == "QuoteLog") {
                    url = '../../Reports/CQReportViewerModule.aspx?xmlFilename=CQQuoteLog.xml';
                }
                else if (radWin == "TripStatus") {
                    url = '../../Reports/CQReportViewerModule.aspx?xmlFilename=CQTripStatus.xml';
                }
                var oWnd = radopen(url, "RadCQReportsPopup");
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCQReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/CharterQuote/CQReportWriter.aspx">
            </telerik:RadWindow>                        
        </Windows>             
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAirProfile" runat="server" Text="Aircraft Profile" OnClientClick="javascript:openReport('AirProfile');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkItinerary" runat="server" Text="Itinerary" OnClientClick="javascript:openReport('Itinerary');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkNotesAlerts" runat="server" Text="Notes/Alerts" OnClientClick="javascript:openReport('NotesAlerts');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassProfile" runat="server" Text="Passenger Profile" OnClientClick="javascript:openReport('PassProfile');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkQuoteLog" runat="server" Text="Quote Log" OnClientClick="javascript:openReport('QuoteLog');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkTripStatus" runat="server" Text="Trip Status" OnClientClick="javascript:openReport('TripStatus');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
