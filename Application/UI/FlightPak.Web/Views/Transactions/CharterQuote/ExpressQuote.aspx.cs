﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.CalculationService;
using FlightPak.Web.Framework.Masters;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class ExpressQuote : BaseSecuredPage
    {
        #region Declarations
        public string EQSessionKey = "EQFILE";          // Maintain whole FILE 
        public string EQException = "EQEXCEPTION";      // Maintain Session Variable for CQ Exception List
        public string EQExceptionTempatelist = "EQExceptionTemplateList"; //All ExceptionTemplate list
        private ExceptionManager exManager;
        List<CQException> ExceptionList = new List<CQException>();
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress;
        private bool selectchangefired = false;
        private delegate void SaveSession();
        public event AjaxRequestHandler RadAjax_AjaxRequest;
        private Delegate _SaveToSession;
        public bool NoRequiredField = true;
        public bool ValidateFile = true;
        public bool IsDepartRural = false;
        public bool IsArriveRural = false;
        public Delegate SaveToSession
        {
            set { _SaveToSession = value; }
        }
        const decimal statuateConvFactor = 1.1508M;
        public enum Legstate
        {
            Add = 1,
            Insert = 2,
            Delete = 3
        }
        #endregion

        /// <summary>
        /// Wire up the SaveCharterQuoteToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveCharterQuoteFileSession = new SaveSession(SaveCharterQuoteToSession);
                        SaveToSession = SaveCharterQuoteFileSession;

                        if (Session["SearchItemPrimaryKeyValue"] != null)
                        {
                            using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                            {
                                long FileID = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"].ToString());
                                var objRetVal = CqSvc.GetCQRequestByID(FileID, true);
                                if (objRetVal.ReturnFlag)
                                {
                                    CQFile FileReq = new CQFile();
                                    FileReq = objRetVal.EntityList[0];
                                    //SetTripModeToNoChange(ref Trip);
                                    FileReq.Mode = CQRequestActionMode.NoChange;
                                    FileReq.State = CQRequestEntityState.NoChange;
                                    Session["EQFILE"] = FileReq;

                                    Session.Remove("EQEXCEPTION");
                                    if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                    {
                                        var objRetval = CqSvc.GetQuoteExceptionList(FileReq.CQMains[0].CQMainID);
                                        if (objRetval.ReturnFlag)
                                        {
                                            Session["EQEXCEPTION"] = objRetval.EntityList;
                                        }
                                    }
                                }
                            }
                            Session.Remove("SearchItemPrimaryKeyValue");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgLegs.ClientID));
                        ResetHomebaseSettings();
                        setMaxlenthofHours();
                        if (Session[EQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[EQSessionKey];
                            if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                            {
                                Int64 QuoteNumInProgress = 1;
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                                FileRequest.HomeBaseAirportID = UserPrincipal.Identity._airportId;
                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                {
                                    hdnHomebaseAirport.Value = FileRequest.HomeBaseAirportID.ToString();
                                }
                                FileRequest.HomebaseID = UserPrincipal.Identity._fpSettings._HomebaseID;
                            }
                        }
                        else
                        {
                            EnableForm(false);
                        }
                        //Company Profile Settings
                        if (UserPrincipal.Identity._fpSettings._IsQuoteWarningMessage != null && (bool)UserPrincipal.Identity._fpSettings._IsQuoteWarningMessage)
                            hdnCompSettingforshowWarn.Value = "true";
                        else
                            hdnCompSettingforshowWarn.Value = "false";
                        hdTimeDisplayTenMin.Value = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? "1" : UserPrincipal.Identity._fpSettings._TimeDisplayTenMin.ToString();
                        hdShowRequestor.Value = UserPrincipal.Identity._fpSettings._IsReqOnly.ToString();
                        hdHighlightTailno.Value = UserPrincipal.Identity._fpSettings._IsANotes.ToString();
                        hdIsDepartAuthReq.Value = UserPrincipal.Identity._fpSettings._IsDepartAuthDuringReqSelection.ToString();
                        hdSetWindReliability.Value = (UserPrincipal.Identity._fpSettings._WindReliability == null ? 3 : UserPrincipal.Identity._fpSettings._WindReliability).ToString();
                        hdSetCrewRules.Value = UserPrincipal.Identity._fpSettings._IsAutoCreateCrewAvailInfo.ToString();
                        hdHomeCategory.Value = (UserPrincipal.Identity._fpSettings._DefaultFlightCatID == null ? 0 : UserPrincipal.Identity._fpSettings._DefaultFlightCatID).ToString();
                        //Load US Country for DOM/Internation calculation 
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                            var fleetretval = FleetService.GetFleet();
                            if (fleetretval.ReturnFlag)
                            {
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.FleetID != null && QuoteInProgress.FleetID > 0)
                                    {
                                        GetFleetList = fleetretval.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();
                                        if (GetFleetList != null && GetFleetList.Count > 0)
                                        {
                                            if (QuoteInProgress.Fleet != null)
                                            {
                                                QuoteInProgress.Fleet.MaximumPassenger = GetFleetList[0].MaximumPassenger != null ? GetFleetList[0].MaximumPassenger : 0;
                                                if (QuoteInProgress.CQLegs != null)
                                                {
                                                    foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false))
                                                    {
                                                        //Leg.PassengerTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);
                                                        Int32 SeatTotal = 0;
                                                        SeatTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);
                                                        if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                                            Leg.State = CQRequestEntityState.Modified;
                                                        if (Leg.PassengerTotal != null)
                                                            Leg.ReservationAvailable = SeatTotal - Leg.PassengerTotal;
                                                        else
                                                            Leg.ReservationAvailable = SeatTotal;
                                                    }
                                                }
                                                SaveQuoteinProgressToFileSession();
                                                FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Fleetcompany = new GetAllCompanyMasterbyHomeBaseId();
                                                if (GetFleetList[0].HomebaseID != null)
                                                {
                                                    Fleetcompany = GetCompany((long)GetFleetList[0].HomebaseID);
                                                    FlightPakMasterService.GetAllAirport Fleetcompanyairport = new GetAllAirport();
                                                    if (Fleetcompany != null)
                                                    {
                                                        if (Fleetcompany.HomebaseAirportID != null)
                                                        {
                                                            Fleetcompanyairport = GetAirport((long)Fleetcompany.HomebaseAirportID);
                                                            if (Fleetcompanyairport != null)
                                                                hdnCountryID.Value = Fleetcompanyairport.CountryID.ToString();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!IsPostBack)
                        {
                            setWarnMessageToDefault();

                            hdnLegNum.Value = "1";
                            #region "Authorization"
                            ApplyPermissions();
                            #endregion
                            #region "Kilometer/Miles"
                            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                                lbMiles.Text = System.Web.HttpUtility.HtmlEncode("Kilometers");
                            else
                                lbMiles.Text = System.Web.HttpUtility.HtmlEncode("Miles(N)");
                            #endregion
                            Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                            if (QuoteInProgress != null)
                            {
                                if (QuoteInProgress.CQLegs == null || QuoteInProgress.CQLegs.Count == 0)
                                {
                                    #region "Add New Leg"
                                    QuoteInProgress.CQLegs = new List<CQLeg>();
                                    CQLeg FirstLeg = new CQLeg();
                                    FirstLeg.LegNUM = 1; //Convert.ToInt16(hdnLegNum.Value);
                                    FirstLeg.IsDeleted = false;
                                    FirstLeg.ShowWarningMessage = true;
                                    hdnShowWarnMessage.Value = "true";

                                    if (UserPrincipal.Identity._fpSettings._CQCrewDuty != null)
                                    {
                                        if (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._CQCrewDuty))
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                                var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == UserPrincipal.Identity._fpSettings._CQCrewDuty.Trim().ToUpper()).ToList();
                                                    if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                                    {
                                                        FirstLeg.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                        CharterQuoteService.CrewDutyRule CDrule = new CrewDutyRule();
                                                        CDrule.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                        CDrule.CrewDutyRuleCD = CrewDtyRuleList[0].CrewDutyRuleCD;
                                                        hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                                        CDrule.CrewDutyRulesDescription = CrewDtyRuleList[0].CrewDutyRulesDescription;
                                                        FirstLeg.FedAviationRegNUM = CrewDtyRuleList[0].FedAviatRegNum != null ? CrewDtyRuleList[0].FedAviatRegNum : string.Empty;
                                                        FirstLeg.CrewDutyRule = CDrule;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    hdSetFarRules.Value = (UserPrincipal.Identity._fpSettings._FedAviationRegNum == null ? string.Empty : UserPrincipal.Identity._fpSettings._FedAviationRegNum).ToString();
                                    //hdHomeCategory.Value = (UserPrincipal.Identity._fpSettings._DefaultFlightCatID == null ? 0 : UserPrincipal.Identity._fpSettings._DefaultFlightCatID).ToString();
                                    if (QuoteInProgress.AircraftID != null)
                                        hdnTypeCode.Value = QuoteInProgress.AircraftID.ToString();
                                    Int64 HomebaseID = 0;
                                    #region "Load Homebase Airport to departure"
                                    if (FileRequest.HomebaseID != null && FileRequest.HomebaseID > 0)
                                    {
                                        HomebaseID = (Int64)FileRequest.HomebaseID;
                                        //if (UserPrincipal.Identity._homeBaseId != null)
                                        //    hdnHomebaseAirport.Value = UserPrincipal.Identity._airportId.ToString();
                                        //else
                                        //    hdnHomebaseAirport.Value = null;
                                        FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = GetCompany(HomebaseID);
                                        if (Comp != null)
                                        {
                                            FlightPakMasterService.GetAllAirport HomebaseAirport = GetAirport((long)Comp.HomebaseAirportID);
                                            if (HomebaseAirport != null)
                                            {
                                                CharterQuoteService.Airport DeptAirport = new CharterQuoteService.Airport();
                                                FirstLeg.DAirportID = HomebaseAirport.AirportID;
                                                DeptAirport.IcaoID = HomebaseAirport.IcaoID;
                                                DeptAirport.AirportID = HomebaseAirport.AirportID;
                                                DeptAirport.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                FirstLeg.Airport1 = DeptAirport;
                                                FirstLeg.ArrivalAirportChanged = true;
                                                FirstLeg.DepartAirportChanged = true;
                                            }
                                        }
                                    }
                                    #endregion
                                    #region "Load File. QuoteInProgress date to leg"
                                    if (FileRequest.EstDepartureDT != null)
                                    {
                                        FirstLeg.DepartureDTTMLocal = FileRequest.EstDepartureDT;
                                        if (FirstLeg.DAirportID != null)
                                        {
                                            CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient();
                                            if (UserPrincipal.Identity._fpSettings._IsAutomaticCalcLegTimes != null && (bool)UserPrincipal.Identity._fpSettings._IsAutomaticCalcLegTimes == true)
                                            {
                                                //GMTDept
                                                DateTime ldGmtDep = objDstsvc.GetGMT((long)FirstLeg.DAirportID, (DateTime)FirstLeg.DepartureDTTMLocal, true, false);
                                                FirstLeg.DepartureGreenwichDTTM = ldGmtDep;
                                            }
                                        }
                                    }
                                    #endregion
                                    setAircraftSettings(ref FirstLeg);
                                    FirstLeg.WindReliability = UserPrincipal.Identity._fpSettings._WindReliability == null ? 3 : (int)UserPrincipal.Identity._fpSettings._WindReliability;
                                    if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                        QuoteInProgress.State = CQRequestEntityState.Modified;
                                    QuoteInProgress.CQLegs.Add(FirstLeg);
                                    #endregion
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            BindLegs(dgLegs, false);
                            if (dgLegs.Items.Count > 0)
                            {
                                dgLegs.Items[0].Selected = true;
                                CalculateQuantity();
                                CalculateQuoteTotal();
                                LoadFileDetails();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        private void setMaxlenthofHours()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    tbHrs.MaxLength = 5;
                }
                else
                {
                    tbHrs.MaxLength = 4;
                }
            }
        }
        protected void LoadFileDetails()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgLegs.SelectedItems.Count > 0)
                {
                    ClearFields();
                    ClearLabels();
                    GridDataItem item = (GridDataItem)dgLegs.SelectedItems[0];
                    if (FileRequest != null)
                    {
                        #region "File Details"
                        FileRequest = (CQFile)Session[EQSessionKey];
                        if (!string.IsNullOrEmpty(FileRequest.FileNUM.ToString()))
                            tbFileNumber.Text = FileRequest.FileNUM.ToString();
                        if (FileRequest.CQFileDescription != null && !string.IsNullOrEmpty(FileRequest.CQFileDescription))
                            tbDescription.Text = FileRequest.CQFileDescription.ToString();
                        if (FileRequest.CQCustomer != null)
                        {
                            hdnCustomerID.Value = FileRequest.CQCustomer.CQCustomerID.ToString();
                            tbCustomer.Text = FileRequest.CQCustomer.CQCustomerCD;
                            if (!string.IsNullOrEmpty(FileRequest.CQCustomer.CQCustomerName))
                                tbCustomerName.Text = FileRequest.CQCustomer.CQCustomerName.ToUpper();
                        }
                        if (FileRequest.SalesPerson != null)
                        {
                            hdnSalesPerson.Value = FileRequest.SalesPerson.SalesPersonID.ToString();
                            tbSalesPerson.Text = FileRequest.SalesPerson.SalesPersonCD.ToString();

                            string firstname = string.Empty;
                            string lastname = string.Empty;
                            string middlename = string.Empty;
                            string salesPersonName = string.Empty;
                            string businessemail = string.Empty;
                            string businessphone = string.Empty;
                            hdnFirstName.Value = string.Empty;
                            hdnLastName.Value = string.Empty;
                            hdnMiddleName.Value = string.Empty;

                            if (!string.IsNullOrEmpty(FileRequest.SalesPerson.LastName))
                            {
                                lastname = FileRequest.SalesPerson.LastName.ToString();
                                hdnLastName.Value = FileRequest.SalesPerson.LastName.ToString();
                            }
                            if (!string.IsNullOrEmpty(FileRequest.SalesPerson.FirstName))
                            {
                                firstname = ", " + FileRequest.SalesPerson.FirstName.ToString();
                                hdnFirstName.Value = FileRequest.SalesPerson.FirstName.ToString();
                            }
                            if (!string.IsNullOrEmpty(FileRequest.SalesPerson.MiddleName))
                            {
                                middlename = " " + FileRequest.SalesPerson.MiddleName.ToString();
                                hdnMiddleName.Value = FileRequest.SalesPerson.MiddleName.ToString();
                            }

                            salesPersonName = lastname + firstname + middlename;
                            lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(salesPersonName.ToUpper());

                            if (FileRequest.SalesPerson.PhoneNUM != null)
                                businessphone = "Business Phone : " + FileRequest.SalesPerson.PhoneNUM;

                            if (FileRequest.SalesPerson.EmailAddress != null)
                                businessemail = "Business E-Mail : " + FileRequest.SalesPerson.EmailAddress;


                            //Fix for SUP-381 (3194)
                            tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(businessphone + "\n" + businessemail);
                        }
                        if (FileRequest.LeadSource != null)
                        {
                            hdnLeadSource.Value = FileRequest.LeadSource.LeadSourceID.ToString();
                            tbLeadSource.Text = FileRequest.LeadSource.LeadSourceCD;
                            if (!string.IsNullOrEmpty(FileRequest.LeadSource.LeadSourceDescription))
                                lbleadSource.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.LeadSource.LeadSourceDescription.ToString().ToUpper());
                        }
                        #endregion
                        CQMain cqMain = new CQMain();
                        cqMain = FileRequest.CQMains.Where(x => x.QuoteNUM == 1 && x.IsDeleted == false).FirstOrDefault();
                        if (cqMain != null)
                        {
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqMain))
                            {
                                #region "Quote Details"
                                if (!string.IsNullOrEmpty(cqMain.QuoteNUM.ToString()))
                                    tbQuoteNumber.Text = cqMain.QuoteNUM.ToString();
                                if (!string.IsNullOrEmpty(cqMain.CQSource.ToString()))
                                {
                                    rblSource.SelectedValue = cqMain.CQSource.ToString();
                                    SetSourceByValue(rblSource.SelectedValue);
                                }
                                if (cqMain.Vendor != null)
                                {
                                    hdnVendor.Value = cqMain.VendorID.Value.ToString();
                                    tbVendor.Text = cqMain.Vendor.VendorCD;
                                    if (!string.IsNullOrEmpty(cqMain.Vendor.Name))
                                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(cqMain.Vendor.Name.ToUpper());
                                }
                                if (cqMain.Fleet != null)
                                {
                                    hdnTailNum.Value = cqMain.FleetID.Value.ToString();
                                    tbTailNum.Text = cqMain.Fleet.TailNum;
                                }
                                if (cqMain.Aircraft != null)
                                {
                                    hdnTypeCode.Value = cqMain.AircraftID.ToString();
                                    tbTypeCode.Text = cqMain.Aircraft.AircraftCD;
                                    if (cqMain.Aircraft.AircraftDescription != null)
                                        lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(cqMain.Aircraft.AircraftDescription.ToUpper());
                                }
                                if (cqMain.FlightHoursTotal != null)
                                {
                                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    {
                                        tbHrs.Text = ConvertTenthsToMins(Math.Round(Convert.ToDecimal(cqMain.FlightHoursTotal), 3).ToString());
                                    }
                                    else
                                    {
                                        tbHrs.Text = "00:00";
                                    }
                                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                    {
                                        tbHrs.Text = Convert.ToString(System.Math.Round(Convert.ToDecimal(cqMain.FlightHoursTotal.ToString()), 1));

                                    }
                                    else
                                    {
                                        tbHrs.Text = "0.0";
                                    }

                                }
                                // tbHrs.Text = cqMain.FlightHoursTotal.ToString();
                                if (cqMain.StdCrewRONTotal != null)
                                    tbRONCrewCharge.Text = cqMain.StdCrewRONTotal.ToString();
                                if (cqMain.LandingFeeTotal != null)
                                    tbLandingFees.Text = cqMain.LandingFeeTotal.ToString();
                                if (cqMain.UsageAdjTotal != null)
                                    tbDailyUseAdj.Text = cqMain.UsageAdjTotal.ToString();
                                if (cqMain.FlightChargeTotal != null)
                                    tbFlightCharges.Text = cqMain.FlightChargeTotal.ToString();
                                if (cqMain.SegmentFeeTotal != null)
                                    tbSegFeesChrg.Text = ((cqMain.SegmentFeeTotal != null ? (decimal)cqMain.SegmentFeeTotal : 0) + (cqMain.TaxesTotal != null ? (decimal)cqMain.TaxesTotal : 0)).ToString();
                                if (cqMain.QuoteTotal != null)
                                    tbTotalQuote.Text = cqMain.QuoteTotal.ToString();
                                #endregion
                            }

                            if (QuoteInProgress != null)
                            {
                                CQLeg cqLeg = new CQLeg();
                                cqLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(item["LegNUM"].Text) && x.IsDeleted == false).FirstOrDefault();
                                if (cqLeg != null)
                                {
                                    hdnLegNum.Value = cqLeg.LegNUM.ToString();
                                    RadPanelItem raditem = this.pnlTrip.FindItemByValue("pnlItemTrip");
                                    raditem.Text = "Leg " + hdnLegNum.Value.ToString() + " Details";
                                    #region Authorization - Check for Add/Update access and make save cancel visible
                                    if (cqLeg.State == CQRequestEntityState.Modified)
                                    {
                                        if (!IsAuthorized(Permission.CharterQuote.EditCQLeg))
                                        {
                                            btnSave.Visible = false;
                                            btnCancel.Visible = false;
                                        }
                                    }
                                    if (cqLeg.State == CQRequestEntityState.Added)
                                    {
                                        if (!IsAuthorized(Permission.CharterQuote.AddCQLeg))
                                        {
                                            btnSave.Visible = false;
                                            btnCancel.Visible = false;
                                        }
                                    }
                                    #endregion
                                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqLeg))
                                    {
                                        FileRequest = (CQFile)Session[EQSessionKey];
                                        #region "Leg Details"
                                        #region Departs
                                        if (cqLeg.Airport1 != null)
                                        {
                                            tbDepart.Text = cqLeg.Airport1.IcaoID;
                                            lbDepart.Text = cqLeg.Airport1.AirportName != null ? System.Web.HttpUtility.HtmlEncode(cqLeg.Airport1.AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            hdnDepart.Value = System.Web.HttpUtility.HtmlEncode(cqLeg.Airport1.AirportID.ToString());
                                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(cqLeg.Airport1.IcaoID);
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    AirportLists = objRetVal.EntityList;
                                                    if (AirportLists != null && AirportLists.Count > 0)
                                                    {
                                                        lbDepartsICAO.Text = AirportLists[0].IcaoID == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID.ToString());
                                                        lbDepartsCity.Text = AirportLists[0].CityName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName.ToString());
                                                        lbDepartsState.Text = AirportLists[0].StateName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName.ToString());
                                                        lbDepartsCountry.Text = AirportLists[0].CountryName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName.ToString());
                                                        lbDepartsAirport.Text = AirportLists[0].AirportName == null ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName.ToString());
                                                        lbDepartsTakeoffbias.Text = AirportLists[0].TakeoffBIAS != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS.ToString()) : System.Web.HttpUtility.HtmlEncode("0");
                                                        lbDepartsLandingbias.Text = AirportLists[0].LandingBIAS != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS.ToString()) : System.Web.HttpUtility.HtmlEncode("0");
                                                        #region Tooltip for AirportDetails
                                                        string builder = string.Empty;
                                                        builder = "ICAO Id: " + (AirportLists[0].IcaoID != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].DSTRegionCD) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].OffsetToGMT.ToString()) : System.Web.HttpUtility.HtmlEncode(string.Empty))
                                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].LongestRunway.ToString()) : System.Web.HttpUtility.HtmlEncode(string.Empty)
                                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty));
                                                        lbDepartIcao.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                                        lbDepart.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                                        #endregion

                                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                                        // Fix for UW-1165(8179)
                                                        string tooltipStr = string.Empty;

                                                        if (!string.IsNullOrEmpty(AirportLists[0].Alerts)) // || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes)) 
                                                        {
                                                            tbDepart.ForeColor = Color.Red;
                                                            string alertStr = "Alerts : \n";
                                                            alertStr += AirportLists[0].Alerts;
                                                            tooltipStr = alertStr;
                                                        }
                                                        else
                                                            tbDepart.ForeColor = Color.Black;

                                                        if (!string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                                        {
                                                            string noteStr = string.Empty;
                                                            if (!string.IsNullOrEmpty(tooltipStr))
                                                                noteStr = "\n\nNotes : \n";
                                                            else
                                                                noteStr = "Notes : \n";
                                                            noteStr += AirportLists[0].GeneralNotes;
                                                            tooltipStr += noteStr;
                                                        }

                                                        tbDepart.ToolTip = tooltipStr;

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            tbDepart.Text = string.Empty;
                                            lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            hdnDepart.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        }
                                        if (cqLeg.DepartureDTTMLocal != DateTime.MinValue && cqLeg.DepartureDTTMLocal != null)
                                        {
                                            DateTime dt = (DateTime)cqLeg.DepartureDTTMLocal;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", cqLeg.DepartureDTTMLocal);
                                            rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbLocalDate.Text = string.Empty;
                                            rmtlocaltime.Text = "0000";
                                        }
                                        if (cqLeg.DepartureGreenwichDTTM != DateTime.MinValue && cqLeg.DepartureGreenwichDTTM != null)
                                        {
                                            DateTime dt = (DateTime)cqLeg.DepartureGreenwichDTTM;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", cqLeg.DepartureGreenwichDTTM);
                                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbUtcDate.Text = string.Empty;
                                            rmtUtctime.Text = "0000";
                                        }
                                        if (cqLeg.HomeDepartureDTTM != DateTime.MinValue && cqLeg.HomeDepartureDTTM != null)
                                        {
                                            DateTime dt = (DateTime)cqLeg.HomeDepartureDTTM;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", cqLeg.HomeDepartureDTTM);
                                            rmtHomeTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbHomeDate.Text = string.Empty;
                                            rmtHomeTime.Text = "0000";
                                        }
                                        #endregion
                                        #region Arrives
                                        if (cqLeg.Airport != null)
                                        {
                                            tbArrival.Text = cqLeg.Airport.IcaoID;
                                            lbArrival.Text = cqLeg.Airport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(cqLeg.Airport.AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            hdnArrival.Value = System.Web.HttpUtility.HtmlEncode(cqLeg.Airport.AirportID.ToString());
                                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(cqLeg.Airport.IcaoID);
                                                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<GetAllAirport>();
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    getAllAirportList = objRetVal.EntityList;
                                                    if (getAllAirportList != null && getAllAirportList.Count > 0)
                                                    {
                                                        lbArrivesICAO.Text = getAllAirportList[0].IcaoID != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].IcaoID) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        lbArrivesCity.Text = getAllAirportList[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CityName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        lbArrivesState.Text = getAllAirportList[0].StateName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].StateName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        lbArrivesCountry.Text = getAllAirportList[0].CountryName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CountryName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        lbArrivesAirport.Text = getAllAirportList[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        Decimal TakeOffBias = getAllAirportList[0].TakeoffBIAS != null ? (Math.Round((decimal)getAllAirportList[0].TakeoffBIAS, 1)) : 0;
                                                        lbArrivesTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(TakeOffBias.ToString());
                                                        Decimal LandingBias = getAllAirportList[0].LandingBIAS != null ? (Math.Round((decimal)getAllAirportList[0].LandingBIAS, 1)) : 0;
                                                        lbArrivesLandingbias.Text = System.Web.HttpUtility.HtmlEncode(LandingBias.ToString());
                                                        #region Tooltip for AirportDetails
                                                        string builder = string.Empty;
                                                        builder = "ICAO : " + (getAllAirportList[0].IcaoID != null ? getAllAirportList[0].IcaoID : string.Empty)
                                                            + "\n" + "City : " + (getAllAirportList[0].CityName != null ? getAllAirportList[0].CityName : string.Empty)
                                                            + "\n" + "State/Province : " + (getAllAirportList[0].StateName != null ? getAllAirportList[0].StateName : string.Empty)
                                                            + "\n" + "Country : " + (getAllAirportList[0].CountryName != null ? getAllAirportList[0].CountryName : string.Empty)
                                                            + "\n" + "Airport : " + (getAllAirportList[0].AirportName != null ? getAllAirportList[0].AirportName : string.Empty)
                                                            + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD != null ? getAllAirportList[0].DSTRegionCD : string.Empty)
                                                            + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                                            + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                                            + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);
                                                        lbArrive.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                                        lbArrival.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                                        #endregion

                                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                                        // Fix for UW-1165(8179)
                                                        string tooltipStr = string.Empty;
                                                        if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts)) // || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes)) 
                                                        {
                                                            tbArrival.ForeColor = Color.Red;
                                                            string alertStr = "Alerts : \n";
                                                            alertStr += getAllAirportList[0].Alerts;
                                                            tooltipStr = alertStr;
                                                        }
                                                        else
                                                            tbArrival.ForeColor = Color.Black;

                                                        if (!string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                                        {
                                                            string noteStr = string.Empty;
                                                            if (!string.IsNullOrEmpty(tooltipStr))
                                                                noteStr = "\n\nNotes : \n";
                                                            else
                                                                noteStr = "Notes : \n";
                                                            noteStr += getAllAirportList[0].GeneralNotes;
                                                            tooltipStr += noteStr;
                                                        }

                                                        tbArrival.ToolTip = tooltipStr;
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            tbArrival.Text = string.Empty;
                                            lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                            hdnArrival.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        }
                                        if (cqLeg.ArrivalDTTMLocal != DateTime.MinValue && cqLeg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime dt = (DateTime)cqLeg.ArrivalDTTMLocal;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", cqLeg.ArrivalDTTMLocal);
                                            rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbArrivalDate.Text = string.Empty;
                                            rmtArrivalTime.Text = "0000";
                                        }
                                        if (cqLeg.ArrivalGreenwichDTTM != DateTime.MinValue && cqLeg.ArrivalGreenwichDTTM != null)
                                        {
                                            DateTime dt = (DateTime)cqLeg.ArrivalGreenwichDTTM;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", cqLeg.ArrivalGreenwichDTTM);
                                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbArrivalUtcDate.Text = string.Empty;
                                            rmtArrivalUtctime.Text = "0000";
                                        }
                                        if (cqLeg.HomeArrivalDTTM != DateTime.MinValue && cqLeg.HomeArrivalDTTM != null)
                                        {
                                            DateTime dt = (DateTime)cqLeg.HomeArrivalDTTM;
                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", cqLeg.HomeArrivalDTTM);
                                            rmtHomeTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbArrivalHomeDate.Text = string.Empty;
                                            rmtHomeTime.Text = "0000";
                                        }
                                        #endregion
                                        #region Distance & Time
                                        //if (cqLeg.Distance != null)
                                        //    tbMiles.Text = cqLeg.Distance.ToString();
                                        //else
                                        //    tbMiles.Text = "0";

                                        tbMiles.Text = "0";
                                        hdnMiles.Value = "0";
                                        if (cqLeg.Distance != null && cqLeg.Distance != decimal.MinValue)
                                        {
                                            tbMiles.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)cqLeg.Distance).ToString();
                                            hdnMiles.Value = cqLeg.Distance.ToString();
                                        }

                                        foreach (ListItem Item in rblistWindReliability.Items)
                                        {
                                            if (Item.Text == cqLeg.WindReliability.ToString())
                                                Item.Selected = true;
                                        }
                                        tbPower.Text = cqLeg.PowerSetting;
                                        if (cqLeg.TakeoffBIAS != null)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                //tbBias.Text = Math.Round((decimal)Leg.TakeoffBIAS, 2).ToString();
                                                tbBias.Text = "00:00";
                                                tbBias.Text = ConvertTenthsToMins(cqLeg.TakeoffBIAS.ToString());
                                            }
                                            else
                                            {
                                                tbBias.Text = "0.0";
                                                tbBias.Text = Math.Round((decimal)cqLeg.TakeoffBIAS, 1).ToString();
                                            }
                                        }
                                        tbTAS.Text = cqLeg.TrueAirSpeed.ToString();
                                        if (cqLeg.LandingBIAS != null)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbLandBias.Text = "00:00";
                                                tbLandBias.Text = ConvertTenthsToMins((cqLeg.LandingBIAS).ToString());
                                            }
                                            else
                                            {
                                                tbLandBias.Text = "0.0";
                                                tbLandBias.Text = Math.Round((decimal)cqLeg.LandingBIAS, 1).ToString();
                                            }
                                        }
                                        // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                        {
                                            if (!string.IsNullOrEmpty(tbBias.Text))
                                            {
                                                if (tbBias.Text.IndexOf(".") < 0)
                                                {
                                                    tbBias.Text = tbBias.Text + ".0";
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(tbLandBias.Text))
                                            {
                                                if (tbLandBias.Text.IndexOf(".") < 0)
                                                {
                                                    tbLandBias.Text = tbLandBias.Text + ".0";
                                                }
                                            }
                                        }
                                        tbWind.Text = cqLeg.WindsBoeingTable.ToString();
                                        rblistWindReliability.SelectedValue = cqLeg.WindReliability != null ? cqLeg.WindReliability.ToString() : "3";
                                        double ETE = 0.0;
                                        if (cqLeg.ElapseTM != null)
                                        {
                                            ETE = RoundElpTime((double)cqLeg.ElapseTM);
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                                tbETE.Text = ConvertTenthsToMins(ETE.ToString());
                                            else
                                            {
                                                tbETE.Text = Math.Round(ETE, 1).ToString();
                                                if (tbETE.Text.IndexOf(".") < 0)
                                                    tbETE.Text = tbETE.Text + ".0";
                                            }
                                        }
                                        #endregion
                                        #region Duty
                                        if (cqLeg.CrewDutyRule != null)
                                        {
                                            tbCrewRules.Text = cqLeg.CrewDutyRule.CrewDutyRuleCD;
                                            hdnCrewRules.Value = System.Web.HttpUtility.HtmlEncode(cqLeg.CrewDutyRulesID.ToString());
                                        }
                                        string lcCdAlert = string.Empty;
                                        if (cqLeg.CrewDutyAlert != null) // Need the name to change as CrewDutyAlert
                                            lcCdAlert = cqLeg.CrewDutyAlert;
                                        if (lcCdAlert.LastIndexOf('D') >= 0)
                                        {
                                            lbTotalDuty.CssClass = "infored";
                                            lbTotalDuty.ForeColor = Color.White;
                                            lbTotalDuty.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbTotalDuty.CssClass = "infoash";
                                            lbTotalDuty.ForeColor = Color.Black;
                                            lbTotalDuty.BackColor = Color.Transparent;
                                        }
                                        if (lcCdAlert.LastIndexOf('F') >= 0)
                                        {
                                            lbTotalFlight.CssClass = "infored";
                                            lbTotalFlight.ForeColor = Color.White;
                                            lbTotalFlight.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbTotalFlight.CssClass = "infoash";
                                            lbTotalFlight.ForeColor = Color.Black;
                                            lbTotalFlight.BackColor = Color.Transparent;
                                        }
                                        if (lcCdAlert.LastIndexOf('R') >= 0)
                                        {
                                            lbRest.CssClass = "infored";
                                            lbRest.ForeColor = Color.White;
                                            lbRest.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbRest.CssClass = "infoash";
                                            lbRest.ForeColor = Color.Black;
                                            lbRest.BackColor = Color.Transparent;
                                        }
                                        chkEndOfDuty.Checked = cqLeg.IsDutyEnd == true ? true : false;
                                        tbOverride.Text = "0.0";
                                        if (cqLeg.CQOverRide != null)
                                        {
                                            if ((decimal)cqLeg.CQOverRide != 0)
                                                tbOverride.Text = Math.Round((decimal)cqLeg.CQOverRide, 1).ToString();
                                        }
                                        if (cqLeg.FedAviationRegNUM != null)
                                            lbFar.Text = System.Web.HttpUtility.HtmlEncode(cqLeg.FedAviationRegNUM.ToString());
                                        lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        if (cqLeg.DutyHours != null && cqLeg.DutyHours != 0)
                                        {
                                            double dutyHours = Math.Round((double)cqLeg.DutyHours, 1);
                                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(dutyHours.ToString());
                                            if (lbTotalDuty.Text.IndexOf(".") < 0)
                                            {
                                                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(lbTotalDuty.Text + ".0");
                                            }
                                        }
                                        else
                                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        if (cqLeg.FlightHours != null && cqLeg.FlightHours != 0)
                                        {
                                            double totalFlightHours = Math.Round((double)cqLeg.FlightHours, 1);
                                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(totalFlightHours.ToString());
                                            if (lbTotalFlight.Text.IndexOf(".") < 0)
                                            {
                                                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(lbTotalFlight.Text + ".0");
                                            }
                                        }
                                        else
                                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        lbRest.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        if (cqLeg.RestHours != null && cqLeg.RestHours != 0)
                                        {
                                            double restHours = Math.Round((double)cqLeg.RestHours, 1);
                                            lbRest.Text = System.Web.HttpUtility.HtmlEncode(restHours.ToString());
                                            if (lbRest.Text.IndexOf(".") < 0)
                                            {
                                                lbRest.Text = System.Web.HttpUtility.HtmlEncode(lbRest.Text + ".0");
                                            }
                                        }
                                        else
                                            lbRest.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        #endregion
                                        #region Flight
                                        if (cqLeg.ChargeTotal != null)
                                        {
                                            if ((decimal)cqLeg.ChargeTotal != 0)
                                                tbCharges.Text = Math.Round((decimal)cqLeg.ChargeTotal, 2).ToString();
                                        }
                                        else
                                        {
                                            tbCharges.Text = "0.00";
                                        }
                                        if (cqLeg.ChargeRate != null)
                                        {
                                            if ((decimal)cqLeg.ChargeRate != 0)
                                                tbLegRate.Text = Math.Round((decimal)cqLeg.ChargeRate, 2).ToString();
                                        }
                                        else
                                        {
                                            tbLegRate.Text = "0.00";
                                        }
                                        if (cqLeg.DutyTYPE != null)
                                            rbDomestic.SelectedValue = cqLeg.DutyTYPE.ToString();
                                        else
                                            rbDomestic.SelectedValue = "1";
                                        #endregion
                                        #region LastPart
                                        if (QuoteInProgress.Fleet != null)
                                        {
                                            if (QuoteInProgress.Fleet.MaximumPassenger != null && QuoteInProgress.Fleet.MaximumPassenger > 0)
                                            {
                                                if (cqLeg.State == CQRequestEntityState.Added)
                                                {
                                                    Int32 SeatTotal = 0;
                                                    SeatTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);
                                                    if (cqLeg.PassengerTotal != null)
                                                        cqLeg.ReservationAvailable = SeatTotal - cqLeg.PassengerTotal;
                                                    else
                                                        cqLeg.ReservationAvailable = SeatTotal;
                                                }
                                            }
                                        }
                                        if (IsAuthorized(Permission.CharterQuote.ViewCQLeg))
                                        {
                                            // tbPaxNotes.Text = cqLeg.Notes;
                                        }
                                        if (!string.IsNullOrEmpty(cqLeg.FlightPurposeDescription))
                                            tbPurpose.Text = cqLeg.FlightPurposeDescription;
                                        if (cqLeg.PassengerTotal != null)
                                            tbPaxCount.Text = cqLeg.PassengerTotal.ToString();
                                        else
                                            tbPaxCount.Text = "0";
                                        if (cqLeg.TaxRate != null)
                                            tbTaxRate.Text = cqLeg.TaxRate.ToString();
                                        else
                                            tbTaxRate.Text = "0.00";
                                        if (cqLeg.IsPositioning == true)
                                        {
                                            chkPOS.Checked = true;
                                        }
                                        else
                                        {
                                            chkPOS.Checked = false;
                                        }
                                        if (cqLeg.IsTaxable == true)
                                        {
                                            chkTaxable.Checked = true;
                                        }
                                        else
                                        {
                                            chkTaxable.Checked = false;
                                        }
                                        #endregion
                                        #region "RON"
                                        if (!string.IsNullOrEmpty(cqLeg.RemainOverNightCNT.ToString()))
                                            tbRON.Text = cqLeg.RemainOverNightCNT.ToString();
                                        else
                                            tbRON.Text = "0";
                                        if (!string.IsNullOrEmpty(cqLeg.DayRONCNT.ToString()))
                                            tbDayRoom.Text = cqLeg.DayRONCNT.ToString();
                                        else
                                            tbDayRoom.Text = "0";
                                        #endregion
                                        FileRequest.EstDepartureDT = QuoteInProgress.CQLegs[0].DepartureDTTMLocal;
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void SaveQuoteinProgressToFileSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    CQMain QuoteToUpdate = new CQMain();
                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        QuoteToUpdate = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                        QuoteToUpdate = QuoteInProgress;
                    }
                    Session[EQSessionKey] = FileRequest;
                }
            }
        }

        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";
                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }
                return result;
            }
        }

        protected double RoundElpTime(double lnElp_Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lnElp_Time))
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    decimal ElapseTMRounding = 0;
                    double lnElp_Min = 0.0;
                    double lnEteRound = 0.0;
                    if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                        ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;
                    if (ElapseTMRounding == 1)
                        lnEteRound = 0;
                    else if (ElapseTMRounding == 2)
                        lnEteRound = 5;
                    else if (ElapseTMRounding == 3)
                        lnEteRound = 10;
                    else if (ElapseTMRounding == 4)
                        lnEteRound = 15;
                    else if (ElapseTMRounding == 5)
                        lnEteRound = 30;
                    else if (ElapseTMRounding == 6)
                        lnEteRound = 60;
                    if (lnEteRound > 0)
                    {
                        lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                        if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                            lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                        else
                            lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));
                        if (lnElp_Min > 0 && lnElp_Min < 60)
                            lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;
                        if (lnElp_Min == 0)
                            lnElp_Time = Math.Floor(lnElp_Time);
                        if (lnElp_Min == 60)
                            lnElp_Time = Math.Ceiling(lnElp_Time);
                    }
                }
                return lnElp_Time;
            }
        }

        protected void Legs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindLegs(dgLegs, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void Legs_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            if (Session[EQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[EQSessionKey];
                                if (FileRequest != null && FileRequest.Mode != CQRequestActionMode.Edit)
                                {
                                    LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInsert") as LinkButton;
                                    insertButton.Visible = false;
                                    LinkButton addButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                                    addButton.Visible = false;
                                    LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton;
                                    deleteButton.Visible = false;
                                }
                            }
                            else
                            {
                                LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInsert") as LinkButton;
                                insertButton.Visible = false;
                                LinkButton addButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                                addButton.Visible = false;
                                LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton;
                                deleteButton.Visible = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void dgPowerSetting_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Power", typeof(Int16));
                        dt.Columns.Add("PowerSetting", typeof(string));
                        dt.Columns.Add("TAS", typeof(decimal));
                        dt.Columns.Add("HrRange", typeof(decimal));
                        dt.Columns.Add("TOBias", typeof(decimal));
                        dt.Columns.Add("LandBias", typeof(decimal));
                        if (QuoteInProgress != null)
                        {
                            if (QuoteInProgress.AircraftID != null)
                            {
                                Int64 AircraftID = (Int64)QuoteInProgress.AircraftID;
                                FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                                lbAircraft.Text = System.Web.HttpUtility.HtmlEncode(Aircraft.AircraftDescription);
                                dt.Rows.Clear();
                                if (Aircraft != null)
                                {
                                    dt.Rows.Add(1, Aircraft.PowerSettings1Description,
                                        Aircraft.PowerSettings1TrueAirSpeed == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1TrueAirSpeed, 0),
                                        Aircraft.PowerSettings1HourRange == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1HourRange, 1),
                                        Aircraft.PowerSettings1TakeOffBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1),
                                        Aircraft.PowerSettings1LandingBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1));
                                    dt.Rows.Add(2, Aircraft.PowerSettings2Description,
                                        Aircraft.PowerSettings2TrueAirSpeed == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2TrueAirSpeed, 0),
                                        Aircraft.PowerSettings2HourRange == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2HourRange, 1),
                                        Aircraft.PowerSettings2TakeOffBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1),
                                        Aircraft.PowerSettings2LandingBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1));
                                    dt.Rows.Add(3, Aircraft.PowerSettings3Description,
                                        Aircraft.PowerSettings3TrueAirSpeed == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3TrueAirSpeed, 0),
                                        Aircraft.PowerSettings3HourRange == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3HourRange, 1),
                                        Aircraft.PowerSettings3TakeOffBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1),
                                        Aircraft.PowerSettings3LandingBias == null ? 0 : Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1));
                                }
                            }
                            else
                            {
                                dt.Rows.Clear();
                                dt.Rows.Add(0, string.Empty, 0, 0.0, 0.0, 0.0);
                                dt.Rows.Add(0, string.Empty, 0, 0.0, 0.0, 0.0);
                                dt.Rows.Add(0, string.Empty, 0, 0.0, 0.0, 0.0);
                            }
                        }
                        else
                        {
                            dt.Rows.Clear();
                            dt.Rows.Add(0, string.Empty, 0, 0.0, 0.0, 0.0);
                            dt.Rows.Add(0, string.Empty, 0, 0.0, 0.0, 0.0);
                            dt.Rows.Add(0, string.Empty, 0, 0.0, 0.0, 0.0);
                        }
                        dgPowerSetting.DataSource = dt;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void Legs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegs.SelectedItems.Count > 0)
                        {
                            CalculateQuantity();
                            CalculateQuoteTotal();
                            SaveCharterQuoteToSession();
                            LoadFileDetails();
                            //GridEnable(true, true, true);
                            selectchangefired = true;
                            //GridDataItem item = (GridDataItem)dgLegs.SelectedItems[0];
                            //dgLegs.Rebind();
                            //MarkLegItemSelected(Convert.ToInt64(item["LegNUM"].Text));
                            ExceptionValidation();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void MarkLegItemSelected(Int64 LegNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegNum))
            {
                foreach (GridDataItem Item in dgLegs.Items)
                {
                    if (Convert.ToInt64(Item["LegNum"].Text) == LegNum)
                    {
                        Item.Selected = true;
                    }
                }
            }
        }

        protected void AddOrInsertNewLeg(Int64 LegNum, Legstate AddOrInsert)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegNum, AddOrInsert))
            {
                if (QuoteInProgress != null)
                {
                    switch (AddOrInsert)
                    {
                        case Legstate.Add:
                            {
                                int Legscount = 1;
                                Legscount = dgLegs.Items.Count;
                                CQLeg CurrLeg = new CQLeg();
                                CurrLeg.State = CQRequestEntityState.Added;
                                CurrLeg.IsDeleted = false;
                                CurrLeg.ShowWarningMessage = true;
                                hdnShowWarnMessage.Value = "true";
                                CurrLeg.LegNUM = Legscount + 1;
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg PrevLeg = new CQLeg();
                                        PrevLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Legscount && x.IsDeleted == false).FirstOrDefault();
                                        if (PrevLeg != null && PrevLeg.Airport != null)
                                        {
                                            CurrLeg.DAirportID = PrevLeg.Airport.AirportID;
                                            CharterQuoteService.Airport DepartAirport = new CharterQuoteService.Airport();
                                            DepartAirport.IcaoID = PrevLeg.Airport.IcaoID;
                                            DepartAirport.AirportID = PrevLeg.Airport.AirportID;
                                            DepartAirport.AirportName = PrevLeg.Airport.AirportName;
                                            CurrLeg.Airport1 = DepartAirport;
                                            CurrLeg.ArrivalAirportChanged = true;
                                            CurrLeg.DepartAirportChanged = true;
                                            #region AutoCalulateLegtimes
                                            if (PrevLeg.ArrivalDTTMLocal != null)
                                            {
                                                DateTime dt = (DateTime)PrevLeg.ArrivalDTTMLocal;
                                                double hourtoadd = 0;
                                                if (PrevLeg.DutyTYPE != null && PrevLeg.DutyTYPE == 1)
                                                    hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                                else
                                                    hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;
                                                dt = dt.AddHours(hourtoadd);
                                                CurrLeg.DepartureDTTMLocal = dt;
                                                DateTime ldGmtDep = DateTime.MinValue;
                                                DateTime ldHomDep = DateTime.MinValue;
                                                using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                                {
                                                    ldGmtDep = objDstsvc.GetGMT((long)CurrLeg.DAirportID, dt, true, false);
                                                    CurrLeg.DepartureGreenwichDTTM = ldGmtDep;
                                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                                    {
                                                        ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ldGmtDep, false, false);
                                                        CurrLeg.HomeDepartureDTTM = ldHomDep;
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                hdnLegNum.Value = CurrLeg.LegNUM.ToString();
                                if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                    QuoteInProgress.State = CQRequestEntityState.Modified;
                                if (QuoteInProgress.CQLegs == null || QuoteInProgress.CQLegs.Count == 0)
                                {
                                    QuoteInProgress.CQLegs = new List<CQLeg>();
                                    #region "Load Homebase Airport to departure"
                                    Int64 HomebaseID = 0;
                                    if (UserPrincipal.Identity._homeBaseId != null)
                                    {
                                        HomebaseID = (Int64)UserPrincipal.Identity._homeBaseId;
                                        FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = GetCompany(HomebaseID);
                                        if (Comp != null)
                                        {
                                            FlightPakMasterService.GetAllAirport HomebaseAirport = GetAirport((long)Comp.HomebaseAirportID);
                                            if (HomebaseAirport != null)
                                            {
                                                CharterQuoteService.Airport DeptAirport = new CharterQuoteService.Airport();
                                                CurrLeg.DAirportID = HomebaseAirport.AirportID;
                                                DeptAirport.IcaoID = HomebaseAirport.IcaoID;
                                                DeptAirport.AirportID = HomebaseAirport.AirportID;
                                                DeptAirport.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                CurrLeg.Airport1 = DeptAirport;
                                                CurrLeg.ArrivalAirportChanged = true;
                                                CurrLeg.DepartAirportChanged = true;
                                            }
                                        }
                                    }
                                    #endregion
                                    #region "Load System date"
                                    CurrLeg.DepartureDTTMLocal = DateTime.Now.Date;
                                    #endregion
                                }

                                #region set Crew Duty Rule
                                if (UserPrincipal.Identity._fpSettings._CQCrewDuty != null)
                                {
                                    if (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._CQCrewDuty))
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                            var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == UserPrincipal.Identity._fpSettings._CQCrewDuty.Trim().ToUpper()).ToList();
                                                if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                                {
                                                    CurrLeg.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                    CharterQuoteService.CrewDutyRule CDrule = new CrewDutyRule();
                                                    CDrule.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                    CDrule.CrewDutyRuleCD = CrewDtyRuleList[0].CrewDutyRuleCD;
                                                    hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                                    CDrule.CrewDutyRulesDescription = CrewDtyRuleList[0].CrewDutyRulesDescription;
                                                    CurrLeg.FedAviationRegNUM = CrewDtyRuleList[0].FedAviatRegNum != null ? CrewDtyRuleList[0].FedAviatRegNum : string.Empty;
                                                    CurrLeg.CrewDutyRule = CDrule;
                                                }
                                            }
                                        }

                                    }
                                }
                                #endregion
                                QuoteInProgress.CQLegs.Add(CurrLeg);
                                BindLegs(dgLegs, true);
                                if (dgLegs.Items.Count > 0)
                                    dgLegs.Items[dgLegs.Items.Count - 1].Selected = true;
                            }
                            break;
                        case Legstate.Insert:
                            {
                                CQLeg CurrLeg = new CQLeg();
                                CurrLeg.State = CQRequestEntityState.Added;
                                CurrLeg.IsDeleted = false;
                                CurrLeg.ShowWarningMessage = true;
                                CurrLeg.LegNUM = Convert.ToInt16(hdnLegNum.Value.ToString()) < 1 ? 1 : Convert.ToInt16(hdnLegNum.Value.ToString());
                                hdnLegNum.Value = CurrLeg.LegNUM.ToString();
                                hdnShowWarnMessage.Value = "true";
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        if (Convert.ToInt16(hdnLegNum.Value.ToString()) > 1)
                                        {
                                            CQLeg PrevLeg = new CQLeg();
                                            PrevLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt16(hdnLegNum.Value.ToString()) - 1 && x.IsDeleted == false).FirstOrDefault();
                                            if (PrevLeg != null && PrevLeg.Airport != null)
                                            {
                                                CurrLeg.DAirportID = PrevLeg.Airport.AirportID;
                                                CharterQuoteService.Airport DepartAirport = new CharterQuoteService.Airport();
                                                DepartAirport.AirportID = PrevLeg.Airport.AirportID;
                                                DepartAirport.IcaoID = PrevLeg.Airport.IcaoID;
                                                DepartAirport.AirportName = PrevLeg.Airport.AirportName;
                                                CurrLeg.Airport1 = DepartAirport;
                                                CurrLeg.ArrivalAirportChanged = true;
                                                CurrLeg.DepartAirportChanged = true;
                                                #region AutoCalulateLegtimes
                                                if (PrevLeg.ArrivalDTTMLocal != null)
                                                {
                                                    DateTime dt = (DateTime)PrevLeg.ArrivalDTTMLocal;
                                                    double hourtoadd = 0;
                                                    if (PrevLeg.DutyTYPE != null && PrevLeg.DutyTYPE == 1)
                                                        hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                                    else
                                                        hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;
                                                    dt = dt.AddHours(hourtoadd);
                                                    CurrLeg.DepartureDTTMLocal = dt;
                                                    DateTime ldGmtDep = DateTime.MinValue;
                                                    DateTime ldHomDep = DateTime.MinValue;
                                                    using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                                    {
                                                        ldGmtDep = objDstsvc.GetGMT((long)CurrLeg.DAirportID, dt, true, false);
                                                        CurrLeg.DepartureGreenwichDTTM = ldGmtDep;
                                                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                                        {
                                                            ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ldGmtDep, false, false);
                                                            CurrLeg.HomeDepartureDTTM = ldHomDep;
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            CQLeg NextLeg = new CQLeg();
                                            NextLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == (Convert.ToInt16(hdnLegNum.Value.ToString()) + 1) && x.IsDeleted == false).FirstOrDefault();
                                            if (NextLeg != null && NextLeg.Airport1 != null)
                                            {
                                                CurrLeg.DAirportID = NextLeg.Airport1.AirportID;
                                                CharterQuoteService.Airport ArrivalAirport = new CharterQuoteService.Airport();
                                                ArrivalAirport.AirportID = NextLeg.Airport1.AirportID;
                                                ArrivalAirport.IcaoID = NextLeg.Airport1.IcaoID;
                                                ArrivalAirport.AirportName = NextLeg.Airport1.AirportName;
                                                CurrLeg.Airport = ArrivalAirport;
                                                CurrLeg.ArrivalAirportChanged = true;
                                                CurrLeg.DepartAirportChanged = true;
                                                #region "Auto Calculate Leg times"
                                                if (NextLeg.DepartureDTTMLocal != null)
                                                {
                                                    DateTime dt = (DateTime)NextLeg.DepartureDTTMLocal;
                                                    double hourtoadd = 0;
                                                    if (NextLeg.DutyTYPE != null && NextLeg.DutyTYPE == 1)
                                                        hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                                    else
                                                        hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;
                                                    dt = dt.AddHours(-1 * hourtoadd);
                                                    CurrLeg.ArrivalDTTMLocal = dt;
                                                    if (!string.IsNullOrEmpty(hdnArrival.Value))
                                                    {
                                                        DateTime ldGmtArr = DateTime.MinValue;
                                                        DateTime ldHomArr = DateTime.MinValue;
                                                        using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                                        {
                                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, false);
                                                            CurrLeg.ArrivalGreenwichDTTM = ldGmtArr;
                                                            if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                                            {
                                                                ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ldGmtArr, false, false);
                                                                CurrLeg.HomeArrivalDTTM = ldHomArr;
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                                if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                    QuoteInProgress.State = CQRequestEntityState.Modified;
                                if (QuoteInProgress.CQLegs == null)
                                    QuoteInProgress.CQLegs = new List<CQLeg>();

                                #region set Crew Duty Rule
                                if (UserPrincipal.Identity._fpSettings._CQCrewDuty != null)
                                {
                                    if (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._CQCrewDuty))
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                            var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.Trim().ToUpper() == UserPrincipal.Identity._fpSettings._CQCrewDuty.Trim().ToUpper()).ToList();
                                                if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                                {
                                                    CurrLeg.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                    CharterQuoteService.CrewDutyRule CDrule = new CrewDutyRule();
                                                    CDrule.CrewDutyRulesID = CrewDtyRuleList[0].CrewDutyRulesID;
                                                    CDrule.CrewDutyRuleCD = CrewDtyRuleList[0].CrewDutyRuleCD;
                                                    hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                                    CDrule.CrewDutyRulesDescription = CrewDtyRuleList[0].CrewDutyRulesDescription;
                                                    CurrLeg.FedAviationRegNUM = CrewDtyRuleList[0].FedAviatRegNum != null ? CrewDtyRuleList[0].FedAviatRegNum : string.Empty;
                                                    CurrLeg.CrewDutyRule = CDrule;
                                                }
                                            }
                                        }

                                    }
                                }
                                #endregion

                                QuoteInProgress.CQLegs.Add(CurrLeg);
                                BindLegs(dgLegs, true);
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            }
                            break;
                    }
                    SaveQuoteinProgressToFileSession();
                    LoadFileDetails();
                }
            }
        }

        protected void UpdateLegnum(Legstate State)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(State))
            {
                List<CQLeg> CqLegs = new List<CQLeg>();
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                    {
                        CqLegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        switch (State)
                        {
                            case Legstate.Add:
                                {
                                }
                                break;
                            case Legstate.Insert:
                                {
                                    if (CqLegs != null && CqLegs.Count > 0)
                                    {
                                        foreach (CQLeg Leg in CqLegs)
                                        {
                                            if (Leg.LegNUM >= Convert.ToInt64(hdnLegNum.Value))
                                            {
                                                Leg.LegNUM = Leg.LegNUM + 1;
                                                if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                                    Leg.State = CQRequestEntityState.Modified;
                                            }
                                        }
                                    }
                                }
                                break;
                            case Legstate.Delete:
                                {
                                    Int64 DeletedLegnum = 0;
                                    //previndex = rtsLegs.Tabs[Convert.ToInt16(hdnLeg.Value) - 1].Index;
                                    DeletedLegnum = Convert.ToInt64(hdnLegNum.Value);
                                    if (DeletedLegnum != CqLegs.Count)
                                    {
                                        foreach (CQLeg Leg in CqLegs)
                                        {
                                            if (Leg.LegNUM > DeletedLegnum)
                                            {
                                                Leg.LegNUM = Leg.LegNUM - 1;
                                                if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                                    Leg.State = CQRequestEntityState.Modified;
                                            }
                                        }
                                    }
                                    else
                                        hdnLegNum.Value = (DeletedLegnum - 1).ToString();
                                }
                                break;
                        }
                    }
                }
            }
        }

        protected void DeleteLeg(Int64 LegNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegNum))
            {
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null)
                    {
                        CQLeg LegToDelete = new CQLeg();
                        if (QuoteInProgress.CQLegs.Count > 0)
                        {
                            LegToDelete = QuoteInProgress.CQLegs.Where(x => x.LegNUM == LegNum && x.IsDeleted == false).FirstOrDefault();
                            if (LegToDelete != null)
                            {
                                UpdateLegnum(Legstate.Delete);
                                if (LegToDelete.CQLegID != 0)
                                {
                                    LegToDelete.IsDeleted = true;
                                    LegToDelete.State = CQRequestEntityState.Deleted;
                                }
                                else
                                    QuoteInProgress.CQLegs.Remove(LegToDelete);
                            }
                        }
                        //BindLegs(dgLegs, true);
                        //if (dgLegs.Items.Count > 0)
                        //{
                        //    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        //}
                    }
                    SaveQuoteinProgressToFileSession();
                }
            }
        }

        protected void Legs_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        DeleteLeg(Convert.ToInt64(hdnLegNum.Value));
                        CalculateQuantity();
                        CalculateQuoteTotal();
                        BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            GridEnable(true, true, true);
                        }
                        else
                        {
                            GridEnable(true, false, false);
                            EnableLegForm(false);
                            ClearLegLabelsAndFields();
                        }
                        LoadFileDetails();
                        //EnableLegForm(false);
                        ExceptionValidation();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void Legs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                if (rblSource.SelectedValue == "1")
                                {
                                    if (string.IsNullOrEmpty(tbTailNum.Text))
                                    {
                                        RadWindowManager1.RadAlert("Tail Number Is Required before Leg entry", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, "confirmCallBackfnVendor");
                                        NoRequiredField = false;
                                    }
                                }
                                else if (rblSource.SelectedValue == "2")
                                {
                                    if (string.IsNullOrEmpty(tbTailNum.Text))
                                    {
                                        RadWindowManager1.RadAlert("Tail Number Is Required before Leg entry", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, "confirmCallBackfnTail");
                                        NoRequiredField = false;
                                    }
                                }
                                else if (rblSource.SelectedValue == "3")
                                {
                                    if (string.IsNullOrEmpty(tbTypeCode.Text))
                                    {
                                        RadWindowManager1.RadAlert("Type Code Is Required before Leg entry", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, "confirmCallBackfnTypeCode");
                                        NoRequiredField = false;
                                    }
                                }
                                if (NoRequiredField)
                                {
                                    SaveCharterQuoteToSession();
                                    dgLegs.SelectedIndexes.Clear();
                                    ClearFields();
                                    EnableForm(true);
                                    EnableLegForm(true);
                                    ExceptionValidation();
                                    AddOrInsertNewLeg(Convert.ToInt64(hdnLegNum.Value), Legstate.Add);
                                    setWarnMessageToDefault();
                                    RadAjaxManager1.FocusControl(tbArrival.ClientID);
                                    GridEnable(true, true, true);
                                }
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                EnableLegForm(true);
                                GridEnable(true, true, false);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                SaveCharterQuoteToSession();
                                dgLegs.SelectedIndexes.Clear();
                                ClearFields();
                                EnableForm(true);
                                if (FileRequest != null)
                                {
                                    if (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)
                                    {
                                        GridEnable(true, true, true);
                                    }
                                }
                                else
                                {
                                    GridEnable(true, true, true);
                                }
                                UpdateLegnum(Legstate.Insert);
                                AddOrInsertNewLeg(Convert.ToInt64(hdnLegNum.Value), Legstate.Insert);
                                setWarnMessageToDefault();
                                RadAjaxManager1.FocusControl(tbArrival.ClientID);
                                break;
                            case RadGrid.DeleteSelectedCommandName:

                                break;
                            case "RowClick":
                                GridDataItem item = (GridDataItem)e.Item;
                                if (!selectchangefired)
                                {
                                    SaveCharterQuoteToSession();
                                    CalculateQuoteTotal();
                                }
                                //CalculateQuoteTotal();
                                setWarnMessageToDefault();
                                BindLegs(dgLegs, true);
                                MarkLegItemSelected(Convert.ToInt64(item["LegNUM"].Text));
                                //EnableLegForm(false);
                                if (FileRequest != null)
                                {
                                    if (FileRequest.State != CQRequestEntityState.NoChange)
                                    {
                                        GridEnable(true, true, true);
                                    }
                                }
                                else
                                {
                                    GridEnable(true, true, true);
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Miles))
            {
                if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                    return Math.Round(Miles * 1.852M, 0);
                else
                    return Miles;
            }
        }

        protected decimal ConvertToMilesBasedOnCompanyProfile(decimal Kilometers)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Kilometers))
            {
                if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                    return Math.Round(Kilometers / 1.852M, 0);
                else
                    return Kilometers;
            }
        }

        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "0.0";
                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);
                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();
                            if (standardList.Count > 0)
                            {
                                result = Convert.ToString(hour + standardList[0].Tenths);
                            }
                        }
                    }
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;
                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }
        }

        protected string directMinstoTenths(string time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "0.0";
                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);
                    decimal decimalOfMin = 0;
                    if (minute > 0) // Added conditon to avoid divide by zero error.
                        decimalOfMin = minute / 60;
                    result = Convert.ToString(hour + decimalOfMin);
                }
                return result;
            }
        }

        protected bool ValidateTenthMinBasedonCompanyprofile(TextBox cntrlToset)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cntrlToset))
            {
                bool ValidationSucceeded = true;
                string ValtoCheck = cntrlToset.Text;
                decimal companyprofileSetting = 1;
                companyprofileSetting = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? 1 : (decimal)UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
                if (companyprofileSetting == 1)
                {
                    var count = 0;
                    var strtext = cntrlToset.Text;
                    for (var i = 0; i < strtext.Length; ++i)
                    {
                        if (strtext[i] == '.')
                            count++;
                    }
                    if (count > 1)
                    {
                        RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                        ValtoCheck = "0.0";
                        ValidationSucceeded = false;
                    }
                    if (ValtoCheck.Length > 0)
                    {
                        if (ValtoCheck.IndexOf(".") >= 0)
                        {
                            string[] strarr = ValtoCheck.Split('.');
                            if (strarr[0].Length > 2)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                ValtoCheck = "0.0";
                                ValidationSucceeded = false;
                            }
                            if (strarr[1].Length > 1)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                ValtoCheck = "0.0";
                                ValidationSucceeded = false;
                            }
                        }
                        else
                        {
                            if (ValtoCheck.Length > 2)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                ValtoCheck = "0.0";
                                ValidationSucceeded = false;
                            }
                            else
                            {
                                ValtoCheck = ValtoCheck + ".0";
                            }
                        }
                    }
                }
                else
                {
                    var hourvalue = "00";
                    var minvalue = "00";
                    if (ValtoCheck.Length > 0)
                    {
                        if (ValtoCheck.IndexOf(":") >= 0)
                        {
                            var strarr = ValtoCheck.Split(':');
                            if (strarr[0].Length <= 2)
                            {
                                hourvalue = "00" + strarr[0];
                                hourvalue = hourvalue.Substring(hourvalue.Length - 2, 2);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                ValtoCheck = "00:00";
                                ValidationSucceeded = false;
                            }
                            if (ValidationSucceeded)
                            {
                                if (strarr[1].Length <= 2)
                                {
                                    if (Convert.ToInt16(strarr[1]) > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                        hourvalue = "00";
                                        minvalue = "00";
                                        ValidationSucceeded = false;
                                    }
                                    else
                                    {
                                        minvalue = "00" + strarr[1];
                                        minvalue = minvalue.Substring(minvalue.Length - 2, 2);
                                    }
                                }
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                ValtoCheck = "00:00";
                                ValidationSucceeded = false;
                            }
                        }
                        else
                        {
                            if (ValtoCheck.Length <= 2)
                            {
                                hourvalue = "00" + ValtoCheck;
                                hourvalue = hourvalue.Substring(hourvalue.Length - 2, 2);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                ValtoCheck = "00:00";
                                ValidationSucceeded = false;
                            }
                        }
                    }
                    ValtoCheck = hourvalue + ":" + minvalue;
                }
                cntrlToset.Text = ValtoCheck;
                return ValidationSucceeded;
            }
        }

        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });
                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });
                return conversionList;
            }
        }

        private FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId GetCompany(Int64 HomeBaseID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllCompanyMasterbyHomeBaseId Companymaster = new GetAllCompanyMasterbyHomeBaseId();
                    var objCompany = objDstsvc.GetListInfoByHomeBaseId(HomeBaseID);
                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> Company = (from Comp in objCompany.EntityList
                                                                                                              where Comp.HomebaseID == HomeBaseID
                                                                                                              select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];
                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }
        }

        private FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 AirportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllAirport Airportmaster = new GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(AirportID);
                    if (objAirport.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = (from Arpt in objAirport.EntityList
                        //                                                                       where Arpt.IcaoID == DepartIcaoID
                        //                                                                       select Arpt).ToList();
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;
                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;
                }
            }
        }

        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);
                    if (objPowerSetting.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = (from aircraft in objPowerSetting.EntityList
                        //                                                                    where aircraft.AircraftID == AircraftID
                        //                                                                    select aircraft).ToList();
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;
                }
            }
        }

        protected void ResetHomebaseSettings()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdShowRequestor.Value = "false";
                hdHighlightTailno.Value = "false";
                hdIsDepartAuthReq.Value = "false";
                hdSetFarRules.Value = string.Empty;
                hdSetCrewRules.Value = "false";
                hdHomeCategory.Value = string.Empty;
            }
        }

        protected void SetWindReliability()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdSetWindReliability.Value))
                {
                    this.rblistWindReliability.SelectedValue = UserPrincipal.Identity._fpSettings._WindReliability == null ? "3" : UserPrincipal.Identity._fpSettings._WindReliability.ToString();
                    //string windReliabilitystr = string.Empty;
                    //switch (UserPrincipal.Identity._fpSettings._WindReliability == null ? "3" : UserPrincipal.Identity._fpSettings._WindReliability.ToString())
                    //{
                    //    case "1": windReliabilitystr = "50"; break;
                    //    case "2": windReliabilitystr = "75"; break;
                    //    case "3": windReliabilitystr = "85"; break;
                    //}
                    //if (!string.IsNullOrEmpty(windReliabilitystr))
                    //    this.rblistWindReliability.SelectedValue = windReliabilitystr;
                }
                else
                    rblistWindReliability.SelectedValue = "3";
            }
        }

        protected void setAircraftSettings(ref CQLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                if (QuoteInProgress.AircraftID != null)
                    hdnTypeCode.Value = QuoteInProgress.AircraftID.ToString();
                Leg.PowerSetting = "1";
                Int64 AircraftID = Convert.ToInt64(hdnTypeCode.Value == string.Empty ? "0" : hdnTypeCode.Value);
                FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                if (Aircraft != null)
                {
                    switch (Leg.PowerSetting)
                    {
                        case "1":
                            Leg.TrueAirSpeed = Aircraft.PowerSettings1TrueAirSpeed != null ? (decimal)Aircraft.PowerSettings1TrueAirSpeed : 0.0M;
                            Leg.TakeoffBIAS = Aircraft.PowerSettings1TakeOffBias != null ? (decimal)Aircraft.PowerSettings1TakeOffBias : 0.0M;
                            Leg.LandingBIAS = Aircraft.PowerSettings1LandingBias != null ? (decimal)Aircraft.PowerSettings1LandingBias : 0.0M;
                            break;
                        case "2":
                            Leg.TrueAirSpeed = Aircraft.PowerSettings2TrueAirSpeed != null ? (decimal)Aircraft.PowerSettings2TrueAirSpeed : 0.0M;
                            Leg.TakeoffBIAS = Aircraft.PowerSettings2TakeOffBias != null ? (decimal)Aircraft.PowerSettings2TakeOffBias : 0.0M;
                            Leg.LandingBIAS = Aircraft.PowerSettings2LandingBias != null ? (decimal)Aircraft.PowerSettings2LandingBias : 0.0M;
                            break;
                        case "3":
                            Leg.TrueAirSpeed = Aircraft.PowerSettings3TrueAirSpeed != null ? (decimal)Aircraft.PowerSettings3TrueAirSpeed : 0.0M;
                            Leg.TakeoffBIAS = Aircraft.PowerSettings3TakeOffBias != null ? (decimal)Aircraft.PowerSettings3TakeOffBias : 0.0M;
                            Leg.LandingBIAS = Aircraft.PowerSettings3LandingBias != null ? (decimal)Aircraft.PowerSettings3LandingBias : 0.0M;
                            break;
                        default:
                            Leg.TrueAirSpeed = 0.0M;
                            Leg.TakeoffBIAS = 0.0M;
                            Leg.LandingBIAS = 0.0M;
                            break;
                    }
                }
            }
        }

        protected void setAircraftSettings()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (QuoteInProgress.AircraftID != null)
                    hdnTypeCode.Value = QuoteInProgress.AircraftID.ToString();
                tbPower.Text = "1";
                tbTAS.Text = "0.0";
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    tbBias.Text = "00:00";
                    tbLandBias.Text = "00:00";
                }
                else
                {
                    tbBias.Text = "0.0";
                    tbLandBias.Text = "0.0";
                }
                Int64 AircraftID = Convert.ToInt64(hdnTypeCode.Value == string.Empty ? "0" : hdnTypeCode.Value);
                FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                if (Aircraft != null)
                {
                    tbPower.Text = Aircraft.PowerSetting == null ? "1" : Aircraft.PowerSetting;
                    switch (tbPower.Text)
                    {
                        case "1":
                            tbTAS.Text = Aircraft.PowerSettings1TrueAirSpeed != null ? Aircraft.PowerSettings1TrueAirSpeed.ToString() : "0.0";
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = Aircraft.PowerSettings1TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString()) : "00:00";
                                tbLandBias.Text = Aircraft.PowerSettings1LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString()) : "00:00";
                            }
                            else
                            {
                                tbBias.Text = Aircraft.PowerSettings1TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString() : "0.0";
                                tbLandBias.Text = Aircraft.PowerSettings1LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString() : "0.0";
                            }
                            break;
                        case "2":
                            tbTAS.Text = Aircraft.PowerSettings2TrueAirSpeed != null ? Aircraft.PowerSettings2TrueAirSpeed.ToString() : "0.0";
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = Aircraft.PowerSettings2TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString()) : "00:00";
                                tbLandBias.Text = Aircraft.PowerSettings2LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString()) : "00:00";
                            }
                            else
                            {
                                tbBias.Text = Aircraft.PowerSettings2TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString() : "0.0";
                                tbLandBias.Text = Aircraft.PowerSettings2LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString() : "0.0";
                            }
                            break;
                        case "3":
                            tbTAS.Text = Aircraft.PowerSettings3TrueAirSpeed != null ? Aircraft.PowerSettings3TrueAirSpeed.ToString() : "0";
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = Aircraft.PowerSettings3TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString()) : "00:00";
                                tbLandBias.Text = Aircraft.PowerSettings3LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString()) : "00:00";
                            }
                            else
                            {
                                tbBias.Text = Aircraft.PowerSettings3TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString() : "0.0";
                                tbLandBias.Text = Aircraft.PowerSettings3LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString() : "0.0";
                            }
                            break;
                        default:
                            lbcvPower.Text = System.Web.HttpUtility.HtmlEncode("Power Setting must be 1, 2, or 3");
                            tbPower.Text = "1";
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Text = "00:00";
                                tbBias.Text = "00:00";
                            }
                            else
                            {
                                tbBias.Text = "0.0";
                                tbLandBias.Text = "0.0";
                            }
                            tbTAS.Text = "0.0";
                            break;
                    }
                    // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        if (!string.IsNullOrEmpty(tbBias.Text))
                        {
                            if (tbBias.Text.IndexOf(".") < 0)
                            {
                                tbBias.Text = tbBias.Text + ".0";
                            }
                        }
                        if (!string.IsNullOrEmpty(tbLandBias.Text))
                        {
                            if (tbLandBias.Text.IndexOf(".") < 0)
                            {
                                tbLandBias.Text = tbLandBias.Text + ".0";
                            }
                        }
                    }
                }
            }
        }

        protected void SetCrewDutyTypeAndFARRules()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string CrewCode = string.Empty;
                if (!string.IsNullOrEmpty(hdnCrewRules.Value))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                        var objRetVal = objDstsvc.GetCrewDutyRuleList();
                        if (objRetVal.ReturnFlag)
                        {
                            CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRulesID.ToString().ToUpper().Trim().Equals(hdnCrewRules.Value.Trim())).ToList();
                            if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                            {
                                CrewCode = CrewDtyRuleList[0].CrewDutyRuleCD == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRuleCD;
                                tbCrewRules.Text = CrewDtyRuleList[0].CrewDutyRuleCD == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRuleCD;
                                hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                this.lbFar.Text = CrewDtyRuleList[0].FedAviatRegNum == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(CrewDtyRuleList[0].FedAviatRegNum.ToString());
                            }
                            else
                            {
                                tbCrewRules.Text = string.Empty;
                                this.lbFar.Text = string.Empty;
                                lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                hdnCrewRules.Value = string.Empty;
                            }
                        }
                        else
                        {
                            tbCrewRules.Text = string.Empty;
                            this.lbFar.Text = string.Empty;
                            lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnCrewRules.Value = string.Empty;
                        }
                    }
                }
            }
        }

        private void SetChangedCrewDutyRuleforOtherLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                    {
                        Int64 Legnum = 0;
                        Legnum = Convert.ToInt64(hdnLegNum.Value);
                        List<CQLeg> PrefLegs = new List<CQLeg>();
                        PrefLegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        //Update previous legs
                        for (int i = (int)Legnum - 1; i >= 0; i--)
                        {
                            CQLeg LegtoUpdate = new CQLeg();
                            LegtoUpdate = (from Leg in PrefLegs
                                           where Leg.LegNUM == i
                                           select Leg).SingleOrDefault();
                            if (LegtoUpdate != null && ((LegtoUpdate.IsDutyEnd != null && !(bool)LegtoUpdate.IsDutyEnd) || LegtoUpdate.IsDutyEnd == null))
                            {
                                if (LegtoUpdate.CQLegID != 0 && LegtoUpdate.State != CQRequestEntityState.Deleted)
                                    LegtoUpdate.State = CQRequestEntityState.Modified;
                                CharterQuoteService.CrewDutyRule CrewDtyRule = new CharterQuoteService.CrewDutyRule();
                                if (!string.IsNullOrEmpty(hdnCrewRules.Value) && hdnCrewRules.Value != "0")
                                {
                                    Int64 CrewDutyRulesID = 0;
                                    if (Int64.TryParse(hdnCrewRules.Value, out CrewDutyRulesID))
                                    {
                                        CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                        CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                        CrewDtyRule.CrewDutyRulesDescription = " ";
                                        LegtoUpdate.CrewDutyRule = CrewDtyRule;
                                        LegtoUpdate.CrewDutyRulesID = CrewDutyRulesID;
                                    }
                                    else
                                    {
                                        LegtoUpdate.CrewDutyRule = null;
                                        LegtoUpdate.CrewDutyRulesID = null;
                                    }
                                }
                                LegtoUpdate.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(lbFar.Text);
                            }
                            else
                                break;
                        }
                        //Update Next legs
                        if (!chkEndOfDuty.Checked)
                        {
                            for (int i = (int)Legnum + 1; i <= PrefLegs.Count; i++)
                            {
                                CQLeg LegtoUpdate = new CQLeg();
                                LegtoUpdate = (from Leg in PrefLegs
                                               where Leg.LegNUM == i
                                               select Leg).SingleOrDefault();
                                if (LegtoUpdate != null)
                                {
                                    if (LegtoUpdate.CQLegID != 0 && LegtoUpdate.State != CQRequestEntityState.Deleted)
                                        LegtoUpdate.State = CQRequestEntityState.Modified;
                                    CharterQuoteService.CrewDutyRule CrewDtyRule = new CharterQuoteService.CrewDutyRule();
                                    if (!string.IsNullOrEmpty(hdnCrewRules.Value) && hdnCrewRules.Value != "0")
                                    {
                                        Int64 CrewDutyRulesID = 0;
                                        if (Int64.TryParse(hdnCrewRules.Value, out CrewDutyRulesID))
                                        {
                                            CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                            CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                            CrewDtyRule.CrewDutyRulesDescription = " ";
                                            LegtoUpdate.CrewDutyRule = CrewDtyRule;
                                            LegtoUpdate.CrewDutyRulesID = CrewDutyRulesID;
                                        }
                                        else
                                        {
                                            LegtoUpdate.CrewDutyRule = null;
                                            LegtoUpdate.CrewDutyRulesID = null;
                                        }
                                    }
                                    LegtoUpdate.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(lbFar.Text);
                                    if (LegtoUpdate.IsDutyEnd != null && (bool)LegtoUpdate.IsDutyEnd)
                                        break;
                                }
                            }
                        }
                    }
                    SaveQuoteinProgressToFileSession();
                }
            }
        }

        private void ClearAllDatesBasedonDeptOrArrival()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdnDepart.Value))
                {
                    if (!string.IsNullOrEmpty(tbLocalDate.Text))
                    {
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime dt = new DateTime();
                            DateTime ldGmtDep = new DateTime();
                            string StartTime = rmtlocaltime.Text.Trim();
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            dt = DateTime.ParseExact(tbLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            //GMTDept
                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), dt, true, false);
                            string startHrs = "0000" + ldGmtDep.Hour.ToString();
                            string startMins = "0000" + ldGmtDep.Minute.ToString();
                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtDep);
                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                            tbHomeDate.Text = string.Empty;
                            rmtHomeTime.Text = "00:00";
                            tbArrivalHomeDate.Text = string.Empty;
                            rmtArrivalHomeTime.Text = "00:00";
                            tbArrivalDate.Text = string.Empty;
                            rmtArrivalTime.Text = "00:00";
                            tbArrivalUtcDate.Text = string.Empty;
                            rmtArrivalUtctime.Text = "00:00";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(hdnArrival.Value))
                {
                    if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                    {
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime dt = new DateTime();
                            DateTime ldGmtArr = new DateTime();
                            string StartTime = rmtArrivalTime.Text.Trim();
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            dt = DateTime.ParseExact(tbArrivalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            //GMTDept
                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, false);
                            string startHrs = "0000" + ldGmtArr.Hour.ToString();
                            string startMins = "0000" + ldGmtArr.Minute.ToString();
                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtArr);
                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                            tbArrivalHomeDate.Text = string.Empty;
                            rmtArrivalHomeTime.Text = "00:00";
                            tbHomeDate.Text = string.Empty;
                            rmtHomeTime.Text = "00:00";
                            tbLocalDate.Text = string.Empty;
                            rmtlocaltime.Text = "00:00";
                            tbUtcDate.Text = string.Empty;
                            rmtUtctime.Text = "00:00";
                        }
                    }
                }
            }
        }

        private void GridEnable(bool add, bool insert, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, insert, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, performinsertCtl, delCtl;
                    insertCtl = (LinkButton)dgLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    performinsertCtl = (LinkButton)dgLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInsert");
                    delCtl = (LinkButton)dgLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    if (IsAuthorized(Permission.CharterQuote.AddCQLeg))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQLeg))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.CharterQuote.AddCQLeg))
                    {
                        performinsertCtl.Visible = true;
                        if (insert)
                        {
                            performinsertCtl.Enabled = true;
                            //performinsertCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            performinsertCtl.Enabled = false;
                            //performinsertCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        performinsertCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected string RounsETEbasedOnCompanyProfileSettings(decimal ETEVal)
        {
            string ETEStr = string.Empty;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ETEVal))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null &&
                            UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            ETEStr = ConvertTenthsToMins(RoundElpTime((double) ETEVal).ToString());
                        }
                        else
                        {
                            ETEStr = Math.Round((decimal) ETEVal, 1).ToString();
                        }

                        ETEStr = Common.Utility.DefaultEteResult(ETEStr);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }

            return ETEStr;
        }

        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbTaxRate.Text = "0.00";
                tbPaxCount.Text = "0";
                tbArrival.Text = string.Empty;
                tbArrivalDate.Text = string.Empty;
                tbArrivalHomeDate.Text = string.Empty;
                tbArrivalUtcDate.Text = string.Empty;
                tbBias.Text = "00:00";
                tbCharges.Text = "0.00";
                tbCrewRules.Text = string.Empty;
                tbDayRoom.Text = "0";
                tbDepart.Text = string.Empty;
                tbETE.Text = string.Empty;
                tbHomeDate.Text = string.Empty;
                tbLandBias.Text = "00:00";
                tbLocalDate.Text = string.Empty;
                tbMiles.Text = "0";
                tbOverride.Text = string.Empty;
                tbPower.Text = string.Empty;
                tbPurpose.Text = string.Empty;
                tbLegRate.Text = "0.00";
                tbRON.Text = "0";
                tbTAS.Text = string.Empty;
                tbUtcDate.Text = string.Empty;
                tbWind.Text = string.Empty;
                rmtArrivalTime.Text = "00:00";
                rmtArrivalHomeTime.Text = "00:00";
                rmtArrivalUtctime.Text = "00:00";
                rmtHomeTime.Text = "00:00";
                rmtlocaltime.Text = "00:00";
                rmtUtctime.Text = "00:00";
                rblSource.SelectedValue = "2";
                tbVendor.Text = string.Empty;
                tbTailNum.Text = string.Empty;
                tbTypeCode.Text = string.Empty;
                tbDescription.Text = string.Empty;
                tbFileNumber.Text = string.Empty;
                tbQuoteNumber.Text = string.Empty;
                tbHrs.Text = string.Empty;
                tbRONCrewCharge.Text = string.Empty;
                tbLandingFees.Text = string.Empty;
                tbDailyUseAdj.Text = string.Empty;
                tbFlightCharges.Text = string.Empty;
                tbSegFeesChrg.Text = string.Empty;
                tbTotalQuote.Text = "0.00";
                tbSalesPerson.Text = string.Empty;
                tbLeadSource.Text = string.Empty;
                tbCustomer.Text = string.Empty;
                tbCustomerName.Text = string.Empty;

                //Fix for SUP-381 (3194)
                tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                hdnSPBusinessPhone.Value = string.Empty;
                hdnSPBusinessEmail.Value = string.Empty;
            }
        }

        private void EnableForm(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                tbTaxRate.Enabled = status;
                tbPaxCount.Enabled = status;
                tbArrival.Enabled = status;
                tbArrivalDate.Enabled = status;
                tbArrivalHomeDate.Enabled = status;
                tbArrivalUtcDate.Enabled = status;
                tbBias.Enabled = status;
                tbCharges.Enabled = status;
                tbCrewRules.Enabled = status;
                tbDayRoom.Enabled = status;
                tbDepart.Enabled = status;
                tbETE.Enabled = status;
                tbHomeDate.Enabled = status;
                tbLandBias.Enabled = status;
                tbLocalDate.Enabled = status;
                tbMiles.Enabled = status;
                tbOverride.Enabled = status;
                tbPower.Enabled = status;
                tbPurpose.Enabled = status;
                tbLegRate.Enabled = status;
                tbRON.Enabled = status;
                tbTAS.Enabled = status;
                tbUtcDate.Enabled = status;
                tbWind.Enabled = status;
                lbArrival.Enabled = status;
                lbArrive.Enabled = status;
                lbcvArrival.Enabled = status;
                lbcvCrewRules.Enabled = status;
                lbcvDepart.Enabled = status;
                lbcvPower.Enabled = status;
                lbDepart.Enabled = status;
                lbDepartIcao.Enabled = status;
                lbFar.Enabled = status;
                lbRest.Enabled = status;
                lbTotalDuty.Enabled = status;
                lbTotalFlight.Enabled = status;
                rmtArrivalTime.Enabled = status;
                rmtArrivalHomeTime.Enabled = status;
                rmtArrivalUtctime.Enabled = status;
                rmtHomeTime.Enabled = status;
                rmtlocaltime.Enabled = status;
                rmtUtctime.Enabled = status;
                btnClosestIcao.Enabled = status;
                btnArrival.Enabled = status;
                btnCrewRules.Enabled = status;
                btnPower.Enabled = status;
                rbDomestic.Enabled = status;
                chkPOS.Enabled = status;
                chkTaxable.Enabled = status;
                btnVendor.Enabled = status;
                btnTailNumber.Enabled = status;
                btnTailNumberSearch.Enabled = status;
                btnTypeCode.Enabled = status;
                btnSalesPerson.Enabled = status;
                btnLeadSource.Enabled = status;
                btnCustomer.Enabled = status;
                tbTotalQuote.Enabled = status;
                if (status)
                {
                    btnClosestIcao.CssClass = "browse-button";
                    btnArrival.CssClass = "browse-button";
                    btnCrewRules.CssClass = "browse-button";
                    btnPower.CssClass = "browse-button";
                    btnVendor.CssClass = "browse-button";
                    btnTailNumber.CssClass = "browse-button";
                    btnTailNumberSearch.CssClass = "charter-rate-button";
                    btnTypeCode.CssClass = "browse-button";
                    btnSalesPerson.CssClass = "browse-button";
                    btnLeadSource.CssClass = "browse-button";
                    btnCustomer.CssClass = "browse-button";
                }
                else
                {
                    btnClosestIcao.CssClass = "browse-button-disabled";
                    btnArrival.CssClass = "browse-button-disabled";
                    btnCrewRules.CssClass = "browse-button-disabled";
                    btnPower.CssClass = "browse-button-disabled";
                    btnVendor.CssClass = "browse-button-disabled";
                    btnTailNumber.CssClass = "browse-button-disabled";
                    btnTailNumberSearch.CssClass = "charter-rate-disable-button";
                    btnTypeCode.CssClass = "browse-button-disabled";
                    btnSalesPerson.CssClass = "browse-button-disabled";
                    btnLeadSource.CssClass = "browse-button-disabled";
                    btnCustomer.CssClass = "browse-button-disabled";
                }
            }
            rblSource.Enabled = status;
            tbVendor.Enabled = status;
            tbTailNum.Enabled = status;
            tbTypeCode.Enabled = status;
            tbDescription.Enabled = status;
            tbSalesPerson.Enabled = status;
            tbLeadSource.Enabled = status;
            tbCustomer.Enabled = status;
            tbCustomerName.Enabled = status;
        }

        private void ClearLabels()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvPower.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbRest.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvVendor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbleadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvCustomerName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            }
        }

        private void ClearLegLabelsAndFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbcvPower.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbRest.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                tbArrival.Text = string.Empty;
                tbDepart.Text = string.Empty;
                tbArrivalDate.Text = string.Empty;
                tbLocalDate.Text = string.Empty;
                rbDomestic.SelectedValue = "1";
                chkPOS.Checked = false;
                chkTaxable.Checked = false;
                tbMiles.Text = "0";
                tbETE.Text = string.Empty;
                tbRON.Text = "0";
                tbDayRoom.Text = "0";
                tbTaxRate.Text = "0.00";
                tbPaxCount.Text = "0";
                tbLegRate.Text = "0.00";
                hdnFirstName.Value = string.Empty;
                hdnLastName.Value = string.Empty;
                hdnMiddleName.Value = string.Empty;
            }
        }

        private void CommonCharterQuoteCalculation(string EventChange, bool isDepartOrArrival, DateTime? ChangedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EventChange, isDepartOrArrival, ChangedDate))
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {

                    if (QuoteInProgress != null)
                    {
                        if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                            SaveCharterQuoteToSession();

                        switch (EventChange)
                        {
                            case "Airport":
                                //Reset Checklist
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateMiles();
                                CalcualteBias();
                                CalculateWind();
                                CalculateETE();
                                ClearAllDatesBasedonDeptOrArrival();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "Miles":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "Power":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalcualteBias();
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "Bias":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "Wind":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "ETE":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "WindReliability":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "Date":
                                //if (QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == QuoteInProgressEntityState.Modified)
                                //{
                                //    QuoteInProgress.resetLogisticsChecklist = true;
                                //    QuoteInProgress.State = QuoteInProgressEntityState.Modified;
                                //}
                                CalculateDateTime(isDepartOrArrival, ChangedDate);
                                CalculateWind();
                                CalculateETE();
                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                {
                                    CQLeg currLeg = new CQLeg();
                                    currLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();
                                    if (currLeg != null)
                                    {
                                        currLeg.IsDepartureConfirmed = isDepartOrArrival;
                                        currLeg.IsArrivalConfirmation = !isDepartOrArrival;
                                    }
                                }
                                CalculateDateTime(isDepartOrArrival, ChangedDate);
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                            case "CrewDutyRule":
                                CalculateQuoteFARRules();
                                //CalculateFlightCost();
                                CalculateQuantity();
                                break;
                        }

                        if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                        {
                            SaveCharterQuoteToSession();
                            CalculateQuantity();
                            CalculateQuoteTotal();

                            BindLegs(dgLegs, true);
                            if (dgLegs.Items.Count > 0)
                            {
                                MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                            }
                            LoadFileDetails();
                        }
                    }

                }
            }
        }

        private int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dateval))
            {
                int lnqtr = 0;
                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from QuoteInProgressLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;
                return lnqtr;
            }
        }

        //checkthis function homebase airportid
        private void CalculateDateTime(bool isDepartureConfirmed, DateTime? ChangedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isDepartureConfirmed, ChangedDate))
            {
                double x10, x11;
                int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                DateTime Arrivaldt;

                if (ChangedDate == null)
                {
                    if (!string.IsNullOrEmpty(tbLocalDate.Text))
                    {
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime dt = new DateTime();
                            //string StartTime = rmtlocaltime.Text.Trim();
                            string StartTime = "0000";
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            if (StartHrs > 23)
                            {
                                RadWindowManager1.RadConfirm("Hour entry must be between 0 and 23", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else if (StartMts > 59)
                            {
                                RadWindowManager1.RadConfirm("Minute entry must be between 0 and 59", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            dt = DateTime.ParseExact(tbLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            ChangedDate = dt;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime dt = new DateTime();
                                string StartTime = rmtArrivalTime.Text.Trim();
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                dt = DateTime.ParseExact(tbArrivalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                ChangedDate = dt;
                                isDepartureConfirmed = false;
                            }
                        }
                    }
                }
                if (ChangedDate != null)
                {
                    DateTime EstDepartDate = (DateTime)ChangedDate;
                    //if (QuoteInProgress.EstDepartureDT != null)
                    //{
                    //    EstDepartDate = (DateTime)QuoteInProgress.EstDepartureDT;
                    //}
                    try
                    {
                        //(!string.IsNullOrEmpty(hdnHomebaseAirport.Value) && hdnHomebaseAirport.Value != "0")&& 
                        if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                            && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                            )
                        {
                            if (!string.IsNullOrEmpty(tbETE.Text))
                            {
                                string ETEstr = "0";
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                {
                                    ETEstr = ConvertMinToTenths(tbETE.Text, true, "1");
                                }
                                else
                                    ETEstr = tbETE.Text;
                                double QuoteInProgressleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));
                                x10 = (QuoteInProgressleg_elp_time * 60 * 60);
                                x11 = (QuoteInProgressleg_elp_time * 60 * 60);
                            }
                            else
                            {
                                x10 = 0;
                                x11 = 0;
                            }
                            DateTime DeptUTCdt = DateTime.MinValue;
                            DateTime ArrUTCdt = DateTime.MinValue;
                            if (isDepartureConfirmed)
                            {
                                //DeptUTCDate
                                if (!string.IsNullOrEmpty(rmtUtctime.Text))
                                {
                                    string DeptUTCTimestr = rmtUtctime.Text.Trim();
                                    DeptUTCHrst = Convert.ToInt16(DeptUTCTimestr.Substring(0, 2));
                                    DeptUTCMtstr = Convert.ToInt16(DeptUTCTimestr.Substring(2, 2));
                                    if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                    {
                                        DeptUTCdt = DateTime.ParseExact(tbUtcDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        DeptUTCdt = DeptUTCdt.AddHours(DeptUTCHrst);
                                        DeptUTCdt = DeptUTCdt.AddMinutes(DeptUTCMtstr);
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(this.tbArrival.Text))
                                {
                                    string Timestr = rmtUtctime.Text.Trim();
                                    Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    Arrivaldt = DateTime.ParseExact(tbUtcDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    Arrivaldt = Arrivaldt.AddHours(Hrst);
                                    Arrivaldt = Arrivaldt.AddMinutes(Mtstr);
                                    Arrivaldt = Arrivaldt.AddSeconds(x10);
                                    ArrUTCdt = Arrivaldt;
                                    tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", Arrivaldt);
                                    string startHrs = "0000" + Arrivaldt.Hour.ToString();
                                    string startMins = "0000" + Arrivaldt.Minute.ToString();
                                    rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbArrivalUtcDate.Text = string.Empty;
                                    rmtArrivalUtctime.Text = string.Empty;
                                }
                            }
                            else
                            {
                                //Arrival utc date
                                string ArrUTCTimestr = rmtArrivalUtctime.Text.Trim();
                                int ArrUTCHrst = Convert.ToInt16(ArrUTCTimestr.Substring(0, 2));
                                int ArrUTCMtstr = Convert.ToInt16(ArrUTCTimestr.Substring(2, 2));
                                ArrUTCdt = DateTime.ParseExact(tbArrivalUtcDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                ArrUTCdt = ArrUTCdt.AddHours(ArrUTCHrst);
                                ArrUTCdt = ArrUTCdt.AddMinutes(ArrUTCMtstr);
                                if (!string.IsNullOrWhiteSpace(this.tbDepart.Text))
                                {
                                    // tbUtcDate.Text = Convert.ToString(Convert.ToDateTime(this.tbArrivalUtcDate.Text).AddSeconds(-(x11)));
                                    string Timestr = rmtArrivalUtctime.Text.Trim();
                                    ArrivalUTChrs = Convert.ToInt16(Timestr.Substring(0, 2));
                                    ArrivalUTCmts = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(ArrivalUTChrs);
                                    dt = dt.AddMinutes(ArrivalUTCmts);
                                    dt = dt.AddSeconds(-(x11));
                                    DeptUTCdt = dt;
                                    tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", dt);
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbUtcDate.Text = string.Empty; //DepartsUTC
                                    rmtUtctime.Text = string.Empty;
                                }
                            }
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime ldLocDep = DateTime.MinValue;
                                DateTime ldLocArr = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {
                                    ldLocDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), DeptUTCdt, false, false);
                                }
                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                {
                                    ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), DeptUTCdt, false, false);
                                }
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {
                                    ldLocArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), ArrUTCdt, false, false);
                                }
                                if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                {
                                    ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdnHomebaseAirport.Value), ArrUTCdt, false, false);
                                }
                                //DateTime ltBlank = EstDepartDate;
                                if (!string.IsNullOrEmpty(this.tbDepart.Text))
                                {
                                    // DateTime Localdt = ldLocDep;
                                    string startHrs = "0000" + ldLocDep.Hour.ToString();
                                    string startMins = "0000" + ldLocDep.Minute.ToString();
                                    tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldLocDep);
                                    rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        // DateTime Homedt = ldHomDep;
                                        string HomedtstartHrs = "0000" + ldHomDep.Hour.ToString();
                                        string HomedtstartMins = "0000" + ldHomDep.Minute.ToString();
                                        tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldHomDep);
                                        rmtHomeTime.Text = HomedtstartHrs.Substring(HomedtstartHrs.Length - 2, 2) + ":" + HomedtstartMins.Substring(HomedtstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = "0000" + Localdt.Hour.ToString();
                                    string startMins = "0000" + Localdt.Minute.ToString();
                                    tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                    rmtlocaltime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                    tbUtcDate.Text = tbLocalDate.Text;
                                    rmtUtctime.Text = rmtlocaltime.Text;
                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        tbHomeDate.Text = tbLocalDate.Text;
                                        rmtHomeTime.Text = rmtlocaltime.Text;
                                    }
                                }
                                if (!string.IsNullOrEmpty(this.tbArrival.Text))
                                {
                                    // DateTime dt = ldLocArr;
                                    string startHrs = "0000" + ldLocArr.Hour.ToString();
                                    string startMins = "0000" + ldLocArr.Minute.ToString();
                                    tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldLocArr);
                                    rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        // DateTime HomeArrivaldt = ldHomArr;
                                        string HomeArrivalstartHrs = "0000" + ldHomArr.Hour.ToString();
                                        string HomeArrivalstartMins = "0000" + ldHomArr.Minute.ToString();
                                        tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldHomArr);
                                        rmtArrivalHomeTime.Text = HomeArrivalstartHrs.Substring(HomeArrivalstartHrs.Length - 2, 2) + ":" + HomeArrivalstartMins.Substring(HomeArrivalstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = "0000" + Localdt.Hour.ToString();
                                    string startMins = "0000" + Localdt.Minute.ToString();
                                    tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                    rmtArrivalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                    tbArrivalUtcDate.Text = tbArrivalDate.Text;
                                    rmtArrivalUtctime.Text = rmtArrivalTime.Text;
                                    if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                                    {
                                        tbArrivalHomeDate.Text = tbArrivalDate.Text;
                                        rmtArrivalHomeTime.Text = rmtlocaltime.Text;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Manually handled
                        if (!string.IsNullOrEmpty(this.tbArrivalDate.Text))
                        {
                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            rmtArrivalTime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                        {
                            if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                            {
                                tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                rmtArrivalHomeTime.Text = "00:00";
                            }
                        }
                        if (!string.IsNullOrEmpty(this.tbArrivalUtcDate.Text))
                        {
                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            rmtArrivalUtctime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(this.tbLocalDate.Text))
                        {
                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            rmtlocaltime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(this.tbUtcDate.Text))
                        {
                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            rmtUtctime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(hdnHomebaseAirport.Value))
                        {
                            if (!string.IsNullOrEmpty(this.tbHomeDate.Text))
                            {
                                tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                rmtHomeTime.Text = "00:00";
                            }
                        }
                    }
                }
            }
        }

        private void CalculateMiles()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    //CalculationManager CM = new CalculationManager();
                    //Function Miles Calculation
                    double Miles = 0;
                    hdnMiles.Value = "0";
                    tbMiles.Text = "0";
                    if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                    {
                        Miles = objDstsvc.GetDistance(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value));
                        tbMiles.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)Miles).ToString();
                        hdnMiles.Value = Miles.ToString();
                    }
                }

            }
        }

        private void CalculateETE()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    double ETE = 0;
                    // lnToBias, lnLndBias, lnTas
                    double Wind = 0;
                    double LandBias = 0;
                    double TAS = 0;
                    double Bias = 0;
                    double Miles = 0;
                    if (!string.IsNullOrEmpty(tbWind.Text))
                    {
                        Wind = Convert.ToDouble(tbWind.Text);
                    }
                    double LandingBias = 0;
                    string LandingBiasStr = string.Empty;
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                    else
                        LandingBiasStr = tbLandBias.Text;
                    if (double.TryParse(LandingBiasStr, out LandingBias))
                        LandBias = LandingBias;
                    else
                        LandBias = 0;
                    double tobias = 0;
                    string TakeOffBiasStr = string.Empty;
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "1");
                    }
                    else
                        TakeOffBiasStr = tbBias.Text;
                    if (double.TryParse(TakeOffBiasStr, out tobias))
                        Bias = tobias;
                    else
                        Bias = 0;
                    //if (!string.IsNullOrEmpty(tbLandBias.Text))
                    //{
                    //    LandBias = Convert.ToDouble(tbLandBias.Text);
                    //}
                    if (!string.IsNullOrEmpty(tbTAS.Text))
                    {
                        TAS = Convert.ToDouble(tbTAS.Text);
                    }
                    //if (!string.IsNullOrEmpty(tbBias.Text))
                    //{
                    //    Bias = Convert.ToDouble(tbBias.Text);
                    //}
                    //if (!string.IsNullOrEmpty(tbMiles.Text))
                    //{
                    //    Miles = Convert.ToDouble(tbMiles.Text);
                    //}
                    if (!string.IsNullOrEmpty(hdnMiles.Value))
                    {
                        Miles = Convert.ToDouble(hdnMiles.Value);
                    }
                    DateTime dt = DateTime.Now;
                    if (!string.IsNullOrEmpty(tbLocalDate.Text))
                    {
                        string StartTime = rmtlocaltime.Text.Trim();
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                        dt = DateTime.ParseExact(tbLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                    }
                    else
                    {
                        //DateTime EstDeptDt;
                        //if (QuoteInProgress.EstDepartureDT != null)
                        //    EstDeptDt = (DateTime)QuoteInProgress.EstDepartureDT;
                        //else
                        //    EstDeptDt = dt;
                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            string StartTime = rmtArrivalTime.Text.Trim();
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            dt = DateTime.ParseExact(tbArrivalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                        }
                    }
                    ETE = objDstsvc.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, dt);
                    ETE = RoundElpTime(ETE);
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        tbETE.Text = "00:00";
                        tbETE.Text = ConvertTenthsToMins(ETE.ToString());
                    }
                    else
                    {
                        tbETE.Text = "0.0";
                        tbETE.Text = Math.Round(ETE, 1).ToString();
                        if (tbETE.Text.IndexOf(".") < 0)
                            tbETE.Text = tbETE.Text + ".0";
                    }
                }
            }
        }

        private void CalculateWind()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (!string.IsNullOrEmpty(tbLocalDate.Text))
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {
                        //string StartTime = rmtlocaltime.Text.Trim();
                        string StartTime = "0000";
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                        DateTime dt = DateTime.ParseExact(tbLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                        //Function Wind Calculation
                        double Wind = 0;
                        if (
                            (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") &&
                            (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0") &&
                            (!string.IsNullOrEmpty(hdnTypeCode.Value) && hdnTypeCode.Value != "0")
                            )
                        {
                            Wind = objDstsvc.GetWind(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt32(rblistWindReliability.SelectedValue), Convert.ToInt64(hdnTypeCode.Value), GetQtr(dt).ToString());
                            tbWind.Text = Wind.ToString();
                        }
                        else
                            tbWind.Text = "0.0";
                    }
                }
                else
                    tbWind.Text = "0.0";

            }
        }

        private void CalcualteBias()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    List<double> BiasList = new List<double>();
                    if (tbPower.Text == string.Empty)
                        tbPower.Text = "1";
                    if (
                        (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                        &&
                        (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                        &&
                        (!string.IsNullOrEmpty(hdnTypeCode.Value) && hdnTypeCode.Value != "0")
                        )
                    {
                        BiasList = objDstsvc.CalcBiasTas(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt64(hdnTypeCode.Value), tbPower.Text);
                        if (BiasList != null)
                        {
                            // lnToBias, lnLndBias, lnTas
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = "00:00";
                                tbBias.Text = ConvertTenthsToMins(BiasList[0].ToString());
                            }
                            else
                            {
                                tbBias.Text = "0.0";
                                tbBias.Text = Math.Round((decimal)BiasList[0], 1).ToString();
                            }
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Text = "00:00";
                                tbLandBias.Text = ConvertTenthsToMins(BiasList[1].ToString());
                            }
                            else
                            {
                                tbLandBias.Text = "0.0";
                                tbLandBias.Text = Math.Round((decimal)BiasList[1], 1).ToString();
                            }
                            tbTAS.Text = BiasList[2].ToString();
                        }
                    }
                    else
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbBias.Text = "00:00";
                            tbLandBias.Text = "00:00";
                        }
                        else
                        {
                            tbBias.Text = "0.0";
                            tbLandBias.Text = "0.0";
                        }
                        tbTAS.Text = "0.0";
                    }

                }
                // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(tbBias.Text))
                    {
                        if (tbBias.Text.IndexOf(".") < 0)
                        {
                            tbBias.Text = tbBias.Text + ".0";
                        }
                    }
                    if (!string.IsNullOrEmpty(tbLandBias.Text))
                    {
                        if (tbLandBias.Text.IndexOf(".") < 0)
                        {
                            tbLandBias.Text = tbLandBias.Text + ".0";
                        }
                    }
                }
            }
        }

        private void CalculateQuoteFARRules()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
                    lnNeededRest, lnWorkArea;
                lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0.0;
                bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                llRProblem = llFProblem = llDutyBegin = llDProblem = false;
                DateTime ltGmtDep, ltGmtArr, llDutyend;
                Int64 lnDuty_RulesID = 0;
                string lcDuty_Rules = string.Empty;
                if ((!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0") && (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0"))
                {
                    SaveCharterQuoteToSession();
                }
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null)
                    {
                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                        lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;
                        lnDuty_Hrs = 0;
                        lnRest_Hrs = 0;
                        lnFlt_Hrs = 0;
                        //if ((DateTime)Preflegs[0].DepartureGreenwichDTTM != null)
                        //{
                        //    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                        //}
                        //else
                        //{
                        //    ltGmtDep = DateTime.MinValue;
                        //}
                        if (Preflegs[0].DepartureGreenwichDTTM != null)
                        {
                            ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                            ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                            llDutyBegin = true;
                            double lnOldDuty_hrs = -1;
                            int legcounter = 0;
                            foreach (CQLeg Leg in Preflegs)
                            {
                                if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                                {
                                    double dbElapseTime = 0.0;
                                    if (Leg.ElapseTM != null)
                                        dbElapseTime = (double)Leg.ElapseTM;
                                    //if (Leg.CrewDutyRulesID != null)
                                    //{
                                    //    dbCrewDutyRulesID = (Int64)Leg.CrewDutyRulesID;
                                    //}
                                    //if (dbCrewDutyRulesID != 0)
                                    //{
                                    double lnOverRide = 0;
                                    double lnDutyLeg_Num = 0;
                                    bool llDutyEnd = false;
                                    string FARNum = string.Empty;
                                    if (Leg.CrewDutyRulesID != null)
                                        lnDuty_RulesID = (Int64)Leg.CrewDutyRulesID;
                                    else
                                        lnDuty_RulesID = 0;
                                    if (Leg.IsDutyEnd == null)
                                        llDutyEnd = false;
                                    else
                                        llDutyEnd = (bool)Leg.IsDutyEnd;
                                    //if it is last leg then  llDutyEnd = true;
                                    if (lnEndLegNum == Leg.LegNUM)
                                        llDutyEnd = true;
                                    //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    //    tbETE.Text = ConvertTenthsToMins(ETE.ToString());
                                    //else
                                    //    tbETE.Text = ETE.ToString();
                                    lnOverRide = (double)(Leg.CQOverRide == null ? 0 : Leg.CQOverRide);
                                    lnDutyLeg_Num = (double)Leg.LegNUM;
                                    using (MasterCatalogServiceClient objectDstsvc = new MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                                      where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                                      select CrewDutyRl).ToList();
                                            if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                            {
                                                FARNum = CrewDtyRule[0].FedAviatRegNum;
                                                lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                                lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                                lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                                lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                                lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                                lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                                lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                            }
                                            else
                                            {
                                                lnDayBeg = 0;
                                                lnDayEnd = 0;
                                                lnMaxDuty = 0;
                                                lnMaxFlt = 0;
                                                lnMinRest = 8;
                                                lnRestMulti = 0;
                                                lnRestPlus = 0;
                                            }
                                        }
                                    }
                                    TimeSpan? Hoursdiff = (DateTime)Leg.DepartureGreenwichDTTM - ltGmtArr;
                                    if (Leg.ElapseTM != 0)
                                    {
                                        lnFlt_Hrs = lnFlt_Hrs + (double)Leg.ElapseTM;
                                        lnDuty_Hrs = lnDuty_Hrs + (double)Leg.ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((QuoteInProgressleg.gmtdep - ltGmtArr),0)/3600,1))
                                    }
                                    else
                                    {
                                        lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((QuoteInProgressleg.gmtdep - ltGmtArr),0)/3600,1))
                                    }
                                    llRProblem = false;
                                    if (llDutyBegin && lnOldDuty_hrs != -1)
                                    {
                                        lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                        if (lnRestMulti > 0)
                                        {
                                            //Here is the Multiple and Plus hours usage
                                            lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                            if (lnRest_Hrs < lnNeededRest && lnRest_Hrs > 0)
                                            {
                                                llRProblem = true;
                                            }
                                        }
                                        else
                                        {
                                            if (lnRest_Hrs < (lnMinRest + lnRestPlus) && lnRest_Hrs > 0)
                                            {
                                                llRProblem = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (llDutyBegin)
                                        {
                                            lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                        }
                                    }
                                    if (llDutyEnd)
                                    {
                                        lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                                    }
                                    if (lnRest_Hrs < 0)
                                        llRProblem = true;
                                    string lcCdAlert = string.Empty;
                                    if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than QuoteInProgressleg.maxflt
                                        lcCdAlert = "F";// Flight time violation and F is stored in QuoteInProgressleg.cdalert as a Flight time error
                                    else
                                        lcCdAlert = string.Empty;
                                    if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                        lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in QuoteInProgressleg.cdalert as a Duty time error
                                    // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                    if (llRProblem)
                                        lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in QuoteInProgressleg.cdalert as a Rest time error 
                                    if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                        Leg.State = CQRequestEntityState.Modified;
                                    Leg.FlightHours = (decimal)lnFlt_Hrs;
                                    Leg.DutyHours = (decimal)lnDuty_Hrs;
                                    Leg.RestHours = (decimal)lnRest_Hrs;
                                    Leg.CrewDutyAlert = lcCdAlert;
                                    //Leg.IsDutyEnd = llDutyEnd;
                                    if (Convert.ToInt64(hdnLegNum.Value) == Leg.LegNUM)
                                    {
                                        lbFar.Text = System.Web.HttpUtility.HtmlEncode(FARNum);
                                        if (lcCdAlert.LastIndexOf('D') >= 0)
                                        {
                                            lbTotalDuty.CssClass = "infored";
                                            //lbTotalDuty.ForeColor = Color.White;
                                            //lbTotalDuty.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbTotalDuty.CssClass = "infoash";
                                            //lbTotalDuty.ForeColor = Color.Black;
                                            //lbTotalDuty.BackColor = Color.Transparent;
                                        }
                                        if (lcCdAlert.LastIndexOf('F') >= 0)
                                        {
                                            lbTotalFlight.CssClass = "infored";
                                            //lbTotalFlight.ForeColor = Color.White;
                                            //lbTotalFlight.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbTotalFlight.CssClass = "infoash";
                                            //lbTotalFlight.ForeColor = Color.Black;
                                            //lbTotalFlight.BackColor = Color.Transparent;
                                        }
                                        if (lcCdAlert.LastIndexOf('R') >= 0)
                                        {
                                            lbRest.CssClass = "infored";
                                            //lbRest.ForeColor = Color.White;
                                            //lbRest.BackColor = Color.Red;
                                        }
                                        else
                                        {
                                            lbRest.CssClass = "infoash";
                                            //lbRest.ForeColor = Color.Black;
                                            //lbRest.BackColor = Color.Transparent;
                                        }
                                        //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        //{
                                        //    lbTotalDuty.Text = ConvertTenthsToMins(lnDuty_Hrs.ToString());
                                        //    lbTotalFlight.Text = ConvertTenthsToMins(lnFlt_Hrs.ToString());
                                        //    lbRest.Text = ConvertTenthsToMins(lnRest_Hrs.ToString());
                                        //}
                                        //else
                                        //{
                                        if (lnDuty_Hrs != 0)
                                        {
                                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(Math.Round(lnDuty_Hrs, 1).ToString());
                                            if (lbTotalDuty.Text.IndexOf(".") < 0)
                                            {
                                                lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode(lbTotalDuty.Text + ".0");
                                            }
                                        }
                                        else
                                            lbTotalDuty.Text = System.Web.HttpUtility.HtmlEncode("0.0");

                                        if (lnFlt_Hrs != 0)
                                        {
                                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(Math.Round(lnFlt_Hrs, 1).ToString());
                                            if (lbTotalFlight.Text.IndexOf(".") < 0)
                                            {
                                                lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode(lbTotalFlight.Text + ".0");
                                            }
                                        }
                                        else
                                            lbTotalFlight.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        if (lnRest_Hrs != 0)
                                        {
                                            lbRest.Text = Math.Round(lnRest_Hrs, 1).ToString();
                                            if (lbRest.Text.IndexOf(".") < 0)
                                            {
                                                lbRest.Text = System.Web.HttpUtility.HtmlEncode(lbRest.Text + ".0");
                                            }
                                        }
                                        else
                                            lbRest.Text = System.Web.HttpUtility.HtmlEncode("0.0");
                                        //}
                                    }
                                    lnLeg_Num = (double)Leg.LegNUM;
                                    llDutyEnd = (bool)Leg.IsDutyEnd;
                                    ltGmtArr = (DateTime)Leg.ArrivalGreenwichDTTM;
                                    if ((bool)Leg.IsDutyEnd)
                                    {
                                        llDutyBegin = true;
                                        lnFlt_Hrs = 0;
                                    }
                                    else
                                    {
                                        llDutyBegin = false;
                                    }
                                    lnOldDuty_hrs = lnDuty_Hrs;
                                    //check next leg if available do the below steps
                                    legcounter++;
                                    if (legcounter < Preflegs.Count)
                                    {
                                        if (llDutyEnd)
                                        {
                                            lnDuty_Hrs = 0;
                                            if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                            {
                                                TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;
                                                lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                                //ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                            }
                                        }
                                        else
                                        {
                                            lnRest_Hrs = 0;
                                        }
                                    }
                                    //check next leg if available do the below steps
                                    //}
                                }
                            }
                        }
                    }
                }
                SaveQuoteinProgressToFileSession();

            }
        }

        #region "text Change events"
        protected void tbDepart_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                        lbDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartIcao.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnDepart.Value = string.Empty;
                        bool isRural = false;

                        if (!string.IsNullOrEmpty(tbDepart.Text))
                        {
                            tbDepart.Text = tbDepart.Text.ToUpper();

                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbDepart.Text.ToUpper().Trim());
                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count > 0)
                                    {
                                        lbDepart.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName != null ? AirportLists[0].AirportName.ToUpper() : string.Empty);
                                        hdnDepart.Value = AirportLists[0].AirportID.ToString();
                                        lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID == null ? string.Empty : AirportLists[0].IcaoID.ToString());
                                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName == null ? string.Empty : AirportLists[0].CityName.ToString());
                                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName == null ? string.Empty : AirportLists[0].StateName.ToString());
                                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName == null ? string.Empty : AirportLists[0].CountryName.ToString());
                                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName == null ? string.Empty : AirportLists[0].AirportName.ToString());
                                        lbDepartsTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0");
                                        lbDepartsLandingbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0");
                                        lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                        if (AirportLists[0].IsRural != null && (bool)AirportLists[0].IsRural)
                                            isRural = true;

                                        #region Tooltip for AirportDetails
                                        string builder = string.Empty;
                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? AirportLists[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? AirportLists[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? AirportLists[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? AirportLists[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? AirportLists[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? AirportLists[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? AirportLists[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? AirportLists[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty);
                                        lbDepartIcao.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        lbDepart.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        #endregion
                                        //Choice --- Codes(WANTED IN FUTURE)
                                        if (Session[EQSessionKey] != null)
                                        {
                                            if (QuoteInProgress != null)
                                            {
                                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                                {
                                                    CQLeg cqLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).FirstOrDefault();
                                                    if (cqLeg != null)
                                                    {
                                                        QuoteInProgress.CQLegs[Convert.ToInt16(hdnLegNum.Value) - 1].DepartAirportChanged = true;
                                                    }
                                                }
                                            }
                                            SaveCharterQuoteToSession();
                                        }
                                        rbDomestic.SelectedValue = "1";
                                        if (!string.IsNullOrEmpty(hdnArrival.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnArrival.Value));
                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> ArrAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                ArrAirportLists = objArrRetVal.EntityList;
                                                if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                                {
                                                    if (ArrAirportLists[0].IsRural != null && (bool)ArrAirportLists[0].IsRural)
                                                        isRural = true;

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (ArrAirportLists[0].CountryID == null || AirportLists[0].CountryID == null)
                                                        {
                                                            rbDomestic.SelectedValue = "1";
                                                        }
                                                        else
                                                        {
                                                            if (ArrAirportLists[0].CountryID != null || AirportLists[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != ArrAirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }
                                                                if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //Taxable Company profile Settings
                                        if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 1)
                                        {
                                            chkTaxable.Checked = true;

                                        }
                                        else if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                                        {
                                            if (rbDomestic.SelectedValue == "1")
                                                chkTaxable.Checked = true;
                                        }
                                        else
                                            chkTaxable.Checked = false;

                                        if (chkTaxable.Checked == true)
                                        {
                                            if (isRural)
                                            {
                                                if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                    tbTaxRate.Text = UserPrincipal.Identity._fpSettings._RuralTax.ToString();
                                            }
                                            else
                                            {
                                                if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                    tbTaxRate.Text = UserPrincipal.Identity._fpSettings._CQFederalTax.ToString();
                                            }
                                            SaveQuoteinProgressToFileSession();
                                        }
                                        //Taxable Company profile Settings

                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                        if (AirportLists[0].AirportID == Convert.ToInt64(hdnDepart.Value))
                                        {
                                            // Fix for UW-1165(8179)
                                            string tooltipStr = string.Empty;

                                            if (!string.IsNullOrEmpty(AirportLists[0].Alerts)) // || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes)) 
                                            {
                                                tbDepart.ForeColor = Color.Red;
                                                string alertStr = "Alerts : \n";
                                                alertStr += AirportLists[0].Alerts;
                                                tooltipStr = alertStr;
                                            }
                                            else
                                                tbDepart.ForeColor = Color.Black;

                                            if (!string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                            {
                                                string noteStr = string.Empty;
                                                if (!string.IsNullOrEmpty(tooltipStr))
                                                    noteStr = "\n\nNotes : \n";
                                                else
                                                    noteStr = "Notes : \n";
                                                noteStr += AirportLists[0].GeneralNotes;
                                                tooltipStr += noteStr;
                                            }

                                            tbDepart.ToolTip = tooltipStr;
                                        }

                                        #endregion

                                        if (tbPower.Text != null)
                                            tbPower.Text = "1";
                                        CommonCharterQuoteCalculation("Airport", true, null);
                                    }
                                    else
                                    {
                                        lbcvDepart.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepart);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbArrive_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArrival);
                        lbArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesICAO.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrive.ToolTip = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnArrival.Value = string.Empty;
                        bool isRural = false;

                        if (!string.IsNullOrEmpty(tbArrival.Text))
                        {
                            tbArrival.Text = tbArrival.Text.ToUpper();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbArrival.Text.ToUpper().Trim());
                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count > 0)
                                    {
                                        lbArrival.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName != null ? AirportLists[0].AirportName.ToUpper() : string.Empty);
                                        hdnArrival.Value = AirportLists[0].AirportID.ToString();
                                        lbArrivesICAO.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID == null ? string.Empty : AirportLists[0].IcaoID.ToString());
                                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName == null ? string.Empty : AirportLists[0].CityName.ToString());
                                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName == null ? string.Empty : AirportLists[0].StateName.ToString());
                                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName == null ? string.Empty : AirportLists[0].CountryName.ToString());
                                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName == null ? string.Empty : AirportLists[0].AirportName.ToString());
                                        lbArrivesTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0");
                                        lbArrivesLandingbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0");
                                        lbcvArrival.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                        if (AirportLists[0].IsRural != null && (bool)AirportLists[0].IsRural)
                                            isRural = true;

                                        #region Tooltip for AirportDetails
                                        string builder = string.Empty;
                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? AirportLists[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? AirportLists[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? AirportLists[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? AirportLists[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? AirportLists[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? AirportLists[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? AirportLists[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? AirportLists[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty);
                                        lbArrivesICAO.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        lbArrive.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        #endregion
                                        //Choice --- Codes(WANTED IN FUTURE)
                                        if (Session[EQSessionKey] != null)
                                        {
                                            if (QuoteInProgress != null)
                                            {
                                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                                {
                                                    CQLeg cqLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).FirstOrDefault();
                                                    if (cqLeg != null)
                                                    {
                                                        QuoteInProgress.CQLegs[Convert.ToInt16(hdnLegNum.Value) - 1].ArrivalAirportChanged = true;
                                                    }
                                                }
                                            }
                                            SaveCharterQuoteToSession();
                                        }
                                        rbDomestic.SelectedValue = "1";
                                        if (!string.IsNullOrEmpty(hdnDepart.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnDepart.Value));
                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                DepAirportLists = objArrRetVal.EntityList;
                                                if (DepAirportLists != null && DepAirportLists.Count > 0)
                                                {
                                                    if (DepAirportLists[0].IsRural != null && (bool)DepAirportLists[0].IsRural)
                                                        isRural = true;

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (DepAirportLists[0].CountryID == null || AirportLists[0].CountryID == null)
                                                        {
                                                            rbDomestic.SelectedValue = "1";
                                                        }
                                                        else
                                                        {
                                                            if (DepAirportLists[0].CountryID != null || AirportLists[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != DepAirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }
                                                                if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                                {
                                                                    rbDomestic.SelectedValue = "2";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //Taxable Company profile Settings
                                        if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 1)
                                        {
                                            chkTaxable.Checked = true;

                                        }
                                        else if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                                        {
                                            if (rbDomestic.SelectedValue == "1")
                                                chkTaxable.Checked = true;
                                        }
                                        else
                                            chkTaxable.Checked = false;

                                        if (chkTaxable.Checked == true)
                                        {
                                            if (isRural)
                                            {
                                                if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                    tbTaxRate.Text = UserPrincipal.Identity._fpSettings._RuralTax.ToString();
                                            }
                                            else
                                            {
                                                if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                    tbTaxRate.Text = UserPrincipal.Identity._fpSettings._CQFederalTax.ToString();
                                            }
                                            SaveQuoteinProgressToFileSession();
                                        }
                                        //Taxable Company profile Settings

                                        #region Company Profile-Color changes in Airport textbox if Alerts are present

                                        if (AirportLists[0].AirportID == Convert.ToInt64(hdnArrival.Value))
                                        {
                                            // Fix for UW-1165(8179)
                                            string tooltipStr = string.Empty;
                                            if (!string.IsNullOrEmpty(AirportLists[0].Alerts)) // || !string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                            {
                                                tbArrival.ForeColor = Color.Red;
                                                string alertStr = "Alerts : \n";
                                                alertStr += AirportLists[0].Alerts;
                                                tooltipStr = alertStr;
                                            }
                                            else
                                                tbArrival.ForeColor = Color.Black;

                                            if (AirportLists[0].GeneralNotes != null)
                                            {
                                                string noteStr = string.Empty;
                                                if (!string.IsNullOrEmpty(tooltipStr))
                                                    noteStr = "\n\nNotes : \n";
                                                else
                                                    noteStr = "Notes : \n";
                                                noteStr += AirportLists[0].GeneralNotes;
                                                tooltipStr += noteStr;
                                            }

                                            tbArrival.ToolTip = tooltipStr;
                                        }

                                        #endregion

                                        if (tbPower.Text != null)
                                            tbPower.Text = "1";
                                        CommonCharterQuoteCalculation("Airport", true, null);
                                    }
                                    else
                                    {
                                        lbcvArrival.Text = "Airport Code Does Not Exist";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrival);
                                    }
                                }
                                else
                                {
                                    lbcvArrival.Text = "Airport Code Does Not Exist";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbArrival);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbMiles_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //DoConvertToMiles
                        decimal Miles = 0;
                        hdnMiles.Value = "0";
                        if (decimal.TryParse(tbMiles.Text, out Miles))
                        {
                            hdnMiles.Value = ConvertToMilesBasedOnCompanyProfile(Miles).ToString();
                        }
                        CommonCharterQuoteCalculation("Miles", true, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbBias);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void tbWind_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CommonCharterQuoteCalculation("Wind", true, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbETE);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void tbPower_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPower);
                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        if (!string.IsNullOrEmpty(tbPower.Text))
                        {
                            Int64 AircraftID = Convert.ToInt64(hdnTypeCode.Value == string.Empty ? "0" : hdnTypeCode.Value);
                            FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                            if (Aircraft != null)
                            {
                                switch (tbPower.Text)
                                {
                                    case "1":
                                        tbTAS.Text = Aircraft.PowerSettings1TrueAirSpeed != null ? Aircraft.PowerSettings1TrueAirSpeed.ToString() : "0";
                                        if (Aircraft.PowerSettings1TakeOffBias != null)
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbBias.Text = "00:00";
                                                tbBias.Text = ConvertTenthsToMins((Aircraft.PowerSettings1TakeOffBias).ToString());
                                            }
                                            else
                                            {
                                                tbBias.Text = "0.0";
                                                tbBias.Text = Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString();
                                            }
                                        tbLandBias.Text = Aircraft.PowerSettings1LandingBias != null ? Aircraft.PowerSettings1LandingBias.ToString() : "0";
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbLandBias.Text = "00:00";
                                            tbLandBias.Text = ConvertTenthsToMins((Aircraft.PowerSettings1LandingBias).ToString());
                                        }
                                        else
                                        {
                                            tbLandBias.Text = "0.0";
                                            tbLandBias.Text = Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString();
                                        }
                                        break;
                                    case "2":
                                        tbTAS.Text = Aircraft.PowerSettings2TrueAirSpeed != null ? Aircraft.PowerSettings2TrueAirSpeed.ToString() : "0";
                                        if (Aircraft.PowerSettings2TakeOffBias != null)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbBias.Text = "00:00";
                                                tbBias.Text = ConvertTenthsToMins((Aircraft.PowerSettings2TakeOffBias).ToString());
                                            }
                                            else
                                            {
                                                tbBias.Text = "0.0";
                                                tbBias.Text = Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString();
                                            }
                                        }
                                        if (Aircraft.PowerSettings2LandingBias != null)
                                        {
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbLandBias.Text = "00:00";
                                                tbLandBias.Text = ConvertTenthsToMins(Aircraft.PowerSettings2LandingBias.ToString());
                                            }
                                            else
                                            {
                                                tbLandBias.Text = "0.0";
                                                tbLandBias.Text = Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString();
                                            }
                                        }
                                        break;
                                    case "3":
                                        tbTAS.Text = Aircraft.PowerSettings3TrueAirSpeed != null ? Aircraft.PowerSettings3TrueAirSpeed.ToString() : "0";
                                        tbBias.Text = Aircraft.PowerSettings3TakeOffBias != null ? Aircraft.PowerSettings3TakeOffBias.ToString() : "0";
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbBias.Text = "00:00";
                                            tbBias.Text = ConvertTenthsToMins(Aircraft.PowerSettings3TakeOffBias.ToString());
                                        }
                                        else
                                        {
                                            tbBias.Text = "0.0";
                                            tbBias.Text = Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString();
                                        }
                                        if (Aircraft.PowerSettings3LandingBias != null)
                                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                            {
                                                tbLandBias.Text = "00:00";
                                                tbLandBias.Text = ConvertTenthsToMins(Aircraft.PowerSettings3LandingBias.ToString());
                                            }
                                            else
                                            {
                                                tbLandBias.Text = "0.0";
                                                tbLandBias.Text = Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString();
                                            }
                                        break;
                                    default:
                                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode("Power Setting must be 1, 2, or 3");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPower);
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbLandBias.Text = "00:00";
                                            tbBias.Text = "00:00";
                                        }
                                        else
                                        {
                                            tbLandBias.Text = "0.0";
                                            tbBias.Text = "0.0";
                                        }
                                        tbTAS.Text = "0.0";
                                        break;
                                }
                            }
                            else
                            {
                                switch (tbPower.Text)
                                {
                                    case "1":
                                    case "2":
                                    case "3":
                                        break;
                                    default:
                                        lbcvPower.Text = System.Web.HttpUtility.HtmlEncode("Power Setting must be 1, 2, or 3");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPower);
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            tbLandBias.Text = "00:00";
                                            tbBias.Text = "00:00";
                                        }
                                        else
                                        {
                                            tbLandBias.Text = "0.0";
                                            tbBias.Text = "0.0";
                                        }
                                        tbTAS.Text = "0.0";
                                        break;
                                }
                            }
                            CommonCharterQuoteCalculation("Power", true, null);
                        }
                        else
                        {
                            lbcvPower.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdPower.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbBias_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (sender is RadMaskedTextBox)
                        //{
                        //    if (((RadMaskedTextBox)sender).ID.ToLower() == "tbbias")
                        //    {
                        //        ValidateTenthMinBasedonCompanyprofile(((RadMaskedTextBox)sender).Text);
                        //        imgbtnTOBias.Focus();
                        //    }
                        //}
                        //if (sender is RadMaskedTextBox)
                        //{
                        //    if (((RadMaskedTextBox)sender).ID.ToLower() == "tblandbias")
                        //    {
                        //        ValidateTenthMinBasedonCompanyprofile(((RadMaskedTextBox)sender).Text);
                        //        imgbtnLandBias.Focus();
                        //    }
                        //}
                        bool validatesucceded = true;
                        if (sender is TextBox)
                        {
                            if (((TextBox)sender).ID.ToLower() == "tbbias")
                            {
                                validatesucceded = ValidateTenthMinBasedonCompanyprofile((TextBox)sender);
                                RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnTOBias);
                            }
                            else if (((TextBox)sender).ID.ToLower() == "tblandbias")
                            {
                                validatesucceded = ValidateTenthMinBasedonCompanyprofile((TextBox)sender);
                                RadAjaxManager.GetCurrent(Page).FocusControl(imgbtnTOBias);
                            }
                            else if (((TextBox)sender).ID.ToLower() == "tbtas")
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbPower);
                            }
                        }
                        if (validatesucceded)
                            CommonCharterQuoteCalculation("Bias", true, null);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbETE_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool validatesucceded = true;
                        validatesucceded = ValidateTenthMinBasedonCompanyprofile(tbETE);
                        if (validatesucceded)
                            CommonCharterQuoteCalculation("ETE", true, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(rblistWindReliability);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbLocalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbLocalDate.Text))
                        {
                            if (!IsValidDate(tbLocalDate.Text))
                            {
                                tbLocalDate.Text = string.Empty;
                                #region restore old date
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.DepartureDTTMLocal != null)
                                        {
                                            DateTime locdt = (DateTime)CurrLeg.DepartureDTTMLocal;
                                            string locstartHrs = "0000" + locdt.Hour.ToString();
                                            string locstartMins = "0000" + locdt.Minute.ToString();
                                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", CurrLeg.DepartureDTTMLocal);
                                            rmtlocaltime.Text = locstartHrs.Substring(locstartHrs.Length - 2, 2) + ":" + locstartMins.Substring(locstartMins.Length - 2, 2);
                                        }
                                        else
                                        {
                                            tbLocalDate.Text = string.Empty;
                                            rmtlocaltime.Text = "0000";
                                        }

                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                #region Date change calculation
                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        //rmtlocaltime.Text = "0000";
                                        string StartTime = rmtlocaltime.Text.Trim();
                                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                            calculate = false;
                                            rmtlocaltime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtlocaltime);
                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                            calculate = false;
                                            rmtlocaltime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtlocaltime);
                                        }
                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tblocaldate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnDepart.Value), dt, true, false);
                                            string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                            string startMins = "0000" + ldGmtDep.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtDep);
                                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtArr = CalculateGMTArr(ldGmtDep);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation

                                            if (DateValid)
                                            {


                                                CommonCharterQuoteCalculation("Date", true, dt);
                                                //Ron Calc
                                                calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                CalculateQuoteTotal();

                                                BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadFileDetails();

                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tblocaldate")
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtlocaltime);
                                                }
                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtlocaltime")
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbUtcDate);
                                                }
                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.DepartureDTTMLocal != null)
                                                        {
                                                            DateTime locdt = (DateTime)CurrLeg.DepartureDTTMLocal;
                                                            string locstartHrs = "0000" + locdt.Hour.ToString();
                                                            string locstartMins = "0000" + locdt.Minute.ToString();
                                                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", CurrLeg.DepartureDTTMLocal);
                                                            rmtlocaltime.Text = locstartHrs.Substring(locstartHrs.Length - 2, 2) + ":" + locstartMins.Substring(locstartMins.Length - 2, 2);
                                                        }
                                                        else
                                                        {
                                                            tbLocalDate.Text = string.Empty;
                                                            rmtlocaltime.Text = "0000";
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbArrivalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                        {
                            if (!IsValidDate(tbArrivalDate.Text))
                            {
                                tbArrivalDate.Text = string.Empty;
                                #region Restore old values
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.CQLegs != null)
                                    {
                                        CQLeg CurrLeg = new CQLeg();
                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                        if (CurrLeg != null && CurrLeg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime dat = (DateTime)CurrLeg.ArrivalDTTMLocal;
                                            string startHrsval = "0000" + dat.Hour.ToString();
                                            string startMinsval = "0000" + dat.Minute.ToString();
                                            tbArrivalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", CurrLeg.ArrivalDTTMLocal);
                                            rmtArrivalTime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region date change calculation
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {

                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtArr = new DateTime();
                                        DateTime ldGmtDep = new DateTime();

                                        string StartTime = rmtArrivalTime.Text.Trim();
                                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                            calculate = false;
                                            rmtArrivalTime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalTime);
                                        }
                                        if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                            calculate = false;
                                            rmtArrivalTime.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalTime);
                                        }
                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbArrivalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                            //if (sender is TextBox)
                                            //{
                                            //    if (((TextBox)sender).ID.ToLower() == "tbarrivaldate")
                                            //        Master.UpdateLegDates((int)QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            //}
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnArrival.Value), dt, true, false);
                                            string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                            string startMins = "0000" + ldGmtArr.Minute.ToString();
                                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtArr);
                                            rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                            bool DateValid = true;
                                            //Company Minron validation
                                            if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                                            {
                                                ldGmtDep = CalculateGMTDep(ldGmtArr);
                                                DateValid = CheckDatechangeValid(ldGmtDep, ldGmtArr, Convert.ToInt64(hdnLegNum.Value));
                                            }
                                            //Company minron validation
                                            if (DateValid)
                                            {

                                                CommonCharterQuoteCalculation("Date", false, dt);
                                                //Ron Calc
                                                calculateRON(Convert.ToInt64(hdnLegNum.Value), "DATE");
                                                //ShowWarnMessage();
                                                CalculateQuantity();
                                                SaveCharterQuoteToSession();
                                                CalculateQuoteTotal();

                                                BindLegs(dgLegs, true);
                                                if (dgLegs.Items.Count > 0)
                                                {
                                                    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                                }
                                                LoadFileDetails();

                                                if (sender is TextBox)
                                                {
                                                    if (((TextBox)sender).ID.ToLower() == "tbarrivaldate")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmtArrivalTime);
                                                    }
                                                }
                                                if (sender is RadMaskedTextBox)
                                                {
                                                    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtarrivaltime")
                                                    {
                                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalUtcDate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                RadWindowManager1.RadAlert("The RON days exceed 99 days.  This is NOT allowed by the system. The system will change this legs date to the previous legs date.", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);

                                                if (QuoteInProgress != null)
                                                {
                                                    if (QuoteInProgress.CQLegs != null)
                                                    {
                                                        CQLeg CurrLeg = new CQLeg();
                                                        CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == Convert.ToInt64(hdnLegNum.Value)).SingleOrDefault();
                                                        if (CurrLeg != null && CurrLeg.ArrivalGreenwichDTTM != null)
                                                        {
                                                            DateTime dat = (DateTime)CurrLeg.ArrivalGreenwichDTTM;
                                                            string startHrsval = "0000" + dat.Hour.ToString();
                                                            string startMinsval = "0000" + dat.Minute.ToString();
                                                            tbLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", CurrLeg.ArrivalGreenwichDTTM);
                                                            rmtlocaltime.Text = startHrsval.Substring(startHrsval.Length - 2, 2) + ":" + startMinsval.Substring(startMinsval.Length - 2, 2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    tbArrivalDate.Text = string.Empty;
                                }
                                #endregion
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbOverride_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbOverride.Text) && !string.IsNullOrEmpty(hdnCrewRules.Value))
                            CommonCharterQuoteCalculation("CrewDutyRule", true, null);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void tbCrewRules_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCrewRules);
                        lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbCrewRules.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                                var objRetVal = objDstsvc.GetCrewDutyRuleList();
                                if (objRetVal.ReturnFlag)
                                {
                                    CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRuleCD.ToString().ToUpper().Trim().Equals(tbCrewRules.Text.ToUpper().Trim())).ToList();
                                }
                                if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                {
                                    RadWindowManager1.RadAlert("Warning- Changing the rules on this leg will change the rules for the entire duty day", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbCrewRules.Text = CrewDtyRuleList[0].CrewDutyRuleCD;
                                    hdnCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                    lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbFar.Text = System.Web.HttpUtility.HtmlEncode(CrewDtyRuleList[0].FedAviatRegNum);
                                    SetChangedCrewDutyRuleforOtherLegs();
                                    CommonCharterQuoteCalculation("CrewDutyRule", true, null);
                                }
                                else
                                {
                                    lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode("Crew Duty Code Does Not Exist");
                                    lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewRules);
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Warning- Changing the rules on this leg will change the rules for the entire duty day", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                            lbcvCrewRules.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbFar.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnCrewRules.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void TailNum_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTailNumber);
                        lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTailNum.Value = string.Empty;
                        //bool isVendor = false;
                        if (!string.IsNullOrEmpty(tbTailNum.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList();
                                if (objRetVal.ReturnFlag)
                                {
                                    if (string.IsNullOrEmpty((hdnVendor.Value)))
                                    {
                                        Fleetlist = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNum.Text.ToUpper().Trim())).ToList();
                                    }
                                    else
                                    {
                                        Fleetlist = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNum.Text.ToUpper().Trim()) && x.VendorID == Convert.ToInt64(hdnVendor.Value)).ToList();
                                        //isVendor = true;
                                    }
                                    if (Fleetlist != null && Fleetlist.Count > 0)
                                    {
                                        tbTailNum.Text = Fleetlist[0].TailNum;
                                        hdnTailNum.Value = Fleetlist[0].FleetID.ToString();
                                        hdnTypeCode.Value = Fleetlist[0].AircraftID.ToString();
                                        tbTypeCode.Text = string.Empty;
                                        hdMaximumPassenger.Value = Fleetlist[0].MaximumPassenger != null ? Fleetlist[0].MaximumPassenger.ToString() : "0";
                                        tbTailNum.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;
                                        if (hdnTailNum.Value != null)
                                            tbTypeCode.Enabled = true; // .ReadOnly = true;
                                        else
                                            tbTypeCode.Enabled = false; //.ReadOnly = false;

                                        #region HomeBase Settings- TripNotes Color Validation
                                        if (!string.IsNullOrEmpty(hdHighlightTailno.Value))
                                        {
                                            if (hdHighlightTailno.Value.ToLower() == "true")
                                            {
                                                if (Fleetlist[0].Notes != null && !string.IsNullOrEmpty(Fleetlist[0].Notes))
                                                {
                                                    this.tbTailNum.ForeColor = Color.Red;
                                                    this.tbTailNum.ToolTip = Fleetlist[0].Notes;
                                                }
                                                else
                                                {
                                                    this.tbTailNum.ForeColor = Color.Black;
                                                    this.tbTailNum.ToolTip = string.Empty;
                                                }
                                            }
                                        }
                                        #endregion
                                        lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        #region Aircraft
                                        if (!string.IsNullOrEmpty(hdnTypeCode.Value))
                                        {
                                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                                            var objRetVal1 = objDstsvc.GetAircraftList();
                                            if (objRetVal1.ReturnFlag)
                                            {
                                                AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID.ToString() == hdnTypeCode.Value).ToList();
                                                if (AircraftList != null && AircraftList.Count > 0)
                                                {
                                                    tbTypeCode.Text = AircraftList[0].AircraftCD.ToString();
                                                    if (!string.IsNullOrEmpty(AircraftList[0].AircraftDescription))
                                                        lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription.ToString().ToUpper());
                                                    hdnTypeCode.Value = AircraftList[0].AircraftID.ToString();
                                                }
                                                else
                                                {
                                                    tbTypeCode.Text = string.Empty;
                                                    lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                    hdnTypeCode.Value = string.Empty;
                                                }
                                            }
                                        }
                                        #endregion

                                        DoLegCalculationsForDomIntlTaxable();
                                        // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteDeactivateAutoUpdateRates != null && (bool)UserPrincipal.Identity._fpSettings._IsQuoteDeactivateAutoUpdateRates)
                                        {
                                            FileRequest = (CQFile)Session[EQSessionKey];

                                            if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
                                            {
                                                Int64 QuoteNumInProgress = 1;
                                                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                                if (QuoteInProgress != null)
                                                {
                                                    if ((FileRequest.CustomerID != null && FileRequest.CustomerID != 0) && (FileRequest.UseCustomFleetCharge != null && (bool)FileRequest.UseCustomFleetCharge))
                                                    {
                                                        BindFleetChargeDetails(0, 0, (long)FileRequest.CustomerID, true);
                                                    }
                                                    else
                                                    {
                                                        if ((QuoteInProgress.CQFleetChargeDetails != null && QuoteInProgress.CQFleetChargeDetails.Count > 0) && QuoteInProgress.State == CQRequestEntityState.Modified)
                                                        {
                                                            RadWindowManager1.RadConfirm("The Quote Will be Updated With New Rates and Charges.", "confirmAutoUpdateRatesFn", 330, 100, null, "Confirmation!");
                                                        }
                                                        else
                                                            BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNum.Value) ? Convert.ToInt64(hdnTailNum.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((FileRequest.CustomerID != null && FileRequest.CustomerID != 0) && (FileRequest.UseCustomFleetCharge != null && (bool)FileRequest.UseCustomFleetCharge))
                                                BindFleetChargeDetails(0, 0, (long)FileRequest.CustomerID, true);
                                            else
                                                BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNum.Value) ? Convert.ToInt64(hdnTailNum.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);
                                        }

                                        CalculateQuantity();
                                        SaveCharterQuoteToSession();
                                        CalculateQuoteTotal();

                                        BindLegs(dgLegs, true);
                                        if (dgLegs.Items.Count > 0)
                                        {
                                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                        }
                                        LoadFileDetails();
                                    }
                                    else
                                    {
                                        this.tbTailNum.ToolTip = string.Empty;
                                        // Fix for UW-744 (7227)
                                        if (rblSource.SelectedValue == "1") // (isVendor)
                                            lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Aircraft Tail Number");
                                        else
                                            lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                        hdnTailNum.Value = string.Empty;
                                        tbTypeCode.Text = string.Empty;
                                        hdnTypeCode.Value = string.Empty;
                                        lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        tbTypeCode.Enabled = false; // .ReadOnly = false;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNum);
                                    }
                                }
                                else
                                {
                                    this.tbTailNum.ToolTip = string.Empty;
                                    // Fix for UW-744 (7227)
                                    if (rblSource.SelectedValue == "1")
                                        lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Aircraft Tail Number");
                                    else
                                        lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                    hdnTailNum.Value = string.Empty;
                                    tbTypeCode.Text = string.Empty;
                                    hdnTypeCode.Value = string.Empty;
                                    lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    tbTypeCode.Enabled = false; //.ReadOnly = false;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNum);
                                }
                            }
                        }
                        else
                        {
                            this.tbTailNum.ToolTip = string.Empty;
                            lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnTailNum.Value = string.Empty;
                            tbTypeCode.Text = string.Empty;
                            hdnTypeCode.Value = string.Empty;
                            lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            tbTypeCode.Enabled = false; //.ReadOnly = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void TypeCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTypeCode);
                        lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTypeCode.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbTypeCode.Text))
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAircraftList();
                                if (objRetVal.ReturnFlag)
                                {
                                    AircraftList = objRetVal.EntityList.Where(x => x.AircraftCD.ToUpper() == tbTypeCode.Text.ToUpper()).ToList();
                                    if (AircraftList != null && AircraftList.Count > 0)
                                    {
                                        hdnTypeCode.Value = AircraftList[0].AircraftID.ToString();
                                        tbTypeCode.Text = AircraftList[0].AircraftCD;
                                        if (!string.IsNullOrEmpty(AircraftList[0].AircraftDescription))
                                            lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription.ToString().ToUpper());

                                        BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNum.Value) ? Convert.ToInt64(hdnTailNum.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);

                                        CalculateQuantity();
                                        SaveCharterQuoteToSession();
                                        CalculateQuoteTotal();

                                        BindLegs(dgLegs, true);
                                        if (dgLegs.Items.Count > 0)
                                        {
                                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                        }
                                        LoadFileDetails();
                                    }
                                    else
                                    {
                                        hdnTypeCode.Value = string.Empty;
                                        lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode("Aircraft Type Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTypeCode);
                                    }
                                }
                                else
                                {
                                    hdnTypeCode.Value = string.Empty;
                                    lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode("Aircraft Type Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTypeCode);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        /// <summary>
        /// To validate Charterquote customer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCustomer_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCustomer);
                        lbcvCustomerName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbCustomerName.Text = string.Empty;
                        hdnCustomerID.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbCustomer.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPakMasterService.CQCustomer();
                                objCQCustomer.CQCustomerCD = tbCustomer.Text.Trim();
                                var objRetVal = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                if (objRetVal != null && objRetVal.Count > 0)
                                {
                                    hdnCustomerID.Value = objRetVal[0].CQCustomerID.ToString();
                                    tbCustomer.Text = objRetVal[0].CQCustomerCD.ToString();
                                    tbCustomerName.Text = objRetVal[0].CQCustomerName;
                                }
                                else
                                {
                                    tbCustomerName.Text = string.Empty;
                                    hdnCustomerID.Value = string.Empty;
                                    lbcvCustomerName.Text = System.Web.HttpUtility.HtmlEncode("Invalid Customer Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomer);
                                }
                            }
                        }
                        else
                        {
                            tbCustomerName.Text = string.Empty;
                            tbCustomer.Text = string.Empty;
                            hdnCustomerID.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomer);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        /// <summary>
        /// To Validate SalesPerson Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbSalesPerson_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSalesPerson.Value = string.Empty;
                        lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        string businessphone = string.Empty;
                        string businessemail = string.Empty;
                        hdnFirstName.Value = string.Empty;
                        hdnMiddleName.Value = string.Empty;
                        hdnLastName.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbSalesPerson.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetSalesPersonList().EntityList.Where(x => x.SalesPersonCD != null && x.SalesPersonCD.Trim().ToUpper().Equals(tbSalesPerson.Text.ToString().ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode("Invalid Sales Person Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbSalesPerson);
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                    SalesPersonList = (List<FlightPakMasterService.GetSalesPerson>)RetValue.ToList();
                                    hdnSalesPerson.Value = SalesPersonList[0].SalesPersonID.ToString();
                                    tbSalesPerson.Text = SalesPersonList[0].SalesPersonCD;
                                    string firstname = string.Empty;
                                    string lastname = string.Empty;
                                    string middlename = string.Empty;
                                    string salesPersonName = string.Empty;

                                    if (!string.IsNullOrEmpty(SalesPersonList[0].LastName))
                                    {
                                        lastname = SalesPersonList[0].LastName.ToString();
                                        hdnLastName.Value = SalesPersonList[0].LastName.ToString();
                                    }
                                    if (!string.IsNullOrEmpty(SalesPersonList[0].FirstName))
                                    {
                                        firstname = ", " + SalesPersonList[0].FirstName.ToString();
                                        hdnFirstName.Value = SalesPersonList[0].FirstName.ToString();
                                    }
                                    if (!string.IsNullOrEmpty(SalesPersonList[0].MiddleName))
                                    {
                                        middlename = " " + SalesPersonList[0].MiddleName.ToString();
                                        hdnMiddleName.Value = SalesPersonList[0].MiddleName.ToString();
                                    }


                                    if (!string.IsNullOrEmpty(SalesPersonList[0].PhoneNUM))
                                    {
                                        hdnSPBusinessPhone.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].PhoneNUM.ToString());
                                        businessphone = "Business Phone : " + System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].PhoneNUM.ToString());
                                    }


                                    if (!string.IsNullOrEmpty(SalesPersonList[0].EmailAddress))
                                    {
                                        hdnSPBusinessEmail.Value = System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].EmailAddress.ToString());
                                        businessemail = "Business E-Mail : " + System.Web.HttpUtility.HtmlEncode(SalesPersonList[0].EmailAddress.ToString());
                                    }

                                    salesPersonName = lastname + firstname + middlename;

                                    lbSalesPersonDesc.Text = System.Web.HttpUtility.HtmlEncode(salesPersonName.ToUpper());
                                    tbSalesPerson.ToolTip = System.Web.HttpUtility.HtmlEncode(businessphone + "\n" + businessemail);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        /// <summary>
        ///  To Validate LeadSource Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbLeadSource_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnLeadSource.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbleadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbLeadSource.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetLeadSourceList().EntityList.Where(x => x.LeadSourceCD != null && x.LeadSourceCD.Trim().ToUpper().Equals(tbLeadSource.Text.ToString().ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode("Invalid Lead Source Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbLeadSource);
                                }
                                else
                                {
                                    List<FlightPakMasterService.LeadSource> LeadSourceList = new List<FlightPakMasterService.LeadSource>();
                                    LeadSourceList = (List<FlightPakMasterService.LeadSource>)RetValue.ToList();
                                    hdnLeadSource.Value = LeadSourceList[0].LeadSourceID.ToString();
                                    tbLeadSource.Text = LeadSourceList[0].LeadSourceCD;
                                    if (LeadSourceList[0].LeadSourceDescription != null)
                                        lbleadSource.Text = System.Web.HttpUtility.HtmlEncode(LeadSourceList[0].LeadSourceDescription.ToString().ToUpper());
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        /// <summary>
        /// Method to validate Vendor Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Vendor_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnVendor);
                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvVendor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnVendor.Value = string.Empty;

                        tbTailNum.Text = string.Empty;
                        tbTailNum.Enabled = false;
                        ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");
                        ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");

                        tbTypeCode.Text = string.Empty;
                        lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbTypeCode.Enabled = false;
                        ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");

                        if (!string.IsNullOrEmpty(tbVendor.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var VendorVal = MasterService.GetVendorInfo().EntityList.Where(x => x.VendorCD.ToString().ToUpper().Trim().Equals(tbVendor.Text.ToUpper().Trim())).ToList();
                                if (VendorVal != null && VendorVal.Count > 0)
                                {
                                    hdnVendor.Value = VendorVal[0].VendorID.ToString();
                                    tbVendor.Text = VendorVal[0].VendorCD.ToString();
                                    if (VendorVal[0].Name != null && !string.IsNullOrEmpty(VendorVal[0].Name))
                                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(VendorVal[0].Name.ToUpper());

                                    if (!string.IsNullOrEmpty(hdnVendor.Value))
                                    {
                                        tbTailNum.Enabled = true; //.ReadOnly = false;
                                        ControlEnable("Button", btnTailNumber, true, "browse-button");
                                        ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");
                                    }
                                }
                                else
                                {
                                    lbcvVendor.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbVendor);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void TaxRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbTaxRate.Text))
                        {
                            bool correctFormat = true;
                            int count = 0;
                            string taxRate = tbTaxRate.Text;
                            for (int i = 0; i < taxRate.Length; ++i)
                            {
                                if (taxRate[i].ToString() == ".")
                                    count++;
                            }
                            if (count > 1)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format for Tax Rate is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                tbTaxRate.Text = "00.00";
                                correctFormat = false;
                            }
                            if (taxRate.IndexOf(".") >= 0)
                            {
                                string[] strarr = taxRate.Split('.');
                                if (strarr[0].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format for Tax Rate is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbTaxRate.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (strarr[1].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format for Tax Rate is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbTaxRate.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[0].Length == 0)
                                        taxRate = "00" + taxRate;
                                    tbTaxRate.Text = taxRate;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[1].Length == 0)
                                        taxRate = taxRate + "00";
                                    if (strarr[1].Length == 1)
                                        taxRate = taxRate + "0";
                                    tbTaxRate.Text = taxRate;
                                }
                            }
                            else
                            {
                                if (taxRate.Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format for Tax Rate is ##.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbTaxRate.Text = "00.00";
                                    correctFormat = false;
                                }
                                else
                                {
                                    tbTaxRate.Text = taxRate + ".00";
                                    SaveCharterQuoteToSession();
                                    BindLegs(dgLegs, true);
                                    if (dgLegs.Items.Count > 0)
                                    {
                                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                    }
                                    LoadFileDetails();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        #endregion
        protected void Source_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbVendor.Text = string.Empty;
                        tbTailNum.Text = string.Empty;
                        tbTypeCode.Text = string.Empty;
                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnVendor.Value = string.Empty;
                        hdnTailNum.Value = string.Empty;
                        hdnTypeCode.Value = string.Empty;
                        tbTailNum.ToolTip = string.Empty;
                        lbcvVendor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbVendor.CssClass = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTailNumber.CssClass = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTypeCode.CssClass = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        SetSourceByValue(rblSource.SelectedValue);

                        //to recalculate when source changes                        
                        //CalculateQuantity();
                        //SaveCharterQuoteToSession();
                        //CalculateQuoteTotal();

                        //BindLegs(dgLegs, true);
                        //if (dgLegs.Items.Count > 0)
                        //{
                        //    MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        //}
                        //LoadLegDetails();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        #region radio, checkbox change or click events
        protected void rblistWindReliability_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CommonCharterQuoteCalculation("WindReliability", true, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(rblistWindReliability);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void chkEndDuty_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CommonCharterQuoteCalculation("CrewDutyRule", true, null);
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPurpose);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        #endregion

        #region "button click events"
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            SaveCharterQuoteToSession();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void btPowerSelect_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgPowerSetting.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgPowerSetting.SelectedItems[0];
                            //Power,PowerSetting,TAS,HrRange,TOBias,LandBias
                            tbPower.Text = item.GetDataKeyValue("Power").ToString();
                            tbTAS.Text = item.GetDataKeyValue("TAS").ToString();
                            //tbBias.Text = item.GetDataKeyValue("HrRange").ToString();
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Text = "00:00";
                                tbBias.Text = item.GetDataKeyValue("TOBias").ToString();
                                tbBias.Text = ConvertTenthsToMins(tbBias.Text);
                            }
                            else
                            {
                                tbBias.Text = "0.0";
                                tbBias.Text = Math.Round((decimal)item.GetDataKeyValue("TOBias"), 1).ToString();
                            }
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Text = "00:00";
                                tbLandBias.Text = item.GetDataKeyValue("LandBias").ToString();
                                tbLandBias.Text = ConvertTenthsToMins(tbLandBias.Text);
                            }
                            else
                            {
                                tbLandBias.Text = "0.0";
                                tbLandBias.Text = Math.Round((decimal)item.GetDataKeyValue("LandBias"), 1).ToString();
                            }
                            rdPowerSetting.VisibleOnPageLoad = false;
                            CommonCharterQuoteCalculation("Power", true, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void btPowerCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdPowerSetting.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void btnCancelTOBias_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdTOBias.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void btnOpenTOBiasPopup_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdTOBias.VisibleOnPageLoad = true;
                        lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(tbDepart.Text);
                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> airPortLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                            if (!string.IsNullOrEmpty(hdnDepart.Value))
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnDepart.Value));
                                if (objRetVal.ReturnFlag)
                                {
                                    airPortLists = objRetVal.EntityList;
                                    if (airPortLists.Count() > 0 && airPortLists != null)
                                    {
                                        lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CityName == null ? string.Empty : airPortLists[0].CityName.ToString());
                                        lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].StateName == null ? string.Empty : airPortLists[0].StateName.ToString());
                                        lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CountryName == null ? string.Empty : airPortLists[0].CountryName.ToString());
                                        lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].AirportName == null ? string.Empty : airPortLists[0].AirportName.ToString());
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void btnSaveLandBias_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdLandBias.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        protected void btnCancelLandBias_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdLandBias.VisibleOnPageLoad = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void btnOpenLandBiasPopup_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        rdLandBias.VisibleOnPageLoad = true;
                        lbArrivesICAO.Text = System.Web.HttpUtility.HtmlEncode(tbArrival.Text);
                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> airPortLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                            if (!string.IsNullOrEmpty(hdnArrival.Value))
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnArrival.Value));
                                if (objRetVal.ReturnFlag)
                                {
                                    airPortLists = objRetVal.EntityList;
                                    if (airPortLists[0].IcaoID == lbArrivesICAO.Text)
                                    {
                                        lbArrivesCity.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CityName != null ? airPortLists[0].CityName : string.Empty);
                                        lbArrivesState.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].StateName != null ? airPortLists[0].StateName : string.Empty);
                                        lbArrivesCountry.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].CountryName != null ? airPortLists[0].CountryName : string.Empty);
                                        lbArrivesAirport.Text = System.Web.HttpUtility.HtmlEncode(airPortLists[0].AirportName != null ? airPortLists[0].AirportName : string.Empty);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        #endregion

        protected void SaveCharterQuoteToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        if (FileRequest.CQFileID != 0)
                            FileRequest.State = CQRequestEntityState.Modified;
                        else
                            FileRequest.State = CQRequestEntityState.Added;

                        #region "File Details"
                        if (!string.IsNullOrEmpty(tbDescription.Text))
                            FileRequest.CQFileDescription = tbDescription.Text;
                        else
                            FileRequest.CQFileDescription = string.Empty;
                        FileRequest.HomeBaseAirportID = UserPrincipal.Identity._airportId;
                        if (FileRequest.HomeBaseAirportID > 0)
                        {
                            hdnHomebaseAirport.Value = FileRequest.HomeBaseAirportID.ToString();
                        }
                        FileRequest.HomebaseID = UserPrincipal.Identity._fpSettings._HomebaseID;

                        //While binding get the CustomerID base on the cqCustomer
                        CharterQuoteService.CQCustomer objCustomer = new CharterQuoteService.CQCustomer();
                        if (!string.IsNullOrWhiteSpace(tbCustomerName.Text))
                        {
                            Int64 CustomerID = 0;
                            if (Int64.TryParse(hdnCustomerID.Value, out CustomerID))
                            {
                                objCustomer.CQCustomerID = Convert.ToInt64(hdnCustomerID.Value);
                                objCustomer.CQCustomerName = tbCustomerName.Text;
                                objCustomer.CQCustomerCD = tbCustomer.Text;
                                FileRequest.CQCustomer = objCustomer;
                                FileRequest.CQCustomerID = CustomerID;

                                FlightPak.Web.FlightPakMasterService.CQCustomerContact objCQCustomerContact = new FlightPak.Web.FlightPakMasterService.CQCustomerContact();
                                objCQCustomerContact.CQCustomerID = objCustomer.CQCustomerID;

                                // Fix for UW-852 (7387)
                                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.CQCustomer oCQCustomer = new FlightPakMasterService.CQCustomer();
                                    oCQCustomer.CQCustomerID = CustomerID;
                                    List<GetCQCustomerByCQCustomerID> CQCustomerList = new List<GetCQCustomerByCQCustomerID>();
                                    CQCustomerList = objMasterService.GetCQCustomerByCQCustomerID(oCQCustomer).EntityList;
                                    if (CQCustomerList != null && CQCustomerList.Count > 0)
                                        FileRequest.Credit = CQCustomerList[0].Credit;

                                    var ReturnValue = objMasterService.GetCQCustomerContactByCQCustomerID(objCQCustomerContact).EntityList; //.GetCQCustomerContactList().EntityList.Where(x => x.CQCustomerID == Convert.ToInt64(hdnCustomerID.Value) && x.IsDeleted == false && x.IsChoice == true).ToList();
                                    if (ReturnValue != null && ReturnValue.Count > 0)
                                    {
                                        //While binding get the ConatctId base on the CqContact
                                        CharterQuoteService.CQCustomerContact objCustContact = new CharterQuoteService.CQCustomerContact();

                                        List<GetCQCustomerContactByCQCustomerID> choicecqCustomerContactList = new List<GetCQCustomerContactByCQCustomerID>();
                                        choicecqCustomerContactList = ReturnValue.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                        if (choicecqCustomerContactList != null && choicecqCustomerContactList.Count > 0)
                                        {
                                            objCustContact.CQCustomerContactID = Convert.ToInt64(choicecqCustomerContactList[0].CQCustomerContactID);
                                            if (choicecqCustomerContactList[0].CQCustomerContactCD != null)
                                                objCustContact.CQCustomerContactCD = Convert.ToInt32(choicecqCustomerContactList[0].CQCustomerContactCD);
                                            objCustContact.ContactName = choicecqCustomerContactList[0].FirstName;
                                            if (choicecqCustomerContactList[0].BusinessPhoneNum != null && choicecqCustomerContactList[0].BusinessPhoneNum != "&nbsp;")
                                                objCustContact.PhoneNum = choicecqCustomerContactList[0].BusinessPhoneNum;
                                            if (choicecqCustomerContactList[0].BusinessFaxNum != null && choicecqCustomerContactList[0].BusinessFaxNum != "&nbsp;")
                                                objCustContact.FaxNum = choicecqCustomerContactList[0].BusinessFaxNum;
                                            if (choicecqCustomerContactList[0].BusinessEmailID != null && choicecqCustomerContactList[0].BusinessEmailID != "&nbsp;")
                                                objCustContact.EmailID = choicecqCustomerContactList[0].BusinessEmailID;

                                            FileRequest.CQCustomerContact = objCustContact;
                                            FileRequest.CQCustomerContactID = choicecqCustomerContactList[0].CQCustomerContactID;
                                            FileRequest.CQCustomerContactName = choicecqCustomerContactList[0].FirstName;
                                        }
                                    }
                                    else
                                    {
                                        FileRequest.CQCustomerContact = null;
                                        FileRequest.CQCustomerContactID = null;
                                    }
                                }
                            }
                        }
                        else
                        {
                            FileRequest.CQCustomer = null;
                            FileRequest.CQCustomerID = null;
                        }

                        //While binding get the SalesPerson base on the salesperson
                        CharterQuoteService.SalesPerson objSalesPerson = new CharterQuoteService.SalesPerson();
                        if (!string.IsNullOrWhiteSpace(tbSalesPerson.Text))
                        {
                            Int64 SalesID = 0;
                            if (Int64.TryParse(hdnSalesPerson.Value, out SalesID))
                            {
                                objSalesPerson.SalesPersonID = Convert.ToInt64(hdnSalesPerson.Value);
                                objSalesPerson.SalesPersonCD = tbSalesPerson.Text;
                                objSalesPerson.FirstName = hdnFirstName.Value; // System.Web.HttpUtility.HtmlDecode(lbSalesPersonDesc.Text);
                                objSalesPerson.LastName = hdnLastName.Value;
                                objSalesPerson.MiddleName = hdnMiddleName.Value;
                                objSalesPerson.PhoneNUM = hdnSPBusinessPhone.Value; //Fix for SUP-381 (3194)
                                objSalesPerson.EmailAddress = hdnSPBusinessEmail.Value; //Fix for SUP-381 (3194)
                                FileRequest.SalesPersonID = SalesID;
                                FileRequest.SalesPerson = objSalesPerson;
                            }
                        }
                        else
                        {
                            FileRequest.SalesPerson = null;
                            FileRequest.SalesPersonID = null;
                        }
                        //While binding get the LeadSource base on the LeadSource
                        CharterQuoteService.LeadSource CqLeadSource = new CharterQuoteService.LeadSource();
                        if (!string.IsNullOrWhiteSpace(tbLeadSource.Text))
                        {
                            Int64 LeadSourceID = 0;
                            if (Int64.TryParse(hdnLeadSource.Value, out LeadSourceID))
                            {
                                CqLeadSource.LeadSourceID = Convert.ToInt64(hdnLeadSource.Value);
                                CqLeadSource.LeadSourceCD = tbLeadSource.Text;
                                CqLeadSource.LeadSourceDescription = System.Web.HttpUtility.HtmlDecode(lbleadSource.Text);
                                FileRequest.LeadSourceID = LeadSourceID;
                                FileRequest.LeadSource = CqLeadSource;
                            }
                        }
                        else
                        {
                            FileRequest.LeadSource = null;
                            FileRequest.LeadSourceID = null;
                        }

                        #endregion

                        if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            if (FileRequest.CQMains[0].CQMainID != 0)
                            {
                                FileRequest.CQMains[0].State = CQRequestEntityState.Modified;
                            }
                            else
                            {
                                FileRequest.CQMains[0].State = CQRequestEntityState.Added;
                            }
                            #region "Quote Details"

                            if (!string.IsNullOrEmpty(rblSource.SelectedValue))
                                FileRequest.CQMains[0].CQSource = Convert.ToDecimal(rblSource.SelectedValue);
                            FileRequest.CQMains[0].Vendor = null;
                            FileRequest.CQMains[0].VendorID = null;
                            if (!string.IsNullOrEmpty(hdnVendor.Value))
                            {
                                FileRequest.CQMains[0].VendorID = Convert.ToInt64(hdnVendor.Value);
                                FlightPak.Web.CharterQuoteService.Vendor objVendor = new FlightPak.Web.CharterQuoteService.Vendor { VendorID = Convert.ToInt64(hdnVendor.Value), VendorCD = tbVendor.Text, Name = System.Web.HttpUtility.HtmlDecode(lbVendorDesc.Text) };
                                FileRequest.CQMains[0].Vendor = objVendor;
                            }
                            FileRequest.CQMains[0].Fleet = null;
                            FileRequest.CQMains[0].FleetID = null;
                            if (!string.IsNullOrEmpty(hdnTailNum.Value))
                            {
                                FileRequest.CQMains[0].FleetID = Convert.ToInt64(hdnTailNum.Value);
                                FlightPak.Web.CharterQuoteService.Fleet objFleet = new FlightPak.Web.CharterQuoteService.Fleet { FleetID = Convert.ToInt64(hdnTailNum.Value), TailNum = tbTailNum.Text };
                                FileRequest.CQMains[0].Fleet = objFleet;
                            }
                            FileRequest.CQMains[0].Aircraft = null;
                            FileRequest.CQMains[0].AircraftID = null;
                            if (!string.IsNullOrEmpty(hdnTypeCode.Value))
                            {
                                FileRequest.CQMains[0].AircraftID = Convert.ToInt64(hdnTypeCode.Value);
                                FlightPak.Web.CharterQuoteService.Aircraft objAircraft = new FlightPak.Web.CharterQuoteService.Aircraft { AircraftID = Convert.ToInt64(hdnTypeCode.Value), AircraftCD = tbTypeCode.Text, AircraftDescription = System.Web.HttpUtility.HtmlDecode(lbTypeCodeDes.Text) };
                                FileRequest.CQMains[0].Aircraft = objAircraft;
                            }
                            #endregion
                        }
                        else
                        {
                            if (FileRequest.CQMains == null)
                                FileRequest.CQMains = new List<CQMain>();
                            QuoteInProgress = new CQMain();
                            QuoteInProgress.QuoteNUM = 1;
                            QuoteInProgress.IsDeleted = false;
                            QuoteInProgress.State = CQRequestEntityState.Added;
                            FileRequest.CQMains.Add(QuoteInProgress);
                            Session[EQSessionKey] = FileRequest;
                        }
                        if (dgLegs.Items.Count > 0)
                        {
                            #region "Leg Details"

                            IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                            if (QuoteInProgress.CQLegs == null || QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).Count() == 0)
                            {
                                CQLeg firstleg = new CQLeg();
                                firstleg.LegNUM = 1;
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {
                                    firstleg.AAirportID = Convert.ToInt64(hdnArrival.Value);
                                    firstleg.ArrivalAirportChanged = true;
                                }
                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {
                                    firstleg.DepartAirportChanged = true;
                                }
                                firstleg.PassengerTotal = 0;
                                firstleg.IsDeleted = false;
                                if (QuoteInProgress.CQLegs == null)
                                    QuoteInProgress.CQLegs = new List<CQLeg>();
                                if (QuoteInProgress.CQMainID != 0 && QuoteInProgress.State != CQRequestEntityState.Deleted)
                                    QuoteInProgress.State = CQRequestEntityState.Modified;
                                QuoteInProgress.CQLegs.Add(firstleg);
                            }
                            CQLeg cqLeg = new CQLeg();
                            cqLeg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(hdnLegNum.Value) && x.IsDeleted == false).FirstOrDefault();

                            bool SaveToSessionLegDetails = false;

                            if (cqLeg != null)
                            {
                                if (cqLeg.CQLegID == 0)
                                    cqLeg.State = CharterQuoteService.CQRequestEntityState.Added;
                                else
                                    cqLeg.State = CharterQuoteService.CQRequestEntityState.Modified;
                                if (cqLeg.State == CQRequestEntityState.Modified)
                                {
                                    if (IsAuthorized(Permission.CharterQuote.EditCQLeg))
                                    {
                                        SaveToSessionLegDetails = true;
                                    }
                                }
                                else if (cqLeg.State == CQRequestEntityState.Added)
                                {
                                    if (IsAuthorized(Permission.CharterQuote.AddCQLeg))
                                    {
                                        SaveToSessionLegDetails = true;
                                    }
                                }
                                if (SaveToSessionLegDetails)
                                {
                                    #region "Leg Details"
                                    #region Departs
                                    CharterQuoteService.Airport DepTArr = new CharterQuoteService.Airport();
                                    if (!string.IsNullOrEmpty(hdnDepart.Value))
                                    {
                                        Int64 AirportID = 0;
                                        if (Int64.TryParse(hdnDepart.Value, out AirportID))
                                        {
                                            DepTArr.AirportID = AirportID;
                                            DepTArr.IcaoID = tbDepart.Text;
                                            DepTArr.AirportName = System.Web.HttpUtility.HtmlDecode(lbDepart.Text);
                                            cqLeg.Airport1 = DepTArr;
                                            cqLeg.DAirportID = AirportID;
                                        }
                                        else
                                        {
                                            cqLeg.Airport1 = null;
                                            cqLeg.DAirportID = null;
                                        }
                                    }
                                    else
                                    {
                                        cqLeg.Airport1 = null;
                                        cqLeg.DAirportID = null;
                                    }
                                    if (!string.IsNullOrEmpty(tbLocalDate.Text))
                                    {
                                        string StartTime = rmtlocaltime.Text.Trim();
                                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                        DateTime dt = DateTime.ParseExact(tbLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        cqLeg.DepartureDTTMLocal = dt;
                                    }
                                    else
                                    {
                                        tbLocalDate.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                    {
                                        string Timestr = rmtUtctime.Text.Trim();
                                        int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                        int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                        DateTime dt = DateTime.ParseExact(tbUtcDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        dt = dt.AddHours(Hrst);
                                        dt = dt.AddMinutes(Mtstr);
                                        cqLeg.DepartureGreenwichDTTM = dt;
                                    }
                                    else
                                    {
                                        tbUtcDate.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(tbHomeDate.Text))
                                    {
                                        string Timestr = rmtHomeTime.Text.Trim();
                                        int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                        int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                        DateTime dt = DateTime.ParseExact(tbHomeDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        dt = dt.AddHours(Hrst);
                                        dt = dt.AddMinutes(Mtstr);
                                        cqLeg.HomeDepartureDTTM = dt;
                                    }
                                    else
                                    {
                                        tbHomeDate.Text = string.Empty;
                                    }
                                    #endregion
                                    #region Arrives
                                    CharterQuoteService.Airport ArrvArr = new CharterQuoteService.Airport();
                                    if (!string.IsNullOrEmpty(hdnArrival.Value))
                                    {
                                        Int64 AirportArriveID = 0;
                                        if (Int64.TryParse(hdnArrival.Value, out AirportArriveID))
                                        {
                                            ArrvArr.AirportID = AirportArriveID;
                                            ArrvArr.IcaoID = tbArrival.Text;
                                            ArrvArr.AirportName = System.Web.HttpUtility.HtmlDecode(lbArrival.Text);
                                            cqLeg.Airport = ArrvArr;
                                            cqLeg.AAirportID = AirportArriveID;
                                        }
                                        else
                                        {
                                            cqLeg.Airport = null;
                                            cqLeg.AAirportID = null;
                                        }
                                    }
                                    else
                                    {
                                        cqLeg.Airport = null;
                                        cqLeg.AAirportID = null;
                                    }
                                    if (!string.IsNullOrEmpty(tbArrivalDate.Text))
                                    {
                                        string Timestr = rmtArrivalTime.Text.Trim();
                                        int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                        int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                        DateTime dt = DateTime.ParseExact(tbArrivalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        dt = dt.AddHours(Hrst);
                                        dt = dt.AddMinutes(Mtstr);
                                        cqLeg.ArrivalDTTMLocal = dt;
                                    }
                                    else
                                    {
                                        tbArrivalDate.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(tbArrivalUtcDate.Text))
                                    {
                                        string Timestr = rmtArrivalUtctime.Text.Trim();
                                        int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                        int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                        DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        dt = dt.AddHours(Hrst);
                                        dt = dt.AddMinutes(Mtstr);
                                        cqLeg.ArrivalGreenwichDTTM = dt;
                                    }
                                    else
                                    {
                                        tbArrivalUtcDate.Text = string.Empty;
                                    }
                                    if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                                    {
                                        string Timestr = rmtArrivalHomeTime.Text.Trim();
                                        int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                        int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                        DateTime dt = DateTime.ParseExact(tbArrivalHomeDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        dt = dt.AddHours(Hrst);
                                        dt = dt.AddMinutes(Mtstr);
                                        cqLeg.HomeArrivalDTTM = dt;
                                    }
                                    else
                                    {
                                        tbArrivalHomeDate.Text = string.Empty;
                                    }
                                    #endregion
                                    #region Distance & Time

                                    decimal distance = 0;
                                    cqLeg.Distance = 0;
                                    if (decimal.TryParse(hdnMiles.Value, out distance))
                                        cqLeg.Distance = distance;
                                    else
                                        cqLeg.Distance = 0;

                                    decimal tobias = 0;
                                    cqLeg.TakeoffBIAS = 0;
                                    string TakeOffBiasStr = string.Empty;
                                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    {
                                        TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "3");
                                    }
                                    else
                                        TakeOffBiasStr = tbBias.Text;
                                    if (decimal.TryParse(TakeOffBiasStr, out tobias))
                                        cqLeg.TakeoffBIAS = tobias;
                                    else
                                        cqLeg.TakeoffBIAS = 0;
                                    cqLeg.TrueAirSpeed = 0;
                                    decimal toTas = 0;
                                    if (decimal.TryParse(tbTAS.Text, out toTas))
                                        cqLeg.TrueAirSpeed = toTas;
                                    else
                                        cqLeg.TrueAirSpeed = 0;
                                    int windreliability = 0;
                                    cqLeg.WindReliability = 0;
                                    if (int.TryParse(rblistWindReliability.SelectedItem.Value, out windreliability))
                                        cqLeg.WindReliability = windreliability;
                                    else
                                        cqLeg.WindReliability = 0;
                                    cqLeg.PowerSetting = tbPower.Text;
                                    decimal LandingBias = 0;
                                    cqLeg.LandingBIAS = 0;
                                    string LandingBiasStr = string.Empty;
                                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    {
                                        LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                                    }
                                    else
                                        LandingBiasStr = tbLandBias.Text;
                                    if (decimal.TryParse(LandingBiasStr, out LandingBias))
                                    {
                                        cqLeg.LandingBIAS = LandingBias;
                                    }
                                    else
                                    {
                                        cqLeg.LandingBIAS = 0;
                                    }
                                    decimal WindsBoeingTable = 0;
                                    cqLeg.WindsBoeingTable = 0;
                                    if (decimal.TryParse(tbWind.Text, out WindsBoeingTable))
                                        cqLeg.WindsBoeingTable = WindsBoeingTable;
                                    else
                                        cqLeg.WindsBoeingTable = 0;
                                    cqLeg.ElapseTM = 0;
                                    decimal ElapseTime = 0;
                                    if (!string.IsNullOrEmpty(tbETE.Text))
                                    {
                                        string ETEStr = "0";
                                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                        {
                                            ETEStr = directMinstoTenths(tbETE.Text);
                                        }
                                        else
                                            ETEStr = tbETE.Text;
                                        if (decimal.TryParse(ETEStr, out ElapseTime))
                                            cqLeg.ElapseTM = ElapseTime;
                                    }
                                    #endregion
                                    #region Duty
                                    CharterQuoteService.CrewDutyRule CrewDtyRule = new CharterQuoteService.CrewDutyRule();
                                    if (!string.IsNullOrEmpty(hdnCrewRules.Value) && hdnCrewRules.Value != "0")
                                    {
                                        Int64 CrewDutyRulesID = 0;
                                        if (Int64.TryParse(hdnCrewRules.Value, out CrewDutyRulesID))
                                        {
                                            CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                            CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                            CrewDtyRule.CrewDutyRulesDescription = " ";
                                            cqLeg.CrewDutyRule = CrewDtyRule;
                                            cqLeg.CrewDutyRulesID = CrewDutyRulesID;
                                        }
                                        else
                                        {
                                            cqLeg.CrewDutyRule = null;
                                            cqLeg.CrewDutyRulesID = null;
                                        }
                                    }
                                    else
                                    {
                                        cqLeg.CrewDutyRule = null;
                                        cqLeg.CrewDutyRulesID = null;
                                    }
                                    cqLeg.IsDutyEnd = chkEndOfDuty.Checked;
                                    decimal Overrideval = 0;
                                    string CQOverRide = "0";
                                    if (!string.IsNullOrEmpty(tbOverride.Text))
                                    {
                                        CQOverRide = tbOverride.Text;
                                    }
                                    cqLeg.CQOverRide = 0;
                                    if (decimal.TryParse(CQOverRide, out Overrideval))
                                        cqLeg.CQOverRide = Overrideval;
                                    else
                                        cqLeg.CQOverRide = 0;
                                    cqLeg.FedAviationRegNUM = null;
                                    if (!string.IsNullOrEmpty(lbFar.Text))
                                        cqLeg.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(lbFar.Text);
                                    else
                                        cqLeg.FedAviationRegNUM = null;
                                    decimal DutyHours = 0;
                                    string DutyStr = "0";
                                    cqLeg.DutyHours = 0;
                                    cqLeg.DutyHours = 0;
                                    if (!string.IsNullOrEmpty(lbTotalDuty.Text))
                                    {
                                        DutyStr = System.Web.HttpUtility.HtmlDecode(lbTotalDuty.Text);
                                        if (decimal.TryParse(DutyStr, out DutyHours))
                                        {
                                            cqLeg.DutyHours = Math.Round(DutyHours, 1);
                                        }
                                    }
                                    else
                                    {
                                        cqLeg.DutyHours = 0;
                                    }
                                    decimal FlightHours = 0;
                                    string FlightHourstr = "0";
                                    cqLeg.FlightHours = 0;
                                    cqLeg.FlightHours = 0;
                                    if (!string.IsNullOrEmpty(lbTotalFlight.Text))
                                    {
                                        FlightHourstr = System.Web.HttpUtility.HtmlDecode(lbTotalFlight.Text);
                                        if (decimal.TryParse(FlightHourstr, out FlightHours))
                                            cqLeg.FlightHours = Math.Round(FlightHours, 1);
                                    }
                                    else
                                    {
                                        cqLeg.FlightHours = 0;
                                    }
                                    decimal RestHours = 0;
                                    string RestHoursrstr = "0";
                                    cqLeg.RestHours = 0;
                                    if (!string.IsNullOrEmpty(lbRest.Text))
                                    {
                                        RestHoursrstr = System.Web.HttpUtility.HtmlDecode(lbRest.Text);
                                        if (decimal.TryParse(RestHoursrstr, out RestHours))
                                            cqLeg.RestHours = Math.Round(RestHours, 2);
                                    }
                                    else
                                    {
                                        cqLeg.RestHours = 0;
                                    }
                                    #endregion
                                    #region FlightDetails
                                    decimal chargetotal = 0;
                                    cqLeg.ChargeTotal = 0;
                                    if (decimal.TryParse(tbCharges.Text, out chargetotal))
                                        cqLeg.ChargeTotal = chargetotal;
                                    else
                                        cqLeg.ChargeTotal = 0;
                                    decimal rate = 0;
                                    cqLeg.ChargeRate = 0;
                                    if (decimal.TryParse(tbLegRate.Text, out rate))
                                        cqLeg.ChargeRate = rate;
                                    else
                                        cqLeg.ChargeRate = 0;
                                    #endregion
                                    #region LastPart
                                    if (QuoteInProgress.Fleet != null)
                                    {
                                        if (QuoteInProgress.Fleet.MaximumPassenger != null && QuoteInProgress.Fleet.MaximumPassenger > 0)
                                        {
                                            if (cqLeg.State == CQRequestEntityState.Added)
                                            {
                                                Int32 SeatTotal = 0;
                                                SeatTotal = Convert.ToInt32((decimal)QuoteInProgress.Fleet.MaximumPassenger);
                                                if (cqLeg.PassengerTotal != null)
                                                    cqLeg.ReservationAvailable = SeatTotal - cqLeg.PassengerTotal;
                                                else
                                                    cqLeg.ReservationAvailable = SeatTotal;
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(tbPaxCount.Text))
                                    {
                                        cqLeg.PassengerTotal = Convert.ToDecimal(tbPaxCount.Text);
                                        cqLeg.PAXBlockedSeat = cqLeg.PassengerTotal;
                                    }
                                    cqLeg.FlightPurposeDescription = null;
                                    if (!string.IsNullOrEmpty(tbPurpose.Text))
                                        cqLeg.FlightPurposeDescription = tbPurpose.Text;
                                    cqLeg.TaxRate = 0;
                                    if (!string.IsNullOrEmpty(tbTaxRate.Text))
                                        cqLeg.TaxRate = Convert.ToDecimal(tbTaxRate.Text);
                                    #endregion
                                    #region "RON"
                                    cqLeg.DayRONCNT = 0;
                                    if (!string.IsNullOrEmpty(tbDayRoom.Text))
                                        cqLeg.DayRONCNT = Convert.ToDecimal(tbDayRoom.Text);
                                    cqLeg.RemainOverNightCNT = 0;
                                    if (!string.IsNullOrEmpty(tbRON.Text))
                                        cqLeg.RemainOverNightCNT = Convert.ToInt32(tbRON.Text);
                                    #endregion
                                    #region "Extra"
                                    if (chkPOS.Checked == true)
                                    {
                                        cqLeg.IsPositioning = true;
                                    }
                                    else
                                    {
                                        cqLeg.IsPositioning = false;
                                    }
                                    if (chkTaxable.Checked == true)
                                    {
                                        cqLeg.IsTaxable = true;
                                    }
                                    else
                                    {
                                        cqLeg.IsTaxable = false;
                                    }
                                    if (rbDomestic.SelectedValue == "1")
                                    {
                                        cqLeg.DutyTYPE = 1;
                                    }
                                    else
                                    {
                                        cqLeg.DutyTYPE = 2;
                                    }
                                    #endregion
                                    #endregion
                                }
                            }
                            #endregion
                        }
                        SaveQuoteinProgressToFileSession();
                    }
                }
            }
        }

        #region Permissions
        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.CharterQuote.ViewCQExpressQuote);
                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                // Check for Field Level Permissions
                if (IsAuthorized(Permission.CharterQuote.ViewCQExpressQuote))
                {
                    // Show read-only form
                    EnableForm(false);
                    // Check Page Controls - Visibility
                    if (IsAuthorized(Permission.CharterQuote.AddCQExpressQuote) || IsAuthorized(Permission.CharterQuote.EditCQExpressQuote))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQExpressQuote))
                    {
                        ControlVisibility("Button", btnDelete, true);
                        ControlVisibility("Button", btnDeleteFile, true);
                    }
                    // Check Page Controls - Disable/Enable
                    if (Session[EQSessionKey] != null)
                    {
                        FileRequest = (CQFile)Session[EQSessionKey];
                        if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                        {
                            EnableForm(true);
                            dgLegs.Rebind();
                            if (dgLegs.Items.Count > 0)
                                EnableLegForm(true);
                            else
                                EnableLegForm(false);
                            if (IsAuthorized(Permission.CharterQuote.AddCQQuotesOnFile) || IsAuthorized(Permission.CharterQuote.EditCQQuotesOnFile))
                            {
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                            }
                            ControlEnable("Button", btnDelete, false, "button-disable");
                            ControlEnable("Button", btnDeleteFile, false, "button-disable");
                            ControlEnable("Button", btnTransferTOCQ, false, "button-disable");
                            lnkReport.Enabled = false;
                            ControlEnable("Button", btnNew, false, "button-disable");
                            ControlEnable("Button", btnNewFile, false, "button-disable");
                            ControlEnable("Button", btnEdit, false, "button-disable");
                            ControlEnable("Button", btnEditFile, false, "button-disable");
                        }
                        else
                        {
                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDelete, true, "button");
                            ControlEnable("Button", btnDeleteFile, true, "button");
                            ControlEnable("Button", btnTransferTOCQ, true, "button");
                            lnkReport.Enabled = true;
                            ControlEnable("Button", btnNew, true, "button");
                            ControlEnable("Button", btnNewFile, true, "button");
                            ControlEnable("Button", btnEdit, true, "button");
                            ControlEnable("Button", btnEditFile, true, "button");
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnEdit, false, "button-disable");
                        ControlEnable("Button", btnTransferTOCQ, false, "button-disable");
                        lnkReport.Enabled = false;
                        ControlEnable("Button", btnDeleteFile, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                        ControlEnable("Button", btnNew, true, "button");
                        ControlEnable("Button", btnNewFile, true, "button");
                    }
                }
            }
        }

        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    default: break;
                }
            }
        }
        #endregion

        #region Button Events
        /// <summary>
        /// To tranfer records to Preflight
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTransferTOCQ_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbFileNumber.Text))
                        {
                            RadWindowManager1.RadConfirm("Transfer Selected Express Quote to Charter Manager?", "confirmTransferCallBackFn", 300, 150, null, "Transfer");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void TransferFileYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[EQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[EQSessionKey];
                            using (CharterQuoteService.CharterQuoteServiceClient CQService = new CharterQuoteService.CharterQuoteServiceClient())
                            {
                                if (FileRequest.CQFileID != null)
                                {
                                    var retval = CQService.CopyExpressQuoteDetails(FileRequest.CQFileID);
                                    if (retval.ReturnFlag)
                                    {
                                        Session.Remove(EQSessionKey);
                                        Session.Remove(EQException);
                                        Session.Remove(EQExceptionTempatelist);
                                        ClearLabels();
                                        ClearFields();
                                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"SaveCloseAndRebindExpressPage();");
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void TransferFileNo_Click(object sender, EventArgs e)
        {
            //To DO 
        }

        /// <summary>
        /// To delete the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[EQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[EQSessionKey];
                            if (FileRequest != null)
                            {
                                if (FileRequest.FileNUM != null)
                                {
                                    RadWindowManager1.RadConfirm("You are about to delete all data for Express Quote file number " + FileRequest.FileNUM + "." + " All data for this FILE will be lost. Are you sure you want to do this?", "confirmDeleteCallBackFn", 330, 110, null, "Express Quote");
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tbFileNumber.Text))
                                RadWindowManager1.RadConfirm("You are about to delete all data for Express Quote file number " + tbFileNumber.Text + "." + " All data for this FILE will be lost. Are you sure you want to do this?", "confirmDeleteCallBackFn", 330, 110, null, "Express Quote");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        /// <summary>
        /// To cancel the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                        {
                            RadWindowManager1.RadConfirm("Are you sure want to Cancel?", "confirmCancelCallBackFn", 330, 110, null, "Express Quote");
                        }
                        //if (FileRequest.CQFileID == 0)
                        //{
                        //    EnableForm(false);
                        //    EnableLegForm(false);
                        //    GridEnable(false, false, false);
                        //    ClearFields();
                        //    ClearLabels();
                        //    dgLegs.DataSource = null;
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        /// <summary>
        /// To Edit the Selected one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EnableForm(true);
                        //EnableLegForm(false);
                        GridEnable(true, true, true);
                        ControlEnable("Button", btnNew, false, "button-disable");
                        ControlEnable("Button", btnEdit, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnSave, true, "button");
                        ControlEnable("Button", btnCancel, true, "button");
                        ControlEnable("Button", btnTransferTOCQ, false, "button-disable");
                        lnkReport.Enabled = false;
                        ControlEnable("Button", btnNewFile, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                        ControlEnable("Button", btnDeleteFile, false, "button-disable");
                        if (Session[EQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[EQSessionKey];
                            FileRequest.Mode = CQRequestActionMode.Edit;
                            FileRequest.State = CQRequestEntityState.Modified;
                            if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                            {
                                if (FileRequest.CQMains[0].CQSource != null)
                                {
                                    string cqsource = string.Empty;
                                    cqsource = FileRequest.CQMains[0].CQSource.ToString();
                                    SetSourceByValue(cqsource);
                                }
                                else
                                {
                                    SetSourceByValue("2");
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        /// <summary>
        /// To save the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateEQFile();
                        if (ValidateFile)
                        {
                            if (string.IsNullOrEmpty(lbcvLeadSource.Text) && string.IsNullOrEmpty(lbcvSalesPerson.Text) && string.IsNullOrEmpty(lbcvCustomerName.Text))
                            {
                                SaveCharterQuoteToSession();
                                FileRequest = (CQFile)Session[EQSessionKey];
                                //EnableForm(false);
                                //GridEnable(false, false, false);
                                Save(FileRequest);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void New_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ControlEnable("Button", btnNew, false, "button-disable");
                        ControlEnable("Button", btnEdit, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnSave, true, "button");
                        ControlEnable("Button", btnCancel, true, "button");
                        ControlEnable("Button", btnTransferTOCQ, false, "button-disable");
                        lnkReport.Enabled = false;
                        ControlEnable("Button", btnNewFile, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                        ControlEnable("Button", btnDeleteFile, false, "button-disable");
                        Session.Remove(EQSessionKey);
                        Session.Remove(EQException);
                        BindExecption();
                        dgException.Rebind();
                        ExpandErrorPanel(false);
                        BindLegs(dgLegs, true);
                        dgLegs.SelectedIndexes.Clear();
                        ClearFields();
                        ClearLabels();
                        EnableForm(true);
                        if (dgLegs.Items.Count == 0)
                            EnableLegForm(false);
                        GridEnable(true, false, false);
                        tbTailNum.Enabled = true;
                        tbVendor.Enabled = false;
                        tbTypeCode.Enabled = false;
                        tbTailNum.Enabled = true; //.ReadOnly = false;
                        ControlEnable("Button", btnTailNumber, true, "browse-button");
                        ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");
                        ControlEnable("Button", btnVendor, false, "browse-button-disabled");
                        ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                        rblSource.SelectedValue = "2";
                        lbTailNumber.CssClass = "mnd_text";
                        SetSourceByValue(rblSource.SelectedValue);
                        FileRequest = new CQFile();
                        FileRequest.ExpressQuote = true;
                        FileRequest.IsDeleted = false;
                        FileRequest.State = CQRequestEntityState.Added;
                        FileRequest.HomeBaseAirportID = UserPrincipal.Identity._fpSettings._HomebaseID;
                        FileRequest.CQMains = new List<CQMain>();
                        QuoteInProgress = new CQMain();
                        if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                            QuoteInProgress.FederalTax = Convert.ToDecimal(UserPrincipal.Identity._fpSettings._CQFederalTax);
                        if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                            QuoteInProgress.QuoteRuralTax = Convert.ToDecimal(UserPrincipal.Identity._fpSettings._RuralTax);
                        QuoteInProgress.QuoteNUM = 1;
                        QuoteInProgress.IsDeleted = false;
                        QuoteInProgress.State = CQRequestEntityState.Added;

                        #region Load Tailnum and Typecode
                        if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList();
                                if (objRetVal.ReturnFlag)
                                {
                                    if (string.IsNullOrEmpty((hdnVendor.Value)))
                                    {
                                        Fleetlist = objRetVal.EntityList.Where(x => x.HomebaseID == (long)FileRequest.HomeBaseAirportID && x.IsInActive == false).ToList();
                                    }

                                    if (Fleetlist != null && Fleetlist.Count == 1)
                                    {
                                        tbTailNum.Text = Fleetlist[0].TailNum;
                                        hdnTailNum.Value = Fleetlist[0].FleetID.ToString();
                                        hdnTypeCode.Value = Fleetlist[0].AircraftID.ToString();

                                        QuoteInProgress.FleetID = Fleetlist[0].FleetID;
                                        CharterQuoteService.Fleet objFleet = new CharterQuoteService.Fleet { FleetID = Fleetlist[0].FleetID, TailNum = Fleetlist[0].TailNum };
                                        QuoteInProgress.Fleet = objFleet;

                                        tbTypeCode.Text = string.Empty;
                                        hdMaximumPassenger.Value = Fleetlist[0].MaximumPassenger != null ? Fleetlist[0].MaximumPassenger.ToString() : "0";
                                        tbTailNum.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;
                                        if (hdnTailNum.Value != null)
                                            tbTypeCode.Enabled = false; //.ReadOnly = true;
                                        else
                                            tbTypeCode.Enabled = true; //.ReadOnly = false;

                                        #region HomeBase Settings- TripNotes Color Validation
                                        if (!string.IsNullOrEmpty(hdHighlightTailno.Value))
                                        {
                                            if (hdHighlightTailno.Value.ToLower() == "true")
                                            {
                                                if (Fleetlist[0].Notes != null && !string.IsNullOrEmpty(Fleetlist[0].Notes))
                                                {
                                                    this.tbTailNum.ForeColor = Color.Red;
                                                    this.tbTailNum.ToolTip = Fleetlist[0].Notes;
                                                }
                                                else
                                                {
                                                    this.tbTailNum.ForeColor = Color.Black;
                                                    this.tbTailNum.ToolTip = string.Empty;
                                                }
                                            }
                                        }
                                        #endregion
                                        lbcvTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        #region Aircraft
                                        List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                                        var objRetVal1 = objDstsvc.GetAircraftList();
                                        if (objRetVal1.ReturnFlag)
                                        {
                                            AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID.ToString() == hdnTypeCode.Value).ToList();
                                            if (AircraftList != null && AircraftList.Count > 0)
                                            {
                                                tbTypeCode.Text = AircraftList[0].AircraftCD.ToString();
                                                if (!string.IsNullOrEmpty(AircraftList[0].AircraftDescription))
                                                    lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription.ToString().ToUpper());
                                                hdnTypeCode.Value = AircraftList[0].AircraftID.ToString();

                                                QuoteInProgress.AircraftID = AircraftList[0].AircraftID;
                                                CharterQuoteService.Aircraft objAircraft = new CharterQuoteService.Aircraft { AircraftID = AircraftList[0].AircraftID, AircraftCD = AircraftList[0].AircraftCD, AircraftDescription = AircraftList[0].AircraftDescription };
                                                QuoteInProgress.Aircraft = objAircraft;
                                            }
                                            else
                                            {
                                                tbTypeCode.Text = string.Empty;
                                                lbTypeCodeDes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                hdnTypeCode.Value = string.Empty;
                                            }
                                        }
                                        #endregion

                                    }

                                }

                            }
                        }
                        #endregion


                        FileRequest.CQMains.Add(QuoteInProgress);
                        FileRequest.QuoteNumInProgress = 1;
                        FileRequest.Mode = CQRequestActionMode.Edit;
                        Session[EQSessionKey] = FileRequest;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }
        #endregion

        /// <summary>
        /// Method to Set the Charter Quote Source
        /// </summary>
        /// <param name="selectedSrc"></param>
        protected void SetSourceByValue(string selectedSrc)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedSrc))
            {
                if (!string.IsNullOrEmpty(selectedSrc))
                {
                    rblSource.SelectedValue = selectedSrc;
                    if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified))
                    {
                        if (selectedSrc == "1")
                        {
                            tbTailNum.Enabled = false;
                            tbVendor.Enabled = true;
                            tbTypeCode.Enabled = false;
                            //tbTailNum.ReadOnly = true;
                            ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");
                            ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");
                            ControlEnable("Button", btnVendor, true, "browse-button");
                            ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                            lbVendor.CssClass = "mnd_text";
                            lbTailNumber.CssClass = "mnd_text";
                        }
                        else if (selectedSrc == "2")
                        {
                            tbTailNum.Enabled = true;
                            tbVendor.Enabled = false;
                            tbTypeCode.Enabled = false;
                            //tbTailNum.ReadOnly = false;
                            ControlEnable("Button", btnTailNumber, true, "browse-button");
                            ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");
                            ControlEnable("Button", btnVendor, false, "browse-button-disabled");
                            ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                            lbTailNumber.CssClass = "mnd_text";
                        }
                        else if (selectedSrc == "3")
                        {
                            tbTailNum.Enabled = false;
                            tbVendor.Enabled = false;
                            tbTypeCode.Enabled = true;
                            //tbTypeCode.ReadOnly = false;
                            ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");
                            ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");
                            ControlEnable("Button", btnVendor, false, "browse-button-disabled");
                            ControlEnable("Button", btnTypeCode, true, "browse-button");
                            lbTypeCode.CssClass = "mnd_text";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Common Method to Bind Legs Data
        /// </summary>
        /// <param name="objGrid">Pass Grid Object</param>
        /// <param name="isBind">Pass Boolean value for Rebind/Not</param>
        public void BindLegs(RadGrid objGrid, Boolean isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objGrid, isBind))
            {
                objGrid.DataSource = new string[] { };
                if (isBind)
                    objGrid.Rebind();
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    CQMain QuoteInProgress = new CQMain();
                    List<CQLeg> LegList = new List<CQLeg>();
                    if (FileRequest != null && FileRequest.CQMains != null)
                    {
                        if (FileRequest.QuoteNumInProgress == null || FileRequest.QuoteNumInProgress == 0)
                            FileRequest.QuoteNumInProgress = 1;
                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                        if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                        {
                            LegList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            if (LegList.Count > 0)
                            {
                                objGrid.DataSource = (from u in LegList
                                                      where (u.IsDeleted == false)
                                                      select
                                                       new
                                                       {
                                                           LegNum = u.LegNUM,
                                                           DepartAir = u.Airport1 != null ? u.Airport1.IcaoID == null ? string.Empty : u.Airport1.IcaoID : string.Empty,
                                                           ArrivalAir = u.Airport != null ? u.Airport.IcaoID == null ? string.Empty : u.Airport.IcaoID : string.Empty,
                                                           ETE = RounsETEbasedOnCompanyProfileSettings(u.ElapseTM != null ? (decimal)u.ElapseTM : 0),
                                                           Distance = u.Distance != null ? u.Distance.ToString() : "0",
                                                           DepartAirportID = u.DAirportID,
                                                           ArrivalAirportID = u.AAirportID,
                                                           DepartDate = u.DepartureDTTMLocal == null ? null : String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", u.DepartureDTTMLocal),
                                                           TA = u.IsDepartureConfirmed != null ? ((bool)u.IsDepartureConfirmed ? "D" : "A") : "D",
                                                           ArrivalDate = u.ArrivalDTTMLocal == null ? null : String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", u.ArrivalDTTMLocal),
                                                           POS = u.IsPositioning != null ? u.IsPositioning : false,
                                                           Tax = u.IsTaxable != null ? u.IsTaxable : false,
                                                           Rate = u.ChargeRate != null ? Math.Round((decimal)u.ChargeRate, 2).ToString() : "0.00",
                                                           Charges = u.ChargeTotal != null ? Math.Round((decimal)u.ChargeTotal, 2).ToString() : "0.00",
                                                           RON = u.RemainOverNightCNT != null ? u.RemainOverNightCNT.ToString() : "0",
                                                           DayRoom = u.DayRONCNT != null ? u.DayRONCNT.ToString() : "0",
                                                           PaxTotal = u.PassengerTotal != null ? u.PassengerTotal.ToString() : "0",
                                                           CD = u.CrewDutyAlert,
                                                           Intl = u.DutyTYPE != null ? u.DutyTYPE == 2 ? Convert.ToBoolean(u.DutyTYPE) : false : false,
                                                           TaxRate = u.TaxRate != null ? u.TaxRate.ToString() : "0",
                                                       }).OrderBy(x => x.LegNum);
                                if (isBind)
                                    objGrid.Rebind();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Common method used to save the whole trip into Database
        /// </summary>
        /// <param name="fileRequest">Pass File Object</param>
        public void Save(CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                SaveFile(fileRequest);
            }
        }

        public void SaveFile(CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                {
                    if (fileRequest.CQMains != null && fileRequest.CQMains.Count > 0)
                    {
                        foreach (CQMain cqMain in fileRequest.CQMains.Where(x => x.IsDeleted == false))
                        {
                            if (cqMain.CQLegs != null && cqMain.CQLegs.Count > 0)
                            {
                                DateTime LegMinDate = DateTime.MinValue;
                                foreach (CQLeg cqLeg in cqMain.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList())
                                {
                                    CQLeg Nextleg = new CQLeg();
                                    Nextleg = cqMain.CQLegs.Where(x => x.LegNUM == (cqLeg.LegNUM + 1)).FirstOrDefault();
                                    if (Nextleg != null)
                                    {
                                        if (Nextleg.DepartureGreenwichDTTM != null)
                                            cqLeg.NextGMTDTTM = Nextleg.DepartureGreenwichDTTM;
                                        if (Nextleg.HomeDepartureDTTM != null)
                                            cqLeg.NextHomeDTTM = Nextleg.HomeDepartureDTTM;
                                        if (Nextleg.DepartureDTTMLocal != null)
                                            cqLeg.NextLocalDTTM = Nextleg.DepartureDTTMLocal;
                                    }
                                    else
                                    {
                                        if (cqLeg.ArrivalGreenwichDTTM != null)
                                            cqLeg.NextGMTDTTM = cqLeg.ArrivalGreenwichDTTM;
                                        if (cqLeg.HomeArrivalDTTM != null)
                                            cqLeg.NextHomeDTTM = cqLeg.HomeArrivalDTTM;
                                        if (cqLeg.ArrivalDTTMLocal != null)
                                            cqLeg.NextLocalDTTM = cqLeg.ArrivalDTTMLocal;
                                    }
                                    if (cqLeg.DepartureDTTMLocal != null)
                                    {
                                        if (LegMinDate == DateTime.MinValue)
                                            LegMinDate = (DateTime)cqLeg.DepartureDTTMLocal;
                                        if (LegMinDate > (DateTime)cqLeg.DepartureDTTMLocal)
                                            LegMinDate = (DateTime)cqLeg.DepartureDTTMLocal;
                                    }
                                }
                                if (LegMinDate != DateTime.MinValue)
                                {
                                    string dtstr = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LegMinDate);
                                    DateTime dt = DateTime.ParseExact(dtstr, UserPrincipal.Identity._fpSettings._ApplicationDateFormat, CultureInfo.InvariantCulture);
                                    fileRequest.EstDepartureDT = dt;
                                }
                            }
                        }
                    }

                    #region Exception Validation
                    List<CQException> eSeverityException = new List<CQException>();
                    List<FlightPak.Web.CharterQuoteService.ExceptionTemplate> ExceptionTemplateList = new List<FlightPak.Web.CharterQuoteService.ExceptionTemplate>();
                    ExceptionTemplateList = GetExceptionTemplateList();
                    fileRequest.AllowFileToSave = true;
                    ExceptionList = null;
                    if (ExceptionTemplateList != null && ExceptionTemplateList.Count > 0)
                    {
                        if (fileRequest != null)
                        {
                            if (fileRequest.CQMains != null)
                            {
                                CQMain cqMain = FileRequest.CQMains.Where(x => x.IsDeleted == false && x.QuoteNUM == 1).FirstOrDefault();
                                if (cqMain != null)
                                {
                                    var ExceptionlistforMandate = Service.ValidateQuoteforMandatoryExcep(cqMain);
                                    if (ExceptionlistforMandate.ReturnFlag)
                                    {
                                        if (ExceptionList == null)
                                            ExceptionList = new List<CharterQuoteService.CQException>();
                                        ExceptionList.AddRange(ExceptionlistforMandate.EntityList);
                                        if (ExceptionList != null && ExceptionList.Count > 0)
                                        {
                                            eSeverityException = (from Exp in ExceptionList
                                                                  join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                                  where ExpTempl.Severity == 1
                                                                  select Exp).ToList();
                                            if (eSeverityException != null && eSeverityException.Count > 0)
                                            {
                                                fileRequest.AllowFileToSave = false;
                                                fileRequest.QuoteNumInProgress = (int)cqMain.QuoteNUM;
                                            }
                                        }
                                        else
                                        {
                                            dgException.Rebind();
                                        }
                                    }
                                }
                            }
                        }
                        if (!fileRequest.AllowFileToSave)
                        {
                            List<CQException> OldExceptionList = new List<CQException>();
                            OldExceptionList = (List<CQException>)Session[EQException];
                            if (OldExceptionList != null && OldExceptionList.Count > 0)
                            {
                                foreach (CQException OldMandatoryException in OldExceptionList.ToList())
                                {
                                    if (OldMandatoryException.CQExceptionID == 0)
                                        OldExceptionList.Remove(OldMandatoryException);
                                }
                            }
                            if (ExceptionList != null && ExceptionList.Count > 0)
                            {
                                if (OldExceptionList == null)
                                    OldExceptionList = new List<CQException>();
                                OldExceptionList.AddRange(ExceptionList);
                            }
                            Session[EQException] = OldExceptionList;
                            //RadWindowManager1.RadAlert("Express Quote File has mandatory exceptions, File is not Saved", 330, 110, "Express Quote", "");
                            dgException.Rebind();
                        }
                    }
                    #endregion
                    if (fileRequest.AllowFileToSave)
                    {
                        ClearChildProperties(ref fileRequest);
                        var ReturnValue = Service.SaveFileRequest(fileRequest);
                        if (ReturnValue.ReturnFlag == true)
                        {
                            FileRequest = ReturnValue.EntityInfo;
                            Session.Remove(EQSessionKey);
                            Session.Remove(EQException);
                            if (FileRequest != null)
                            {
                                CQMain Quoteinprogress = new CQMain();
                                Quoteinprogress = getQuoteInProgress(FileRequest);
                                if (Quoteinprogress != null)
                                {
                                    var objRetval = Service.GetQuoteExceptionList(Quoteinprogress.CQMainID);
                                    if (objRetval.ReturnFlag)
                                    {
                                        Session[EQException] = objRetval.EntityList;
                                    }

                                }
                                SetCQModeToNoChange(ref FileRequest);
                                Session[EQSessionKey] = FileRequest;
                                FileRequest = null;
                                BindExecption();
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"SaveCloseAndRebindPage();");
                            }
                        }
                        else
                        {
                            if (ReturnValue.ReturnFlag == false && !string.IsNullOrEmpty(ReturnValue.ErrorMessage))
                            {
                                RadAjaxManager1.ResponseScripts.Add(@"jAlert('" + ReturnValue.ErrorMessage + " ','" + ModuleNameConstants.CharterQuote.CQExpQuote + "');");
                            }
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Express Quote File has mandatory exceptions, File is not Saved", 330, 110, ModuleNameConstants.CharterQuote.CQExpQuote, "");
                    }
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (RadAjax_AjaxRequest != null)
                {
                    RadAjax_AjaxRequest(sender, e);
                }

                if (e.Argument.ToString() == "tbDepart_TextChanged")
                    tbDepart_TextChanged(sender, e);
                if (e.Argument.ToString() == "tbArrive_TextChanged")
                    tbArrive_TextChanged(sender, e);
                if (e.Argument.ToString() == "tbPower_TextChanged")
                    tbPower_TextChanged(sender, e);
                if (e.Argument.ToString() == "TailNum_TextChanged")
                    TailNum_TextChanged(sender, e);
                if (e.Argument.ToString() == "tbCrewRules_TextChanged")
                    tbCrewRules_TextChanged(sender, e);
                if (e.Argument.ToString() == "TypeCode_TextChanged")
                    TypeCode_TextChanged(sender, e);
                if (e.Argument.ToString() == "Vendor_TextChanged")
                    Vendor_TextChanged(sender, e);
                if (e.Argument.ToString() == "tbCustomer_TextChanged")
                    tbCustomer_TextChanged(sender, e);
                if (e.Argument.ToString() == "tbSalesPerson_TextChanged")
                    tbSalesPerson_TextChanged(sender, e);
                if (e.Argument.ToString() == "tbLeadSource_TextChanged")
                    tbLeadSource_TextChanged(sender, e);
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //if (RadAjax_AjaxRequest != null)
                //{
                //    RadAjax_AjaxRequest(sender, e);
                //}
            }
        }

        private void ClearChildProperties(ref CQFile cqFile)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqFile))
            {
                if (cqFile.CQCustomerID != null) cqFile.CQCustomer = null;
                if (cqFile.CQCustomerContactID != null) cqFile.CQCustomerContact = null;
                if (cqFile.LeadSourceID != null) cqFile.LeadSource = null;
                if (cqFile.SalesPersonID != null) cqFile.SalesPerson = null;
                if (cqFile.HomebaseID != null) cqFile.Company = null;
                if (cqFile.ExchangeRateID != null) cqFile.ExchangeRate1 = null;
                #region CQMain Properties
                if (cqFile.CQMains != null)
                {
                    foreach (CQMain cqMain in cqFile.CQMains.ToList())
                    {
                        #region CQMain Properties
                        if (cqMain.VendorID != null) cqMain.Vendor = null;
                        if (cqMain.FleetID != null) cqMain.Fleet = null;
                        if (cqMain.AircraftID != null) cqMain.Aircraft = null;
                        if (cqMain.PassengerRequestorID != null) cqMain.Passenger = null;
                        if (cqMain.HomebaseID != null) cqMain.Company = null;
                        if (cqMain.ClientID != null) cqMain.Client = null;
                        if (cqMain.CQLostBusinessID != null) cqMain.CQLostBusiness = null;
                        #endregion
                        #region CQFleetChargeDetail Properties
                        if (cqMain.CQFleetChargeDetails != null)
                        {
                            foreach (CQFleetChargeDetail cqfltChrgDet in cqMain.CQFleetChargeDetails.ToList())
                            {
                                if (cqfltChrgDet.AccountID != null) cqfltChrgDet.Account = null;
                                if (cqfltChrgDet.AircraftCharterRateID != null) cqfltChrgDet.AircraftCharterRate = null;
                                if (cqfltChrgDet.FleetCharterRateID != null) cqfltChrgDet.FleetCharterRate = null;
                                //if (cqfltChrgDet.FeeGroupID != null) cqfltChrgDet.FeeGroup = null;
                            }
                        }
                        #endregion
                        if (cqMain.CQLegs != null)
                        {
                            foreach (CQLeg Leg in cqMain.CQLegs.ToList())
                            {
                                #region CQLeg Properties
                                if (Leg.DAirportID != null) Leg.Airport1 = null;
                                if (Leg.AAirportID != null) Leg.Airport = null;
                                if (Leg.FBOID != null) Leg.FBO = null;
                                if (Leg.CrewDutyRulesID != null) Leg.CrewDutyRule = null;
                                if (Leg.ClientID != null) Leg.Client = null;
                                if (Leg.OIAAirportID != null) Leg.Airport2 = null;
                                if (Leg.OIDAirportID != null) Leg.Airport3 = null;
                                if (Leg.OQAAirportID != null) Leg.Airport4 = null;
                                if (Leg.OQDAirportID != null) Leg.Airport5 = null;
                                #endregion
                                #region CQPassenger Properties
                                if (Leg.CQPassengers != null)
                                {
                                    foreach (CQPassenger PAX in Leg.CQPassengers.ToList())
                                    {
                                        if (PAX.PassengerRequestorID != null) PAX.Passenger = null;
                                        if (PAX.FlightPurposeID != null) PAX.FlightPurpose = null;
                                        if (PAX.PassportID != null) PAX.CrewPassengerPassport = null;
                                    }
                                }
                                #endregion
                                #region CQFBOList Properties
                                if (Leg.CQFBOLists != null)
                                {
                                    foreach (CQFBOList fbo in Leg.CQFBOLists.ToList())
                                    {
                                        if (fbo.FBOID != null) fbo.FBO = null;
                                        if (fbo.AirportID != null) fbo.Airport = null;
                                    }
                                }
                                #endregion
                                #region CQHotelList Properties
                                if (Leg.CQHotelLists != null)
                                {
                                    foreach (CQHotelList hotel in Leg.CQHotelLists.ToList())
                                    {
                                        if (hotel.HotelID != null) hotel.Hotel = null;
                                        if (hotel.AirportID != null) hotel.Airport = null;
                                    }
                                }
                                #endregion
                                #region CQTransportList Properties
                                if (Leg.CQTransportLists != null)
                                {
                                    foreach (CQTransportList transport in Leg.CQTransportLists.ToList())
                                    {
                                        if (transport.TransportID != null) transport.Transport = null;
                                        if (transport.AirportID != null) transport.Airport = null;
                                    }
                                }
                                #endregion
                                #region CQCateringList Properties
                                if (Leg.CQCateringLists != null)
                                {
                                    foreach (CQCateringList Catering in Leg.CQCateringLists.ToList())
                                    {
                                        if (Catering.CateringID != null) Catering.Catering = null;
                                        if (Catering.AirportID != null) Catering.Airport = null;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
                #endregion
            }
        }

        protected void SetCQModeToNoChange(ref CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                if (fileRequest != null)
                {
                    fileRequest.Mode = CQRequestActionMode.NoChange;
                    fileRequest.State = CQRequestEntityState.NoChange;
                    if (fileRequest.CQMains != null)
                    {
                        foreach (CQMain cqMain in fileRequest.CQMains)
                        {
                            cqMain.State = CQRequestEntityState.NoChange;
                            cqMain.Mode = CQRequestActionMode.NoChange;
                            if (cqMain.CQFleetChargeDetails != null)
                            {
                                foreach (CQFleetChargeDetail cqFleetCharge in cqMain.CQFleetChargeDetails)
                                {
                                    cqFleetCharge.State = CQRequestEntityState.NoChange;
                                }
                            }
                            if (cqMain.CQLegs != null)
                            {
                                foreach (CQLeg cqLeg in cqMain.CQLegs)
                                {
                                    cqLeg.State = CQRequestEntityState.NoChange;
                                    if (cqLeg.CQCateringLists != null)
                                    {
                                        foreach (CQCateringList cqCatering in cqLeg.CQCateringLists)
                                        {
                                            cqCatering.State = CQRequestEntityState.NoChange;
                                        }
                                    }
                                    if (cqLeg.CQFBOLists != null)
                                    {
                                        foreach (CQFBOList cqFBO in cqLeg.CQFBOLists)
                                        {
                                            cqFBO.State = CQRequestEntityState.NoChange;
                                        }
                                    }
                                    if (cqLeg.CQHotelLists != null)
                                    {
                                        foreach (CQHotelList cqHotel in cqLeg.CQHotelLists)
                                        {
                                            cqHotel.State = CQRequestEntityState.NoChange;
                                        }
                                    }
                                    if (cqLeg.CQPassengers != null)
                                    {
                                        foreach (CQPassenger cqPax in cqLeg.CQPassengers)
                                        {
                                            cqPax.State = CQRequestEntityState.NoChange;
                                        }
                                    }
                                    if (cqLeg.CQTransportLists != null)
                                    {
                                        foreach (CQTransportList cqTransport in cqLeg.CQTransportLists)
                                        {
                                            cqTransport.State = CQRequestEntityState.NoChange;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void EnableLegForm(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                tbTaxRate.Enabled = status;
                tbPaxCount.Enabled = status;
                tbArrival.Enabled = status;
                tbArrivalDate.Enabled = status;
                tbCharges.Enabled = status;
                tbDayRoom.Enabled = status;
                tbDepart.Enabled = status;
                tbETE.Enabled = status;
                tbLocalDate.Enabled = status;
                tbMiles.Enabled = status;
                tbLegRate.Enabled = status;
                tbRON.Enabled = status;
                rbDomestic.Enabled = status;
                rmtlocaltime.Enabled = status;
                rmtArrivalTime.Enabled = status;
                chkPOS.Enabled = status;
                chkTaxable.Enabled = status;
                if (status)
                {
                    btnClosestIcao.CssClass = "browse-button";
                    btnArrival.CssClass = "browse-button";
                }
                else
                {
                    btnClosestIcao.CssClass = "browse-button-disabled";
                    btnArrival.CssClass = "browse-button-disabled";
                }
            }
        }

        private void ValidateEQFile()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgLegs.Items.Count == 0)
                {
                    bool NoLegExists = false;
                    if (rblSource.SelectedValue == "1")
                    {
                        if (string.IsNullOrEmpty(tbVendor.Text) && string.IsNullOrEmpty(hdnVendor.Value))
                        {
                            RadWindowManager1.RadAlert("Vendor Code Is Required", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, "confirmCallBackfnVendor");
                            ValidateFile = false;
                        }
                        else
                        {
                            NoLegExists = true;
                        }
                    }
                    else if (rblSource.SelectedValue == "2")
                    {
                        if (string.IsNullOrEmpty(tbTailNum.Text) && string.IsNullOrEmpty(hdnTailNum.Value))
                        {
                            RadWindowManager1.RadAlert("Tail Number Is Required", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, "confirmCallBackfnTail");
                            ValidateFile = false;
                        }
                        else
                        {
                            NoLegExists = true;
                        }
                    }
                    else if (rblSource.SelectedValue == "3")
                    {
                        if (string.IsNullOrEmpty(tbTypeCode.Text) && string.IsNullOrEmpty(hdnTypeCode.Value))
                        {
                            RadWindowManager1.RadAlert("Type Code Is Required", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, "confirmCallBackfnTypeCode");
                            ValidateFile = false;
                        }
                        else
                        {
                            NoLegExists = true;
                        }
                    }
                    if (NoLegExists)
                    {
                        RadWindowManager1.RadAlert("File cannot be saved without leg", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                        ValidateFile = false;
                    }
                }
                //May be included in future
                //else
                //{
                //    //Validation on leg side
                //    string LegExecptions = string.Empty;
                //    LegExecptions = ValidatLegGrid();
                //    if (!string.IsNullOrEmpty(LegExecptions))
                //    {
                //        RadWindowManager1.RadAlert(LegExecptions, 360, 150, "Express Quote", null);
                //        ValidateFile = false;
                //    }
                //}
            }
        }

        private string ValidatLegGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string errorDescription = string.Empty;
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    if (FileRequest != null)
                    {
                        if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            QuoteInProgress = new CQMain();
                            Int64 QuoteNumInProgress = 1;
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                            if (QuoteInProgress != null)
                            {
                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                {
                                    List<CQLeg> cqValLegs = new List<CQLeg>();
                                    cqValLegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).ToList();
                                    if (cqValLegs != null)
                                    {
                                        foreach (CQLeg cqLeg in cqValLegs)
                                        {
                                            //for (int i = 0; i < cqValLegs.Count; i++)
                                            //{
                                            //string errorDescription = string.Empty;
                                            if (cqLeg.DAirportID == null)
                                            {
                                                if (!string.IsNullOrEmpty(errorDescription))
                                                    errorDescription = errorDescription + "<br>" + "Leg " + cqLeg.LegNUM + ":" + "Depart Icao Is Required";
                                                else
                                                    errorDescription = "Leg " + cqLeg.LegNUM + ":" + "Depart Icao Is Required";
                                            }
                                            if (cqLeg.AAirportID == null)
                                            {
                                                errorDescription = errorDescription + "<br>" + "Leg " + cqLeg.LegNUM + ":" + "Arrival Icao Is Required";
                                            }
                                            if (cqLeg.DepartureDTTMLocal == null)
                                            {
                                                errorDescription = errorDescription + "<br>" + "Leg " + cqLeg.LegNUM + ":" + "Depart date Is Required";
                                            }
                                            if (cqLeg.ArrivalDTTMLocal == null)
                                            {
                                                errorDescription = errorDescription + "<br>" + "Leg " + cqLeg.LegNUM + ":" + "Arrive date Is Required";
                                            }
                                            //}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return errorDescription;
            }
        }

        protected void CancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                        {
                            CQFile FileRequest = (CQFile)Session[EQSessionKey];
                            if (FileRequest != null)
                            {
                                SetQuoteModeToNoChange(ref FileRequest);
                                using (CharterQuoteServiceClient CQService = new CharterQuoteServiceClient())
                                {
                                    if (FileRequest.CQFileID != 0)
                                    {
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(FileRequest.EntityKey.EntitySetName, FileRequest.CQFileID);
                                        }
                                        var objCQFile = CQService.GetCQRequestByID(FileRequest.CQFileID, true);
                                        if (objCQFile.ReturnFlag)
                                        {
                                            FileRequest = objCQFile.EntityList[0];
                                            Session[EQSessionKey] = FileRequest;
                                            Session[EQException] = null;
                                            if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                            {
                                                var objRetval = CQService.GetQuoteExceptionList(FileRequest.CQMains[0].CQMainID);
                                                if (objRetval.ReturnFlag)
                                                {
                                                    Session[EQException] = objRetval.EntityList;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            FileRequest = null;
                                            Session.Remove(EQException);
                                        }
                                    }
                                    else
                                    {
                                        FileRequest = null;
                                        Session.Remove(EQSessionKey);
                                        Session.Remove(EQException);
                                    }
                                }
                            }
                            SetQuoteMode();
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindPage();");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void CancelNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void SetQuoteModeToNoChange(ref CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                if (fileRequest != null)
                {
                    fileRequest.Mode = CQRequestActionMode.NoChange;
                    fileRequest.State = CQRequestEntityState.NoChange;
                    if (fileRequest.CQMains != null)
                    {
                        foreach (CQMain quote in fileRequest.CQMains)
                        {
                            quote.State = CQRequestEntityState.NoChange;
                            if (quote.CQLegs != null)
                            {
                                foreach (CQLeg leg in quote.CQLegs)
                                {
                                    leg.State = CQRequestEntityState.NoChange;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void SetQuoteMode()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        ControlEnable("Button", btnNew, false, "button_disable");
                        ControlEnable("Button", btnEdit, false, "button_disable");
                        //ControlEnable("Button", btn, false, "button_disable");
                        ControlEnable("Button", btnNewFile, false, "button_disable");
                        ControlEnable("Button", btnEditFile, false, "button_disable");
                    }
                    else
                    {
                        ControlEnable("Button", btnNew, true, "button");
                        ControlEnable("Button", btnEdit, true, "button");
                        //ControlEnable("Button", btnExpQuote, true, "button");
                        ControlEnable("Button", btnNewFile, true, "button");
                        ControlEnable("Button", btnEditFile, true, "button");
                    }
                }
                else
                {
                    ControlEnable("Button", btnNew, true, "button");
                    ControlEnable("Button", btnEdit, true, "button");
                    //ControlEnable("Button", btnExpQuote, true, "button");
                    ControlEnable("Button", btnNewFile, true, "button");
                    ControlEnable("Button", btnEditFile, true, "button");
                }
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbbtSearch.Text))
                        {
                            CQFile FileRequest = new CQFile();
                            if (Session[EQSessionKey] != null)
                                FileRequest = (CQFile)Session[EQSessionKey];
                            if (FileRequest != null)
                            {
                                if (FileRequest.Mode == CQRequestActionMode.Edit)
                                {
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File Unsaved, please Save or Cancel File', 330, 110,'" + ModuleNameConstants.CharterQuote.CQExpQuote + "');");
                                }
                                else
                                {
                                    Int64 SearchFileNum = 0;
                                    if (Int64.TryParse(tbbtSearch.Text, out SearchFileNum))
                                    {
                                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                                        {
                                            //Method will be changed once manager completed
                                            var objRetVal = Service.GetCQRequestByFileNum(SearchFileNum, true);
                                            if (objRetVal.ReturnFlag)
                                            {
                                                if (objRetVal.EntityList[0].ExpressQuote != null && objRetVal.EntityList[0].ExpressQuote == true && objRetVal.EntityList[0].IsDeleted == false)
                                                {
                                                    FileRequest = objRetVal.EntityList[0];
                                                    Session[EQSessionKey] = FileRequest;
                                                    Session.Remove(EQException);
                                                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                                    {
                                                        var objRetval = Service.GetQuoteExceptionList(FileRequest.CQMains[0].CQMainID);
                                                        if (objRetval.ReturnFlag)
                                                        {
                                                            Session[EQException] = objRetval.EntityList;
                                                        }
                                                    }

                                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindSelect();");
                                                }
                                                else
                                                {
                                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File No. Does Not Exist', 300, 110,'" + ModuleNameConstants.CharterQuote.CQExpQuote + "');");
                                                    tbbtSearch.Text = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File No. Does Not Exist', 300, 110,'" + ModuleNameConstants.CharterQuote.CQExpQuote + "');");
                                                tbbtSearch.Text = string.Empty;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('File No. Does Not Exist', 300, 110,'" + ModuleNameConstants.CharterQuote.CQExpQuote + "');");
                                        tbbtSearch.Text = string.Empty;
                                    }
                                }
                            }
                        }
                        else
                        {
                            lbTripSearchMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void DeleteFileYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                        {
                            CheckAutorization(Permission.CharterQuote.DeleteCQExpressQuote);
                            var ReturnValue = Service.DeleteFile(FileRequest.CQFileID);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                Session.Remove("EQException");
                                Session.Remove(EQSessionKey);
                                ClearFields();
                                ClearLabels();
                                BindLegs(dgLegs, true);
                                ControlEnable("Button", btnNew, true, "button");
                                ControlEnable("Button", btnEdit, false, "button-disable");
                                ControlEnable("Button", btnDelete, false, "button-disable");
                                ControlEnable("Button", btnSave, false, "button-disable");
                                ControlEnable("Button", btnCancel, false, "button-disable");
                                ControlEnable("Button", btnTransferTOCQ, false, "button-disable");
                                lnkReport.Enabled = false;
                                ControlEnable("Button", btnNewFile, true, "button");
                                ControlEnable("Button", btnEditFile, false, "button-disable");
                                ControlEnable("Button", btnDeleteFile, false, "button-disable");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void DeleteFileNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        #region "ExpressQuote Exception"
        private void ExpandErrorPanel(bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isExpand))
            {
                RadPanelItem pnlItem = pnlException.FindItemByValue("pnlItemException");
                pnlItem.Expanded = isExpand;
            }
        }

        public List<FlightPak.Web.CharterQuoteService.ExceptionTemplate> GetExceptionTemplateList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPak.Web.CharterQuoteService.ExceptionTemplate> ExceptionTemplateList = new List<FlightPak.Web.CharterQuoteService.ExceptionTemplate>();
                if (Session[EQExceptionTempatelist] == null)
                {
                    using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                    {
                        var ObjExTemplRetVal = Service.GetCQBusinessErrorsList();
                        if (ObjExTemplRetVal.ReturnFlag)
                        {
                            ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                            Session[EQExceptionTempatelist] = ExceptionTemplateList;
                        }
                    }
                }
                else
                    ExceptionTemplateList = (List<FlightPak.Web.CharterQuoteService.ExceptionTemplate>)Session[EQExceptionTempatelist];
                return ExceptionTemplateList;
            }
        }

        protected string GetSubmoduleModule(long SubModuleID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SubModuleID))
            {
                string Retval = string.Empty;
                switch (SubModuleID)
                {
                    case 0: Retval = "File"; break;
                    case 1: Retval = "Quote"; break;
                    case 2: Retval = "Leg"; break;
                    case 3: Retval = "PAX"; break;
                    case 4: Retval = "Logistics"; break;
                    case 5: Retval = "Reports"; break;
                }
                return Retval;
            }
        }

        /// <summary>
        /// Exception Grid Bind 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exception_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                BindExecption();
            }
        }

        private void BindExecption()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[EQException] != null)
                {
                    ExceptionList = (List<CQException>)Session[EQException];
                    if (ExceptionList.Count > 0)
                    {
                        List<FlightPak.Web.CharterQuoteService.ExceptionTemplate> ExceptionTemplateList = new List<FlightPak.Web.CharterQuoteService.ExceptionTemplate>();
                        using (CharterQuoteServiceClient CharterQuoteSvc = new CharterQuoteServiceClient())
                        {
                            ExceptionTemplateList = GetExceptionTemplateList();
                            dgException.DataSource = (from Exp in ExceptionList
                                                      join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                      select new
                                                      {
                                                          Severity = ExpTempl.Severity,
                                                          ExceptionDescription = Exp.CQExceptionDescription,
                                                          SubModuleID = ExpTempl.SubModuleID,
                                                          SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                                          DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                                      }).OrderBy(x => x.DisplayOrder).ToList();
                            ExpandErrorPanel(true);
                        }
                    }
                    else
                    {
                        ExpandErrorPanel(false);
                    }
                }
                else
                {
                    dgException.DataSource = new string[] { };
                }
            }
        }

        /// <summary>
        /// Exception Grid Item Databound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exception_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem Item = (GridDataItem)e.Item;
                    System.Web.UI.WebControls.Image imgSeverity = (System.Web.UI.WebControls.Image)Item.FindControl("imgSeverity");
                    string severity = Item.GetDataKeyValue("Severity").ToString().Trim();
                    // Default Image Path
                    string imagePath = "~/App_Themes/Default/images/info.png";
                    if (severity == "1")
                    {
                        imagePath = "~/App_Themes/Default/images/info_red.gif";
                    }
                    else if (severity == "2")
                    {
                        imagePath = "~/App_Themes/Default/images/info_yellow.gif";
                    }
                    imgSeverity.ImageUrl = imagePath;
                }
                //if (Severity.Critical == allErrorList[0].ExceptionSeverity)
                //{
                //    RedirectToMain();
                //    //if (seltab == "Main")
                //    //else
                //    //    Response.Redirect("PreflightLegs.aspx?seltab=Legs");
                //}
            }
        }

        private void ExceptionValidation()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                {
                    #region Exception Validation
                    List<CQException> eSeverityException = new List<CQException>();
                    List<FlightPak.Web.CharterQuoteService.ExceptionTemplate> ExceptionTemplateList = new List<FlightPak.Web.CharterQuoteService.ExceptionTemplate>();
                    ExceptionTemplateList = GetExceptionTemplateList();
                    FileRequest.AllowFileToSave = true;
                    ExceptionList = null;
                    if (ExceptionTemplateList != null && ExceptionTemplateList.Count > 0)
                    {
                        if (FileRequest != null)
                        {
                            if (FileRequest.CQMains != null)
                            {
                                CQMain cqMain = FileRequest.CQMains.Where(x => x.IsDeleted == false && x.QuoteNUM == 1).FirstOrDefault();
                                if (cqMain != null)
                                {
                                    var ExceptionlistforMandate = Service.ValidateQuoteforMandatoryExcep(cqMain);
                                    if (ExceptionlistforMandate.ReturnFlag)
                                    {
                                        if (ExceptionList == null)
                                            ExceptionList = new List<CharterQuoteService.CQException>();
                                        ExceptionList.AddRange(ExceptionlistforMandate.EntityList);
                                        if (ExceptionList != null && ExceptionList.Count > 0)
                                        {
                                            eSeverityException = (from Exp in ExceptionList
                                                                  join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                                  where ExpTempl.Severity == 1
                                                                  select Exp).ToList();
                                            if (eSeverityException != null && eSeverityException.Count > 0)
                                            {
                                                FileRequest.AllowFileToSave = false;
                                                FileRequest.QuoteNumInProgress = (int)cqMain.QuoteNUM;
                                            }
                                        }
                                        else
                                        {
                                            dgException.Rebind();
                                        }
                                    }
                                }
                            }
                        }
                        if (!FileRequest.AllowFileToSave)
                        {
                            List<CQException> OldExceptionList = new List<CQException>();
                            OldExceptionList = (List<CQException>)Session[EQException];
                            if (OldExceptionList != null && OldExceptionList.Count > 0)
                            {
                                foreach (CQException OldMandatoryException in OldExceptionList.ToList())
                                {
                                    if (OldMandatoryException.CQExceptionID == 0)
                                        OldExceptionList.Remove(OldMandatoryException);
                                }
                            }
                            if (ExceptionList != null && ExceptionList.Count > 0)
                            {
                                if (OldExceptionList == null)
                                    OldExceptionList = new List<CQException>();
                                OldExceptionList.AddRange(ExceptionList);
                            }
                            Session[EQException] = OldExceptionList;
                            //RadWindowManager1.RadAlert("Express Quote File has mandatory exceptions, File is not Saved", 330, 110, "Express Quote", "");
                            dgException.Rebind();
                        }
                    }
                    #endregion
                }
            }
        }

        private CQMain getQuoteInProgress(CQFile cqFile)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqFile))
            {
                CQMain QuoteInProgress = new CQMain();
                if (cqFile.CQMains != null && cqFile.CQMains.Count > 0)
                {
                    Int64 QuoteNumInProgress = 1;
                    QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                }
                return QuoteInProgress;
            }
        }

        #endregion

        #region "Express Quote Calculation"

        public decimal CalculateSumOfETE(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {

                    if (orderNum == 1)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == false && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;

                    }
                    else if (orderNum == 2)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == true && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                    else
                    {

                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                }
                return result;
            }
        }

        public DateTime CalculateMinDeparttDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DepartureDTTMLocal != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Min(x => x.DepartureDTTMLocal).Value;
                }
                return result;
            }
        }

        public DateTime CalculateMaxArrivalDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.ArrivalDTTMLocal != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Max(x => x.ArrivalDTTMLocal).Value;
                }
                return result;
            }
        }

        public decimal CalculateSumOfTotalCharge()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.ChargeTotal).Value;
                }
                return result;
            }
        }

        public string GetStateSegFee(string DepartState, string ArrivalState)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartState, ArrivalState))
            {
                string segState = string.Empty;
                if (!string.IsNullOrEmpty(DepartState) && (DepartState == "AK" || DepartState == "HI"))
                    segState = DepartState;
                if (!string.IsNullOrEmpty(ArrivalState) && (ArrivalState == "AK" || ArrivalState == "HI"))
                    segState = ArrivalState;
                return segState;
            }
        }

        public decimal CalculateSegFee()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTotSegFee = 0;
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnCqSegAk = 0;
                decimal lnCqSegHi = 0;
                decimal lnIntlSegFeeRate = 0;
                decimal lnDomSegFeeRate = 0;

                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                    {
                        if (QuoteInProgress.FederalTax != null)
                            lnTaxRate = (decimal)QuoteInProgress.FederalTax;
                        lnExRate = (FileRequest.ExchangeRate != null && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;

                        FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId company;
                        if (FileRequest.HomebaseID != null)
                            company = GetCompany((long)FileRequest.HomebaseID);
                        else
                            company = GetCompany((long)UserPrincipal.Identity._homeBaseId);
                        if (company != null)
                        {
                            if (company.SegmentFeeAlaska != null)
                                lnCqSegAk = (decimal)company.SegmentFeeAlaska * lnExRate;
                            if (company.SegementFeeHawaii != null)
                                lnCqSegHi = (decimal)company.SegementFeeHawaii * lnExRate;
                            if (company.ChtQouteIntlSegCHG != null)
                                lnIntlSegFeeRate = (decimal)company.ChtQouteIntlSegCHG * lnExRate;
                            if (company.ChtQouteDOMSegCHG != null)
                                lnDomSegFeeRate = (decimal)company.ChtQouteDOMSegCHG * lnExRate;
                        }
                        foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => (x.IsDeleted == false && x.DAirportID != null && x.AAirportID != null)).ToList())
                        {
                            FlightPak.Web.FlightPakMasterService.GetAllAirport DepartAirport = GetAirport((long)Leg.DAirportID);
                            FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirport = GetAirport((long)Leg.AAirportID);
                            decimal lnDomSegFee = 0;
                            decimal lnIntlSegFee = 0;
                            decimal lnPax_Total = 0;

                            if (DepartAirport != null && ArrAirport != null)
                            {
                                bool DepartRural = false;
                                bool ArrRural = false;
                                if (DepartAirport.IsRural != null)
                                    DepartRural = (bool)DepartAirport.IsRural;
                                if (ArrAirport.IsRural != null)
                                    ArrRural = (bool)ArrAirport.IsRural;

                                if (!ArrRural && !DepartRural)
                                {
                                    string lcCqSegState = string.Empty;
                                    if (DepartAirport.StateName != null && ArrAirport.StateName != null)
                                    {
                                        lcCqSegState = GetStateSegFee(DepartAirport.StateName, ArrAirport.StateName);
                                    }

                                    //if ((bool)QuoteInProgress.IsTaxable && (bool)Leg.IsTaxable)
                                    if ((bool)Leg.IsTaxable)
                                    {
                                        switch (lcCqSegState)
                                        {
                                            case "AK":
                                                lnDomSegFeeRate = lnCqSegAk > 0 ? lnCqSegAk : lnDomSegFeeRate;
                                                break;
                                            case "HI":
                                                lnDomSegFeeRate = lnCqSegHi > 0 ? lnCqSegHi : lnDomSegFeeRate;
                                                break;
                                        }
                                        lnPax_Total = Leg.PassengerTotal != null ? (decimal)Leg.PassengerTotal : 0;
                                        if (Leg.DutyTYPE == 1)
                                            lnDomSegFee = lnDomSegFeeRate * lnPax_Total; //lnDomSegFee = lnDomSegFeeRate * lnExRate * lnPax_Total;
                                        else if (Leg.DutyTYPE == 2)
                                            lnIntlSegFee = lnIntlSegFeeRate * lnPax_Total; //lnIntlSegFee = lnIntlSegFeeRate * lnExRate * lnPax_Total;

                                        lnTotSegFee += lnDomSegFee + lnIntlSegFee;
                                    }
                                }
                            }
                        }
                        //QuoteInProgress.SegmentFeeTotal = lnTotSegFee;
                        //SaveQuoteinProgressToFileSession();
                    }
                }
                return lnTotSegFee;
            }
        }

        public void CalculateQuoteTotal()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnTotFltHrs = 0;
                decimal lnTotDays = 0;
                decimal lnTotalDays = 0;
                decimal lnMinDays = 0;
                decimal lnMinDailyUsage = 0;
                decimal lnTotLndDsc = 0;
                decimal lnDiscRate = 0;
                decimal lnTotCqDisc = 0;
                decimal lnTotBuyAmt = 0;
                decimal lnTotAdjAmt = 0;
                decimal lnTotTaxAmt = 0;
                decimal lnTotCrwRon = 0;
                decimal lnTotUseAdj = 0;
                decimal lnTotlndfee = 0;
                decimal totchrg = 0;
                decimal lnTotDiscAmt = 0;
                decimal totsub = 0;
                decimal lnTotCost = 0;
                decimal lnTotMargin = 0;
                decimal lnTotMarPct = 0;
                decimal lnTotSegFee = 0;

                string lcFleetType = string.Empty;
                DateTime locarrdt, locdepdt;

                if (QuoteInProgress != null)
                {
                    lnTaxRate = QuoteInProgress.FederalTax != null ? (decimal)QuoteInProgress.FederalTax : 0;
                    lnExRate = (FileRequest.ExchangeRate != null && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;

                    if (QuoteInProgress.SubtotalTotal == null)
                        QuoteInProgress.SubtotalTotal = 0.0M;

                    if (QuoteInProgress.QuoteTotal == null)
                        QuoteInProgress.QuoteTotal = 0.0M;

                    if (QuoteInProgress.CostTotal == null)
                        QuoteInProgress.CostTotal = 0.0M;

                    if (QuoteInProgress.UsageAdjTotal == null)
                        QuoteInProgress.UsageAdjTotal = 0.0M;

                    if (QuoteInProgress.DiscountAmtTotal == null)
                        QuoteInProgress.DiscountAmtTotal = 0.0M;

                    if (QuoteInProgress.FlightChargeTotal == null)
                        QuoteInProgress.FlightChargeTotal = 0.0M;

                    if (QuoteInProgress.IsOverrideCost == null) QuoteInProgress.IsOverrideCost = false;
                    if (QuoteInProgress.IsOverrideDiscountAMT == null) QuoteInProgress.IsOverrideDiscountAMT = false;
                    if (QuoteInProgress.IsOverrideDiscountPercentage == null) QuoteInProgress.IsOverrideDiscountPercentage = false;
                    if (QuoteInProgress.IsOverrideLandingFee == null) QuoteInProgress.IsOverrideLandingFee = false;
                    if (QuoteInProgress.IsOverrideSegmentFee == null) QuoteInProgress.IsOverrideSegmentFee = false;
                    if (QuoteInProgress.IsOverrideTaxes == null) QuoteInProgress.IsOverrideTaxes = false;
                    if (QuoteInProgress.IsOverrideTotalQuote == null) QuoteInProgress.IsOverrideTotalQuote = false;
                    if (QuoteInProgress.IsOverrideUsageAdj == null) QuoteInProgress.IsOverrideUsageAdj = false;
                    if (QuoteInProgress.IsDailyTaxAdj == null) QuoteInProgress.IsDailyTaxAdj = false;
                    if (QuoteInProgress.IsLandingFeeTax == null) QuoteInProgress.IsLandingFeeTax = false;

                    QuoteInProgress.IsTaxable = true;

                    if (QuoteInProgress.FleetID != null)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                            var objRetVal = objDstsvc.GetFleetProfileList();
                            if (objRetVal.ReturnFlag)
                            {
                                Fleetlist = objRetVal.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();

                                if (Fleetlist != null && Fleetlist.Count > 0)
                                {
                                    lcFleetType = Fleetlist[0].FleetSize;
                                    if (Fleetlist[0].MinimumDay != null)
                                        lnMinDays = (decimal)Fleetlist[0].MinimumDay;
                                }
                            }
                        }
                    }

                    lnTotFltHrs = CalculateSumOfETE(4); // any no greater than three to get all values of ete.
                    locdepdt = CalculateMinDeparttDate();
                    locarrdt = CalculateMaxArrivalDate();
                    totchrg = CalculateSumOfTotalCharge();
                    if (locarrdt != DateTime.MinValue && locdepdt != DateTime.MinValue)
                    {
                        //Fix for date subtraction issue
                        DateTime ArrDate = new DateTime(locarrdt.Year, locarrdt.Month, locarrdt.Day);
                        DateTime DepDate = new DateTime(locdepdt.Year, locdepdt.Month, locdepdt.Day);
                        TimeSpan daydiff = ArrDate.Subtract(DepDate);
                        lnTotalDays = daydiff.Days + 1;

                    }

                    lnTotDays = lnTotalDays;
                    if (lnTotalDays < 1)
                        lnTotalDays = 1;
                    lnMinDailyUsage = lnTotalDays * lnMinDays;
                    lnTotAdjAmt = Math.Round(lnMinDailyUsage - lnTotFltHrs, 2);

                    if (lnTotAdjAmt < 0)
                        lnTotAdjAmt = 0;

                    lnTotLndDsc = 0;
                    lnDiscRate = 0;
                    lnTotCqDisc = 0;
                    lnTotBuyAmt = 0;

                    lnTotSegFee = CalculateSegFee();

                    if (FileRequest.DiscountPercentage != null)
                        lnDiscRate = (decimal)FileRequest.DiscountPercentage;

                    if (QuoteInProgress.IsOverrideDiscountPercentage != null && (bool)QuoteInProgress.IsOverrideDiscountPercentage) // Override Discount % in quote 
                        lnDiscRate = QuoteInProgress.DiscountPercentageTotal != null ? (decimal)QuoteInProgress.DiscountPercentageTotal : 0;
                    else
                        QuoteInProgress.DiscountPercentageTotal = lnDiscRate;

                    if (QuoteInProgress.CQFleetChargeDetails != null)
                    {
                        #region Initializing to 0
                        foreach (CQFleetChargeDetail cqFleetChrgDet in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                        {
                            if (cqFleetChrgDet.CQFleetChargeDetailID != 0)
                                cqFleetChrgDet.State = CQRequestEntityState.Modified;
                            cqFleetChrgDet.FeeAMT = 0;

                            if (cqFleetChrgDet.Quantity == null)
                                cqFleetChrgDet.Quantity = 0;


                            if (cqFleetChrgDet.SellDOM == null)
                                cqFleetChrgDet.SellDOM = 0;

                            if (cqFleetChrgDet.IsDiscountDOM == null)
                                cqFleetChrgDet.IsDiscountDOM = false;

                            if (cqFleetChrgDet.IsTaxDOM == null)
                                cqFleetChrgDet.IsTaxDOM = false;

                            if (cqFleetChrgDet.BuyDOM == null)
                                cqFleetChrgDet.BuyDOM = 0;

                            if (cqFleetChrgDet.SellIntl == null)
                                cqFleetChrgDet.SellIntl = 0;

                            if (cqFleetChrgDet.IsDiscountIntl == null)
                                cqFleetChrgDet.IsDiscountIntl = false;

                            if (cqFleetChrgDet.IsTaxIntl == null)
                                cqFleetChrgDet.IsTaxIntl = false;

                            if (cqFleetChrgDet.BuyIntl == null)
                                cqFleetChrgDet.BuyIntl = 0;




                        }
                        lnTotCqDisc = 0;
                        //QuoteInProgress.DiscountAmtTotal = 0; //lnTotCqDisc
                        lnTotTaxAmt = 0;
                        //QuoteInProgress.TaxesTotal = 0; //lnTotTaxAmt
                        lnTotBuyAmt = 0;
                        //QuoteInProgress.CostTotal = 0; //lnTotBuyAmt
                        lnTotlndfee = 0;
                        //QuoteInProgress.LandingFeeTotal = 0; //lnTotlndfee
                        totsub = 0;
                        //QuoteInProgress.SubtotalTotal = 0; //totsun
                        lnTotCrwRon = 0;
                        //QuoteInProgress.StdCrewRONTotal =0//lnTotCrwRon
                        #endregion

                        #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition

                        foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3 && x.NegotiatedChgUnit != null && x.NegotiatedChgUnit == 5))
                        {
                            decimal chargeRateDisc = 0.0M;

                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;


                            chargeRate.FeeAMT = chargeRate.Quantity * chargeRate.SellDOM;

                            // Calculate Discount
                            if ((bool)chargeRate.IsDiscountDOM)
                            {
                                chargeRateDisc = Math.Round((decimal)chargeRate.FeeAMT * lnDiscRate / 100, 2);
                                lnTotCqDisc += chargeRateDisc;
                            }

                            // Calculate Tax
                            if ((bool)chargeRate.IsTaxDOM && (bool)QuoteInProgress.IsTaxable && chargeRate.FeeAMT != null)
                            {
                                decimal taxAmount = 0.0M;
                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                    taxAmount = Math.Round(((decimal)chargeRate.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                else
                                    taxAmount = Math.Round((decimal)chargeRate.FeeAMT * (decimal)lnTaxRate / 100, 2);

                                lnTotTaxAmt += taxAmount;
                            }

                            decimal buyAmount = 0.0M;
                            buyAmount = (decimal)chargeRate.Quantity * (decimal)chargeRate.BuyDOM;
                            lnTotBuyAmt += buyAmount;

                        }
                        #endregion

                        #region Calculate Landing Fee
                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                        {
                            foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null))
                            {
                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {

                                    decimal lnLegLndngFee = 0;
                                    FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirpot = GetAirport((long)Leg.AAirportID);

                                    if (ArrAirpot != null)
                                    {
                                        switch (lcFleetType)
                                        {
                                            case "S":
                                                lnLegLndngFee = ArrAirpot.LandingFeeSmall != null ? (decimal)ArrAirpot.LandingFeeSmall : 0;
                                                break;
                                            case "M":
                                                lnLegLndngFee = ArrAirpot.LandingFeeMedium != null ? (decimal)ArrAirpot.LandingFeeMedium : 0;
                                                break;
                                            case "L":
                                                lnLegLndngFee = ArrAirpot.LandingFeeLarge != null ? (decimal)ArrAirpot.LandingFeeLarge : 0;
                                                break;
                                        }
                                        lnTotlndfee += lnLegLndngFee;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Calculate Fee Amount for all the Standard Charges and Additional Fees, Where OrderNum = 1

                        var StandardCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();

                        if (StandardCharge != null && QuoteInProgress.CQLegs != null)
                        {
                            if (StandardCharge.CQFleetChargeDetailID != 0)
                                StandardCharge.State = CQRequestEntityState.Modified;

                            decimal buyAmount = 0.0M;
                            foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning != null && x.IsPositioning == false))
                            {
                                decimal lnFeeAmt = 0;

                                if (leg.ElapseTM == null)
                                    leg.ElapseTM = 0;

                                if (leg.DutyTYPE == null)
                                    leg.DutyTYPE = 1;


                                if (StandardCharge.NegotiatedChgUnit == 1)
                                {
                                    lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }
                                else if (StandardCharge.NegotiatedChgUnit == 2)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }
                                else if (StandardCharge.NegotiatedChgUnit == 3)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }
                                else if (StandardCharge.NegotiatedChgUnit == 8)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }

                                leg.ChargeTotal = lnFeeAmt;
                                leg.ChargeRate = (decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl;
                                StandardCharge.FeeAMT += lnFeeAmt;
                                lnTotBuyAmt += buyAmount;


                                // Calculate Discount
                                decimal chargeRateDisc = 0.0M;
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsDiscountDOM : StandardCharge.IsDiscountIntl))
                                {
                                    chargeRateDisc = Math.Round((decimal)StandardCharge.FeeAMT * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }

                                // Calculate Tax
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsTaxDOM : StandardCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && lnFeeAmt != null)
                                {
                                    decimal taxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        taxAmount = Math.Round((decimal)lnFeeAmt * lnTaxRate / 100, 2);

                                    lnTotTaxAmt += taxAmount;
                                }

                            }
                        }
                        #endregion

                        #region Calculate Fee Amount for all Positioning Charges, Where OrderNum = 2
                        var PositionCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 2).FirstOrDefault();

                        if (PositionCharge != null && QuoteInProgress.CQLegs != null)
                        {
                            if (PositionCharge.CQFleetChargeDetailID != 0)
                                PositionCharge.State = CQRequestEntityState.Modified;

                            decimal buyAmount = 0.0M;
                            foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning != null && x.IsPositioning == true))
                            {
                                decimal lnFeeAmt = 0;

                                if (PositionCharge.NegotiatedChgUnit == 1)
                                {
                                    lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }
                                else if (PositionCharge.NegotiatedChgUnit == 2)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }
                                else if (PositionCharge.NegotiatedChgUnit == 3)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }
                                else if (PositionCharge.NegotiatedChgUnit == 8)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }

                                leg.ChargeTotal = lnFeeAmt;
                                leg.ChargeRate = (decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl;

                                PositionCharge.FeeAMT += lnFeeAmt;
                                lnTotBuyAmt += buyAmount;

                                // Calculate Discount
                                decimal chargeRateDisc = 0.0M;
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsDiscountDOM : PositionCharge.IsDiscountIntl))
                                {
                                    chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }

                                // Calculate Tax
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsTaxDOM : PositionCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && PositionCharge.FeeAMT != null)
                                {
                                    decimal taxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        taxAmount = Math.Round((decimal)lnFeeAmt * lnTaxRate / 100, 2);
                                    lnTotTaxAmt += taxAmount;
                                }
                            }
                        }
                        #endregion

                        #region Calculate Standard Crew RON Charge, Where OrderNum = 3
                        var RonCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 3).FirstOrDefault();

                        if (RonCharge != null && QuoteInProgress.CQLegs != null)
                        {
                            if (RonCharge.CQFleetChargeDetailID != 0)
                                RonCharge.State = CQRequestEntityState.Modified;

                            decimal domRonQty = 0;
                            decimal intlRonQty = 0;
                            decimal domFeeAmt = 0;
                            decimal intlFeeAmt = 0;
                            decimal domBuyAmt = 0;
                            decimal intlBuyAmt = 0;

                            domRonQty = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DutyTYPE == 1).Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                            intlRonQty = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DutyTYPE == 2).Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;

                            domFeeAmt = (QuoteInProgress.StandardCrewDOM != null ? (decimal)QuoteInProgress.StandardCrewDOM : 1) * (decimal)domRonQty * (decimal)RonCharge.SellDOM;
                            intlFeeAmt = (QuoteInProgress.StandardCrewINTL != null ? (decimal)QuoteInProgress.StandardCrewINTL : 1) * (decimal)intlRonQty * (decimal)RonCharge.SellIntl;

                            domBuyAmt = (QuoteInProgress.StandardCrewDOM != null ? (decimal)QuoteInProgress.StandardCrewDOM : 1) * (decimal)domRonQty * (decimal)RonCharge.BuyDOM;
                            intlBuyAmt = (QuoteInProgress.StandardCrewINTL != null ? (decimal)QuoteInProgress.StandardCrewINTL : 1) * (decimal)intlRonQty * (decimal)RonCharge.BuyIntl;

                            RonCharge.FeeAMT = domFeeAmt + intlFeeAmt;

                            decimal buyAmount = 0.0M;
                            buyAmount = domBuyAmt + intlBuyAmt;
                            lnTotBuyAmt += buyAmount;
                            lnTotCrwRon += (decimal)RonCharge.FeeAMT;

                            decimal domchargeRateDisc = 0.0M;
                            if (RonCharge.IsDiscountDOM != null && (bool)RonCharge.IsDiscountDOM)
                            {
                                domchargeRateDisc = Math.Round((decimal)domFeeAmt * lnDiscRate / 100, 2);
                                lnTotCqDisc += domchargeRateDisc;
                            }

                            decimal intlchargeRateDisc = 0.0M;
                            if (RonCharge.IsDiscountIntl != null && (bool)RonCharge.IsDiscountIntl)
                            {
                                intlchargeRateDisc = Math.Round((decimal)intlFeeAmt * lnDiscRate / 100, 2);
                                lnTotCqDisc += intlchargeRateDisc;
                            }


                            // Calculate Tax
                            if ((bool)RonCharge.IsTaxDOM && (QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT != null)
                            {
                                decimal domtaxAmount = 0.0M;
                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                    domtaxAmount = Math.Round(((decimal)domFeeAmt - (decimal)domchargeRateDisc) * lnTaxRate / 100, 2);
                                else
                                    domtaxAmount = Math.Round((decimal)domFeeAmt * lnTaxRate / 100, 2);

                                lnTotTaxAmt += domtaxAmount;
                            }

                            if ((bool)RonCharge.IsTaxIntl && (QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT != null)
                            {
                                decimal intltaxAmount = 0.0M;
                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                    intltaxAmount = Math.Round(((decimal)intlFeeAmt - (decimal)intlchargeRateDisc) * lnTaxRate / 100, 2);
                                else
                                    intltaxAmount = Math.Round((decimal)intlFeeAmt * lnTaxRate / 100, 2);

                                lnTotTaxAmt += intltaxAmount;
                            }

                        }
                        #endregion

                        #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3) Condition

                        foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3))
                        {
                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;

                            decimal buyAmount = 0.0M;

                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                            {
                                foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null))
                                {
                                    decimal lnFeeAmt = 0;
                                    if (chargeRate.NegotiatedChgUnit == 1)
                                    {
                                        lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 2)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 3)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 4)
                                    {
                                        lnFeeAmt = (decimal)((leg.ElapseTM * chargeRate.Quantity) / 100) * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)((leg.ElapseTM * chargeRate.Quantity) / 100) * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 5)
                                    {
                                        // Already Captured in this region - Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 6)
                                    {
                                        lnFeeAmt = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 7)
                                    {
                                        lnFeeAmt = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 8)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }

                                    chargeRate.FeeAMT += lnFeeAmt;
                                    lnTotBuyAmt += buyAmount;

                                    decimal chargeRateDisc = 0.0M;
                                    if (chargeRate.NegotiatedChgUnit != 5)
                                    {
                                        // Calculate Discount
                                        if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl))
                                        {
                                            chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                            lnTotCqDisc += chargeRateDisc;
                                        }

                                        // Calculate Tax
                                        if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl) && (bool)QuoteInProgress.IsTaxable && lnFeeAmt != null)
                                        {
                                            decimal taxAmount = 0.0M;
                                            if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                                taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                            else
                                                taxAmount = Math.Round((decimal)lnFeeAmt * (decimal)lnTaxRate / 100, 2);

                                            lnTotTaxAmt += taxAmount;
                                        }
                                    }

                                }
                            }
                        }
                        #endregion

                        #region Calculate Adjusted hours TotAdjAmt for first leg and ordernum 1

                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                        {
                            CQLeg FirstLeg = new CQLeg();
                            FirstLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.LegNUM == 1).SingleOrDefault();
                            lnTotUseAdj = 0;
                            if (FirstLeg != null)
                            {
                                //if (FirstLeg.DutyTYPE == 1)
                                //{
                                var StandardChargeDet = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();

                                if (StandardChargeDet != null)
                                {
                                    if (StandardChargeDet.CQFleetChargeDetailID != 0)
                                        StandardChargeDet.State = CQRequestEntityState.Modified;
                                    decimal buyAmount = 0.0M;

                                    if (QuoteInProgress.IsOverrideUsageAdj != null)
                                        lnTotUseAdj = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.SellDOM : (decimal)StandardChargeDet.SellIntl), 2);

                                    if (QuoteInProgress.IsOverrideUsageAdj != null)
                                        buyAmount = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.BuyDOM : (decimal)StandardChargeDet.BuyIntl), 2);

                                    lnTotBuyAmt += buyAmount;

                                    // Calculate Discount
                                    decimal chargeRateDisc = 0.0M;
                                    if ((bool)(((decimal)FirstLeg.DutyTYPE == 1) ? StandardChargeDet.IsDiscountDOM : StandardChargeDet.IsDiscountIntl))
                                    {
                                        chargeRateDisc = Math.Round((decimal)lnTotUseAdj * lnDiscRate / 100, 2);
                                        lnTotCqDisc += chargeRateDisc;
                                    }

                                    // Calculate Tax
                                    if (QuoteInProgress.IsDailyTaxAdj != null && QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsDailyTaxAdj && (bool)QuoteInProgress.IsTaxable)
                                    {
                                        decimal taxAmount = 0.0M;
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                            taxAmount = Math.Round(((decimal)lnTotUseAdj - (decimal)chargeRateDisc) * (decimal)QuoteInProgress.FederalTax / 100, 2);
                                        else
                                            taxAmount = Math.Round((decimal)lnTotUseAdj * (decimal)QuoteInProgress.FederalTax / 100, 2);

                                        lnTotTaxAmt += taxAmount;
                                    }
                                }
                                //}
                            }
                        }
                        #endregion

                        totchrg = CalculateSumOfTotalCharge();

                        if (QuoteInProgress.IsOverrideLandingFee != null)
                            lnTotlndfee = (bool)QuoteInProgress.IsOverrideLandingFee ? (decimal)QuoteInProgress.LandingFeeTotal : lnTotlndfee;

                        if (QuoteInProgress.IsLandingFeeTax != null && (bool)QuoteInProgress.IsLandingFeeTax)
                            lnTotTaxAmt += Math.Round((lnTotlndfee * lnTaxRate) / 100, 2);

                        //Sum of fee amount where ordernum > 3
                        decimal totQuoteChrg = 0;
                        totQuoteChrg = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3).Sum(x => x.FeeAMT).Value;

                        QuoteInProgress.SubtotalTotal = totQuoteChrg + totchrg;

                        if (QuoteInProgress.IsOverrideSegmentFee != null && !(bool)QuoteInProgress.IsOverrideSegmentFee)
                            QuoteInProgress.SegmentFeeTotal = lnTotSegFee;

                        if (QuoteInProgress.IsOverrideUsageAdj != null && !(bool)QuoteInProgress.IsOverrideUsageAdj)
                            QuoteInProgress.UsageAdjTotal = lnTotUseAdj;

                        if (QuoteInProgress.IsOverrideLandingFee != null && !(bool)QuoteInProgress.IsOverrideLandingFee)
                            QuoteInProgress.LandingFeeTotal = lnTotlndfee;

                        QuoteInProgress.SubtotalTotal = QuoteInProgress.SubtotalTotal + QuoteInProgress.UsageAdjTotal +
                                QuoteInProgress.SegmentFeeTotal + lnTotCrwRon + QuoteInProgress.LandingFeeTotal;
                        lnTotDiscAmt = lnTotCqDisc + lnTotLndDsc;

                        if (QuoteInProgress.IsOverrideDiscountAMT != null && !(bool)QuoteInProgress.IsOverrideDiscountAMT)
                            QuoteInProgress.DiscountAmtTotal = lnTotDiscAmt;
                        else
                        {
                            if (QuoteInProgress.DiscountAmtTotal != null)
                                lnTotDiscAmt = (decimal)QuoteInProgress.DiscountAmtTotal;

                            decimal subTotal = 0;
                            subTotal = ((decimal)QuoteInProgress.SubtotalTotal - ((decimal)QuoteInProgress.LandingFeeTotal + (decimal)QuoteInProgress.SegmentFeeTotal));

                            // Condtion for Attempted to divide by zero Error.
                            if (lnTotDiscAmt != 0 && subTotal != 0)
                                lnDiscRate = ((lnTotDiscAmt * 100) / subTotal);

                            lnDiscRate = Math.Round(lnDiscRate, 2);
                            QuoteInProgress.DiscountPercentageTotal = lnDiscRate;
                        }


                        if (QuoteInProgress.IsOverrideTaxes != null && !(bool)QuoteInProgress.IsOverrideTaxes)
                            QuoteInProgress.TaxesTotal = lnTotTaxAmt;
                        else if (QuoteInProgress.TaxesTotal != null)
                            lnTotTaxAmt = (decimal)QuoteInProgress.TaxesTotal;

                        if (QuoteInProgress.IsOverrideTotalQuote != null)
                            QuoteInProgress.QuoteTotal = (bool)QuoteInProgress.IsOverrideTotalQuote ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt);
                        QuoteInProgress.FlightHoursTotal = Math.Round(lnTotFltHrs, 2);
                        QuoteInProgress.DaysTotal = lnTotDays;
                        QuoteInProgress.AverageFlightHoursTotal = lnTotDays == 0 ? 0 : Math.Round(lnTotFltHrs / lnTotDays, 2);
                        QuoteInProgress.AdditionalFeeTotalD = totQuoteChrg;
                        QuoteInProgress.StdCrewRONTotal = lnTotCrwRon;
                        if (QuoteInProgress.IsOverrideUsageAdj != null)
                            QuoteInProgress.UsageAdjTotal = !(bool)QuoteInProgress.IsOverrideUsageAdj ? lnTotUseAdj : (decimal)QuoteInProgress.UsageAdjTotal;

                        QuoteInProgress.AdjAmtTotal = lnTotAdjAmt;
                        if (QuoteInProgress.PrepayTotal == null)
                            QuoteInProgress.PrepayTotal = 0;

                        if (QuoteInProgress.IsOverrideTotalQuote != null)
                            QuoteInProgress.RemainingAmtTotal = ((bool)QuoteInProgress.IsOverrideTotalQuote ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt)) - (decimal)QuoteInProgress.PrepayTotal;

                        QuoteInProgress.FlightChargeTotal = totchrg;

                        if (QuoteInProgress.IsOverrideCost != null && (bool)QuoteInProgress.IsOverrideCost)
                            lnTotCost = (decimal)QuoteInProgress.CostTotal;
                        else
                            QuoteInProgress.CostTotal = lnTotBuyAmt;

                        //Margin Calculation
                        lnTotMargin = (decimal)QuoteInProgress.SubtotalTotal - (decimal)QuoteInProgress.CostTotal - (decimal)QuoteInProgress.DiscountAmtTotal;
                        lnTotMarPct = (decimal)QuoteInProgress.SubtotalTotal == 0 ? 0 : Math.Round((lnTotMargin / (decimal)QuoteInProgress.SubtotalTotal)*100, 2);

                        QuoteInProgress.MarginTotal = lnTotMargin;
                        QuoteInProgress.MarginalPercentTotal = lnTotMarPct;
                    }
                    //}
                }
                BindCalculatedData();
                SaveQuoteinProgressToFileSession();
            }
        }

        protected void BindCalculatedData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            tbHrs.Text = string.Empty;
                            tbRONCrewCharge.Text = string.Empty;
                            tbLandingFees.Text = string.Empty;
                            tbDailyUseAdj.Text = string.Empty;
                            tbFlightCharges.Text = string.Empty;
                            tbSegFeesChrg.Text = string.Empty;
                            tbTotalQuote.Text = string.Empty;
                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM.ToString() == "1" && x.IsDeleted == false).FirstOrDefault();

                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (QuoteInProgress != null)
                                {
                                    if (QuoteInProgress.FlightHoursTotal != null)
                                    {
                                        tbHrs.Text = ConvertTenthsToMins(Math.Round(Convert.ToDecimal(QuoteInProgress.FlightHoursTotal), 3).ToString());
                                    }
                                    else
                                    {
                                        tbHrs.Text = "00:00";
                                    }
                                }
                            }
                            else
                            {
                                if (QuoteInProgress != null)
                                {

                                    if (QuoteInProgress.FlightHoursTotal != null)
                                    {
                                        tbHrs.Text = Convert.ToString(System.Math.Round(Convert.ToDecimal(QuoteInProgress.FlightHoursTotal.ToString()), 1));
                                    }
                                    else
                                        tbHrs.Text = "0.0";
                                }
                            }


                            //if (QuoteInProgress.FlightHoursTotal != null)
                            //    tbHrs.Text = Math.Round((decimal)QuoteInProgress.FlightHoursTotal, 2).ToString();
                            if (QuoteInProgress.StdCrewRONTotal != null)
                                tbRONCrewCharge.Text = Math.Round((decimal)QuoteInProgress.StdCrewRONTotal, 2).ToString();
                            if (QuoteInProgress.LandingFeeTotal != null)
                                tbLandingFees.Text = Math.Round((decimal)QuoteInProgress.LandingFeeTotal, 2).ToString();
                            if (QuoteInProgress.UsageAdjTotal != null)
                                tbDailyUseAdj.Text = Math.Round((decimal)QuoteInProgress.UsageAdjTotal, 2).ToString();
                            if (QuoteInProgress.FlightChargeTotal != null)
                                tbFlightCharges.Text = Math.Round((decimal)QuoteInProgress.FlightChargeTotal, 2).ToString();
                            if (QuoteInProgress.SegmentFeeTotal != null && QuoteInProgress.TaxesTotal != null)
                                tbSegFeesChrg.Text = Math.Round(((decimal)QuoteInProgress.SegmentFeeTotal + (decimal)QuoteInProgress.TaxesTotal), 2).ToString();
                            if (QuoteInProgress.StdCrewRONTotal != null && QuoteInProgress.LandingFeeTotal != null && QuoteInProgress.UsageAdjTotal != null && QuoteInProgress.FlightChargeTotal != null && QuoteInProgress.SegmentFeeTotal != null && QuoteInProgress.TaxesTotal != null)
                                tbTotalQuote.Text = Math.Round(((decimal)QuoteInProgress.StdCrewRONTotal + (decimal)QuoteInProgress.LandingFeeTotal + (decimal)QuoteInProgress.UsageAdjTotal + (decimal)QuoteInProgress.FlightChargeTotal + (decimal)QuoteInProgress.SegmentFeeTotal + (decimal)QuoteInProgress.TaxesTotal), 2).ToString();

                            if (!string.IsNullOrEmpty(tbTotalQuote.Text))
                                QuoteInProgress.QuoteTotal = Convert.ToDecimal(tbTotalQuote.Text);
                        }
                    }
                }
            }
        }


        private void BindFleetChargeDetails(long fleetID, long aircraftID, long CQCustomerID, bool isCQCustomer)
        {
            // Bind Fleet Charge Details into QuoteInProgress Object, based on Tail Number / Type Code
            FileRequest = (CQFile)Session[EQSessionKey];

            if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
            {
                Int64 QuoteNumInProgress = 1;
                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                if (QuoteInProgress != null)
                {
                    List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

                    if (QuoteInProgress.CQFleetChargeDetails != null)
                    {
                        // Delete the Existing Fleet Charge Details
                        List<CQFleetChargeDetail> ChargeToDeleteList = new List<CQFleetChargeDetail>();
                        ChargeToDeleteList = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList();

                        if (ChargeToDeleteList != null && ChargeToDeleteList.Count > 0)
                        {
                            foreach (CQFleetChargeDetail fleetCharge in ChargeToDeleteList)
                            {
                                if (fleetCharge.CQFleetChargeDetailID != 0)
                                {
                                    fleetCharge.IsDeleted = true;
                                    fleetCharge.State = CQRequestEntityState.Deleted;
                                }
                                else
                                    QuoteInProgress.CQFleetChargeDetails.Remove(fleetCharge);
                            }
                        }
                    }

                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        // Get New Charter Rates from Fleet Profile Catalog, based on CQCustomerID 
                        if (isCQCustomer == true && (CQCustomerID != null && CQCustomerID != 0))
                        {
                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == CQCustomerID).ToList();
                            //Get Quote details from CQCustomer
                            using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CQCustomer oCQCustomer = new FlightPakMasterService.CQCustomer();

                                var objCQCustomer = objMastersvc.GetCQCustomerList();
                                if (objCQCustomer != null && objCQCustomer.EntityList != null && objCQCustomer.EntityList.Count > 0)
                                {
                                    oCQCustomer = objCQCustomer.EntityList.Where(c => c.CQCustomerID == CQCustomerID).SingleOrDefault();
                                    if (oCQCustomer != null)
                                    {
                                        QuoteInProgress.StandardCrewDOM = oCQCustomer.DomesticStdCrewNum;
                                        QuoteInProgress.StandardCrewINTL = oCQCustomer.IntlStdCrewNum;
                                        QuoteInProgress.IsLandingFeeTax = oCQCustomer.LandingFeeTax;
                                        QuoteInProgress.IsDailyTaxAdj = oCQCustomer.DailyUsageAdjTax;
                                    }
                                }
                            }

                            using (MasterCatalogServiceClient objDstsvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList().EntityList.Where(x => x.FleetID == fleetID && x.IsDeleted == false);
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    Fleetlist = objRetVal.ToList();
                                    if (Fleetlist != null && Fleetlist.ToList().Count > 0)
                                    {
                                        if (Fleetlist[0].TailNum != null)
                                        {
                                            QuoteInProgress.StandardCrewDOM = Fleetlist[0].StandardCrewDOM;
                                            QuoteInProgress.StandardCrewINTL = Fleetlist[0].StandardCrewIntl;
                                            QuoteInProgress.IsLandingFeeTax = Fleetlist[0].IsTaxLandingFee;
                                            QuoteInProgress.IsDailyTaxAdj = Fleetlist[0].IsTaxDailyAdj;
                                        }
                                    }
                                }
                            }
                        }
                        // Get New Charter Rates from Fleet Profile Catalog
                        else if (fleetID != null && fleetID != 0)
                        {
                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == fleetID && x.AircraftTypeID == null).ToList();
                            //Get Quote details from Fleet
                            FlightPak.Web.FlightPakMasterService.Fleet fleet = new FlightPakMasterService.Fleet();

                            using (MasterCatalogServiceClient objDstsvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList().EntityList.Where(x => x.FleetID == fleetID && x.IsDeleted == false);
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    Fleetlist = objRetVal.ToList();
                                    if (Fleetlist != null)
                                    {
                                        if (Fleetlist[0].TailNum != null)
                                        {
                                            QuoteInProgress.StandardCrewDOM = Fleetlist[0].StandardCrewDOM;
                                            QuoteInProgress.StandardCrewINTL = Fleetlist[0].StandardCrewIntl;
                                            QuoteInProgress.IsLandingFeeTax = Fleetlist[0].IsTaxLandingFee;
                                            QuoteInProgress.IsDailyTaxAdj = Fleetlist[0].IsTaxDailyAdj;
                                        }
                                    }
                                }
                            }
                        }
                        // Get New Charter Rates from Aircraft Type Catalog, if the Source is selected as Type and Tail Number is Null/Empty
                        else if (aircraftID != null && aircraftID != 0)
                        {
                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID == null && (x.AircraftTypeID != null && x.AircraftTypeID == aircraftID)).ToList();
                            //Get Quote details from Aircraft
                            FlightPak.Web.FlightPakMasterService.Aircraft aircraft = new FlightPakMasterService.Aircraft();
                            aircraft = GetAircraft(aircraftID);
                            if (aircraft != null)
                            {
                                QuoteInProgress.StandardCrewDOM = aircraft.DomesticStdCrewNum;
                                QuoteInProgress.StandardCrewINTL = aircraft.IntlStdCrewNum;
                                QuoteInProgress.IsLandingFeeTax = aircraft.LandingFeeTax;
                                QuoteInProgress.IsDailyTaxAdj = aircraft.DailyUsageAdjTax;
                            }
                        }

                        if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                        {
                            if (QuoteInProgress.CQFleetChargeDetails == null)
                                QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();

                            foreach (FlightPakMasterService.GetAllFleetNewCharterRate chargeRate in FleetChargeRateList)
                            {
                                CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                fleetCharge.State = CQRequestEntityState.Added;
                                fleetCharge.CQFlightChargeDescription = chargeRate.FleetNewCharterRateDescription;
                                fleetCharge.ChargeUnit = chargeRate.ChargeUnit;
                                fleetCharge.NegotiatedChgUnit = chargeRate.NegotiatedChgUnit;
                                fleetCharge.Quantity = 0;
                                fleetCharge.BuyDOM = chargeRate.BuyDOM;
                                fleetCharge.SellDOM = chargeRate.SellDOM;
                                fleetCharge.IsTaxDOM = chargeRate.IsTaxDOM;
                                fleetCharge.IsDiscountDOM = chargeRate.IsDiscountDOM;
                                fleetCharge.BuyIntl = chargeRate.BuyIntl;
                                fleetCharge.SellIntl = chargeRate.SellIntl;
                                fleetCharge.IsTaxIntl = chargeRate.IsTaxIntl;
                                fleetCharge.IsDiscountIntl = chargeRate.IsDiscountIntl;
                                fleetCharge.FeeAMT = 0;
                                fleetCharge.OrderNUM = chargeRate.OrderNum;
                                fleetCharge.IsDeleted = false;

                                QuoteInProgress.CQFleetChargeDetails.Add(fleetCharge);
                            }
                            CalculateQuantity();
                            CalculateQuoteTotal();
                            SaveQuoteinProgressToFileSession();
                        }
                    }

                }
            }
        }

        public void CalculateQuantity()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                {
                    foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false))
                    {
                        if (chargeRate.NegotiatedChgUnit != null)
                        {
                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;

                            //Quantity = Sum of ETE where cqfltchg.nchrgunit is 1 
                            if (chargeRate.NegotiatedChgUnit == 1)
                            {
                                chargeRate.Quantity = CalculateSumOfETE((int)chargeRate.OrderNUM);
                            }

                            //Quantity = Sum of Distance where cqfltchg.nchrgunit is 2
                            //Quantity = Sum of Distance multiplied by StatueConvFactor where nchrgunit is 3
                            if (chargeRate.NegotiatedChgUnit == 2)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM);
                            }

                            if (chargeRate.NegotiatedChgUnit == 3)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * statuateConvFactor;
                            }

                            //Quantity = Total number of legs where nchrgunit is 7
                            if (chargeRate.NegotiatedChgUnit == 7)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                    chargeRate.Quantity = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Count();
                            }

                            if (chargeRate.NegotiatedChgUnit == 8)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * 1.852M;
                            }

                            //Quantity = Sum of RON and Day Room of all legs where ordernum is 3 
                            if (chargeRate.OrderNUM == 3)
                            {
                                chargeRate.Quantity = CalculateSumOfRON();
                            }
                        }
                    }

                    SaveQuoteinProgressToFileSession();

                    //BindStandardCharges(true);
                }
            }
        }

        public decimal CalculateSumOfDistance(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    if (orderNum == 1)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == false).Sum(x => x.Distance).Value;
                    }
                    else if (orderNum == 2)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == true).Sum(x => x.Distance).Value;
                    }
                    else
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Sum(x => x.Distance).Value;
                    }
                }
                return result;
            }
        }

        public decimal CalculateSumOfRON()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DayRONCNT != null && x.RemainOverNightCNT != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                }
                return result;
            }
        }

        public void CalculateRemainingRon(Int64 LegNUM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegNUM))
            {
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null)
                    {
                        CQLeg LeginProcess = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM).SingleOrDefault();

                        foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null))
                        {
                            if (Leg.LegNUM >= LegNUM)
                            {
                                Int64 currlegnum = (Int64)Leg.LegNUM;
                                Int64 nextLegnum = currlegnum + 1;

                                CQLeg CurrentLeg = new CQLeg();
                                CurrentLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == currlegnum && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();

                                if (CurrentLeg != null)
                                {
                                    CQLeg NextLeg = new CQLeg();
                                    NextLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && (x.LegNUM == nextLegnum) && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();
                                    if (NextLeg != null)
                                    {
                                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                        {
                                            if (NextLeg.CQLegID != 0)
                                                NextLeg.State = CQRequestEntityState.Modified;

                                            if (NextLeg.DepartureDTTMLocal != null && CurrentLeg.ArrivalDTTMLocal != null)
                                            {
                                                DateTime CurrDate = new DateTime();
                                                DateTime NextDate = new DateTime();

                                                NextDate = new DateTime(((DateTime)NextLeg.DepartureDTTMLocal).Year, ((DateTime)NextLeg.DepartureDTTMLocal).Month, ((DateTime)NextLeg.DepartureDTTMLocal).Day);
                                                CurrDate = new DateTime(((DateTime)CurrentLeg.ArrivalDTTMLocal).Year, ((DateTime)CurrentLeg.ArrivalDTTMLocal).Month, ((DateTime)CurrentLeg.ArrivalDTTMLocal).Day);

                                                TimeSpan daydiff = NextDate.Subtract(CurrDate);
                                                if (daydiff.Days < 0)
                                                {
                                                    CurrentLeg.RemainOverNightCNT = 0;
                                                    LeginProcess.ShowWarningMessage = false;
                                                    RadAjaxManager1.ResponseScripts.Add(@"radconfirm('Warning - A problem within the legs has occurred and Auto RON can not be used until corrected.  Either the Departure Date or Time, Arrival Date or Time or the Ron Count needs to be adjusted.  Check the AutoRON Is Activated/AutoRON Is De-Activated After all corrections have been made.',null, 440, 210,null,'Confirmation!');");
                                                    break;
                                                }
                                                else
                                                {
                                                    if (NextLeg.CQLegID != 0)
                                                        NextLeg.State = CQRequestEntityState.Modified;
                                                    int hourtoadd = 0;
                                                    int minutestoAdd = 0;
                                                    hourtoadd = ((DateTime)NextLeg.DepartureDTTMLocal).Hour;
                                                    minutestoAdd = ((DateTime)NextLeg.DepartureDTTMLocal).Minute;

                                                    NextLeg.DepartureDTTMLocal = ((DateTime)CurrentLeg.ArrivalDTTMLocal).AddDays((double)CurrentLeg.RemainOverNightCNT);

                                                    NextLeg.DepartureDTTMLocal = new DateTime(
                                                        ((DateTime)NextLeg.DepartureDTTMLocal).Year,
                                                        ((DateTime)NextLeg.DepartureDTTMLocal).Month,
                                                        ((DateTime)NextLeg.DepartureDTTMLocal).Day,
                                                        hourtoadd,
                                                        minutestoAdd,
                                                        0);

                                                    NextLeg.DepartureGreenwichDTTM = objDstsvc.GetGMT((Int64)CurrentLeg.DAirportID, (DateTime)NextLeg.DepartureDTTMLocal, true, false);
                                                    DoDatetimecalculationforLeg(ref NextLeg);
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        public void calculateRON(Int64 LegNUM, string FieldChanged)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegNUM, FieldChanged))
            {
                if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                {
                    CQLeg CurrentLeg = new CQLeg();
                    CurrentLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();

                    if (CurrentLeg != null)
                    {
                        if (UserPrincipal.Identity._fpSettings._IsAutoRON != null && (bool)UserPrincipal.Identity._fpSettings._IsAutoRON)
                        {
                            switch (FieldChanged.ToUpper())
                            {
                                case "RON":
                                    {
                                        CQLeg NextLeg = new CQLeg();
                                        NextLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && (x.LegNUM == LegNUM + 1) && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();
                                        if (NextLeg != null)
                                        {
                                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                            {
                                                if (NextLeg.CQLegID != 0)
                                                    NextLeg.State = CQRequestEntityState.Modified;

                                                int hourtoadd = 0;
                                                int minutestoAdd = 0;
                                                hourtoadd = ((DateTime)NextLeg.DepartureDTTMLocal).Hour;
                                                minutestoAdd = ((DateTime)NextLeg.DepartureDTTMLocal).Minute;


                                                NextLeg.DepartureDTTMLocal = ((DateTime)CurrentLeg.ArrivalDTTMLocal).AddDays((double)CurrentLeg.RemainOverNightCNT);

                                                NextLeg.DepartureDTTMLocal = new DateTime(
                                                    ((DateTime)NextLeg.DepartureDTTMLocal).Year,
                                                    ((DateTime)NextLeg.DepartureDTTMLocal).Month,
                                                    ((DateTime)NextLeg.DepartureDTTMLocal).Day,
                                                    hourtoadd,
                                                    minutestoAdd,
                                                    0);

                                                NextLeg.DepartureGreenwichDTTM = objDstsvc.GetGMT((Int64)CurrentLeg.DAirportID, (DateTime)NextLeg.DepartureDTTMLocal, true, false);
                                                DoDatetimecalculationforLeg(ref NextLeg);
                                            }
                                        }

                                    }
                                    break;
                                case "DATE":
                                    {
                                        CQLeg PrevLeg = new CQLeg();
                                        PrevLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && (x.LegNUM == LegNUM - 1) && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();
                                        if (PrevLeg != null)
                                        {
                                            if (PrevLeg.CQLegID != 0)
                                                PrevLeg.State = CQRequestEntityState.Modified;

                                            DateTime CurrDate = new DateTime();
                                            DateTime PrevDate = new DateTime();

                                            CurrDate = new DateTime(((DateTime)CurrentLeg.DepartureDTTMLocal).Year, ((DateTime)CurrentLeg.DepartureDTTMLocal).Month, ((DateTime)CurrentLeg.DepartureDTTMLocal).Day);
                                            PrevDate = new DateTime(((DateTime)PrevLeg.ArrivalDTTMLocal).Year, ((DateTime)PrevLeg.ArrivalDTTMLocal).Month, ((DateTime)PrevLeg.ArrivalDTTMLocal).Day);

                                            TimeSpan daydiff = CurrDate.Subtract(PrevDate);
                                            if (daydiff.Days >= 0)
                                                PrevLeg.RemainOverNightCNT = daydiff.Days;
                                            else
                                            {
                                                PrevLeg.RemainOverNightCNT = 0;
                                                CurrentLeg.ShowWarningMessage = false;
                                                RadAjaxManager1.ResponseScripts.Add(@"radconfirm('Warning - A problem within the legs has occurred and Auto RON can not be used until corrected.  Either the Departure Date or Time, Arrival Date or Time or the Ron Count needs to be adjusted.  Check the AutoRON Is Activated/AutoRON Is De-Activated After all corrections have been made.',null, 440, 210,null,'Confirmation!');");
                                            }
                                        }
                                    }
                                    break;

                            }

                            if (UserPrincipal.Identity._fpSettings._IsAutoRollover != null && (bool)UserPrincipal.Identity._fpSettings._IsAutoRollover)
                                CalculateRemainingRon(LegNUM);
                        }
                    }
                    SaveQuoteinProgressToFileSession();
                }
            }
        }

        public void DoDatetimecalculationforLeg(ref CQLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                CalculateDateTime(ref Leg);
                CalculateWind(ref Leg);
                CalculateETE(ref Leg);
                Leg.IsDepartureConfirmed = true;
                CalculateDateTime(ref Leg);
            }
        }

        private void CalculateETE(ref CQLeg Leg)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                //try
                //{
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    double ETE = 0;
                    // lnToBias, lnLndBias, lnTas
                    double Wind = 0;
                    double LandBias = 0;
                    double TAS = 0;
                    double Bias = 0;
                    double Miles = 0;

                    if (Leg.WindsBoeingTable != null)
                        Wind = (double)Leg.WindsBoeingTable;

                    if (Leg.LandingBIAS != null)
                        LandBias = (double)Leg.LandingBIAS;

                    if (Leg.TakeoffBIAS != null)
                        Bias = (double)Leg.TakeoffBIAS;


                    if (Leg.TrueAirSpeed != null)
                        TAS = (long)Leg.TrueAirSpeed;



                    if (Leg.Distance != null)
                    {
                        Miles = (double)Leg.Distance;
                    }

                    DateTime dt = DateTime.Now;

                    if (Leg.DepartureDTTMLocal != null)
                        dt = (DateTime)Leg.DepartureDTTMLocal;
                    ETE = objDstsvc.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, dt);

                    ETE = RoundElpTime(ETE);

                    Leg.ElapseTM = (decimal)ETE;
                }

            }
        }

        private void CalculateWind(ref CQLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                if (Leg.DepartureDTTMLocal != null)
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {
                        //string StartTime = rmtlocaltime.Text.Trim();
                        string StartTime = "0000";

                        DateTime dt = (DateTime)Leg.DepartureDTTMLocal;

                        //Function Wind Calculation
                        double Wind = 0;
                        if (Leg.DAirportID != null && Leg.AAirportID != null && Leg.DAirportID != 0 && Leg.AAirportID != 0
                            && QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID != null
                            )
                        {
                            Wind = objDstsvc.GetWind((long)Leg.DAirportID, (long)Leg.AAirportID, (int)Leg.WindReliability, (long)QuoteInProgress.AircraftID, GetQtr(dt).ToString());
                            Leg.WindsBoeingTable = (decimal)Wind;
                        }
                    }
                }
                else
                    Leg.WindsBoeingTable = 0;

            }
        }

        private void CalculateDateTime(ref CQLeg Leg)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                double x10, x11;
                DateTime Arrivaldt;
                DateTime ChangedDate = (DateTime)Leg.DepartureDTTMLocal;
                bool isDepartureConfirmed = true;
                if (ChangedDate != null)
                {


                    DateTime EstDepartDate = (DateTime)ChangedDate;

                    try
                    {

                        //(!string.IsNullOrEmpty(hdnHomebaseAirport.Value) && hdnHomebaseAirport.Value != "0")&& 

                        if (Leg.DAirportID != null && Leg.AAirportID != null && Leg.DAirportID != 0 && Leg.AAirportID != 0)
                        {

                            if (Leg.ElapseTM != null)
                            {
                                double QuoteInProgressleg_elp_time = RoundElpTime((double)Leg.ElapseTM);
                                x10 = (QuoteInProgressleg_elp_time * 60 * 60);
                                x11 = (QuoteInProgressleg_elp_time * 60 * 60);
                            }
                            else
                            {
                                x10 = 0;
                                x11 = 0;
                            }
                            DateTime DeptUTCdt = DateTime.MinValue;
                            DateTime ArrUTCdt = DateTime.MinValue;

                            if (isDepartureConfirmed)
                            {
                                //DeptUTCDate
                                if (Leg.DepartureGreenwichDTTM != null)
                                    DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;

                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {
                                    Arrivaldt = DeptUTCdt;
                                    Arrivaldt = Arrivaldt.AddSeconds(x10);
                                    ArrUTCdt = Arrivaldt;

                                    Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                }
                            }

                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime ldLocDep = DateTime.MinValue;
                                DateTime ldLocArr = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                if (Leg.DAirportID != null && Leg.DAirportID != 0)
                                {
                                    ldLocDep = objDstsvc.GetGMT((long)Leg.DAirportID, DeptUTCdt, false, false);
                                }

                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                {
                                    ldHomDep = objDstsvc.GetGMT((long)FileRequest.HomeBaseAirportID, DeptUTCdt, false, false);
                                }

                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {
                                    ldLocArr = objDstsvc.GetGMT((long)Leg.AAirportID, ArrUTCdt, false, false);
                                }

                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                {
                                    ldHomArr = objDstsvc.GetGMT((long)FileRequest.HomeBaseAirportID, ArrUTCdt, false, false);
                                }

                                //DateTime ltBlank = EstDepartDate;

                                if (Leg.DAirportID != null && Leg.DAirportID != 0)
                                {
                                    Leg.DepartureDTTMLocal = ldLocDep;
                                    if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                        // DateTime Homedt = ldHomDep;
                                        Leg.HomeDepartureDTTM = ldHomDep;
                                }

                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {
                                    Leg.ArrivalDTTMLocal = ldLocArr;
                                    // DateTime dt = ldLocArr;

                                    if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                        Leg.HomeArrivalDTTM = ldHomArr;
                                    // DateTime HomeArrivaldt = ldHomArr;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Manually handled

                        if (Leg.ArrivalDTTMLocal == null)
                            Leg.ArrivalDTTMLocal = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                        if (Leg.HomeDepartureDTTM == null || Leg.HomeArrivalDTTM == null)
                        {
                            Leg.HomeDepartureDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);
                            Leg.HomeArrivalDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);
                        }

                        if (Leg.ArrivalGreenwichDTTM == null)
                            Leg.ArrivalGreenwichDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                        if (Leg.DepartureDTTMLocal == null)
                            Leg.DepartureDTTMLocal = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                        if (Leg.DepartureGreenwichDTTM == null)
                            Leg.DepartureGreenwichDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);
                    }
                }
            }
        }

        private DateTime CalculateGMTArr(DateTime gmtdep)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(gmtdep))
            {
                double Wind = 0;
                double ETE = 0;
                double LandBias = 0;
                double TAS = 0;
                double Bias = 0;
                double Miles = 0;
                DateTime ArrUTCdt = DateTime.MinValue;
                using (CalculationService.CalculationServiceClient CalcService = new CalculationService.CalculationServiceClient())
                {

                    //Function Wind Calculation
                    if (
                    (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                    &&
                    (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                    &&
                    (!string.IsNullOrEmpty(hdnTypeCode.Value) && hdnTypeCode.Value != "0")
                    )
                    {
                        Wind = CalcService.GetWind(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt32(rblistWindReliability.SelectedValue), Convert.ToInt64(hdnTypeCode.Value), GetQtr(gmtdep).ToString());
                    }

                    double LandingBias = 0;
                    string LandingBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                    else
                        LandingBiasStr = tbLandBias.Text;

                    if (double.TryParse(LandingBiasStr, out LandingBias))
                        LandBias = LandingBias;
                    else
                        LandBias = 0;

                    double tobias = 0;
                    string TakeOffBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "1");
                    }
                    else
                        TakeOffBiasStr = tbBias.Text;
                    if (double.TryParse(TakeOffBiasStr, out tobias))
                        Bias = tobias;
                    else
                        Bias = 0;

                    //if (!string.IsNullOrEmpty(tbLandBias.Text))
                    //{
                    //    LandBias = Convert.ToDouble(tbLandBias.Text);
                    //}
                    if (!string.IsNullOrEmpty(tbTAS.Text))
                    {
                        TAS = Convert.ToDouble(tbTAS.Text);
                    }

                    ETE = CalcService.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, gmtdep);

                    ETE = RoundElpTime(ETE);
                    double x10 = 0;
                    double x11 = 0;
                    x10 = (ETE * 60 * 60);
                    x11 = (ETE * 60 * 60);


                    ArrUTCdt = gmtdep;
                    ArrUTCdt = ArrUTCdt.AddSeconds(x10);
                }
                return ArrUTCdt;
            }
        }

        private DateTime CalculateGMTDep(DateTime gmtarr)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(gmtarr))
            {
                double Wind = 0;
                double ETE = 0;
                double LandBias = 0;
                double TAS = 0;
                double Bias = 0;
                double Miles = 0;
                DateTime depUTCdt = DateTime.MinValue;
                using (CalculationService.CalculationServiceClient CalcService = new CalculationService.CalculationServiceClient())
                {

                    //Function Wind Calculation
                    if (
                    (!string.IsNullOrEmpty(hdnDepart.Value) && hdnDepart.Value != "0")
                    &&
                    (!string.IsNullOrEmpty(hdnArrival.Value) && hdnArrival.Value != "0")
                    &&
                    (!string.IsNullOrEmpty(hdnTypeCode.Value) && hdnTypeCode.Value != "0")
                    )
                    {
                        Wind = CalcService.GetWind(Convert.ToInt64(hdnDepart.Value), Convert.ToInt64(hdnArrival.Value), Convert.ToInt32(rblistWindReliability.SelectedValue), Convert.ToInt64(hdnTypeCode.Value), GetQtr(gmtarr).ToString());
                    }

                    double LandingBias = 0;
                    string LandingBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        LandingBiasStr = ConvertMinToTenths(tbLandBias.Text, true, "1");
                    else
                        LandingBiasStr = tbLandBias.Text;

                    if (double.TryParse(LandingBiasStr, out LandingBias))
                        LandBias = LandingBias;
                    else
                        LandBias = 0;

                    double tobias = 0;
                    string TakeOffBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        TakeOffBiasStr = ConvertMinToTenths(tbBias.Text, true, "1");
                    }
                    else
                        TakeOffBiasStr = tbBias.Text;
                    if (double.TryParse(TakeOffBiasStr, out tobias))
                        Bias = tobias;
                    else
                        Bias = 0;

                    //if (!string.IsNullOrEmpty(tbLandBias.Text))
                    //{
                    //    LandBias = Convert.ToDouble(tbLandBias.Text);
                    //}
                    if (!string.IsNullOrEmpty(tbTAS.Text))
                    {
                        TAS = Convert.ToDouble(tbTAS.Text);
                    }

                    ETE = CalcService.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, gmtarr);

                    ETE = RoundElpTime(ETE);
                    double x10 = 0;
                    double x11 = 0;
                    x10 = (ETE * 60 * 60);
                    x11 = (ETE * 60 * 60);


                    depUTCdt = gmtarr;
                    depUTCdt = depUTCdt.AddSeconds(-1 * x10);
                }
                return depUTCdt;
            }
        }

        private bool CheckDatechangeValid(DateTime gmtdep, DateTime gmtarr, Int64 LegNUM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(gmtdep, gmtarr, LegNUM))
            {
                bool returnval = true;
                if (LegNUM > 1)
                {
                    if (UserPrincipal.Identity._fpSettings._IsAutoRON)
                    {
                        if (QuoteInProgress != null)
                        {
                            CQLeg CurrLeg = new CQLeg();
                            CQLeg PrevLeg = new CQLeg();
                            CQLeg NextLeg = new CQLeg();

                            CurrLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM).SingleOrDefault();
                            PrevLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM - 1).SingleOrDefault();
                            NextLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM + 1).SingleOrDefault();

                            if (CurrLeg != null && PrevLeg != null)
                            {
                                if (PrevLeg.ArrivalGreenwichDTTM != null)
                                {
                                    double minron = 0;
                                    if (UserPrincipal.Identity._fpSettings._MinuteHoursRON != null)
                                        minron = (double)UserPrincipal.Identity._fpSettings._MinuteHoursRON;
                                    if (gmtdep <= ((DateTime)PrevLeg.ArrivalGreenwichDTTM).AddHours(minron))
                                    {
                                        if (PrevLeg.CQLegID != 0)
                                            PrevLeg.State = CQRequestEntityState.Modified;
                                        PrevLeg.RemainOverNightCNT = 0;
                                    }
                                    else
                                    {
                                        DateTime CurrDate = new DateTime();
                                        DateTime PrevDate = new DateTime();

                                        CurrDate = new DateTime(gmtdep.Year, gmtdep.Month, gmtdep.Day);
                                        PrevDate = new DateTime(((DateTime)PrevLeg.ArrivalDTTMLocal).Year, ((DateTime)PrevLeg.ArrivalDTTMLocal).Month, ((DateTime)PrevLeg.ArrivalDTTMLocal).Day);

                                        TimeSpan daydiff = CurrDate.Subtract(PrevDate);
                                        if (daydiff.Days > 99)
                                            returnval = false;

                                    }
                                }
                            }


                            if (CurrLeg != null && NextLeg != null)
                            {
                                if (NextLeg.DepartureGreenwichDTTM != null)
                                {
                                    if (gmtarr.AddHours((double)UserPrincipal.Identity._fpSettings._MinuteHoursRON) >= ((DateTime)NextLeg.DepartureGreenwichDTTM))
                                    {
                                        tbRON.Text = "0";
                                    }
                                    else
                                    {
                                        DateTime CurrDate = new DateTime();
                                        DateTime NextDate = new DateTime();

                                        CurrDate = new DateTime(gmtarr.Year, gmtarr.Month, gmtarr.Day);
                                        NextDate = new DateTime(((DateTime)NextLeg.DepartureDTTMLocal).Year, ((DateTime)NextLeg.DepartureDTTMLocal).Month, ((DateTime)NextLeg.DepartureDTTMLocal).Day);

                                        TimeSpan daydiff = NextDate.Subtract(CurrDate);
                                        if (daydiff.Days > 99)
                                            returnval = false;

                                    }
                                }
                            }


                            SaveQuoteinProgressToFileSession();
                        }
                    }
                }
                return returnval;
            }
        }

        public void DoLegCalculationsForDomIntlTaxable()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Fleetcompany = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                    List<FlightPakMasterService.GetFleet> GetFleetList = new List<FlightPakMasterService.GetFleet>();
                    List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<FlightPakMasterService.GetAllAirport>();
                    var fleetretval = FleetService.GetFleet();
                    if (fleetretval.ReturnFlag)
                    {
                        if (QuoteInProgress != null)
                        {
                            if (QuoteInProgress.FleetID != 0 && QuoteInProgress.FleetID != null)
                            {
                                GetFleetList = fleetretval.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();
                                if (GetFleetList != null && GetFleetList.Count > 0)
                                {
                                    if (GetFleetList[0].HomebaseID != null)
                                    {
                                        Fleetcompany = GetCompany((long)GetFleetList[0].HomebaseID);
                                        FlightPakMasterService.GetAllAirport Fleetcompanyairport = new FlightPakMasterService.GetAllAirport();
                                        if (Fleetcompany.HomebaseAirportID != null)
                                        {
                                            Int64 countryID = 0;
                                            Fleetcompanyairport = GetAirport((long)Fleetcompany.HomebaseAirportID);
                                            if (Fleetcompanyairport != null && Fleetcompanyairport.CountryID != null)
                                                countryID = (long)Fleetcompanyairport.CountryID;

                                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count >= 0)
                                            {
                                                List<CQLeg> PrefLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                                foreach (CQLeg Leg in PrefLeg)
                                                {
                                                    bool isRural = false;
                                                    if (Leg.DAirportID != 0 && Leg.AAirportID != 0 && Leg.DAirportID != null && Leg.AAirportID != null)
                                                    {
                                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            var objDepRetVal = objDstsvc.GetAirportByAirportID((long)Leg.DAirportID).EntityList;
                                                            var objArrRetVal = objDstsvc.GetAirportByAirportID((long)Leg.AAirportID).EntityList;

                                                            Leg.DutyTYPE = 1;
                                                            #region "DOM/INTL"
                                                            if (countryID != null && countryID != 0)
                                                            {
                                                                if (objDepRetVal.Count > 0 && objArrRetVal.Count > 0)
                                                                {
                                                                    if (objDepRetVal[0].CountryID == null)
                                                                    {
                                                                        Leg.DutyTYPE = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (objDepRetVal[0].CountryID != null)
                                                                        {
                                                                            if (countryID != objDepRetVal[0].CountryID)
                                                                            {
                                                                                Leg.DutyTYPE = 2;
                                                                            }
                                                                        }
                                                                    }

                                                                    if (objArrRetVal[0].CountryID == null)
                                                                    {
                                                                        Leg.DutyTYPE = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (objArrRetVal[0].CountryID != null)
                                                                        {

                                                                            if (countryID != objArrRetVal[0].CountryID)
                                                                            {
                                                                                Leg.DutyTYPE = 2;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion

                                                            if (objDepRetVal[0].IsRural != null && (bool)objDepRetVal[0].IsRural)
                                                                isRural = true;

                                                            if (objArrRetVal[0].IsRural != null && (bool)objArrRetVal[0].IsRural)
                                                                isRural = true;

                                                            #region TAX
                                                            //Taxable Company profile Settings
                                                            if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 1)
                                                            {
                                                                Leg.IsTaxable = true;

                                                            }
                                                            else if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                                                            {
                                                                if (Leg.DutyTYPE == 1)
                                                                    Leg.IsTaxable = true;
                                                            }
                                                            else
                                                                Leg.IsTaxable = false;

                                                            if ((bool)Leg.IsTaxable)
                                                            {
                                                                Leg.TaxRate = 0;

                                                                if (isRural)
                                                                {
                                                                    if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                                        Leg.TaxRate = UserPrincipal.Identity._fpSettings._RuralTax;
                                                                }
                                                                else
                                                                {
                                                                    if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                                        Leg.TaxRate = UserPrincipal.Identity._fpSettings._CQFederalTax;
                                                                }
                                                            }
                                                            //Taxable Company profile Settings
                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            SaveQuoteinProgressToFileSession();
                        }
                    }
                }
            }
        }
        #endregion

        #region TextChange Events

        protected void rbDomestic_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        CalculateQuoteTotal();
                        BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadFileDetails();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void chkPOS_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CalculateQuantity();
                        SaveCharterQuoteToSession();
                        CalculateQuoteTotal();

                        BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadFileDetails();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void chkTaxable_CheckChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkTaxable.Checked == true)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                bool isRural = false;
                                if (!string.IsNullOrEmpty(hdnArrival.Value))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.GetAllAirport> ArrAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                    var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnArrival.Value));

                                    if (objArrRetVal.ReturnFlag)
                                    {
                                        ArrAirportLists = objArrRetVal.EntityList;

                                        if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                        {
                                            if (ArrAirportLists[0].IsRural != null && (bool)ArrAirportLists[0].IsRural)
                                                isRural = true;

                                            if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                            {
                                                if (ArrAirportLists[0].CountryID == null)
                                                {
                                                    rbDomestic.SelectedValue = "1";
                                                }
                                                else
                                                {
                                                    if (ArrAirportLists[0].CountryID != null)
                                                    {
                                                        if (hdnCountryID.Value != ArrAirportLists[0].CountryID.ToString())
                                                        {
                                                            rbDomestic.SelectedValue = "2";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!string.IsNullOrEmpty(hdnDepart.Value))
                                {
                                    var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdnDepart.Value));

                                    if (objArrRetVal.ReturnFlag)
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                        AirportLists = objArrRetVal.EntityList;

                                        if (AirportLists != null && AirportLists.Count > 0)
                                        {
                                            if (AirportLists[0].IsRural != null && (bool)AirportLists[0].IsRural)
                                                isRural = true;

                                            if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                            {
                                                if (AirportLists[0].CountryID == null || AirportLists[0].CountryID == null)
                                                {
                                                    rbDomestic.SelectedValue = "1";
                                                }
                                                else
                                                {
                                                    if (AirportLists[0].CountryID != null || AirportLists[0].CountryID != null)
                                                    {
                                                        if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                        {
                                                            rbDomestic.SelectedValue = "2";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (isRural)
                                {
                                    if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                        tbTaxRate.Text = UserPrincipal.Identity._fpSettings._RuralTax.ToString();
                                }
                                else
                                {
                                    if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                        tbTaxRate.Text = UserPrincipal.Identity._fpSettings._CQFederalTax.ToString();
                                }
                                SaveQuoteinProgressToFileSession();
                            }
                        }
                        else
                        {
                            tbTaxRate.Text = "0.00";
                        }
                        //Taxable Company profile Settings

                        CalculateQuantity();
                        SaveCharterQuoteToSession();
                        CalculateQuoteTotal();

                        BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadFileDetails();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void tbRON_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbRON.Text))
                        {
                            decimal ron = 0.0M;
                            if (decimal.TryParse(tbRON.Text, out ron))
                            {
                                if (ron < 0)
                                {
                                    ron = 0;
                                }
                                else if (ron > 99)
                                {
                                    RadWindowManager1.RadAlert("The RON days exceed 99 days.This is NOT allowed by the system", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbRON.Text = "0";
                                }
                                else
                                {
                                    //ShowWarnMessage();
                                    CalculateQuantity();
                                    calculateRON(Convert.ToInt64(hdnLegNum.Value), "RON");
                                    SaveCharterQuoteToSession();
                                    CalculateQuoteTotal();

                                    BindLegs(dgLegs, true);
                                    if (dgLegs.Items.Count > 0)
                                    {
                                        MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                                    }
                                    LoadFileDetails();
                                }
                            }
                            else
                            {
                                tbRON.Text = "0";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void tbDayRoom_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        CalculateQuantity();
                        calculateRON(Convert.ToInt64(hdnLegNum.Value), "RON");
                        //ShowWarnMessage();
                        CalculateQuoteTotal();

                        BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadFileDetails();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void tbLegRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbLegRate.Text))
                        {
                            bool correctFormat = true;
                            int count = 0;
                            string legRate = tbLegRate.Text;
                            for (int i = 0; i < legRate.Length; ++i)
                            {
                                if (legRate[i].ToString() == ".")
                                    count++;
                            }
                            if (count > 1)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is ###############.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                tbLegRate.Text = "0.00";
                                correctFormat = false;
                            }
                            if (legRate.IndexOf(".") >= 0)
                            {
                                string[] strarr = legRate.Split('.');
                                if (strarr[0].Length > 15)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ###############.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbLegRate.Text = "0.00";
                                    correctFormat = false;
                                }
                                if (strarr[1].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ###############.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbLegRate.Text = "0.00";
                                    correctFormat = false;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[0].Length == 0)
                                        legRate = "0" + legRate;
                                    tbLegRate.Text = legRate;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[1].Length == 0)
                                        legRate = legRate + "00";
                                    if (strarr[1].Length == 1)
                                        legRate = legRate + "0";
                                    tbLegRate.Text = legRate;
                                }
                            }
                            else
                            {
                                if (legRate.Length > 15)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ###############.##", 330, 100, ModuleNameConstants.CharterQuote.CQExpQuote, null);
                                    tbLegRate.Text = "0.00";
                                    correctFormat = false;
                                }
                                else
                                {
                                    tbLegRate.Text = legRate + ".00";
                                    SaveCharterQuoteToSession();
                                }
                            }
                        }

                        //SaveCharterQuoteToSession();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        protected void tbPaxCount_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        BindLegs(dgLegs, true);
                        if (dgLegs.Items.Count > 0)
                        {
                            MarkLegItemSelected(Convert.ToInt64(hdnLegNum.Value));
                        }
                        LoadFileDetails();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        #endregion

        // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates
        protected void ConfirmAutoUpdateRateYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNum.Value) ? Convert.ToInt64(hdnTailNum.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
                }
            }
        }

        public bool IsValidDate(string dateString)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dateString))
            {
                bool isValid = true;

                if (!string.IsNullOrEmpty(dateString))
                {
                    DateTime mindate = new DateTime(1900, 1, 1);
                    DateTime maxdate = new DateTime(2100, 12, 31);

                    DateTime Departdate = DateTime.ParseExact(dateString, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                    if (Departdate.Year < 1900 || Departdate.Year > 2100)
                    {
                        isValid = false;
                        RadAjaxManager1.ResponseScripts.Add(@"radalert('Please enter/select Date between " + String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", mindate) + " and " + String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", maxdate) + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQExpQuote + "' ) ;");
                    }
                }
                return isValid;
            }
        }

        #region "Reports"
        /// <summary>
        /// To show reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkReport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session[EQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[EQSessionKey];
                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        string userCd = Microsoft.Security.Application.Encoder.UrlEncode(UserPrincipal.Identity._name.Trim());
                        string fileNum = Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(FileRequest.FileNUM));
                        string fileId = Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(FileRequest.CQFileID));
                        string quoteNum = Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(FileRequest.CQMains[0].QuoteNUM));
                        string cQMainId = Microsoft.Security.Application.Encoder.UrlEncode(Convert.ToString(FileRequest.CQMains[0].CQMainID));
                        RedirectToPage(string.Format(@"..\..\Reports\CQReportViewer.aspx?xmlFilename=CQItinerary.xml&UserCD={0}&filenum={1}&fileid={2}&quotenum={3}&quoteid={4}&isexpressquote=true", userCd, fileNum, fileId, quoteNum, cQMainId));
                    }
                }
            }
        }
        #endregion

        //protected void TenthMin_TextChanged(object sender, EventArgs e)
        //{

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                string TextboxID = string.Empty;
        //                string TextboxValue = string.Empty;

        //                if (sender != null && sender.ToString().Contains("RadNumericTextBox"))
        //                {
        //                    TextboxID = ((RadNumericTextBox)sender).ID;
        //                    TextboxValue = ((RadNumericTextBox)sender).Text.Trim();
        //                }
        //                else
        //                {
        //                    TextboxID = ((TextBox)sender).ID;
        //                    TextboxValue = ((TextBox)sender).Text.Trim();
        //                }
        //                hdnTempHoursTxtbox.Value = TextboxID;
        //                if (ValidateText(TextboxValue))
        //                {
        //                    if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2))
        //                    {
        //                        if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
        //                        {
        //                            if (TextboxValue.IndexOf(":") != -1)
        //                            {
        //                                string[] timeArray = TextboxValue.Split(':');
        //                                if (string.IsNullOrEmpty(timeArray[0]))
        //                                {
        //                                    timeArray[0] = "0";
        //                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
        //                                }
        //                                if (!string.IsNullOrEmpty(timeArray[1]))
        //                                {
        //                                    Int64 result = 0;
        //                                    if (timeArray[1].Length != 2)
        //                                    {
        //                                        result = (Convert.ToInt32(timeArray[1])) / 10;
        //                                        if (result < 1)
        //                                        {
        //                                            if (timeArray[0] != null)
        //                                            {
        //                                                if (timeArray[0].Trim() == "")
        //                                                {
        //                                                    timeArray[0] = "0";
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                timeArray[0] = "0";
        //                                            }
        //                                            TextboxValue = timeArray[0] + ":" + "0" + timeArray[1];

        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    TextboxValue = TextboxValue + "00";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                TextboxValue = TextboxValue + ":00";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            TextboxValue = "0:00";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if ((TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != "."))
        //                        {
        //                            if (TextboxValue.IndexOf(".") != -1)
        //                            {
        //                                string[] timeArray = TextboxValue.Split('.');
        //                                if (string.IsNullOrEmpty(timeArray[0]))
        //                                {
        //                                    timeArray[0] = "0";
        //                                    TextboxValue = timeArray[0] + "." + timeArray[1];
        //                                }
        //                                if (!string.IsNullOrEmpty(timeArray[1]))
        //                                {
        //                                    if (timeArray[0] != null)
        //                                    {
        //                                        if (timeArray[0].Trim() == "")
        //                                        {
        //                                            timeArray[0] = "0";
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        timeArray[0] = "0";
        //                                    }
        //                                    TextboxValue = timeArray[0] + "." + timeArray[1];
        //                                }
        //                                else
        //                                {
        //                                    TextboxValue = TextboxValue + "0";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                TextboxValue = TextboxValue + ".0";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            TextboxValue = "0.0";
        //                        }
        //                    }
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);

        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQExpQuote);
        //        }
        //    }

        //}

        private bool ValidateText(string text)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(text))
            {
                bool IsCheck = true;
                if (UserPrincipal.Identity != null)
                {
                    Regex Tenths = new Regex("^[0-9]{0,7}(\\.[0-9]{0,1})?$");
                    Regex Minutes = new Regex("^[0-9]{0,7}(:([0-5]{0,1}[0-9]{0,1}))?$");
                    string TenthFormatMsg = "The Format is NNNNNNN.N";
                    string MinuteFormatMsg = "The Format is HHHHHHH:MM";

                    if (hdnTempHoursTxtbox.Value == "tbHrs")
                    {
                        Tenths = new Regex("^[0-9]{0,2}(\\.[0-9]{0,1})?$");
                        Minutes = new Regex("^[0-9]{0,2}(:([0-5]{0,1}[0-9]{0,1}))?$");
                        TenthFormatMsg = "The Format is NN.N";
                        MinuteFormatMsg = "The Format is HH:MM";
                    }
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        if (!string.IsNullOrEmpty(text))
                        {
                            if (!Tenths.IsMatch(text))
                            {
                                string msgToDisplay = "You entered: " + text + "." + "<br/>" + "Its a Invalid Format." + "<br/>" + TenthFormatMsg;
                                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Postflight.POLeg, "alertCrewCallBackFn");
                                IsCheck = false;
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(text))
                        {
                            if (!Minutes.IsMatch(text))
                            {
                                if (text.IndexOf(":") != -1)
                                {
                                    string[] timeArray = text.Split(':');
                                    if (!string.IsNullOrEmpty(timeArray[1]))
                                    {
                                        Int64 result;
                                        bool isNum = Int64.TryParse(timeArray[1], out result);
                                        if (isNum)
                                        {
                                            if (result > 59)
                                            {
                                                string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Minute entry must be between 0 and 59.";
                                                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Postflight.POLeg, "alertCrewCallBackFn");
                                                IsCheck = false;
                                            }
                                            else
                                            {
                                                string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format." + "<br/>" + MinuteFormatMsg;
                                                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Postflight.POLeg, "alertCrewCallBackFn");
                                                IsCheck = false;
                                            }
                                        }
                                        else
                                        {

                                            string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format." + "<br/>" + MinuteFormatMsg;
                                            RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Postflight.POLeg, "alertCrewCallBackFn");
                                            IsCheck = false;
                                        }

                                    }
                                    else
                                    {
                                        string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format." + "<br/>" + MinuteFormatMsg;
                                        RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Postflight.POLeg, "alertCrewCallBackFn");
                                        IsCheck = false;
                                    }
                                }
                                else
                                {
                                    string msgToDisplay = "You entered: " + text + " . " + "<br/>" + "Invalid Format." + "<br/>" + MinuteFormatMsg;
                                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Postflight.POLeg, "alertCrewCallBackFn");
                                    IsCheck = false;
                                }
                            }

                        }
                    }
                }
                return IsCheck;
            }
        }

        private void setWarnMessageToDefault()
        {
            if (QuoteInProgress != null)
            {
                if (QuoteInProgress.CQLegs != null)
                {
                    foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false))
                    {
                        Leg.ShowWarningMessage = true;
                        hdnShowWarnMessage.Value = "true";
                    }
                }
            }
        }
    }
}
