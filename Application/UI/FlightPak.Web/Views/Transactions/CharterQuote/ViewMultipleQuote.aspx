﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewMultipleQuote.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CharterQuote.ViewMultipleQuote" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Multiple Quote</title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadScriptManager ID="radScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <div id="DivExternalForm" runat="server">
            <table width="100%">
                <tr>
                    <td>File No.
                    </td>
                    <td>
                        <asp:Label ID="lbFileNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Available Quotes
                    </td>
                    <td>
                        <asp:CheckBoxList ID="chklstquote" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="lnkInvoiceReport" runat="server" CssClass="print_preview_icon"
                            ToolTip="Preview Report" OnClick="lnkInvoiceReport_Click" CausesValidation="false"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
