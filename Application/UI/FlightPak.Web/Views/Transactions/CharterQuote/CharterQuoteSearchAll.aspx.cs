﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class SearchAll : BaseSecuredPage
    {

        #region Comments
        //UWA ID:  2566 - Fixed by Prabhu Jan 08, 2014
        //Description: When selecting any of these quotes via "See All Files" the system should display specifics related to the selected quote.
        //            For example: I click on File 64, quote 4, yet the system focus settles on quote 1.

        #endregion

        private ExceptionManager exManager;
        // private string TripID;
        public CharterQuote.CharterQuoteRequest FileRequest = new CharterQuote.CharterQuoteRequest();
        public CQFile FileReq = new CQFile();
        public Int64 FileID = 0;
        public Int64 QuotenumInProgress = 1;
        public bool IsExpressQuote = false;
        public string Param = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["FromPage"] != null)
                        {
                            //Prakash hdnPageRequested.Value = Request.QueryString["FromPage"];
                            if (Request.QueryString["FromPage"].ToString() == "1")
                            {
                                dgSearch.AllowMultiRowSelection = true;
                            }
                            else
                            {
                                dgSearch.AllowMultiRowSelection = false;
                            }
                            if (Request.QueryString["FromPage"].ToString() == "Report")
                            {
                                hdnPageRequested.Value = "Report";
                            }
                        }
                        if (Request.QueryString["ControlName"] != null)
                        {
                            Param = Request.QueryString["ControlName"];
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["IsExpressQuote"]) && Request.QueryString["IsExpressQuote"].ToString().ToLower() == "true")
                        {
                            IsExpressQuote = true;
                            GridColumn alert = dgSearch.MasterTableView.Columns.FindByDataField("Alert");
                            GridColumn IsFinWarn = dgSearch.MasterTableView.Columns.FindByDataField("IsFinancialWarning");
                            alert.Display = false;
                            IsFinWarn.Display = false;
                        }
                        if (!IsPostBack)
                        {
                            if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            {
                                RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            }
                            if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                            {
                                chkHomebase.Checked = true;
                            }
                            DateTime estDepart = DateTime.Now;
                            if (UserPrincipal.Identity._fpSettings._SearchBack != null)
                            {
                                estDepart = estDepart.AddDays(-((double)UserPrincipal.Identity._fpSettings._SearchBack));
                            }
                            else
                            {
                                estDepart = estDepart.AddDays(-(30));
                            }
                            tbDepartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", estDepart);

                            if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString() == "Report")
                            {
                                hdnPageRequested.Value = "Report";
                            }
                        }
                        dgSearch.MasterTableView.NoMasterRecordsText = "File No. Does Not Exist";

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void dgSearch_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgSearch.DataSource = DoSearchFilter();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void dgSearch_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                        {
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem item = (GridDataItem)e.Item;
                                string dateFormat = "";
                                if (item["EstDepartureDT"].Text != "&nbsp;")
                                {
                                    DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "EstDepartureDT");
                                    if (date != null)
                                    {
                                        dateFormat = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date);
                                        item["EstDepartureDT"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(dateFormat);
                                    }
                                }


                                if (item["QuoteDt"].Text != "&nbsp;")
                                {
                                    DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "QuoteDt");
                                    if (date != null)
                                    {
                                        dateFormat =String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date);
                                        item["QuoteDt"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(dateFormat);
                                    }
                                }

                                if (item["Alert"].Text == "!")
                                {
                                    item["Alert"].ForeColor = System.Drawing.Color.Red;
                                }

                                if (!string.IsNullOrEmpty(item["IsFinancialWarning"].Text))
                                {
                                    if (item["IsFinancialWarning"].Text.ToLower() == "true")
                                    {
                                        item["IsFinancialWarning"].Text = "$";
                                        item["IsFinancialWarning"].ForeColor = System.Drawing.Color.Red;
                                    }
                                    else
                                        item["IsFinancialWarning"].Text = string.Empty;

                                }
                            }
                            if (e.Item is GridCommandItem)
                            {
                                GridCommandItem Item = (GridCommandItem)e.Item;
                                Button Insert = (Button)Item.FindControl("lbtnInitInsert");
                                Button Ok = (Button)Item.FindControl("btnOK");
                                Button Submit = (Button)Item.FindControl("btnSUB");
                                if (Insert != null && Ok != null && Submit != null)
                                {
                                    if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "Move")
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = true;
                                        Submit.Visible = false;
                                    }
                                    else if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "Report")
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = false;
                                        Submit.Visible = true;
                                    }
                                    else
                                    {
                                        Insert.Visible = true;
                                        Ok.Visible = false;
                                        Submit.Visible = false;
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgSearch;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
            //SelectItem(); 
        }
        protected void dgSearch_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                //InsertSelectedRow();
                                if (!string.IsNullOrEmpty(Request.QueryString["IsExpressQuote"]) && Request.QueryString["IsExpressQuote"].ToString().ToLower() == "true")
                                {
                                    _LoadEQtoSession();
                                }
                                else
                                {
                                    _LoadCQtoSession();
                                }
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                        		Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void _LoadCQtoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgSearch.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
                            FileID = Convert.ToInt64(item["CQFileID"].Text);



                            Int64 convFileNum = 0;
                            Int64 Filenum = 0;
                            if (Int64.TryParse(item["FileNUM"].Text, out convFileNum))
                                Filenum = convFileNum;
                            if (Session["CQFILE"] != null)
                            {
                                #region "Already a CQFile session exists"
                                CQFile CurrFile = (CQFile)Session["CQFILE"];
                                if (CurrFile.Mode == CQRequestActionMode.Edit && CurrFile.CQFileID != FileID)
                                {
                                    //this.PreflightHeader.HeaderMessage.ForeColor = System.Drawing.Color.Red;
                                    //this.PreflightHeader.HeaderMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                                    //RadWindowManager1.RadAlert("Trip Unsaved, please Save or Cancel Trip", 330, 100, "Trip Alert", null);
                                    RadWindowManager1.RadConfirm("File: " + CurrFile.FileNUM.ToString() + " Unsaved, ignore changes and load the selected Trip: " + Filenum + " ?", "confirmCallBackFn", 330, 100, null, "Confirmation!");
                                }
                                else
                                {
                                    using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                                    {
                                        var objRetVal = CqSvc.GetCQRequestByID(FileID, false);
                                        if (objRetVal.ReturnFlag)
                                        {
                                            Session.Remove("CQFILE");
                                            Session.Remove("CQEXCEPTION");
                                            Session.Remove("CQQUOTEONFILEDICIMG");
                                            Session.Remove("CQCQQuoteOnFileBase64");

                                            FileReq = objRetVal.EntityList[0];
                                            FileReq.Mode = CQRequestActionMode.NoChange;
                                            FileReq.State = CQRequestEntityState.NoChange;

                                            if (FileReq.CQMains != null)
                                            {
                                                //UWA ID:  2566
                                                if (Int64.TryParse(item["QuoteNUM"].Text, out QuotenumInProgress))
                                                    FileReq.QuoteNumInProgress = QuotenumInProgress;
                                                else
                                                    FileReq.QuoteNumInProgress = 1;
                                                //UWA ID:  2566

                                                foreach (CQMain quote in FileReq.CQMains)
                                                {
                                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                        if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                        {
                                                            foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                            {
                                                                CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                                objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                                objImageFile.RecordType = "CQQuoteOnFile";
                                                                objImageFile.UWAFileName = fwh.UWAFileName;
                                                                objImageFile.RecordID = quote.CQMainID;
                                                                objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                                objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                                objImageFile.IsDeleted = false;
                                                                if (quote.ImageList == null)
                                                                    quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                                quote.ImageList.Add(objImageFile);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                                {
                                                    var objRetval = CqSvc.GetQuoteExceptionList(FileReq.CQMains[0].CQMainID);
                                                    if (objRetval.ReturnFlag)
                                                    {
                                                        Session["CQEXCEPTION"] = objRetval.EntityList;
                                                    }
                                                }
                                            }

                                            Session["CQFILE"] = FileReq;
                                            
                                            RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region "If no session"
                                using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                                {
                                    var objRetVal = CqSvc.GetCQRequestByID(FileID, false);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        Session.Remove("CQFILE");
                                        Session.Remove("CQEXCEPTION");
                                        Session.Remove("CQQUOTEONFILEDICIMG");
                                        Session.Remove("CQCQQuoteOnFileBase64");

                                        FileReq = objRetVal.EntityList[0];
                                        FileReq.Mode = CQRequestActionMode.NoChange;
                                        FileReq.State = CQRequestEntityState.NoChange;


                                        //Trip.AllowTripToNavigate = true;
                                        //Trip.AllowTripToSave = true;

                                        if (FileReq.CQMains != null)
                                        {
                                            //UWA ID:  2566
                                            if (Int64.TryParse(item["QuoteNUM"].Text, out QuotenumInProgress))
                                                FileReq.QuoteNumInProgress = QuotenumInProgress;
                                            else
                                                FileReq.QuoteNumInProgress = 1;
                                            //UWA ID:  2566

                                            foreach (CQMain quote in FileReq.CQMains)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                    if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                    {
                                                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                        {
                                                            CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                            objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                            objImageFile.RecordType = "CQQuoteOnFile";
                                                            objImageFile.UWAFileName = fwh.UWAFileName;
                                                            objImageFile.RecordID = quote.CQMainID;
                                                            objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                            objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                            objImageFile.IsDeleted = false;
                                                            if (quote.ImageList == null)
                                                                quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                            quote.ImageList.Add(objImageFile);
                                                        }
                                                    }
                                                }
                                            }
                                            if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                            {
                                                var objRetval = CqSvc.GetQuoteExceptionList(FileReq.CQMains[0].CQMainID);
                                                if (objRetval.ReturnFlag)
                                                {
                                                    Session["CQEXCEPTION"] = objRetval.EntityList;
                                                }
                                            }
                                        }

                                        Session["CQFILE"] = FileReq;
                                        RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                    }
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            RadAjaxManager1.ResponseScripts.Add(@"showMessageBox('Please select a file.','Universal Weather And Aviation')");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                 }
                catch (Exception ex)
                {
                    //  The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }
        protected void RetrieveSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (lbcvLeadSource.Text == string.Empty && lbcvSalesPerson.Text == string.Empty)
                        {
                            dgSearch.DataSource = DoSearchFilter();
                            dgSearch.DataBind();
                            dgSearch.Focus();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }
        public List<View_CharterQuoteMainList> DoSearchFilter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<View_CharterQuoteMainList> cqLists = new List<View_CharterQuoteMainList>();
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Int64 homeBaseID = 0;
                        Int64 leadsourceID = 0;
                        Int64 salesPersonID = 0;
                        string cqCustomerType = string.Empty;
                        if (UserPrincipal != null)
                        {
                            homeBaseID = (long)UserPrincipal.Identity._homeBaseId;
                        }
                        if (!string.IsNullOrEmpty(hdnSalesPerson.Value))
                            salesPersonID = Convert.ToInt64(hdnSalesPerson.Value);

                        if (!string.IsNullOrEmpty(hdnLeadSource.Value))
                            leadsourceID = Convert.ToInt64(hdnLeadSource.Value);

                        if (!string.IsNullOrEmpty(hdnCustomerType.Value))
                            cqCustomerType = hdnCustomerType.Value;

                        using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            DateTime? estDepart;
                            if (!string.IsNullOrEmpty(tbDepartDate.Text))
                                estDepart = DateTime.ParseExact(tbDepartDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            else
                                estDepart = DateTime.MinValue;

                            if (estDepart == DateTime.MinValue)
                            {
                                //Part for Min Date...

                            }

                            cqLists = new List<View_CharterQuoteMainList>();
                            var ObjCQmainlist = objService.GetAllCQMainList(chkHomebase.Checked ? homeBaseID : 0, (DateTime)estDepart, IsExpressQuote, leadsourceID, cqCustomerType, salesPersonID);
                            if (ObjCQmainlist.ReturnFlag)
                            {
                                if (string.IsNullOrEmpty(ddlQuoteStage.SelectedValue))
                                {

                                    cqLists = ObjCQmainlist.EntityList.ToList();
                                }
                                else
                                {
                                    cqLists = ObjCQmainlist.EntityList.Where(x => x.QuoteStage != null && x.QuoteStage.ToString().ToUpper().Trim().Equals(ddlQuoteStage.SelectedValue.ToString().ToUpper().Trim())).ToList();

                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
                return cqLists;
            }
        }
        //suji
        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
                        FileID = Convert.ToInt64(item["CQFileID"].Text);

                        Int64 convFileNum = 0;
                        Int64 Filenum = 0;
                        if (Int64.TryParse(item["FileNUM"].Text, out convFileNum))
                            Filenum = convFileNum;

                        using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                        {
                            var objRetVal = CqSvc.GetCQRequestByID(FileID, IsExpressQuote);
                            if (objRetVal.ReturnFlag)
                            {
                                FileReq = objRetVal.EntityList[0];
                                FileReq.Mode = CQRequestActionMode.NoChange;
                                FileReq.State = CQRequestEntityState.NoChange;

                                if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                {
                                    //UWA ID:  2566
                                    if (Int64.TryParse(item["QuoteNUM"].Text, out QuotenumInProgress))
                                        FileReq.QuoteNumInProgress = QuotenumInProgress;
                                    else
                                        FileReq.QuoteNumInProgress = 1;
                                    //UWA ID:  2566
                                }

                                Session.Remove("CQFILE");
                                Session.Remove("CQEXCEPTION");
                                Session.Remove("CQSELECTEDQUOTENUM");
                                Session.Remove("CQQUOTEONFILEDICIMG");
                                Session.Remove("CQCQQuoteOnFileBase64");
                                Session.Remove("EQFILE");
                                Session.Remove("EQEXCEPTION");

                                List<CQException> exceptionList = new List<CQException>();
                                if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                {
                                    var objRetval = CqSvc.GetQuoteExceptionList(FileReq.CQMains[0].CQMainID);
                                    if (objRetval.ReturnFlag)
                                    {
                                        exceptionList  = objRetval.EntityList;
                                    }
                                }

                                if (IsExpressQuote)
                                {
                                    Session["EQFILE"] = FileReq;
                                    Session["EQEXCEPTION"] = exceptionList;
                                }
                                else
                                {
                                    Session["CQFILE"] = FileReq;
                                    Session["CQEXCEPTION"] = exceptionList;
                                }

                                RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }
        protected void No_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager1.ResponseScripts.Add(@"CloseRadWindow('CloseWindow');");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        #region "EQ Load to Session"

        protected void _LoadEQtoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgSearch.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
                            FileID = Convert.ToInt64(item["CQFileID"].Text);

                            Int64 convFileNum = 0;
                            Int64 Filenum = 0;
                            if (Int64.TryParse(item["FileNUM"].Text, out convFileNum))
                                Filenum = convFileNum;
                            if (Session["EQFILE"] != null)
                            {
                                CQFile CurrFile = (CQFile)Session["EQFILE"];
                                if (CurrFile.Mode == CQRequestActionMode.Edit && CurrFile.CQFileID != FileID)
                                {
                                    //this.PreflightHeader.HeaderMessage.ForeColor = System.Drawing.Color.Red;
                                    //this.PreflightHeader.HeaderMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                                    //RadWindowManager1.RadAlert("Trip Unsaved, please Save or Cancel Trip", 330, 100, "Trip Alert", null);
                                    RadWindowManager1.RadConfirm("File: " + CurrFile.FileNUM.ToString() + " Unsaved, ignore changes and load the selected Trip: " + Filenum + " ?", "confirmCallBackFn", 330, 100, null, "Confirmation!");
                                }
                                else
                                {
                                    using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                                    {
                                        var objRetVal = CqSvc.GetCQRequestByID(FileID, true);
                                        if (objRetVal.ReturnFlag)
                                        {
                                            FileReq = objRetVal.EntityList[0];
                                            //SetTripModeToNoChange(ref Trip);
                                            FileReq.Mode = CQRequestActionMode.NoChange;
                                            FileReq.State = CQRequestEntityState.NoChange;
                                            Session["EQFILE"] = FileReq;

                                            Session.Remove("EQEXCEPTION");
                                            if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                            {
                                                var objRetval = CqSvc.GetQuoteExceptionList(FileReq.CQMains[0].CQMainID);
                                                if (objRetval.ReturnFlag)
                                                {
                                                    Session["EQEXCEPTION"] = objRetval.EntityList;
                                                }
                                            }

                                            RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                                {
                                    var objRetVal = CqSvc.GetCQRequestByID(FileID, true);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        FileReq = objRetVal.EntityList[0];
                                        FileReq.Mode = CQRequestActionMode.NoChange;
                                        FileReq.State = CQRequestEntityState.NoChange;
                                        //Trip.AllowTripToNavigate = true;
                                        //Trip.AllowTripToSave = true;
                                        Session["EQFILE"] = FileReq;

                                        Session.Remove("EQEXCEPTION");
                                        if (FileReq.CQMains != null && FileReq.CQMains.Count > 0)
                                        {
                                            var objRetval = CqSvc.GetQuoteExceptionList(FileReq.CQMains[0].CQMainID);
                                            if (objRetval.ReturnFlag)
                                            {
                                                Session["EQEXCEPTION"] = objRetval.EntityList;
                                            }
                                        }

                                        RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //  The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        #endregion

        /// <summary>
        /// To Validate SalesPerson Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbSalesPerson_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSalesPerson.Value = string.Empty;
                        lbSalesPersonFirst.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        string firstname = string.Empty;
                        string lastname = string.Empty;
                        string middlename = string.Empty;

                        if (!string.IsNullOrEmpty(tbSalesPerson.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetSalesPersonList().EntityList.Where(x => x.SalesPersonCD != null && x.SalesPersonCD.Trim().ToUpper().Equals(tbSalesPerson.Text.ToString().ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode("Invalid Sales Person Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbSalesPerson);
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                    SalesPersonList = (List<FlightPakMasterService.GetSalesPerson>)RetValue.ToList();
                                    hdnSalesPerson.Value = SalesPersonList[0].SalesPersonID.ToString();
                                    tbSalesPerson.Text = SalesPersonList[0].SalesPersonCD;
                                    if (SalesPersonList[0].FirstName != null)
                                        firstname = SalesPersonList[0].FirstName.ToString();

                                    if (SalesPersonList[0].LastName != null)
                                        lastname = SalesPersonList[0].LastName.ToString();

                                    if (SalesPersonList[0].MiddleName != null)
                                        middlename = SalesPersonList[0].MiddleName.ToString();

                                    if (!string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(middlename))
                                        lbSalesPersonFirst.Text = System.Web.HttpUtility.HtmlEncode(lastname + ", " + firstname + " " + middlename);
                                    else if (string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(middlename))
                                        lbSalesPersonFirst.Text = System.Web.HttpUtility.HtmlEncode(firstname + " " + middlename);
                                    else if (string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(middlename))
                                        lbSalesPersonFirst.Text = System.Web.HttpUtility.HtmlEncode(firstname);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        /// <summary>
        ///  To Validate LeadSource Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbLeadSource_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnLeadSource.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbLeadSource.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbLeadSource.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetLeadSourceList().EntityList.Where(x => x.LeadSourceCD != null && x.LeadSourceCD.Trim().ToUpper().Equals(tbLeadSource.Text.ToString().Replace("amp;", "").ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvLeadSource.Text = System.Web.HttpUtility.HtmlEncode("Invalid Lead Source Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbLeadSource);
                                }
                                else
                                {
                                    List<FlightPakMasterService.LeadSource> LeadSourceList = new List<FlightPakMasterService.LeadSource>();
                                    LeadSourceList = (List<FlightPakMasterService.LeadSource>)RetValue.ToList();
                                    tbLeadSource.Text = LeadSourceList[0].LeadSourceCD.ToString();
                                    hdnLeadSource.Value = LeadSourceList[0].LeadSourceID.ToString();
                                    if (LeadSourceList[0].LeadSourceDescription != null)
                                        lbLeadSource.Text = System.Web.HttpUtility.HtmlEncode(LeadSourceList[0].LeadSourceDescription.ToString().ToUpper());
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void dgSearch_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSearch, Page.Session);
        }

        //protected void ddlstCustomertype_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlstCustomertype.SelectedValue != null)
        //        hdnCustomerType.Value = ddlstCustomertype.SelectedValue;
        //    else
        //        hdnCustomerType.Value = string.Empty;
        //}
    }
}
