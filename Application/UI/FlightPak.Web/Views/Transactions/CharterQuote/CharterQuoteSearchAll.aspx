﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CharterQuoteSearchAll.aspx.cs"
    Inherits="FlightPak.Web.Views.CharterQuote.SearchAll" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Charter See All</title>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.RadGrid.RadGrid_Office2010Silver').css('height', 'auto !important');
            $('.RadWindow.RadWindow_Metro.rwNormalWindow').css({ height: $('#DivExternalForm > .box1').height() })
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
            <script type="text/javascript" src="../../../Scripts/Common.js"></script>
            <script type="text/javascript">

                function confirmCallBackFn(arg) {
                    if (arg == true) {
                        document.getElementById('<%=btnYes.ClientID%>').click();
                    }
                    else {

                        document.getElementById('<%=btnNo.ClientID%>').click();
                    }
                }

                function openWin(radWin) {
                    var url = '';
                    if (radWin == "RadSalesPersonPopup") {
                        url = '/Views/CharterQuote/SalesPersonPopup.aspx?SalesPersonID=' + document.getElementById('<%=tbSalesPerson.ClientID%>').value;
                }
                else if (radWin == "RadLeadSourcePopup") {
                    url = '/Views/CharterQuote/LeadSourcePopup.aspx?LeadSourceID=' + document.getElementById('<%=tbLeadSource.ClientID%>').value;
                }

            var oWnd = radopen(url, radWin);
        }

        // this function is used to display the value of selected Client code from popup
        function OnClientSalesPersonClose(oWnd, args) {
            var combo = $find("<%= tbSalesPerson.ClientID %>");
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg != null) {
                if (arg) {
                    document.getElementById("<%=tbSalesPerson.ClientID%>").value = arg.SalesPersonCD;
                        document.getElementById("<%=hdnSalesPerson.ClientID%>").value = arg.SalesPersonID;
                        if (arg.SalesPersonCD != null)
                            document.getElementById("<%=lbcvSalesPerson.ClientID%>").innerHTML = "";
                        var step = "tbSalesPerson_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbSalesPerson.ClientID%>").value = "";
                        document.getElementById("<%=lbSalesPersonFirst.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnSalesPerson.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientLeadSourceClose(oWnd, args) {
                var combo = $find("<%= tbLeadSource.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbLeadSource.ClientID%>").value = arg.Code;
                        document.getElementById("<%=lbLeadSource.ClientID%>").innerHTML = arg.CustomerName;
                        document.getElementById("<%=hdnLeadSource.ClientID%>").value = arg.LeadSourceID;
                        if (arg.LeadSourceCD != null)
                            document.getElementById("<%=lbcvLeadSource.ClientID%>").innerHTML = "";
                        var step = "tbLeadSource_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbLeadSource.ClientID%>").value = "";
                        document.getElementById("<%=lbLeadSource.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnLeadSource.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.location.reload();
            }

            function CloseRadWindow(arg) {
                GetRadWindow().Close();

            }


            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);



                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker


                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                    }
                }
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page                
                oArg = new Object();
                grid = $find("<%= dgSearch.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "FileNUM");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "CQFileID");
                        if (i == 0) {
                            oArg.FileNUM = cell1.innerHTML + ",";
                        }
                        else {

                            oArg.FileNUM += cell1.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.FileNUM = "";
                }

                if (oArg.FileNUM != "")
                    oArg.FileNUM = oArg.FileNUM.substring(0, oArg.FileNUM.length - 1)
                else
                    oArg.FileNUM = "";

                oArg.Arg1 = oArg.FileNUM;

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);

                }
            }
            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());

                    sender.value = formattedDate;
                }
            }

            function RowDblClick() {
                var frompagestr = "";

                frompagestr = document.getElementById("<%= hdnPageRequested.ClientID%>").value;
                if (frompagestr.toLowerCase() == "report") {
                    returnToParent();
                }
                else {
                    var masterTable = $find("<%= dgSearch.ClientID %>").get_masterTableView();
                    masterTable.fireCommand("InitInsert", "");
                    return false;
                }
            }
            function prepareSearchInput(input) { input.value = input.value; }
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadSalesPersonPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientSalesPersonClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/SalesPersonPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadLeadSourcePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientLeadSourceClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/LeadSourcePopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
            <ClientEvents OnDateSelected="dateSelected" />
            <DateInput ID="DateInput1" runat="server">
            </DateInput>
            <Calendar ID="Calendar1" runat="server">
                <SpecialDays>
                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                        ItemStyle-BorderStyle="Solid">
                    </telerik:RadCalendarDay>
                </SpecialDays>
            </Calendar>
        </telerik:RadDatePicker>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tbDepartDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <div id="DivExternalForm" runat="server">
            <table width="100%" class="box1">
                <tr>
                    <td valign="top">
                        <fieldset>
                            <legend>Search</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel120">
                                        
                                        <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                    </td>
                                    <td class="tdLabel130">Starting Departure Date
                                    </td>
                                    <td class="tdLabel90">
                                        <asp:TextBox ID="tbDepartDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                            onkeydown="return tbDate_OnKeyDown(this, event);" onclick="showPopup(this, event);"
                                            MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">Salesperson
                                                </td>
                                                <td class="tdLabel100">
                                                    <asp:TextBox ID="tbSalesPerson" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                        CssClass="text50" ValidationGroup="Save" onBlur="return RemoveSpecialChars(this)"
                                                        OnTextChanged="tbSalesPerson_TextChanged" AutoPostBack="true">                                                                                                    
                                                    </asp:TextBox>
                                                    <asp:Button ID="btnSalesPerson" OnClientClick="javascript:openWin('RadSalesPersonPopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                    <asp:HiddenField ID="hdnSalesPerson" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lbSalesPersonFirst" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                    <asp:Label ID="lbcvSalesPerson" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">Lead Source
                                                </td>
                                                <td class="tdLabel100">
                                                    <asp:TextBox ID="tbLeadSource" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                        CssClass="text50" ValidationGroup="Save" onBlur="return RemoveSpecialChars(this)"
                                                        OnTextChanged="tbLeadSource_TextChanged" AutoPostBack="true">                                                                                                    
                                                    </asp:TextBox>
                                                    <asp:Button ID="btnLeadSource" OnClientClick="javascript:openWin('RadLeadSourcePopup');return false;"
                                                        CssClass="browse-button" runat="server" />
                                                    <asp:HiddenField ID="hdnLeadSource" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lbLeadSource" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                    <asp:Label ID="lbcvLeadSource" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel90">Quote Stage
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlQuoteStage" AutoPostBack="true" runat="server">
                                                        <asp:ListItem Value="" Selected="True">All</asp:ListItem>
                                                        <asp:ListItem Value="In Progress" >In Progress</asp:ListItem>
                                                        <asp:ListItem Value="Invoiced">Invoiced</asp:ListItem>
                                                        <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField runat="server" ID="hdnCustomerType" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="button" OnClick="RetrieveSearch_Click" />
                                        <asp:HiddenField ID="hdnPageRequested" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgSearch" runat="server" AllowSorting="true" Visible="true"
                            OnItemCommand="dgSearch_ItemCommand" Width="980px" Height="470px" OnItemDataBound="dgSearch_ItemDataBound"
                            OnNeedDataSource="dgSearch_BindData" AutoGenerateColumns="false" PageSize="10"
                            AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
                            OnPreRender="dgSearch_PreRender">
                            <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="true">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FileNUM" HeaderText="File" HeaderStyle-Width="50px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="30px"
                                        CurrentFilterFunction="EqualTo" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="QuoteNUM" HeaderText="Quote No" HeaderStyle-Width="80px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="60px"
                                        CurrentFilterFunction="EqualTo" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No" HeaderStyle-Width="80px" 
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="60px"
                                        CurrentFilterFunction="EqualTo" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Trip Status" HeaderStyle-Width="80px" 
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="60px"
                                        CurrentFilterFunction="StartsWith" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" DataFormatString="{0:dd/MM/yyyy}"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                        FilterControlWidth="70px" HeaderStyle-Width="100px" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="QuoteDt" HeaderText="Quote Date" DataFormatString="{0:dd/MM/yyyy}"
                                        FilterControlWidth="60px" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                        CurrentFilterFunction="EqualTo" HeaderStyle-Width="90px" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CQCustomerName" HeaderText="Customer" AutoPostBackOnFilter="false"
                                        ShowFilterIcon="false" FilterControlWidth="80px" CurrentFilterFunction="StartsWith"
                                        HeaderStyle-Width="100px" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CQFileDescription" HeaderText="Trip Description"
                                        HeaderStyle-Width="100px" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                        FilterControlWidth="80px" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Alert" HeaderText="Alert" HeaderStyle-Width="40px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="20px"
                                        CurrentFilterFunction="StartsWith" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IsFinancialWarning" HeaderText="Financial Warning"
                                        AllowFiltering="false" HeaderStyle-Width="100px" AutoPostBackOnFilter="false"
                                        ShowFilterIcon="false" ItemStyle-ForeColor="Red" ItemStyle-HorizontalAlign="Center"
                                        FilterControlWidth="80px" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileExcep" HeaderText="Exceptions" HeaderStyle-Width="70px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-ForeColor="Red" AutoPostBackOnFilter="false"
                                        ShowFilterIcon="false" FilterControlWidth="10px" CurrentFilterFunction="StartsWith"
                                        AllowFiltering="false" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" HeaderStyle-Width="80px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="50px"
                                        CurrentFilterFunction="StartsWith" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CQFileID" HeaderText="CQFileID" CurrentFilterFunction="EqualTo"
                                        ShowFilterIcon="false" Display="false" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LeadSourceCD" HeaderText="Lead Source" HeaderStyle-Width="90px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="70px" FilterDelay="500"
                                        CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SalesPersonCD" HeaderText="Salesperson" HeaderStyle-Width="90px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="70px" FilterDelay="500"
                                        CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="QuoteStage" HeaderText="Quote Stage" HeaderStyle-Width="90px"
                                        AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterControlWidth="70px"
                                        CurrentFilterFunction="StartsWith" FilterDelay="500">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CustomerType" HeaderText="Customer Type" HeaderStyle-Width="110px"
                                        Display="false">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px; text-align: right;">
                                        <asp:Button ID="lbtnInitInsert" runat="server" ToolTip="OK" CommandName="InitInsert"
                                            CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                        <asp:Button ID="btnOK" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                            CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                        <asp:Button ID="btnSUB" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                            CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                    </div>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <ClientSettings>
                                <ClientEvents OnRowDblClick="RowDblClick" />
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tblHidden" style="display: none; position: relative">
                            <tr>
                                <td>
                                    <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
                                    <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
