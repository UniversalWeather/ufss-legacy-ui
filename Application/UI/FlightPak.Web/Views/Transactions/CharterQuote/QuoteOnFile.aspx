﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CharterQuote.Master"
    AutoEventWireup="true" CodeBehind="QuoteOnFile.aspx.cs" Inherits="FlightPak.Web.Views.CharterQuote.QuoteOnFile"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CharterQuote.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CharterQuoteHeadContent" runat="server">
    <style type="text/css">
        .rgDataDiv{ height: auto !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CharterQuoteBodyContent" runat="server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Visible="true" runat="server"
        Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function alertCallBackFn(arg) {
                document.getElementById('<%=btnAlertOK.ClientID%>').click();
            }

            // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates
            function confirmAutoUpdateRatesFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnConfirmAutoUpdateRateYes.ClientID%>').click();
                }
            }

            function confirmDeleteQuoteCallBack(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnConfirmQuoteDeleteYes.ClientID%>').click();
                }
            }

            function confirmDeleteImgCallBack(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnConfirmImgDeleteYes.ClientID%>').click();
                }
            }




            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }

            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function ShowAirportInfoPopup(Airportid) {
                window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
                return false;
            }

            //To Open Compare Charter Rates
            function openViewCharterRates() {
                var oWnd = radopen("/Views/Transactions/CharterQuote/ViewChaterRates.aspx", "RadViewCharterRates");

            }

            //To Open Live Trip
            function openLiveTrip() {
                if (document.getElementById("<%=hdnIsHavingLeg.ClientID%>").value == "1") {
                    var oWnd = radopen("/Views/Transactions/CharterQuote/LiveTrip.aspx", "RadLiveTrip");
                }
                else {
                    radalert("Quote " + document.getElementById("<%=hdnQuoteNum.ClientID %>").value + " has no Legs!", 330, 100, "Charter", "");
                    return false;
                }
            }
            //this function is used to navigate to pop up screen's with the selected code
            function OpenCQSearch() {
                var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "RadCQSearch");
            }
            function openVendor() {
                var oWnd = radopen("/Views/Settings/Logistics/VendorMasterPopUp.aspx?VendorCD=" + document.getElementById("<%=tbVendor.ClientID%>").value, "RadVendor");
            }

            function openTailNum() {
                if (document.getElementById("<%=hdnVendor.ClientID%>").value == "") {
                    var oWnd = radopen("/Views/Settings/Fleet/FleetProfilePopup.aspx?TailNumber=" + document.getElementById("<%=tbTailNumber.ClientID%>").value, "RadTailNum");
                }
                else {
                    if (document.getElementById("<%=hdnVendor.ClientID%>").value != "") {
                        var oWnd = radopen("/Views/Settings/Fleet/FleetProfilePopup.aspx?VendorCD=" + document.getElementById('<%=tbVendor.ClientID%>').value + "&Vendor=" + document.getElementById('<%=hdnVendor.ClientID%>').value + "&FromPage=" + "expressquote", "RadTailNum");
                    }
                }
            }

            function openRequestor() {
                var requestorCd = '';
                if ($.trim($('#<%=lbRequestorMsg.ClientID%>').text())=='')
                    requestorCd = document.getElementById("<%=tbRequestor.ClientID%>").value;
                var oWnd = radopen("/Views/Settings/People/PassengerRequestorsPopup.aspx?fromPage=" + "CQPax" + "&PassengerRequestorID=" + document.getElementById("<%=hdnRequestor.ClientID%>").value + "&ShowRequestor=true&CQCustomerID=" + document.getElementById("<%=hdnCQCustomerID.ClientID%>").value +  "&PassengerRequestorCD=" + requestorCd, "RadRequestor");
            }

            function openFleetNewCharter() {
                var oWnd = radopen("/Views/Settings/Fleet/FleetNewCharterRate.aspx?IsPopup=&FromPage=charter&FleetID=" + document.getElementById("<%=hdnTailNo.ClientID%>").value + "&TailNum=" + document.getElementById("<%=tbTailNumber.ClientID%>").value, "RadFleetNewCharter");
            }

            function openAircraft() {
                var oWnd = radopen("/Views/Settings/Fleet/FleetNewCharterRate.aspx?IsPopup=&FromPage=charter&AicraftID=" + document.getElementById("<%=hdnTypeCode.ClientID%>").value + "&TailNum=" + document.getElementById("<%=tbTailNumber.ClientID%>").value + "&Description=" + document.getElementById("<%=lbTypeCodeDesc.ClientID%>").innerHTML, "RadFleetNewCharterTypeCode");
            }

            function openTypeCode() {
                if (document.getElementById("<%=hdnTailNo.ClientID%>").value == "") {
                    var oWnd = radopen("/Views/Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + document.getElementById('<%=tbTypeCode.ClientID%>').value + "&tailNoAirCraftID=" + document.getElementById("<%=hdnTypeCode.ClientID%>").value, "RadTypeCode");
                }
            }

            function openWin(radWin) {

                var url = '';
                if (radWin == "rdHistory") {
                    url = "../../Transactions/History.aspx?FromPage=" + "CharterQuote";
                }

                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }

            //this function is used to display the value of selected country code from popup
            function OnClientCloseVendorPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbVendor.ClientID%>").value = arg.VendorCD;
                        document.getElementById("<%=hdnVendor.ClientID%>").value = arg.VendorID;
                        if (arg.Name != null)
                            document.getElementById("<%=lbVendorDesc.ClientID%>").innerHTML = arg.Name;

                        var step = "Vendor_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbVendor.ClientID%>").value = "";
                        document.getElementById("<%=hdnVendor.ClientID%>").value = "";
                        document.getElementById("<%=lbVendorDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=lbVendorMsg.ClientID%>").innerHTML = "";
                    }
                }
            }
            //this function is used to display the value of selected Payment code from popup
            function OnClientCloseTailNumPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {

                        var step = "Donot Call Textchange";
                        if (document.getElementById("<%=tbTailNumber.ClientID%>").value != arg.TailNum) {
                            step = "TailNo_TextChanged";
                        }
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;
                        if (arg.FleetID != null)
                            document.getElementById("<%=hdnTailNo.ClientID%>").value = arg.FleetID;


                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnTailNo.ClientID%>").value = "";
                        document.getElementById("<%=lbTailNumberMsg.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnClientRequestorPopupClose(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=hdnRequestor.ClientID%>").value = arg.PassengerRequestorID;
                        document.getElementById("<%=tbRequestor.ClientID%>").value = arg.PassengerRequestorCD;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbRequestorPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.PassengerName != null && arg.PassengerName != "&nbsp;")
                            document.getElementById("<%=lbRequestorDesc.ClientID%>").innerHTML = arg.PassengerName;

                        var step = "Requestor_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdnRequestor.ClientID%>").value = "";
                        document.getElementById("<%=tbRequestor.ClientID%>").value = "";
                        document.getElementById("<%=tbRequestorPhone.ClientID%>").value = "";
                        document.getElementById("<%=lbRequestorMsg.ClientID%>").innerHTML = "";
                        document.getElementById("<%=lbRequestorDesc.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnClientTypeClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        var step = "Donot Call Textchange";
                        if (document.getElementById("<%=tbTypeCode.ClientID%>").value == arg.AircraftCD) {
                            step = "TypeCode_TextChanged";
                        }
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = arg.AircraftID;
                        if (arg.AircraftDescription != null)
                            document.getElementById("<%=lbTypeCodeDesc.ClientID%>").innerHTML = arg.AircraftDescription;


                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                        if (arg.AircraftCD != null)
                            document.getElementById("<%=lbTypeCodeMsg.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = "";
                        document.getElementById("<%=lbTypeCodeDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = "";
                    }
                }
            }

            function OnClientLiveTrip(oWnd, args) {
                document.getElementById('<%=btnOkMsg.ClientID%>').click();
            }

            function SaveCloseAndRebindLivePage() {
                var oWnd = radalert("Quote Transfer To Preflight Completed", 360, 110, "Charter");
                oWnd.add_close(function () {
                    window.location.href = window.location.href;
                });
            }

            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function DeleteRecord() {
                var masterTable = $find('<%= dgQuote.ClientID %>').get_masterTableView();
                if (masterTable.get_selectedItems().length > 0) {
                    var _quoteNum = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "QuoteNUM");
                    var msg = 'Warning - You are about to delete the Charter No. ' + _quoteNum.innerHTML + ' Are you sure you want to delete this record?';
                    radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                    return false;
                }
                else {
                    //If no records are selected 
                    radalert('Please select any one Quote!', 330, 100, "Delete", "");
                    return false;
                }
            }

            function callBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find('<%= dgQuote.ClientID %>');
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }

            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        alert("Please enter/select Date between 01/01/1900 and 12/31/2100");
                        control.value = "";
                    }
                }
                return false;
            }

            function maxLength(field, maxChars) {
                if (field.value.length >= maxChars) {
                    event.returnValue = false;
                    return false;
                }
            }

            function maxLengthPaste(field, maxChars) {
                event.returnValue = false;
                if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
                    return false;
                }
                event.returnValue = true;
            }
            function CheckCQName() {
                var nam = document.getElementById('<%=tbCQimgName.ClientID%>').value;
                if (nam != "") {
                    document.getElementById("<%=fuCQ.ClientID %>").disabled = false;
                }
                else {
                    document.getElementById("<%=fuCQ.ClientID %>").disabled = true;
                }
            }
            function cqvalidate() {
                var uploadcontrol = document.getElementById('<%=fuCQ.ClientID%>').value;
                //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[Bb][Mm][Pp])$/;
                //var reg = /([^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG])).\2)/gm;
                if (uploadcontrol.length > 0) {
                    //if (reg.test(uploadcontrol)) {
                        return true;
                    }
                    else {
                        //alert("Only .bmp files are allowed!");
                        return false;
                    }
                //}
            }
            function FileCQ_onchange() {
                if (cqvalidate()) {
                    __doPostBack('__Page', 'LoadCQImage');
                }
            }

            function OnClientFleetCharterClose(oWnd, args) {
                //alert('Close');
                var step = "TailNo_TextChanged";
                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientFleetCharterTypeCodeClose(oWnd, args) {
                var step = "TypeCode_TextChanged";
                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function fnRefreshPage() {
                __doPostBack('__Page', 'LoadCQImageFromSession');
            }

            function OpenCQRadWindow() {
                var oWnd = $find("<%=RadCQImagePopup.ClientID%>");
                document.getElementById('<%=imgCQPopup.ClientID%>').src = document.getElementById('<%=imgCQ.ClientID%>').src;
                oWnd.show();
            }
            function CloseCQRadWindow() {
                var oWnd = $find("<%=RadCQImagePopup.ClientID%>");
                document.getElementById('<%=imgCQPopup.ClientID%>').src = null;
                oWnd.close();
            }
            function printCQImage() {
                var image = document.getElementById('<%=imgCQPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageCQDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlCQFileName = document.getElementById('<%=ddlCQFileName.ClientID%>');
                if (ddlCQFileName.length > 0) {
                    var ImageName = ddlCQFileName.options[ddlCQFileName.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });
                    radconfirm(Msg, callBackFunction, 400, 100, null, "Company Profile");
                    args.set_cancel(true);
                }
                return ReturnValue;
            }
            function browserName() {
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf("msie") != -1) return 'Internet Explorer';
                if (agt.indexOf("chrome") != -1) return 'Chrome';
                if (agt.indexOf("opera") != -1) return 'Opera';
                if (agt.indexOf("firefox") != -1) return 'Firefox';
                if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
                if (agt.indexOf("netscape") != -1) return 'Netscape';
                if (agt.indexOf("safari") != -1) return 'Safari';
                if (agt.indexOf("staroffice") != -1) return 'Star Office';
                if (agt.indexOf("webtv") != -1) return 'WebTV';
                if (agt.indexOf("beonex") != -1) return 'Beonex';
                if (agt.indexOf("chimera") != -1) return 'Chimera';
                if (agt.indexOf("netpositive") != -1) return 'NetPositive';
                if (agt.indexOf("phoenix") != -1) return 'Phoenix';
                if (agt.indexOf("skipstone") != -1) return 'SkipStone';
                if (agt.indexOf('\/') != -1) {
                    if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
                        return navigator.userAgent.substr(0, agt.indexOf('\/'));
                    }
                    else return 'Netscape';
                } else if (agt.indexOf(' ') != -1)
                    return navigator.userAgent.substr(0, agt.indexOf(' '));
                else return navigator.userAgent;
            }
            function GetBrowserName() {
                document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCQImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseCQRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnimgprint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printCQImage();return false;" />
                                    </div>
                                    <asp:Image ID="imgCQPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadVendor" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseVendorPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/VendorMasterPopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadTailNum" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTailNumPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFleetNewCharter" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFleetCharterClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetNewCharterRate.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFleetNewCharterTypeCode" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFleetCharterTypeCodeClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetNewCharterRate.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRequestor" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientRequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadTypeCode" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTypeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadLiveTrip" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientLiveTrip" KeepInScreenBounds="true" Modal="true" Behaviors="Close"
                VisibleStatusbar="false" Title="Live Trip" NavigateUrl="~/Views/Transactions/CharterQuote/LiveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadViewCharterRates" runat="server" Width="1020px" Height="480px"
                OnClientResizeEnd="GetDimensions" KeepInScreenBounds="true" Modal="true" Behaviors="Close"
                VisibleStatusbar="false" Title="View Charter Rates" NavigateUrl="~/Views/Transactions/CharterQuote/ViewChaterRates.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div style="width: 712px;">
            <asp:HiddenField ID="hdnQuoteNum" runat="server" />
            <asp:HiddenField ID="hdnCalculateQuote" runat="server" />
            <asp:HiddenField ID="hdnCQCustomerID" runat="server" />
            <table width="100%">
                <tr>
                    <td style="float:right !important;">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnCRTail_Top" runat="server" CssClass="button" Text="Compare Charter Rates"
                                        OnClientClick="javascript:openViewCharterRates();return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnLiveTrip_Top" runat="server" CssClass="button" Text="Live Trip" OnClientClick="javascript:openLiveTrip();return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCopy_Top" runat="server" CssClass="button" Text="Copy" CausesValidation="false"
                                        OnClick="Copy_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAddQuote" runat="server" CssClass="ui_nav" Text="Add Quote" OnClick="btnAddQuote_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnDeleteQuote" runat="server" CssClass="ui_nav" Text="Delete Quote"
                                        OnClick="btnDeleteQuote_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgQuote" runat="server" AllowSorting="true" Visible="true" OnNeedDataSource="Quote_BindData"
                            OnItemCommand="Quote_ItemCommand" OnItemCreated="Quote_ItemCreated" OnItemDataBound="Quote_ItemDataBound"
                            AutoGenerateColumns="false" PageSize="10" AllowPaging="false" AllowFilteringByColumn="false"
                            PagerStyle-AlwaysVisible="true" Width="710px" AllowMultiRowSelection="false">
                            <MasterTableView DataKeyNames="QuoteID,CQMainID,CustomerID,CQFileID,FileNUM,VendorID,FleetID,AircraftID,IsAccepted,IsSubmitted,TripID"
                                CommandItemDisplay="Bottom" AllowPaging="false" AllowFilteringByColumn="false">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="QuoteNUM" HeaderText="Quote No." AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="EqualTo" ShowFilterIcon="false" HeaderStyle-Width="50px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CQSource" HeaderText="Source" AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" UniqueName="CQSource"
                                        HeaderStyle-Width="50px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Vendor" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" UniqueName="From" AllowFiltering="false" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lbVendor" runat="server" Text='<%#Eval("Vendor.Name")%>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Tail No." CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" UniqueName="From" AllowFiltering="false" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lbTailNumber" runat="server" Text='<%#Eval("Fleet.TailNum")%>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Type Code" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" UniqueName="From" AllowFiltering="false" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lbTypeCode" runat="server" Text='<%#Eval("Aircraft.AircraftCD")%>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="FlightHoursTotal" HeaderText="Duration" AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderStyle-Width="70px">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="" HeaderText="U" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>--%>
                                    <%--<telerik:GridBoundColumn DataField="QuoteTotal" HeaderText="Total Charges" AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Total Charges" FilterDelay="4000" ItemStyle-CssClass="fc_textbox_130"
                                        HeaderStyle-Width="160" UniqueName="QuoteTotal" CurrentFilterFunction="Contains">
                                        <ItemTemplate>
                                            <telerik:RadNumericTextBox ID="tbQuoteTotal" runat="server" Type="Currency" Culture="en-US"
                                                DbValue='<%# Bind("QuoteTotal") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                ReadOnly="true">
                                            </telerik:RadNumericTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="IsFinancialWarning" HeaderText="Financial Warning"
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                        HeaderStyle-Width="120px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TripStatus" HeaderText="TS Status" AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderStyle-Width="120px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"
                                        HeaderStyle-Width="80px" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Except." CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" UniqueName="Exception" AllowFiltering="false" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:Label ID="lbException" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Quote Status" CurrentFilterFunction="Contains"
                                        ShowFilterIcon="false" UniqueName="QuoteStatus" AllowFiltering="false" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="lbQuoteStatus" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="LastAcceptDT" HeaderText="Accepted Date" AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderStyle-Width="120px">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IsLog" HeaderText="Log No." AutoPostBackOnFilter="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderStyle-Width="60px">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px; float: left; clear: both;">
                                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                            Visible="false">
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                            Visible="false">
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                            Visible="false">
                                        </asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                    </div>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td rowspan="2" valign="top" style="width: 450px">
                        <fieldset>
                            <legend>Source</legend>
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td colspan="3" align="left" valign="top">
                                        <asp:RadioButtonList ID="rblSource" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Source_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="1">Vendor</asp:ListItem>
                                            <asp:ListItem Value="2">Inside</asp:ListItem>
                                            <asp:ListItem Value="3">Type</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel85" valign="top">
                                        Vendor
                                    </td>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbVendor" runat="server" CssClass="text70" MaxLength="10" AutoPostBack="true"
                                                        OnTextChanged="Vendor_TextChanged"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnVendor" CssClass="browse-button" runat="server" OnClientClick="javascript:openVendor();return false;" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnVendor" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbVendorDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" valign="top">
                                        <asp:Label ID="lbVendorMsg" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Tail No.
                                    </td>
                                    <td valign="top" colspan="2">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbTailNumber" runat="server" CssClass="text70" MaxLength="9" AutoPostBack="true"
                                                        OnTextChanged="TailNo_TextChanged"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnTailNumber" CssClass="browse-button" runat="server" OnClientClick="javascript:openTailNum();return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnTailNumberSearch" CssClass="charter-rate-button" runat="server"
                                                        OnClientClick="javascript:openFleetNewCharter();return false;" ToolTip="Fleet Charter Rates" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnTailNo" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" valign="top">
                                        <asp:Label ID="lbTailNumberMsg" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Type Code
                                    </td>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbTypeCode" runat="server" CssClass="text70" MaxLength="10" AutoPostBack="true"
                                                        OnTextChanged="TypeCode_TextChanged"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnTypeCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openTypeCode();return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnTypeCodeSearch" CssClass="charter-rate-button" runat="server"
                                                        OnClientClick="javascript:openAircraft();return false;" ToolTip="Aircraft Charter Rates" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnTypeCode" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbTypeCodeDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" valign="top">
                                        <asp:Label ID="lbTypeCodeMsg" runat="server" class="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel85" valign="top">
                                        Requestor
                                    </td>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbRequestor" runat="server" CssClass="text70" MaxLength="10" AutoPostBack="true"
                                                        OnTextChanged="Requestor_TextChanged"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnRequestor" CssClass="browse-button" runat="server" OnClientClick="javascript:openRequestor();return false;" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnRequestor" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbRequestorDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" valign="top">
                                        <asp:Label ID="lbRequestorMsg" CssClass="alert-text" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Phone
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="tbRequestorPhone" runat="server" ReadOnly="true" MaxLength="25"
                                            CssClass="inpt_non_edit text110"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td valign="top" style="width: 250px">
                        <fieldset>
                            <legend>Tax Rates</legend>
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td valign="top">
                                        Federal/VAT
                                    </td>
                                    <td colspan="2" valign="top">
                                        <asp:TextBox ID="tbFed" runat="server" CssClass="text70" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                            AutoPostBack="true" OnTextChanged="Fed_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        Rural
                                    </td>
                                    <td colspan="2" valign="top">
                                        <asp:TextBox ID="tbRural" runat="server" CssClass="text70" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                            AutoPostBack="true" OnTextChanged="Rural_TextChanged"></asp:TextBox>
                                        <%-- <telerik:RadNumericTextBox ID="tbRural" runat="server" Type="Currency" Culture="en-US" MaxLength="5" AutoPostBack="true" OnTextChanged="Rural_TextChanged"
                                            NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" colspan="3">
                                        <asp:CheckBox ID="chkTaxableQuote" runat="server" Text="Taxable Quote" AutoPostBack="true"
                                            OnCheckedChanged="TaxableQuote_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                            <div style="height: 2px;">
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 100px">
                        <fieldset>
                            <legend>Quote Stage</legend>
                            <table cellpadding="1" cellspacing="1">
                                <tr>
                                    <td valign="top">
                                        <asp:RadioButtonList ID="rblQuoteStage" AutoPostBack="true" runat="server" RepeatDirection="Vertical">
                                            <asp:ListItem Value="In Progress" Selected="True">In Progress</asp:ListItem>
                                            <asp:ListItem Value="Invoiced">Invoiced</asp:ListItem>
                                            <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                            <div style="height: 2px;">
                            </div>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <fieldset>
                            <table style="width: 680px">
                                <tr>
                                    <td valign="top" colspan="2">
                                        <asp:CheckBox ID="chkSubmitted" runat="server" Text="Submitted Quote To Customer"
                                            OnCheckedChanged="Submitted_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                    <td valign="top" colspan="2">
                                        <asp:CheckBox ID="chkAccepted" runat="server" Text="Accepted By Customer" OnCheckedChanged="Accepted_CheckedChanged"
                                            AutoPostBack="true" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Submitted
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:TextBox ID="tbLastSubmittedDt" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                            onclick="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"
                                            OnTextChanged="LastSubmittedDt_TextChanged" AutoPostBack="true" onchange="parseDate(this, event);"></asp:TextBox>
                                    </td>
                                    <td>
                                        Last Accepted
                                    </td>
                                    <td class="tdLabel130">
                                        <asp:TextBox ID="tbLastAcceptedDt" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                            onclick="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"
                                            onchange="parseDate(this, event);" OnTextChanged="AcceptedDt_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                    <td>
                                        Rejection Date
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbRejectionDt" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                            onclick="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"
                                            OnTextChanged="RejectionDt_TextChanged" AutoPostBack="true" onchange="parseDate(this, event);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Lost Opportunity Code
                                    </td>
                                    <td colspan="5" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlLostOpportunity" runat="server" CssClass="text80" OnSelectedIndexChanged="ddlLostOpportunity_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tdLabel10">
                                                    <asp:HiddenField ID="hdnLostOpportunity" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbLostOpportunity" runat="server" CssClass="input_no_bg"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <%-- <td colspan="4">
                                        <asp:Label ID="lbLostOpportunity" runat="server" CssClass="input_no_bg"></asp:Label>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td>
                                        Comments
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="tbComments" runat="server" CssClass="text480" MaxLength="200"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <fieldset>
                            <legend>Attachments</legend>
                            <table style="width: 100%;">
                                <tr style="display: none;">
                                    <td class="tdLabel100" colspan="2">
                                        Document Name
                                    </td>
                                    <td style="vertical-align: top" colspan="2">
                                        <asp:TextBox ID="tbCQimgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckCQName();"
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="tdLabel270">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:FileUpload ID="fuCQ" runat="server" ClientIDMode="Static" onchange="javascript:return FileCQ_onchange();" />
                                                    <asp:Label ID="lbCQDocum" CssClass="alert-text" runat="server" style="float:left;">All file types are allowed</asp:Label>
                                                    <asp:HiddenField ID="hdnCQDocument" runat="server" />                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--<asp:RegularExpressionValidator ID="revCQImg" runat="server" ControlToValidate="fuCQ"
                                                        ValidationGroup="Save" CssClass="alert-text" ErrorMessage="Select Only .bmp, .gif, .jpg file type."
                                                        ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Jj][Pp][Gg])|.*\.([Bb][Mm][Pp])|.*\.([jJ][pP][eE][gG])$)"
                                                        Display="Static"></asp:RegularExpressionValidator>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" class="tdLabel110">
                                        <asp:DropDownList ID="ddlCQFileName" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                            AutoPostBack="true" CssClass="text200" ClientIDMode="AutoID" OnSelectedIndexChanged="ddlCQFileName_SelectedIndexChanged">
                                        </asp:DropDownList>                                       
                                    </td>
                                    <td valign="top" class="tdLabel80">
                                        <asp:Image ID="imgCQ" Width="50px" Height="50px" runat="server" OnClick="OpenCQRadWindow();"
                                            AlternateText="" ClientIDMode="Static" />
                                        <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                    </td>
                                    <td valign="top" align="right" class="custom_radbutton">
                                        <telerik:RadButton ID="radbtnCQDelete" runat="server" Text="Delete" Enabled="true"
                                            ClientIDMode="Static" OnClick="btncqdeleteImage_Click" />
                                        <asp:HiddenField ID="hdnBrowserName" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <div style="float: left; width: 712px; padding: 0px 0 10px 0;">
                <div style="float: right;">
                    <asp:Button ID="btnCRTail" runat="server" CssClass="button" Text="Compare Charter Rates"
                        OnClientClick="javascript:openViewCharterRates();return false;" />
                    <asp:Button ID="btnLiveTrip" runat="server" CssClass="button" Text="Live Trip" OnClientClick="javascript:openLiveTrip();return false;" />
                    <asp:Button ID="btnCopy" runat="server" CssClass="button" Text="Copy" CausesValidation="false"
                        OnClick="Copy_Click" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete File" CausesValidation="false"
                        OnClick="Delete_Click" />
                    <asp:Button ID="btnEditFile" runat="server" CssClass="button" Text="Edit File" OnClick="Edit_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                        OnClick="Cancel_Click" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                        OnClick="Save_Click" />
                    <asp:Button ID="btnNext" runat="server" CssClass="button" Text="Next >" CausesValidation="false"
                        OnClick="Next_Click" />
                    <asp:Button ID="btnOkMsg" runat="server" Style="display: none" OnClick="btnOkMsg_Click" />
                </div>
            </div>
            <div style="float: left; width: 712px; padding: 0px 0 10px 0;">
                <telerik:RadPanelBar ID="RadPanelBar1" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                    runat="server" CssClass="charterquote-panel-bar">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Leg Details" Value="pnlItemTrip">
                            <ContentTemplate>
                                <div style="float: left;">
                                    <div style="float: right;">
                                        <asp:Button ID="btnCalender" runat="server" Visible="false" Text="Calender" CssClass="ui_nav" />
                                        <asp:Button ID="btnAddColumns" runat="server" Visible="false" Text="+/- Columns"
                                            CssClass="ui_nav" />
                                    </div>
                                    <div style="float: left;" class="charterquote-custom-grid tfoot">
                                        <telerik:RadGrid ID="dgLegs" runat="server" AllowSorting="true" Visible="true" Width="707px"
                                            OnNeedDataSource="Legs_BindData" OnItemDataBound="Legs_ItemDataBound">
                                            <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="false" DataKeyNames="LegNum">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" ShowFilterIcon="false"
                                                        HeaderStyle-Width="40px">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Departs" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                        UniqueName="DepartAir">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDepartAir" runat="server" Text='<%# Eval("DepartAir") %>'
                                                                CssClass="tdtext100" CausesValidation="false" OnClientClick='<%#Eval("DepartAirportID", "return ShowAirportInfoPopup(\"{0}\")")%>'>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Arrives" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                        UniqueName="ArrivalAir">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkArrivalAir" runat="server" Text='<%# Eval("ArrivalAir") %>'
                                                                CssClass="tdtext100" CausesValidation="false" OnClientClick='<%#Eval("ArrivalAirportID", "return ShowAirportInfoPopup(\"{0}\")")%>'>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn DataField="ETE" HeaderText="ETE" HeaderStyle-Width="50px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Depart Date" DataType="System.DateTime"
                                                        HeaderStyle-Width="115px" ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TA" HeaderText="TA" HeaderStyle-Width="40px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrive Date" HeaderStyle-Width="115px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridCheckBoxColumn DataField="POS" HeaderText="POS" HeaderStyle-Width="60px">
                                                    </telerik:GridCheckBoxColumn>
                                                    <telerik:GridCheckBoxColumn DataField="Tax" HeaderText="Tax" HeaderStyle-Width="40px">
                                                    </telerik:GridCheckBoxColumn>
                                                    <%--<telerik:GridBoundColumn DataField="Rate" HeaderText="Rate" HeaderStyle-Width="50px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>--%>
                                                    <telerik:GridTemplateColumn HeaderText="Rate" FilterDelay="4000" ItemStyle-CssClass="fc_textbox"
                                                        HeaderStyle-Width="50px" UniqueName="Rate" CurrentFilterFunction="Contains">
                                                        <ItemTemplate>
                                                            <telerik:RadNumericTextBox ID="tbRate" runat="server" Type="Currency" Culture="en-US"
                                                                DbValue='<%# Bind("Rate") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                ReadOnly="true">
                                                            </telerik:RadNumericTextBox>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <%-- <telerik:GridBoundColumn DataField="Charges" HeaderText="Charges" HeaderStyle-Width="60px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>--%>
                                                    <telerik:GridTemplateColumn HeaderText="Charges" FilterDelay="4000" ItemStyle-CssClass="fc_textbox"
                                                        HeaderStyle-Width="60px" UniqueName="Rate" CurrentFilterFunction="Contains">
                                                        <ItemTemplate>
                                                            <telerik:RadNumericTextBox ID="tbCharges" runat="server" Type="Currency" Culture="en-US"
                                                                DbValue='<%# Bind("Charges") %>' NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Left"
                                                                ReadOnly="true">
                                                            </telerik:RadNumericTextBox>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn DataField="RON" HeaderText="RON" HeaderStyle-Width="50px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DayRoom" HeaderText="DayRoom" HeaderStyle-Width="70px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="PaxTotal" HeaderText="PaxTotal" HeaderStyle-Width="70px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CD" HeaderText="CD" HeaderStyle-Width="30px"
                                                        ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Distance" HeaderText="Distance" ShowFilterIcon="false">
                                                    </telerik:GridBoundColumn>
                                                </Columns>
                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                <PagerStyle AlwaysVisible="false" Mode="NumericPages" Wrap="false" />
                                            </MasterTableView>
                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                <Scrolling AllowScroll="true" />
                                                <Selecting AllowRowSelect="true" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </div>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:HiddenField ID="hdnIsHavingLeg" runat="server" />
                    <!-- // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates -->
                    <asp:Button ID="btnConfirmAutoUpdateRateYes" runat="server" Text="Button" OnClick="ConfirmAutoUpdateRateYes_Click" />
                    <asp:Button ID="btnConfirmQuoteDeleteYes" runat="server" Text="Button" OnClick="ConfirmQuoteDeleteYes_Click" />
                    <asp:Button ID="btnConfirmImgDeleteYes" runat="server" Text="Button" OnClick="ConfirmDeleteImgYes_Click" />
                    <asp:Button ID="btnAlertOK" runat="server" OnClick="AlertOK_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
