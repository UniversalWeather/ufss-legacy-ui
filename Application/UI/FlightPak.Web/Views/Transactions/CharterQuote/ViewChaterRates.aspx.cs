﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Web.CharterQuoteService;
using Telerik.Web.UI;
using System.Data;
using System.Globalization;

namespace FlightPak.Web.Views.Transactions.CharterQuote
{
    public partial class ViewChaterRates : BaseSecuredPage
    {
        #region Declarations
        public string CQSessionKey = "CQFILE";
        public string CQSelectedQuoteNum = "CQSELECTEDQUOTENUM"; // Maintain for selected Quote Number after save scenario.
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        private ExceptionManager exManager;
        const decimal statuateConvFactor = 1.1508M;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbTail.Enabled = false;
                tbTypeCode.Enabled = false;
                btnTailNum.Enabled = true;
                btnTailNum.CssClass = "browse-button";
                btntypeCode.Enabled = false;
                btntypeCode.CssClass = "browse-button-disabled";
                tbTypeCode.Text = string.Empty;

                if (Session[CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[CQSessionKey];

                    Int64 QuoteNumInProgress = 1;
                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                    tbQuoteNo.Text = QuoteNumInProgress.ToString();

                    if (FileRequest.FileNUM != null)
                        tbFileNo.Text = FileRequest.FileNUM.ToString();

                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null)
                        {
                            if (QuoteInProgress.Fleet != null)
                            {
                                if (QuoteInProgress.Fleet.TailNum != null)
                                    lbSelectedTail.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Fleet.TailNum);
                                if (QuoteInProgress.Fleet.MarginalPercentage != null)
                                    lbSelectedMinimumMargin.Text = System.Web.HttpUtility.HtmlEncode(Math.Round((decimal)QuoteInProgress.Fleet.MarginalPercentage, 2).ToString());
                                else
                                    lbSelectedMinimumMargin.Text = System.Web.HttpUtility.HtmlEncode("0");
                            }
                            if (QuoteInProgress.Aircraft != null)
                            {
                                if (QuoteInProgress.Aircraft.AircraftCD != null)
                                    lbSelectedTypeCode.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Aircraft.AircraftCD);
                                if (QuoteInProgress.Aircraft.AircraftDescription != null)
                                    lbselectedDescription.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Aircraft.AircraftDescription);
                            }
                            if (QuoteInProgress.FlightChargeTotal != null)
                                lbSelectedFlightCharges.Text = System.Web.HttpUtility.HtmlEncode(Math.Round((decimal)QuoteInProgress.FlightChargeTotal, 2).ToString());
                            if (QuoteInProgress.AdditionalFeeTotalD != null)
                                lbSelectedAdditionalfees.Text = System.Web.HttpUtility.HtmlEncode(Math.Round((decimal)QuoteInProgress.AdditionalFeeTotalD, 2).ToString());
                            if (QuoteInProgress.QuoteTotal != null)
                                lbSelectedCost.Text = System.Web.HttpUtility.HtmlEncode(Math.Round((decimal)QuoteInProgress.QuoteTotal, 2).ToString());
                            if (QuoteInProgress.MarginTotal != null)
                                lbSelectedMargin.Text = System.Web.HttpUtility.HtmlEncode(Math.Round((decimal)QuoteInProgress.MarginTotal, 2).ToString());
                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                            {
                                CQLeg cqleg = new CQLeg();
                                cqleg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == 1).FirstOrDefault();
                                if (cqleg != null)
                                {
                                    if (cqleg.DepartureDTTMLocal != null)
                                        lbSelectedDeparturedateLeg.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", cqleg.DepartureDTTMLocal));
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void radbtnlstCharterRates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radbtnlstCharterRates.SelectedValue != null && radbtnlstCharterRates.SelectedValue == "1")
            {
                btnTailNum.Enabled = true;
                btnTailNum.CssClass = "browse-button";
                btntypeCode.Enabled = false;
                btntypeCode.CssClass = "browse-button-disabled";
                tbTypeCode.Text = string.Empty;
                hdnTypeCode.Value = string.Empty;
                ClearLabels();
            }
            else
            {
                if (radbtnlstCharterRates.SelectedValue != null && radbtnlstCharterRates.SelectedValue == "2")
                {
                    tbTypeCode.Text = string.Empty;
                    btntypeCode.Enabled = true;
                    btntypeCode.CssClass = "browse-button";
                    btnTailNum.Enabled = false;
                    btnTailNum.CssClass = "browse-button-disabled";
                    tbTail.Text = string.Empty;
                    hdnTailNo.Value = string.Empty;
                    ClearLabels();
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Argument.ToString() == "Tail_TextChanged")
                {
                    #region "TailNumber Compare"
                    ClearLabels();
                    if (!string.IsNullOrEmpty(hdnTailNo.Value))
                    {
                        string error = string.Empty;
                        string desc = string.Empty;
                        string fleetids = string.Empty;
                        string tailNum = string.Empty;
                        string remainTailNo = string.Empty;
                        string remainAircraftCD = string.Empty;
                        string remainFleetID = string.Empty;
                        fleetids = hdnTailNo.Value;
                        string[] fleetTailid = fleetids.Split(',');

                        for (int i = 0; i < fleetTailid.Length; i++)
                        {
                            string fleetid = string.Empty;
                            fleetid = fleetTailid[i];
                            remainFleetID += fleetid + ",";

                            List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetCRList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

                            using (FlightPakMasterService.MasterCatalogServiceClient FleetChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FleetCRList = FleetChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.IsDeleted == false && x.FleetID == Convert.ToInt64(fleetid)).ToList();
                                if (FleetCRList != null && FleetCRList.Count > 0)
                                {
                                    #region
                                    using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        List<FlightPakMasterService.GetAllFleetForPopup> fleetlist = new List<FlightPakMasterService.GetAllFleetForPopup>();

                                        fleetlist = FleetService.GetAllFleetForPopupList().EntityList.Where(x => x.FleetID == Convert.ToInt64(fleetid) && x.IsDeleted == false).ToList();
                                        if (fleetlist != null && fleetlist.Count > 0)
                                        {
                                            foreach (Control cntrl in DivExternalForm.Controls)
                                            {
                                                if (cntrl is Label)
                                                {
                                                    int j = 0;
                                                    j = i + 1;
                                                    if (cntrl.ClientID.Equals(("lbtailNum" + j).Trim()))
                                                    {
                                                        if (!string.IsNullOrEmpty(fleetlist[0].TailNum))
                                                        {
                                                            ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(fleetlist[0].TailNum);
                                                            remainTailNo += ((Label)cntrl).Text + ",";
                                                        }
                                                    }
                                                    if (cntrl.ClientID.Equals(("lbaircraftCode" + j).Trim()))
                                                    {
                                                        if (!string.IsNullOrEmpty(fleetlist[0].AirCraft_AircraftCD))
                                                        {
                                                            ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(fleetlist[0].AirCraft_AircraftCD);
                                                            remainAircraftCD += ((Label)cntrl).Text + ",";
                                                        }
                                                    }
                                                    if (cntrl.ClientID.Equals(("lbdescription" + j).Trim()))
                                                    {
                                                        if (!string.IsNullOrEmpty(fleetlist[0].TypeDescription))
                                                            ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(fleetlist[0].TypeDescription);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    DoLegCalculationsForQuote();
                                    BindFleetChargeDetails(fleetid, string.Empty);
                                    CalculateQuantity();
                                    CalculateQuoteTotal();

                                    foreach (Control cntrl in DivExternalForm.Controls)
                                    {
                                        if (cntrl is Label)
                                        {
                                            int k = 0;
                                            k = i + 1;
                                            if (cntrl.ClientID.Equals(("lbFlightCharges" + k).Trim()))
                                            {
                                                if (QuoteInProgress.FlightChargeTotal != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.FlightChargeTotal, 2).ToString();
                                            }
                                            if (cntrl.ClientID.Equals(("lbAdditionalfees" + k).Trim()))
                                            {
                                                if (QuoteInProgress.AdditionalFeeTotalD != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.AdditionalFeeTotalD, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbCost" + k).Trim()))
                                            {
                                                if (QuoteInProgress.QuoteTotal != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.QuoteTotal, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMargin" + k).Trim()))
                                            {
                                                if (QuoteInProgress.MarginTotal != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.MarginTotal, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMinimumMargin" + k).Trim()))
                                            {
                                                if (QuoteInProgress.Fleet != null)
                                                {
                                                    if (QuoteInProgress.Fleet.MarginalPercentage != null)
                                                    {
                                                        ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.Fleet.MarginalPercentage, 2).ToString();
                                                    }
                                                    else
                                                    {
                                                        ((Label)cntrl).Text = "0";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetAllFleetForPopup> fleetlist = new List<FlightPakMasterService.GetAllFleetForPopup>();
                                    fleetlist = FleetChargeRateService.GetAllFleetForPopupList().EntityList.Where(x => x.FleetID == Convert.ToInt64(fleetid) && x.IsDeleted == false).ToList();
                                    if (fleetlist != null && fleetlist.Count > 0)
                                    {
                                        foreach (Control cntrl in DivExternalForm.Controls)
                                        {
                                            if (cntrl is Label)
                                            {
                                                int j = 0;
                                                j = i + 1;
                                                if (cntrl.ClientID.Equals(("lbtailNum" + j).Trim()))
                                                {
                                                    if (!string.IsNullOrEmpty(fleetlist[0].TailNum))
                                                        ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(fleetlist[0].TailNum);
                                                }
                                                if (cntrl.ClientID.Equals(("lbaircraftCode" + j).Trim()))
                                                {
                                                    if (!string.IsNullOrEmpty(fleetlist[0].AirCraft_AircraftCD))
                                                        ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(fleetlist[0].AirCraft_AircraftCD);
                                                }
                                                if (cntrl.ClientID.Equals(("lbdescription" + j).Trim()))
                                                {
                                                    if (!string.IsNullOrEmpty(fleetlist[0].TypeDescription))
                                                        ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(fleetlist[0].TypeDescription);
                                                }
                                            }
                                        }
                                        tailNum = fleetlist[0].TailNum.ToString();
                                    }
                                    error += tailNum + ",";

                                    foreach (Control cntrl in DivExternalForm.Controls)
                                    {
                                        if (cntrl is Label)
                                        {
                                            int k = 0;
                                            k = i + 1;
                                            if (cntrl.ClientID.Equals(("lbFlightCharges" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbAdditionalfees" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbCost" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMargin" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMinimumMargin" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(error))
                        {
                            error = error.Substring(0, error.Length - 1);
                            RadWindowManager1.RadAlert("No Charter Rate table exists for the Tail no : " + error + ".", 300, 160, "Compare Charter Rates", null);
                            //if (!string.IsNullOrEmpty(remainTailNo))
                            //{
                            //    remainTailNo = remainTailNo.Substring(0, remainTailNo.Length - 1);
                            //    tbTail.Text = remainTailNo;
                            //}
                            //if (!string.IsNullOrEmpty(remainAircraftCD))
                            //{
                            //    remainAircraftCD = remainAircraftCD.Substring(0, remainAircraftCD.Length - 1);
                            //    tbTypeCode.Text = remainAircraftCD;
                            //}
                            //if (!string.IsNullOrEmpty(remainFleetID))
                            //{
                            //    remainFleetID = remainFleetID.Substring(0, remainFleetID.Length - 1);
                            //    hdnTailNo.Value = remainFleetID;
                            //}
                            hdnTypeCode.Value = string.Empty;
                        }
                    }
                    #endregion
                }

                if (e.Argument.ToString() == "TypeCode_TextChanged")
                {
                    #region "Aircraft Compare"
                    ClearLabels();
                    if (!string.IsNullOrEmpty(hdnTypeCode.Value))
                    {
                        string error = string.Empty;
                        string desc = string.Empty;
                        string aircraftids = string.Empty;
                        aircraftids = hdnTypeCode.Value;
                        string[] aircraftid = aircraftids.Split(',');
                        string aircraftCD = string.Empty;
                        string remainAircraftCD = string.Empty;
                        string remainAircraftID = string.Empty;

                        for (int i = 0; i < aircraftid.Length; i++)
                        {
                            string typecodeid = string.Empty;
                            typecodeid = aircraftid[i];
                            remainAircraftID += typecodeid;

                            List<FlightPakMasterService.GetAllFleetNewCharterRate> AircraftCRList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

                            using (FlightPakMasterService.MasterCatalogServiceClient AircraftChargeRateService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                AircraftCRList = AircraftChargeRateService.GetAllFleetNewCharterRate().EntityList.Where(x => x.IsDeleted == false && x.AircraftTypeID == Convert.ToInt64(typecodeid)).ToList();
                                if (AircraftCRList != null && AircraftCRList.Count > 0)
                                {
                                    #region
                                    using (FlightPakMasterService.MasterCatalogServiceClient AircraftService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        List<FlightPakMasterService.GetAllAircraft> aircraftlist = new List<FlightPakMasterService.GetAllAircraft>();

                                        aircraftlist = AircraftService.GetAircraftList().EntityList.Where(x => x.AircraftID == Convert.ToInt64(typecodeid) && x.IsDeleted == false).ToList();
                                        if (aircraftlist != null && aircraftlist.Count > 0)
                                        {
                                            foreach (Control cntrl in DivExternalForm.Controls)
                                            {
                                                if (cntrl is Label)
                                                {
                                                    int j = 0;
                                                    j = i + 1;
                                                    if (cntrl.ClientID.Equals(("lbaircraftCode" + j).Trim()))
                                                    {
                                                        if (!string.IsNullOrEmpty(aircraftlist[0].AircraftCD))
                                                        {
                                                            ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(aircraftlist[0].AircraftCD);
                                                            remainAircraftCD += ((Label)cntrl).Text + ",";
                                                        }
                                                    }
                                                    if (cntrl.ClientID.Equals(("lbdescription" + j).Trim()))
                                                    {
                                                        if (!string.IsNullOrEmpty(aircraftlist[0].AircraftDescription))
                                                            ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(aircraftlist[0].AircraftDescription);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    DoLegCalculationsForQuote();
                                    BindFleetChargeDetails(string.Empty, typecodeid);
                                    CalculateQuantity();
                                    CalculateQuoteTotal();

                                    foreach (Control cntrl in DivExternalForm.Controls)
                                    {
                                        if (cntrl is Label)
                                        {
                                            int k = 0;
                                            k = i + 1;
                                            if (cntrl.ClientID.Equals(("lbFlightCharges" + k).Trim()))
                                            {
                                                if (QuoteInProgress.FlightChargeTotal != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.FlightChargeTotal, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbAdditionalfees" + k).Trim()))
                                            {
                                                if (QuoteInProgress.AdditionalFeeTotalD != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.AdditionalFeeTotalD, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbCost" + k).Trim()))
                                            {
                                                if (QuoteInProgress.QuoteTotal != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.QuoteTotal, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMargin" + k).Trim()))
                                            {
                                                if (QuoteInProgress.MarginTotal != null)
                                                    ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.MarginTotal, 2).ToString();
                                                else
                                                    ((Label)cntrl).Text = "0";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMinimumMargin" + k).Trim()))
                                            {
                                                if (QuoteInProgress.Fleet != null)
                                                {
                                                    if (QuoteInProgress.Fleet.MarginalPercentage != null)
                                                    {
                                                        ((Label)cntrl).Text = Math.Round((decimal)QuoteInProgress.Fleet.MarginalPercentage, 2).ToString();
                                                    }
                                                    else
                                                    {
                                                        ((Label)cntrl).Text = "0";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetAllAircraft> aircraftlist = new List<FlightPakMasterService.GetAllAircraft>();
                                    aircraftlist = AircraftChargeRateService.GetAircraftList().EntityList.Where(x => x.AircraftID == Convert.ToInt64(typecodeid) && x.IsDeleted == false).ToList();
                                    if (aircraftlist != null && aircraftlist.Count > 0)
                                    {
                                        foreach (Control cntrl in DivExternalForm.Controls)
                                        {
                                            if (cntrl is Label)
                                            {
                                                int j = 0;
                                                j = i + 1;
                                                if (cntrl.ClientID.Equals(("lbaircraftCode" + j).Trim()))
                                                {
                                                    if (!string.IsNullOrEmpty(aircraftlist[0].AircraftCD))
                                                        ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(aircraftlist[0].AircraftCD);
                                                }
                                                if (cntrl.ClientID.Equals(("lbdescription" + j).Trim()))
                                                {
                                                    if (!string.IsNullOrEmpty(aircraftlist[0].AircraftDescription))
                                                        ((Label)cntrl).Text = System.Web.HttpUtility.HtmlEncode(aircraftlist[0].AircraftDescription);
                                                }
                                            }
                                        }
                                        aircraftCD = aircraftlist[0].AircraftCD.ToString();
                                    }
                                    error += aircraftCD + ",";

                                    foreach (Control cntrl in DivExternalForm.Controls)
                                    {
                                        if (cntrl is Label)
                                        {
                                            int k = 0;
                                            k = i + 1;
                                            if (cntrl.ClientID.Equals(("lbFlightCharges" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbAdditionalfees" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbCost" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMargin" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                            if (cntrl.ClientID.Equals(("lbMinimumMargin" + k).Trim()))
                                            {
                                                ((Label)cntrl).Text = "-";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(error))
                        {
                            error = error.Substring(0, error.Length - 1);
                            RadWindowManager1.RadAlert("No Charter Rate table exists for the Aircraft Type : " + error + ". Select another Aircraft Type", 300, 160, "Compare Charter Rates", null);
                            //if (!string.IsNullOrEmpty(remainAircraftCD))
                            //{
                            //    remainAircraftCD = remainAircraftCD.Substring(0, remainAircraftCD.Length - 1);
                            //    tbTypeCode.Text = remainAircraftCD;
                            //}
                            //if (!string.IsNullOrEmpty(remainAircraftID))
                            //{
                            //    remainAircraftID = remainAircraftID.Substring(0, remainAircraftID.Length - 1);
                            //    hdnTypeCode.Value = remainAircraftID;
                            //}
                        }
                    }
                    #endregion
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
            }
        }

        protected void ClearLabels()
        {
            lbtailNum1.Text = string.Empty;
            lbtailNum2.Text = string.Empty;
            lbtailNum3.Text = string.Empty;
            lbtailNum4.Text = string.Empty;
            lbtailNum5.Text = string.Empty;
            lbtailNum6.Text = string.Empty;
            lbtailNum7.Text = string.Empty;
            lbtailNum8.Text = string.Empty;
            lbtailNum9.Text = string.Empty;
            lbtailNum10.Text = string.Empty;
            lbaircraftCode1.Text = string.Empty;
            lbaircraftCode2.Text = string.Empty;
            lbaircraftCode3.Text = string.Empty;
            lbaircraftCode4.Text = string.Empty;
            lbaircraftCode5.Text = string.Empty;
            lbaircraftCode6.Text = string.Empty;
            lbaircraftCode7.Text = string.Empty;
            lbaircraftCode8.Text = string.Empty;
            lbaircraftCode9.Text = string.Empty;
            lbaircraftCode10.Text = string.Empty;
            lbdescription1.Text = string.Empty;
            lbdescription2.Text = string.Empty;
            lbdescription3.Text = string.Empty;
            lbdescription4.Text = string.Empty;
            lbdescription5.Text = string.Empty;
            lbdescription6.Text = string.Empty;
            lbdescription7.Text = string.Empty;
            lbdescription8.Text = string.Empty;
            lbdescription9.Text = string.Empty;
            lbdescription10.Text = string.Empty;
            lbFlightCharges1.Text = string.Empty;
            lbFlightCharges2.Text = string.Empty;
            lbFlightCharges3.Text = string.Empty;
            lbFlightCharges4.Text = string.Empty;
            lbFlightCharges5.Text = string.Empty;
            lbFlightCharges6.Text = string.Empty;
            lbFlightCharges7.Text = string.Empty;
            lbFlightCharges8.Text = string.Empty;
            lbFlightCharges9.Text = string.Empty;
            lbFlightCharges10.Text = string.Empty;
            lbAdditionalfees1.Text = string.Empty;
            lbAdditionalfees2.Text = string.Empty;
            lbAdditionalfees3.Text = string.Empty;
            lbAdditionalfees4.Text = string.Empty;
            lbAdditionalfees5.Text = string.Empty;
            lbAdditionalfees6.Text = string.Empty;
            lbAdditionalfees7.Text = string.Empty;
            lbAdditionalfees8.Text = string.Empty;
            lbAdditionalfees9.Text = string.Empty;
            lbAdditionalfees10.Text = string.Empty;
            lbCost1.Text = string.Empty;
            lbCost2.Text = string.Empty;
            lbCost3.Text = string.Empty;
            lbCost4.Text = string.Empty;
            lbCost5.Text = string.Empty;
            lbCost6.Text = string.Empty;
            lbCost7.Text = string.Empty;
            lbCost8.Text = string.Empty;
            lbCost9.Text = string.Empty;
            lbCost10.Text = string.Empty;
            lbMargin1.Text = string.Empty;
            lbMargin2.Text = string.Empty;
            lbMargin3.Text = string.Empty;
            lbMargin4.Text = string.Empty;
            lbMargin5.Text = string.Empty;
            lbMargin6.Text = string.Empty;
            lbMargin7.Text = string.Empty;
            lbMargin8.Text = string.Empty;
            lbMargin9.Text = string.Empty;
            lbMargin10.Text = string.Empty;
            lbMinimumMargin1.Text = string.Empty;
            lbMinimumMargin2.Text = string.Empty;
            lbMinimumMargin3.Text = string.Empty;
            lbMinimumMargin4.Text = string.Empty;
            lbMinimumMargin5.Text = string.Empty;
            lbMinimumMargin6.Text = string.Empty;
            lbMinimumMargin7.Text = string.Empty;
            lbMinimumMargin8.Text = string.Empty;
            lbMinimumMargin9.Text = string.Empty;
            lbMinimumMargin10.Text = string.Empty;
        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            ClearLabels();
            tbTail.Text = string.Empty;
            tbTypeCode.Text = string.Empty;
        }

        #region "Calculation"

        public FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = (from aircraft in objPowerSetting.EntityList
                        //                                                                    where aircraft.AircraftID == AircraftID
                        //                                                                    select aircraft).ToList();

                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;

                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;
                }
            }
        }
        protected double RoundElpTime(double lnElp_Time)
        {
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {
                decimal ElapseTMRounding = 0;
                double lnElp_Min = 0.0;
                double lnEteRound = 0.0;
                if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                    ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;
                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;
                if (lnEteRound > 0)
                {
                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));
                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;
                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);
                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);
                }
            }
            return lnElp_Time;
        }
        private int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dateval))
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }
        public decimal CalculateSumOfETE(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {

                    if (orderNum == 1)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == false && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;

                    }
                    else if (orderNum == 2)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == true && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                    else
                    {

                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                }
                return result;
            }
        }
        public FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 AirportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.GetAllAirport Airportmaster = new FlightPak.Web.FlightPakMasterService.GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(AirportID);
                    if (objAirport.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = (from Arpt in objAirport.EntityList
                        //                                                                       where Arpt.IcaoID == DAirportID
                        //                                                                       select Arpt).ToList();

                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;

                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;
                }
            }
        }
        public FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId GetCompany(Int64 HomeBaseID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Companymaster = new FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                    var objCompany = objDstsvc.GetListInfoByHomeBaseId(HomeBaseID);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> Company = (from Comp in objCompany.EntityList
                                                                                                              where Comp.HomebaseID == HomeBaseID
                                                                                                              select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }
        }
        public DateTime CalculateMinDeparttDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DepartureDTTMLocal != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Min(x => x.DepartureDTTMLocal).Value;
                }
                return result;
            }
        }
        public DateTime CalculateMaxArrivalDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.ArrivalDTTMLocal != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Max(x => x.ArrivalDTTMLocal).Value;
                }
                return result;
            }
        }
        public decimal CalculateSumOfTotalCharge()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.ChargeTotal).Value;
                }
                return result;
            }
        }
        public decimal CalculateSumOfDistance(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    if (orderNum == 1)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == false).Sum(x => x.Distance).Value;
                    }
                    else if (orderNum == 2)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == true).Sum(x => x.Distance).Value;
                    }
                    else
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Sum(x => x.Distance).Value;
                    }
                }
                return result;
            }
        }
        public decimal CalculateSumOfRON()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DayRONCNT != null && x.RemainOverNightCNT != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                }
                return result;
            }
        }
        public decimal CalculateSegFee()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTotSegFee = 0;
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnCqSegAk = 0;
                decimal lnCqSegHi = 0;
                decimal lnIntlSegFeeRate = 0;
                decimal lnDomSegFeeRate = 0;

                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                    {
                        lnTaxRate = QuoteInProgress.FederalTax != null ? (decimal)QuoteInProgress.FederalTax : 0.0M;
                        lnExRate = (FileRequest.ExchangeRate != null && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;

                        FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId company = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                        if (FileRequest.HomebaseID != null)
                            company = GetCompany((long)FileRequest.HomebaseID);
                        else
                        {
                            if (UserPrincipal.Identity._homeBaseId != null)
                                company = GetCompany((long)UserPrincipal.Identity._homeBaseId);
                        }
                        if (company != null)
                        {
                            if (company.SegmentFeeAlaska != null)
                                lnCqSegAk = (decimal)company.SegmentFeeAlaska * lnExRate;
                            if (company.SegementFeeHawaii != null)
                                lnCqSegHi = (decimal)company.SegementFeeHawaii * lnExRate;
                            if (company.ChtQouteIntlSegCHG != null)
                                lnIntlSegFeeRate = (decimal)company.ChtQouteIntlSegCHG * lnExRate;
                            if (company.ChtQouteDOMSegCHG != null)
                                lnDomSegFeeRate = (decimal)company.ChtQouteDOMSegCHG * lnExRate;
                        }
                        foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => (x.IsDeleted == false && x.DAirportID != null && x.AAirportID != null)).ToList())
                        {
                            FlightPak.Web.FlightPakMasterService.GetAllAirport DepartAirport = GetAirport((long)Leg.DAirportID);
                            FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirport = GetAirport((long)Leg.AAirportID);
                            decimal lnDomSegFee = 0;
                            decimal lnIntlSegFee = 0;
                            decimal lnPax_Total = 0;

                            if (DepartAirport != null && ArrAirport != null)
                            {
                                bool DepartRural = false;
                                bool ArrRural = false;
                                if (DepartAirport.IsRural != null)
                                    DepartRural = (bool)DepartAirport.IsRural;
                                if (ArrAirport.IsRural != null)
                                    ArrRural = (bool)ArrAirport.IsRural;

                                if (!ArrRural && !DepartRural)
                                {
                                    string lcCqSegState = string.Empty;
                                    if (DepartAirport.StateName != null && ArrAirport.StateName != null)
                                    {
                                        lcCqSegState = GetStateSegFee(DepartAirport.StateName, ArrAirport.StateName);
                                    }

                                    if ((bool)QuoteInProgress.IsTaxable && (bool)Leg.IsTaxable)
                                    {
                                        switch (lcCqSegState)
                                        {
                                            case "AK":
                                                lnDomSegFeeRate = lnCqSegAk;
                                                break;
                                            case "HI":
                                                lnDomSegFeeRate = lnCqSegHi;
                                                break;
                                        }
                                        lnPax_Total = Leg.PassengerTotal != null ? (decimal)Leg.PassengerTotal : 0;
                                        if (Leg.DutyTYPE == 1)
                                            lnDomSegFee = lnDomSegFeeRate * lnPax_Total; //lnDomSegFee = lnDomSegFeeRate * lnExRate * lnPax_Total;
                                        else if (Leg.DutyTYPE == 2)
                                            lnIntlSegFee = lnIntlSegFeeRate * lnPax_Total; //lnIntlSegFee = lnIntlSegFeeRate * lnExRate * lnPax_Total;

                                        lnTotSegFee += lnDomSegFee + lnIntlSegFee;
                                    }
                                }
                            }
                        }
                        //QuoteInProgress.SegmentFeeTotal = lnTotSegFee;
                        //SaveQuoteinProgressToFileSession();
                    }
                }
                return lnTotSegFee;
            }
        }
        public string GetStateSegFee(string DepartState, string ArrivalState)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartState, ArrivalState))
            {
                string segState = string.Empty;
                if (!string.IsNullOrEmpty(DepartState) && (DepartState == "AK" || DepartState == "HI"))
                    segState = DepartState;
                if (!string.IsNullOrEmpty(DepartState) && (DepartState == "AK" || DepartState == "HI"))
                    segState = ArrivalState;
                return segState;
            }
        }
        public void DoLegCalculationsForQuote()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient CalcService = new CalculationService.CalculationServiceClient())
                {
                    // Loop through Leg level for Calculation
                    if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                    {
                        List<CQLeg> LegList = new List<CQLeg>();
                        LegList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        foreach (CQLeg Leg in LegList)
                        {
                            #region Miles
                            double Miles = 0;
                            if (Leg.DAirportID > 0 && Leg.AAirportID > 0)
                            {
                                Miles = CalcService.GetDistance((long)Leg.DAirportID, (long)Leg.AAirportID);
                            }
                            Leg.Distance = (decimal)Miles;
                            #endregion
                            #region Bias
                            List<double> BiasList = new List<double>();

                            if (Leg.PowerSetting == null)
                                Leg.PowerSetting = "1";

                            if (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID != 0)
                            {
                                FlightPakMasterService.Aircraft TripAircraft = new FlightPakMasterService.Aircraft();
                                TripAircraft = GetAircraft((long)QuoteInProgress.AircraftID);
                                Leg.PowerSetting = TripAircraft.PowerSetting != null ? TripAircraft.PowerSetting : "1";
                            }

                            if (
                                (Leg.DAirportID != null && Leg.DAirportID > 0)
                                &&
                                (Leg.AAirportID != null && Leg.AAirportID > 0)
                                &&
                                (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID > 0)
                                )
                            {
                                BiasList = CalcService.CalcBiasTas((long)Leg.DAirportID, (long)Leg.AAirportID, (long)QuoteInProgress.AircraftID, Leg.PowerSetting);
                                if (BiasList != null)
                                {
                                    // lnToBias, lnLndBias, lnTas
                                    Leg.TakeoffBIAS = (decimal)BiasList[0];
                                    Leg.LandingBIAS = (decimal)BiasList[1];
                                    Leg.TrueAirSpeed = (decimal)BiasList[2];
                                }
                            }
                            else
                            {
                                Leg.TakeoffBIAS = 0.0M;
                                Leg.LandingBIAS = 0.0M; ;
                                Leg.TrueAirSpeed = 0.0M;
                            }
                            #endregion
                            #region "Wind"
                            Leg.WindsBoeingTable = 0.0M;
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;
                                //Function Wind Calculation
                                double Wind = 0;
                                if (
                                (Leg.DAirportID != null && Leg.DAirportID > 0)
                                &&
                                (Leg.AAirportID != null && Leg.AAirportID > 0)
                                &&
                                (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID > 0)
                                )
                                {
                                    Wind = CalcService.GetWind((long)Leg.DAirportID, (long)Leg.AAirportID, Convert.ToInt32(Leg.WindReliability), (long)QuoteInProgress.AircraftID, GetQtr(dt).ToString());
                                    Leg.WindsBoeingTable = (decimal)Wind;
                                }
                            }
                            #endregion
                            #region "ETE"
                            double ETE = 0;
                            // lnToBias, lnLndBias, lnTas
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;
                                ETE = CalcService.GetIcaoEte((double)Leg.WindsBoeingTable, (double)Leg.LandingBIAS, (double)Leg.TrueAirSpeed, (double)Leg.TakeoffBIAS, (double)Leg.Distance, dt);
                                ETE = RoundElpTime(ETE);
                            }
                            Leg.ElapseTM = (decimal)ETE;
                            #endregion
                            #region CalculateDatetime
                            double x10, x11;
                            // int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                            // DateTime Arrivaldt;
                            //try
                            //{
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime ChangedDate;
                                ChangedDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (ChangedDate != null)
                                {
                                    DateTime EstDepartDate = (DateTime)ChangedDate;
                                    //if (QuoteInProgress.EstDepartureDT != null)
                                    //{
                                    //    EstDepartDate = (DateTime)QuoteInProgress.EstDepartureDT;
                                    //}
                                    try
                                    {
                                        //(FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)&&
                                        if ((Leg.DAirportID != null && Leg.DAirportID > 0)
                                            && (Leg.AAirportID != null && Leg.AAirportID > 0)
                                            )
                                        {
                                            if (!string.IsNullOrEmpty(Leg.ElapseTM.ToString()))
                                            {
                                                string ETEstr = "0";

                                                ETEstr = Leg.ElapseTM.ToString();


                                                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                                x10 = (tripleg_elp_time * 60 * 60);
                                                x11 = (tripleg_elp_time * 60 * 60);
                                            }
                                            else
                                            {
                                                x10 = 0;
                                                x11 = 0;
                                            }
                                            DateTime DeptUTCdt;
                                            DateTime ArrUTCdt;

                                            DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;
                                            ArrUTCdt = DeptUTCdt;
                                            ArrUTCdt = ArrUTCdt.AddSeconds(x10);
                                            DateTime ldLocDep = DateTime.MinValue;
                                            DateTime ldLocArr = DateTime.MinValue;
                                            DateTime ldHomDep = DateTime.MinValue;
                                            DateTime ldHomArr = DateTime.MinValue;
                                            ldLocDep = CalcService.GetGMT((long)Leg.DAirportID, DeptUTCdt, false, false);
                                            if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                ldHomDep = CalcService.GetGMT((long)FileRequest.HomeBaseAirportID, DeptUTCdt, false, false);
                                            ldLocArr = CalcService.GetGMT((long)Leg.AAirportID, ArrUTCdt, false, false);
                                            if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                ldHomArr = CalcService.GetGMT((long)FileRequest.HomeBaseAirportID, ArrUTCdt, false, false);

                                            if (Leg.DAirportID > 0)
                                            {
                                                Leg.DepartureDTTMLocal = ldLocDep;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = ldHomDep;
                                                Leg.DepartureGreenwichDTTM = DeptUTCdt;
                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;
                                                Leg.DepartureDTTMLocal = Localdt;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = Localdt;
                                                Leg.DepartureGreenwichDTTM = Localdt;
                                            }

                                            if (Leg.AAirportID > 0)
                                            {
                                                Leg.ArrivalDTTMLocal = ldLocArr;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = ldHomArr;
                                                Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;
                                                Leg.ArrivalDTTMLocal = Localdt;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = Localdt;
                                                Leg.ArrivalGreenwichDTTM = Localdt;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //Manually Handled
                                        if (Leg.ArrivalDTTMLocal == null)
                                        {
                                            Leg.ArrivalDTTMLocal = EstDepartDate;
                                        }
                                        if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                            if (Leg.HomeArrivalDTTM == null)
                                            {
                                                Leg.HomeArrivalDTTM = EstDepartDate;
                                            }

                                        if (Leg.ArrivalGreenwichDTTM == null)
                                        {
                                            Leg.ArrivalGreenwichDTTM = EstDepartDate;
                                        }

                                        if (Leg.DepartureDTTMLocal == null)
                                        {
                                            Leg.DepartureDTTMLocal = EstDepartDate;
                                        }

                                        if (Leg.DepartureGreenwichDTTM == null)
                                        {
                                            Leg.DepartureGreenwichDTTM = EstDepartDate;
                                        }
                                        if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                        {
                                            if (Leg.HomeDepartureDTTM == null)
                                            {
                                                Leg.HomeDepartureDTTM = EstDepartDate;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        #region "PreflightFARRule"
                        double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
                           lnNeededRest, lnWorkArea;

                        lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0;

                        bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                        llRProblem = llFProblem = llDutyBegin = llDProblem = false;

                        DateTime ltGmtDep, ltGmtArr, llDutyend;
                        Int64 lnDuty_RulesID = 0;
                        string lcDuty_Rules = string.Empty;

                        if (QuoteInProgress.CQLegs != null)
                        {
                            List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                            lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                            lnDuty_Hrs = 0;
                            lnRest_Hrs = 0;
                            lnFlt_Hrs = 0;
                            if (Preflegs[0].DepartureGreenwichDTTM != null)
                            {
                                ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                                ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                                llDutyBegin = true;
                                double lnOldDuty_hrs = -1;

                                int legcounter = 0;
                                foreach (CQLeg Leg in Preflegs)
                                {
                                    if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                                    {
                                        double dbElapseTime = 0.0;
                                        long dbCrewDutyRulesID = 0;

                                        if (Leg.ElapseTM != null)
                                            dbElapseTime = (double)Leg.ElapseTM;

                                        //if (Leg.CrewDutyRulesID != null)
                                        //{

                                        //    dbCrewDutyRulesID = (Int64)Leg.CrewDutyRulesID;
                                        //}
                                        //if (dbCrewDutyRulesID != 0)
                                        //{
                                        double lnOverRide = 0;
                                        double lnDutyLeg_Num = 0;

                                        bool llDutyEnd = false;
                                        string FARNum = "";

                                        if (Leg.CrewDutyRulesID != null)
                                            lnDuty_RulesID = (Int64)Leg.CrewDutyRulesID;
                                        else
                                            lnDuty_RulesID = 0;
                                        if (Leg.IsDutyEnd == null)
                                            llDutyEnd = false;
                                        else
                                            llDutyEnd = (bool)Leg.IsDutyEnd;

                                        //if it is last leg then  llDutyEnd = true;
                                        if (lnEndLegNum == Leg.LegNUM)
                                            llDutyEnd = true;

                                        lnOverRide = (double)(Leg.CQOverRide == null ? 0 : Leg.CQOverRide);
                                        lnDutyLeg_Num = (double)Leg.LegNUM;

                                        FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient objectDstsvc = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                                        var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                                      where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                                      select CrewDutyRl).ToList();
                                            if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                            {
                                                FARNum = CrewDtyRule[0].FedAviatRegNum;
                                                lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                                lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                                lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                                lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                                lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                                lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                                lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                            }
                                            else
                                            {
                                                lnDayBeg = 0;
                                                lnDayEnd = 0;
                                                lnMaxDuty = 0;
                                                lnMaxFlt = 0;
                                                lnMinRest = 8;
                                                lnRestMulti = 0;
                                                lnRestPlus = 0;
                                            }
                                        }
                                        TimeSpan? Hoursdiff = (DateTime)Leg.DepartureGreenwichDTTM - ltGmtArr;
                                        if (Leg.ElapseTM != 0)
                                        {
                                            lnFlt_Hrs = lnFlt_Hrs + (double)Leg.ElapseTM;
                                            lnDuty_Hrs = lnDuty_Hrs + (double)Leg.ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                                        }
                                        else
                                        {
                                            lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                                        }

                                        llRProblem = false;

                                        if (llDutyBegin && lnOldDuty_hrs != -1)
                                        {
                                            lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                            if (lnRestMulti > 0)
                                            {
                                                //Here is the Multiple and Plus hours usage
                                                lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                                if (lnRest_Hrs < lnNeededRest)
                                                {
                                                    llRProblem = true;
                                                }
                                            }
                                            else
                                            {
                                                if (lnRest_Hrs < (lnMinRest + lnRestPlus))
                                                {
                                                    llRProblem = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (llDutyBegin)
                                            {
                                                lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                            }
                                        }

                                        if (llDutyEnd)
                                        {
                                            lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                                        }

                                        string lcCdAlert = "";
                                        if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than tripleg.maxflt
                                            lcCdAlert = "F";// Flight time violation and F is stored in tripleg.cdalert as a Flight time error
                                        else
                                            lcCdAlert = "";

                                        if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                            lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in tripleg.cdalert as a Duty time error
                                        // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                        if (llRProblem)
                                            lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in tripleg.cdalert as a Rest time error 

                                        if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                            Leg.State = CQRequestEntityState.Modified;
                                        Leg.FlightHours = (decimal)lnFlt_Hrs;
                                        Leg.DutyHours = (decimal)lnDuty_Hrs;
                                        Leg.RestHours = (decimal)lnRest_Hrs;
                                        Leg.CrewDutyAlert = lcCdAlert;
                                        //Leg.IsDutyEnd = llDutyEnd;
                                        lnLeg_Num = (double)Leg.LegNUM;
                                        llDutyEnd = false;
                                        if (Leg.IsDutyEnd != null)
                                            llDutyEnd = (bool)Leg.IsDutyEnd;

                                        ltGmtArr = (DateTime)Leg.ArrivalGreenwichDTTM;
                                        if ((bool)Leg.IsDutyEnd)
                                        {
                                            llDutyBegin = true;
                                            lnFlt_Hrs = 0;
                                        }
                                        else
                                        {
                                            llDutyBegin = false;
                                        }
                                        lnOldDuty_hrs = lnDuty_Hrs;
                                        //check next leg if available do the below steps
                                        legcounter++;
                                        if (legcounter < Preflegs.Count)
                                        {
                                            if (llDutyEnd)
                                            {
                                                lnDuty_Hrs = 0;

                                                if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                                {
                                                    TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                                    lnRest_Hrs = (hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                                    //ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                    ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                }
                                            }
                                            else
                                            {
                                                lnRest_Hrs = 0;
                                            }
                                        }
                                        //check next leg if available do the below steps

                                        //}
                                    }
                                }
                            }
                        }
                        //}
                        //catch (Exception)
                        //{
                        //    lbTotalDuty.Text = "0.0";
                        //    lbTotalFlight.Text = "0.0";
                        //    lbRest.Text = "0.0";
                        //}
                        #endregion

                        //Prakash
                        //SaveQuoteinProgressToFileSession();
                    }
                }
            }
        }
        private void BindFleetChargeDetails(string fleetID, string aircraftID)
        {
            // Bind Fleet Charge Details into QuoteInProgress Object, based on Tail Number / Type Code
            if (Session[CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[CQSessionKey];

                if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
                {
                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == 1 && x.IsDeleted == false).FirstOrDefault();

                    if (QuoteInProgress != null)
                    {
                        List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

                        if (QuoteInProgress.CQFleetChargeDetails != null)
                        {
                            // Delete the Existing Fleet Charge Details
                            List<CQFleetChargeDetail> ChargeToDeleteList = new List<CQFleetChargeDetail>();
                            ChargeToDeleteList = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList();

                            if (ChargeToDeleteList != null && ChargeToDeleteList.Count > 0)
                            {
                                foreach (CQFleetChargeDetail fleetCharge in ChargeToDeleteList)
                                {
                                    if (fleetCharge.CQFleetChargeDetailID != 0)
                                    {
                                        fleetCharge.IsDeleted = true;
                                        fleetCharge.State = CQRequestEntityState.Deleted;
                                    }
                                    else
                                        QuoteInProgress.CQFleetChargeDetails.Remove(fleetCharge);
                                }
                            }
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            // Get New Charter Rates from Fleet Profile Catalog
                            if (!string.IsNullOrEmpty(fleetID) && fleetID != "0")
                            {
                                FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == Convert.ToInt64(fleetID) && x.AircraftTypeID == null).ToList();
                            }
                            // Get New Charter Rates from Aircraft Type Catalog, if the Source is selected as Type and Tail Number is Null/Empty
                            else if (!string.IsNullOrEmpty(aircraftID) && aircraftID != "0")
                            {
                                FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID == null && (x.AircraftTypeID != null && x.AircraftTypeID == Convert.ToInt64(aircraftID))).ToList();
                            }

                            if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                            {
                                if (QuoteInProgress.CQFleetChargeDetails == null)
                                    QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();

                                foreach (FlightPakMasterService.GetAllFleetNewCharterRate chargeRate in FleetChargeRateList)
                                {
                                    CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                    fleetCharge.State = CQRequestEntityState.Added;
                                    fleetCharge.CQFlightChargeDescription = chargeRate.FleetNewCharterRateDescription;
                                    fleetCharge.ChargeUnit = chargeRate.ChargeUnit;
                                    fleetCharge.NegotiatedChgUnit = chargeRate.NegotiatedChgUnit;
                                    fleetCharge.Quantity = 0;
                                    fleetCharge.BuyDOM = chargeRate.BuyDOM;
                                    fleetCharge.SellDOM = chargeRate.SellDOM;
                                    fleetCharge.IsTaxDOM = chargeRate.IsTaxDOM;
                                    fleetCharge.IsDiscountDOM = chargeRate.IsDiscountDOM;
                                    fleetCharge.BuyIntl = chargeRate.BuyIntl;
                                    fleetCharge.SellIntl = chargeRate.SellIntl;
                                    fleetCharge.IsTaxIntl = chargeRate.IsTaxIntl;
                                    fleetCharge.IsDiscountIntl = chargeRate.IsDiscountIntl;
                                    fleetCharge.FeeAMT = 0;
                                    fleetCharge.OrderNUM = chargeRate.OrderNum;
                                    fleetCharge.IsDeleted = false;

                                    QuoteInProgress.CQFleetChargeDetails.Add(fleetCharge);
                                }
                                //Prakash
                                //SaveQuoteinProgressToFileSession();
                            }
                        }

                    }
                }
            }
        }
        public void CalculateQuantity()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                {
                    foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false))
                    {
                        if (chargeRate.NegotiatedChgUnit != null)
                        {
                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;

                            //Quantity = Sum of ETE where cqfltchg.nchrgunit is 1 
                            if (chargeRate.NegotiatedChgUnit == 1)
                            {
                                if (chargeRate.OrderNUM != null)
                                    chargeRate.Quantity = CalculateSumOfETE((int)chargeRate.OrderNUM);
                            }

                            //Quantity = Sum of Distance where cqfltchg.nchrgunit is 2
                            //Quantity = Sum of Distance multiplied by StatueConvFactor where nchrgunit is 3
                            if (chargeRate.NegotiatedChgUnit == 2)
                            {
                                if (chargeRate.OrderNUM != null)
                                    chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM);
                            }

                            if (chargeRate.NegotiatedChgUnit == 3)
                            {
                                if (chargeRate.OrderNUM != null)
                                    chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * statuateConvFactor;
                            }

                            //Quantity = Total number of legs where nchrgunit is 7
                            if (chargeRate.NegotiatedChgUnit == 7)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                    chargeRate.Quantity = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Count();
                            }

                            if (chargeRate.NegotiatedChgUnit == 8)
                            {
                                if (chargeRate.OrderNUM != null)
                                    chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * 1.852M;
                            }

                            //Quantity = Sum of RON and Day Room of all legs where ordernum is 3 
                            if (chargeRate.OrderNUM == 3)
                            {
                                chargeRate.Quantity = CalculateSumOfRON();
                            }
                        }
                    }
                    //Prakash
                    //SaveQuoteinProgressToFileSession();

                    //BindStandardCharges(true);
                }
            }
        }
        public void CalculateQuoteTotal()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnTotFltHrs = 0;
                decimal lnTotDays = 0;
                decimal lnTotalDays = 0;
                decimal lnMinDays = 0;
                decimal lnMinDailyUsage = 0;
                decimal lnTotLndDsc = 0;
                decimal lnDiscRate = 0;
                decimal lnTotCqDisc = 0;
                decimal lnTotBuyAmt = 0;
                decimal lnTotAdjAmt = 0;
                decimal lnTotTaxAmt = 0;
                decimal lnTotCrwRon = 0;
                decimal lnTotUseAdj = 0;
                decimal lnTotlndfee = 0;
                decimal totchrg = 0;
                decimal lnTotDiscAmt = 0;
                decimal totsub = 0;
                decimal lnTotCost = 0;
                decimal lnTotMargin = 0;
                decimal lnTotMarPct = 0;
                decimal lnTotSegFee = 0;

                string lcFleetType = string.Empty;
                DateTime locarrdt, locdepdt;

                if (QuoteInProgress != null)
                {
                    lnTaxRate = QuoteInProgress.FederalTax != null ? (decimal)QuoteInProgress.FederalTax : 0;
                    lnExRate = (FileRequest.ExchangeRate != null && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;

                    if (QuoteInProgress.SubtotalTotal == null)
                        QuoteInProgress.SubtotalTotal = 0.0M;

                    if (QuoteInProgress.QuoteTotal == null)
                        QuoteInProgress.QuoteTotal = 0.0M;

                    if (QuoteInProgress.CostTotal == null)
                        QuoteInProgress.CostTotal = 0.0M;

                    if (QuoteInProgress.UsageAdjTotal == null)
                        QuoteInProgress.UsageAdjTotal = 0.0M;

                    if (QuoteInProgress.DiscountAmtTotal == null)
                        QuoteInProgress.DiscountAmtTotal = 0.0M;

                    if (QuoteInProgress.FlightChargeTotal == null)
                        QuoteInProgress.FlightChargeTotal = 0.0M;

                    if (QuoteInProgress.IsOverrideCost == null) QuoteInProgress.IsOverrideCost = false;
                    if (QuoteInProgress.IsOverrideDiscountAMT == null) QuoteInProgress.IsOverrideDiscountAMT = false;
                    if (QuoteInProgress.IsOverrideDiscountPercentage == null) QuoteInProgress.IsOverrideDiscountPercentage = false;
                    if (QuoteInProgress.IsOverrideLandingFee == null) QuoteInProgress.IsOverrideLandingFee = false;
                    if (QuoteInProgress.IsOverrideSegmentFee == null) QuoteInProgress.IsOverrideSegmentFee = false;
                    if (QuoteInProgress.IsOverrideTaxes == null) QuoteInProgress.IsOverrideTaxes = false;
                    if (QuoteInProgress.IsOverrideTotalQuote == null) QuoteInProgress.IsOverrideTotalQuote = false;
                    if (QuoteInProgress.IsOverrideUsageAdj == null) QuoteInProgress.IsOverrideUsageAdj = false;
                    if (QuoteInProgress.IsTaxable == null) QuoteInProgress.IsTaxable = false;
                    if (QuoteInProgress.IsDailyTaxAdj == null) QuoteInProgress.IsDailyTaxAdj = false;
                    if (QuoteInProgress.IsLandingFeeTax == null) QuoteInProgress.IsLandingFeeTax = false;

                    if (QuoteInProgress.FleetID != null)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                            var objRetVal = objDstsvc.GetFleetProfileList();
                            if (objRetVal.ReturnFlag)
                            {
                                Fleetlist = objRetVal.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();

                                if (Fleetlist != null && Fleetlist.Count > 0)
                                {
                                    lcFleetType = Fleetlist[0].FleetSize;
                                    if (Fleetlist[0].MinimumDay != null)
                                        lnMinDays = (decimal)Fleetlist[0].MinimumDay;
                                }
                            }
                        }

                        lnTotFltHrs = CalculateSumOfETE(4); // any no greater than three to get all values of ete.
                        locdepdt = CalculateMinDeparttDate();
                        locarrdt = CalculateMaxArrivalDate();
                        totchrg = CalculateSumOfTotalCharge();
                        if (locarrdt != DateTime.MinValue && locdepdt != DateTime.MinValue)
                        {
                            TimeSpan daydiff = locarrdt.Subtract(locdepdt);
                            lnTotalDays = daydiff.Days + 1;

                        }
                        lnTotDays = lnTotalDays;
                        if (lnTotalDays < 1)
                            lnTotalDays = 1;
                        lnMinDailyUsage = lnTotalDays * lnMinDays;
                        lnTotAdjAmt = Math.Round(lnMinDailyUsage - lnTotFltHrs, 2);

                        if (lnTotAdjAmt < 0)
                            lnTotAdjAmt = 0;

                        lnTotLndDsc = 0;
                        lnDiscRate = 0;
                        lnTotCqDisc = 0;
                        lnTotBuyAmt = 0;

                        lnTotSegFee = CalculateSegFee();

                        if (FileRequest.DiscountPercentage != null)
                            lnDiscRate = (decimal)FileRequest.DiscountPercentage;

                        if (QuoteInProgress.IsOverrideDiscountPercentage != null && (bool)QuoteInProgress.IsOverrideDiscountPercentage) // Override Discount % in quote 
                            lnDiscRate = QuoteInProgress.DiscountPercentageTotal != null ? (decimal)QuoteInProgress.DiscountPercentageTotal : 0;
                        else
                            QuoteInProgress.DiscountPercentageTotal = lnDiscRate;

                        if (QuoteInProgress.CQFleetChargeDetails != null)
                        {
                            #region Initializing to 0
                            foreach (CQFleetChargeDetail cqFleetChrgDet in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                            {
                                if (cqFleetChrgDet.CQFleetChargeDetailID != 0)
                                    cqFleetChrgDet.State = CQRequestEntityState.Modified;
                                cqFleetChrgDet.FeeAMT = 0;

                                if (cqFleetChrgDet.Quantity == null)
                                    cqFleetChrgDet.Quantity = 0;


                                if (cqFleetChrgDet.SellDOM == null)
                                    cqFleetChrgDet.SellDOM = 0;

                                if (cqFleetChrgDet.IsDiscountDOM == null)
                                    cqFleetChrgDet.IsDiscountDOM = false;

                                if (cqFleetChrgDet.IsTaxDOM == null)
                                    cqFleetChrgDet.IsTaxDOM = false;

                                if (cqFleetChrgDet.BuyDOM == null)
                                    cqFleetChrgDet.BuyDOM = 0;

                                if (cqFleetChrgDet.SellIntl == null)
                                    cqFleetChrgDet.SellIntl = 0;

                                if (cqFleetChrgDet.IsDiscountIntl == null)
                                    cqFleetChrgDet.IsDiscountIntl = false;

                                if (cqFleetChrgDet.IsTaxIntl == null)
                                    cqFleetChrgDet.IsTaxIntl = false;

                                if (cqFleetChrgDet.BuyIntl == null)
                                    cqFleetChrgDet.BuyIntl = 0;




                            }
                            lnTotCqDisc = 0;
                            //QuoteInProgress.DiscountAmtTotal = 0; //lnTotCqDisc
                            lnTotTaxAmt = 0;
                            //QuoteInProgress.TaxesTotal = 0; //lnTotTaxAmt
                            lnTotBuyAmt = 0;
                            //QuoteInProgress.CostTotal = 0; //lnTotBuyAmt
                            lnTotlndfee = 0;
                            //QuoteInProgress.LandingFeeTotal = 0; //lnTotlndfee
                            totsub = 0;
                            //QuoteInProgress.SubtotalTotal = 0; //totsun
                            lnTotCrwRon = 0;
                            //QuoteInProgress.StdCrewRONTotal =0//lnTotCrwRon
                            #endregion

                            #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition

                            foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3 && x.NegotiatedChgUnit != null && x.NegotiatedChgUnit == 5))
                            {
                                decimal chargeRateDisc = 0.0M;

                                if (chargeRate.CQFleetChargeDetailID != 0)
                                    chargeRate.State = CQRequestEntityState.Modified;


                                chargeRate.FeeAMT = chargeRate.Quantity * chargeRate.SellDOM;

                                // Calculate Discount
                                if ((bool)chargeRate.IsDiscountDOM)
                                {
                                    chargeRateDisc = Math.Round((decimal)chargeRate.FeeAMT * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }

                                // Calculate Tax
                                if ((bool)chargeRate.IsTaxDOM && (bool)QuoteInProgress.IsTaxable && chargeRate.FeeAMT != null)
                                {
                                    decimal taxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        taxAmount = Math.Round(((decimal)chargeRate.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        taxAmount = Math.Round((decimal)chargeRate.FeeAMT * (decimal)lnTaxRate / 100, 2);

                                    lnTotTaxAmt += taxAmount;
                                }

                                decimal buyAmount = 0.0M;
                                buyAmount = (decimal)chargeRate.Quantity * (decimal)chargeRate.BuyDOM;
                                lnTotBuyAmt += buyAmount;

                            }
                            #endregion

                            #region Calculate Landing Fee
                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                            {
                                foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null))
                                {
                                    if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                    {

                                        decimal lnLegLndngFee = 0;
                                        FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirpot = GetAirport((long)Leg.AAirportID);

                                        if (ArrAirpot != null)
                                        {
                                            switch (lcFleetType)
                                            {
                                                case "S":
                                                    lnLegLndngFee = ArrAirpot.LandingFeeSmall != null ? (decimal)ArrAirpot.LandingFeeSmall : 0;
                                                    break;
                                                case "M":
                                                    lnLegLndngFee = ArrAirpot.LandingFeeMedium != null ? (decimal)ArrAirpot.LandingFeeMedium : 0;
                                                    break;
                                                case "L":
                                                    lnLegLndngFee = ArrAirpot.LandingFeeLarge != null ? (decimal)ArrAirpot.LandingFeeLarge : 0;
                                                    break;
                                            }
                                            lnTotlndfee += lnLegLndngFee;
                                        }
                                    }


                                }

                            }
                            #endregion

                            #region Calculate Fee Amount for all the Standard Charges and Additional Fees, Where OrderNum = 1

                            var StandardCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();

                            if (StandardCharge != null && QuoteInProgress.CQLegs != null)
                            {
                                if (StandardCharge.CQFleetChargeDetailID != 0)
                                    StandardCharge.State = CQRequestEntityState.Modified;

                                decimal buyAmount = 0.0M;
                                foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning != null && x.IsPositioning == false))
                                {
                                    decimal lnFeeAmt = 0;

                                    if (leg.ElapseTM == null)
                                        leg.ElapseTM = 0;

                                    if (leg.DutyTYPE == null)
                                        leg.DutyTYPE = 1;


                                    if (StandardCharge.NegotiatedChgUnit == 1)
                                    {
                                        lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                        buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                    }
                                    else if (StandardCharge.NegotiatedChgUnit == 2)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                        buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                    }
                                    else if (StandardCharge.NegotiatedChgUnit == 3)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                        buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                    }
                                    else if (StandardCharge.NegotiatedChgUnit == 8)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                        buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                    }

                                    leg.ChargeTotal = lnFeeAmt;
                                    leg.ChargeRate = (decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl;
                                    StandardCharge.FeeAMT += lnFeeAmt;
                                    lnTotBuyAmt += buyAmount;


                                    // Calculate Discount
                                    decimal chargeRateDisc = 0.0M;
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsDiscountDOM : StandardCharge.IsDiscountIntl))
                                    {
                                        chargeRateDisc = Math.Round((decimal)StandardCharge.FeeAMT * lnDiscRate / 100, 2);
                                        lnTotCqDisc += chargeRateDisc;
                                    }

                                    // Calculate Tax
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsTaxDOM : StandardCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && lnFeeAmt != null)
                                    {
                                        decimal taxAmount = 0.0M;
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                            taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                        else
                                            taxAmount = Math.Round((decimal)lnFeeAmt * lnTaxRate / 100, 2);

                                        lnTotTaxAmt += taxAmount;
                                    }

                                }
                            }
                            #endregion

                            #region Calculate Fee Amount for all Positioning Charges, Where OrderNum = 2
                            var PositionCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 2).FirstOrDefault();

                            if (PositionCharge != null && QuoteInProgress.CQLegs != null)
                            {
                                if (PositionCharge.CQFleetChargeDetailID != 0)
                                    PositionCharge.State = CQRequestEntityState.Modified;

                                decimal buyAmount = 0.0M;
                                foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning != null && x.IsPositioning == true))
                                {
                                    decimal lnFeeAmt = 0;

                                    if (PositionCharge.NegotiatedChgUnit == 1)
                                    {
                                        lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                        buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                    }
                                    else if (PositionCharge.NegotiatedChgUnit == 2)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                        buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                    }
                                    else if (PositionCharge.NegotiatedChgUnit == 3)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                        buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                    }
                                    else if (PositionCharge.NegotiatedChgUnit == 8)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                        buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                    }

                                    leg.ChargeTotal = lnFeeAmt;
                                    leg.ChargeRate = (decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl;

                                    PositionCharge.FeeAMT += lnFeeAmt;
                                    lnTotBuyAmt += buyAmount;

                                    // Calculate Discount
                                    decimal chargeRateDisc = 0.0M;
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsDiscountDOM : PositionCharge.IsDiscountIntl))
                                    {
                                        chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                        lnTotCqDisc += chargeRateDisc;
                                    }

                                    // Calculate Tax
                                    if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsTaxDOM : PositionCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && PositionCharge.FeeAMT != null)
                                    {
                                        decimal taxAmount = 0.0M;
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                            taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                        else
                                            taxAmount = Math.Round((decimal)lnFeeAmt * lnTaxRate / 100, 2);
                                        lnTotTaxAmt += taxAmount;
                                    }
                                }
                            }
                            #endregion

                            #region Calculate Standard Crew RON Charge, Where OrderNum = 3
                            var RonCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 3).FirstOrDefault();

                            if (RonCharge != null && QuoteInProgress.CQLegs != null)
                            {
                                if (RonCharge.CQFleetChargeDetailID != 0)
                                    RonCharge.State = CQRequestEntityState.Modified;

                                decimal domRonQty = 0;
                                decimal intlRonQty = 0;
                                decimal domFeeAmt = 0;
                                decimal intlFeeAmt = 0;
                                decimal domBuyAmt = 0;
                                decimal intlBuyAmt = 0;

                                domRonQty = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DutyTYPE == 1).Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                                intlRonQty = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DutyTYPE == 2).Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;

                                domFeeAmt = (QuoteInProgress.StandardCrewDOM != null ? (decimal)QuoteInProgress.StandardCrewDOM : 1) * (decimal)domRonQty * (decimal)RonCharge.SellDOM;
                                intlFeeAmt = (QuoteInProgress.StandardCrewINTL != null ? (decimal)QuoteInProgress.StandardCrewINTL : 1) * (decimal)intlRonQty * (decimal)RonCharge.SellIntl;

                                domBuyAmt = (QuoteInProgress.StandardCrewDOM != null ? (decimal)QuoteInProgress.StandardCrewDOM : 1) * (decimal)domRonQty * (decimal)RonCharge.BuyDOM;
                                intlBuyAmt = (QuoteInProgress.StandardCrewINTL != null ? (decimal)QuoteInProgress.StandardCrewINTL : 1) * (decimal)intlRonQty * (decimal)RonCharge.BuyIntl;

                                RonCharge.FeeAMT = domFeeAmt + intlFeeAmt;

                                decimal buyAmount = 0.0M;
                                buyAmount = domBuyAmt + intlBuyAmt;
                                lnTotBuyAmt += buyAmount;
                                lnTotCrwRon += (decimal)RonCharge.FeeAMT;

                                decimal domchargeRateDisc = 0.0M;
                                if (RonCharge.IsDiscountDOM != null && (bool)RonCharge.IsDiscountDOM)
                                {
                                    domchargeRateDisc = Math.Round((decimal)domFeeAmt * lnDiscRate / 100, 2);
                                    lnTotCqDisc += domchargeRateDisc;
                                }

                                decimal intlchargeRateDisc = 0.0M;
                                if (RonCharge.IsDiscountIntl != null && (bool)RonCharge.IsDiscountIntl)
                                {
                                    intlchargeRateDisc = Math.Round((decimal)intlFeeAmt * lnDiscRate / 100, 2);
                                    lnTotCqDisc += intlchargeRateDisc;
                                }


                                // Calculate Tax
                                if ((bool)RonCharge.IsTaxDOM && (QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT != null)
                                {
                                    decimal domtaxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        domtaxAmount = Math.Round(((decimal)domFeeAmt - (decimal)domchargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        domtaxAmount = Math.Round((decimal)domFeeAmt * lnTaxRate / 100, 2);

                                    lnTotTaxAmt += domtaxAmount;
                                }

                                if ((bool)RonCharge.IsTaxIntl && (QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT != null)
                                {
                                    decimal intltaxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        intltaxAmount = Math.Round(((decimal)intlFeeAmt - (decimal)intlchargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        intltaxAmount = Math.Round((decimal)intlFeeAmt * lnTaxRate / 100, 2);

                                    lnTotTaxAmt += intltaxAmount;
                                }

                            }
                            #endregion

                            #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3) Condition

                            foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3))
                            {
                                if (chargeRate.CQFleetChargeDetailID != 0)
                                    chargeRate.State = CQRequestEntityState.Modified;

                                decimal buyAmount = 0.0M;

                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                {
                                    foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null))
                                    {
                                        decimal lnFeeAmt = 0;
                                        if (chargeRate.NegotiatedChgUnit == 1)
                                        {
                                            lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 2)
                                        {
                                            lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 3)
                                        {
                                            lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 4)
                                        {
                                            lnFeeAmt = (decimal)((leg.ElapseTM * chargeRate.Quantity) / 100) * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)((leg.ElapseTM * chargeRate.Quantity) / 100) * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 5)
                                        {
                                            // Already Captured in this region - Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 6)
                                        {
                                            lnFeeAmt = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 7)
                                        {
                                            lnFeeAmt = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }
                                        else if (chargeRate.NegotiatedChgUnit == 8)
                                        {
                                            lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                            buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                        }

                                        chargeRate.FeeAMT += lnFeeAmt;
                                        lnTotBuyAmt += buyAmount;

                                        decimal chargeRateDisc = 0.0M;
                                        if (chargeRate.NegotiatedChgUnit != 5)
                                        {
                                            // Calculate Discount
                                            if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl))
                                            {
                                                chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                                lnTotCqDisc += chargeRateDisc;
                                            }

                                            // Calculate Tax
                                            if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl) && (bool)QuoteInProgress.IsTaxable && lnFeeAmt != null)
                                            {
                                                decimal taxAmount = 0.0M;
                                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                                    taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                                else
                                                    taxAmount = Math.Round((decimal)lnFeeAmt * (decimal)lnTaxRate / 100, 2);

                                                lnTotTaxAmt += taxAmount;
                                            }
                                        }

                                    }
                                }
                            }
                            #endregion

                            #region Calculate Adjusted hours TotAdjAmt for first leg and ordernum 1

                            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                            {
                                CQLeg FirstLeg = new CQLeg();
                                FirstLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.LegNUM == 1).SingleOrDefault();
                                lnTotUseAdj = 0;
                                if (FirstLeg != null)
                                {
                                    if (FirstLeg.DutyTYPE == 1)
                                    {
                                        var StandardChargeDet = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();

                                        if (StandardChargeDet != null)
                                        {
                                            if (StandardChargeDet.CQFleetChargeDetailID != 0)
                                                StandardChargeDet.State = CQRequestEntityState.Modified;
                                            decimal buyAmount = 0.0M;

                                            if (QuoteInProgress.IsOverrideUsageAdj != null)
                                                lnTotUseAdj = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.SellDOM : (decimal)StandardChargeDet.SellIntl), 2);

                                            if (QuoteInProgress.IsOverrideUsageAdj != null)
                                                buyAmount = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.BuyDOM : (decimal)StandardChargeDet.BuyIntl), 2);

                                            lnTotBuyAmt += buyAmount;

                                            // Calculate Discount
                                            decimal chargeRateDisc = 0.0M;
                                            if ((bool)(((decimal)FirstLeg.DutyTYPE == 1) ? StandardChargeDet.IsDiscountDOM : StandardChargeDet.IsDiscountIntl))
                                            {
                                                chargeRateDisc = Math.Round((decimal)lnTotUseAdj * lnDiscRate / 100, 2);
                                                lnTotCqDisc += chargeRateDisc;
                                            }

                                            // Calculate Tax
                                            if (QuoteInProgress.IsDailyTaxAdj != null && QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsDailyTaxAdj && (bool)QuoteInProgress.IsTaxable)
                                            {
                                                decimal taxAmount = 0.0M;
                                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                                    taxAmount = Math.Round(((decimal)lnTotUseAdj - (decimal)chargeRateDisc) * (decimal)QuoteInProgress.FederalTax / 100, 2);
                                                else
                                                    taxAmount = Math.Round((decimal)lnTotUseAdj * (decimal)QuoteInProgress.FederalTax / 100, 2);

                                                lnTotTaxAmt += taxAmount;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            totchrg = CalculateSumOfTotalCharge();

                            if (QuoteInProgress.IsOverrideLandingFee != null)
                                lnTotlndfee = (bool)QuoteInProgress.IsOverrideLandingFee ? (decimal)QuoteInProgress.LandingFeeTotal : lnTotlndfee;

                            if (QuoteInProgress.IsLandingFeeTax != null && (bool)QuoteInProgress.IsLandingFeeTax)
                                lnTotTaxAmt += Math.Round((lnTotlndfee * lnTaxRate) / 100, 2);

                            //Sum of fee amount where ordernum > 3
                            decimal totQuoteChrg = 0;
                            totQuoteChrg = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3).Sum(x => x.FeeAMT).Value;

                            QuoteInProgress.SubtotalTotal = totQuoteChrg + totchrg;

                            if (QuoteInProgress.IsOverrideSegmentFee != null && !(bool)QuoteInProgress.IsOverrideSegmentFee)
                                QuoteInProgress.SegmentFeeTotal = lnTotSegFee;

                            if (QuoteInProgress.IsOverrideUsageAdj != null && !(bool)QuoteInProgress.IsOverrideUsageAdj)
                                QuoteInProgress.UsageAdjTotal = lnTotUseAdj;

                            if (QuoteInProgress.IsOverrideLandingFee != null && !(bool)QuoteInProgress.IsOverrideLandingFee)
                                QuoteInProgress.LandingFeeTotal = lnTotlndfee;

                            QuoteInProgress.SubtotalTotal = QuoteInProgress.SubtotalTotal + QuoteInProgress.UsageAdjTotal +
                                    QuoteInProgress.SegmentFeeTotal + lnTotCrwRon + QuoteInProgress.LandingFeeTotal;
                            lnTotDiscAmt = lnTotCqDisc + lnTotLndDsc;

                            if (QuoteInProgress.IsOverrideDiscountAMT != null && !(bool)QuoteInProgress.IsOverrideDiscountAMT)
                                QuoteInProgress.DiscountAmtTotal = lnTotDiscAmt;
                            else
                            {
                                if (QuoteInProgress.DiscountAmtTotal != null)
                                    lnTotDiscAmt = (decimal)QuoteInProgress.DiscountAmtTotal;

                                decimal subTotal = 0;
                                subTotal = ((decimal)QuoteInProgress.SubtotalTotal - ((decimal)QuoteInProgress.LandingFeeTotal + (decimal)QuoteInProgress.SegmentFeeTotal));

                                // Condtion for Attempted to divide by zero Error.
                                if (lnTotDiscAmt != 0 && subTotal != 0)
                                    lnDiscRate = ((lnTotDiscAmt * 100) / subTotal);

                                lnDiscRate = Math.Round(lnDiscRate, 2);
                                QuoteInProgress.DiscountPercentageTotal = lnDiscRate;
                            }

                            if (QuoteInProgress.IsOverrideTaxes != null && !(bool)QuoteInProgress.IsOverrideTaxes)
                                QuoteInProgress.TaxesTotal = lnTotTaxAmt;
                            else if (QuoteInProgress.TaxesTotal != null)
                                lnTotTaxAmt = (decimal)QuoteInProgress.TaxesTotal;

                            if (QuoteInProgress.IsOverrideTotalQuote != null)
                                QuoteInProgress.QuoteTotal = (bool)QuoteInProgress.IsOverrideTotalQuote ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt);
                            QuoteInProgress.FlightHoursTotal = Math.Round(lnTotFltHrs, 2);
                            QuoteInProgress.DaysTotal = lnTotDays;
                            QuoteInProgress.AverageFlightHoursTotal = lnTotDays == 0 ? 0 : Math.Round(lnTotFltHrs / lnTotDays, 2);
                            QuoteInProgress.AdditionalFeeTotalD = totQuoteChrg;
                            QuoteInProgress.StdCrewRONTotal = lnTotCrwRon;
                            if (QuoteInProgress.IsOverrideUsageAdj != null)
                                QuoteInProgress.UsageAdjTotal = !(bool)QuoteInProgress.IsOverrideUsageAdj ? lnTotUseAdj : (decimal)QuoteInProgress.UsageAdjTotal;

                            QuoteInProgress.AdjAmtTotal = lnTotAdjAmt;
                            if (QuoteInProgress.PrepayTotal == null)
                                QuoteInProgress.PrepayTotal = 0;

                            if (QuoteInProgress.IsOverrideTotalQuote != null)
                                QuoteInProgress.RemainingAmtTotal = ((bool)QuoteInProgress.IsOverrideTotalQuote ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt)) - (decimal)QuoteInProgress.PrepayTotal;

                            QuoteInProgress.FlightChargeTotal = totchrg;

                            if (QuoteInProgress.IsOverrideCost != null && (bool)QuoteInProgress.IsOverrideCost)
                                lnTotCost = (decimal)QuoteInProgress.CostTotal;
                            else
                                QuoteInProgress.CostTotal = lnTotBuyAmt;

                            //Margin Calculation
                            lnTotMargin = (decimal)QuoteInProgress.SubtotalTotal - (decimal)QuoteInProgress.CostTotal - (decimal)QuoteInProgress.DiscountAmtTotal;
                            lnTotMarPct = (decimal)QuoteInProgress.SubtotalTotal == 0 ? 0 : Math.Round((lnTotMargin / (decimal)QuoteInProgress.SubtotalTotal) * 100, 2);

                            QuoteInProgress.MarginTotal = lnTotMargin;
                            QuoteInProgress.MarginalPercentTotal = lnTotMarPct;
                        }
                    }
                }
                //Prakash
                //SaveQuoteinProgressToFileSession();
            }
        }

        #endregion
    }
}